namespace isr.VI.Dummy
{

    /// <summary> A session factory. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-29 </para>
    /// </remarks>
    public class SessionFactory : Pith.SessionFactoryBase
    {

        /// <summary> Creates gpib interface session. </summary>
        /// <returns> The new gpib interface session. </returns>
        public override Pith.InterfaceSessionBase GpibInterfaceSession()
        {
            return new GpibInterfaceSession();
        }

        /// <summary> Creates resources manager. </summary>
        /// <returns> The new resources manager. </returns>
        public override Pith.ResourcesProviderBase ResourcesProvider()
        {
            return new ResourcesProvider() { ResourceFinder = new Pith.IntegratedResourceFinder() };
        }

        /// <summary> Creates a session. </summary>
        /// <returns> The new session. </returns>
        public override Pith.SessionBase Session()
        {
            return new Session();
        }
    }
}
