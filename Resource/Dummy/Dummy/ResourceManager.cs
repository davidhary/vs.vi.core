using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace isr.VI.Dummy
{

    /// <summary> Resource Manager Extensions. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class ResourceManager : IDisposable
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        public ResourceManager() : base()
        {
        }

        #region "IDisposable Support"

        /// <summary> True to disposed value. </summary>
        private bool _DisposedValue; // To detect redundant calls


        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this._DisposedValue && disposing )
                {
                }
            }
            finally
            {
                this._DisposedValue = true;
            }
        }

        /// <summary> Finalizes this object. </summary>
        /// <remarks>
        /// David, 2015-11-23. Override Finalize() only if Dispose(disposing As Boolean) above has code
        /// to free unmanaged resources.
        /// </remarks>
        ~ResourceManager()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( false );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            // uncommented the following line because Finalize() is overridden above.
            GC.SuppressFinalize( this );
        }
        #endregion

        #endregion

        #region " Resource Manager "

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A VI.ResourceNameInfo. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public Pith.ResourceNameInfo ParseResource( string resourceName )
        {
            return new Pith.ResourceNameInfo( resourceName );
        }

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources. </summary>
        /// <returns> List of all resources. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IEnumerable<string> FindResources()
        {
            return new List<string>();
        }

        /// <summary> Tries to find resources. </summary>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if resources were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindResources( ref IEnumerable<string> resources )
        {
            resources = Array.Empty<string>();
            return true;
        }

        /// <summary> Tries to find resources. </summary>
        /// <param name="filter">    A pattern specifying the search. </param>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if resources were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindResources( string filter, ref IEnumerable<string> resources )
        {
            resources = new string[] { filter };
            return true;
        }

        /// <summary> Name of the resource. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private string _ResourceName;

        /// <summary> Returns true if the specified resource exists. </summary>
        /// <param name="resourceName"> The resource name. </param>
        /// <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool Exists( string resourceName )
        {
            this._ResourceName = resourceName;
            return true;
        }

        /// <summary> Searches for the interface. </summary>
        /// <param name="resourceName"> The interface resource name. </param>
        /// <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool InterfaceExists( string resourceName )
        {
            this._ResourceName = resourceName;
            return true;
        }

        /// <summary> Searches for all interfaces. </summary>
        /// <returns> The found interface resource names. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IEnumerable<string> FindInterfaces()
        {
            return Array.Empty<string>();
        }

        /// <summary> Tries to find interfaces. </summary>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if interfaces were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindInterfaces( ref IEnumerable<string> resources )
        {
            resources = Array.Empty<string>();
            return true;
        }

        /// <summary> Searches for the interfaces. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IEnumerable<string> FindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            return new string[] { Pith.ResourceNamesManager.BuildInterfaceResourceName( interfaceType, 0 ) };
        }

        /// <summary> Tries to find interfaces. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="resources">     [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if interfaces were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindInterfaces( Pith.HardwareInterfaceType interfaceType, ref IEnumerable<string> resources )
        {
            resources = new string[] { Pith.ResourceNamesManager.BuildInterfaceResourceName( interfaceType, 0 ) };
            return true;
        }

        /// <summary> Searches for the instrument. </summary>
        /// <param name="resourceName"> The instrument resource name. </param>
        /// <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool FindInstrument( string resourceName )
        {
            this._ResourceName = resourceName;
            return true;
        }

        /// <summary> Searches for instruments. </summary>
        /// <returns> The found instrument resource names. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IEnumerable<string> FindInstruments()
        {
            return Array.Empty<string>();
        }

        /// <summary> Tries to find instruments. </summary>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindInstruments( ref IEnumerable<string> resources )
        {
            resources = Array.Empty<string>();
            return true;
        }

        /// <summary> Searches for instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            return new string[] { interfaceType == Pith.HardwareInterfaceType.Gpib
                                                        ? Pith.ResourceNamesManager.BuildGpibInstrumentResource(0, 0)
                                                        : Pith.ResourceNamesManager.BuildUsbInstrumentResource( 0, 0, 1, 1 ) };
        }

        /// <summary> Tries to find instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="resources">     [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindInstruments( Pith.HardwareInterfaceType interfaceType, ref IEnumerable<string> resources )
        {
            resources = new string[] { interfaceType == Pith.HardwareInterfaceType.Gpib
                                                        ? Pith.ResourceNamesManager.BuildGpibInstrumentResource(0, 0)
                                                        : Pith.ResourceNamesManager.BuildUsbInstrumentResource( 0, 0, 1, 1 ) };
            return true;
        }

        /// <summary> Searches for instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType, int boardNumber )
        {
            return new string[] { $"{Pith.ResourceNamesManager.InterfaceBaseName[( int ) interfaceType]}{boardNumber}" };
        }

        /// <summary> Tries to find instruments. </summary>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
        /// <param name="resources">       [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public bool TryFindInstruments( Pith.HardwareInterfaceType interfaceType, int interfaceNumber, ref IEnumerable<string> resources )
        {
            resources = new string[] { $"{Pith.ResourceNamesManager.InterfaceBaseName[( int ) interfaceType]}{interfaceNumber}" };
            return true;
        }

        #endregion

    }
}
