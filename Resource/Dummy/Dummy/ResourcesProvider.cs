namespace isr.VI.Dummy
{

    /// <summary> The VISA resources provider. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class ResourcesProvider : Pith.DummyResourcesProvider
    {
    }
}
