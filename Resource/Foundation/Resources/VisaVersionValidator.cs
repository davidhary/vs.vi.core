using System;
using System.Diagnostics;

using isr.VI.Foundation.ExceptionExtensions;

namespace isr.VI.Foundation
{

    /// <summary> A visa version validator. </summary>
    public sealed class VisaVersionValidator
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        private VisaVersionValidator() : base()
        {
        }
        #endregion

        #region " VALIDATE VISA VERSIONS "

        /// <summary>
        /// Validates the visa assembly and system file versions using settings versions.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaAssemblyVersions()
        {
            var result = ValidateVisaAssemblyVersion();
            if ( result.Success )
                result = ValidateVisaSystemFileVersion();
            return result;
        }

        /// <summary>
        /// Validates the implementation and specification visa versions against settings values.
        /// </summary>
        /// <remarks> David, 2020-04-12. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateFunctionalVisaVersions()
        {
            return string.Equals( ImplementationVersion.ToString(), My.Settings.Default.ImplementationVersion ) ? string.Equals( ImplementationVersion.ToString(), My.Settings.Default.ImplementationVersion ) ? (true, string.Empty) : (false, $"Expected specification version {My.Settings.Default.SpecificationVersion} different from actual {SpecificationVersion}") : (false, $"Expected implementation version {My.Settings.Default.ImplementationVersion} different from actual {ImplementationVersion}");
        }

        /// <summary> Gets the version of this VISA.NET implementation. </summary>
        /// <value> The implementation version. </value>
        public static Version ImplementationVersion => Ivi.Visa.GlobalResourceManager.ImplementationVersion;

        /// <summary> Gets the specification version. </summary>
        /// <value> The specification version. </value>
        public static Version SpecificationVersion => Ivi.Visa.GlobalResourceManager.SpecificationVersion;

        #endregion

        #region " VISA SYSTEM DLL (visa32.dll) FILE VERSION "

        /// <summary> Gets the filename of the VISA system DLL file (x86 is used). </summary>
        /// <value> The the filename of the VISA system DLL file (x86 is used). </value>
        public static string VisaSystemFileName { get; private set; } = "visa32.dll";

        /// <summary> Gets the full name of the VISA system DLL file (x86 is used). </summary>
        /// <value> The full name of the VISA system DLL file (x86 is used). </value>
        public static string VisaSystemFileFullName => System.IO.Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.System ), VisaSystemFileName );

        /// <summary> Gets the visa system file product version. </summary>
        /// <value> The visa system file product version. </value>
        public static string VisaSystemFileProductVersion { get; private set; }

        /// <summary> Reads the file version info of the VISA system DLL file (x86 is used). </summary>
        /// <returns> The file version info of the VISA system DLL file (x86 is used). </returns>
        public static FileVersionInfo ReadVisaSystemFileVersionInfo()
        {
            return FileVersionInfo.GetVersionInfo( VisaSystemFileFullName );
        }

        /// <summary> Validates the VISA system DLL file (x86 is used). </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="expectedVersion"> The expected version. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaSystemFileVersion( string expectedVersion )
        {
            VisaSystemFileProductVersion = ReadVisaSystemFileVersionInfo().ProductVersion;
            return Pith.ResourcesProviderBase.AreEqual( expectedVersion, VisaSystemFileProductVersion ) ? (true, string.Empty) : (false, $"{My.Settings.Default.VisaImplementationVendor} VISA {My.Settings.Default.VisaProductVersion} {nameof( VisaSystemFileProductVersion )} {VisaSystemFileFullName} version {VisaSystemFileProductVersion} different from expected {expectedVersion}");
        }

        /// <summary> Gets the expected VISA system DLL file (x86 is used). </summary>
        /// <value> The expected foundation system file version. </value>
        public static string ExpectedVisaSystemFileVersion
        {
            get {
                if ( Environment.Is64BitProcess )
                {
                    return My.Settings.Default.VisaSystem64FileProductVersion;
                }
                else
                {
                    // IDE is running in 32 bit mode.
                    return My.Settings.Default.VisaSystem32FileProductVersion;
                }
            }
        }

        /// <summary> Validates the VISA system DLL file (x86 is used) version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaSystemFileVersion()
        {
            return ValidateVisaSystemFileVersion( ExpectedVisaSystemFileVersion );
        }

        #endregion

        #region " VISA ASSEMBLY (ivi.visa.dll) FILE VERSION "

        /// <summary> Gets the filename of the .NET visa assembly file name. </summary>
        /// <value> The filename of the .NET visa assembly file name. </value>
        public static string VisaAssemblyFileName { get; private set; } = "ivi.visa.dll";

        /// <summary> Gets the full name of the .NET visa assembly file name. </summary>
        /// <value> The full name of the .NET visa assembly file name. </value>
        public static string VisaAssemblyFileFullName => System.IO.Path.Combine( My.MyProject.Application.Info.DirectoryPath, VisaAssemblyFileName );

        /// <summary> Gets the visa assembly file version. </summary>
        /// <value> The visa assembly file version. </value>
        public static string VisaAssemblyFileVersion { get; private set; }

        /// <summary> Reads the .NET visa assembly file version. </summary>
        /// <returns> The .NET visa assembly file version. </returns>
        public static FileVersionInfo ReadVisaAssemblyFileVersionInfo()
        {
            return FileVersionInfo.GetVersionInfo( VisaAssemblyFileFullName );
        }

        /// <summary> Validates the .NET visa assembly file version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="expectedVersion"> The expected version. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaAssemblyVersion( string expectedVersion )
        {
            VisaAssemblyFileVersion = ReadVisaAssemblyFileVersionInfo().FileVersion;
            return Pith.ResourcesProviderBase.AreEqual( expectedVersion, VisaAssemblyFileVersion ) ? (true, string.Empty) : (false, $"{My.Settings.Default.VisaImplementationVendor} VISA {My.Settings.Default.VisaProductVersion} {nameof( VisaAssemblyFileVersion )} {VisaAssemblyFileFullName} version {VisaAssemblyFileVersion} different from expected {expectedVersion}");
        }

        /// <summary> Validates the .NET visa assembly file version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaAssemblyVersion()
        {
            return ValidateVisaAssemblyVersion( ExpectedVisaAssemblyVersion );
        }

        /// <summary> Gets the expected .NET visa assembly version. </summary>
        /// <value> The expected .NET visa assembly version. </value>
        public static string ExpectedVisaAssemblyVersion => My.Settings.Default.VisaAssemblyVersion;

        #endregion

        #region " VISA PRODUCT VERSION (registry)"

        /// <summary> Gets the registry pathname of the vendor version. </summary>
        /// <value> The the registry pathname of the vendor version. </value>
        public static string VendorVersionPath { get; set; } = My.Settings.Default.VendorVersionPath;

        /// <summary> Gets the name of the vendor version registry key. </summary>
        /// <value> The name of the vendor version registry key. </value>
        public static string VendorVersionKeyName { get; set; } = "CurrentVersion";

        /// <summary> Gets the visa vendor version. </summary>
        /// <value> The visa vendor version. </value>
        public static string VisaVendorVersion { get; private set; }

        /// <summary> Reads the Visa vendor version. </summary>
        /// <returns> The Visa vendor version. </returns>
        public static Version ReadVisaVendorVersion()
        {
            string version = Core.MachineInfo.ReadRegistry( Microsoft.Win32.RegistryHive.LocalMachine, VendorVersionPath, VendorVersionKeyName, "0.0.0.0" );
            return new Version( version );
        }

        /// <summary> Validates the Visa vendor version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="expectedVersion"> The expected version. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static (bool Success, string Details) ValidateVisaVendorVersion( string expectedVersion )
        {
            try
            {
                VisaVendorVersion = ReadVisaVendorVersion().ToString();
                return Pith.ResourcesProviderBase.AreEqual( expectedVersion, VisaVendorVersion ) ? (true, string.Empty) : (false, $"{My.Settings.Default.VisaImplementationVendor} VISA {My.Settings.Default.VisaProductVersion} {nameof( VisaVendorVersion )} version {VisaVendorVersion} different from expected {expectedVersion}");
            }
            catch ( Exception ex )
            {
                return (false, ex.ToString());
            }
        }

        /// <summary> Validates the Visa vendor version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaVendorVersion()
        {
            return ValidateVisaVendorVersion( ExpectedVendorVersion );
        }

        /// <summary> Gets the expected vendor version. </summary>
        /// <value> The expected vendor version. </value>
        public static string ExpectedVendorVersion => My.Settings.Default.VisaProductVersion;

        #endregion


    }
}
