using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.VI.Foundation.ExceptionExtensions;

namespace isr.VI.Foundation
{

    /// <summary> The VISA resources provider. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class ResourcesProvider : Pith.ResourcesProviderBase
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        public ResourcesProvider() : base()
        {
        }

        #region "IDisposable Support"

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception occurred disposing resource manager", $"Exception {ex}" );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " RESOURCE MANAGER "

        /// <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public override bool IsDummy { get; } = false;

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
        public override Pith.ResourceNameInfo ParseResource( string resourceName )
        {
            string activity = $"parsing resource {resourceName}";
            try
            {
                return this.ResourceFinder.ParseResource( resourceName );
            }
            catch ( MissingMethodException ex )
            {
                throw new Core.OperationFailedException( $"Failed {activity}; IVI.Visa assembly {My.Settings.Default.VisaAssemblyVersion} and system file {My.Settings.Default.VisaSystem64FileProductVersion} versions are expected", ex );
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources in the resource names cache. </summary>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources()
        {
            return this.ResourceFinder.FindResources();
        }

        /// <summary> Tries to find resources in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details) TryFindResources()
        {
            return this.ResourceFinder.TryFindResources();
        }

        /// <summary> Lists all resources in the resource names cache. </summary>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources( string filter )
        {
            return this.ResourceFinder.FindResources( filter );
        }

        /// <summary> Tries to find resources in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindResources( string filter )
        {
            return this.ResourceFinder.TryFindResources( filter );
        }

        /// <summary> Returns true if the specified resource exists in the resource names cache. </summary>
        /// <param name="resourceName"> The resource name. </param>
        /// <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
        public override bool Exists( string resourceName )
        {
            return this.ResourceFinder.Exists( resourceName ) && !Pith.ResourceNamesManager.IsTcpipResource( resourceName ) || Pith.ResourceNamesManager.PingTcpipResource( resourceName );
        }

        #endregion

        #region " INTERFACES "

        /// <summary> Searches for the interface in the resource names cache. </summary>
        /// <param name="resourceName"> The interface resource name. </param>
        /// <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
        public override bool InterfaceExists( string resourceName )
        {
            return this.ResourceFinder.InterfaceExists( resourceName );
        }

        /// <summary> Searches for all interfaces in the resource names cache. </summary>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces()
        {
            return this.ResourceFinder.FindInterfaces( Pith.ResourceNamesManager.BuildInterfaceFilter() );
        }

        /// <summary> Try find interfaces in the resource names cache. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInterfaces()
        {
            return this.ResourceFinder.TryFindInterfaces();
        }

        /// <summary> Searches for the interfaces in the resource names cache. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            return this.ResourceFinder.FindInterfaces( Pith.ResourceNamesManager.BuildInterfaceFilter( interfaceType ) );
        }

        /// <summary> Try find interfaces in the resource names cache. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            return this.ResourceFinder.TryFindInterfaces( interfaceType );
        }

        #endregion

        #region " INSTRUMENTS  "

        /// <summary> Searches for the instrument in the resource names cache. </summary>
        /// <param name="resourceName"> The instrument resource name. </param>
        /// <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
        public override bool FindInstrument( string resourceName )
        {
            return this.ResourceFinder.FindInstrument( resourceName );
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments()
        {
            return this.ResourceFinder.FindInstruments( Pith.ResourceNamesManager.BuildInstrumentFilter() );
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of
        /// the.
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments()
        {
            return this.ResourceFinder.TryFindInstruments();
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            return this.ResourceFinder.FindInstruments( Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType ) );
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of
        /// the.
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            return this.ResourceFinder.TryFindInstruments( interfaceType );
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType, int boardNumber )
        {
            return this.ResourceFinder.FindInstruments( Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType, boardNumber ) );
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments( Pith.HardwareInterfaceType interfaceType, int interfaceNumber )
        {
            return this.ResourceFinder.TryFindInstruments( interfaceType, interfaceNumber );
        }

        #endregion

        #region " VERSION VALIDATION "

        /// <summary>
        /// Validates the implementation specification visa versions against settings values.
        /// </summary>
        /// <remarks> David, 2020-04-12. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public override (bool Success, string Details) ValidateFunctionalVisaVersions()
        {
            return VisaVersionValidator.ValidateFunctionalVisaVersions();
        }

        /// <summary> Validates the visa assembly versions against settings values. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public override (bool Success, string Details) ValidateVisaAssemblyVersions()
        {
            return VisaVersionValidator.ValidateVisaAssemblyVersions();
        }

        #endregion

    }
}
