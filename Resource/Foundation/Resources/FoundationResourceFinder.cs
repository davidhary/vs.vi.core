using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Foundation
{

    /// <summary> An IVI Foundation-based Resource Finder. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class FoundationResourceFinder : Pith.ResourceFinderBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        public FoundationResourceFinder() : base()
        {
        }

        #endregion

        #region " CONVERSIONS "

        /// <summary> Converts the given value. </summary>
        /// <param name="value"> The detailed information derived when VISA.NET parses a resource
        /// descriptor. </param>
        /// <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
        [CLSCompliant( false )]
        public static Pith.HardwareInterfaceType ConvertInterfaceType( Ivi.Visa.HardwareInterfaceType value )
        {
            return Enum.IsDefined( typeof( Pith.HardwareInterfaceType ), ( int ) value ) ? ( Pith.HardwareInterfaceType ) Conversions.ToInteger( ( int ) value ) : Pith.HardwareInterfaceType.Gpib;
        }

        /// <summary> Converts the given value. </summary>
        /// <param name="value"> The detailed information derived when VISA.NET parses a resource
        /// descriptor. </param>
        /// <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
        [CLSCompliant( false )]
        public static Ivi.Visa.HardwareInterfaceType ConvertInterfaceType( Pith.HardwareInterfaceType value )
        {
            return Enum.IsDefined( typeof( Ivi.Visa.HardwareInterfaceType ), ( int ) value ) ? ( Ivi.Visa.HardwareInterfaceType ) Conversions.ToInteger( ( int ) value ) : Ivi.Visa.HardwareInterfaceType.Gpib;
        }

        /// <summary> Convert parse result. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <param name="value"> The detailed information derived when VISA.NET parses a resource
        /// descriptor. </param>
        /// <returns> The <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
        [CLSCompliant( false )]
        public static Pith.ResourceNameInfo ConvertParseResult( Ivi.Visa.ParseResult value )
        {
            return new Pith.ResourceNameInfo( value.OriginalResourceName, ConvertInterfaceType( value.InterfaceType ), value.InterfaceNumber );
        }

        /// <summary> Converts a value to a resource open state. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="value"> The detailed information derived when VISA.NET parses a resource
        /// descriptor. </param>
        /// <returns> Value as an isr.VI.Pith.ResourceOpenState. </returns>
        [CLSCompliant( false )]
        public static Pith.ResourceOpenState ToResourceOpenState( Ivi.Visa.ResourceOpenStatus value )
        {
            switch ( value )
            {
                case Ivi.Visa.ResourceOpenStatus.Unknown:
                    {
                        return Pith.ResourceOpenState.Unknown;
                    }

                case Ivi.Visa.ResourceOpenStatus.Success:
                    {
                        return Pith.ResourceOpenState.Success;
                    }

                case Ivi.Visa.ResourceOpenStatus.DeviceNotResponding:
                    {
                        return Pith.ResourceOpenState.DeviceNotResponding;
                    }

                case Ivi.Visa.ResourceOpenStatus.ConfigurationNotLoaded:
                    {
                        return Pith.ResourceOpenState.ConfigurationNotLoaded;
                    }

                default:
                    {
                        throw new InvalidOperationException( $"Unhandled case for {nameof( Ivi.Visa.ResourceOpenStatus )} {value}({( int ) value})" );
                    }
            }
        }

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">          Thrown when a Native error condition occurs. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
        public override Pith.ResourceNameInfo ParseResource( string resourceName )
        {
            string activity = $"parsing resource {resourceName}";
            try
            {
                return ConvertParseResult( Ivi.Visa.GlobalResourceManager.Parse( resourceName ) );
            }
            catch ( MissingMethodException ex )
            {
                throw new Core.OperationFailedException( $"Failed {activity}; IVI.Visa assembly {My.Settings.Default.VisaAssemblyVersion} and system file {My.Settings.Default.VisaSystem64FileProductVersion} versions are expected", ex );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, resourceName, "@DM", "Parsing resource" );
                throw new Pith.NativeException( nativeError, ex );
            }
            catch
            {
                throw;
            }
        }

        /// <summary> Gets or sets the using like pattern. </summary>
        /// <value> The using like pattern. </value>
        public override bool UsingLikePattern { get; } = false;

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources()
        {
            string resources = string.Empty;
            try
            {
                return Ivi.Visa.GlobalResourceManager.Find( Pith.ResourceNamesManager.AllResourcesFilter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, resources, "@DM", "Finding resources" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Lists all resources. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources( string filter )
        {
            try
            {
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, string.Empty, "@DM", "Finding resources" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for all interfaces. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces()
        {
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInterfaceFilter();
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding interfaces" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for the interfaces. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces( string filter )
        {
            try
            {
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding interfaces" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for the interfaces. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) FindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInterfaceFilter( interfaceType );
                var resources = Ivi.Visa.GlobalResourceManager.Find( filter );
                return (resources.Any(), "Resources not found", resources);
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding interfaces" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for instruments. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( string filter )
        {
            try
            {
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for instruments. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments()
        {
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInstrumentFilter();
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for instruments. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType );
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Searches for instruments. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType, int boardNumber )
        {
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType, boardNumber );
                return Ivi.Visa.GlobalResourceManager.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        #endregion

    }
}
