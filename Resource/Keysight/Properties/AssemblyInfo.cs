﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( isr.VI.Keysight.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.VI.Keysight.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.VI.Keysight.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
