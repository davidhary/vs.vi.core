namespace isr.VI.Pith
{

    /// <summary> A session factory for dummy session, interface and resource manager. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-29 </para>
    /// </remarks>
    public class DummySessionFactory : SessionFactoryBase
    {

        /// <summary> Creates gpib interface session. </summary>
        /// <returns> The new gpib interface session. </returns>
        public override InterfaceSessionBase GpibInterfaceSession()
        {
            return new DummyGpibInterfaceSession();
        }

        /// <summary> Creates resources manager. </summary>
        /// <returns> The new resources manager. </returns>
        public override ResourcesProviderBase ResourcesProvider()
        {
            return new DummyResourcesProvider() { ResourceFinder = new IntegratedResourceFinder() };
        }

        /// <summary> Creates a session. </summary>
        /// <returns> The new session. </returns>
        public override SessionBase Session()
        {
            return new DummySession();
        }
    }
}
