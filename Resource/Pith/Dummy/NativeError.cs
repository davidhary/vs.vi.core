using System;
using System.Diagnostics;

using isr.Core.SplitExtensions;

namespace isr.VI.Pith
{

    /// <summary> A dummy native error. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-24 </para>
    /// </remarks>
    public class DummyNativeError : NativeErrorBase
    {

        /// <summary> Constructor. </summary>
        /// <param name="errorCode"> The error code. </param>
        public DummyNativeError( int errorCode ) : base( errorCode )
        {
            this.InitErrorCode( errorCode );
        }

        /// <summary> Initializes the error code. </summary>
        /// <param name="errorCode"> The error code. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitErrorCode( int errorCode )
        {
            try
            {
                this._ErrorCodeName = errorCode.ToString();
            }
            catch ( Exception ex )
            {
                this._ErrorCodeName = "UnknownError";
                Debug.Assert( !Debugger.IsAttached, $"Check the code {ex}" );
            }

            this._ErrorCodeDescription = this._ErrorCodeName.SplitWords();
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <param name="errorCode">       The error code. </param>
        /// <param name="resourceName">    Name of the resource. </param>
        /// <param name="lastMessageSent"> The last message sent. </param>
        /// <param name="lastAction">      The last action. </param>
        public DummyNativeError( int errorCode, string resourceName, string lastMessageSent, string lastAction ) : base( errorCode, resourceName, lastMessageSent, lastAction )
        {
            this.InitErrorCode( errorCode );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <param name="errorCode">       The error code. </param>
        /// <param name="resourceName">    Name of the resource. </param>
        /// <param name="nodeNumber">      The node number. </param>
        /// <param name="lastMessageSent"> The last message sent. </param>
        /// <param name="lastAction">      The last action. </param>
        public DummyNativeError( int errorCode, string resourceName, int nodeNumber, string lastMessageSent, string lastAction ) : base( errorCode, resourceName, nodeNumber, lastMessageSent, lastAction )
        {
            this.InitErrorCode( errorCode );
        }

        /// <summary> Information describing the error code. </summary>
        private string _ErrorCodeDescription;

        /// <summary> Gets information describing the error code. </summary>
        /// <value> Information describing the error code. </value>
        public override string ErrorCodeDescription => this._ErrorCodeDescription;

        /// <summary> Name of the error code. </summary>
        private string _ErrorCodeName;

        /// <summary> Gets the name of the error code. </summary>
        /// <value> The name of the error code. </value>
        public override string ErrorCodeName => this._ErrorCodeName;
    }
}
