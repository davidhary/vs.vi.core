using System;
using System.Diagnostics;

using isr.VI.Pith.ExceptionExtensions;

namespace isr.VI.Pith
{

    /// <summary> A Dummy GPIB interface session. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-21 </para>
    /// </remarks>
    public class DummyGpibInterfaceSession : InterfaceSessionBase
    {

        #region " CONSTRUCTOR "

        /// <summary> Constructor. </summary>
        public DummyGpibInterfaceSession() : base()
        {
        }

        #region " Disposable Support"


        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    try
                    {
                        this.CloseSession();
                    }
                    catch ( Exception ex )
                    {
                        Debug.Assert( !Debugger.IsAttached, "Failed discarding enabled events.", $"Failed discarding enabled events. {ex.ToFullBlownString()}" );
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion
        #endregion

        #region " SESSION "

        /// <summary> Gets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public override bool IsDummy { get; } = true;

        #endregion

        #region " GPIB INTERFACE "

        /// <summary> Sends the interface clear. </summary>
        public override void SendInterfaceClear()
        {
        }

        /// <summary> Gets the type of the hardware interface. </summary>
        /// <value> The type of the hardware interface. </value>
        public override HardwareInterfaceType HardwareInterfaceType => HardwareInterfaceType.Gpib;

        /// <summary> Gets the hardware interface number. </summary>
        /// <value> The hardware interface number. </value>
        public override int HardwareInterfaceNumber => 0;

        /// <summary> Returns all instruments to some default state. </summary>
        public override void ClearDevices()
        {
        }

        /// <summary> Clears the specified device. </summary>
        /// <param name="gpibAddress"> The instrument address. </param>
        public override void SelectiveDeviceClear( int gpibAddress )
        {
        }

        /// <summary> Clears the specified device. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        public override void SelectiveDeviceClear( string resourceName )
        {
        }

        /// <summary> Clears the interface. </summary>
        public override void ClearInterface()
        {
        }

        #endregion

    }
}
