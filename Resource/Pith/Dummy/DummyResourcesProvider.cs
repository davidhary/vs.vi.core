using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.VI.Pith.ExceptionExtensions;

namespace isr.VI.Pith
{

    /// <summary> A Dummy local visa resources manager. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class DummyResourcesProvider : ResourcesProviderBase
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        public DummyResourcesProvider() : base()
        {
        }

        #region "IDisposable Support"


        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception occurred disposing resource manager", $"Exception {ex.ToFullBlownString()}" );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " RESOURCE MANAGER "

        /// <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public override bool IsDummy => true;

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A VI.ResourceNameInfo. </returns>
        public override ResourceNameInfo ParseResource( string resourceName )
        {
            return new ResourceNameInfo( resourceName );
        }

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources in the resource names cache. </summary>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources()
        {
            return new List<string>();
        }

        /// <summary> Tries to find resources in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details) TryFindResources()
        {
            return (true, string.Empty);
        }

        /// <summary> Lists all resources in the resource names cache. </summary>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources( string filter )
        {
            return new List<string>();
        }

        /// <summary> Tries to find resources in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindResources( string filter )
        {
            return (true, string.Empty, Array.Empty<string>());
        }

        /// <summary> Returns true if the specified resource exists in the resource names cache. </summary>
        /// <param name="resourceName"> The resource name. </param>
        /// <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
        public override bool Exists( string resourceName )
        {
            return true;
        }

        #endregion

        #region " INTERFACES "

        /// <summary> Searches for the interface in the resource names cache. </summary>
        /// <param name="resourceName"> The interface resource name. </param>
        /// <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
        public override bool InterfaceExists( string resourceName )
        {
            return true;
        }

        /// <summary> Searches for all interfaces in the resource names cache. </summary>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces()
        {
            return new List<string>();
        }

        /// <summary> Try find interfaces in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInterfaces()
        {
            return (true, string.Empty, Array.Empty<string>());
        }

        /// <summary> Searches for the interfaces in the resource names cache. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces( HardwareInterfaceType interfaceType )
        {
            return new List<string>();
        }

        /// <summary> Try find interfaces in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInterfaces( HardwareInterfaceType interfaceType )
        {
            return (true, string.Empty, Array.Empty<string>());
        }

        #endregion

        #region " INSTRUMENTS  "

        /// <summary> Searches for the instrument in the resource names cache. </summary>
        /// <param name="resourceName"> The instrument resource name. </param>
        /// <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
        public override bool FindInstrument( string resourceName )
        {
            return true;
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments()
        {
            return new List<string>();
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments()
        {
            return (true, string.Empty, Array.Empty<string>());
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( HardwareInterfaceType interfaceType )
        {
            return new List<string>();
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments( HardwareInterfaceType interfaceType )
        {
            return (true, string.Empty, Array.Empty<string>());
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( HardwareInterfaceType interfaceType, int boardNumber )
        {
            return new List<string>();
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments( HardwareInterfaceType interfaceType, int interfaceNumber )
        {
            return (true, string.Empty, Array.Empty<string>());
        }

        #endregion

        #region " VALIDATE VISA VERSION "


        /// <summary>
        /// Validates the specification and implementation visa versions against settings values.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public override (bool Success, string Details) ValidateFunctionalVisaVersions()
        {
            return (true, string.Empty);
        }

        /// <summary> Validates the visa assembly versions against settings values. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public override (bool Success, string Details) ValidateVisaAssemblyVersions()
        {
            return (true, string.Empty);
        }

        #endregion

    }
}
