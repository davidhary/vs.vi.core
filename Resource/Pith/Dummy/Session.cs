using System;

namespace isr.VI.Pith
{

    /// <summary> A Dummy message based session. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-20 </para>
    /// </remarks>
    public class DummySession : SessionBase
    {

        #region " CONSTRUCTION and CLEANUP "


        /// <summary>
        /// Initializes a new instance of the MessageBasedSession object from the specified resource name.
        /// </summary>
        public DummySession() : base()
        {
        }

        #endregion

        #region " SESSION "

        /// <summary> Gets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public override bool IsDummy { get; } = true;


        /// <summary>
        /// Gets the session open sentinel. When open, the session is capable of addressing the hardware.
        /// See also <see cref="P:VI.Pith.SessionBase.IsDeviceOpen" />.
        /// </summary>
        /// <value> The is session open. </value>
        public override bool IsSessionOpen => this.IsDeviceOpen;

        /// <summary> Initializes a dummy session. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <param name="timeout">      The open timeout. </param>
        protected override void CreateSession( string resourceName, TimeSpan timeout )
        {
            this._IsSessionDisposed = false;
            this.ClearLastError();
        }

        /// <summary> Discards session. </summary>
        protected override void DiscardAllEvents()
        {
            this.ClearLastError();
        }

        private bool _IsSessionDisposed;
        /// <summary> Gets or sets the sentinel indication that the VISA session is disposed. </summary>
        /// <value> The is session disposed. </value>
        public override bool IsSessionDisposed => this._IsSessionDisposed;

        /// <summary> Dispose session. </summary>
        protected override void DisposeSession()
        {
            this._IsSessionDisposed = true;
        }


        /// <summary>
        /// Checks if the candidate resource name exists. If so, assign to the
        /// <see cref="isr.VI.Pith.SessionBase.ValidatedResourceName">validated resource name</see>
        /// </summary>
        /// <returns> <c>true</c> if it the resource exists; otherwise <c>false</c> </returns>
        public override bool ValidateCandidateResourceName()
        {
            using var rm = new DummyResourcesProvider();
            return this.ValidateCandidateResourceName( rm );
        }


        /// <summary>
        /// Gets or sets the sentinel indicating if call backs are performed in a specific
        /// synchronization context.
        /// </summary>
        /// <remarks>
        /// For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object marshals
        /// callbacks across threads appropriately.<para>
        /// DH: 3339 Setting true prevents display.
        /// </para><para>
        /// Note that setting to false also breaks display updates.
        /// </para>
        /// </remarks>
        /// <value>
        /// The sentinel indicating if call backs are performed in a specific synchronization context.
        /// </value>
        public override bool SynchronizeCallbacks { get; set; }

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Gets read buffer size. </summary>
        /// <returns> The read buffer size. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private int Get_ReadBufferSize()
        {
            return 1024;
        }

        #endregion

        #region " READ/WRITE "

        /// <summary> The communication timeout. </summary>
        private TimeSpan _CommunicationTimeout = My.Settings.Default.DefaultOpenSessionTimeout;

        /// <summary> Gets or sets the communication timeout. </summary>
        /// <value> The communication timeout. </value>
        public override TimeSpan CommunicationTimeout
        {
            get => this._CommunicationTimeout;

            set {
                if ( this.CommunicationTimeout != value )
                {
                    this._CommunicationTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The termination character. </summary>
        private byte _TerminationCharacter = Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineValue;

        /// <summary> Gets or sets the termination character. </summary>
        /// <value> The termination character. </value>
        public override byte ReadTerminationCharacter
        {
            get => this._TerminationCharacter;

            set {
                if ( this.ReadTerminationCharacter != value )
                {
                    this._TerminationCharacter = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True to enable, false to disable the termination character. </summary>
        private bool _TerminationCharacterEnabled = true;

        /// <summary> Gets or sets the termination character enabled. </summary>
        /// <value> The termination character enabled. </value>
        public override bool ReadTerminationCharacterEnabled
        {
            get => this._TerminationCharacterEnabled;

            set {
                if ( this.ReadTerminationCharacterEnabled != value )
                {
                    this._TerminationCharacterEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Synchronously reads ASCII-encoded string data. </summary>
        /// <returns> The received message. </returns>
        public override string ReadFreeLine()
        {
            this.ClearLastError();
            this.LastMessageReceived = this.EmulatedReply;
            return this.LastMessageReceived;
        }


        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
        /// data with the <see cref="ReadTerminationCharacter">termination character</see>. <para>
        /// Per IVI documentation: Appends a newline (0xA) to the formatted I/O write buffer, flushes the
        /// buffer, and sends an End-of-Line with the buffer if required.</para>
        /// </summary>
        /// <returns> The received message. </returns>
        public override string ReadFiniteLine()
        {
            this.ClearLastError();
            this.LastMessageReceived = this.EmulatedReply;
            return this.LastMessageReceived;
        }


        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
        /// data with the <see cref="ReadTerminationCharacter">termination character</see>.
        /// </summary>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        protected override string SyncWriteLine( string dataToWrite )
        {
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                this.ClearLastError();
                this.LastMessageSent = dataToWrite;
            }

            return dataToWrite;
        }


        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.<para>
        /// Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
        /// formatted I/O write buffer</para>
        /// </summary>
        /// <remarks> David, 2020-07-23. </remarks>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        protected override string SyncWrite( string dataToWrite )
        {
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                this.ClearLastError();
                this.LastMessageSent = dataToWrite;
            }

            return dataToWrite;
        }

        /// <summary> Gets the size of the input buffer. </summary>
        /// <value> The size of the input buffer. </value>
        public override int InputBufferSize => 4096;

        #endregion

        #region " REGISTERS "

        /// <summary> Reads status byte. </summary>
        /// <returns> The status byte. </returns>
        protected override ServiceRequests ThreadUnsafeReadStatusByte()
        {
            this.ClearLastError();
            this.StatusByte = this.EmulatedStatusByte;
            return this.StatusByte;
        }

        /// <summary> Clears the device. </summary>
        protected override void Clear()
        {
            this.ClearLastError();
        }

        #endregion

        #region " EVENTS "

        /// <summary> Discard service requests. </summary>
        public override void DiscardServiceRequests()
        {
            this.ClearLastError();
        }

        /// <summary>   Awaits service request status. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="timeout">  The open timeout. </param>
        /// <returns>   A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
        public override (bool TimedOut, ServiceRequests StatusByte, TimeSpan Elapsed) AwaitServiceRequest( TimeSpan timeout )
        {
            this.MakeEmulatedReplyIfEmpty( this.DefaultServiceRequestEnableBitmask );
            return (false, this.DefaultServiceRequestEnableBitmask, TimeSpan.Zero);
        }

        /// <summary> True to enable, false to disable the service request event handler. </summary>
        private bool _ServiceRequestEventHandlerEnabled;


        /// <summary>
        /// Gets or sets or set (protected) the sentinel indication if a service request event handler
        /// was enabled and registered.
        /// </summary>
        /// <value>
        /// <c>True</c> if service request event is enabled and registered; otherwise, <c>False</c>.
        /// </value>
        public override bool ServiceRequestEventEnabled
        {
            get => this._ServiceRequestEventHandlerEnabled;

            set {
                if ( this.ServiceRequestEventEnabled != value )
                {
                    this._ServiceRequestEventHandlerEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Enables the service request. </summary>
        public override void EnableServiceRequestEventHandler()
        {
            if ( !this.ServiceRequestEventEnabled )
            {
                this.ClearLastError();
                this.ServiceRequestEventEnabled = true;
            }
        }

        /// <summary> Disables the service request. </summary>
        public override void DisableServiceRequestEventHandler()
        {
            if ( this.ServiceRequestEventEnabled )
            {
                this.ClearLastError();
                this.ServiceRequestEventEnabled = false;
            }
        }

        #endregion

        #region " TRIGGER "


        /// <summary>
        /// Asserts a software or hardware trigger depending on the interface; Sends a bus trigger.
        /// </summary>
        public override void AssertTrigger()
        {
            this.ClearLastError();
        }

        #endregion

        #region " INTERFACE "

        /// <summary> Clears the interface. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        protected override void ImplementClearHardwareInterface()
        {
            if ( this.SupportsClearInterface && this.IsSessionOpen )
            {
                using var gi = new DummyGpibInterfaceSession();
                gi.OpenSession( this.ResourceNameInfo.InterfaceResourceName );
                if ( gi.IsOpen )
                {
                    gi.SelectiveDeviceClear( this.OpenResourceName );
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed opening GPIB Interface Session {this.ResourceNameInfo.InterfaceResourceName}" );
                }
            }
        }

        /// <summary> Clears the device (SDC). </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        protected override void ClearDevice()
        {
            this.Clear();
            if ( this.SupportsClearInterface && this.IsSessionOpen )
            {
                using var gi = new DummyGpibInterfaceSession();
                gi.OpenSession( this.ResourceNameInfo.InterfaceResourceName );
                if ( gi.IsOpen )
                {
                    gi.SelectiveDeviceClear( this.OpenResourceName );
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed opening GPIB Interface Session {this.ResourceNameInfo.InterfaceResourceName}" );
                }
            }
        }

        #endregion

        #region " ICS 9065 KEEP ALIVE "

        /// <summary> Sends a TCP/IP message to keep the ICS 9065 connection from timing out. </summary>
        /// <returns> <c>true</c> if success; otherwise <c>false</c> </returns>
        public override void KeepAlive()
        {
        }

        #endregion

    }
}
