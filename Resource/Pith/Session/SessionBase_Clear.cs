using System;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        #region " CLS "

        /// <summary> The clear refractory period. </summary>
        private TimeSpan _ClearRefractoryPeriod;

        /// <summary> Gets or sets the clear refractory period. </summary>
        /// <value> The clear refractory period. </value>
        public TimeSpan ClearRefractoryPeriod
        {
            get => this._ClearRefractoryPeriod;

            set {
                if ( !value.Equals( this.ClearRefractoryPeriod ) )
                {
                    this._ClearRefractoryPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the clear execution state command. </summary>
        /// <remarks>
        /// SCPI: "*CLS".
        /// <see cref="VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand"> </see>
        /// </remarks>
        /// <value> The clear execution state command. </value>
        public string ClearExecutionStateCommand { get; set; } = Ieee488.Syntax.ClearExecutionStateCommand;

        /// <summary> Returns <c>True</c> if the instrument supports clear execution state. </summary>
        /// <value> <c>True</c> if the instrument supports clear execution state. </value>
        public bool SupportsClearExecutionState => !string.IsNullOrEmpty( this.ClearExecutionStateCommand );

        /// <summary> Sets the known clear state. </summary>
        /// <remarks> Sends the '*CLS' message. '*CLS' clears the error queue. </remarks>
        public virtual void ClearExecutionState()
        {
            // testing using OPC in place of the refractory periods.
            this.OperationCompleted = new bool?();
            if ( this.IsSessionOpen )
            {
                if ( this.SupportsClearExecutionState )
                {
                    if ( this.SupportsOperationComplete )
                    {
                        _ = this.QueryOperationCompleted();
                    }
                    else
                    {
                        Core.ApplianceBase.DoEventsWait( this.StatusReadDelay );
                        // was before clear; should be after?
                        // isr.Core.ApplianceBase.DoEvents()
                        // isr.Core.ApplianceBase.DoEventsWait(Me.ClearRefractoryPeriod)
                    }

                    // !@# was before clear; should be after?
                    isr.Core.ApplianceBase.DoEvents();
                    isr.Core.ApplianceBase.DoEventsWait( this.ClearRefractoryPeriod );
                    this.Execute( this.ClearExecutionStateCommand );
                }

                if ( this.SupportsOperationComplete )
                {
                    // !@#
                    Core.ApplianceBase.DoEventsWait( this.ClearRefractoryPeriod );
                    _ = this.QueryOperationCompleted();
                }
                else
                {
                    // was before clear; should be after?
                    Core.ApplianceBase.DoEventsWait( this.ClearRefractoryPeriod );
                    this.OperationCompleted = true;
                }

                Core.ApplianceBase.DoEventsWait( this.StatusReadDelay );
                _ = this.ReadStatusRegister();
            }
        }

        #endregion

        #region " RST "

        /// <summary> The reset refractory period. </summary>
        private TimeSpan _ResetRefractoryPeriod;

        /// <summary> Gets or sets the reset refractory period. </summary>
        /// <value> The reset refractory period. </value>
        public TimeSpan ResetRefractoryPeriod
        {
            get => this._ResetRefractoryPeriod;

            set {
                if ( !value.Equals( this.ResetRefractoryPeriod ) )
                {
                    this._ResetRefractoryPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the reset known state command. </summary>
        /// <remarks>
        /// SCPI: "*RST".
        /// <see cref="VI.Pith.Ieee488.Syntax.ResetKnownStateCommand"> </see>
        /// </remarks>
        /// <value> The reset known state command. </value>
        public string ResetKnownStateCommand { get; set; } = Ieee488.Syntax.ResetKnownStateCommand;

        /// <summary> Returns <c>True</c> if the instrument supports reset known state. </summary>
        /// <value> <c>True</c> if the instrument supports reset known state. </value>
        public bool SupportsResetKnownStateState => !string.IsNullOrEmpty( this.ResetKnownStateCommand );


        /// <summary>
        /// Sends a <see cref="ResetKnownStateCommand">reset (RST)</see> command to the instrument to set
        /// the known reset (default) state.
        /// </summary>
        /// <remarks> Sends the '*RST' message. </remarks>
        public void ResetKnownState()
        {
            // To_DO: move to status subsystem and test for busy.
            this.ResetRegistersKnownState();
            this.OperationCompleted = new bool?();
            if ( this.IsSessionOpen )
            {
                if ( this.SupportsResetKnownStateState )
                {
                    if ( this.SupportsOperationComplete )
                    {
                        _ = this.QueryOperationCompleted();
                    }
                    else
                    {
                        Core.ApplianceBase.DoEventsWait( this.StatusReadDelay );
                    }

                    this.Execute( this.ResetKnownStateCommand );
                }

                if ( this.SupportsOperationComplete )
                {
                    // !@#
                    Core.ApplianceBase.DoEventsWait( this.ResetRefractoryPeriod );
                    _ = this.QueryOperationCompleted();
                }
                else
                {
                    Core.ApplianceBase.DoEventsWait( this.ResetRefractoryPeriod );
                    this.OperationCompleted = true;
                }

                Core.ApplianceBase.DoEventsWait( this.StatusReadDelay );
                _ = this.ReadStatusRegister();
            }
        }

        #endregion

        #region " STATUS AND READ DELAYS "


        /// <summary>
        /// Delays operations by waiting for a task to complete. The delay is preceded and followed with
        /// <see cref="isr.Core.ApplianceBase.DoEvents"/>
        /// to release messages currently in the message queue.
        /// </summary>
        /// <returns> A TimeSpan. </returns>
        public TimeSpan DoEventsReadDelay()
        {
            return Core.ApplianceBase.DoEventsWaitElapsed( this.ReadDelay );
        }

        /// <summary> The read delay. </summary>
        private TimeSpan _ReadDelay;


        /// <summary>
        /// Gets or sets the to delay time to apply when reading immediately after a write.
        /// </summary>
        /// <value> The read delay. </value>
        public TimeSpan ReadDelay
        {
            get => this._ReadDelay;

            set {
                if ( !value.Equals( this._ReadDelay ) )
                {
                    this._ReadDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        /// <summary>
        /// Delays operations by waiting for a task to complete. The delay is preceded and followed with
        /// <see cref="isr.Core.ApplianceBase.DoEvents"/>
        /// to release messages currently in the message queue.
        /// </summary>
        /// <returns> A TimeSpan. </returns>
        public TimeSpan DoEventsStatusReadDelay()
        {
            return Core.ApplianceBase.DoEventsWaitElapsed( this.StatusReadDelay );
        }

        /// <summary> The status read delay. </summary>
        private TimeSpan _StatusReadDelay;


        /// <summary>
        /// Gets or sets the to delay time to apply when reading the status byte after a write.
        /// </summary>
        /// <value> The read delay. </value>
        public TimeSpan StatusReadDelay
        {
            get => this._StatusReadDelay;

            set {
                if ( !value.Equals( this._StatusReadDelay ) )
                {
                    this._StatusReadDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
