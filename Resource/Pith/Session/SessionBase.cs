using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using isr.Core;
using isr.Core.EscapeSequencesExtensions;
using isr.VI.Pith.ExceptionExtensions;

namespace isr.VI.Pith
{

    /// <summary> Base class for SessionBase. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public abstract partial class SessionBase : Core.Models.ViewModelBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized constructor for use only by derived class. </summary>
        protected SessionBase() : base()
        {
            this._OperationCompleted = new bool?();
            // Me._ResourceNameInfo = New ResourceNameInfo
            this.Enabled = true;
            this._LastMessageReceived = string.Empty;
            this._LastMessageSent = string.Empty;
            this.UseDefaultTerminationThis();
            this.InitializeServiceRequestRegisterBitsThis();
            this.CommunicationTimeouts = new Stack<TimeSpan>();
            My.Settings.Default.PropertyChanged += this.HandleSettingsPropertyChanged;
            this._DeviceClearRefractoryPeriod = TimeSpan.FromMilliseconds( 1050d );
            this._InterfaceClearRefractoryPeriod = TimeSpan.FromMilliseconds( 1050d );
            this._ResetRefractoryPeriod = TimeSpan.FromMilliseconds( 200d );
            this._ClearRefractoryPeriod = TimeSpan.FromMilliseconds( 100d );
            this._ReadDelay = TimeSpan.Zero;
            this._StatusReadDelay = TimeSpan.Zero;
            this.TimeoutCandidate = TimeSpan.FromMilliseconds( 3000d );
            this._ServiceRequestEnableCommand = string.Format( Ieee488.Syntax.ServiceRequestEnableCommandFormat, 16 ) + Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineEscape;
            this._DefaultOperationCompleteBitmask = ServiceRequests.StandardEvent;
            this._DefaultOperationServiceRequestEnableBitmask = ServiceRequests.All & ~ServiceRequests.RequestingService & ~ServiceRequests.MessageAvailable;
            this._DefaultServiceRequestEnableBitmask = ServiceRequests.All & ~ServiceRequests.RequestingService;
            this._DefaultStandardEventEnableBitmask = StandardEvents.All & ~StandardEvents.RequestControl;
            this._OperationCompleteExpectedBitmask = this._DefaultOperationCompleteBitmask;
            this._ServiceRequestWaitCompleteEnabledBitmask = this._DefaultOperationCompleteBitmask;
            this._StandardEventWaitCompleteEnabledBitmask = this._DefaultStandardEventEnableBitmask;
        }

        /// <summary> Validates the given visa session. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The visa session. </param>
        /// <returns> A SessionBase. </returns>
        public static SessionBase Validated( SessionBase session )
        {
            return session is null ? throw new ArgumentNullException( nameof( session ) ) : session;
        }

        #region " Disposable Support "


        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status. </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }


        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        this.MessageNotificationLevel = NotifySyncLevel.None;
                        this.LastNativeError = null;
                        this.DiscardAllEvents();
                        this.RemoveServiceRequestedEventHandlers();
                        this.RemovePropertyChangedEventHandlers();
                        this.CommunicationTimeouts.Clear();
                        if ( this.StatusByteLocker is object )
                        {
                            this.StatusByteLocker.Dispose();
                            this.StatusByteLocker = null;
                        }
                    }
                    // dispose of unmanaged code here; unmanaged code gets disposed if disposing or not.
                    My.Settings.Default.PropertyChanged -= this.HandleSettingsPropertyChanged;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary> Finalizes this object. </summary>
        /// <remarks>
        /// Overrides should Dispose(disposing As Boolean) has code to free unmanaged resources.
        /// </remarks>
        ~SessionBase()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " SESSION "


        /// <summary>
        /// Gets or sets the Enabled sentinel. When enabled, the session is allowed to engage actual
        /// hardware; otherwise, opening the session does not attempt to link to the hardware.
        /// </summary>
        /// <value> The enabled sentinel. </value>
        public bool Enabled { get; set; }

        /// <summary> Gets or sets the default open timeout. </summary>
        /// <value> The default open timeout. </value>
        public TimeSpan DefaultOpenTimeout { get; set; } = My.Settings.Default.DefaultOpenSessionTimeout;


        /// <summary>
        /// Gets or sets the session open sentinel. When open, the session is capable of addressing the
        /// hardware. See also <see cref="IsDeviceOpen"/>.
        /// </summary>
        /// <value> The is session open. </value>
        public abstract bool IsSessionOpen { get; }

        /// <summary> True if device is open, false if not. </summary>
        private bool _IsDeviceOpen;


        /// <summary>
        /// Gets or sets the Device Open sentinel. When open, the device is capable of addressing real
        /// hardware if the session is open. See also <see cref="IsSessionOpen"/>.
        /// </summary>
        /// <value> The is device open. </value>
        public bool IsDeviceOpen
        {
            get => this._IsDeviceOpen;

            protected set {
                if ( value != this.IsDeviceOpen )
                {
                    this._IsDeviceOpen = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> Gets or sets the state of the resource open. </summary>
        /// <value> The resource open state. </value>
        public ResourceOpenState ResourceOpenState { get; set; }

        /// <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public abstract bool IsDummy { get; }

        /// <summary> Executes the session open actions. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The short title of the device. </param>
        protected virtual void OnSessionOpen( string resourceName, string resourceTitle )
        {
            this.OnSessionOpen( resourceName, resourceTitle, ResourceOpenState.Success );
        }

        /// <summary> Executes the session open actions. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <param name="resourceName">      The name of the resource. </param>
        /// <param name="resourceTitle">     The short title of the device. </param>
        /// <param name="resourceOpenState"> The resource open state. </param>
        protected virtual void OnSessionOpen( string resourceName, string resourceTitle, ResourceOpenState resourceOpenState )
        {
            // Use SynchronizeCallbacks to specify that the object marshals callbacks across threads appropriately.
            this.SynchronizeCallbacks = true;
            this.OpenResourceName = resourceName;
            this.OpenResourceTitle = resourceTitle;
            this.CandidateResourceTitle = this.OpenResourceTitle;
            this.ResourceNameCaption = $"{this.OpenResourceTitle}.{this.OpenResourceName}";
            this.ResourceTitleCaption = this.OpenResourceTitle;
#if false
            this.LastInputOutputStopwatch = new Stopwatch();
#endif
            this.ResourceOpenState = resourceOpenState;
            this.IsDeviceOpen = resourceOpenState != ResourceOpenState.Unknown;
            this.LastAction = $"{resourceName} open";
            this.StatusPrompt = this.LastAction;
            this.HandleSessionOpen( resourceName, resourceTitle );
            this.CommunicationTimeouts.Push( this.CommunicationTimeout );
        }

        /// <summary> Creates a session. </summary>
        /// <remarks> Throws an exception if the resource is not accessible. </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <param name="timeout">      The timeout. </param>
        protected abstract void CreateSession( string resourceName, TimeSpan timeout );

        /// <summary> Check if a session can be created. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public (bool Success, string Details) CanCreateSession( string resourceName, TimeSpan timeout )
        {
            try
            {
                this.CreateSession( resourceName, timeout );
                return (true, string.Empty);
            }
            catch ( Exception ex )
            {
                return (false, ex.ToFullBlownString());
            }
            finally
            {
                try
                {
                    this.DisposeSession();
                }
                catch
                {
                }
                finally
                {
                }
            }
        }


        /// <summary>
        /// Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
        /// </summary>
        /// <remarks>
        /// Call this first. The synchronization context is captured as part of the property change and
        /// other event handlers and is no longer needed here.
        /// </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The short title of the device. </param>
        /// <param name="timeout">       The timeout. </param>
        public void OpenSession( string resourceName, string resourceTitle, TimeSpan timeout )
        {
            try
            {
                this.ClearLastError();
                this.ResourceNameInfo = new ResourceNameInfo( resourceName );
                this.CandidateResourceName = resourceName;
                this.CandidateResourceTitle = resourceTitle;
                this.LastAction = $"Opening {resourceName}";
                this.StatusPrompt = this.LastAction;
                this.CommunicationTimeouts.Clear();
                this.CreateSession( resourceName, timeout );
                this.OnSessionOpen( resourceName, resourceTitle );
            }
            catch ( Exception )
            {
                throw;
            }
        }


        /// <summary>
        /// Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
        /// </summary>
        /// <remarks>
        /// Call this first. The synchronization context is captured as part of the property change and
        /// other event handlers and is no longer needed here.
        /// </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The short title of the device. </param>
        public void OpenSession( string resourceName, string resourceTitle )
        {
            this.OpenSession( resourceName, resourceTitle, this.DefaultOpenTimeout );
        }


        /// <summary>
        /// Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
        /// </summary>
        /// <remarks>
        /// Call this first. The synchronization context is captured as part of the property change and
        /// other event handlers and is no longer needed here.
        /// </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        public void OpenSession( string resourceName )
        {
            this.OpenSession( resourceName, resourceName, this.DefaultOpenTimeout );
        }


        /// <summary>
        /// Opens a VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
        /// </summary>
        /// <remarks>
        /// Call this first. The synchronization context is captured as part of the property change and
        /// other event handlers and is no longer needed here.
        /// </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <param name="timeout">      The timeout. </param>
        public void OpenSession( string resourceName, TimeSpan timeout )
        {
            this.OpenSession( resourceName, resourceName, timeout );
        }

        /// <summary> Gets or sets the sentinel indication that the VISA session is disposed. </summary>
        /// <value> The is session disposed. </value>
        public abstract bool IsSessionDisposed { get; }


        /// <summary>
        /// Disposes the VISA <see cref="VI.Pith.SessionBase">Session</see> ending access to the
        /// instrument.
        /// </summary>
        protected abstract void DisposeSession();

        /// <summary> Discard all events. </summary>
        protected abstract void DiscardAllEvents();


        /// <summary>
        /// Closes the VISA <see cref="VI.Pith.SessionBase">Session</see> to access the instrument.
        /// </summary>
        public void CloseSession()
        {
            this.LastAction = $"{this.OpenResourceName} clearing last error";
            this.ClearLastError();
            this.CommunicationTimeouts.Clear();
            if ( this.IsDeviceOpen )
            {
                this.LastAction = $"{this.OpenResourceName} discarding all events";
                this.DiscardAllEvents();
                this.LastAction = $"{this.OpenResourceName} disabling service request";
                this.DisableServiceRequestEventHandler();
                this.IsDeviceOpen = false;
            }

            this.LastAction = $"{this.OpenResourceName} closed";
            this.StatusPrompt = this.LastAction;
            // disposes the session.
            if ( this.IsSessionOpen )
                this.DisposeSession();
            // must use sync notification to prevent race condition if disposing the session.
            this.SyncNotifyPropertyChanged( nameof( this.IsSessionOpen ) );
        }

        /// <summary> The last action. </summary>
        private string _LastAction;

        /// <summary> Gets or sets the last action. </summary>
        /// <value> The last action. </value>
        public string LastAction
        {
            get => this._LastAction;

            set {
                if ( !string.Equals( this.LastAction, value ) )
                {
                    this._LastAction = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last node number. </summary>
        private int? _LastNodeNumber;

        /// <summary> Gets or sets the last node. </summary>
        /// <value> The last node. </value>
        public int? LastNodeNumber
        {
            get => this._LastNodeNumber;

            set {
                if ( !Nullable.Equals( this.LastNodeNumber, value ) )
                {
                    this._LastNodeNumber = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }


        /// <summary>
        /// Gets the sentinel indicating if call backs are performed in a specific synchronization
        /// context.
        /// </summary>
        /// <value>
        /// The sentinel indicating if call backs are performed in a specific synchronization context.
        /// </value>
        public virtual bool SynchronizeCallbacks { get; set; }

        #region " ERROR / STATUS "

        /// <summary> Clears the last error. </summary>
        protected void ClearLastError()
        {
            this.LastNativeError = NativeErrorBase.Success;
        }

        /// <summary> Gets the last native error. </summary>
        /// <value> The last native error. </value>
        protected NativeErrorBase LastNativeError { get; set; }

        #endregion

        #region " MESSAGE EVENTS "

        /// <summary> The notify synchronize levels. </summary>
        private BindingList<KeyValuePair<NotifySyncLevel, string>> _NotifySyncLevels;

        /// <summary> Gets a list of the message notification events. </summary>
        /// <value> A list of the message notification events. </value>
        public BindingList<KeyValuePair<NotifySyncLevel, string>> NotifySyncLevels
        {
            get {
                if ( this._NotifySyncLevels is null || !this._NotifySyncLevels.Any() )
                {
                    this._NotifySyncLevels = new BindingList<KeyValuePair<NotifySyncLevel, string>>();
                    var extender = new Core.EnumExtender<NotifySyncLevel>();
                    extender.Populate( this._NotifySyncLevels, extender.ValueNamePairs );
                }

                return this._NotifySyncLevels;
            }
        }

        /// <summary> The message notification event. </summary>
        private KeyValuePair<NotifySyncLevel, string> _MessageNotificationEvent;

        /// <summary> Gets or sets the message notification event. </summary>
        /// <value> The message notification event. </value>
        public KeyValuePair<NotifySyncLevel, string> MessageNotificationEvent
        {
            get => this._MessageNotificationEvent;

            set {
                if ( value.Key != this.MessageNotificationEvent.Key )
                {
                    this._MessageNotificationEvent = value;
                    this.MessageNotificationLevel = value.Key;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The message notification level. </summary>
        private NotifySyncLevel _MessageNotificationLevel;

        /// <summary> Gets or sets the message notification level. </summary>
        /// <value> The message notification level. </value>
        public NotifySyncLevel MessageNotificationLevel
        {
            get => this._MessageNotificationLevel;

            set {
                if ( value != this.MessageNotificationLevel )
                {
                    this._MessageNotificationLevel = value;
                    this.NotifyPropertyChanged();
                    this.MessageNotificationEvent = ToNotificationLevel( value );
                }
            }
        }

        /// <summary> Converts a value to a notification level. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a KeyValuePair(Of NotifySyncLevel, String) </returns>
        public static KeyValuePair<NotifySyncLevel, string> ToNotificationLevel( NotifySyncLevel value )
        {
            return new KeyValuePair<NotifySyncLevel, string>( value, value.ToString() );
        }

        /// <summary> The last message received. </summary>
        private string _LastMessageReceived;

        /// <summary> Gets or sets the last message Received. </summary>
        /// <remarks>
        /// The last message sent is posted asynchronously. This may not be processed fast enough with
        /// TSP devices to determine the state of the instrument.
        /// </remarks>
        /// <value> The last message Received. </value>
        public string LastMessageReceived
        {
            get => this._LastMessageReceived;

            protected set {
                this._LastMessageReceived = value;
                if ( this.MessageNotificationLevel == NotifySyncLevel.Async )
                {
                    this.NotifyPropertyChanged();
                }
                else if ( this.MessageNotificationLevel == NotifySyncLevel.Sync )
                {
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last message sent. </summary>
        private string _LastMessageSent;

        /// <summary> Gets or sets the last message Sent. </summary>
        /// <remarks> The last message sent is posted asynchronously. </remarks>
        /// <value> The last message Sent. </value>
        public string LastMessageSent
        {
            get => this._LastMessageSent;

            protected set {
                this._LastMessageSent = value;
                if ( this.MessageNotificationLevel == NotifySyncLevel.Async )
                {
                    this.NotifyPropertyChanged();
                }
                else if ( this.MessageNotificationLevel == NotifySyncLevel.Sync )
                {
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region " READ / WRITE "

        /// <summary> Gets the ASCII character used to end reading. </summary>
        /// <value> The termination character. </value>
        public abstract byte ReadTerminationCharacter { get; set; }

        /// <summary>
        /// Gets the termination character enabled specifying whether the read operation ends when a
        /// termination character is received.
        /// </summary>
        /// <value> The termination character enabled. </value>
        public abstract bool ReadTerminationCharacterEnabled { get; set; }

        /// <summary> Gets the timeout for I/O communication on this resource session. </summary>
        /// <value> The communication timeout. </value>
        public abstract TimeSpan CommunicationTimeout { get; set; }

        /// <summary> Gets the emulated reply. </summary>
        /// <value> The emulated reply. </value>
        public string EmulatedReply { get; set; }

        /// <summary>
        /// Synchronously reads ASCII-encoded string data. Read characters into the return string until
        /// an END indicator or <see cref="ReadTerminationCharacter">termination character</see>
        /// termination character is reached. Limited by the
        /// <see cref="InputBufferSize"/>. Will time out if end of line is not read before reading a
        /// buffer.
        /// </summary>
        /// <returns> The received message. </returns>
        public abstract string ReadFiniteLine();

        /// <summary>
        /// Synchronously reads ASCII-encoded string data until an END indicator or
        /// <see cref="ReadTerminationCharacter">termination character</see>
        /// termination character is reached irrespective of the buffer size.
        /// </summary>
        /// <returns> The received message. </returns>
        public abstract string ReadFreeLine();

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks> David, 2020-07-23. </remarks>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        protected abstract string SyncWriteLine( string dataToWrite );

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.<para>
        /// Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
        /// formatted I/O write buffer</para>
        /// </summary>
        /// <remarks> David, 2020-07-23. </remarks>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        protected abstract string SyncWrite( string dataToWrite );

        /// <summary> Gets or set the value of the last status byte. </summary>
        /// <value> The status byte. </value>
        public ServiceRequests StatusByte { get; set; }

        /// <summary> Thread unsafe read status byte. </summary>
        /// <returns> The VI.Pith.ServiceRequests. </returns>
        protected abstract ServiceRequests ThreadUnsafeReadStatusByte();

        /// <summary> Gets the status byte locker. </summary>
        /// <value> The status byte locker. </value>
        private ReaderWriterLockSlim StatusByteLocker { get; set; } = new ReaderWriterLockSlim();

        /// <summary> Reads status byte. </summary>
        /// <returns> The status byte. </returns>
        public virtual ServiceRequests ReadStatusByte()
        {
            this.StatusByteLocker.EnterReadLock();
            try
            {
                return this.ThreadUnsafeReadStatusByte();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.StatusByteLocker.ExitReadLock();
            }
        }

        /// <summary> Gets the size of the input buffer. </summary>
        /// <value> The size of the input buffer. </value>
        public abstract int InputBufferSize { get; }

        #endregion

        #region " EMULATION "

        /// <summary> Makes emulated reply. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeTrueFalseReply( bool value )
        {
            this.EmulatedReply = ToTrueFalse( value );
        }

        /// <summary> Makes emulated reply if empty. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeTrueFalseReplyIfEmpty( bool value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeTrueFalseReply( value );
            }
        }

        /// <summary> Makes emulated reply. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReply( bool value )
        {
            this.EmulatedReply = ToOneZero( value );
        }

        /// <summary> Makes emulated reply if empty. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( bool value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeEmulatedReply( value );
            }
        }

        /// <summary> Makes emulated reply. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReply( double value )
        {
            this.EmulatedReply = value.ToString();
        }

        /// <summary> Makes emulated reply if empty. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( double value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeEmulatedReply( value );
            }
        }

        /// <summary>   Makes emulated reply. </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="value">    The emulated value. </param>
        public void MakeEmulatedReply( decimal value )
        {
            this.EmulatedReply = value.ToString();
        }

        /// <summary>   Makes emulated reply if empty. </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="value">    The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( decimal value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeEmulatedReply( value );
            }
        }

        /// <summary> Makes emulated reply. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReply( int value )
        {
            this.EmulatedReply = value.ToString();
        }

        /// <summary> Makes emulated reply if empty. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( int value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeEmulatedReply( value );
            }
        }

        /// <summary> Makes emulated reply. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReply( TimeSpan value )
        {
            this.EmulatedReply = value.ToString();
        }

        /// <summary> Makes emulated reply if empty. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( TimeSpan value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeEmulatedReply( value );
            }
        }

        /// <summary> Makes emulated reply. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReply( string value )
        {
            this.EmulatedReply = value;
        }

        /// <summary> Makes emulated reply if empty. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( string value )
        {
            if ( string.IsNullOrWhiteSpace( this.EmulatedReply ) )
            {
                this.MakeEmulatedReply( value );
            }
        }

        #endregion

        #region " TRIGGER "


        /// <summary>
        /// Asserts a software or hardware trigger depending on the interface; Sends a bus trigger.
        /// </summary>
        public abstract void AssertTrigger();

        #endregion

        #region " INTERFACE "

        /// <summary> Supports clear interface. </summary>
        /// <value> <c>True</c> if supports clearing the interface. </value>
        public bool SupportsClearInterface => this.ResourceNameInfo is object && (HardwareInterfaceType.Gpib == this.ResourceNameInfo.InterfaceType);

        /// <summary> The interface clear refractory period. </summary>
        private TimeSpan _InterfaceClearRefractoryPeriod;

        /// <summary> Gets or sets the Interface clear refractory period. </summary>
        /// <value> The Interface clear refractory period. </value>
        public TimeSpan InterfaceClearRefractoryPeriod
        {
            get => this._InterfaceClearRefractoryPeriod;

            set {
                if ( !value.Equals( this.InterfaceClearRefractoryPeriod ) )
                {
                    this._InterfaceClearRefractoryPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Clears the interface. </summary>
        public void ClearInterface()
        {
            if ( this.SupportsClearInterface )
            {
                this.ImplementClearHardwareInterface();
                if ( this.IsSessionOpen )
                    Core.ApplianceBase.DoEventsWait( this.InterfaceClearRefractoryPeriod );
            }
        }

        /// <summary> Implement clear hardware interface. </summary>
        protected abstract void ImplementClearHardwareInterface();

        /// <summary> Clears the device (CDC). </summary>
        /// <remarks>
        /// When communicating with a message-based device, particularly when you are first developing
        /// your program, you may need to tell the device to clear its I/O buffers so that you can start
        /// again. In addition, if a device has more information than you need, you may want to read
        /// until you have everything you need and then tell the device to throw the rest away. The
        /// <c>viClear()</c> operation performs these tasks. More specifically, the clear operation lets
        /// a controller send the device clear command to the device it is associated with, as specified
        /// by the interface specification and the type of device. The action that the device takes
        /// depends on the interface to which it is connected. <para>
        /// For a GPIB device, the controller sends the IEEE 488.1 SDC (04h) command.</para><para>
        /// For a VXI or MXI device, the controller sends the Word Serial Clear (FFFFh) command.</para>
        /// <para>
        /// For the ASRL INSTR or TCPIP SOCKET resource, the controller sends the string "*CLS\n". The
        /// I/O protocol must be set to VI_PROT_4882_STRS for this service to be available to these
        /// resources.</para>
        /// For more details on these clear commands, refer to your device documentation, the IEEE 488.1
        /// standard, or the VXI bus specification. <para>
        /// Source: NI-Visa HTML help.</para>
        /// </remarks>
        protected abstract void Clear();

        /// <summary> Clears the device (SDC). </summary>
        protected abstract void ClearDevice();


        /// <summary>
        /// Clears the active state. Issues selective device clear. Waits for the
        /// <see cref="DeviceClearRefractoryPeriod"/> before releasing control.
        /// </summary>
        public virtual void ClearActiveState()
        {
            this.ClearActiveState( this.DeviceClearRefractoryPeriod );
        }


        /// <summary>
        /// Clears the active state. Issues selective device clear. Waits for the
        /// <paramref name="refractoryPeriod"/> before releasing control.
        /// </summary>
        /// <remarks>   David, 2021-05-27. </remarks>
        /// <param name="refractoryPeriod">     The refractory period. </param>
        /// <param name="delayMilliseconds">    (Optional) The delay before issuing the clear. <para>
        ///                                     A delay is required before issuing a device clear on some TSP
        ///                                     devices, such as the 2600. First a 1ms delay was added.
        ///                                     Without the delay, the instrument had error -286 TSP Runtime
        ///                                     Error and User Abort error if the clear command was issued
        ///                                     after turning off the status _G.status.request_enable=0
        ///                                     Thereafter, the instrument has a resource not found error
        ///                                     when trying to connect after failing to connect because
        ///                                     instruments were off. Stopping here in debug mode seems to
        ///                                     have alleviated this issue.  So, the delay was increased to
        ///                                     10 ms.
        ///                                     </para> </param>
        public virtual void ClearActiveState( TimeSpan refractoryPeriod, int delayMilliseconds = 10 )
        {
            this.StatusPrompt = $"{this.OpenResourceName} SDC";
            Core.ApplianceBase.Delay( TimeSpan.FromMilliseconds( delayMilliseconds ) );
            this.ClearDevice();
            if ( this.IsSessionOpen )
                Core.ApplianceBase.DoEventsWait( refractoryPeriod );
            this.StatusPrompt = $"{this.OpenResourceName} SDC; done";
            _ = this.QueryOperationCompleted();
        }

        /// <summary> The device clear delay period. </summary>
        private TimeSpan _DeviceClearDelayPeriod;

        /// <summary> Gets or sets the device clear Delay period. </summary>
        /// <value> The device clear Delay period. </value>
        public TimeSpan DeviceClearDelayPeriod
        {
            get => this._DeviceClearDelayPeriod;

            set {
                if ( !value.Equals( this.DeviceClearDelayPeriod ) )
                {
                    this._DeviceClearDelayPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The device clear refractory period. </summary>
        private TimeSpan _DeviceClearRefractoryPeriod;

        /// <summary> Gets or sets the device clear refractory period. </summary>
        /// <value> The device clear refractory period. </value>
        public TimeSpan DeviceClearRefractoryPeriod
        {
            get => this._DeviceClearRefractoryPeriod;

            set {
                if ( !value.Equals( this.DeviceClearRefractoryPeriod ) )
                {
                    this._DeviceClearRefractoryPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OVERRIDE NOT REQUIRED "

        #region " TERMINATION "

        /// <summary> The termination sequence. </summary>
        private string _TerminationSequence;

        /// <summary> Gets or sets the termination sequence. </summary>
        /// <value> The termination sequence. </value>
        public string TerminationSequence
        {
            get => this._TerminationSequence;

            set {
                if ( (value ?? "") != (this.TerminationSequence ?? "") )
                {
                    this._TerminationSequence = value;
                    this.NotifyPropertyChanged();
                    _ = this.DefineTermination( value.ReplaceCommonEscapeSequences().ToCharArray() );
                }
            }
        }

        /// <summary> Determine if we are equal. </summary>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> True if equal, false if not. </returns>
        private static bool AreEqual( IEnumerable<char> left, IEnumerable<char> right )
        {
            bool result = true;
            if ( left is null )
            {
                result = right is null;
            }
            else if ( right is null )
            {
                result = false;
            }
            else if ( left.Any() )
            {
                if ( right.Any() )
                {
                    if ( left.Count() == right.Count() )
                    {
                        for ( int i = 0, loopTo = left.Count() - 1; i <= loopTo; i++ )
                        {
                            if ( left.ElementAtOrDefault( i ) != right.ElementAtOrDefault( i ) )
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        result = false;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        /// <summary> The termination characters. </summary>
        private char[] _TerminationCharacters;

        /// <summary>   Termination characters. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <returns>   A char[]. </returns>
        public char[] TerminationCharacters()
        {
            return this._TerminationCharacters;
        }

        /// <summary>   Enumerates define termination in this collection. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">    The value. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process define termination in this collection.
        /// </returns>
        public IEnumerable<char> DefineTermination( IEnumerable<char> value )
        {
            if ( !AreEqual( value, this.TerminationCharacters() ) )
            {
                this._TerminationCharacters = new char[(value.Count())];
                Array.Copy( value.ToArray(), this._TerminationCharacters, value.Count() );
                this.NotifyPropertyChanged( nameof( this.TerminationCharacters ) );
                this.TerminationSequence = new string( this._TerminationCharacters );
            }
            return value;
        }

        /// <summary> Use default termination. </summary>
        private void UseDefaultTerminationThis()
        {
            this.NewTerminationThis( Environment.NewLine.ToCharArray()[1] );
        }

        /// <summary> Use default termination. </summary>
        public void UseDefaultTermination()
        {
            this.UseDefaultTerminationThis();
        }

        /// <summary> Creates a new termination. </summary>
        /// <param name="value"> The emulated value. </param>
        private void NewTerminationThis( char value )
        {
            _ = this.DefineTermination( new char[] { value } );
        }

        /// <summary> Creates a new termination. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="values"> The values. </param>
        private void NewTerminationThis( char[] values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            if ( values.Length == 0 )
                throw new InvalidOperationException( "Failed creating new termination; Source array must have at least one character" );
            var terms = new char[values.Length];
            Array.Copy( values, terms, values.Length );
            _ = this.DefineTermination( terms );
        }

        /// <summary> Creates a new termination. </summary>
        /// <param name="values"> The values. </param>
        public void NewTermination( char[] values )
        {
            this.NewTerminationThis( values );
        }

        /// <summary> Use new line termination. </summary>
        public void UseNewLineTermination()
        {
            this.NewTerminationThis( Environment.NewLine.ToCharArray() );
        }

        /// <summary> Use line feed termination. </summary>
        public void UseLinefeedTermination()
        {
            this.NewTerminationThis( Environment.NewLine.ToCharArray()[1] );
        }

        /// <summary> Use Carriage Return termination. </summary>
        public void UseCarriageReturnTermination()
        {
            this.NewTerminationThis( Environment.NewLine.ToCharArray()[0] );
        }

        #endregion

        #region " EXECUTE "

        /// <summary> Executes the command. </summary>
        /// <param name="command"> The command. </param>
        public void Execute( string command )
        {
            _ = this.WriteLine( command );
        }

        /// <summary>   Executes the command returning elapsed time. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   The <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo ExecuteElapsed( string dataToWrite )
        {
            if ( string.IsNullOrWhiteSpace( dataToWrite ) )
                throw new ArgumentNullException( nameof( dataToWrite ) );

            Stopwatch sw = Stopwatch.StartNew();
            return new ExecuteInfo( dataToWrite, this.WriteLine( dataToWrite ), sw.Elapsed );
        }

        /// <summary> Executes the command using the specified timeout. </summary>
        /// <param name="command"> The command. </param>
        /// <param name="timeout"> The timeout. </param>
        public void Execute( string command, TimeSpan timeout )
        {
            try
            {
                this.StoreCommunicationTimeout( timeout );
                _ = this.WriteLine( command );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RestoreCommunicationTimeout();
            }
        }

        /// <summary>
        /// Executes the <see cref="Action">action</see>/&gt; using the specified timeout.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="action">  The action. </param>
        /// <param name="timeout"> The timeout. </param>
        public void Execute( Action action, TimeSpan timeout )
        {
            if ( action is null )
                throw new ArgumentNullException( nameof( action ) );
            try
            {
                this.StoreCommunicationTimeout( timeout );
                action();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RestoreCommunicationTimeout();
            }
        }

        /// <summary> Executes the command using the specified timeout. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="action">  The action. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A Boolean? </returns>
        public bool? Execute( Func<bool?> action, TimeSpan timeout )
        {
            if ( action is null )
                throw new ArgumentNullException( nameof( action ) );
            try
            {
                this.StoreCommunicationTimeout( timeout );
                return action();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RestoreCommunicationTimeout();
            }
        }

        #endregion

        #region " READ / WRITE LINE"

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface. Replaces Escape
        /// characters and Terminates the data with the <see cref="ReadTerminationCharacter">termination
        /// character</see>.
        /// </summary>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> A String. </returns>
        public string WriteEscapedLine( string format, params object[] args )
        {
            return this.WriteEscapedLine( string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }


        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        public string WriteEscapedLine( string dataToWrite )
        {
            return this.WriteLine( dataToWrite.ReplaceCommonEscapeSequences() );
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   The <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo WriteEscapedLineElapsed( string dataToWrite )
        {
            Stopwatch sw = Stopwatch.StartNew();
            return new ExecuteInfo( dataToWrite, this.WriteEscapedLine( dataToWrite ), sw.Elapsed );
        }


        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
        /// data with the <see cref="ReadTerminationCharacter">termination character</see>. <para>
        /// Per IVI documentation: Appends a newline (0xA) to the formatted I/O write buffer, flushes the
        /// buffer, and sends an end-of-line with the buffer if required.</para>
        /// </summary>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> A String. </returns>
        public string WriteLine( string format, params object[] args )
        {
            return this.WriteLine( string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        public string WriteLine( string dataToWrite )
        {
#if LogIO
             My.MyLibrary.Appliance.Logger.WriteEntry( $"w: {dataToWrite}" );
#endif
            return this.SyncWriteLine( dataToWrite );
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   The <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo WriteLineElapsed( string dataToWrite )
        {
            Stopwatch sw = Stopwatch.StartNew();
            return new ExecuteInfo( dataToWrite, this.WriteLine( dataToWrite ), sw.Elapsed );
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="format">   The format of the data to write. </param>
        /// <param name="args">     The format arguments. </param>
        /// <returns>   The <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo WriteLineElapsed( string format, params object[] args )
        {
            return this.WriteLineElapsed( string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }


        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.<para>
        /// Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
        /// formatted I/O write buffer</para>
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns> the sent message. </returns>
        public string Write( string dataToWrite )
        {
#if LogIO
             My.MyLibrary.Appliance.Logger.WriteEntry( $"w: {dataToWrite}" );
#endif
            return this.SyncWrite( dataToWrite );
        }

        /// <summary>
        /// Synchronously reads ASCII-encoded string data until an END indicator or
        /// <see cref="ReadTerminationCharacter">termination character</see>
        /// termination character is reached irrespective of the buffer size.
        /// </summary>
        /// <returns> The string. </returns>
        public string ReadLine()
        {
#if LogIO
            string value = this.ReadFiniteLine();
            My.MyLibrary.Appliance.Logger.WriteEntry( $"r: {value.TrimEnd()}" );
            return value
#else
            return this.ReadFreeLine();
#endif
        }

        /// <summary>
        /// Reads to and including the end <see cref="TerminationCharacters">termination character</see>.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <returns>   A Tuple:  (string ReceivedMessage, <see cref="ReadInfo"/>) </returns>
        public (string ReceivedMessage, ReadInfo ReadInfo) ReadElapsed()
        {
            Stopwatch sw = Stopwatch.StartNew();
            string s = this.ReadFreeLine();
            return (s, new ReadInfo( s, sw.Elapsed ));
        }

        #endregion

        #region " QUERY "

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns>
        /// The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
        /// </returns>
        public string Query( string dataToWrite )
        {
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                _ = this.WriteLine( dataToWrite );
                return this.ReadLine();
            }
            return string.Empty;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns>
        /// The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
        /// </returns>
        public string Query( string format, params object[] args )
        {
            if ( !string.IsNullOrWhiteSpace( format ) )
            {
                _ = this.WriteLine( format, args );
                return this.ReadLine();
            }
            return string.Empty;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/> ) </returns>
        public (string ReceivedMessage, QueryInfo QueryInfo) QueryElapsed( string dataToWrite )
        {
            Stopwatch sw = Stopwatch.StartNew();
            string receivedMessage = string.Empty;
            string sentMessage = string.Empty;
            TimeSpan writeTime = TimeSpan.Zero;
            TimeSpan readTime = TimeSpan.Zero;
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                sentMessage = this.WriteLine( dataToWrite );
                writeTime = sw.Elapsed;
                receivedMessage = this.ReadLine();
                readTime = sw.Elapsed.Subtract( writeTime );
            }
            return (receivedMessage, new QueryInfo( dataToWrite, sentMessage, receivedMessage,
                                                    new ElapsedTimeSpan[] { new ElapsedTimeSpan( ElapsedTimeIdentity.WriteTime, writeTime ),
                                                        new ElapsedTimeSpan( ElapsedTimeIdentity.ReadTime, readTime ),
                                                        new ElapsedTimeSpan( ElapsedTimeIdentity.QueryTime, sw.Elapsed ) } ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="format">   The format of the data to write. </param>
        /// <param name="args">     The format arguments. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/> ) </returns>
        public (string SentMessage, QueryInfo QueryInfo) QueryElapsed( string format, params object[] args )
        {
            return this.QueryElapsed( string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read
        /// after the specified delay.
        /// </summary>
        /// <param name="readDelay">   The read delay. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns>
        /// The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
        /// </returns>
        public string Query( TimeSpan readDelay, string dataToWrite )
        {
            string result = string.Empty;
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                _ = this.WriteLine( dataToWrite );
                System.Threading.Thread.SpinWait( 10 );
                ApplianceBase.DoEventsWait( readDelay );
                result = this.ReadLine();
            }
            return result;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="readDelay"> The timespan to delay reading after writing. </param>
        /// <param name="format">    The format of the data to write. </param>
        /// <param name="args">      The format arguments. </param>
        /// <returns>
        /// The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
        /// </returns>
        public string Query( TimeSpan readDelay, string format, params object[] args )
        {
            if ( !string.IsNullOrWhiteSpace( format ) )
            {
                _ = this.WriteLine( format, args );
                Core.ApplianceBase.DoEventsWait( readDelay );
                return this.ReadLine();
            }

            return string.Empty;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data. A final read is performed after a
        /// delay.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="readDelay">    The read delay. </param>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/> ) </returns>
        public (string SentMessage, QueryInfo QueryInfo) QueryElapsed( TimeSpan readDelay, string dataToWrite )
        {
            Stopwatch sw = Stopwatch.StartNew();
            string sentMessage = dataToWrite;
            string receivedMessage = string.Empty;

            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                sentMessage = this.WriteLine( dataToWrite );
                System.Threading.Thread.SpinWait( 10 );
                ApplianceBase.DoEventsWait( readDelay );
                receivedMessage = this.ReadLine();
            }
            return (receivedMessage, new QueryInfo( dataToWrite, sentMessage, receivedMessage, sw.Elapsed ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="readDelay">    The read delay. </param>
        /// <param name="format">       The format of the data to write. </param>
        /// <param name="args">         The format arguments. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/> ) </returns>
        public (string SentMessage, QueryInfo QueryInfo) QueryElapsed( TimeSpan readDelay, string format, params object[] args )
        {
            return this.QueryElapsed( readDelay, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        #endregion

        #region " QUERY TRIM END "

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimTermination( string dataToWrite )
        {
            return this.Query( dataToWrite ).TrimEnd( this.TerminationCharacters() );
        }

        /// <summary>
        /// Queries the device and returns a string save the termination character. Expects terminated
        /// query command.
        /// </summary>
        /// <param name="readDelay">   The timespan to delay reading after writing. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimEnd( TimeSpan readDelay, string dataToWrite )
        {
            return this.Query( readDelay, dataToWrite ).TrimEnd( this.TerminationCharacters() );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimEnd( string dataToWrite )
        {
            return this.Query( dataToWrite ).TrimEnd( this.TerminationCharacters() );
        }

        /// <summary>
        /// Queries the device and returns a string save the termination character. Expects terminated
        /// query command.
        /// </summary>
        /// <param name="readDelay"> The timespan to delay reading after writing. </param>
        /// <param name="format">    The format of the data to write. </param>
        /// <param name="args">      The format arguments. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimEnd( TimeSpan readDelay, string format, params object[] args )
        {
            return this.Query( readDelay, format, args ).TrimEnd( this.TerminationCharacters() );
        }

        /// <summary>
        /// Queries the device and returns a string save the termination character. Expects terminated
        /// query command.
        /// </summary>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimEnd( string format, params object[] args )
        {
            return this.Query( format, args ).TrimEnd( this.TerminationCharacters() );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="readDelay"> The timespan to delay reading after writing. </param>
        /// <param name="format">    The format of the data to write. </param>
        /// <param name="args">      The format arguments. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimTermination( TimeSpan readDelay, string format, params object[] args )
        {
            return this.Query( readDelay, format, args ).TrimEnd( this.TerminationCharacters() );
        }


        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns>
        /// The <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see> without the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string QueryTrimTermination( string format, params object[] args )
        {
            return this.Query( format, args ).TrimEnd( this.TerminationCharacters() );
        }

        #endregion

        #region " READ TRIM END "


        /// <summary>
        /// Synchronously reads ASCII-encoded string data. Reads up to the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </summary>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string ReadLineTrimTermination()
        {
            return this.ReadLine().TrimEnd( this.TerminationCharacters() );
        }


        /// <summary>
        /// Synchronously reads ASCII-encoded string data. Reads up to the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </summary>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters">termination characters</see>.
        /// </returns>
        public string ReadLineTrimEnd()
        {
            return this.ReadLine().TrimEnd( this.TerminationCharacters() );
        }

        /// <summary> Reads free line trim end. </summary>
        /// <returns> The free line trim end. </returns>
        public string ReadFreeLineTrimEnd()
        {
            return this.ReadFreeLine().TrimEnd( this.TerminationCharacters() );
        }


        /// <summary>
        /// Synchronously reads ASCII-encoded string data. Reads up to the
        /// <see cref="TerminationCharacters">termination character</see>.
        /// </summary>
        /// <returns>
        /// The received message without the carriage return (13) and line feed (10) characters.
        /// </returns>
        public string ReadLineTrimNewLine()
        {
            return this.ReadLine().TrimEnd( new char[] { Convert.ToChar( 13 ), Convert.ToChar( 10 ) } );
        }


        /// <summary>
        /// Synchronously reads ASCII-encoded string data. Reads up to the
        /// <see cref="TerminationCharacters">termination characters</see>.
        /// </summary>
        /// <returns> The received message without the line feed (10) characters. </returns>
        public string ReadLineTrimLinefeed()
        {
            return this.ReadLine().TrimEnd( Convert.ToChar( 10 ) );
        }

        #endregion

        #region " READ LINES "


        /// <summary>
        /// Reads multiple lines from the instrument until data is no longer available.
        /// </summary>
        /// <param name="pollDelay"> Time to wait between service requests. </param>
        /// <param name="timeout">   Specifies the time to wait for message available. </param>
        /// <param name="trimEnd">   Specifies a directive to trim the end character from each line. </param>
        /// <returns> Data. </returns>
        public string ReadLines( TimeSpan pollDelay, TimeSpan timeout, bool trimEnd )
        {
            return this.ReadLines( pollDelay, timeout, false, trimEnd );
        }


        /// <summary>
        /// Reads multiple lines from the instrument until data is no longer available.
        /// </summary>
        /// <param name="pollDelay">  Time to wait between service requests. </param>
        /// <param name="timeout">    Specifies the time to wait for message available. </param>
        /// <param name="trimSpaces"> Specifies a directive to trim leading and trailing spaces from each
        /// line. This also trims the end character. </param>
        /// <param name="trimEnd">    Specifies a directive to trim the end character from each line. </param>
        /// <returns> Data. </returns>
        public string ReadLines( TimeSpan pollDelay, TimeSpan timeout, bool trimSpaces, bool trimEnd )
        {
            var listBuilder = new System.Text.StringBuilder();
            var sw = Stopwatch.StartNew();
            bool timedOut = false;
            while ( !timedOut )
            {

                // allow message available time to materialize
                _ = this.ReadStatusRegister();
                while ( !this.HasMeasurementEvent && !timedOut )
                {
                    timedOut = sw.Elapsed > timeout;
                    Core.ApplianceBase.DoEventsWait( pollDelay );
                    _ = this.ReadStatusRegister();
                }

                if ( this.HasMeasurementEvent )
                {
                    timedOut = false;
                    sw.Restart();
                    _ = trimSpaces
                        ? listBuilder.AppendLine( this.ReadLine().Trim() )
                        : trimEnd ? listBuilder.AppendLine( this.ReadLineTrimEnd() ) : listBuilder.AppendLine( this.ReadLine() );
                }
            }

            return listBuilder.ToString();
        }

        /// <summary> Reads multiple lines from the instrument until timeout. </summary>
        /// <param name="timeout">        Specifies the time in millisecond to wait for message available. </param>
        /// <param name="trimSpaces">     Specifies a directive to trim leading and trailing spaces from
        /// each line. </param>
        /// <param name="expectedLength"> Specifies the amount of data expected without trimming. </param>
        /// <returns> Data. </returns>
        public string ReadLines( TimeSpan timeout, bool trimSpaces, int expectedLength )
        {
            try
            {
                var listBuilder = new System.Text.StringBuilder();
                this.StoreCommunicationTimeout( timeout );
                int currentLength = 0;
                while ( currentLength < expectedLength )
                {
                    string buffer = this.ReadLine();
                    expectedLength += buffer.Length;
                    _ = trimSpaces ? listBuilder.AppendLine( buffer.Trim() ) : listBuilder.AppendLine( buffer );
                }

                return listBuilder.ToString();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.RestoreCommunicationTimeout();
            }
        }

        #endregion

        #region " DISCARD UNREAD DATA "


        /// <summary>
        /// Reads and discards all data from the VISA session until the END indicator is read.
        /// </summary>
        /// <remarks> Uses 10ms poll delay, 100ms timeout. </remarks>
        public void DiscardUnreadData()
        {
            this.DiscardUnreadData( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 100d ) );
        }

        /// <summary> Information describing the discarded. </summary>
        private System.Text.StringBuilder _DiscardedData;

        /// <summary> Gets the information describing the discarded. </summary>
        /// <value> Information describing the discarded. </value>
        public string DiscardedData => this._DiscardedData is null ? string.Empty : this._DiscardedData.ToString();

        /// <summary>
        /// Reads and discards all data from the VISA session until the END indicator is read and no data
        /// are added to the output buffer.
        /// </summary>
        /// <param name="pollDelay"> Time to wait between service requests. </param>
        /// <param name="timeout">   Specifies the time to wait for message available. </param>
        public void DiscardUnreadData( TimeSpan pollDelay, TimeSpan timeout )
        {

            // to make sure this does not last forever.
            var endTime = DateTime.UtcNow.Add( TimeSpan.FromSeconds( 2d ) );
            this._DiscardedData = new System.Text.StringBuilder();
            do
            {
                // allow message available time to materialize
                _ = Core.ApplianceBase.DoEventsWaitUntil( pollDelay, timeout, () => this.QueryMessageAvailableStatus() );
                if ( this.MessageAvailable )
                    _ = this._DiscardedData.AppendLine( this.ReadLine() );
            }
            while ( this.MessageAvailable && DateTime.UtcNow < endTime );
        }

        #endregion

        #region " TIMEOUT MANAGEMENT "

        /// <summary> Gets or sets the reference to the stack of communication timeouts. </summary>
        /// <value> The timeouts. </value>
        protected Stack<TimeSpan> CommunicationTimeouts { get; private set; }

        private TimeSpan _TimeoutCnandidate;

        /// <summary> Gets or sets the milliseconds timeout candidate. </summary>
        /// <value> The milliseconds timeout candidate. </value>
        public TimeSpan TimeoutCandidate
        {
            get => this._TimeoutCnandidate;

            set {
                if ( this.TimeoutCandidate != value )
                {
                    this._TimeoutCnandidate = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Restores the last timeout from the stack. </summary>
        public void RestoreCommunicationTimeout()
        {
            if ( this.CommunicationTimeouts.Any() )
            {
                this.CommunicationTimeout = this.CommunicationTimeouts.Pop();
            }

            this.TimeoutCandidate = this.CommunicationTimeout;
        }

        /// <summary> Saves the current timeout and sets a new setting timeout. </summary>
        /// <param name="timeout"> Specifies the new timeout. </param>
        public void StoreCommunicationTimeout( TimeSpan timeout )
        {
            this.CommunicationTimeouts.Push( this.CommunicationTimeout );
            this.CommunicationTimeout = timeout;
            this.TimeoutCandidate = timeout;
        }

        #endregion

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "Session Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        public void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DefaultOpenSessionTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResourceTitle ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClosedCaption ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.DefaultOpenSessionTimeout ):
                    {
                        this.DefaultOpenTimeout = My.Settings.Default.DefaultOpenSessionTimeout;
                        break;
                    }

                case nameof( My.MySettings.SessionClosureRefractoryPeriod ):
                    {
                        break;
                    }

                case nameof( My.MySettings.ClosedCaption ):
                    {
                        this.ResourceClosedCaption = My.Settings.Default.ClosedCaption;
                        break;
                    }

                case nameof( My.MySettings.ResourceTitle ):
                    {
                        this.CandidateResourceTitle = My.Settings.Default.ResourceTitle;
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.DeviceClearRefractoryPeriod = My.Settings.Default.DeviceClearRefractoryPeriod;
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.StatusReadTurnaroundTime = My.Settings.Default.StatusReadTurnaroundTime;
                        break;
                    }
            }
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleSettingsPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.Settings.Default )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = Core.WindowsForms.ShowDialogOkay( $"Exception {activity}--ignored;. {ex.ToFullBlownString()}", "Exception Occurred", Core.MyMessageBoxIcon.Error );
            }
        }

        #endregion

        #region " STATUS PROMPT "

        /// <summary> The status prompt. </summary>
        private string _StatusPrompt;

        /// <summary> Gets or sets a prompt describing the Status. </summary>
        /// <value> A prompt describing the Status. </value>
        public string StatusPrompt
        {
            get => this._StatusPrompt;

            set {
                this._StatusPrompt = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion

    }

    /// <summary> Values that represent notify Synchronization levels. </summary>
    public enum NotifySyncLevel
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "No notification" )]
        None = 0,

        /// <summary> An enum constant representing the sync] option. </summary>
        [Description( "Synchronize" )]
        Sync,

        /// <summary> An enum constant representing the async] option. </summary>
        [Description( "A-Synchronize" )]
        Async
    }
}
