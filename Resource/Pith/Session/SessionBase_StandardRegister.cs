using System;

using isr.Core.EnumExtensions;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        /// <summary> The default standard event enable bitmask. </summary>
        private StandardEvents _DefaultStandardEventEnableBitmask;

        /// <summary> Gets or sets the default standard event enable bitmask. </summary>
        /// <value> The default standard event enable bitmask. </value>
        public StandardEvents DefaultStandardEventEnableBitmask
        {
            get => this._DefaultStandardEventEnableBitmask;

            set {
                if ( value != this.DefaultStandardEventEnableBitmask )
                {
                    this._DefaultStandardEventEnableBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Returns a detailed report of the event status register (ESR) byte. </summary>
        /// <param name="value">     Specifies the value that was read from the status register. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> Returns a detailed report of the event status register (ESR) byte. </returns>
        public static string BuildReport( StandardEvents value, string delimiter )
        {
            if ( string.IsNullOrWhiteSpace( delimiter ) )
            {
                delimiter = "; ";
            }

            var builder = new System.Text.StringBuilder();
            foreach ( StandardEvents eventValue in Enum.GetValues( typeof( StandardEvents ) ) )
            {
                if ( eventValue != StandardEvents.None && eventValue != StandardEvents.All && (eventValue & value) != 0 )
                {
                    if ( builder.Length > 0 )
                    {
                        _ = builder.Append( delimiter );
                    }

                    _ = builder.Append( eventValue.Description() );
                }
            }

            if ( builder.Length > 0 )
            {
                _ = builder.Append( "." );
                _ = builder.Insert( 0, string.Format( System.Globalization.CultureInfo.CurrentCulture, "The device standard status register reported: 0x{0:X2}{1}", ( int ) value, Environment.NewLine ) );
            }

            return builder.ToString();
        }

        /// <summary> The standard event status. </summary>
        private StandardEvents? _StandardEventStatus;

        /// <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
        /// <value>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
        /// Events</see>.
        /// </value>
        public StandardEvents? StandardEventStatus
        {
            get => this._StandardEventStatus;

            set {
                if ( !Nullable.Equals( value, this.StandardEventStatus ) )
                {
                    this._StandardEventStatus = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.StandardEventStatusHasValue ) );
                }

                this.StandardRegisterCaption = value.HasValue ? string.Format( RegisterValueFormat, ( int ) value.Value ) : UnknownRegisterValueCaption;
            }
        }

        /// <summary> Gets the standard event status has value. </summary>
        /// <value> The standard event status has value. </value>
        public bool StandardEventStatusHasValue => this.StandardEventStatus.HasValue;

        /// <summary> Gets or sets the standard event query command. </summary>
        /// <remarks> <see cref="VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand"></see> </remarks>
        /// <value> The standard event query command. </value>
        public string StandardEventStatusQueryCommand { get; set; } = Ieee488.Syntax.StandardEventStatusQueryCommand;

        /// <summary> Queries the Standard Event enable bit mask. </summary>
        /// <returns>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
        /// Events</see>.
        /// </returns>
        public StandardEvents? QueryStandardEventStatus()
        {
            if ( !string.IsNullOrWhiteSpace( this.StandardEventStatusQueryCommand ) )
            {
                this.StandardEventStatus = ( StandardEvents? ) this.Query( 0, this.StandardEventStatusQueryCommand );
            }

            return this.StandardEventStatus;
        }

        /// <summary> The standard register caption. </summary>
        private string _StandardRegisterCaption = "0x..";

        /// <summary> Gets or sets the Standard register caption. </summary>
        /// <value> The Standard register caption. </value>
        public string StandardRegisterCaption
        {
            get => this._StandardRegisterCaption;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.StandardRegisterCaption ) )
                {
                    this._StandardRegisterCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The standard event enable bitmask. </summary>
        private StandardEvents? _StandardEventEnableBitmask;

        /// <summary> Gets or sets the cached Standard Event enable bit mask. </summary>
        /// <value>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
        /// Events</see>.
        /// </value>
        public StandardEvents? StandardEventEnableBitmask
        {
            get => this._StandardEventEnableBitmask;

            protected set {
                if ( !this.StandardEventEnableBitmask.Equals( value ) )
                {
                    this._StandardEventEnableBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        /// <summary>
        /// Apply (Write and then read) bitmask setting the device to turn on the Event Summary Bit (ESM)
        /// upon any of the SCPI events unmasked by the bitmask. Uses *ERE to select (unmask) the events
        /// that will set the ESB.
        /// </summary>
        /// <param name="bitmask"> The bitmask; zero to disable all events. </param>
        public void ApplyStandardEventEnableBitmask( StandardEvents bitmask )
        {
            this.WriteStandardEventEnableBitmask( bitmask );
            _ = this.QueryStandardEventEnableBitmask();
        }


        /// <summary>
        /// Apply (Write and then read) bitmask setting the device to turn on the Event Summary Bit (ESM)
        /// upon any of the SCPI events unmasked by the bitmask. Uses *ERE to select (unmask) the events
        /// that will set the ESB.
        /// </summary>
        /// <param name="commandFormat"> The standard event enable command format. </param>
        /// <param name="bitmask">       The bitmask; zero to disable all events. </param>
        public void ApplyStandardEventEnableBitmask( string commandFormat, StandardEvents bitmask )
        {
            this.StandardEventEnableCommandFormat = commandFormat;
            this.ApplyStandardEventEnableBitmask( bitmask );
        }

        /// <summary> Gets the standard event enable query command. </summary>
        /// <remarks>
        /// SCPI: "*ESE?".
        /// <see cref="VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand"> </see>
        /// </remarks>
        /// <value> The standard event enable query command. </value>
        public string StandardEventEnableQueryCommand { get; set; } = Ieee488.Syntax.StandardEventEnableQueryCommand;

        /// <summary> Queries the Standard Event enable bit mask. </summary>
        /// <returns>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.StandardEvents">Standard
        /// Events</see>.
        /// </returns>
        public StandardEvents? QueryStandardEventEnableBitmask()
        {
            if ( !string.IsNullOrWhiteSpace( this.StandardEventEnableQueryCommand ) )
            {
                this.StandardEventEnableBitmask = ( StandardEvents? ) this.Query( 0, this.StandardEventEnableQueryCommand );
            }

            return this.StandardEventEnableBitmask;
        }

        /// <summary> Gets the supports standard event enable query. </summary>
        /// <value> The supports standard event enable query. </value>
        public bool SupportsStandardEventEnableQuery => !string.IsNullOrWhiteSpace( this.StandardEventEnableQueryCommand );

        /// <summary> Gets or sets the standard event enable command format. </summary>
        /// <remarks>
        /// SCPI: "*ESE {0:D}".
        /// <see cref="VI.Pith.Ieee488.Syntax.StandardEventEnableCommandFormat"> </see>
        /// </remarks>
        /// <value> The standard event enable command format. </value>
        public string StandardEventEnableCommandFormat { get; set; } = Ieee488.Syntax.StandardEventEnableCommandFormat;

        /// <summary> Writes the Standard Event enable bitmask. </summary>
        /// <param name="bitmask"> The bitmask; zero to disable all events. </param>
        public void WriteStandardEventEnableBitmask( StandardEvents bitmask )
        {
            if ( string.IsNullOrWhiteSpace( this.StandardEventEnableCommandFormat ) )
            {
                this.StandardEventEnableBitmask = StandardEvents.None;
            }
            else
            {
                _ = this.WriteLine( this.StandardEventEnableCommandFormat, ( object ) ( int ) bitmask );
                this.StandardEventEnableBitmask = bitmask;
            }
        }
    }
}
