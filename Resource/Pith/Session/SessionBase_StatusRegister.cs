using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.TimeSpanExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        #region " SERVICE REQUEST REGISTER EVENTS: REPORT "

        /// <summary> Returns a detailed report of the service request register (SRQ) byte. </summary>
        /// <param name="value">     Specifies the value that was read from the service request register. </param>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> The structured report. </returns>
        public static string BuildReport( ServiceRequests value, string delimiter )
        {
            if ( string.IsNullOrWhiteSpace( delimiter ) )
            {
                delimiter = "; ";
            }

            var builder = new System.Text.StringBuilder();
            foreach ( ServiceRequests element in Enum.GetValues( typeof( ServiceRequests ) ) )
            {
                if ( (element & value) != 0 && element != ServiceRequests.All )
                {
                    if ( builder.Length > 0 )
                    {
                        _ = builder.Append( delimiter );
                    }

                    _ = builder.Append( element.Description() );
                }
            }

            return builder.ToString();
        }

        #endregion

        #region " SUSPENSION "

        /// <summary> Number of suspended service requested. </summary>
        private int _SuspendedServiceRequestedCount;

        /// <summary> Gets or sets the number of services requested while suspended. </summary>
        /// <value> The number of services requested. </value>
        public int SuspendedServiceRequestedCount
        {
            get => this._SuspendedServiceRequestedCount;

            set {
                if ( this.SuspendedServiceRequestedCount != value )
                {
                    this._SuspendedServiceRequestedCount = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the type of the service request. </summary>
        /// <value> The type of the service request. </value>
        public string ServiceRequestType { get; set; }

        /// <summary> Gets or sets the service request handling suspended. </summary>
        /// <value> The service request handling suspended. </value>
        public bool ServiceRequestHandlingSuspended { get; set; }

        /// <summary> Resume service request handing. </summary>
        public virtual void ResumeServiceRequestHanding()
        {
            this.LastAction = "Resuming service request handing";
            if ( this.ServiceRequestEventEnabled )
            {
                this.ClearLastError();
                this.SuspendedServiceRequestedCount = 0;
                this.ServiceRequestType = string.Empty;
                this.DiscardServiceRequests();
            }

            this.ServiceRequestHandlingSuspended = false;
        }

        /// <summary> Suspends the service request handling. </summary>
        public virtual void SuspendServiceRequestHanding()
        {
            this.LastAction = "Suspending service request handing";
            if ( this.ServiceRequestEventEnabled )
            {
                this.ClearLastError();
                this.SuspendedServiceRequestedCount = 0;
                this.ServiceRequestType = string.Empty;
                this.DiscardServiceRequests();
            }

            this.ServiceRequestHandlingSuspended = true;
        }

        #endregion

        #region " EVENT "

        /// <summary> Notifies of the ServiceRequested event. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnServiceRequested( EventArgs e )
        {
            this.SyncNotifyServiceRequested( e );
        }

        /// <summary> Removes the ServiceRequested event handlers. </summary>
        protected void RemoveServiceRequestedEventHandlers()
        {
            this._ServiceRequestedEventHandlers?.RemoveAll();
        }

        /// <summary> The ServiceRequested event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _ServiceRequestedEventHandlers = new();

        /// <summary> Event queue for all listeners interested in ServiceRequested events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> ServiceRequested
        {
            add {
                this._ServiceRequestedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._ServiceRequestedEventHandlers.RemoveValue( value );
            }
        }

        private void OnServiceRequested( object sender, EventArgs e )
        {
            this._ServiceRequestedEventHandlers.Post( sender, e );
        }


        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContextCollection{TEventArgs}.Send">sends</see> or invokes the
        /// <see cref="ServiceRequested">ServiceRequested Event</see>.
        /// </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyServiceRequested( EventArgs e )
        {
            this._ServiceRequestedEventHandlers.Send( this, e );
        }

        #endregion

        #region " SRQ ENABLE "

        /// <summary> The service request enable command. </summary>
        private string _ServiceRequestEnableCommand;

        /// <summary> Gets or sets the service request enable command. </summary>
        /// <value> The service request enable command. </value>
        public string ServiceRequestEnableCommand
        {
            get => this._ServiceRequestEnableCommand;

            protected set {
                if ( !string.Equals( this.ServiceRequestEnableCommand, value ) )
                {
                    this._ServiceRequestEnableCommand = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the emulated status byte. </summary>
        /// <value> The emulated status byte. </value>
        public ServiceRequests EmulatedStatusByte { get; set; }

        /// <summary> Makes emulated status byte. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReply( ServiceRequests value )
        {
            this.EmulatedStatusByte = value;
        }

        /// <summary> Makes emulated status byte if none. </summary>
        /// <param name="value"> The emulated value. </param>
        public void MakeEmulatedReplyIfEmpty( ServiceRequests value )
        {
            if ( this.EmulatedStatusByte == ServiceRequests.None )
            {
                this.MakeEmulatedReply( value );
            }
        }

        /// <summary> Queries if a service request is enabled. </summary>
        /// <param name="bitmask"> The bit mask. </param>
        /// <returns> <c>true</c> if a service request is enabled; otherwise <c>false</c> </returns>
        public bool IsServiceRequestEnabled( ServiceRequests bitmask )
        {
            return this.ServiceRequestEventEnabled && (this.ServiceRequestEnabledBitmask.GetValueOrDefault( ServiceRequests.None ) & bitmask) != 0;
        }

        /// <summary> The standard event enabled bitmask. </summary>
        private ServiceRequests? _ServiceRequestEnabledBitmask;

        /// <summary> Gets or sets the cached Standard Event enabled bit mask. </summary>
        /// <value>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Standard
        /// Events</see>.
        /// </value>
        public ServiceRequests? ServiceRequestEnabledBitmask
        {
            get => this._ServiceRequestEnabledBitmask;

            set {
                if ( !this.ServiceRequestEnabledBitmask.Equals( value ) )
                {
                    this._ServiceRequestEnabledBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The service request enable query command. </summary>
        private string _ServiceRequestEnableQueryCommand = Ieee488.Syntax.ServiceRequestEnableQueryCommand;

        /// <summary> Gets or sets the service request enable query command. </summary>
        /// <remarks>
        /// SCPI: "*SRE?".
        /// <see cref="VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand"> </see>
        /// </remarks>
        /// <value> The service request enable command format. </value>
        public string ServiceRequestEnableQueryCommand
        {
            get => this._ServiceRequestEnableQueryCommand;

            set {
                if ( !string.Equals( value, this.ServiceRequestEnableQueryCommand ) )
                {
                    this._ServiceRequestEnableQueryCommand = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The service request enable command format. </summary>
        private string _ServiceRequestEnableCommandFormat;

        /// <summary> Gets or sets the service request enable command format. </summary>
        /// <remarks>
        /// SCPI: "*SRE {0:D}".
        /// <see cref="VI.Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat"> </see>
        /// </remarks>
        /// <value> The service request enable command format. </value>
        public string ServiceRequestEnableCommandFormat
        {
            get => this._ServiceRequestEnableCommandFormat;

            set {
                if ( !string.Equals( value, this.ServiceRequestEnableCommandFormat ) )
                {
                    this._ServiceRequestEnableCommandFormat = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Queries the service request enable bit mask. </summary>
        /// <returns>
        /// <c>null</c> if value is not known; otherwise <see cref="int">Service Requests</see>.
        /// </returns>
        public int QueryServiceRequestEnableBitmask()
        {
            int value = default;
            if ( !string.IsNullOrWhiteSpace( this.ServiceRequestEnableQueryCommand ) )
            {
                value = this.Query( 0, this.ServiceRequestEnableQueryCommand );
                this.ServiceRequestEnabledBitmask = ( ServiceRequests ) Conversions.ToInteger( value );
            }
            return value;
        }

        /// <summary> Gets the supports service request enable query. </summary>
        /// <value> The supports service request enable query. </value>
        public bool SupportsServiceRequestEnableQuery => !string.IsNullOrWhiteSpace( this.ServiceRequestEnableQueryCommand );


        /// <summary>
        /// Apply (Write and then read) bitmask setting the device to turn on the Requesting Service
        /// (RQS) / Master Summary Status (MSS) bit and issue an SRQ upon any of the SCPI events unmasked
        /// by the bitmask. Uses *SRE to select (unmask) the events that will issue an SRQ.
        /// </summary>
        /// <param name="bitmask"> The bitmask; zero to disable all events. </param>
        public void ApplyServiceRequestEnableBitmask( ServiceRequests bitmask )
        {
            this.WriteServiceRequestEnableBitmask( bitmask );
            _ = this.QueryServiceRequestEnableBitmask();
        }


        /// <summary>
        /// Apply (Write and then read) bitmask setting the device to turn on the Requesting Service
        /// (RQS) / Master Summary Status (MSS) bit and issue an SRQ upon any of the SCPI events unmasked
        /// by the bitmask. Uses *SRE to select (unmask) the events that will issue an SRQ.
        /// </summary>
        /// <param name="commandFormat"> The service request enable command format. </param>
        /// <param name="bitmask">       The bitmask; zero to disable all events. </param>
        public void ApplyServiceRequestEnableBitmask( string commandFormat, ServiceRequests bitmask )
        {
            this.ServiceRequestEnableCommandFormat = commandFormat;
            this.ApplyServiceRequestEnableBitmask( bitmask );
        }

        /// <summary> Writes a service request enable bitmask. </summary>
        /// <param name="bitmask"> The bitmask; zero to disable all events. </param>
        public void WriteServiceRequestEnableBitmask( ServiceRequests bitmask )
        {
            if ( string.IsNullOrWhiteSpace( this.ServiceRequestEnableCommandFormat ) )
            {
                this.ServiceRequestEnabledBitmask = ServiceRequests.None;
                this.ServiceRequestEnableCommand = string.Empty;
            }
            else
            {
                _ = this.WriteLine( this.ServiceRequestEnableCommandFormat, ( int ) bitmask );
                this.ServiceRequestEnabledBitmask = bitmask;
                this.ServiceRequestEnableCommand = string.Format( this.ServiceRequestEnableCommandFormat, ( int ) bitmask );
            }
        }

        /// <summary> Returns <c>True</c> is this instrument supports service request enable. </summary>
        /// <value> The supports service request enable. </value>
        public bool SupportsServiceRequestEnable => !string.IsNullOrWhiteSpace( this.ServiceRequestEnableCommandFormat );

        /// <summary> Emulate service request. </summary>
        /// <param name="statusByte"> The status byte. </param>
        public void EmulateServiceRequest( ServiceRequests statusByte )
        {
            this.EmulatedStatusByte = statusByte;
            if ( this.ServiceRequestEventEnabled )
                this.OnServiceRequested( EventArgs.Empty );
        }


        /// <summary>
        /// Gets or sets or set the sentinel indication if a service request event handler was enabled
        /// and registered.
        /// </summary>
        /// <value>
        /// <c>True</c> if service request event is enabled and registered; otherwise, <c>False</c>.
        /// </value>
        public abstract bool ServiceRequestEventEnabled { get; set; }

        /// <summary> Discard pending service requests. </summary>
        public abstract void DiscardServiceRequests();

        /// <summary> Enables and adds the service request event handler. </summary>
        public abstract void EnableServiceRequestEventHandler();

        /// <summary> Disables and removes the service request event handler. </summary>
        public abstract void DisableServiceRequestEventHandler();

        #endregion

        #region " STANDARD SERVICE REQUEST ENABLE "

        /// <summary> The default requesting service bitmask. </summary>
        private ServiceRequests _DefaultRequestingServiceBitmask = ServiceRequests.StandardEvent;

        /// <summary> Gets or sets the default requesting service bitmask. </summary>
        /// <value> The default requesting service bitmask. </value>
        public ServiceRequests DefaultRequestingServiceBitmask
        {
            get => this._DefaultRequestingServiceBitmask;

            set {
                if ( value != this.DefaultRequestingServiceBitmask )
                {
                    this._DefaultRequestingServiceBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The default operation complete bitmask. </summary>
        private ServiceRequests _DefaultOperationCompleteBitmask;

        /// <summary> Gets or sets the default operation complete bitmask. </summary>
        /// <value> The default operation complete bitmask. </value>
        public ServiceRequests DefaultOperationCompleteBitmask
        {
            get => this._DefaultOperationCompleteBitmask;

            set {
                if ( value != this.DefaultOperationCompleteBitmask )
                {
                    this._DefaultOperationCompleteBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The default operation service request enable bitmask. </summary>
        private ServiceRequests _DefaultOperationServiceRequestEnableBitmask;

        /// <summary> Gets or sets the default Operation service request enable bitmask. </summary>
        /// <remarks>
        /// Exclude message available to prevent handling a service request on messages.
        /// </remarks>
        /// <value> The default Operation service request enable bitmask. </value>
        public ServiceRequests DefaultOperationServiceRequestEnableBitmask
        {
            get => this._DefaultOperationServiceRequestEnableBitmask;

            set {
                if ( value != this.DefaultOperationServiceRequestEnableBitmask )
                {
                    this._DefaultOperationServiceRequestEnableBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The default service request enable bitmask. </summary>
        private ServiceRequests _DefaultServiceRequestEnableBitmask;

        /// <summary> Gets or sets the default service request enable bitmask. </summary>
        /// <value> The default service request enable bitmask. </value>
        public ServiceRequests DefaultServiceRequestEnableBitmask
        {
            get => this._DefaultServiceRequestEnableBitmask;

            set {
                if ( value != this.DefaultServiceRequestEnableBitmask )
                {
                    this._DefaultServiceRequestEnableBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The standard service enable command. </summary>
        private string _StandardServiceEnableCommand;

        /// <summary> Gets or sets the standard service enable command. </summary>
        /// <remarks>
        /// This command combines the commands for <see cref="ClearActiveState()"/>,
        /// <see cref="ServiceRequestEnableCommandFormat"/> and
        /// <see cref="StandardEventEnableCommandFormat"/>. <para>
        /// SCPI: *CLS; *ESE {0:D}; *SRE {1:D}</para>
        /// </remarks>
        /// <value> The standard service enable command. </value>
        public string StandardServiceEnableCommand
        {
            get => this._StandardServiceEnableCommand;

            protected set {
                if ( !string.Equals( value, this.ServiceRequestEnableCommand ) )
                {
                    this._StandardServiceEnableCommand = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the standard service enable command format. </summary>
        /// <remarks>
        /// <see cref="VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat"></see>
        /// This command combines the commands for <see cref="ClearActiveState()"/>,
        /// <see cref="ServiceRequestEnableCommandFormat"/> and
        /// <see cref="StandardEventEnableCommandFormat"/>. <para>
        /// SCPI: *CLS; *ESE {0:D}; *SRE {1:D}</para>
        /// </remarks>
        /// <value> The standard service enable command format. </value>
        public string StandardServiceEnableCommandFormat { get; set; } = Ieee488.Syntax.StandardServiceEnableCommandFormat;


        /// <summary>
        /// Applies (Writes and then reads) the standard and service bitmasks to unmask events to let the
        /// device set the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and
        /// Master Summary Status (MSS) upon any of the unmasked SCPI events. Uses *ESE to select
        /// (unmask) the events that set the ESB and *SRE to select (unmask) the events that will set the
        /// Master Summary Status (MSS) bit for requesting service. Does not issue *OPC; Does not define
        /// the default wait complete bitmasks.
        /// </summary>
        /// <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
        /// <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
        /// SRQ. </param>
        public void ApplyStandardServiceRequestEnableBitmasks( StandardEvents standardEventEnableBitmask, ServiceRequests serviceRequestEnableBitmask )
        {
            _ = this.ReadStatusRegister();
            if ( string.IsNullOrWhiteSpace( this.StandardServiceEnableCommandFormat ) )
            {
                this.StandardServiceEnableCommand = string.Empty;
            }
            else
            {
                string command = this.WriteLine( this.StandardServiceEnableCommandFormat, ( int ) standardEventEnableBitmask, ( int ) serviceRequestEnableBitmask );
                this.StandardServiceEnableCommand = command;
                _ = this.QueryServiceRequestEnableBitmask();
                _ = this.QueryStandardEventEnableBitmask();
            }
        }


        /// <summary>
        /// Writes the standard and service bitmasks to unmask events to let the device set the Event
        /// Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
        /// (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
        /// the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
        /// bit for requesting service. Does not issue *OPC; Does not define the default wait complete
        /// bitmasks.
        /// </summary>
        /// <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
        /// <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
        /// SRQ. </param>
        public void WriteStandardServiceRequestEnableBitmasks( StandardEvents standardEventEnableBitmask, ServiceRequests serviceRequestEnableBitmask )
        {
            _ = this.ReadStatusRegister();
            if ( string.IsNullOrWhiteSpace( this.StandardServiceEnableCommandFormat ) )
            {
                this.StandardServiceEnableCommand = string.Empty;
            }
            else
            {
                string command = this.WriteLine( this.StandardServiceEnableCommandFormat, ( int ) standardEventEnableBitmask, ( int ) serviceRequestEnableBitmask );
                this.StandardServiceEnableCommand = command;
                this.ServiceRequestEnabledBitmask = serviceRequestEnableBitmask;
                this.StandardEventEnableBitmask = standardEventEnableBitmask;
            }
        }


        /// <summary>
        /// Applies (Writes and then reads) the standard and service bitmasks to unmask events to let the
        /// device set the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and
        /// Master Summary Status (MSS) upon any of the unmasked SCPI events. Uses *ESE to select
        /// (unmask) the events that set the ESB and *SRE to select (unmask) the events that will set the
        /// Master Summary Status (MSS) bit for requesting service. Does not issue *OPC; Does not define
        /// the default wait complete bitmasks.
        /// </summary>
        /// <remarks> 3475. Add Or VI.Pith.Ieee4882.ServiceRequests.OperationEvent. </remarks>
        public void ApplyStandardServiceRequestEnableBitmasks()
        {
            this.ApplyStandardServiceRequestEnableBitmasks( this.DefaultStandardEventEnableBitmask, this.DefaultOperationCompleteBitmask );
        }


        /// <summary>
        /// Gets or sets the standard service operation complete enable command format.
        /// </summary>
        /// <remarks>
        /// <see cref="VI.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat"> </see>
        /// This command combines the commands for <see cref="ClearActiveState()"/>,
        /// <see cref="ServiceRequestEnableCommandFormat"/>,
        /// <see cref="StandardEventEnableCommandFormat"/>, and <see cref="OperationCompleteCommand"/>.
        /// <para>
        /// SCPI: *CLS; *ESE {0:D}; *SRE {1:D}; *OPC</para>
        /// </remarks>
        /// <value> The standard service enable operation complete enable command format. </value>
        public string StandardServiceEnableCompleteCommandFormat { get; set; } = Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat;


        /// <summary>
        /// Applies (Writes and then reads) the standard and service bitmasks to unmask events to let the
        /// device set the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and
        /// Master Summary Status (MSS) upon any of the unmasked SCPI events. Uses *ESE to select
        /// (unmask) the events that set the ESB and *SRE to select (unmask) the events that will set the
        /// Master Summary Status (MSS) bit for requesting service. Also issues *OPC and defines the
        /// default wait complete bitmasks.
        /// </summary>
        /// <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
        /// <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
        /// SRQ. </param>
        public void ApplyStandardServiceCompleteEnableBitmasks( StandardEvents standardEventEnableBitmask, ServiceRequests serviceRequestEnableBitmask )
        {
            _ = this.ReadStatusRegister();
            if ( string.IsNullOrWhiteSpace( this.StandardServiceEnableCompleteCommandFormat ) )
            {
                this.ServiceRequestEnabledBitmask = ServiceRequests.None;
                this.StandardEventEnableBitmask = StandardEvents.None;
            }
            else
            {
                _ = this.WriteLine( this.StandardServiceEnableCompleteCommandFormat, ( int ) standardEventEnableBitmask, ( int ) serviceRequestEnableBitmask );
                if ( this.SupportsServiceRequestEnableQuery )
                {
                    _ = this.QueryServiceRequestEnableBitmask();
                }
                else
                {
                    this.ServiceRequestEnabledBitmask = serviceRequestEnableBitmask;
                }

                if ( this.ServiceRequestEnabledBitmask.GetValueOrDefault( ServiceRequests.None ) != ServiceRequests.None )
                {
                    this.ServiceRequestWaitCompleteEnabledBitmask = this.ServiceRequestEnabledBitmask.GetValueOrDefault( ServiceRequests.None );
                }

                if ( this.SupportsStandardEventEnableQuery )
                {
                    _ = this.QueryStandardEventEnableBitmask();
                }
                else
                {
                    this.StandardEventEnableBitmask = standardEventEnableBitmask;
                }

                if ( this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None ) != StandardEvents.None )
                {
                    this.StandardEventWaitCompleteEnabledBitmask = this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None );
                }
            }
        }


        /// <summary>
        /// Writes the standard and service bitmasks to unmask events to let the device set the Event
        /// Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
        /// (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
        /// the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
        /// bit for requesting service. Also issues *OPC and defines the default wait complete bitmasks.
        /// </summary>
        /// <param name="standardEventEnableBitmask">  Specifies standard events will issue an SRQ. </param>
        /// <param name="serviceRequestEnableBitmask"> Specifies which status registers will issue an
        /// SRQ. </param>
        public void WriteStandardServiceCompleteEnableBitmasks( StandardEvents standardEventEnableBitmask, ServiceRequests serviceRequestEnableBitmask )
        {
            _ = this.ReadStatusRegister();
            if ( string.IsNullOrWhiteSpace( this.StandardServiceEnableCompleteCommandFormat ) )
            {
                this.ServiceRequestEnabledBitmask = ServiceRequests.None;
                this.StandardEventEnableBitmask = StandardEvents.None;
            }
            else
            {
                _ = this.WriteLine( this.StandardServiceEnableCompleteCommandFormat, ( int ) standardEventEnableBitmask, ( int ) serviceRequestEnableBitmask );
                this.ServiceRequestEnabledBitmask = serviceRequestEnableBitmask;
                if ( this.ServiceRequestEnabledBitmask.GetValueOrDefault( ServiceRequests.None ) != ServiceRequests.None )
                {
                    this.ServiceRequestWaitCompleteEnabledBitmask = this.ServiceRequestEnabledBitmask.GetValueOrDefault( ServiceRequests.None );
                }

                this.StandardEventEnableBitmask = standardEventEnableBitmask;
                if ( this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None ) != StandardEvents.None )
                {
                    this.StandardEventWaitCompleteEnabledBitmask = this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None );
                }
            }
        }

        /// <summary> Gets or sets the operation complete enable command format. </summary>
        /// <remarks>
        /// <see cref="VI.Pith.Ieee488.Syntax.OperationCompleteEnableCommandFormat"> </see>
        /// This command combines the commands for <see cref="ClearActiveState()"/>,
        /// <see cref="StandardEventEnableCommandFormat"/>, and <see cref="OperationCompleteCommand"/>.
        /// <para>
        /// SCPI: *CLS; *ESE {0:D}; *OPC</para>
        /// </remarks>
        /// <value> The operation complete enable complete format. </value>
        public string OperationCompleteEnableCommandFormat { get; set; } = Ieee488.Syntax.OperationCompleteEnableCommandFormat;


        /// <summary>
        /// Applies (Writes and then reads) the standard bitmask to unmask events to let the device set
        /// the Event Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> upon any of the
        /// SCPI events. Uses *ESE to select (unmask) the events that set the ESB. Also issues *OPC and
        /// defines the default wait complete bitmasks.
        /// </summary>
        /// <param name="standardEventEnableBitmask"> Specifies standard events will issue an SRQ. </param>
        public void ApplyOperationCompleteEnableBitmask( StandardEvents standardEventEnableBitmask )
        {
            _ = this.ReadStatusRegister();
            if ( string.IsNullOrWhiteSpace( this.OperationCompleteEnableCommandFormat ) )
            {
                this.ServiceRequestEnabledBitmask = ServiceRequests.None;
                this.StandardEventEnableBitmask = StandardEvents.None;
            }
            else
            {
                _ = this.WriteLine( this.OperationCompleteEnableCommandFormat, ( object ) ( int ) standardEventEnableBitmask );
                if ( this.SupportsStandardEventEnableQuery )
                {
                    _ = this.QueryStandardEventEnableBitmask();
                }
                else
                {
                    this.StandardEventEnableBitmask = standardEventEnableBitmask;
                }

                if ( this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None ) != StandardEvents.None )
                {
                    this.StandardEventWaitCompleteEnabledBitmask = this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None );
                }
            }
        }


        /// <summary>
        /// Writes the standard bitmask to unmask events to let the device set the Event Summary bit
        /// (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> upon any of the SCPI events. Uses *ESE
        /// to select (unmask) the events that set the ESB. Also issues *OPC and defines the default wait
        /// complete bitmasks.
        /// </summary>
        /// <param name="standardEventEnableBitmask"> Specifies standard events will issue an SRQ. </param>
        public void WriteOperationCompleteEnableBitmask( StandardEvents standardEventEnableBitmask )
        {
            _ = this.ReadStatusRegister();
            if ( string.IsNullOrWhiteSpace( this.OperationCompleteEnableCommandFormat ) )
            {
                this.ServiceRequestEnabledBitmask = ServiceRequests.None;
                this.StandardEventEnableBitmask = StandardEvents.None;
            }
            else
            {
                _ = this.WriteLine( this.OperationCompleteEnableCommandFormat, ( object ) ( int ) standardEventEnableBitmask );
                this.StandardEventEnableBitmask = standardEventEnableBitmask;
                if ( this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None ) != StandardEvents.None )
                {
                    this.StandardEventWaitCompleteEnabledBitmask = this.StandardEventEnableBitmask.GetValueOrDefault( StandardEvents.None );
                }
            }
        }

        #endregion

        #region " SERVICE REQUEST STATUS "

        /// <summary> Bitmask value changed. </summary>
        /// <param name="bitmask">  The bit mask. </param>
        /// <param name="oldValue"> The old value. </param>
        /// <param name="newValue"> The value. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool BitmaskValueChanged( int bitmask, int oldValue, int newValue )
        {
            return (bitmask & oldValue) != (bitmask & newValue);
        }

        /// <summary> Applies the service request described by value. </summary>
        /// <param name="value"> Specifies the value that was read from the service request register. </param>
        /// <returns> The VI.Pith.ServiceRequests. </returns>
        public ServiceRequests ApplyServiceRequest( ServiceRequests value )
        {
            return this.ApplyServiceRequest( this.ServiceRequestStatus, value );
        }

        /// <summary> Applies the service request described by value. </summary>
        /// <param name="oldValue"> The old value. </param>
        /// <param name="newValue"> The value. </param>
        /// <returns> The VI.Pith.ServiceRequests. </returns>
        protected ServiceRequests ApplyServiceRequest( ServiceRequests oldValue, ServiceRequests newValue )
        {

            // save the new status value
            this.ServiceRequestStatus = newValue;

            // error notification does not cause fetching device error is a message is available.
            if ( BitmaskValueChanged( ( int ) this.ErrorAvailableBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.ErrorAvailable ) );
            if ( BitmaskValueChanged( ( int ) this.MessageAvailableBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.MessageAvailable ) );
            if ( BitmaskValueChanged( ( int ) this.MeasurementEventBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.HasMeasurementEvent ) );
            if ( BitmaskValueChanged( ( int ) this.SystemEventBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.HasSystemEvent ) );
            if ( BitmaskValueChanged( ( int ) this.QuestionableEventBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.HasQuestionableEvent ) );
            if ( BitmaskValueChanged( ( int ) this.StandardEventBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.HasStandardEvent ) );
            if ( BitmaskValueChanged( ( int ) this.RequestingServiceBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.RequestingService ) );
            if ( BitmaskValueChanged( ( int ) this.OperationEventBit, ( int ) oldValue, ( int ) newValue ) )
                this.NotifyPropertyChanged( nameof( this.HasOperationEvent ) );

            // Notify is status changed
            if ( !newValue.Equals( oldValue ) )
                this.SyncNotifyPropertyChanged( nameof( this.ServiceRequestStatus ) );

            // ignore device error in the presence of a message in order to prevent query interrupted errors.
            if ( !this.MessageAvailable && this.ErrorAvailable )
                this.SyncNotifyDeviceErrorOccurred( EventArgs.Empty );

            // update the status caption
            this.StatusRegisterCaption = string.Format( RegisterValueFormat, ( int ) newValue );
            return newValue;
        }

        /// <summary> Gets or sets the cached service request Status. </summary>
        /// <remarks>
        /// The service request status is posted to be parsed by the status subsystem that is specific to
        /// the instrument at hand. This is critical to the proper workings of the status subsystem. The
        /// service request status is posted asynchronously. This may not be processed fast enough to
        /// determine the next action. Not sure how to address this at this time.
        /// </remarks>
        /// <value>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Service
        /// Requests</see>.
        /// </value>
        public ServiceRequests ServiceRequestStatus { get; private set; }

        /// <summary> Delay read status register. </summary>
        /// <returns> The VI.Pith.ServiceRequests. </returns>
        public ServiceRequests DelayReadStatusRegister()
        {
            return this.ReadStatusRegister( this.StatusReadDelay );
        }

        /// <summary> Reads the status register. </summary>
        /// <param name="readDelay"> The read delay. </param>
        /// <returns>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Service
        /// Requests</see>.
        /// </returns>
        public ServiceRequests ReadStatusRegister( TimeSpan readDelay )
        {
            ApplianceBase.DoEventsWait( readDelay );
            return this.ApplyServiceRequest( this.ReadStatusByte() );
        }

        /// <summary> Reads the status register. </summary>
        /// <returns>
        /// <c>null</c> if value is not known; otherwise <see cref="VI.Pith.ServiceRequests">Service
        /// Requests</see>.
        /// </returns>
        public ServiceRequests ReadStatusRegister()
        {
            return this.ApplyServiceRequest( this.ReadStatusByte() );
        }

        /// <summary> The unknown register value caption. </summary>
        /// <value> The unknown register value caption. </value>
        protected static string UnknownRegisterValueCaption { get; set; } = "0x..";

        /// <summary> The register value format. </summary>
        /// <value> The register value format. </value>
        protected static string RegisterValueFormat { get; set; } = "0x{0:X2}";

        /// <summary> The status register caption. </summary>
        private string _StatusRegisterCaption = "0x..";

        /// <summary> Gets or sets the status register caption. </summary>
        /// <value> The status register caption. </value>
        public string StatusRegisterCaption
        {
            get => this._StatusRegisterCaption;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.StatusRegisterCaption ) )
                {
                    this._StatusRegisterCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SRQ WAIT FOR "

        /// <summary> The default status read turnaround time. </summary>
        private TimeSpan _DefaultStatusReadTurnaroundTime = TimeSpan.FromMilliseconds( 10d );

        /// <summary> Gets or sets the default status read turnaround time. </summary>
        /// <value> The default status read turnaround time. </value>
        public TimeSpan DefaultStatusReadTurnaroundTime
        {
            get => this._DefaultStatusReadTurnaroundTime;

            set {
                if ( !TimeSpan.Equals( value, this.DefaultStatusReadTurnaroundTime ) )
                {
                    this._DefaultStatusReadTurnaroundTime = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The status read turnaround time. </summary>
        private TimeSpan _StatusReadTurnaroundTime;

        /// <summary> Gets or sets the status read turnaround time. </summary>
        /// <value> The status read turnaround time. </value>
        public TimeSpan StatusReadTurnaroundTime
        {
            get => this._StatusReadTurnaroundTime;

            set {
                if ( !TimeSpan.Equals( value, this.StatusReadTurnaroundTime ) )
                {
                    this._StatusReadTurnaroundTime = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the effective status read turnaround time. </summary>
        /// <value> The effective status read turnaround time. </value>
        public TimeSpan EffectiveStatusReadTurnaroundTime => this.StatusReadTurnaroundTime > TimeSpan.Zero ? this.StatusReadTurnaroundTime : this.DefaultStatusReadTurnaroundTime > TimeSpan.Zero ? this.DefaultStatusReadTurnaroundTime : TimeSpan.FromMilliseconds( 10d );

        /// <summary> Await service request. </summary>
        /// <remarks>
        /// Delay defaults to <see cref="StatusReadDelay"/> and poll delay default to
        /// <see cref="EffectiveStatusReadTurnaroundTime"/>
        /// </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A ( bool, ServiceRequests, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public virtual (bool TimedOut, ServiceRequests StatusByte, TimeSpan Elapsed) AwaitServiceRequest( TimeSpan timeout )
        {
            return this.AwaitStatusBitmask( this.DefaultRequestingServiceBitmask, timeout );
        }

        /// <summary> Await status register bitmask. </summary>
        /// <remarks>
        /// Delay defaults to <see cref="StatusReadDelay"/> and poll delay default to
        /// <see cref="EffectiveStatusReadTurnaroundTime"/>
        /// </remarks>
        /// <param name="bitmask"> Specifies the anticipated status bitmask. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A ( bool, ServiceRequests, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public virtual (bool TimedOut, ServiceRequests StatusByte, TimeSpan Elapsed) AwaitStatusBitmask( ServiceRequests bitmask, TimeSpan timeout )
        {
            // emulate the reply for disconnected operations.
            this.MakeEmulatedReplyIfEmpty( bitmask );
            var cts = new System.Threading.CancellationTokenSource();
            var t = StartAwaitingBitmaskTask( bitmask, timeout, this.StatusReadDelay, this.EffectiveStatusReadTurnaroundTime, () => this.ReadStatusByte() );
            t.Wait();
            return t.Result;
        }

        /// <summary>   Await status register bitmask. </summary>
        /// <remarks>
        /// Delay defaults to <see cref="StatusReadDelay"/> and poll delay default to
        /// <see cref="EffectiveStatusReadTurnaroundTime"/>
        /// </remarks>
        /// <param name="bitmask">  Specifies the anticipated status bitmask. </param>
        /// <param name="timeout">  The timeout. </param>
        /// <returns> A ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public virtual (bool TimedOut, int StatusByte, TimeSpan Elapsed) AwaitStatusBitmask( byte bitmask, TimeSpan timeout )
        {
            // emulate the reply for disconnected operations.
            this.MakeEmulatedReplyIfEmpty( bitmask );
            var cts = new System.Threading.CancellationTokenSource();
            var t = StartAwaitingBitmaskTask( bitmask, timeout, this.StatusReadDelay, this.EffectiveStatusReadTurnaroundTime, () => ( byte ) this.ReadStatusByte() );
            t.Wait();
            return t.Result;
        }

        /// <summary>   Await status register bitmask. </summary>
        /// <remarks>
        /// Delay defaults to <see cref="StatusReadDelay"/> and poll delay default to
        /// <see cref="EffectiveStatusReadTurnaroundTime"/>
        /// </remarks>
        /// <param name="bitmask">          Specifies the anticipated status bitmask. </param>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="queryStatusError"> The status read. </param>
        /// <returns>   A Tuple: ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public virtual (bool TimedOut, int StatusByte, TimeSpan Elapsed) AwaitStatusBitmask( byte bitmask, TimeSpan timeout, Func<(bool, string, int)> queryStatusError )
        {
            // emulate the reply for disconnected operations.
            this.MakeEmulatedReplyIfEmpty( bitmask );
            var cts = new System.Threading.CancellationTokenSource();
            var t = StartAwaitingBitmaskTask( bitmask, timeout, this.StatusReadDelay, this.EffectiveStatusReadTurnaroundTime, queryStatusError );
            t.Wait();
            return t.Result;
        }

        /// <summary>   Await status. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="queryStatusDone">  The query status done. </param>
        /// <returns>   A Tuple: ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public virtual (bool TimedOut, int StatusByte, TimeSpan Elapsed) AwaitStatus( TimeSpan timeout, Func<(bool, int)> queryStatusDone )
        {
            // emulate the reply for disconnected operations.
            this.MakeEmulatedReplyIfEmpty( 0 );
            var cts = new System.Threading.CancellationTokenSource();
            var t = StartAwaitingStatusTask( timeout, this.StatusReadDelay, this.EffectiveStatusReadTurnaroundTime, queryStatusDone );
            t.Wait();
            return t.Result;
        }

        /// <summary> Await status register bitmask. </summary>
        /// <param name="bitmask">   Specifies the anticipated status bitmask. </param>
        /// <param name="timeout">   The timeout. </param>
        /// <param name="delay">     The delay. </param>
        /// <param name="pollDelay"> The poll delay. </param>
        /// <returns> A ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public (bool TimedOut, ServiceRequests StatusByte, TimeSpan Elapsed) AwaitStatusBitmask( ServiceRequests bitmask, TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay )
        {
            // emulate the reply for disconnected operations.
            this.MakeEmulatedReplyIfEmpty( bitmask );
            var cts = new System.Threading.CancellationTokenSource();
            var t = StartAwaitingBitmaskTask( bitmask, timeout, delay, pollDelay, () => this.ReadStatusByte() );
            t.Wait();
            return t.Result;
        }

        #endregion

        #region " TASKS STARTING METHODS "


        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask"> Specifies the anticipated bitmask. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="action">  The action. </param>
        /// <returns> A Threading.Tasks.Task(Of ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time).</returns>
        public static Task<(bool TimedOut, int StatusByte, TimeSpan Elpased)> StartAwaitingBitmaskTask( int bitmask, TimeSpan timeout, Func<int> action )
        {
            return Task.Factory.StartNew<(bool, int, TimeSpan)>( () => {
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                int reading = action.Invoke();
                if ( timeout <= TimeSpan.Zero )
                    return (false, reading, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( (bitmask & reading) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    reading = action.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading, sw.Elapsed);
            } );
        }


        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask"> Specifies the anticipated bitmask. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="action">  The action. </param>
        /// <returns> A Threading.Tasks.Task(Of ( bool, int?, TimeSpan )) (true if timed out, status byte, elapsed time).</returns>
        public static Task<(bool TimedOut, int? StatusByte, TimeSpan Elapsed)> StartAwaitingBitmaskTask( int bitmask, TimeSpan timeout, Func<int?> action )
        {
            return Task.Factory.StartNew<(bool, int?, TimeSpan)>( () => {
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                var reading = action.Invoke();
                if ( timeout <= TimeSpan.Zero )
                    return (false, reading, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( (bitmask & reading) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    reading = action.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading, sw.Elapsed);
            } );
        }


        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask"> Specifies the anticipated bitmask. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <param name="action">  The action. </param>
        /// <returns> A Threading.Tasks.Task(Of ( bool, ServiceRequests, TimeSpan )) (true if timed out, status byte, elapsed time).</returns>
        public static Task<(bool TimedOut, ServiceRequests StatusByte, TimeSpan Elapsed)> StartAwaitingBitmaskTask( ServiceRequests bitmask, TimeSpan timeout, Func<ServiceRequests> action )
        {
            return Task.Factory.StartNew<(bool, ServiceRequests, TimeSpan)>( () => {
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                var reading = action.Invoke();
                if ( timeout <= TimeSpan.Zero )
                    return (false, reading, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( (bitmask & reading) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    reading = action.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading, sw.Elapsed);
            } );
        }


        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask">   Specifies the anticipated bitmask. </param>
        /// <param name="timeout">   The timeout. </param>
        /// <param name="delay">     The delay. </param>
        /// <param name="pollDelay"> Specifies time between serial polls. </param>
        /// <param name="action">    The action. </param>
        /// <returns> A Threading.Tasks.Task(Of ( bool, int?, TimeSpan )) (true if timed out, status byte, elapsed time).</returns>
        public static Task<(bool TimedOut, int? StatusByte, TimeSpan Elapsed)> StartAwaitingBitmaskTask( int bitmask, TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay, Func<int?> action )
        {
            return Task.Factory.StartNew<(bool, int?, TimeSpan)>( () => {
                delay.SpinWait();
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                var reading = action.Invoke();
                if ( timeout <= TimeSpan.Zero )
                    return (false, reading, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( (bitmask & reading) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    pollDelay.SpinWait();
                    reading = action.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading, sw.Elapsed);
            } );
        }

        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask">          Specifies the anticipated bitmask. </param>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="delay">            The delay. </param>
        /// <param name="pollDelay">        Specifies time between serial polls. </param>
        /// <param name="queryStatusError"> The status read. </param>
        /// <returns>
        /// A Threading.Tasks.Task(Of ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time).
        /// </returns>
        public static Task<(bool TimedOut, int StatusByte, TimeSpan Elapsed)> StartAwaitingBitmaskTask( int bitmask, TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay,
                                                                                    Func<(bool hasError, string details, int statusByte)> queryStatusError )
        {
            return Task.Factory.StartNew<(bool, int, TimeSpan)>( () => {
                delay.SpinWait();
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                var reading = queryStatusError.Invoke();
                if ( timeout <= TimeSpan.Zero || reading.hasError )
                    return (false, reading.statusByte, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( !reading.hasError && (bitmask & reading.statusByte) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    pollDelay.SpinWait();
                    reading = queryStatusError.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading.statusByte, sw.Elapsed);
            } );
        }

        /// <summary>   Starts awaiting status. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="delay">            The delay. </param>
        /// <param name="pollDelay">        Specifies time between serial polls. </param>
        /// <param name="queryStatusDone">  The query status done. </param>
        /// <returns>   A Tuple: ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public static Task<(bool TimedOut, int StatusByte, TimeSpan Elapsed)> StartAwaitingStatusTask( TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay,
            Func<(bool done, int statusByte)> queryStatusDone )
        {
            return Task.Factory.StartNew<(bool, int, TimeSpan)>( () => {
                delay.SpinWait();
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                var reading = queryStatusDone.Invoke();
                if ( timeout <= TimeSpan.Zero || reading.done )
                    return (false, reading.statusByte, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( !reading.done && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    pollDelay.SpinWait();
                    reading = queryStatusDone.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading.statusByte, sw.Elapsed);
            } );
        }


        /// <summary>   Await status. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="isStatusReady">    The is status ready. </param>
        /// <returns>   A Tuple: ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) </returns>
        public virtual (bool TimedOut, int StatusByte, TimeSpan Elapsed) AwaitStatus( TimeSpan timeout, Func<int, bool> isStatusReady )
        {
            // emulate the reply for disconnected operations.
            this.MakeEmulatedReplyIfEmpty( 0 );
            var cts = new System.Threading.CancellationTokenSource();
            var t = this.StartAwaitingStatusTask( timeout, this.StatusReadDelay, this.EffectiveStatusReadTurnaroundTime, isStatusReady );
            t.Wait();
            return t.Result;
        }


        /// <summary>   Starts awaiting status. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="delay">            The delay. </param>
        /// <param name="pollDelay">        Specifies time between serial polls. </param>
        /// <param name="isStatusReady">    The is status ready. </param>
        /// <returns>   A Tuple: ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time) 
        /// Elapsed time includes the <paramref name="delay"/> and therefore might exceed the <paramref name="timeout"/></returns>
        public Task<(bool TimedOut, int StatusByte, TimeSpan Elapsed)> StartAwaitingStatusTask( TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay,
            Func<int, bool> isStatusReady )
        {
            return Task.Factory.StartNew<(bool, int, TimeSpan)>( () => {
                TimeSpan elapsed = TimeSpan.Zero;
                delay.SpinWait();
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                int statusByte = ( int ) this.ReadStatusByte();
                var reading = isStatusReady.Invoke( statusByte );
                if ( timeout <= TimeSpan.Zero || reading )
                {
                    elapsed = sw.Elapsed;
                    elapsed = elapsed.Add( delay );
                    return (false, statusByte, elapsed);
                }
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( !reading && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    pollDelay.SpinWait();
                    statusByte = ( int ) this.ReadStatusByte();
                    reading = isStatusReady.Invoke( statusByte );
                    timedOut = sw.ElapsedTicks >= ticks;
                }
                elapsed = sw.Elapsed;
                elapsed = elapsed.Add( delay );
                return (timedOut, statusByte, sw.Elapsed.Add( delay ));
            } );
        }


        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wit(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask">   Specifies the anticipated bitmask. </param>
        /// <param name="timeout">   The timeout. </param>
        /// <param name="delay">     The delay. </param>
        /// <param name="pollDelay"> Specifies time between serial polls. </param>
        /// <param name="action">    The action. </param>
        /// <returns> A Threading.Tasks.Task(Of ( bool, int, TimeSpan )) (true if timed out, status byte, elapsed time).
        /// Elapsed time includes the <paramref name="delay"/> and therefore might exceed the <paramref name="timeout"/></returns>
        public static Task<(bool TimedOut, int StatusByte, TimeSpan Elapsed)> StartAwaitingBitmaskTask( int bitmask, TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay, Func<int> action )
        {
            return Task.Factory.StartNew<(bool, int, TimeSpan)>( () => {
                TimeSpan elapsed = TimeSpan.Zero;
                delay.SpinWait();
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                int reading = action.Invoke();
                if ( timeout <= TimeSpan.Zero )
                {
                    elapsed = sw.Elapsed;
                    elapsed = elapsed.Add( delay );
                    return (false, reading, elapsed);
                }
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( (bitmask & reading) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    pollDelay.SpinWait();
                    reading = action.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }
                elapsed = sw.Elapsed;
                elapsed = elapsed.Add( delay );
                return (timedOut, reading, sw.Elapsed);
            } );
        }

        /// <summary>
        /// Starts a task waiting for a bitmask. The task complete after a timeout or if the action
        /// function value matches the bitmask value.
        /// </summary>
        /// <remarks>
        /// The task timeout is included in the task function. Otherwise, upon Wait(timeout), the task
        /// deadlocks attempting to get the task result. For more information see
        /// https://blog.stephencleary.com/2012/07/dont-block-on-async-code.html. That document is short
        /// on examples for how to resolve this issue.
        /// </remarks>
        /// <param name="bitmask">   Specifies the anticipated bitmask. </param>
        /// <param name="timeout">   The timeout. </param>
        /// <param name="delay">     The delay. </param>
        /// <param name="pollDelay"> Specifies time between serial polls. </param>
        /// <param name="action">    The action. </param>
        /// <returns> A Threading.Tasks.Task(Of ( bool, ServiceRequests, TimeSpan )) (true if timed out, status byte, elapsed time).</returns>
        public static Task<(bool TimedOut, ServiceRequests StatusByte, TimeSpan Elapsed)> StartAwaitingBitmaskTask( ServiceRequests bitmask, TimeSpan timeout, TimeSpan delay, TimeSpan pollDelay, Func<ServiceRequests> action )
        {
            return Task.Factory.StartNew<(bool, ServiceRequests, TimeSpan)>( () => {
                delay.SpinWait();
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                var reading = action.Invoke();
                if ( timeout <= TimeSpan.Zero )
                    return (false, reading, TimeSpan.Zero);
                bool timedOut = sw.ElapsedTicks >= ticks;
                while ( (bitmask & reading) != bitmask && !timedOut )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                    pollDelay.SpinWait();
                    reading = action.Invoke();
                    timedOut = sw.ElapsedTicks >= ticks;
                }

                return (timedOut, reading, sw.Elapsed);
            } );
        }

        #endregion

        #region " REGISTERS "

        /// <summary> Sets the subsystem registers to their reset state. </summary>
        private void ResetRegistersKnownState()
        {
            this.StandardEventStatus = new StandardEvents?();
        }

        /// <summary> Reads the event registers based on the service request register status. </summary>
        public void ReadStandardEventRegisters()
        {
            if ( this.HasStandardEvent )
                _ = this.QueryStandardEventStatus();
        }

        #endregion

    }
}
