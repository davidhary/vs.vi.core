using System;
using System.Diagnostics;

using isr.Core;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        /// <summary> Initializes the service request register bits. </summary>
        private void InitializeServiceRequestRegisterBitsThis()
        {
            this._MeasurementEventBit = ServiceRequests.MeasurementEvent;
            this._SystemEventBit = ServiceRequests.SystemEvent;
            this._ErrorAvailableBit = ServiceRequests.ErrorAvailable;
            this._QuestionableEventBit = ServiceRequests.QuestionableEvent;
            this._MessageAvailableBit = ServiceRequests.MessageAvailable;
            this._StandardEventBit = ServiceRequests.StandardEvent;
            this._RequestingServiceBit = ServiceRequests.RequestingService;
            this._OperationEventBit = ServiceRequests.OperationEvent;
        }

        #region " BIT MASKING "

        /// <summary> Query if 'statusValue' is fully bit-masked. </summary>
        /// <param name="statusValue"> The status value. </param>
        /// <param name="bitmask">     The bitmask. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool IsFullyMasked( int statusValue, int bitmask )
        {
            return bitmask != 0 && (statusValue & bitmask) == bitmask;
        }

        /// <summary> Checks if the service request status if fully masked. </summary>
        /// <param name="bitmask"> The bitmask. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool IsFullyMasked( ServiceRequests bitmask )
        {
            return IsFullyMasked( ( int ) this.ServiceRequestStatus, ( int ) bitmask );
        }

        /// <summary> Query if 'bitmask' is partially masked. </summary>
        /// <param name="statusValue"> The status value. </param>
        /// <param name="bitmask">     The bitmask. </param>
        /// <returns> <c>true</c> if partially masked; otherwise <c>false</c> </returns>
        public static bool IsPartiallyMasked( int statusValue, int bitmask )
        {
            return bitmask != 0 && (statusValue & bitmask) != 0;
        }

        /// <summary> Query if 'bitmask' is partially masked. </summary>
        /// <param name="bitmask"> The bitmask. </param>
        /// <returns> <c>true</c> if partially masked; otherwise <c>false</c> </returns>
        public bool IsPartiallyMasked( ServiceRequests bitmask )
        {
            return IsPartiallyMasked( ( int ) this.ServiceRequestStatus, ( int ) bitmask );
        }

        /// <summary> Count set bits. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> The total number of set bits. </returns>
        public static int CountSetBits( int value )
        {
            int count = 0;
            while ( value > 0 )
            {
                count += value & 1;
                value >>= 1;
            }

            return count;
        }

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: MEASUREMENT "

        /// <summary> The measurement event bit. </summary>
        private ServiceRequests _MeasurementEventBit;


        /// <summary>
        /// Gets or sets the bit that would be set when an enabled measurement event has occurred.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Measurement event bit value. </value>
        public ServiceRequests MeasurementEventBit
        {
            get => this._MeasurementEventBit;

            set {
                if ( !value.Equals( this.MeasurementEventBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Measurement Event Status bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._MeasurementEventBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether an enabled measurement event has occurred. </summary>
        /// <value>
        /// <c>True</c> if an enabled measurement event has occurred; otherwise, <c>False</c>.
        /// </value>
        public bool HasMeasurementEvent => 0 != (this.ServiceRequestStatus & this.MeasurementEventBit);

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: SYSTEM EVENT "

        /// <summary> The system event bit. </summary>
        private ServiceRequests _SystemEventBit;


        /// <summary>
        /// Gets or sets the bit that would be set for detecting if a System Event has occurred.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The System Event bit value. </value>
        public ServiceRequests SystemEventBit
        {
            get => this._SystemEventBit;

            set {
                if ( !value.Equals( this.SystemEventBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"System Event Status bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._SystemEventBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether a System Event has occurred. </summary>
        /// <value> <c>True</c> if System Event bit is on; otherwise, <c>False</c>. </value>
        public bool HasSystemEvent => 0 != (this.ServiceRequestStatus & this.SystemEventBit);

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: ERROR "

        /// <summary> The error available bit. </summary>
        private ServiceRequests _ErrorAvailableBit;

        /// <summary> Gets or sets the bit that would be set if an error event has occurred. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The error event bit. </value>
        public ServiceRequests ErrorAvailableBit
        {
            get => this._ErrorAvailableBit;

            set {
                if ( !value.Equals( this.ErrorAvailableBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Error Available Status Bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._ErrorAvailableBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether [Error available]. </summary>
        /// <value> <c>True</c> if [Error available]; otherwise, <c>False</c>. </value>
        public bool ErrorAvailable => this.IsErrorBitSet( this.ServiceRequestStatus );

        /// <summary>
        /// Reads the status registers and returns true if the error bit is set; Does not affect the
        /// cached <see cref="ErrorAvailable"/> property.
        /// </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   <c>true</c> if it error bit is on; otherwise <c>false</c> </returns>
        public bool IsErrorBitSet( ServiceRequests statusByte )
        {
            return (this.ErrorAvailableBit & statusByte) != 0;
        }

        /// <summary>
        /// Reads the status registers and returns true if the error bit is set; Does not affect the
        /// cached <see cref="ErrorAvailable"/> property.
        /// </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   <c>true</c> if it error bit is on; otherwise <c>false</c> </returns>
        public bool IsErrorBitSet( int statusByte )
        {
            return (( int ) this.ErrorAvailableBit & statusByte) != 0;
        }

        /// <summary>
        /// Reads the status registers and returns true if the error bit is set; Does not affect the
        /// cached <see cref="ErrorAvailable"/> property.
        /// </summary>
        /// <returns> <c>true</c> if it error bit is on; otherwise <c>false</c> </returns>
        public bool IsErrorBitSet()
        {
            // read status byte without affecting the error available property.
            return this.IsErrorBitSet( this.ReadStatusByte() );
        }

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: QUESTIONABLE "

        /// <summary> The questionable event bit. </summary>
        private ServiceRequests _QuestionableEventBit;


        /// <summary>
        /// Gets or sets the bit that would be set if a Questionable event has occurred.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Questionable event bit. </value>
        public ServiceRequests QuestionableEventBit
        {
            get => this._QuestionableEventBit;

            set {
                if ( !value.Equals( this.QuestionableEventBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Questionable Event Status bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._QuestionableEventBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether a Questionable event has occurred. </summary>
        /// <value> <c>True</c> if Questionable event has occurred; otherwise, <c>False</c>. </value>
        public bool HasQuestionableEvent => 0 != (this.ServiceRequestStatus & this.QuestionableEventBit);

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: MESSAGE "

        /// <summary> The message available bit. </summary>
        private ServiceRequests _MessageAvailableBit;

        /// <summary> Gets or sets the bit that would be set if a message is available. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Message available bit. </value>
        public ServiceRequests MessageAvailableBit
        {
            get => this._MessageAvailableBit;

            set {
                if ( !value.Equals( this.MessageAvailableBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Message Available Status Bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._MessageAvailableBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether a message is available. </summary>
        /// <value> <c>True</c> if a message is available; otherwise, <c>False</c>. </value>
        public bool MessageAvailable => this.IsMessageAvailable( this.ServiceRequestStatus );

        /// <summary>   Queries if a message is available. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if the message is available, false if not. </returns>
        public bool IsMessageAvailable( ServiceRequests statusByte )
        {
            return this.MessageAvailableBit == (statusByte & this.MessageAvailableBit);
        }

        /// <summary>   Queries if a message is available. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if the message is available, false if not. </returns>
        public bool IsMessageAvailable( int statusByte )
        {
            return ( int ) this.MessageAvailableBit == (statusByte & ( int ) this.MessageAvailableBit);
        }

        /// <summary> Reads the status register a few times or until a message is available. </summary>
        /// <remarks>
        /// Delays looking for the message status by the status latency to make sure the instrument had
        /// enough time to process the previous command.
        /// </remarks>
        /// <param name="latency">     The latency. </param>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <returns> <c>True</c> if message is available. </returns>
        public bool QueryMessageAvailableStatus( TimeSpan latency, int repeatCount )
        {
            do
            {
                if ( latency > TimeSpan.Zero )
                    ApplianceBase.Delay( latency );
                _ = this.ReadStatusRegister();
                repeatCount -= 1;
            }
            while ( repeatCount > 0 && !this.MessageAvailable );
            return this.MessageAvailable;
        }

        /// <summary> Reads the status byte and returns true if a message is available. </summary>
        /// <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
        public bool QueryMessageAvailableStatus()
        {
            _ = this.ReadStatusRegister();
            return this.MessageAvailable;
        }

        /// <summary> Reads the status register until timeout or a message is available. </summary>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      Specifies the time to wait for message available. </param>
        /// <returns> <c>True</c> if the message available bit is on; otherwise, <c>False</c>. </returns>
        public bool QueryMessageAvailableStatus( TimeSpan pollInterval, TimeSpan timeout )
        {
            var sw = Stopwatch.StartNew();
            bool messageAvailable = this.QueryMessageAvailableStatus();
            while ( !messageAvailable && sw.Elapsed <= timeout )
            {
                System.Threading.Thread.SpinWait( 10 );
                ApplianceBase.DoEventsWait( pollInterval );
                messageAvailable = this.QueryMessageAvailableStatus();
            }

            return messageAvailable;
        }

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: STANDARD EVENT "

        /// <summary> The standard event bit. </summary>
        private ServiceRequests _StandardEventBit;


        /// <summary>
        /// Gets or sets bit that would be set if an enabled standard event has occurred.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Standard Event bit. </value>
        public ServiceRequests StandardEventBit
        {
            get => this._StandardEventBit;

            set {
                if ( !value.Equals( this.StandardEventBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Standard Event Status bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._StandardEventBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether an enabled standard event has occurred. </summary>
        /// <value>
        /// <c>True</c> if an enabled standard event has occurred; otherwise, <c>False</c>.
        /// </value>
        public bool HasStandardEvent => 0 != (this.ServiceRequestStatus & this.StandardEventBit);

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: SERVICE EVENT (SRQ) "

        /// <summary> The requesting service bit. </summary>
        private ServiceRequests _RequestingServiceBit = ServiceRequests.RequestingService;


        /// <summary>
        /// Gets or sets bit that would be set if a requested service or master summary status event has
        /// occurred.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The requested service or master summary status bit. </value>
        public ServiceRequests RequestingServiceBit
        {
            get => this._RequestingServiceBit;

            set {
                if ( !value.Equals( this.RequestingServiceBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Requesting Service Status Bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._RequestingServiceBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating if the device has requested service. </summary>
        /// <value> <c>True</c> if the device has requested service; otherwise, <c>False</c>. </value>
        public bool RequestingService => 0 != (this.ServiceRequestStatus & this.RequestingServiceBit);

        #endregion

        #region " SERVICE REQUEST REGISTER EVENTS: OPERATION "

        /// <summary> The operation event bit. </summary>
        private ServiceRequests _OperationEventBit;

        /// <summary> Gets or sets the bit that would be set if an operation event has occurred. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Operation event bit. </value>
        public ServiceRequests OperationEventBit
        {
            get => this._OperationEventBit;

            set {
                if ( !value.Equals( this.OperationEventBit ) )
                {
                    if ( CountSetBits( ( int ) value ) > 1 )
                    {
                        throw new InvalidOperationException( $"Operation Event Status bit cannot be set to a value={value} having more than one bit" );
                    }

                    this._OperationEventBit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets a value indicating whether an operation event has occurred. </summary>
        /// <value> <c>True</c> if an operation event has occurred; otherwise, <c>False</c>. </value>
        public bool HasOperationEvent => 0 != (this.ServiceRequestStatus & this.OperationEventBit);

        #endregion

        #region " EVENTS: DEVICE ERROR OCCURRED "

        /// <summary> Removes the <see cref="DeviceErrorOccurred"/> event handlers. </summary>
        protected void RemoveDeviceErrorOccurredEventHandlers()
        {
            this._DeviceErrorOccurredEventHandlers?.RemoveAll();
        }

        /// <summary> The  <see cref="DeviceErrorOccurred"/>  event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _DeviceErrorOccurredEventHandlers = new();

        /// <summary> Event queue for all listeners interested in <see cref="DeviceErrorOccurred"/> events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> DeviceErrorOccurred
        {
            add {
                this._DeviceErrorOccurredEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._DeviceErrorOccurredEventHandlers.RemoveValue( value );
            }
        }

        private void OnDeviceErrorOccurred( object sender, EventArgs e )
        {
            this._DeviceErrorOccurredEventHandlers.Post( sender, e );
        }


        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContextCollection{TEventArgs}.Send">sends</see> or invokes the
        /// <see cref="DeviceErrorOccurred">Error Occurred Event</see>.
        /// </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyDeviceErrorOccurred( EventArgs e )
        {
            this._DeviceErrorOccurredEventHandlers.Send( this, e );
        }

        #endregion

        #region " EVENT HANDLER ERROR "

        /// <summary>   Event queue for all listeners interested in EventHandlerError events. </summary>
        public event EventHandler<isr.Core.ErrorEventArgs> EventHandlerError;

        /// <summary>   Raises the isr. core. error event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected virtual void OnEventHandlerError( isr.Core.ErrorEventArgs e )
        {
            EventHandler<isr.Core.ErrorEventArgs> evt = this.EventHandlerError;
            evt?.Invoke( this, e );
        }

        /// <summary>   Raises the isr. core. error event. </summary>
        /// <remarks>   David, 2021-05-03. </remarks>
        /// <param name="ex">   The exception. </param>
        protected virtual void OnEventHandlerError( Exception ex )
        {
            this.OnEventHandlerError( new isr.Core.ErrorEventArgs( ex ) );
        }

        #endregion

    }
}
