using System;
using System.Timers;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

#if false
        /// <summary> Gets the last input output stopwatch. </summary>
        /// <remarks> Keeps track of the time since the last message based operation. </remarks>
        /// <value> The last input output stopwatch. </value>
        protected Stopwatch LastInputOutputStopwatch { get; private set; }
#endif

        /// <summary>   Restarts the keep alive timer. </summary>
        /// <remarks>   David, 2021-06-01. </remarks>
        protected void RestartKeepAliveTimer()
        {
            if ( this._LazyKeepAliveTimer.IsValueCreated )
            {
                this.KeepAliveTimer.Stop();
                this.KeepAliveTimer.Start();
            }
        }

        private TimeSpan _KeepAliveInterval = TimeSpan.Zero;
        /// <summary>
        /// Gets the keep alive interval. Must be smaller or equal to half the communication timeout
        /// interval.
        /// </summary>
        /// <remarks> Required only with VISA Non-Standard. </remarks>
        /// <value> The keep alive interval. </value>
        public TimeSpan KeepAliveInterval
        {
            get => this._KeepAliveInterval;
            set {
                if ( (value != this.KeepAliveInterval) )
                {
                    if ( value == TimeSpan.Zero )
                    {
                        if ( this._LazyKeepAliveTimer.IsValueCreated )
                        {
                            this.KeepAliveTimer.Stop();
                        }
                    }
                    else
                    {
                        this.KeepAliveTimer.Stop();
                        this.KeepAliveTimer.Interval = value.TotalMilliseconds;
                        this.KeepAliveTimer.Start();
                    }
                    this._KeepAliveInterval = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        private readonly Lazy<System.Timers.Timer> _LazyKeepAliveTimer = new();

        /// <summary>   Gets or sets the keep alive timer. </summary>
        /// <value> The keep alive timer. </value>
        private System.Timers.Timer KeepAliveTimer
        {
            get {
                if ( !this._LazyKeepAliveTimer.IsValueCreated )
                    this._LazyKeepAliveTimer.Value.Elapsed += this.KeepAliveTimerElapsed;
                return this._LazyKeepAliveTimer.Value;
            }
        }

        /// <summary> Keep alive timer elapsed. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Elapsed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes" )]
        private void KeepAliveTimerElapsed( object sender, ElapsedEventArgs e )
        {
            try
            {
                if ( sender is System.Timers.Timer tmr )
                {
                    this.KeepAlive();
                }
            }
            catch ( Exception ex )
            {
                if ( sender is System.Timers.Timer tmr )
                {
                    tmr.Stop();
                }
                this.OnEventHandlerError( ex );
            }
            finally
            {
            }
        }

        private TimeSpan _KeepAliveLockTimeout;
        /// <summary>   Gets or sets the keep alive lock timeout. </summary>
        /// <value> The keep alive lock timeout. </value>
        public TimeSpan KeepAliveLockTimeout
        {
            get => this._KeepAliveLockTimeout;
            set {
                if ( value != this.KeepAliveLockTimeout )
                {
                    this._KeepAliveLockTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Keep alive. </summary>
        /// <remarks>   David, 2021-06-01. </remarks>
        public abstract void KeepAlive();


    }
}
