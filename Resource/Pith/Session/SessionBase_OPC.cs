using System;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        /// <summary> Gets or sets the wait command. </summary>
        /// <value> The wait command. </value>
        public string WaitCommand { get; set; } = Ieee488.Syntax.WaitCommand;

        /// <summary> Issues the wait command. </summary>
        public void Wait()
        {
            _ = this.WriteLine( this.WaitCommand );
        }

        /// <summary> The operation completed. </summary>
        private bool? _OperationCompleted;


        /// <summary>
        /// Gets or sets the cached value indicating whether the last operation completed.
        /// </summary>
        /// <value>
        /// <c>null</c> if operation completed contains no value, <c>True</c> if operation completed;
        /// otherwise, <c>False</c>.
        /// </value>
        public bool? OperationCompleted
        {
            get => this._OperationCompleted;

            set {
                if ( !Nullable.Equals( value, this.OperationCompleted ) )
                {
                    this._OperationCompleted = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the operation completed query command. </summary>
        /// <remarks>
        /// SCPI: "*OPC?".
        /// <see cref="VI.Pith.Ieee488.Syntax.OperationCompletedQueryCommand"> </see>
        /// </remarks>
        /// <value> The operation completed query command. </value>
        public string OperationCompletedQueryCommand { get; set; } = Ieee488.Syntax.OperationCompletedQueryCommand;

        /// <summary> Queries operation completed. </summary>
        /// <param name="timeout"> Specifies how long to wait for the service request before throwing
        /// the timeout exception. Set to zero for an infinite (120 seconds)
        /// timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool? QueryOperationCompleted( TimeSpan timeout )
        {
            this.OperationCompleted = this.Execute( this.QueryOperationCompleted, timeout );
            return default;
        }

        /// <summary> Issues the operation completion query, waits and returns a reply. </summary>
        /// <remarks> Sends the '*OPC?' query. </remarks>
        /// <returns> <c>True</c> if operation is complete; <c>False</c> otherwise. </returns>
        public bool? QueryOperationCompleted()
        {
            this.MakeEmulatedReplyIfEmpty( true );
            this.OperationCompleted = string.IsNullOrWhiteSpace( this.OperationCompletedQueryCommand ) || this.Query( true, this.OperationCompletedQueryCommand );
            return this.OperationCompleted;
        }

        /// <summary> The standard event wait complete enabled bitmask. </summary>
        private StandardEvents _StandardEventWaitCompleteEnabledBitmask;

        /// <summary> Gets or sets the standard event wait complete bitmask. </summary>
        /// <remarks> 3475. Add Or VI.Pith.Ieee4882.ServiceRequests.OperationEvent. </remarks>
        /// <value> The standard event wait complete bitmask. </value>
        public StandardEvents StandardEventWaitCompleteEnabledBitmask
        {
            get => this._StandardEventWaitCompleteEnabledBitmask;

            set {
                if ( value != this.StandardEventWaitCompleteEnabledBitmask )
                {
                    this._StandardEventWaitCompleteEnabledBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The service request wait complete enabled bitmask. </summary>
        private ServiceRequests _ServiceRequestWaitCompleteEnabledBitmask;

        /// <summary> Gets or sets the service request wait complete bitmask. </summary>
        /// <value> The service request wait complete bitmask. </value>
        public ServiceRequests ServiceRequestWaitCompleteEnabledBitmask
        {
            get => this._ServiceRequestWaitCompleteEnabledBitmask;

            set {
                if ( value != this.ServiceRequestWaitCompleteEnabledBitmask )
                {
                    this._ServiceRequestWaitCompleteEnabledBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The operation complete expected bitmask. </summary>
        private ServiceRequests _OperationCompleteExpectedBitmask;

        /// <summary> Gets or sets the expected service request operation complete bitmask. </summary>
        /// <value> The expected service request operation complete bitmask. </value>
        public ServiceRequests OperationCompleteExpectedBitmask
        {
            get => this._OperationCompleteExpectedBitmask;

            set {
                if ( value != this.OperationCompleteExpectedBitmask )
                {
                    this._OperationCompleteExpectedBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the operation completed command. </summary>
        /// <remarks>
        /// SCPI: "*OPC".
        /// <see cref="VI.Pith.Ieee488.Syntax.OperationCompleteCommand"> </see>
        /// </remarks>
        /// <value> The operation complete query command. </value>
        public string OperationCompleteCommand { get; set; } = Ieee488.Syntax.OperationCompleteCommand;

        /// <summary> Gets the supports operation complete. </summary>
        /// <value> The supports operation complete. </value>
        public bool SupportsOperationComplete => !string.IsNullOrWhiteSpace( this.OperationCompleteCommand );

        /// <summary> Issue operation complete. </summary>
        /// <remarks>
        /// When the *OPC command is sent, the instrument exits from the Operation Complete Command Idle
        /// State (OCIS) And enters the Operation Complete Command Active State (OCAS). In OCAS, the
        /// instrument continuously monitors the No-Operation-Pending flag. After the last pending
        /// overlapped command Is complete (NoOperation-Pending flag set to true), the Operation Complete
        /// (OPC) bit in the Standard Event Status Register sets, And the instrument goes back into OCIS.
        /// </remarks>
        public void IssueOperationComplete()
        {
            if ( this.SupportsOperationComplete )
                this.Execute( this.OperationCompleteCommand );
        }


        /// <summary>
        /// Enables service requests upon detection of completion. <para>
        /// Writes the standard and service bitmasks to unmask events to let the device set the Event
        /// Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
        /// (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
        /// the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
        /// bit for requesting service. Also issues *OPC.</para><para>
        /// This sets the default bitmasks.</para>
        /// </summary>
        public void EnableServiceRequestWaitComplete()
        {
            this.EnableServiceRequestWaitComplete( this.StandardEventWaitCompleteEnabledBitmask, this.ServiceRequestWaitCompleteEnabledBitmask );
        }


        /// <summary>
        /// Enables service requests upon detection of completion. <para>
        /// Writes the standard and service bitmasks to unmask events to let the device set the Event
        /// Summary bit (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/> and Master Summary Status
        /// (MSS) upon any of the unmasked SCPI events. Uses *ESE to select (unmask) the events that set
        /// the ESB and *SRE to select (unmask) the events that will set the Master Summary Status (MSS)
        /// bit for requesting service. Also issues *OPC.</para><para>
        /// This sets the default bitmasks.</para>
        /// </summary>
        /// <param name="standardEventEnableBitmask">  The standard event enable bitmask. </param>
        /// <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
        public void EnableServiceRequestWaitComplete( StandardEvents standardEventEnableBitmask, ServiceRequests serviceRequestEnableBitmask )
        {
            this.WriteStandardServiceCompleteEnableBitmasks( standardEventEnableBitmask, serviceRequestEnableBitmask );
        }


        /// <summary>
        /// Enables service requests upon detection of completion. <para>
        /// Writes the standard bitmask to unmask events to let the device set the Event Summary bit
        /// (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/>. Uses *ESE to select (unmask) the
        /// events that set the ESB. Also issues *OPC.</para><para>
        /// This sets the default bitmask.</para>
        /// </summary>
        public void EnableWaitComplete()
        {
            this.EnabletWaitComplete( this.StandardEventWaitCompleteEnabledBitmask );
        }


        /// <summary>
        /// Enables service requests upon detection of completion. <para>
        /// Writes the standard bitmask to unmask events to let the device set the Event Summary bit
        /// (ESB) <see cref="Pith.ServiceRequests.StandardEvent"/>. Uses *ESE to select (unmask) the
        /// events that set the ESB. Also issues *OPC.</para><para>
        /// This sets the default bitmask.</para>
        /// </summary>
        /// <param name="standardEventEnableBitmask"> The standard event enable bitmask. </param>
        public void EnabletWaitComplete( StandardEvents standardEventEnableBitmask )
        {
            this.WriteOperationCompleteEnableBitmask( standardEventEnableBitmask );
        }

        /// <summary> Waits for completion of last operation. </summary>
        /// <remarks> Assumes requesting service event is registered with the instrument. </remarks>
        /// <param name="timeout"> Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        /// <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
        public (bool TimedOut, ServiceRequests Status, TimeSpan Elapsed) AwaitOperationCompleted( TimeSpan timeout )
        {
            return this.AwaitOperationCompleted( this.OperationCompleteExpectedBitmask, timeout );
        }

        /// <summary> Waits for completion of last operation. </summary>
        /// <param name="value">   The value. </param>
        /// <param name="timeout"> Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        /// <returns> A (TimedOut As Boolean, Status As VI.Pith.ServiceRequests) </returns>
        public (bool TimedOut, ServiceRequests Status, TimeSpan Elapsed) AwaitOperationCompleted( ServiceRequests value, TimeSpan timeout )
        {
            return this.AwaitStatusBitmask( value, timeout, this.StatusReadDelay, this.EffectiveStatusReadTurnaroundTime );
        }

        /// <summary> Define service request wait complete bitmasks. </summary>
        public void DefineServiceRequestWaitCompleteBitmasks()
        {
            this.DefineServiceRequestWaitCompleteBitmasks( this.DefaultStandardEventEnableBitmask, this.DefaultOperationCompleteBitmask );
        }

        /// <summary> Define service request wait complete bitmasks. </summary>
        /// <param name="standardEventEnableBitmask">  The standard event enable bitmask. </param>
        /// <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
        public void DefineServiceRequestWaitCompleteBitmasks( StandardEvents standardEventEnableBitmask, ServiceRequests serviceRequestEnableBitmask )
        {
            this.ServiceRequestWaitCompleteEnabledBitmask = serviceRequestEnableBitmask;
            this.StandardEventWaitCompleteEnabledBitmask = standardEventEnableBitmask;
        }
    }
}
