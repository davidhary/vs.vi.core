using System;
using System.Linq;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        #region " RESOURCE NAME PARSE INFO "

        /// <summary> Gets or sets information describing the parsed resource name. </summary>
        /// <value> Information describing the resource. </value>
        public ResourceNameInfo ResourceNameInfo { get; private set; }

        #endregion

        #region " FILTER "

        /// <summary> A filter specifying the resources. </summary>
        private string _ResourcesFilter;

        /// <summary> Gets or sets the resources search pattern. </summary>
        /// <value> The resources search pattern. </value>
        public string ResourcesFilter
        {
            get => this._ResourcesFilter;

            set {
                if ( !string.Equals( value, this.ResourcesFilter ) )
                {
                    this._ResourcesFilter = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " NAME  "

        /// <summary> Updates the validated and open resource names and title. </summary>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The short title of the device. </param>
        public void HandleSessionOpen( string resourceName, string resourceTitle )
        {
            this.ValidatedResourceName = resourceName;
            this.CandidateResourceNameConnected = string.Equals( resourceName, this.CandidateResourceName, StringComparison.OrdinalIgnoreCase );
            this.OpenResourceName = resourceName;
            this.OpenResourceTitle = resourceTitle;
        }

        /// <summary> Gets the name of the designated resource. </summary>
        /// <value> The name of the designated resource. </value>
        public string DesignatedResourceName => this.CandidateResourceNameConnected ? this.ValidatedResourceName : this.CandidateResourceName;


        /// <summary>
        /// Checks if the candidate resource name exists. If so, assign to the
        /// <see cref="ValidatedResourceName">validated resource name</see>
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public abstract bool ValidateCandidateResourceName();


        /// <summary>
        /// Checks if the candidate resource name exists. If so, assign to the
        /// <see cref="ValidatedResourceName">validated resource name</see>;
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="manager"> The manager. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ValidateCandidateResourceName( ResourcesProviderBase manager )
        {
            if ( manager is null )
                throw new ArgumentNullException( nameof( manager ) );
            bool result = string.IsNullOrWhiteSpace( this.ResourcesFilter ) ? manager.FindResources().ToArray().Contains( this.CandidateResourceName ) : manager.FindResources( this.ResourcesFilter ).ToArray().Contains( this.CandidateResourceName );
            this.ValidatedResourceName = result ? this.CandidateResourceName : string.Empty;
            this.CandidateResourceNameConnected = result;
            return result;
        }

        /// <summary> Name of the validated resource. </summary>
        private string _ValidatedResourceName;

        /// <summary> Gets or sets the name of the validated (e.g., located) resource. </summary>
        /// <value> The name of the validated (e.g., located)  resource. </value>
        public string ValidatedResourceName
        {
            get => this._ValidatedResourceName;

            set {
                if ( !string.Equals( value, this.ValidatedResourceName ) )
                {
                    this._ValidatedResourceName = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> True if candidated resource name validated. </summary>
        private bool _CandidatedResourceNameValidated;

        /// <summary> Gets or sets the candidate resource name validated. </summary>
        /// <value> The candidate resource name validated. </value>
        public bool CandidateResourceNameConnected
        {
            get => this._CandidatedResourceNameValidated;

            set {
                if ( value != this.CandidateResourceNameConnected )
                {
                    this._CandidatedResourceNameValidated = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> Name of the candidate resource. </summary>
        private string _CandidateResourceName;

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <value> The name of the candidate resource. </value>
        public string CandidateResourceName
        {
            get => this._CandidateResourceName;

            set {
                if ( value is null )
                    value = string.Empty;
                if ( !string.Equals( this.CandidateResourceName, value ) )
                {
                    this._CandidateResourceName = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> Name of the open resource. </summary>
        private string _OpenResourceName;

        /// <summary> Gets or sets the name of the open resource. </summary>
        /// <value> The name of the open resource. </value>
        public string OpenResourceName
        {
            get => this._OpenResourceName;

            set {
                if ( value is null )
                    value = string.Empty;
                if ( !string.Equals( this.OpenResourceName, value ) )
                {
                    this._OpenResourceName = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> Updates the captions. </summary>
        private void UpdateCaptions()
        {
            if ( this.IsSessionOpen )
            {
                this.ResourceNameCaption = $"{this.OpenResourceTitle}.{this.OpenResourceName}";
                this.ResourceTitleCaption = this.OpenResourceTitle;
                this.CandidateResourceTitle = this.OpenResourceTitle;
            }
            else if ( string.IsNullOrWhiteSpace( this.OpenResourceTitle ) )
            {
                // if session was not open then display the candidate values
                this.ResourceNameCaption = $"{this.CandidateResourceTitle}.{this.CandidateResourceName}.{this.ResourceClosedCaption}";
                this.ResourceTitleCaption = $"{this.CandidateResourceTitle}.{this.ResourceClosedCaption}";
            }
            else
            {
                // if session was open then display the open values
                this.ResourceNameCaption = $"{this.OpenResourceTitle}.{this.OpenResourceName}.{this.ResourceClosedCaption}";
                this.ResourceTitleCaption = $"{this.OpenResourceTitle}.{this.ResourceClosedCaption}";
            }
        }

        #endregion

        #region " TITLE "

        /// <summary> The candidate resource title. </summary>
        private string _CandidateResourceTitle;

        /// <summary> Gets or sets the candidate resource title. </summary>
        /// <value> The candidate resource title. </value>
        public string CandidateResourceTitle
        {
            get => this._CandidateResourceTitle;

            set {
                if ( value is null )
                    value = string.Empty;
                if ( !string.Equals( value, this.CandidateResourceTitle ) )
                {
                    this._CandidateResourceTitle = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> The open resource title. </summary>
        private string _OpenResourceTitle;

        /// <summary> Gets or sets a short title for the device. </summary>
        /// <value> The short title of the device. </value>
        public string OpenResourceTitle
        {
            get => this._OpenResourceTitle;

            set {
                if ( value is null )
                    value = string.Empty;
                if ( !string.Equals( this.OpenResourceTitle, value ) )
                {
                    this._OpenResourceTitle = value;
                    this.NotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> The resource title caption. </summary>
        private string _ResourceTitleCaption;

        /// <summary> Gets or sets the Title caption. </summary>
        /// <value> The Title caption. </value>
        public string ResourceTitleCaption
        {
            get => this._ResourceTitleCaption;

            set {
                if ( !string.Equals( this.ResourceTitleCaption, value ) )
                {
                    this._ResourceTitleCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " CAPTION "

        /// <summary> The resource closed caption. </summary>
        private string _ResourceClosedCaption;

        /// <summary> Gets or sets the default resource name closed caption. </summary>
        /// <value> The resource closed caption. </value>
        public string ResourceClosedCaption
        {
            get => this._ResourceClosedCaption;

            set {
                if ( !string.Equals( this.ResourceClosedCaption, value ) )
                {
                    this._ResourceClosedCaption = value;
                    this.SyncNotifyPropertyChanged();
                    this.UpdateCaptions();
                }
            }
        }

        /// <summary> The resource name caption. </summary>
        private string _ResourceNameCaption;

        /// <summary> Gets or sets the resource name caption. </summary>
        /// <value> The <see cref="ResourceNameCaption"/> resource tagged as closed if not open. </value>
        public string ResourceNameCaption
        {
            get => this._ResourceNameCaption;

            set {
                if ( value is null )
                    value = string.Empty;
                if ( !string.Equals( this.ResourceNameCaption, value ) )
                {
                    this._ResourceNameCaption = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
