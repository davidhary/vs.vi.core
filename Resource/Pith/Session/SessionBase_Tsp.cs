using System;
using System.Diagnostics;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        #region " QUERY PRINT "

        /// <summary> Gets or sets the print command format. </summary>
        /// <value> The print command format. </value>
        private string PrintCommandFormat { get; set; } = "_G.print({0})";


        /// <summary> Builds query print command. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> A String. </returns>
        private string BuildQueryPrintCommand( string queryCommand )
        {
            return string.Format( this.PrintCommandFormat, queryCommand );
        }

        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read.
        /// </summary>
        /// <param name="dataToPrint"> The LUA command to print. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        private string QueryPrintTrimEnd( string dataToPrint )
        {
            return this.QueryTrimEnd( this.PrintCommandFormat, dataToPrint );
        }

        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read.
        /// </summary>
        /// <param name="format"> The format for building the LUA command to be printed. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        private string QueryPrintTrimEnd( string format, params object[] args )
        {
            return this.QueryTrimEnd( this.PrintCommandFormat, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        #endregion

        #region " QUERY PRINT AND PARSE "

        #region " BOOLEAN "


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Boolean return value.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format for building the LUA command to be printed. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public bool QueryPrint( bool emulatingValue, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseBoolean( this.QueryPrintStringFormatTrimEnd( 1, format, args ) );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Boolean return value.
        /// </summary>
        /// <param name="value">  [in,out] Value read from the instrument. </param>
        /// <param name="format"> The format for building the LUA command to be printed. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref bool value, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( 1, format, args ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Boolean return value.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToPrint">      The data to print. </param>
        /// <returns>   The parsed value or default. </returns>
        public bool QueryPrint( bool emulatingValue, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseBoolean( this.QueryPrintStringFormatTrimEnd( 1, dataToPrint ) );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Boolean return value.
        /// </summary>
        /// <param name="value">       [in,out] Value read from the instrument. </param>
        /// <param name="dataToPrint"> The data to print. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref bool value, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( 1, dataToPrint ), out value );
        }

        #endregion

        #region " DOUBLE "


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Double return value.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format for building the LUA command to be printed. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public double QueryPrint( double emulatingValue, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDouble( this.QueryPrintTrimEnd( format, args ) );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Double return value.
        /// </summary>
        /// <param name="value">  [in,out] Value read from the instrument. </param>
        /// <param name="format"> The format for building the LUA command to be printed. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref double value, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintTrimEnd( format, args ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Double return value.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToPrint">      The data to print. </param>
        /// <returns>   The parsed value or default. </returns>
        public double QueryPrint( double emulatingValue, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDouble( this.QueryPrintTrimEnd( dataToPrint ) );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Double return value.
        /// </summary>
        /// <param name="value">       [in,out] Value read from the instrument. </param>
        /// <param name="dataToPrint"> The data to print. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref double value, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintTrimEnd( dataToPrint ), out value );
        }

        #endregion

        #region " INTEGER "


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Integer return value.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format for building the LUA command to be printed. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public int QueryPrint( int emulatingValue, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseInteger( this.QueryPrintTrimEnd( format, args ) );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Integer return value.
        /// </summary>
        /// <param name="value">  [in,out] Value read from the instrument. </param>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref int value, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintTrimEnd( format, args ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Integer return value.
        /// </summary>
        /// <param name="dataToPrint"> The data to print. </param>
        /// <returns> The parsed value or default. </returns>
        public int QueryPrintInt( string dataToPrint )
        {
            return this.ParseInteger( this.QueryPrintTrimEnd( dataToPrint ) );
        }


        /// <summary>
        /// Performs a synchronous write of a LUA print of the data to print, followed by a synchronous
        /// read. Parses the Integer return value.
        /// </summary>
        /// <param name="value">       [in,out] Value read from the instrument. </param>
        /// <param name="dataToPrint"> The data to print. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref int value, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintTrimEnd( dataToPrint ), out value );
        }

        #endregion

        #endregion

        #region " QUERY PRINT STRING "

        /// <summary> Gets or sets the query print string format command. </summary>
        /// <value> The query print string format command. </value>
        public string PrintCommandStringFormat { get; set; } = "_G.print(string.format('{0}',{1}))";


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. The format conforms to the 'C' query command and returns the Boolean outcome.
        /// </summary>
        /// <remarks>
        /// The format string follows the same rules as the printf family of standard C functions. The
        /// only differences are that the options or modifiers *, l, L, n, p, and h are not supported and
        /// that there is an extra option, q. The q option formats a string in a form suitable to be
        /// safely read back by the Lua interpreter: the string is written between double quotes, and all
        /// double quotes, newlines, embedded zeros, and backslashes in the string are correctly escaped
        /// when written. For instance, the call string.format('%q', 'a string with ''quotes'' and [BS]n
        /// new line') will produce the string: a string with [BS]''quotes[BS]'' and [BS]new line The
        /// options c, d, E, e, f, g, G, i, o, u, X, and x all expect a number as argument, whereas q and
        /// s expect a string. This function does not accept string values containing embedded zeros.
        /// </remarks>
        /// <param name="format"> The LUA format of the data top be printed. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        public string QueryPrintStringFormatTrimEnd( string format, params string[] args )
        {
            return this.QueryTrimEnd( this.PrintCommandStringFormat, format, Parameterize( args ) );
        }

        /// <summary> Gets or sets the print command string number format. </summary>
        /// <value> The print command string number format. </value>
        public string PrintCommandStringNumberFormat { get; set; } = "_G.print(string.format('%{0}f',{1}))";


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. The format conforms to the 'C' query command and returns the Boolean outcome.
        /// </summary>
        /// <param name="numberFormat"> Number of formats. </param>
        /// <param name="dataToPrint">  The data to print. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        public string QueryPrintStringFormatTrimEnd( decimal numberFormat, string dataToPrint )
        {
            return this.QueryTrimEnd( this.PrintCommandStringNumberFormat, numberFormat, dataToPrint );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. The format conforms to the 'C' query command and returns the Boolean outcome.
        /// </summary>
        /// <param name="numberFormat"> Number of formats. </param>
        /// <param name="format">       The LUA format of the data top be printed. </param>
        /// <param name="args">         The format arguments. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        public string QueryPrintStringFormatTrimEnd( decimal numberFormat, string format, params object[] args )
        {
            return this.QueryPrintStringFormatTrimEnd( numberFormat, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary> Gets or sets the query print string integer format command. </summary>
        /// <value> The query print string integer format command. </value>
        public string PrintCommandStringIntegerFormat { get; set; } = "_G.print(string.format('%d',{1}))";


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. The format conforms to the 'C' query command and returns the Boolean outcome.
        /// </summary>
        /// <param name="numberFormat"> The number format for integer type. </param>
        /// <param name="dataToPrint">  The data to print. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        public string QueryPrintStringFormatTrimEnd( int numberFormat, string dataToPrint )
        {
            return this.QueryTrimEnd( this.PrintCommandStringIntegerFormat, numberFormat, dataToPrint );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. The format conforms to the 'C' query command and returns the Boolean outcome.
        /// </summary>
        /// <param name="numberFormat"> The number format for integer type. </param>
        /// <param name="format">       The LUA format of the data top be printed. </param>
        /// <param name="args">         The format arguments. </param>
        /// <returns>
        /// The received message without the <see cref="TerminationCharacters()">termination characters</see>.
        /// </returns>
        public string QueryPrintStringFormatTrimEnd( int numberFormat, string format, params object[] args )
        {
            return this.QueryPrintStringFormatTrimEnd( numberFormat, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }


        /// <summary>
        /// Returns a string from the parameter array of arguments for use when running the function.
        /// </summary>
        /// <param name="args"> Specifies a parameter array of arguments. </param>
        /// <returns> A String. </returns>
        public static string Parameterize( params string[] args )
        {
            var arguments = new System.Text.StringBuilder();
            int i;
            if ( args is object && args.Length >= 0 )
            {
                var loopTo = args.Length - 1;
                for ( i = 0; i <= loopTo; i++ )
                {
                    if ( i > 0 )
                    {
                        _ = arguments.Append( "," );
                    }

                    _ = arguments.Append( args[i] );
                }
            }

            return arguments.ToString();
        }

        #endregion

        #region " QUERY PRINT STRING AND PARSE "


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Decimal.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="numberFormat">     The number format for the numeric value to use in the print
        ///                                 string format statement. </param>
        /// <param name="dataToPrint">      The data to print. </param>
        /// <returns>   The parsed value or default. </returns>
        public decimal QueryPrint( decimal emulatingValue, decimal numberFormat, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDecimal( this.QueryPrintStringFormatTrimEnd( numberFormat, dataToPrint ) );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Decimal.
        /// </summary>
        /// <param name="numberFormat"> The number format for the numeric value to use in the print
        /// string format statement. </param>
        /// <param name="value">        [in,out] Value read from the instrument. </param>
        /// <param name="dataToPrint">  The data to print. </param>
        /// <returns> True returned value is valid. </returns>
        public bool TryQueryPrint( decimal numberFormat, ref decimal value, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( numberFormat, dataToPrint ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Decimal.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="numberFormat">     The number format for the numeric value to use in the print
        ///                                 string format statement. </param>
        /// <param name="format">           The format for constructing the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public decimal QueryPrint( decimal emulatingValue, decimal numberFormat, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDecimal( this.QueryPrintStringFormatTrimEnd( numberFormat, format, args ) );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Decimal.
        /// </summary>
        /// <param name="numberFormat"> The number format for the numeric value to use in the print
        /// string format statement. </param>
        /// <param name="value">        [in,out] Value read from the instrument. </param>
        /// <param name="format">       The format for constructing the data to write. </param>
        /// <param name="args">         The format arguments. </param>
        /// <returns> True returned value is valid. </returns>
        public bool TryQueryPrint( decimal numberFormat, ref decimal value, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( numberFormat, format, args ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Double.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="numberFormat">     The number format for the numeric value to use in the print
        ///                                 string format statement. </param>
        /// <param name="dataToPrint">      The data to print. </param>
        /// <returns>   The parsed value or default. </returns>
        public double QueryPrint( double emulatingValue, decimal numberFormat, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDouble( this.QueryPrintStringFormatTrimEnd( numberFormat, dataToPrint ) );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Double.
        /// </summary>
        /// <param name="numberFormat"> The number format for the numeric value to use in the print
        /// string format statement. </param>
        /// <param name="value">        [in,out] Value read from the instrument. </param>
        /// <param name="dataToPrint">  The data to print. </param>
        /// <returns> True returned value is valid. </returns>
        public bool TryQueryPrint( decimal numberFormat, ref double value, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( numberFormat, dataToPrint ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Double.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="numberFormat">     The number format for the numeric value to use in the print
        ///                                 string format statement. </param>
        /// <param name="format">           The format for constructing the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public double QueryPrint( double emulatingValue, decimal numberFormat, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDouble( this.QueryPrintStringFormatTrimEnd( numberFormat, format, args ) );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Double.
        /// </summary>
        /// <param name="numberFormat"> The number format for the numeric value to use in the print
        /// string format statement. </param>
        /// <param name="value">        [in,out] Value read from the instrument. </param>
        /// <param name="format">       The format for constructing the data to write. </param>
        /// <param name="args">         The format arguments. </param>
        /// <returns> True returned value is valid. </returns>
        public bool TryQueryPrint( decimal numberFormat, ref double value, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( numberFormat, format, args ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Integer.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="numberFormat">     The number format for the numeric value to use in the print
        ///                                 string format statement. </param>
        /// <param name="dataToPrint">      The data to print. </param>
        /// <returns>   The parsed value or default. </returns>
        public int QueryPrint( int emulatingValue, int numberFormat, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseInteger( this.QueryPrintStringFormatTrimEnd( numberFormat, dataToPrint ) );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to integer.
        /// </summary>
        /// <param name="numberFormat"> The number format for the numeric value to use in the print
        /// string format statement. </param>
        /// <param name="value">        [in,out] Value read from the instrument. </param>
        /// <param name="dataToPrint">  The data to print. </param>
        /// <returns> True returned value is valid. </returns>
        public bool TryQueryPrint( int numberFormat, ref int value, string dataToPrint )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( numberFormat, dataToPrint ), out value );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to Integer.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="numberFormat">     The number format for the numeric value to use in the print
        ///                                 string format statement. </param>
        /// <param name="format">           The format for constructing the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public int QueryPrint( int emulatingValue, int numberFormat, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseInteger( this.QueryPrintStringFormatTrimEnd( numberFormat, format, args ) );
        }


        /// <summary>
        /// Performs a synchronous write of a Lua print string format command, followed by a synchronous
        /// read. Parses the reading to integer.
        /// </summary>
        /// <param name="numberFormat"> The number format for the numeric value to use in the print
        /// string format statement. </param>
        /// <param name="value">        [in,out] Value read from the instrument. </param>
        /// <param name="format">       The format for constructing the data to write. </param>
        /// <param name="args">         The format arguments. </param>
        /// <returns> True returned value is valid. </returns>
        public bool TryQueryPrint( int numberFormat, ref int value, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return TryParse( this.QueryPrintStringFormatTrimEnd( numberFormat, format, args ), out value );
        }


        #endregion

        #region " NIL "

        /// <summary> Represents the LUA nil value. </summary>
        public const string NilValue = "nil";

        /// <summary> Equals nil. </summary>
        /// <param name="value"> Specifies the global which to look for. </param>
        /// <returns> <c>true</c> if equals nil; otherwise <c>false</c> </returns>
        public static bool EqualsNil( string value )
        {
            return string.IsNullOrWhiteSpace( value ) || string.Equals( NilValue, value, StringComparison.OrdinalIgnoreCase );
        }

        /// <summary> Returns <c>True</c> if the specified global exists. </summary>
        /// <param name="value"> Specifies the global which to look for. </param>
        /// <returns> <c>True</c> if the specified global exists; otherwise <c>False</c> </returns>
        public bool IsGlobalExists( string value )
        {
            return !EqualsNil( this.QueryPrintTrimEnd( value ) );
        }

        /// <summary> Returns <c>True</c> if the specified global is Nil. </summary>
        /// <param name="value"> Specifies the global which to look for. </param>
        /// <returns> <c>True</c> if the specified global exists; otherwise <c>False</c> </returns>
        public bool IsNil( string value )
        {
            return EqualsNil( this.QueryPrintTrimEnd( value ) );
        }

        /// <summary> Returns <c>True</c> if the specified global is Nil. </summary>
        /// <param name="format"> The format for building the global. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the specified global exists; otherwise <c>False</c> </returns>
        public bool IsNil( string format, params object[] args )
        {
            return EqualsNil( this.QueryPrintTrimEnd( format, args ) );
        }


        /// <summary>
        /// Checks the series of values and return <c>True</c> if any one of them is nil.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> Specifies a list of nil objects to check. </param>
        /// <returns> <c>True </c> if any value is nil; otherwise, <c>False</c> </returns>
        public bool IsNil( string[] values )
        {
            if ( values is null || values.Length == 0 )
            {
                throw new ArgumentNullException( nameof( values ) );
            }
            else
            {
                foreach ( string value in values )
                {
                    if ( !string.IsNullOrWhiteSpace( value ) )
                    {
                        if ( this.IsNil( value ) )
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        #endregion

        #region " TSP / LUA SYNTAX "

        /// <summary> Returns true if the validation command returns true. </summary>
        /// <exception cref="FormatException">          Thrown when the format of the ? is incorrect. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="format"> The format for constructing the assertion. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> True if statement true, false if not. </returns>
        public bool IsStatementTrue( string format, params object[] args )
        {
            bool value;
            try
            {
                string result = this.QueryPrintTrimEnd( format, args );
                value = string.Equals( "true", result, StringComparison.OrdinalIgnoreCase ) || (string.Equals( "false", result, StringComparison.OrdinalIgnoreCase )
                        ? false
                        : throw new FormatException( $"Statement '{string.Format( format, args )}' returned '{result}', which is not Boolean" ));
            }
            catch ( FormatException )
            {
                throw;
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Statement '{string.Format( format, args )}' failed. Last message = '{this.LastMessageSent}'", ex );
            }

            return value;
        }

        #endregion

        #region " NODE "

        #region " EXECUTE COMMAND - NODE "

        /// <summary> Gets the execute command.  Requires node number and command arguments. </summary>
        public const string ExecuteNodeCommandFormat = "_G.node[{0}].execute(\"{1}\") _G.waitcomplete({0})";

        /// <summary> Executes a command on the remote node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> Specifies the node number. </param>
        /// <param name="format">     The format for constructing the data to write. </param>
        /// <param name="args">       The format arguments. </param>
        /// <returns> The command message. </returns>
        public string ExecuteCommand( int nodeNumber, string format, params object[] args )
        {
            if ( string.IsNullOrWhiteSpace( format ) )
            {
                throw new ArgumentNullException( nameof( format ) );
            }
            else
            {
                string command = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
                _ = this.WriteLine( ExecuteNodeCommandFormat, nodeNumber, command );
                return command;
            }
        }

        #endregion

        #region " QUERY - NODE "

        /// <summary> Gets the value returned by executing a command on the node.
        /// Requires node number and value to get arguments. </summary>
        public const string NodeValueGetterCommandFormat1 = "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete({0}) _G.waitcomplete() _G.print(_G.node[{0}].dataqueue.next())";


        /// <summary>
        /// Executes a query on the remote node and prints the result. This leaves an item in the input
        /// buffer that must be retried.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber">  Specifies the remote node number. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        public void ExecuteQuery( int nodeNumber, string dataToWrite )
        {
            _ = string.IsNullOrWhiteSpace( dataToWrite )
                ? throw new ArgumentNullException( nameof( dataToWrite ) )
                : this.WriteLine( NodeValueGetterCommandFormat1, nodeNumber, dataToWrite );
        }


        /// <summary>
        /// Executes a query on the remote node prints the result. This leaves an item in the input
        /// buffer that must be retried.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> Specifies the remote node number. </param>
        /// <param name="format">     The format for constructing the data to write. </param>
        /// <param name="args">       The format arguments. </param>
        public void ExecuteQuery( int nodeNumber, string format, params object[] args )
        {
            if ( string.IsNullOrWhiteSpace( format ) )
            {
                throw new ArgumentNullException( nameof( format ) );
            }
            else
            {
                string command = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
                _ = this.WriteLine( NodeValueGetterCommandFormat1, nodeNumber, command );
            }
        }


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// </summary>
        /// <param name="nodeNumber">  Specifies the remote node number. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The string. </returns>
        public string QueryPrintTrimEnd( int nodeNumber, string dataToWrite )
        {
            this.ExecuteQuery( nodeNumber, dataToWrite );
            return this.ReadLineTrimEnd();
        }


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// </summary>
        /// <param name="nodeNumber"> Specifies the remote node number. </param>
        /// <param name="format">     The format for constructing the data to write. </param>
        /// <param name="args">       The format arguments. </param>
        /// <returns> The string. </returns>
        public string QueryPrintTrimEnd( int nodeNumber, string format, params object[] args )
        {
            this.ExecuteQuery( nodeNumber, format, args );
            return this.ReadLineTrimEnd();
        }

        #endregion

        #region " QUERY - NODE AND PARSE "

        #region " BOOLEAN "


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// Parses the results as a Boolean.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="nodeNumber">       Specifies the remote node number. </param>
        /// <param name="format">           The format for constructing the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public bool? QueryPrint( bool emulatingValue, int nodeNumber, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            this.ExecuteQuery( nodeNumber, format, args );
            return this.ParseBoolean( this.ReadLineTrimEnd() );
        }


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// Parses the results as a Boolean.
        /// </summary>
        /// <param name="result">     [in,out] The result value. </param>
        /// <param name="nodeNumber"> Specifies the remote node number. </param>
        /// <param name="format">     The format for constructing the data to write. </param>
        /// <param name="args">       The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref bool result, int nodeNumber, string format, params object[] args )
        {
            this.ExecuteQuery( nodeNumber, format, args );
            return TryParse( this.ReadLineTrimEnd(), out result );
        }


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// Parses the results as a Boolean.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="nodeNumber">       Specifies the remote node number. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public bool QueryPrint( bool emulatingValue, int nodeNumber, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            this.ExecuteQuery( nodeNumber, dataToWrite );
            return this.ParseBoolean( this.ReadLineTrimEnd() );
        }


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// Parses the results as a Boolean.
        /// </summary>
        /// <param name="result">      [in,out] The result value. </param>
        /// <param name="nodeNumber">  Specifies the remote node number. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref bool result, int nodeNumber, string dataToWrite )
        {
            this.ExecuteQuery( nodeNumber, dataToWrite );
            return TryParse( this.ReadLineTrimEnd(), out result );
        }

        #endregion

        #region " DOUBLE "


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// Parses the results as a Double.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="nodeNumber">       Specifies the remote node number. </param>
        /// <param name="format">           The format for constructing the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public double? QueryPrint( double emulatingValue, int nodeNumber, string format, params object[] args )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            this.ExecuteQuery( nodeNumber, format, args );
            return this.ParseDouble( this.ReadLineTrimEnd() );
        }

        /// <summary> Executes a command on the remote node and retrieves a Double. </summary>
        /// <param name="result">     [in,out] The result value. </param>
        /// <param name="nodeNumber"> Specifies the remote node number. </param>
        /// <param name="format">     The format for constructing the data to write. </param>
        /// <param name="args">       The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref double result, int nodeNumber, string format, params object[] args )
        {
            this.ExecuteQuery( nodeNumber, format, args );
            return TryParse( this.ReadLineTrimEnd(), out result );
        }


        /// <summary>
        /// Executes a command on the remote node and prints the result, followed by a synchronous read.
        /// Parses the results as a Double.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="nodeNumber">       Specifies the remote node number. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public double QueryPrint( double emulatingValue, int nodeNumber, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            this.ExecuteQuery( nodeNumber, dataToWrite );
            return this.ParseDouble( this.ReadLineTrimEnd() );
        }

        /// <summary> Executes a command on the remote node and retrieves a Double. </summary>
        /// <param name="result">      [in,out] The result value. </param>
        /// <param name="nodeNumber">  Specifies the remote node number. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQueryPrint( ref double result, int nodeNumber, string dataToWrite )
        {
            this.ExecuteQuery( nodeNumber, dataToWrite );
            return TryParse( this.ReadLineTrimEnd(), out result );
        }

        #endregion

        #endregion

        #region " NIL "

        /// <summary> Returns <c>True</c> if the specified node global is Nil. </summary>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="value">      Specifies the global which to look for. </param>
        /// <returns> <c>True</c> if the specified node global exists; otherwise <c>False</c> </returns>
        public bool IsNil( int nodeNumber, string value )
        {
            return string.Equals( NilValue, this.QueryPrintTrimEnd( nodeNumber, value ) );
        }


        /// <summary>
        /// Checks the series of values and return <c>True</c> if any one of them is nil.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="values">     Specifies a list of nil objects to check. </param>
        /// <returns> <c>True </c> if any value is nil; otherwise, <c>False</c> </returns>
        public bool IsNil( int nodeNumber, params string[] values )
        {
            if ( values is null || values.Length == 0 )
                throw new ArgumentNullException( nameof( values ) );
            bool affirmative = false;
            foreach ( string value in values )
            {
                if ( !string.IsNullOrWhiteSpace( value ) )
                {
                    if ( this.IsNil( nodeNumber, value ) )
                    {
                        affirmative = true;
                        break;
                    }
                }
            }

            return affirmative;
        }


        /// <summary>
        /// Checks the series of values and return <c>True</c> if any one of them is nil.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="isControllerNode"> true if this object is controller node. </param>
        /// <param name="nodeNumber">       Specifies the remote node number to validate. </param>
        /// <param name="values">           Specifies a list of nil objects to check. </param>
        /// <returns> <c>True </c> if any value is nil; otherwise, <c>False</c> </returns>
        public bool IsNil( bool isControllerNode, int nodeNumber, params string[] values )
        {
            return values is null || values.Length == 0
                ? throw new ArgumentNullException( nameof( values ) )
                : isControllerNode ? this.IsNil( values ) : this.IsNil( nodeNumber, values );
        }

        /// <summary> Loops until the name is found or timeout. </summary>
        /// <remarks> Use to verify if the Test Script exists. </remarks>
        /// <param name="nodeNumber"> Specifies the node number. </param>
        /// <param name="name">       Specifies the script name. </param>
        /// <param name="timeout">    The timeout. </param>
        /// <returns> <c>True </c> if nil; otherwise, <c>False</c> </returns>
        public bool WaitNotNil( int nodeNumber, string name, TimeSpan timeout )
        {

            // TO_DO: Use task; See session await service request
            var pollDelay = TimeSpan.FromMilliseconds( Math.Min( 10d, timeout.TotalMilliseconds ) );
            bool detected = !this.IsNil( nodeNumber, name );
            var sw = Stopwatch.StartNew();
            while ( !detected && sw.Elapsed <= timeout )
            {
                System.Threading.Thread.SpinWait( 10 );
                Core.ApplianceBase.DoEventsWait( pollDelay );
                detected = !this.IsNil( nodeNumber, name );
            }

            return detected;
        }

        #endregion

        #endregion




    }
}
