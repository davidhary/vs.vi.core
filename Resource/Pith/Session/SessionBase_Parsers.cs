using System;
using System.Diagnostics;

using isr.Core;
using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        #region " BOOLEAN PARSERS "

        /// <summary> Parses a value to a Boolean. </summary>
        /// <exception cref="FormatException"> Thrown when the format of the received message is
        /// incorrect. </exception>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>True</c> if the value equals '1' or <c>False</c> if '0'; otherwise an exception is thrown.
        /// </returns>
        public bool ParseBoolean( string value )
        {
            return TryParse( value, out bool result )
                ? result
                : throw new FormatException( $"{this.ResourceNameCaption} '{value}' is invalid Boolean format" );
        }

        /// <summary> Converts a value to an one zero. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> value as a String. </returns>
        public static string ToOneZero( bool value )
        {
            return $"{value.GetHashCode():'1';'1';'0'}";
        }

        /// <summary> Converts a value to a true false. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> value as a String. </returns>
        public static string ToTrueFalse( bool value )
        {
            return $"{value.GetHashCode():'true';'true';'false'}";
        }

        /// <summary> Tries to parse a value to a Boolean. </summary>
        /// <param name="value">  The value. </param>
        /// <param name="result"> [out] Value read from the instrument. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public static bool TryParse( string value, out bool result )
        {
            result = default;
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return false;
            }
            else if ( int.TryParse( value, out int numericValue ) )
            {
                result = numericValue != 0;
                return true;
            }
            else
            {
                return bool.TryParse( value, out result );
            }
        }

        #endregion

        #region " DECIMAL PARSERS "

        /// <summary> Parses a value to Decimal. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <exception cref="FormatException">       Thrown when the format of the received message is
        /// incorrect. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> Value if the value is a valid number; otherwise, Default. </returns>
        public decimal ParseDecimal( string value )
        {
            return value is null
                ? throw new ArgumentNullException( nameof( value ), "Query not executed" )
                : string.IsNullOrWhiteSpace( value )
                    ? throw new ArgumentException( "Query returned an empty string", nameof( value ) )
                    : decimal.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent, System.Globalization.CultureInfo.InvariantCulture,
                                        out decimal parsedValue )
                                    ? parsedValue
                                    : throw new FormatException( $"{this.ResourceNameCaption} '{value}' is invalid Decimal format" );
        }

        /// <summary> Tries to parse a Decimal reading. </summary>
        /// <param name="value">  The value. </param>
        /// <param name="result"> [in,out] The result. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public static bool TryParse( string value, out decimal result )
        {
            result = default;
            return !string.IsNullOrWhiteSpace( value )
                    && decimal.TryParse( value,
                                         System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
                                         System.Globalization.CultureInfo.InvariantCulture, out result );
        }

        #endregion

        #region " DOUBLE PARSERS "

        /// <summary>   Parses a value to Double. </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <exception cref="FormatException">          Thrown when the format of the received message is
        ///                                             incorrect. </exception>
        /// <param name="value">    The value. </param>
        /// <returns>   Value if the value is a valid number; otherwise, Default. </returns>
        public double ParseDouble( string value )
        {
            return value is null
                ? throw new ArgumentNullException( nameof( value ), "Query not executed" )
                : string.IsNullOrWhiteSpace( value )
                    ? throw new ArgumentException( "Query returned an empty string", nameof( value ) )
                    : double.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
                                              System.Globalization.CultureInfo.InvariantCulture, out double parsedValue )
                                    ? parsedValue
                                    : throw new FormatException( $"{this.ResourceNameCaption} '{value}' is invalid Double format" );
        }

        /// <summary> Tries to parse a Double reading. </summary>
        /// <param name="value">  The value. </param>
        /// <param name="result"> [out] The result. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public static bool TryParse( string value, out double result )
        {
            result = default;
            return !string.IsNullOrWhiteSpace( value )
                    && double.TryParse( value,
                                        System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
                                        System.Globalization.CultureInfo.InvariantCulture, out result );
        }

        #endregion

        #region " INTEGER PARSERS "

        /// <summary> Parses a value to Integer. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <exception cref="FormatException">       Thrown when the format of the received message is
        /// incorrect. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> Value if the value is a valid number; otherwise, Default. </returns>
        public int ParseInteger( string value )
        {
            return value is null
                ? throw new ArgumentNullException( nameof( value ), "Query not executed" )
                : string.IsNullOrWhiteSpace( value )
                    ? throw new ArgumentException( "Query returned an empty string", nameof( value ) )
                    : int.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
                                                System.Globalization.CultureInfo.InvariantCulture, out int parsedValue )
                                    ? parsedValue
                                    : throw new FormatException( $"{this.ResourceNameCaption} '{value}' is invalid Integer format" );
        }

        /// <summary> Tries to parse a value to Integer. </summary>
        /// <param name="value">  The value. </param>
        /// <param name="result"> [out] The result. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public static bool TryParse( string value, out int result )
        {
            result = default;
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return false;
            }
            // check if we have an infinity.
            else if ( TryParse( value, out double inf ) && (inf > int.MaxValue || inf < int.MinValue) )
            {
                result = inf > int.MaxValue ? int.MaxValue : int.MinValue;
                return true;
            }
            else
            {
                return int.TryParse( value, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent,
                                     System.Globalization.CultureInfo.InvariantCulture, out result );
            }
        }

        #endregion

        #region " BOOLEAN QUERY AND PARSE 

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public bool Query( bool emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseBoolean( this.QueryTrimEnd( dataToWrite ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public bool? Query( bool emulatingValue, string format, params object[] args )
        {
            return this.Query( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   A Tuple:  (bool ParsedValue, <see cref="QueryParseBooleanInfo"/> )  </returns>
        public (bool ParsedValue, QueryParseBooleanInfo) QueryElapsed( bool emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            bool parsedValue = this.ParseBoolean( parsedMessage );
            return ( parsedValue, new QueryParseBooleanInfo( parsedValue, parsedMessage, queryInfo ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   A Tuple:  (bool ParsedValue, <see cref="QueryParseBooleanInfo"/> )  </returns>
        public (bool ParsedValue, QueryParseBooleanInfo) QueryElapsed( bool emulatingValue, string format, params object[] args )
        {
            return this.QueryElapsed( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   The second. </returns>
        public bool Query( bool emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            dataToWrite = this.QueryTrimEnd( dataToWrite );
            return this.ParseBoolean( dataToWrite.Split( ',' )[receivedMessageItemIndex] );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   A Tuple:  (bool ParsedValue, <see cref="QueryParseBooleanInfo"/> )  </returns>
        public (bool ParsedValue, QueryParseBooleanInfo) QueryElapsed( bool emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters ).Split( ',' )[receivedMessageItemIndex];
            bool parsedValue = this.ParseBoolean( parsedMessage );
            return (parsedValue, new QueryParseBooleanInfo( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">       [in,out] The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref bool value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return SessionBase.TryParse( this.QueryTrimEnd( dataToWrite ), out value );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">  [in,out] The value. </param>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref bool value, string format, params object[] args )
        {
            return this.TryQuery( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        [in,out] The value. </param>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   A Tuple:  (bool success, <see cref="QueryParseBooleanInfo"/> )  </returns>
        public (bool Success, QueryParseBooleanInfo QueryParseBooleanInfo) TryQueryElapsed( ref bool value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            bool success = SessionBase.TryParse( parsedMessage, out bool parsedValue );
            return (success, new QueryParseBooleanInfo( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="format">   The format of the data to write. </param>
        /// <param name="args">     The format arguments. </param>
        /// <returns>   A Tuple:  (bool success, <see cref="QueryParseBooleanInfo"/> )  </returns>
        public (bool Success, QueryParseBooleanInfo QueryParseBooleanInfo) TryQueryElapsed( ref bool value, string format, params object[] args )
        {
            return this.TryQueryElapsed( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        #endregion

        #region " DECIMAL QUERY AND PARSE 

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public decimal Query( decimal emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDecimal( this.QueryTrimEnd( dataToWrite ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public decimal? Query( decimal emulatingValue, string format, params object[] args )
        {
            return this.Query( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   A Tuple:  (decimal ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (decimal ParsedValue, QueryParseInfo<decimal> QueryParseBooleanInfo) QueryElapsed( decimal emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            decimal parsedValue = this.ParseDecimal( parsedMessage );
            return (parsedValue, new QueryParseInfo<decimal>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   A Tuple:  (decimal ParsedValue, <see cref="QueryParseInfo{T}"/> ) </returns>
        public (decimal ParsedValue, QueryParseInfo<decimal> QueryParseBooleanInfo) QueryElapsed( decimal emulatingValue, string format, params object[] args )
        {
            return this.QueryElapsed( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   The second. </returns>
        public decimal Query( decimal emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            dataToWrite = this.QueryTrimEnd( dataToWrite );
            return this.ParseDecimal( dataToWrite.Split( ',' )[receivedMessageItemIndex] );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   A Tuple:  (decimal ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (decimal ParsedValue, QueryParseInfo<decimal> QueryParseBooleanInfo) QueryElapsed( decimal emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters ).Split( ',' )[receivedMessageItemIndex];
            decimal parsedValue = this.ParseDecimal( parsedMessage );
            return (parsedValue, new QueryParseInfo<decimal>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">       [in,out] The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref decimal value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return SessionBase.TryParse( this.QueryTrimEnd( dataToWrite ), out value );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">  [in,out] The value. </param>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref decimal value, string format, params object[] args )
        {
            return this.TryQuery( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        [in,out] The value. </param>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   A Tuple:  (bool Success, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (bool Success, QueryParseInfo<decimal> QueryParseBooleanInfo) TryQueryElapsed( ref decimal value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            bool success = SessionBase.TryParse( parsedMessage, out decimal parsedValue );
            return (success, new QueryParseInfo<decimal>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="format">   The format of the data to write. </param>
        /// <param name="args">     The format arguments. </param>
        /// <returns>   A Tuple:  (bool Success, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (bool Success, QueryParseInfo<decimal> QueryParseBooleanInfo) TryQueryElapsed( ref decimal value, string format, params object[] args )
        {
            return this.TryQueryElapsed( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        #endregion

        #region " DOUBLE QUERY AND PARSE 

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public double Query( double emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseDouble( this.QueryTrimEnd( dataToWrite ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public double? Query( double emulatingValue, string format, params object[] args )
        {
            return this.Query( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   A Tuple:  (double ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (double ParsedValue, QueryParseInfo<double> QueryParseBooleanInfo) QueryElapsed( double emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            double parsedValue = this.ParseDouble( parsedMessage );
            return (parsedValue, new QueryParseInfo<double>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   A Tuple:  (double ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (double ParsedValue, QueryParseInfo<double> QueryParseBooleanInfo) QueryElapsed( double emulatingValue, string format, params object[] args )
        {
            return this.QueryElapsed( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   The second. </returns>
        public double Query( double emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            dataToWrite = this.QueryTrimEnd( dataToWrite );
            return this.ParseDouble( dataToWrite.Split( ',' )[receivedMessageItemIndex] );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   A Tuple:  (double ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (double ParsedValue, QueryParseInfo<double> QueryParseBooleanInfo) QueryElapsed( double emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters ).Split( ',' )[receivedMessageItemIndex];
            double parsedValue = this.ParseDouble( parsedMessage );
            return (parsedValue, new QueryParseInfo<double>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">       [in,out] The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref double value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return SessionBase.TryParse( this.QueryTrimEnd( dataToWrite ), out value );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">  [in,out] The value. </param>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref double value, string format, params object[] args )
        {
            return this.TryQuery( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        [in,out] The value. </param>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   A Tuple:  (bool Success, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (bool Success, QueryParseInfo<double> QueryParseBooleanInfo) TryQueryElapsed( ref double value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            bool success = SessionBase.TryParse( parsedMessage, out double parsedValue );
            return (success, new QueryParseInfo<double>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="format">   The format of the data to write. </param>
        /// <param name="args">     The format arguments. </param>
        /// <returns>   A Tuple:  (bool Success, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (bool Success, QueryParseInfo<double> QueryParseBooleanInfo) TryQueryElapsed( ref double value, string format, params object[] args )
        {
            return this.TryQueryElapsed( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        #endregion

        #region " INTEGER QUERY AND PARSE 

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public int Query( int emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            return this.ParseInteger( this.QueryTrimEnd( dataToWrite ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   The parsed value or default. </returns>
        public int? Query( int emulatingValue, string format, params object[] args )
        {
            return this.Query( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   A Tuple:  (int ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseBooleanInfo) QueryElapsed( int emulatingValue, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            int parsedValue = this.ParseInteger( parsedMessage );
            return (parsedValue, new QueryParseInfo<int>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="format">           The format of the data to write. </param>
        /// <param name="args">             The format arguments. </param>
        /// <returns>   A Tuple:  (int ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseBooleanInfo) QueryElapsed( int emulatingValue, string format, params object[] args )
        {
            return this.QueryElapsed( emulatingValue, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   The second. </returns>
        public int Query( int emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            dataToWrite = this.QueryTrimEnd( dataToWrite );
            return this.ParseInteger( dataToWrite.Split( ',' )[receivedMessageItemIndex] );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="emulatingValue">           The value to emulate if emulated reply is empty. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="dataToWrite">              The data to write. </param>
        /// <returns>   A Tuple:  (int ParsedValue, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseBooleanInfo) QueryElapsed( int emulatingValue, int receivedMessageItemIndex, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( emulatingValue );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters ).Split( ',' )[receivedMessageItemIndex];
            int parsedValue = this.ParseInteger( parsedMessage );
            return (parsedValue, new QueryParseInfo<int>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">       [in,out] The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref int value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            return SessionBase.TryParse( this.QueryTrimEnd( dataToWrite ), out value );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">  [in,out] The value. </param>
        /// <param name="format"> The format of the data to write. </param>
        /// <param name="args">   The format arguments. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref int value, string format, params object[] args )
        {
            return this.TryQuery( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        [in,out] The value. </param>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   A Tuple:  (bool Success, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (bool Success, QueryParseInfo<int> QueryParseBooleanInfo) TryQueryElapsed( ref int value, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( value );
            (string receivedMessage, QueryInfo queryInfo) = this.QueryElapsed( dataToWrite );
            string parsedMessage = receivedMessage.TrimEnd( this._TerminationCharacters );
            bool success = SessionBase.TryParse( parsedMessage, out int parsedValue );
            return (success, new QueryParseInfo<int>( parsedValue, parsedMessage, queryInfo ));
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">    [in,out] The value. </param>
        /// <param name="format">   The format of the data to write. </param>
        /// <param name="args">     The format arguments. </param>
        /// <returns>   A Tuple:  (bool Success, <see cref="QueryParseInfo{T}"/> )  </returns>
        public (bool Success, QueryParseInfo<int> QueryParseBooleanInfo) TryQueryElapsed( ref int value, string format, params object[] args )
        {
            return this.TryQueryElapsed( ref value, string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args ) );
        }

        #endregion

        #region " TIME SPAN QUERY AND PARSE 

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// </summary>
        /// <remarks> see also: https://msdn.microsoft.com/en-us/library/ee372287.aspx#Other. </remarks>
        /// <param name="format">      The format for parsing the result. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The parsed value. </returns>
        public TimeSpan Query( string format, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( TimeSpan.Zero );
            return TimeSpan.ParseExact( this.QueryTrimEnd( dataToWrite ), format, System.Globalization.CultureInfo.InvariantCulture );
        }


        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// Parses the time span return value.
        /// </summary>
        /// <param name="result">      [in,out] The result. </param>
        /// <param name="format">      The format for parsing the result. For example, "s\.FFFFFFF",
        /// convert the value to time span from seconds. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> <c>True</c> if the parsed value is valid. </returns>
        public bool TryQuery( ref TimeSpan result, string format, string dataToWrite )
        {
            this.MakeEmulatedReplyIfEmpty( result );
            return TimeSpan.TryParseExact( this.QueryTrimEnd( dataToWrite ), format, System.Globalization.CultureInfo.InvariantCulture, out result );
        }

        #endregion

        #region " ENUM READ WRITE PARSER "

        /// <summary> Queries first enum value. </summary>
        /// <param name="value">          The value. </param>
        /// <param name="enumReadWrites"> enumeration read and write values. </param>
        /// <param name="dataToWrite">    The data to write. </param>
        /// <returns> The first enum value. </returns>
        public T? QueryFirst<T>( T value, EnumReadWriteCollection enumReadWrites, string dataToWrite ) where T : struct
        {
            long v = this.QueryFirst( Conversions.ToLong( Enum.ToObject( typeof( T ), value ) ), enumReadWrites, dataToWrite );
            return v.ToNullableEnum<T>();
        }

        /// <summary>   Queries first enum value. </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="enumReadWrites">   enumeration read and write values. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The first enum value. </returns>
        public long QueryFirst( long emulatingValue, EnumReadWriteCollection enumReadWrites, string dataToWrite )
        {
            if ( enumReadWrites is null )
                throw new ArgumentNullException( nameof( enumReadWrites ) );
            this.MakeEmulatedReplyIfEmpty( enumReadWrites.SelectItemOrDefault( emulatingValue ).ReadValue );
            return this.Parse( enumReadWrites, this.QueryTrimEnd( dataToWrite ).Split( ',' )[0].Trim() );
        }


        /// <summary>
        /// Issues the query command and parses the returned values into an Enum using the enum name.
        /// </summary>
        /// <param name="value">          The value. </param>
        /// <param name="enumReadWrites"> enumeration read and write values. </param>
        /// <param name="dataToWrite">    The data to write. </param>
        /// <returns> The parsed value or none if unknown. </returns>
        public T? Query<T>( T value, EnumReadWriteCollection enumReadWrites, string dataToWrite ) where T : struct
        {
            long v = this.Query( Conversions.ToLong( Enum.ToObject( typeof( T ), value ) ), enumReadWrites, dataToWrite );
            return v.ToNullableEnum<T>();
        }


        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// Parses the string return value to an integer using the 'parse dictionary'.
        /// </summary>
        /// <remarks>   David, 2021-04-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="emulatingValue">   The value to emulate if emulated reply is empty. </param>
        /// <param name="enumReadWrites">   enumeration read and write values. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <returns>   The parsed value or default. </returns>
        public long Query( long emulatingValue, EnumReadWriteCollection enumReadWrites, string dataToWrite )
        {
            if ( enumReadWrites is null )
                throw new ArgumentNullException( nameof( enumReadWrites ) );
            this.MakeEmulatedReplyIfEmpty( enumReadWrites.SelectItemOrDefault( emulatingValue ).ReadValue );
            return this.Parse( enumReadWrites, this.QueryTrimEnd( dataToWrite ) );
        }

        /// <summary> Parses a value to Long using the 'parse dictionary'. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <exception cref="FormatException">       Thrown when the format of the ? is incorrect. </exception>
        /// <param name="enumReadWrites"> enumeration read and write values. </param>
        /// <param name="value">          The value. </param>
        /// <returns> Value if the value is a valid number; otherwise, Default. </returns>
        public long Parse( EnumReadWriteCollection enumReadWrites, string value )
        {
            long parsedValue = value is null
                ? throw new ArgumentNullException( nameof( value ), "Query not executed" )
                : enumReadWrites is null
                    ? throw new ArgumentNullException( nameof( enumReadWrites ), "Parse dictionary not provided" )
                    : string.IsNullOrWhiteSpace( value )
                                    ? throw new ArgumentException( "Query returned an empty string", nameof( value ) )
                                    : enumReadWrites.Exists( value )
                                                    ? enumReadWrites.SelectItem( value ).EnumValue
                                                    : throw new FormatException( $"{this.ResourceNameCaption} '{value}' not found in the parse dictionary" );
            return parsedValue;
        }

        /// <summary>   Tries to parse a value to Long using the 'parse dictionary'. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="enumReadWrites">   enumeration read and write values. </param>
        /// <param name="key">              The key. </param>
        /// <param name="value">            The value. </param>
        /// <returns>   <c>True</c> if the parsed value is valid. </returns>
        public static bool TryParse( EnumReadWriteCollection enumReadWrites, string key, out long value )
        {
            if ( !string.IsNullOrWhiteSpace( key ) && enumReadWrites is object && enumReadWrites.Exists( key ) )
            {
                value = enumReadWrites.SelectItem( key ).EnumValue;
                return true;
            }
            else
            {
                value = 0;
                return false;
            }
        }

        /// <summary>
        /// Synchronously writes the Enum write value without reading back the value from the device.
        /// </summary>
        /// <param name="value">          The value. </param>
        /// <param name="commandFormat">  The command format for creating the data to write. </param>
        /// <param name="enumReadWrites"> enumeration read and write values. </param>
        /// <returns> The value or none if unknown. </returns>
        public T? Write<T>( T value, string commandFormat, EnumReadWriteCollection enumReadWrites ) where T : struct
        {
            long v = this.Write( Conversions.ToLong( Enum.ToObject( typeof( T ), value ) ), commandFormat, enumReadWrites );
            return v.ToNullableEnum<T>();
        }

        /// <summary>
        /// Synchronously writes the Enum write value without reading back the value from the device.
        /// </summary>
        /// <param name="value">          The value. </param>
        /// <param name="commandFormat">  The command format for creating the data to write. </param>
        /// <param name="enumCodeValues"> Dictionary of parses. </param>
        /// <returns> The value or none if unknown. </returns>
        public long Write( long value, string commandFormat, EnumReadWriteCollection enumCodeValues )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) && enumCodeValues is object )
            {
                _ = this.WriteLine( commandFormat, enumCodeValues.SelectItem( value ).WriteValue );
            }

            return value;
        }

        #endregion

        #region " ENUM QUERY AND PARSE 

        /// <summary> Parse enum value. </summary>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        /// type. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> A Nullable(Of. </returns>
        public static T? ParseEnumValue<T>( string value ) where T : struct
        {
            return string.IsNullOrWhiteSpace( value )
                ? new T?()
                : Enum.TryParse( value, out T result )
                    ? ( T? ) result
                    : throw new InvalidCastException( string.Format( System.Globalization.CultureInfo.CurrentCulture, "Can't convert {0} to {1}", value, typeof( T ).ToString() ) );
        }

        /// <summary> Queries first enum value. </summary>
        /// <param name="value">       The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The first enum value. </returns>
        public T? QueryFirstEnumValue<T>( T? value, string dataToWrite ) where T : struct
        {
            string currentValue = value.ToString();
            this.MakeEmulatedReplyIfEmpty( currentValue );
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                _ = this.WriteLine( dataToWrite );
                currentValue = this.ReadLineTrimEnd();
                currentValue = currentValue.Split( ',' )[0].Trim();
            }

            return ParseEnumValue<T>( currentValue );
        }


        /// <summary>
        /// Issues the query command and parses the returned values into an Enum using the enum value.
        /// </summary>
        /// <param name="value">       The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The parsed value or none if unknown. </returns>
        public T? QueryEnumValue<T>( T? value, string dataToWrite ) where T : struct
        {
            string currentValue = value.ToString();
            this.MakeEmulatedReplyIfEmpty( currentValue );
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                _ = this.WriteLine( dataToWrite );
                currentValue = this.ReadLineTrimEnd();
            }

            return ParseEnumValue<T>( currentValue );
        }


        /// <summary>
        /// Issues the query command and parses the returned values into an Enum using the enum name.
        /// </summary>
        /// <param name="value">       The value. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The parsed value or none if unknown. </returns>
        public T? QueryEnum<T>( T? value, string dataToWrite ) where T : struct
        {
            string currentValue = value.ToString();
            this.MakeEmulatedReplyIfEmpty( currentValue );
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                _ = this.WriteLine( dataToWrite );
                currentValue = this.ReadLineTrimEnd();
            }

            if ( string.IsNullOrWhiteSpace( currentValue ) )
            {
                return new T?();
            }
            else if ( Core.ParseExtensions.ParseExtensionMethods.IsNumber( currentValue ) )
            {
                return ( T ) ( object ) int.Parse( currentValue );
            }
            else
            {
                var se = new StringEnumerator<T>();
                return se.ParseContained( currentValue.BuildDelimitedValue() );
            }
        }


        /// <summary>
        /// Synchronously writes the Enum name without reading back the value from the device.
        /// </summary>
        /// <param name="value">         The value. </param>
        /// <param name="commandFormat"> The command format for creating the data to write. </param>
        /// <returns> The value or none if unknown. </returns>
        public T? Write<T>( T value, string commandFormat ) where T : struct
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                var se = new StringEnumerator<T>();
                _ = this.WriteLine( commandFormat, se.ToString( value ).ExtractBetween() );
            }

            return value;
        }


        /// <summary>
        /// Synchronously writes the Enum value without reading back the value from the device.
        /// </summary>
        /// <param name="value">         The value. </param>
        /// <param name="commandFormat"> The command format for creating the data to write. </param>
        /// <returns> The value or none if unknown. </returns>
        public T? WriteEnumValue<T>( T value, string commandFormat ) where T : struct
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                int v = Conversions.ToInteger( Enum.ToObject( typeof( T ), value ) );
                _ = this.WriteLine( commandFormat, ( object ) v );
            }

            return value;
        }

        #endregion

    }
}
