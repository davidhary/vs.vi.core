using System;

using isr.Core.EnumExtensions;

namespace isr.VI
{
    /// <summary>   Information about the Read action. </summary>
    /// <remarks>   David, 2021-04-12. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>" )]
    public struct ReadInfo : IInfoReport
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-12. </remarks>
        /// <param name="receivedMessage">  The message that was Received. </param>
        /// <param name="elapsedTimes">     List of times of the elapsed. </param>
        public ReadInfo( string receivedMessage, ElapsedTimeSpan[] elapsedTimes )
        {
            this.ReceivedMessage = receivedMessage;
            this._ElapsedTimes = elapsedTimes;
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-12. </remarks>
        /// <param name="receivedMessage">  The message that was Received. </param>
        /// <param name="elapsedTime">      The elapsed time. </param>
        public ReadInfo( string receivedMessage, TimeSpan elapsedTime )
        {
            this.ReceivedMessage = receivedMessage;
            this._ElapsedTimes = new ElapsedTimeSpan[] { new ElapsedTimeSpan( ElapsedTimeIdentity.ReadTime, elapsedTime ) };
        }

        /// <summary>   Gets the empty <see cref="ReadInfo"/>. </summary>
        /// <value> The empty <see cref="ReadInfo"/>. </value>
        public static ReadInfo Empty => new( string.Empty, TimeSpan.Zero );


        /// <summary>   Gets or sets the message that was Received. </summary>
        /// <value> The message that was Received. </value>
        public string ReceivedMessage { get; set; }

        /// <summary>   (Immutable) list of times of the elapsed. </summary>
        private readonly ElapsedTimeSpan[] _ElapsedTimes;

        /// <summary>   Gets the elapsed times. </summary>
        /// <remarks>   David, 2021-04-12. </remarks>
        /// <returns>   The elapsed times. </returns>
        public ElapsedTimeSpan[] GetElapsedTimes()
        {
            return this._ElapsedTimes;
        }

        /// <summary>   Builds elapsed times message. </summary>
        /// <remarks>   David, 2021-04-12. </remarks>
        /// <param name="elapsedTimes"> The elapsed. </param>
        /// <returns>   A string. </returns>
        private static string BuildElapsedTimesMessage( ElapsedTimeSpan[] elapsedTimes )
        {
            System.Text.StringBuilder builder = new();
            for ( int i = 0; i < elapsedTimes.Length; i++ )
            {
                _ = builder.Append( $"{elapsedTimes[i].Identity.Description()}: {elapsedTimes[i].Elapsed:ss\\.fff}; " );
            }
            return builder.ToString();
        }

        /// <summary>   Builds the information report. </summary>
        /// <remarks>   David, 2021-04-12. </remarks>
        /// <returns>   A string. </returns>
        public string BuildInfoReport()
        {
            System.Text.StringBuilder builder = new();
            _ = builder.AppendLine( $"Received {this.ReceivedMessage}" );
            _ = builder.AppendLine( $"  {BuildElapsedTimesMessage( this._ElapsedTimes )}" );
            return builder.ToString();
        }

    }
}
