namespace isr.VI
{
    /// <summary>   Interface for information report. </summary>
    /// <remarks>   David, 2021-04-12. </remarks>
    public interface IInfoReport
    {
        /// <summary>   Builds information report. </summary>
        /// <returns>   A string. </returns>
        string BuildInfoReport();
    }
}
