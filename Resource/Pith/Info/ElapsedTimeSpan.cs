using System;

namespace isr.VI
{
    /// <summary>   An elapsed time span. </summary>
    /// <remarks>   David, 2021-04-10. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1815:Override equals and operator equals on value types", Justification = "<Pending>" )]
    public struct ElapsedTimeSpan
    {
        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="identity"> The identity. </param>
        /// <param name="elapsed">  The elapsed. </param>
        public ElapsedTimeSpan( ElapsedTimeIdentity identity, TimeSpan elapsed )
        {
            this.Identity = identity;
            this.Elapsed = elapsed;
        }
        /// <summary>   Gets or sets the identity. </summary>
        /// <value> The identity. </value>
        public ElapsedTimeIdentity Identity { get; set; }
        /// <summary>   Gets or sets the elapsed. </summary>
        /// <value> The elapsed. </value>
        public TimeSpan Elapsed { get; set; }
    }

    /// <summary>   Values that represent elapsed time identities. </summary>
    /// <remarks>   David, 2021-04-10. </remarks>
    public enum ElapsedTimeIdentity
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary>   An enum constant representing the write time option. </summary>
        [System.ComponentModel.Description( "Write Time" )]
        WriteTime,

        /// <summary>   An enum constant representing the write time option. </summary>
        [System.ComponentModel.Description( "Read Time" )]
        ReadTime,

        /// <summary>   An enum constant representing the query time option. </summary>
        [System.ComponentModel.Description( "Query Time" )]
        QueryTime,

        /// <summary>   An enum constant representing time to determine that the instrument is ready to receive a write command. </summary>
        [System.ComponentModel.Description( "Write Ready Delay" )]
        WriteReadyDelay,

        /// <summary>   An enum constant representing time to determine that the instrument is ready receive a write command. </summary>
        [System.ComponentModel.Description( "Read Ready Delay" )]
        ReadReadyDelay,

        /// <summary>   An enum constant representing the read delay option. </summary>
        [System.ComponentModel.Description( "Read Delay" )]
        ReadDelay,

        /// <summary>   An enum constant representing the total write time option. </summary>
        [System.ComponentModel.Description( "Total Write Time" )]
        TotalWriteTime,

    }


}
