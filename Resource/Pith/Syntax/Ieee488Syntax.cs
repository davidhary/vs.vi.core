using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace isr.VI.Pith.Ieee488
{
    /// <summary> Defines the standard IEEE488 command set. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2005-01-15, 1.0.1841.x. </para></remarks>
    public static class Syntax
    {

        #region " IEEE 488.2 STANDARD COMMANDS "

        /// <summary> Gets the Clear Status (CLS) command. </summary>
        public const string ClearExecutionStateCommand = "*CLS";

        /// <summary> Gets the Identity query (*IDN?) command. </summary>
        public const string IdentityQueryCommand = "*IDN?";

        /// <summary> Gets the operation complete (*OPC) command. </summary>
        public const string OperationCompleteCommand = "*OPC";

        /// <summary> Gets the operation complete query (*OPC?) command. </summary>
        public const string OperationCompletedQueryCommand = "*OPC?";

        /// <summary> Gets the options query (*OPT?) command. </summary>
        public const string OptionsQueryCommand = "*OPT?";

        /// <summary> Gets the Wait (*WAI) command. </summary>
        public const string WaitCommand = "*WAI";

        /// <summary> Gets the Standard Event Enable (*ESE) command. </summary>
        public const string StandardEventEnableCommandFormat = "*ESE {0:D}";

        /// <summary> Gets the Standard Event Enable query (*ESE?) command. </summary>
        public const string StandardEventEnableQueryCommand = "*ESE?";

        /// <summary> Gets the Standard Event Enable (*ESR?) command. </summary>
        public const string StandardEventStatusQueryCommand = "*ESR?";

        /// <summary> Gets the Service Request Enable (*SRE) command. </summary>
        public const string ServiceRequestEnableCommandFormat = "*SRE {0:D}";

        /// <summary> Gets the Standard Event and Service Request Enable '*CLS; *ESE {0:D}; *SRE {1:D}' command format. </summary>
        public const string StandardServiceEnableCommandFormat = "*CLS; *ESE {0:D}; *SRE {1:D}";

        /// <summary> Gets the Standard Event and Service Request Enable '*CLS; *ESE {0:D}; *SRE {1:D}; *OPC' command format. </summary>
        public const string StandardServiceEnableCompleteCommandFormat = "*CLS; *ESE {0:D}; *SRE {1:D}; *OPC";

        /// <summary> Gets the Operation Complete Enable '*CLS; *ESE {0:D}; *OPC' command format. </summary>
        public const string OperationCompleteEnableCommandFormat = "*CLS; *ESE {0:D}; *OPC";

        /// <summary> Gets the Service Request Enable query (*SRE?) command. </summary>
        public const string ServiceRequestEnableQueryCommand = "*SRE?";

        /// <summary> Gets the Service Request Status query (*STB?) command. </summary>
        public const string ServiceRequestQueryCommand = "*STB?";

        /// <summary> Gets the reset to know state (*RST) command. </summary>
        public const string ResetKnownStateCommand = "*RST";

        #endregion

        #region " KEITHLEY IEEE488 COMMANDS "

        /// <summary> Gets the Language query (*LANG?) command. </summary>
        public const string LanguageQueryCommand = "*LANG?";

        /// <summary> Gets the Language command format (*LANG). </summary>
        public const string LanguageCommandFormat = "*LANG {0}";

        /// <summary> The language scpi. </summary>
        public const string LanguageScpi = "SCPI";

        /// <summary> The language TSP. </summary>
        public const string LanguageTsp = "TSP";

        #endregion

        #region " BUILDERS "

        /// <summary> Builds the device clear (DCL) command. </summary>
        /// <returns>
        /// An enumerator that allows for-each to be used to process build device clear command in this
        /// collection.
        /// </returns>
        public static IEnumerable<byte> BuildDeviceClear()
        {
            // Thee DCL command to the interface.
            var commands = new byte[] { Convert.ToByte( ( int ) CommandCode.Untalk ), Convert.ToByte( ( int ) CommandCode.Unlisten ),
                                              Convert.ToByte( ( int ) CommandCode.DeviceClear ),
                                              Convert.ToByte( ( int ) CommandCode.Untalk ), Convert.ToByte( ( int ) CommandCode.Unlisten ) };
            return commands;
        }

        /// <summary> Builds selective device clear (SDC) in this collection. </summary>
        /// <param name="gpibAddress"> The gpib address. </param>
        /// <returns>
        /// An enumerator that allows for-each to be used to process build selective device clear in this
        /// collection.
        /// </returns>
        public static IEnumerable<byte> BuildSelectiveDeviceClear( byte gpibAddress )
        {
            var commands = new byte[] { Convert.ToByte((int)CommandCode.Untalk), Convert.ToByte((int)CommandCode.Unlisten),
                                            Convert.ToByte( Convert.ToByte((int)CommandCode.ListenAddressGroup) | gpibAddress),
                                            Convert.ToByte((int)CommandCode.SelectiveDeviceClear),
                                            Convert.ToByte((int)CommandCode.Untalk), Convert.ToByte((int)CommandCode.Unlisten) };
            return commands;
        }

        #endregion

    }

    /// <summary> Values that represent IEEE 488.2 Command Code. </summary>
    public enum CommandCode
    {

        /// <summary> An enum constant representing the none option. </summary>
        None = 0,

        /// <summary> An enum constant representing the go to local option. </summary>
        [Description( "GTL" )]
        GoToLocal = 0x1,

        /// <summary> An enum constant representing the selective device clear option. </summary>
        [Description( "SDC" )]
        SelectiveDeviceClear = 0x4,

        /// <summary> An enum constant representing the group execute trigger option. </summary>
        [Description( "GET" )]
        GroupExecuteTrigger = 0x8,

        /// <summary> An enum constant representing the local lockout option. </summary>
        [Description( "LLO" )]
        LocalLockout = 0x11,

        /// <summary> An enum constant representing the device clear option. </summary>
        [Description( "DCL" )]
        DeviceClear = 0x14,

        /// <summary> An enum constant representing the serial poll enable option. </summary>
        [Description( "SPE" )]
        SerialPollEnable = 0x18,

        /// <summary> An enum constant representing the serial poll disable option. </summary>
        [Description( "SPD" )]
        SerialPollDisable = 0x19,

        /// <summary> An enum constant representing the listen address group option. </summary>
        [Description( "LAG" )]
        ListenAddressGroup = 0x20,

        /// <summary> An enum constant representing the talk address group option. </summary>
        [Description( "TAG" )]
        TalkAddressGroup = 0x40,

        /// <summary> An enum constant representing the secondary command group option. </summary>
        [Description( "SCG" )]
        SecondaryCommandGroup = 0x60,

        /// <summary> An enum constant representing the unlisten option. </summary>
        [Description( "UNL" )]
        Unlisten = 0x3F,

        /// <summary> An enum constant representing the untalk option. </summary>
        [Description( "UNT" )]
        Untalk = 0x5F
    }
}
