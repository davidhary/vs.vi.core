using System;

namespace isr.VI.Pith
{
    public partial class SessionBase
    {

        #region " QUERY PAYLOAD "


        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read.
        /// Parses the Boolean return value.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="payload"> The payload. </param>
        /// <returns>
        /// <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
        /// <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
        /// </returns>
        public bool Query( PayloadBase payload )
        {
            if ( payload is null )
                throw new ArgumentNullException( nameof( payload ) );
            this.EmulatedReply = payload.SimulatedPayload;
            payload.QueryStatus = PayloadStatus.Okay | PayloadStatus.Sent;
            payload.QueryMessage = payload.BuildQueryCommand();
            payload.ReceivedMessage = this.QueryTrimEnd( payload.QueryMessage );
            payload.QueryStatus |= PayloadStatus.QueryReceived;
            payload.FromReading( payload.ReceivedMessage );
            return PayloadStatus.Okay == (payload.QueryStatus & PayloadStatus.Okay);
        }

        #endregion

        #region " WRITE PAYLOAD "

        /// <summary> Writes the payload to the device. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="payload"> The payload. </param>
        /// <returns>
        /// <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
        /// <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
        /// </returns>
        public bool Write( PayloadBase payload )
        {
            if ( payload is null )
                throw new ArgumentNullException( nameof( payload ) );
            payload.CommandStatus = PayloadStatus.Okay | PayloadStatus.Sent;
            payload.SentMessage = payload.BuildCommand();
            _ = this.WriteLine( payload.SentMessage );
            return PayloadStatus.Okay == (payload.CommandStatus & PayloadStatus.Okay);
        }

        #endregion

    }
}
