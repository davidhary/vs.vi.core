using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace isr.VI.Pith
{

    /// <summary> Base class manager of VISA resource names. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-21 </para>
    /// </remarks>
    public sealed class ResourceNamesManager
    {

        #region " CONSTRUCTION "


        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private ResourceNamesManager() : base()
        {
        }

        #endregion

        #region " SEARCH PATTERNS "

        /// <summary> Name of the gpib resource base. </summary>
        public const string GpibResourceBaseName = "GPIB";

        /// <summary> Name of the gpib vxi resource base. </summary>
        public const string GpibVxiResourceBaseName = "GPIBVxi";

        /// <summary> Name of the serial resource base. </summary>
        public const string SerialResourceBaseName = "ASRL";

        /// <summary> Name of the TCP IP resource base. </summary>
        public const string TcpIPResourceBaseName = "TCPIP";

        /// <summary> Name of the USB resource base. </summary>
        public const string UsbResourceBaseName = "USB";

        /// <summary> Name of the pxi resource base. </summary>
        public const string PxiResourceBaseName = "Pxi";

        /// <summary> Name of the vxi resource base. </summary>
        public const string VxiResourceBaseName = "Vxi";

        /// <summary> Interface base name. </summary>
        public const string InterfaceBaseName = "INTFC";

        /// <summary> Name of the instrument base. </summary>
        public const string InstrumentBaseName = "INSTR";

        /// <summary> Name of the backplane base. </summary>
        public const string BackplaneBaseName = "BACKPLANE";

        /// <summary> Name of the raw base. </summary>
        public const string RawBaseName = "RAW";

        /// <summary> A pattern specifying all resources search. </summary>
        public const string AllResourcesFilter = "?*";

        /// <summary> A pattern specifying the Gpib, USB or TCPIP search. </summary>
        public const string GpibUsbTcpIPFilter = "(GPIB|USB|TCPIP)?*";

        /// <summary> A pattern specifying the Gpib, USB or TCPIP instrument search. </summary>
        public const string GpibUsbTcpIPInstrumentFilter = GpibUsbTcpIPFilter + InstrumentBaseName;

        /// <summary> The backplane filter format. </summary>
        public const string BackplaneFilterFormat = "{0}?*" + BackplaneBaseName;

        /// <summary> The raw filter format. </summary>
        public const string RawFilterFormat = "{0}?*" + RawBaseName;

        /// <summary> Interface for ilter format. </summary>
        public const string InterfaceFilterFormat = "{0}?*" + InterfaceBaseName;

        /// <summary> The instrument filter format. </summary>
        public const string InstrumentFilterFormat = "{0}?*" + InstrumentBaseName;

        /// <summary> The instrument board filter format. </summary>
        public const string InstrumentBoardFilterFormat = "{0}{1}?*" + InstrumentBaseName;

        /// <summary> Interface for esource format. </summary>
        public const string InterfaceResourceFormat = "{0}{1}::" + InterfaceBaseName;

        /// <summary> Parse address. </summary>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <returns> A String. </returns>
        public static string ParseAddress( string resourceName )
        {
            string address = string.Empty;
            if ( !string.IsNullOrWhiteSpace( resourceName ) )
            {
                int addressLocation = resourceName.IndexOf( "::", StringComparison.OrdinalIgnoreCase ) + 2;
                int addressWidth = resourceName.LastIndexOf( "::", StringComparison.OrdinalIgnoreCase ) - addressLocation;
                address = resourceName.Substring( addressLocation, addressWidth );
            }

            return address;
        }

        /// <summary> Parse gpib address. </summary>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <returns> An Integer. </returns>
        public static int ParseGpibAddress( string resourceName )
        {
            string address = ParseAddress( resourceName );
            if ( int.TryParse( address, out int value ) )
            {
            }

            return value;
        }

        /// <summary> Parse interface number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> An Integer. </returns>
        public static int ParseInterfaceNumber( string resourceName )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
                throw new ArgumentNullException( nameof( resourceName ) );
            var interfaceType = ParseHardwareInterfaceType( resourceName );
            string baseName = InterfaceResourceBaseName( interfaceType );
            var parts = resourceName.Split( ':' );
            if ( parts.Count() <= 0 || parts[0].Length <= baseName.Length || !int.TryParse( parts[0].Substring( baseName.Length ), out int result ) )
            {
                result = 0;
            }

            return result;
        }

        /// <summary> Parse resource type. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A HardwareInterfaceType. </returns>
        public static ResourceType ParseResourceType( string resourceName )
        {
            var result = ResourceType.None;
            if ( !string.IsNullOrWhiteSpace( resourceName ) )
            {
                foreach ( ResourceType v in Enum.GetValues( typeof( ResourceType ) ) )
                {
                    if ( v != ResourceType.None && resourceName.EndsWith( ResourceTypeBaseName( v ), StringComparison.OrdinalIgnoreCase ) )
                    {
                        result = v;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary> Resource type base name. </summary>
        /// <param name="resourceType"> Type of the resource. </param>
        /// <returns> A String. </returns>
        public static string ResourceTypeBaseName( ResourceType resourceType )
        {
            string result = string.Empty;
            switch ( resourceType )
            {
                case ResourceType.None:
                    {
                        break;
                    }

                case ResourceType.Instrument:
                    {
                        result = InstrumentBaseName;
                        break;
                    }

                case ResourceType.Interface:
                    {
                        result = InterfaceBaseName;
                        break;
                    }

                case ResourceType.Backplane:
                    {
                        result = BackplaneBaseName;
                        break;
                    }
            }

            return result;
        }

        /// <summary> Parse hardware interface resource base name. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A String. </returns>
        public static string ParseInterfaceResourceBaseName( string resourceName )
        {
            string result = HardwareInterfaceType.Custom.ToString();
            if ( !string.IsNullOrWhiteSpace( resourceName ) )
            {
                foreach ( HardwareInterfaceType v in Enum.GetValues( typeof( HardwareInterfaceType ) ) )
                {
                    if ( v != HardwareInterfaceType.Custom && resourceName.StartsWith( InterfaceResourceBaseName( v ), StringComparison.OrdinalIgnoreCase ) )
                    {
                        result = InterfaceResourceBaseName( v );
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary> Parse hardware interface type. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A HardwareInterfaceType. </returns>
        public static HardwareInterfaceType ParseHardwareInterfaceType( string resourceName )
        {
            var result = HardwareInterfaceType.Custom;
            if ( !string.IsNullOrWhiteSpace( resourceName ) )
            {
                foreach ( HardwareInterfaceType v in Enum.GetValues( typeof( HardwareInterfaceType ) ) )
                {
                    if ( v != HardwareInterfaceType.Custom && resourceName.StartsWith( InterfaceResourceBaseName( v ), StringComparison.OrdinalIgnoreCase ) )
                    {
                        result = v;
                        break;
                    }
                }
            }

            return result;
        }

        /// <summary> Returns the interface resource base name. </summary>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The interface base name. </returns>
        public static string InterfaceResourceBaseName( HardwareInterfaceType interfaceType )
        {
            switch ( interfaceType )
            {
                case HardwareInterfaceType.Gpib:
                    {
                        return GpibResourceBaseName;
                    }

                case HardwareInterfaceType.GpibVxi:
                    {
                        return GpibVxiResourceBaseName;
                    }

                case HardwareInterfaceType.Pxi:
                    {
                        return PxiResourceBaseName;
                    }

                case HardwareInterfaceType.Serial:
                    {
                        return SerialResourceBaseName;
                    }

                case HardwareInterfaceType.Tcpip:
                    {
                        return TcpIPResourceBaseName;
                    }

                case HardwareInterfaceType.Usb:
                    {
                        return UsbResourceBaseName;
                    }

                case HardwareInterfaceType.Vxi:
                    {
                        return VxiResourceBaseName;
                    }

                default:
                    {
                        throw new ArgumentException( $"Unhandled case {interfaceType}", nameof( interfaceType ) );
                    }
            }
        }

        /// <summary> Returns the Interface resource name. </summary>
        /// <param name="interfaceName"> The name of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The Interface resource name, e.g., 'GPIB?*INTFC'. </returns>
        public static string BuildInterfaceResourceName( string interfaceName, int boardNumber )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, InterfaceResourceFormat, interfaceName, boardNumber );
        }

        /// <summary> Returns the Interface resource name. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The Interface resource name, e.g., 'GPIB?*INTFC'. </returns>
        public static string BuildInterfaceResourceName( HardwareInterfaceType interfaceType, int boardNumber )
        {
            return BuildInterfaceResourceName( InterfaceResourceBaseName( interfaceType ), boardNumber );
        }

        /// <summary> Returns the Interface search pattern. </summary>
        /// <returns> The Interface search pattern '?*INTFC'. </returns>
        public static string BuildInterfaceFilter()
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, InterfaceFilterFormat, "" );
        }

        /// <summary> Returns the Interface search pattern. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The Interface search pattern, e.g., 'GPIB?*INTFC'. </returns>
        public static string BuildInterfaceFilter( HardwareInterfaceType interfaceType )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, InterfaceFilterFormat, InterfaceResourceBaseName( interfaceType ) );
        }

        /// <summary> Returns the Instrument search pattern. </summary>
        /// <returns> The Instrument search pattern, e.g., '?*INSTR'. </returns>
        public static string BuildInstrumentFilter()
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, InstrumentFilterFormat, "" );
        }

        /// <summary> Returns the Instrument search pattern. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The Instrument search pattern, e.g., 'GPIB?*INSTR'. </returns>
        public static string BuildInstrumentFilter( HardwareInterfaceType interfaceType )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, InstrumentFilterFormat, InterfaceResourceBaseName( interfaceType ) );
        }

        /// <summary> Returns the Instrument search pattern. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        /// <param name="interface1">       Type of the interface. </param>
        /// <param name="interface2">       The second interface. </param>
        /// <param name="usingLikePattern"> True to use the Like pattern, which uses square brackets for
        /// or alternative values. </param>
        /// <returns> The Instrument search pattern, e.g., '(GPIB|USB)?*INSTR'. </returns>
        public static string BuildInstrumentFilter( HardwareInterfaceType interface1, HardwareInterfaceType interface2, bool usingLikePattern )
        {
            return usingLikePattern ? $"[{InterfaceResourceBaseName( interface1 )}|{InterfaceResourceBaseName( interface2 )}]?*{InstrumentBaseName}" : $"({InterfaceResourceBaseName( interface1 )}|{InterfaceResourceBaseName( interface2 )})?*{InstrumentBaseName}";
        }

        /// <summary> Returns the Instrument search pattern. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        /// <param name="interface1">       Type of the interface. </param>
        /// <param name="interface2">       The second interface. </param>
        /// <param name="interface3">       The third interface. </param>
        /// <param name="usingLikePattern"> True to use the Like pattern, which uses square brackets for
        /// or alternative values. </param>
        /// <returns> The Instrument search pattern, e.g., '(GPIB|USB)?*INSTR'. </returns>
        public static string BuildInstrumentFilter( HardwareInterfaceType interface1, HardwareInterfaceType interface2, HardwareInterfaceType interface3, bool usingLikePattern )
        {
            return usingLikePattern ? $"[{InterfaceResourceBaseName( interface1 )}|{InterfaceResourceBaseName( interface2 )}|{InterfaceResourceBaseName( interface3 )}]?*{InstrumentBaseName}" : $"({InterfaceResourceBaseName( interface1 )}|{InterfaceResourceBaseName( interface2 )}|{InterfaceResourceBaseName( interface3 )})?*{InstrumentBaseName}";
        }

        /// <summary> Returns the Instrument search pattern. </summary>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
        /// <returns> The Instrument search pattern, e.g., 'GPIB0?*INSTR'. </returns>
        public static string BuildInstrumentFilter( HardwareInterfaceType interfaceType, int interfaceNumber )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, InstrumentBoardFilterFormat, InterfaceResourceBaseName( interfaceType ), interfaceNumber );
        }

        #endregion

        #region " RESOURCE NAMES "

        /// <summary> The Gpib instrument resource format. </summary>
        private const string _GpibInstrumentResourceFormat = "GPIB{0}::{1}::INSTR";

        /// <summary> Builds Gpib instrument resource. </summary>
        /// <param name="boardNumber"> The board number. </param>
        /// <param name="address">     The address. </param>
        /// <returns> The resource name. </returns>
        public static string BuildGpibInstrumentResource( int boardNumber, int address )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, _GpibInstrumentResourceFormat, boardNumber, address );
        }

        /// <summary> Builds Gpib instrument resource. </summary>
        /// <param name="boardNumber"> The board number. </param>
        /// <param name="address">     The address. </param>
        /// <returns> The resource name. </returns>
        public static string BuildGpibInstrumentResource( string boardNumber, string address )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, _GpibInstrumentResourceFormat, boardNumber, address );
        }

        /// <summary> The USB instrument resource format. </summary>
        private const string _UsbInstrumentResourceFormat = "USB{0}::0x{1:X}::0x{2:X}::{3}::INSTR";

        /// <summary> Builds Gpib instrument resource. </summary>
        /// <param name="boardNumber">    The board number. </param>
        /// <param name="manufacturerId"> Identifier for the manufacturer. </param>
        /// <param name="modelNumber">    The model number. </param>
        /// <param name="serialNumber">   The serial number. </param>
        /// <returns> The resource name. </returns>
        public static string BuildUsbInstrumentResource( int boardNumber, int manufacturerId, int modelNumber, int serialNumber )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, _UsbInstrumentResourceFormat, boardNumber, manufacturerId, modelNumber, serialNumber );
        }

        #endregion

        #region " PING "

        /// <summary> Enumerates resources that can be pinged or non-TCP/IP resources. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resources"> The resources. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process ping filter in this collection.
        /// </returns>
        public static IEnumerable<string> PingFilter( IEnumerable<string> resources )
        {
            if ( resources is null )
                throw new ArgumentNullException( nameof( resources ) );
            var l = new List<string>();
            foreach ( string resource in resources )
            {
                if ( !IsTcpipResource( resource ) || PingTcpipResource( resource ) )
                {
                    l.Add( resource );
                }
            }

            return l;
        }

        /// <summary> Query if 'resourceName' is TCP IP resource. </summary>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <returns> <c>true</c> if TCP IP resource; otherwise <c>false</c> </returns>
        public static bool IsTcpipResource( string resourceName )
        {
            return !string.IsNullOrWhiteSpace( resourceName ) && resourceName.StartsWith( HardwareInterfaceType.Tcpip.ToString(), StringComparison.OrdinalIgnoreCase );
        }

        /// <summary> Converts a resourceName to a resource address. </summary>
        /// <remarks> Works only on TCP/IP resources. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <returns> The resource TCP/IP address. </returns>
        public static string ToResourceAddress( string resourceName )
        {
            return string.IsNullOrWhiteSpace( resourceName )
                ? throw new ArgumentNullException( nameof( resourceName ) )
                : !IsTcpipResource( resourceName )
                ? throw new InvalidOperationException( $"Unable to convert resource {resourceName} to a {HardwareInterfaceType.Tcpip} resource" )
                : resourceName.Split( ':' )[2];
        }

        /// <summary> Pings a TcpIP resource name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool PingTcpipResource( string resourceName )
        {
            return string.IsNullOrWhiteSpace( resourceName )
                ? throw new ArgumentNullException( nameof( resourceName ) )
                : FastPingInternal( ToResourceAddress( resourceName ), My.Settings.Default.PingTimeout );
        }

        /// <summary>   Fast ping internal. </summary>
        /// <remarks>   David, 2021-03-25. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="address">  The address. </param>
        /// <param name="timeout">  The timeout. </param>
        /// <returns>   True if it succeeds, false if it fails. </returns>
        private static bool FastPingInternal( string address, TimeSpan timeout )
        {
            if ( string.IsNullOrWhiteSpace( address ) )
                throw new ArgumentNullException( nameof( address ) );
            var ping = new System.Net.NetworkInformation.Ping();
            var buffer = new byte[] { 0, 0 };
            return ping.Send( address, ( int ) timeout.TotalMilliseconds, buffer ).Status == System.Net.NetworkInformation.IPStatus.Success;
        }

        /// <summary> Fast ping. </summary>
        /// <remarks> David, 2020-08-07. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="address"> The address. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public static bool FastPing( string address, TimeSpan timeout )
        {
            if ( string.IsNullOrWhiteSpace( address ) )
                throw new ArgumentNullException( nameof( address ) );
            var ping = new System.Net.NetworkInformation.Ping();
            var pingOptions = new System.Net.NetworkInformation.PingOptions( My.Settings.Default.PingHops, true );
            var buffer = new byte[] { 0, 0 };
            return ping.Send( address, ( int ) timeout.TotalMilliseconds, buffer, pingOptions ).Status == System.Net.NetworkInformation.IPStatus.Success;
        }

        #endregion

    }

    /// <summary> Values that represent resource types. </summary>
    public enum ResourceType
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not specified" )]
        None = 0,

        /// <summary> An enum constant representing the instrument option. </summary>
        [Description( "Instrument" )]
        Instrument,

        /// <summary> An enum constant representing the interface] option. </summary>
        [Description( "Interface" )]
        Interface,

        /// <summary> An enum constant representing the backplane option. </summary>
        [Description( "Backplane" )]
        Backplane
    }

    /// <summary> Values that represent hardware interface types. </summary>
    public enum HardwareInterfaceType
    {

        /// <summary> An enum constant representing the custom option. </summary>
        [Description( "Custom" )]
        Custom = 0,

        /// <summary> An enum constant representing the gpib interface. </summary>
        [Description( "GPIB" )]
        Gpib = 1,

        /// <summary> An enum constant representing the vxi option. </summary>
        [Description( "VXI" )]
        Vxi = 2,

        /// <summary> An enum constant representing the gpib vxi option. </summary>
        [Description( "GPIB VXI" )]
        GpibVxi = 3,

        /// <summary> An enum constant representing the serial option. </summary>
        [Description( "Serial" )]
        Serial = 4,

        /// <summary> An enum constant representing the pxi option. </summary>
        [Description( "PXI" )]
        Pxi = 5,

        /// <summary> An enum constant representing the TCP/IP option. </summary>
        [Description( "TCPIP" )]
        Tcpip = 6,

        /// <summary> An enum constant representing the USB option. </summary>
        [Description( "USB" )]
        Usb = 7
    }
}
