using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using isr.Core;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Pith
{

    /// <summary> Information about the resource name. </summary>
    /// <remarks>
    /// David, 2020-06-06. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class ResourceNameInfo
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="resourceName"> The name of the resource. </param>
        public ResourceNameInfo( string resourceName ) : base()
        {
            this.ResourceName = resourceName;
            this.ParseThis( resourceName );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="fields"> The fields. </param>
        public ResourceNameInfo( IEnumerable<string> fields ) : base()
        {
            if ( fields?.Any() == true )
            {
                var queue = new Queue<string>( fields );
                if ( queue.Any() )
                    this.ResourceName = queue.Dequeue();
                this.ParseThis( this.ResourceName );
            }
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="resourceName">    The name of the resource. </param>
        /// <param name="interfaceType">   The type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number. </param>
        public ResourceNameInfo( string resourceName, HardwareInterfaceType interfaceType, int interfaceNumber ) : base()
        {
            this.ResourceName = resourceName;
            this.InterfaceType = interfaceType;
            this.InterfaceNumber = interfaceNumber;
            this.InterfaceBaseName = ResourceNamesManager.InterfaceResourceBaseName( this.InterfaceType );
            this.InterfaceResourceName = ResourceNamesManager.BuildInterfaceResourceName( this.InterfaceBaseName, this.InterfaceNumber );
            this.ResourceType = ResourceNamesManager.ParseResourceType( resourceName );
            this.ResourceAddress = string.Empty;
            if ( this.ResourceType == ResourceType.Instrument )
            {
                this.ResourceAddress = ResourceNamesManager.ParseAddress( resourceName );
            }

            if ( this.InterfaceType == HardwareInterfaceType.Gpib )
            {
                if ( !int.TryParse( this.ResourceAddress, out int address ) )
                {
                    address = 0;
                }
                this.GpibAddress = address;
            }
        }

        /// <summary> Parses. </summary>
        /// <param name="resourceName"> The name of the resource. </param>
        private void ParseThis( string resourceName )
        {
            this.ResourceName = resourceName;
            this.InterfaceType = ResourceNamesManager.ParseHardwareInterfaceType( resourceName );
            this.InterfaceNumber = ResourceNamesManager.ParseInterfaceNumber( resourceName );
            this.InterfaceBaseName = ResourceNamesManager.InterfaceResourceBaseName( this.InterfaceType );
            this.InterfaceResourceName = ResourceNamesManager.BuildInterfaceResourceName( this.InterfaceBaseName, this.InterfaceNumber );
            this.ResourceType = ResourceNamesManager.ParseResourceType( resourceName );
            this.ResourceAddress = string.Empty;
            if ( this.ResourceType == ResourceType.Instrument )
            {
                this.ResourceAddress = ResourceNamesManager.ParseAddress( resourceName );
            }

            if ( this.InterfaceType == HardwareInterfaceType.Gpib )
            {
                if ( !int.TryParse( this.ResourceAddress, out int address ) )
                {
                    address = 0;
                }
                this.GpibAddress = address;
            }

            this.UsingLanController = System.Globalization.CultureInfo.InvariantCulture.CompareInfo.IndexOf( resourceName, "gpib0,", System.Globalization.CompareOptions.OrdinalIgnoreCase ) >= 0;
        }

        /// <summary> Parses resource name. </summary>
        /// <param name="resourceName"> The name of the resource. </param>
        public void Parse( string resourceName )
        {
            this.ParseThis( resourceName );
        }

        #endregion

        #region " FIELDS "

        /// <summary> Gets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        public string ResourceName { get; set; }

        /// <summary> Gets the type of the resource. </summary>
        /// <value> The type of the resource. </value>
        public ResourceType ResourceType { get; set; }

        /// <summary> Gets the type of the interface. </summary>
        /// <value> The type of the interface. </value>
        public HardwareInterfaceType InterfaceType { get; set; }

        /// <summary> Gets the interface number. </summary>
        /// <value> The interface number. </value>
        public int InterfaceNumber { get; set; }

        /// <summary> Gets the resource address. </summary>
        /// <value> The resource address. </value>
        public string ResourceAddress { get; set; }

        /// <summary> Gets the gpib address. </summary>
        /// <value> The gpib address. </value>
        public int GpibAddress { get; set; }

        /// <summary> Gets the sentinel indicating if the resource uses a LAN controller. </summary>
        /// <value> The sentinel indicating if the resource uses a LAN controller. </value>
        public bool UsingLanController { get; set; }

        /// <summary> Gets the is parsed. </summary>
        /// <value> The is parsed. </value>
        public bool IsParsed => !string.IsNullOrWhiteSpace( this.ResourceName );

        /// <summary> Gets the name of the interface base. </summary>
        /// <value> The name of the interface base. </value>
        public string InterfaceBaseName { get; private set; }

        /// <summary> Gets the name of the interface resource. </summary>
        /// <value> The name of the interface resource. </value>
        public string InterfaceResourceName { get; private set; }

        #endregion

        #region " STORAGE "

        /// <summary> Builds header record. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> A String. </returns>
        public static string BuildHeaderRecord( string delimiter )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( nameof( ResourceName ) );
            _ = builder.Append( delimiter );
            _ = builder.Append( nameof( InterfaceBaseName ) );
            return builder.ToString();
        }

        /// <summary> Builds data record. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="delimiter"> The delimiter. </param>
        /// <returns> A String. </returns>
        public string BuildDataRecord( string delimiter )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( this.ResourceName );
            _ = builder.Append( delimiter );
            _ = builder.Append( this.InterfaceBaseName );
            return builder.ToString();
        }

        #endregion

    }

    /// <summary> Collection of resource name informations. </summary>
    /// <remarks>
    /// David, 2020-06-06. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class ResourceNameInfoCollection : System.Collections.ObjectModel.KeyedCollection<string, ResourceNameInfo>
    {

        #region " CONSTRUCTION "


        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" /> class that uses the default
        /// equality comparer.
        /// </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        public ResourceNameInfoCollection() : base()
        {
            this.Keys = new List<string>();
            this.DefaultFileName = "VisaResources.txt";
            this.BackupFileName = "VisaResourcesBackup.txt";
            this.DefaultFolderName = Path.Combine( Environment.GetFolderPath( Environment.SpecialFolder.CommonDocuments ), "isr.visa" );
        }


        /// <summary>
        /// When implemented in a derived class, extracts the key from the specified element.
        /// </summary>
        /// <param name="item"> The element from which to extract the key. </param>
        /// <returns> The key for the specified element. </returns>
        protected override string GetKeyForItem( ResourceNameInfo item )
        {
            return item.ResourceName;
        }

        #endregion

        #region " ITEM MANAGEMENT "


        /// <summary>
        /// Adds or replaces an object to the end of the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="item"> The object to be added to the end of the
        /// <see cref="T:System.Collections.ObjectModel.Collection`1" />. The value
        /// can be <see langword="null" /> for reference types. </param>
        public new void Add( ResourceNameInfo item )
        {
            if ( this.Contains( item.ResourceName ) )
                this.Remove( item.ResourceName );
            base.Add( item );
            this.Keys.Add( item.ResourceName );
        }


        /// <summary>
        /// Adds an object to the end of the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="resourceName"> The resource name to remove. </param>
        public void Add( string resourceName )
        {
            this.Add( new ResourceNameInfo( resourceName ) );
        }

        /// <summary> Removes the given resourceName. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="resourceName"> The resource name to remove. </param>
        public new void Remove( string resourceName )
        {
            _ = base.Remove( resourceName );
            _ = this.Keys.Remove( resourceName );
        }


        /// <summary>
        /// Removes all elements from the <see cref="T:System.Collections.ObjectModel.Collection`1" />.
        /// </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        public new void Clear()
        {
            base.Clear();
            this.Keys.Clear();
        }

        /// <summary> Gets the keys. </summary>
        /// <value> The keys. </value>
        public IList<string> Keys { get; private set; }

        /// <summary> Gets a list of names of the resources. </summary>
        /// <value> A list of names of the resources. </value>
        public IList<string> ResourceNames => this.Keys;

        /// <summary> Populates the given items. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="items"> The items. </param>
        /// <returns> An Integer. </returns>
        public int Populate( IList<ResourceNameInfo> items )
        {
            foreach ( ResourceNameInfo item in items )
                this.Add( item );
            return this.Count;
        }

        /// <summary> Adds a new resource. </summary>
        /// <remarks> David, 2020-06-08. </remarks>
        /// <param name="resourceName"> The resource name to remove. </param>
        public static void AddNewResource( string resourceName )
        {
            var rm = new ResourceNameInfoCollection();
            rm.ReadResources();
            rm.Add( resourceName );
            rm.WriteResources();
            rm.BackupResources();
        }
        #endregion

        #region " FIND "

        /// <summary> Enumerates the items in this collection that meet given criteria. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="pattern"> Specifies the pattern. </param>
        /// <returns> An enumerator that allows foreach to be used to process the matched items. </returns>
        public IList<string> Find( string pattern )
        {
            var coll = new ResourceNameInfoCollection();
            _ = coll.Populate( this.FindResourceNamesInfo( pattern ) );
            return coll.ResourceNames;
        }

        /// <summary> Query if <see cref="ResourceNameInfo.ResourceName"/> matches the pattern. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        /// <param name="resourceNameInfo"> Information describing the resource name. </param>
        /// <param name="pattern">          Specifies the pattern. </param>
        /// <returns> True if match, false if not. </returns>
        public static bool IsMatch( ResourceNameInfo resourceNameInfo, string pattern )
        {
            return LikeOperator.LikeString( resourceNameInfo.ResourceName, pattern, CompareMethod.Binary );
        }

        /// <summary> Finds the resource names informations in this collection. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="pattern"> Specifies the pattern. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the resource names informations in
        /// this collection.
        /// </returns>
        public IList<ResourceNameInfo> FindResourceNamesInfo( string pattern )
        {
            return (from v in this
                    where LikeOperator.LikeString( v.ResourceName, pattern, CompareMethod.Binary )
                    select v).ToList();
        }

        #endregion

        #region " STORAGE "

        /// <summary> Opens a file. </summary>
        /// <param name="fileName">   Filename of the file. </param>
        /// <param name="openMode">   The open mode. </param>
        /// <param name="accessMode"> The access mode. </param>
        /// <param name="shareMode">  The share mode. </param>
        /// <returns> An Integer. </returns>
        [System.Security.Permissions.FileIOPermission( System.Security.Permissions.SecurityAction.Demand, Unrestricted = true )]
        private static int OpenFile( string fileName, OpenMode openMode, OpenAccess accessMode, OpenShare shareMode )
        {
            int fileNumber = 0;
            try
            {
                fileNumber = FileSystem.FreeFile();
                // TO_DO: Use task; See VI Session Base Await Service request
                int trialCount = 10;
                while ( trialCount > 0 )
                {
                    trialCount -= 1;
                    try
                    {
                        FileSystem.FileOpen( fileNumber, fileName, openMode, accessMode, shareMode );
                        trialCount = 0;
                        break;
                    }
                    catch
                    {
                        if ( trialCount == 1 )
                            throw;
                    }

                    if ( trialCount > 0 )
                    {
                        ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 100d ) );
                    }
                }
            }
            catch
            {
                // close to meet strong guarantees
                try
                {
                    CloseFile( fileNumber );
                }
                finally
                {
                }

                throw;
            }
            finally
            {
                ApplianceBase.DoEvents();
            }

            return fileNumber;
        }

        /// <summary> Closes a file. </summary>
        /// <param name="fileNumber"> The file number. </param>
        [System.Security.Permissions.FileIOPermission( System.Security.Permissions.SecurityAction.Demand, Unrestricted = true )]
        private static void CloseFile( int fileNumber )
        {
            // close the file if not closed
            if ( fileNumber != 0 )
                FileSystem.FileClose( fileNumber );
        }

        /// <summary> Query if this  is file exists. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <returns> True if file exists, false if not. </returns>
        public bool IsFileExists()
        {
            return new FileInfo( this.DefaultFullFileName ).Exists;
        }

        /// <summary> The delimiter. </summary>
        private const string _Delimiter = "|";

        /// <summary> Reads resources from file. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="filename"> The filename to read. </param>
        public void ReadResources( string filename )
        {
            this.Clear();
            string[] fields;
            int fileNumber = 0;
            var fi = new FileInfo( filename );
            // file has to be written before it is read.
            if ( !fi.Exists )
                return;
            try
            {
                fileNumber = OpenFile( filename, OpenMode.Input, OpenAccess.Read, OpenShare.LockWrite );
                using var reader = new Microsoft.VisualBasic.FileIO.TextFieldParser( filename );
                reader.TextFieldType = Microsoft.VisualBasic.FileIO.FieldType.Delimited;
                reader.Delimiters = new string[] { _Delimiter };
                bool isHeaderRow = true;
                while ( !reader.EndOfData )
                {
                    fields = reader.ReadFields();
                    if ( !isHeaderRow && fields is object && fields.Length > 0 )
                    {
                        this.Add( new ResourceNameInfo( fields ) );
                    }

                    isHeaderRow = false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                // close to meet strong guarantees
                try
                {
                    CloseFile( fileNumber );
                }
                finally
                {
                }
            }
        }

        /// <summary> Reads resources from default file. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        public void ReadResources()
        {
            this.ReadResources( this.DefaultFullFileName );
            var fi = new FileInfo( this.BackupFullFileName );
            if ( !fi.Exists )
                this.WriteResources( this.BackupFullFileName );
        }

        /// <summary> Restore resources. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        public void RestoreResources()
        {
            var fi = new FileInfo( this.BackupFullFileName );
            if ( !fi.Exists )
                this.WriteResources( this.BackupFullFileName );
            this.ReadResources( this.BackupFullFileName );
        }

        /// <summary> Writes resources to file. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="filename"> The filename to read. </param>
        public void WriteResources( string filename )
        {
            int fileNumber = 0;
            try
            {
                var fi = new FileInfo( filename );
                if ( !fi.Directory.Exists )
                {
                    fi.Directory.Create();
                }

                fileNumber = OpenFile( filename, OpenMode.Output, OpenAccess.Write, OpenShare.LockReadWrite );
                FileSystem.PrintLine( fileNumber, ResourceNameInfo.BuildHeaderRecord( _Delimiter ) );
                foreach ( ResourceNameInfo resourceNameInfo in this )
                    FileSystem.PrintLine( fileNumber, resourceNameInfo.BuildDataRecord( _Delimiter ) );
            }
            catch
            {
                throw;
            }
            finally
            {
                // close to meet strong guarantees
                try
                {
                    CloseFile( fileNumber );
                }
                finally
                {
                }
            }
        }

        /// <summary> Writes resources to default file. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        public void WriteResources()
        {
            this.WriteResources( this.DefaultFullFileName );
            var fi = new FileInfo( this.BackupFullFileName );
            if ( !fi.Exists )
                this.WriteResources( this.BackupFullFileName );
        }

        /// <summary> Backup resources. </summary>
        /// <remarks> David, 2020-06-07. </remarks>
        public void BackupResources()
        {
            this.WriteResources( this.BackupFullFileName );
        }

        /// <summary> Gets the default folder name. </summary>
        /// <value> The default folder name. </value>
        public string DefaultFolderName { get; set; }

        /// <summary> Gets the default file name. </summary>
        /// <value> The default file name. </value>
        public string DefaultFileName { get; set; }

        /// <summary> Gets the default full file name. </summary>
        /// <value> The default full file name. </value>
        public string DefaultFullFileName => Path.Combine( this.DefaultFolderName, this.DefaultFileName );

        /// <summary> Gets the filename of the backup file. </summary>
        /// <value> The filename of the backup file. </value>
        public string BackupFileName { get; set; }

        /// <summary> Gets the full filename of the backup file. </summary>
        /// <value> The full filename of the backup file. </value>
        public string BackupFullFileName => Path.Combine( this.DefaultFolderName, this.BackupFileName );

        #endregion

    }
}
