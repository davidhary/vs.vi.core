using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.VI.Pith
{

    /// <summary> An Integrated Scientific Resources-based Resource finder. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class IntegratedResourceFinder : ResourceFinderBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        public IntegratedResourceFinder() : base()
        {
        }

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
        public override ResourceNameInfo ParseResource( string resourceName )
        {
            return new ResourceNameInfo( resourceName );
        }

        /// <summary> Gets or sets the using like pattern. </summary>
        /// <value> The using like pattern. </value>
        public override bool UsingLikePattern { get; } = true;

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Fetches resource cache. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <returns> The resource cache. </returns>
        private static ResourceNameInfoCollection FetchResourceCache()
        {
            var resourceNameInfoCollection = new ResourceNameInfoCollection();
            resourceNameInfoCollection.ReadResources();
            return resourceNameInfoCollection;
        }

        /// <summary> Lists all resources in the resource names cache. </summary>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources()
        {
            return FetchResourceCache().Find( ResourceNamesManager.AllResourcesFilter );
        }

        /// <summary> Lists all resources in the resource names cache. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources( string filter )
        {
            try
            {
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for all interfaces in the resource names cache. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces()
        {
            string filter = string.Empty;
            try
            {
                filter = ResourceNamesManager.BuildInterfaceFilter();
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for the interfaces in the resource names cache. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces( string filter )
        {
            try
            {
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for the interfaces in the resource names cache. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) FindInterfaces( HardwareInterfaceType interfaceType )
        {
            string filter = string.Empty;
            try
            {
                filter = ResourceNamesManager.BuildInterfaceFilter( interfaceType );
                IEnumerable<string> resources = FetchResourceCache().Find( filter );
                return (resources.Any(), "Resources not found", resources);
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <remarks> David, 2020-04-10. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( string filter )
        {
            try
            {
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments()
        {
            string filter = string.Empty;
            try
            {
                filter = ResourceNamesManager.BuildInstrumentFilter();
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( HardwareInterfaceType interfaceType )
        {
            string filter = string.Empty;
            try
            {
                filter = ResourceNamesManager.BuildInstrumentFilter( interfaceType );
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        /// <summary> Searches for instruments in the resource names cache. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( HardwareInterfaceType interfaceType, int boardNumber )
        {
            string filter = string.Empty;
            try
            {
                filter = ResourceNamesManager.BuildInstrumentFilter( interfaceType, boardNumber );
                return FetchResourceCache().Find( filter );
            }
            catch ( Exception ex )
            {
                throw new Core.OperationFailedException( $"Failed finding resources using filter='{filter}'", ex );
            }
        }

        #endregion

    }
}
