namespace isr.VI.Pith
{

    /// <summary> A session factory base. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-29 </para>
    /// </remarks>
    public abstract class SessionFactoryBase
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        protected SessionFactoryBase() : base()
        {
        }

        /// <summary> Creates Gpib interface SessionBase. </summary>
        /// <returns> The new Gpib interface SessionBase. </returns>
        public abstract InterfaceSessionBase GpibInterfaceSession();

        /// <summary> Creates resources manager. </summary>
        /// <returns> The new resources manager. </returns>
        public abstract ResourcesProviderBase ResourcesProvider();

        /// <summary> Creates the SessionBase. </summary>
        /// <returns> The new SessionBase. </returns>
        public abstract SessionBase Session();
    }
}
