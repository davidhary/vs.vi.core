## ISR VI Pith<sub>&trade;</sub>: Virtual Instrument Essence Class Library
### Revision History

*7.3.7638 2020-11-29*  
Converted to C#. 

*7.3.7524 2020-08-07*  
Speeds up finding instruments. 
Fixes creating a session to a ICS-9065 inaccessible device. 

*7.3.7462 2020-06-06*  
Uses new resource name info in place or resource name 
parse info and add a resource name store.

*7.2.7411 2020-04-16*  
Using native IVI assembly installed using the Keysight 
IO Libraries because vendor code is required.

*7.2.7367 2020-03-03*  
Visa 19.5.

*7.2.7278 2019-12-05*  
Uses the new core framework. Separates implementation
of instruments clear, reset, initialize and preset from updating the
instrument representation in code.

*7.1.7257 2019-11-12*  
Visa 19. Renames service request enable commands.

*6.1.6937 2018-12-27*  
Commence version 6.1.

*6.0.6928 2018-12-20*  
Branch VI.6.0 node at Commit hash:
fa17a5d7cbf1b1641f4606bc474f5b5d54e177f9 parent
87fa0117a6\#gitext://gotocommit/87fa0117a63251513f60aadee87568afe63ab953.

*5.0.6897 2018-11-19*  
Adds a thread safe method for reading the status byte.

*5.0.6893 2018-11-15*  
Updates how service request handling is suspended and
resumed. Updates how service request handling enabling is set.

*5.0.6892 2018-11-14*  
Defaults to using Keep Alive attributes. Checks alive
on session open. Updates how termination character and timeouts are set.

*5.0.6842 2018-09-25*  
Changes how communication timeout is initialized and
updated.

*5.0.6765 2018-07-10*  
Updates to NI-VISA 2018.

*5.0.6657 2018-03-24*  
2018 release. Refactor for better model/view
separation.

*4.2.6633 2018-02-28*  
Adds payload base class and query and write commands
for a payload.

*4.2.6583 2018-01-09*  
Updates to 2018.

*4.0.6384 2017-06-24*  
Defaults to UTC time.

*4.0.6123 2016-10-06*  
Throws Format exceptions on failure to parse values
with information about the instrument resource and the invalid values.

*4.0.5968 2016-05-04*  
Fixes return value when parsing a boolean.

*4.0.5918 2016-03-15*  
Adds support for detecting orphan resources. Adds
support for keeping TCP/IP sessions alive.

*4.0.5868 2016-01-25*  
Adds dummy session. Allows creating a session without
a resource name.

*4.0.5803 2015-11-21 Created.

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
