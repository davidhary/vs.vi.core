using System;

namespace isr.VI.National.Visa.ExceptionExtensions
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, Pith.DeviceException exception )
        {
            if ( exception is object )
            {
                exception.AddExceptionData( value );
            }

            return exception is object;
        }

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, Pith.NativeException exception )
        {
            if ( exception is object && exception.InnerError is object )
            {
                exception.AddExceptionData( value );
            }

            return exception is object;
        }

        /// <summary> Adds an exception data to 'exception'. </summary>
        /// <param name="value">     The value. </param>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionData( Exception value, Ivi.Visa.NativeVisaException exception )
        {
            if ( exception is object )
            {
                if ( exception.ErrorCode > 0 )
                {
                    value.Data.Add( $"{value.Data.Count}-Warning", $"0x{exception.ErrorCode:X}" );
                }
                else if ( exception.ErrorCode < 0 )
                {
                    value.Data.Add( $"{value.Data.Count}-Error", $"-0x{-exception.ErrorCode:X}" );
                }
            }

            return exception is object;
        }

        /// <summary> Adds an exception data to 'Exception'. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool AddExceptionData( this Exception exception )
        {
            return AddExceptionData( exception, exception as Ivi.Visa.NativeVisaException ) || AddExceptionData( exception, exception as Pith.NativeException ) || AddExceptionData( exception, exception as Pith.DeviceException );
        }

        /// <summary> Adds an exception data. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionDataThis( Exception exception )
        {
            return exception.AddExceptionData() || Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        internal static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString( this Exception value, int level )
        {
            return Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( value, level, AddExceptionDataThis );
        }
    }
}
