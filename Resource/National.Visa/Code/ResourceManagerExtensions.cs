using System;
using System.Collections.Generic;
using System.Linq;

using isr.VI.National.Visa.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.National.Visa
{

    /// <summary> Resource Manager Extensions. </summary>
    /// <remarks> (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para></remarks>
    internal static class ResourceManagerExtensions
    {

        #region " CONVERSIONS "

        /// <summary> Converts the given value. </summary>
        /// <param name="value"> Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
        public static Pith.HardwareInterfaceType ConvertInterfaceType( Ivi.Visa.HardwareInterfaceType value )
        {
            return Enum.IsDefined( typeof( Pith.HardwareInterfaceType ), ( int ) value ) ? ( Pith.HardwareInterfaceType ) Conversions.ToInteger( ( int ) value ) : Pith.HardwareInterfaceType.Gpib;
        }

        /// <summary> Converts the given value. </summary>
        /// <param name="value"> Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <returns> A Ivi.Visa.HardwareInterfaceType. </returns>
        public static Ivi.Visa.HardwareInterfaceType ConvertInterfaceType( Pith.HardwareInterfaceType value )
        {
            return Enum.IsDefined( typeof( Ivi.Visa.HardwareInterfaceType ), ( int ) value ) ? ( Ivi.Visa.HardwareInterfaceType ) Conversions.ToInteger( ( int ) value ) : Ivi.Visa.HardwareInterfaceType.Gpib;
        }

        /// <summary> Build resource message. </summary>
        /// <param name="ex"> Details of the exception. </param>
        /// <returns> A an array with a single resource expressing the error. </returns>
        private static IEnumerable<string> BuildResourceMessage( Exception ex )
        {
            if ( ex.InnerException is Ivi.Visa.VisaException )
            {
                Ivi.Visa.NativeVisaException nativeEx = ( Ivi.Visa.NativeVisaException ) ex.InnerException;
                if ( nativeEx.ErrorCode == Ivi.Visa.NativeErrorCode.ResourceNotFound )
                {
                    // Insufficient location information or the device or resource is not present in the system.  
                    // VISA error code -1073807343 (0xBFFF0011), ErrorResourceNotFound
                    return new string[] { "Error: Resource Not Found" };
                }
                else
                {
                    return new string[] { $"{nativeEx.Message} code={nativeEx.ErrorCode}:{nativeEx.ErrorCode}" };
                }
            }
            else
            {
                return new string[] { ex.Message };
            }
        }

        /// <summary> Convert parse result. </summary>
        /// <param name="value"> Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <returns> The <see cref="VI.Pith.ResourceNameInfo"/>. </returns>
        public static Pith.ResourceNameInfo ConvertParseResult( Ivi.Visa.ParseResult value )
        {
            return new Pith.ResourceNameInfo( value.OriginalResourceName, ConvertInterfaceType( value.InterfaceType ), value.InterfaceNumber );
        }

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Try parse resource. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value">        Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>True</c> if parsed or false if failed parsing. </returns>
        public static Pith.ResourceNameInfo ParseResource( this NationalInstruments.Visa.ResourceManager value, string resourceName )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                return ConvertParseResult( value.Parse( resourceName ) );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, resourceName, "@DM", "Parsing resource" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value"> Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <returns> List of all resources. </returns>
        public static IEnumerable<string> FindResources( this NationalInstruments.Visa.ResourceManager value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            string resources = string.Empty;
            try
            {
                resources = Pith.ResourceNamesManager.AllResourcesFilter;
                return value.Find( Pith.ResourceNamesManager.AllResourcesFilter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, resources, "@DM", "Finding resources" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Tries to find resources. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">     Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if resources were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindResources( this NationalInstruments.Visa.ResourceManager value, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                return value.FindResources().Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Pith.NativeException ex )
            {
                resources = new string[] { ex.Message };
                return false;
            }
        }

        /// <summary> Tries to find resources. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">     Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="filter">    A pattern specifying the search. </param>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if resources were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindResources( this NationalInstruments.Visa.ResourceManager value, string filter, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                resources = value.Find( filter );
                return resources.Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                resources = new string[] { ex.ToFullBlownString() };
                return false;
            }
        }

        /// <summary> Returns true if the specified resource exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">        Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resourceName"> The resource name. </param>
        /// <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
        public static bool Exists( this NationalInstruments.Visa.ResourceManager value, string resourceName )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            if ( string.IsNullOrWhiteSpace( resourceName ) )
                throw new ArgumentNullException( nameof( resourceName ) );
            var resources = value.FindResources();
            return resources.Contains( resourceName, StringComparer.CurrentCultureIgnoreCase );
        }

        /// <summary> Searches for the interface. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">        Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resourceName"> The interface resource name. </param>
        /// <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
        public static bool InterfaceExists( this NationalInstruments.Visa.ResourceManager value, string resourceName )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            var resourceNameInfo = value.ParseResource( resourceName );
            if ( resourceNameInfo.IsParsed )
            {
                IEnumerable<string> resources = Array.Empty<string>();
                if ( value.TryFindInterfaces( resourceNameInfo.InterfaceType, ref resources ) )
                {
                    return resources.Contains( resourceName, StringComparer.CurrentCultureIgnoreCase );
                }
            }

            return false;
        }

        /// <summary> Searches for all interfaces. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value"> Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <returns> The found interface resource names. </returns>
        public static IEnumerable<string> FindInterfaces( this NationalInstruments.Visa.ResourceManager value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInterfaceFilter();
                return value.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding interfaces" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Tries to find interfaces. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">     Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if interfaces were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindInterfaces( this NationalInstruments.Visa.ResourceManager value, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                resources = value.FindInterfaces();
                return resources.Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Pith.NativeException ex )
            {
                resources = new string[] { ex.Message };
                return false;
            }
        }

        /// <summary> Searches for the interfaces. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value">         Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public static IEnumerable<string> FindInterfaces( this NationalInstruments.Visa.ResourceManager value, Pith.HardwareInterfaceType interfaceType )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInterfaceFilter( interfaceType );
                return value.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding interfaces" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Tries to find interfaces. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">         Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="resources">     [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if interfaces were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindInterfaces( this NationalInstruments.Visa.ResourceManager value, Pith.HardwareInterfaceType interfaceType, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                resources = value.FindInterfaces( interfaceType );
                return resources.Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Pith.NativeException ex )
            {
                resources = new string[] { ex.Message };
                return false;
            }
        }

        /// <summary> Searches for the instrument. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">        Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resourceName"> The instrument resource name. </param>
        /// <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
        public static bool InstrumentExists( this NationalInstruments.Visa.ResourceManager value, string resourceName )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            var result = value.ParseResource( resourceName );
            if ( result.IsParsed )
            {
                IEnumerable<string> resources = Array.Empty<string>();
                if ( value.TryFindInstruments( result.InterfaceType, result.InterfaceNumber, ref resources ) )
                {
                    return resources.Contains( resourceName, StringComparer.CurrentCultureIgnoreCase );
                }
            }

            return false;
        }

        /// <summary> Searches for instruments. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value"> Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <returns> The found instrument resource names. </returns>
        public static IEnumerable<string> FindInstruments( this NationalInstruments.Visa.ResourceManager value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInstrumentFilter();
                return value.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Tries to find instruments. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">     Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindInstruments( this NationalInstruments.Visa.ResourceManager value, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                resources = value.FindInstruments();
                return resources.Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Pith.NativeException ex )
            {
                resources = new string[] { ex.Message };
                return false;
            }
        }

        /// <summary> Searches for instruments. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value">         Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public static IEnumerable<string> FindInstruments( this NationalInstruments.Visa.ResourceManager value, Pith.HardwareInterfaceType interfaceType )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType );
                return value.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Tries to find instruments. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">         Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="resources">     [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindInstruments( this NationalInstruments.Visa.ResourceManager value, Pith.HardwareInterfaceType interfaceType, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                resources = value.FindInstruments( interfaceType );
                return resources.Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Pith.NativeException ex )
            {
                resources = new string[] { ex.Message };
                return false;
            }
        }

        /// <summary> Searches for instruments. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">       Thrown when a Native error condition occurs. </exception>
        /// <param name="value">         Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public static IEnumerable<string> FindInstruments( this NationalInstruments.Visa.ResourceManager value, Pith.HardwareInterfaceType interfaceType, int boardNumber )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            string filter = string.Empty;
            try
            {
                filter = Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType, boardNumber );
                return value.Find( filter );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                var nativeError = new NativeError( ex.ErrorCode, filter, "@DM", "Finding instruments" );
                throw new Pith.NativeException( nativeError, ex );
            }
        }

        /// <summary> Tries to find instruments. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value">           Reference to the
        /// <see cref="NationalInstruments.Visa.ResourceManager">resource
        /// manager</see>. </param>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
        /// <param name="resources">       [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public static bool TryFindInstruments( this NationalInstruments.Visa.ResourceManager value, Pith.HardwareInterfaceType interfaceType, int interfaceNumber, ref IEnumerable<string> resources )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            try
            {
                resources = value.FindInstruments( interfaceType, interfaceNumber );
                return resources.Any();
            }
            catch ( ArgumentException ex )
            {
                resources = BuildResourceMessage( ex );
                return false;
            }
            catch ( Pith.NativeException ex )
            {
                resources = new string[] { ex.Message };
                return false;
            }
        }

        #endregion

    }
}
