using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.VI.National.Visa.ExceptionExtensions;

namespace isr.VI.National.Visa
{

    /// <summary> The VIAA Resources Provider. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class ResourcesProvider : Pith.ResourcesProviderBase
    {

        #region " CONSTRUCTOR "

        /// <summary> Default constructor. </summary>
        public ResourcesProvider() : base()
        {
            this.VisaResourceManager = new NationalInstruments.Visa.ResourceManager();
        }

        #region "IDisposable Support"

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.VisaResourceManager is object )
                    {
                        this.VisaResourceManager.Dispose();
                        this.VisaResourceManager = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception occurred disposing resource manager", $"Exception {ex.ToFullBlownString()}" );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " RESOURCE MANAGER "

        /// <summary> Gets or sets the manager for resource. </summary>
        /// <value> The resource manager. </value>
        private NationalInstruments.Visa.ResourceManager VisaResourceManager { get; set; }

        /// <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public override bool IsDummy { get; } = false;

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A VI.ResourceNameInfo. </returns>
        public override Pith.ResourceNameInfo ParseResource( string resourceName )
        {
            return this.VisaResourceManager.ParseResource( resourceName );
        }

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources. </summary>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources()
        {
            return this.VisaResourceManager.FindResources();
        }

        /// <summary> Tries to find resources. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details) TryFindResources()
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindResources( ref resources ), "Resources not found");
        }

        /// <summary> Lists all resources. </summary>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns> List of all resources. </returns>
        public override IEnumerable<string> FindResources( string filter )
        {
            return this.VisaResourceManager.Find( filter );
        }

        /// <summary> Tries to find resources. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="filter"> A pattern specifying the search. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindResources( string filter )
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindResources( filter, ref resources ), "Resources not found", resources);
        }

        /// <summary> Returns true if the specified resource exists. </summary>
        /// <param name="resourceName"> The resource name. </param>
        /// <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
        public override bool Exists( string resourceName )
        {
            return this.VisaResourceManager.Exists( resourceName ) && !Pith.ResourceNamesManager.IsTcpipResource( resourceName ) || Pith.ResourceNamesManager.PingTcpipResource( resourceName );
        }

        #endregion

        #region " INTERFACES "

        /// <summary> Searches for the interface. </summary>
        /// <param name="resourceName"> The interface resource name. </param>
        /// <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
        public override bool InterfaceExists( string resourceName )
        {
            return this.VisaResourceManager.InterfaceExists( resourceName );
        }

        /// <summary> Searches for all interfaces. </summary>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces()
        {
            return this.VisaResourceManager.Find( Pith.ResourceNamesManager.BuildInterfaceFilter() );
        }

        /// <summary> Try find interfaces. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInterfaces()
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindInterfaces( ref resources ), "Resources not found", resources);
        }

        /// <summary> Searches for the interfaces. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public override IEnumerable<string> FindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            return this.VisaResourceManager.Find( Pith.ResourceNamesManager.BuildInterfaceFilter( interfaceType ) );
        }

        /// <summary> Try find interfaces. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindInterfaces( interfaceType, ref resources ), "Resources not found", resources);
        }

        #endregion

        #region " INSTRUMENTS  "

        /// <summary> Searches for the instrument. </summary>
        /// <param name="resourceName"> The instrument resource name. </param>
        /// <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
        public override bool FindInstrument( string resourceName )
        {
            return this.VisaResourceManager.InstrumentExists( resourceName );
        }

        /// <summary> Searches for instruments. </summary>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments()
        {
            return this.VisaResourceManager.Find( Pith.ResourceNamesManager.BuildInstrumentFilter() );
        }

        /// <summary> Tries to find instruments in the resource names cache. </summary>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments()
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindInstruments( ref resources ), "Resources not found", resources);
        }

        /// <summary> Searches for instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            return this.VisaResourceManager.Find( Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType ) );
        }

        /// <summary> Tries to find instruments. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindInstruments( interfaceType, ref resources ), "Resources not found", resources);
        }

        /// <summary> Searches for instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public override IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType, int boardNumber )
        {
            return this.VisaResourceManager.Find( Pith.ResourceNamesManager.BuildInstrumentFilter( interfaceType, boardNumber ) );
        }

        /// <summary> Tries to find instruments. </summary>
        /// <remarks> David, 2020-06-06. </remarks>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String, Resources As IEnumerable(Of String))
        /// </returns>
        public override (bool Success, string Details, IEnumerable<string> Resources) TryFindInstruments( Pith.HardwareInterfaceType interfaceType, int interfaceNumber )
        {
            IEnumerable<string> resources = new List<string>();
            return (this.VisaResourceManager.TryFindInstruments( interfaceType, interfaceNumber, ref resources ), "Resources not found", resources);
        }

        #endregion

        #region " VERSION VALIDATION "

        /// <summary>
        /// Validates the implementation and specification visa versions against settings values.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public override (bool Success, string Details) ValidateFunctionalVisaVersions()
        {
            return ResourceManager.ValidateFunctionalVisaVersions();
        }

        /// <summary> Validates the visa assembly versions against settings values. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public override (bool Success, string Details) ValidateVisaAssemblyVersions()
        {
            return ResourceManager.ValidateVisaAssemblyVersions();
        }

        #endregion

    }
}
