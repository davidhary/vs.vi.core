using System;
using System.Diagnostics;

using isr.VI.National.Visa.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.National.Visa
{

    /// <summary> A National Instrument message based session. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-20 </para>
    /// </remarks>
    public class Session : Pith.SessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the MessageBasedSession object from the specified resource name.
        /// </summary>
        public Session() : base()
        {
            // flags service request as not enabled.
            this._EnabledEventType = Ivi.Visa.EventType.Custom;
        }

        #region " Disposable Support"

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    try
                    {
                        this.TcpipSession = null;
                        this.DisposeSession();
                    }
                    catch ( Exception ex )
                    {
                        Debug.Assert( !Debugger.IsAttached, "Failed discarding enabled events.", $"Failed discarding enabled events. {ex.ToFullBlownString()}" );
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary> Gets or sets the sentinel indicating whether this is a dummy session. </summary>
        /// <value> The dummy sentinel. </value>
        public override bool IsDummy { get; } = false;

        /// <summary> The visa session. </summary>
        private NationalInstruments.Visa.MessageBasedSession _VisaSession;

        /// <summary> The visa session. </summary>
        /// <remarks>
        /// Must be defined without events; Otherwise, setting the timeout causes a memory exception.
        /// </remarks>
        /// <value> The visa session. </value>
        [CLSCompliant( false )]
        public NationalInstruments.Visa.MessageBasedSession VisaSession
        {
            get => this._VisaSession;

            set {
                this._VisaSession = value;
                this.TcpipSession = this.VisaSession as NationalInstruments.Visa.TcpipSession;
            }
        }

        /// <summary> Gets the type of the hardware interface. </summary>
        /// <value> The type of the hardware interface. </value>
        [CLSCompliant( false )]
        public Ivi.Visa.HardwareInterfaceType HardwareInterfaceType => this.VisaSession is null ? Ivi.Visa.HardwareInterfaceType.Custom : this.VisaSession.HardwareInterfaceType;

        /// <summary> Gets the TCP IP session. </summary>
        /// <value> The TCP IP session. </value>
        [CLSCompliant( false )]
        public NationalInstruments.Visa.TcpipSession TcpipSession { get; private set; }

        /// <summary>
        /// Gets the session open sentinel. When open, the session is capable of addressing the hardware.
        /// See also <see cref="P:VI.Pith.SessionBase.IsDeviceOpen" />.
        /// </summary>
        /// <value> The is session open. </value>
        public override bool IsSessionOpen => !(this.VisaSession is null || this.VisaSession.IsDisposed);

        /// <summary> Executes the session open action. </summary>
        /// <param name="resourceName">  Name of the resource. </param>
        /// <param name="resourceTitle"> The resource title. </param>
        protected override void OnSessionOpen( string resourceName, string resourceTitle )
        {
            // 6824: disable events if enabled. Apparently, the events are enabled, 
            // which causes issues trying to read STB to detect message available as VISA might be 
            // reading the STB byte already.
            this.VisaSession.DisableEvent( Ivi.Visa.EventType.ServiceRequest );
            // reads session defaults.
            base.OnSessionOpen( resourceName, resourceTitle );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="NationalInstruments.Visa.Session" /> class.
        /// </summary>
        /// <remarks>
        /// This method does not lock the resource. Rev 4.1 and 5.0 of VISA did not support this call and
        /// could not verify the resource.
        /// </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <exception cref="isr.VI.Pith.NativeException">          Thrown when a Native error condition occurs. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <param name="timeout">      The open timeout. </param>
        protected override void CreateSession( string resourceName, TimeSpan timeout )
        {
            try
            {
                this.ClearLastError();
                var result = Ivi.Visa.ResourceOpenStatus.Success;
                if ( this.Enabled )
                {
                    string activity = "creating visa session";
                    using ( var resMgr = new NationalInstruments.Visa.ResourceManager() )
                    {
                        result = Ivi.Visa.ResourceOpenStatus.Unknown;
                        this.VisaSession = ( NationalInstruments.Visa.MessageBasedSession ) resMgr.Open( resourceName, Ivi.Visa.AccessModes.None, ( int ) timeout.TotalMilliseconds, out result );
                    }

                    if ( result != Ivi.Visa.ResourceOpenStatus.Success )
                        throw new Core.OperationFailedException( $"Failed {activity}; '{result}';. {resourceName}" );
                    if ( this.VisaSession is null || this.VisaSession.IsDisposed )
                        throw new Core.OperationFailedException( $"Failed {activity};. {resourceName}" );
                    // sets the keep alive attribute
                    this.KeepAliveAttribute = this.ResourceNameInfo.UsingLanController;
                }
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.DisposeSession();
                this.LastNativeError = new NativeError( ex.ErrorCode, resourceName, "@opening", "opening session" );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            catch
            {
                this.DisposeSession();
                throw;
            }
        }

        /// <summary> Gets the sentinel indication that the VISA session is disposed. </summary>
        /// <value> The is session disposed. </value>
        public override bool IsSessionDisposed => this._VisaSession is null || this._VisaSession.IsDisposed;

        /// <summary>
        /// Disposes the VISA <see cref="T:isr.VI.Pith.SessionBase">Session</see> ending access to the
        /// instrument.
        /// </summary>
        protected override void DisposeSession()
        {
            if ( this.VisaSession is object && !this.VisaSession.IsDisposed )
            {
                this.TcpipSession = null;
                this._VisaSession.Dispose();
                this._VisaSession = null;
            }
        }

        /// <summary> Discards the session events. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        protected override void DiscardAllEvents()
        {
            if ( this.IsSessionOpen )
            {
                try
                {
                    this.VisaSession.DiscardEvents( Ivi.Visa.EventType.AllEnabled );
                }
                catch ( Ivi.Visa.NativeVisaException ex )
                {
                    this.LastNativeError = new NativeError( ex.ErrorCode, this.ResourceNameCaption, "@discarding", "discarding  all events" );
                    throw new Pith.NativeException( this.LastNativeError, ex );
                }
            }
        }

        /// <summary>
        /// Checks if the candidate resource name exists. If so, assign to the
        /// <see cref="VI.Pith.SessionBase.ValidatedResourceName">validated resource name</see>
        /// </summary>
        /// <returns> <c>true</c> if it the resource exists; otherwise <c>false</c> </returns>
        public override bool ValidateCandidateResourceName()
        {
            using var rm = new ResourcesProvider();
            return this.ValidateCandidateResourceName( rm );
        }

        /// <summary>
        /// Gets or sets the sentinel indicating if call backs are performed in a specific
        /// synchronization context.
        /// </summary>
        /// <remarks>
        /// For .NET Framework 2.0, use SynchronizeCallbacks to specify that the object marshals
        /// callbacks across threads appropriately.<para>
        /// DH: 3339 Setting true prevents display.
        /// </para><para>
        /// Note that setting to false also breaks display updates.
        /// </para>
        /// </remarks>
        /// <value>
        /// The sentinel indicating if call backs are performed in a specific synchronization context.
        /// </value>
        public override bool SynchronizeCallbacks
        {
            get {
                if ( this.IsSessionOpen )
                {
                    base.SynchronizeCallbacks = this.VisaSession.SynchronizeCallbacks;
                }

                return base.SynchronizeCallbacks;
            }

            set {
                base.SynchronizeCallbacks = value;
                if ( this.IsSessionOpen )
                {
                    this.VisaSession.SynchronizeCallbacks = value;
                }
            }
        }

        #endregion

        #region " ATTRIBUTE READ WRITE "

        /// <summary> Query if 'visaSession' 'Attribute' is supported. </summary>
        /// <param name="nativeVisaSession"> A native visa session. </param>
        /// <param name="attribute">         The attribute. </param>
        /// <returns> <c>true</c> if supported; otherwise <c>false</c> </returns>
        private static bool IsSupported( Ivi.Visa.INativeVisaSession nativeVisaSession, Ivi.Visa.NativeVisaAttribute attribute )
        {
            bool result = false;
            if ( nativeVisaSession is object )
            {
                result = true;
                if ( result && nativeVisaSession.HardwareInterfaceType != Ivi.Visa.HardwareInterfaceType.Gpib && attribute.ToString().StartsWith( Ivi.Visa.HardwareInterfaceType.Gpib.ToString(), StringComparison.OrdinalIgnoreCase ) )
                {
                    result = false;
                }

                if ( result && nativeVisaSession.HardwareInterfaceType != Ivi.Visa.HardwareInterfaceType.Pxi && attribute.ToString().StartsWith( Ivi.Visa.HardwareInterfaceType.Pxi.ToString(), StringComparison.OrdinalIgnoreCase ) )
                {
                    result = false;
                }

                if ( result && nativeVisaSession.HardwareInterfaceType != Ivi.Visa.HardwareInterfaceType.Serial && attribute.ToString().StartsWith( Ivi.Visa.HardwareInterfaceType.Serial.ToString(), StringComparison.OrdinalIgnoreCase ) )
                {
                    result = false;
                }

                if ( result && nativeVisaSession.HardwareInterfaceType != Ivi.Visa.HardwareInterfaceType.Tcp && attribute.ToString().StartsWith( Ivi.Visa.HardwareInterfaceType.Tcp.ToString(), StringComparison.OrdinalIgnoreCase ) )
                {
                    result = false;
                }

                if ( result && nativeVisaSession.HardwareInterfaceType != Ivi.Visa.HardwareInterfaceType.Usb && attribute.ToString().StartsWith( Ivi.Visa.HardwareInterfaceType.Usb.ToString(), StringComparison.OrdinalIgnoreCase ) )
                {
                    result = false;
                }

                if ( result && nativeVisaSession.HardwareInterfaceType != Ivi.Visa.HardwareInterfaceType.Vxi && attribute.ToString().StartsWith( Ivi.Visa.HardwareInterfaceType.Vxi.ToString(), StringComparison.OrdinalIgnoreCase ) )
                {
                    result = false;
                }
            }

            return result;
        }

        /// <summary> Gets read buffer size. </summary>
        /// <returns> The read buffer size. </returns>
        private int Get_ReadBufferSize()
        {
            return this.VisaSession is Ivi.Visa.INativeVisaSession s ? s.GetAttributeInt32( Ivi.Visa.NativeVisaAttribute.ReadBufferSize ) : 1024;
        }

        /// <summary> Reads an attribute. </summary>
        /// <param name="attribute">    The attribute. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The attribute. </returns>
        private bool ReadAttribute( Ivi.Visa.NativeVisaAttribute attribute, bool defaultValue )
        {
            return this.VisaSession is Ivi.Visa.INativeVisaSession s && IsSupported( s, attribute ) ? s.GetAttributeBoolean( attribute ) : defaultValue;
        }

        /// <summary> Writes an attribute. </summary>
        /// <param name="attribute"> The attribute. </param>
        /// <param name="value">     The value. </param>
        private void WriteAttribute( Ivi.Visa.NativeVisaAttribute attribute, bool value )
        {
            if ( this.VisaSession is Ivi.Visa.INativeVisaSession s && IsSupported( s, attribute ) )
            {
                s.SetAttributeBoolean( attribute, value );
            }
        }


        /// <summary> Reads an attribute. </summary>
        /// <param name="attribute">    The attribute. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The attribute. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private byte ReadAttribute( Ivi.Visa.NativeVisaAttribute attribute, byte defaultValue )
        {
            return this.VisaSession is Ivi.Visa.INativeVisaSession s && IsSupported( s, attribute ) ? s.GetAttributeByte( attribute ) : defaultValue;
        }

        /// <summary> Writes an attribute. </summary>
        /// <param name="attribute"> The attribute. </param>
        /// <param name="value">     The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Code Quality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void WriteAttribute( Ivi.Visa.NativeVisaAttribute attribute, byte value )
        {
            if ( this.VisaSession is Ivi.Visa.INativeVisaSession s && IsSupported( s, attribute ) )
            {
                s.SetAttributeByte( attribute, value );
            }
        }

        /// <summary> Reads an attribute. </summary>
        /// <param name="attribute">    The attribute. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The attribute. </returns>
        private int ReadAttribute( Ivi.Visa.NativeVisaAttribute attribute, int defaultValue )
        {
            return this.VisaSession is Ivi.Visa.INativeVisaSession s && IsSupported( s, attribute ) ? s.GetAttributeInt32( attribute ) : defaultValue;
        }

        /// <summary> Writes an attribute. </summary>
        /// <param name="attribute"> The attribute. </param>
        /// <param name="value">     The value. </param>
        private void WriteAttribute( Ivi.Visa.NativeVisaAttribute attribute, int value )
        {
            if ( this.VisaSession is Ivi.Visa.INativeVisaSession s && IsSupported( s, attribute ) )
            {
                s.SetAttributeInt32( attribute, value );
            }
        }

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Gets or sets the timeout attribute. </summary>
        /// <value> The timeout attribute. </value>
        public int TimeoutAttribute
        {
            get => this.ReadAttribute( Ivi.Visa.NativeVisaAttribute.TimeoutValue, 2000 );

            set => this.WriteAttribute( Ivi.Visa.NativeVisaAttribute.TimeoutValue, value );
        }

        /// <summary> Gets or sets the keep alive attribute. </summary>
        /// <value> The keep alive attribute. </value>
        public bool KeepAliveAttribute
        {
            get => this.ReadAttribute( Ivi.Visa.NativeVisaAttribute.TcpKeepAlive, false );

            set => this.WriteAttribute( Ivi.Visa.NativeVisaAttribute.TcpKeepAlive, value );
        }

        #endregion

        #region " READ/WRITE "

        /// <summary> Gets or sets the ASCII character used to end reading. </summary>
        /// <value> The termination character. </value>
        public override byte ReadTerminationCharacter
        {
            get => this.IsSessionOpen ? this.VisaSession.TerminationCharacter : Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineValue;

            set {
                if ( this.ReadTerminationCharacter != value )
                {
                    if ( this.IsSessionOpen )
                        this.VisaSession.TerminationCharacter = value;
                    if ( !(this.TcpipSession is null || this.TcpipSession.IsDisposed) )
                    {
                        this.TcpipSession.TerminationCharacter = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets or sets the termination character enabled specifying whether the read operation ends
        /// when a termination character is received.
        /// </summary>
        /// <value> The termination character enabled. </value>
        public override bool ReadTerminationCharacterEnabled
        {
            get => !this.IsSessionOpen || this.VisaSession.TerminationCharacterEnabled;

            set {
                if ( this.ReadTerminationCharacterEnabled != value )
                {
                    if ( this.IsSessionOpen )
                        this.VisaSession.TerminationCharacterEnabled = value;
                    if ( !(this.TcpipSession is null || this.TcpipSession.IsDisposed) )
                    {
                        this.TcpipSession.TerminationCharacterEnabled = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the timeout for I/O communication on this resource session. </summary>
        /// <value> The communication timeout. </value>
        public override TimeSpan CommunicationTimeout
        {
            get => this.IsSessionOpen ? TimeSpan.FromMilliseconds( this.VisaSession.TimeoutMilliseconds ) : TimeSpan.Zero;

            set {
                if ( this.CommunicationTimeout != value )
                {
                    if ( this.IsSessionOpen )
                        this.VisaSession.TimeoutMilliseconds = ( int ) value.TotalMilliseconds;
                    if ( !(this.TcpipSession is null || this.TcpipSession.IsDisposed) )
                    {
                        this.TcpipSession.TimeoutMilliseconds = ( int ) value.TotalMilliseconds;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Query if 'readStatus' is read ended. </summary>
        /// <param name="readStatus"> The read status. </param>
        /// <returns> <c>true</c> if read ended; otherwise <c>false</c> </returns>
        private static bool IsReadEnded( Ivi.Visa.ReadStatus readStatus )
        {
            return readStatus == Ivi.Visa.ReadStatus.EndReceived || readStatus == Ivi.Visa.ReadStatus.TerminationCharacterEncountered;
        }

        /// <summary>
        /// Synchronously reads ASCII-encoded string data irrespective of the buffer size.
        /// </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <returns> The received message. </returns>
        public override string ReadFreeLine()
        {
            var builder = new System.Text.StringBuilder();
            try
            {
                this.ClearLastError();
                var endReadstatus = Ivi.Visa.ReadStatus.Unknown;
                int bufferSize = this.Get_ReadBufferSize();
                if ( this.IsSessionOpen )
                {
                    bool hitEndRead = false;
                    do
                    {
                        string msg = this.VisaSession.RawIO.ReadString( bufferSize, out endReadstatus );
                        hitEndRead = IsReadEnded( endReadstatus );
                        _ = builder.Append( msg );
                        Core.ApplianceBase.DoEvents();
                    }
                    while ( !hitEndRead );
                    this.LastMessageReceived = builder.ToString();
                }
                else
                {
                    this.LastMessageReceived = this.EmulatedReply;
                }

                return builder.ToString();
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, this.LastMessageSent, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastMessageSent, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            catch ( Ivi.Visa.IOTimeoutException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( Ivi.Visa.NativeErrorCode.Timeout, this.OpenResourceName, this.LastNodeNumber.Value, this.LastMessageSent, this.LastAction ) : new NativeError( Ivi.Visa.NativeErrorCode.Timeout, this.OpenResourceName, this.LastMessageSent, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            finally
            {
                // must clear the reply after each reading otherwise could get cross information.
                this.EmulatedReply = string.Empty;
                this.RestartKeepAliveTimer();
            }
        }

        /// <summary>
        /// Synchronously reads ASCII-encoded string data. Reads up to the
        /// <see cref="ReadTerminationCharacter">termination character</see>.
        /// </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <returns> The received message. </returns>
        public override string ReadFiniteLine()
        {
            try
            {
                this.ClearLastError();
                this.LastMessageReceived = this.IsSessionOpen ? this.VisaSession.RawIO.ReadString() : this.EmulatedReply;
                // must clear the reply after each reading otherwise could get cross information.
                this.EmulatedReply = string.Empty;
                return this.LastMessageReceived;
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, this.LastMessageSent, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastMessageSent, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            catch ( Ivi.Visa.IOTimeoutException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( Ivi.Visa.NativeErrorCode.Timeout, this.OpenResourceName, this.LastNodeNumber.Value, this.LastMessageSent, this.LastAction ) : new NativeError( Ivi.Visa.NativeErrorCode.Timeout, this.OpenResourceName, this.LastMessageSent, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            finally
            {
                this.RestartKeepAliveTimer();
            }
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
        /// data with the <see cref="ReadTerminationCharacter">termination character</see>.
        /// </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        protected override string SyncWriteLine( string dataToWrite )
        {
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                try
                {
                    this.ClearLastError();
                    if ( this.IsSessionOpen )
                    {
                        this.VisaSession.FormattedIO.WriteLine( dataToWrite );
                    }

                    this.LastMessageSent = dataToWrite;
                }
                catch ( Ivi.Visa.NativeVisaException ex )
                {
                    this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, dataToWrite, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, dataToWrite, this.LastAction );
                    throw new Pith.NativeException( this.LastNativeError, ex );
                }
                finally
                {
                    this.RestartKeepAliveTimer();
                }
            }

            return dataToWrite;
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.<para>
        /// Per IVI documentation: Converts the specified string to an ASCII string and appends it to the
        /// formatted I/O write buffer</para>
        /// </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> A String. </returns>
        protected override string SyncWrite( string dataToWrite )
        {
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                try
                {
                    this.ClearLastError();
                    if ( this.IsSessionOpen )
                    {
                        this.VisaSession.RawIO.Write( dataToWrite );
                    }

                    this.LastMessageSent = dataToWrite;
                }
                catch ( Ivi.Visa.NativeVisaException ex )
                {
                    this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, dataToWrite, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, dataToWrite, this.LastAction );
                    throw new Pith.NativeException( this.LastNativeError, ex );
                }
                finally
                {
                    this.RestartKeepAliveTimer();
                }
            }

            return dataToWrite;
        }

        /// <summary> Size of the input buffer. </summary>
        private int _InputBufferSize;

        /// <summary> Gets the size of the input buffer. </summary>
        /// <value> The size of the input buffer. </value>
        public override int InputBufferSize
        {
            get {
                if ( this._InputBufferSize == 0 )
                    this._InputBufferSize = this.Get_ReadBufferSize();
                return this._InputBufferSize;
            }
        }

        #endregion

        #region " REGISTERS "

        /// <summary> Reads status byte. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        /// <returns> The status byte. </returns>
        protected override Pith.ServiceRequests ThreadUnsafeReadStatusByte()
        {
            try
            {
                this.ClearLastError();
                var value = this.EmulatedStatusByte;
                this.EmulatedStatusByte = 0;
                if ( this.IsSessionOpen )
                {
                    value = ( Pith.ServiceRequests ) Conversions.ToInteger( this.VisaSession.ReadStatusByte() );
                }

                this.StatusByte = value;
                return this.StatusByte;
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, "@STB", this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, "@STB", this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            finally
            {
                this.RestartKeepAliveTimer();
            }
        }

        /// <summary> Clears the device. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        protected override void Clear()
        {
            try
            {
                this.ClearLastError();
                if ( this.IsSessionOpen )
                    this.VisaSession.Clear();
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, "@DCL", this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, "@DCL", this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            finally
            {
                this.RestartKeepAliveTimer();
            }
        }

        /// <summary> Clears the device (SDC). </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        protected override void ClearDevice()
        {
            this.Clear();
            if ( this.SupportsClearInterface && this.Enabled )
            {
                using var gi = new GpibInterfaceSession();
                gi.OpenSession( this.ResourceNameInfo.InterfaceResourceName );
                if ( gi.IsOpen )
                {
                    gi.SelectiveDeviceClear( this.VisaSession.ResourceName );
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed opening GPIB Interface Session {this.ResourceNameInfo.InterfaceResourceName}" );
                }
            }
        }

        #endregion

        #region " EVENTS "

        #region " SUSPEND / RESUME SRQ "

        /// <summary> Resume service request handing. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        public override void ResumeServiceRequestHanding()
        {
            this.LastAction = "Resuming service request";
            string lastMessage = string.Empty;
            try
            {
                base.ResumeServiceRequestHanding();
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, lastMessage, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, lastMessage, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
        }

        /// <summary> Suspends the service request handling. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        public override void SuspendServiceRequestHanding()
        {
            this.LastAction = "Suspending service request";
            string lastMessage = string.Empty;
            try
            {
                base.SuspendServiceRequestHanding();
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, lastMessage, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, lastMessage, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
        }

        #endregion

        /// <summary> Visa session service request. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Message based session event information. </param>
        private void OnServiceRequested( object sender, Ivi.Visa.VisaEventArgs e )
        {
            if ( sender is object && e is object && Ivi.Visa.EventType.ServiceRequest == e.EventType )
            {
                if ( this.ServiceRequestHandlingSuspended )
                {
                    this.ServiceRequestType = e.EventType.ToString();
                    this.SuspendedServiceRequestedCount += 1;
                }
                else
                {
                    this.OnServiceRequested( EventArgs.Empty );
                }
            }
        }

        /// <summary> Type of the enabled event. </summary>
        private Ivi.Visa.EventType _EnabledEventType;

        /// <summary>
        /// Gets or sets or set (protected) the sentinel indication if a service request event handler
        /// was enabled and registered.
        /// </summary>
        /// <value>
        /// <c>True</c> if service request event is enabled and registered; otherwise, <c>False</c>.
        /// </value>
        public override bool ServiceRequestEventEnabled
        {
            get => Ivi.Visa.EventType.ServiceRequest == this._EnabledEventType;

            set {
                if ( this.ServiceRequestEventEnabled != value )
                {
                    this._EnabledEventType = value ? Ivi.Visa.EventType.ServiceRequest : Ivi.Visa.EventType.Custom;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Enables and adds the service request event handler. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        public override void EnableServiceRequestEventHandler()
        {
            string lastMessage = string.Empty;
            this.LastAction = "Enabling service request";
            try
            {
                this.ClearLastError();
                if ( !this.ServiceRequestEventEnabled )
                {
                    if ( this.IsSessionOpen )
                    {
                        // must define the handler before enabling the events.
                        // Firewall access must be granted to VXI instrument for service request handling;
                        // With Windows 1909, both public and private network check box access must be checked.
                        lastMessage = "add SRQ handler";
                        this.VisaSession.ServiceRequest += this.OnServiceRequested;
                        // Enabling the VISA session events causes an exception of unsupported mechanism.
                        // Apparently, the service request event is enabled when adding the event handler.
                        // verified using NI Trace. The NI trace shows
                        // viEnableEvent (TCPIP0::192.168.0.144::inst0::INSTR (0x00000001), 0x3FFF200B (VI_EVENT_SERVICE_REQ), 2 (0x2), 0 (0x0))
                        // as success following with the same command as failure.
                        // Note that enabling causes the unsupported mechanism exception.
                        // Removed per above note: Me.VisaSession.EnableEvent(Ivi.Visa.EventType.ServiceRequest)
                    }
                    // this turns on the enabled sentinel
                    lastMessage = "turning on service request enabled";
                    this.ServiceRequestEventEnabled = true;
                }
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, lastMessage, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, lastMessage, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
        }

        /// <summary> Disables and removes the service request event handler. </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        public override void DisableServiceRequestEventHandler()
        {
            string lastMessage = string.Empty;
            this.LastAction = "Disabling service request";
            try
            {
                this.ClearLastError();
                if ( this.ServiceRequestEventEnabled )
                {
                    if ( this.IsSessionOpen )
                    {
                        lastMessage = "discard events";
                        this.DiscardServiceRequests();
                        // Apparently, the service request event is enabled when removing the event handler.
                        // Note that disabling twice does not cause an exception.
                        // Removed per above note: Me.VisaSession.DisableEvent(Ivi.Visa.EventType.ServiceRequest)
                        if ( this.HardwareInterfaceType == Ivi.Visa.HardwareInterfaceType.Tcp )
                        {
                            lastMessage = "remove TCP/IP SRQ handler";
                            this.TcpipSession.ServiceRequest -= this.OnServiceRequested;
                        }
                        else
                        {
                            lastMessage = "SRQ handler";
                            this.VisaSession.ServiceRequest -= this.OnServiceRequested;
                        }
                    }
                    // this turns off the enabled sentinel
                    lastMessage = "turning off service request enabled";
                    this.ServiceRequestEventEnabled = false;
                }
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, lastMessage, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, lastMessage, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
        }

        /// <summary> Discard pending service requests. </summary>
        public override void DiscardServiceRequests()
        {
            if ( this.IsSessionOpen )
                this.VisaSession.DiscardEvents( Ivi.Visa.EventType.ServiceRequest );
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the 'trigger' command. </summary>
        /// <value> The 'trigger' command. </value>
        private string TriggerCommand { get; set; } = "*TRG";

        /// <summary>
        /// Asserts a software or hardware trigger depending on the interface; Sends a bus trigger.
        /// </summary>
        /// <exception cref="isr.VI.Pith.NativeException"> Thrown when a Native error condition occurs. </exception>
        public override void AssertTrigger()
        {
            try
            {
                this.ClearLastError();
                if ( this.IsSessionOpen )
                    this.VisaSession.AssertTrigger();
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                this.LastNativeError = this.LastNodeNumber.HasValue ? new NativeError( ex.ErrorCode, this.OpenResourceName, this.LastNodeNumber.Value, this.TriggerCommand, this.LastAction ) : new NativeError( ex.ErrorCode, this.OpenResourceName, this.TriggerCommand, this.LastAction );
                throw new Pith.NativeException( this.LastNativeError, ex );
            }
            finally
            {
                this.RestartKeepAliveTimer();
            }
        }


        #endregion

        #region " INTERFACE "

        /// <summary> Keeps the TCP/IP interface alive. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="tcpipSession"> The TCP IP session. </param>
        /// <returns> A TimeSpan. </returns>
        [CLSCompliant( false )]
        public static TimeSpan KeepInterfaceAlive( NationalInstruments.Visa.TcpipSession tcpipSession )
        {
            if ( tcpipSession is null )
                throw new ArgumentNullException( nameof( tcpipSession ) );
            var sw = Stopwatch.StartNew();
            using ( var gi = new NationalInstruments.Visa.TcpipSocket( $"tcpip0::{tcpipSession.Address}::{tcpipSession.Port}::socket" ) )
            {
                if ( gi is object && !gi.IsDisposed )
                {
                    if ( gi.ResourceLockState == Ivi.Visa.ResourceLockState.NoLock )
                        gi.LockResource( 1 );
                    gi.UnlockResource();
                }
            }

            return sw.Elapsed;
        }

        /// <summary> Clears the interface. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        protected override void ImplementClearHardwareInterface()
        {
            if ( this.SupportsClearInterface && this.Enabled )
            {
                using var gi = new GpibInterfaceSession();
                gi.OpenSession( this.ResourceNameInfo.InterfaceResourceName );
                if ( gi.IsOpen )
                {
                    gi.SelectiveDeviceClear( this.VisaSession.ResourceName );
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed opening GPIB Interface Session {this.ResourceNameInfo.InterfaceResourceName}" );
                }
            }
        }


        #endregion

        #region " KEEP ALIVE "

        /// <summary>   Keep alive. </summary>
        /// <remarks>   David, 2021-06-21. </remarks>
        public override void KeepAlive()
        {
            throw new NotImplementedException();
        }

        #endregion


    }
}
