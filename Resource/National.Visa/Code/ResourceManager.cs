using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.VI.Foundation;

namespace isr.VI.National.Visa
{

    /// <summary> A VISA Resource Manager. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-10, 3.0.5001.x. </para>
    /// </remarks>
    public class ResourceManager : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        public ResourceManager() : base()
        {
            this.VisaResourceManager = new NationalInstruments.Visa.ResourceManager();
        }

        #region " IDisposable Support "

        /// <summary> True if disposed. </summary>
        private bool _Disposed; // To detect redundant calls


        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this._Disposed && disposing )
                {
                    if ( this.VisaResourceManager is object )
                    {
                        this.VisaResourceManager.Dispose();
                        this.VisaResourceManager = null;
                    }
                }
            }
            finally
            {
                this._Disposed = true;
            }
        }

        /// <summary> Finalizes this object. </summary>
        /// <remarks>
        /// David, 2015-11-23. Override Finalize() only if Dispose(disposing As Boolean) above has code
        /// to free unmanaged resources.
        /// </remarks>
        ~ResourceManager()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( false );
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            // uncommented the following line because Finalize() is overridden above.
            GC.SuppressFinalize( this );
        }
        #endregion

        #endregion

        #region " RESOURCE MANAGER "

        /// <summary> Gets the manager for resource. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <value> The resource manager. </value>
        private NationalInstruments.Visa.ResourceManager VisaResourceManager { get; set; }

        #endregion

        #region " PARSE RESOURCES "

        /// <summary> Parse resource. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A VI.ResourceNameInfo. </returns>
        public Pith.ResourceNameInfo ParseResource( string resourceName )
        {
            string activity = $"parsing resource {resourceName}";
            try
            {
                return this.VisaResourceManager.ParseResource( resourceName );
            }
            catch ( MissingMethodException ex )
            {
                throw new Core.OperationFailedException( $"Failed {activity}; Please verify using IVI.Visa version {My.Settings.Default.FoundationVisaAssemblyVersion} with NI VISA {My.Settings.Default.FoundationSystemFileVersion64}", ex );
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region " FIND RESOURCES "

        /// <summary> Lists all resources. </summary>
        /// <returns> List of all resources. </returns>
        public IEnumerable<string> FindResources()
        {
            return this.VisaResourceManager.FindResources();
        }

        /// <summary> Tries to find resources. </summary>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if resources were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindResources( ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindResources( ref resources );
        }

        /// <summary> Tries to find resources. </summary>
        /// <param name="filter">    A pattern specifying the search. </param>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if resources were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindResources( string filter, ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindResources( filter, ref resources );
        }

        /// <summary> Returns true if the specified resource exists. </summary>
        /// <param name="resourceName"> The resource name. </param>
        /// <returns> <c>True</c> if the resource was located; Otherwise, <c>False</c>. </returns>
        public bool Exists( string resourceName )
        {
            return this.VisaResourceManager.Exists( resourceName );
        }

        /// <summary> Searches for the interface. </summary>
        /// <param name="resourceName"> The interface resource name. </param>
        /// <returns> <c>True</c> if the interface was located; Otherwise, <c>False</c>. </returns>
        public bool InterfaceExists( string resourceName )
        {
            return this.VisaResourceManager.InterfaceExists( resourceName );
        }

        /// <summary> Searches for all interfaces. </summary>
        /// <returns> The found interface resource names. </returns>
        public IEnumerable<string> FindInterfaces()
        {
            return this.VisaResourceManager.FindInterfaces();
        }

        /// <summary> Tries to find interfaces. </summary>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if interfaces were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindInterfaces( ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindInterfaces( ref resources );
        }

        /// <summary> Searches for the interfaces. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found interface resource names. </returns>
        public IEnumerable<string> FindInterfaces( Pith.HardwareInterfaceType interfaceType )
        {
            return this.VisaResourceManager.FindInterfaces( interfaceType );
        }

        /// <summary> Tries to find interfaces. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="resources">     [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if interfaces were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindInterfaces( Pith.HardwareInterfaceType interfaceType, ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindInterfaces( interfaceType, ref resources );
        }

        /// <summary> Searches for the instrument. </summary>
        /// <param name="resourceName"> The instrument resource name. </param>
        /// <returns> <c>True</c> if the instrument was located; Otherwise, <c>False</c>. </returns>
        public bool FindInstrument( string resourceName )
        {
            return this.VisaResourceManager.InstrumentExists( resourceName );
        }

        /// <summary> Searches for instruments. </summary>
        /// <returns> The found instrument resource names. </returns>
        public IEnumerable<string> FindInstruments()
        {
            return this.VisaResourceManager.FindInstruments();
        }

        /// <summary> Tries to find instruments. </summary>
        /// <param name="resources"> [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindInstruments( ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindInstruments( ref resources );
        }

        /// <summary> Searches for instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> The found instrument resource names. </returns>
        public IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType )
        {
            return this.VisaResourceManager.FindInstruments( interfaceType );
        }

        /// <summary> Tries to find instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="resources">     [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindInstruments( Pith.HardwareInterfaceType interfaceType, ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindInstruments( interfaceType, ref resources );
        }

        /// <summary> Searches for instruments. </summary>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <param name="boardNumber">   The board number. </param>
        /// <returns> The found instrument resource names. </returns>
        public IEnumerable<string> FindInstruments( Pith.HardwareInterfaceType interfaceType, int boardNumber )
        {
            return this.VisaResourceManager.FindInstruments( interfaceType, boardNumber );
        }

        /// <summary> Tries to find instruments. </summary>
        /// <param name="interfaceType">   Type of the interface. </param>
        /// <param name="interfaceNumber"> The interface number (e.g., board or port number). </param>
        /// <param name="resources">       [in,out] The resources. </param>
        /// <returns>
        /// <c>True</c> if instruments were located or false if failed or no instrument resources were
        /// located. If exception occurred, the exception details are returned in the first element of the
        /// <paramref name="resources"/>.
        /// </returns>
        public bool TryFindInstruments( Pith.HardwareInterfaceType interfaceType, int interfaceNumber, ref IEnumerable<string> resources )
        {
            return this.VisaResourceManager.TryFindInstruments( interfaceType, interfaceNumber, ref resources );
        }

        #endregion

        #region " REGISTERY VISA VERSION "

        /// <summary> Gets the full pathname of the vendor version file. </summary>
        /// <value> The full pathname of the vendor version file. </value>
        public static string VendorVersionPath { get; set; } = @"SOFTWARE\National Instruments\NI-VISA\";

        /// <summary> Gets the name of the vendor version key. </summary>
        /// <value> The name of the vendor version key. </value>
        public static string VendorVersionKeyName { get; set; } = "CurrentVersion";

        /// <summary> Reads vendor version. </summary>
        /// <returns> The vendor version. </returns>
        public static Version ReadVendorVersion()
        {
            string version = Core.MachineInfo.ReadRegistry( Microsoft.Win32.RegistryHive.LocalMachine, VendorVersionPath, VendorVersionKeyName, "0.0.0.0" );
            return new Version( version );
        }

        /// <summary> Validates the vendor version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="expectedVersion"> The expected version. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVendorVersion( string expectedVersion )
        {
            string actualVersion = ReadVendorVersion().ToString();
            return Pith.ResourcesProviderBase.AreEqual( expectedVersion, actualVersion ) ? (true, string.Empty) : (false, $"Vendor {VendorVersionPath} version {actualVersion} different from expected {expectedVersion}");
        }

        /// <summary> Validates the National Instruments vendor version. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVendorVersion()
        {
            return ValidateVendorVersion( ExpectedVendorVersion );
        }

        /// <summary> Gets the expected vendor version. </summary>
        /// <value> The expected vendor version. </value>
        public static string ExpectedVendorVersion => My.Settings.Default.NationalRegisteredVisaVersion;

        /// <summary>
        /// Attempts to validate visa vendor and assembly versions using settings versions.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateVisaAssemblyVersions()
        {
            var result = ValidateVendorVersion();
            if ( result.Success )
                result = VisaVersionValidator.ValidateVisaAssemblyVersions();
            return result;
        }

        /// <summary>
        /// Validates the implementation specification visa versions against settings values.
        /// </summary>
        /// <remarks> David, 2020-04-12. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateFunctionalVisaVersions()
        {
            return string.Equals( ImplementationVersion.ToString(), My.Settings.Default.ImplementationVersion ) ? string.Equals( ImplementationVersion.ToString(), My.Settings.Default.ImplementationVersion ) ? (true, string.Empty) : (false, $"Expected specification version {My.Settings.Default.SpecificationVersion} different from actual {SpecificationVersion}") : (false, $"Expected implementation version {My.Settings.Default.ImplementationVersion} different from actual {ImplementationVersion}");
        }

        /// <summary> Gets the implementation version. </summary>
        /// <value> The implementation version. </value>
        public static Version ImplementationVersion
        {
            get {
                using var rm = new NationalInstruments.Visa.ResourceManager();
                return rm.ImplementationVersion;
            }
        }

        /// <summary> Gets the specification version. </summary>
        /// <value> The specification version. </value>
        public static Version SpecificationVersion
        {
            get {
                using var rm = new NationalInstruments.Visa.ResourceManager();
                return rm.SpecificationVersion;
            }
        }

        #endregion

    }
}
