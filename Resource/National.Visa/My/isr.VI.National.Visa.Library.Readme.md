## ISR VI National Visa<sub>&trade;</sub>: Virtual Instrument Class Library using NI VISA
### Revision History

*7.3.7639 2020-11-30*  
Converted to C#. 

*7.7207 2019-09-25*  
Visa Session: Adds check for hardware supported
attribute; Ignores unsupported attributes on read or write. Tested with
Keep Alive attribute on USB connected instrument.

*5.0.6893 2018-11-15*  
Updates how service request handling is suspended and
resumed. Updates how service request handling enabling is set. Disposes
the internal sessions when disposing the VI session.

*5.0.6892 2018-11-14*  
Defaults to using Keep Alive attributes. Checks alive
on session open. Updates how termination character and timeouts are set.

*4.0.5918 2016-03-15*  
Adds support for detecting orphan resources. Adds
support for keeping TCP/IP sessions alive.

*4.0.5868 2016-01-25*  
Allows creating a session without a resource name.

*4.0.5803 2015-11-21 Created.

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
