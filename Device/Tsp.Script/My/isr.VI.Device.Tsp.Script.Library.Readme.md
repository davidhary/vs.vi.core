## ISR VI Tsp<sub>&trade;</sub>: Test Script Processor Class Library

* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*7.3.7643 2020-12-04*  
Converted to C3.

*4.0.5803 2015-11-21 New structures and name spaces.

*3.0.5226 2014-05-23*  
Uses new stack trace parser. Fixes the checking for
visa and device errors.

*3.0.5181 2014-03-09*  
Uses abstract properties to define instrument
commands.

*3.0.4871 2013-05-03*  
Amend commit notes..

*3.0.4870 2013-05-02*  
Add Tsp SCPI Instrument to support message based
interface.

*3.0.4710 2012-11-23*  
Removes .VB tags from assemblies.

*3.0.4696 2012-11-09*  
Allows deletion of non-outdated scripts.

*3.0.4694 2012-11-07*  
Replaces GPIB instrument with SCPI Instrument to
support USB and Ethernet.

*3.0.4693 2012-11-06*  
Fixes dispose code to always dispose the parent class.
Extends model mask to the 2600B. Spell checked. Supports 2600B GPIB.

*3.0.4504 2012-05-01*  
Adds x86 project.

*3.0.4498 2012-04-17*  
Upgraded to VS2010. Implements code analysis rules for
.NET 4.0.

*3.0.4232 2011-08-03*  
Standardize code elements and documentation.

*3.0.4213 2011-07-15*  
Simplifies the assembly information.

*3.0.4056 2011-02-08*  
Uses debugger attached to control debug messages.

*3.0.4049 2011-01-01*  
Adds contact check.

*3.0.4006 2010-12-20*  
Allows using controllers on nodes other than node 1.

*3.0.3576 2009-10-16*  
Keeps leading spaces when fetching source \-- fixes
loading scripts to some 2600 instruments.

*3.0.3548 2009-09-18*  
Correctly reports when scripts are up to date nor
requiring deletion. Reads firmware version when enumerating nodes.

*3.0.3539 2009-09-09*  
Ignores TSP reset errors to allow using a single
instrument. Updates scripts loading to register the script short name.

*3.0.3508 2009-08-09*  
Adds script source and file formats to use when
writing the script file. Determines reading source files based on the
version prefix.

*3.0.3498 2009-07-30*  
Adds saving and loading binary scripts on non-A
instruments. Adds reading and writing scripts in compressed format.

*3.0.3463 2009-06-25*  
Removes application settings.

*3.0.3458 2009-06-20*  
Expands TSP Instrument for script management and more.

*3.0.3342 2009-02-23*  
Adds a TSP instrument that works outside the status
system.

*3.0.3082 2008-06-09*  
Fixes bug reading user scripts \-- returned value was
not used when returned success from function.

*3.0.2961 2008-02-09*  
Upgrades code to using Managed VISA and ISR's VISA.

*2.0.2925 2008-01-04*  
Upgrades code to .NET 3.5.

*1.0.2853 2007-10-24*  
Updates error raising code in methods with resume next
clauses.

*1.0.2685 2007-05-09*  
Turns off prompts and errors before disconnecting.

*1.0.2616 2007-03-01*  
Created.

\(C\) 2007 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  
Firmware for the Thermal Transient Meter instrument was developed and
tested using Eclipse ® from the [Eclipse Foundation](http://www.eclipse.org).

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)  
[table.copy](http://www.loop.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)  
[Working LUA base-64 codec](http://www.it-rfc.de)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
