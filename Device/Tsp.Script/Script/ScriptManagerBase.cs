using System;
using System.Collections.Generic;
using System.Diagnostics;
using isr.Core.StackTraceExtensions;
using Microsoft.VisualBasic.CompilerServices;
using isr.VI.Tsp.Script.ExceptionExtensions;

namespace isr.VI.Tsp.Script
{

    /// <summary> Loads and runs TSP scripts. </summary>
    /// <remarks>
    /// (c) 2007 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2007-03-12, 1.15.2627.x. </para>
    /// </remarks>
    public abstract class ScriptManagerBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ScriptManagerBase" /> class. </summary>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected ScriptManagerBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.LastFetchedSavedScripts = string.Empty;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this._NewProgramRequired = string.Empty;
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.FirmwareExists = new bool?();
            this._SupportfirmwareExists = new bool?();
            this.LastFetchedSavedScripts = string.Empty;
            this.LastFetchedSavedRemoteScripts = string.Empty;
            this.LastFetchScriptSource = string.Empty;
        }

        #endregion

        #region " SESSION / STATUS SUBSYSTEM "

        /// <summary> Gets the display subsystem. </summary>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <value> The display subsystem. </value>
        public DisplaySubsystemBase DisplaySubsystem { get; set; }

        /// <summary> Gets the link subsystem. </summary>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <value> The link subsystem. </value>
        public LinkSubsystemBase LinkSubsystem { get; set; }

        /// <summary> Gets the interactive subsystem. </summary>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <value> The interactive subsystem. </value>
        public LocalNodeSubsystemBase InteractiveSubsystem { get; set; }

        #endregion

        #region " IDENTITY "

        /// <summary> The new program required. </summary>
        private string _NewProgramRequired;

        /// <summary>
        /// Gets the message indicating that a new program is required for the instrument because this
        /// instrument is not included in the instrument list.
        /// </summary>
        /// <value> The new program required. </value>
        public string NewProgramRequired
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._NewProgramRequired ) )
                {
                    this._NewProgramRequired = "A new version of the program is required;. for instrument {0}";
                    this._NewProgramRequired = string.Format( System.Globalization.CultureInfo.CurrentCulture, this._NewProgramRequired, this.StatusSubsystem.Identity );
                }

                return this._NewProgramRequired;
            }
        }

        #endregion

        #region " NAME AND FILE PATH "

        /// <summary>Gets or sets the script AutoLoadEnabled.
        /// </summary>
        private bool _AutoLoadEnabled;

        /// <summary> Gets or sets the auto load enabled sentinel. </summary>
        /// <value> The automatic load enabled. </value>
        public bool AutoLoadEnabled
        {
            get => this._AutoLoadEnabled;

            set {
                if ( !value.Equals( this.AutoLoadEnabled ) )
                {
                    this._AutoLoadEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the script name. </summary>
        /// <value> The name. </value>
        public string Name { get; private set; }

        /// <summary> Script name setter. </summary>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="value"> Specifies the script name. </param>
        public void ScriptNameSetter( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
                value = string.Empty;
            if ( !string.Equals( value, this.Name, StringComparison.OrdinalIgnoreCase ) )
            {
                if ( ScriptEntityBase.IsValidScriptName( value ) )
                {
                    this.Name = value;
                    this.NotifyPropertyChanged( nameof( this.Name ) );
                }
                else
                {
                    // now report the error to the calling module
                    throw new System.IO.IOException( string.Format( System.Globalization.CultureInfo.CurrentCulture, "Invalid file name'. value '{0}' must not include any of these characters '{1}'.", value, Syntax.Constants.IllegalScriptNameCharacters ) );
                }
            }
        }

        /// <summary> Full pathname of the file. </summary>
        private string _FilePath;

        /// <summary> Gets or sets the script file name. </summary>
        /// <value> The full pathname of the file. </value>
        public string FilePath
        {
            get => this._FilePath;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.FilePath, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._FilePath = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " USER SCRIPTS "

        /// <summary> Stores user script names. </summary>
        private string[] _UserScriptNames;

        /// <summary> Returns the list of user script names. </summary>
        /// <returns> the list of user script names. </returns>
        public string[] UserScriptNames()
        {
            return this._UserScriptNames;
        }

        /// <summary> Gets all users scripts from the instrument. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <exception cref="TimeoutException">         Thrown when a Timeout error condition occurs. </exception>
        /// <returns> The user script names. </returns>
        private int FetchUserScriptNamesThis()
        {
            string activity = $"{this.ResourceNameCaption} fetching scripts";
            // load the function which to execute and get its name.
            string functionName;
            functionName = this.LoadPrintUserScriptNames();
            if ( string.IsNullOrWhiteSpace( functionName ) )
            {

                // now report the error to the calling module
                if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
                {
                    // this is called withing the device error query command. Me.StatusSubsystem.QueryStandardEventStatus()
                    if ( this.StatusSubsystem.HasDeviceError )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity};. Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity};. Failed fetching device errors" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity};. no device errors" );
                }
            }

            // Disable automatic display of errors - leave error messages in queue and enable error Prompt.
            _ = this.InteractiveSubsystem.WriteShowErrors( false );

            // Turn off prompts
            _ = this.InteractiveSubsystem.WriteShowPrompts( false );
            activity = $"{this.ResourceNameCaption} executing {functionName} fetching user script names";
            // run the function
            char delimiter = ',';
            TspExecutionState callState;
            this.StatusSubsystem.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, activity );
            Syntax.Lua.CallFunction( this.Session, functionName, "'" + delimiter + "'" );

            // wait till we get a reply from the instrument or timeout.
            var sw = Stopwatch.StartNew();
            var timeout = TimeSpan.FromMilliseconds( 2000d );
            do
                System.Threading.Thread.Sleep( 50 );
            while ( !(sw.Elapsed > timeout | this.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 1d ), 3 )) );
            if ( !this.StatusSubsystem.MessageAvailable )
            {
                throw new TimeoutException( $"Timeout waiting for {activity}" );
            }

            // read the names
            string names = this.Session.ReadLine();
            if ( string.IsNullOrWhiteSpace( names ) )
            {
            }

            // check if return value is an error a prompt. This will happened if the call is bad.
            else if ( (names ?? "") == Syntax.Lua.NilValue )
            {

                // read the next value, which should be true
                string expectedValue = "1";
                string value = this.Session.ReadLineTrimEnd();
                if ( string.IsNullOrWhiteSpace( value ) || !value.Trim().StartsWith( expectedValue, StringComparison.OrdinalIgnoreCase ) )
                {
                    Debug.Assert( !Debugger.IsAttached, "Expected True (1)", "Expected True {0} got {1}", expectedValue, value );
                }

                // read execution state explicitly, because session events are disabled.
                _ = this.InteractiveSubsystem.ReadExecutionState();
            }
            else
            {

                // parse execution state explicitly, because session events are disabled.
                callState = this.InteractiveSubsystem.ParseExecutionState( names, TspExecutionState.Unknown );
                if ( TspExecutionState.IdleError == callState )
                {

                    // now report the error to the calling module
                    if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
                    {
                        // down inside the device error query: Me.StatusSubsystem.QueryStandardEventStatus()
                        // this is called withing the device error query command. Me.StatusSubsystem.QueryStandardEventStatus()
                        if ( this.StatusSubsystem.HasDeviceError )
                        {
                            throw new Core.OperationFailedException( $"Failed {activity};. Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                        }
                        else
                        {
                            throw new Core.OperationFailedException( $"Failed {activity};. Failed fetching device errors" );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity};. No device errors" );
                    }
                }

                // check if return value is false.  This will happen if the function failed.
                else if ( (names.Substring( 0, 4 ) ?? "") == Syntax.Lua.FalseValue )
                {
                    activity = $"{activity} w/ argument {names.Split( delimiter )[1]}";
                    // if failure, get the failure message
                    if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
                    {
                        // done inside the query Me.StatusSubsystem.QueryStandardEventStatus()
                        var e = new Core.ActionEventArgs();
                        if ( this.StatusSubsystem.HasDeviceError )
                        {
                            throw new Core.OperationFailedException( $"Failed {activity};. Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                        }
                        else
                        {
                            throw new Core.OperationFailedException( $"Failed {activity};. Failed fetching device errors because {e.Details}" );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity};. No device errors" );
                    }
                }
                else
                {

                    // split the return values
                    this._UserScriptNames = names.Split( delimiter );

                    // read the next value, which should be true
                    string expectedValue = "1";
                    string value = this.Session.ReadLineTrimEnd();
                    if ( string.IsNullOrWhiteSpace( value ) || !(value.StartsWith( Syntax.Lua.TrueValue, StringComparison.OrdinalIgnoreCase ) || value.Trim().StartsWith( expectedValue, StringComparison.OrdinalIgnoreCase )) )
                    {
                        _ = this.PublishWarning( $"Expected {expectedValue} or {Syntax.Lua.TrueValue} but received {value}" );
                    }

                    if ( !this.InteractiveSubsystem.ProcessExecutionStateEnabled )
                    {
                        // read execution state explicitly, because session events are disabled.
                        _ = this.InteractiveSubsystem.ReadExecutionState();
                    }
                }
            }

            // return <c>True</c> if we got one or more.
            return this.UserScriptNames().Length;
        }

        /// <summary> Gets all users scripts from the instrument. </summary>
        /// <returns> The number of user script names that were fetched. </returns>
        public int FetchUserScriptNames()
        {

            // store state of prompts and errors.
            this.InteractiveSubsystem.StoreStatus();
            this.StatusSubsystem.Session.LastNodeNumber = new int?();
            bool processExecutionStateWasEnabled = this.InteractiveSubsystem.ProcessExecutionStateEnabled;
            // disable session events.
            this.InteractiveSubsystem.ProcessExecutionStateEnabled = false;
            try
            {
                return this.FetchUserScriptNamesThis();
            }
            catch
            {

                // remove any remaining values.
                this.Session.DiscardUnreadData( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 100d ) );
                throw;
            }
            finally
            {

                // restore state of prompts and errors.
                this.InteractiveSubsystem.RestoreStatus();

                // add a wait to ensure the system returns the last status.
                System.Threading.Thread.Sleep( 100 );

                // flush the buffer until empty to completely reading the status.
                var bits = this.Session.MeasurementEventBit;
                this.Session.MeasurementEventBit = this.StatusSubsystem.MessageAvailableBit;
                this.Session.DiscardUnreadData();
                this.Session.MeasurementEventBit = bits;
                this.InteractiveSubsystem.ProcessExecutionStateEnabled = processExecutionStateWasEnabled;
            }
        }

        /// <summary>
        /// Loads the 'printUserScriptNames' function and return the function name. The function is
        /// loaded only if it does not exists already.
        /// </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <returns> The function name. </returns>
        public string LoadPrintUserScriptNames()
        {

            // store state of prompts and errors.
            this.InteractiveSubsystem.StoreStatus();
            bool processExecutionStateWasEnabled = this.InteractiveSubsystem.ProcessExecutionStateEnabled;
            this.InteractiveSubsystem.ProcessExecutionStateEnabled = false;
            try
            {
                string functionName;
                functionName = "printUserScriptNames";
                string functionCode;
                if ( this.Session.IsGlobalExists( functionName ) )
                {
                    return functionName;
                }
                else
                {
                    functionCode = " function printUserScriptNames( delimiter ) local scripts = nil for i,v in _G.pairs(_G.script.user.scripts) do if scripts == nil then scripts = i else scripts = scripts .. delimiter .. i end end _G.print (scripts) _G.waitcomplete() end ";

                    // load the function
                    _ = this.Session.WriteLine( functionCode );
                    return functionName;
                }
            }
            catch ( Exception ex )
            {

                // remove any remaining values.
                this.Session.DiscardUnreadData();
                throw new Core.OperationFailedException( ex, "Failed loading print user script names functions. Discarded: {0}.", this.Session.DiscardedData );
            }
            finally
            {

                // restore state of prompts and errors.
                this.InteractiveSubsystem.RestoreStatus();
                this.InteractiveSubsystem.ProcessExecutionStateEnabled = processExecutionStateWasEnabled;
            }
        }

        #endregion

        #region " BINARY SCRIPTS "

        /// <summary> Loads the binary script function for creating binary scripts. </summary>
        /// <remarks>
        /// Non-A instruments requires having a special function for creating the binary scripts.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="node"> Specifies the node. </param>
        private void LoadBinaryScriptsFunction( NodeEntityBase node )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            string activity = $"{this.ResourceNameCaption} loading Binary Scripts resource to node {node.Number}";
            const string scriptName = "CreateBinaries";
            var script = new ScriptEntity( scriptName, NodeEntityBase.ModelFamilyMask( InstrumentModelFamily.K2600 ) ) { Source = Core.EmbeddedResourceManager.TryReadEmbeddedTextResource( System.Reflection.Assembly.GetExecutingAssembly(), "BinaryScripts.tsp" ) };
            this.LoadRunUserScript( script, node );
            if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
            {
                // done inside the query Me.StatusSubsystem.QueryStandardEventStatus()
                if ( this.StatusSubsystem.HasDeviceError )
                {
                    throw new Core.OperationFailedException( $"Failed {activity};. Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity};. Failed fetching device errors" );
                }
            }
        }

        /// <summary> Checks if the script is Binary. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name"> Specifies the name of the script. </param>
        /// <param name="node"> Specifies the node entity. </param>
        /// <returns> <c>True</c> if the script is a binary script; otherwise, <c>False&gt;</c>. </returns>
        public bool? IsBinaryScript( string name, NodeEntityBase node )
        {
            return node is null
                ? throw new ArgumentNullException( nameof( node ) )
                : ( bool? ) (node.IsController ? this.Session.IsStatementTrue( "_G.isr.script.isBinary({0})", name ) : this.Session.IsStatementTrue( "_G.isr.script.isBinary({0},{1})", name, node.Number ));
        }

        /// <summary> Converts the script to binary format. </summary>
        /// <remarks> Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="name"> Specifies the script name. </param>
        /// <param name="node"> Specifies the node entity. </param>
        public void ConvertBinaryScript( string name, NodeEntityBase node )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( string.IsNullOrWhiteSpace( name ) )
            {
                return;
            }
            else if ( this.IsBinaryScript( name, node ) == true )
            {
                return;
            }

            this.Session.LastNodeNumber = node.Number;
            if ( node.InstrumentModelFamily == InstrumentModelFamily.K2600A || node.InstrumentModelFamily == InstrumentModelFamily.K3700 )
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Nilifying {0};. ", name );
                string message = this.Session.ExecuteCommand( node.Number, "{0}.source = nil waitcomplete()", name );
                this.CheckThrowDeviceException( false, message );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Waiting completion;. " );
                this.LinkSubsystem.WaitComplete( node.Number, this.SaveTimeout, false );
                this.CheckThrowDeviceException( false, "clearing script '{0}' using '{1}';. ", name, message );
            }
            else
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Creating binary script{0};. ", name );

                // non-A instruments require having a special function for creating the binary scripts.
                this.LoadBinaryScriptsFunction( node );
                string message = this.Session.ExecuteCommand( node.Number, "CreateBinaryScript('{0}', {1}) waitcomplete()", name, name );
                this.CheckThrowDeviceException( false, message );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Waiting completion;. " );
                this.LinkSubsystem.WaitComplete( node.Number, this.SaveTimeout, false );
                this.CheckThrowDeviceException( false, "creating binary script '{0}' using '{1}';. ", name, message );
                string fullName = "script.user.scripts." + name;
                if ( this.Session.WaitNotNil( node.Number, fullName, this._SaveTimeout ) )
                {
                    this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "assigning binary script name to '{0}';. ", name );
                    message = this.Session.ExecuteCommand( node.Number, "{0} = nil waitcomplete() {0} = {1} waitcomplete()", name, fullName );
                    this.LinkSubsystem.WaitComplete( node.Number, this.SaveTimeout, false );
                    this.CheckThrowDeviceException( false, "assigning binary script name to '{0}' using '{1}';. ", name, message );
                    if ( this.Session.WaitNotNil( node.Number, name, this._SaveTimeout ) )
                    {
                        if ( !this.IsBinaryScript( name, node ) == true )
                        {
                            throw new Core.OperationFailedException( "{0} failed creating binary script '{1}';. reference to script '{2}' on node {3}--new script not found on the remote node.{4}{5}", this.ResourceNameCaption, name, fullName, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( "{0} failed creating script '{1}';. reference to script '{2}' on node {3}--new script not found on the remote node.{4}{5}", this.ResourceNameCaption, name, fullName, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( "{0} failed creating script '{1}' using '{2}' on node {3}--new script not found on the remote node.{4}{5}", this.ResourceNameCaption, fullName, name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }
            }
        }

        /// <summary> Saves the specifies script to non-volatile memory. </summary>
        /// <remarks> Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">           Specifies the script name. </param>
        /// <param name="node">           Specifies the node entity. </param>
        /// <param name="isSaveAsBinary"> Specifies the condition requesting clearing the source before
        /// saving. </param>
        /// <param name="isBootScript">   Specifies the condition indicating if this is a boot script. </param>
        /// <returns> <c>True</c> if the script was saved. </returns>
        public bool SaveScript( string name, NodeEntityBase node, bool isSaveAsBinary, bool isBootScript )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );

            // saving just the script name does not work if script was created and name assigned.
            string commandFormat = "script.user.scripts.{0}.save() waitcomplete()";
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "saving script '{0} on node {1};. ", name, node.Number );
            this.Session.LastNodeNumber = node.Number;
            if ( isSaveAsBinary )
            {
                this.ConvertBinaryScript( name, node );
            }

            if ( isBootScript )
            {
                commandFormat = "script.user.scripts.{0}.autorun = 'yes' " + commandFormat;
            }

            if ( node.IsController )
            {
                this.Session.EnableWaitComplete();
                _ = this.Session.WriteLine( commandFormat, name );
                _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( this.SaveTimeout ).Status );
            }
            else
            {
                string message = this.Session.ExecuteCommand( node.Number, commandFormat, name );
                this.CheckThrowDeviceException( false, message );
                this.LinkSubsystem.WaitComplete( node.Number, this.SaveTimeout, false );
                this.CheckThrowDeviceException( false, "saving script '{0}' using '{1}';. ", name, message );
            }

            return this.SavedScriptExists( name, node, true );
        }

        #endregion

        #region " SCRIPT SOURCE "

        /// <summary> Clears the <see cref="Name">specified</see> script source. </summary>
        public void ClearScriptSource()
        {
            this.ClearScriptSource( this.Name );
        }

        /// <summary> Clears the <paramref name="value">specified</paramref> script source. </summary>
        /// <param name="value"> Specifies the script name. </param>
        public void ClearScriptSource( string value )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                _ = this.Session.WriteLine( "{0}.source = nil ", value );
            }
        }

        /// <summary> Fetches the <paramref name="name">specified</paramref> script source. </summary>
        /// <remarks>
        /// Requires setting the correct bits for message available bits. No longer trimming spaces as
        /// this caused failure to load script to the 2602.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name"> Specifies the script name. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void FetchScriptSource( string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            this.LastFetchScriptSource = string.Empty;
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "fetching script source;. " );
            _ = this.Session.WriteLine( "isr.script.list({0})", name );
            System.Threading.Thread.Sleep( 500 );
            this.LastFetchScriptSource = this.Session.ReadLines( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 400d ), true );

            // trap non-empty buffer.
            try
            {
                if ( (this.Session.ReadStatusRegister() & Pith.ServiceRequests.MessageAvailable) != 0 )
                {
                    Debug.Assert( !Debugger.IsAttached, "Buffer Not empty" );
                }

                _ = NodeEntityBase.NodeExists( this.Session, 1 );
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Buffer Not empty {ex.ToFullBlownString()}" );
            }

            this.CheckThrowDeviceException( true, "reading source;. last sent: '{0}'; last read: '{1}'", this.Session.LastMessageSent, this.Session.LastMessageReceived );
        }

        /// <summary> Fetches the <paramref name="name">specified</paramref> script source. </summary>
        /// <remarks> Requires setting the proper message available bits for the session. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> Specifies the subsystem node. </param>
        /// <param name="name">       Specifies the script name. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void FetchScriptSource( int nodeNumber, string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            this.LastFetchScriptSource = string.Empty;

            // clear data queue and report if not empty.
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "clearing data queue" );
            this.LinkSubsystem.ClearDataQueue( nodeNumber );
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "listing script source" );
            _ = this.Session.WriteLine( "isr.script.list({0},{1})", name, nodeNumber );
            System.Threading.Thread.Sleep( 500 );
            this.LastFetchScriptSource = this.Session.ReadLines( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 400d ), true );

            // trap non-empty buffer.
            try
            {
                if ( (this.Session.ReadStatusRegister() & Pith.ServiceRequests.MessageAvailable) != 0 )
                {
                    Debug.Assert( !Debugger.IsAttached, "Buffer not empty" );
                }

                _ = NodeEntityBase.NodeExists( this.Session, 1 );
            }
            catch
            {
                Debug.Assert( !Debugger.IsAttached, "Buffer not empty" );
            }

            this.CheckThrowDeviceException( true, "reading source;. last sent: '{0}'.", this.Session.LastMessageSent );
        }

        /// <summary> Fetches the script source. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="script"> Specifies the script. </param>
        /// <param name="node">   Specifies the node. </param>
        public void FetchScriptSource( ScriptEntityBase script, NodeEntityBase node )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( node.IsController )
            {
                this.FetchScriptSource( script.Name );
            }
            else
            {
                this.FetchScriptSource( node.Number, script.Name );
            }
        }

        /// <summary> Lists the <paramref name="value">specified</paramref> script. </summary>
        /// <param name="value">      Specifies the script name. </param>
        /// <param name="trimSpaces"> Specifies a directive to trim leading and trailing spaces from each
        /// line. </param>
        /// <returns> The script source. </returns>
        public string FetchUserScriptSource( string value, bool trimSpaces )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                _ = this.Session.WriteLine( "print({0}.source)", value );
                return this.Session.ReadLines( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 400d ), trimSpaces, !trimSpaces );
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary> Check if a user script was saved as a binary script. </summary>
        /// <param name="name"> Gets or sets the script name. </param>
        /// <returns> <c>True</c> if binary; otherwise, <c>False</c>. </returns>
        public bool IsScriptSavedAsBinary( string name )
        {
            this.Session.MakeTrueFalseReplyIfEmpty( true );
            return this.Session.IsStatementTrue( "string.sub({0}.source,1,24) == 'loadstring(table.concat('", name );
        }

        /// <summary> Gets the last source fetched from the instrument. </summary>
        /// <value> The last fetch script source. </value>
        public string LastFetchScriptSource { get; private set; }

        /// <summary> Runs the named script. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <exception cref="TimeoutException">         Thrown when a Timeout error condition occurs. </exception>
        /// <param name="timeout"> Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        public void RunScript( TimeSpan timeout )
        {

            // store state of prompts and errors.
            this.InteractiveSubsystem.StoreStatus();
            try
            {

                // Disable automatic display of errors - leave error messages in queue and enable error Prompt.
                _ = this.InteractiveSubsystem.WriteShowErrors( false );

                // Turn off prompts
                _ = this.InteractiveSubsystem.WriteShowPrompts( false );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "running script '{0}';. ", this.Name );
                this.Session.LastNodeNumber = new int?();
                string returnedValue = "1";
                _ = this.Session.WriteLine( "{0}.run() waitcomplete() print('{1}') ", this.Name, returnedValue );
                // wait till we get a reply from the instrument or timeout.
                if ( this.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 40d ), timeout ) )
                {
                    string value = this.Session.ReadLineTrimEnd();
                    if ( string.IsNullOrWhiteSpace( value ) || !value.Trim().StartsWith( returnedValue, StringComparison.OrdinalIgnoreCase ) )
                    {
                        if ( string.IsNullOrWhiteSpace( value ) )
                        {
                            throw new Core.OperationFailedException( "Script '{0}' failed;. script returned no value.", this.Name );
                        }
                        else
                        {
                            throw new Core.OperationFailedException( "Script '{0}' failed;. returned value '{1}' instead of the expected '{2}'.", this.Name, value, returnedValue );
                        }
                    }
                }
                else
                {
                    throw new TimeoutException( "Timeout waiting operation completion running the script '" + this.Name + "'" );
                }
            }
            catch
            {

                // remove any remaining values.
                this.Session.DiscardUnreadData();
                throw;
            }
            finally
            {

                // restore state of prompts and errors.
                this.InteractiveSubsystem.RestoreStatus();

                // add a wait to ensure the system returns the last status.
                System.Threading.Thread.Sleep( 100 );

                // flush the buffer until empty to completely reading the status.
                this.Session.DiscardUnreadData();
            }
        }

        #endregion

        #region " NILIFY SCRIPT "

        /// <summary> The nilify timeout. </summary>
        private TimeSpan _NilifyTimeout = TimeSpan.Zero;

        /// <summary> Gets or sets the time out for nilifying a script. </summary>
        /// <value> The Nilify timeout. </value>
        public TimeSpan NilifyTimeout
        {
            get => this._NilifyTimeout;

            set {
                if ( this._NilifyTimeout == value )
                {
                    this._NilifyTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Makes a script nil. Return <c>True</c> if the script is nil. </summary>
        /// <remarks> Assumes the script is known to exist. Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="name"> Specifies the script name. </param>
        private void NilifyScriptThis( string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            this.Session.EnableServiceRequestWaitComplete();
            _ = this.Session.WriteLine( "{0} = nil waitcomplete()", name );
            this.CheckThrowDeviceException( true, "nilifying script;. using the command '{0}'", this.Session.LastMessageSent );
            if ( this.NilifyTimeout > TimeSpan.Zero )
            {
                _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( this.NilifyTimeout ).Status );
            }

            if ( !this.Session.IsNil( name ) )
            {
                throw new Core.OperationFailedException( "Instrument '{0}' script {1} still exists after nil.", this.ResourceNameCaption, name );
            }
        }

        /// <summary> Makes a script nil. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name"> Specifies the script name. </param>
        public void NilifyScript( string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            if ( !this.Session.IsNil( name ) )
            {
                this.NilifyScriptThis( name );
            }
        }

        /// <summary>
        /// Makes a script nil and returns <c>True</c> if the script was nilified. Does not check if the
        /// script exists.
        /// </summary>
        /// <remarks> Assumes the script is known to exist. Waits till completion. </remarks>
        /// <param name="nodeNumber"> Specifies the remote node number. </param>
        /// <param name="name">       Specifies the script name. </param>
        /// <returns> <c>True</c> if the script is nil; otherwise <c>False</c>. </returns>
        private bool NilifyScriptThis( int nodeNumber, string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
            {
                return false;
            }

            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "nilifying script '{0} on node {1};. ", name, nodeNumber );
            this.Session.LastNodeNumber = nodeNumber;

            // ignore errors as failure is handled below.
            string message = this.Session.ExecuteCommand( nodeNumber, "{0} = nil", name );
            this.CheckThrowDeviceException( false, message );
            this.LinkSubsystem.WaitComplete( nodeNumber, this.NilifyTimeout, false );
            this.CheckThrowDeviceException( false, "nilifying script '{0}' using '{1}';. ", name, message );

            // allow the next command to create an error if wait complete times out.
            bool affirmative = this.Session.IsNil( nodeNumber, name );
            this.Session.LastNodeNumber = new int?();
            return affirmative;
        }

        /// <summary> Makes a script nil and returns <c>True</c> if the script was nullified. </summary>
        /// <param name="nodeNumber"> Specifies the remote node number. </param>
        /// <param name="name">       Specifies the script name. </param>
        /// <returns> <c>True</c> if the script is nil; otherwise <c>False</c>. </returns>
        public bool NilifyScript( int nodeNumber, string name )
        {
            return !string.IsNullOrWhiteSpace( name ) && (this.Session.IsNil( nodeNumber, name ) || this.NilifyScriptThis( nodeNumber, name ));
        }

        #endregion

        #region " DELETE SCRIPTS "

        /// <summary> Removes the script from the device. Updates the script list. </summary>
        /// <param name="scriptName"> Name of the script. </param>
        public void RemoveScript( string scriptName )
        {
            this.DeleteScript( scriptName, true );
            if ( this.StatusSubsystem.CollectGarbageWaitComplete( this.DeleteTimeout, "deleting script '{0}';. ", scriptName ) )
            {
                _ = this.FetchUserScriptNames();
            }
        }

        /// <summary>
        /// Deletes the <paramref name="name">specified</paramref> script. Also nilifies the script if
        /// delete command worked.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        public void DeleteScript( string name, bool refreshScriptCatalog )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            if ( this.SavedScriptExists( name, refreshScriptCatalog ) )
            {
                this.DeleteSavedScript( name );
            }
            else
            {
                this.NilifyScript( name );
            }
        }

        /// <summary>
        /// Deletes the <paramref name="name">specified</paramref> saved script. Also nilifies the script
        /// if delete command worked. Returns <c>True</c> if the script was deleted.
        /// </summary>
        /// <remarks> Assumes the script is known to exist. Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="name"> Specifies the script name. </param>
        public void DeleteSavedScript( string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Enabling wait completion;. " );
            this.Session.EnableServiceRequestWaitComplete();
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Deleting script;. " );
            _ = this.Session.WriteLine( "script.delete('{0}') waitcomplete()", name );

            // make script nil
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Awaiting operation completion;. " );
            if ( this.DeleteTimeout > TimeSpan.Zero )
            {
                _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( this.DeleteTimeout ).Status );
            }

            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Deleting script;. " );
            this.NilifyScript( name );

            // make sure to re-check that script is gone.
            if ( this.SavedScriptExists( name, true ) )
            {
                throw new Core.OperationFailedException( "Instrument '{0}' script {1} still exists after nil.", this.ResourceNameCaption, name );
            }
        }

        /// <summary>
        /// Deletes the <paramref name="name">specified</paramref> saved script. Also nilifies the script
        /// if delete command worked.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        public void DeleteSavedScript( string name, bool refreshScriptCatalog )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            if ( this.SavedScriptExists( name, refreshScriptCatalog ) )
            {
                this.DeleteSavedScript( name );
            }
        }

        /// <summary>
        /// Deletes the <paramref name="name">specified</paramref> script. Also nilifies the script if
        /// delete command worked.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">                 Specifies the node entity. </param>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        /// <returns> <c>True</c> if the script is nil; otherwise <c>False</c>. </returns>
        public bool DeleteScript( NodeEntityBase node, string name, bool refreshScriptCatalog )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                return false;
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( node.IsController )
            {
                this.DeleteScript( name, refreshScriptCatalog );
                return true;
            }
            else
            {
                return this.SavedScriptExists( node, name, refreshScriptCatalog ) ? this.DeleteSavedScript( node, name ) : this.NilifyScript( node.Number, name );
            }
        }

        /// <summary>
        /// Deletes the <paramref name="name">specified</paramref> saved script. Also nilifies the script
        /// if delete command worked. Then checks if the script was deleted and if so returns true.
        /// Otherwise, returns false.
        /// </summary>
        /// <remarks> Presumes the saved script exists. Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node entity. </param>
        /// <param name="name"> Specifies the script name. </param>
        /// <returns> <c>True</c> if the script is nil; otherwise <c>False</c>. </returns>
        public bool DeleteSavedScript( NodeEntityBase node, string name )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( string.IsNullOrWhiteSpace( name ) )
                return false;
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Deleting script '{0}';. ", name );
            this.Session.LastNodeNumber = node.Number;
            // failure is handled below.
            string message = this.Session.ExecuteCommand( node.Number, "script.delete('{0}')", name );
            this.CheckThrowDeviceException( false, message );
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Wait complete;. " );
            this.LinkSubsystem.WaitComplete( node.Number, this.DeleteTimeout, false );
            this.CheckThrowDeviceException( false, "deleting script '{0}' using '{1}';. ", name, message );

            // make script nil
            bool affirmative;
            if ( this.NilifyScript( node.Number, name ) )
            {
                // make sure to re-check that script is gone.
                if ( this.SavedScriptExists( node, name, true ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' saved script {1} still exists after nil on node {2};. {3}{4}", this.ResourceNameCaption, name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    affirmative = false;
                }
                else
                {
                    affirmative = true;
                }
            }
            else
            {
                affirmative = false;
            }

            this.Session.LastNodeNumber = new int?();
            return affirmative;
        }

        /// <summary> Deletes the <see cref="Name">specified</see> script. </summary>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        /// <returns> <c>True</c> if the script is nil; otherwise <c>False</c>. </returns>
        public bool DeleteSavedScript( bool refreshScriptCatalog )
        {
            this.DeleteSavedScript( this.Name, refreshScriptCatalog );
            return true;
        }

        /// <summary> The delete timeout. </summary>
        private TimeSpan _DeleteTimeout;

        /// <summary> Gets or sets the time out for deleting a script. </summary>
        /// <value> The delete timeout. </value>
        public TimeSpan DeleteTimeout
        {
            get => this._DeleteTimeout;

            set {
                if ( !this.DeleteTimeout.Equals( value ) )
                {
                    this._DeleteTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FETCH SCRIPTS "

        /// <summary> The author prefix. </summary>
        private const string _AuthorPrefix = "isr_";

        /// <summary> The last fetched author scripts. </summary>
        private List<string> _LastFetchedAuthorScripts;

        /// <summary> Last fetched author scripts. </summary>
        /// <returns> A list of strings. </returns>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> LastFetchedAuthorScripts()
        {
            return this._LastFetchedAuthorScripts is null ? new System.Collections.ObjectModel.ReadOnlyCollection<string>( new List<string>() ) : new System.Collections.ObjectModel.ReadOnlyCollection<string>( this._LastFetchedAuthorScripts );
        }

        /// <summary>
        /// Fetches the list of saved scripts and saves it in the
        /// <see cref="LastFetchedSavedScripts"></see>
        /// </summary>
        public void FetchSavedScripts()
        {
            try
            {
                this.Session.StoreCommunicationTimeout( this.SaveTimeout );
                this.LastFetchedSavedScripts = string.Empty;
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "fetching saved scripts;. " );
                this.Session.LastNodeNumber = new int?();
                _ = this.Session.WriteLine( "do {0} print( names ) end ", Syntax.Lua.ScriptCatalogGetterCommand );
                this.LastFetchedSavedScripts = this.Session.ReadLineTrimEnd();
                if ( string.IsNullOrWhiteSpace( this.LastFetchedSavedScripts ) )
                {
                    this.LastFetchedSavedScripts = string.Empty;
                    this.CheckThrowDeviceException( true, "fetching saved scripts;. last sent: '{0}'; last received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                }
                else
                {
                    this._LastFetchedAuthorScripts = new List<string>();
                    var scripts = this.LastFetchedSavedScripts.Split( ',' );
                    foreach ( string s in scripts )
                    {
                        if ( s.StartsWith( _AuthorPrefix, StringComparison.OrdinalIgnoreCase ) )
                        {
                            this._LastFetchedAuthorScripts.Add( s );
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Session.RestoreCommunicationTimeout();
            }
        }

        /// <summary>
        /// Fetches the list of saved scripts and saves it in the
        /// <see cref="LastFetchedSavedScripts"></see>
        /// </summary>
        /// <param name="nodeNumber"> Specifies the subsystem node. </param>
        public void FetchSavedScripts( int nodeNumber )
        {
            try
            {
                this.Session.StoreCommunicationTimeout( this._SaveTimeout );
                this.LastFetchedSavedRemoteScripts = string.Empty;
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "fetching catalog;. " );
                this.Session.LastNodeNumber = nodeNumber;
                _ = this.Session.WriteLine( Syntax.Node.ValueGetterCommandFormat2, nodeNumber, Syntax.Lua.ScriptCatalogGetterCommand, "names" );
                this.LastFetchedSavedRemoteScripts = this.Session.ReadLineTrimEnd();
                this.CheckThrowDeviceException( true, "fetching catalog;. last sent: '{0}'.", this.Session.LastMessageSent );
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Session.RestoreCommunicationTimeout();
                this.Session.LastNodeNumber = new int?();
            }
        }

        /// <summary>
        /// Fetches the list of saved scripts and saves it in the
        /// <see cref="LastFetchedSavedScripts"></see>
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        public void FetchSavedScripts( NodeEntityBase node )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( node.IsController )
            {
                this.FetchSavedScripts();
            }
            else
            {
                try
                {
                    this.Session.StoreCommunicationTimeout( this._SaveTimeout );
                    this.LastFetchedSavedRemoteScripts = string.Empty;
                    this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "fetching catalog;. " );
                    this.Session.LastNodeNumber = node.Number;
                    _ = this.Session.WriteLine( Syntax.Node.ValueGetterCommandFormat2, node.Number, Syntax.Lua.ScriptCatalogGetterCommand, "names" );
                    this.CheckThrowDeviceException( true, "fetching catalog using the command '{0}';. ", this.Session.LastMessageSent );
                    this.LastFetchedSavedRemoteScripts = this.Session.ReadLineTrimEnd();
                }
                catch
                {
                    throw;
                }
                finally
                {
                    this.Session.LastNodeNumber = new int?();
                    this.Session.RestoreCommunicationTimeout();
                }
            }
        }

        /// <summary>
        /// Gets a comma-separated and comma-terminated list of the saved scripts that was fetched last.
        /// A new script is fetched after save and delete.
        /// </summary>
        /// <value> The last fetched saved scripts. </value>
        public string LastFetchedSavedScripts { get; private set; }

        /// <summary>
        /// Gets a comma-separated and comma-terminated list of the saved scripts that was fetched last
        /// from the remote node.  A new script is fetched after save and delete.
        /// </summary>
        /// <value> The last fetched saved remote scripts. </value>
        public string LastFetchedSavedRemoteScripts { get; private set; }

        #endregion

        #region " RUN SCRIPT "

        /// <summary> Runs the named script. </summary>
        /// <remarks> Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <exception cref="TimeoutException">         Thrown when a Timeout error condition occurs. </exception>
        /// <param name="name">    Specifies the script name. </param>
        /// <param name="timeout"> Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        public void RunScript( string name, TimeSpan timeout )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            string returnedValue = "1";
            _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "running script;. " );
            _ = this.Session.WriteLine( "{0}.run() waitcomplete() print('{1}') ", name, returnedValue );

            // wait till we get a reply from the instrument or timeout.
            if ( this.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 40d ), timeout ) )
            {
                string value = this.Session.ReadLineTrimEnd();
                if ( string.IsNullOrWhiteSpace( value ) || !value.Trim().StartsWith( returnedValue, StringComparison.OrdinalIgnoreCase ) )
                {
                    if ( string.IsNullOrWhiteSpace( value ) )
                    {
                        throw new Core.OperationFailedException( "Script '{0}' failed;. script returned no value.", name );
                    }
                    else
                    {
                        throw new Core.OperationFailedException( "Script '{0}' failed;. returned value {1} is not the same as expected '{2}'.", name, value, returnedValue );
                    }
                }
            }
            else
            {
                throw new TimeoutException( "Timeout waiting operation completion running the script '" + name + "'" );
            }
        }

        /// <summary> Runs the named script. </summary>
        /// <remarks> Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> Specifies the subsystem node. </param>
        /// <param name="name">       Specifies the script name. </param>
        /// <param name="timeout">    Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        public void RunScript( int nodeNumber, string name, TimeSpan timeout )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Running script '{0}' on node {1};. ", name, nodeNumber );
            this.Session.LastNodeNumber = nodeNumber;
            string message = this.Session.ExecuteCommand( nodeNumber, "{0}.run()", name );
            this.CheckThrowDeviceException( false, message );
            this.LinkSubsystem.WaitComplete( nodeNumber, timeout, false );
            this.CheckThrowDeviceException( false, "running script '{0}' using '{1}';. ", name, message );
            this.Session.LastNodeNumber = new int?();
        }

        /// <summary> Runs the script. </summary>
        /// <remarks> Waits for operation completion. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="script"> Specifies the script. </param>
        /// <param name="node">   Specifies the subsystem node. </param>
        public void RunScript( ScriptEntityBase script, NodeEntityBase node )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( string.IsNullOrWhiteSpace( script.Name ) )
                throw new InvalidOperationException( "script name is empty" );
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Running script '{0}' on node {1};. ", this.Name, node.Number );
            this.Session.LastNodeNumber = node.Number;
            string message = this.Session.ExecuteCommand( node.Number, "node[{1}].execute( '{0}.run()' ) waitcomplete({1})", script.Name, node.Number );
            this.CheckThrowDeviceException( true, message );
            this.LinkSubsystem.WaitComplete( node.Number, script.Timeout, true );
            this.CheckThrowDeviceException( true, "running script '{0}' using '{1}';. ", this.Name, message );
            this.Session.LastNodeNumber = new int?();
        }

        #endregion

        #region " SAVE SCRIPT "

        /// <summary> The save timeout. </summary>
        private TimeSpan _SaveTimeout = TimeSpan.Zero;

        /// <summary> Gets or sets the time out for saving a script. </summary>
        /// <value> The save timeout. </value>
        public TimeSpan SaveTimeout
        {
            get => this._SaveTimeout;

            set {
                if ( !this.SaveTimeout.Equals( value ) )
                {
                    this._SaveTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Saves the specifies script to non-volatile memory. </summary>
        /// <param name="name"> Gets or sets the script name. </param>
        /// <returns> <c>True</c> is script exist; otherwise, <c>False</c>. </returns>
        public bool SaveScript( string name )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
            {
                return false;
            }

            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "saving script '{0}';. ", name );
            this.Session.LastNodeNumber = new int?();
            _ = this.Session.WriteLine( "{0}.save()", name );
            return this.SavedScriptExists( name, true );
        }

        #endregion

        #region " FIND SCRIPT "

        /// <summary> Checks if the specified script exists as a saved script. </summary>
        /// <param name="name"> Specifies the script name. </param>
        /// <returns>
        /// <c>True</c> if the specified script exists as a saved script; otherwise,
        /// <c>False</c>.
        /// </returns>
        public bool LastSavedScriptExists( string name )
        {
            return this.SavedScriptExists( name, false );
        }

        /// <summary>
        /// Checks <c>True</c> if the specified script exists as a saved script on the remote node.
        /// </summary>
        /// <param name="name"> Specifies the script name. </param>
        /// <returns>
        /// <c>True</c> if the specified script exists as a saved script on the remote node;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool LastSavedRemoteScriptExists( string name )
        {
            return !string.IsNullOrWhiteSpace( name ) && this.LastFetchedSavedRemoteScripts.IndexOf( name + ",", 0, StringComparison.OrdinalIgnoreCase ) >= 0;
        }

        /// <summary> Checks if the specified script exists as a saved script. </summary>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        /// <returns>
        /// <c>True</c> if the specified script exists as a saved script; otherwise, <c>False</c>.
        /// </returns>
        public bool SavedScriptExists( string name, bool refreshScriptCatalog )
        {
            if ( refreshScriptCatalog )
            {
                this.FetchSavedScripts();
            }

            return this.LastFetchedSavedScripts.IndexOf( name + ",", 0, StringComparison.OrdinalIgnoreCase ) >= 0;
        }

        /// <summary> Returns <c>True</c> if the specified script exists as a saved script. </summary>
        /// <param name="node">                 Specifies the node to validate. </param>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        /// <returns>
        /// <c>True</c> if the specified script exists as a saved script on the remote node;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool SavedScriptExists( NodeEntityBase node, string name, bool refreshScriptCatalog )
        {
            if ( refreshScriptCatalog )
            {
                this.FetchSavedScripts( node );
            }

            return this.LastSavedRemoteScriptExists( name );
        }

        /// <summary> Returns <c>True</c> if the specified script exists as a saved script. </summary>
        /// <param name="nodeNumber">           Specifies the remote node number to validate. </param>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        /// <returns>
        /// <c>True</c> if the specified script exists as a saved script on the remote node;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool SavedScriptExists( int nodeNumber, string name, bool refreshScriptCatalog )
        {
            if ( refreshScriptCatalog )
            {
                this.FetchSavedScripts( nodeNumber );
            }

            return this.LastSavedRemoteScriptExists( name );
        }

        /// <summary> Returns <c>True</c> if the specified script exists as a saved script. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">                 Specifies the script name. </param>
        /// <param name="node">                 Specifies the node to validate. </param>
        /// <param name="refreshScriptCatalog"> True to refresh the list of saved scripts. </param>
        /// <returns>
        /// <c>True</c> if the specified script exists as a saved script; otherwise,
        /// <c>False</c>.
        /// </returns>
        public bool SavedScriptExists( string name, NodeEntityBase node, bool refreshScriptCatalog )
        {
            return node is null
                ? throw new ArgumentNullException( nameof( node ) )
                : node.IsController ? this.SavedScriptExists( name, refreshScriptCatalog ) : this.SavedScriptExists( node.Number, name, refreshScriptCatalog );
        }

        /// <summary> Checks if the script is not nil. </summary>
        /// <param name="name"> The script name. </param>
        /// <returns> <c>True</c> is script exist; otherwise, <c>False</c>. </returns>
        public bool ScriptExists( string name )
        {
            return !(string.IsNullOrWhiteSpace( name ) || this.Session.IsNil( name ));
        }

        #endregion

        #region " PARSE SCRIPT "

        /// <summary> Parses the script. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">        specifies the source code for the script. </param>
        /// <param name="retainOutline"> Specifies if the code outline is retained or trimmed. </param>
        /// <returns> Parsed script. </returns>
        public static string ParseScript( string source, bool retainOutline )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            var sourceLines = source.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            var newSource = new System.Text.StringBuilder();
            bool isInCommentBlock = false;
            bool wasInCommentBlock;
            TspChunkLineContentType lineType;
            foreach ( string line in sourceLines )
            {
                string chunkLine = TrimTspChuckLine( line, retainOutline );
                wasInCommentBlock = isInCommentBlock;
                lineType = ParseTspChuckLine( chunkLine, isInCommentBlock );
                if ( lineType == TspChunkLineContentType.None )
                {
                }

                // if no data, nothing to do.

                else if ( wasInCommentBlock )
                {

                    // if was in a comment block exit the comment block if
                    // received a end of comment block
                    if ( lineType == TspChunkLineContentType.EndCommentBlock )
                    {
                        isInCommentBlock = false;
                    }
                }
                else if ( lineType == TspChunkLineContentType.StartCommentBlock )
                {
                    isInCommentBlock = true;
                }
                else if ( lineType == TspChunkLineContentType.Comment )
                {
                }

                // if comment line do nothing

                else if ( lineType == TspChunkLineContentType.Syntax || lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                {
                    if ( lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                    {
                        chunkLine = chunkLine.Substring( 0, chunkLine.IndexOf( Syntax.Lua.StartCommentChunk, StringComparison.OrdinalIgnoreCase ) );
                    }

                    if ( !string.IsNullOrWhiteSpace( chunkLine ) )
                    {
                        _ = newSource.AppendLine( chunkLine );
                    }

                    if ( lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                    {
                        isInCommentBlock = true;
                    }
                }
            }

            return newSource.ToString();
        }

        /// <summary> Parses a TSP chuck line.  This assumes that the line was trimmed. </summary>
        /// <param name="value">            Specifies the line. </param>
        /// <param name="isInCommentBlock"> <c>True</c> if this object is in comment block. </param>
        /// <returns> The parsed <see cref="TspChunkLineContentType">content type.</see> </returns>
        public static TspChunkLineContentType ParseTspChuckLine( string value, bool isInCommentBlock )
        {
            if ( value is null )
                value = string.Empty;
            value = value.Trim().Replace( Conversions.ToString( Convert.ToChar( 9, System.Globalization.CultureInfo.CurrentCulture ) ), " " ).Trim();
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return TspChunkLineContentType.None;
            }

            // check if start of comment block
            else if ( value.StartsWith( Syntax.Lua.StartCommentChunk, StringComparison.OrdinalIgnoreCase ) )
            {
                return TspChunkLineContentType.StartCommentBlock;
            }
            else if ( value.Contains( Syntax.Lua.StartCommentChunk ) )
            {
                return TspChunkLineContentType.SyntaxStartCommentBlock;
            }

            // check if in a comment block
            else if ( isInCommentBlock && value.Contains( Syntax.Lua.EndCommentChunk ) )
            {

                // check if end of comment block
                return TspChunkLineContentType.EndCommentBlock;
            }

            // skip comment lines.
            else
            {
                return value.StartsWith( Syntax.Lua.CommentChunk, StringComparison.OrdinalIgnoreCase )
                    ? TspChunkLineContentType.Comment
                    : TspChunkLineContentType.Syntax;
            }
        }

        /// <summary> Trims the TSP chuck line. </summary>
        /// <param name="value">         Specifies the line. </param>
        /// <param name="retainOutline"> Specifies if the code outline is retained or trimmed. </param>
        /// <returns> The trimmed chunk line. </returns>
        public static string TrimTspChuckLine( string value, bool retainOutline )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }
            else
            {
                if ( retainOutline )
                {
                    // remove leading and lagging spaces and horizontal tabs
                    return value.Replace( Conversions.ToString( Convert.ToChar( 9, System.Globalization.CultureInfo.CurrentCulture ) ), "  " ).TrimEnd();
                }
                else
                {
                    // remove leading and lagging spaces and horizontal tabs
                    return value.Replace( Conversions.ToString( Convert.ToChar( 9, System.Globalization.CultureInfo.CurrentCulture ) ), " " ).Trim();
                }

                // unreachable code
#if false

                if (string.IsNullOrWhiteSpace(value))
                {
                    return string.Empty;
                }

                // check if start of comment block
                else if (value.Contains(Syntax.Lua.StartCommentChunk))
                {

                    // return the start of comment chunk
                    return Syntax.Lua.StartCommentChunk;
                }

                // check if end of comment block
                else if (value.Contains(Syntax.Lua.EndCommentChunk))
                {

                    // return the end of comment chunk
                    return Syntax.Lua.EndCommentChunk;
                }

                // check if a comment line
                else if ((value.Substring(0, 2) ?? "") == Syntax.Lua.CommentChunk)
                {

                    // return the comment chunk
                    return Syntax.Lua.CommentChunk;
                }

                // remove a trailing comment.  This cannot be easily done
                // become of commands such as
                // print( '----' )
                // print( " --- -" )
                else if (value.Contains("\""))
                {
                    return value;
                }
                else if (value.Contains("'"))
                {
                    return value;
                }
                else
                {
                    // if no text in the line, we can safely remove a trailing comment.
                    if (value.Contains(Syntax.Lua.CommentChunk))
                    {
                        value = value.Substring(0, value.IndexOf(Syntax.Lua.CommentChunk, StringComparison.OrdinalIgnoreCase));
                    }

                    return value;
                }
#endif

            }
        }

        #endregion

        #region " OPEN FILE "

        /// <summary> Opens a script file as a text reader. </summary>
        /// <param name="filePath"> Specifies the script file path. </param>
        /// <returns>
        /// A reference to an open
        /// <see cref="System.IO.TextReader">Text Stream</see>.
        /// </returns>
        public static System.IO.StreamReader OpenScriptFile( string filePath )
        {

            // Check name
            return string.IsNullOrWhiteSpace( filePath ) || !System.IO.File.Exists( filePath ) ? null : new System.IO.StreamReader( filePath );
        }

        /// <summary> Opens a script file as a text reader. </summary>
        /// <returns>
        /// A reference to an open
        /// <see cref="System.IO.TextReader">Text Stream</see>.
        /// </returns>
        public System.IO.StreamReader OpenScriptFile()
        {

            // Check name
            return string.IsNullOrWhiteSpace( this.Name ) || string.IsNullOrWhiteSpace( this._FilePath ) || !System.IO.File.Exists( this.FilePath )
                ? null
                : new System.IO.StreamReader( this.FilePath );
        }

        #endregion

        #region " READ AND WRITE "

        /// <summary> Reads the script from the script file. </summary>
        /// <param name="filePath"> Specifies the script file path. </param>
        /// <returns> The script. </returns>
        public static string ReadScript( string filePath )
        {
            using var tspFile = OpenScriptFile( filePath );
            return tspFile.ReadToEnd();
        }

        /// <summary> Writes the script to file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source">   specifies the source code for the script. </param>
        /// <param name="filePath"> Specifies the script file path. </param>
        public static void WriteScript( string source, string filePath )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            using var tspFile = new System.IO.StreamWriter( filePath );
            tspFile.Write( source );
        }

        #endregion

        #region " TSP SCRIPTS: PARSE, READ, WRITE "

        /// <summary> Reads the scripts, parses them and saves them to file. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="System.IO.FileNotFoundException">    Thrown when the requested file is not present. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="filePath">      Specifies the folder where scripts are stored. </param>
        /// <param name="retainOutline"> Specifies if the code outline is retained or trimmed. </param>
        public static void ReadParseWriteScript( string filePath, bool retainOutline )
        {
            if ( string.IsNullOrWhiteSpace( filePath ) )
                throw new ArgumentNullException( nameof( filePath ) );
            // check if file exists.
            if ( ScriptEntityBase.FileSize( filePath ) <= 2L )
            {
                throw new System.IO.FileNotFoundException( "Script file not found", filePath );
            }
            else
            {
                string scriptSource = ReadScript( filePath );
                if ( string.IsNullOrWhiteSpace( scriptSource ) )
                {
                    throw new Core.OperationFailedException( "Failed reading script;. file '{0}' includes no source.", filePath );
                }
                else
                {
                    scriptSource = ParseScript( scriptSource, retainOutline );
                    if ( string.IsNullOrWhiteSpace( scriptSource ) )
                    {
                        throw new Core.OperationFailedException( "Failed reading script;. parsed script from '{0}' is empty.", filePath );
                    }
                    else
                    {
                        filePath += ".debug";
                        WriteScript( scriptSource, filePath );
                    }
                }
            }
        }

        /// <summary> Reads the scripts, parses them and saves them to file. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="instrumentModelNumber"> The instrument model number. </param>
        /// <param name="folderPath">            Specifies the folder where scripts are stored. </param>
        /// <param name="scripts">               Specifies the collection of scripts. </param>
        /// <param name="retainOutline">         Specifies if the code outline is retained or trimmed. </param>
        public static void ReadParseWriteScripts( string instrumentModelNumber, string folderPath, ScriptEntityCollection scripts, bool retainOutline )
        {
            if ( instrumentModelNumber is null )
                throw new ArgumentNullException( nameof( instrumentModelNumber ) );
            if ( folderPath is null )
                throw new ArgumentNullException( nameof( folderPath ) );
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( scripts is object && scripts.Count > 0 )
            {
                foreach ( ScriptEntityBase script in scripts )
                {
                    if ( script.IsModelMatch( instrumentModelNumber ) && script.RequiresReadParseWrite() )
                    {
                        if ( string.IsNullOrWhiteSpace( script.FileName ) )
                        {
                            throw new InvalidOperationException( string.Format( System.Globalization.CultureInfo.CurrentCulture, "File name not specified for script '{0}'.", script.FileName ) );
                        }
                        else
                        {
                            string filePath = System.IO.Path.Combine( folderPath, script.FileName );
                            ReadParseWriteScript( filePath, retainOutline );
                        }
                    }
                }
            }
        }

        #endregion

        #region " TSP SCRIPTS: LOAD FROM FILE "

        /// <summary> Loads a named script into the instrument. </summary>
        public void LoadScriptFileSimple()
        {
            this.LoadScriptFileSimple( this.Name, this.FilePath );
        }

        /// <summary>
        /// Loads a named script into the instrument allowing control over how errors and prompts are
        /// handled. For loading a script that does not includes functions, turn off errors and turn on
        /// the prompt.
        /// </summary>
        /// <exception cref="System.IO.FileNotFoundException">    Thrown when the requested file is not present. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="showErrors">    Specifies the condition for turning off or on error checking
        /// while the script is loaded. </param>
        /// <param name="showPrompts">   Specifies the condition for turning off or on the TSP prompts
        /// while the script is loaded. </param>
        /// <param name="retainOutline"> Specifies if the code outline is retained or trimmed. </param>
        public void LoadScriptFile( bool showErrors, bool showPrompts, bool retainOutline )
        {

            // store the status
            this.InteractiveSubsystem.StoreStatus();
            string chunkLine;
            try
            {
                bool isInCommentBlock;
                isInCommentBlock = false;
                int lineNumber;
                lineNumber = 0;

                // flush the buffer until empty and update the TSP status.
                this.Session.DiscardUnreadData();

                // store state of prompts and errors.
                // statusSubsystem.StoreStatus()

                // Disable automatic display of errors - leave error messages in queue
                // and enable error Prompt. or otherwise...
                _ = this.InteractiveSubsystem.WriteShowErrors( showErrors );

                // Turn on prompts
                _ = this.InteractiveSubsystem.WriteShowPrompts( showPrompts );

                // flush the buffer until empty and update the TSP status.
                this.Session.DiscardUnreadData();
                bool isFirstLine;
                isFirstLine = true;
                string commandLine;
                bool wasInCommentBlock;
                TspChunkLineContentType lineType;
                string activity;
                string actionDetails;
                using ( var tspFile = this.OpenScriptFile() )
                {
                    if ( tspFile is null )
                    {
                        throw new System.IO.FileNotFoundException( "Failed opening TSP Script file", this.FilePath );
                    }

                    using var debugFile = new System.IO.StreamWriter( this.FilePath + ".debug" );
                    this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "sending a 'loadscript' for script '{0}' from file '{1}'", this.Name, this._FilePath );
                    this.Session.LastNodeNumber = new int?();
                    while ( !tspFile.EndOfStream )
                    {
                        chunkLine = TrimTspChuckLine( tspFile.ReadLine(), retainOutline );
                        lineNumber += 1;
                        wasInCommentBlock = isInCommentBlock;
                        lineType = ParseTspChuckLine( chunkLine, isInCommentBlock );
                        if ( lineType == TspChunkLineContentType.None )
                        {
                        }

                        // if no data, nothing to do.

                        else if ( wasInCommentBlock )
                        {

                            // if was in a comment block exit the comment block if
                            // received a end of comment block
                            if ( lineType == TspChunkLineContentType.EndCommentBlock )
                            {
                                isInCommentBlock = false;
                            }
                        }
                        else if ( lineType == TspChunkLineContentType.StartCommentBlock )
                        {
                            isInCommentBlock = true;
                        }
                        else if ( lineType == TspChunkLineContentType.Comment )
                        {
                        }

                        // if comment line do nothing

                        else if ( lineType == TspChunkLineContentType.Syntax || lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                        {
                            if ( lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                            {
                                chunkLine = chunkLine.Substring( 0, chunkLine.IndexOf( Syntax.Lua.StartCommentChunk, StringComparison.OrdinalIgnoreCase ) );
                            }

                            // end each line with a space
                            chunkLine += " ";
                            if ( isFirstLine )
                            {
                                activity = $"{this.ResourceNameCaption} sending a 'loadscript'";
                                actionDetails = $"sending a 'loadscript' for script '{this.Name}' from file '{this.FilePath}'";
                                // issue a start of script command.  The command
                                // 'loadscript' identifies the beginning of the named script.
                                commandLine = "loadscript " + this.Name + " ";
                                _ = this.Session.WriteLine( commandLine );
                                if ( ( int? ) this.InteractiveSubsystem.ExecutionState != ( int? ) TspExecutionState.IdleError == true )
                                {
                                    isFirstLine = false;
                                }
                                else if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
                                {
                                    // done inside the query Me.StatusSubsystem.QueryStandardEventStatus()
                                    if ( this.StatusSubsystem.HasDeviceError )
                                    {
                                        throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                                    }
                                    else
                                    {
                                        throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; Failed fetching device errors" );
                                    }
                                }
                                else
                                {
                                    throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; No device errors" );
                                }
                            }
                            // Continuation prompt. TSP received script line successfully; waiting for next line.
                            switch ( this.InteractiveSubsystem.ExecutionState )
                            {
                                case TspExecutionState.IdleContinuation:
                                    {
                                        break;
                                    }

                                case TspExecutionState.IdleError:
                                    {
                                        Debug.Assert( !Debugger.IsAttached, "this should not happen :)" );
                                        break;
                                    }

                                case TspExecutionState.IdleReady:
                                    {
                                        // Ready prompt. TSP received script successfully; ready for next command.
                                        break;
                                    }

                                default:
                                    {
                                        break;
                                    }
                                    // do nothing
                            }

                            activity = $"{this.ResourceNameCaption} sending a syntax line";
                            actionDetails = $@"sending a syntax line:
{chunkLine} 
for script {this.Name} from file '{this.FilePath}'";
                            _ = this.Session.WriteLine( chunkLine );
                            if ( ( int? ) this.InteractiveSubsystem.ExecutionState == ( int? ) TspExecutionState.IdleError == true )
                            {
                                // now report the error to the calling module
                                if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
                                {
                                    // done inside the query Me.StatusSubsystem.QueryStandardEventStatus()
                                    if ( this.StatusSubsystem.HasDeviceError )
                                    {
                                        throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                                    }
                                    else
                                    {
                                        throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; Failed fetching device errors" );
                                    }
                                }
                                else
                                {
                                    throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; No device errors" );
                                }
                            }
                            else
                            {
                                // increment debug line number
                                debugFile.WriteLine( chunkLine );
                            }

                            switch ( this.InteractiveSubsystem.ExecutionState )
                            {
                                case TspExecutionState.IdleError:
                                    {
                                        Debug.Assert( !Debugger.IsAttached, "this should not happen :)" );
                                        break;
                                    }

                                case TspExecutionState.IdleReady:
                                    {
                                        break;
                                    }

                                default:
                                    {
                                        break;
                                    }
                                    // do nothing
                            }

                            if ( lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                            {
                                isInCommentBlock = true;
                            }
                        }
                    }
                }

                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "ending loaded script '{0}';. ", this.Name );
                this.Session.LastNodeNumber = new int?();
                activity = $"{this.ResourceNameCaption} sending an 'endscript'";
                actionDetails = $"sending an 'endscript' for script '{this.Name}' from file '{this.FilePath}'";

                // Tell TSP complete script has been downloaded.
                commandLine = "endscript waitcomplete() print('1') ";
                _ = this.Session.WriteLine( commandLine );
                if ( ( int? ) this.InteractiveSubsystem.ExecutionState == ( int? ) TspExecutionState.IdleError == true )
                {
                    if ( (this.Session.ReadStatusRegister() & this.StatusSubsystem.ErrorAvailableBit) != 0 )
                    {
                        if ( this.StatusSubsystem.HasDeviceError )
                        {
                            throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; Device errors: {this.StatusSubsystem.DeviceErrorReport}" );
                        }
                        else
                        {
                            throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; Failed fetching device errors" );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"{activity} failed;. {actionDetails}; No device errors" );
                    }
                }

                // wait till we get a reply from the instrument or timeout.

                // The command above does not seem to work!  It looks like the print does not get executed!
                var sw = Stopwatch.StartNew();
                var timeout = TimeSpan.FromMilliseconds( 3000d );
                string value = string.Empty;
                do
                {
                    do
                        System.Threading.Thread.Sleep( 50 );
                    while ( !this.Session.QueryMessageAvailableStatus() && sw.Elapsed <= timeout );
                    if ( this.Session.QueryMessageAvailableStatus() )
                    {
                        value = this.Session.ReadLine();
                        if ( !value.StartsWith( "1", StringComparison.OrdinalIgnoreCase ) )
                        {
                            _ = this.InteractiveSubsystem.ParseExecutionState( value, TspExecutionState.IdleReady );
                        }
                    }
                }
                while ( !value.StartsWith( "1", StringComparison.OrdinalIgnoreCase ) && sw.Elapsed <= timeout );
                if ( sw.Elapsed > timeout )
                {
                    // Throw New isr.Tsp.ScriptCallException("Timeout waiting operation completion loading the script '" & Me._name & "'")
                }

                // add a wait to ensure the system returns the last status.
                System.Threading.Thread.Sleep( 100 );

                // flush the buffer until empty to completely reading the status.
                this.Session.DiscardUnreadData();

                // get the script state if showing prompts
                switch ( this.InteractiveSubsystem.ExecutionState )
                {
                    case TspExecutionState.IdleError:
                        {
                            Debug.Assert( !Debugger.IsAttached, "this should not happen :)" );
                            break;
                        }

                    default:
                        {
                            break;
                        }
                        // do nothing
                }
            }
            catch
            {

                // remove any remaining values.
                this.Session.DiscardUnreadData();
                throw;
            }
            finally
            {

                // restore state of prompts and errors.
                this.InteractiveSubsystem.RestoreStatus();
            }
        }

        /// <summary> Loads a named script into the instrument. </summary>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <param name="name">     Specifies the script name. </param>
        /// <param name="filePath"> The file path. </param>
        public void LoadScriptFileSimple( string name, string filePath )
        {
            try
            {
                using var tspFile = OpenScriptFile( filePath );
                if ( tspFile is null )
                {
                    throw new System.IO.FileNotFoundException( "Failed opening script file", filePath );
                }

                string line;
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "load script {0} from {1};. ", name, filePath );
                this.Session.LastNodeNumber = new int?();
                _ = this.Session.WriteLine( "loadscript {0}", name );
                while ( !tspFile.EndOfStream )
                {
                    line = tspFile.ReadLine().Trim();
                    if ( !string.IsNullOrWhiteSpace( line ) )
                    {
                        _ = this.Session.WriteLine( line );
                    }

                    Core.ApplianceBase.DoEvents();
                }

                _ = this.Session.WriteLine( "endscript" );
                this.CheckThrowDeviceException( true, "loading script '{0}';. last line was '{1}'", name, this.Session.LastMessageSent );
            }
            catch
            {

                // remove any remaining values.
                this.Session.DiscardUnreadData();
                throw;
            }
            finally
            {
            }
        }

        /// <summary>
        /// Loads a named script into the instrument allowing control over how errors and prompts are
        /// handled. For loading a script that does not includes functions, turn off errors and turn on
        /// the prompt.
        /// </summary>
        /// <exception cref="System.IO.IOException"> Thrown when an IO failure occurred. </exception>
        /// <param name="name">          Specifies the script name. </param>
        /// <param name="filePath">      Specifies the script file name. </param>
        /// <param name="retainOutline"> Specifies if the code outline is retained or trimmed. </param>
        public void LoadScriptFileLegacy( string name, string filePath, bool retainOutline )
        {
            string chunkLine;
            string commandLine;
            try
            {
                using ( var tspFile = OpenScriptFile( filePath ) )
                {
                    if ( tspFile is null )
                    {

                        // now report the error to the calling module
                        throw new System.IO.IOException( "Failed opening TSP Script File '" + filePath + "'." );
                    }

                    using var debugFile = new System.IO.StreamWriter( filePath + ".debug" );
                    bool isInCommentBlock;
                    isInCommentBlock = false;
                    int lineNumber;
                    lineNumber = 0;
                    bool isFirstLine;
                    isFirstLine = true;
                    bool wasInCommentBlock;
                    TspChunkLineContentType lineType;
                    while ( !tspFile.EndOfStream )
                    {
                        chunkLine = tspFile.ReadLine();
                        chunkLine = TrimTspChuckLine( chunkLine, retainOutline );
                        lineNumber += 1;
                        wasInCommentBlock = isInCommentBlock;
                        lineType = ParseTspChuckLine( chunkLine, isInCommentBlock );
                        if ( lineType == TspChunkLineContentType.None )
                        {
                        }

                        // if no data, nothing to do.

                        else if ( wasInCommentBlock )
                        {

                            // if was in a comment block exit the comment block if
                            // received a end of comment block
                            if ( lineType == TspChunkLineContentType.EndCommentBlock )
                            {
                                isInCommentBlock = false;
                            }
                        }
                        else if ( lineType == TspChunkLineContentType.StartCommentBlock )
                        {
                            isInCommentBlock = true;
                        }
                        else if ( lineType == TspChunkLineContentType.Comment )
                        {
                        }

                        // if comment line do nothing

                        else if ( lineType == TspChunkLineContentType.Syntax || lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                        {
                            if ( lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                            {
                                chunkLine = chunkLine.Substring( 0, chunkLine.IndexOf( Syntax.Lua.StartCommentChunk, StringComparison.OrdinalIgnoreCase ) );
                            }

                            // end each line with a space
                            chunkLine += " ";
                            if ( isFirstLine )
                            {
                                // issue a start of script command.  The command
                                // 'loadscript' identifies the beginning of the named script.
                                commandLine = "loadscript " + name + " ";
                                _ = this.Session.WriteLine( commandLine );
                                isFirstLine = false;
                                this.CheckThrowDeviceException( true, "sending a 'loadscript' for script '{2}';. from file '{1}'", name, filePath );
                            }

                            _ = this.Session.WriteLine( chunkLine );

                            // increment debug line number
                            debugFile.WriteLine( chunkLine );
                            if ( lineType == TspChunkLineContentType.SyntaxStartCommentBlock )
                            {
                                isInCommentBlock = true;
                            }
                        }
                    }
                }

                // Tell TSP complete script has been downloaded.
                commandLine = "endscript waitcomplete() print('1') ";
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "sending an 'endscript' for script '{0}'; from file '{1}'", name, filePath );
                _ = this.Session.WriteLine( commandLine );

                // wait till we get a reply from the instrument or timeout.
                var sw = Stopwatch.StartNew();
                var timeout = TimeSpan.FromMilliseconds( 3000d );
                string value = string.Empty;
                do
                {
                    do
                        System.Threading.Thread.Sleep( 50 );
                    while ( !this.Session.QueryMessageAvailableStatus() && sw.Elapsed <= timeout );
                    if ( this.Session.QueryMessageAvailableStatus() )
                    {
                        value = this.Session.ReadLine();
                    }
                }
                while ( !value.StartsWith( "1", StringComparison.OrdinalIgnoreCase ) && sw.Elapsed <= timeout );
                if ( sw.Elapsed > timeout )
                {
                    // Throw New isr.Tsp.ScriptCallException("Timeout waiting operation completion loading the script '" & Me._name & "'")
                }

                // add a wait to ensure the system returns the last status.
                System.Threading.Thread.Sleep( 100 );

                // flush the receive buffer until empty.
                this.Session.DiscardUnreadData();
            }
            catch
            {

                // flush the receive buffer until empty.
                this.Session.DiscardUnreadData();
                throw;
            }
            finally
            {
            }
        }

        /// <summary> Loads the specified TSP script from file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <param name="folderPath"> Specifies the script folder path. </param>
        /// <param name="script">     Specifies reference to a valid
        /// <see cref="ScriptEntityBase">script</see> </param>
        public void LoadUserScriptFile( string folderPath, ScriptEntityBase script )
        {
            if ( folderPath is null )
                throw new ArgumentNullException( nameof( folderPath ) );
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( !this.Session.IsNil( script.Name ) )
            {
                // script already exists
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} already exists;. ", this.ResourceNameCaption, script.Name );
                return;
            }

            string filePath = System.IO.Path.Combine( folderPath, script.FileName );
            // check if file exists.
            if ( ScriptEntityBase.FileSize( filePath ) <= 2L )
            {
                throw new System.IO.FileNotFoundException( "Script file not found or is empty;. ", script.FileName );
            }

            this.DisplaySubsystem.DisplayLine( 2, "Loading {0} from file", script.Name );
            try
            {
                this.LoadScriptFileLegacy( script.Name, filePath, true );
            }
            catch
            {
                this.DisplaySubsystem.DisplayLine( 2, "Failed loading {0} from file", script.Name );
                throw;
            }

            // do a garbage collection
            if ( !this.StatusSubsystem.CollectGarbageWaitComplete( script.Timeout, "collecting garbage;. " ) )
            {
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Ignoring instrument '{0}' error(s) collecting garbage after loading {1};. {2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
            }

            _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' {1} script loaded;. ", this.ResourceNameCaption, script.Name );
            this.DisplaySubsystem.DisplayLine( 2, "Done loading {0} from file", script.Name );
        }

        #endregion

        #region " TSP SCRIPTS: LOAD "

        /// <summary>
        /// Load the code. Code could be embedded as a comma separated string table format, in which case
        /// the script should be concatenated first.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> Includes the script as a long string. </param>
        public void LoadString( string value )
        {
            if ( value is null )
                throw new ArgumentNullException( nameof( value ) );
            var scriptLines = value.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            this.LoadString( scriptLines );
        }

        /// <summary>
        /// Load the code. Code could be embedded as a comma separated string table format, in which case
        /// the script should be concatenated first.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scriptLines"> Includes the script in lines. </param>
        public void LoadString( string[] scriptLines )
        {
            if ( scriptLines is null )
                throw new ArgumentNullException( nameof( scriptLines ) );
            if ( scriptLines is object )
            {
                foreach ( string line in scriptLines )
                {
                    string scriptLine = line.Trim();
                    if ( !string.IsNullOrWhiteSpace( scriptLine ) )
                    {
                        _ = this.Session.WriteLine( scriptLine );
                    }
                }
            }
        }

        /// <summary> Loads the script embedded in the string. </summary>
        /// <param name="scriptLines"> Contains the script code line by line. </param>
        public void LoadScript( string[] scriptLines )
        {
            try
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "initiating load script for script '{0}';. ", this.Name );
                this.Session.LastNodeNumber = new int?();
                _ = this.Session.WriteLine( "loadscript " + this.Name );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "loading script '{0}';. ", this.Name );
                this.LoadString( scriptLines );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "ending script '{0}';. ", this.Name );
                _ = this.Session.WriteLine( "endscript" );
            }
            catch
            {

                // remove any remaining values.
                this.Session.DiscardUnreadData();
                throw;
            }
        }

        /// <summary> Loads the script embedded in the string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">        Contains the script name. </param>
        /// <param name="scriptLines"> Contains the script code line by line. </param>
        public void LoadScript( string name, string[] scriptLines )
        {
            if ( scriptLines is null )
                throw new ArgumentNullException( nameof( scriptLines ) );
            string firstLine = scriptLines[0];
            // check if we already have the load/end constructs.
            this.Session.LastNodeNumber = new int?();
            if ( firstLine.Contains( name ) || firstLine.Contains( "loadscript" ) )
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "loading script '{0}';. ", name );
                this.Session.LastNodeNumber = new int?();
                this.LoadString( scriptLines );
            }
            else
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "initiating load script for script '{0}';. ", name );
                _ = this.Session.WriteLine( "loadscript " + name );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "loading script lines for script '{0}';. ", name );
                this.LoadString( scriptLines );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "ending script for script '{0}';. ", name );
                _ = this.Session.WriteLine( "endscript waitcomplete()" );
            }
        }

        /// <summary> Loads the script embedded in the string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="name">   Contains the script name. </param>
        /// <param name="source"> Contains the script code line by line. </param>
        public void LoadScript( string name, string source )
        {
            if ( string.IsNullOrWhiteSpace( name ) )
                throw new ArgumentNullException( nameof( name ) );
            if ( string.IsNullOrWhiteSpace( source ) )
                throw new ArgumentNullException( nameof( source ) );
            if ( source.Substring( 0, 50 ).Trim().StartsWith( "{", true, System.Globalization.CultureInfo.CurrentCulture ) )
            {
                source = "loadstring(table.concat(" + source + "))() ";
            }

            var scriptLines = source.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            this.LoadScript( name, scriptLines );
        }

        /// <summary> Loads an anonymous script embedded in the string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="source"> Contains the script code line by line. </param>
        public void LoadScript( string source )
        {
            if ( source is null )
                throw new ArgumentNullException( nameof( source ) );
            if ( source.Substring( 0, 50 ).Trim().StartsWith( "{", true, System.Globalization.CultureInfo.CurrentCulture ) )
            {
                source = "loadstring(table.concat(" + source + "))() ";
            }

            this.LoadString( source );
        }

        #endregion

        #region " TSP X: SCRIPT COLLECTION  - LEGACY "

        /// <summary> The legacy scripts. </summary>
        private ScriptEntityCollection _LegacyScripts;

        /// <summary> Gets the list of legacy scripts. </summary>
        /// <returns> List of legacy scripts. </returns>
        public ScriptEntityCollection LegacyScripts()
        {
            return this._LegacyScripts;
        }

        /// <summary> Adds a new script to the list of legacy scripts. </summary>
        /// <param name="name"> Specifies the name of the script. </param>
        /// <returns> The added script. </returns>
        public ScriptEntityBase AddLegacyScript( string name )
        {
            var script = new ScriptEntity( name, "" );
            this._LegacyScripts.Add( script );
            return script;
        }

        /// <summary> Create a new instance of the legacy scripts. </summary>
        public void NewLegacyScripts()
        {
            this._LegacyScripts = new ScriptEntityCollection();
        }

        #endregion

        #region " TSP X: SCRIPT COLLECTION "

        /// <summary> The scripts. </summary>
        private ScriptEntityCollection _Scripts;

        /// <summary> Gets the list of scripts. </summary>
        /// <returns> List of scripts. </returns>
        public ScriptEntityCollection Scripts()
        {
            return this._Scripts;
        }

        /// <summary> Adds a new script to the list of scripts. </summary>
        /// <param name="name">      Specifies the name of the script. </param>
        /// <param name="modelMask"> Specifies the family of instrument models for this script. </param>
        /// <returns> The added script. </returns>
        public ScriptEntityBase AddScript( string name, string modelMask )
        {
            var script = new ScriptEntity( name, modelMask );
            this._Scripts.Add( script );
            return script;
        }

        /// <summary> Create a new instance of the scripts. </summary>
        public void NewScripts()
        {
            this._Scripts = new ScriptEntityCollection();
        }

        #endregion

        #region " TSP X: SCRIPTS "

        /// <summary>
        /// Checks and returns <c>True</c> if all scripts are loaded on the specified node.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the collection of scripts. </param>
        /// <param name="node">    Specifies the node. </param>
        /// <returns> <c>True</c> if all scripts are loaded on the specified node. </returns>
        public bool AllUserScriptsExist( ScriptEntityCollection scripts, NodeEntityBase node )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( scripts is object && scripts.Count > 0 )
            {
                foreach ( ScriptEntityBase script in scripts )
                {

                    // return <c>False</c> if any script is not loaded.
                    if ( script.IsModelMatch( node.ModelNumber ) && !this.Session.IsNil( node.IsController, node.Number, script.Name ) )
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary> Checks and returns <c>True</c> if all scripts were executed. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the collection of scripts. </param>
        /// <param name="node">    Specifies the node. </param>
        /// <returns> <c>True</c> if all scripts were executed on the specified node. </returns>
        public bool AllUserScriptsExecuted( ScriptEntityCollection scripts, NodeEntityBase node )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( scripts is object && scripts.Count > 0 )
            {
                foreach ( ScriptEntityBase script in scripts )
                {

                    // return <c>False</c> if any script is not loaded.
                    if ( script.IsModelMatch( node.ModelNumber ) && script.Namespaces() is object && script.Namespaces().Length > 0 && this.Session.IsNil( node.IsController, node.Number, script.Namespaces() ) )
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary> Checks and returns <c>True</c> if all scripts are loaded. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the collection of scripts. </param>
        /// <param name="node">    Specifies the node. </param>
        /// <returns> <c>True</c> if all scripts were saved on the specified node. </returns>
        public bool AllUserScriptsSaved( ScriptEntityCollection scripts, NodeEntityBase node )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( scripts is object && scripts.Count > 0 )
            {
                this.FetchSavedScripts( node );
                foreach ( ScriptEntityBase script in scripts )
                {
                    // return <c>False</c> if any script is not saved.
                    if ( script.IsModelMatch( node.ModelNumber ) && !this.SavedScriptExists( script.Name, node, false ) )
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        /// <summary> Returns <c>True</c> if any of the specified scripts exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the collection of scripts. </param>
        /// <param name="node">    Specifies the node. </param>
        /// <returns> <c>True</c> if any of the specified scripts exists. </returns>
        public bool AnyUserScriptExists( ScriptEntityCollection scripts, NodeEntityBase node )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( scripts is object && scripts.Count > 0 )
            {
                foreach ( ScriptEntityBase script in scripts )
                {
                    if ( !string.IsNullOrWhiteSpace( script.Name ) )
                    {
                        if ( script.IsModelMatch( node.ModelNumber ) && !this.Session.IsNil( node.IsController, node.Number, script.Name ) )
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        #endregion

        #region " TSP X: SCRIPTS FIRMWARE "

        /// <summary> Returns the released main firmware version. </summary>
        /// <returns> The released main firmware version. </returns>
        public string FirmwareReleasedVersionGetter()
        {
            return this.LinkSubsystem.ControllerNode is object ? this.FirmwareReleasedVersionGetter( this.LinkSubsystem.ControllerNode ) : this._Scripts[0].ReleasedFirmwareVersion;
        }

        /// <summary> Returns the released main firmware version. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> The released main firmware version. </returns>
        public string FirmwareReleasedVersionGetter( NodeEntityBase node )
        {
            return this.LinkSubsystem.ControllerNode is null
                ? this._Scripts[0].ReleasedFirmwareVersion
                : node is null
                    ? throw new ArgumentNullException( nameof( node ) )
                    : this._Scripts.SelectSerialNumberScript( node ).ReleasedFirmwareVersion;
        }

        /// <summary> Returns the embedded main firmware version. </summary>
        /// <returns> The actual embedded firmware version. </returns>
        public string FirmwareVersionGetter()
        {
            return this.LinkSubsystem.ControllerNode is object ? this.FirmwareVersionGetter( this.LinkSubsystem.ControllerNode ) : "<unknown>";
        }

        /// <summary> Returns the embedded main firmware version. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> The released main firmware version. </returns>
        public string FirmwareVersionGetter( NodeEntityBase node )
        {
            return node is null
                ? throw new ArgumentNullException( nameof( node ) )
                : this._Scripts.SelectSerialNumberScript( node ).EmbeddedFirmwareVersion;
        }

        /// <summary> Returns the main firmware name from the controller node. </summary>
        /// <returns> The firmware name. </returns>
        public string FirmwareNameGetter()
        {
            return this.FirmwareNameGetter( this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Returns the node firmware name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> The node firmware name. </returns>
        public string FirmwareNameGetter( NodeEntityBase node )
        {
            return node is null ? throw new ArgumentNullException( nameof( node ) ) : this._Scripts.SelectSerialNumberScript( node ).Name;
        }

        /// <summary> The firmware exists. </summary>
        private bool? _FirmwareExists;

        /// <summary> Gets or sets the firmware exists. </summary>
        /// <value> The firmware exists. </value>
        public bool? FirmwareExists
        {
            get => this._FirmwareExists;

            set {
                if ( !Nullable.Equals( value, this.FirmwareExists ) )
                {
                    this._FirmwareExists = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Checks if firmware exists on the controller node. </summary>
        /// <returns> <c>True</c> if the firmware exists; otherwise, <c>False</c>. </returns>
        public bool FindFirmware()
        {
            return this.FindFirmware( this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Checks if the main firmware exists . </summary>
        /// <remarks> Value is cached in the <see cref="FirmwareExists">sentinel</see> </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if the firmware exists; otherwise, <c>False</c>. </returns>
        public bool FindFirmware( NodeEntityBase node )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            this._FirmwareExists = !this.Session.IsNil( this.FirmwareNameGetter( node ) );
            return this._FirmwareExists.Value;
        }

        /// <summary> Returns the Support firmware name. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if the support firmware exists; otherwise, <c>False</c>. </returns>
        public string SupportFirmwareNameGetter( NodeEntityBase node )
        {
            return node is null ? throw new ArgumentNullException( nameof( node ) ) : this._Scripts.SelectSupportScript( node ).Name;
        }

        /// <summary> The supportfirmware exists. </summary>
        private bool? _SupportfirmwareExists;

        /// <summary> Gets or sets the firmware exists. </summary>
        /// <value> The firmware exists. </value>
        public bool? SupportFirmwareExists1
        {
            get => this._SupportfirmwareExists;

            set {
                if ( !Nullable.Equals( value, this.SupportFirmwareExists1 ) )
                {
                    this._SupportfirmwareExists = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Checks if the Support firmware exists on the controller node. </summary>
        /// <returns> <c>True</c> if the support firmware exists; otherwise, <c>False</c>. </returns>
        public bool FindSupportFirmware()
        {
            return this.FindSupportFirmware( this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Checks if the Support firmware exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if the support firmware exists; otherwise, <c>False</c>. </returns>
        public bool FindSupportFirmware( NodeEntityBase node )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            this._SupportfirmwareExists = !this.Session.IsNil( this.SupportFirmwareNameGetter( node ) );
            return this._SupportfirmwareExists.Value;
        }

        /// <summary> Reads the firmware versions of the controller node. </summary>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool ReadFirmwareVersions()
        {
            return this._Scripts.ReadFirmwareVersions( this.LinkSubsystem.ControllerNode, this.Session );
        }

        /// <summary> Reads the firmware versions of the controller node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool ReadFirmwareVersions( NodeEntityBase node )
        {
            return node is null ? throw new ArgumentNullException( nameof( node ) ) : this._Scripts.ReadFirmwareVersions( node, this.Session );
        }

        /// <summary> Checks if all scripts were saved. </summary>
        /// <param name="refreshScriptCatalog"> Specifies the condition for updating the catalog of saved
        /// scripts before checking the status of these scripts.
        /// </param>
        /// <param name="node">                 Specifies the node. </param>
        /// <returns> <c>True</c> if all scripts were saved; otherwise, <c>False</c>. </returns>
        public bool AllScriptsSaved( bool refreshScriptCatalog, NodeEntityBase node )
        {
            return node is null || this._Scripts.FindSavedScripts( node, this, refreshScriptCatalog );
        }

        /// <summary> Checks if all scripts were saved. </summary>
        /// <param name="refreshScriptCatalog"> Specifies the condition for updating the catalog of saved
        /// scripts before checking the status of these scripts.
        /// </param>
        /// <returns> <c>True</c> if all scripts were saved; otherwise, <c>False</c>. </returns>
        public bool AllScriptsSaved( bool refreshScriptCatalog )
        {
            return this.AllScriptsSaved( refreshScriptCatalog, this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Checks if all scripts exist. </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if all scripts exist; otherwise, <c>False</c>. </returns>
        public bool AllScriptsExist( NodeEntityBase node )
        {
            return node is null || this._Scripts.FindScripts( node, this.Session );
        }

        /// <summary> Checks if all scripts exist. </summary>
        /// <returns> <c>True</c> if all scripts exist; otherwise, <c>False</c>. </returns>
        public bool AllScriptsExist()
        {
            return this.AllScriptsExist( this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Checks if any script exists on the controller node. </summary>
        /// <returns> <c>True</c> if any script exists; otherwise, <c>False</c>. </returns>
        public bool AnyScriptExists()
        {
            return this.AnyScriptExists( this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Checks if any script exists. </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if any script exists; otherwise, <c>False</c>. </returns>
        public bool AnyScriptExists( NodeEntityBase node )
        {
            return node is object && this._Scripts.FindAnyScript( node, this.Session );
        }

        /// <summary> Returns <c>True</c> if any legacy scripts exist on the controller node. </summary>
        /// <returns> <c>True</c> if any legacy script exists; otherwise, <c>False</c>. </returns>
        public bool AnyLegacyScriptExists()
        {
            return this.AnyLegacyScriptExists( this.LinkSubsystem.ControllerNode );
        }

        /// <summary> Returns <c>True</c> if any legacy scripts exist. </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if any legacy script exists; otherwise, <c>False</c>. </returns>
        public bool AnyLegacyScriptExists( NodeEntityBase node )
        {
            return node is object && this._LegacyScripts.FindAnyScript( node, this.Session );
        }

        #endregion

        #region " TSP X: DELETE SCRIPTS "

        /// <summary> Check the script version and determines if the script needs to be deleted. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="script">                    Specifies the
        /// <see cref="ScriptEntityBase">script</see>to delete.
        /// </param>
        /// <param name="node">                      Specifies the system node. </param>
        /// <param name="allowDeletingNewerScripts"> true to allow, false to deny deleting newer scripts. </param>
        /// <returns>
        /// Returns <c>True</c> if script was deleted or did not exit. Returns <c>False</c> if a VISA
        /// error was encountered. Assumes that the list of saved scripts is current.
        /// </returns>
        public bool IsDeleteUserScriptRequired( ScriptEntityBase script, NodeEntityBase node, bool allowDeletingNewerScripts )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( this.Session.IsNil( node.IsController, node.Number, script.Name ) )
            {
                // ignore error if nil
                if ( !this.TraceVisaDeviceOperationOkay( node.Number, "looking for script '{0}'. Ignoring error;. ", script.Name ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) looking for script {1} on node {2};. nothing to do.", this.ResourceNameCaption, script.Name, node.Number );
                }
                // return false, script does not exists.
                return false;
            }
            // check if can read version 
            else if ( this.Session.IsNil( node.IsController, node.Number, script.Namespaces() ) )
            {

                // reading version requires an intact namespace. A missing name space may be missing, this might 
                // indicate that referenced scripts were deleted or failed loading so this script should be deleted.
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' reports that some namespaces '{1}' on node {2} are nil;. script '{3}' will be deleted.", this.ResourceNameCaption, script.Namespaces(), node.Number, script.Name );
                return true;
            }
            else if ( !script.FirmwareVersionGetterExists( node, this.Session ) )
            {

                // reading version requires a supported version function. Delete if a firmware version function is not
                // defined.
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' firmware version function not defined;. script '{1}' will be deleted.", this.ResourceNameCaption, script.Name );
                return true;
            }

            // read the script firmware version
            _ = script.QueryFirmwareVersion( node, this.Session );
            var validation = script.ValidateFirmware();
            switch ( validation )
            {
                case FirmwareVersionStatus.None:
                    {
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' '{1}' script firmware version on node {2} is irrelevant;. script will be deleted.", this.ResourceNameCaption, script.Name, node.Number );
                        return true;
                    }

                case FirmwareVersionStatus.Current:
                    {
                        _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} on node {2} is up to date;. Nothing to do.", this.ResourceNameCaption, script.Name, node.Number );
                        return false;
                    }

                case FirmwareVersionStatus.Missing:
                    {
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' custom firmware '{1}' version on node {2} is not known;. script version function is not defined. Script will be deleted.", this.ResourceNameCaption, script.Name, node.Number );
                        return true;
                    }

                case FirmwareVersionStatus.Newer:
                    {
                        if ( allowDeletingNewerScripts )
                        {
                            _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' existing custom firmware '{1}' on node {2} version '{3}' is newer than the specified version '{4}';. The scripts will be deleted to allow uploading the older script.", this.ResourceNameCaption, script.Name, node.Number, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion );
                            return true;
                        }
                        else
                        {
                            _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' existing custom firmware '{1}' on node {2} version '{3}' is newer than the specified version '{4}';. A newer version of the program is required. Script will not be deleted.", this.ResourceNameCaption, script.Name, node.Number, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion );
                            return false;
                        }

                    }

                case FirmwareVersionStatus.Older:
                    {
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' existing custom firmware '{1}' on node {2} version '{3}' is older than the specified version '{4}';. Script will be deleted.", this.ResourceNameCaption, script.Name, node.Number, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion );
                        return true;
                    }

                case FirmwareVersionStatus.ReferenceUnknown:
                    {
                        _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' custom firmware '{1}' released version not given;. Script will not be deleted.{2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                        return false;
                    }

                case FirmwareVersionStatus.Unknown:
                    {
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' firmware '{1}' on node {2} version was not read;. Script will be deleted.", this.ResourceNameCaption, script.Name, node.Number );
                        return true;
                    }

                default:
                    {
                        _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' encountered unhandled firmware version status {1} on node {2};. Nothing to do. Ignored.", this.ResourceNameCaption, validation, node.Number );
                        return false;
                    }
            }

        }

        /// <summary> Checks if delete is required on any user script. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts">               Specifies the list of the scripts to be deleted. </param>
        /// <param name="node">                  Specifies the node. </param>
        /// <param name="refreshScriptsCatalog"> Refresh catalog before checking if script exists. </param>
        /// <returns> <c>True</c> if delete is required on any user script. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool IsDeleteUserScriptRequired( ScriptEntityCollection scripts, NodeEntityBase node, bool refreshScriptsCatalog )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            // <c>True</c> if any delete action was executed
            // Dim scriptsDeleted As Boolean = False

            if ( scripts is object && scripts.Count > 0 )
            {
                if ( refreshScriptsCatalog )
                {
                    this.FetchSavedScripts( node );
                }

                // deletion of scripts must be done in reverse order.
                for ( int i = scripts.Count - 1; i >= 0; i -= 1 )
                {
                    var script = scripts[i];
                    if ( script.IsModelMatch( node.ModelNumber ) )
                    {
                        try
                        {
                            if ( !script.IsBootScript && this.IsDeleteUserScriptRequired( script, node, scripts.AllowDeletingNewerScripts ) )
                            {

                                // stop in design time to make sure delete is not incorrect.
                                Debug.Assert( !Debugger.IsAttached, "ARE YOU SURE?" );
                                return this.IsDeleteUserScriptRequired( script, node, scripts.AllowDeletingNewerScripts );
                            }
                        }
                        catch ( Exception ex )
                        {
                            _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred testing if delete is required for firmware {0} from node {1};. {2}", script.Name, node.Number, ex.ToFullBlownString() );
                        }
                    }
                }
            }

            return false;
        }

        /// <summary> Checks if delete is required on any node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts">               Specifies the list of the scripts to be deleted. </param>
        /// <param name="nodes">                 Specifies the list of nodes on which scripts are deleted. </param>
        /// <param name="refreshScriptsCatalog"> Refresh catalog before checking if script exists. </param>
        /// <returns> <c>True</c> if delete is required on any node. </returns>
        public bool IsDeleteUserScriptRequired( ScriptEntityCollection scripts, NodeEntityCollection nodes, bool refreshScriptsCatalog )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( nodes is null )
                throw new ArgumentNullException( nameof( nodes ) );

            // clear buffers before deleting.
            this.Session.DiscardUnreadData();
            foreach ( NodeEntityBase node in nodes )
            {
                if ( this.IsDeleteUserScriptRequired( scripts, node, refreshScriptsCatalog ) )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary> Deletes the user script. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="script">                Specifies the <see cref="ScriptEntityBase">script</see>to
        /// delete. </param>
        /// <param name="node">                  Specifies the system node. </param>
        /// <param name="refreshScriptsCatalog"> Refresh catalog before checking if script exists. </param>
        /// <returns>
        /// Returns <c>True</c> if script was deleted or did not exit. Returns <c>False</c> if deletion
        /// failed.
        /// </returns>
        public bool DeleteUserScript( ScriptEntityBase script, NodeEntityBase node, bool refreshScriptsCatalog )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( this.Session.IsNil( node.IsController, node.Number, script.Name ) )
            {
                // ignore error if nil
                if ( !this.TraceVisaDeviceOperationOkay( node.Number, "looking for script '{0}'. Ignoring error;. ", script.Name ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) looking for script {1} on node;. Nothing to do.", this.ResourceNameCaption, script.Name, node.Number );
                }

                script.IsDeleted = true;
                return true;
            }

            this.DisplaySubsystem.DisplayLine( 2, "Deleting {0}:{1}", node.Number, script.Name );
            if ( node.IsController )
            {
                this.DeleteScript( script.Name, refreshScriptsCatalog );
                if ( !this.TraceVisaDeviceOperationOkay( false, "deleting {0};. ", script.Name ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) deleting script {1} on node {2};. ", this.ResourceNameCaption, script.Name, node.Number );
                    return false;
                }

                // do a garbage collection
                _ = this.StatusSubsystem.CollectGarbageWaitComplete( this.DeleteTimeout, "collecting garbage--ignoring error;. " );
            }
            else
            {
                if ( this.DeleteScript( node, script.Name, refreshScriptsCatalog ) )
                {
                    if ( !this.TraceVisaDeviceOperationOkay( node.Number, "deleting script {0};. ", script.Name ) )
                    {
                        _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) deleting {1} on node {2};. ", this.ResourceNameCaption, script.Name, node.Number );
                        return false;
                    }
                }
                else
                {
                    if ( !this.TraceVisaDeviceOperationOkay( node.Number, "deleting script {0};. ", script.Name, node.Number ) )
                    {
                        // report failure if not an instrument or VISA error (handler returns Okay.)
                        _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) deleting {1} on node {2};. {3}{4}", this.ResourceNameCaption, script.Name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    }

                    return false;
                }

                // do a garbage collection
                _ = this.LinkSubsystem.CollectGarbageWaitComplete( node, this.DeleteTimeout, "collecting garbage on node {0}--ignoring error;. ", ( object ) node.Number );
            }

            script.IsDeleted = true;
            return true;
        }

        /// <summary>
        /// Deletes user scripts that are out-dated or where a deletion is set for the script.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts">               Specifies the list of the scripts to be deleted. </param>
        /// <param name="node">                  Specifies the node. </param>
        /// <param name="refreshScriptsCatalog"> Refresh catalog before checking if script exists. </param>
        /// <param name="deleteOutdatedOnly">    if set to <c>True</c> deletes only if scripts is out of
        /// date. </param>
        /// <returns> <c>True</c> if success, <c>False</c> otherwise. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool DeleteUserScripts( ScriptEntityCollection scripts, NodeEntityBase node, bool refreshScriptsCatalog, bool deleteOutdatedOnly )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            bool success = true;

            // <c>True</c> if any delete action was executed
            bool scriptsDeleted = false;
            if ( scripts is object && scripts.Count > 0 )
            {
                if ( refreshScriptsCatalog )
                {
                    this.FetchSavedScripts( node );
                }

                // deletion of scripts must be done in reverse order.
                for ( int i = scripts.Count - 1; i >= 0; i -= 1 )
                {
                    var script = scripts[i];
                    if ( script.IsModelMatch( node.ModelNumber ) )
                    {
                        try
                        {
                            if ( !script.IsDeleted && (script.RequiresDeletion || !deleteOutdatedOnly || this.IsDeleteUserScriptRequired( script, node, scripts.AllowDeletingNewerScripts )) )
                            {
                                if ( this.DeleteUserScript( script, node, false ) )
                                {

                                    // mark that scripts were deleted, i.e., that any script was deleted 
                                    // or if a script that existed no longer exists.
                                    scriptsDeleted = true;
                                }
                                else
                                {
                                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "failed deleting script '{1}' from node {2};. ", this.ResourceNameCaption, script.Name, node.Number );
                                    success = false;
                                }
                            }
                        }
                        catch ( Exception ex )
                        {
                            try
                            {
                                success = success && this.Session.IsNil( script.Name );
                                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred deleting firmware {0} from node {1};. {2}", script.Name, node.Number, ex.ToFullBlownString() );
                            }
                            catch
                            {
                                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception occurred checking existence after attempting deletion of firmware {0} from node {1};. {2}", script.Name, node.Number, ex.ToFullBlownString() );
                                success = false;
                            }
                        }
                    }
                }
            }

            if ( scriptsDeleted )
            {
                // reset to refresh the instrument display.
                _ = this.LinkSubsystem.ResetNode( node );
            }

            return success;
        }

        /// <summary> Deletes user scripts from the remote instrument. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts">               Specifies the list of the scripts to be deleted. </param>
        /// <param name="nodes">                 Specifies the list of nodes on which scripts are deleted. </param>
        /// <param name="refreshScriptsCatalog"> Refresh catalog before checking if script exists. </param>
        /// <param name="deleteOutdatedOnly">    if set to <c>True</c> deletes only if scripts is out of
        /// date. </param>
        /// <returns> <c>True</c> if success, <c>False</c> otherwise. </returns>
        public bool DeleteUserScripts( ScriptEntityCollection scripts, NodeEntityCollection nodes, bool refreshScriptsCatalog, bool deleteOutdatedOnly )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( nodes is null )
                throw new ArgumentNullException( nameof( nodes ) );

            // clear buffers before deleting.
            this.Session.DiscardUnreadData();
            bool success = true;
            foreach ( NodeEntityBase node in nodes )
                success &= this.DeleteUserScripts( scripts, node, refreshScriptsCatalog, deleteOutdatedOnly );
            return success;
        }

        #endregion

        #region " LOAD AND RUN SCRIPTS; Extended methods with error management"

        /// <summary> Loads and executes the specified TSP script from file. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="script"> Specifies reference to a valid <see cref="ScriptEntity">script</see> </param>
        /// <param name="node">   Specifies the node. </param>
        public void LoadRunUserScript( ScriptEntityBase script, NodeEntityBase node )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( this.Session.IsNil( node.IsController, node.Number, script.Name ) )
            {
                if ( string.IsNullOrWhiteSpace( script.Source ) && script.Source.Length > 10 )
                {
                    this.DisplaySubsystem.DisplayLine( 2, "Attempted loading empty script {0}:{1}", node.Number, script.Name );
                    throw new Core.OperationFailedException( "Attempted loading empty script;. {0}:{1}", node.Number, script.Name );
                }

                if ( node.IsController )
                {
                    if ( this.LoadUserScript( script ) )
                    {
                        if ( !this.RunUserScript( script ) )
                        {
                            throw new Core.OperationFailedException( "Failed running script;. {0}:{1}", node.Number, script.Name );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( "Failed loading script;. {0}:{1}", node.Number, script.Name );
                    }
                }
                else
                {
                    this.LoadUserScript( node, script );
                    if ( !this.RunUserScript( node, script ) )
                    {
                        throw new Core.OperationFailedException( "Failed running script;. {0}:{1}", node.Number, script.Name );
                    }
                }
            }
        }

        /// <summary> Loads and runs the user scripts on the controller instrument. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the collection of scripts. </param>
        /// <param name="node">    Specifies the node. </param>
        /// <returns> The run user scripts. </returns>
        public bool LoadRunUserScripts( ScriptEntityCollection scripts, NodeEntityBase node )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );

            // reset all status values so as to force a read.
            this.DefineKnownResetState();

            // <c>True</c> if any load and run action was executed
            bool resetRequired = false;
            if ( scripts is object && scripts.Count > 0 )
            {
                foreach ( ScriptEntityBase script in scripts )
                {
                    if ( script.IsModelMatch( node.ModelNumber ) )
                    {

                        // reset if a new script will be loaded.
                        resetRequired = resetRequired || this.Session.IsNil( node.IsController, node.Number, script.Name );
                        this.LoadRunUserScript( script, node );
                    }
                }
            }

            if ( resetRequired )
            {
                // reset to refresh the instrument display.
                _ = this.LinkSubsystem.ResetNode( node );
            }

            return true;
        }

        /// <summary>
        /// Loads and runs the user scripts on the controller instrument for the specified node.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the collection of scripts. </param>
        /// <param name="nodes">   Specifies the nodes. </param>
        /// <returns> The run user scripts. </returns>
        public bool LoadRunUserScripts( ScriptEntityCollection scripts, NodeEntityCollection nodes )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( nodes is null )
                throw new ArgumentNullException( nameof( nodes ) );

            // clear buffers before deleting.
            this.Session.DiscardUnreadData();
            foreach ( NodeEntityBase node in nodes )
            {
                if ( !this.LoadRunUserScripts( scripts, node ) )
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary> Copies a script source. </summary>
        /// <remarks>
        /// For binary scripts, the controller and remote nodes must be binary compatible.
        /// </remarks>
        /// <param name="sourceName">      The name of the source script. </param>
        /// <param name="destinationName"> The name of the destination script. </param>
        public void CopyScript( string sourceName, string destinationName )
        {
            _ = this.Session.WriteLine( "{1}=script.new( {0}.source , '{1}' ) waitcomplete()", sourceName, destinationName );
        }

        /// <summary> Copies a script from the controller node to a remote node. </summary>
        /// <remarks>
        /// For binary scripts, the controller and remote nodes must be binary compatible.
        /// </remarks>
        /// <param name="nodeNumber">      . </param>
        /// <param name="sourceName">      The script name on the controller node. </param>
        /// <param name="destinationName"> The script name on the remote node. </param>
        public void CopyScript( int nodeNumber, string sourceName, string destinationName )
        {

            // loads and runs the specified script.
            string commands = string.Format( System.Globalization.CultureInfo.CurrentCulture, "node[{0}].execute('waitcomplete() {2}=script.new({1}.source,[[{2}]])') waitcomplete({0}) waitcomplete()", nodeNumber, sourceName, destinationName );
            this.LoadString( commands );
        }

        /// <summary> Copies script source from one script to another. </summary>
        /// <remarks>
        /// For binary scripts, the controller and remote nodes must be binary compatible.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">            Specifies a node on which to copy. </param>
        /// <param name="sourceName">      The script name on the controller node. </param>
        /// <param name="destinationName"> The script name on the remote node. </param>
        public void CopyScript( NodeEntityBase node, string sourceName, string destinationName )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( node.IsController )
            {
                this.CopyScript( sourceName, destinationName );
            }
            else
            {
                this.CopyScript( node.Number, sourceName, destinationName );
            }
        }

        /// <summary> Loads and runs an anonymous script. </summary>
        /// <param name="commands"> Specifies the script commands. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool LoadRunAnonymousScript( string commands )
        {
            string prefix = "loadandrunscript";
            string suffix = "endscript waitcomplete()";
            string loadCommand = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{1}{0}{2}{0}{3}", Environment.NewLine, prefix, commands, suffix );
            this.LoadString( loadCommand );
            return true;
        }

        /// <summary> Builds a script for loading a script to the remote node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber">        Specifies the remote node number. </param>
        /// <param name="script">            Specifies the script. </param>
        /// <param name="loadingScriptName"> Specifies the name of the loading script that will be deleted
        /// after the process is done. </param>
        /// <returns> The script with the load and end commands. </returns>
        private static System.Text.StringBuilder BuildScriptLoaderScript( int nodeNumber, ScriptEntityBase script, string loadingScriptName )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            string prefix = "loadstring(table.concat(";
            string suffix = "))()";
            bool binaryDecorationRequire = script.IsBinaryScript && !script.Source.Contains( prefix );
            var loadCommands = new System.Text.StringBuilder( script.Source.Length + 512 );
            _ = loadCommands.AppendFormat( "loadandrunscript {0}", loadingScriptName );
            _ = loadCommands.AppendLine();
            _ = loadCommands.AppendLine( "do" );
            _ = loadCommands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "node[{0}].dataqueue.add([[", nodeNumber );
            if ( binaryDecorationRequire )
            {
                _ = loadCommands.Append( prefix );
            }

            _ = loadCommands.AppendLine( script.Source );
            if ( binaryDecorationRequire )
            {
                _ = loadCommands.AppendLine( suffix );
            }

            _ = loadCommands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "]]) waitcomplete()", nodeNumber );
            _ = loadCommands.AppendLine();
            _ = loadCommands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "node[{0}].execute([[waitcomplete() {1}=script.new(dataqueue.next(),'{1}')]])", nodeNumber, script.Name );
            _ = loadCommands.AppendLine();
            _ = loadCommands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "waitcomplete({0})", nodeNumber );
            _ = loadCommands.AppendLine( " waitcomplete()" );
            _ = loadCommands.AppendLine( "end" );
            _ = loadCommands.AppendLine( "endscript" );
            return loadCommands;
        }

        /// <summary> Create a new script on the remote node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">   The node. </param>
        /// <param name="script"> . </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool UploadScript( NodeEntityBase node, ScriptEntityBase script )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            this.Session.LastNodeNumber = node.Number;
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "loading script '{0}';. ", script.Name );
            bool affirmative = true;
            if ( (script.IsBinaryScript || !this.Session.IsNil( script.Name ) && this.IsBinaryScript( script.Name, this.LinkSubsystem.ControllerNode ).GetValueOrDefault( false )) == true )
            {
                string tempName = "isr_temp";
                try
                {
                    // clear the data queue.
                    this.LinkSubsystem.ClearDataQueue( node.Number );
                    string scriptLoaderScript = BuildScriptLoaderScript( node.Number, script, tempName ).ToString();

                    // load and ran the temporary script. 
                    this.LoadString( scriptLoaderScript );

                    // enable wait completion (the wait complete command is included in the loader script
                    this.Session.EnableWaitComplete();
                    _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( script.Timeout ).Status );
                    affirmative = this.TraceVisaDeviceOperationOkay( node.Number, "loading script '{0}';. ", tempName );
                }
                catch ( Pith.NativeException ex )
                {
                    this.TraceVisaOperation( ex, node.Number, "loading script '{0}';. ", tempName );
                    affirmative = false;
                }
                catch ( Exception ex )
                {
                    this.TraceOperation( ex, node.Number, "loading script '{0}';. ", tempName );
                    affirmative = false;
                }

                if ( affirmative )
                {
                    try
                    {
                        // remove the temporary script if there.
                        if ( !this.Session.IsNil( tempName ) )
                        {
                            this.DeleteScript( tempName, false );
                        }

                        _ = this.TraceVisaDeviceOperationOkay( node.Number, "deleting script '{0}';. ", tempName );
                    }
                    catch ( Pith.NativeException ex )
                    {
                        this.TraceVisaOperation( ex, node.Number, "deleting script '{0}';. ", tempName );
                    }
                    catch ( Exception ex )
                    {
                        this.TraceOperation( ex, node.Number, "deleting script '{0}';. ", tempName );
                    }
                }
            }

            // if scripts is already stored in the controller node in non-binary format, just
            // copy it (upload) from the controller to the remote node.
            else if ( !this.UploadScript( node, script.Name, script.Timeout ) )
            {
                affirmative = false;
            }

            if ( !affirmative )
                return affirmative;

            // verify that the script was loaded.
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Verifying script '{0}' loaded on node {1};. ", this.Name, node.Number );
            this.Session.LastNodeNumber = node.Number;

            // check if the script short name exists.
            if ( this.Session.WaitNotNil( node.Number, script.Name, script.Timeout ) )
            {
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' loaded script '{1}' to node {2};. ", this.ResourceNameCaption, script.Name, node.Number );
                affirmative = true;
            }
            else
            {

                // if script short name not found, check to see if the script long name is found.
                string fullName = "script.user.scripts." + script.Name;
                if ( this.Session.WaitNotNil( node.Number, fullName, script.Timeout ) )
                {

                    // in design mode, assert a problem.
                    // 3783. this was asserted the first time after upgrading the 3706 to firmware 1.32.a
                    Debug.Assert( !Debugger.IsAttached, "Failed setting script short name" );

                    // assign the new script name
                    string message = this.Session.ExecuteCommand( node.Number, "{0} = {1} waitcomplete() ", script.Name, fullName );
                    this.CheckThrowDeviceException( false, message );
                    this.LinkSubsystem.WaitComplete( node.Number, this.SaveTimeout, false );
                    this.CheckThrowDeviceException( false, "uploading script '{0}' using '{1}';. ", this.Name, message );
                    if ( this.Session.WaitNotNil( node.Number, script.Name, script.Timeout ) )
                    {
                        affirmative = true;
                    }
                    else
                    {
                        _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "failed referencing script '{1}' using '{2}' on node {3};. --new script not found on the remote node.{4}{5}", this.ResourceNameCaption, script.Name, fullName, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                        affirmative = false;
                    }
                }
                else
                {

                    // if both long and short names not found, report failure.
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "failed uploading script '{1}' to node {2} from script '{3}';. --new script not found on the remote node.{4}{5}", this.ResourceNameCaption, fullName, node.Number, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    affirmative = false;
                }
            }

            this.Session.LastNodeNumber = new int?();
            return affirmative;
        }

        /// <summary>
        /// Uploads a script from the controller node to a remote node using the same name on the remote
        /// node. Does not require having the ISR Support script on the controller node.
        /// </summary>
        /// <remarks>
        /// For binary scripts, the controller and remote nodes must be binary compatible.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">       The node. </param>
        /// <param name="scriptName"> The script name on the controller node. </param>
        /// <param name="timeout">    Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool UploadScript( NodeEntityBase node, string scriptName, TimeSpan timeout )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( string.IsNullOrWhiteSpace( scriptName ) )
                throw new ArgumentNullException( nameof( scriptName ) );

            // NOTE: WAIT COMPLETE is required on the system before a wait complete is tested on the node
            // otherwise getting error 1251.

            var commands = new System.Text.StringBuilder( 1024 );
            if ( this.Session.IsNil( this.Scripts().SelectSupportScript( this.LinkSubsystem.ControllerNode ).Namespaces() ) )
            {

                // loads and runs the specified script.
                _ = commands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "node[{0}].dataqueue.add({1}.source) waitcomplete()", node.Number, scriptName );
                _ = commands.AppendLine();
                _ = commands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "node[{0}].execute('waitcomplete() {1}=script.new(dataqueue.next(),[[{1}]])')", node.Number, scriptName );
            }
            else
            {
                _ = commands.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "isr.script.uploadScript(node[{0}],{1})", node.Number, scriptName );
            }

            _ = commands.AppendLine();
            _ = commands.AppendLine( "waitcomplete(0)" );
            bool affirmative = false;
            try
            {
                this.LoadString( commands.ToString() );
                this.LinkSubsystem.EnableWaitComplete( 0 );
                _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( timeout ).Status );
                affirmative = this.TraceVisaDeviceOperationOkay( node.Number, "timeout uploading script '{0}';. ", scriptName );
            }
            catch ( Pith.NativeException ex )
            {
                this.TraceVisaOperation( ex, node.Number, "timeout uploading script '{0}';. ", scriptName );
            }
            catch ( Exception ex )
            {
                this.TraceOperation( ex, node.Number, "timeout uploading script '{0}';. ", scriptName );
            }

            return affirmative;
        }

        /// <summary> Loads the specified script from the local to the remote node. </summary>
        /// <remarks> Will not load a script list that includes the create script command. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">   Specifies the node. </param>
        /// <param name="script"> Specifies reference to a valid
        /// <see cref="ScriptEntity">script</see> </param>
        public void LoadUserScript( NodeEntityBase node, ScriptEntityBase script )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            this.Session.LastNodeNumber = node.Number;
            if ( !this.Session.IsNil( node.Number, script.Name ) )
            {
                this.DisplaySubsystem.DisplayLine( 2, "{0}:{1} exists--nothing to do", node.Number, script.Name );
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} already exists on node {2};. Nothing to do.", this.ResourceNameCaption, script.Name, node.Number );
                return;
            }

            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Uploading {0}:{1}", node.Number, script.Name );
            this.DisplaySubsystem.DisplayLine( 2, "Uploading {0}:{1}", node.Number, script.Name );
            if ( !this.UploadScript( node, script ) )
            {
                this.DisplaySubsystem.DisplayLine( 2, "Failed uploading {0}:{1}", node.Number, script.Name );
                return;
            }

            this.DisplaySubsystem.DisplayLine( 2, "Verifying {0}:{1}", node.Number, script.Name );
            if ( !this.Session.WaitNotNil( node.Number, script.Name, this._SaveTimeout ) )
            {
                this.DisplaySubsystem.DisplayLine( 2, "{0}:{1} not found after loading", node.Number, script.Name );
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "failed loading script '{1}' to node {2};. --new script not found on the remote node.{3}{4}", this.ResourceNameCaption, script.Name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                return;
            }

            // do a garbage collection
            this.DisplaySubsystem.DisplayLine( 2, "Cleaning local node" );
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Collecting garbage after uploading {0}:{1}", node.Number, script.Name );

            // do a garbage collection
            _ = this.StatusSubsystem.CollectGarbageWaitComplete( script.Timeout, "Collecting garbage after uploading {0}:{1}", node.Number, script.Name );

            // do a garbage collection on remote node
            this.DisplaySubsystem.DisplayLine( 2, "Cleaning node {0}", ( object ) node.Number );
            _ = this.LinkSubsystem.CollectGarbageWaitComplete( node, script.Timeout, "Collecting garbage after uploading {0}:{1}", node.Number, script.Name );
            this.DisplaySubsystem.DisplayLine( 2, "{0}:{1} Loaded", node.Number, script.Name );
        }

        /// <summary> Loads the specified TSP script from code. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="script"> Specifies reference to a valid <see cref="ScriptEntity">script</see> </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool LoadUserScript( ScriptEntityBase script )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( !this.Session.IsNil( script.Name ) )
            {
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} already exists;. ", this.ResourceNameCaption, script.Name );
                return true;
            }

            bool affirmative;
            try
            {
                this.DisplaySubsystem.DisplayLine( 2, "Loading {0}", script.Name );
                affirmative = this.TraceVisaDeviceOperationOkay( false, "Ready to load {0};. ", script.Name );
                if ( !affirmative )
                {
                    // report failure if not an instrument or VISA error (handler returns Okay.)
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) before loading {1};. {2}Problem ignored @{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }
                // ignore device errors.
                affirmative = true;
            }
            catch ( Pith.NativeException ex )
            {
                this.TraceVisaOperation( ex, "Ready to load {0};. ", script.Name );
                affirmative = false;
            }
            catch ( Exception ex )
            {
                this.TraceOperation( ex, "Ready to load {0};. ", script.Name );
                affirmative = false;
            }

            try
            {
                if ( affirmative )
                {
                    this.LoadScript( script.Name, script.Source );
                    affirmative = this.TraceVisaDeviceOperationOkay( false, "loading {0};. ", script.Name );
                    if ( !affirmative )
                    {
                        // report failure if not an instrument or VISA error (handler returns Okay.)
                        _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) loading {1};. {2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    }
                }
            }
            catch ( Pith.NativeException ex )
            {
                this.TraceVisaOperation( ex, "loading {0};. ", script.Name );
                affirmative = false;
            }
            catch ( Exception ex )
            {
                this.TraceOperation( ex, "loading {0};. ", script.Name );
                affirmative = false;
            }

            if ( affirmative )
            {
                // do a garbage collection
                if ( !this.StatusSubsystem.CollectGarbageWaitComplete( script.Timeout, "collecting garbage;. " ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Ignoring instrument '{0}' error(s) collecting garbage after loading {1};. {2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }

                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' {1} script loaded;. ", this.ResourceNameCaption, script.Name );
                this.DisplaySubsystem.DisplayLine( 2, "{0} Loaded", script.Name );
            }

            return affirmative;
        }

        /// <summary> Executes the specified TSP script from file. </summary>
        /// <param name="script"> Specifies reference to a valid <see cref="ScriptEntity">script</see> </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool RunUserScript( ScriptEntityBase script )
        {
            return this.RunUserScript( script, true );
        }

        /// <summary> Executes the specified TSP script from file. </summary>
        /// <remarks>
        /// David, 2009-05-13. Modified to run irrespective of the existence of the name spaces because a
        /// new script can be loaded on top of existing old code and the new version number will not
        /// materialize.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="script">    Specifies reference to a valid <see cref="ScriptEntity">script</see> </param>
        /// <param name="runAlways"> true to run always. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool RunUserScript( ScriptEntityBase script, bool runAlways )
        {
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( this.Session.IsNil( script.Name ) )
            {
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "failed running {1} because it does not exist;. {2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                return false;
            }

            // taken out to run always.
            if ( !runAlways && script.Namespaces() is object && script.Namespaces().Length > 0 && !this.Session.IsNil( script.Namespaces() ) )
            {
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} already run. Nothing to do;. ", this.ResourceNameCaption, script.Name );
                return true;
            }

            bool affirmative;
            try
            {
                this.DisplaySubsystem.DisplayLine( 2, "Running {0}", script.Name );
                this.RunScript( script.Name, script.Timeout );
                this.DisplaySubsystem.DisplayLine( 2, "Done running {0}", script.Name );
                affirmative = this.TraceVisaDeviceOperationOkay( false, "running {0};. ", script.Name );
                if ( !affirmative )
                {
                    // report failure if not an instrument or VISA error (handler returns Okay.)
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) running {1};. {2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }
            }
            catch ( Pith.NativeException ex )
            {
                this.TraceVisaOperation( ex, "running {0};. ", script.Name );
                affirmative = false;
            }
            catch ( Exception ex )
            {
                this.TraceOperation( ex, "running {0};. ", script.Name );
                affirmative = false;
            }

            if ( !affirmative )
                return affirmative;
            if ( this.Session.IsNil( script.Name ) )
            {
                if ( !this.TraceVisaDeviceOperationOkay( false, "script {0} not found after running;. ", script.Name ) )
                {
                    // report failure if not an instrument or VISA error (handler returns Okay.)
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) running script {1};. {2}{3}", this.ResourceNameCaption, script.Name, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }

                affirmative = false;
            }
            else if ( script.Namespaces() is object && script.Namespaces().Length > 0 && this.Session.IsNil( script.Namespaces() ) )
            {
                if ( this.TraceVisaDeviceOperationOkay( false, "some of the namespace(s) {0} are nil after running {1};. ", script.NamespaceList, script.Name ) )
                {
                    // if not a visa error, report the specific namespaces.
                    foreach ( string value in script.Namespaces() )
                    {
                        if ( this.Session.IsNil( value ) )
                        {
                            _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' namespace {1} is nil;. {2}{3}", this.ResourceNameCaption, value, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                        }
                    }
                }

                affirmative = false;
            }
            else
            {
                this.DisplaySubsystem.DisplayLine( 2, "Done running {0}", script.Name );
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' {1} script run okay;. ", this.ResourceNameCaption, script.Name );
                affirmative = true;
            }

            return affirmative;
        }

        /// <summary> Executes the specified TSP script from file. </summary>
        /// <param name="node">   The node. </param>
        /// <param name="script"> Specifies reference to a valid <see cref="ScriptEntity">script</see> </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool RunUserScript( NodeEntityBase node, ScriptEntityBase script )
        {
            return this.RunUserScript( node, script, true );
        }

        /// <summary> Executes a loaded script on the local node. </summary>
        /// <remarks>
        /// David, 2009-05-13. Modified to run irrespective of the existence of the name spaces because a
        /// new script can be loaded on top of existing old code and the new version number will not
        /// materialize.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">      The node. </param>
        /// <param name="script">    Specifies reference to a valid <see cref="ScriptEntity">script</see> </param>
        /// <param name="runAlways"> true to run always. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool RunUserScript( NodeEntityBase node, ScriptEntityBase script, bool runAlways )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( this.Session.IsNil( node.Number, script.Name ) )
            {
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "failed running {1} because it does not exist on node {2};. {3}{4}", this.ResourceNameCaption, script.Name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                return false;
            }

            if ( !runAlways && script.Namespaces() is object && script.Namespaces().Length > 0 && !this.Session.IsNil( node.Number, script.Namespaces() ) )
            {
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} already run on node {2};. Nothing to do.", this.ResourceNameCaption, script.Name, node.Number );
                return true;
            }

            this.DisplaySubsystem.DisplayLine( 2, "Running {0}:{1}", node.Number, script.Name );
            string message = this.Session.ExecuteCommand( node.Number, "script.user.scripts.{0}()", script.Name );
            if ( this.TraceVisaDeviceOperationOkay( node.Number, false, message ) )
            {
                this.LinkSubsystem.WaitComplete( node.Number, script.Timeout, false );
                if ( !this.TraceVisaDeviceOperationOkay( node.Number, false, "running user script '{0}' using '{1}';. ", this.Name, message ) )
                {
                    this.DisplaySubsystem.DisplayLine( 2, "Failed waiting {0}:{1}", node.Number, script.Name );
                }
            }
            else
            {
                this.DisplaySubsystem.DisplayLine( 2, "Failed running {0}:{1}", node.Number, script.Name );
            }

            // do a garbage collection
            this.DisplaySubsystem.DisplayLine( 2, "Waiting cleanup node {0}", ( object ) node.Number );
            if ( !this.LinkSubsystem.CollectGarbageWaitComplete( node, script.Timeout, "collecting garbage after running script {0};. ", script.Name ) )
            {
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Ignoring instrument '{0}' error(s) collecting garbage after loading script {1} to node {2};. {3}{4}", this.ResourceNameCaption, script.Name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
            }

            if ( this.Session.IsNil( node.Number, script.Name ) )
            {
                if ( !this.TraceVisaDeviceOperationOkay( node.Number, "script {0} not found after running;. ", script.Name ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) loading script {1} to node {2};. {3}{4}", this.ResourceNameCaption, script.Name, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }

                return false;
            }
            else if ( script.Namespaces() is object && script.Namespaces().Length > 0 && this.Session.IsNil( node.Number, script.Namespaces() ) )
            {
                if ( this.TraceVisaDeviceOperationOkay( node.Number, "some of the namespace(s) {0} are nil after running {1};. ", script.NamespaceList, script.Name ) )
                {
                    // if not a visa error, report the specific namespaces.
                    foreach ( string value in script.Namespaces() )
                    {
                        if ( this.Session.IsNil( value ) )
                        {
                            _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' namespace {1} is nil on node {2};. {3}{4}", this.ResourceNameCaption, value, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                        }
                    }
                }

                return false;
            }
            else
            {
                this.DisplaySubsystem.DisplayLine( 2, "Done running {0}:{1}", node.Number, script.Name );
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' {1} script run on node {2};. ", this.ResourceNameCaption, script.Name, node.Number );
                return true;
            }

        }

        #endregion

        #region " TSP X: AUTO RUN SCRIPTS "

        /// <summary> Builds the commands for the auto run script. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="commands"> Specifies the list of commands to add to the script. </param>
        /// <param name="scripts">  Specifies the list of scripts which to include in the run. </param>
        /// <returns> The auto run commands. </returns>
        public static string BuildAutoRunScript( System.Text.StringBuilder commands, ScriptEntityCollection scripts )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            var script = new System.Text.StringBuilder( 1024 );
            var uniqueScripts = new System.Collections.Specialized.ListDictionary();
            // add code to run all scripts other than the boot script
            if ( scripts is object )
            {
                foreach ( ScriptEntityBase scriptEntity in scripts )
                {
                    if ( !string.IsNullOrWhiteSpace( scriptEntity.Name ) )
                    {
                        if ( !scriptEntity.IsBootScript )
                        {
                            if ( !uniqueScripts.Contains( scriptEntity.Name ) )
                            {
                                uniqueScripts.Add( scriptEntity.Name, scriptEntity );
                                _ = script.AppendFormat( "{0}.run()", scriptEntity.Name );
                                _ = script.AppendLine();
                            }
                        }
                    }
                }
            }

            // add the custom commands.
            if ( commands is object && commands.Length > 1 )
            {
                _ = script.AppendLine( commands.ToString() );
            }

            return script.ToString();
        }

        #endregion

        #region " TSP X: SAVE SCRIPTS "

        /// <summary>
        /// checks if save is required for the specified script. Presumes list of saved scripts was
        /// retrieved.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scriptName">     Specifies the script to save. </param>
        /// <param name="node">           Specifies the node. </param>
        /// <param name="isSaveAsBinary"> Specifies the condition requesting saving the source as binary. </param>
        /// <param name="isBootScript">   Specifies the condition indicating if this is a boot script. </param>
        /// <returns> <c>True</c> if save required; otherwise, <c>False</c>. </returns>
        public bool IsSaveRequired( string scriptName, NodeEntityBase node, bool isSaveAsBinary, bool isBootScript )
        {
            return string.IsNullOrWhiteSpace( scriptName )
                ? throw new ArgumentNullException( nameof( scriptName ) )
                : node is null
                ? throw new ArgumentNullException( nameof( node ) )
                : isSaveAsBinary && !this.IsBinaryScript( scriptName, node ).GetValueOrDefault( false ) || !this.SavedScriptExists( scriptName, node, false ) || isBootScript && node.BootScriptSaveRequired;
        }

        /// <summary> Saves the user script in non-volatile memory. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scriptName">     Specifies the script to save. </param>
        /// <param name="node">           Specifies the node. </param>
        /// <param name="isSaveAsBinary"> Specifies the condition requesting saving the source as binary. </param>
        /// <param name="isBootScript">   Specifies the condition indicating if this is a boot script. </param>
        /// <param name="timeout">        The timeout. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool SaveUserScript( string scriptName, NodeEntityBase node, bool isSaveAsBinary, bool isBootScript, TimeSpan timeout )
        {
            if ( string.IsNullOrWhiteSpace( scriptName ) )
                throw new ArgumentNullException( nameof( scriptName ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( this.Session.IsNil( node.IsController, node.Number, scriptName ) )
            {
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' custom firmware {1} not saved on node {2};. --it is not loaded. Error may be ignored.{3}{4}", this.ResourceNameCaption, scriptName, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                return false;
            }

            if ( this.IsSaveRequired( scriptName, node, isSaveAsBinary, isBootScript ) )
            {
                if ( !isBootScript )
                {
                    // if a script is saved, boot save is required.
                    node.BootScriptSaveRequired = true;
                }

                // do a garbage collection
                this.DisplaySubsystem.DisplayLine( 2, "Cleaning node {0}", ( object ) node.Number );
                if ( !this.LinkSubsystem.CollectGarbageWaitComplete( node, timeout, "collecting garbage before saving script {0} on node {1};. ", scriptName, node.Number ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Ignoring instrument '{0}' error(s) collecting garbage after loading {1} on node {2};. {3}{4}", this.ResourceNameCaption, scriptName, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                }

                this.DisplaySubsystem.DisplayLine( 2, "Saving {0}:{1}", node.Number, scriptName );
                if ( this.SaveScript( scriptName, node, isSaveAsBinary, isBootScript ) )
                {

                    // if saved boot script, boot script save no longer required.
                    if ( isBootScript )
                    {
                        node.BootScriptSaveRequired = false;
                    }

                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Instrument '{0}' saved script {1} on node {2};. ", this.ResourceNameCaption, scriptName, node.Number );
                }
                else
                {
                    if ( !this.TraceVisaDeviceOperationOkay( node.Number, "saving script {0};. ", scriptName ) )
                    {
                        // report failure if not an instrument or VISA error (handler returns Okay.)
                        _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Instrument '{0}' had error(s) saving script {1} on node {2};. {3}{4}", this.ResourceNameCaption, scriptName, node.Number, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    }

                    return false;
                }
            }
            else
            {
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Instrument '{0}' script {1} already saved on node {2};. ", this.ResourceNameCaption, scriptName, node.Number );
            }

            return true;
        }

        /// <summary> Saves the user scripts. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the list of scripts to save. </param>
        /// <param name="node">    Specifies the node. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool SaveUserScripts( ScriptEntityCollection scripts, NodeEntityBase node )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            bool success = true;

            // <c>True</c> if any save action was executed
            bool resetRequired = false;
            this.FetchSavedScripts( node );
            if ( scripts is object )
            {
                foreach ( ScriptEntityBase script in scripts )
                {
                    if ( script.IsModelMatch( node.ModelNumber ) && !string.IsNullOrWhiteSpace( script.Name ) )
                    {
                        if ( !this.Session.IsNil( node.IsController, node.Number, script.Name ) )
                        {
                            resetRequired = resetRequired || this.IsSaveRequired( script.Name, node, (script.FileFormat & ScriptFileFormats.Binary) != 0, script.IsBootScript );
                            success &= this.SaveUserScript( script.Name, node, (script.FileFormat & ScriptFileFormats.Binary) != 0, script.IsBootScript, script.Timeout );
                        }
                    }
                }
            }

            if ( resetRequired )
            {
                // reset to refresh the instrument display.
                _ = this.LinkSubsystem.ResetNode( node );
            }

            if ( success )
            {
                this.DisplaySubsystem.DisplayLine( 2, "All scripts saved on node {0}", ( object ) node.Number );
            }
            else
            {
                this.DisplaySubsystem.DisplayLine( 2, "Failed saving script on node {0}", ( object ) node.Number );
            }

            return success;
        }

        /// <summary> Saves all users scripts. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the list of the scripts to be deleted. </param>
        /// <param name="nodes">   Specifies the list of nodes on which scripts are deleted. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool SaveUserScripts( ScriptEntityCollection scripts, NodeEntityCollection nodes )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( nodes is null )
                throw new ArgumentNullException( nameof( nodes ) );

            // clear buffers before deleting.
            this.Session.DiscardUnreadData();
            bool success = true;
            foreach ( NodeEntityBase node in nodes )
                success &= this.SaveUserScripts( scripts, node );
            return success;
        }

        /// <summary>
        /// Updates users scripts. Deletes out-dated scripts and loads and runs new scripts as required
        /// on all nodes.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts">      Specifies the list of the scripts to be deleted. </param>
        /// <param name="node">         Specifies the node on which scripts are updated. </param>
        /// <param name="isSecondPass"> Set true on the second pass through if first pass requires
        /// loading new scripts. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool UpdateUserScripts( ScriptEntityCollection scripts, NodeEntityBase node, bool isSecondPass )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            string prefix = node.IsController ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Instrument '{0}'", this.ResourceNameCaption ) : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Instrument '{0}' node #{1}", this.ResourceNameCaption, node.Number );

            // run scripts so that we can read their version numbers. Scripts will run only if not ran, namely 
            // if there namespaces are not defined.
            if ( !scripts.RunScripts( node, this ) )
            {

                // report any failure.
                if ( isSecondPass )
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} failed running some firmware scripts because {1};. {2}{3}", prefix, scripts.OutcomeDetails, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    return false;
                }
                else
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} failed running some firmware scripts because {1}--problem ignored;. ", prefix, scripts.OutcomeDetails );
                }
            }

            // read scripts versions.
            if ( !scripts.ReadFirmwareVersions( node, this.Session ) )
            {
                if ( isSecondPass )
                {
                    // report any failure.
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, " failed reading some firmware versions because {1};. {2}{3}", prefix, scripts.OutcomeDetails, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    return false;
                }
                else
                {
                    // report any failure.
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, " failed reading some firmware versions because {1}. Problem ignored;. ", prefix, scripts.OutcomeDetails );
                }
            }

            // make sure program is up to date.
            if ( !isSecondPass && scripts.IsProgramOutdated( node ) )
            {
                _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} program out of date because {1}. System initialization aborted;. {2}{3}", prefix, scripts.OutcomeDetails, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                return false;
            }

            if ( scripts.VersionsUnspecified( node ) )
            {
                if ( isSecondPass )
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} failed verifying firmware version because {1};. {2}{3}", prefix, scripts.OutcomeDetails, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    return false;
                }
                else
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} failed verifying firmware version because {1}--problem ignored;. ", prefix, scripts.OutcomeDetails );
                }
            }

            if ( scripts.AllVersionsCurrent( node ) )
            {
                return true;
            }
            else if ( isSecondPass )
            {
                _ = string.IsNullOrWhiteSpace( scripts.OutcomeDetails )
                    ? this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} failed updating scripts;. Check log for details.{1}{2}", prefix, Environment.NewLine, new StackFrame( true ).UserCallStack() )
                    : this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} failed updating scripts because {1};. {2}{3}", prefix, scripts.OutcomeDetails, Environment.NewLine, new StackFrame( true ).UserCallStack() );

                return false;
            }
            else
            {


                // delete scripts that are out-dated or slated for deletion.
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "{0} deleting out-dated scripts;. ", prefix );
                if ( !this.DeleteUserScripts( scripts, node, true, true ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "{0} failed deleting out-dated scripts;. Check log for details. Problem ignored.", prefix );
                }

                if ( this.LoadRunUserScripts( scripts, node ) )
                {
                    return this.UpdateUserScripts( scripts, node, true );
                }
                else
                {
                    _ = this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "{0} failed loading and/or running scripts;. Check log for details.{1}{2}", prefix, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                    return false;
                }
            }
        }

        /// <summary> Updates users scripts deleting, as necessary, those that are out of date. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="scripts"> Specifies the list of the scripts to be deleted. </param>
        /// <param name="nodes">   Specifies the list of nodes on which scripts are deleted. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool UpdateUserScripts( ScriptEntityCollection scripts, NodeEntityCollection nodes )
        {
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( nodes is null )
                throw new ArgumentNullException( nameof( nodes ) );

            // clear buffers before deleting.
            this.Session.DiscardUnreadData();
            foreach ( NodeEntityBase node in nodes )
            {
                if ( !this.UpdateUserScripts( scripts, node, false ) )
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region " TSP X: WRITE "

        /// <summary> Writes the script to file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="System.IO.IOException">           Thrown when an IO failure occurred. </exception>
        /// <param name="folderPath"> Specifies the script file folder. </param>
        /// <param name="script">     Specifies the script. </param>
        /// <param name="node">       Specifies the node. </param>
        /// <param name="compress">   Specifies the compression condition. True to compress the source
        /// before saving. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool WriteScriptFile( string folderPath, ScriptEntityBase script, NodeEntityBase node, bool compress )
        {
            if ( folderPath is null )
                throw new ArgumentNullException( nameof( folderPath ) );
            if ( script is null )
                throw new ArgumentNullException( nameof( script ) );
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            string filePath = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0}.{1}", script.FileName, node.ModelNumber );
            filePath = System.IO.Path.Combine( folderPath, filePath );
            try
            {
                using var scriptFile = new System.IO.StreamWriter( filePath );
                if ( scriptFile is null )
                {

                    // now report the error to the calling module
                    throw new System.IO.IOException( "Failed opening TSP Script File '" + filePath + "'." );
                }

                this.FetchScriptSource( script, node );
                if ( compress )
                {
                    scriptFile.WriteLine( "{0}{1}{2}", ScriptEntityBase.CompressedPrefix, ScriptEntityBase.Compress( this.LastFetchScriptSource ), ScriptEntityBase.CompressedSuffix );
                }
                else
                {
                    scriptFile.WriteLine( this.LastFetchScriptSource );
                }

                return true;
            }
            catch
            {

                // clear receive buffer.
                this.Session.DiscardUnreadData();
                throw;
            }
            finally
            {
            }
        }

        /// <summary> Writes the script to file. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="folderPath"> Specifies the script file folder. </param>
        /// <param name="scripts">    Specifies the scripts. </param>
        /// <param name="nodes">      Specifies the nodes. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool WriteScriptFiles( string folderPath, ScriptEntityCollection scripts, NodeEntityCollection nodes )
        {
            if ( folderPath is null )
                throw new ArgumentNullException( nameof( folderPath ) );
            if ( scripts is null )
                throw new ArgumentNullException( nameof( scripts ) );
            if ( nodes is null )
                throw new ArgumentNullException( nameof( nodes ) );

            // clear receive buffer.
            this.Session.DiscardUnreadData();
            bool success = true;
            foreach ( NodeEntityBase node in nodes )
            {
                foreach ( ScriptEntityBase script in scripts )
                {
                    // do not save the script if is has no file name. Future upgrade might suggest adding a file name to the boot script.
                    if ( script.IsModelMatch( node.ModelNumber ) && !string.IsNullOrWhiteSpace( script.FileName ) && !script.SavedToFile )
                    {
                        this.DisplaySubsystem.DisplayLine( 2, "Writing {0}:{1}", node.ModelNumber, script.Name );
                        if ( this.WriteScriptFile( folderPath, script, node, (script.FileFormat & ScriptFileFormats.Compressed) != 0 ) )
                        {
                            script.SavedToFile = true;
                            success = success && true;
                        }
                        else
                        {
                            success = false;
                        }
                    }
                }
            }

            return success;
        }

        #endregion

        #region " TSP X - FRAMEWORK SCRIPTS "

        /// <summary> Define scripts. </summary>
        public virtual void DefineScripts()
        {
        }

        /// <summary> Deletes the user scripts. </summary>
        /// <param name="statusSubsystem">  A reference to a
        /// <see cref="VI.Tsp.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        /// <see cref="VI.Tsp.DisplaySubsystemBase">display
        /// subsystem</see>. </param>
        public virtual void DeleteUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem )
        {
        }

        /// <summary> Uploads the user scripts to the controller instrument. </summary>
        /// <param name="statusSubsystem">  A reference to a
        /// <see cref="VI.Tsp.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        /// <see cref="VI.Tsp.DisplaySubsystemBase">display
        /// subsystem</see>. </param>
        /// <param name="accessSubsystem">  The access subsystem. </param>
        /// <returns> <c>True</c> if uploaded; otherwise, <c>False</c>. </returns>
        public virtual bool UploadUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem, AccessSubsystemBase accessSubsystem )
        {
            return false;
        }

        /// <summary> Saves the user scripts applying the release value. </summary>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="statusSubsystem">  A reference to a
        /// <see cref="VI.Tsp.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        /// <see cref="VI.Tsp.DisplaySubsystemBase">display
        /// subsystem</see>. </param>
        /// <param name="accessSubsystem">  The access subsystem. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public virtual bool SaveUserScripts( TimeSpan timeout, StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem, AccessSubsystemBase accessSubsystem )
        {
            return false;
        }

        /// <summary> Try read parse user scripts. </summary>
        /// <param name="statusSubsystem">  A reference to a
        /// <see cref="VI.Tsp.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        /// <see cref="VI.Tsp.DisplaySubsystemBase">display
        /// subsystem</see>. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public virtual bool TryReadParseUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem )
        {
            return false;
        }


        #endregion

    }

    /// <summary> Enumerates the content type of a TSP chunk line. </summary>
    public enum TspChunkLineContentType
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined" )]
        None,

        /// <summary> An enum constant representing the start comment block option. </summary>
        [System.ComponentModel.Description( "Start Comment Block" )]
        StartCommentBlock,

        /// <summary> An enum constant representing the end comment block option. </summary>
        [System.ComponentModel.Description( "End Comment Block" )]
        EndCommentBlock,

        /// <summary> An enum constant representing the comment option. </summary>
        [System.ComponentModel.Description( "Comment" )]
        Comment,

        /// <summary> An enum constant representing the syntax option. </summary>
        [System.ComponentModel.Description( "Syntax" )]
        Syntax,

        /// <summary> An enum constant representing the syntax start comment block option. </summary>
        [System.ComponentModel.Description( "Syntax and Start Comment Block" )]
        SyntaxStartCommentBlock,

        /// <summary> An enum constant representing the chunk name declaration option. </summary>
        [System.ComponentModel.Description( "Chunk Name Declaration" )]
        ChunkNameDeclaration,

        /// <summary> An enum constant representing the chunk name require option. </summary>
        [System.ComponentModel.Description( "Chunk Name Requirement" )]
        ChunkNameRequire,

        /// <summary> An enum constant representing the chunk name loaded option. </summary>
        [System.ComponentModel.Description( "Chunk Name Loaded" )]
        ChunkNameLoaded
    }
}
