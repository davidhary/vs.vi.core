using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.Script
{

    /// <summary> Provides the contract for a Script Entity. </summary>
    /// <remarks>
    /// David, 2009-03-02, 3.0.3348. <para>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public abstract class ScriptEntityBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        protected ScriptEntityBase() : base()
        {
            this.Name = string.Empty;
            this.ModelMask = string.Empty;
            this.EmbeddedFirmwareVersion = string.Empty;
            // Me._isBootScript = False
            // Me._isSupportScript = False
            // Me._isPrimaryScript = False
            this.FileName = string.Empty;
            this.FirmwareVersionGetter = string.Empty;
            this.NamespaceListSetter( "" );
            this.ReleasedFirmwareVersion = string.Empty;
            // Me._requiresReadParseWrite = False
            // Me._savedToFile = False
            this._Source = string.Empty;
            this.SourceFormat = ScriptFileFormats.None;
            this.Timeout = TimeSpan.FromMilliseconds( 10000d );
        }

        /// <summary> Constructs this class. </summary>
        /// <param name="name">      Specifies the script name. </param>
        /// <param name="modelMask"> Specifies the model families for this script. </param>
        protected ScriptEntityBase( string name, string modelMask ) : this()
        {
            this.Name = name;
            this.ModelMask = modelMask;
        }

        #endregion

        #region " SHARED "

        /// <summary> Returns the compressed code prefix. </summary>
        /// <value> The compressed prefix. </value>
        public static string CompressedPrefix => "<COMPRESSED>";

        /// <summary> Returns the compressed code suffix. </summary>
        /// <value> The compressed suffix. </value>
        public static string CompressedSuffix => "</COMPRESSED>";

        /// <summary> Returns a compressed value. </summary>
        /// <param name="value"> The string being chopped. </param>
        /// <returns> Compressed value. </returns>
        public static string Compress( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }

            string result = string.Empty;

            // Compress the byte array
            using ( var memoryStream = new System.IO.MemoryStream() )
            {
                using var compressedStream = new System.IO.Compression.GZipStream( memoryStream, System.IO.Compression.CompressionMode.Compress );

                // Convert the uncompressed string into a byte array
                var values = System.Text.Encoding.UTF8.GetBytes( value );
                compressedStream.Write( values, 0, values.Length );

                // Don't FLUSH here - it possibly leads to data loss!
                compressedStream.Close();
                var compressedValues = memoryStream.ToArray();

                // Convert the compressed byte array back to a string
                result = Convert.ToBase64String( compressedValues );
                memoryStream.Close();
            }

            return result;
        }

        /// <summary> Returns the decompressed string of the value. </summary>
        /// <remarks>
        /// David, 2009-04-09, 1.1.3516. Bug fix in getting the size. Changed  memoryStream.Length - 5 to
        /// memoryStream.Length - 4.
        /// </remarks>
        /// <param name="value"> The string being chopped. </param>
        /// <returns> Decompressed value. </returns>
        public static string Decompress( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return string.Empty;
            }

            string result = string.Empty;

            // Convert the compressed string into a byte array
            var compressedValues = Convert.FromBase64String( value );

            // Decompress the byte array
            using ( var memoryStream = new System.IO.MemoryStream( compressedValues ) )
            {
                using var compressedStream = new System.IO.Compression.GZipStream( memoryStream, System.IO.Compression.CompressionMode.Decompress );

                // it looks like we are getting a bogus size.
                var sizeBytes = new byte[4];
                memoryStream.Position = memoryStream.Length - 4L;
                _ = memoryStream.Read( sizeBytes, 0, 4 );
                int outputSize = BitConverter.ToInt32( sizeBytes, 0 );
                memoryStream.Position = 0L;
                var values = new byte[outputSize];
                _ = compressedStream.Read( values, 0, outputSize );

                // Convert the decompressed byte array back to a string
                result = System.Text.Encoding.UTF8.GetString( values );
            }

            return result;
        }

        /// <summary> Query if 'value' includes any of the characters. </summary>
        /// <param name="value">      The value. </param>
        /// <param name="characters"> The characters. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool IncludesAny( string value, string characters )
        {
            // 2954: changed to [characters] from ^[characters]+$
            var r = new System.Text.RegularExpressions.Regex( $"[{characters}]", System.Text.RegularExpressions.RegexOptions.IgnoreCase );
            return r.IsMatch( value );
        }

        /// <summary> Query if 'value' is valid script name. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if valid script name; otherwise <c>false</c> </returns>
        public static bool IsValidScriptName( string value )
        {
            return !string.IsNullOrWhiteSpace( value ) && !IncludesAny( value, Syntax.Constants.IllegalScriptNameCharacters );
        }

        /// <summary> Query if 'value' is valid script file name. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if valid script name; otherwise <c>false</c> </returns>
        public static bool IsValidScriptFileName( string value )
        {
            return !string.IsNullOrWhiteSpace( value ) && !IncludesAny( value, Syntax.Constants.IllegalFileCharacters );
        }

        /// <summary> Returns the file size. </summary>
        /// <param name="path"> The path. </param>
        /// <returns> System.Int64. </returns>
        public static long FileSize( string path )
        {
            long size = 0L;
            if ( !string.IsNullOrWhiteSpace( path ) )
            {
                var info = new System.IO.FileInfo( path );
                if ( info.Exists )
                    size = info.Length;
            }

            return size;
        }

        #endregion

        #region " FIRMWARE "

        /// <summary> Gets or sets the embedded firmware version. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The embedded firmware version. </value>
        public string EmbeddedFirmwareVersion { get; set; }

        /// <summary> Gets or sets the released firmware version. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The released firmware version. </value>
        public string ReleasedFirmwareVersion { get; set; }

        /// <summary> Gets or sets the firmware version getter function to be printed from the instrument. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The firmware version command. </value>
        public string FirmwareVersionGetter { get; set; }

        /// <summary> Checks if the firmware version getter exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> Specifies reference to the Tsp Session. </param>
        /// <returns>
        /// <c>True</c> if the firmware version command exists; otherwise, <c>False</c>.
        /// </returns>
        public bool FirmwareVersionGetterExists( Pith.SessionBase session )
        {
            return session is null
                ? throw new ArgumentNullException( nameof( session ) )
                : !session.IsNil( this.FirmwareVersionGetter.TrimEnd( "()".ToCharArray() ) );
        }

        /// <summary> Checks if the firmware version command exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> The node number. </param>
        /// <param name="session">    Specifies reference to the Tsp Session. </param>
        /// <returns>
        /// <c>True</c> if the firmware version command exists; otherwise, <c>False</c>.
        /// </returns>
        public bool FirmwareVersionGetterExists( int nodeNumber, Pith.SessionBase session )
        {
            return session is null
                ? throw new ArgumentNullException( nameof( session ) )
                : !session.IsNil( nodeNumber, this.FirmwareVersionGetter.TrimEnd( "()".ToCharArray() ) );
        }

        /// <summary> Checks if the firmware version command exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">    Specifies the node. </param>
        /// <param name="session"> Specifies reference to the Tsp Session. </param>
        /// <returns>
        /// <c>True</c> if the firmware version command exists; otherwise, <c>False</c>.
        /// </returns>
        public bool FirmwareVersionGetterExists( NodeEntityBase node, Pith.SessionBase session )
        {
            return node is null
                ? throw new ArgumentNullException( nameof( node ) )
                : session is null
                ? throw new ArgumentNullException( nameof( session ) )
                : node.IsController ? this.FirmwareVersionGetterExists( session ) : this.FirmwareVersionGetterExists( node.Number, session );
        }

        /// <summary>
        /// Queries the embedded firmware version from a remote node and saves it to
        /// <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> Specifies reference to the Tsp Session. </param>
        /// <returns> The firmware version. </returns>
        public string QueryFirmwareVersion( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.EmbeddedFirmwareVersion = this.FirmwareVersionGetterExists( session ) ? session.QueryTrimEnd( Syntax.Lua.PrintCommand( this.FirmwareVersionGetter ) ) : Syntax.Lua.NilValue;
            return this.EmbeddedFirmwareVersion;
        }

        /// <summary>
        /// Queries the embedded firmware version from a remote node and saves it to
        /// <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="nodeNumber"> The node number. </param>
        /// <param name="session">    Specifies reference to the Tsp Session. </param>
        /// <returns> The firmware version. </returns>
        public string QueryFirmwareVersion( int nodeNumber, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.EmbeddedFirmwareVersion = this.FirmwareVersionGetterExists( nodeNumber, session ) ? session.QueryPrintTrimEnd( nodeNumber, this.FirmwareVersionGetter ) : Syntax.Lua.NilValue;
            return this.EmbeddedFirmwareVersion;
        }

        /// <summary>
        /// Queries the embedded firmware version from a remote node and saves it to
        /// <see cref="EmbeddedFirmwareVersion">the firmware version cache.</see>
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">    Specifies the node. </param>
        /// <param name="session"> Specifies reference to the Tsp Session. </param>
        /// <returns> The firmware version. </returns>
        public string QueryFirmwareVersion( NodeEntityBase node, Pith.SessionBase session )
        {
            return node is null
                ? throw new ArgumentNullException( nameof( node ) )
                : session is null
                ? throw new ArgumentNullException( nameof( session ) )
                : node.IsController ? this.QueryFirmwareVersion( session ) : this.QueryFirmwareVersion( node.Number, session );
        }

        /// <summary> Validates the released against the embedded firmware. </summary>
        /// <returns> The <see cref="FirmwareVersionStatus">version status</see>. </returns>
        public FirmwareVersionStatus ValidateFirmware()
        {
            if ( string.IsNullOrWhiteSpace( this.ReleasedFirmwareVersion ) )
            {
                return FirmwareVersionStatus.ReferenceUnknown;
            }
            else if ( string.IsNullOrWhiteSpace( this.EmbeddedFirmwareVersion ) )
            {
                return FirmwareVersionStatus.Unknown;
            }
            else if ( (this.EmbeddedFirmwareVersion ?? "") == Syntax.Lua.NilValue )
            {
                return FirmwareVersionStatus.Missing;
            }
            else
            {
                switch ( new Version( this.EmbeddedFirmwareVersion ).CompareTo( new Version( this.ReleasedFirmwareVersion ) ) )
                {
                    case var @case when @case > 0:
                        {
                            return FirmwareVersionStatus.Newer;
                        }

                    case 0:
                        {
                            return FirmwareVersionStatus.Current;
                        }

                    default:
                        {
                            return FirmwareVersionStatus.Older;
                        }
                }
            }
        }

        #endregion

        #region " FILE "

        /// <summary> Gets or sets the filename of the file. </summary>
        /// <value> The name of the file. </value>
        public string FileName { get; set; }

        /// <summary> Gets or sets the filename of the resource file. </summary>
        /// <value> The filename of the resource file. </value>
        public string ResourceFileName { get; set; }

        /// <summary>
        /// Gets or sets the file format which to write. Defaults to uncompressed format.
        /// </summary>
        /// <value> The file format. </value>
        public ScriptFileFormats FileFormat { get; set; }

        /// <summary>
        /// Gets or sets the condition indicating if the script was already saved to file. This property
        /// is set <c>True</c> if the script source is already in the correct format so no new file needs
        /// to be saved.
        /// </summary>
        /// <value> The saved to file. </value>
        public bool SavedToFile { get; set; }

        /// <summary> Gets or sets the format of the contents that was used to set the source. </summary>
        /// <value> The source format. </value>
        public ScriptFileFormats SourceFormat { get; private set; }

        #endregion

        #region " SCRIPT MANAGEMENT "

        /// <summary> Gets or sets the condition indicating if this script was deleted. </summary>
        /// <value> The is deleted. </value>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Gets or sets the condition indicating if this scripts needs to be deleted on the instrument.
        /// At this time this is used in design mode. It might be used later for refreshing the stored
        /// scripts.
        /// </summary>
        /// <value> The requires deletion. </value>
        public bool RequiresDeletion { get; set; }

        /// <summary> True to requires read parse write. </summary>
        private bool _RequiresReadParseWrite;

        /// <summary> Indicates if the script requires update from file. </summary>
        /// <returns>
        /// <c>True</c> if the script requires update from file; otherwise, <c>False</c>.
        /// </returns>
        public bool RequiresReadParseWrite()
        {
            return this._RequiresReadParseWrite;
        }

        #endregion

        #region " MODEL MANAGEMENT "

        /// <summary> Specifies the family of instrument models for this script. </summary>
        /// <value> The model mask. </value>
        public string ModelMask { get; private set; }

        /// <summary>
        /// Checks if the <paramref name="model">model</paramref> matches the
        /// <see cref="ModelMask">mask</see>.
        /// </summary>
        /// <param name="model"> Actual mode. </param>
        /// <param name="mask">  Mode mask using '%' to signify ignored characters and * to specify
        /// wildcard suffix. </param>
        /// <returns>
        /// <c>True</c> if the <paramref name="model">model</paramref> matches the
        /// <see cref="ModelMask">mask</see>.
        /// </returns>
        public static bool IsModelMatch( string model, string mask )
        {
            char wildcard = '*';
            char ignore = '%';
            if ( string.IsNullOrWhiteSpace( mask ) )
            {
                return true;
            }
            else if ( string.IsNullOrWhiteSpace( model ) )
            {
                return false;
            }
            else if ( mask.Contains( Conversions.ToString( wildcard ) ) )
            {
                int length = mask.IndexOf( wildcard );
                var m = mask.Substring( 0, length ).ToCharArray();
                var candidate = model.Substring( 0, length ).ToCharArray();
                for ( int i = 0, loopTo1 = m.Length - 1; i <= loopTo1; i++ )
                {
                    char c = m[i];
                    if ( c != ignore && c != candidate[i] )
                    {
                        return false;
                    }
                }
            }
            else if ( mask.Length != model.Length )
            {
                return false;
            }
            else
            {
                var m = mask.ToCharArray();
                var candidate = model.ToCharArray();
                for ( int i = 0, loopTo = m.Length - 1; i <= loopTo; i++ )
                {
                    char c = m[i];
                    if ( c != ignore && c != candidate[i] )
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Checks if the <paramref name="model">model</paramref> matches the
        /// <see cref="ModelMask">mask</see>.
        /// </summary>
        /// <param name="model"> The model. </param>
        /// <returns>
        /// <c>True</c> if the <paramref name="model">model</paramref> matches the
        /// <see cref="ModelMask">mask</see>.
        /// </returns>
        public bool IsModelMatch( string model )
        {
            return IsModelMatch( model, this.ModelMask );
        }

        #endregion

        #region " SCRIPT SPECIFICATIONS "

        /// <summary>
        /// Gets or sets the sentinel indicating if this is a binary script. This is determined when
        /// setting the source.
        /// </summary>
        /// <value> <c>True</c> if this is a binary script; otherwise, <c>False</c>. </value>
        public bool IsBinaryScript { get; private set; }

        /// <summary> Gets or sets the sentinel indicating if this is a Boot script. </summary>
        /// <value> <c>True</c> if this is a Boot script; otherwise, <c>False</c>. </value>
        public bool IsBootScript { get; set; }

        /// <summary> Gets or sets the sentinel indicating if this is a Primary script. </summary>
        /// <value> <c>True</c> if this is a Primary script; otherwise, <c>False</c>. </value>
        public bool IsPrimaryScript { get; set; }

        /// <summary> Gets or sets the sentinel indicating if this is a Support script. </summary>
        /// <value> <c>True</c> if this is a Support script; otherwise, <c>False</c>. </value>
        public bool IsSupportScript { get; set; }

        /// <summary> Gets or sets the name of the script. </summary>
        /// <value> The name. </value>
        public string Name { get; private set; }

        /// <summary> Source for the. </summary>
        private string _Source;

        /// <summary> Gets or sets the source for the script. </summary>
        /// <value> The source. </value>
        public string Source
        {
            get => this._Source;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                this._Source = string.Empty;
                this.IsBinaryScript = false;
                this.SourceFormat = ScriptFileFormats.None;
                this._RequiresReadParseWrite = this.ReleasedFirmwareVersion.Trim().StartsWith( "+", true, System.Globalization.CultureInfo.CurrentCulture );
                this.SourceFormat = ScriptFileFormats.None;
                if ( !this.RequiresReadParseWrite() )
                {
                    if ( value.StartsWith( CompressedPrefix, false, System.Globalization.CultureInfo.CurrentCulture ) )
                    {
                        int fromIndex = value.IndexOf( CompressedPrefix, StringComparison.OrdinalIgnoreCase ) + CompressedPrefix.Length;
                        int toIndex = value.IndexOf( CompressedSuffix, StringComparison.OrdinalIgnoreCase ) - 1;
                        this._Source = value.Substring( fromIndex, toIndex - fromIndex + 1 );
                        this._Source = Decompress( this.Source );
                        this.SourceFormat |= ScriptFileFormats.Compressed;
                    }
                    else
                    {
                        this._Source = value;
                    }

                    if ( !string.IsNullOrWhiteSpace( this.Source ) )
                    {
                        string snippet = this.Source.Substring( 0, 50 ).Trim();
                        this.IsBinaryScript = snippet.StartsWith( "{", true, System.Globalization.CultureInfo.CurrentCulture ) || snippet.StartsWith( "loadstring", true, System.Globalization.CultureInfo.CurrentCulture ) || snippet.StartsWith( "loadscript", true, System.Globalization.CultureInfo.CurrentCulture );
                    }

                    if ( !this.Source.EndsWith( " ", true, System.Globalization.CultureInfo.CurrentCulture ) )
                    {
                        this._Source = this.Source.Insert( this.Source.Length, " " );
                    }

                    if ( this.IsBinaryScript )
                    {
                        this.SourceFormat |= ScriptFileFormats.Binary;
                    }
                }

                // tag file as saved if source format and file format match.
                this.SavedToFile = this.SourceFormat == this.FileFormat;
            }
        }

        /// <summary> Gets or sets the timeout. </summary>
        /// <value> The timeout. </value>
        public TimeSpan Timeout { get; set; }

        /// <summary> Namespace list setter. </summary>
        /// <param name="value"> The value. </param>
        private void NamespaceListSetter( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
                value = string.Empty;
            this._NamespaceList = value;
            this._Namespaces = string.IsNullOrWhiteSpace( value ) ? Array.Empty<string>() : this.NamespaceList.Split( ',' );
        }

        /// <summary> List of namespaces. </summary>
        private string _NamespaceList;

        /// <summary> Gets or sets a list of namespaces. </summary>
        /// <value> A List of namespaces. </value>
        public string NamespaceList
        {
            get => this._NamespaceList;

            set => this.NamespaceListSetter( value );
        }

        /// <summary> The namespaces. </summary>
        private string[] _Namespaces;

        /// <summary> Gets the namespaces. </summary>
        /// <returns> A list of. </returns>
        public string[] Namespaces()
        {
            return this._Namespaces;
        }

        #endregion

    }

    /// <summary>
    /// A <see cref="System.Collections.ObjectModel.KeyedCollection{TKey, TItem}">collection</see> of
    /// <see cref="ScriptEntityBase">script entity</see>
    /// items keyed by the <see cref="ScriptEntityBase.Name">name.</see>
    /// </summary>
    /// <remarks>
    /// David, 2009-03-02, 3.0.3348. <para>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class ScriptEntityCollection : ScriptEntityBaseCollection<ScriptEntityBase>
    {

        /// <summary> Gets key for item. </summary>
        /// <param name="item"> The item. </param>
        /// <returns> The key for item. </returns>
        protected override string GetKeyForItem( ScriptEntityBase item )
        {
            return base.GetKeyForItem( item );
        }

        /// <summary>
        /// Gets the condition indicating if scripts that are newer than the scripts specified by the
        /// program can be deleted. This is required to allow the program install the scripts it
        /// considers current.
        /// </summary>
        /// <value> The allow deleting newer scripts. </value>
        public bool AllowDeletingNewerScripts { get; set; }
    }

    /// <summary>
    /// A <see cref="System.Collections.ObjectModel.KeyedCollection{TKey, TItem}">collection</see> of
    /// <see cref="ScriptEntityBase">script entity</see>
    /// items keyed by the <see cref="ScriptEntityBase.Name">name.</see>
    /// </summary>
    /// <remarks>
    /// David, 2009-03-02, 3.0.3348.x <para>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class ScriptEntityBaseCollection<TItem> : System.Collections.ObjectModel.KeyedCollection<string, TItem> where TItem : ScriptEntityBase
    {

        #region " SELECT SCRIPT "

        /// <summary> Gets key for item. </summary>
        /// <param name="item"> The item. </param>
        /// <returns> The key for item. </returns>
        protected override string GetKeyForItem( TItem item )
        {
            return item.Name + item.ModelMask;
        }

        /// <summary>
        /// Returns reference to the boot script for the specified node or nothing if a boot script does
        /// not exist.
        /// </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns>
        /// Reference to the boot script for the specified node or nothing if a boot script does not
        /// exist.
        /// </returns>
        public TItem SelectBootScript( NodeEntityBase node )
        {
            if ( node is object )
            {
                foreach ( TItem script in this.Items )
                {
                    if ( script.IsModelMatch( node.ModelNumber ) && script.IsBootScript )
                    {
                        return script;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns reference to the Serial Number script for the specified node or nothing if a serial
        /// number script does not exist.
        /// </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns>
        /// Reference to the Serial Number script for the specified node or nothing if a serial number
        /// script does not exist.
        /// </returns>
        public TItem SelectSerialNumberScript( NodeEntityBase node )
        {
            if ( node is null )
            {
                return null;
            }

            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && script.IsPrimaryScript )
                {
                    return script;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns reference to the support script for the specified node or nothing if a support script
        /// does not exist.
        /// </summary>
        /// <param name="node"> The node. </param>
        /// <returns>
        /// Reference to the support script for the specified node or nothing if a support script does
        /// not exist.
        /// </returns>
        public TItem SelectSupportScript( NodeEntityBase node )
        {
            if ( node is null )
            {
                return null;
            }

            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && script.IsSupportScript )
                {
                    return script;
                }
            }

            return null;
        }

        #endregion

        #region " FIRMWARE "

        /// <summary> The identified script. </summary>

        /// <summary> Gets any script identified using the test methods. </summary>
        /// <value> The identified script. </value>
        public TItem IdentifiedScript { get; private set; }

        /// <summary> The outcome details. </summary>
        private System.Text.StringBuilder _OutcomeDetails;

        /// <summary> Gets the status message. Used with reading to identify any problem. </summary>
        /// <value> The outcome details. </value>
        public string OutcomeDetails => this._OutcomeDetails.ToString();

        /// <summary> Reads the firmware version of all scripts. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">    Specified the node. </param>
        /// <param name="session"> Specifies the TSP session. </param>
        /// <returns> <c>True</c> if okay; <c>False</c> if any exception had occurred. </returns>
        public bool ReadFirmwareVersions( NodeEntityBase node, Pith.SessionBase session )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) )
                {
                    // clear the embedded version.
                    script.EmbeddedFirmwareVersion = string.Empty;
                    if ( !script.IsBootScript && !string.IsNullOrWhiteSpace( script.Name ) )
                    {
                        if ( session.IsNil( node.IsController, node.Number, script.Name ) )
                        {
                            this.IdentifiedScript = script;
                            if ( this._OutcomeDetails.Length > 0 )
                            {
                                _ = this._OutcomeDetails.AppendLine();
                            }

                            _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' not found on '{1}' node {2}.", script.Name, session.ResourceNameCaption, node.Number );
                        }
                        else if ( session.IsNil( node.IsController, node.Number, script.Namespaces() ) )
                        {
                            this.IdentifiedScript = script;
                            if ( this._OutcomeDetails.Length > 0 )
                            {
                                _ = this._OutcomeDetails.AppendLine();
                            }

                            _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' not executed on '{1}' node {2}.", script.Name, session.ResourceNameCaption, node.Number );
                        }
                        else if ( !script.FirmwareVersionGetterExists( node, session ) )
                        {
                            // existing script must be out of date because it does not support the firmware version command.
                            this.IdentifiedScript = script;
                            if ( this._OutcomeDetails.Length > 0 )
                            {
                                _ = this._OutcomeDetails.AppendLine();
                            }

                            _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' version function not defined on '{1}' node {2}.", script.Name, session.ResourceNameCaption, node.Number );
                        }
                        else
                        {
                            _ = script.QueryFirmwareVersion( node, session );
                        }
                    }
                }
            }

            return this._OutcomeDetails.Length == 0;
        }

        /// <summary> Returns <c>True</c> if all script versions are current. </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if all script versions are current. </returns>
        public bool AllVersionsCurrent( NodeEntityBase node )
        {
            if ( node is null )
            {
                return false;
            }

            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !script.IsBootScript )
                {
                    var outcome = script.ValidateFirmware();
                    switch ( outcome )
                    {
                        case FirmwareVersionStatus.Current:
                            {
                                break;
                            }

                        case FirmwareVersionStatus.Missing:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' version function not defined.", script.Name );
                                break;
                            }

                        case FirmwareVersionStatus.Newer:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' embedded version '{1}' is newer than the released version '{2}' indicating that this program is out-dated. You must obtain a newer version of this program.", script.Name, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion );
                                break;
                            }

                        case FirmwareVersionStatus.Older:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' embedded version '{1}' is older than the released version '{2}' indicating that this script is out-dated.", script.Name, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion );
                                break;
                            }

                        case FirmwareVersionStatus.ReferenceUnknown:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware {0} released version not specified.", script.Name );
                                break;
                            }

                        case FirmwareVersionStatus.Unknown:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware {0} failed reading embedded version.", script.Name );
                                break;
                            }

                        default:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "unhandled outcome reading custom firmware '{0}' version.", script.Name );
                                break;
                            }
                    }

                    if ( outcome != FirmwareVersionStatus.Current )
                    {
                        this.IdentifiedScript = script;
                    }
                }
            }

            return this._OutcomeDetails.Length == 0;
        }

        /// <summary> Returns <c>True</c> if any script has an unspecified version. </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if any script has an unspecified version. </returns>
        public bool VersionsUnspecified( NodeEntityBase node )
        {
            if ( node is null )
            {
                return false;
            }

            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !script.IsBootScript )
                {
                    var outcome = script.ValidateFirmware();
                    switch ( outcome )
                    {
                        case FirmwareVersionStatus.Missing:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' version function not defined.", script.Name );
                                break;
                            }

                        case FirmwareVersionStatus.Unknown:
                            {
                                _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware {0} failed reading embedded version.", script.Name );
                                break;
                            }

                        default:
                            {
                                break;
                            }
                    }

                    if ( outcome != FirmwareVersionStatus.Current )
                    {
                        this.IdentifiedScript = script;
                    }
                }
            }

            return this._OutcomeDetails.Length > 0;
        }

        /// <summary>
        /// Returns <c>True</c> if any script version is newer than its released version.
        /// </summary>
        /// <param name="node"> Specifies the node. </param>
        /// <returns> <c>True</c> if any script version is newer than its released version. </returns>
        public bool IsProgramOutdated( NodeEntityBase node )
        {
            if ( node is null )
            {
                return false;
            }

            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !script.IsBootScript && script.ValidateFirmware() == FirmwareVersionStatus.Newer )
                {
                    _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' embedded version '{1}' is newer than the released version '{2}' indicating that this program is out-dated. You must obtain a newer version of this program.", script.Name, script.EmbeddedFirmwareVersion, script.ReleasedFirmwareVersion );
                    this.IdentifiedScript = script;
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region " UPDATE "

        /// <summary> Returns <c>True</c> if any script requires update from file. </summary>
        /// <returns> <c>True</c> if any script requires update from file. </returns>
        public bool RequiresReadParseWrite()
        {
            foreach ( TItem script in this.Items )
            {
                if ( script.RequiresReadParseWrite() )
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region " ACTIONS "

        /// <summary>
        /// Runs existing scripts if they did not ran so their versions can be checked. Running exits
        /// after the first script that failed running assuming that scripts depend on previous scripts.
        /// </summary>
        /// <param name="node">               Specifies the node. </param>
        /// <param name="tspScriptSubsystem"> The tsp script subsystem. </param>
        /// <returns> <c>True</c> if okay; <c>False</c> if any exception had occurred. </returns>
        public bool RunScripts( NodeEntityBase node, ScriptManagerBase tspScriptSubsystem )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( tspScriptSubsystem is null )
                throw new ArgumentNullException( nameof( tspScriptSubsystem ) );
            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !script.IsBootScript && !string.IsNullOrWhiteSpace( script.Name ) )
                {
                    if ( tspScriptSubsystem.Session.IsNil( node.IsController, node.Number, script.Name ) )
                    {
                        this.IdentifiedScript = script;
                        if ( this._OutcomeDetails.Length > 0 )
                        {
                            _ = this._OutcomeDetails.AppendLine();
                        }

                        _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' not found on '{1}' on node {2}.", script.Name, tspScriptSubsystem.ResourceNameCaption, node.Number );
                        // if script did not run, do not run subsequent scripts.
                        return false;
                    }
                    else if ( tspScriptSubsystem.Session.IsNil( node.IsController, node.Number, script.Namespaces() ) )
                    {
                        tspScriptSubsystem.Session.LastAction = $"{tspScriptSubsystem.ResourceNameCaption} running {script.Name} On node {node.Number}.";
                        tspScriptSubsystem.Session.LastNodeNumber = node.Number;
                        // if script not ran, run it now. Throw exception on failure.
                        tspScriptSubsystem.RunScript( script, node );
                        if ( tspScriptSubsystem.Session.IsNil( node.IsController, node.Number, script.Name ) )
                        {
                            _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "Instrument '{0}' script {1} not found after running on node {2}.", tspScriptSubsystem.ResourceNameCaption, script.Name, node.Number );

                            // if script did not run, do not run subsequent scripts.
                            return false;
                        }
                        else if ( tspScriptSubsystem.Session.IsNil( node.IsController, node.Number, script.Namespaces() ) )
                        {
                            _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "some of the namespace(s) {0} are nil after running {1} on '{2}' node {3}", script.NamespaceList, script.Name, tspScriptSubsystem.ResourceNameCaption, node.Number );

                            // if script did not run, do not run subsequent scripts.
                            return false;
                        }
                    }
                }
            }

            return this._OutcomeDetails.Length == 0;
        }

        /// <summary> Checks if all scripts exist. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">    Specifies the node. </param>
        /// <param name="session"> Specifies the <see cref="VI.Pith.SessionBase">TSP session.</see> </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c> if any script does not exist. </returns>
        public bool FindScripts( NodeEntityBase node, Pith.SessionBase session )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !string.IsNullOrWhiteSpace( script.Name ) )
                {
                    if ( session.IsNil( node.IsController, node.Number, script.Name ) )
                    {
                        this.IdentifiedScript = script;
                        if ( this._OutcomeDetails.Length > 0 )
                        {
                            _ = this._OutcomeDetails.AppendLine();
                        }

                        _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' not found on '{1}' on node {2}.", script.Name, session.ResourceNameCaption, node.Number );
                        // if script not found return false
                        return false;
                    }
                }
            }

            return this._OutcomeDetails.Length == 0;
        }

        /// <summary> Checks if all scripts were saved. </summary>
        /// <param name="node">                 Specifies the node. </param>
        /// <param name="tspScriptSubsystem">   The tsp script subsystem. </param>
        /// <param name="refreshScriptCatalog"> Specifies the condition for updating the catalog of saved
        /// scripts before checking the status of these scripts.
        /// </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c> if any script does not exist. </returns>
        public bool FindSavedScripts( NodeEntityBase node, ScriptManagerBase tspScriptSubsystem, bool refreshScriptCatalog )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( tspScriptSubsystem is null )
                throw new ArgumentNullException( nameof( tspScriptSubsystem ) );
            if ( refreshScriptCatalog )
            {
                tspScriptSubsystem.FetchSavedScripts();
            }

            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !string.IsNullOrWhiteSpace( script.Name ) )
                {
                    if ( !tspScriptSubsystem.SavedScriptExists( script.Name, node, false ) )
                    {
                        this.IdentifiedScript = script;
                        if ( this._OutcomeDetails.Length > 0 )
                        {
                            _ = this._OutcomeDetails.AppendLine();
                        }

                        _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "saved custom firmware '{0}' not found on '{1}' on node {2}.", script.Name, tspScriptSubsystem.ResourceNameCaption, node.Number );
                        // if script not found return false
                        return false;
                    }
                }
            }

            return this._OutcomeDetails.Length == 0;
        }

        /// <summary> Checks if any script exists on the specified instrument and node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node">    Specifies the node. </param>
        /// <param name="session"> Specifies the <see cref="VI.Pith.SessionBase">TSP session.</see> </param>
        /// <returns>
        /// <c>True</c> if okay; otherwise, <c>False</c> if any exception had occurred.
        /// </returns>
        public bool FindAnyScript( NodeEntityBase node, Pith.SessionBase session )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( TItem script in this.Items )
            {
                if ( script.IsModelMatch( node.ModelNumber ) && !string.IsNullOrWhiteSpace( script.Name ) )
                {
                    if ( !session.IsNil( node.IsController, node.Number, script.Name ) )
                    {
                        this.IdentifiedScript = script;
                        if ( this._OutcomeDetails.Length > 0 )
                        {
                            _ = this._OutcomeDetails.AppendLine();
                        }

                        _ = this._OutcomeDetails.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, "custom firmware '{0}' found on '{1}' node {2}.", script.Name, session.ResourceNameCaption, node.Number );
                        // if script found return true
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary> Checks if any script exists on all nodes from the specified session. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">      Specifies the <see cref="VI.Pith.SessionBase">TSP session.</see> </param>
        /// <param name="nodeEntities"> The node entities. </param>
        /// <returns> <c>True</c> if any script exists. </returns>
        public bool FindAnyScript( Pith.SessionBase session, NodeEntityBase[] nodeEntities )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( nodeEntities is null )
                throw new ArgumentNullException( nameof( nodeEntities ) );
            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( NodeEntityBase node in nodeEntities )
            {
                if ( this.FindAnyScript( node, session ) )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary> Checks if any script exists on all nodes from the specified session. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">      Specifies the <see cref="VI.Pith.SessionBase">TSP session.</see> </param>
        /// <param name="nodeEntities"> The node entities. </param>
        /// <returns> <c>True</c> if any script exists. </returns>
        public bool FindAnyScript( Pith.SessionBase session, NodeEntityCollection nodeEntities )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( nodeEntities is null )
                throw new ArgumentNullException( nameof( nodeEntities ) );
            this._OutcomeDetails = new System.Text.StringBuilder();
            this.IdentifiedScript = null;
            foreach ( NodeEntityBase node in nodeEntities )
            {
                if ( this.FindAnyScript( node, session ) )
                {
                    return true;
                }
            }

            return false;
        }


        #endregion

    }

    #region " ENUMERATIONS "

    /// <summary> Enumerates the script file formats. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags()]
    public enum ScriptFileFormats
    {

        /// <summary> An enum constant representing the uncompressed human readable option. </summary>
        [System.ComponentModel.Description( "Uncompressed Human Readable" )]
        None = 0,

        /// <summary> An enum constant representing the binary option. </summary>
        [System.ComponentModel.Description( "Binary format" )]
        Binary = 1,

        /// <summary> An enum constant representing the compressed option. </summary>
        [System.ComponentModel.Description( "Compressed" )]
        Compressed = 2
    }

    /// <summary> Enumerates the validation status. </summary>
    public enum FirmwareVersionStatus
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "No Specified" )]
        None = 0,

        /// <summary> Firmware is older than expected (released) indicating that the firmware needs to be updated. </summary>
        [System.ComponentModel.Description( "Version as older than expected" )]
        Older = 1,

        /// <summary> Firmware is current.  </summary>
        [System.ComponentModel.Description( "Version is current" )]
        Current = 2,

        /// <summary> Embedded firmware is newer than expected indicating that the distributed program is out of date. </summary>
        [System.ComponentModel.Description( "Version as new than expected" )]
        Newer = 3,

        /// <summary> Embedded firmware version was not set. </summary>
        [System.ComponentModel.Description( "Expected version not known (empty)" )]
        Unknown = 4,

        /// <summary> Released firmware was not set. </summary>
        [System.ComponentModel.Description( "Expected version not known (empty)" )]
        ReferenceUnknown = 5,

        /// <summary> Version command function does not exist. </summary>
        [System.ComponentModel.Description( "Version command is missing -- version is nil" )]
        Missing = 6
    }
}

#endregion
