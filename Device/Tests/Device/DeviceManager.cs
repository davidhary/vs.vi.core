
namespace isr.VI.DeviceTests
{

    /// <summary>
    /// Test manager for <see cref="VisaSessionBase"/> and <see cref="SubsystemBase"/> Tests.
    /// </summary>
    /// <remarks>
    /// David, 2019-12-12 <para>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public sealed partial class DeviceManager
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private DeviceManager() : base()
        {
        }
    }
}
