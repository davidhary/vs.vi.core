using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.SplitExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{
    public sealed partial class DeviceManager
    {

        #region " SESSION "

        /// <summary> Assert session initial values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">        The session. </param>
        /// <param name="resourceInfo">   Information describing the resource. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSessionInitialValuesShouldMatch( Pith.SessionBase session, ResourceSettingsBase resourceInfo, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            if ( resourceInfo is null )
                throw new ArgumentNullException( nameof( resourceInfo ) );
            if ( !resourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{resourceInfo.ResourceTitle} not found" );
            string propertyName = nameof( Pith.ServiceRequests.ErrorAvailable ).SplitWords();
            int expectedErrorAvailableBits = ( int ) Pith.ServiceRequests.ErrorAvailable;
            int actualErrorAvailableBits = ( int ) session.ErrorAvailableBit;
            Assert.AreEqual( expectedErrorAvailableBits, actualErrorAvailableBits, $"{resourceInfo.ResourceTitle} {propertyName} bits on creating device should match" );
        }

        /// <summary> Assert session open values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">      The session. </param>
        /// <param name="resourceInfo"> Information describing the resource. </param>
        public static void AssertSessionOpenValuesShouldMatch( Pith.SessionBase session, ResourceSettingsBase resourceInfo )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( resourceInfo is null )
                throw new ArgumentNullException( nameof( resourceInfo ) );
            string propertyName = nameof( Pith.SessionBase.ValidatedResourceName ).SplitWords();
            string actualResource = session.ValidatedResourceName;
            string expectedResource = resourceInfo.ResourceName;
            Assert.AreEqual( expectedResource, actualResource, $"{resourceInfo.ResourceTitle} {propertyName} should match" );
            propertyName = nameof( Pith.SessionBase.CandidateResourceName ).SplitWords();
            actualResource = session.CandidateResourceName;
            expectedResource = resourceInfo.ResourceName;
            Assert.AreEqual( expectedResource, actualResource, $"{resourceInfo.ResourceTitle} {propertyName} should match" );
            propertyName = nameof( Pith.SessionBase.OpenResourceName ).SplitWords();
            actualResource = session.OpenResourceName;
            expectedResource = resourceInfo.ResourceName;
            Assert.AreEqual( expectedResource, actualResource, $"{resourceInfo.ResourceTitle} {propertyName} should match" );
            propertyName = nameof( Pith.SessionBase.OpenResourceTitle ).SplitWords();
            string actualTitle = session.OpenResourceTitle;
            string expectedTitle = resourceInfo.ResourceTitle;
            Assert.AreEqual( expectedTitle, actualTitle, $"{resourceInfo.ResourceTitle} {propertyName} should match" );
            propertyName = nameof( Pith.SessionBase.CandidateResourceTitle ).SplitWords();
            actualTitle = session.CandidateResourceTitle;
            expectedTitle = resourceInfo.ResourceTitle;
            Assert.AreEqual( expectedTitle, actualTitle, $"{resourceInfo.ResourceTitle} {propertyName} should match" );
        }

        /// <summary> Assert termination values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">        The session. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertTerminationValuesShouldMatch( Pith.SessionBase session, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );

            // read termination is enabled from the status subsystem. It is disabled at the IVI Visa level. 
            bool actualReadTerminationEnabled = session.ReadTerminationCharacterEnabled;
            bool expectedReadTerminationEnabled = subsystemsInfo.InitialReadTerminationEnabled;
            string propertyName = nameof( Pith.SessionBase.ReadTerminationCharacterEnabled ).SplitWords();
            Assert.AreEqual( expectedReadTerminationEnabled, actualReadTerminationEnabled, $"{session.ResourceNameCaption} initial {propertyName} should match" );

            propertyName = nameof( Pith.SessionBase.ReadTerminationCharacter ).SplitWords();
            int actualTermination = session.ReadTerminationCharacter;
            int expectedTermination = subsystemsInfo.InitialReadTerminationCharacter;
            Assert.AreEqual( expectedTermination, actualTermination, $"{session.ResourceNameCaption} initial {propertyName} value should match" );

            propertyName = nameof( Pith.SessionBase.ReadTerminationCharacterEnabled ).SplitWords();
            expectedReadTerminationEnabled = !subsystemsInfo.ReadTerminationEnabled;
            session.ReadTerminationCharacterEnabled = expectedReadTerminationEnabled;
            actualReadTerminationEnabled = session.ReadTerminationCharacterEnabled;
            Assert.AreEqual( expectedReadTerminationEnabled, actualReadTerminationEnabled, $"{session.ResourceNameCaption} toggled {propertyName} should match" );

            expectedReadTerminationEnabled = subsystemsInfo.ReadTerminationEnabled;
            session.ReadTerminationCharacterEnabled = expectedReadTerminationEnabled;
            actualReadTerminationEnabled = session.ReadTerminationCharacterEnabled;
            Assert.AreEqual( expectedReadTerminationEnabled, actualReadTerminationEnabled, $"{session.ResourceNameCaption} restored {propertyName} should match" );

            propertyName = nameof( Pith.SessionBase.ReadTerminationCharacter ).SplitWords();
            actualTermination = session.ReadTerminationCharacter;
            expectedTermination = subsystemsInfo.ReadTerminationCharacter;
            Assert.AreEqual( expectedTermination, actualTermination, $"{session.ResourceNameCaption} {propertyName} value should match" );
            Assert.AreEqual( ( byte ) Strings.AscW( session.TerminationCharacters().ElementAtOrDefault( 0 ) ), session.ReadTerminationCharacter, $"{session.ResourceNameCaption} first termination character value should match" );
        }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Assert device errors should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertDeviceErrorsShouldMatch( StatusSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string propertyName = nameof( Pith.ServiceRequests.ErrorAvailable ).SplitWords();
            Assert.IsFalse( subsystem.Session.IsErrorBitSet(), $"{subsystem.ResourceNameCaption} {propertyName} bit {subsystem.Session.ServiceRequestStatus:X} should be off" );
            Assert.IsFalse( subsystem.ErrorAvailable, $"{subsystem.ResourceNameCaption} error available bit {subsystem.Session.ServiceRequestStatus:X} is on; last device error: {subsystem.CompoundErrorMessage}" );
            _ = nameof( Pith.ServiceRequests.ErrorAvailable ).SplitWords();
            string errors = subsystem.DeviceErrorReport;
            Assert.IsTrue( string.IsNullOrWhiteSpace( errors ), $"{subsystem.ResourceNameCaption} device errors: {errors}" );
            Assert.IsFalse( subsystem.HasDeviceError, $"{subsystem.ResourceNameCaption} device error report: {subsystem.DeviceErrorReport}" );

            var deviceError = new DeviceError();
            deviceError.Parse( subsystemsInfo.ParseCompoundErrorMessage );
            propertyName = nameof( DeviceError.ErrorMessage ).SplitWords();
            Assert.AreEqual( subsystemsInfo.ParseErrorMessage, deviceError.ErrorMessage, $"{subsystem.ResourceNameCaption} parsed {propertyName} should match" );

            propertyName = nameof( DeviceError.ErrorNumber ).SplitWords();
            Assert.AreEqual( subsystemsInfo.ParseErrorNumber, deviceError.ErrorNumber, $"{subsystem.ResourceNameCaption} parsed {propertyName} should match" );

            propertyName = nameof( DeviceError.ErrorLevel ).SplitWords();
            Assert.AreEqual( subsystemsInfo.ParseErrorLevel, deviceError.ErrorLevel, $"{subsystem.ResourceNameCaption} parsed {propertyName} should match" );
        }

        /// <summary> Assert session device errors should clear. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device">         The device. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSessionDeviceErrorsShouldClear( VisaSessionBase device, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            device.Session.ClearActiveState();
            AssertDeviceErrorsShouldMatch( device.StatusSubsystemBase, subsystemsInfo );
        }

        /// <summary> Assert device errors should read. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device">         The device. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertDeviceErrorsShouldRead( VisaSessionBase device, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );

            // send an erroneous command
            string erroneousCommand = subsystemsInfo.ErroneousCommand;
            _ = device.StatusSubsystemBase.WriteLine( erroneousCommand );
            var appliedDelay = Core.ApplianceBase.DoEventsWaitElapsed( TimeSpan.FromMilliseconds( subsystemsInfo.ErrorAvailableMillisecondsDelay ) );

            // read the service request status; this should generate an error available 
            int value = ( int ) device.Session.ReadStatusRegister();

            // check the error bits
            var actualServiceRequest = device.StatusSubsystemBase.ErrorAvailableBit;
            var expectedSeriveRequest = Pith.ServiceRequests.ErrorAvailable;
            Assert.AreEqual( expectedSeriveRequest, actualServiceRequest,
                            $"{device.ResourceNameCaption} error bits expected {expectedSeriveRequest:X} <> actual {actualServiceRequest:X}" );

            // check the error available status
            Assert.IsTrue( (value & ( int ) expectedSeriveRequest) == ( int ) expectedSeriveRequest,
                          $"{device.ResourceNameCaption} error bits {expectedSeriveRequest:X} are expected in value: {value:X}; applied {appliedDelay:s\\.fff}s delay" );

            // check the error available status
            actualServiceRequest = device.Session.ServiceRequestStatus;
            Assert.IsTrue( (actualServiceRequest & expectedSeriveRequest) == expectedSeriveRequest, $"{device.ResourceNameCaption} error bits {expectedSeriveRequest:X} are expected in {nameof( Pith.SessionBase.ServiceRequestStatus )}: {actualServiceRequest:X}" );
            bool actualErrorAvailable = device.StatusSubsystemBase.ErrorAvailable;
            Assert.IsTrue( actualErrorAvailable, $"{device.ResourceNameCaption} an error is expected" );

            string propertyName = nameof( DeviceError.ErrorMessage ).SplitWords();
            Assert.AreEqual( subsystemsInfo.ExpectedErrorMessage, device.StatusSubsystemBase.DeviceErrorQueue.LastError.ErrorMessage, $"{device.ResourceNameCaption} {propertyName} should match" );

            propertyName = nameof( DeviceError.ErrorNumber ).SplitWords();
            Assert.AreEqual( subsystemsInfo.ExpectedErrorNumber, device.StatusSubsystemBase.DeviceErrorQueue.LastError.ErrorNumber, $"{device.ResourceNameCaption} {propertyName} should match" );

            propertyName = nameof( DeviceError.ErrorLevel ).SplitWords();
            Assert.AreEqual( subsystemsInfo.ExpectedErrorLevel, device.StatusSubsystemBase.DeviceErrorQueue.LastError.ErrorLevel, $"{device.ResourceNameCaption} {propertyName} should match" );
        }

        #endregion

        #region " CHANNEL SUBSYSTEM "

        /// <summary> Assert initial subsystem values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( ChannelSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            Assert.IsTrue( string.IsNullOrWhiteSpace( subsystem.ClosedChannels ), $"Scan list {subsystem.ClosedChannels}; expected empty" );
        }

        #endregion

        #region " MEASURE SUBSYSTEM "

        /// <summary>   Assert subsystem initial values should match. </summary>
        /// <remarks>   David, 2021-07-05. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="subsystem">        The subsystem. </param>
        /// <param name="subsystemsInfo">   Information describing the subsystems. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( MeasureSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
        }

        #endregion

        #region " MULTIMETER SUBSYSTEM "

        /// <summary> Assert initial subsystem values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( MultimeterSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.PowerLineCycles )}";
            double expectedPowerLineCycles = subsystemsInfo.InitialPowerLineCycles;
            double? actualPowerLineCycles = subsystem.QueryPowerLineCycles().GetValueOrDefault( 0d );
            Assert.IsTrue( actualPowerLineCycles.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );

            double epsilon = subsystemsInfo.LineFrequency / TimeSpan.TicksPerSecond;
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles.Value, epsilon, $"{subsystem.ResourceNameCaption} {propertyName} value should match expect value withing {epsilon}" );
            propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.AutoRangeEnabled )}";
            bool expectedBoolean = subsystemsInfo.InitialAutoRangeEnabled;
            bool? actualBoolean = subsystem.QueryAutoRangeEnabled().GetValueOrDefault( false );
            Assert.IsTrue( actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match" );

            propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.AutoZeroEnabled )}";
            expectedBoolean = subsystemsInfo.InitialAutoZeroEnabled;
            actualBoolean = subsystem.QueryAutoZeroEnabled().GetValueOrDefault( false );
            Assert.IsTrue( actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match" );

            propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FunctionMode )}";
            var expectedFunctionMode = subsystemsInfo.InitialMultimeterFunction;
            var actualFunctionMode = subsystem.QueryFunctionMode();
            Assert.IsTrue( actualFunctionMode.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedFunctionMode, ( object ) actualFunctionMode, $"{subsystem.ResourceNameCaption} {propertyName} should match" );

            propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FilterEnabled )}";
            expectedBoolean = subsystemsInfo.InitialFilterEnabled;
            actualBoolean = subsystem.QueryFilterEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match" );

            propertyName = $"initial {propertyName}reading {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.MovingAverageFilterEnabled )}";
            expectedBoolean = subsystemsInfo.InitialMovingAverageFilterEnabled;
            actualBoolean = subsystem.QueryMovingAverageFilterEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match" );

            propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FilterWindow )}";
            double expectedFilterWindow = subsystemsInfo.InitialFilterWindow;
            var actualFilterWindow = subsystem.QueryFilterWindow();
            Assert.IsTrue( actualBoolean.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedFilterWindow, actualFilterWindow.Value, 0.1d * expectedFilterWindow, $"{subsystem.ResourceNameCaption} {propertyName} should match withing {epsilon}" );

            propertyName = $"initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FilterCount )}";
            int expectedFilterCount = subsystemsInfo.InitialFilterCount;
            var actualFilterCount = subsystem.QueryFilterCount();
            Assert.IsTrue( actualFilterCount.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedFilterCount, actualFilterCount.Value, $"{subsystem.ResourceNameCaption} {propertyName} should match" );
        }

        #endregion

        #region " OUTPUT SUBSYSTEM "

        /// <summary> Assert digital output signal polarity should toggle. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="digitalOutputLineNumber"> The digital output line number. </param>
        /// <param name="subsystem">               The subsystem. </param>
        public static void AssertDigitalOutputSignalPolarityShouldToggle( int digitalOutputLineNumber, DigitalOutputSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            string deviceName = subsystem.ResourceNameCaption;
            string propertyName = $"{typeof( DigitalOutputSubsystemBase )}.{nameof( DigitalOutputSubsystemBase.CurrentDigitalActiveLevel )}";
            string activity = $"Reading [{deviceName}].[{propertyName}({digitalOutputLineNumber})] initial value";
            var initialPolarity = subsystem.QueryDigitalActiveLevel( digitalOutputLineNumber );
            Assert.IsTrue( initialPolarity.HasValue, $"{activity} should have a value" );

            var expectedPolarity = initialPolarity.Value == DigitalActiveLevels.High ? DigitalActiveLevels.Low : DigitalActiveLevels.High;
            activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}";
            var actualPolarity = subsystem.ApplyDigitalActiveLevel( digitalOutputLineNumber, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );

            expectedPolarity = initialPolarity.Value;
            activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}";
            actualPolarity = subsystem.ApplyDigitalActiveLevel( digitalOutputLineNumber, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );
        }

        /// <summary> Assert digital output signal polarity should toggle. </summary>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertDigitalOutputSignalPolarityShouldToggle( DigitalOutputSubsystemBase subsystem )
        {
            var initialPolarities = new List<DigitalActiveLevels>();
            var inputLineNumbers = new List<int>( new int[] { 1, 2, 3, 4 } );
            var inputLineNumbersCaption = new System.Text.StringBuilder();
            int inputLineNumber;
            foreach ( var currentInputLineNumber in inputLineNumbers )
            {
                inputLineNumber = currentInputLineNumber;
                if ( inputLineNumbersCaption.Length > 0 )
                    _ = inputLineNumbersCaption.Append( "," );
                _ = inputLineNumbersCaption.Append( $"{inputLineNumber}" );
                initialPolarities.Add( subsystem.QueryDigitalActiveLevel( inputLineNumber ).GetValueOrDefault( DigitalActiveLevels.Low ) );
            }

            foreach ( var currentInputLineNumber1 in inputLineNumbers )
            {
                inputLineNumber = currentInputLineNumber1;
                AssertDigitalOutputSignalPolarityShouldToggle( inputLineNumber, subsystem );
            }

            inputLineNumber = inputLineNumbers[0];
            string propertyName = $"{typeof( DigitalOutputSubsystemBase )}.{nameof( DigitalOutputSubsystemBase.CurrentDigitalActiveLevel )}";
            string deviceName = subsystem.ResourceNameCaption;
            string activity = $"Reading [{deviceName}].[{propertyName}({inputLineNumber})] initial value";
            var initialPolarity = subsystem.QueryDigitalActiveLevel( inputLineNumber );
            Assert.IsTrue( initialPolarity.HasValue, $"{activity} should have a value" );
            var expectedPolarity = initialPolarity.Value == DigitalActiveLevels.High ? DigitalActiveLevels.Low : DigitalActiveLevels.High;
            activity = $"Setting [{deviceName}].[{propertyName}({inputLineNumbersCaption})] to {expectedPolarity}";
            var actualPolarity = subsystem.ApplyDigitalActiveLevel( inputLineNumbers, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );
            expectedPolarity = initialPolarity.Value;
            activity = $"Setting [{deviceName}].[{propertyName}({inputLineNumbersCaption})] to {expectedPolarity}";
            actualPolarity = subsystem.ApplyDigitalActiveLevel( inputLineNumbers, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );
            activity = $"Applying initial [{deviceName}].[{propertyName}s] to lines {inputLineNumbersCaption}";
            var actualPolarities = new List<DigitalActiveLevels?>( subsystem.ApplyDigitalActiveLevels( inputLineNumbers, initialPolarities ) );
            foreach ( var currentInputLineNumber2 in inputLineNumbers )
            {
                inputLineNumber = currentInputLineNumber2;
                Assert.AreEqual( initialPolarities[inputLineNumber - 1], actualPolarities[inputLineNumber - 1].Value, $"{activity} [{deviceName}].[{propertyName}({inputLineNumber})] should equal actual value" );
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Assert initial subsystem values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( RouteSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string propertyName = nameof( RouteSubsystemBase.ClosedChannels ).SplitWords();
            string expectedClosedChannels = subsystemsInfo.InitialClosedChannels;
            if ( subsystem.SupportsClosedChannelsQuery )
            {
                string actualClosedChannels = subsystem.QueryClosedChannels();
                Assert.AreEqual( expectedClosedChannels, actualClosedChannels, $"{subsystem.ResourceNameCaption} initial {propertyName} should be expected" );
            }

            if ( !subsystem.ScanListPersists )
            {
                propertyName = nameof( RouteSubsystemBase.ScanList ).SplitWords();
                string expectedScanList = subsystemsInfo.InitialScanList;
                string actualScanList = subsystem.QueryScanList();
                Assert.AreEqual( expectedScanList, actualScanList, $"{subsystem.ResourceNameCaption} initial {propertyName} should be expected" );
            }
        }

        /// <summary> Assert slot card information should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="cardNumber">   The card number. </param>
        /// <param name="expectedName"> Name of the expected. </param>
        public static void AssertSlotCardInfoShouldMatch( RouteSubsystemBase subsystem, int cardNumber, string expectedName )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            string propertyName = nameof( RouteSubsystemBase.SlotCardType ).SplitWords();
            string actualName = subsystem.QuerySlotCardType( cardNumber );
            Assert.AreEqual( expectedName, actualName, $"{subsystem.ResourceNameCaption} slot card #{cardNumber} {propertyName} should be expected" );
            propertyName = nameof( RouteSubsystemBase.SlotCardSettlingTime ).SplitWords();
            var existingSettlingTime = subsystem.QuerySlotCardSettlingTime( cardNumber );
            var expectedSettlingTime = TimeSpan.FromMilliseconds( 11d );
            var actualSettlingTime = subsystem.ApplySlotCardSettlingTime( cardNumber, expectedSettlingTime );
            Assert.AreEqual( expectedSettlingTime, actualSettlingTime, $"{subsystem.ResourceNameCaption} slot card #{cardNumber} {propertyName} should be expected" );
            actualSettlingTime = subsystem.ApplySlotCardSettlingTime( cardNumber, existingSettlingTime );
            Assert.AreEqual( existingSettlingTime, actualSettlingTime, $"{subsystem.ResourceNameCaption} slot card #{cardNumber} restored {propertyName} should be expected" );
        }

        /// <summary> Assert scan card installed should match. </summary>
        /// <remarks> David, 2020-04-04. </remarks>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertScanCardInstalledShouldMatch( SystemSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            Assert.AreEqual( subsystemsInfo.ScanCardInstalled, subsystem.InstalledScanCards.Any(), $"Scan card should {(subsystemsInfo.ScanCardInstalled ? string.Empty : "not")} be installed" );
        }

        /// <summary> Assert scan card internal scan should set. </summary>
        /// <remarks> David, 2020-04-04. </remarks>
        /// <param name="routeSubsystem">  The route subsystem. </param>
        /// <param name="systemSubsystem"> The system subsystem. </param>
        /// <param name="subsystemsInfo">  Information describing the subsystems. </param>
        public static void AssertScanCardInternalScanShouldSet( RouteSubsystemBase routeSubsystem, SystemSubsystemBase systemSubsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( !systemSubsystem.InstalledScanCards.Any() )
                return;
            string expectedScanList = subsystemsInfo.InitialScanList;
            string actualScanList = routeSubsystem.QueryScanList();
            Assert.AreEqual( expectedScanList, actualScanList, $"Scan list should be '{expectedScanList}'" );
            expectedScanList = "(@1,2)";
            actualScanList = routeSubsystem.ApplyScanList( expectedScanList );
            Assert.AreEqual( expectedScanList, actualScanList, $"Scan list should be '{expectedScanList}'" );
            var expectedScanlistType = ScanListType.None;
            string expectedSelectedScanlistType = routeSubsystem.ScanListTypeReadWrites.SelectItem( ( long ) expectedScanlistType ).ReadValue;
            string actualSelectedScanlistType = routeSubsystem.ApplySelectedScanListType( expectedSelectedScanlistType );
            var actualScanListType = routeSubsystem.ScanListType;
            Assert.AreEqual( expectedSelectedScanlistType, actualSelectedScanlistType, $"Scan list type should be '{expectedSelectedScanlistType}'" );
            Assert.AreEqual( expectedScanlistType, actualScanListType, $"Scan list type should be '{expectedScanlistType}'" );
            expectedScanlistType = ScanListType.Internal;
            actualScanListType = routeSubsystem.ApplySelectedScanListType( expectedScanlistType );
            Assert.AreEqual( expectedScanlistType, actualScanListType, $"Applied scan list type should be '{expectedScanlistType}'" );
            expectedScanlistType = ScanListType.None;
            actualScanListType = routeSubsystem.ApplySelectedScanListType( expectedScanlistType );
            Assert.AreEqual( expectedScanlistType, actualScanListType, $"Cleared scan list type should be '{expectedScanlistType}'" );
            expectedScanList = "(@1,2)";
            actualScanList = routeSubsystem.QueryScanList();
            Assert.AreEqual( expectedScanList, actualScanList, $"Scan list should be '{expectedScanList}'" );

            // Dim caldidateScanListType As String = routeSubsystem.ScanListTypeReadWrites.SelectItem(expectedScanlistType).WriteValue
            // expectedSelectedScanlistType = "INT"
            // actualScanList = routeSubsystem.QueryScanList
            // Assert.AreEqual(expectedSelectedScanlistType, actualSelectedScanlistType, $"Scan list type should be '{expectedSelectedScanlistType}'")
            // expectedSelectedScanlistType = "NONE"
            // actualSelectedScanlistType = routeSubsystem.ApplySelectedScanListType(expectedSelectedScanlistType)
            // Assert.AreEqual(expectedSelectedScanlistType, actualSelectedScanlistType, $"Scan list type should be '{expectedSelectedScanlistType}'")
        }


        #endregion

        #region " SENSE SUBSYSTEM "

        /// <summary> Assert initial sense subsystem values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( SenseSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string deviceName = subsystem.ResourceNameCaption;
            string propertyName = $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.PowerLineCycles )}";
            double expectedPowerLineCycles = subsystemsInfo.InitialPowerLineCycles;
            double actualPowerLineCycles = subsystem.QueryPowerLineCycles().GetValueOrDefault( 0d );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, subsystemsInfo.LineFrequency / TimeSpan.TicksPerSecond, $"[{deviceName}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.AutoRangeEnabled )}";
            bool expectedBoolean = subsystemsInfo.InitialAutoRangeEnabled;
            bool actualBoolean = subsystem.QueryAutoRangeEnabled().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{deviceName}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.FunctionMode )}";
            var actualFunctionMode = subsystem.QueryFunctionMode().GetValueOrDefault( SenseFunctionModes.Resistance );
            var expectedFunctionMode = subsystemsInfo.InitialSenseFunction;
            Assert.AreEqual( expectedFunctionMode, actualFunctionMode, $"[{deviceName}].[{propertyName}] should equal expected value" );
        }

        /// <summary> Assert sense subsystem function mode should toggle. </summary>
        /// <remarks> David, 2020-07-29. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="toggleFirst">  The toggle first. </param>
        /// <param name="toggleSecond"> The toggle second. </param>
        public static void AssertFunctionModeShouldToggle( SenseSubsystemBase subsystem, SenseFunctionModes toggleFirst, SenseFunctionModes toggleSecond )
        {
            var initialFunctionMode = subsystem.QueryFunctionMode().GetValueOrDefault( SenseFunctionModes.None );
            var expectedFunctionMode = initialFunctionMode == toggleFirst ? toggleSecond : toggleFirst;
            var actualFunctionMode = subsystem.ApplyFunctionMode( expectedFunctionMode ).GetValueOrDefault( SenseFunctionModes.None );
            Assert.AreEqual( expectedFunctionMode, actualFunctionMode, $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.FunctionMode )} should be toggled" );

            // restore function mode
            actualFunctionMode = subsystem.ApplyFunctionMode( initialFunctionMode ).GetValueOrDefault( SenseFunctionModes.None );
            Assert.AreEqual( initialFunctionMode, actualFunctionMode, $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.FunctionMode )} should be restored" );
        }

        #endregion

        #region " SOURCE SUBSYSTEM "

        /// <summary> Assert initial subsystem values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( SourceSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
        }

        /// <summary> Assert output enabled should toggle. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">     The subsystem. </param>
        /// <param name="outputEnabled"> True to enable, false to disable the output. </param>
        public static void AssertOutputEnabledSouldToggle( SourceSubsystemBase subsystem, bool outputEnabled )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            bool expectedOutputEnabled = outputEnabled;
            bool actualOutputEnabled = subsystem.ApplyOutputEnabled( expectedOutputEnabled ).GetValueOrDefault( !expectedOutputEnabled );
            Assert.AreEqual( expectedOutputEnabled, actualOutputEnabled, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.OutputEnabled )} is {actualOutputEnabled}; expected {expectedOutputEnabled}" );
        }

        #endregion

        #region " STATUS SUBSYSTEM "

        /// <summary> Assert line frequency should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertLineFrequencyShouldMatch( StatusSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string propertyName = nameof( StatusSubsystemBase.LineFrequency ).SplitWords();
            double actualFrequency = subsystem.LineFrequency.GetValueOrDefault( 0d );
            Assert.AreEqual( subsystemsInfo.LineFrequency, actualFrequency, $"{subsystem.ResourceNameCaption} {propertyName} should match" );
        }

        /// <summary> Assert integration period should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertIntegrationPeriodShouldMatch( StatusSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string propertyName = "Integration Period";
            double expectedPowerLineCycles = subsystemsInfo.InitialPowerLineCycles;
            var expectedIntegrationPeriod = StatusSubsystemBase.FromSecondsPrecise( expectedPowerLineCycles / subsystemsInfo.LineFrequency );
            var actualIntegrationPeriod = StatusSubsystemBase.FromPowerLineCycles( expectedPowerLineCycles );
            Assert.AreEqual( expectedIntegrationPeriod, actualIntegrationPeriod, $"{propertyName} for {expectedPowerLineCycles} power line cycles is {actualIntegrationPeriod}; expected {expectedIntegrationPeriod}" );
            propertyName = "Power line cycles";
            double actualPowerLineCycles = StatusSubsystemBase.ToPowerLineCycles( actualIntegrationPeriod );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, subsystemsInfo.LineFrequency / TimeSpan.TicksPerSecond, $"{propertyName} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}" );
        }

        /// <summary> Assert device model should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="resourceInfo"> Information describing the resource. </param>
        public static void AssertDeviceModelShouldMatch( StatusSubsystemBase subsystem, ResourceSettingsBase resourceInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( resourceInfo is null )
                throw new ArgumentNullException( nameof( resourceInfo ) );
            string propertyName = nameof( VersionInfoBase.Model ).SplitWords();
            Assert.AreEqual( resourceInfo.ResourceModel, subsystem.VersionInfoBase.Model, $"Version Info {propertyName} {subsystem.ResourceNameCaption} Identity: '{subsystem.VersionInfoBase.Identity}'", System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Assert orphan messages should be empty. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertOrphanMessagesShouldBeEmpty( StatusSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            string orphanMessages = subsystem.UnreadMessages;
            Assert.IsTrue( string.IsNullOrWhiteSpace( orphanMessages ), $"{subsystem.ResourceNameCaption} orphan messages {orphanMessages} should be empty" );
        }

        /// <summary>   Assert keep alive message could be sent. </summary>
        /// <remarks>   David, 2021-06-01. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="subsystem">    The subsystem. </param>
        public static void AssertKeepAliveMessageCouldBeSent( StatusSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            subsystem.Session.KeepAliveLockTimeout = TimeSpan.FromMilliseconds( 12 );
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            subsystem.Session.KeepAlive();
            sw.Stop();
            Console.Out.WriteLine( $"Keep alive took {sw.ElapsedMilliseconds:F0}ms" );
            _ = subsystem.ReadIdentity();
        }

        #endregion

        #region " TRIGGER SUBSYSTEM "

        /// <summary> Assert initial subsystem values should match. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem">      The subsystem. </param>
        /// <param name="subsystemsInfo"> Information describing the subsystems. </param>
        public static void AssertSubsystemInitialValuesShouldMatch( TriggerSubsystemBase subsystem, SubsystemsSettingsBase subsystemsInfo )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            if ( subsystemsInfo is null )
                throw new ArgumentNullException( nameof( subsystemsInfo ) );
            string propertyName = nameof( TriggerSubsystemBase.TriggerSource ).SplitWords();
            var expectedTriggerSource = subsystemsInfo.InitialTriggerSource;
            var actualTriggerSource = subsystem.QueryTriggerSource();
            Assert.IsTrue( actualTriggerSource.HasValue, $"{subsystem.ResourceNameCaption} {propertyName} should have a value" );
            Assert.AreEqual( expectedTriggerSource, actualTriggerSource.Value, $"{subsystem.ResourceNameCaption} initial {propertyName} should be expected" );
        }

        #endregion

        #region " SERVICE REQUEST POLLING "

        /// <summary> Assert device could be polled. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        public static void AssertDeviceShouldBePolled( VisaSessionBase device )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            Assert.IsTrue( device.IsDeviceOpen, "session should be open" );

            // ensure service request handling is disabled.
            device.Session.DisableServiceRequestEventHandler();
            // must refresh the service request here to update the status register flags.
            _ = device.Session.ReadStatusRegister();
            Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should be disabled" );
            device.Session.ApplyServiceRequestEnableBitmask( device.Session.DefaultServiceRequestEnableBitmask );
            Assert.IsFalse( device.Session.ServiceRequestEventEnabled, "service request event should not be enabled" );
            string expectedServiceRequestEnableCommand = string.Format( $"*SRE {( int ) device.Session.ServiceRequestEnabledBitmask}" );
            Assert.AreEqual( expectedServiceRequestEnableCommand, device.Session.ServiceRequestEnableCommand, $"{nameof( Pith.SessionBase.ServiceRequestEnableCommand ).SplitWords()} should be as expected" );
            try
            {
                device.PollTimespan = TimeSpan.FromMilliseconds( 100d );
                Assert.IsFalse( device.PollAutoRead, $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be disabled" );
                device.PollAutoRead = true;
                Assert.IsTrue( device.PollAutoRead, $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be enabled" );
                Assert.IsFalse( device.PollEnabled, $"{nameof( VisaSessionBase.PollEnabled ).SplitWords()} should be disabled" );
                device.PollEnabled = true;
                Assert.IsTrue( device.PollEnabled, $"{nameof( VisaSessionBase.PollEnabled ).SplitWords()} should be enabled" );
                Assert.IsTrue( string.IsNullOrWhiteSpace( device.PollReading ), $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be empty" );

                // read operation completion
                string expectedSPollMessage = "1";
                string pollMessageName = "Operation Completed";
                string queryCommand = "*OPC?";
                _ = device.Session.WriteLine( queryCommand );

                // wait for the message
                device.StartAwaitingPollReadingTask( TimeSpan.FromTicks( 100L * device.PollTimespan.Ticks ) ).Wait();
                Assert.IsFalse( string.IsNullOrWhiteSpace( device.PollReading ), $"Poll reading '{device.PollReading}' should not be empty" );
                Assert.AreEqual( expectedSPollMessage, device.PollReading, $"Expected {pollMessageName} {queryCommand} message" );
                device.PollAutoRead = false;
                device.PollEnabled = false;
                _ = device.Session.ReadStatusRegister();
                // wait for the poll timer to disabled
                device.StartAwaitingPollTimerDisabledTask( TimeSpan.FromTicks( 2L * device.PollTimespan.Ticks ) ).Wait();
                Assert.IsFalse( device.PollAutoRead, $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be disabled" );
                Assert.IsFalse( device.PollEnabled, $"{nameof( VisaSessionBase.PollEnabled ).SplitWords()} should be disabled" );
                Assert.IsFalse( device.PollTimerEnabled, $"{nameof( VisaSessionBase.PollTimerEnabled ).SplitWords()} should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                device.PollAutoRead = false;
                device.PollEnabled = false;
                _ = device.Session.ReadStatusRegister();
            }
        }

        /// <summary>   Assert query result should be polled. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="device">                               The device. </param>
        /// <param name="queryCommand">                         The query command. </param>
        /// <param name="expectedPollMessageAvailableBitmask">  The expected poll message available
        ///                                                     bitmask. </param>
        /// <param name="expectedReply">                        The expected reply. </param>
        public static void AssertQueryResultShouldBePolled( VisaSessionBase device, string queryCommand,
                                                               int expectedPollMessageAvailableBitmask, string expectedReply )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            Assert.IsTrue( device.IsDeviceOpen, "session should be open" );

            // ensure service request handling is disabled.
            device.Session.DisableServiceRequestEventHandler();
            // must refresh the service request here to update the status register flags.
            _ = device.Session.ReadStatusRegister();
            Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should be disabled" );
            device.PollMessageAvailableBitmask = expectedPollMessageAvailableBitmask;
            try
            {
                device.PollTimespan = TimeSpan.FromMilliseconds( 100d );
                Assert.IsFalse( device.PollAutoRead, $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be disabled" );
                device.PollAutoRead = true;
                Assert.IsTrue( device.PollAutoRead, $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be enabled" );
                Assert.IsFalse( device.PollEnabled, $"{nameof( VisaSessionBase.PollEnabled ).SplitWords()} should be disabled" );
                device.PollEnabled = true;
                Assert.IsTrue( device.PollEnabled, $"{nameof( VisaSessionBase.PollEnabled ).SplitWords()} should be enabled" );
                Assert.IsTrue( string.IsNullOrWhiteSpace( device.PollReading ), $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be empty" );

                // read operation completion
                string expectedSPollMessage = expectedReply;
                string pollMessageName = $"Custom command: {queryCommand}";
                _ = device.Session.WriteLine( queryCommand );

                // wait for the message
                device.StartAwaitingPollReadingTask( TimeSpan.FromTicks( 100L * device.PollTimespan.Ticks ) ).Wait();
                Assert.IsFalse( string.IsNullOrWhiteSpace( device.PollReading ), $"Poll reading '{device.PollReading}' should not be empty" );
                Assert.AreEqual( expectedSPollMessage, device.PollReading, $"Expected {pollMessageName} {queryCommand} message" );
                device.PollAutoRead = false;
                device.PollEnabled = false;
                _ = device.Session.ReadStatusRegister();
                // wait for the poll timer to disabled
                device.StartAwaitingPollTimerDisabledTask( TimeSpan.FromTicks( 2L * device.PollTimespan.Ticks ) ).Wait();
                Assert.IsFalse( device.PollAutoRead, $"{nameof( VisaSessionBase.PollAutoRead ).SplitWords()} should be disabled" );
                Assert.IsFalse( device.PollEnabled, $"{nameof( VisaSessionBase.PollEnabled ).SplitWords()} should be disabled" );
                Assert.IsFalse( device.PollTimerEnabled, $"{nameof( VisaSessionBase.PollTimerEnabled ).SplitWords()} should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                device.PollAutoRead = false;
                device.PollEnabled = false;
                _ = device.Session.ReadStatusRegister();
            }
        }


        #endregion

        #region " SERVICE REQUEST HANDLING "

        /// <summary> Name of the service request message. </summary>
        private static string _ServiceRequestMessageName = "Operation Completed";

        /// <summary> Message describing the expected service request. </summary>
        private static string _ExpectedServiceRequestMessage = string.Empty;
        private static readonly Core.Concurrent.ConcurrentToken<string> ServiceRequestReadingToken = new();

        /// <summary> Handles the service request. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static void HandleServiceRequest( object sender, EventArgs e )
        {
            try
            {
                Pith.SessionBase mbs = ( Pith.SessionBase ) sender;
                var sb = mbs.ReadStatusByte();
                if ( (sb & Pith.ServiceRequests.MessageAvailable) != 0 )
                {
                    ServiceRequestReadingToken.Value = mbs.ReadLineTrimEnd();
                }
                else
                {
                    Assert.Fail( "MAV in status register is not set, which means that message is not available. Make sure the command to enable SRQ is correct, and the instrument is 488.2 compatible." );
                }
            }
            catch ( Exception ex )
            {
                Assert.Fail( ex.ToString() );
            }
        }

        /// <summary> Assert service request should be handled. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        private static void AssertServiceRequestShouldBeHandled( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsFalse( session.ServiceRequestEventEnabled, "service request event should not be enabled" );
            string expectedServiceRequestEnableCommand = string.Format( $"*SRE {( int ) session.ServiceRequestEnabledBitmask}" );
            Assert.AreEqual( expectedServiceRequestEnableCommand, session.ServiceRequestEnableCommand, "should equal the service request enabled command" );
            try
            {
                session.EnableServiceRequestEventHandler();
                Assert.IsTrue( session.ServiceRequestEventEnabled, $"Service request event not enabled" );
                session.ServiceRequested += HandleServiceRequest;
                // read operation completion
                _ExpectedServiceRequestMessage = "1";
                _ServiceRequestMessageName = "Operation Completed";
                string queryCommand = "*OPC?";
                _ = session.WriteLine( queryCommand );
                // wait for the message
                _ = Core.ApplianceBase.DoEventsWaitUntil( TimeSpan.FromMilliseconds( 400d ), () => !string.IsNullOrWhiteSpace( ServiceRequestReadingToken.Value ) );
                Assert.AreEqual( _ExpectedServiceRequestMessage, ServiceRequestReadingToken.Value, $"Expected {_ServiceRequestMessageName} {queryCommand} message" );
                session.ServiceRequested -= HandleServiceRequest;
                session.DisableServiceRequestEventHandler();
                // must refresh the service request here to update the status register flags.
                _ = session.ReadStatusRegister();
                Assert.IsFalse( session.ServiceRequestEventEnabled, $"Service request event should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                session.DisableServiceRequestEventHandler();
                _ = session.ReadStatusRegister();
            }
        }

        /// <summary>   Assert session service request should be handled. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        public static void AssertServiceRequestShouldBeHandledBySession( VisaSessionBase device )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            Assert.IsTrue( device.IsDeviceOpen, "session should be open" );
            device.Session.ApplyServiceRequestEnableBitmask( device.Session.DefaultServiceRequestEnableBitmask );
            AssertServiceRequestShouldBeHandled( device.Session );
        }

        /// <summary> Assert device should handle service request. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        public static void AssertServiceRequestShouldBeHandledByDevice( VisaSessionBase device )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            Assert.IsTrue( device.IsDeviceOpen, "session should be open" );
            device.Session.ApplyServiceRequestEnableBitmask( device.Session.DefaultServiceRequestEnableBitmask );
            Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should not be enabled" );
            string expectedServiceRequestEnableCommand = string.Format( $"*SRE {( int ) device.Session.ServiceRequestEnabledBitmask}" );
            Assert.AreEqual( expectedServiceRequestEnableCommand, device.Session.ServiceRequestEnableCommand, $"{nameof( Pith.SessionBase.ServiceRequestEnableCommand ).SplitWords()} should equal the command" );
            int expectedPollMessageAvailbleBitmask = ( int ) Pith.ServiceRequests.MessageAvailable;
            Assert.AreEqual( expectedPollMessageAvailbleBitmask, device.PollMessageAvailableBitmask, "Message available " );
            try
            {
                device.Session.EnableServiceRequestEventHandler();
                Assert.IsTrue( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should be enabled" );
                Assert.IsFalse( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead ).SplitWords()} should be off" );
                device.ServiceRequestAutoRead = true;
                Assert.IsTrue( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead ).SplitWords()} should be on" );
                Assert.IsFalse( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned ).SplitWords()} should not be assigned" );
                device.AddServiceRequestEventHandler();
                Assert.IsTrue( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned ).SplitWords()} should be assigned" );

                // read operation completion
                _ExpectedServiceRequestMessage = "1";
                _ServiceRequestMessageName = "Operation Completed";
                string queryCommand = "*OPC?";
                _ = device.Session.WriteLine( queryCommand );

                // wait for the message
                device.StartAwaitingServiceRequestReadingTask( TimeSpan.FromMilliseconds( 10000d ) ).Wait();
                Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ), $"Service request reading '{device.ServiceRequestReading}' should not be empty" );
                Assert.AreEqual( _ExpectedServiceRequestMessage, device.ServiceRequestReading, $"Expected {_ServiceRequestMessageName} {queryCommand} message" );
                device.Session.DisableServiceRequestEventHandler();
                // must refresh the service request here to update the status register flags.
                _ = device.Session.ReadStatusRegister();
                Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( device.ServiceRequestHandlerAssigned )
                    device.RemoveServiceRequestEventHandler();
                device.Session.DisableServiceRequestEventHandler();
                _ = device.Session.ReadStatusRegister();
            }
        }

        /// <summary>   Assert service request should be handled. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="device">                               The device. </param>
        /// <param name="queryCommand">                         The query command. </param>
        /// <param name="expectedPollMessageAvailableBitmask">  The expected poll message available
        ///                                                     bitmask. </param>
        /// <param name="expectedReply">                        The expected reply. </param>
        public static void AssertServiceRequestShouldBeHandled( VisaSessionBase device, string queryCommand,
                                                               int expectedPollMessageAvailableBitmask, string expectedReply )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            Assert.IsTrue( device.IsDeviceOpen, "session should be open" );
            device.PollMessageAvailableBitmask = expectedPollMessageAvailableBitmask;
            try
            {
                device.Session.EnableServiceRequestEventHandler();
                Assert.IsTrue( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should be enabled" );
                Assert.IsFalse( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead ).SplitWords()} should be off" );
                device.ServiceRequestAutoRead = true;
                Assert.IsTrue( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead ).SplitWords()} should be on" );
                Assert.IsFalse( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned ).SplitWords()} should not be assigned" );
                device.AddServiceRequestEventHandler();
                Assert.IsTrue( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned ).SplitWords()} should be assigned" );

                // read operation completion
                _ExpectedServiceRequestMessage = expectedReply;
                string pollMessageName = $"Custom command: {queryCommand}";
                _ = device.Session.WriteLine( queryCommand );

                // wait for the message
                device.StartAwaitingServiceRequestReadingTask( TimeSpan.FromMilliseconds( 10000d ) ).Wait();
                Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ), $"Service request reading '{device.ServiceRequestReading}' should not be empty" );
                Assert.AreEqual( _ExpectedServiceRequestMessage, device.ServiceRequestReading, $"Expected {_ServiceRequestMessageName} {queryCommand} message" );
                device.Session.DisableServiceRequestEventHandler();
                // must refresh the service request here to update the status register flags.
                _ = device.Session.ReadStatusRegister();
                Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled ).SplitWords()} should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( device.ServiceRequestHandlerAssigned )
                    device.RemoveServiceRequestEventHandler();
                device.Session.DisableServiceRequestEventHandler();
                _ = device.Session.ReadStatusRegister();
            }
        }


        #endregion

    }
}
