using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{
    public sealed partial class DeviceManager
    {

        #region " DEVICE TRACE MESSAGE TEST "

        /// <summary> Assert device talks. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void AssertDeviceTraceMessageShouldBeQueued( TestSite testInfo, VisaSessionBase device )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            device.AddListener( testInfo.TraceMessagesQueueListener );
            string payload = "Device message";
            int traceEventId = 1;
            _ = device.Talker.Publish( TraceEventType.Warning, traceEventId, payload );

            // with the new talker, the device identifies the following libraries: 
            // 0x0100 core agnostic; 0x01006 vi device and 0x01026 Keithley Meter
            // so these test looks for the first warning
            int fetchNumber = 0;
            Core.TraceMessage traceMessage = null;
            while ( testInfo.TraceMessagesQueueListener.Any )
            {
                traceMessage = testInfo.TraceMessagesQueueListener.TryDequeue();
                fetchNumber += 1;
                if ( traceMessage.EventType <= TraceEventType.Warning )
                {
                    // we expect a single such message
                    break;
                }
            }

            if ( traceMessage is null )
                Assert.Fail( $"{payload} failed to trace fetch number {fetchNumber}" );
            Assert.AreEqual( traceEventId, traceMessage.Id, $"{payload} trace event id mismatch fetch #{fetchNumber} message {traceMessage.Details}" );
            Assert.AreEqual( 0, testInfo.TraceMessagesQueueListener.Count, $"{payload} expected no more messages after fetch #{fetchNumber} message {traceMessage.Details}" );
            traceEventId = 1;
            payload = "Status subsystem message";
            _ = device.Talker.Publish( TraceEventType.Warning, traceEventId, payload );
            traceMessage = testInfo.TraceMessagesQueueListener.TryDequeue();
            if ( traceMessage is null )
                Assert.Fail( $"{payload} failed to trace" );
            Assert.AreEqual( traceEventId, traceMessage.Id, $"{payload} trace event id mismatch" );
        }

        #endregion

        #region " DEVICE SHOULD OPEN and CLOSE "

        /// <summary>   Assert session should open without device errors. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="device">           The device. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertDeviceShouldOpenWithoutDeviceErrors( TestSite testInfo, VisaSessionBase device, ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            var (success, details) = device.TryOpenSession( resourceSettings.ResourceName, resourceSettings.ResourceTitle );
            Assert.IsTrue( success, $"Failed to open session: {details}" );
            testInfo.AssertMessageQueue();
            if ( device.IsSessionOpen )
                _ = device.StatusSubsystemBase.TryQueryExistingDeviceErrors();
            testInfo.AssertMessageQueue();
            Console.Out.WriteLine($"{resourceSettings.ResourceName} is open");
        }

        /// <summary> Asserts closing a session. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void AssertDeviceShopuldCloseWithoutErrors( TestSite testInfo, VisaSessionBase device )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            try
            {
                testInfo.AssertMessageQueue();
                if ( device.IsSessionOpen )
                    _ = device.StatusSubsystemBase.TryQueryExistingDeviceErrors();
                testInfo.AssertMessageQueue();
            }
            catch
            {
                throw;
            }
            finally
            {
                device.CloseSession();
            }

            Assert.IsFalse( device.IsDeviceOpen, $"{device.ResourceNameCaption} failed closing session" );
            testInfo.AssertMessageQueue();
        }

        #endregion

    }
}
