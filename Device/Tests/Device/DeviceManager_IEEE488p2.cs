using System;

using isr.Core.SplitExtensions;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{
    public sealed partial class DeviceManager
    {

        #region " SESSION OPEN, CLOSE "

        /// <summary>   Queries if a given assert visa session should open. </summary>
        /// <remarks>
        /// Like <see cref="AssertDeviceShouldOpenWithoutDeviceErrors(TestSite, VisaSessionBase, ResourceSettingsBase)"/>, this
        /// function uses the Visa Session base to open the session but does not create a status
        /// subsystem.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="session">          The visa session. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaSessionShouldOpen( TestSite testInfo, VisaSession session, ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            session.SubsystemSupportMode = SubsystemSupportMode.Native;
            session.OpenSession( resourceSettings.ResourceName, resourceSettings.ResourceTitle );
            testInfo.AssertMessageQueue();
        }

        /// <summary>   Assert visa session should close. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The visa session. </param>
        public static void AssertVisaSessionShouldClose( TestSite testInfo, VisaSession session )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            try
            {
                testInfo.AssertMessageQueue();
                session.CloseSession();
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            Assert.IsFalse( session.IsDeviceOpen, $"{session.ResourceNameCaption} failed closing session" );
            testInfo.AssertMessageQueue();
        }

        /// <summary>   Queries if a given assert visa session base should open. </summary>
        /// <remarks>
        /// Unlike <see cref="AssertDeviceShouldOpenWithoutDeviceErrors(TestSite, VisaSessionBase, ResourceSettingsBase)"/>, this
        /// function uses the Session base to open the session thus not assume the existence of the
        /// status subsystem.
        /// </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="session">          The visa session. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaSessionBaseShouldOpen( TestSite testInfo, VisaSession session, ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            session.SubsystemSupportMode = SubsystemSupportMode.Native;
            session.Session.OpenSession( resourceSettings.ResourceName, resourceSettings.ResourceTitle );
            testInfo.AssertMessageQueue();
        }

        /// <summary> Closes a session. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The visa session. </param>
        public static void AssertVisaSessionBaseShouldClose( TestSite testInfo, VisaSession session )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            try
            {
                testInfo.AssertMessageQueue();
                session.Session.CloseSession();
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            Assert.IsFalse( session.IsDeviceOpen, $"{session.ResourceNameCaption} failed closing session" );
            testInfo.AssertMessageQueue();
        }

        /// <summary> Opens the base visa session. </summary>
        /// <remarks>
        /// Unlike <see cref="AssertDeviceShouldOpenWithoutDeviceErrors(TestSite, VisaSessionBase, ResourceSettingsBase)"/>, this
        /// function uses the Session base to open the session thus not assume the existence of the
        /// status subsystem.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="device">           The device. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssetVisaSessionBaseShouldOpen( TestSite testInfo, VisaSessionBase device, ResourceSettingsBase resourceSettings )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            device.SubsystemSupportMode = SubsystemSupportMode.Native;
            device.Session.OpenSession( resourceSettings.ResourceName, resourceSettings.ResourceTitle );
            testInfo.AssertMessageQueue();
        }

        /// <summary>   Assert visa session base should close. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void AssertVisaSessionBaseShouldClose( TestSite testInfo, VisaSessionBase device )
        {
            if ( testInfo is null )
                throw new ArgumentNullException( nameof( testInfo ) );
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            try
            {
                testInfo.AssertMessageQueue();
                device.Session.CloseSession();
            }
            catch
            {
                throw;
            }
            finally
            {
            }

            Assert.IsFalse( device.IsDeviceOpen, $"{device.ResourceNameCaption} failed closing session" );
            testInfo.AssertMessageQueue();
        }

        #endregion

        #region " SERVICE REQUEST EVENTS "

        /// <summary> Assert status bitmask should be waited for. </summary>
        /// <param name="testInfo">        Information describing the test. </param>
        /// <param name="session">         The session. </param>
        /// <param name="expectedBitmask"> The expected bitmask. </param>
        /// <param name="timeout">         The timeout. </param>
        public static void AssertBitmaskShouldWaitFor( TestSite testInfo, Pith.SessionBase session, Pith.ServiceRequests expectedBitmask, TimeSpan timeout )
        {
            (_, Pith.ServiceRequests status) = (false, expectedBitmask);
            var (TimedOut, StatusByte, _) = session.AwaitStatusBitmask( expectedBitmask, timeout );
            Assert.IsFalse( TimedOut, $"Getting 0x{( int ) StatusByte:X2} awaiting 0x{( int ) expectedBitmask:X2} should not time out" );
            Assert.IsTrue( (StatusByte & status) != 0, $"Status byte 0x{( int ) StatusByte:X2} 0x{( int ) status:X2} bitmask should be set" );
            testInfo.TraceMessage( $"{session.OpenResourceTitle} status byte 0x{( int ) StatusByte:X2} 0x{( int ) status:X2} bitmask was set" );
        }

        /// <summary>   Assert bitmasks should be set. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="session">          The session. </param>
        /// <param name="expectedBitmask">  The expected bitmask. </param>
        /// <param name="secondaryBitmask"> The secondary bitmask. </param>
        /// <param name="timeout">          The timeout. </param>
        public static void AssertBitmasksShouldBeSet( TestSite testInfo, Pith.SessionBase session, Pith.ServiceRequests expectedBitmask, Pith.ServiceRequests secondaryBitmask, TimeSpan timeout )
        {
            (_, Pith.ServiceRequests status) = (false, expectedBitmask);
            var (TimedOut, StatusByte, _) = session.AwaitStatusBitmask( expectedBitmask, timeout );
            Assert.IsFalse( TimedOut, $"Awaiting 0x{( int ) expectedBitmask:X2} should not time out" );
            Assert.IsTrue( (StatusByte & status) != 0, $"Status byte 0x{( int ) StatusByte:X2} 0x{( int ) status:X2} bitmask should be set" );
            Assert.IsTrue( (StatusByte & secondaryBitmask) != 0, $"Status byte 0x{( int ) StatusByte:X2} 0x{( int ) secondaryBitmask:X2} bitmask should also be set" );
            testInfo.TraceMessage( $"{session.OpenResourceTitle} status byte 0x{( int ) StatusByte:X2} 0x{( int ) status:X2} bitmask was set" );
        }

        /// <summary>   Assert bitmask should be set. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="session">          The session. </param>
        /// <param name="expectedBitmask">  The expected bitmask. </param>
        /// <param name="timeout">          The timeout. </param>
        public static void AssertBitmaskShouldBeSet( TestSite testInfo, Pith.SessionBase session, byte expectedBitmask, TimeSpan timeout )
        {
            (_, byte status) = (false, expectedBitmask);
            var (TimedOut, StatusByte, _) = session.AwaitStatusBitmask( expectedBitmask, timeout );
            Assert.IsFalse( TimedOut, $"Awaiting 0x{expectedBitmask:X2} should not time out" );
            Assert.IsTrue( (StatusByte & status) != 0, $"Status byte 0x{( int ) StatusByte:X2} 0x{( int ) status:X2} bitmask should be set" );
            testInfo.TraceMessage( $"{session.OpenResourceTitle} status byte 0x{( int ) StatusByte:X2} 0x{( int ) status:X2} bitmask was set" );
        }

        /// <summary> Assert should await for status bitmasks set and clear. </summary>
        /// <param name="testInfo">     Information describing the test. </param>
        /// <param name="session">      The session. </param>
        /// <param name="setBitmask">   The set bitmask. </param>
        /// <param name="clearBitmask"> The clear bitmask. </param>
        /// <param name="timeout">      The timeout. </param>
        public static void AssertBitmaskSetClearShouldWaitFor( TestSite testInfo, Pith.SessionBase session, Pith.ServiceRequests setBitmask, Pith.ServiceRequests clearBitmask, TimeSpan timeout )
        {
            (bool TimedOut, Pith.ServiceRequests Status) expectedOutcome = (false, setBitmask);
            var (TimedOut, Status, _) = session.AwaitStatusBitmask( setBitmask, timeout );
            Assert.IsFalse( TimedOut, $"Awaiting 0x{( int ) setBitmask:X2} should not time out" );
            Assert.IsTrue( (Status & expectedOutcome.Status) != 0, $"Status byte 0x{( int ) Status:X2} 0x{( int ) expectedOutcome.Status:X2} bitmask should be set" );
            Assert.IsTrue( (Status & clearBitmask) == 0, $"Status byte 0x{( int ) Status:X2} 0x{( int ) clearBitmask:X2} bitmask should also be clear" );
            testInfo.TraceMessage( $"{session.OpenResourceTitle} status byte 0x{( int ) Status:X2} 0x{( int ) expectedOutcome.Status:X2} bitmask was set" );
        }

        /// <summary> Assert shoudld wait for status bitmask. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertStatusBitmaskShouldWaitFor( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            if ( session.ServiceRequestEventEnabled )
            {
                testInfo.TraceMessage( $"{session.OpenResourceTitle} Disabling service request" );
                session.DisableServiceRequestEventHandler();
            }
            else
            {
                testInfo.TraceMessage( $"{session.OpenResourceTitle} service request handler was disabled" );
            }

            Assert.IsFalse( session.ServiceRequestEventEnabled, $"Service request event should be disabled" );
            string queryCommand = "*CLS; *WAI";
            _ = session.WriteLine( queryCommand );
            Core.ApplianceBase.DoEventsWait( session.StatusReadDelay );
            var timeout = TimeSpan.FromMilliseconds( 200d );
            var expectedBitmask = Pith.ServiceRequests.StandardEvent;
            var actualOutcome = session.AwaitStatusBitmask( expectedBitmask, timeout );

            // session.MakeEmulatedReplyIfEmpty(expectedBitmask)
            var task = Pith.SessionBase.StartAwaitingBitmaskTask( expectedBitmask, timeout, () => session.ReadStatusByte() );
            task.Wait();
            // actualOutcome = (Not .Wait(timeout), CType(.Result, Pith.ServiceRequests))
            actualOutcome = task.Result;
            actualOutcome = session.AwaitStatusBitmask( expectedBitmask, timeout );
            Assert.IsTrue( actualOutcome.TimedOut, $"waiting for event summary event on *CLS is expected to time out" );
        }

        /// <summary> Assert wait complete should enable. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertWaitCompleteShouldEnable( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            string propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Pith.StandardEvents actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualStandardEventEnableBitmask:X2}" );

            // program the standard event request register.

            var expectedStandardEventEnableBitmask = session.StandardEventWaitCompleteEnabledBitmask;
            propertyName = nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ).SplitWords();
            Assert.IsTrue( ( int ) expectedStandardEventEnableBitmask > 0, $"{propertyName} 0x{( int ) expectedStandardEventEnableBitmask:X2} should be positive" );
            session.EnableWaitComplete();
            actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            propertyName = nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after enabling" );
            session.ClearExecutionState();
            _ = session.QueryOperationCompleted();
            actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            propertyName = nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after clearing execution state." );
            testInfo.TraceMessage( $"Enabled {propertyName} is 0x{( int ) actualStandardEventEnableBitmask:X2}" );
        }

        /// <summary> Assert should enable wait complete service request. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertWaitCompleteServiceRequestShouldEnable( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            string propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            Pith.ServiceRequests actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualServiceRequestEnableBitmask:X2}" );
            propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Pith.StandardEvents actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualStandardEventEnableBitmask:X2}" );

            // program the service request register.
            var expectedServiceRequestEnableBitmask = session.ServiceRequestWaitCompleteEnabledBitmask;
            var expectedStandardEventEnableBitmask = session.StandardEventWaitCompleteEnabledBitmask;
            session.EnableServiceRequestWaitComplete();
            propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedServiceRequestEnableBitmask, session.ServiceRequestWaitCompleteEnabledBitmask, $"{propertyName} should match after enabling" );
            propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, session.StandardEventWaitCompleteEnabledBitmask, $"{propertyName} should match after enabling" );
            session.ClearExecutionState();
            _ = session.QueryOperationCompleted();
            actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            propertyName = nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after clearing execution state." );
            testInfo.TraceMessage( $"Enabled {propertyName} is 0x{( int ) actualStandardEventEnableBitmask:X2}" );
            propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            Assert.AreEqual( expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after clearing execution state." );
            testInfo.TraceMessage( $"Enabled {propertyName} is 0x{( int ) actualServiceRequestEnableBitmask:X2}" );
        }

        /// <summary> Assert standard service request should enable. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertStandardServiceRequestShouldEnable( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            string propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            Pith.ServiceRequests actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualServiceRequestEnableBitmask:X2}" );
            propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Pith.StandardEvents actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualStandardEventEnableBitmask:X2}" );

            // program the service request register.
            var expectedServiceRequestEnableBitmask = session.DefaultServiceRequestEnableBitmask;
            var expectedStandardEventEnableBitmask = session.DefaultStandardEventEnableBitmask;
            session.ApplyStandardServiceRequestEnableBitmasks( expectedStandardEventEnableBitmask, expectedServiceRequestEnableBitmask );
            actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after enabling" );
            propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after enabling" );
            session.ClearExecutionState();
            _ = session.QueryOperationCompleted();
            actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            propertyName = nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after clearing execution state." );
            propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            Assert.AreEqual( expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after clearing execution state." );
        }

        /// <summary> Assert standard service request should disable. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertStandardServiceRequestShouldDisable( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            string propertyName = nameof( Pith.SessionBase.ServiceRequestWaitCompleteEnabledBitmask ).SplitWords();
            Pith.ServiceRequests actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualServiceRequestEnableBitmask:X2}" );
            propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Pith.StandardEvents actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            testInfo.TraceMessage( $"Initial {propertyName} is 0x{( int ) actualStandardEventEnableBitmask:X2}" );

            // program the service request register.
            var expectedServiceRequestEnableBitmask = Pith.ServiceRequests.None;
            var expectedStandardEventEnableBitmask = Pith.StandardEvents.None;
            session.ApplyStandardServiceRequestEnableBitmasks( expectedStandardEventEnableBitmask, expectedServiceRequestEnableBitmask );
            actualServiceRequestEnableBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( session.QueryServiceRequestEnableBitmask() );
            actualStandardEventEnableBitmask = ( Pith.StandardEvents ) Conversions.ToInteger( session.QueryStandardEventEnableBitmask() );
            propertyName = nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ).SplitWords();
            Assert.AreEqual( expectedServiceRequestEnableBitmask, actualServiceRequestEnableBitmask, $"{propertyName} should match after enabling" );
            propertyName = nameof( Pith.SessionBase.StandardEventEnableBitmask ).SplitWords();
            Assert.AreEqual( expectedStandardEventEnableBitmask, actualStandardEventEnableBitmask, $"{propertyName} should match after enabling" );
        }

        /// <summary> Assert wait for message available. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertMessageAvailableShouldWaitFor( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );

            // set service request enable
            AssertStandardServiceRequestShouldEnable( testInfo, session );
            string queryCommand = "*OPC?";
            _ = session.WriteLine( queryCommand );
            try
            {
                AssertBitmasksShouldBeSet( testInfo, session, Pith.ServiceRequests.MessageAvailable, Pith.ServiceRequests.RequestingService, TimeSpan.FromMilliseconds( 200d ) );
            }
            catch
            {
                throw;
            }
            finally
            {
                string result = session.ReadLineTrimEnd();
                // must refresh the service request here to update the status register flags.
                _ = session.ReadStatusRegister();
                Assert.IsTrue( result.StartsWith( "0", StringComparison.OrdinalIgnoreCase ) || result.StartsWith( "1", StringComparison.OrdinalIgnoreCase ), $"{queryCommand} returns 0 or 1" );
            }


            // clear service request enable
            AssertStandardServiceRequestShouldDisable( testInfo, session );
            _ = session.WriteLine( queryCommand );
            try
            {
                AssertBitmaskShouldWaitFor( testInfo, session, Pith.ServiceRequests.MessageAvailable, TimeSpan.FromMilliseconds( 200d ) );
            }
            catch
            {
                throw;
            }
            finally
            {
                string result = session.ReadLineTrimEnd();
                // must refresh the service request here to update the status register flags.
                _ = session.ReadStatusRegister();
                Assert.IsTrue( result.StartsWith( "0", StringComparison.OrdinalIgnoreCase ) || result.StartsWith( "1", StringComparison.OrdinalIgnoreCase ), $"{queryCommand} returns 0 or 1" );
            }
        }

        /// <summary>   Assert query result should be detected. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="testInfo">         Information describing the test. </param>
        /// <param name="session">          The session. </param>
        /// <param name="queryCommand">     The query command. </param>
        /// <param name="expectedBitmask">  The expected bitmask. </param>
        /// <param name="expectedReply">    The expected reply. </param>
        public static void AssertQueryResultShouldBeDetected( TestSite testInfo, Pith.SessionBase session, string queryCommand, byte expectedBitmask, string expectedReply )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );

            // write query command
            _ = session.WriteLine( queryCommand );
            try
            {
                AssertBitmaskShouldBeSet( testInfo, session, expectedBitmask, TimeSpan.FromMilliseconds( 200d ) );
            }
            catch
            {
                throw;
            }
            finally
            {
                string actualReply = session.ReadLineTrimEnd();
                // must refresh the service request here to update the status register flags.
                _ = session.ReadStatusRegister();
                Assert.IsTrue( expectedReply.StartsWith( actualReply, StringComparison.OrdinalIgnoreCase ), $"{queryCommand} returned {actualReply} should return {expectedReply}" );
            }

        }

        /// <summary> Assert wait for operation completion. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo">      Information describing the test. </param>
        /// <param name="session">       The session. </param>
        /// <param name="actionCommand"> The action command, which, by added '?' or value can be used to
        /// read current value and set the same value thus not changing
        /// things. </param>
        public static void AssertOperationCompletionShouldWaitFor( TestSite testInfo, Pith.SessionBase session, string actionCommand )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            AssertWaitCompleteShouldEnable( testInfo, session );
            _ = session.WriteLine( actionCommand );
            AssertBitmaskShouldWaitFor( testInfo, session, Pith.ServiceRequests.StandardEvent, TimeSpan.FromMilliseconds( 200d ) );
        }

        /// <summary> Assert wait for service request operation completion. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo">      Information describing the test. </param>
        /// <param name="session">       The session. </param>
        /// <param name="actionCommand"> The action command, which, by added '?' or value can be used to
        /// read current value and set the same value thus not changing
        /// things. </param>
        public static void AssertServiceRequestOperationCompletionWaitFor( TestSite testInfo, Pith.SessionBase session, string actionCommand )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            AssertWaitCompleteServiceRequestShouldEnable( testInfo, session );
            _ = session.WriteLine( actionCommand );
            AssertBitmasksShouldBeSet( testInfo, session, Pith.ServiceRequests.StandardEvent, Pith.ServiceRequests.RequestingService, TimeSpan.FromMilliseconds( 200d ) );
        }

        /// <summary>   Assert service request handling should toggle. </summary>
        /// <remarks> Leaves the system at it present start. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="session">  The session. </param>
        public static void AssertServiceRequestHandlingShouldToggle( TestSite testInfo, Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            Assert.IsTrue( session.IsDeviceOpen, $"Attempted a call with closed session" );
            for ( int i = 1; i <= 2; i++ )
            {
                if ( session.ServiceRequestEventEnabled )
                {
                    testInfo.TraceMessage( "Disabling service request handling" );
                    session.DisableServiceRequestEventHandler();
                    Assert.IsFalse( session.ServiceRequestEventEnabled, $"Service request event not disabled" );
                }
                else
                {
                    testInfo.TraceMessage( "Enabling service request handling" );
                    session.EnableServiceRequestEventHandler();
                    Assert.IsTrue( session.ServiceRequestEventEnabled, $"Service request event not enabled" );
                }
            }
        }

        #endregion

    }
}
