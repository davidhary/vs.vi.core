using System;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{
    public sealed partial class DeviceManager
    {

        #region " VISA RESOURCE CHECKS "

        /// <summary> Assert visa resource manager should include a resource. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaResourceManagerShouldIncludeResource( ResourceSettingsBase resourceSettings )
        {
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            if ( !resourceSettings.ResourcePinged )
                Assert.Inconclusive( $"{resourceSettings.ResourceTitle} not found" );
            string resourcesFilter = SessionFactory.Get.Factory.ResourcesProvider().ResourceFinder.BuildMinimalResourcesFilter();
            string[] resources;
            using ( var rm = SessionFactory.Get.Factory.ResourcesProvider() )
            {
                resources = rm.FindResources( resourcesFilter ).ToArray();
            }

            Assert.IsTrue( resources.Any(), $"VISA Resources {(resources.Any() ? string.Empty : "not")} found among {resourcesFilter}" );
            Assert.IsTrue( resources.Contains( resourceSettings.ResourceName ), $"Resource {resourceSettings.ResourceName} not found among {resourcesFilter}" );
        }

        /// <summary> Assert visa session base should find the resource. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device">           The device. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaSessionBaseShouldFindResource( VisaSessionBase device, ResourceSettingsBase resourceSettings )
        {
            if ( device is null )
                throw new ArgumentNullException( nameof( device ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            Assert.IsTrue( VisaSessionBase.Find( resourceSettings.ResourceName, device.ResourcesFilter ), $"VISA Resource {resourceSettings.ResourceName} not found among {device.ResourcesFilter}" );
        }

        /// <summary> Assert visa session should find the resource. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">          The device. </param>
        /// <param name="resourceSettings"> The resource settings. </param>
        public static void AssertVisaSessionShouldFindResource( VisaSession session, ResourceSettingsBase resourceSettings )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( resourceSettings is null )
                throw new ArgumentNullException( nameof( resourceSettings ) );
            Assert.IsTrue( VisaSessionBase.Find( resourceSettings.ResourceName, session.ResourcesFilter ), $"VISA Resource {resourceSettings.ResourceName} not found among {session.ResourcesFilter}" );
        }

        #endregion

    }
}
