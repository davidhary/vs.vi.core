using System;

using isr.Core;

namespace isr.VI.DeviceTests
{

    /// <summary> Resource test settings base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public abstract class ResourceSettingsBase : ApplicationSettingsBase
    {

        #region " CONSTRUCTORS "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        protected ResourceSettingsBase() : base()
        {
        }

        #endregion

        #region " TEST CONFIGURATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool Exists
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool Verbose
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool Enabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool All
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " DEVICE RESOURCE INFORMATION "

        /// <summary> Gets or sets the Model of the resource. </summary>
        /// <value> The Model of the resource. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "2002" )]
        public virtual string ResourceModel
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> The resource pinged. </summary>
        private bool? _ResourcePinged;

        /// <summary> Gets the resource pinged. </summary>
        /// <value> The resource pinged. </value>
        public virtual bool ResourcePinged
        {
            get {
                if ( !this._ResourcePinged.HasValue )
                {
                    this._ResourcePinged = !string.IsNullOrWhiteSpace( this.ResourceName );
                }

                return this._ResourcePinged.Value;
            }
        }

        /// <summary> Gets or sets the resource names delimiter. </summary>
        /// <value> The resource names delimiter. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "|" )]
        public virtual string ResourceNamesDelimiter
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets the zero-based index of the host information. </summary>
        /// <value> The host information index. </value>
        public int HostInfoIndex { get; private set; }

        /// <summary> Name of the resource. </summary>
        private string _ResourceName;

        /// <summary> Gets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        public string ResourceName
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._ResourceName ) )
                {
                    this._ResourceName = string.Empty;
                    this.HostInfoIndex = -1;
                    foreach ( string value in this.ResourceNames.Split( this.ResourceNamesDelimiter.ToCharArray() ) )
                    {
                        this.HostInfoIndex += 1;
                        if ( !Pith.ResourceNamesManager.IsTcpipResource( value ) || Pith.ResourceNamesManager.PingTcpipResource( value ) )
                        {
                            this._ResourceName = value;
                            break;
                        }
                    }
                }

                return this._ResourceName;
            }
        }

        /// <summary> Gets or sets the names of the candidate resources. </summary>
        /// <value> The names of the candidate resources. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( @"TCPIP0::192.168.0.254::gpib0,7::INSTR|TCPIP0::192.168.0.254::gpib0,7::INSTR|
TCPIP0::10.1.1.25::gpib0,7::INSTR|TCPIP0::10.1.1.24::gpib0,7::INSTR" )]
        public virtual string ResourceNames
        {
            get => this.AppSettingGetter( string.Empty ).Replace( " ", string.Empty ).Replace( Environment.NewLine, string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Title of the resource. </summary>
        /// <value> The Title of the resource. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "DMM2002" )]
        public virtual string ResourceTitle
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the language. </summary>
        /// <value> The language. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "SCPI" )]
        public virtual string Language
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the firmware revision. </summary>
        /// <value> The firmware revision. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1.6.4c" )]
        public virtual string FirmwareRevision
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        #endregion

    }
}
