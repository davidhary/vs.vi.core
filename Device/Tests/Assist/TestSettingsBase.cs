
namespace isr.VI.DeviceTests
{

    /// <summary> Test settings base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public abstract class TestSettingsBase : Core.ApplicationSettingsBase
    {

        #region " CONSTRUCTORS "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        protected TestSettingsBase() : base()
        {
        }

        #endregion

        #region " TEST CONFIGURATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool Exists
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool Verbose
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool Enabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool All
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        #endregion

    }
}
