
namespace isr.VI.DeviceTests
{

    /// <summary> The Subsystems Test Settings base class. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para>
    /// </remarks>
    public abstract class SubsystemsSettingsBase : Core.ApplicationSettingsBase
    {

        #region " CONSTRUCTORS "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        protected SubsystemsSettingsBase() : base()
        {
        }

        #endregion

        #region " TEST CONFIGURATION "

        /// <summary> Returns true if test settings exist. </summary>
        /// <value> <c>True</c> if testing settings exit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool Exists
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to output test messages at the verbose level. </summary>
        /// <value> The verbose messaging level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool Verbose
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Returns true to enable this device. </summary>
        /// <value> The device enable option. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool Enabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets all. </summary>
        /// <value> all. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool All
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " DEVICE SESSION INFORMATION "

        /// <summary> Gets or sets the keep alive query command. </summary>
        /// <value> The keep alive query command. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "*OPC?" )]
        public virtual string KeepAliveQueryCommand
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the keep-alive command. </summary>
        /// <value> The keep-alive command. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "*OPC" )]
        public virtual string KeepAliveCommand
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial read termination enabled. </summary>
        /// <value> The initial read termination enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool InitialReadTerminationEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial read termination character. </summary>
        /// <value> The initial read termination character. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public virtual int InitialReadTerminationCharacter
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the read termination enabled. </summary>
        /// <value> The read termination enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool ReadTerminationEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the read termination character. </summary>
        /// <value> The read termination character. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public virtual int ReadTerminationCharacter
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the erroneous command. </summary>
        /// <value> The erroneous command. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "*CLL" )]
        public virtual string ErroneousCommand
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the error available milliseconds delay. </summary>
        /// <value> The error available milliseconds delay. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public virtual int ErrorAvailableMillisecondsDelay
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets a message describing the expected compound error. </summary>
        /// <value> A message describing the expected compound error. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "-285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20" )]
        public virtual string ExpectedCompoundErrorMessage
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets a message describing the expected error. </summary>
        /// <value> A message describing the expected error. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "-285" )]
        public virtual string ExpectedErrorMessage
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the expected error number. </summary>
        /// <value> The expected error number. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "TSP Syntax error at line 1: unexpected symbol near `*'" )]
        public virtual int ExpectedErrorNumber
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the expected error level. </summary>
        /// <value> The expected error level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "20" )]
        public virtual int ExpectedErrorLevel
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets a message describing the parse compound error. </summary>
        /// <value> A message describing the parse compound error. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "-113,\"Undefined header;1;2018/05/26 14: 00:14.871\"" )]
        public virtual string ParseCompoundErrorMessage
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets a message describing the Parse error. </summary>
        /// <value> A message describing the Parse error. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "Undefined header" )]
        public virtual string ParseErrorMessage
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the parse error number. </summary>
        /// <value> The parse error number. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "-113" )]
        public virtual int ParseErrorNumber
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the parse error level. </summary>
        /// <value> The parse error level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1" )]
        public virtual int ParseErrorLevel
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " INITIAL VALUES: ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the initial closed channels. </summary>
        /// <value> The initial closed channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "(@)" )]
        public virtual string InitialClosedChannels
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Initial scan list settings. </summary>
        /// <value> The initial scan list settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "(@)" )]
        public virtual string InitialScanList
        {
            get => this.AppSettingGetter( string.Empty );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " INITIAL VALUES: SCANNER "

        /// <summary> Gets or sets the scan card installed. </summary>
        /// <value> The scan card installed. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool ScanCardInstalled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the number of scan cards. </summary>
        /// <value> The number of scan cards. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "" )]
        public virtual int? ScanCardCount
        {
            get => this.AppSettingGetter( new int?() );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " INITIAL VALUES: SENSE SUBSYSTEM "

        /// <summary>
        /// Gets or sets the front terminals control enabled. With manual front terminals switch, this
        /// would normally be set to <c><see langword="False"/></c>.
        /// </summary>
        /// <value> The front terminals control enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool FrontTerminalsControlEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial front terminals selected. </summary>
        /// <value> The initial front terminals selected. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool InitialFrontTerminalsSelected
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Initial power line cycles settings. </summary>
        /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1" )]
        public virtual double InitialPowerLineCycles
        {
            get => ( double ) this.AppSettingGetter( 0m );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Initial auto Delay Enabled settings. </summary>
        /// <value> The auto Delay settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool InitialAutoDelayEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Initial auto Range enabled settings. </summary>
        /// <value> The auto Range settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool InitialAutoRangeEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the Initial auto zero Enabled settings. </summary>
        /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool InitialAutoZeroEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial sense function. </summary>
        /// <value> The initial sense function. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "VoltageDC" )]
        public virtual SenseFunctionModes InitialSenseFunction
        {
            get => this.AppSettingEnum<SenseFunctionModes>();

            set => this.AppSettingEnumSetter( value );
        }

        /// <summary> Gets or sets the initial multimeter function. </summary>
        /// <value> The initial multimeter function. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "VoltageDC" )]
        public virtual MultimeterFunctionModes InitialMultimeterFunction
        {
            get => this.AppSettingEnum<MultimeterFunctionModes>();

            set => this.AppSettingEnumSetter( value );
        }

        /// <summary> Gets or sets the initial filter enabled. </summary>
        /// <value> The initial filter enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool InitialFilterEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial moving average filter enabled. </summary>
        /// <value> The initial moving average filter enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public virtual bool InitialMovingAverageFilterEnabled
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the number of initial filters. </summary>
        /// <value> The number of initial filters. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public virtual int InitialFilterCount
        {
            get => this.AppSettingGetter( 0 );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial filter window. </summary>
        /// <value> The initial filter window. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0.001" )]
        public virtual double InitialFilterWindow
        {
            get => ( double ) this.AppSettingGetter( 0m );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial remote sense selected. </summary>
        /// <value> The initial remote sense selected. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public virtual bool InitialRemoteSenseSelected
        {
            get => this.AppSettingGetter( false );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " INITIAL VALUES: SOURCE MEASURE UNIT "

        /// <summary> Gets or sets the initial source function mode. </summary>
        /// <value> The initial source function mode. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "VoltageDC" )]
        public virtual SourceFunctionModes InitialSourceFunction
        {
            get => this.AppSettingEnum<SourceFunctionModes>();

            set => this.AppSettingEnumSetter( value );
        }

        /// <summary> Gets or sets the initial source level. </summary>
        /// <value> The initial source level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public virtual double InitialSourceLevel
        {
            get => ( double ) this.AppSettingGetter( 0m );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the initial source limit. </summary>
        /// <value> The initial source limit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0.000105" )]
        public virtual double InitialSourceLimit
        {
            get => ( double ) this.AppSettingGetter( 0m );

            set => this.AppSettingSetter( value );
        }

        /// <summary> Gets or sets the maximum output power of the instrument. </summary>
        /// <value> The maximum output power . </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public virtual double MaximumOutputPower
        {
            get => ( double ) this.AppSettingGetter( 0m );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " INITIAL VALUES: STATUS SUBSYSTEM "

        /// <summary> Gets or sets the line frequency. </summary>
        /// <value> The line frequency. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "60" )]
        public virtual double LineFrequency
        {
            get => ( double ) this.AppSettingGetter( 0m );

            set => this.AppSettingSetter( value );
        }

        #endregion

        #region " INITIAL VALUES: TRIGGER SUBSYSTEM "

        /// <summary> Gets or sets the initial trigger source. </summary>
        /// <value> The initial trigger source. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "Immediate" )]
        public virtual TriggerSources InitialTriggerSource
        {
            get => this.AppSettingEnum<TriggerSources>();

            set => this.AppSettingEnumSetter( value );
        }

        #endregion

    }
}
