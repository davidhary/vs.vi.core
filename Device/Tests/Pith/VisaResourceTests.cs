using System;
using System.Linq;

using isr.VI.Pith;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{

    /// <summary> VISA Resource Settings tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    [TestClass()]
    public class VisaResourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public virtual TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " VISA VERSION TESTS "

        /// <summary> (Unit Test Method) validates the visa version test. </summary>
        /// <remarks>
        /// To test the 32bit version, change the default processor architecture from the Test, Test
        /// Settings menu.e.
        /// </remarks>
        [TestMethod()]
        public void ValidateVisaVersionTest()
        {
            var (success, details) = SessionFactory.ValidateFunctionalVisaVersions();
            Assert.IsTrue( success, details );
        }

        #endregion

        #region " VISA RESOURCE TEST "

        /// <summary> (Unit Test Method) tests locating visa resources. </summary>
        [TestMethod()]
        public void VisaResourcesTest()
        {
            // resourcesFilter = resourcesFilter.Replace("(", "[")
            // resourcesFilter = resourcesFilter.Replace(")", "]")
            // resourcesFilter = "[TCPIP|GPIB]*INSTR"
            // resourcesFilter = "(TCPIP|GPIB)*INSTR"
            string[] resources;
            using ( var rm = SessionFactory.Get.Factory.ResourcesProvider() )
            {
                string resourcesFilter = rm.ResourceFinder.BuildMinimalResourcesFilter();
                resources = rm.FindResources( resourcesFilter ).ToArray();
            }

            Assert.IsTrue( resources.Any(), $"VISA Resources {(resources.Any() ? string.Empty : "not")} found" );
        }

        #endregion

        #region " RESOURCE NAME INFO "

        /// <summary> (Unit Test Method) tests visa resource name information. </summary>
        [TestMethod()]
        public void VisaResourceNameInfoTest()
        {
            var resourceNameInfoCollection = new ResourceNameInfoCollection();
            int expectedCount;
            int actualCount;
            if ( !resourceNameInfoCollection.IsFileExists() )
            {
                // if a new file, add a known resource.
                string resourceName = "TCPIP0::192.168.0.254::gpib0,15::INSTR";
                resourceNameInfoCollection.Add( resourceName );
                resourceNameInfoCollection.WriteResources();
                Assert.IsTrue( resourceNameInfoCollection.IsFileExists() );
                expectedCount = resourceNameInfoCollection.Count;
                resourceNameInfoCollection.ReadResources();
                actualCount = resourceNameInfoCollection.Count;
                Assert.AreEqual( expectedCount, actualCount, $"{nameof( ResourceNameInfoCollection )}.count should match" );
                Assert.IsTrue( resourceNameInfoCollection.Contains( resourceName ), $"{nameof( ResourceNameInfoCollection )} should contain {resourceName}" );
            }

            resourceNameInfoCollection.ReadResources();
            Assert.IsTrue( resourceNameInfoCollection.Any(), $"{nameof( ResourceNameInfoCollection )} should have items" );
            resourceNameInfoCollection.WriteResources();
        }

        #endregion

    }
}
