using System;
using System.Linq;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{

    /// <summary> Session base tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    [TestClass()]
    public class SessionBaseTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public virtual TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " SESSION TERMINATION "

        /// <summary> (Unit Test Method) tests initial termination. </summary>
        [TestMethod()]
        public void InitialTerminationTest()
        {
            using var session = SessionFactory.Get.Factory.Session();
            Assert.AreEqual( ( byte ) Strings.AscW( Environment.NewLine.ToCharArray()[1] ),
                            ( byte ) Strings.AscW( session.TerminationCharacters().ElementAtOrDefault( 0 ) ),
                            "Initial termination character set to line feed" );
        }

        /// <summary> A test for New Termination. </summary>
        [TestMethod()]
        public void NewTerminationTest()
        {
            using var Session = SessionFactory.Get.Factory.Session();
            var values = Environment.NewLine.ToCharArray();
            Session.NewTermination( values );
            Assert.AreEqual( values.Length, Session.TerminationCharacters().Count() );
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
                Assert.AreEqual( ( byte ) Strings.AscW( values[i] ), ( byte ) Strings.AscW( Session.TerminationCharacters().ElementAtOrDefault( i ) ) );
        }

        #endregion

    }
}
