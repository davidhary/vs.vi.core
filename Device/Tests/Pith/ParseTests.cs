using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core.EnumExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.DeviceTests
{
    /// <summary> A session tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    [TestClass()]
    public class ParseTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public virtual TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " REGISTER VALUE FORMAT PROVIDER "

        /// <summary> (Unit Test Method) tests hexadecimal value format provider. </summary>
        [TestMethod()]
        public void HexValueFormatProviderTest()
        {
            var provider = Pith.HexFormatProvider.FormatProvider( 2 );
            var expectedValue = Pith.ServiceRequests.All & ~Pith.ServiceRequests.RequestingService;
            string expectedCaption = $"0x{( int ) expectedValue:X2}";
            string actualCaption = provider.Format( expectedValue );
            Assert.AreEqual( expectedCaption, actualCaption, $"Should be the hexadecimal caption of {expectedValue}" );

            var actualValue = provider.ParseEnumHexValue<Pith.ServiceRequests>( actualCaption ).GetValueOrDefault( Pith.ServiceRequests.None );
            Assert.AreEqual( expectedValue, actualValue, $"Should be the hexadecimal value parsed value {expectedValue}" );

            expectedCaption = "0x..";
            actualCaption = provider.NullValueCaption;
            Assert.AreEqual( expectedCaption, actualCaption, $"Should be the null caption" );

            provider = Pith.HexFormatProvider.FormatProvider( 4 );
            expectedValue = Pith.ServiceRequests.All & ~Pith.ServiceRequests.RequestingService;
            expectedCaption = $"0x{( int ) expectedValue:X4}";
            actualCaption = provider.Format( expectedValue );
            Assert.AreEqual( expectedCaption, actualCaption, $"Should be the hexadecimal caption of {expectedValue}" );

            actualValue = provider.ParseEnumHexValue<Pith.ServiceRequests>( actualCaption ).GetValueOrDefault( Pith.ServiceRequests.None );
            Assert.AreEqual( expectedValue, actualValue, $"Should be the hexadecimal value parsed value {expectedValue}" );

            expectedCaption = "0x....";
            actualCaption = provider.NullValueCaption;
            Assert.AreEqual( expectedCaption, actualCaption, $"Should be the null caption" );
        }


        #endregion

        #region " PARSE: ENUM; BOOLEAN "

        /// <summary> (Unit Test Method) nullable tests. </summary>
        /// <remarks> David, 2020-10-28. </remarks>
        [TestMethod()]
        public void NullableTests()
        {
            Assert.AreEqual( new bool?(), new bool?(), "? New Boolean? = New Boolean?" );
            Assert.AreNotEqual( new bool?( true ), new bool?(), "? New Boolean?(True) = New Boolean?" );
            // VB.Net: Assert.IsNull(new bool?() == new bool?(), "? Null = New Boolean? = New Boolean?");
            Assert.IsTrue( new bool?() == new bool?(), "? New Boolean? = New Boolean?" );
        }


        /// <summary> A test for ParseEnumValue. </summary>
        /// <param name="value">    The value. </param>
        /// <param name="expected"> The expected. </param>
        public static void ParseEnumValueTestHelper<T>( string value, T? expected ) where T : struct
        {
            T? actual;
            actual = Pith.SessionBase.ParseEnumValue<T>( value );
            Assert.AreEqual( expected, actual );
        }

        /// <summary> (Unit Test Method) tests parse enum value. </summary>
        [TestMethod()]
        public void ParseEnumValueTest()
        {
            ParseEnumValueTestHelper<TraceEventType>( "2", TraceEventType.Error );
        }

        /// <summary> (Unit Test Method) tests parse boolean. </summary>
        [TestMethod()]
        public void ParseBooleanTest()
        {
            string reading = "0";
            bool expectedResult = false;
            bool successParsing = Pith.SessionBase.TryParse( reading, out bool actualResult );
            Assert.AreEqual( expectedResult, actualResult, "Value set to {0}", ( object ) actualResult );
            Assert.AreEqual( true, successParsing, "Success set to {0}", ( object ) actualResult );
            reading = "1";
            expectedResult = true;
            successParsing = Pith.SessionBase.TryParse( reading, out actualResult );
            Assert.AreEqual( expectedResult, actualResult, "Value set to {0}", ( object ) actualResult );
            Assert.AreEqual( true, successParsing, "Success set to {0}", ( object ) actualResult );
        }

        /// <summary> (Unit Test Method) tests enum names. </summary>
        /// <remarks> David, 2020-10-28. </remarks>
        [TestMethod()]
        public void EnumNamesTest()
        {
            var traceEvent = TraceEventType.Verbose;
            string expectedValue = "Verbose";
            string actualValue = traceEvent.Names();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( TraceEventType )}.{nameof( TraceEventType.Verbose )} should match" );
            var armSource = ArmSources.Bus;
            expectedValue = "Bus";
            actualValue = armSource.Names();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( ArmSources )}.{nameof( ArmSources.Bus )} should match" );
            expectedValue = "Bus (BUS)";
            actualValue = armSource.Description();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( Core.EnumExtensions.EnumExtensionsMethods.Description )} of {nameof( ArmSources )}.{nameof( ArmSources.Bus )} should match" );
            expectedValue = "Bus (BUS)";
            actualValue = armSource.Descriptions();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( Core.EnumExtensions.EnumExtensionsMethods.Descriptions )} of {nameof( ArmSources )}.{nameof( ArmSources.Bus )} should match" );
            armSource = ArmSources.Bus | ArmSources.External;
            expectedValue = "Bus, External";
            actualValue = armSource.Names();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( Core.EnumExtensions.EnumExtensionsMethods.Names )} of {nameof( ArmSources )}.({nameof( ArmSources.Bus )} or {nameof( ArmSources.External )}) should match" );
            expectedValue = "Bus (BUS), External (EXT)";
            actualValue = armSource.Descriptions();
            Assert.AreEqual( expectedValue, actualValue, $"{nameof( Core.EnumExtensions.EnumExtensionsMethods.Descriptions )} of {nameof( ArmSources )}.({nameof( ArmSources.Bus )} or {nameof( ArmSources.External )}) should match" );
        }

        #endregion

        #region " ENUM READ WRITE "

        /// <summary> Tests unknown enum read write value. </summary>
        /// <param name="enumReadWrites">    The enum read writes. </param>
        /// <param name="expectedEnumValue"> The expected enum value. </param>
        private static void TestUnknownEnumReadWriteValue( Pith.EnumReadWriteCollection enumReadWrites, long expectedEnumValue )
        {
            Assert.IsFalse( enumReadWrites.Exists( expectedEnumValue ), $"{expectedEnumValue} should Not exist" );
            _ = Core.MSTest.Asserts.Instance.Throws<KeyNotFoundException>( () => enumReadWrites.SelectItem( expectedEnumValue ), $"{expectedEnumValue} enum value Is out of range" );
        }

        /// <summary> Tests unknown enum read write value. </summary>
        /// <param name="enumReadWrites">    The enum read writes. </param>
        /// <param name="expectedReadValue"> The expected read value. </param>
        private static void TestUnknownEnumReadWriteValue( Pith.EnumReadWriteCollection enumReadWrites, string expectedReadValue )
        {
            Assert.IsFalse( enumReadWrites.Exists( expectedReadValue ), $"{expectedReadValue} should Not exist" );
            _ = Core.MSTest.Asserts.Instance.Throws<KeyNotFoundException>( () => enumReadWrites.SelectItem( expectedReadValue ), $"{expectedReadValue} read value should Not exist" );
        }

        /// <summary> Tests enum read write value. </summary>
        /// <param name="enumReadWrites">    The enum read writes. </param>
        /// <param name="expectedEnumValue"> The expected enum value. </param>
        /// <param name="expectedReadValue"> The expected read value. </param>
        private static void TestEnumReadWriteValue( Pith.EnumReadWriteCollection enumReadWrites, long expectedEnumValue, string expectedReadValue )
        {
            Assert.IsTrue( enumReadWrites.Exists( expectedEnumValue ), $"{expectedEnumValue} exists" );
            Assert.IsTrue( enumReadWrites.Exists( expectedReadValue ), $"{expectedReadValue} exists" );
            var enumReadWrite = enumReadWrites.SelectItem( expectedEnumValue );
            Assert.AreEqual( expectedEnumValue, enumReadWrite.EnumValue, "expected item equals item selected from the collection" );
            Assert.AreEqual( expectedReadValue, enumReadWrite.ReadValue, "expected read value equals read value selected from the collection" );
        }

        /// <summary> (Unit Test Method) tests enum read write. </summary>
        [TestMethod()]
        public void EnumReadWriteTest()
        {
            var enumReadWrites = new Pith.EnumReadWriteCollection();
            MultimeterSubsystemBase.DefineFunctionModeReadWrites( enumReadWrites );
            long expectedEnumValue = 1000L;
            string expectedReadValue = "xx";
            TestUnknownEnumReadWriteValue( enumReadWrites, expectedEnumValue );
            TestUnknownEnumReadWriteValue( enumReadWrites, expectedReadValue );
            expectedEnumValue = ( long ) MultimeterFunctionModes.VoltageDC;
            expectedReadValue = "dmm.FUNC_DC_VOLTAGE";
            TestEnumReadWriteValue( enumReadWrites, expectedEnumValue, expectedReadValue );
            long removedEnumValue = ( long ) MultimeterFunctionModes.ResistanceCommonSide;
            _ = enumReadWrites.RemoveAt( removedEnumValue );
            TestUnknownEnumReadWriteValue( enumReadWrites, removedEnumValue );

            // check the diminished collection
            TestEnumReadWriteValue( enumReadWrites, expectedEnumValue, expectedReadValue );
        }

        #endregion

        #region " BUFFER READINGS TESTS "

        /// <summary> (Unit Test Method) tests parse reading unit. </summary>
        [TestMethod()]
        public void ParseReadingUnitTest()
        {
            double expectedValue = 1.234d;
            var expectedUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            string readingValue = $"{expectedValue}VDC";
            var expectedAmount = new Arebis.TypedUnits.Amount( expectedValue, expectedUnit );
            var actualAmount = BufferReading.ToAmount( readingValue );
            Assert.AreEqual( expectedAmount, actualAmount );
        }


        #endregion

        #region " BITMAPS TESTS "

        /// <summary> Define measurement events bitmasks. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( MeasurementEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.ReadingOverflow, 1 << 0 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit1, 1 << 1 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit1, 1 << 2 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit2, 1 << 3 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit2, 1 << 4 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.ReadingAvailable, 1 << 5 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.BufferAvailable, 1 << 7 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.BufferHalfFull, 1 << 8 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.BufferFull, 1 << 9 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.BufferPretriggered, 1 << 11 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.Questionable, 1 << 31, true );
        }

        /// <summary> Define operation event bitmasks. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( OperationEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Arming, 1 << 6 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Calibrating, 1 << 0 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Idle, 1 << 10 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Measuring, 1 << 4 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Triggering, 1 << 5 );
        }

        /// <summary> Define questionable event bitmasks. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( QuestionableEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            bitmaskDictionary.Add( QuestionableEventBitmaskKey.CalibrationSummary, 1 << 4 );
            bitmaskDictionary.Add( QuestionableEventBitmaskKey.CommandWarning, 1 << 8 );
            // bitmaskDictionary.Add(QuestionableEventBitmask.TemperatureSummary, 1 << 14)
        }

        /// <summary> (Unit Test Method) tests build bitmap dictionary. </summary>
        [TestMethod()]
        public void BuildBitmapDictionaryTest()
        {
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            DefineBitmasks( measurementBitmasks );

            // test existing key
            int queryKey = ( int ) MeasurementEventBitmaskKey.HighLimit2;
            int expectedBitmask = 1 << 4;
            int actualBitmask = measurementBitmasks[queryKey];
            Assert.AreEqual( expectedBitmask, actualBitmask, $"Bitmask of key 0x{queryKey:X} should match expected value" );

            // test non existing key
            queryKey = ( int ) MeasurementEventBitmaskKey.FirstReadingInGroup;
            _ = Core.MSTest.Asserts.Instance.Throws<ArgumentException>( () => measurementBitmasks.IsAnyBitOn( queryKey ), $"Bitmask of key 0x{queryKey:X} should throw an exception" );

            // test invalid bitmask
            queryKey = ( int ) MeasurementEventBitmaskKey.FirstReadingInGroup;
            int existingBitmask = 1 << 4;
            _ = Core.MSTest.Asserts.Instance.Throws<ArgumentException>( () => measurementBitmasks.Add( queryKey, existingBitmask ), $"Bitmask of key 0x{queryKey:X} should throw an exception because {existingBitmask} already exists" );
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            DefineBitmasks( operationBitmasks );

            // test existing key
            queryKey = ( int ) OperationEventBitmaskKey.Arming;
            expectedBitmask = 1 << 6;
            actualBitmask = operationBitmasks[queryKey];
            Assert.AreEqual( expectedBitmask, actualBitmask, $"Bitmask of key 0x{queryKey:X} should match expected value" );

            // test non existing key
            queryKey = ( int ) OperationEventBitmaskKey.Setting;
            _ = Core.MSTest.Asserts.Instance.Throws<ArgumentException>( () => operationBitmasks.IsAnyBitOn( queryKey ), $"Bitmask of key 0x{queryKey:X} should throw an exception" );

            // test invalid bitmask
            queryKey = ( int ) OperationEventBitmaskKey.Setting;
            existingBitmask = 1 << 6;
            _ = Core.MSTest.Asserts.Instance.Throws<ArgumentException>( () => operationBitmasks.Add( queryKey, existingBitmask ), $"Bitmask of key 0x{queryKey:X} should throw an exception because {existingBitmask} already exists" );
            var questionableBitmasks = new QuestionableEventsBitmaskDictionary();
            DefineBitmasks( questionableBitmasks );

            // test existing key
            queryKey = ( int ) QuestionableEventBitmaskKey.CalibrationSummary;
            expectedBitmask = 1 << 4;
            actualBitmask = questionableBitmasks[queryKey];
            Assert.AreEqual( expectedBitmask, actualBitmask, $"Bitmask of key 0x{queryKey:X} should match expected value" );

            // test non existing key
            queryKey = ( int ) QuestionableEventBitmaskKey.TemperatureSummary;
            _ = Core.MSTest.Asserts.Instance.Throws<ArgumentException>( () => questionableBitmasks.IsAnyBitOn( queryKey ), $"Bitmask of key 0x{queryKey:X} should throw an exception" );

            // test invalid bitmask
            queryKey = ( int ) QuestionableEventBitmaskKey.TemperatureSummary;
            existingBitmask = 1 << 4;
            _ = Core.MSTest.Asserts.Instance.Throws<ArgumentException>( () => questionableBitmasks.Add( queryKey, existingBitmask ), $"Bitmask of key 0x{queryKey:X} should throw an exception because {existingBitmask} already exists" );
        }

        #endregion

    }
}
