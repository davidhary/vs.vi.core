﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Device VI Tests" )]
[assembly: AssemblyDescription( "Virtual Device Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Device.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
