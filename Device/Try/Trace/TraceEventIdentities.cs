using System;
using System.Collections.Generic;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Try
{

    /// <summary> A trace event identities. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-17 </para>
    /// </remarks>
    [TestClass()]
    public class TraceEventIdentities
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        #endregion

        #region " SAVE TRACE EVENT IDENTITIES "

        /// <summary> Save the trace event identities. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="fileName"> Filename of the file. </param>
        /// <param name="openMode"> The open mode. </param>
        /// <param name="values">   The values. </param>
        private static void SaveTraceEventIdentities( string fileName, OpenMode openMode, IEnumerable<KeyValuePair<string, int>> values )
        {
            if ( values is null )
                throw new ArgumentNullException( nameof( values ) );
            int fileNo = FileSystem.FreeFile();
            if ( openMode == OpenMode.Output && System.IO.File.Exists( fileName ) )
            {
                System.IO.File.Delete( fileName );
            }

            FileSystem.FileOpen( fileNo, fileName, openMode );
            FileSystem.WriteLine( fileNo, "hex", "decimal", "description" );
            foreach ( KeyValuePair<string, int> value in values )
                FileSystem.WriteLine( fileNo, value.Value.ToString( "X" ), value.Value.ToString(), value.Key );
            FileSystem.FileClose( fileNo );
        }

        /// <summary> (Unit Test Method) enumerates test event identities. </summary>
        [TestMethod()]
        public void SaveTestEventIdentities()
        {
            string fileName = TestAssist.TraceEventProjectIdentitiesFileName;
            var values = new List<KeyValuePair<string, int>>();
            foreach ( Pith.My.ProjectTraceEventId value in Enum.GetValues( typeof( Pith.My.ProjectTraceEventId ) ) )
                values.Add( new KeyValuePair<string, int>( Core.EnumExtensions.EnumExtensionsMethods.Description( value ), ( int ) value ) );
            SaveTraceEventIdentities( fileName, OpenMode.Output, values );
        }

        #endregion

    }
}
