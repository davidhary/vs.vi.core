using System;

using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Try
{

    /// <summary> A resource tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "ni.visa" )]
    public class NationalVisaResourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        #endregion

        #region " VERSION TESTS "

        /// <summary> (Unit Test Method) tests visa functional versions. </summary>
        [TestMethod()]
        public void VisaFunctionalVersionsTest()
        {
            var actualVersion = National.Visa.ResourceManager.ImplementationVersion;
            var expectedVersion = new Version( National.Visa.My.Settings.Default.ImplementationVersion );
            Assert.AreEqual( expectedVersion.ToString(), actualVersion.ToString(), $"{nameof( National.Visa.ResourceManager.ImplementationVersion ).SplitWords()} should match" );
            actualVersion = National.Visa.ResourceManager.SpecificationVersion;
            expectedVersion = new Version( National.Visa.My.Settings.Default.SpecificationVersion );
            Assert.AreEqual( expectedVersion.ToString(), actualVersion.ToString(), $"{nameof( National.Visa.ResourceManager.SpecificationVersion ).SplitWords()} should match" );
        }

        #endregion

    }
}
