using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Try
{

    /// <summary> A nullable tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-11 </para>
    /// </remarks>
    [TestClass()]
    public class NullableTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
        }

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        #endregion

        #region " NULLABLE TESTS "

        /// <summary> A test for Nullable equality. </summary>
        [TestMethod()]
        public void NullableBooleanTest()
        {
            var @bool = default( bool? );
            bool boolValue = true;
            Assert.AreEqual( true, @bool is null, "Initialized to nothing" );
            Assert.AreEqual( true, @bool.Equals( new bool?() ), "Nullable not set equals to a new Boolean" );
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable not set is not equal to {0}", ( object ) boolValue );
            // VB.Net Assert.AreEqual(default(bool?), @bool == boolValue, "Nullable '=' operator yields Nothing");
            Assert.AreNotEqual( default( bool? ), @bool == boolValue, "Nullable '=' operator yields false" );
            boolValue = false;
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable not set is not equal to True" );
            @bool = boolValue;
            Assert.AreEqual( true, @bool.HasValue, "Has value -- set to {0}", ( object ) boolValue );
            @bool = new bool?();
            Assert.AreEqual( true, @bool is null, "Nullable set to new Boolean is still nothing" );
            Assert.AreEqual( true, @bool.Equals( new bool?() ), "Nullable set to new Boolean equals to a new Boolean" );
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable set to new Boolean not equal to {0}", ( object ) boolValue );
            boolValue = false;
            Assert.AreEqual( false, @bool.Equals( boolValue ), "Nullable set to new Boolean not equal to True" );
            @bool = boolValue;
            Assert.AreEqual( true, @bool.HasValue, "Has value -- set to {0}", ( object ) boolValue );
        }

        /// <summary> A test for Nullable Integer equality. </summary>
        [TestMethod()]
        public void NullableIntegerTest()
        {
            int integerValue = 1;
            var nullInt = default( int? );
            Assert.AreEqual( true, nullInt is null, "Initialized to nothing" );
            nullInt = new int?();
            Assert.AreEqual( true, nullInt is null, "Nullable set to new Boolean is nothing" );
            Assert.AreEqual( true, nullInt.Equals( new int?() ), "Nullable set to new Integer equals to a new Integer" );
            Assert.AreEqual( false, nullInt.Equals( integerValue ), "Nullable set to new Integer not equal to {0}", ( object ) integerValue );
            Assert.AreEqual( false, nullInt.Equals( integerValue ), "Nullable set to new Integer not equal to {0}", ( object ) integerValue );
            nullInt = integerValue;
            Assert.AreEqual( true, nullInt.HasValue, "Set to {0}", ( object ) integerValue );
            Assert.AreEqual( integerValue, nullInt.Value, "Set to  {0}", ( object ) integerValue );
        }

        /// <summary> (Unit Test Method) tests parse boolean. </summary>
        [TestMethod()]
        public void ParseBooleanTest()
        {
            string reading = "0";
            bool expectedResult = false;
            bool successParsing = Pith.SessionBase.TryParse( reading, out bool actualResult );
            Assert.AreEqual( expectedResult, actualResult, "Value set to {0}", ( object ) actualResult );
            Assert.AreEqual( true, successParsing, "Success set to {0}", ( object ) actualResult );
            reading = "1";
            expectedResult = true;
            successParsing = Pith.SessionBase.TryParse( reading, out actualResult );
            Assert.AreEqual( expectedResult, actualResult, "Value set to {0}", ( object ) actualResult );
            Assert.AreEqual( true, successParsing, "Success set to {0}", ( object ) actualResult );
        }

        #endregion

    }
}
