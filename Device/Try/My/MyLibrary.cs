namespace isr.VI.Try.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.TryTests;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI Try";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Unit Tests for the VI Libraries";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "isr.VI.Try";
    }
}
