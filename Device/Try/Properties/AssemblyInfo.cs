﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.VI.Try.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.VI.Try.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.VI.Try.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
