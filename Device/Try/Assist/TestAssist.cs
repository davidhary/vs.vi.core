namespace isr.VI.Try
{

    /// <summary> Gets or sets the filename of the trace event project identities file. </summary>
    internal sealed partial class TestAssist
    {

        #region " TRACE EVENT IDENTITY FILE "

        /// <summary> Gets the filename of the trace event project identities file. </summary>
        /// <value> The filename of the trace event project identities file. </value>
        public static string TraceEventProjectIdentitiesFileName => Core.AppSettingsReader.Get().AppSettingValue();

        #endregion

    }
}
