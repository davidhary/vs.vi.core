﻿using System.Diagnostics;
using isr.Core;

namespace isr.VI.Tsp2.My
{

    /// <summary>   Implements the <see cref="isr.Core.ApplianceBase"/> for this class library. </summary>
        /// <remarks>   David, 2020-09-29. </remarks>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    public class Appliance : ApplianceBase
    {

        #region " CONSTRUCTION "

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-29. </remarks>
        public Appliance() : base(MyProject.Application)
        {
            // the application info gets by the assembly that reads it for the first time.
            var info = Application.Info;
        }

        #endregion

        /// <summary>   Gets the identifier of the trace source. </summary>
        /// <value> The identifier of the trace event. </value>
        public override int TraceEventId
        {
            get
            {
                return MyLibrary.TraceEventId;
            }
        }

        /// <summary>   The assembly title. </summary>
        /// <value> The assembly title. </value>
        public override string AssemblyTitle
        {
            get
            {
                return MyLibrary.AssemblyTitle;
            }
        }

        /// <summary>   Information describing the assembly. </summary>
        /// <value> Information describing the assembly. </value>
        public override string AssemblyDescription
        {
            get
            {
                return MyLibrary.AssemblyDescription;
            }
        }

        /// <summary>   The assembly product. </summary>
        /// <value> The assembly product. </value>
        public override string AssemblyProduct
        {
            get
            {
                return MyLibrary.AssemblyProduct;
            }
        }
    }

    public partial class MyLibrary
    {

        /// <summary>   The appliance object provider. </summary>
        private static readonly ThreadSafeObjectProvider<Appliance> _ApplianeObjectProvider = new ThreadSafeObjectProvider<Appliance>();

        /// <summary>   Gets the implementation of the <see cref="isr.Core.ApplianceBase"/> for this class library. </summary>
        /// <value> The implementation of the <see cref="isr.Core.ApplianceBase"/> for this class library. </value>
        [System.ComponentModel.Design.HelpKeyword("My.MyLibrary.Appliance")]
        public static Appliance Appliance
        {
            get
            {
                return _ApplianeObjectProvider.GetInstance;
            }
        }

        /// <summary>   Gets the logger. </summary>
        /// <value> The logger. </value>
        public static Logger Logger
        {
            get
            {
                return Appliance.Logger;
            }
        }

        /// <summary>   Gets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public static TraceMessagesQueue UnpublishedTraceMessages
        {
            get
            {
                return Appliance.UnpublishedTraceMessages;
            }
        }
    }

        /// <summary>   A thread safe object provider. This class cannot be inherited. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        /// <typeparam name="T">    Generic type parameter. </typeparam>
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
    internal sealed class ThreadSafeObjectProvider<T> where T : new()
    {
        /// <summary>   The context. </summary>
        private readonly Microsoft.VisualBasic.MyServices.Internal.ContextValue<T> _Context = new Microsoft.VisualBasic.MyServices.Internal.ContextValue<T>();

        /// <summary>   Gets the get instance. </summary>
        /// <value> The get instance. </value>
        internal T GetInstance
        {
            [DebuggerHidden()]
            get
            {
                var Value = _Context.Value;
                if (Value is null)
                {
                    Value = new T();
                    _Context.Value = Value;
                }

                return Value;
            }
        }

        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        [DebuggerHidden()]
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public ThreadSafeObjectProvider() : base()
        {
        }
    }
}