using System;
using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp2
{

    /// <summary> Defines a Status Subsystem for a TSP System. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-10-07 </para>
    /// </remarks>
    public abstract class StatusSubsystemBase : VI.StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
        /// </summary>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based TSP session</see>. </param>
        protected StatusSubsystemBase( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ), Tsp.Syntax.EventLog.NoErrorCompoundMessage )
        {
            this.VersionInfo = new VersionInfo();
            InitializeSession( session );
        }

        #endregion

        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based TSP session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.DeviceClearDelayPeriod = TimeSpan.FromMilliseconds( 10d );
            session.ClearExecutionStateCommand = Tsp.Syntax.Status.ClearExecutionStateCommand;
            session.OperationCompleteCommand = Tsp.Syntax.Lua.OperationCompleteCommand;
            session.OperationCompletedQueryCommand = Tsp.Syntax.Lua.OperationCompletedQueryCommand;
            session.ResetKnownStateCommand = Tsp.Syntax.Lua.ResetKnownStateCommand;
            session.ServiceRequestEnableCommandFormat = Tsp.Syntax.Status.ServiceRequestEnableCommandFormat;

            // session.ServiceRequestEnableQueryCommand = Tsp.Syntax.Status.ServiceRequestEnableQueryCommand
            session.ServiceRequestEnableQueryCommand = Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand;

            // session.StandardEventStatusQueryCommand = Tsp.Syntax.Status.StandardEventStatusQueryCommand
            session.StandardEventStatusQueryCommand = Pith.Ieee488.Syntax.StandardEventStatusQueryCommand;

            // session.StandardEventEnableQueryCommand = Tsp.Syntax.Status.StandardEventEnableQueryCommand
            session.StandardEventEnableQueryCommand = Pith.Ieee488.Syntax.StandardEventEnableQueryCommand;
            session.StandardServiceEnableCommandFormat = Tsp.Syntax.Status.StandardServiceEnableCommandFormat;
            session.StandardServiceEnableCompleteCommandFormat = Tsp.Syntax.Status.StandardServiceEnableCompleteCommandFormat;
            session.WaitCommand = Tsp.Syntax.Lua.WaitCommand;
            // session.WaitCommand = VI.Pith.Ieee488.Syntax.WaitCommand

            session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.SerialNumber = new long?();
            this.SerialNumberReading = string.Empty;
            base.InitKnownState();
            try
            {
                // flush the input buffer in case the instrument has some leftovers.
                this.Session.DiscardUnreadData();
                if ( !string.IsNullOrWhiteSpace( this.Session.DiscardedData ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Data discarded after turning prompts and errors off;. Data: {0}.", this.Session.DiscardedData );
                }
            }
            catch ( Pith.NativeException ex )
            {
                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString() );
            }

            try
            {
                // flush write may cause the instrument to send off a new data.
                this.Session.DiscardUnreadData();
                if ( !string.IsNullOrWhiteSpace( this.Session.DiscardedData ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Unread data discarded after discarding unset data;. Data: {0}.", this.Session.DiscardedData );
                }
            }
            catch ( Pith.NativeException ex )
            {
                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString() );
            }

            // wait for operations to complete
            _ = this.Session.QueryOperationCompleted();
        }

        #endregion

        #region " PRESET "

        /// <summary> Gets or sets the preset command. </summary>
        /// <remarks>
        /// SCPI: ":STAT:PRES".
        /// <see cref="F:isr.VI.Pith.Scpi.Syntax.ScpiSyntax.StatusPresetCommand"></see>
        /// </remarks>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = string.Empty;

        #endregion

        #region " LANGUAGE "

        /// <summary> Gets or sets the Language query command. </summary>
        /// <value> The Language query command. </value>
        protected override string LanguageQueryCommand { get; set; } = Pith.Ieee488.Syntax.LanguageQueryCommand;

        /// <summary> Gets or sets the Language command format. </summary>
        /// <value> The Language command format. </value>
        protected override string LanguageCommandFormat { get; set; } = Pith.Ieee488.Syntax.LanguageCommandFormat;

        #endregion

        #region " MEASUREMENT EVENTS "

        /// <summary> Gets or sets the measurement status query command. </summary>
        /// <value> The measurement status query command. </value>
        protected override string MeasurementStatusQueryCommand { get; set; } = string.Empty; // Not available with TSP2

        /// <summary> Gets or sets the measurement event condition query command. </summary>
        /// <value> The measurement event condition query command. </value>
        protected override string MeasurementEventConditionQueryCommand { get; set; } = string.Empty; // Not available with TSP2

        #endregion

        #region " OPERATION REGISTER EVENTS "

        /// <summary> Gets or sets the operation event enable Query command. </summary>
        /// <value> The operation event enable Query command. </value>
        protected override string OperationEventEnableQueryCommand { get; set; } = Tsp.Syntax.Status.OperationEventEnableQueryCommand;

        /// <summary> Gets or sets the operation event enable command format. </summary>
        /// <value> The operation event enable command format. </value>
        protected override string OperationEventEnableCommandFormat { get; set; } = Tsp.Syntax.Status.OperationEventEnableCommandFormat;

        /// <summary> Gets or sets the operation event status query command. </summary>
        /// <value> The operation event status query command. </value>
        protected override string OperationEventStatusQueryCommand { get; set; } = Tsp.Syntax.Status.OperationEventQueryCommand;

        /// <summary> Programs the Operation register event enable bit mask. </summary>
        /// <param name="value"> The bitmask. </param>
        /// <returns> The mask to use for enabling the events; nothing if unknown. </returns>
        public override int? WriteOperationEventEnableBitmask( int value )
        {
            if ( (value & ( int ) OperationEventBits.UserRegister) != 0 )
            {
                // if enabling the user register, enable all events on the user register. 
                value = 0x4FFF;
            }

            return this.WriteOperationEventEnableBitmask( value );
        }

        #endregion

        #region " QUESTIONABLE REGISTER "

        /// <summary> Gets or sets the questionable status query command. </summary>
        /// <value> The questionable status query command. </value>
        protected override string QuestionableStatusQueryCommand { get; set; } = Pith.Scpi.Syntax.QuestionableEventQueryCommand;

        #endregion

        #region " LINE FREQUENCY "

        /// <summary> Gets or sets line frequency query command. </summary>
        /// <value> The line frequency query command. </value>
        protected override string LineFrequencyQueryCommand { get; set; } = Tsp.Syntax.LocalNode.LineFrequencyQueryCommand;

        #endregion

        #region " IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = Tsp.Syntax.LocalNode.IdentityQueryCommand;

        /// <summary> Gets or sets the serial number query command. </summary>
        /// <value> The serial number query command. </value>
        protected override string SerialNumberQueryCommand { get; set; } = Tsp.Syntax.LocalNode.SerialNumberFormattedQueryCommand;

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.PublishVerbose( "Trying to read identity;. " );
                Core.ApplianceBase.DoEvents();
                // wait for the delay time.
                // Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.PublishVerbose( $"Setting identity to {value};. " );
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = Tsp.Syntax.EventLog.ClearEventLogCommand;

        /// <summary> Gets or sets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected override string DeviceErrorQueryCommand { get; set; } = string.Empty; // VI.Pith.Scpi.Syntax.LastSystemErrorQueryCommand

        /// <summary> Gets or sets the 'Next Error' query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string DequeueErrorQueryCommand { get; set; } = string.Empty; // VI.Pith.Scpi.Syntax.LastSystemErrorQueryCommand

        /// <summary> Gets or sets the 'Next Error' query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = Tsp.Syntax.EventLog.NextErrorFormattedQueryCommand;

        /// <summary> Enqueue device error. </summary>
        /// <param name="compoundErrorMessage"> Message describing the compound error. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected override DeviceError EnqueueDeviceError( string compoundErrorMessage )
        {
            var de = new TspDeviceError();
            de.Parse( compoundErrorMessage );
            if ( de.IsError )
                this.DeviceErrorQueue.Enqueue( de );
            return de;
        }

        #endregion

        #region " COLLECT GARBAGE "

        /// <summary> Gets or sets the collect garbage wait complete command. </summary>
        /// <value> The collect garbage wait complete command. </value>
        protected override string CollectGarbageWaitCompleteCommand { get; set; } = Tsp.Syntax.Lua.CollectGarbageWaitCompleteCommand;

        #endregion

    }

    /// <summary> Enumerates the status bits for the operations register. </summary>
    [Flags()]
    public enum OperationEventBits
    {

        /// <summary>Empty.</summary>
        [System.ComponentModel.Description( "Empty" )]
        None = 0,

        /// <summary>Calibrating.</summary>
        [System.ComponentModel.Description( "Calibrating" )]
        Calibrating = 0x1,

        /// <summary>Measuring.</summary>
        [System.ComponentModel.Description( "Measuring" )]
        Measuring = 0x10,

        /// <summary>Prompts enabled.</summary>
        [System.ComponentModel.Description( "Prompts Enabled" )]
        Prompts = 0x800,

        /// <summary>User Register.</summary>
        [System.ComponentModel.Description( "User Register" )]
        UserRegister = 0x1000,

        /// <summary>User Register.</summary>
        [System.ComponentModel.Description( "Instrument summary" )]
        InstrumentSummary = 0x2000,

        /// <summary>Program running.</summary>
        [System.ComponentModel.Description( "Program Running" )]
        ProgramRunning = 0x4000,

        /// <summary>Unknown value. Sets bit 16 (zero based and beyond the register size).</summary>
        [System.ComponentModel.Description( "Unknown" )]
        Unknown = 0x10000
    }
}
