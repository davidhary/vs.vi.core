using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp
{

    /// <summary> A TSP visa session. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-24 </para>
    /// </remarks>
    public class VisaSession : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="VisaSession" /> class. </summary>
        public VisaSession() : base()
        {
            this.ApplyDefaultSyntax();
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                }
            }
            // release unmanaged-only resources.
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SYNTAX "

        /// <summary> Applies the default syntax. </summary>
        private void ApplyDefaultSyntax()
        {
            this.Session.ClearExecutionStateCommand = Syntax.Status.ClearExecutionStateCommand;
            this.Session.OperationCompletedQueryCommand = Syntax.Lua.OperationCompletedQueryCommand;
            this.Session.ResetKnownStateCommand = Syntax.Lua.ResetKnownStateCommand;
            this.Session.ServiceRequestEnableCommandFormat = Syntax.Status.ServiceRequestEnableCommandFormat;
            this.Session.ServiceRequestEnableQueryCommand = Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand;
            this.Session.StandardEventStatusQueryCommand = Syntax.Status.StandardEventStatusQueryCommand;
            this.Session.StandardEventEnableQueryCommand = Syntax.Status.StandardEventEnableQueryCommand;
            this.Session.StandardServiceEnableCommandFormat = Syntax.Status.StandardServiceEnableCommandFormat;
            this.Session.WaitCommand = Syntax.Lua.WaitCommand;
            this.Session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            this.Session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            this.Session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            this.Session.OperationEventBit = Pith.ServiceRequests.OperationEvent;
            this.Session.QuestionableEventBit = Pith.ServiceRequests.QuestionableEvent;
            this.Session.RequestingServiceBit = Pith.ServiceRequests.RequestingService;
            this.Session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
            this.Session.SystemEventBit = Pith.ServiceRequests.SystemEvent;
        }

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Applies the settings. </summary>
        protected override void ApplySettings()
        {
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
