using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp
{

    /// <summary>
    /// Defines the contract that must be implemented by a Source Current Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class CurrentSourceSubsystemBase : SourceMeasureUnitBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentSourceSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.StatusSubsystemBase">TSP status
        /// Subsystem</see>. </param>
        public CurrentSourceSubsystemBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Level = 0.105d;
            this.VoltageLimit = 0.000105d;
            this.Range = new double?();
            this.AutoRangeEnabled = true;
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Auto Range enabled. </summary>
        private bool? _AutoRangeEnabled;

        /// <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoRangeEnabled
        {
            get => this._AutoRangeEnabled;

            protected set {
                if ( !Equals( this.AutoRangeEnabled, value ) )
                {
                    this._AutoRangeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Writes the enabled state of the current Auto Range and reads back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command enables or disables the over-current Auto Range (OCP)
        /// function. The enabled state is On (1); the disabled state is Off (0). If the over-current
        /// AutoRange function is enabled and the output goes into constant current operation, the output
        /// is disabled and OCP is set in the Questionable Condition status register. The *RST value =
        /// Off.
        /// </remarks>
        /// <param name="value"> Enable if set to <c>true</c>; otherwise, disable. </param>
        /// <returns>
        /// <c>True</c> if <see cref="AutoRangeEnabled">Auto Range Enabled</see>;
        /// <c>False</c> otherwise.
        /// </returns>
        public bool? ApplyAutoRangeEnabled( bool value )
        {
            _ = this.WriteAutoRangeEnabled( value );
            return this.QueryAutoRangeEnabled();
        }

        /// <summary> Queries the current AutoRange state. </summary>
        /// <returns> True if the AutoRange is on; Otherwise, False. </returns>
        public bool? QueryAutoRangeEnabled()
        {
            this.AutoRangeEnabled = this.Session.QueryPrint( this.AutoRangeEnabled.GetValueOrDefault( true ), "{0}.source.rangei", this.SourceMeasureUnitReference );
            return this.AutoRangeEnabled;
        }

        /// <summary>
        /// Writes the enabled state of the current Auto Range without reading back the value from the
        /// device.
        /// </summary>
        /// <remarks>
        /// This command enables or disables the over-current AutoRange (OCP)
        /// function. The enabled state is On (1); the disabled state is Off (0). If the over-current
        /// AutoRange function is enabled and the output goes into constant current operation, the output
        /// is disabled and OCP is set in the Questionable Condition status register. The *RST value =
        /// Off.
        /// </remarks>
        /// <param name="value"> Enable if set to <c>true</c>; otherwise, disable. </param>
        /// <returns>
        /// <c>True</c> if <see cref="AutoRangeEnabled">Auto Range Enabled</see>;
        /// <c>False</c> otherwise.
        /// </returns>
        public bool? WriteAutoRangeEnabled( bool value )
        {
            _ = this.Session.WriteLine( string.Format( System.Globalization.CultureInfo.InvariantCulture, "{0}.source.rangei = {{0:'1';'1';'0'}} ", this.SourceMeasureUnitReference ), ( object ) Conversions.ToInteger( value ) );
            this.AutoRangeEnabled = value;
            return this.AutoRangeEnabled;
        }

        #endregion

        #region " LEVEL "

        /// <summary> The level. </summary>
        private double? _Level;

        /// <summary> Gets or sets the cached Source Current Level. </summary>
        /// <value> The Source Current Level. Actual current depends on the power supply mode. </value>
        public double? Level
        {
            get => this._Level;

            protected set {
                if ( !Nullable.Equals( this.Level, value ) )
                {
                    this._Level = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source current level. </summary>
        /// <remarks>
        /// This command set the immediate output current level. The value is in Amperes. The immediate
        /// level is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <param name="value"> The current level. </param>
        /// <returns> The Source Current Level. </returns>
        public double? ApplyLevel( double value )
        {
            _ = this.WriteLevel( value );
            return this.QueryLevel();
        }

        /// <summary> Queries the current level. </summary>
        /// <returns> The current level or none if unknown. </returns>
        public double? QueryLevel()
        {
            const decimal printFormat = 9.6m;
            this.Level = this.Session.QueryPrint( this.Level.GetValueOrDefault( 0d ), printFormat, "{0}.source.leveli", this.SourceMeasureUnitReference );
            return this.Level;
        }

        /// <summary>
        /// Writes the source current level without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output current level. The value is in Amperes. The immediate
        /// level is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <param name="value"> The current level. </param>
        /// <returns> The Source Current Level. </returns>
        public double? WriteLevel( double value )
        {
            _ = this.Session.WriteLine( "{0}.source.leveli={1}", this.SourceMeasureUnitReference, value );
            this.Level = value;
            return this.Level;
        }

        #endregion

        #region " RANGE "

        /// <summary> The Current Range. </summary>
        private double? _Range;

        /// <summary>
        /// Gets or sets the cached Source current range. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Range
        {
            get => this._Range;

            protected set {
                if ( !Nullable.Equals( this.Range, value ) )
                {
                    this._Range = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Source current Range. </summary>
        /// <remarks>
        /// The value is in Amperes. At *RST, the range is set to Auto and the specific range is unknown.
        /// </remarks>
        /// <param name="value"> The current Range. </param>
        /// <returns> The Current Range. </returns>
        public double? ApplyRange( double value )
        {
            _ = this.WriteRange( value );
            return this.QueryRange();
        }

        /// <summary> Queries the current Range. </summary>
        /// <returns> The current Range or none if unknown. </returns>
        public double? QueryRange()
        {
            const decimal printFormat = 9.6m;
            this.Range = this.Session.QueryPrint( this.Range.GetValueOrDefault( 0.99d ), printFormat, "{0}.source.rangei", this.SourceMeasureUnitReference );
            return this.Range;
        }

        /// <summary>
        /// Writes the Source current Range without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the current Range. The value is in Amperes. At *RST, the range is auto and
        /// the value is not known.
        /// </remarks>
        /// <param name="value"> The Source current Range. </param>
        /// <returns> The Source Current Range. </returns>
        public double? WriteRange( double value )
        {
            if ( value >= Pith.Scpi.Syntax.Infinity - 1d )
            {
                _ = this.Session.WriteLine( "{0}.source.rangei={0}.source.rangei.max", this.SourceMeasureUnitReference );
                value = Pith.Scpi.Syntax.Infinity;
            }
            else if ( value <= Pith.Scpi.Syntax.NegativeInfinity + 1d )
            {
                _ = this.Session.WriteLine( "{0}.source.rangei={0}.source.rangei.min", this.SourceMeasureUnitReference );
                value = Pith.Scpi.Syntax.NegativeInfinity;
            }
            else
            {
                _ = this.Session.WriteLine( "{0}.source.rangei={1}", this.SourceMeasureUnitReference, value );
            }

            this.Range = value;
            return this.Range;
        }

        #endregion

        #region " VOLTAGE LIMIT "

        /// <summary> The Voltage Limit. </summary>
        private double? _VoltageLimit;

        /// <summary>
        /// Gets or sets the cached source Voltage Limit for a Current Source. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? VoltageLimit
        {
            get => this._VoltageLimit;

            protected set {
                if ( !Nullable.Equals( this.VoltageLimit, value ) )
                {
                    this._VoltageLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Voltage Limit. </summary>
        /// <remarks>
        /// This command set the immediate output Voltage Limit. The value is in Amperes. The immediate
        /// Limit is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Voltage Limit. </param>
        /// <returns> The Source Voltage Limit. </returns>
        public double? ApplyVoltageLimit( double value )
        {
            _ = this.WriteVoltageLimit( value );
            return this.QueryVoltageLimit();
        }

        /// <summary> Queries the Voltage Limit. </summary>
        /// <returns> The Voltage Limit or none if unknown. </returns>
        public double? QueryVoltageLimit()
        {
            const decimal printFormat = 9.6m;
            this.VoltageLimit = this.Session.QueryPrint( this.VoltageLimit.GetValueOrDefault( 0.099d ), printFormat, "{0}.source.limitv", this.SourceMeasureUnitReference );
            return this.VoltageLimit;
        }

        /// <summary>
        /// Writes the source Voltage Limit without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command set the immediate output Voltage Limit. The value is in Amperes. The immediate
        /// Limit is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Voltage Limit. </param>
        /// <returns> The Source Voltage Limit. </returns>
        public double? WriteVoltageLimit( double value )
        {
            if ( value >= Pith.Scpi.Syntax.Infinity - 1d )
            {
                _ = this.Session.WriteLine( "{0}.source.limitv={0}.source.limitv.max", this.SourceMeasureUnitReference );
                value = Pith.Scpi.Syntax.Infinity;
            }
            else if ( value <= Pith.Scpi.Syntax.NegativeInfinity + 1d )
            {
                _ = this.Session.WriteLine( "{0}.source.limitv={0}.source.limitv.min", this.SourceMeasureUnitReference );
                value = Pith.Scpi.Syntax.NegativeInfinity;
            }
            else
            {
                _ = this.Session.WriteLine( "{0}.source.limitv={1}", this.SourceMeasureUnitReference, value );
            }

            this.VoltageLimit = value;
            return this.VoltageLimit;
        }

        #endregion

    }
}
