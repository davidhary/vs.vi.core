namespace isr.VI.Tsp
{

    /// <summary> Defines a System Subsystem for a TSP System. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-10-07 </para>
    /// </remarks>
    public abstract class SystemSubsystemBase : VI.SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.StatusSubsystemBase">TSP status
        /// Subsystem</see>. </param>
        protected SystemSubsystemBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " SYNTAX "

        /// <summary> Gets or sets the TSP revision query command. </summary>
        /// <value> The language revision query command. </value>
        protected override string LanguageRevisionQueryCommand { get; set; } = "_G.print(_G.localnode.revision)";

        #endregion

    }
}
