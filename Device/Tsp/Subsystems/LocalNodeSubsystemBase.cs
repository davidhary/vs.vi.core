using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp
{

    /// <summary> Defines the local node for a TSP System. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-11-01. Based on legacy status subsystem. </para>
    /// </remarks>
    public abstract class LocalNodeSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SystemSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.StatusSubsystemBase">TSP status
        /// Subsystem</see>. </param>
        protected LocalNodeSubsystemBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this._InitializeTimeout = TimeSpan.FromMilliseconds( 30000d );
            this.ShowErrorsStack = new Stack<bool?>();
            this.ShowPromptsStack = new Stack<bool?>();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> The initialize timeout. </summary>
        private TimeSpan _InitializeTimeout;

        /// <summary> Gets or sets the time out for doing a reset and clear on the instrument. </summary>
        /// <value> The connect timeout. </value>
        public TimeSpan InitializeTimeout
        {
            get => this._InitializeTimeout;

            set {
                if ( !value.Equals( this.InitializeTimeout ) )
                {
                    this._InitializeTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Defines the active state of the local node. </summary>
        public void ClearActiveState()
        {
            this.ExecutionState = TspExecutionState.IdleReady;
        }

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        public override void DefineClearExecutionState()
        {
            _ = this.ReadExecutionState();
            // Set all cached values that get reset by CLS
            this.ClearStatus();
            _ = this.Session.QueryOperationCompleted();
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override void InitKnownState()
        {
            base.InitKnownState();
            try
            {
                this.Session.StoreCommunicationTimeout( this.InitializeTimeout );
                // turn prompts off. This may not be necessary.
                this.TurnPromptsErrorsOff();
            }
            catch ( Exception ex )
            {
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Exception ignored turning off prompts;. {0}", ex.ToFullBlownString() );
            }
            finally
            {
                this.Session.RestoreCommunicationTimeout();
            }

            try
            {
                // flush the input buffer in case the instrument has some leftovers.
                this.Session.DiscardUnreadData();
                if ( !string.IsNullOrWhiteSpace( this.Session.DiscardedData ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Data discarded after turning prompts and errors off;. Data: {0}.", this.Session.DiscardedData );
                }
            }
            catch ( Pith.NativeException ex )
            {
                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString() );
            }

            try
            {
                // flush write may cause the instrument to send off a new data.
                this.Session.DiscardUnreadData();
                if ( !string.IsNullOrWhiteSpace( this.Session.DiscardedData ) )
                {
                    _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Unread data discarded after discarding unset data;. Data: {0}.", this.Session.DiscardedData );
                }
            }
            catch ( Pith.NativeException ex )
            {
                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception ignored clearing read buffer;. {0}", ex.ToFullBlownString() );
            }
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();

            // clear elements.
            this.ClearStatus();

            // enable processing of execution state.
            this.ProcessExecutionStateEnabled = true;

            // read the prompts status
            _ = this.QueryShowPrompts();

            // read the errors status
            _ = this.QueryShowErrors();
            _ = this.Session.QueryOperationCompleted();
            this.ExecutionState = new TspExecutionState?();
        }

        #endregion

        #region " SESSION "

        /// <summary> Handles Session property change. </summary>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected override void HandlePropertyChanged( Pith.SessionBase sender, string propertyName )
        {
            base.HandlePropertyChanged( sender, propertyName );
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Pith.SessionBase.LastMessageReceived ):
                    {
                        // parse the command to get the TSP execution state.
                        _ = this.ParseExecutionState( this.Session.LastMessageReceived, TspExecutionState.IdleReady );
                        break;
                    }

                case nameof( Pith.SessionBase.LastMessageSent ):
                    {
                        // set the TSP status
                        this.ExecutionState = TspExecutionState.Processing;
                        break;
                    }

                case nameof( Pith.SessionBase.ResourceNameCaption ):
                    {
                        this.NotifyPropertyChanged( nameof( this.ResourceNameCaption ) );
                        break;
                    }

                case nameof( Pith.SessionBase.ResourceTitleCaption ):
                    {
                        this.NotifyPropertyChanged( nameof( this.ResourceTitleCaption ) );
                        break;
                    }
            }
        }

        #endregion

        #region " ASSET TRIGGER "

        /// <summary> Issues a hardware trigger. </summary>
        public void AssertTrigger()
        {
            this.ExecutionState = TspExecutionState.IdleReady;
            this.Session.AssertTrigger();
        }

        #endregion

        #region " EXECUTION STATE "

        /// <summary> True to enable, false to disable the process execution state. </summary>
        private bool _ProcessExecutionStateEnabled;

        /// <summary> Gets or sets the process execution state enabled. </summary>
        /// <value> The process execution state enabled. </value>
        public bool ProcessExecutionStateEnabled
        {
            get => this._ProcessExecutionStateEnabled;

            set {
                if ( !value.Equals( this._ProcessExecutionStateEnabled ) )
                {
                    this._ProcessExecutionStateEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> State of the execution. </summary>
        private TspExecutionState? _ExecutionState;

        /// <summary>
        /// Gets or sets the last TSP execution state. Setting the last state is useful when closing the
        /// Tsp System.
        /// </summary>
        /// <value> The last state. </value>
        public TspExecutionState? ExecutionState
        {
            get => this._ExecutionState;

            set {
                if ( value.HasValue && !this.ExecutionState.HasValue || !value.HasValue && this.ExecutionState.HasValue || !value.Equals( this.ExecutionState ) )
                {
                    this._ExecutionState = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the instrument Execution State caption. </summary>
        /// <value> The state caption. </value>
        public string ExecutionStateCaption => this.ExecutionState.HasValue ? Core.EnumExtensions.EnumExtensionsMethods.Description( this.ExecutionState.Value ) : "N/A";

        /// <summary> Parses the state of the TSP prompt and saves it in the state cache value. </summary>
        /// <param name="value">        Specifies the read buffer. </param>
        /// <param name="defaultValue"> The default value. </param>
        /// <returns> The instrument Execution State. </returns>
        public TspExecutionState ParseExecutionState( string value, TspExecutionState defaultValue )
        {
            var state = defaultValue;
            if ( string.IsNullOrWhiteSpace( value ) || value.Length < 4 )
            {
            }
            else
            {
                value = value.Substring( 0, 4 );
                if ( value.StartsWith( Syntax.Constants.ReadyPrompt, true, System.Globalization.CultureInfo.CurrentCulture ) )
                {
                    state = TspExecutionState.IdleReady;
                }
                else if ( value.StartsWith( Syntax.Constants.ContinuationPrompt, true, System.Globalization.CultureInfo.CurrentCulture ) )
                {
                    state = TspExecutionState.IdleContinuation;
                }
                else if ( value.StartsWith( Syntax.Constants.ErrorPrompt, true, System.Globalization.CultureInfo.CurrentCulture ) )
                {
                    state = TspExecutionState.IdleError;
                }
                else
                {
                    // no prompt -- set to the default state
                    state = defaultValue;
                }
            }

            this.ExecutionState = state;
            return state;
        }

        /// <summary> Reads the state of the TSP prompt and saves it in the state cache value. </summary>
        /// <returns> The instrument Execution State. </returns>
        public TspExecutionState? ReadExecutionState()
        {

            // check status of the prompt flag.
            if ( this.ShowPrompts.HasValue )
            {

                // if prompts are on, 
                if ( this.ShowPrompts.Value )
                {

                    // do a read. This raises an event that parses the state
                    if ( this.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 1d ), 3 ) )
                    {
                        _ = this.Session.ReadLine();
                    }
                }
                else
                {
                    this.ExecutionState = TspExecutionState.Unknown;
                }
            }

            // check if we have data in the output buffer.  
            else if ( this.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 1d ), 3 ) )
            {

                // if data exists in the buffer, it may indicate that the prompts are already on 
                // so just go read the output buffer. Once read, the status will be parsed.
                _ = this.Session.ReadLine();
            }
            else
            {

                // if we have no value then we must first read the prompt status
                // once read, the status will be parsed.
                _ = this.QueryShowPrompts();
            }

            return this.ExecutionState;
        }

        #endregion

        #region " SHOW ERRORS "

        /// <summary> The show errors. </summary>
        private bool? _ShowErrors;

        /// <summary> Gets or sets the Show Errors sentinel. </summary>
        /// <remarks>
        /// When true, the unit will automatically display the errors stored in the error queue, and then
        /// clear the queue. Errors will be processed at the end of executing a command message (just
        /// prior to issuing a prompt if prompts are enabled). When false, errors will not display.
        /// Errors will be left in the error queue and must be explicitly read or cleared. The error
        /// prompt (TSP?) is enabled.
        /// </remarks>
        /// <value> <c>True</c> to show errors; otherwise <c>False</c>. </value>
        public bool? ShowErrors
        {
            get => this._ShowErrors;

            protected set {
                if ( !Nullable.Equals( value, this.ShowErrors ) )
                {
                    this._ShowErrors = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the show errors sentinel. </summary>
        /// <param name="value"> <c>True</c> to show errors; otherwise, <c>False</c>. </param>
        /// <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        public bool? ApplyShowErrors( bool value )
        {
            _ = this.WriteShowErrors( value );
            return this.QueryShowErrors();
        }

        /// <summary> Reads the condition for showing errors. </summary>
        /// <returns> <c>True</c> to show errors; otherwise <c>False</c>. </returns>
        public bool? QueryShowErrors()
        {
            this.ShowErrors = this.Session.QueryPrint( false, Syntax.LocalNode.ShowErrors );
            if ( !this.ProcessExecutionStateEnabled )
            {
                // read execution state explicitly, because session events are disabled.
                _ = this.ReadExecutionState();
            }

            return this.ShowErrors;
        }

        /// <summary> Sets the condition for showing errors. </summary>
        /// <param name="value"> true to value. </param>
        /// <returns> <c>True</c> to show errors; otherwise <c>False</c>. </returns>
        public bool? WriteShowErrors( bool value )
        {
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "showing errors;. " );
            this.Session.LastNodeNumber = new int?();
            _ = this.Session.WriteLine( Syntax.LocalNode.ShowErrorsSetterCommand, ( object ) Conversions.ToInteger( value ) );
            this.ShowErrors = value;
            if ( !this.ProcessExecutionStateEnabled )
            {
                // read execution state explicitly, because session events are disabled.
                _ = this.ReadExecutionState();
            }

            return this.ShowErrors;
        }

        #endregion

        #region " SHOW PROMPTS "

        /// <summary> The show prompts. </summary>
        private bool? _ShowPrompts;

        /// <summary> Gets or sets the Show Prompts sentinel. </summary>
        /// <remarks>
        /// When true, prompts are issued after each command message is processed by the instrument.<para>
        /// When false prompts are not issued.</para><para>
        /// Command messages do not generate prompts. Rather, the TSP instrument generates prompts in
        /// response to command messages. When prompting is enabled, the instrument generates prompts in
        /// response to command messages. There are three prompts that might be returned:</para><para>
        /// “TSP&gt;” is the standard prompt. This prompt indicates that everything is normal and the
        /// command is done processing.</para><para>
        /// “TSP?” is issued if there are entries in the error queue when the prompt is issued. Like the
        /// “TSP&gt;” prompt, it indicates the command is done processing. It does not mean the previous
        /// command generated an error, only that there are still errors in the queue when the command
        /// was done processing.</para><para>
        /// “&gt;&gt;&gt;&gt;” is the continuation prompt. This prompt is used when downloading scripts
        /// or flash images. When downloading scripts or flash images, many command messages must be sent
        /// as a unit. The continuation prompt indicates that the instrument is expecting more messages
        /// as part of the current command.</para>
        /// </remarks>
        /// <value> The show prompts. </value>
        public bool? ShowPrompts
        {
            get => this._ShowPrompts;

            protected set {
                if ( !Nullable.Equals( value, this.ShowPrompts ) )
                {
                    this._ShowPrompts = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the show Prompts sentinel. </summary>
        /// <param name="value"> <c>True</c> to show Prompts; otherwise, <c>False</c>. </param>
        /// <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        public bool? ApplyShowPrompts( bool value )
        {
            _ = this.WriteShowPrompts( value );
            return this.QueryShowPrompts();
        }

        /// <summary> Queries the condition for showing prompts. Controls prompting. </summary>
        /// <returns> <c>True</c> to show prompts; otherwise <c>False</c>. </returns>
        public bool? QueryShowPrompts()
        {
            this.ShowPrompts = this.Session.QueryPrint( false, Syntax.LocalNode.ShowPrompts );
            if ( !this.ProcessExecutionStateEnabled )
            {
                // read execution state explicitly, because session events are disabled.
                _ = this.ReadExecutionState();
            }

            return this.ShowPrompts;
        }

        /// <summary> Sets the condition for showing prompts. Controls prompting. </summary>
        /// <param name="value"> true to value. </param>
        /// <returns> <c>True</c> to show prompts; otherwise <c>False</c>. </returns>
        public bool? WriteShowPrompts( bool value )
        {
            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "showing prompts;. " );
            this.Session.LastNodeNumber = new int?();
            _ = this.Session.WriteLine( Syntax.LocalNode.ShowPromptsSetterCommand, ( object ) Conversions.ToInteger( value ) );
            this.ShowPrompts = value;
            if ( !this.ProcessExecutionStateEnabled )
            {
                // read execution state explicitly, because session events are disabled.
                _ = this.ReadExecutionState();
            }

            return this.ShowPrompts;
        }

        /// <summary>
        /// Turns off prompts and errors. It seems that the new systems come with prompts and errors off
        /// when the instrument is started or reset so this is not needed.
        /// </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void TurnPromptsErrorsOff()
        {

            // flush the input buffer in case the instrument has some leftovers.
            this.Session.DiscardUnreadData();
            string showPromptsCommand = "<failed to issue>";
            try
            {
                // turn off prompt transmissions
                _ = this.WriteShowPrompts( false );
                showPromptsCommand = this.Session.LastMessageSent;
            }
            catch
            {
            }

            // flush again in case turning off prompts added stuff to the buffer.
            this.Session.DiscardUnreadData();
            if ( !string.IsNullOrWhiteSpace( this.Session.DiscardedData ) )
            {
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Unread data discarded after turning prompts off;. Data: {0}.", this.Session.DiscardedData );
            }

            string showErrorsCommand = "<failed to issue>";
            try
            {
                // turn off error transmissions
                _ = this.WriteShowErrors( false );
                showErrorsCommand = this.Session.LastMessageSent;
            }
            catch
            {
            }

            // flush again in case turning off errors added stuff to the buffer.
            this.Session.DiscardUnreadData();
            if ( !string.IsNullOrWhiteSpace( this.Session.DiscardedData ) )
            {
                _ = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Unread data discarded after turning errors off;. Data: {0}.", this.Session.DiscardedData );
            }

            // now validate
            if ( this.QueryShowErrors().GetValueOrDefault( true ) )
            {
                throw new Core.OperationFailedException( this.ResourceNameCaption, showErrorsCommand, "turning off automatic error display--still on." );
            }
            else if ( this.QueryShowPrompts().GetValueOrDefault( true ) )
            {
                throw new Core.OperationFailedException( this.ResourceNameCaption, showPromptsCommand, "turning off test script prompts--still on." );
            }
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets or sets the stack for storing the show errors states. </summary>
        /// <value> A stack of show errors. </value>
        private Stack<bool?> ShowErrorsStack { get; set; }

        /// <summary> Gets or sets the stack for storing the show prompts states. </summary>
        /// <value> A stack of show prompts. </value>
        private Stack<bool?> ShowPromptsStack { get; set; }

        /// <summary> Clears the status. </summary>
        public void ClearStatus()
        {

            // clear the stacks
            this.ShowErrorsStack.Clear();
            this.ShowPromptsStack.Clear();
            this.ExecutionState = this.Session.IsDeviceOpen ? TspExecutionState.IdleReady : TspExecutionState.Closed;
        }

        /// <summary> Restores the status of errors and prompts. </summary>
        public void RestoreStatus()
        {
            var lastValue = this.ShowErrorsStack.Pop();
            if ( lastValue.HasValue )
            {
                _ = this.WriteShowErrors( lastValue.Value );
            }

            lastValue = this.ShowPromptsStack.Pop();
            if ( lastValue.HasValue )
            {
                _ = this.WriteShowPrompts( lastValue.Value );
            }
        }

        /// <summary> Saves the current status of errors and prompts. </summary>
        public void StoreStatus()
        {
            this.ShowErrorsStack.Push( this.QueryShowErrors() );
            this.ShowPromptsStack.Push( this.QueryShowPrompts() );
        }

        #endregion

    }

    /// <summary> Enumerates the TSP Execution State. </summary>
    public enum TspExecutionState
    {

        /// <summary> Not defined. </summary>
        [System.ComponentModel.Description( "Not defined" )]
        None,

        /// <summary> Closed. </summary>
        [System.ComponentModel.Description( "Closed" )]
        Closed,

        /// <summary> Received the continuation prompt.
        /// Send between lines when loading a script indicating that
        /// TSP received script line successfully and is waiting for next line
        /// or the end script command. </summary>
        [System.ComponentModel.Description( "Continuation" )]
        IdleContinuation,

        /// <summary> Received the error prompt. Error occurred;
        /// handle as desired. Use “errorqueue” commands to read and clear errors. </summary>
        [System.ComponentModel.Description( "Error" )]
        IdleError,

        /// <summary> Received the ready prompt. For example, TSP received script successfully and is ready for next command. </summary>
        [System.ComponentModel.Description( "Ready" )]
        IdleReady,

        /// <summary> A command was sent to the instrument. </summary>
        [System.ComponentModel.Description( "Processing" )]
        Processing,

        /// <summary> Cannot tell because prompt are off. </summary>
        [System.ComponentModel.Description( "Unknown" )]
        Unknown
    }
}
