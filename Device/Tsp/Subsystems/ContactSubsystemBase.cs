using System;
using System.Diagnostics;

using isr.Core;
using isr.Core.EnumExtensions;

namespace isr.VI.Tsp
{

    /// <summary> Defines the contract that must be implemented by a Contact Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class ContactSubsystemBase : SourceMeasureUnitBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.StatusSubsystemBase">TSP status
        /// Subsystem</see>. </param>
        public ContactSubsystemBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.ContactCheckOkay = new bool?();
            this.ContactCheckThreshold = new int?();
            this.ContactCheckSpeedMode = new ContactCheckSpeedMode?();
            this.ContactResistances = string.Empty;
        }

        #endregion

        #region " CONTACT CHECK "

        #region " CONTACT CHECK SPEED MODE "

        /// <summary> The Contact Check Speed Mode. </summary>
        private ContactCheckSpeedMode? _ContactCheckSpeedMode;

        /// <summary> Gets or sets the cached Contact Check Speed Mode. </summary>
        /// <value>
        /// The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if not set or
        /// unknown.
        /// </value>
        public ContactCheckSpeedMode? ContactCheckSpeedMode
        {
            get => this._ContactCheckSpeedMode;

            protected set {
                if ( !Nullable.Equals( this.ContactCheckSpeedMode, value ) )
                {
                    this._ContactCheckSpeedMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Contact Check Speed Mode. </summary>
        /// <param name="value"> The  Contact Check Speed Mode. </param>
        /// <returns>
        /// The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if unknown.
        /// </returns>
        public ContactCheckSpeedMode? ApplyContactCheckSpeedMode( ContactCheckSpeedMode value )
        {
            _ = this.WriteContactCheckSpeedMode( value );
            return this.QueryContactCheckSpeedMode();
        }

        /// <summary> Queries the Contact Check Speed Mode. </summary>
        /// <returns>
        /// The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if unknown.
        /// </returns>
        public ContactCheckSpeedMode? QueryContactCheckSpeedMode()
        {
            string currentValue = this.ContactCheckSpeedMode.ToString();
            this.Session.MakeEmulatedReplyIfEmpty( currentValue );
            currentValue = this.Session.QueryTrimEnd( $"_G.print({this.SourceMeasureUnitReference}.contact.speed())" );
            if ( string.IsNullOrWhiteSpace( currentValue ) )
            {
                string message = "Failed fetching Contact Check Speed Mode";
                Debug.Assert( !Debugger.IsAttached, message );
                this.ContactCheckSpeedMode = new ContactCheckSpeedMode?();
            }
            else
            {
                var se = new StringEnumerator<ContactCheckSpeedMode>();
                // strip the SMU reference.
                currentValue = currentValue.Substring( currentValue.LastIndexOf( ".", StringComparison.OrdinalIgnoreCase ) + 1 ).Trim( '.' );
                this.ContactCheckSpeedMode = se.ParseContained( currentValue.Substring( 4 ) );
            }

            return this.ContactCheckSpeedMode;
        }

        /// <summary>
        /// Writes the Contact Check Speed Mode without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The Contact Check Speed Mode. </param>
        /// <returns>
        /// The <see cref="ContactCheckSpeedMode">Contact Check Speed Mode</see> or none if unknown.
        /// </returns>
        public ContactCheckSpeedMode? WriteContactCheckSpeedMode( ContactCheckSpeedMode value )
        {
            _ = this.Session.WriteLine( "{0}.contact.speed={0}.{1}", this.SourceMeasureUnitReference, value.ExtractBetween() );
            this.ContactCheckSpeedMode = value;
            return this.ContactCheckSpeedMode;
        }

        #endregion

        #region " THRESHOLD "

        /// <summary> The contact check threshold. </summary>
        private int? _ContactCheckThreshold;

        /// <summary> Gets or sets (Protected) the contact check threshold. </summary>
        /// <value> The contact check threshold. </value>
        public int? ContactCheckThreshold
        {
            get => this._ContactCheckThreshold;

            set {
                if ( !Nullable.Equals( value, this.ContactCheckThreshold ) )
                {
                    this._ContactCheckThreshold = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Programs and reads back the Contact Check Threshold Level. </summary>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// The <see cref="ContactCheckThreshold">Contact Check Threshold</see> or nothing if not known.
        /// </returns>
        public int? ApplyContactCheckThreshold( int value )
        {
            _ = this.WriteContactCheckThreshold( value );
            return this.QueryContactCheckThreshold();
        }

        /// <summary> Reads back the Contact Check Threshold Level. </summary>
        /// <returns>
        /// The <see cref="ContactCheckThreshold">Contact Check Threshold</see> or nothing if not known.
        /// </returns>
        public virtual int? QueryContactCheckThreshold()
        {
            this.ContactCheckThreshold = this.Session.QueryPrint( this.ContactCheckThreshold.GetValueOrDefault( 15 ), "{0}.contact.threshold()", this.SourceMeasureUnitReference );
            return this.ContactCheckThreshold;
        }

        /// <summary>
        /// Programs the Contact Check Threshold Level without updating the value from the device.
        /// </summary>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// The <see cref="ContactCheckThreshold">Contact Check Threshold</see> or nothing if not known.
        /// </returns>
        public virtual int? WriteContactCheckThreshold( int value )
        {
            _ = this.Session.WriteLine( "{0}.contact.threshold={1}", this.SourceMeasureUnitReference, value );
            this.ContactCheckThreshold = value;
            return this.ContactCheckThreshold;
        }

        #endregion

        #region " RESISTANCES "

        /// <summary> The contact resistances. </summary>
        private string _ContactResistances;

        /// <summary> Gets or sets (Protected) the contact resistances. </summary>
        /// <value> The contact resistances. </value>
        public string ContactResistances
        {
            get => this._ContactResistances;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ContactResistances, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ContactResistances = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Reads the Contact Resistances. </summary>
        /// <returns>
        /// The <see cref="ContactResistances">Contact Resistances</see> or nothing if not known.
        /// </returns>
        public virtual string QueryContactResistances()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ContactResistances );
            this.ContactResistances = this.Session.QueryTrimEnd( $"_G.print({this.SourceMeasureUnitReference}.contact.r())" );
            return this.ContactResistances;
        }

        #endregion

        #region " CONTACT CHECK OKAY "

        /// <summary> ContactCheckOkay. </summary>
        private bool? _ContactCheckOkay;

        /// <summary> Gets or sets the cached Contact Check Okay sentinel. </summary>
        /// <value>
        /// <c>null</c> if Contact Check Okay is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ContactCheckOkay
        {
            get => this._ContactCheckOkay;

            protected set {
                if ( !Equals( this.ContactCheckOkay, value ) )
                {
                    this._ContactCheckOkay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Queries the Contact Check status. Also sets the <see cref="ContactCheckOkay">contact
        /// check</see> sentinel.
        /// </summary>
        /// <returns>
        /// <c>null</c> if not known; <c>True</c> if ContactCheckOkay; otherwise, <c>False</c>.
        /// </returns>
        public bool? QueryContactCheckOkay()
        {
            this.Session.MakeTrueFalseReplyIfEmpty( true );
            this.ContactCheckOkay = this.Session.IsStatementTrue( "{0}.contact.check()", this.SourceMeasureUnitReference );
            return this.ContactCheckOkay;
        }

        #region " CONTACT CHECK "

        /// <summary> Determines whether contact resistances are below the specified threshold. </summary>
        /// <param name="threshold"> The threshold. </param>
        /// <returns>
        /// <c>True</c> if passed, <c>False</c> if failed, <c>True</c> if passed. Exception is thrown if
        /// failed configuring contact check.
        /// </returns>
        public bool? CheckContacts( int threshold )
        {
            this.ContactResistances = "-1,-1";
            if ( !threshold.Equals( this.ContactCheckThreshold ) )
            {
                this.Session.LastNodeNumber = new int?();
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "writing contact check limit {0};. ", ( object ) threshold );
                _ = this.WriteContactCheckThreshold( threshold );
                this.CheckThrowDeviceException( false, "setting up contact check threshold;. using '{0}'", this.Session.LastMessageSent );
            }

            if ( !this.ContactCheckSpeedMode.Equals( Tsp.ContactCheckSpeedMode.Fast ) )
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "writing contact mode {0};. ", Tsp.ContactCheckSpeedMode.Fast );
                _ = this.WriteContactCheckSpeedMode( Tsp.ContactCheckSpeedMode.Fast );
                this.CheckThrowDeviceException( false, "setting up contact check speed;. using '{0}'", this.Session.LastMessageSent );
            }

            this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "querying contact check;. " );
            _ = this.QueryContactCheckOkay();
            this.CheckThrowDeviceException( true, "checking contact;. using '{0}'", this.Session.LastMessageSent );
            if ( this.ContactCheckOkay.HasValue && !this.ContactCheckOkay.Value )
            {
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "reading contact check resistance;. " );
                _ = this.QueryContactResistances();
                this.CheckThrowDeviceException( true, "reading contacts;. using '{0}'", this.Session.LastMessageSent );
                _ = string.IsNullOrWhiteSpace( this._ContactResistances )
                    ? this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Contact check failed;. Failed fetching contact resistances using  '{0}'", this.Session.LastMessageSent )
                    : this.Talker.Publish( TraceEventType.Warning, My.MyLibrary.TraceEventId, "Contact check failed;. Contact resistance {0} exceeded the limit {1}", this.ContactResistances, this.ContactCheckThreshold );
            }

            return this.ContactCheckOkay ?? new bool?();
        }

        #endregion

        #endregion

        #endregion

    }

    /// <summary> Specifies the contact check speed modes. </summary>
    public enum ContactCheckSpeedMode
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary> An enum constant representing the fast option. </summary>
        [System.ComponentModel.Description( "Fast (CONTACT_FAST)" )]
        Fast,

        /// <summary> An enum constant representing the medium option. </summary>
        [System.ComponentModel.Description( "Medium (CONTACT_MEDIUM)" )]
        Medium,

        /// <summary> An enum constant representing the slow option. </summary>
        [System.ComponentModel.Description( "Slow (CONTACT_SLOW)" )]
        Slow
    }
}
