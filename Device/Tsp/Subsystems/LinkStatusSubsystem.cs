using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.Tsp
{

    /// <summary> A link status subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-28 </para>
    /// </remarks>
    public class LinkStatusSubsystem : VI.StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="LinkStatusSubsystem" /> class.
        /// </summary>
        /// <param name="session"> The session. </param>
        public LinkStatusSubsystem( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ) )
        {
            this.VersionInfo = new VersionInfo();
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        /// <returns> A StatusSubsystem. </returns>
        public static LinkStatusSubsystem Create( Pith.SessionBase session )
        {
            LinkStatusSubsystem subsystem = null;
            try
            {
                subsystem = new LinkStatusSubsystem( session );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region " ERROR QUEUE: NODE "

        /// <summary> Gets or sets the node entities. </summary>
        /// <remarks> Required for reading the system errors. </remarks>
        /// <value> The node entities. </value>
        public NodeEntityCollection NodeEntities { get; private set; }

        /// <summary> The error queue count query command. </summary>
        private string _ErrorQueueCountQueryCommand = "_G.print(_G.string.format('%d',_G.errorqueue.count))";

        /// <summary> Gets or sets The ErrorQueueCount query command. </summary>
        /// <value> The ErrorQueueCount query command. </value>
        protected override string ErrorQueueCountQueryCommand
        {
            get => this._ErrorQueueCountQueryCommand;

            set {
                this._ErrorQueueCountQueryCommand = value;
                base.ErrorQueueCountQueryCommand = value;
            }
        }

        /// <summary> Queries error count on a remote node. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> . </param>
        /// <returns> The error count. </returns>
        public int QueryErrorQueueCount( NodeEntityBase node )
        {
            int? count;
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            this.ErrorQueueCountQueryCommand = node.IsController ? "_G.print(_G.string.format('%d',_G.errorqueue.count))" : $"_G.print(_G.string.format('%d',node[{node.Number}].errorqueue.count))";
            count = this.QueryErrorQueueCount();
            return count.GetValueOrDefault( 0 );
        }

        /// <summary> Clears the error cache. </summary>
        public override void ClearErrorCache()
        {
            base.ClearErrorCache();
            this.DeviceErrorQueue = new Queue<TspDeviceError>();
        }

        /// <summary> Queries if a given node exists. </summary>
        /// <param name="nodeNumber"> The node number. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool NodeExists( int nodeNumber )
        {
            bool affirmative = true;
            return this.NodeEntities.Count > 0 && this.NodeEntities.Contains( NodeEntityBase.BuildKey( nodeNumber ) ) ? affirmative : affirmative;
        }

        /// <summary> Clears the error queue for the specified node. </summary>
        /// <param name="nodeNumber"> The node number. </param>
        private void ClearErrorQueue( int nodeNumber )
        {
            if ( !this.NodeExists( nodeNumber ) )
            {
                _ = this.Session.WriteLine( "node[{0}].errorqueue.clear() waitcomplete({0})", ( object ) nodeNumber );
            }
        }

        /// <summary> Clears the error queue. </summary>
        public override void ClearErrorQueue()
        {
            if ( this.NodeEntities is null )
            {
                base.ClearErrorQueue();
            }
            else
            {
                this.ClearErrorCache();
                foreach ( NodeEntityBase node in this.NodeEntities )
                    this.ClearErrorQueue( node.Number );
            }
        }

        /// <summary> Gets the error queue. </summary>
        /// <value> A Queue of device errors. </value>
        protected new Queue<TspDeviceError> DeviceErrorQueue { get; private set; }

        /// <summary> Returns the queued error. </summary>
        /// <remarks> Sends the error print format query and reads back and parses the error. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> . </param>
        /// <returns> The queued error. </returns>
        private TspDeviceError QueryQueuedError( NodeEntityBase node )
        {
            var err = new TspDeviceError();
            if ( this.QueryErrorQueueCount( node ) > 0 )
            {
                if ( node is null )
                    throw new ArgumentNullException( nameof( node ) );
                this.Session.LastAction = this.Talker.Publish( TraceEventType.Information, My.MyLibrary.TraceEventId, "Querying queued device errors;. " );
                string message;
                if ( node.IsController )
                {
                    this.Session.LastNodeNumber = node.ControllerNodeNumber;
                    message = this.Session.QueryPrintStringFormatTrimEnd( $"%d,%s,%d,node{node.Number}", "_G.errorqueue.next()" );
                }
                else
                {
                    this.Session.LastNodeNumber = node.Number;
                    message = this.Session.QueryPrintStringFormatTrimEnd( $"%d,%s,%d,node{node.Number}", $"node[{node.Number}].errorqueue.next()" );
                }

                this.CheckThrowDeviceException( false, "getting queued error;. using {0}.", this.Session.LastMessageSent );
                err = new TspDeviceError();
                err.Parse( message );
            }

            return err;
        }

        /// <summary> Reads the device errors. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="node"> . </param>
        /// <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
        private string QueryDeviceErrors( NodeEntityBase node )
        {
            if ( node is null )
                throw new ArgumentNullException( nameof( node ) );
            TspDeviceError deviceError;
            do
            {
                deviceError = this.QueryQueuedError( node );
                if ( deviceError.IsError )
                {
                    this.DeviceErrorQueue.Enqueue( deviceError );
                }
            }
            while ( this.ErrorAvailable );
            var builder = new System.Text.StringBuilder();
            if ( this.DeviceErrorQueue is object && this.DeviceErrorQueue.Count > 0 )
            {
                _ = builder.AppendLine( $"Instrument {this.ResourceNameCaption} Node {node.Number} Errors:" );
                _ = builder.AppendLine();
                foreach ( TspDeviceError e in this.DeviceErrorQueue )
                    _ = builder.AppendLine( e.ErrorMessage );
            }

            this.AppendDeviceErrorMessage( builder.ToString() );
            return this.DeviceErrorReport;
        }

        /// <summary> Reads the device errors. </summary>
        /// <returns> <c>True</c> if device has errors, <c>False</c> otherwise. </returns>
        protected override string QueryDeviceErrors()
        {
            this.ClearErrorCache();
            foreach ( NodeEntityBase node in this.NodeEntities )
                _ = this.QueryDeviceErrors( node );
            this.NotifyPropertyChanged( nameof( this.DeviceErrorReport ) );
            return this.DeviceErrorReport;
        }

        #endregion

        #region " IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = Syntax.LocalNode.IdentityQueryCommand;

        /// <summary> Gets or sets the serial number query command. </summary>
        /// <value> The serial number query command. </value>
        protected override string SerialNumberQueryCommand { get; set; } = "_G.print(string.format('%d',_G.localnode.serialno))";

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.PublishVerbose( "Trying to read identity;. " );
                Core.ApplianceBase.DoEvents();
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.PublishVerbose( $"Setting identity to {value};. " );
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

    }
}
