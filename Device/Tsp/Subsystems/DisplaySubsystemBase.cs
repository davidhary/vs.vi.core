using System;

namespace isr.VI.Tsp
{

    /// <summary> Defines a System Subsystem for a TSP System. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-10-07 </para>
    /// </remarks>
    public abstract class DisplaySubsystemBase : VI.DisplaySubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.StatusSubsystemBase">status
        /// Subsystem</see>. </param>
        protected DisplaySubsystemBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.RestoreMainScreenWaitCompleteCommand = Syntax.Display.RestoreMainWaitCompleteCommand;
        }

        #endregion

        #region " EXISTS "

        /// <summary>
        /// Reads the display existence indicator. Some TSP instruments (e.g., 3706) may have no display.
        /// </summary>
        /// <returns> <c>True</c> if the display exists; otherwise, <c>False</c>. </returns>
        public override bool? QueryExists()
        {
            // detect the display
            this.Session.MakeEmulatedReplyIfEmpty( this.Exists.GetValueOrDefault( true ) );
            this.Exists = !this.Session.IsNil( Syntax.Display.SubsystemName );
            return this.Exists;
        }

        #endregion

        #region " CLEAR "

        /// <summary> Gets or sets the clear command. </summary>
        /// <value> The clear command. </value>
        protected override string ClearCommand { get; set; } = Syntax.Display.ClearCommand;

        /// <summary> Clears the display. </summary>
        /// <remarks> Sets the display to the user mode. </remarks>
        public override void ClearDisplay()
        {
            this.DisplayScreen = DisplayScreens.User;
            if ( this.QueryExists().GetValueOrDefault( false ) )
            {
                _ = this.Session.WriteLine( Syntax.Display.ClearCommand );
            }
        }

        /// <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
        /// <remarks> Sets the display to user. </remarks>
        public void TryClearDisplayMeasurement()
        {
            if ( (this.Exists.GetValueOrDefault( false ) && ( int? ) (this.DisplayScreen & DisplayScreens.Measurement) == 0) == true )
            {
                _ = this.TryClearDisplay();
            }
        }

        /// <summary> Clears the display if not in measurement mode and set measurement mode. </summary>
        /// <remarks> Sets the display to the measurement. </remarks>
        public void ClearDisplayMeasurement()
        {
            if ( (this.QueryExists().GetValueOrDefault( false ) && ( int? ) (this.DisplayScreen & DisplayScreens.Measurement) == 0) == true )
            {
                this.ClearDisplay();
            }

            this.DisplayScreen = DisplayScreens.Measurement;
        }

        #endregion

        #region " DISPLAY CHARACTER "

        /// <summary> Displays the character. </summary>
        /// <param name="lineNumber">      The line number. </param>
        /// <param name="position">        The position. </param>
        /// <param name="characterNumber"> The character number. </param>
        public void DisplayCharacter( int lineNumber, int position, int characterNumber )
        {
            this.DisplayScreen = DisplayScreens.User | DisplayScreens.Custom;
            if ( !this.QueryExists().GetValueOrDefault( false ) )
            {
                return;
            }
            // ignore empty character.
            if ( characterNumber <= 0 || characterNumber > Syntax.Display.MaximumCharacterNumber )
            {
                return;
            }

            _ = this.Session.WriteLine( Syntax.Display.SetCursorCommandFormat, lineNumber, position );
            _ = this.Session.WriteLine( Syntax.Display.SetCharacterCommandFormat, ( object ) characterNumber );
        }

        #endregion

        #region " DISPLAY LINE "

        /// <summary> Displays a message on the display. </summary>
        /// <param name="lineNumber"> The line number. </param>
        /// <param name="value">      The value. </param>
        public override void DisplayLine( int lineNumber, string value )
        {
            this.DisplayScreen = DisplayScreens.User | DisplayScreens.Custom;
            if ( !this.QueryExists().GetValueOrDefault( false ) )
            {
                return;
            }

            // ignore empty strings.
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return;
            }

            int length = Syntax.Display.FirstLineLength;
            if ( lineNumber < 1 )
            {
                lineNumber = 1;
            }
            else if ( lineNumber > 2 )
            {
                lineNumber = 2;
            }

            if ( lineNumber == 2 )
            {
                length = Syntax.Display.SecondLineLength;
            }

            _ = this.Session.WriteLine( Syntax.Display.SetCursorLineCommandFormat, ( object ) lineNumber );
            if ( value.Length < length )
                value = value.PadRight( length );
            _ = this.Session.WriteLine( Syntax.Display.SetTextCommandFormat, value );
        }

        /// <summary> Displays the program title. </summary>
        /// <param name="title">    Top row data. </param>
        /// <param name="subtitle"> Bottom row data. </param>
        public void DisplayTitle( string title, string subtitle )
        {
            this.DisplayLine( 0, title );
            this.DisplayLine( 2, subtitle );
        }

        #endregion

        #region " RESTORE "

        /// <summary> Gets or sets the restore display command. </summary>
        /// <value> The restore display command. </value>
        public string RestoreMainScreenWaitCompleteCommand { get; set; }

        /// <summary> Restores the instrument display. </summary>
        /// <param name="timeout"> The timeout. </param>
        public void RestoreDisplay( TimeSpan timeout )
        {
            this.DisplayScreen = DisplayScreens.Default;
            if ( this.Exists.HasValue && this.Exists.Value && !string.IsNullOrWhiteSpace( this.RestoreMainScreenWaitCompleteCommand ) )
            {
                // Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
                this.Session.Execute( $"{this.RestoreMainScreenWaitCompleteCommand}; {this.Session.OperationCompleteCommand}" );
                _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( timeout ).Status );
            }
        }

        /// <summary> Restores the instrument display. </summary>
        public void RestoreDisplay()
        {
            this.DisplayScreen = DisplayScreens.Default;
            if ( this.Exists.HasValue && this.Exists.Value && !string.IsNullOrWhiteSpace( this.RestoreMainScreenWaitCompleteCommand ) )
            {
                // Documentation error: Display Main equals 1, not 0. This code should work on other instruments.
                _ = this.Session.WriteLine( this.RestoreMainScreenWaitCompleteCommand );
                _ = this.Session.DoEventsReadDelay();
                _ = this.Session.QueryOperationCompleted();
            }
        }

        #endregion

    }
}
