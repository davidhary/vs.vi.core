using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading.Tasks;

using isr.Core;
using isr.Core.Models;
using isr.Core.TaskExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Pith;

namespace isr.VI
{
    public partial class VisaSessionBase : OpenerViewModel
    {

        #region " SELECTOR "

        /// <summary> Gets or sets the resources search pattern. </summary>
        /// <value> The resources search pattern. </value>
        public string ResourcesFilter
        {
            get => this._SessionFactory.ResourcesFilter;

            set {
                if ( !string.Equals( value, this.ResourcesFilter ) )
                {
                    this._SessionFactory.ResourcesFilter = value;
                    this.NotifyPropertyChanged();
                    this.Session.ResourcesFilter = value;
                }
            }
        }

        /// <summary> The session factory. </summary>
        private SessionFactory _SessionFactory;

        /// <summary> Gets the session factory. </summary>
        /// <value> The session factory. </value>
        public SessionFactory SessionFactory
        {
            get {
                // enable validation; Validation is disabled by default to facilitate
                // resource selection in case of resource manager mismatch between 
                // VISA implementations.
                this._SessionFactory.ValidationEnabled = this.ValidationEnabled;
                return this._SessionFactory;
            }
        }

        /// <summary> Attempts to validate resource from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryValidateResource( string resourceName )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
                resourceName = string.Empty;
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceTitleCaption} validating the resource";
                _ = this.PublishInfo( $"{activity};. " );
                this.Session.StatusPrompt = activity;
                result = this.SessionFactory.TryValidateResource( resourceName );
                if ( !string.IsNullOrEmpty( result.Details ) )
                    _ = this.PublishInfo( $"{activity};. reported {result.Details}" );
                this.Session.StatusPrompt = result.Success ? $"done {activity}" : $"failed {activity}";
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }
            finally
            {
                // this enables opening if the validated name has value
                this.ValidatedResourceName = this.SessionFactory.ValidatedResourceName;
            }

            return result;
        }

        /// <summary> Validates the resource name described by resourceName. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        public void ValidateResourceName( string resourceName )
        {
            _ = this.TryValidateResource( resourceName );
        }

        #endregion

        #region " RESOURCE NAME VALIDATION TASK "

        /// <summary> Gets or sets the resource name validation asynchronous task. </summary>
        /// <value> The resource name validation asynchronous task. </value>
        private Task ResourceNameValidationAsyncTask { get; set; }

        /// <summary> Gets or sets the resource name validation task. </summary>
        /// <value> The resource name validation task. </value>
        private Task ResourceNameValidationTask { get; set; }

        /// <summary> Query if the task validating the resource name is active. </summary>
        /// <returns>
        /// <c>true</c> if the task validating the resource name is active; otherwise <c>false</c>
        /// </returns>
        public bool IsValidatingResourceName()
        {
            return this.ResourceNameValidationTask.IsTaskActive();
        }

        /// <summary> Await resource name validation. </summary>
        /// <param name="timeout"> The timeout. </param>
        public void AwaitResourceNameValidation( TimeSpan timeout )
        {
            _ = (this.ResourceNameValidationTask?.Wait( timeout ));
        }

        /// <summary> Asynchronously validate the resource name. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A Threading.Tasks.Task. </returns>
        public Task AsyncValidateResourceName( string resourceName )
        {
            this.ResourceNameValidationAsyncTask = this.AsyncAwaitValidateResourceName( resourceName );
            return this.ResourceNameValidationTask;
        }

        /// <summary> Asynchronous await validate resource name. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A Task. </returns>
        private async Task AsyncAwaitValidateResourceName( string resourceName )
        {
            this.ResourceNameValidationTask = Task.Run( () => this.ValidateResourceName( resourceName ) );
            await this.ResourceNameValidationTask;
        }

        #endregion

        #region " RESOURCE NAME "

        /// <summary> Gets or sets the name of the candidate resource. </summary>
        /// <value> The name of the candidate resource. </value>
        public override string CandidateResourceName
        {
            get => base.CandidateResourceName;

            set {
                if ( !string.Equals( value, this.Session.CandidateResourceName ) || !string.Equals( value, base.CandidateResourceName ) )
                {
                    this.Session.CandidateResourceName = value;
                    this.SessionFactory.CandidateResourceName = value;
                    base.CandidateResourceName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the candidate resource name validated. </summary>
        /// <value> The candidate resource name validated. </value>
        public override bool CandidateResourceNameValidated
        {
            get => base.CandidateResourceNameValidated;

            set {
                this.Session.CandidateResourceNameConnected = value;
                base.CandidateResourceNameValidated = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Returns true if the validated candidate resource name was also connected. </summary>
        /// <value> The resource name connected. </value>
        public bool ResourceNameConnected => this.CandidateResourceNameValidated && string.Equals( this.CandidateResourceName, this.OpenResourceName );

        /// <summary> Gets or sets the name of the open resource. </summary>
        /// <value> The name of the open resource. </value>
        public override string OpenResourceName
        {
            get => base.OpenResourceName;

            set {
                if ( !string.Equals( value, this.Session.OpenResourceName ) || !string.Equals( value, base.OpenResourceName ) )
                {
                    this.Session.OpenResourceName = value;
                    base.OpenResourceName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the candidate resource title. </summary>
        /// <value> The candidate resource title. </value>
        public override string CandidateResourceTitle
        {
            get => base.CandidateResourceTitle;

            set {
                if ( !string.Equals( value, this.Session.CandidateResourceTitle ) || !string.Equals( value, base.CandidateResourceTitle ) )
                {
                    this.Session.CandidateResourceTitle = value;
                    base.CandidateResourceTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the resource title. </summary>
        /// <value> The resource title. </value>
        public override string OpenResourceTitle
        {
            get => base.OpenResourceTitle;

            set {
                if ( !string.Equals( value, this.Session.OpenResourceTitle ) || !string.Equals( value, base.OpenResourceTitle ) )
                {
                    this.Session.OpenResourceTitle = value;
                    base.OpenResourceTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the resource name caption. </summary>
        /// <value>
        /// The caption for the <see cref="VI.Pith.SessionBase.OpenResourceName"/> open or
        /// <see cref="VI.Pith.SessionBase.CandidateResourceName"/>resource names.
        /// </value>
        public override string ResourceNameCaption
        {
            get => base.ResourceNameCaption;

            set {
                if ( !string.Equals( value, this.Session.ResourceNameCaption ) || !string.Equals( value, base.ResourceNameCaption ) )
                {
                    this.Session.ResourceNameCaption = value;
                    base.ResourceNameCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the resource Title caption. </summary>
        /// <value>
        /// The caption for the <see cref="VI.Pith.SessionBase.OpenResourceTitle"/> open,
        /// <see cref="VI.Pith.SessionBase.CandidateResourceTitle"/> or identity.
        /// </value>
        public override string ResourceTitleCaption
        {
            get => base.ResourceTitleCaption;

            set {
                if ( !string.Equals( value, this.Session.ResourceTitleCaption ) || !string.Equals( value, base.ResourceTitleCaption ) )
                {
                    this.Session.ResourceTitleCaption = value;
                    base.ResourceTitleCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OPENER "

        /// <summary> Gets the Open status. </summary>
        /// <value> The Open status. </value>
        public override bool IsOpen => this.IsDeviceOpen;

        /// <summary> Gets the is session open. </summary>
        /// <value> The is session open. </value>
        public bool IsSessionOpen => this.Session is object && this.Session.IsSessionOpen;

        /// <summary> Gets the is device open. </summary>
        /// <remarks>
        /// The session device open property can be toggled without having the actual session open.  This
        /// is useful when emulating session functionality.
        /// </remarks>
        /// <value> The is device open. </value>
        public bool IsDeviceOpen => this.Session is object && this.Session.IsDeviceOpen;

        /// <summary> Initializes the session prior to opening access to the instrument. </summary>
        protected virtual void BeforeOpening()
        {
            this.ApplySettings();
        }

        /// <summary> Notifies an open changed. </summary>
        public override void NotifyOpenChanged()
        {
            this.NotifyPropertyChanged( nameof( this.IsDeviceOpen ) );
            this.NotifyPropertyChanged( nameof( this.IsSessionOpen ) );
            base.NotifyOpenChanged();
        }

        /// <summary> Opens the session. </summary>
        /// <remarks> Register the device trace notifier before opening the session. </remarks>
        /// <exception cref="isr.Core.OperationFailedException">   Thrown when operation failed to execute. </exception>
        /// <exception cref="OperationCanceledException"> Thrown when an Operation Canceled error condition
        /// occurs. </exception>
        /// <param name="resourceName">  Name of the resource. </param>
        /// <param name="resourceTitle"> The resource title. </param>
        public virtual void OpenSession( string resourceName, string resourceTitle )
        {
            bool success = false;
            try
            {
                if ( this.SubsystemSupportMode == SubsystemSupportMode.Full )
                {
                    this.CandidateResourceName = resourceName;
                    this.CandidateResourceTitle = resourceTitle;
                }

                string activity = $"Opening session to {resourceName}";
                if ( this.Enabled )
                    _ = this.PublishVerbose( $"{activity};. " );
                // initializes the session keep-alive commands.
                this.BeforeOpening();
                this.Session.OpenSession( resourceName, resourceTitle );
                if ( this.Session.IsSessionOpen )
                {
                    _ = this.PublishVerbose( $"Session open to {resourceName};. " );
                }
                else if ( this.Session.Enabled )
                {
                    throw new OperationFailedException( $"Unable to open session to {resourceName};. " );
                }
                else if ( !this.IsDeviceOpen )
                {
                    throw new OperationFailedException( $"Unable to emulate {resourceName};. " );
                }

                if ( this.Session.IsSessionOpen || this.IsDeviceOpen && !this.Session.Enabled )
                {
                    activity = $"{resourceTitle}:{resourceName} handling opening actions";
                    var e = new CancelEventArgs();
                    this.OnOpening( e );
                    if ( e.Cancel )
                        throw new OperationCanceledException( $"{activity} canceled;. " );
                    activity = $"{resourceTitle}:{resourceName} handling opened actions";

                    // this clears any existing errors using SDC, RST, and CLS.  
                    this.OnOpened( EventArgs.Empty );
                    if ( this.IsDeviceOpen )
                    {
                        activity = $"tagging {resourceTitle}:{resourceName} as open";
                        base.OpenResource( resourceName, resourceTitle );
                        // this causes a second device reset in order to initialize all the subsystems.
                        // which is a duplication.  Possibly at this point there is no need to do a full reset etc.
                        activity = $"{resourceTitle}:{resourceName} handling initialization";
                        this.OnInitializing( e );
                        if ( e.Cancel )
                            throw new OperationCanceledException( $"{activity} canceled;. " );
                        this.OnInitialized( e );
                    }
                    else
                    {
                        throw new OperationFailedException( $"{activity} failed;. " );
                    }
                }
                else
                {
                    throw new OperationFailedException( $"{activity} failed;. " );
                }

                success = true;
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( !success )
                {
                    _ = this.PublishWarning( $"{resourceName} failed connecting. Disconnecting." );
                    _ = this.TryCloseSession();
                }
            }
        }

        /// <summary> Allows the derived device to take actions after closing. </summary>
        /// <remarks> This override should occur as the last call of the overriding method. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosed( EventArgs e )
        {
            base.OnClosed( e );
        }

        /// <summary> Closes the session. </summary>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        public virtual void CloseSession()
        {
            string activity = string.Empty;
            try
            {
                // check if already closed.
                if ( this.IsDisposed || !this.IsDeviceOpen )
                    return;
                var e = new CancelEventArgs();
                activity = $"{this.OpenResourceTitle}:{this.OpenResourceName} handling closing actions";
                this.OnClosing( e );
                if ( e.Cancel )
                {
                    _ = this.PublishWarning( $"{activity} canceled;. " );
                }
                else
                {
                    activity = $"{this.CandidateResourceTitle} removing service request handler";
                    this.RemoveServiceRequestEventHandler();
                    activity = $"{this.CandidateResourceTitle} disabling service request handler";
                    this.Session.DisableServiceRequestEventHandler();
                    activity = $"{this.CandidateResourceTitle} closing session";
                    this.Session.CloseSession();
                    activity = $"{this.CandidateResourceTitle} handling closed actions";
                    this.OnClosed( EventArgs.Empty );
                }
            }
            catch ( NativeException ex )
            {
                throw new OperationFailedException( $"Failed {activity} while closing the VISA session.", ex );
            }
            catch ( Exception ex )
            {
                throw new OperationFailedException( $"Exception occurred {activity} while closing the session.", ex );
            }
        }

        #endregion

        #region " OPENER DEVICE or STATUS ONLY "

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks>
        /// This override should occur as the first call of the overriding method. After this call, the
        /// parent class adds the subsystems.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( this.SubsystemSupportMode == SubsystemSupportMode.StatusOnly )
            {
                if ( this.Subsystems.Count > 1 )
                {
                    _ = this.PublishWarning( $"{this.ResourceNameCaption} subsystem count {this.Subsystems.Count} on opening;. " );
                    this.Subsystems.Clear();
                    this.Subsystems.Add( this.StatusSubsystemBase );
                }
                else
                {
                    this.IsInitialized = false;
                }
            }

            base.OnOpening( e );
        }

        /// <summary> Allows the derived device to take actions after opening. </summary>
        /// <remarks>
        /// This override should occur as the last call of the overriding method. The subsystems are
        /// added as part of the <see cref="OnOpening(CancelEventArgs)"/> method. The synchronization
        /// context is captured as part of the property change and other event handlers and is no longer
        /// needed here.
        /// </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpened( EventArgs e )
        {
            if ( e is null )
                e = EventArgs.Empty;
            if ( this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                // saving the resource names in full mode only
                this.OpenResourceTitle = this.Session.OpenResourceTitle;
                this.OpenResourceName = this.Session.OpenResourceName;
                var outcome = TraceEventType.Information;
                if ( this.Session.Enabled & !this.Session.IsSessionOpen )
                    outcome = TraceEventType.Warning;
                _ = this.Publish( outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ", this.ResourceNameCaption, this.Session.Enabled.GetHashCode(), this.Session.IsDeviceOpen.GetHashCode(), this.Session.IsSessionOpen.GetHashCode() );
                ResourceNameInfoCollection.AddNewResource( this.OpenResourceName );
            }

            // A talker is assigned to the subsystem when the device is constructed. 
            // This talker is assigned to each subsystem when it is added.
            // Me.Subsystems.AssignTalker(Me.Talker)

            // The synchronization context is captured as part of the property change and other event
            // handlers and is no longer needed here.

            // reset and clear the device to remove any existing errors.
            if ( this.SubsystemSupportMode != SubsystemSupportMode.Native )
                this.StatusSubsystemBase.OnDeviceOpen();

            // 20181219: this is included in the base method: Me.NotifyPropertyChanged(NameOf(VisaSessionBase.IsDeviceOpen))
            // 2016/01/18: this was done before adding listeners, which was useful when using the device
            // as a class in a 'meter'. As a result, the actions taken when handling the Opened event, 
            // such as Reset and Initialize do not get reported. 
            // The solution was to add the device Initialize event to process publishing and initialization of device
            // and subsystems.
            base.OnOpened( e );
        }

        /// <summary> Opens a resource. </summary>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The resource title. </param>
        public override void OpenResource( string resourceName, string resourceTitle )
        {
            this.OpenSession( resourceName, resourceTitle );
            if ( this.IsOpen )
            {
                // at this point, the resource might have been updated using the instrument identity information.
                base.OpenResource( this.OpenResourceName, this.OpenResourceTitle );
            }
            else
            {
                base.OpenResource( resourceName, resourceTitle );
            }
        }

        /// <summary> Try open session. </summary>
        /// <param name="resourceName">  Name of the resource. </param>
        /// <param name="resourceTitle"> The resource title. </param>
        /// <returns>
        /// The (Success As Boolean, Details As String); Success: <c>True</c> if session opened;
        /// otherwise <c>False</c>
        /// </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public virtual (bool Success, string Details) TryOpenSession( string resourceName, string resourceTitle )
        {
            string activity = $"opening VISA session to {resourceTitle}:{resourceName}";
            bool success = true;
            string details = string.Empty;
            try
            {
                this.OpenSession( resourceName, resourceTitle );
            }
            catch ( Exception ex )
            {
                success = false;
                details = $"Exception {activity};. {ex.ToFullBlownString()}";
                _ = this.PublishException( activity, ex );
            }

            return (success && this.IsDeviceOpen, details);
        }

        /// <summary> Closes the resource. </summary>
        public override void CloseResource()
        {
            this.CloseSession();
            base.CloseResource();
        }

        /// <summary> Try close session. </summary>
        /// <returns>
        /// The (Success As Boolean, Details As String); Success: <c>True</c> if session closed;
        /// otherwise <c>False</c>
        /// </returns>
        public virtual (bool Success, string Details) TryCloseSession()
        {
            string activity = $"closing VISA session to {this.OpenResourceTitle}:{this.OpenResourceName}";
            bool success = true;
            string details = string.Empty;
            try
            {
                this.CloseResource();
                if ( !this.IsDeviceOpen )
                    details = $"failed {activity}";
            }
            catch ( OperationFailedException ex )
            {
                success = false;
                details = $"Exception {activity};. {ex.ToFullBlownString()}";
                _ = this.PublishException( activity, ex );
            }

            return (success && !this.IsDeviceOpen, details);
        }

        /// <summary> The subsystem support mode. </summary>
        private SubsystemSupportMode _SubsystemSupportMode;

        /// <summary> Gets or sets the subsystem support mode. </summary>
        /// <value> The subsystem support mode. </value>
        public SubsystemSupportMode SubsystemSupportMode
        {
            get => this._SubsystemSupportMode;

            set {
                if ( value != this.SubsystemSupportMode )
                {
                    this._SubsystemSupportMode = value;
                    this.NotifyOpenChanged();
                }
            }
        }

        #endregion

        #region " INITIALIZE "

        /// <summary> Executes the initializing actions. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnInitializing( CancelEventArgs e )
        {
            if ( e is object )
            {
                base.OnInitializing( e );
                // if starting a Visa Session (w/o a status subsystem, skip initializing.
                if ( e is object && !e.Cancel )
                {
                    // 16-2-18: replaces calls to reset and then init known states
                    // Me.ResetClearInit()
                    // 12/2019: Reset clear init is already part of the On Opened function.
                    if ( this.SubsystemSupportMode == SubsystemSupportMode.StatusOnly )
                    {
                        this.StatusSubsystemBase.DefineKnownResetState();
                        this.StatusSubsystemBase.DefineClearExecutionState();
                        this.StatusSubsystemBase.InitKnownState();
                    }
                    else if ( this.SubsystemSupportMode == SubsystemSupportMode.Full )
                    {
                        this.DefineResetKnownState();
                        this.DefineClearExecutionState();
                        this.InitKnownState();
                    }
                }
            }
        }

        /// <summary> Executes the initialized action. </summary>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnInitialized( EventArgs e )
        {
            bool hasIdentity = this.StatusSubsystemBase?.Identity is object;
            this.Identity = hasIdentity ? this.StatusSubsystemBase.Identity : string.Empty;
            if ( this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                // resource name is saved only with full support
                // update the session open resource title, which updates the visa session base title as well.
                if ( !string.IsNullOrWhiteSpace( this.StatusSubsystemBase.VersionInfoBase.Model ) )
                {
                    this.Session.OpenResourceTitle = this.StatusSubsystemBase.VersionInfoBase.Model;
                }
            }

            base.OnInitialized( e );
        }

        #endregion

    }

    /// <summary> Values that represent session subsystem support mode. </summary>
    public enum SubsystemSupportMode
    {

        /// <summary> An enum constant representing the full option. </summary>
        [Description( "Full subsystems support" )]
        Full,

        /// <summary> An enum constant representing the status only option. </summary>
        [Description( "Status subsystem support" )]
        StatusOnly,

        /// <summary> An enum constant representing the native option. </summary>
        [Description( "No subsystem support" )]
        Native
    }
}
