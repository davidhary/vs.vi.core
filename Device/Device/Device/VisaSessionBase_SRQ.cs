using System;
using System.Diagnostics;

using isr.Core;

namespace isr.VI
{
    public partial class VisaSessionBase
    {

        private readonly Core.Concurrent.ConcurrentToken<string> _ServiceRequestFailureMessageToken = new();

        /// <summary> Gets or sets a message describing the service request failure. </summary>
        /// <value> A message describing the service request failure. </value>
        public string ServiceRequestFailureMessage
        {
            get => this._ServiceRequestFailureMessageToken.Value;

            set {
                if ( string.IsNullOrEmpty( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ServiceRequestFailureMessage ) )
                {
                    this._ServiceRequestFailureMessageToken.Value = value;
                    if ( !string.IsNullOrWhiteSpace( this.ServiceRequestFailureMessage ) )
                    {
                        _ = this.PublishWarning( this.ServiceRequestFailureMessage );
                    }
                }
                // send every time
                this.SyncNotifyPropertyChanged();
            }
        }

        /// <summary> Processes the service request. </summary>
        protected abstract void ProcessServiceRequest();

        /// <summary> Reads the event registers after receiving a service request. </summary>
        /// <returns> True if it succeeds, false if it fails. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected bool TryProcessServiceRequest()
        {
            bool result = true;
            try
            {
                this.ProcessServiceRequest();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"{nameof( VisaSessionBase.ServiceRequestStatus )}.{ex.Data.Count}", this.ServiceRequestStatus );
                ex.Data.Add( $"{nameof( Pith.SessionBase.LastMessageSent )}.{ex.Data.Count}", this.Session.LastMessageSent );
                ex.Data.Add( $"{nameof( Pith.SessionBase.LastMessageReceived )}.{ex.Data.Count}", this.Session.LastMessageReceived );
                this.ServiceRequestFailureMessage = this.PublishException( "processing service request", ex );
                result = false;
            }

            return result;
        }

        /// <summary> The service request Status concurrent token. </summary>
        private readonly Core.Concurrent.ConcurrentToken<int> _ServiceRequestStatusToken = new();

        /// <summary> Gets or sets the Service Request Status. </summary>
        /// <value> The Service Request Status. </value>
        public int ServiceRequestStatus
        {
            get => this._ServiceRequestStatusToken.Value;

            set {
                if ( !int.Equals( this.ServiceRequestStatus, value ) )
                {
                    this._ServiceRequestStatusToken.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The service request reading concurrent token. </summary>
        private readonly Core.Concurrent.ConcurrentToken<string> _ServiceRequestReadingToken = new();

        /// <summary> Gets or sets the Service Request reading. </summary>
        /// <value> The Service Request reading. </value>
        public string ServiceRequestReading
        {
            get => this._ServiceRequestReadingToken.Value;

            set {
                if ( !string.Equals( this.ServiceRequestReading, value ) )
                {
                    this._ServiceRequestReadingToken.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }


        /// <summary>   (Immutable) the lazy service request automatic read token. </summary>
        private readonly Lazy<Core.Concurrent.ConcurrentToken<bool>> _LazyServiceRequestAutoReadToken = new( () => new Core.Concurrent.ConcurrentToken<bool>() );

        /// <summary> Gets or sets the automatic read Service Requesting option . </summary>
        /// <value> The automatic read service request option. </value>
        public bool ServiceRequestAutoRead
        {
            get => this._LazyServiceRequestAutoReadToken.Value.Value;

            set {
                if ( this.ServiceRequestAutoRead != value )
                {
                    this._LazyServiceRequestAutoReadToken.Value.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Removes the ServiceRequested event handlers. </summary>
        protected void RemoveServiceRequestedEventHandlers()
        {
            this._ServiceRequestedEventHandlers?.RemoveAll();
        }

        /// <summary> The ServiceRequested event handlers. </summary>
        private readonly EventHandlerContextCollection<EventArgs> _ServiceRequestedEventHandlers = new();

        /// <summary> Event queue for all listeners interested in ServiceRequested events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public event EventHandler<EventArgs> ServiceRequested
        {
            add {
                this._ServiceRequestedEventHandlers.Add( new EventHandlerContext<EventArgs>( value ) );
            }

            remove {
                this._ServiceRequestedEventHandlers.RemoveValue( value );
            }
        }

        /// <summary>   Raises the service requested event. </summary>
        /// <remarks>   David, 2020-11-30. </remarks>
        /// <param name="sender">   Source of the event. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        private void OnServiceRequested( object sender, EventArgs e )
        {
            this._ServiceRequestedEventHandlers.Post( sender, e );
        }

        /// <summary>
        /// Safely and synchronously <see cref="EventHandlerContextCollection{TEventArgs}.Send">sends</see> or invokes the
        /// <see cref="ServiceRequested">ServiceRequested Event</see>.
        /// </summary>
        /// <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
        protected void SyncNotifyServiceRequested( EventArgs e )
        {
            this._ServiceRequestedEventHandlers.Send( this, e );
        }

        /// <summary> Session base service requested. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionBaseServiceRequested( object sender, EventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( Pith.SessionBase )} service request";
            try
            {
                _ = this.TryProcessServiceRequest();
                if ( this.ServiceNotificationLevel == Pith.NotifySyncLevel.Sync )
                {
                    this._ServiceRequestedEventHandlers.Send( this, e );
                }
                else if ( this.ServiceNotificationLevel == Pith.NotifySyncLevel.Async )
                {
                    this._ServiceRequestedEventHandlers.Post( this, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Starts awaiting service request reading task. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A Threading.Tasks.Task. </returns>
        public async System.Threading.Tasks.Task StartAwaitingServiceRequestReadingTask( TimeSpan timeout )
        {
            await System.Threading.Tasks.Task.Factory.StartNew( () => {
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                while ( this.ServiceRequestEventEnabled && string.IsNullOrWhiteSpace( this.ServiceRequestReading )
                            && string.IsNullOrWhiteSpace( this.ServiceRequestFailureMessage )
                            && sw.ElapsedTicks < ticks )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    ApplianceBase.DoEvents();
                }
            } ).ConfigureAwait( false );
        }
    }
}
