using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.VI.ExceptionExtensions;
using isr.VI.Pith;

namespace isr.VI
{
    public partial class VisaSessionBase
    {

        private System.Timers.Timer _PollTimerThis;

        /// <summary>   Gets or sets the poll timer this. </summary>
        /// <value> The poll timer this. </value>
        private System.Timers.Timer PollTimerThis
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PollTimerThis;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PollTimerThis != null )
                {
                    this._PollTimerThis.Elapsed -= this.PollTimer_Elapsed;
                }

                this._PollTimerThis = value;
                if ( this._PollTimerThis != null )
                {
                    this._PollTimerThis.Elapsed += this.PollTimer_Elapsed;
                }
            }
        }

        /// <summary> Gets or sets the poll synchronizing object. </summary>
        /// <value> The poll synchronizing object. </value>
        public System.ComponentModel.ISynchronizeInvoke PollSynchronizingObject
        {
            get => this.PollTimerThis.SynchronizingObject;

            set => this.PollTimerThis.SynchronizingObject = value;
        }

        /// <summary> The poll reading. </summary>
        private string _PollReading;

        /// <summary> Gets or sets the poll reading. </summary>
        /// <value> The poll reading. </value>
        public string PollReading
        {
            get => this._PollReading;

            set {
                if ( !string.Equals( this.PollReading, value ) )
                {
                    this._PollReading = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True to poll automatic read. </summary>
        private bool _PollAutoRead;

        /// <summary> Gets or sets the automatic read polling option . </summary>
        /// <value> The automatic read status. </value>
        public bool PollAutoRead
        {
            get => this._PollAutoRead;

            set {
                if ( this.PollAutoRead != value )
                {
                    this._PollAutoRead = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the poll Timespan. </summary>
        /// <value> The poll Timespan. </value>
        public TimeSpan PollTimespan
        {
            get => TimeSpan.FromMilliseconds( this.PollTimerThis.Interval );

            set {
                if ( this.PollTimespan != value )
                {
                    this.PollTimerThis.Interval = value.TotalMilliseconds;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The poll enabled. </summary>
        private Core.Concurrent.ConcurrentToken<bool> _PollEnabled;

        /// <summary> Gets or sets the poll enabled. </summary>
        /// <value> The poll enabled. </value>
        public bool PollEnabled
        {
            get => this._PollEnabled.Value;

            set {
                if ( this.PollEnabled != value )
                {
                    if ( value )
                    {
                        // turn off the poll message available sentinel.
                        this.PollMessageAvailable = false;
                        // enable only if service request is not enabled. 
                        this.PollTimerThis.Enabled = !this.Session.ServiceRequestEventEnabled;
                    }

                    this._PollEnabled.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The poll message available bitmask. </summary>
        private int _PollMessageAvailableBitmask = ( int ) ServiceRequests.MessageAvailable;

        /// <summary> Gets or sets the poll message available bitmask. </summary>
        /// <value> The message available bitmask. </value>
        public int PollMessageAvailableBitmask
        {
            get => this._PollMessageAvailableBitmask;

            set {
                if ( this.PollMessageAvailableBitmask != value )
                {
                    this._PollMessageAvailableBitmask = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Queries if a message is available. </summary>
        /// <param name="statusByte"> The status byte. </param>
        /// <returns> <c>true</c> if a message is available; otherwise <c>false</c> </returns>
        private bool IsPollMessageAvailable( int statusByte )
        {
            return SessionBase.IsFullyMasked( statusByte, this.PollMessageAvailableBitmask );
        }

        /// <summary> True if poll message available. </summary>
        private bool _PollMessageAvailable;

        /// <summary> Gets or sets the poll message available status. </summary>
        /// <value> The poll message available. </value>
        public bool PollMessageAvailable
        {
            get => this._PollMessageAvailable;

            set {
                if ( this.PollMessageAvailable != value )
                {
                    this._PollMessageAvailable = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Event handler. Called by _PollTimer for tick events. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void PollTimer_Elapsed( object sender, EventArgs e )
        {
            if ( sender is not System.Timers.Timer tmr || !this.IsSessionOpen )
                return;
            try
            {
                // disable the timer.
                tmr.Stop();
                tmr.Enabled = false;
                if ( !this.Session.ServiceRequestEventEnabled )
                {
                    _ = this.Session.DoEventsStatusReadDelay();
                    if ( this.IsPollMessageAvailable( ( int ) this.Session.ReadStatusRegister() ) )
                    {
                        if ( this.PollAutoRead )
                        {
                            this.PollReading = string.Empty;
                            // allow other threads to execute
                            _ = this.Session.DoEventsReadDelay();
                            // result is also stored in the last message received.
                            this.PollReading = this.Session.ReadFreeLineTrimEnd();
                            // read the status register after delay allowing the status to stabilize.
                            _ = this.Session.DelayReadStatusRegister();
                        }

                        this.PollMessageAvailable = true;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                tmr.Enabled = Nullable.Equals( true, this.PollEnabled && !this.Session.ServiceRequestEventEnabled );
            }
        }

        /// <summary> Starts awaiting poll reading task. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A Threading.Tasks.Task. </returns>
        public System.Threading.Tasks.Task StartAwaitingPollReadingTask( TimeSpan timeout )
        {
            return System.Threading.Tasks.Task.Factory.StartNew( () => {
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                while ( this.PollEnabled && this.PollAutoRead && string.IsNullOrWhiteSpace( this.PollReading ) && sw.ElapsedTicks < ticks )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    Core.ApplianceBase.DoEvents();
                }
            } );
        }

        /// <summary> Gets the poll timer enabled. </summary>
        /// <value> The poll timer enabled. </value>
        public bool PollTimerEnabled => this.PollTimerThis.Enabled;

        /// <summary> Starts awaiting poll timer disabled task. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A Threading.Tasks.Task. </returns>
        public System.Threading.Tasks.Task StartAwaitingPollTimerDisabledTask( TimeSpan timeout )
        {
            return System.Threading.Tasks.Task.Factory.StartNew( () => {
                long ticks = timeout.Ticks;
                var sw = Stopwatch.StartNew();
                while ( this.PollTimerEnabled && sw.ElapsedTicks < ticks )
                {
                    System.Threading.Thread.SpinWait( 1 );
                    Core.ApplianceBase.DoEvents();
                }
            } );
        }
    }
}
