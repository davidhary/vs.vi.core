using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI
{

    /// <summary> A session factory. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-11-29 </para>
    /// </remarks>
    public partial class SessionFactory : Core.Models.SelectorViewModel
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        public SessionFactory() : base()
        {
            this.NewThis();
        }

        /// <summary> Constructor. </summary>
        /// <param name="talker"> The talker. </param>
        public SessionFactory( Core.ITraceMessageTalker talker ) : base( talker )
        {
            this.NewThis();
        }

        /// <summary> Common constructors. </summary>
        private void NewThis()
        {
            this.Searchable = true;
            this.PingFilterEnabled = false;
            this.ApplySessionFactory();
        }

        /// <summary>
        /// The locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        private static readonly object SyncLocker = new();

        /// <summary>
        /// The shared instance.
        /// </summary>
        private static SessionFactory _Instance;

        /// <summary> Returns a new or existing instance of this class. </summary>
        /// <remarks>
        /// Use this property to get an instance of this class. Returns the default instance of this form
        /// allowing to use this form as a singleton for.
        /// </remarks>
        /// <value> <c>A</c> new or existing instance of the class. </value>
        public static SessionFactory Get
        {
            get {
                if ( _Instance is null )
                {
                    lock ( SyncLocker )
                    {
                        _Instance = new SessionFactory();
                        _Instance.ApplySessionFactory();
                    }
                }

                return _Instance;
            }
        }

        /// <summary> Gets or sets the factory. </summary>
        /// <value> The factory. </value>
        public Pith.SessionFactoryBase Factory { get; set; }

        #endregion

        #region " RESOURE MANAGER "

        /// <summary> A filter specifying the resources. </summary>
        private string _ResourcesFilter;

        /// <summary> Gets or sets the resources search pattern. </summary>
        /// <value> The resources search pattern. </value>
        public string ResourcesFilter
        {
            get {
                if ( string.IsNullOrEmpty( this._ResourcesFilter ) )
                {
                    this._ResourcesFilter = Get.Factory.ResourcesProvider().ResourceFinder.BuildMinimalResourcesFilter();
                }

                return this._ResourcesFilter;
            }

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourcesFilter, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ResourcesFilter = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True to enable, false to disable the ping filter. </summary>
        private bool _PingFilterEnabled;

        /// <summary>
        /// Gets or sets the Ping Filter enabled sentinel. When enabled, Tcp/IP resources are added only
        /// if they can be pinged.
        /// </summary>
        /// <value> The ping filter enabled. </value>
        public bool PingFilterEnabled
        {
            get => this._PingFilterEnabled;

            set {
                if ( value != this.PingFilterEnabled )
                {
                    this._PingFilterEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Enumerate resource names. </summary>
        /// <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
        /// <returns> A list of <see cref="T:System.String" />. </returns>
        public override BindingList<string> EnumerateResources( bool applyEnabledFilters )
        {
            IEnumerable<string> resources = Array.Empty<string>();
            using ( var rm = this.Factory.ResourcesProvider() )
            {
                resources = string.IsNullOrWhiteSpace( this.ResourcesFilter ) ? rm.FindResources() : rm.FindResources( this.ResourcesFilter );
            }
            // TO_DO: ignore ping option at this time since this is the only filter we use.
            // If applyEnabledFilters AndAlso Me.PingFilterEnabled Then resources = VI.Pith.ResourceNamesManager.PingFilter(resources)
            if ( applyEnabledFilters )
                resources = Pith.ResourceNamesManager.PingFilter( resources );
            return this.EnumerateResources( resources );
        }

        /// <summary> Enumerates the default resource name patterns in this collection. </summary>
        /// <returns>
        /// An enumerator that allows for each to be used to process the default resource name patters in
        /// this collection.
        /// </returns>
        public static BindingList<string> EnumerateDefaultResourceNamePatterns()
        {
            return new BindingList<string>() { "GPIB[board]::number[::INSTR]", "GPIB[board]::INTFC", "TCPIP[board]::host address[::LAN device name][::INSTR]", "TCPIP[board]::host address::port::SOCKET" };
        }

        /// <summary> Validates the functional visa versions. </summary>
        /// <remarks> David, 2020-04-16. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ValidateFunctionalVisaVersions()
        {
            using var rm = Get.Factory.ResourcesProvider();
            return rm.ValidateFunctionalVisaVersions();
        }

        #endregion

        #region " RESOURCE SELECTION "

        /// <summary> Queries if a given check resource exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public override bool QueryResourceExists( string resourceName )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
                throw new ArgumentNullException( nameof( resourceName ) );
            using var rm = this.Factory.ResourcesProvider();
            return rm.Exists( resourceName );
        }

        /// <summary> Attempts to select resource from the given data. </summary>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override (bool Success, string Details) TryValidateResource( string resourceName )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
                resourceName = string.Empty;
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                this.CandidateResourceName = resourceName;
                activity = "checking if need to select the resource";
                if ( string.IsNullOrWhiteSpace( resourceName ) )
                {
                    activity = "attempted to select an empty resource name";
                    result = (true, activity);
                }
                else if ( string.Equals( resourceName, this.ValidatedResourceName, StringComparison.OrdinalIgnoreCase ) )
                {
                    activity = $"resource {resourceName} already validated";
                    result = (true, activity);
                }
                else if ( this.ValidationEnabled )
                {
                    activity = $"finding resource {resourceName}";
                    if ( this.QueryResourceExists( resourceName ) )
                    {
                        activity = $"setting validated resource name to {resourceName}";
                        this.ValidatedResourceName = resourceName;
                    }
                    else
                    {
                        activity = $"resource {resourceName} not found; clearing validated resource name";
                        this.ValidatedResourceName = string.Empty;
                    }
                }
                else
                {
                    activity = $"validation disabled--setting validated resource name to {resourceName}";
                    this.ValidatedResourceName = resourceName;
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }
            finally
            {
            }

            return result;
        }

        #endregion

        #region " I TALKER "

        /// <summary> Identifies talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        #endregion

        #region " TALKER PUBLISH "

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
