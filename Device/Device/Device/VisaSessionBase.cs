using System;
using System.Diagnostics;
using System.Linq;

using isr.Core;
using isr.VI.ExceptionExtensions;
using isr.VI.Pith;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by session opening classes. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-24, . from device base. </para>
    /// </remarks>
    public abstract partial class VisaSessionBase : IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <param name="session"> The session. </param>
        /// <param name="talker">  The talker. </param>
        protected VisaSessionBase( SessionBase session, TraceMessageTalker talker ) : base( talker )
        {
            this.NewThis( session, false );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <param name="session"> The session. </param>
        protected VisaSessionBase( SessionBase session ) : base()
        {
            this.NewThis( session, false );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected VisaSessionBase( StatusSubsystemBase statusSubsystem ) : base()
        {
            this.Subsystems = new SubsystemCollection() { statusSubsystem };
            this.StatusSubsystemBase = statusSubsystem;
            this.NewThis( StatusSubsystemBase.Validated( statusSubsystem ).Session, false );
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        /// <param name="talker">          The talker. </param>
        protected VisaSessionBase( StatusSubsystemBase statusSubsystem, TraceMessageTalker talker ) : base( talker )
        {
            this.Subsystems = new SubsystemCollection() { statusSubsystem };
            this.StatusSubsystemBase = statusSubsystem;
            this.NewThis( StatusSubsystemBase.Validated( statusSubsystem ).Session, false );
        }

        /// <summary> Initializes a new instance of the <see cref="VisaSessionBase" /> class. </summary>
        protected VisaSessionBase() : base()
        {
            this.Subsystems = new SubsystemCollection();
            this.NewThis( SessionFactory.Get.Factory.Session(), true );
        }

        /// <summary> Initializes a new instance of the <see cref="VisaSessionBase" /> class. </summary>
        /// <param name="session">        A reference to a <see cref="SessionBase">message based
        /// session</see>. </param>
        /// <param name="isSessionOwner"> true if this object is session owner. </param>
        private void NewThis( SessionBase session, bool isSessionOwner )
        {
            this.ElapsedTimeStopwatch = new Stopwatch();
            if ( session is null )
            {
                isSessionOwner = true;
                this.Assign_Session( SessionFactory.Get.Factory.Session(), isSessionOwner );
            }
            else
            {
                this.Assign_Session( session, isSessionOwner );
            }

            base.ResourceClosedCaption = Pith.My.MySettings.Default.ClosedCaption;
            base.CandidateResourceTitle = Pith.My.MySettings.Default.ResourceTitle;
            this._ServiceNotificationLevel = NotifySyncLevel.Sync;
            this.Subsystems.AssignTalker( this.Talker );
            this._SessionFactory = new SessionFactory( this.Talker ) {
                Searchable = true,
                PingFilterEnabled = false,
                ResourcesFilter = session.ResourcesFilter
            };
            this.PollTimerThis = new System.Timers.Timer() { Enabled = false, AutoReset = true, Interval = 1000d };
            this._PollEnabled = new Core.Concurrent.ConcurrentToken<bool>() { Value = false };
        }

        #region " I Disposable Support "

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the disposed status. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        if ( this._PollEnabled is object )
                        {
                            this._PollEnabled.Dispose();
                            this._PollEnabled = null;
                        }

                        if ( this.PollTimerThis is object )
                        {
                            this.PollTimerThis.Dispose();
                            this.PollTimerThis = null;
                        }
                        // release both managed and unmanaged resources
                        this.Session?.DisableServiceRequestEventHandler();
                        this.RemoveServiceRequestEventHandler();
                        this.RemoveServiceRequestedEventHandlers();
                        this.RemoveOpeningEventHandlers();
                        this.RemoveOpeningEventHandlers();
                        this.RemoveClosingEventHandlers();
                        this.RemoveClosedEventHandlers();
                        this.RemoveInitializingEventHandlers();
                        this.RemoveInitializingEventHandlers();
                        // this also removes the talker that was assigned to the subsystems.
                        this.AssignTalker( null );
                        if ( this.IsSessionOwner )
                        {
                            try
                            {
                                this.CloseSession();
                                this.Assign_Session( null, true );
                            }
                            catch ( ObjectDisposedException ex )
                            {
                                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                            }
                        }

                        this.StatusSubsystemBase = null;
                        this.Subsystems.Clear();
                    }
                    else
                    {
                        // release only unmanaged resources.
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary> Finalizes this object. </summary>
        /// <remarks>
        /// David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
        /// unmanaged resources.
        /// </remarks>
        ~VisaSessionBase()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Clears the interface if interface clear is supported for this resource. </summary>
        public void ClearInterface()
        {
            if ( this.Session.SupportsClearInterface )
            {
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} clearing interface" );
                this.Session.ClearInterface();
                // this will publish a report if device errors occurred
                _ = this.Session.ReadStatusRegister();
            }
        }

        /// <summary>
        /// Issues <see cref="VI.Pith.SessionBase.ClearActiveState()">Selective device clear (SDC)</see>. and
        /// applies default settings and clears the Device.
        /// </summary>
        public override void ClearActiveState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} clearing active state (SDC)" );
            this.Session.ClearActiveState( this.Session.DeviceClearRefractoryPeriod );
            // this will publish a report if device errors occurred
            _ = this.Session.ReadStatusRegister();
        }

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        public virtual void DefineClearExecutionState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} defining system clear execution state" );
            this.Subsystems.DefineClearExecutionState();
            // this will publish a report if device errors occurred
            _ = this.Session.ReadStatusRegister();
        }

        /// <summary>
        /// Clears the queues and resets all registers to zero. Sets system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks> *CLS. </remarks>
        public void ClearExecutionState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} clearing execution state" );
            this.Session.ClearExecutionState();
            this.DefineClearExecutionState();
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public void InitKnownState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} initializing known state" );
            if ( this.StatusSubsystemBase is object )
            {
                this.Subsystems.InitKnownState();
                this.IsInitialized = true;
                ApplianceBase.DoEventsWait( this.Session.StatusReadDelay );
                // this will publish a report if device errors occurred
                _ = this.Session.ReadStatusRegister();
            }

            this.IsInitialized = true;
        }

        /// <summary> Sets the system to its Preset known state. </summary>
        public void PresetKnownState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} presetting system known state" );
            this.Subsystems.PresetKnownState();
            ApplianceBase.DoEventsWait( this.Session.StatusReadDelay );
            // this will publish a report if device errors occurred
            _ = this.Session.ReadStatusRegister();
        }

        /// <summary> Defines the system and subsystems reset (RST) (default) known state. </summary>
        public virtual void DefineResetKnownState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} resetting subsystems known state" );
            this.Subsystems.DefineKnownResetState();
            ApplianceBase.DoEventsWait( this.Session.StatusReadDelay );
            // this will publish a report if device errors occurred
            _ = this.Session.ReadStatusRegister();
        }

        /// <summary>
        /// Resets the device to its known reset (RST) (default) state and defines the relevant system
        /// and subsystems state values.
        /// </summary>
        /// <remarks> *RST. </remarks>
        public virtual void ResetKnownState()
        {
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} resetting known state" );
            this.Session.ResetKnownState();
            this.DefineResetKnownState();
        }

        /// <summary>
        /// Resets, clears and initializes the device. Starts with issuing a selective-device-clear,
        /// reset (RST), Clear Status (CLS, and clear error queue) and initialize.
        /// </summary>
        public void ResetClearInit()
        {

            // issues selective device clear.
            this.ClearActiveState();

            // reset device
            this.ResetKnownState();

            // Clear the device Status and set more defaults
            this.ClearExecutionState();

            // initialize the device must be done after clear (5892)
            this.InitKnownState();
        }

        /// <summary>
        /// Resets, clears and initializes the device. Starts with issuing a selective-device-clear,
        /// reset (RST), Clear Status (CLS, and clear error queue) and initialize.
        /// </summary>
        /// <param name="timeout"> The timeout to use. This allows using a longer timeout than the
        /// minimal timeout set for the session. Typically, a source meter
        /// requires a 5000 milliseconds timeout. </param>
        public void ResetClearInit( TimeSpan timeout )
        {
            try
            {
                this.Session.StoreCommunicationTimeout( timeout );
                this.ResetClearInit();
            }
            catch
            {
                throw;
            }
            finally
            {
                this.Session.RestoreCommunicationTimeout();
            }
        }

        #endregion

        #region " RESOURCE NAME INFO "

        /// <summary>
        /// Checks if the specified resource name exists. Use for checking if the instrument is turned on.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName">    Name of the resource. </param>
        /// <param name="resourcesFilter"> The resources search pattern. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public static bool Find( string resourceName, string resourcesFilter )
        {
            if ( string.IsNullOrWhiteSpace( resourceName ) )
                throw new ArgumentNullException( nameof( resourceName ) );
            using var rm = SessionFactory.Get.Factory.ResourcesProvider();
            return string.IsNullOrWhiteSpace( resourcesFilter ) ? rm.FindResources().ToArray().Contains( resourceName ) : rm.FindResources( resourcesFilter ).ToArray().Contains( resourceName );
        }

        /// <summary>
        /// Checks if the candidate resource name exists thus checking if the instrument is turned on.
        /// </summary>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool ValidateCandidateResourceName()
        {
            return this.Session.ValidateCandidateResourceName( SessionFactory.Get.Factory.ResourcesProvider() );
        }

        #endregion

        #region " SESSION "

        /// <summary> true if this object is session owner. </summary>
        /// <value> The is session owner. </value>
        private bool IsSessionOwner { get; set; }

        /// <summary> Gets the session. </summary>
        /// <value> The session. </value>
        public SessionBase Session { get; private set; }

        /// <summary>
        /// Constructor-safe session assignment. If session owner and session exists, must close first.
        /// </summary>
        /// <param name="session">        The value. </param>
        /// <param name="isSessionOwner"> true if this object is session owner. </param>
        private void Assign_Session( SessionBase session, bool isSessionOwner )
        {
            if ( this.Session is object )
            {
                this.Session.PropertyChanged -= this.SessionPropertyChanged;
                this.Session.DeviceErrorOccurred -= this.HandleDeviceErrorOccurred;
                ApplianceBase.DoEvents();
                if ( this.IsSessionOwner )
                {
                    this.Session.Dispose();
                    // release the session
                    // Trying to null the session raises an ObjectDisposedException 
                    // if session service request handler was not released. 
                    this.Session = null;
                }
            }

            this.IsSessionOwner = isSessionOwner;
            this.Session = session;
            if ( this.Session is object )
            {
                this.Session.DeviceErrorOccurred += this.HandleDeviceErrorOccurred;
                this.Session.PropertyChanged += this.SessionPropertyChanged;
            }
        }

        /// <summary> Handles the session property changed action. </summary>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SessionBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SessionBase.Enabled ):
                    {
                        this.NotifyPropertyChanged( nameof( this.Enabled ) );
                        ApplianceBase.DoEvents();
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} {this.Enabled.GetHashCode():enabled;enabled;disabled};. " );
                        ApplianceBase.DoEvents();
                        break;
                    }

                case nameof( SessionBase.CandidateResourceName ):
                    {
                        this.CandidateResourceName = sender.CandidateResourceName;
                        break;
                    }

                case nameof( SessionBase.CandidateResourceTitle ):
                    {
                        this.CandidateResourceTitle = sender.CandidateResourceTitle;
                        break;
                    }

                case nameof( SessionBase.OpenResourceTitle ):
                    {
                        this.OpenResourceTitle = sender.OpenResourceTitle;
                        break;
                    }

                case nameof( SessionBase.OpenResourceName ):
                    {
                        this.OpenResourceName = sender.OpenResourceName;
                        break;
                    }

                case nameof( SessionBase.ResourceTitleCaption ):
                    {
                        this.ResourceTitleCaption = sender.ResourceTitleCaption;
                        break;
                    }

                case nameof( SessionBase.ResourceNameCaption ):
                    {
                        this.ResourceNameCaption = sender.ResourceNameCaption;
                        break;
                    }

                case nameof( SessionBase.ServiceRequestEventEnabled ):
                    {
                        if ( sender.ServiceRequestEventEnabled )
                        {
                            this.PollEnabled = false;
                        }

                        break;
                    }

                case nameof( SessionBase.ResourcesFilter ):
                    {
                        this.ResourcesFilter = sender.ResourcesFilter;
                        break;
                    }
            }
        }

        /// <summary> Handles the Session property changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            string activity = $"handling {nameof( SessionBase )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as SessionBase, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Gets or sets the Enabled sentinel of the device. A device is enabled when hardware can be
        /// used.
        /// </summary>
        /// <value> <c>True</c> if hardware device is enabled; <c>False</c> otherwise. </value>
        public virtual bool Enabled
        {
            get => this.Session.Enabled;

            set => this.Session.Enabled = value;
        }

        /// <summary> The last compound error. </summary>
        private string _LastCompoundError;

        /// <summary> Gets or sets the last compound error. </summary>
        /// <value> The last compound error. </value>
        public string LastCompoundError
        {
            get => this._LastCompoundError;

            set {
                if ( !string.Equals( value, this.LastCompoundError, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LastCompoundError = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last error report. </summary>
        private string _LastErrorReport;

        /// <summary> Gets or sets the last error report. </summary>
        /// <value> The last error report. </value>
        public string LastErrorReport
        {
            get => this._LastErrorReport;

            set {
                if ( !string.Equals( value, this.LastErrorReport, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LastErrorReport = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Handles the <see cref="isr.VI.Pith.SessionBase.DeviceErrorOccurred"/> event.
        /// </summary>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="e">         Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void HandleErrorOccurred( SessionBase subsystem, EventArgs e )
        {
            if ( subsystem is null || e is null )
                return;
            string activity = $"handling {nameof( SessionBase )}.{nameof( SessionBase.DeviceErrorOccurred )} event";
            try
            {
                if ( this.StatusSubsystemBase is null )
                {
                    if ( this.Session.ErrorAvailable )
                    {
                        activity = $"report error bit";
                        this.LastCompoundError = $"Error bit {( int ) this.Session.ErrorAvailableBit:X2}";
                        _ = this.PublishWarning( $"{this.ResourceNameCaption} {this.LastCompoundError}" );
                    }
                }
                else
                {
                    // when this event is reported, the status subsystem has already fetched the error 
                    // and created a report.  This method saves this report. 
                    activity = $"updating the error report";
                    // the error gets published when the subsystem queries the device errors. 
                    this.LastErrorReport = this.StatusSubsystemBase.DeviceErrorReport;
                    activity = $"updating the compound error";
                    this.LastCompoundError = this.StatusSubsystemBase.CompoundErrorMessage;
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Handles the <see cref="isr.VI.Pith.SessionBase.DeviceErrorOccurred"/> event.
        /// </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceErrorOccurred( object sender, EventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SessionBase )}.{nameof( SessionBase.DeviceErrorOccurred )} event";
            try
            {
                this.HandleErrorOccurred( sender as SessionBase, e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #region " SESSION INITIALIZATION PROPERTIES "

        /// <summary>
        /// Gets or sets the bits that would be set for detecting if an error is available.
        /// </summary>
        /// <value> The error available bits. </value>
        protected virtual ServiceRequests ErrorAvailableBits { get; set; } = ServiceRequests.ErrorAvailable;

        /// <summary> Gets or sets the keep alive interval. </summary>
        /// <remarks> Required only with VISA Non-Standard. </remarks>
        /// <value> The keep alive interval. </value>
        protected virtual TimeSpan KeepAliveInterval { get; set; } = TimeSpan.Zero; // TimeSpan.FromSeconds(58)

        /// <summary> Gets or sets the is alive command. </summary>
        /// <remarks> Required only with VISA Non-Standard. </remarks>
        /// <value> The is alive command. </value>
        protected virtual string IsAliveCommand { get; set; } = string.Empty; // "*OPC"

        /// <summary> Gets or sets the is alive query command. </summary>
        /// <remarks> Required only with VISA Non-Standard. </remarks>
        /// <value> The is alive query command. </value>
        protected virtual string IsAliveQueryCommand { get; set; } = string.Empty; // "*OPC?"

        #endregion

        #endregion

        #region " SERVICE REQUEST MANAGEMENT "

        /// <summary> The service notification level. </summary>
        private NotifySyncLevel _ServiceNotificationLevel;

        /// <summary> Gets or sets the service request notification level. </summary>
        /// <value> The service request notification level. </value>
        public NotifySyncLevel ServiceNotificationLevel
        {
            get => this._ServiceNotificationLevel;

            set {
                if ( value != this.ServiceNotificationLevel )
                {
                    this._ServiceNotificationLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the service request enabled bitmask. </summary>
        /// <value> The service request enabled bitmask. </value>
        public ServiceRequests ServiceRequestEnabledBitmask => this.IsDeviceOpen ? this.Session.ServiceRequestEnabledBitmask.GetValueOrDefault( ServiceRequests.None ) : ServiceRequests.None;

        /// <summary> Gets the session service request event enabled sentinel. </summary>
        /// <value> <c>True</c> if session service request event is enabled. </value>
        public bool ServiceRequestEventEnabled => this.IsSessionOpen && this.Session.ServiceRequestEventEnabled;

        /// <summary> True if service request handler assigned. </summary>
        private bool _ServiceRequestHandlerAssigned;

        /// <summary>
        /// Gets or sets an indication if an handler was assigned to the service request event.
        /// </summary>
        /// <value> <c>True</c> if a handler was assigned to the service request event. </value>
        public bool ServiceRequestHandlerAssigned
        {
            get => this._ServiceRequestHandlerAssigned;

            protected set {
                this._ServiceRequestHandlerAssigned = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Registers the device service request handler. </summary>
        /// <remarks>
        /// The Session service request handler must be registered and enabled before registering the
        /// device event handler.
        /// </remarks>
        public void AddServiceRequestEventHandler()
        {
            if ( !this.ServiceRequestHandlerAssigned )
            {
                this.Session.ServiceRequested += this.SessionBaseServiceRequested;
                this.ServiceRequestHandlerAssigned = true;
            }
        }

        /// <summary> Removes the device service request event handler. </summary>
        public void RemoveServiceRequestEventHandler()
        {
            if ( this.ServiceRequestHandlerAssigned && this.Session is object )
            {
                this.Session.ServiceRequested -= this.SessionBaseServiceRequested;
            }

            this.ServiceRequestHandlerAssigned = false;
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Enumerates the Presettable subsystems. </summary>
        /// <value> The subsystems. </value>
        public SubsystemCollection Subsystems { get; private set; }

        #endregion

        #region " STATUS SUBSYSTEM BASE "

        /// <summary> Gets or sets the Status SubsystemBase. </summary>
        /// <value> The Status SubsystemBase. </value>
        public StatusSubsystemBase StatusSubsystemBase { get; private set; }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Applies the settings. </summary>
        protected abstract void ApplySettings();

        #endregion

    }
}
