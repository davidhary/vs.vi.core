
namespace isr.VI
{
    public partial class SessionFactory
    {

        /// <summary> Applies the session factory. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        public void ApplySessionFactory()
        {
            this.Factory = new Foundation.SessionFactory();
            // Me.Factory = New isr.VI.National.Visa.SessionFactory
        }
    }
}
