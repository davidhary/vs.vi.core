using System.Diagnostics;

using isr.Core;

namespace isr.VI
{
    public partial class VisaSessionBase
    {

        /// <summary> Assigns a talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( ITraceMessageTalker talker )
        {
            base.AssignTalker( talker );
        }

        /// <summary> Identifies talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Removes the listener described by listener. </summary>
        /// <param name="listener"> The listener. </param>
        public override void RemoveListener( IMessageListener listener )
        {
            base.RemoveListener( listener );
        }

        /// <summary> Adds a listener. </summary>
        /// <param name="listener"> The listener. </param>
        public override void AddListener( IMessageListener listener )
        {
            base.AddListener( listener );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            base.ApplyListenerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            base.ApplyTalkerTraceLevel( listenerType, value );
            if ( listenerType == ListenerType.Logger )
            {
                this.NotifyPropertyChanged( nameof( this.TraceLogLevel ) );
            }
            else
            {
                this.NotifyPropertyChanged( nameof( this.TraceShowLevel ) );
            }
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            base.ApplyTalkerTraceLevels( talker );
        }

        /// <summary> Applies the talker listeners trace levels described by talker. </summary>
        /// <param name="talker"> The talker. </param>
        public override void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            base.ApplyListenerTraceLevels( talker );
        }

    }
}
