using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI
{

    /// <summary> Encapsulates handling a device reported error. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class DeviceError
    {

        #region " CONSTRUCTOR "

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceError" /> class specifying no error.
        /// </summary>
        public DeviceError() : this( Pith.Scpi.Syntax.NoErrorCompoundMessage )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceError" /> class specifying no error.
        /// </summary>
        /// <param name="noErrorCompoundMessage"> Message describing the no error compound. </param>
        public DeviceError( string noErrorCompoundMessage ) : base()
        {
            this.NoErrorCompoundMessage = noErrorCompoundMessage;
            this.CompoundErrorMessage = noErrorCompoundMessage;
            this.ErrorNumber = 0;
            this._ErrorLevel = 0;
            this.ErrorMessage = Pith.Scpi.Syntax.NoErrorMessage;
            this.Severity = TraceEventType.Verbose;
            this._Timestamp = DateTimeOffset.Now;
        }

        /// <summary> Initializes a new instance of the <see cref="DeviceError" /> class. </summary>
        /// <param name="value"> The value. </param>
        public DeviceError( DeviceError value ) : base()
        {
            if ( value is null )
            {
                this.NoErrorCompoundMessage = Pith.Scpi.Syntax.NoErrorCompoundMessage;
                this.CompoundErrorMessage = Pith.Scpi.Syntax.NoErrorCompoundMessage;
                this.ErrorMessage = Pith.Scpi.Syntax.NoErrorMessage;
                this.ErrorNumber = 0;
            }
            else
            {
                this.NoErrorCompoundMessage = value.NoErrorCompoundMessage;
                this.CompoundErrorMessage = value.CompoundErrorMessage;
                this.ErrorMessage = value.ErrorMessage;
                this.ErrorNumber = value.ErrorNumber;
                this._ErrorLevel = value.ErrorLevel;
                this._Timestamp = value.Timestamp;
            }
        }

        /// <summary> Gets the no error. </summary>
        /// <value> The no error. </value>
        public static DeviceError NoError => new( Pith.Scpi.Syntax.NoErrorCompoundMessage );

        #endregion

        #region " PARSE "

        /// <summary> True if error level parsed. </summary>
        private bool _ErrorLevelParsed;

        /// <summary> Parses the error message. </summary>
        /// <remarks>
        /// TSP2 error: -285,TSP Syntax error at line 1: unexpected symbol near `*',level=1 TSP error: -
        /// 285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20 SCPI Error: -113,
        /// "Undefined header;1;2018/05/26 14:00:14.871".
        /// </remarks>
        /// <param name="compoundError"> The compound error. </param>
        public virtual void Parse( string compoundError )
        {
            if ( string.IsNullOrWhiteSpace( compoundError ) )
            {
                this.CompoundErrorMessage = string.Empty;
                this.ErrorNumber = 0;
                this.ErrorMessage = string.Empty;
                this.ErrorLevel = 0;
                this.Severity = TraceEventType.Verbose;
                this.Timestamp = DateTimeOffset.Now;
            }
            else
            {
                this.CompoundErrorMessage = compoundError;
                var parts = new Queue<string>( compoundError.Split( ',' ) );
                bool localTryParse1()
                {
                    _ = this.ErrorNumber;
                    var ret = int.TryParse( compoundError, out int argresult ); this.ErrorNumber = argresult; return ret;
                }

                if ( parts.Any() )
                {
                    bool localTryParse()
                    {
                        _ = this.ErrorNumber;
                        var ret = int.TryParse( parts.Dequeue(), out int argresult ); this.ErrorNumber = argresult; return ret;
                    }

                    if ( localTryParse() )
                    {
                        this.Severity = this.ErrorNumber < 0 ? TraceEventType.Error : this.ErrorNumber > 0 ? TraceEventType.Warning : TraceEventType.Verbose;
                    }
                    else
                    {
                        this.ErrorNumber = int.MinValue;
                        this.Severity = TraceEventType.Error;
                    }

                    if ( parts.Any() )
                    {
                        this.ErrorMessage = parts.Dequeue().Trim().Trim( '"' ).Trim();
                        if ( parts.Any() )
                        {
                            this.ParseErrorLevel( parts.Dequeue() );
                        }
                    }
                    else
                    {
                        this.ErrorMessage = string.Empty;
                    }
                }
                else if ( localTryParse1() )
                {
                    this.ErrorMessage = compoundError;
                }
                else
                {
                    this.ErrorNumber = 0;
                    this.ErrorMessage = compoundError;
                }

                this.ParseErrorMessage( this.ErrorMessage );
            }
        }

        /// <summary> Parse error level. </summary>
        /// <param name="message"> The message. </param>
        private void ParseErrorLevel( string message )
        {
            this._ErrorLevelParsed = false;
            char levelDelimiter = '=';
            string levelPrefix = "level";
            var parts = new Queue<string>( message.Split( levelDelimiter ) );
            if ( parts.Any() )
            {
                if ( string.Equals( levelPrefix, parts.Dequeue() ) && parts.Any() )
                {
                    bool localTryParse()
                    {
                        _ = this.ErrorLevel;
                        var ret = int.TryParse( parts.Dequeue(), out int argresult ); this.ErrorLevel = argresult; return ret;
                    }

                    if ( localTryParse() )
                    {
                        this._ErrorLevelParsed = true;
                    }
                    else
                    {
                        this.ErrorLevel = 0;
                    }
                }
            }
            else
            {
                this.ErrorLevel = 0;
            }
        }

        /// <summary> Parse error message. </summary>
        /// <param name="message"> The message. </param>
        private void ParseErrorMessage( string message )
        {
            char errorDelimiter = ';';
            if ( !string.IsNullOrWhiteSpace( message ) && message.Contains( Conversions.ToString( errorDelimiter ) ) )
            {
                var parts = new Queue<string>( message.Split( errorDelimiter ) );
                if ( parts.Any() )
                    this.ErrorMessage = parts.Dequeue();
                if ( parts.Any() && int.TryParse( parts.Dequeue(), out int level ) && !this._ErrorLevelParsed )
                {
                    this.ErrorLevel = level;
                }

                bool localTryParse()
                {
                    _ = this.Timestamp;
                    var ret = DateTimeOffset.TryParse( parts.Dequeue(), out DateTimeOffset argresult ); this.Timestamp = argresult; return ret;
                }

                if ( !(parts.Any() && localTryParse()) )
                {
                    this.Timestamp = DateTimeOffset.Now;
                }
            }
        }

        #endregion

        #region " ERROR INFO "

        /// <summary> Builds error message. </summary>
        /// <returns> A String. </returns>
        public virtual string BuildErrorMessage()
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0},{1}", this.ErrorNumber, this.ErrorMessage );
        }

        /// <summary> Gets or sets a message describing the no error compound message. </summary>
        /// <value> A message describing the no error compound. </value>
        public string NoErrorCompoundMessage { get; protected set; }

        /// <summary> Gets a value indicating whether the error number represent and error. </summary>
        /// <value> The is error. </value>
        public bool IsError => this.ErrorNumber != 0;

        /// <summary> The error Level. </summary>
        private int _ErrorLevel;

        /// <summary> Gets or sets (protected) the error Level. </summary>
        /// <value> The error Level. </value>
        public virtual int ErrorLevel
        {
            get => this._ErrorLevel;

            protected set => this._ErrorLevel = value;
        }

        /// <summary> Gets or sets (protected) the error number. </summary>
        /// <value> The error number. </value>
        public int ErrorNumber { get; protected set; }

        /// <summary> Gets or sets (protected) the error message. </summary>
        /// <value> A message describing the error. </value>
        public string ErrorMessage { get; protected set; }

        /// <summary> Gets or sets (protected) the compound error message. </summary>
        /// <value> A message describing the compound error. </value>
        public string CompoundErrorMessage { get; protected set; }

        /// <summary> Gets or sets the severity. </summary>
        /// <value> The severity. </value>
        public TraceEventType Severity { get; protected set; }

        /// <summary> Returns a string that represents the current object. </summary>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return this.CompoundErrorMessage;
        }

        /// <summary> The error Level. </summary>
        private DateTimeOffset _Timestamp;

        /// <summary> Gets or sets the timestamp. </summary>
        /// <value> The timestamp. </value>
        public DateTimeOffset Timestamp
        {
            get => this._Timestamp;

            protected set => this._Timestamp = value;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Indicates whether the current <see cref="T:DeviceError"></see> value is equal to a specified
        /// object.
        /// </summary>
        /// <param name="obj"> An object. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as DeviceError );
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:DeviceError"></see> value is equal to a specified
        /// object.
        /// </summary>
        /// <remarks>
        /// The two Parameters are the same if they have the same actual and cached values.
        /// </remarks>
        /// <param name="value"> The value to compare. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:DeviceError"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( DeviceError value )
        {
            return value is object && (this.CompoundErrorMessage ?? "") == (value.CompoundErrorMessage ?? "");
        }

        /// <summary> Returns a hash code for this instance. </summary>
        /// <returns> A hash code for this object. </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary> Implements the operator =. </summary>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( DeviceError left, DeviceError right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( DeviceError left, DeviceError right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        #endregion

    }

    /// <summary> Queue of device errors. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-12 </para>
    /// </remarks>
    public class DeviceErrorQueue : Queue<DeviceError>
    {

        /// <summary> Constructor. </summary>
        /// <param name="noErrorCompoundMessage"> A message describing the empty error message. </param>
        public DeviceErrorQueue( string noErrorCompoundMessage ) : base()
        {
            this.NoErrorCompoundMessage = noErrorCompoundMessage;
        }

        /// <summary> Gets a message describing the no error compound message. </summary>
        /// <value> A message describing the no error compound. </value>
        public string NoErrorCompoundMessage { get; private set; }

        /// <summary> Gets the last error. </summary>
        /// <value> The last error. </value>
        public DeviceError LastError => this.Count == 0 ? new DeviceError( this.NoErrorCompoundMessage ) : this.ElementAtOrDefault( this.Count - 1 );
    }
}
