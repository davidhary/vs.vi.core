using System;

namespace isr.VI
{

    /// <summary> Implements a reading value. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-11-01 </para>
    /// </remarks>
    public class ReadingValue : ReadingEntity
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructs a measured value without specifying the value or its validity, which must be
        /// specified for the value to be made valid.
        /// </summary>
        /// <param name="readingType"> Type of the reading. </param>
        public ReadingValue( ReadingElementTypes readingType ) : base( readingType )
        {
            this.Generator = new RandomNumberGenerator();
        }

        /// <summary> Constructs a copy of an existing value. </summary>
        /// <param name="model"> The model. </param>
        public ReadingValue( ReadingValue model ) : base( model )
        {
            if ( model is object )
            {
                this.Value = model.Value;
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ReadingValue left, ReadingValue right )
        {
            return left is null ? right is null : right is not null && Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ReadingValue left, ReadingValue right )
        {
            return !Equals( left, right );
        }

        /// <summary> Returns True if equal. </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals. </returns>
        public static bool Equals( ReadingValue left, ReadingValue right )
        {
            return left is null
                ? right is null
                : right is not null
&& Nullable.Equals( left.Value, right.Value ) && string.Equals( left.RawValueReading, right.RawValueReading ) && string.Equals( left.RawUnitsReading, right.RawUnitsReading );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as ReadingValue );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="other"> The other <see cref="ReadingValue">Range</see> to compare for equality
        /// with this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( ReadingValue other )
        {
            return other is object && Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <returns> An <see cref="System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Value.GetHashCode();
        }

        #endregion

        #region " VALUE "

        /// <summary> Gets the value. </summary>
        /// <value> The value. </value>
        public double? Value { get; set; }

        /// <summary> Resets value to nothing. </summary>
        public override void Reset()
        {
            base.Reset();
            this.Value = new double?();
        }

        /// <summary>
        /// Applies the reading to create the specific reading type in the inherited class.
        /// </summary>
        /// <param name="rawValueReading"> The raw value reading. </param>
        /// <param name="rawUnitsReading"> The raw units reading. </param>
        /// <returns> <c>True</c> if parsed. </returns>
        public override bool TryApplyReading( string rawValueReading, string rawUnitsReading )
        {
            if ( base.TryApplyReading( rawValueReading, rawUnitsReading ) )
            {
                // convert reading to numeric
                return this.TryApplyReading( rawValueReading );
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Applies the reading to create the specific reading type in the inherited class.
        /// </summary>
        /// <param name="valueReading"> The value reading. </param>
        /// <returns> <c>True</c> if parsed. </returns>
        public override bool TryApplyReading( string valueReading )
        {
            if ( base.TryApplyReading( valueReading ) )
            {
                // convert reading to numeric
                if ( double.TryParse( valueReading, System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent, System.Globalization.CultureInfo.CurrentCulture, out double value ) )
                {
                    this.Value = value;
                    return true;
                }
                else if ( double.TryParse( valueReading, out value ) )
                {
                    this.Value = value;
                    return true;
                }
                else
                {
                    this.Value = Pith.Scpi.Syntax.NotANumber;
                    return false;
                }
            }
            else
            {
                this.Value = new double?();
                return false;
            }
        }

        #endregion

        #region " TO STRING "

        /// <summary> Returns a string that represents the current object. </summary>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return this.Value.HasValue ? this.Value.Value.ToString() : this.RawValueReading;
        }

        #endregion

        #region " SIMULATION "

        /// <summary> Gets the generator. </summary>
        /// <value> The generator. </value>
        public RandomNumberGenerator Generator { get; private set; }

        /// <summary> Holds the simulated value. </summary>
        /// <value> The simulated value. </value>
        public double SimulatedValue => this.Generator.Value;

        #endregion

    }
}
