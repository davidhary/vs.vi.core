using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI
{

    /// <summary> Provides an contract for a Node Entity. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-03-02, 3.0.3348.x. </para>
    /// </remarks>
    public abstract class NodeEntityBase
    {

        #region " CONSTRUCTION "

        /// <summary> Constructs the class. </summary>
        /// <param name="number">               Specifies the node number. </param>
        /// <param name="controllerNodeNumber"> The controller node number. </param>
        protected NodeEntityBase( int number, int controllerNodeNumber ) : base()
        {
            this.ControllerNodeNumber = controllerNodeNumber;
            this.Number = number;
            this.UniqueKey = BuildKey( number );
            this.ModelNumber = string.Empty;
            this.InstrumentModelFamily = InstrumentModelFamily.None;
            this.FirmwareVersion = string.Empty;
            this.SerialNumber = string.Empty;
        }

        /// <summary> Builds a key. </summary>
        /// <param name="nodeNumber"> The node number. </param>
        /// <returns> The unique value for the node number. </returns>
        public static string BuildKey( int nodeNumber )
        {
            return nodeNumber.ToString();
        }

        #endregion

        #region " INIT "

        /// <summary> Initializes the node properties . </summary>
        /// <exception cref="FormatException"> Thrown when the format of the received message is
        /// incorrect. </exception>
        /// <param name="session"> The session. </param>
        public void InitializeKnownState( Pith.SessionBase session )
        {
            this.QueryModelNumber( session );
            if ( this.InstrumentModelFamily == InstrumentModelFamily.None )
            {
                throw new FormatException( $"Node #{this.Number} instrument model {this.ModelNumber} has no defined instrument family;. " );
            }

            this.QueryFirmwareVersion( session );
            this.QuerySerialNumber( session );
        }

        #endregion

        #region " NODE INFO "

        /// <summary> Queries if a given node exists. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">    The session. </param>
        /// <param name="nodeNumber"> The node number. </param>
        /// <returns> <c>True</c> is node exists; otherwise, <c>False</c>. </returns>
        public static bool NodeExists( Pith.SessionBase session, int nodeNumber )
        {
            return session is null ? throw new ArgumentNullException( nameof( session ) ) : !session.IsNil( $"node[{nodeNumber}]" );
        }

        /// <summary>
        /// Gets the condition to indicate that the boot script must be re-saved because a script
        /// reference changes as would happened if a new binary script was created with a table reference
        /// that differs from the table reference of the previous script that was used in the previous
        /// boot script.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The boot script save required. </value>
        public bool BootScriptSaveRequired { get; set; }

        /// <summary> Returns the controller node number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The controller node number. </value>
        public int ControllerNodeNumber { get; private set; }

        /// <summary> Gets the data queue capacity. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The data queue capacity. </value>
        public int? DataQueueCapacity { get; set; }

        /// <summary> Queries the data queue capacity. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        public void QueryDataQueueCapacity( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.DataQueueCapacity = session.QueryPrint( 0, 1, $"node[{this.Number}].dataqueue.capacity" );
        }

        /// <summary> Gets the data queue Count. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The data queue Count. </value>
        public int? DataQueueCount { get; set; }

        /// <summary> Queries the data queue Count. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        public void QueryDataQueueCount( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.DataQueueCount = session.QueryPrint( 0, 1, $"node[{this.Number}].dataqueue.count" );
        }

        /// <summary> Gets the firmware version. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The firmware version. </value>
        public string FirmwareVersion { get; set; }

        /// <summary> Queries firmware version. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        public void QueryFirmwareVersion( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.FirmwareVersion = session.QueryTrimEnd( $"_G.print(node[{this.Number}].revision)" );
        }

        /// <summary> Gets the <see cref="InstrumentModelFamily">instrument model family.</see> </summary>
        /// <value> The instrument model family. </value>
        public InstrumentModelFamily InstrumentModelFamily { get; private set; }

        /// <summary> Gets the is controller. </summary>
        /// <value> The is controller. </value>
        public bool IsController => this.Number == this.ControllerNodeNumber;

        /// <summary> Gets the model number. </summary>
        /// <value> The model number. </value>
        public string ModelNumber { get; private set; }

        /// <summary> Queries the serial number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">    The session. </param>
        /// <param name="nodeNumber"> The node number. </param>
        /// <returns> The model number. </returns>
        public static string QueryModelNumber( Pith.SessionBase session, int nodeNumber )
        {
            return session is null
                ? throw new ArgumentNullException( nameof( session ) )
                : session.QueryTrimEnd( $"_G.print(node[{nodeNumber}].model)" );
        }

        /// <summary> Queries controller node model. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        /// <returns> The controller node model. </returns>
        public static string QueryControllerNodeModel( Pith.SessionBase session )
        {
            return session is null ? throw new ArgumentNullException( nameof( session ) ) : session.QueryTrimEnd( "_G.print(_G.localnode.model)" );
        }

        /// <summary> Queries the serial number. </summary>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="session"> The session. </param>
        public void QueryModelNumber( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.ModelNumber = this.Number == this.ControllerNodeNumber ? QueryControllerNodeModel( session ) : QueryModelNumber( session, this.Number );
            if ( string.IsNullOrWhiteSpace( this.ModelNumber ) )
            {
                throw new Core.OperationFailedException( "Failed reading node model--empty." );
            }

            this.InstrumentModelFamily = ParseModelNumber( this.ModelNumber );
        }

        /// <summary> Gets or sets the node number. </summary>
        /// <value> The node number. </value>
        public int Number { get; private set; }

        /// <summary> Gets or sets the serial number. </summary>
        /// <value> The serial number. </value>
        public string SerialNumber { get; set; }

        /// <summary> Queries the serial number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        public void QuerySerialNumber( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.SerialNumber = session.QueryTrimEnd( $"_G.print(node[{this.Number}].serialno)" );
        }

        /// <summary> Gets or sets the unique key. </summary>
        /// <value> The unique key. </value>
        public string UniqueKey { get; private set; }

        #endregion

        #region " INSTRUMENT MODEL "

        /// <summary>
        /// Returns <c>True</c> if the <paramref name="model">model</paramref> matches the mask.
        /// </summary>
        /// <param name="model"> Actual mode. </param>
        /// <param name="mask">  Mode mask using '%' to signify ignored characters and * to specify
        /// wildcard suffix. </param>
        /// <returns>
        /// Returns <c>True</c> if the <paramref name="model">model</paramref> matches the mask.
        /// </returns>
        public static bool IsModelMatch( string model, string mask )
        {
            char wildcard = '*';
            char ignore = '%';
            if ( string.IsNullOrWhiteSpace( mask ) )
            {
                return true;
            }
            else if ( string.IsNullOrWhiteSpace( model ) )
            {
                return false;
            }
            else if ( mask.Contains( Conversions.ToString( wildcard ) ) )
            {
                int length = mask.IndexOf( wildcard );
                var m = mask.Substring( 0, length ).ToCharArray();
                var candidate = model.Substring( 0, length ).ToCharArray();
                for ( int i = 0, loopTo1 = m.Length - 1; i <= loopTo1; i++ )
                {
                    char c = m[i];
                    if ( c != ignore && c != candidate[i] )
                    {
                        return false;
                    }
                }
            }
            else if ( mask.Length != model.Length )
            {
                return false;
            }
            else
            {
                var m = mask.ToCharArray();
                var candidate = model.ToCharArray();
                for ( int i = 0, loopTo = m.Length - 1; i <= loopTo; i++ )
                {
                    char c = m[i];
                    if ( c != ignore && c != candidate[i] )
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary> Parses the model number to get the model family. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> A <see cref="InstrumentModelFamily">model family</see>. </returns>
        public static InstrumentModelFamily ParseModelNumber( string value )
        {
            foreach ( InstrumentModelFamily item in Enum.GetValues( typeof( InstrumentModelFamily ) ) )
            {
                if ( item != 0 && IsModelMatch( value, ModelFamilyMask( item ) ) )
                {
                    return item;
                }
            }

            return InstrumentModelFamily.None;
        }

        /// <summary> Returns the mask for the model family. </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> The mask for the model family. </returns>
        public static string ModelFamilyMask( InstrumentModelFamily value )
        {
            switch ( value )
            {
                case InstrumentModelFamily.K2600:
                    {
                        return "26%%";
                    }

                case InstrumentModelFamily.K2600A:
                    {
                        return "26%%%";
                    }

                case InstrumentModelFamily.K3700:
                    {
                        return "37*";
                    }

                case InstrumentModelFamily.K2450:
                    {
                        return "245%";
                    }

                case InstrumentModelFamily.K6500:
                    {
                        return "65%%";
                    }

                case InstrumentModelFamily.K7500:
                    {
                        return "75%%";
                    }

                default:
                    {
                        throw new ArgumentOutOfRangeException( nameof( value ), value, "Unhandled model family" );
                    }
            }
        }

        /// <summary> Model family resource file suffix. </summary>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> The suffix for the model family resource file name. </returns>
        public static string ModelFamilyResourceFileSuffix( InstrumentModelFamily value )
        {
            switch ( value )
            {
                case InstrumentModelFamily.K2600:
                    {
                        return ".2600";
                    }

                case InstrumentModelFamily.K2600A:
                    {
                        return ".2600A";
                    }

                case InstrumentModelFamily.K3700:
                    {
                        return ".3700";
                    }

                case InstrumentModelFamily.K2450:
                    {
                        return ".2450";
                    }

                case InstrumentModelFamily.K6500:
                    {
                        return ".6500";
                    }

                case InstrumentModelFamily.K7500:
                    {
                        return ".7500";
                    }

                default:
                    {
                        throw new ArgumentOutOfRangeException( nameof( value ), value, "Unhandled model family" );
                    }
            }
        }

        #endregion

    }

    /// <summary>
    /// A <see cref="System.Collections.ObjectModel.KeyedCollection{TKey, TItem}">collection</see> of
    /// <see cref="NodeEntityBase">Node entity</see>
    /// items keyed by the <see cref="NodeEntityBase.UniqueKey">unique key.</see>
    /// </summary>
    public class NodeEntityCollection : NodeEntityBaseCollection<NodeEntityBase>
    {

        /// <summary> Gets key for item. </summary>
        /// <param name="item"> The item. </param>
        /// <returns> The key for item. </returns>
        protected override string GetKeyForItem( NodeEntityBase item )
        {
            return base.GetKeyForItem( item );
        }
    }

    /// <summary>
    /// A <see cref="System.Collections.ObjectModel.KeyedCollection{TKey, TItem}">collection</see> of
    /// <see cref="NodeEntityBase">Node entity</see>
    /// items keyed by the <see cref="NodeEntityBase.UniqueKey">unique key.</see>
    /// </summary>
    public class NodeEntityBaseCollection<TItem> : System.Collections.ObjectModel.KeyedCollection<string, TItem> where TItem : NodeEntityBase
    {

        /// <summary> Gets key for item. </summary>
        /// <param name="item"> The item. </param>
        /// <returns> The key for item. </returns>
        protected override string GetKeyForItem( TItem item )
        {
            return item.UniqueKey;
        }
    }

    /// <summary> Enumerates the instrument model families. </summary>
    public enum InstrumentModelFamily
    {

        /// <summary>Not defined.</summary>
        [System.ComponentModel.Description( "Not defined" )]
        None = 0,

        /// <summary>26xx Source Meters.</summary>
        [System.ComponentModel.Description( "26xx Source Meters" )]
        K2600 = 1,

        /// <summary>26xxA Source Meters.</summary>
        [System.ComponentModel.Description( "26xxA Source Meters" )]
        K2600A = 2,

        /// <summary>37xx Switch Systems.</summary>
        [System.ComponentModel.Description( "37xx Switch Systems" )]
        K3700 = 3,

        /// <summary>24xx Source Meters.</summary>
        [System.ComponentModel.Description( "245x Source Meters" )]
        K2450 = 4,

        /// <summary>75xx meters.</summary>
        [System.ComponentModel.Description( "65xx Meters" )]
        K6500 = 5,

        /// <summary>75xx meters.</summary>
        [System.ComponentModel.Description( "75xx Meters" )]
        K7500 = 6
    }
}
