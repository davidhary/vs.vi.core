using System;
using System.Linq;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.VI
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> Information about the version of a TSP instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        public VersionInfo() : base()
        {
            this.ClearThis();
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        private void ClearThis()
        {
            this.FirmwareVersion = new Version();
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        public override void Clear()
        {
            base.Clear();
            this.ClearThis();
        }

        /// <summary> Gets or sets the firmware version. </summary>
        /// <value> The firmware version. </value>
        public Version FirmwareVersion { get; set; }

        /// <summary> Gets the numbers. </summary>
        /// <param name="input"> The input. </param>
        /// <returns> The numbers. </returns>
        private static string GetNumbers( string input )
        {
            return new string( input.Where( c => char.IsDigit( c ) ).ToArray() );
        }

        /// <summary> Gets not numbers. </summary>
        /// <param name="input"> The input. </param>
        /// <returns> The not numbers. </returns>
        private static string GetNotNumbers( string input )
        {
            return new string( input.Where( c => !char.IsDigit( c ) ).ToArray() );
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument revision
        /// e.g., <c>2.1.6</c>. The source meter identity includes no board
        /// specs. </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
                this.FirmwareVersion = new Version();
            }
            else
            {
                base.ParseFirmwareRevision( revision );
                _ = new Version();
                if ( !Version.TryParse( revision, out Version rev ) )
                {
                    _ = new Version();
                    // 3700 revision is 1.53c.
                    var values = revision.Split( '.' );
                    var builder = new System.Text.StringBuilder();
                    foreach ( string v in values )
                    {
                        string vv = GetNumbers( v );
                        if ( int.TryParse( vv, out int iv ) )
                        {
                            if ( builder.Length > 0 )
                                _ = builder.Append( "." );
                            _ = builder.Append( vv );
                        }

                        if ( Version.TryParse( builder.ToString(), out Version tempRev ) )
                        {
                            rev = Version.Parse( builder.ToString() );
                        }

                        vv = GetNotNumbers( v );
                        if ( !string.IsNullOrWhiteSpace( vv ) )
                        {
                            foreach ( char c in vv.ToUpperInvariant().ToCharArray() )
                            {
                                int value = Convert.ToInt16( c ) - Convert.ToInt16( 'A' ) + 1;
                                if ( builder.Length > 0 )
                                    _ = builder.Append( "." );
                                _ = builder.Append( value.ToString() );
                                if ( Version.TryParse( builder.ToString(), out tempRev ) )
                                {
                                    rev = Version.Parse( builder.ToString() );
                                }
                            }
                        }
                    }
                }

                this.FirmwareVersion = rev;
            }
        }
    }
}
