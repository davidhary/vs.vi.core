
namespace isr.VI
{

    /// <summary> Encapsulate the node information. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-03-02, 3.0.3348.x. </para>
    /// </remarks>
    public class NodeEntity : NodeEntityBase
    {

        /// <summary> Constructs the class. </summary>
        /// <param name="number">               Specifies the node number. </param>
        /// <param name="controllerNodeNumber"> The controller node number. </param>
        public NodeEntity( int number, int controllerNodeNumber ) : base( number, controllerNodeNumber )
        {
        }
    }
}
