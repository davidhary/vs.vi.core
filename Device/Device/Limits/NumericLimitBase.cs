namespace isr.VI
{

    /// <summary> Defines the SCPI numeric limit subsystem base. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para>
    /// David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
    /// </remarks>
    public abstract class NumericLimitBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="NumericLimitBase" /> class. </summary>
        /// <param name="limitNumber">     The limit number. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected NumericLimitBase( int limitNumber, StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.LimitNumber = limitNumber;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Enabled = false;
        }

        #endregion

        #region " NUMERIC COMMAND BUILDER "

        /// <summary> Gets or sets the limit number. </summary>
        /// <value> The limit number. </value>
        protected int LimitNumber { get; private set; }

        /// <summary> Builds a command. </summary>
        /// <param name="baseCommand"> The base command. </param>
        /// <returns> A String. </returns>
        protected string BuildCommand( string baseCommand )
        {
            return string.Format( System.Globalization.CultureInfo.InvariantCulture, baseCommand, this.LimitNumber );
        }

        #endregion

        #region " LIMIT FAILED "

        /// <summary> The failed. </summary>
        private bool? _Failed;

        /// <summary> Gets or sets the cached In Limit Failed Condition sentinel. </summary>
        /// <value>
        /// <c>null</c> if In Limit Failed Condition is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Failed
        {
            get => this._Failed;

            protected set {
                if ( !Equals( this.Failed, value ) )
                {
                    this._Failed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Limit Failed query command. </summary>
        /// <remarks> SCPI: ":CALC2:LIM[#]:FAIL?". </remarks>
        /// <value> The Limit Failed query command. </value>
        protected virtual string FailedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Delay Failed sentinel. Also sets the
        /// <see cref="Failed">Failed</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
        public bool? QueryFailed()
        {
            this.Failed = this.Query( this.Failed, this.BuildCommand( this.FailedQueryCommand ) );
            return this.Failed;
        }

        #endregion

        #region " LIMIT ENABLED "

        /// <summary> The enabled. </summary>
        private bool? _Enabled;

        /// <summary> Gets or sets the cached Limit Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Enabled
        {
            get => this._Enabled;

            protected set {
                if ( !Equals( this.Enabled, value ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyEnabled( bool value )
        {
            _ = this.WriteEnabled( value );
            return this.QueryEnabled();
        }

        /// <summary> Gets or sets the Limit enabled query command. </summary>
        /// <remarks> SCPI: "CALC2:LIM[#]:STAT?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string EnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Delay Enabled sentinel. Also sets the
        /// <see cref="Enabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryEnabled()
        {
            this.Enabled = this.Query( this.Enabled, this.BuildCommand( this.EnabledQueryCommand ) );
            return this.Enabled;
        }

        /// <summary> Gets or sets the Limit enabled command Format. </summary>
        /// <remarks> SCPI: "CALC2:LIM[#]:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string EnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteEnabled( bool value )
        {
            this.Enabled = this.Write( value, this.BuildCommand( this.EnabledCommandFormat ) );
            return this.Enabled;
        }

        #endregion

    }
}
