using System;

namespace isr.VI
{

    /// <summary> Defines the SCPI Composite Limit subsystem. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para>
    /// David, 2008-03-25, 5.0.3004 Port to new SCPI library. </para>
    /// </remarks>
    public abstract class CompositeLimitBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="CompositeLimitBase" /> class. </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected CompositeLimitBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.LimitMode = VI.LimitMode.Grading;
            this.BinningControl = VI.BinningControl.Immediate;
            this.FailureBits = 15;
            this.PassBits = 15;
            this.AutoClearEnabled = true;
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the composite limits clear command. </summary>
        /// <remarks> SCPI: ":CLAC2:CLIM:CLE". </remarks>
        /// <value> The composite limits clear command. </value>
        protected virtual string ClearCommand { get; set; }

        /// <summary>
        /// Clears composite limits. Returns the instrument output to the TTL settings per SOURC2:TTL.
        /// </summary>
        public void ClearLimits()
        {
            if ( !string.IsNullOrWhiteSpace( this.ClearCommand ) )
                this.Session.Execute( this.ClearCommand );
        }

        #endregion

        #region " AUTO CLEAR ENABLED "

        /// <summary> The automatic clear enabled. </summary>
        private bool? _AutoClearEnabled;

        /// <summary> Gets or sets the cached Composite Limits Auto Clear enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Composite Limits Auto Clear enabled is not known; <c>True</c> if output is on;
        /// otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoClearEnabled
        {
            get => this._AutoClearEnabled;

            protected set {
                if ( !Equals( this.AutoClearEnabled, value ) )
                {
                    this._AutoClearEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Composite Limits Auto Clear enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoClearEnabled( bool value )
        {
            _ = this.WriteAutoClearEnabled( value );
            return this.QueryAutoClearEnabled();
        }

        /// <summary> Gets or sets the Composite Limits Auto Clear enabled query command. </summary>
        /// <remarks> SCPI: ":CALC2:CLIM:CLE:AUTO?". </remarks>
        /// <value> The Composite Limits Auto Clear enabled query command. </value>
        protected virtual string AutoClearEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Delay Enabled sentinel. Also sets the
        /// <see cref="AutoClearEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoClearEnabled()
        {
            this.AutoClearEnabled = this.Query( this.AutoClearEnabled, this.AutoClearEnabledQueryCommand );
            return this.AutoClearEnabled;
        }

        /// <summary> Gets or sets the Composite Limits Auto Clear enabled command Format. </summary>
        /// <remarks> SCPI: ":CALC2:CLIM:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Composite Limits Auto Clear enabled query command. </value>
        protected virtual string AutoClearEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoClearEnabled( bool value )
        {
            this.AutoClearEnabled = this.Write( value, this.AutoClearEnabledCommandFormat );
            return this.AutoClearEnabled;
        }

        #endregion

        #region " FAILURE BITS "

        /// <summary> The failure bits. </summary>
        private int? _FailureBits;

        /// <summary> Gets or sets the cached Composite Limits Failure Bits. </summary>
        /// <value> The Composite Limits Failure Bits or none if not set or unknown. </value>
        public int? FailureBits
        {
            get => this._FailureBits;

            protected set {
                if ( !Nullable.Equals( this.FailureBits, value ) )
                {
                    this._FailureBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Composite Limits Failure Bits. </summary>
        /// <param name="value"> The current Composite Limits Failure Bits. </param>
        /// <returns> The Composite Limits Failure Bits or none if unknown. </returns>
        public int? ApplyFailureBits( int value )
        {
            _ = this.WriteFailureBits( value );
            return this.QueryFailureBits();
        }

        /// <summary> Gets or sets the Lower Limit failure Bits query command. </summary>
        /// <remarks> SCPI: ":CALC2:CLIM:FAIL:SOUR2?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string FailureBitsQueryCommand { get; set; }

        /// <summary> Queries the current Lower Limit Failure Bits. </summary>
        /// <returns> The Lower Limit Failure Bits or none if unknown. </returns>
        public int? QueryFailureBits()
        {
            this.FailureBits = this.Query( this.FailureBits, this.FailureBitsQueryCommand );
            return this.FailureBits;
        }

        /// <summary> Gets or sets the Lower Limit Failure Bits query command. </summary>
        /// <remarks> SCPI: "::CALC2:CLIM:FAIL:SOUR2 {0}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string FailureBitsCommandFormat { get; set; }

        /// <summary>
        /// Sets back the Lower Limit Failure Bits without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current Lower Limit Failure Bits. </param>
        /// <returns> The Lower Limit Failure Bits or none if unknown. </returns>
        public int? WriteFailureBits( int value )
        {
            this.FailureBits = this.Write( value, this.FailureBitsCommandFormat );
            return this.FailureBits;
        }

        #endregion

        #region " LIMITS PASS BITS "

        /// <summary> The pass bits. </summary>
        private int? _PassBits;

        /// <summary> Gets or sets the cached Composite Limits Pass Bits. </summary>
        /// <value> The Composite Limits Pass Bits or none if not set or unknown. </value>
        public int? PassBits
        {
            get => this._PassBits;

            protected set {
                if ( !Nullable.Equals( this.PassBits, value ) )
                {
                    this._PassBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Composite Limits Pass Bits. </summary>
        /// <param name="value"> The current Composite Limits Pass Bits. </param>
        /// <returns> The Composite Limits Pass Bits or none if unknown. </returns>
        public int? ApplyPassBits( int value )
        {
            _ = this.WritePassBits( value );
            return this.QueryPassBits();
        }

        /// <summary> Gets or sets the Lower Limit Pass Bits query command. </summary>
        /// <remarks> SCPI: ":CALC2:CLIM:PASS:SOUR2?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string PassBitsQueryCommand { get; set; }

        /// <summary> Queries the current Lower Limit Pass Bits. </summary>
        /// <returns> The Lower Limit Pass Bits or none if unknown. </returns>
        public int? QueryPassBits()
        {
            this.PassBits = this.Query( this.PassBits, this.PassBitsQueryCommand );
            return this.PassBits;
        }

        /// <summary> Gets or sets the Lower Limit Pass Bits query command. </summary>
        /// <remarks> SCPI: "::CALC2:CLIM:PASS:SOUR2 {0}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string PassBitsCommandFormat { get; set; }

        /// <summary>
        /// Sets back the Lower Limit Pass Bits without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current Lower Limit Pass Bits. </param>
        /// <returns> The Lower Limit Pass Bits or none if unknown. </returns>
        public int? WritePassBits( int value )
        {
            this.PassBits = this.Write( value, this.PassBitsCommandFormat );
            return this.PassBits;
        }

        #endregion

        #region " BINNING CONTROL "

        /// <summary> The Binning Control. </summary>
        private BinningControl? _BinningControl;

        /// <summary> Gets or sets the cached Binning Control. </summary>
        /// <value>
        /// The <see cref="BinningControl">Binning Control</see> or none if not set or unknown.
        /// </value>
        public BinningControl? BinningControl
        {
            get => this._BinningControl;

            protected set {
                if ( !this.BinningControl.Equals( value ) )
                {
                    this._BinningControl = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Binning Control. </summary>
        /// <param name="value"> The Binning Control. </param>
        /// <returns>
        /// The <see cref="BinningControl">source  Binning Control</see> or none if unknown.
        /// </returns>
        public BinningControl? ApplyBinningControl( BinningControl value )
        {
            _ = this.WriteBinningControl( value );
            return this.QueryBinningControl();
        }

        /// <summary> Gets or sets the Binning Control query command. </summary>
        /// <remarks> SCPI: ":CALC2:CLIM:BCON". </remarks>
        /// <value> The Binning Control query command. </value>
        protected virtual string BinningControlQueryCommand { get; set; }

        /// <summary> Queries the Binning Control. </summary>
        /// <returns> The <see cref="BinningControl"> Binning Control</see> or none if unknown. </returns>
        public BinningControl? QueryBinningControl()
        {
            this.BinningControl = this.Query( this.BinningControlQueryCommand, this.BinningControl );
            return this.BinningControl;
        }

        /// <summary> Gets or sets the Binning Control command format. </summary>
        /// <remarks> SCPI: ":CALC2:CLIM:BCON {0}". </remarks>
        /// <value> The Binning Control query command format. </value>
        protected virtual string BinningControlCommandFormat { get; set; }

        /// <summary> Writes the Binning Control without reading back the value from the device. </summary>
        /// <param name="value"> The Binning Control. </param>
        /// <returns> The <see cref="BinningControl"> Binning Control</see> or none if unknown. </returns>
        public BinningControl? WriteBinningControl( BinningControl value )
        {
            this.BinningControl = this.Write( this.BinningControlCommandFormat, value );
            return this.BinningControl;
        }

        #endregion

        #region " LIMIT MODE "

        /// <summary> The Limit Mode. </summary>
        private LimitMode? _LimitMode;

        /// <summary> Gets or sets the cached Limit Mode. </summary>
        /// <value> The <see cref="LimitMode">Limit Mode</see> or none if not set or unknown. </value>
        public LimitMode? LimitMode
        {
            get => this._LimitMode;

            protected set {
                if ( !this.LimitMode.Equals( value ) )
                {
                    this._LimitMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit Mode. </summary>
        /// <param name="value"> The Limit Mode. </param>
        /// <returns> The <see cref="LimitMode">source  Limit Mode</see> or none if unknown. </returns>
        public LimitMode? ApplyLimitMode( LimitMode value )
        {
            _ = this.WriteLimitMode( value );
            return this.QueryLimitMode();
        }

        /// <summary> Gets or sets the Limit Mode query command. </summary>
        /// <remarks> SCPI: "CALC2:CLIM:MODE". </remarks>
        /// <value> The Limit Mode query command. </value>
        protected virtual string LimitModeQueryCommand { get; set; }

        /// <summary> Queries the Limit Mode. </summary>
        /// <returns> The <see cref="LimitMode"> Limit Mode</see> or none if unknown. </returns>
        public LimitMode? QueryLimitMode()
        {
            this.LimitMode = this.Query( this.LimitModeQueryCommand, this.LimitMode );
            return this.LimitMode;
        }

        /// <summary> Gets or sets the Limit Mode command format. </summary>
        /// <remarks> SCPI: "CALC2:CLIM:MODE". </remarks>
        /// <value> The Limit Mode command format. </value>
        protected virtual string LimitModeCommandFormat { get; set; }

        /// <summary> Writes the Limit Mode without reading back the value from the device. </summary>
        /// <param name="value"> The Limit Mode. </param>
        /// <returns> The <see cref="LimitMode"> Limit Mode</see> or none if unknown. </returns>
        public LimitMode? WriteLimitMode( LimitMode value )
        {
            this.LimitMode = this.Write( this.LimitModeCommandFormat, value );
            return this.LimitMode;
        }

        #endregion

    }

    /// <summary> Enumerates the binning control mode. </summary>
    public enum BinningControl
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined ()" )]
        None,

        /// <summary> An enum constant representing the immediate option. </summary>
        [System.ComponentModel.Description( "Immediate (IMM)" )]
        Immediate,

        /// <summary> An enum constant representing the end] option. </summary>
        [System.ComponentModel.Description( "End (END)" )]
        End
    }

    /// <summary> Enumerates the grading control mode. </summary>
    public enum LimitMode
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined ()" )]
        None,

        /// <summary> An enum constant representing the grading option. </summary>
        [System.ComponentModel.Description( "Grading (GRAD)" )]
        Grading,

        /// <summary> An enum constant representing the end] option. </summary>
        [System.ComponentModel.Description( "End (END)" )]
        End
    }
}
