namespace isr.VI.Tsp.Syntax
{

    /// <summary> Defines the TSP Node syntax. Modified for TSP2. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2005-01-15, 1.0.1841.x. </para></remarks>
    public static class Node
    {

        #region " NODE IDENTITY "

        /// <summary>
        /// Gets or sets the IDN query (print) command returning an identity formatted for Keithley
        /// instruments matching the SCPI standard.
        /// </summary>
        /// <remarks>
        /// Same as '*IDN?'.<para>
        /// Requires setting the subsystem reference.
        /// </para><code>
        /// Value = String.Format(KeithleyInstrumentIdentityQueryCommandBuilder,"node name")
        /// </code>
        /// </remarks>
        /// <value> The identity query (print) command format. </value>
        public static string KeithleyInstrumentIdentityQueryCommandBuilder { get; set; } = "_G.print(\"Keithley Instruments Inc., Model \"..{0}.model..\", \"..{0}.serialno..\", \"..{0}.version)";

        #endregion

        #region " NODE COMMANDS "

        /// <summary> Gets the status clear (CLS) command message. Requires a node number argument. </summary>
        public const string CollectNodeGarbageFormat = "_G.node[{0}].execute('collectgarbage()') _G.waitcomplete({0})";

        /// <summary> Gets the execute command.  Requires node number and command arguments. </summary>
        public const string ExecuteNodeCommandFormat = "_G.node[{0}].execute(\"{1}\") _G.waitcomplete({0})";

        /// <summary> Gets the value returned by executing a command on the node.
        /// Requires node number and value to get arguments. </summary>
        public const string ValueGetterCommandFormat1 = "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete({0}) _G.waitcomplete() _G.print(_G.node[{0}].dataqueue.next())";
        // 3517 "_G.node[{0}].execute('dataqueue.add({1})') _G.waitcomplete(0) _G.print(_G.node[{0}].dataqueue.next())"

        /// <summary> Gets the value returned by executing a command on the node.
        /// Requires node number, command, and value to get arguments. </summary>
        public const string ValueGetterCommandFormat2 = "_G.node[{0}].execute(\"do {1} dataqueue.add({2}) end\") _G.waitcomplete({0}) _G.waitcomplete() _G.print(_G.node[{0}].dataqueue.next())";
        // 3517 "_G.node[{0}].execute(""do {1} dataqueue.add({2}) end"") _G.waitcomplete(0) _G.print(_G.node[{0}].dataqueue.next())"

        /// <summary> Gets the connect rule command. Requires node number and value arguments. </summary>
        public const string ConnectRuleSetterCommandFormat = "_G.node[{0}].channel.connectrule = {1}  _G.waitcomplete({0})";

        #endregion

        #region " SYSTEM COMMAND BUILDERS "

        /// <summary> Gets or sets the reset command Builder. </summary>
        /// <remarks>
        /// Same as '*RST'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The reset known state command builder. </value>
        public static string ResetKnownStateCommandBuilder { get; set; } = "{0}.reset()";

        /// <summary> Gets or sets the status clear (CLS) command Builder. </summary>
        /// <remarks>
        /// Same as '*CLS'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The clear execution state command builder. </value>
        public static string ClearExecutionStateCommandBuilder { get; set; } = "{0}.status.reset()";

        #endregion

        #region " ERROR QUEUE COMMAND BUILDERS "

        /// <summary> Gets or sets the error queue clear command builder. </summary>
        /// <remarks>
        /// Same as ':STAT:QUE:CLEAR'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The clear error queue command builder. </value>
        public static string ClearErrorQueueCommandBuilder { get; set; } = "{0}.eventlog.clear()";

        /// <summary> Gets or sets the error queue query (print) command builder. </summary>
        /// <remarks>
        /// Same as ':STAT:QUE?'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The error queue query (print) command builder. </value>
        public static string ErrorQueueQueryCommandBuilder { get; set; } = "_G.print(string.format('%d,%s,level=%d',{0}.eventlog.next(eventlog.SEV_ERROR)))";

        /// <summary> Gets or sets the error queue count query (print) command builder. </summary>
        /// <remarks> Requires setting the subsystem reference. </remarks>
        /// <value> The error queue count query (print) command builder. </value>
        public static string ErrorQueueCountQueryCommandBuilder { get; set; } = "_G.print({0}.eventlog.getcount(eventlog.SEV_ERROR))";

        /// <summary> The node error count value builder. </summary>
        public const string NodeErrorCountBuilder = "node[{0}].eventlog.getcount(eventlog.SEV_ERROR)";

        #endregion

        #region " OPERATION EVENTS "

        /// <summary> Gets or sets the operation event enable command format builder. </summary>
        /// <remarks>
        /// Same as ''.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The operation event enable command format builder. </value>
        public static string OperationEventEnableCommandFormatBuilder { get; set; } = "{0}.status.operation.enable = {{0}}";

        /// <summary> Gets or sets the operation event enable query (print) command builder. </summary>
        /// <remarks>
        /// Same as ''.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The operation event enable query (print) command builder. </value>
        public static string OperationEventEnableQueryCommandBuilder { get; set; } = "_G.print(_G.tostring({0}.status.operation.enable))";

        /// <summary> Gets or sets the operation event status query (print) command builder. </summary>
        /// <remarks>
        /// Same as ''.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The operation event query (print) command builder. </value>
        public static string OperationEventQueryCommandBuilder { get; set; } = "_G.print(_G.tostring({0}.status.operation.event))";

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Gets or sets the service request enable command format builder. </summary>
        /// <remarks>
        /// Same as *SRE {0:D}'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The service request enable command format builder. </value>
        public static string ServiceRequestEnableCommandFormatBuilder { get; set; } = "{0}.status.request_enable = {{0}}";

        /// <summary> Gets or sets the service request enable query (print) command builder. </summary>
        /// <remarks>
        /// Same as ''.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The service request enable query (print) command builder. </value>
        public static string ServiceRequestEnableQueryCommandBuilder { get; set; } = "_G.print(_G.tostring({0}.status.request_enable))";

        /// <summary> Gets or sets the service request enable query (print) command builder. </summary>
        /// <remarks>
        /// Same as '*ESR?'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The service request event query (print) command builder. </value>
        public static string ServiceRequestEventQueryCommandBuilder { get; set; } = "_G.print(_G.tostring({0}.status.condition))";

        #endregion

        #region " STANDARD EVENTS "

        /// <summary> Gets or sets the standard event enable command format builder. </summary>
        /// <remarks>
        /// Same as *ESE {0:D}'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The standard event enable command format builder. </value>
        public static string StandardEventEnableCommandFormatBuilder { get; set; } = "{0}.status.standard.enable = {{0}}";

        /// <summary> Gets or sets the standard event enable query (print) command builder. </summary>
        /// <remarks>
        /// Same as ''.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The standard event enable query (print) command builder. </value>
        public static string StandardEventEnableQueryCommandBuilder { get; set; } = "_G.print(_G.tostring({0}.status.standard.enable))";

        /// <summary> Gets or sets the standard event status query (print) command builder. </summary>
        /// <remarks>
        /// Same as *ESR?'.<para>
        /// Requires setting the subsystem reference.
        /// </para>
        /// </remarks>
        /// <value> The standard event query (print) command builder. </value>
        public static string StandardEventQueryCommandBuilder { get; set; } = "_G.waitcomplete() _G.print(_G.tostring({0}.status.standard.event))";

        #endregion

    }
}
