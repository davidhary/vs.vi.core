using System;

namespace isr.VI.Tsp.Syntax
{

    /// <summary> Defines the syntax of the LUA base system. </summary>
    /// <remarks> (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2005-01-15, 1.0.1841.x. </para></remarks>
    public static class Lua
    {

        #region " CONSTANTS "

        /// <summary> Represents the LUA nil value. </summary>
        public const string NilValue = "nil";

        /// <summary> Represents the LUA true value </summary>
        public const string TrueValue = "true";

        /// <summary> Represents the LUA false value </summary>
        public const string FalseValue = "false";

        /// <summary> The global node reference. </summary>
        public const string GlobalNode = "_G";

        #endregion

        #region " CHUNK CONSTANTS "

        /// <summary> Gets the chunk defining a start of comment block. </summary>
        public const string StartCommentChunk = "--[[";

        /// <summary> Gets the chunk defining an end of comment block. </summary>
        public const string EndCommentChunk = "]]--";

        /// <summary> Gets the chunk defining a comment. </summary>
        public const string CommentChunk = "--";

        /// <summary> Gets the signature of a chunk line defining a chunk name. </summary>
        public const string DeclareChunkNameSignature = "local chunkName =";

        /// <summary> Gets the signature of a chunk line defining a require statement for the chunk name. </summary>
        public const string RequireChunkNameSignature = "require(\"";

        /// <summary> Gets the signature of a chunk line defining a loaded statement for the chunk name. </summary>
        public const string LoadedChunkNameSignature = "_G._LOADED[";

        #endregion

        #region " SCRIPT COMMANDS "

        /// <summary>
        /// Gets a command to retrieve a catalog from the local node.
        /// This command must be enclosed in a 'do end' construct.
        /// a print(names) or dataqueue.add(names) needs to be added to get the data through.
        /// </summary>
        public const string ScriptCatalogGetterCommand = "local names='' for name in script.user.catalog() do names = names .. name .. ',' end";

        #endregion

        #region " SYSTEM COMMANDS "

        /// <summary> The collect garbage command. </summary>
        public const string CollectGarbageCommand = "_G.collectgarbage()";

        /// <summary> The  collect garbage command. </summary>
        public const string CollectGarbageWaitCompleteCommand = "_G.collectgarbage() _G.waitcomplete(0)";

        /// <summary>
        /// The operation complete command (Same as '*OPC'). This function sets the operation complete
        /// status bit when all overlapped commands are completed.
        /// </summary>
        /// <remarks> *OPC will fail on a compound command (e.g., _G.opc(); *OPC) </remarks>
        public const string OperationCompleteCommand = "_G.opc()";

        /// <summary> Gets or set the operation completed query command. </summary>
        /// <remarks> Same as '*OPC?'. </remarks>
        public const string OperationCompletedQueryCommand = "_G.waitcomplete() print('1') ";

        /// <summary> The query (print) command format. </summary>
        public const string PrintCommandFormat = "_G.print({0})";

        /// <summary> The query (print) command to send to the instrument. The
        /// format conforms to the 'C' query command and returns the Boolean outcome. </summary>
        /// <remarks> The format string follows the same rules as the printf family of standard C
        /// functions. The only differences are that the options or modifiers *, l, L, n, p, and h are
        /// not supported and that there is an extra option, q. The q option formats a string in a form
        /// suitable to be safely read back by the Lua interpreter: the string is written between double
        /// quotes, and all double quotes, newlines, embedded zeros, and backslashes in the string are
        /// correctly escaped when written. For instance, the call string.format('%q', 'a string with
        /// ''quotes'' and [BS]n new line') will produce the string: a string with [BS]''quotes[BS]'' and
        /// [BS]new line The options c, d, E, e, f, g, G, i, o, u, X, and x all expect a number as
        /// argument, whereas q and s expect a string. This function does not accept string values
        /// containing embedded zeros. </remarks>
        public const string PrintCommandStringFormat = "_G.print(string.format('{0}',{1}))";

        /// <summary> The query (print) command string number format. </summary>
        /// <remarks> _G.print(string.format('%9.6f',smu.source.ilimit.level))\n </remarks>
        public const string PrintCommandStringNumberFormat = "_G.print(string.format('%{0}f',{1}))";

        /// <summary> The query (print) command string integer format. </summary>
        public const string PrintCommandStringIntegerFormat = "_G.print(string.format('%d',{0}))";

        /// <summary> Returns the query (print) command for the specified arguments. </summary>
        /// <param name="args"> Specifies the function arguments. </param>
        /// <returns> A String. </returns>
        public static string PrintCommand( string args )
        {
            return Tsp.Syntax.Constants.Build( PrintCommandFormat, args );
        }

        /// <summary> The reset to known state command (Same as '*RST'). </summary>
        public const string ResetKnownStateCommand = "_G.reset()";

        /// <summary> The wait command. </summary>
        public const string WaitGroupCommandFormat = "_G.waitcomplete({0})";

        /// <summary> The wait command. </summary>
        public const string WaitCommand = "_G.waitcomplete()";

        #endregion

        #region " COMMAND BUILDERS "

        /// <summary> Builds a command. </summary>
        /// <param name="format"> Specifies a format string for the command. </param>
        /// <param name="args">   Specifies the arguments for the command. </param>
        /// <returns> The command. </returns>
        public static string Build( string format, params object[] args )
        {
            return string.Format( System.Globalization.CultureInfo.InvariantCulture, format, args );
        }

        #endregion

        #region " SYSTEM COMMANDS "

        /// <summary> Gets the function call command for a function w/o arguments. </summary>
        private const string _CallFunctionCommandFormat = "_G.pcall( {0} )";

        /// <summary>
        /// Gets the function call command for a function with arguments.
        /// </summary>
        private const string _CallFunctionArgumentsCommandFormat = "_G.pcall( {0} , {1} )";

        /// <summary> Returns a command to run the specified function with arguments. </summary>
        /// <param name="functionName"> Specifies the function name. </param>
        /// <param name="args">         Specifies the function arguments. </param>
        /// <returns> A String. </returns>
        public static string CallFunctionCommand( string functionName, string args )
        {
            return string.IsNullOrWhiteSpace( args ) ? Build( _CallFunctionCommandFormat, functionName ) : Build( _CallFunctionArgumentsCommandFormat, functionName, args );
        }

        #endregion

        #region " FUNCTION "

        /// <summary>
        /// Returns a string from the parameter array of arguments for use when running the function.
        /// </summary>
        /// <param name="args"> Specifies a parameter array of arguments. </param>
        /// <returns> A comma-separated string. </returns>
        public static string Parameterize( params string[] args )
        {
            var arguments = new System.Text.StringBuilder();
            int i;
            if ( args is object && args.Length >= 0 )
            {
                var loopTo = args.Length - 1;
                for ( i = 0; i <= loopTo; i++ )
                {
                    if ( i > 0 )
                    {
                        _ = arguments.Append( "," );
                    }

                    _ = arguments.Append( args[i] );
                }
            }

            return arguments.ToString();
        }

        /// <summary> Calls the function with the given arguments in protected mode. </summary>
        /// <remarks>
        /// Protected mode means that any error inside the function is not propagated; instead, the call
        /// (Lua pcall) catches the error and returns a status code. Its first result is the status code
        /// (a Boolean), which is <c>True</c> if the call succeeds without errors. In such case, pcall
        /// also returns all results from the call, after this first result. In case of any error, pcall
        /// returns false plus the error message.
        /// </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session">      The session. </param>
        /// <param name="functionName"> Specifies the function name. </param>
        /// <param name="args">         Specifies the function arguments. </param>
        public static void CallFunction( Pith.SessionBase session, string functionName, string args )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            if ( string.IsNullOrWhiteSpace( functionName ) )
                throw new ArgumentNullException( nameof( functionName ) );
            string callStatement;
            callStatement = CallFunctionCommand( functionName, args );
            callStatement = PrintCommand( callStatement );
            _ = session.WriteLine( callStatement );
        }

        #endregion

    }
}
