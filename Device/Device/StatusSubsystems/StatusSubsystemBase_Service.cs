namespace isr.VI
{
    public abstract partial class StatusSubsystemBase
    {

        #region " STATUS REGISTER EVENTS: REQUESTING SERVICE "

        /// <summary>
        /// Gets or sets bit that would be set if a requested service or master summary status event has
        /// occurred.
        /// </summary>
        /// <value> The requested service or master summary status bit. </value>
        public virtual Pith.ServiceRequests RequestingServiceBit
        {
            get => this.Session.RequestingServiceBit;

            set {
                if ( value != this.RequestingServiceBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.RequestingServiceBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating if the device has requested service. </summary>
        /// <value> <c>True</c> if the device has requested service; otherwise, <c>False</c>. </value>
        public bool RequestingService => this.Session.RequestingService;

        /// <summary>   Discard service requests. </summary>
        /// <remarks>   David, 2021-04-17. </remarks>
        public virtual void DiscardServiceRequests()
        {
            this.Session.DiscardServiceRequests();
        }

        #endregion

        #region " STATUS REGISTER EVENTS: ERROR "

        /// <summary> Gets or sets the bit that would be set if an error event has occurred. </summary>
        /// <value> The error event bit. </value>
        public virtual Pith.ServiceRequests ErrorAvailableBit
        {
            get => this.Session.ErrorAvailableBit;

            set {
                if ( value != this.ErrorAvailableBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.ErrorAvailableBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether [Error available]. </summary>
        /// <value> <c>True</c> if [Error available]; otherwise, <c>False</c>. </value>
        public bool ErrorAvailable => this.Session.ErrorAvailable;

        #endregion

        #region " STATUS REGISTER EVENTS: MESSAGE "

        /// <summary> Gets or sets the bit that would be set if a message is available. </summary>
        /// <value> The Message available bit. </value>
        public virtual Pith.ServiceRequests MessageAvailableBit
        {
            get => this.Session.MessageAvailableBit;

            set {
                if ( value != this.MessageAvailableBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.MessageAvailableBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether [Message available]. </summary>
        /// <value> <c>True</c> if [Message available]; otherwise, <c>False</c>. </value>
        public virtual bool MessageAvailable => this.Session.MessageAvailable;

        /// <summary>   Is error available. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   A Tuple: ( true if error, details, status byte) </returns>
        public virtual (bool HasError, string Details, int StatusByte) IsStatusError( int statusByte )
        {
            return this.Session.IsErrorBitSet( statusByte ) ? (true, "Error", statusByte) : (false, string.Empty, statusByte);
        }

        /// <summary>   Is status error or warning. </summary>
        /// <remarks>   David, 2021-04-17. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   A Tuple. </returns>
        public virtual (bool HasError, string Details, int StatusByte) IsStatusErrorOrWarning( int statusByte )
        {
            return this.Session.IsErrorBitSet( statusByte ) ? (true, "Error", statusByte) : (false, string.Empty, statusByte);
        }


        /// <summary>   Queries status error. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <returns>   A Tuple: ( true if error, details, status byte) </returns>
        public (bool hasError, string Details, int StatusByte) QueryStatusError()
        {
            return this.IsStatusError( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Query if 'statusByte' is status busy. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if status busy, false if not. </returns>
        public virtual bool IsStatusBusy( int statusByte )
        {
            return 0 == statusByte;
        }

        /// <summary>   Queries status not busy. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <returns>   The status not busy. </returns>
        public virtual (bool done, int StatusByte) QueryStatusNotBusy()
        {
            return (true, 0);
        }

        /// <summary>   Query if 'statusByte' is status data ready. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if status data ready, false if not. </returns>
        public virtual bool IsStatusDataReady( int statusByte )
        {
            return this.Session.IsMessageAvailable( statusByte );
        }

        /// <summary>   Query if 'statusByte' is status query result ready. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if status query result ready, false if not. </returns>
        public virtual bool IsStatusQueryResultReady( int statusByte )
        {
            return this.Session.IsMessageAvailable( statusByte );
        }

        #endregion

        #region " STATUS REGISTER EVENTS: MEASUREMENT "

        /// <summary>
        /// Gets or sets the bit that would be set when an enabled measurement event has occurred.
        /// </summary>
        /// <value> The Measurement event bit value. </value>
        public virtual Pith.ServiceRequests MeasurementEventBit
        {
            get => this.Session.MeasurementEventBit;

            set {
                if ( value != this.MeasurementEventBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.MeasurementEventBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether an enabled measurement event has occurred. </summary>
        /// <value>
        /// <c>True</c> if an enabled measurement event has occurred; otherwise, <c>False</c>.
        /// </value>
        public bool HasMeasurementEvent => this.Session.HasMeasurementEvent;

        #endregion

        #region " STATUS REGISTER EVENTS: SYSTEM "

        /// <summary> Gets or sets the bit that would be set if an System event has occurred. </summary>
        /// <value> The System event bit. </value>
        public virtual Pith.ServiceRequests SystemEventBit
        {
            get => this.Session.SystemEventBit;

            set {
                if ( value != this.SystemEventBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.SystemEventBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether an System event has occurred. </summary>
        /// <value> <c>True</c> if an System event has occurred; otherwise, <c>False</c>. </value>
        public bool HasSystemEvent => this.Session.HasSystemEvent;

        #endregion

        #region " STATUS REGISTER EVENTS: OPERATION "

        /// <summary> Gets or sets the bit that would be set if an operation event has occurred. </summary>
        /// <value> The Operation event bit. </value>
        public virtual Pith.ServiceRequests OperationEventBit
        {
            get => this.Session.OperationEventBit;

            set {
                if ( value != this.OperationEventBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.OperationEventBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether an operation event has occurred. </summary>
        /// <value> <c>True</c> if an operation event has occurred; otherwise, <c>False</c>. </value>
        public bool HasOperationEvent => this.Session.HasOperationEvent;

        #endregion

        #region " STATUS REGISTER EVENTS: QUESTIONABLE "

        /// <summary>
        /// Gets or sets the bit that would be set if an Questionable event has occurred.
        /// </summary>
        /// <value> The Questionable event bit. </value>
        public virtual Pith.ServiceRequests QuestionableEventBit
        {
            get => this.Session.QuestionableEventBit;

            set {
                if ( value != this.QuestionableEventBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.QuestionableEventBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether an Questionable event has occurred. </summary>
        /// <value> <c>True</c> if an Questionable event has occurred; otherwise, <c>False</c>. </value>
        public bool HasQuestionableEvent => this.Session.HasQuestionableEvent;

        #endregion

        #region " STATUS REGISTER EVENTS: STANDARD EVENT "

        /// <summary>
        /// Gets or sets bit that would be set if an enabled standard event has occurred.
        /// </summary>
        /// <value> The Standard Event bit. </value>
        public virtual Pith.ServiceRequests StandardEventBit
        {
            get => this.Session.StandardEventBit;

            set {
                if ( value != this.StandardEventBit )
                {
                    // the session sends the notification and notify the status subsystem via the session property change event.
                    this.Session.StandardEventBit = value;
                }
            }
        }

        /// <summary> Gets a value indicating whether an enabled standard event has occurred. </summary>
        /// <value>
        /// <c>True</c> if an enabled standard event has occurred; otherwise, <c>False</c>.
        /// </value>
        public bool HasStandardEvent => this.Session.HasStandardEvent;

        #endregion

    }
}
