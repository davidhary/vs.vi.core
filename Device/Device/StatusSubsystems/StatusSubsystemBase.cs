using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;
using isr.Core.StackTraceExtensions;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by Status Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class StatusSubsystemBase : SubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
        /// </summary>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        protected StatusSubsystemBase( Pith.SessionBase session ) : this( session, Pith.Scpi.Syntax.NoErrorCompoundMessage )
        {
            this._PresetRefractoryPeriod = TimeSpan.FromMilliseconds( 100d );
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
        /// </summary>
        /// <param name="session">                A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        /// <param name="noErrorCompoundMessage"> A message describing the no error. </param>
        protected StatusSubsystemBase( Pith.SessionBase session, string noErrorCompoundMessage ) : base( Pith.SessionBase.Validated( session ) )
        {
            session.ResourcesFilter = SessionFactory.Get.Factory.ResourcesProvider().ResourceFinder.BuildMinimalResourcesFilter();
            this.ApplySessionThis( session );
            this._ExpectedLanguage = string.Empty;
            this.DeviceErrorBuilder = new System.Text.StringBuilder();
            this.NoErrorCompoundMessage = noErrorCompoundMessage;
            this.DeviceErrorQueue = new DeviceErrorQueue( noErrorCompoundMessage );
            this._InitializeTimeout = TimeSpan.FromMilliseconds( 5000d );
            this._InitRefractoryPeriod = TimeSpan.FromMilliseconds( 100d );
            // Me.StandardServiceEnableCommandFormat = Vi.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat
            // Me.StandardServiceEnableCompleteCommandFormat = Vi.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat
            this.OperationEventMap = new Dictionary<int, string>();
        }

        /// <summary> Validated the given status subsystem base. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="statusSubsystemBase"> The status subsystem base. </param>
        /// <returns> A StatusSubsystemBase. </returns>
        public static StatusSubsystemBase Validated( StatusSubsystemBase statusSubsystemBase )
        {
            return statusSubsystemBase is null ? throw new ArgumentNullException( nameof( statusSubsystemBase ) ) : statusSubsystemBase;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Executes the device open actions. </summary>
        /// <remarks>
        /// This was added in order to defer reading error status after clearing the device state when
        /// opening the device.
        /// </remarks>
        public virtual void OnDeviceOpen()
        {

            // clear the device active state
            this.Session.ClearActiveState();

            // reset device
            this.Session.ResetKnownState();

            // Clear the device Status and set more defaults
            this.Session.ClearExecutionState();
        }

        /// <summary> The initialize refractory period. </summary>
        private TimeSpan _InitRefractoryPeriod;

        /// <summary> Gets or sets the initialize refractory period. </summary>
        /// <value> The initialize refractory period. </value>
        public TimeSpan InitRefractoryPeriod
        {
            get => this._InitRefractoryPeriod;

            set {
                if ( !value.Equals( this.InitRefractoryPeriod ) )
                {
                    this._InitRefractoryPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Enables and sets the default bitmasks for wait complete. </summary>
        /// <param name="standardEventEnableBitmask">  The standard event enable bitmask. </param>
        /// <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
        public virtual void EnableServiceRequestWaitComplete( Pith.StandardEvents standardEventEnableBitmask, Pith.ServiceRequests serviceRequestEnableBitmask )
        {
            this.Session.EnableServiceRequestWaitComplete( standardEventEnableBitmask, serviceRequestEnableBitmask );
        }

        /// <summary> Enables the service requests. </summary>
        /// <param name="serviceRequestEnableBitmask"> The service request enable bitmask. </param>
        public virtual void EnableServiceRequestEvents( Pith.ServiceRequests serviceRequestEnableBitmask )
        {
            this.Session.ApplyServiceRequestEnableBitmask( serviceRequestEnableBitmask );
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override void InitKnownState()
        {
            base.InitKnownState();
            if ( this.Session.IsSessionOpen )
            {
                Core.ApplianceBase.DoEvents();
                // ?TO_DO: testing using OPC in place of the refractory periods.
                if ( this.Session.SupportsOperationComplete )
                {
                    _ = this.Session.QueryOperationCompleted();
                    Core.ApplianceBase.DoEventsWait( this.Session.StatusReadDelay );
                }
                else
                {
                    Core.ApplianceBase.DoEventsWait( this.InitRefractoryPeriod );
                }
            }

            string activity = string.Empty;
            try
            {
                activity = $"{this.Session.ResourceNameCaption} clearing error queue";
                _ = this.PublishVerbose( $"{activity};. " );
                this.ClearErrorQueue();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }

            try
            {
                activity = $"{this.Session.ResourceNameCaption} clearing error cache";
                _ = this.PublishVerbose( $"{activity};. " );
                this.ClearErrorCache();
                activity = $"{this.Session.ResourceNameCaption} clearing error report";
                _ = this.PublishVerbose( $"{activity};. " );
                this.ClearErrorReport();
                activity = $"{this.Session.ResourceNameCaption} clearing orphan messages";
                _ = this.PublishVerbose( $"{activity};. " );
                this.ClearUnreadMessages();
                this.LastUnreadMessage = string.Empty;
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }

            try
            {
                activity = $"{this.Session.ResourceNameCaption} defining wait complete bit masks";
                _ = this.PublishVerbose( $"{activity};. " );
                this.EnableServiceRequestWaitComplete( this.Session.DefaultStandardEventEnableBitmask, this.Session.DefaultOperationCompleteBitmask );
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }

            try
            {
                activity = $"{this.Session.ResourceNameCaption} clearing instrument messages queue";
                _ = this.PublishVerbose( $"{activity};. " );
                this.DiscardUnreadMessages( TimeSpan.FromMilliseconds( 10 ) );
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }

            try
            {
                activity = $"{this.Session.ResourceNameCaption} querying identity";
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.QueryIdentity();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }

            try
            {
                activity = $"{this.Session.ResourceNameCaption} defining register event bit values";
                _ = this.PublishVerbose( $"{activity};. " );
                this.DefineEventBitmasks();
                activity = $"{this.Session.ResourceNameCaption} enable full service request";
                _ = this.PublishVerbose( $"{activity};. " );
                this.EnableServiceRequestEvents( this.Session.DefaultServiceRequestEnableBitmask );
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> The initialize timeout. </summary>
        private TimeSpan _InitializeTimeout;

        /// <summary> Gets or sets the time out for doing a reset and clear on the instrument. </summary>
        /// <value> The connect timeout. </value>
        public TimeSpan InitializeTimeout
        {
            get => this._InitializeTimeout;

            set {
                if ( !value.Equals( this.InitializeTimeout ) )
                {
                    this._InitializeTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> Clears the queues and sets all registers to zero. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Identity = string.Empty;
            this.ResetRegistersKnownState();
            _ = this.QueryLineFrequency();
            _ = this.Session.ReadStatusRegister();
        }

        #region " PRESET "

        /// <summary> Gets or sets the preset command. </summary>
        /// <remarks>
        /// SCPI: ":STAT:PRES".
        /// <see cref="VI.Pith.Scpi.Syntax.StatusPresetCommand"> </see>
        /// </remarks>
        /// <value> The preset command. </value>
        protected virtual string PresetCommand { get; set; } = Pith.Scpi.Syntax.StatusPresetCommand;

        /// <summary> Returns the instrument registers to there preset power on state. </summary>
        /// <remarks>
        /// SCPI: "*???".<para>
        /// <see cref="VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand"> </see> </para><para>
        /// When this command is sent, the SCPI event registers are affected as follows:<p>
        /// 1. All bits of the positive transition filter registers are set to one (1).</p><p>
        /// 2. All bits of the negative transition filter registers are cleared to zero (0).</p><p>
        /// 3. All bits of the following registers are cleared to zero (0):</p><p>
        /// a. Operation Event Enable Register.</p><p>
        /// b. Questionable Event Enable Register.</p><p>
        /// 4. All bits of the following registers are set to one (1):</p><p>
        /// a. Trigger Event Enable Register.</p><p>
        /// b. Arm Event Enable Register.</p><p>
        /// c. Sequence Event Enable Register.</p><p>
        /// Note: Registers not included in the above list are not affected by this command.</p> </para>
        /// </remarks>
        public override void PresetKnownState()
        {
            if ( !string.IsNullOrWhiteSpace( this.PresetCommand ) )
            {
                this.Session.OperationCompleted = new bool?();
                this.Execute( this.PresetCommand );
                if ( this.Session.IsSessionOpen )
                {
                    Core.ApplianceBase.DoEventsWait( this.PresetRefractoryPeriod );
                }

                _ = this.Session.QueryOperationCompleted();
                _ = this.Session.ReadStatusRegister();
                this.PresetRegistersKnownState();
            }
        }

        /// <summary> The preset refractory period. </summary>
        private TimeSpan _PresetRefractoryPeriod;

        /// <summary> Gets or sets the post-preset refractory period. </summary>
        /// <value> The post-preset refractory period. </value>
        public TimeSpan PresetRefractoryPeriod
        {
            get => this._PresetRefractoryPeriod;

            set {
                if ( !value.Equals( this.PresetRefractoryPeriod ) )
                {
                    this._PresetRefractoryPeriod = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary> Applies the session described by visaSession. </summary>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based
        /// session</see>. </param>
        private void ApplySessionThis( Pith.SessionBase session )
        {
            session.ReadTerminationCharacterEnabled = true;
            session.ReadTerminationCharacter = Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineValue;
            session.DeviceErrorOccurred += this.HandleDeviceErrorOccurred;
        }

        /// <summary> Handles the device error occurred. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void HandleDeviceErrorOccurred( object sender, EventArgs e )
        {
            string activity = "checking if already reading error";
            try
            {
                if ( sender is Pith.SessionBase sessionBase && this.ErrorAvailable && !this.MessageAvailable )
                {
                    activity = "reading device errors";
                    this.QueryExistingDeviceErrors();
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the session property changed action. </summary>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected override void HandlePropertyChanged( Pith.SessionBase sender, string propertyName )
        {
            base.HandlePropertyChanged( sender, propertyName );
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Pith.SessionBase.ErrorAvailable ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.ErrorAvailable ) );
                        break;
                    }

                case nameof( Pith.SessionBase.ErrorAvailableBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.ErrorAvailableBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.HasMeasurementEvent ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.HasMeasurementEvent ) );
                        break;
                    }

                case nameof( Pith.SessionBase.MeasurementEventBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.MeasurementEventBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.MessageAvailableBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.MessageAvailableBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.MessageAvailable ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.MessageAvailable ) );
                        break;
                    }

                case nameof( Pith.SessionBase.HasOperationEvent ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.HasOperationEvent ) );
                        break;
                    }

                case nameof( Pith.SessionBase.OperationEventBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.OperationEventBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.HasQuestionableEvent ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.HasQuestionableEvent ) );
                        break;
                    }

                case nameof( Pith.SessionBase.QuestionableEventBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.QuestionableEventBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.HasStandardEvent ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.HasStandardEvent ) );
                        break;
                    }

                case nameof( Pith.SessionBase.StandardEventBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.StandardEventBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.HasSystemEvent ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.HasSystemEvent ) );
                        break;
                    }

                case nameof( Pith.SessionBase.SystemEventBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.SystemEventBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.RequestingService ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.RequestingService ) );
                        break;
                    }

                case nameof( Pith.SessionBase.RequestingServiceBit ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.RequestingServiceBit ) );
                        break;
                    }

                case nameof( Pith.SessionBase.LastMessageReceived ):
                    {
                        string value = sender.LastMessageReceived;
                        if ( !string.IsNullOrWhiteSpace( value ) )
                        {
                            _ = this.PublishInfo( $"{this.ResourceNameCaption} received: '{value.InsertCommonEscapeSequences()}'" );
                        }

                        break;
                    }

                case nameof( Pith.SessionBase.LastMessageSent ):
                    {
                        string value = sender.LastMessageSent;
                        if ( !string.IsNullOrWhiteSpace( value ) )
                        {
                            _ = this.PublishInfo( $"{this.ResourceNameCaption} sent: '{value}'" );
                        }

                        break;
                    }

                case nameof( Pith.SessionBase.OperationCompleted ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.OperationCompleted ) );
                        break;
                    }

                case nameof( Pith.SessionBase.ServiceRequestStatus ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.ServiceRequestStatus ) );
                        break;
                    }

                case nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.ServiceRequestEnabledBitmask ) );
                        break;
                    }

                case nameof( Pith.SessionBase.ServiceRequestWaitCompleteEnabledBitmask ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.ServiceRequestWaitCompleteEnabledBitmask ) );
                        break;
                    }

                case nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.StandardEventWaitCompleteEnabledBitmask ) );
                        break;
                    }

                case nameof( Pith.SessionBase.StandardEventEnableBitmask ):
                    {
                        this.NotifyPropertyChanged( nameof( Pith.SessionBase.StandardEventEnableBitmask ) );
                        break;
                    }
            }
        }

        #endregion

        #region " IDENTITY "

        /// <summary> The identity. </summary>
        private string _Identity;

        /// <summary> Gets or sets the device identity string (*IDN?). </summary>
        /// <value> The identity. </value>
        public string Identity
        {
            get => this._Identity;

            set {
                if ( string.IsNullOrEmpty( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.Identity ) )
                {
                    this._Identity = value;
                    this.NotifyPropertyChanged();
                    if ( !string.IsNullOrWhiteSpace( value ) )
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} identified;. as {value}" );
                }
            }
        }

        /// <summary> Gets or sets the identity query command. </summary>
        /// <remarks>
        /// SCPI: "*IDN?".
        /// <see cref="VI.Pith.Ieee488.Syntax.IdentityQueryCommand"> </see>
        /// </remarks>
        /// <value> The identity query command. </value>
        protected virtual string IdentityQueryCommand { get; set; } = Pith.Ieee488.Syntax.IdentityQueryCommand;

        /// <summary> Writes the identity query command. </summary>
        /// <remarks> This is required for systems which require asynchronous communication. </remarks>
        public void WriteIdentityQueryCommand()
        {
            _ = this.WriteLine( this.IdentityQueryCommand );
        }

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the '*IDN?' query. </remarks>
        /// <returns> System.String. </returns>
        public abstract string QueryIdentity();

        /// <summary>   Reads an identity. </summary>
        /// <remarks>   David, 2021-04-08. required in case the identify includes multiple lines. </remarks>
        /// <returns>   The identity. </returns>
        public virtual string ReadIdentity()
        {
            return string.Empty;
        }

        /// <summary>   Reads an identity. </summary>
        /// <remarks>   David, 2021-04-08. required in case the identify includes multiple lines. </remarks>
        /// <param name="firstLine">    The first line. </param>
        /// <returns>   The identity. </returns>
        public virtual string ReadIdentity( string firstLine )
        {
            return firstLine;
        }

        /// <summary> The version information base. </summary>
        private VersionInfoBase _VersionInfoBase;

        /// <summary> Gets or sets information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfoBase VersionInfoBase
        {
            get => this._VersionInfoBase;

            set {
                this._VersionInfoBase = value;
                if ( value is object )
                {
                    this.SerialNumberReading = value.SerialNumber;
                }
            }
        }


        #region " SERIAL NUMBER "

        /// <summary> The serial number reading. </summary>
        private string _SerialNumberReading;

        /// <summary> Gets or sets the serial number reading. </summary>
        /// <value> The serial number reading. </value>
        public string SerialNumberReading
        {
            get => this._SerialNumberReading;

            set {
                if ( string.IsNullOrEmpty( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.SerialNumberReading ) )
                {
                    this._SerialNumberReading = value;
                    this.NotifyPropertyChanged();
                    if ( string.IsNullOrWhiteSpace( value ) )
                    {
                        this.SerialNumber = new long?();
                    }
                    else
                    {
                        if ( long.TryParse( this.SerialNumberReading, System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out long numericValue ) )
                        {
                        }

                        this.SerialNumber = numericValue;
                    }
                }
            }
        }

        /// <summary> Gets or sets the serial number query command. </summary>
        /// <value> The serial number query command. </value>
        protected virtual string SerialNumberQueryCommand { get; set; }

        /// <summary> Reads and returns the instrument serial number. </summary>
        /// <exception cref="VI.Pith.NativeException"> Thrown when a Visa error condition occurs. </exception>
        public string QuerySerialNumber()
        {
            if ( !string.IsNullOrWhiteSpace( this.SerialNumberQueryCommand ) )
            {
                this.Session.LastAction = this.PublishInfo( "Reading serial number;. " );
                this.Session.LastNodeNumber = new int?();
                string value = this.Session.QueryTrimEnd( this.SerialNumberQueryCommand );
                this.CheckThrowDeviceException( false, "getting serial number;. using {0}.", this.Session.LastMessageSent );
                this.SerialNumberReading = value;
            }
            else if ( string.IsNullOrWhiteSpace( this.SerialNumberReading ) && this.SerialNumber.HasValue )
            {
                this.SerialNumberReading = this.SerialNumber.Value.ToString();
            }

            return this.SerialNumberReading;
        }

        /// <summary> The serial number. </summary>
        private long? _SerialNumber;

        /// <summary> Reads and returns the instrument serial number. </summary>
        /// <value> The serial number. </value>
        public long? SerialNumber
        {
            get => this._SerialNumber;

            set {
                if ( !Nullable.Equals( this.SerialNumber, value ) )
                {
                    this._SerialNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region " LANGUAGE "

        /// <summary> The Language. </summary>
        private string _Language;

        /// <summary> Gets or sets the device Language string (*IDN?). </summary>
        /// <value> The Language. </value>
        public string Language
        {
            get => this._Language;

            set {
                if ( string.IsNullOrEmpty( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.Language ) )
                {
                    value = value.Trim();
                    this._Language = value;
                    this.NotifyPropertyChanged();
                }

                this.LanguageValidated = this.IsExpectedLanguage();
            }
        }

        /// <summary> The expected language. </summary>
        private string _ExpectedLanguage;

        /// <summary> Gets or sets the device ExpectedLanguage string (*IDN?). </summary>
        /// <value> The ExpectedLanguage. </value>
        public string ExpectedLanguage
        {
            get => this._ExpectedLanguage;

            set {
                if ( string.IsNullOrEmpty( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ExpectedLanguage ) )
                {
                    value = value.Trim();
                    this._ExpectedLanguage = value;
                    this.NotifyPropertyChanged();
                }

                this.LanguageValidated = this.IsExpectedLanguage();
            }
        }

        /// <summary>
        /// Query if the language is the expected language or if the expected language is empty and thus
        /// invariant.
        /// </summary>
        /// <returns> <c>true</c> if expected language; otherwise <c>false</c> </returns>
        public bool IsExpectedLanguage()
        {
            return !string.IsNullOrWhiteSpace( this.Language ) && string.Equals( this.ExpectedLanguage, this.Language, StringComparison.OrdinalIgnoreCase );
        }

        /// <summary> True if language validated. </summary>
        private bool _LanguageValidated;

        /// <summary> Gets or sets the sentinel indicating if the language was validated. </summary>
        /// <value> The LanguageValidated. </value>
        public bool LanguageValidated
        {
            get => this._LanguageValidated;

            set {
                if ( !Equals( value, this.LanguageValidated ) )
                {
                    this._LanguageValidated = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the language. </summary>
        /// <param name="value"> The required language. </param>
        /// <returns> The language. </returns>
        public string ApplyLanguage( string value )
        {
            _ = this.WriteLanguage( value );
            return this.QueryLanguage();
        }

        /// <summary> Attempts to apply expected language from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) TryApplyExpectedLanguage()
        {
            (bool Success, string Details) result = (true, string.Empty);
            // check the language status. 
            _ = this.QueryLanguage();
            if ( this.LanguageValidated )
            {
                _ = this.PublishVerbose( $"Device language {this.Language} validated;. " );
            }
            else
            {
                // set the device to the correct language.
                _ = this.PublishInfo( $"Setting device to {this.ExpectedLanguage} language;. " );
                _ = this.ApplyLanguage( this.ExpectedLanguage );
                if ( !this.LanguageValidated )
                {
                    result = (false, this.PublishWarning( $"Incorrect {nameof( this.Language )} settings {this.Language}; must be {this.ExpectedLanguage}" ));
                }
            }

            return result;
        }

        /// <summary> Gets or sets the Language query command. </summary>
        /// <remarks> SCPI: "*LANG?". </remarks>
        /// <exception cref="isr.VI.Pith.DeviceException">       Thrown when a Device error condition occurs. </exception>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The Language query command. </value>
        protected virtual string LanguageQueryCommand { get; set; }

        /// <summary> Queries the Language. </summary>
        /// <remarks> Sends the '*LANG?' query. </remarks>
        /// <returns> The Language or Empty if not required or unknown. </returns>
        public virtual string QueryLanguage()
        {
            this.Language = string.IsNullOrWhiteSpace( this.LanguageQueryCommand ) ? string.Empty : this.Session.QueryTrimEnd( this.LanguageQueryCommand );
            return this.Language;
        }

        /// <summary> Gets or sets the Language command format. </summary>
        /// <remarks> *LANG {0}". </remarks>
        /// <exception cref="isr.VI.Pith.DeviceException">       Thrown when a Device error condition occurs. </exception>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The Language command format. </value>
        protected virtual string LanguageCommandFormat { get; set; }

        /// <summary> Writes the Language value without reading back the value from the device. </summary>
        /// <remarks> Changing the language causes the instrument to reboot. </remarks>
        /// <param name="value"> The current Language. </param>
        /// <returns> The Language or Empty if not required or unknown. </returns>
        public string WriteLanguage( string value )
        {
            this.Language = string.IsNullOrWhiteSpace( this.LanguageQueryCommand ) ? value : this.WriteLine( this.LanguageCommandFormat, value );
            return this.Language;
        }

        #endregion

        #region " CHECK AND THROW "

        /// <summary>
        /// Checks and throws an exception if device errors occurred. Can only be used after receiving a
        /// full reply from the device.
        /// </summary>
        /// <exception cref="VI.Pith.DeviceException"> Thrown when a device error condition occurs. </exception>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="args">           A variable-length parameters list containing arguments. </param>
        public void CheckThrowDeviceException( bool flushReadFirst, string format, params object[] args )
        {
            if ( (this.Session.ReadStatusRegister() & this.ErrorAvailableBit) != 0 )
            {
                if ( flushReadFirst )
                    this.Session.DiscardUnreadData();
                _ = this.Session.QueryStandardEventStatus();
                _ = this.QueryDeviceErrors();
                string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
                throw new Pith.DeviceException( this.ResourceNameCaption, $"{details}. {this.DeviceErrorReport}." );
            }
        }

        #endregion

        #region " CHECK AND TRACE "

        /// <summary>
        /// Check and reports visa or device error occurred. Can only be used after receiving a full
        /// reply from the device.
        /// </summary>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Specifies the report format. </param>
        /// <param name="args">       Specifies the report arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public virtual bool TraceVisaDeviceOperationOkay( int nodeNumber, string format, params object[] args )
        {
            return true;
        }

        /// <summary>
        /// Checks and reports if a visa or device error occurred. Can only be used after receiving a
        /// full reply from the device.
        /// </summary>
        /// <param name="nodeNumber">     Specifies the remote node number to validate. </param>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="args">           A variable-length parameters list containing arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool TraceVisaDeviceOperationOkay( int nodeNumber, bool flushReadFirst, string format, params object[] args )
        {
            bool success;
            this.TraceVisaOperation( nodeNumber, format, args );
            _ = this.Session.ReadStatusRegister();
            success = !this.ErrorAvailable;
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            if ( success )
            {
                _ = this.PublishInfo( $"{this.ResourceNameCaption} node {nodeNumber} done {details}" );
            }
            else
            {
                if ( flushReadFirst )
                    this.Session.DiscardUnreadData();
                _ = this.Session.QueryStandardEventStatus();
                _ = this.QueryDeviceErrors();
                string errors = this.DeviceErrorReport;
                if ( !string.IsNullOrWhiteSpace( errors ) )
                {
                    _ = this.PublishWarning( $@"{this.ResourceNameCaption} node {nodeNumber} device errors: {errors};. Details: 
{details}
{new StackFrame( true ).UserCallStack()}" );
                }
            }

            return success;
        }

        /// <summary>
        /// Checks and reports if a visa or device error occurred. Can only be used after receiving a
        /// full reply from the device.
        /// </summary>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="args">           A variable-length parameters list containing arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool TraceVisaDeviceOperationOkay( bool flushReadFirst, string format, params object[] args )
        {
            this.TraceVisaOperation( format, args );
            _ = this.Session.ReadStatusRegister();
            bool success = !this.ErrorAvailable;
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            if ( success )
            {
                _ = this.PublishInfo( $"{this.ResourceNameCaption} done {details}" );
            }
            else
            {
                if ( flushReadFirst )
                    this.Session.DiscardUnreadData();
                _ = this.Session.QueryStandardEventStatus();
                _ = this.QueryDeviceErrors();
                string errors = this.DeviceErrorReport;
                if ( !string.IsNullOrWhiteSpace( errors ) )
                {
                    _ = this.PublishWarning( $@"{this.ResourceNameCaption} device errors: {errors};. Details: 
{details}
{new StackFrame( true ).UserCallStack()}" );
                }
            }

            return success;
        }

        /// <summary> Reports operation action. Can be used with queries. </summary>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( string format, params object[] args )
        {
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            _ = this.PublishInfo( $"{this.ResourceNameCaption} done {details}" );
        }

        /// <summary> Reports if a visa error occurred. Can be used with queries. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ex">     The exception. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( Pith.NativeException ex, string format, params object[] args )
        {
            if ( ex is null )
                throw new ArgumentNullException( nameof( ex ) );
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, $@"{this.ResourceNameCaption} VISA error: {ex.InnerError.BuildErrorCodeDetails()};. Details: 
{details}
{new StackFrame( true ).UserCallStack()}" );
        }

        /// <summary> Reports if a visa error occurred. Can be used with queries. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ex">     The exception. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceOperation( Exception ex, string format, params object[] args )
        {
            if ( ex is null )
                throw new ArgumentNullException( nameof( ex ) );
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, $@"{this.ResourceNameCaption} exception: {ex.Message};. Details: 
{details}
{new StackFrame( true ).UserCallStack()}" );
        }

        /// <summary> Trace visa node operation. Can be used with queries. </summary>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Describes the format to use. </param>
        /// <param name="args">       A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( int nodeNumber, string format, params object[] args )
        {
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            _ = this.PublishInfo( $"{this.ResourceNameCaption} node {nodeNumber} done {details}" );
        }

        /// <summary> Trace visa failed operation. Can be used with queries. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ex">         The exception. </param>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Describes the format to use. </param>
        /// <param name="args">       A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( Pith.NativeException ex, int nodeNumber, string format, params object[] args )
        {
            if ( ex is null )
                throw new ArgumentNullException( nameof( ex ) );
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            _ = this.PublishWarning( $@"{this.ResourceNameCaption} node {nodeNumber} VISA error: {ex.InnerError.BuildErrorCodeDetails()};. Details: 
{details}
{new StackFrame( true ).UserCallStack()}" );
        }

        /// <summary> Trace failed operation. Can be used with queries. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="ex">         The exception. </param>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Describes the format to use. </param>
        /// <param name="args">       A variable-length parameters list containing arguments. </param>
        public void TraceOperation( Exception ex, int nodeNumber, string format, params object[] args )
        {
            if ( ex is null )
                throw new ArgumentNullException( nameof( ex ) );
            string details = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, $@"{this.ResourceNameCaption} node {nodeNumber} exception: {ex.Message};. Details: 
{details}
{new StackFrame( true ).UserCallStack()}" );
        }

        #endregion

        #region " COLLECT GARBAGE "

        /// <summary> Gets or sets the collect garbage wait complete command. </summary>
        /// <value> The collect garbage wait complete command. </value>
        protected virtual string CollectGarbageWaitCompleteCommand { get; set; } = string.Empty;

        /// <summary> Collect garbage wait complete. </summary>
        /// <param name="timeout"> Specifies how long to wait for the service request before throwing
        /// the timeout exception. Set to zero for an infinite (120 seconds)
        /// timeout. </param>
        /// <param name="format">  Specifies the report format. </param>
        /// <param name="args">    Specifies the report arguments. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool CollectGarbageWaitComplete( TimeSpan timeout, string format, params object[] args )
        {
            return string.IsNullOrWhiteSpace( this.CollectGarbageWaitCompleteCommand ) || this.CollectGarbageWaitCompleteThis( timeout, format, args );
        }

        /// <summary> Does garbage collection. Reports operations synopsis. </summary>
        /// <param name="timeout"> Specifies the time to wait for the instrument to return operation
        /// completed. </param>
        /// <param name="format">  Describes the format to use. </param>
        /// <param name="args">    A variable-length parameters list containing arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool CollectGarbageWaitCompleteThis( TimeSpan timeout, string format, params object[] args )
        {
            bool affirmative;
            try
            {
                this.Session.LastAction = this.PublishInfo( "Collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                this.Session.LastNodeNumber = new int?();
                // do a garbage collection
                this.Session.EnableServiceRequestWaitComplete();
                _ = this.WriteLine( this.CollectGarbageWaitCompleteCommand );
                affirmative = this.TraceVisaDeviceOperationOkay( true, "collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
            }
            catch ( Pith.NativeException ex )
            {
                this.TraceVisaOperation( ex, "collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                affirmative = false;
            }
            catch ( Exception ex )
            {
                this.TraceOperation( ex, "collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                affirmative = false;
            }

            if ( affirmative )
            {
                try
                {
                    _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( timeout ).Status );
                    affirmative = this.TraceVisaDeviceOperationOkay( true, "awaiting completion after collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                }
                catch ( Pith.NativeException ex )
                {
                    this.TraceVisaOperation( ex, "awaiting completion after collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                    affirmative = false;
                }
                catch ( Exception ex )
                {
                    this.TraceOperation( ex, "awaiting completion after collecting garbage after {0};. ", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                    affirmative = false;
                }
            }

            return affirmative;
        }

        #endregion

        #region " LINE FREQUENCY "

        /// <summary> Gets or sets the station line frequency. </summary>
        /// <remarks> This value is shared and used for all systems on this station. </remarks>
        /// <value> The station line frequency. </value>
        public static double? StationLineFrequency { get; set; } = new double?();

        /// <summary> The line frequency. </summary>
        private double? _LineFrequency;

        /// <summary> Gets or sets the line frequency. </summary>
        /// <value> The line frequency. </value>
        public double? LineFrequency
        {
            get => this._LineFrequency;

            set {
                if ( !Nullable.Equals( this.LineFrequency, value ) )
                {
                    StationLineFrequency = value;
                    this._LineFrequency = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the default line frequency. </summary>
        /// <value> The default line frequency. </value>
        public static double DefaultLineFrequency { get; set; } = 60d;

        /// <summary> Gets or sets line frequency query command. </summary>
        /// <value> The line frequency query command. </value>
        protected virtual string LineFrequencyQueryCommand { get; set; } = Pith.Scpi.Syntax.ReadLineFrequencyCommand;

        /// <summary> Reads the line frequency. </summary>
        /// <remarks> Sends the <see cref="LineFrequencyQueryCommand"/> query. </remarks>
        /// <returns> System.Nullable{System.Double}. </returns>
        public double? QueryLineFrequency()
        {
            if ( !this.LineFrequency.HasValue )
            {
                this.LineFrequency = string.IsNullOrWhiteSpace( this.LineFrequencyQueryCommand )
                    ? DefaultLineFrequency
                    : this.Session.Query( this.LineFrequency.GetValueOrDefault( DefaultLineFrequency ), this.LineFrequencyQueryCommand );
            }

            return this.LineFrequency;
        }

        /// <summary> Converts power line cycles to time span. </summary>
        /// <param name="powerLineCycles"> The power line cycles. </param>
        /// <returns>
        /// The integration period corresponding to the specified number of power line .
        /// </returns>
        public static TimeSpan FromPowerLineCycles( double powerLineCycles )
        {
            return FromPowerLineCycles( powerLineCycles, StationLineFrequency.GetValueOrDefault( DefaultLineFrequency ) );
        }

        /// <summary> Converts integration period to Power line cycles. </summary>
        /// <param name="integrationPeriod"> The integration period. </param>
        /// <returns> The number of power line cycles corresponding to the integration period. </returns>
        public static double ToPowerLineCycles( TimeSpan integrationPeriod )
        {
            return ToPowerLineCycles( integrationPeriod, StationLineFrequency.GetValueOrDefault( DefaultLineFrequency ) );
        }

        /// <summary> Converts power line cycles to time span. </summary>
        /// <param name="powerLineCycles"> The power line cycles. </param>
        /// <param name="frequency">       The frequency. </param>
        /// <returns>
        /// The integration period corresponding to the specified number of power line .
        /// </returns>
        public static TimeSpan FromPowerLineCycles( double powerLineCycles, double frequency )
        {
            return FromSecondsPrecise( powerLineCycles / frequency );
        }

        /// <summary> Converts integration period to Power line cycles. </summary>
        /// <param name="integrationPeriod"> The integration period. </param>
        /// <param name="frequency">         The frequency. </param>
        /// <returns> The number of power line cycles corresponding to the integration period. </returns>
        public static double ToPowerLineCycles( TimeSpan integrationPeriod, double frequency )
        {
            return TotalSecondsPrecise( integrationPeriod ) * frequency;
        }

        /// <summary> Total seconds precise. </summary>
        /// <param name="timespan"> The timespan. </param>
        /// <returns> The total number of seconds precise. </returns>
        public static double TotalSecondsPrecise( TimeSpan timespan )
        {
            return timespan.Ticks / ( double ) TimeSpan.TicksPerSecond;
        }

        /// <summary> Converts seconds to time span with tick timespan accuracy. </summary>
        /// <param name="seconds"> The seconds. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan FromSecondsPrecise( double seconds )
        {
            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerSecond * seconds) );
        }

        #endregion

    }
}
