using System;

namespace isr.VI
{
    public abstract partial class StatusSubsystemBase
    {

        /// <summary>   Executes the command after waiting for status ready. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="readyToQueryTimeout">  The ready to query timeout. </param>
        /// <param name="dataToWrite">          The data to write. </param>
        /// <returns>   <see cref="ExecuteInfo"/> </returns>
        public ExecuteInfo ExecuteStatusReady( TimeSpan readyToQueryTimeout, string dataToWrite )
        {
            return string.IsNullOrWhiteSpace( dataToWrite )
                ? ExecuteInfo.Empty
                : this.Session.ExecuteStatusReady( readyToQueryTimeout, dataToWrite, ( statusByte ) => !this.IsStatusBusy( statusByte ), this.IsStatusError );
        }

    }
}
