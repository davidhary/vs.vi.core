using System;

namespace isr.VI
{
    public abstract partial class StatusSubsystemBase
    {

        #region " DEVICE ERRORS: CLEAR "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <remarks>
        /// SCPI: ":STAT:QUE:CLEAR".
        /// <see cref="VI.Pith.Scpi.Syntax.ClearErrorQueueCommand"> </see>
        /// </remarks>
        /// <value> The clear error queue command. </value>
        protected virtual string ClearErrorQueueCommand { get; set; } = Pith.Scpi.Syntax.ClearSystemErrorQueueCommand;

        /// <summary> Clears messages from the error queue. </summary>
        /// <remarks>
        /// Sends the <see cref="ClearErrorQueueCommand">clear error queue</see> message.
        /// </remarks>
        public virtual void ClearErrorQueue()
        {
            this.ClearErrorCache();
            if ( !string.IsNullOrWhiteSpace( this.ClearErrorQueueCommand ) )
                this.Session.Execute( this.ClearErrorQueueCommand );
            this.ErrorQueueCount = new int?();
        }

        /// <summary> Clears the error cache. </summary>
        public virtual void ClearErrorCache()
        {
            this.DeviceErrorQueue.Clear();
            this.DeviceErrorBuilder = new System.Text.StringBuilder();
        }

        /// <summary> Gets or sets the last message that was sent before the error. </summary>
        /// <value> The message sent before error. </value>
        public string MessageSentBeforeError { get; private set; }

        /// <summary> Gets or sets the last message that was received before the error. </summary>
        /// <value> The message received before error. </value>
        public string MessageReceivedBeforeError { get; private set; }

        #endregion

        #region " UNREAD MESSAGES "

        /// <summary> The builder of unread messages. </summary>
        private readonly System.Text.StringBuilder _UnreadMessagesBuilder = new();

        /// <summary> Gets the unread messages. </summary>
        /// <value> The orphan messages. </value>
        public string UnreadMessages => this._UnreadMessagesBuilder.ToString();

        /// <summary> Clears the unread messages. </summary>
        public void ClearUnreadMessages()
        {
            _ = this._UnreadMessagesBuilder.Clear();
        }

        /// <summary>   The last unread message. </summary>
        private string _LastUnreadMessage;

        /// <summary> Gets or sets the last unread message. </summary>
        /// <value> The last unread message. </value>
        public string LastUnreadMessage
        {
            get => this._LastUnreadMessage;

            set {
                this._LastUnreadMessage = value;
                if ( !string.IsNullOrWhiteSpace( value ) )
                    _ = this._UnreadMessagesBuilder.AppendLine( value );
                this.NotifyPropertyChanged();
                if ( !string.IsNullOrWhiteSpace( value ) )
                    this.NotifyPropertyChanged( nameof( this.UnreadMessages ) );
            }
        }

        /// <summary>   Discards unread messages. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        protected virtual void DiscardUnreadMessages()
        {
            while ( this.IsStatusDataReady( ( int ) this.Session.ReadStatusByte() ) )
            {
                this.LastUnreadMessage = this.Session.ReadFreeLine();
                _ = this.PublishWarning( $"{this.ResourceNameCaption} message {this.LastUnreadMessage} was added to the cache of orphan messages before reading device errors in order to prevent a Query Unterminated error" );
            }
        }

        /// <summary>   Discards unread messages using a short timeout. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="timeout">  The timeout. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types",
            Justification = "Timeout exception is used to enforce reading the output queue." )]
        public virtual void DiscardUnreadMessages( TimeSpan timeout )
        {
            try
            {
                this.Session.StoreCommunicationTimeout( timeout );
                this.DiscardUnreadMessages();
            }
            catch
            {
            }
            finally
            {
                this.Session.RestoreCommunicationTimeout();
                if ( this._UnreadMessagesBuilder.Length > 0 )
                    _ = this.PublishWarning( $"{this.ResourceNameCaption} had unread messages {this.UnreadMessages}" );
            }
        }

        #endregion

        #region " DEVICE ERRORS READ "

        /// <summary>
        /// Queries any existing device errors. This assumes that error available bits where detected on
        /// the status register.
        /// </summary>
        protected virtual void QueryExistingDeviceErrors()
        {
            if ( this.ReadingDeviceErrors )
                return;
            if ( this.MessageAvailable )
            {
                // Developer: This is a bug and run condition; 
                // On reading the status register, the Session Base class is expected to turn off 
                // the error available flag if an message is present in the presence of an error.
                // Please check if a run condition had occurred causing the message to appear after the status
                // register was read as indicating that no such message existed.
                this.LastUnreadMessage = this.Session.ReadLineTrimEnd();
                _ = this.PublishWarning( $"{this.ResourceNameCaption} message {this.LastUnreadMessage} was added to the cache of orphan messages before reading device errors in order to prevent a Query Unterminated error" );
            }
            // There are currently two queue reading commands and two single error reading commands.
            if ( !string.IsNullOrWhiteSpace( this.NextDeviceErrorQueryCommand ) )
            {
                _ = this.QueryDeviceErrors();
            }
            else if ( !string.IsNullOrWhiteSpace( this.DequeueErrorQueryCommand ) )
            {
                _ = this.QueryErrorQueue();
            }

            if ( !string.IsNullOrWhiteSpace( this.DeviceErrorQueryCommand ) )
            {
                if ( string.IsNullOrWhiteSpace( this.NextDeviceErrorQueryCommand ) && string.IsNullOrWhiteSpace( this.DequeueErrorQueryCommand ) )
                {
                    // If No Queue reading commands, 
                    // clear the cache as this device reports only the last error 
                    this.ClearErrorCache();
                }

                _ = this.QueryLastError();
            }
            else if ( !string.IsNullOrWhiteSpace( this.LastSystemErrorQueryCommand ) )
            {
                if ( string.IsNullOrWhiteSpace( this.NextDeviceErrorQueryCommand ) && string.IsNullOrWhiteSpace( this.DequeueErrorQueryCommand ) )
                {
                    // If No Queue reading commands, 
                    // clear the cache as this device reports only the last error 
                    this.ClearErrorCache();
                }

                this.WriteLastSystemErrorQueryCommand();
                Core.ApplianceBase.DoEvents();
                _ = this.ReadLastSystemError();
            }
        }

        /// <summary> Attempts to clear error cache from the given data. </summary>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryClearErrorCache()
        {
            string activity = string.Empty;
            (bool Success, string Details) result = (true, string.Empty);
            try
            {
                activity = "checking the error available";
                activity = "clearing error cache";
                this.ClearErrorCache();
            }
            catch ( Exception ex )
            {
                result = (false, this.PublishException( activity, ex ));
            }

            return result;
        }

        /// <summary> Queries device errors. </summary>
        /// <returns> (Success, Details) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryQueryExistingDeviceErrors()
        {
            string activity = "reading device errors";
            (bool Success, string Details) result = (true, string.Empty);
            try
            {
                activity = "checking if already reading error";
                if ( this.ErrorAvailable && !this.ReadingDeviceErrors )
                {
                    activity = "reading device errors";
                    this.QueryExistingDeviceErrors();
                }
            }
            catch ( Exception ex )
            {
                result = (false, this.PublishException( activity, ex ));
            }

            return result;
        }

        /// <summary> Gets or sets the device errors queue. </summary>
        /// <value> The device errors. </value>
        public DeviceErrorQueue DeviceErrorQueue { get; private set; }

        /// <summary> Gets or sets a message describing the no error compound message. </summary>
        /// <value> A message describing the no error compound. </value>
        public string NoErrorCompoundMessage { get; set; }

        /// <summary> True to reading device errors. </summary>
        private bool _ReadingDeviceErrors;

        /// <summary> Gets or sets the reading device errors. </summary>
        /// <value> The reading device errors. </value>
        public bool ReadingDeviceErrors
        {
            get => this._ReadingDeviceErrors;

            protected set {
                if ( value != this.ReadingDeviceErrors )
                {
                    this._ReadingDeviceErrors = value;
                    this.NotifyPropertyChanged();
                    if ( value )
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} Reading device errors;. " );
                }
            }
        }

        #endregion

        #region " DEVICE ERRORS: REPORT "

        /// <summary> True if has error report, false if not. </summary>
        private bool _HasErrorReport;

        /// <summary> Gets or sets the has error report. </summary>
        /// <value> The has error report. </value>
        public bool HasErrorReport
        {
            get => this._HasErrorReport;

            set {
                if ( value != this.HasErrorReport )
                {
                    this._HasErrorReport = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The device error report. </summary>
        private string _DeviceErrorReport;

        /// <summary> Gets or sets the device error report. </summary>
        /// <value> The device error report. </value>
        public string DeviceErrorReport
        {
            get => this._DeviceErrorReport;

            set {
                if ( !string.Equals( value, this.DeviceErrorReport ) )
                {
                    this._DeviceErrorReport = value;
                    this.NotifyPropertyChanged();
                    this.HasErrorReport = !string.IsNullOrEmpty( value );
                }
            }
        }

        /// <summary> builds the device error report. </summary>
        /// <returns> A String. </returns>
        protected string BuildDeviceErrorReport()
        {
            var builder = new System.Text.StringBuilder( this.DeviceErrorBuilder.ToString() );
            if ( builder.Length > 0 )
            {
                if ( !Equals( this.Session.StandardEventStatus, 0 ) )
                {
                    string report = Pith.SessionBase.BuildReport( this.Session.StandardEventStatus.Value, ";" );
                    if ( !string.IsNullOrWhiteSpace( report ) )
                    {
                        _ = builder.AppendLine( report );
                    }

                    if ( !string.IsNullOrWhiteSpace( this.MessageReceivedBeforeError ) )
                    {
                        _ = builder.AppendLine( $"Received: {this.MessageReceivedBeforeError}" );
                    }

                    if ( !string.IsNullOrWhiteSpace( this.MessageSentBeforeError ) )
                    {
                        _ = builder.AppendLine( $"Sent: {this.MessageSentBeforeError}" );
                    }
                }

                return builder.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary> True if has device error, false if not. </summary>
        private bool _HasDeviceError;

        /// <summary> Gets or sets the has device error. </summary>
        /// <value> The has device error. </value>
        public bool HasDeviceError
        {
            get => this._HasDeviceError;

            set {
                if ( value != this.HasDeviceError )
                {
                    this._HasDeviceError = value;
                    this.NotifyPropertyChanged();
                    this.ErrorForeColor = SelectErrorColor( value );
                }
            }
        }

        /// <summary> Message describing the compound error. </summary>
        private string _CompoundErrorMessage = string.Empty;

        /// <summary> Gets or sets a message describing the compound error. </summary>
        /// <value> A message describing the compound error. </value>
        public string CompoundErrorMessage
        {
            get => this._CompoundErrorMessage;

            set {
                if ( !string.Equals( value, this.CompoundErrorMessage ) )
                {
                    this._CompoundErrorMessage = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Select error color. </summary>
        /// <param name="isError"> True if is error, false if not. </param>
        /// <returns> A Drawing.Color. </returns>
        protected static System.Drawing.Color SelectErrorColor( bool isError )
        {
            return isError ? System.Drawing.Color.OrangeRed : System.Drawing.Color.Aquamarine;
        }

        /// <summary> The error foreground color. </summary>
        private System.Drawing.Color _ErrorForeColor = System.Drawing.Color.White;

        /// <summary> Gets or sets the color of the error foreground. </summary>
        /// <value> The color of the error foreground. </value>
        public System.Drawing.Color ErrorForeColor
        {
            get => this._ErrorForeColor;

            set {
                if ( !Equals( value, this._ErrorForeColor ) )
                {
                    this._ErrorForeColor = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Clears the error report. </summary>
        public virtual void ClearErrorReport()
        {
            this.DeviceErrorReport = string.Empty;
            this.CompoundErrorMessage = string.Empty;
            this.HasDeviceError = false;
            this.MessageSentBeforeError = string.Empty;
            this.MessageReceivedBeforeError = string.Empty;
        }

        /// <summary> Reports last error. </summary>
        protected void ReportLastError()
        {
            this.DeviceErrorReport = this.BuildDeviceErrorReport();
            var lastError = this.DeviceErrorQueue.LastError;
            this.CompoundErrorMessage = lastError.CompoundErrorMessage;
            this.HasDeviceError = lastError.IsError;
            if ( this.HasDeviceError )
                _ = this.PublishWarning( $"{this.ResourceNameCaption} error;. {this.CompoundErrorMessage}" );
            if ( this.HasErrorReport )
                _ = this.PublishWarning( $"{this.ResourceNameCaption} errors;. {this.DeviceErrorReport}" );
        }

        #endregion

        #region " DEVICE ERRORS: PROTECTED QUERY METHODS AND MEMBERS "

        /// <summary> The device errors. </summary>
        /// <value> The device error builder. </value>
        protected System.Text.StringBuilder DeviceErrorBuilder { get; set; }

        /// <summary> Appends a device error message. </summary>
        /// <param name="value"> The value. </param>
        public void AppendDeviceErrorMessage( string value )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
                _ = this.DeviceErrorBuilder.AppendLine( value );
        }

        /// <summary> Enqueues device error. </summary>
        /// <param name="compoundErrorMessage"> Message describing the compound error. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected virtual DeviceError EnqueueDeviceError( string compoundErrorMessage )
        {
            var de = new DeviceError( this.NoErrorCompoundMessage );
            de.Parse( compoundErrorMessage );
            if ( de.IsError )
                this.DeviceErrorQueue.Enqueue( de );
            return de;
        }

        /// <summary> Enqueue last error. </summary>
        /// <param name="compoundErrorMessage"> Message describing the compound error. </param>
        /// <returns> A DeviceError. </returns>
        public DeviceError EnqueueLastError( string compoundErrorMessage )
        {
            var de = this.EnqueueDeviceError( compoundErrorMessage );
            if ( de.IsError )
            {
                this.AppendDeviceErrorMessage( $@"{this.ResourceNameCaption} Last Error:
{de.ErrorMessage}" );
                this.ReportLastError();
            }

            return de;
        }

        #endregion

        #region " QUERY DEVICE ERROR: USES NEXT ERROR COMMANDS :STAT:QUE? "

        /// <summary> Gets or sets the 'Next Error' query command. </summary>
        /// <remarks>
        /// SCPI: ":STAT:QUE?".
        /// <see cref="VI.Pith.Scpi.Syntax.NextErrorQueryCommand"> </see>
        /// </remarks>
        /// <value> The error queue query command. </value>
        protected virtual string NextDeviceErrorQueryCommand { get; set; }

        /// <summary> Reads the device errors. </summary>
        /// <returns> The device errors. </returns>
        protected virtual string QueryDeviceErrors()
        {
            if ( this.ReadingDeviceErrors )
                return string.Empty;
            bool errorsRecorded = false;
            try
            {
                this.MessageSentBeforeError = this.Session.LastMessageSent;
                this.MessageReceivedBeforeError = this.Session.LastMessageReceived;
                this.ReadingDeviceErrors = true;
                this.ClearErrorCache();
                if ( !string.IsNullOrWhiteSpace( this.NextDeviceErrorQueryCommand ) && this.Session.IsErrorBitSet() )
                {
                    var builder = new System.Text.StringBuilder();
                    var de = DeviceError.NoError;
                    do
                    {
                        de = this.EnqueueDeviceError( this.Session.QueryTrimEnd( this.NextDeviceErrorQueryCommand ) );
                        if ( de.IsError )
                        {
                            errorsRecorded = true;
                            _ = builder.AppendLine( de.CompoundErrorMessage );
                        }
                    }
                    while ( this.Session.IsErrorBitSet() && de.IsError );
                    // this is a kludge because the 7510 does not clear the error queue.
                    if ( !de.IsError && this.Session.IsErrorBitSet() )
                        this.Session.Execute( this.ClearErrorQueueCommand );
                    if ( builder.Length > 0 )
                    {
                        _ = this.Session.QueryStandardEventStatus();
                        this.AppendDeviceErrorMessage( builder.ToString() );
                    }
                }

                if ( errorsRecorded )
                    this.ReportLastError();
                return this.DeviceErrorReport;
            }
            catch
            {
                throw;
            }
            finally
            {
                this.ReadingDeviceErrors = false;
            }
        }

        #endregion

        #region " QUERY DEVICE ERROR: USES DEQUEUE COMMANDS ?? "

        /// <summary> Gets or sets the 'Next Error' query command. </summary>
        /// <value> The error queue query command. </value>
        protected virtual string DequeueErrorQueryCommand { get; set; }

        /// <summary> Queries the next error from the device error queue. </summary>
        /// <returns> The <see cref="DeviceError">Device Error structure.</see> </returns>
        protected DeviceError QueryNextError()
        {
            var de = new DeviceError( this.NoErrorCompoundMessage );
            if ( !string.IsNullOrWhiteSpace( this.DequeueErrorQueryCommand ) )
            {
                de = this.EnqueueDeviceError( this.Session.QueryTrimEnd( this.DequeueErrorQueryCommand ) );
            }

            return de;
        }

        /// <summary> Reads the device errors. </summary>
        /// <returns> The device errors. </returns>
        protected virtual string QueryErrorQueue()
        {
            if ( this.ReadingDeviceErrors )
                return string.Empty;
            bool errorsRecorded = false;
            try
            {
                this.MessageSentBeforeError = this.Session.LastMessageSent;
                this.MessageReceivedBeforeError = this.Session.LastMessageReceived;
                this.ReadingDeviceErrors = true;
                this.ClearErrorCache();
                if ( !string.IsNullOrWhiteSpace( this.DequeueErrorQueryCommand ) && this.Session.IsErrorBitSet() )
                {
                    var builder = new System.Text.StringBuilder();
                    var de = DeviceError.NoError;
                    do
                    {
                        de = this.QueryNextError();
                        if ( de.IsError )
                        {
                            errorsRecorded = true;
                            _ = builder.AppendLine( de.CompoundErrorMessage );
                        }
                    }
                    while ( this.Session.IsErrorBitSet() && de.IsError );

                    // this is a kludge because the 7510 does not clear the error queue.
                    if ( !de.IsError && this.Session.IsErrorBitSet() )
                        this.Session.Execute( this.ClearErrorQueueCommand );
                    if ( builder.Length > 0 )
                    {
                        _ = this.Session.QueryStandardEventStatus();
                        this.AppendDeviceErrorMessage( builder.ToString() );
                    }
                }

                if ( errorsRecorded )
                    this.ReportLastError();
                return this.DeviceErrorReport;
            }
            catch
            {
                throw;
            }
            finally
            {
                this.ReadingDeviceErrors = false;
            }
        }

        #endregion

        #region " LAST ERROR "

        /// <summary> Gets or sets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected virtual string DeviceErrorQueryCommand { get; set; }

        /// <summary> Queries the last error from the device. </summary>
        /// <returns> The <see cref="DeviceError">Device Error structure.</see> </returns>
        protected virtual DeviceError QueryLastError()
        {
            var de = new DeviceError( this.NoErrorCompoundMessage );
            if ( !string.IsNullOrWhiteSpace( this.DeviceErrorQueryCommand ) )
            {
                de = this.EnqueueDeviceError( this.Session.QueryTrimEnd( this.DeviceErrorQueryCommand ) );
                if ( de.IsError )
                {
                    this.AppendDeviceErrorMessage( $@"{this.ResourceNameCaption} Last Error:
{de.ErrorMessage}" );
                    this.ReportLastError();
                }
            }

            return de;
        }

        #endregion

        #region " LAST SYSTEM ERROR "

        /// <summary> Gets or sets the last system error query command. </summary>
        /// <value> The last error query command. </value>
        protected virtual string LastSystemErrorQueryCommand { get; set; }

        /// <summary> Reads last error. </summary>
        /// <returns> The last error. </returns>
        protected DeviceError ReadLastSystemError()
        {
            var de = new DeviceError( this.NoErrorCompoundMessage );
            if ( !string.IsNullOrWhiteSpace( this.LastSystemErrorQueryCommand ) )
            {
                de = this.EnqueueLastError( this.Session.ReadLine() );
            }

            return de;
        }

        /// <summary> Writes the last error query command. </summary>
        protected void WriteLastSystemErrorQueryCommand()
        {
            if ( !string.IsNullOrWhiteSpace( this.LastSystemErrorQueryCommand ) )
            {
                _ = this.Session.WriteLine( this.LastSystemErrorQueryCommand );
            }
        }

        #endregion

        #region " ERROR QUEUE COUNT "

        /// <summary> The ErrorQueueCount. </summary>
        private int? _ErrorQueueCount;

        /// <summary> Gets or sets the cached error queue count. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? ErrorQueueCount
        {
            get => this._ErrorQueueCount;

            protected set {
                if ( !Nullable.Equals( this.ErrorQueueCount, value ) )
                {
                    this._ErrorQueueCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets The ErrorQueueCount query command. </summary>
        /// <value> The ErrorQueueCount query command. </value>
        protected virtual string ErrorQueueCountQueryCommand { get; set; }

        /// <summary> Queries The ErrorQueueCount. </summary>
        /// <returns> The ErrorQueueCount or none if unknown. </returns>
        public int? QueryErrorQueueCount()
        {
            this.ErrorQueueCount = this.Query( this.ErrorQueueCount, this.ErrorQueueCountQueryCommand );
            return this.ErrorQueueCount;
        }

        #endregion

    }
}
