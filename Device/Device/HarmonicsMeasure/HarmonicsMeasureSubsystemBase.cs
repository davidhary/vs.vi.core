using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;

using isr.Core.EscapeSequencesExtensions;

namespace isr.VI
{

    /// <summary>
    /// Defines the contract that must be implemented by a Harmonics Measure Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class HarmonicsMeasureSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="HarmonicsMeasureSubsystemBase" /> class.
        /// </summary>
        /// <remarks>   David, 2020-07-28. </remarks>
        /// <param name="statusSubsystem">  A reference to a <see cref="StatusSubsystemBase">status
        ///                                 subsystem</see>. </param>
        /// <param name="readingAmounts">   The reading amounts. </param>
        protected HarmonicsMeasureSubsystemBase( StatusSubsystemBase statusSubsystem, ReadingAmounts readingAmounts ) : base( statusSubsystem )
        {
            this.ReadingAmounts = readingAmounts;
            this.DefaultMeasurementUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.DefaultMeasureModeDecimalPlaces = 3;
            this.GeneratorOutputLevelRange = new Core.Primitives.RangeR( double.MinValue, double.MaxValue );
            this.GeneratorTimerRange = new Core.Primitives.RangeI( int.MinValue, int.MaxValue );
            this.VoltmeterHighLimitRange = new Core.Primitives.RangeR( double.MinValue, double.MaxValue );
            this.VoltmeterLowLimitRange = new Core.Primitives.RangeR( double.MinValue, double.MaxValue );

            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultMeasureModeDecimalPlaces;
            this.DefineMeasureModeDecimalPlaces();
            this.DefineMeasureModeReadWrites();
            this.DefineMeasureModeRanges();
            this.DefineMeasureModeUnits();

        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.DefineFunctionClearKnownState();
            this.NotifyPropertyChanged( nameof( this.ReadingAmounts ) );
        }

        /// <summary>   Sets the known initial post reset state. </summary>
        /// <remarks>   Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            _ = this.ParsePrimaryReading( string.Empty );
        }

        #endregion

        #region " QUERY MANAGEMENT "

        /// <summary>   Gets or sets the ready to query timeout. </summary>
        /// <value> The ready to query timeout. </value>
        public TimeSpan ReadyToQueryTimeout { get; set; } = TimeSpan.FromMilliseconds( 100 );

        /// <summary>   Gets or sets the ready to read timeout. </summary>
        /// <value> The ready to read timeout. </value>
        public TimeSpan ReadyToReadTimeout { get; set; } = TimeSpan.FromMilliseconds( 10 );

        /// <summary>   Gets or sets the ready to write timeout. </summary>
        /// <value> The ready to write timeout. </value>
        public TimeSpan ReadyToWriteTimeout { get; set; } = TimeSpan.FromMilliseconds( 20 );

        #endregion

        #region " CLEAR, TRIGGER, FETCH, FETCH, MEASURE "

        /// <summary>   Gets or sets the 'clear known state' command. </summary>
        /// <value> The 'clear known state' command. </value>
        protected virtual string ClearKnownStateCommand { get; set; }

        /// <summary>   Gets or sets the clear known state refractory time span. </summary>
        /// <value> The clear known state refractory time span. </value>
        public abstract TimeSpan ClearKnownStateRefractoryTimeSpan { get; set; }

        /// <summary>   Clears the known state. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <returns>   <see cref="ExecuteInfo"/>. </returns>
        public virtual ExecuteInfo ClearKnownState()
        {
            this.DefineClearExecutionState();
            ExecuteInfo executeInfo = this.ExecuteElapsed( this.ClearKnownStateCommand );
            if ( !String.IsNullOrWhiteSpace( this.ClearKnownStateCommand ) )
                Core.ApplianceBase.DoEventsWait( this.ClearKnownStateRefractoryTimeSpan );
            return executeInfo;
        }

        /// <summary>   Gets or sets the Trigger command. </summary>
        /// <remarks>   SCPI: '*TRG'. </remarks>
        /// <value> The Trigger command. </value>
        protected virtual string TriggerCommand { get; set; }

        /// <summary>   Triggers a single short. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   <see cref="ExecuteInfo"/>. </returns>
        public virtual ExecuteInfo AssertTrigger( bool checkStatus = false )
        {
            return checkStatus
                ? this.ExecuteStatusReady( this.ReadyToQueryTimeout, this.TriggerCommand )
                : this.ExecuteElapsed( this.TriggerCommand );
        }

        /// <summary>   Gets or sets the fetch command. </summary>
        /// <remarks>   SCPI: 'FETCh?'. </remarks>
        /// <value> The fetch command. </value>
        public virtual string FetchCommand { get; set; }

        /// <summary>   Converts a readInfo to a query information. </summary>
        /// <remarks>   David, 2021-04-16. </remarks>
        /// <param name="result">   The result. </param>
        /// <returns>   Result as a Tuple. </returns>
        private (string receivedMessage, QueryInfo queryInfo) ToQueryInfo( (string ReceivedMessage, ReadInfo ReadInfo) result )
        {
            return (result.ReceivedMessage, new QueryInfo( result.ReadInfo ));
        }

        /// <summary>   Fetches the reading. </summary>
        /// <remarks>
        /// Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
        /// there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
        /// the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
        /// Buffer. Thus, subsequent executions of FETCh? acquire the same data.
        /// </remarks>
        /// <returns>   (double? ParsedValue, QueryParseInfo{double} queryParseInfo) </returns>
        public virtual (double? ParsedValue, QueryParseInfo<double> queryParseInfo) FetchReading( bool checkStatus = false )
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            (string receivedMessage, QueryInfo queryInfo) =
                checkStatus
                ? String.IsNullOrEmpty( this.FetchCommand )
                    ? this.ToQueryInfo( this.ReadIfStatusDataReady( this.ReadyToReadTimeout ) )
                    : this.QueryIfStatusDataReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.FetchCommand )
                : String.IsNullOrEmpty( this.FetchCommand )
                    ? this.ToQueryInfo( this.ReadElapsed() )
                    : this.QueryElapsed( this.FetchCommand );
            if ( string.IsNullOrWhiteSpace( receivedMessage ) )
            {
                return (new double?(), new QueryParseInfo<double>( false, default, this.PrimaryReading.ReadingCaption, queryInfo ));
            }
            else
            {
                double? parsedValue = this.ParsePrimaryReading( receivedMessage );
                return (parsedValue, new QueryParseInfo<double>( parsedValue.HasValue, parsedValue.GetValueOrDefault( default ),
                                    this.PrimaryReading.ReadingCaption, queryInfo ));
            }
        }

        /// <summary>   Gets or sets the read command. </summary>
        /// <remarks>   SCPI: 'READ'. </remarks>
        /// <value> The read command. </value>
        protected virtual string ReadCommand { get; set; }

        /// <summary>   Initiates an operation and then fetches the data with status awareness. </summary>
        /// <remarks>
        /// Issues the 'READ?' query, which performs a trigger initiation and then a
        /// <see cref="FetchReading(bool)">Fetch Reading</see>
        /// The initiate triggers a new measurement cycle which puts new data in the Sample Buffer. Fetch
        /// reads that new data. The
        /// <see cref="VI.HarmonicsMeasureSubsystemBase.QueryParseReadingAmounts(string)">Measure</see>
        /// command places the instrument in a “one-shot” mode and then performs a read.
        /// </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// (double? ParsedValue, ExecuteInfo ExecuteInfo, QueryParseInfo{double} queryParseInfo)
        /// </returns>
        public virtual (double? ParsedValue, ExecuteInfo ExecuteInfo, QueryParseInfo<double> queryParseInfo) Read( bool checkStatus = false )
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            ExecuteInfo executeInfo = this.AssertTrigger( checkStatus );
            (double? parsedValue, QueryParseInfo<double> queryParseInfo) = this.FetchReading( checkStatus );
            return (parsedValue, executeInfo, queryParseInfo);
        }

        /// <summary>   Trigger fetch with status awareness. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// (double? ParsedValue, ExecuteInfo ExecuteInfo, QueryParseInfo{double} queryParseInfo)
        /// </returns>
        public virtual (double? ParsedValue, ExecuteInfo ExecuteInfo, QueryParseInfo<double> queryParseInfo) TriggerFetch( bool checkStatus = false )
        {
            return this.Read( checkStatus );
        }

        /// <summary>   Gets or sets The Measure query command. </summary>
        /// <value> The Measure query command. </value>
        protected virtual string MeasureQueryCommand { get; set; }

        /// <summary>   Queries readings into the reading amounts with status awareness. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <returns>   The reading or none if unknown. </returns>
        public virtual double? QueryParseReadingAmounts()
        {
            return this.QueryParseReadingAmounts( this.MeasureQueryCommand );
        }

        /// <summary>
        /// Query a measured value from the instrument with status awareness. Does not use
        /// <see cref="HarmonicsMeasureSubsystemBase.ReadingAmounts"/>.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <returns>   The reading or none if unknown. </returns>
        public virtual double? QueryParsePrimaryReading()
        {
            return this.QueryParsePrimaryReading( this.MeasureQueryCommand );
        }

        /// <summary>   Estimates the lower bound on measurement time. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <returns>   A TimeSpan. </returns>
        public virtual TimeSpan EstimateMeasurementTime()
        {
            return TimeSpan.FromMilliseconds( this.GeneratorTimer.GetValueOrDefault( 6 ) );
        }

        #endregion

        #region " STREAMING "

        /// <summary>   Adds a buffer reading. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>   An int. </returns>
        public bool AddBufferReading( string receivedMessage )
        {
            var (Success, _, _) = this.ParsePrimaryReadingFull( receivedMessage );
            if ( Success )
            {
                var newReading = new MeasuredAmount( this.PrimaryReading );
                this.BufferReadingsBindingList.Add( newReading );
                this.EnqueueRange( new MeasuredAmount[] { newReading } );
                this.NotifyPropertyChanged( nameof( this.BufferReadingsCount ) );
                this.NotifyPropertyChanged( nameof( this.NewReadingsCount ) );
            }
            return Success;
        }

        #endregion

        #region " BUFFER READINGS "

        /// <summary>   Gets or sets a list of buffer readings bindings. </summary>
        /// <value> A list of buffer readings bindings. </value>
        public Core.BindingLists.InvokingBindingList<MeasuredAmount> BufferReadingsBindingList { get; private set; } = new();

        /// <summary>   Gets the number of buffer readings. </summary>
        /// <value> The number of buffer readings. </value>
        public int BufferReadingsCount => this.BufferReadingsBindingList.Count;

        /// <summary>   Gets or sets the items locker. </summary>
        /// <value> The items locker. </value>
        protected ReaderWriterLockSlim ItemsLocker { get; private set; } = new ReaderWriterLockSlim();

        /// <summary>   Gets or sets a queue of new buffer readings. </summary>
        /// <value> A thread safe Queue of buffer readings. </value>
        public System.Collections.Concurrent.ConcurrentQueue<MeasuredAmount> NewBufferReadingsQueue { get; private set; }

        /// <summary>   Clears the buffer readings queue. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        public void ClearBufferReadingsQueue()
        {
            this.NewBufferReadingsQueue = new System.Collections.Concurrent.ConcurrentQueue<MeasuredAmount>();
        }

        /// <summary>   Gets the number of new readings. </summary>
        /// <value> The number of new readings. </value>
        public int NewReadingsCount
        {
            get {
                try
                {
                    this.ItemsLocker.EnterReadLock();
                    return this.NewBufferReadingsQueue.Count;
                }
                finally
                {
                    this.ItemsLocker.ExitReadLock();
                }
            }
        }

        /// <summary>   Enqueue range. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="items">    The items. </param>
        public void EnqueueRange( IList<MeasuredAmount> items )
        {
            if ( items is null || !items.Any() )
                return;
            this.ItemsLocker.EnterWriteLock();
            try
            {
                foreach ( MeasuredAmount item in items )
                    this.NewBufferReadingsQueue.Enqueue( item );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
        }

        /// <summary>   Dequeues the specified number of new values. </summary>
        /// <remarks>   David, 2020-08-01. </remarks>
        /// <param name="count">    Number of values to dequeue. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process dequeue range in this collection.
        /// </returns>
        public IList<MeasuredAmount> DequeueRange( int count )
        {
            var result = new List<MeasuredAmount>();
            try
            {
                this.ItemsLocker.EnterReadLock();
                MeasuredAmount value = null;
                while ( this.NewBufferReadingsQueue.Any() & count > 0 )
                {
                    if ( this.NewBufferReadingsQueue.TryDequeue( out value ) )
                    {
                        result.Add( value );
                        count -= 1;
                    }
                }
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }

            return result;
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary>   Auto Range enabled. </summary>
        private bool? _AutoRangeEnabled;

        /// <summary>   Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public virtual bool? AutoRangeEnabled
        {
            get => this._AutoRangeEnabled;

            set {
                if ( !Equals( this.AutoRangeEnabled, value ) )
                {
                    this._AutoRangeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Auto Range Enabled sentinel. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">    if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns>  A Tuple: (bool? ParsedValue, WriteBooleanInfo WriteBooleanInfo, QueryParseBooleanInfo QueryParseBooleanInfo). </returns>
        public (bool? ParsedValue, WriteBooleanInfo WriteBooleanInfo, QueryParseBooleanInfo QueryParseBooleanInfo) ApplyAutoRangeEnabled( bool value )
        {
            (_, WriteBooleanInfo writeBooleanInfo) = this.WriteAutoRangeEnabled( value );
            (bool? parsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) = this.QueryAutoRangeEnabled();
            return (parsedValue, writeBooleanInfo, QueryParseBooleanInfo);
        }

        /// <summary>   Gets or sets the automatic Range enabled query command. </summary>
        /// <remarks>   SCPI: ":RANG:AUTO?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledQueryCommand { get; set; }

        /// <summary>   Writes the automatic range enabled query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteAutoRangeEnabledQueryCommand()
        {
            return this.WriteLineElapsed( this.AutoRangeEnabledQueryCommand );
        }

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="AutoRangeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <returns> A Tuple: (bool? ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) . </returns>
        public virtual (bool? ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryAutoRangeEnabled()
        {
            (bool? parsedValue, QueryParseBooleanInfo queryParseBooleanInfo) = this.QueryElapsed( this.AutoRangeEnabled, this.AutoRangeEnabledQueryCommand );
            this.AutoRangeEnabled = parsedValue;
            return (parsedValue, queryParseBooleanInfo);
        }

        /// <summary>   Gets or sets the automatic Range enabled command Format. </summary>
        /// <remarks>   SCPI: ":RANGE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        if set to <c>True</c> is enabled. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   (bool SentValue, WriteBooleanInfo WriteBooleanInfo) </returns>
        public virtual (bool SentValue, WriteBooleanInfo WriteBooleanInfo) WriteAutoRangeEnabled( bool value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.AutoRangeEnabledCommandFormat, value ) );
                this.AutoRangeEnabled = value;
                return (value, new WriteBooleanInfo( value, executeInfo ));
            }
            else
            {
                (bool sentValue, WriteBooleanInfo writeBooleanInfo) = this.WriteLineElapsed( value, this.AutoRangeEnabledCommandFormat );
                this.AutoRangeEnabled = sentValue;
                return (sentValue, writeBooleanInfo);
            }
        }

        #endregion

        #region " ACCESS RIGHTS MODE "

        /// <summary>   The access rights mode. </summary>
        private int? _AccessRightsMode;

        /// <summary>   Gets or sets the Access Rights Mode. </summary>
        /// <value> The Access Rights Mode. </value>
        public int? AccessRightsMode
        {
            get => this._AccessRightsMode;

            protected set {
                if ( !Nullable.Equals( this.AccessRightsMode, value ) )
                {
                    this._AccessRightsMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Access Rights Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Access Rights Mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple:  (int ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (int ParsedValue, WriteInfo<int> WriteInfo, QueryParseInfo<int> QueryParseInfo) ApplyAccessRightsMode( int value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteAccessRightsMode( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryAccessRightsMode( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Access Rights Mode query command. </summary>
        /// <value> The Access Rights Mode query command. </value>
        protected virtual string AccessRightsModeQueryCommand { get; set; }

        /// <summary>   Writes the access rights mode query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteAccessRightsModeQueryCommand()
        {
            return this.WriteLineElapsed( this.AccessRightsModeQueryCommand );
        }

        /// <summary>   Queries parse access rights mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        protected (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryParseAccessRightsMode( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.AccessRightsModeQueryCommand )
                : this.QueryElapsed( this.AccessRightsModeQueryCommand );
            var (hasValue, parseInfo) = this.TryParseAccessRightsMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<int>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.AccessRightsModeQueryCommand}'" );
        }

        /// <summary>   Parse access rights mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns> A Tuple: (int ParsedValue, <see cref="ParseInfo{T}"/>) </returns>
        public (int ParsedValue, ParseInfo<int> ParseInfo) ParseAccessRightsMode( string receivedMessage )
        {
            (bool hasValue, ParseInfo<int> parseInfo) = this.TryParseAccessRightsMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.AccessRightsModeQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse the access rights mode from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A Tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<int> ParseInfo) TryParseAccessRightsMode( string receivedMessage, out int parsedValue );

        /// <summary>   Queries The Access Rights Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryAccessRightsMode( bool checkStatus = false )
        {
            var reply = this.QueryParseAccessRightsMode( checkStatus );
            this.AccessRightsMode = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Access Rights Mode command format. </summary>
        /// <value> The Access Rights Mode command format. </value>
        protected virtual string AccessRightsModeCommandFormat { get; set; }

        /// <summary>
        /// Writes The Access Rights Mode without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Access Rights Mode. </remarks>
        /// <param name="value">        The AccessRightsMode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int SentValue, WriteInfo{int} WriteInfo) </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteAccessRightsMode( int value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.AccessRightsModeCommandFormat, value ) );
                this.AccessRightsMode = value;
                return (value, new WriteInfo<int>( value, executeInfo ));
            }
            else
            {
                (int sentValue, WriteInfo<int> writeInfo) = this.WriteLineElapsed( value, this.AccessRightsModeCommandFormat );
                this.AccessRightsMode = sentValue;
                return (sentValue, writeInfo);
            }
        }

        #endregion

        #region " BANDWIDTH LIMITING ENABLED "

        /// <summary>   The bandwidth limiting enabled. </summary>
        private bool? _BandwidthLimitingEnabled;

        /// <summary>   Gets or sets the bandwidth limiting enabled. </summary>
        /// <value> The bandwidth limiting enabled. </value>
        public bool? BandwidthLimitingEnabled
        {
            get => this._BandwidthLimitingEnabled;

            protected set {
                if ( !Equals( this.BandwidthLimitingEnabled, value ) )
                {
                    this._BandwidthLimitingEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the bandwidth limiting enabled sentinel. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        if set to <c>True</c> if enabling; False if disabling. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (bool ParsedValue, WriteBooleanInfo WriteBooleanInfo, QueryParseBooleanInfo
        /// QueryParseBooleanInfo)
        /// </returns>
        public (bool ParsedValue, WriteBooleanInfo WriteBooleanInfo, QueryParseBooleanInfo QueryParseBooleanInfo) ApplyBandwidthLimitingEnabled( bool value, bool checkStatus = false )
        {
            var (_, writeBooleanInfo) = this.WriteBandwidthLimitingEnabled( value, checkStatus );
            var (parsedValue, queryParseBooleanInfo) = this.QueryBandwidthLimitingEnabled( checkStatus );
            return (parsedValue, writeBooleanInfo, queryParseBooleanInfo);
        }

        /// <summary>   Gets or sets the bandwidth limiting enabled query command. </summary>
        /// <value> The bandwidth limiting enabled query command. </value>
        protected virtual string BandwidthLimitingEnabledQueryCommand { get; set; }

        /// <summary>   Writes the bandwidth limiting enabled query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteBandwidthLimitingEnabledQueryCommand()
        {
            return this.WriteLineElapsed( this.BandwidthLimitingEnabledQueryCommand );
        }

        /// <summary>   Queries parse bandwidth limiting enabled. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <returns>
        /// A tuple: (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo ParseBooleanInfo)
        /// </returns>
        protected (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryParseBandwidthLimitingEnabled( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.BandwidthLimitingEnabledQueryCommand )
                : this.QueryElapsed( this.BandwidthLimitingEnabledQueryCommand );
            var (hasValue, parseBooleanInfo) = this.TryParseBandwidthLimitingEnabled( receivedMessage, out bool parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseBooleanInfo( queryInfo, parseBooleanInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseBooleanInfo.ParsedMessage}' for query '{this.BandwidthLimitingEnabledQueryCommand}'" );
        }

        /// <summary>   Parse bandwidth limiting enabled. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>   A tuple: (bool ParsedValue, ParseBooleanInfo ParseBooleanInfo)  </returns>
        public (bool ParsedValue, ParseBooleanInfo ParseBooleanInfo) ParseBandwidthLimitingEnabled( string receivedMessage )
        {
            var (hasValue, parseBooleanInfo) = this.TryParseBandwidthLimitingEnabled( receivedMessage, out bool parsedValue );
            return hasValue
                ? (parsedValue, parseBooleanInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseBooleanInfo.ParsedMessage}' for query '{this.BandwidthLimitingEnabledQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse a bandwidth limiting enabled from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool Success, ParseBooleanInfo ParseBooleanInfo) </returns>
        public abstract (bool HasValue, ParseBooleanInfo ParseBooleanInfo) TryParseBandwidthLimitingEnabled( string receivedMessage, out bool parsedValue );

        /// <summary>
        /// Queries the bandwidth limiting enabled sentinel. Also sets the
        /// <see cref="BandwidthLimitingEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) </returns>
        public (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryBandwidthLimitingEnabled( bool checkStatus = false )
        {
            var reply = this.QueryParseBandwidthLimitingEnabled( checkStatus );
            this.BandwidthLimitingEnabled = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets the bandwidth limiting enabled command Format. </summary>
        /// <remarks>   SCPI: ":SENSE:Zero:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The bandwidth limiting enabled query command. </value>
        protected virtual string BandwidthLimitingEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the bandwidth limiting enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        if set to <c>True</c> is enabled. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (bool SentValue, string SentMessage, ElapsedTimeSpan[] ElapsedTimes)
        /// </returns>
        public (bool SentValue, WriteBooleanInfo) WriteBandwidthLimitingEnabled( bool value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ),
                                                                 string.Format( this.BandwidthLimitingEnabledCommandFormat, value.GetHashCode() ) );
                this.BandwidthLimitingEnabled = value;
                return (value, new WriteBooleanInfo( value, executeInfo ));
            }
            else
            {
                var (sentValue, writeBooleanInfo) = this.WriteLineElapsed( value, this.BandwidthLimitingEnabledCommandFormat );
                this.BandwidthLimitingEnabled = sentValue;
                return (sentValue, writeBooleanInfo);
            }
        }

        #endregion

        #region " GENERATOR OUTPUT LEVEL "

        /// <summary>   The Generator Output Level range. </summary>
        private Core.Primitives.RangeR _GeneratorOutputLevelRange;

        /// <summary>   The Generator Output Level range in seconds. </summary>
        /// <value> The Generator Output Level range. </value>
        public Core.Primitives.RangeR GeneratorOutputLevelRange
        {
            get => this._GeneratorOutputLevelRange;

            set {
                if ( this.GeneratorOutputLevelRange != value )
                {
                    this._GeneratorOutputLevelRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The Generator OutputLevel. </summary>
        private double? _GeneratorOutputLevel;

        /// <summary>   Gets or sets the generator output level. </summary>
        /// <value> The generator output level. </value>
        public double? GeneratorOutputLevel
        {
            get => this._GeneratorOutputLevel;

            protected set {
                if ( !Nullable.Equals( this.GeneratorOutputLevel, value ) )
                {
                    this._GeneratorOutputLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Query if 'value' is generator output level out of range. </summary>
        /// <remarks>   David, 2021-06-17. </remarks>
        /// <param name="value">    The Generator Output Level. </param>
        /// <returns>   True if generator output level out of range, false if not. </returns>
        public bool IsGeneratorOutputLevelOutOfRange( double value )
        {
            return !this.GeneratorOutputLevelRange.Contains( value );
        }

        /// <summary>   Writes and reads back the Generator Output Level. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Generator Output Level. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, WriteInfo<double> WriteInfo, QueryParseInfo<double> QueryParseInfo) ApplyGeneratorOutputLevel( double value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteGeneratorOutputLevel( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryGeneratorOutputLevel( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Generator Output Level query command. </summary>
        /// <value> The Generator Output Level query command. </value>
        protected virtual string GeneratorOutputLevelQueryCommand { get; set; }

        /// <summary>   Writes the generator output level query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteGeneratorOutputLevelQueryCommand()
        {
            return this.WriteLineElapsed( this.GeneratorOutputLevelQueryCommand );
        }

        /// <summary>   Queries parse generator output level. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo).
        /// </returns>
        protected (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryParseGeneratorOutputLevel( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.GeneratorOutputLevelQueryCommand )
                : this.QueryElapsed( this.GeneratorOutputLevelQueryCommand );
            (bool hasValue, ParseInfo<double> parseInfo) = this.TryParseGeneratorOutputLevel( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<double>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.GeneratorOutputLevelQueryCommand}'" );
        }

        /// <summary>   Parse generator output level. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>(double ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public (double ParsedValue, ParseInfo<double> ParseInfo) ParseGeneratorOutputLevel( string receivedMessage )
        {
            (bool hasValue, ParseInfo<double> parseInfo) = this.TryParseGeneratorOutputLevel( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.GeneratorOutputLevelQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse a generator output level from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<double> ParseInfo) TryParseGeneratorOutputLevel( string receivedMessage, out double parsedValue );

        /// <summary>   Queries The Generator Output Level. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryGeneratorOutputLevel( bool checkStatus = false )
        {
            var reply = this.QueryParseGeneratorOutputLevel( checkStatus );
            this.GeneratorOutputLevel = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Generator Output Level command format. </summary>
        /// <value> The Generator Output Level command format. </value>
        protected virtual string GeneratorOutputLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Generator Output Level without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Generator Output Level. </remarks>
        /// <param name="value">        The GeneratorOutputLevel. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        public (double sentValue, WriteInfo<double> writeInfo) WriteGeneratorOutputLevel( double value, bool checkStatus = false )
        {
            if ( this.IsGeneratorOutputLevelOutOfRange( value ) )
                throw new ArgumentException( $"Requested Generator Output Level of {value} is out of range {this.GeneratorOutputLevelRange}", nameof( value ) );
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.GeneratorOutputLevelCommandFormat, value ) );
                this.GeneratorOutputLevel = value;
                return (value, new WriteInfo<double>( value, executeInfo ));
            }
            else
            {
                (double sentValue, WriteInfo<double> writeInfo) = this.WriteLineElapsed( value, this.GeneratorOutputLevelCommandFormat );
                this.GeneratorOutputLevel = value;
                return (sentValue, writeInfo);
            }
        }

        #endregion

        #region " GENERATOR TIMER "

        /// <summary>   The Generator Timer range. </summary>
        private Core.Primitives.RangeI _GeneratorTimerRange;

        /// <summary>   The Generator Timer range in seconds. </summary>
        /// <value> The Generator Timer range. </value>
        public Core.Primitives.RangeI GeneratorTimerRange
        {
            get => this._GeneratorTimerRange;

            set {
                if ( this.GeneratorTimerRange != value )
                {
                    this._GeneratorTimerRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The Generator Timer. </summary>
        private int? _GeneratorTimer;

        /// <summary>   Gets or sets the Generator Timer. </summary>
        /// <value> The Generator Timer. </value>
        public int? GeneratorTimer
        {
            get => this._GeneratorTimer;

            protected set {
                if ( !Nullable.Equals( this.GeneratorTimer, value ) )
                {
                    this._GeneratorTimer = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Generator Timer. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Generator Timer. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (int ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (int ParsedValue, WriteInfo<int> WriteInfo, QueryParseInfo<int> QueryParseInfo) ApplyGeneratorTimer( int value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteGeneratorTimer( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryGeneratorTimer( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Generator Timer query command. </summary>
        /// <value> The Generator Timer query command. </value>
        protected virtual string GeneratorTimerQueryCommand { get; set; }

        /// <summary>   Writes the generator timer query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteGeneratorTimerQueryCommand()
        {
            return this.WriteLineElapsed( this.GeneratorTimerQueryCommand );
        }

        /// <summary>   Queries parse Generator Timer. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo).
        /// </returns>
        protected (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryParseGeneratorTimer( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.GeneratorTimerQueryCommand )
                : this.QueryElapsed( this.GeneratorTimerQueryCommand );
            (bool hasValue, ParseInfo<int> parseInfo) = this.TryParseGeneratorTimer( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<int>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.GeneratorTimerQueryCommand}'" );
        }

        /// <summary>   Parse Generator Timer. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>(int ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public (int ParsedValue, ParseInfo<int> ParseInfo) ParseGeneratorTimer( string receivedMessage )
        {
            (bool hasValue, ParseInfo<int> parseInfo) = this.TryParseGeneratorTimer( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.GeneratorTimerQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse a Generator Timer from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<int> ParseInfo) TryParseGeneratorTimer( string receivedMessage, out int parsedValue );

        /// <summary>   Queries The Generator Timer. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A Tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryGeneratorTimer( bool checkStatus = false )
        {
            var reply = this.QueryParseGeneratorTimer( checkStatus );
            this.GeneratorTimer = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Generator Timer command format. </summary>
        /// <value> The Generator Timer command format. </value>
        protected virtual string GeneratorTimerCommandFormat { get; set; }

        /// <summary>   Writes The Generator Timer without reading back the value from the device. </summary>
        /// <remarks>   This command sets The Generator Timer. </remarks>
        /// <param name="value">        The GeneratorTimer. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        public (int sentValue, WriteInfo<int> writeInfo) WriteGeneratorTimer( int value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.GeneratorTimerCommandFormat, value ) );
                this.GeneratorTimer = value;
                return (value, new WriteInfo<int>( value, executeInfo ));
            }
            else
            {
                (int sentValue, WriteInfo<int> writeInfo) = this.WriteLineElapsed( value, this.GeneratorTimerCommandFormat );
                this.GeneratorTimer = value;
                return (sentValue, writeInfo);
            }
        }

        #endregion

        #region " IMPEDANCE RANGE MODE "

        /// <summary>   The Impedance Range mode. </summary>
        private int? _ImpedanceRangeMode;

        /// <summary>   Gets or sets the Impedance Range Mode. </summary>
        /// <value> The Impedance Range Mode. </value>
        public virtual int? ImpedanceRangeMode
        {
            get => this._ImpedanceRangeMode;

            protected set {
                if ( !Nullable.Equals( this.ImpedanceRangeMode, value ) )
                {
                    this._ImpedanceRangeMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Impedance Range Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Impedance Range Mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple:  (int ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (int ParsedValue, WriteInfo<int> WriteInfo, QueryParseInfo<int> QueryParseInfo) ApplyImpedanceRangeMode( int value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteImpedanceRangeMode( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryImpedanceRangeMode( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Impedance Range Mode query command. </summary>
        /// <value> The Impedance Range Mode query command. </value>
        protected virtual string ImpedanceRangeModeQueryCommand { get; set; }

        /// <summary>   Writes the impedance range mode query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteImpedanceRangeModeQueryCommand()
        {
            return this.WriteLineElapsed( this.ImpedanceRangeCommandFormat );
        }

        /// <summary>   Queries parse Impedance Range mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        protected (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryParseImpedanceRangeMode( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.ImpedanceRangeModeQueryCommand )
                : this.QueryElapsed( this.ImpedanceRangeModeQueryCommand );
            var (hasValue, parseInfo) = this.TryParseImpedanceRangeMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<int>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.ImpedanceRangeModeQueryCommand}'" );
        }

        /// <summary>   Parse Impedance Range mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns> A Tuple: (int ParsedValue, <see cref="ParseInfo{T}"/>) </returns>
        public (int ParsedValue, ParseInfo<int> ParseInfo) ParseImpedanceRangeMode( string receivedMessage )
        {
            (bool hasValue, ParseInfo<int> parseInfo) = this.TryParseImpedanceRangeMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.ImpedanceRangeModeQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse the Impedance Range mode from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A Tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<int> ParseInfo) TryParseImpedanceRangeMode( string receivedMessage, out int parsedValue );

        /// <summary>   Queries The Impedance Range Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryImpedanceRangeMode( bool checkStatus = false )
        {
            var reply = this.QueryParseImpedanceRangeMode( checkStatus );
            this.ImpedanceRangeMode = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Impedance Range Mode command format. </summary>
        /// <value> The Impedance Range Mode command format. </value>
        protected virtual string ImpedanceRangeModeCommandFormat { get; set; }

        /// <summary>
        /// Writes The Impedance Range Mode without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Impedance Range Mode. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="value">        The ImpedanceRangeMode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int SentValue, WriteInfo{int} WriteInfo) </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteImpedanceRangeMode( int value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.ImpedanceRangeModeCommandFormat, value ) );
                var status = this.AwaitStatus( this.ImpedanceRangeRefractoryTimeSpan );
                var (hasError, Details, StatusByte) = this.StatusSubsystem.IsStatusError( status.StatusByte );
                if ( hasError )
                    throw new InvalidOperationException( $"Status error 0x{StatusByte:X2}:'{Details}' after writing '{executeInfo.SentMessage}'" );
                this.ImpedanceRangeMode = value;
                return (value, new WriteInfo<int>( value, executeInfo ));
            }
            else
            {
                (int sentValue, WriteInfo<int> writeInfo) = this.WriteLineElapsed( value, this.ImpedanceRangeModeCommandFormat );
                var status = this.AwaitStatus( this.ImpedanceRangeRefractoryTimeSpan );
                var (hasError, Details, StatusByte) = this.StatusSubsystem.IsStatusError( status.StatusByte );
                if ( hasError )
                    throw new InvalidOperationException( $"Status error 0x{StatusByte:X2}:'{Details}' after writing '{writeInfo.SentMessage}'" );
                this.ImpedanceRangeMode = sentValue;
                return (sentValue, writeInfo);
            }
        }

        /// <summary>   Gets or sets the impedance range change execution refractory time span. </summary>
        /// <value> The impedance range change execution refractory time span. </value>
        public virtual TimeSpan ImpedanceRangeRefractoryTimeSpan { get; set; }


        #endregion

        #region " IMPEDANCE RANGE "

        /// <summary>   The impedance range. </summary>
        private Core.Primitives.RangeR _ImpedanceRangeRange;

        /// <summary>   Gets or sets the range of Impedance Range. </summary>
        /// <value> The range Impedance Range. </value>
        public Core.Primitives.RangeR ImpedanceRangeRange
        {
            get => this._ImpedanceRangeRange;

            set {
                if ( this.ImpedanceRangeRange != value )
                {
                    this._ImpedanceRangeRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The impedance range. </summary>
        private double? _ImpedanceRange;

        /// <summary>   Gets or sets the Impedance Range. </summary>
        /// <value> The Impedance Range. </value>
        public virtual double? ImpedanceRange
        {
            get => this._ImpedanceRange;

            protected set {
                if ( !Nullable.Equals( this.ImpedanceRange, value ) )
                {
                    this._ImpedanceRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Applies the Impedance Range described by value. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The impedance value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="WriteInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, WriteInfo<double> WriteInfo, QueryParseInfo<double> QueryParseInfo) ApplyImpedanceRange( double value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteImpedanceRange( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryImpedanceRange( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets the 'Impedance Range query' command. </summary>
        /// <value> The 'Impedance Range query' command. </value>
        protected virtual string ImpedanceRangeQueryCommand { get; set; }

        /// <summary>   Writes the impedance range query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteImpedanceRangeQueryCommand()
        {
            return this.WriteLineElapsed( this.ImpedanceRangeQueryCommand );
        }

        /// <summary>   Queries parse impedance range. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        protected (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryParseImpedanceRange( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.ImpedanceRangeQueryCommand )
                : this.QueryElapsed( this.ImpedanceRangeQueryCommand );
            (bool hasValue, ParseInfo<double> ParseInfo) = this.TryParseImpedanceRange( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<double>( queryInfo, ParseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseInfo.ParsedMessage}' for query '{this.ImpedanceRangeQueryCommand}'" );
        }

        /// <summary>   Parse impedance range. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>   A tuple: (double ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) . </returns>
        public (double ParsedValue, ParseInfo<double> ParseInfo) ParseImpedanceRange( string receivedMessage )
        {
            (bool hasValue, ParseInfo<double> ParseInfo) = this.TryParseImpedanceRange( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, ParseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseInfo.ParsedMessage}' for query '{this.ImpedanceRangeQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse an impedance range from the given data, returning a default value rather
        /// than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<double> ParseInfo) TryParseImpedanceRange( string receivedMessage, out double parsedValue );

        /// <summary>   Queries Impedance Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public virtual (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryImpedanceRange( bool checkStatus = false )
        {
            var reply = this.QueryParseImpedanceRange( checkStatus );
            this.ImpedanceRange = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets the Impedance Range command format. </summary>
        /// <value> The Impedance Range command format. </value>
        protected virtual string ImpedanceRangeCommandFormat { get; set; }

        /// <summary>   Writes a scaled Impedance Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The impedance value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        protected virtual (double sentValue, WriteInfo<double> writeInfo) WriteScaledImpedanceRange( double value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.ImpedanceRangeCommandFormat, value ) );
                return (value, new WriteInfo<double>( value, executeInfo ));
            }
            else
            {
                return this.WriteLineElapsed( value, this.ImpedanceRangeCommandFormat );
            }
        }

        /// <summary>   Writes a Impedance Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The impedance value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double SentValue, <see cref="WriteInfo{T}"/> WriteInfo) </returns>
        public virtual (double SentValue, WriteInfo<double> WriteInfo) WriteImpedanceRange( double value, bool checkStatus = false )
        {
            var reply = this.WriteScaledImpedanceRange( value, checkStatus );
            this.ImpedanceRange = value;
            return reply;
        }

        #endregion

        #region " MEASUREMENT START MODE "

        /// <summary>   The Measurement Start mode. </summary>
        private int? _MeasurementStartMode;

        /// <summary>   Gets or sets the Measurement Start Mode. </summary>
        /// <value> The Measurement Start Mode. </value>
        public int? MeasurementStartMode
        {
            get => this._MeasurementStartMode;

            protected set {
                if ( !Nullable.Equals( this.MeasurementStartMode, value ) )
                {
                    this._MeasurementStartMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Measurement Start Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Measurement Start Mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple:  (int ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (int ParsedValue, WriteInfo<int> WriteInfo, QueryParseInfo<int> QueryParseInfo) ApplyMeasurementStartMode( int value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteMeasurementStartMode( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryMeasurementStartMode( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Measurement Start Mode query command. </summary>
        /// <value> The Measurement Start Mode query command. </value>
        protected virtual string MeasurementStartModeQueryCommand { get; set; }

        /// <summary>   Writes the measurement start mode query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteMeasurementStartModeQueryCommand()
        {
            return this.WriteLineElapsed( this.MeasurementStartModeQueryCommand );
        }

        /// <summary>   Queries parse Measurement Start mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        protected (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryParseMeasurementStartMode( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.MeasurementStartModeQueryCommand )
                : this.QueryElapsed( this.MeasurementStartModeQueryCommand );
            var (hasValue, parseInfo) = this.TryParseMeasurementStartMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<int>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.MeasurementStartModeQueryCommand}'" );
        }

        /// <summary>   Parse Measurement Start mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns> A Tuple: (int ParsedValue, <see cref="ParseInfo{T}"/>) </returns>
        public (int ParsedValue, ParseInfo<int> ParseInfo) ParseMeasurementStartMode( string receivedMessage )
        {
            (bool hasValue, ParseInfo<int> parseInfo) = this.TryParseMeasurementStartMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.MeasurementStartModeQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse the Measurement Start mode from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A Tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<int> ParseInfo) TryParseMeasurementStartMode( string receivedMessage, out int parsedValue );

        /// <summary>   Queries The Measurement Start Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryMeasurementStartMode( bool checkStatus = false )
        {
            var reply = this.QueryParseMeasurementStartMode( checkStatus );
            this.MeasurementStartMode = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Measurement Start Mode command format. </summary>
        /// <value> The Measurement Start Mode command format. </value>
        protected virtual string MeasurementStartModeCommandFormat { get; set; }

        /// <summary>
        /// Writes The Measurement Start Mode without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Measurement Start Mode. </remarks>
        /// <param name="value">        The MeasurementStartMode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int SentValue, WriteInfo{int} WriteInfo) </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteMeasurementStartMode( int value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.MeasurementStartModeCommandFormat, value ) );
                this.MeasurementStartMode = value;
                return (value, new WriteInfo<int>( value, executeInfo ));
            }
            else
            {
                (int sentValue, WriteInfo<int> writeInfo) = this.WriteLineElapsed( value, this.MeasurementStartModeCommandFormat );
                this.MeasurementStartMode = sentValue;
                return (sentValue, writeInfo);
            }
        }

        #endregion

        #region " VOLTMETER OUTPUT ENABLED "

        /// <summary>   The Voltmeter Output enabled. </summary>
        private bool? _VoltmeterOutputEnabled;

        /// <summary>   Gets or sets the Voltmeter Output enabled. </summary>
        /// <value> The Voltmeter Output enabled. </value>
        public bool? VoltmeterOutputEnabled
        {
            get => this._VoltmeterOutputEnabled;

            protected set {
                if ( !Equals( this.VoltmeterOutputEnabled, value ) )
                {
                    this._VoltmeterOutputEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Voltmeter Output enabled sentinel. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        if set to <c>True</c> if enabling; False if disabling. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (bool ParsedValue, WriteBooleanInfo WriteBooleanInfo, QueryParseBooleanInfo
        /// QueryParseBooleanInfo)
        /// </returns>
        public (bool ParsedValue, WriteBooleanInfo WriteBooleanInfo, QueryParseBooleanInfo QueryParseBooleanInfo) ApplyVoltmeterOutputEnabled( bool value, bool checkStatus = false )
        {
            var (_, writeBooleanInfo) = this.WriteVoltmeterOutputEnabled( value, checkStatus );
            var (parsedValue, queryParseBooleanInfo) = this.QueryVoltmeterOutputEnabled( checkStatus );
            return (parsedValue, writeBooleanInfo, queryParseBooleanInfo);
        }

        /// <summary>   Gets or sets the Voltmeter Output enabled query command. </summary>
        /// <value> The Voltmeter Output enabled query command. </value>
        protected virtual string VoltmeterOutputEnabledQueryCommand { get; set; }

        /// <summary>   Writes the Voltmeter Output enabled query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteVoltmeterOutputEnabledQueryCommand()
        {
            return this.WriteLineElapsed( this.VoltmeterOutputEnabledQueryCommand );
        }

        /// <summary>   Queries parse Voltmeter Output enabled. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <returns>
        /// A tuple: (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo ParseBooleanInfo)
        /// </returns>
        protected (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryParseVoltmeterOutputEnabled( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.VoltmeterOutputEnabledQueryCommand )
                : this.QueryElapsed( this.VoltmeterOutputEnabledQueryCommand );
            var (hasValue, parseBooleanInfo) = this.TryParseVoltmeterOutputEnabled( receivedMessage, out bool parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseBooleanInfo( queryInfo, parseBooleanInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseBooleanInfo.ParsedMessage}' for query '{this.VoltmeterOutputEnabledQueryCommand}'" );
        }

        /// <summary>   Parse Voltmeter Output enabled. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>   A tuple: (bool ParsedValue, ParseBooleanInfo ParseBooleanInfo)  </returns>
        public (bool ParsedValue, ParseBooleanInfo ParseBooleanInfo) ParseVoltmeterOutputEnabled( string receivedMessage )
        {
            var (hasValue, parseBooleanInfo) = this.TryParseVoltmeterOutputEnabled( receivedMessage, out bool parsedValue );
            return hasValue
                ? (parsedValue, parseBooleanInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseBooleanInfo.ParsedMessage}' for query '{this.VoltmeterOutputEnabledQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse a Voltmeter Output enabled from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool Success, ParseBooleanInfo ParseBooleanInfo) </returns>
        public abstract (bool HasValue, ParseBooleanInfo ParseBooleanInfo) TryParseVoltmeterOutputEnabled( string receivedMessage, out bool parsedValue );

        /// <summary>
        /// Queries the Voltmeter Output enabled sentinel. Also sets the
        /// <see cref="VoltmeterOutputEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) </returns>
        public (bool ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryVoltmeterOutputEnabled( bool checkStatus = false )
        {
            var reply = this.QueryParseVoltmeterOutputEnabled( checkStatus );
            this.VoltmeterOutputEnabled = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets the Voltmeter Output enabled command Format. </summary>
        /// <remarks>   SCPI: ":SENSE:Zero:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Voltmeter Output enabled query command. </value>
        protected virtual string VoltmeterOutputEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Voltmeter Output enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        if set to <c>True</c> is enabled. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (bool SentValue, string SentMessage, ElapsedTimeSpan[] ElapsedTimes)
        /// </returns>
        public (bool SentValue, WriteBooleanInfo) WriteVoltmeterOutputEnabled( bool value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ),
                                                                 string.Format( this.VoltmeterOutputEnabledCommandFormat, value.GetHashCode() ) );
                this.VoltmeterOutputEnabled = value;
                return (value, new WriteBooleanInfo( value, executeInfo ));
            }
            else
            {
                var (sentValue, writeBooleanInfo) = this.WriteLineElapsed( value, this.VoltmeterOutputEnabledCommandFormat );
                this.VoltmeterOutputEnabled = sentValue;
                return (sentValue, writeBooleanInfo);
            }
        }

        #endregion

        #region " VOLTMETER RANGE MODE "

        /// <summary>   The Voltmeter Range mode. </summary>
        private int? _VoltmeterRangeMode;

        /// <summary>   Gets or sets the Voltmeter Range Mode. </summary>
        /// <value> The Voltmeter Range Mode. </value>
        public virtual int? VoltmeterRangeMode
        {
            get => this._VoltmeterRangeMode;

            protected set {
                if ( !Nullable.Equals( this.VoltmeterRangeMode, value ) )
                {
                    this._VoltmeterRangeMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Voltmeter Range Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Voltmeter Range Mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple:  (int ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (int ParsedValue, WriteInfo<int> WriteInfo, QueryParseInfo<int> QueryParseInfo) ApplyVoltmeterRangeMode( int value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteVoltmeterRangeMode( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryVoltmeterRangeMode( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Voltmeter Range Mode query command. </summary>
        /// <value> The Voltmeter Range Mode query command. </value>
        protected virtual string VoltmeterRangeModeQueryCommand { get; set; }

        /// <summary>   Writes the Voltmeter Range mode query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteVoltmeterRangeModeQueryCommand()
        {
            return this.WriteLineElapsed( this.VoltmeterRangeModeQueryCommand );
        }

        /// <summary>   Queries parse Voltmeter Range mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        protected (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryParseVoltmeterRangeMode( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.VoltmeterRangeModeQueryCommand )
                : this.QueryElapsed( this.VoltmeterRangeModeQueryCommand );
            var (hasValue, parseInfo) = this.TryParseVoltmeterRangeMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<int>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.VoltmeterRangeModeQueryCommand}'" );
        }

        /// <summary>   Parse Voltmeter Range mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>   A Tuple: (int ParsedValue, <see cref="ParseInfo{T}"/>) </returns>
        public (int ParsedValue, ParseInfo<int> ParseInfo) ParseVoltmeterRangeMode( string receivedMessage )
        {
            (bool hasValue, ParseInfo<int> parseInfo) = this.TryParseVoltmeterRangeMode( receivedMessage, out int parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.VoltmeterRangeModeQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse the Voltmeter Range mode from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A Tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<int> ParseInfo) TryParseVoltmeterRangeMode( string receivedMessage, out int parsedValue );

        /// <summary>   Queries The Voltmeter Range Mode. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) </returns>
        public (int ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryVoltmeterRangeMode( bool checkStatus = false )
        {
            var reply = this.QueryParseVoltmeterRangeMode( checkStatus );
            this.VoltmeterRangeMode = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Voltmeter Range Mode command format. </summary>
        /// <value> The Voltmeter Range Mode command format. </value>
        protected virtual string VoltmeterRangeModeCommandFormat { get; set; }

        /// <summary>
        /// Writes The Voltmeter Range Mode without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Voltmeter Range Mode. </remarks>
        /// <param name="value">        The MeasureRangeMode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int SentValue, WriteInfo{int} WriteInfo) </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteVoltmeterRangeMode( int value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.VoltmeterRangeModeCommandFormat, value ) );
                this.VoltmeterRangeMode = value;
                return (value, new WriteInfo<int>( value, executeInfo ));
            }
            else
            {
                (int sentValue, WriteInfo<int> writeInfo) = this.WriteLineElapsed( value, this.VoltmeterRangeModeCommandFormat );
                this.VoltmeterRangeMode = sentValue;
                return (sentValue, writeInfo);
            }
        }

        #endregion

        #region " VOLTMETER RANGE "

        /// <summary>   The range of the Voltmeter. </summary>
        private Core.Primitives.RangeR _VoltmeterRangeRange;

        /// <summary>   Gets or sets the range of Voltmeter Range. </summary>
        /// <value> The range Voltmeter Range. </value>
        public Core.Primitives.RangeR VoltmeterRangeRange
        {
            get => this._VoltmeterRangeRange;

            set {
                if ( this.VoltmeterRangeRange != value )
                {
                    this._VoltmeterRangeRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   The Voltmeter Range. </summary>
        private double? _VoltmeterRange;

        /// <summary>   Gets or sets the Voltmeter Range. </summary>
        /// <value> The Voltmeter Range. </value>
        public virtual double? VoltmeterRange
        {
            get => this._VoltmeterRange;

            protected set {
                if ( !Nullable.Equals( this.VoltmeterRange, value ) )
                {
                    this._VoltmeterRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Applies the Voltmeter Range described by value. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The VoltageMeasure value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="WriteInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, WriteInfo<double> WriteInfo, QueryParseInfo<double> QueryParseInfo) ApplyVoltmeterRange( double value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteVoltmeterRange( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryVoltmeterRange( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets the 'Voltmeter Range query' command. </summary>
        /// <value> The 'Voltmeter Range query' command. </value>
        protected virtual string VoltmeterRangeQueryCommand { get; set; }

        /// <summary>   Writes the Voltmeter Range query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteVoltmeterRangeQueryCommand()
        {
            return this.WriteLineElapsed( this.VoltmeterRangeQueryCommand );
        }

        /// <summary>   Queries parse Voltmeter Range. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        protected (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryParseVoltmeterRange( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.VoltmeterRangeQueryCommand )
                : this.QueryElapsed( this.VoltmeterRangeQueryCommand );
            (bool hasValue, ParseInfo<double> ParseInfo) = this.TryParseVoltmeterRange( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<double>( queryInfo, ParseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseInfo.ParsedMessage}' for query '{this.VoltmeterRangeQueryCommand}'" );
        }

        /// <summary>   Parse Voltmeter Range. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>   A tuple: (double ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) . </returns>
        public (double ParsedValue, ParseInfo<double> ParseInfo) ParseVoltmeterRange( string receivedMessage )
        {
            (bool hasValue, ParseInfo<double> ParseInfo) = this.TryParseVoltmeterRange( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, ParseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseInfo.ParsedMessage}' for query '{this.VoltmeterRangeQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse an Voltmeter Range from the given data, returning a default value rather
        /// than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>
        /// A tuple: (bool hasValue, <see cref="ParseInfo{T}"/> ParseInfo)
        /// </returns>
        public abstract (bool hasValue, ParseInfo<double> ParseInfo) TryParseVoltmeterRange( string receivedMessage, out double parsedValue );

        /// <summary>   Queries Voltmeter Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public virtual (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryVoltmeterRange( bool checkStatus = false )
        {
            var reply = this.QueryParseVoltmeterRange( checkStatus );
            this.VoltmeterRange = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets the Voltmeter Range command format. </summary>
        /// <value> The Voltmeter Range command format. </value>
        protected virtual string VoltmeterRangeCommandFormat { get; set; }

        /// <summary>   Writes a scaled Voltmeter Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The VoltageMeasure value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        protected virtual (double sentValue, WriteInfo<double> writeInfo) WriteScaledVoltmeterRange( double value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.VoltmeterRangeCommandFormat, value ) );
                return (value, new WriteInfo<double>( value, executeInfo ));
            }
            else
            {
                return this.WriteLineElapsed( value, this.VoltmeterRangeCommandFormat );
            }
        }

        /// <summary>   Writes a Voltmeter Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="value">        The VoltageMeasure value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        public virtual (double sentValue, WriteInfo<double> writeInfo) WriteVoltmeterRange( double value, bool checkStatus = false )
        {
            var reply = this.WriteScaledVoltmeterRange( value, checkStatus );
            var status = this.AwaitStatus( this.VoltmeterRangeRefractoryTimeSpan );
            var (hasError, Details, StatusByte) = this.StatusSubsystem.IsStatusError( status.StatusByte );
            if ( hasError )
                throw new InvalidOperationException( $"Status error 0x{StatusByte:X2}:'{Details}' after writing '{reply.writeInfo.SentMessage}'" );
            this.VoltmeterRange = value;
            return reply;
        }

        /// <summary>   Gets or sets the Voltmeter Range change execution refractory time span. </summary>
        /// <value> The Voltmeter Range change execution refractory time span. </value>
        public virtual TimeSpan VoltmeterRangeRefractoryTimeSpan { get; set; }

        #endregion

        #region " VOLTMETER HIGH LIMIT "

        /// <summary>   The Voltmeter High Limit range. </summary>
        private Core.Primitives.RangeR _VoltmeterHighLimitRange;

        /// <summary>   The Voltmeter High Limit range in seconds. </summary>
        /// <value> The Voltmeter High Limit range. </value>
        public Core.Primitives.RangeR VoltmeterHighLimitRange
        {
            get => this._VoltmeterHighLimitRange;

            set {
                if ( this.VoltmeterHighLimitRange != value )
                {
                    this._VoltmeterHighLimitRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Query if 'value' is out of voltmeter high limit range. </summary>
        /// <remarks>   David, 2021-06-17. </remarks>
        /// <param name="value">    The Generator Output Level. </param>
        /// <returns>   True if value is out of voltmeter high limit range, false if not. </returns>
        public bool IsVoltmeterHighLimitOutOfRange( double value )
        {
            return !this.VoltmeterHighLimitRange.Contains( value );
        }

        /// <summary>   The Voltmeter HighLimit. </summary>
        private double? _VoltmeterHighLimit;

        /// <summary>   Gets or sets the Voltmeter High Limit. </summary>
        /// <value> The Voltmeter High Limit. </value>
        public double? VoltmeterHighLimit
        {
            get => this._VoltmeterHighLimit;

            protected set {
                if ( !Nullable.Equals( this.VoltmeterHighLimit, value ) )
                {
                    this._VoltmeterHighLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Voltmeter High Limit. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Voltmeter High Limit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, WriteInfo<double> WriteInfo, QueryParseInfo<double> QueryParseInfo) ApplyVoltmeterHighLimit( double value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteVoltmeterHighLimit( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryVoltmeterHighLimit( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Voltmeter High Limit query command. </summary>
        /// <value> The Voltmeter High Limit query command. </value>
        protected virtual string VoltmeterHighLimitQueryCommand { get; set; }

        /// <summary>   Writes the voltmeter high limit query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteVoltmeterHighLimitQueryCommand()
        {
            return this.WriteLineElapsed( this.VoltmeterHighLimitQueryCommand );
        }

        /// <summary>   Queries parse Voltmeter High Limit. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo).
        /// </returns>
        protected (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryParseVoltmeterHighLimit( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.VoltmeterHighLimitQueryCommand )
                : this.QueryElapsed( this.VoltmeterHighLimitQueryCommand );
            (bool hasValue, ParseInfo<double> parseInfo) = this.TryParseVoltmeterHighLimit( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<double>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.VoltmeterHighLimitQueryCommand}'" );
        }

        /// <summary>   Parse Voltmeter High Limit. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>(double ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public (double ParsedValue, ParseInfo<double> ParseInfo) ParseVoltmeterHighLimit( string receivedMessage )
        {
            (bool hasValue, ParseInfo<double> parseInfo) = this.TryParseVoltmeterHighLimit( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.VoltmeterHighLimitQueryCommand}'" );
        }

        /// <summary>
        /// Attempts to parse a Voltmeter High Limit from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<double> ParseInfo) TryParseVoltmeterHighLimit( string receivedMessage, out double parsedValue );

        /// <summary>   Queries The Voltmeter High Limit. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryVoltmeterHighLimit( bool checkStatus = false )
        {
            var reply = this.QueryParseVoltmeterHighLimit( checkStatus );
            this.VoltmeterHighLimit = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Voltmeter High Limit command format. </summary>
        /// <value> The Voltmeter High Limit command format. </value>
        protected virtual string VoltmeterHighLimitCommandFormat { get; set; }

        /// <summary>   Writes a scaled Voltmeter high limit. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The Voltmeter high limit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        protected virtual (double sentValue, WriteInfo<double> writeInfo) WriteScaledVoltmeterHighLimit( double value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.VoltmeterHighLimitCommandFormat, value ) );
                return (value, new WriteInfo<double>( value, executeInfo ));
            }
            else
            {
                return this.WriteLineElapsed( value, this.VoltmeterHighLimitCommandFormat );
            }
        }

        /// <summary>
        /// Writes The Voltmeter High Limit without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Voltmeter High Limit. </remarks>
        /// <param name="value">        The VoltmeterHighLimit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        public (double sentValue, WriteInfo<double> writeInfo) WriteVoltmeterHighLimit( double value, bool checkStatus = false )
        {
            if ( this.IsVoltmeterHighLimitOutOfRange( value ) )
                throw new ArgumentException( $"Requested low limit of {value} is out of range {this.VoltmeterHighLimitRange}", nameof( value ) );
            (double sentValue, WriteInfo<double> writeInfo) = this.WriteScaledVoltmeterHighLimit( value, checkStatus );
            this.VoltmeterHighLimit = value;
            return (sentValue, writeInfo);
        }

        #endregion

        #region " VOLTMETER LOW LIMIT "

        /// <summary>   The Voltmeter Low Limit range. </summary>
        private Core.Primitives.RangeR _VoltmeterLowLimitRange;

        /// <summary>   The Voltmeter Low Limit range in seconds. </summary>
        /// <value> The Voltmeter Low Limit range. </value>
        public Core.Primitives.RangeR VoltmeterLowLimitRange
        {
            get => this._VoltmeterLowLimitRange;

            set {
                if ( this.VoltmeterLowLimitRange != value )
                {
                    this._VoltmeterLowLimitRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Query if 'value' is out of voltmeter low limit range. </summary>
        /// <remarks>   David, 2021-06-17. </remarks>
        /// <param name="value">    The Generator Output Level. </param>
        /// <returns>   True if value is out of voltmeter low limit range, false if not. </returns>
        public bool IsVoltmeterLowLimitOutOfRange( double value )
        {
            return !this.VoltmeterLowLimitRange.Contains( value );
        }


        /// <summary>   The Voltmeter LowLimit. </summary>
        private double? _VoltmeterLowLimit;

        /// <summary>   Gets or sets the Voltmeter Low Limit. </summary>
        /// <value> The Voltmeter Low Limit. </value>
        public double? VoltmeterLowLimit
        {
            get => this._VoltmeterLowLimit;

            protected set {
                if ( !Nullable.Equals( this.VoltmeterLowLimit, value ) )
                {
                    this._VoltmeterLowLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Writes and reads back the Voltmeter Low Limit. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="value">        The Voltmeter Low Limit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A tuple: (double ParsedValue, <see cref="WriteInfo{T}"/> WriteInfo,
        /// <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, WriteInfo<double> WriteInfo, QueryParseInfo<double> QueryParseInfo) ApplyVoltmeterLowLimit( double value, bool checkStatus = false )
        {
            var (_, writeInfo) = this.WriteVoltmeterLowLimit( value, checkStatus );
            var (parsedValue, queryParseInfo) = this.QueryVoltmeterLowLimit( checkStatus );
            return (parsedValue, writeInfo, queryParseInfo);
        }

        /// <summary>   Gets or sets The Voltmeter Low Limit query command. </summary>
        /// <value> The Voltmeter Low Limit query command. </value>
        protected virtual string VoltmeterLowLimitQueryCommand { get; set; }

        /// <summary>   Writes the voltmeter low limit query command. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public virtual ExecuteInfo WriteVoltmeterLowLimitQueryCommand()
        {
            return this.WriteLineElapsed( this.VoltmeterLowLimitQueryCommand );
        }

        /// <summary>   Queries parse Voltmeter Low Limit. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo).
        /// </returns>
        protected (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryParseVoltmeterLowLimit( bool checkStatus = false )
        {
            (string receivedMessage, QueryInfo queryInfo) = checkStatus
                ? this.QueryStatusReady( this.ReadyToQueryTimeout, this.ReadyToReadTimeout, this.VoltmeterLowLimitQueryCommand )
                : this.QueryElapsed( this.VoltmeterLowLimitQueryCommand );
            (bool hasValue, ParseInfo<double> parseInfo) = this.TryParseVoltmeterLowLimit( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, new QueryParseInfo<double>( queryInfo, parseInfo ))
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.VoltmeterLowLimitQueryCommand }'" );
        }

        /// <summary>   Parse Voltmeter Low Limit. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <returns>(double ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public (double ParsedValue, ParseInfo<double> ParseInfo) ParseVoltmeterLowLimit( string receivedMessage )
        {
            (bool hasValue, ParseInfo<double> parseInfo) = this.TryParseVoltmeterLowLimit( receivedMessage, out double parsedValue );
            return hasValue
                ? (parsedValue, parseInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{parseInfo.ParsedMessage}' for query '{this.VoltmeterLowLimitQueryCommand }'" );
        }

        /// <summary>
        /// Attempts to parse a Voltmeter Low Limit from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public abstract (bool HasValue, ParseInfo<double> ParseInfo) TryParseVoltmeterLowLimit( string receivedMessage, out double parsedValue );

        /// <summary>   Queries The Voltmeter Low Limit. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryVoltmeterLowLimit( bool checkStatus = false )
        {
            var reply = this.QueryParseVoltmeterLowLimit( checkStatus );
            this.VoltmeterLowLimit = reply.ParsedValue;
            return reply;
        }

        /// <summary>   Gets or sets The Voltmeter Low Limit command format. </summary>
        /// <value> The Voltmeter Low Limit command format. </value>
        protected virtual string VoltmeterLowLimitCommandFormat { get; set; }

        /// <summary>   Writes a scaled Voltmeter Low limit. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The Voltmeter Low limit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        protected virtual (double sentValue, WriteInfo<double> writeInfo) WriteScaledVoltmeterLowLimit( double value, bool checkStatus = false )
        {
            if ( checkStatus )
            {
                ExecuteInfo executeInfo = this.WriteStatusReady( TimeSpan.FromMilliseconds( 100 ), string.Format( this.VoltmeterLowLimitCommandFormat, value ) );
                return (value, new WriteInfo<double>( value, executeInfo ));
            }
            else
            {
                return this.WriteLineElapsed( value, this.VoltmeterLowLimitCommandFormat );
            }
        }

        /// <summary>
        /// Writes The Voltmeter Low Limit without reading back the value from the device.
        /// </summary>
        /// <remarks>   This command sets The Voltmeter Low Limit. </remarks>
        /// <param name="value">        The VoltmeterLowLimit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        public (double sentValue, WriteInfo<double> writeInfo) WriteVoltmeterLowLimit( double value, bool checkStatus = false )
        {
            if ( this.IsVoltmeterLowLimitOutOfRange( value ) )
                throw new ArgumentException( $"Requested low limit of {value} is out of range {this.VoltmeterLowLimitRange}", nameof( value ) );
            (double sentValue, WriteInfo<double> writeInfo) = this.WriteScaledVoltmeterLowLimit( value, checkStatus );
            this.VoltmeterLowLimit = value;
            return (sentValue, writeInfo);
        }

        #endregion

        #region " MEASURE UNIT "

        /// <summary>   Gets or sets the default measurement unit. </summary>
        /// <value> The default measure unit. </value>
        public Arebis.TypedUnits.Unit DefaultMeasurementUnit { get; set; }

        /// <summary>   Gets or sets the function unit. </summary>
        /// <value> The function unit. </value>
        public Arebis.TypedUnits.Unit MeasurementUnit
        {
            get => this.PrimaryReading.Amount.Unit;

            set {
                if ( this.MeasurementUnit != value )
                {
                    this.PrimaryReading.ApplyUnit( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FRONT / READ TERMINAL SUPPORT "

        /// <summary>   Gets true if the subsystem supports front terminals selection query. </summary>
        /// <value>
        /// The value indicating if the subsystem supports front terminals selection query.
        /// </value>
        public bool SupportsFrontTerminalsSelectionQuery => false;

        /// <summary>   Gets or sets the terminals caption. </summary>
        /// <value> The terminals caption. </value>
        public string TerminalsCaption { get; set; } = "N/A";

        #endregion

    }
}
