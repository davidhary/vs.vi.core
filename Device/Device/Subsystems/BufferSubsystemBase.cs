using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using isr.Core.TimeSpanExtensions;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by a Buffer Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract class BufferSubsystemBase : SubsystemPlusStatusBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="BufferSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected BufferSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this._OrderedReadingElementTypes = new List<ReadingElementTypes>() { ReadingElementTypes.Reading, ReadingElementTypes.Timestamp, ReadingElementTypes.Status, ReadingElementTypes.Units };
            this.BufferStreamTasker = new Core.Tasker();
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
        /// If True, the method has been called directly or indirectly by a user's code--managed and
        /// unmanaged resources can be disposed.</para><para>
        /// If False, the method has been called by the runtime from inside the finalizer and you should
        /// not reference other objects--only unmanaged resources can be disposed.</para>
        /// </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
        /// resources;
        /// False if this
        /// method releases
        /// only unmanaged
        /// resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.ItemsLocker is object )
                    {
                        this.ItemsLocker.Dispose();
                        this.ItemsLocker = null;
                    }
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Capacity = 0;
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the Clear Buffer command. </summary>
        /// <remarks> SCPI: ":TRAC:CLE". </remarks>
        /// <value> The ClearBuffer command. </value>
        protected virtual string ClearBufferCommand { get; set; }

        /// <summary> Clears the buffer. </summary>
        public void ClearBuffer()
        {
            _ = this.WriteLine( this.ClearBufferCommand );
        }

        #endregion

        #region " FILL ONCE ENABLED "

        /// <summary> Fill Once enabled. </summary>
        private bool? _FillOnceEnabled;

        /// <summary> Gets or sets the cached Fill Once Enabled sentinel. </summary>
        /// <remarks> When this is enabled, a delay is added before each measurement. </remarks>
        /// <value>
        /// <c>null</c> if Fill Once Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? FillOnceEnabled
        {
            get => this._FillOnceEnabled;

            protected set {
                if ( !Equals( this.FillOnceEnabled, value ) )
                {
                    this._FillOnceEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Fill Once Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyFillOnceEnabled( bool value )
        {
            _ = this.WriteFillOnceEnabled( value );
            return this.QueryFillOnceEnabled();
        }

        /// <summary> Gets or sets the automatic Delay enabled query command. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected virtual string FillOnceEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Fill Once Enabled sentinel. Also sets the
        /// <see cref="FillOnceEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryFillOnceEnabled()
        {
            this.FillOnceEnabled = this.Query( this.FillOnceEnabled, this.FillOnceEnabledQueryCommand );
            return this.FillOnceEnabled;
        }

        /// <summary> Gets or sets the automatic Delay enabled command Format. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected virtual string FillOnceEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Fill Once Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteFillOnceEnabled( bool value )
        {
            this.FillOnceEnabled = this.Write( value, this.FillOnceEnabledCommandFormat );
            return this.FillOnceEnabled;
        }

        #endregion

        #region " CAPACITY "

        /// <summary> The maximum capacity. </summary>
        private int? _MaximumCapacity;

        /// <summary> Gets or sets the maximum Buffer Capacity. </summary>
        /// <value> The Maximum Buffer Capacity or none if not set or unknown. </value>
        public virtual int? MaximumCapacity
        {
            get => this._MaximumCapacity;

            protected set {
                if ( !Nullable.Equals( this.MaximumCapacity, value ) )
                {
                    this._MaximumCapacity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The capacity. </summary>
        private int? _Capacity;

        /// <summary> Gets or sets the cached Buffer Capacity. </summary>
        /// <value> The Buffer Capacity or none if not set or unknown. </value>
        public virtual int? Capacity
        {
            get => this._Capacity;

            protected set {
                if ( !Nullable.Equals( this.Capacity, value ) )
                {
                    this._Capacity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Buffer Capacity. </summary>
        /// <param name="value"> The current Capacity. </param>
        /// <returns> The Capacity or none if unknown. </returns>
        public int? ApplyCapacity( int value )
        {
            _ = this.WriteCapacity( value );
            return this.QueryCapacity();
        }

        /// <summary> Gets or sets the points count query command. </summary>
        /// <remarks> SCPI: :TRAC:POIN:COUN? </remarks>
        /// <value> The points count query command. </value>
        protected virtual string CapacityQueryCommand { get; set; }

        /// <summary> Queries the current Capacity. </summary>
        /// <returns> The Capacity or none if unknown. </returns>
        public int? QueryCapacity()
        {
            if ( !string.IsNullOrWhiteSpace( this.CapacityQueryCommand ) )
            {
                this.Capacity = this.Session.Query( 0, this.CapacityQueryCommand );
            }

            return this.Capacity;
        }

        /// <summary> Gets or sets the points count command format. </summary>
        /// <remarks> SCPI: :TRAC:POIN:COUN {0} </remarks>
        /// <value> The points count query command format. </value>
        protected virtual string CapacityCommandFormat { get; set; }

        /// <summary> Write the Buffer Capacity without reading back the value from the device. </summary>
        /// <param name="value"> The current Capacity. </param>
        /// <returns> The Capacity or none if unknown. </returns>
        public int? WriteCapacity( int value )
        {
            if ( !string.IsNullOrWhiteSpace( this.CapacityCommandFormat ) )
            {
                _ = this.Session.WriteLine( this.CapacityCommandFormat, ( object ) value );
            }

            this.Capacity = value;
            return this.Capacity;
        }

        #endregion

        #region " ACTUAL POINT COUNT "

        /// <summary> Number of ActualPoint. </summary>
        private int? _ActualPointCount;

        /// <summary> Gets or sets the cached Buffer ActualPointCount. </summary>
        /// <value> The Buffer ActualPointCount or none if not set or unknown. </value>
        public int? ActualPointCount
        {
            get => this._ActualPointCount;

            protected set {
                if ( !Nullable.Equals( this.ActualPointCount, value ) )
                {
                    this._ActualPointCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the ActualPoint count query command. </summary>
        /// <remarks> SCPI: :TRAC:ACT? </remarks>
        /// <value> The ActualPoint count query command. </value>
        protected virtual string ActualPointCountQueryCommand { get; set; }

        /// <summary> Queries the current ActualPointCount. </summary>
        /// <returns> The ActualPointCount or none if unknown. </returns>
        public int? QueryActualPointCount()
        {
            if ( !string.IsNullOrWhiteSpace( this.ActualPointCountQueryCommand ) )
            {
                this.ActualPointCount = this.Session.Query( 0, this.ActualPointCountQueryCommand );
            }

            return this.ActualPointCount;
        }

        #endregion

        #region " FIRST POINT NUMBER "

        /// <summary> Number of First Point. </summary>
        private int? _FirstPointNumber;

        /// <summary> Gets or sets the cached buffer First Point Number. </summary>
        /// <value> The buffer First Point Number or none if not set or unknown. </value>
        public int? FirstPointNumber
        {
            get => this._FirstPointNumber;

            protected set {
                if ( !Nullable.Equals( this.FirstPointNumber, value ) )
                {
                    this._FirstPointNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets The First Point Number query command. </summary>
        /// <remarks> SCPI: :TRAC:ACT:STA? </remarks>
        /// <value> The First Point Number query command. </value>
        protected virtual string FirstPointNumberQueryCommand { get; set; }

        /// <summary> Queries the current FirstPointNumber. </summary>
        /// <returns> The First Point Number or none if unknown. </returns>
        public int? QueryFirstPointNumber()
        {
            if ( !string.IsNullOrWhiteSpace( this.FirstPointNumberQueryCommand ) )
            {
                this.FirstPointNumber = this.Session.Query( 0, this.FirstPointNumberQueryCommand );
            }

            return this.FirstPointNumber;
        }

        #endregion

        #region " LAST POINT NUMBER "

        /// <summary> Number of Last Point. </summary>
        private int? _LastPointNumber;

        /// <summary> Gets or sets the cached buffer Last Point Number. </summary>
        /// <value> The buffer Last Point Number or none if not set or unknown. </value>
        public int? LastPointNumber
        {
            get => this._LastPointNumber;

            protected set {
                if ( !Nullable.Equals( this.LastPointNumber, value ) )
                {
                    this._LastPointNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets The Last Point Number query command. </summary>
        /// <remarks> SCPI: :TRAC:ACT:END? </remarks>
        /// <value> The Last Point Number query command. </value>
        protected virtual string LastPointNumberQueryCommand { get; set; }

        /// <summary> Queries the current Last Point Number. </summary>
        /// <returns> The LastPointNumber or none if unknown. </returns>
        public int? QueryLastPointNumber()
        {
            if ( !string.IsNullOrWhiteSpace( this.LastPointNumberQueryCommand ) )
            {
                this.LastPointNumber = this.Session.Query( 0, this.LastPointNumberQueryCommand );
            }

            return this.LastPointNumber;
        }

        #endregion

        #region " AVAILABLE POINT COUNT "

        /// <summary> Number of Available Points. </summary>
        private int? _AvailablePointCount;

        /// <summary> Gets or sets the number of points still available to fill in the buffer. </summary>
        /// <value> The Available Points Count or none if not set or unknown. </value>
        public int? AvailablePointCount
        {
            get => this._AvailablePointCount;

            protected set {
                if ( !Nullable.Equals( this.AvailablePointCount, value ) )
                {
                    this._AvailablePointCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Available Points count query command. </summary>
        /// <remarks> SCPI: ":TRAC:FREE?". </remarks>
        /// <value> The Available Points count query command. </value>
        protected virtual string AvailablePointCountQueryCommand { get; set; }

        /// <summary> Queries the current Available Point Count. </summary>
        /// <returns> The Available Point Count or none if unknown. </returns>
        public int? QueryAvailablePointCount()
        {
            if ( !string.IsNullOrWhiteSpace( this.AvailablePointCountQueryCommand ) )
            {
                this.AvailablePointCount = this.Session.Query( 0, this.AvailablePointCountQueryCommand );
            }

            return this.AvailablePointCount;
        }

        #endregion

        #region " BUFFER FREE "

        /// <summary> Gets or sets the Buffer Free query command. </summary>
        /// <remarks> SCPI: ":TRAC:FREE?". </remarks>
        /// <value> The buffer free query command. </value>
        protected virtual string BufferFreePointCountQueryCommand { get; set; }

        /// <summary> Queries the buffer free count. </summary>
        /// <returns> The buffer free and actual point count. </returns>
        public (int BufferFree, int ActualPointCount) QueryBufferFreePointCount()
        {
            int bufferFree = 0;
            int actualPointCount = 0;
            if ( !string.IsNullOrWhiteSpace( this.ActualPointCountQueryCommand ) )
            {
                string reading = this.Session.QueryTrimTermination( this.BufferFreePointCountQueryCommand );
                if ( !string.IsNullOrEmpty( reading ) )
                {
                    var values = new Queue<string>( reading.Split( ',' ) );
                    if ( values.Any() && int.TryParse( values.Dequeue(), out bufferFree ) )
                    {
                        if ( values.Any() && int.TryParse( values.Dequeue(), out actualPointCount ) )
                        {
                        }
                    }
                }
            }

            this.ActualPointCount = actualPointCount;
            this.AvailablePointCount = bufferFree;
            return (bufferFree, actualPointCount);
        }

        #endregion

        #region " DATA "

        /// <summary> String. </summary>
        private string _Data;

        /// <summary> Gets or sets the cached Buffer Data. </summary>
        /// <value> The data. </value>
        public string Data
        {
            get => this._Data;

            protected set {
                if ( !Equals( this.Data, value ) )
                {
                    this._Data = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the data query command. </summary>
        /// <remarks> SCPI: ":TRAC:DATA?". </remarks>
        /// <value> The data query command. </value>
        protected virtual string DataQueryCommand { get; set; }

        /// <summary> Queries the current Data. </summary>
        /// <returns> The Data or none if unknown. </returns>
        public string QueryData()
        {
            if ( !string.IsNullOrWhiteSpace( this.DataQueryCommand ) )
            {
                _ = this.Session.WriteLine( this.DataQueryCommand );
                // read the entire data.
                this.Data = this.Session.ReadFreeLineTrimEnd();
            }

            return this.Data;
        }

        /// <summary> Queries the current Data. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The Data or empty if none. </returns>
        public string QueryData( string queryCommand )
        {
            if ( !string.IsNullOrWhiteSpace( queryCommand ) )
            {
                this.Data = string.Empty;
                _ = this.Session.WriteLine( queryCommand );
                // read the entire data.
                this.Data = this.Session.ReadFreeLineTrimEnd();
            }

            return this.Data;
        }

        #endregion

        #region " BUFFER STREAM "

        /// <summary> Queries the current Data. </summary>
        /// <returns> The Data or empty if none. </returns>
        public IList<BufferReading> QueryBufferReadings()
        {
            int count = this.QueryActualPointCount().GetValueOrDefault( 0 );
            if ( count > 0 )
            {
                int first = this.QueryFirstPointNumber().GetValueOrDefault( 0 );
                int last = this.QueryLastPointNumber().GetValueOrDefault( 0 );
                return this.QueryBufferReadings( first, last );
            }
            else
            {
                return new List<BufferReading>();
            }
        }

        /// <summary> Gets or sets the buffer read command format. </summary>
        /// <value> The buffer read command format. </value>
        public virtual string BufferReadCommandFormat { get; set; } = ":TRAC:DATA? {0},{1},'defbuffer1',READ,TST,STAT,UNIT";

        /// <summary> Queries the current Data. </summary>
        /// <param name="firstIndex"> Zero-based index of the first. </param>
        /// <param name="lastIndex">  Zero-based index of the last. </param>
        /// <returns> The Data or empty if none. </returns>
        public IList<BufferReading> QueryBufferReadings( int firstIndex, int lastIndex )
        {
            _ = this.QueryData( string.Format( this.BufferReadCommandFormat, firstIndex, lastIndex ) );
            return this.EnumerateBufferReadings( this.Data );
        }

        private IEnumerable<ReadingElementTypes> _OrderedReadingElementTypes;

        /// <summary> Gets or sets a list of types of the readings. </summary>
        /// <value> A list of types of the readings. </value>
        public IEnumerable<ReadingElementTypes> OrderedReadingElementTypes
        {
            get => this._OrderedReadingElementTypes;

            set {
                if ( !Equals( value, this.OrderedReadingElementTypes ) )
                {
                    this._OrderedReadingElementTypes = value;
                    this.NotifyPropertyChanged();
                }

                this.ReadingElementTypes = JoinReadingElementTypes( value );
            }
        }

        /// <summary> Join reading Element types. </summary>
        /// <param name="values"> The values. </param>
        /// <returns> The combined ReadingElementTypes. </returns>
        private static ReadingElementTypes JoinReadingElementTypes( IEnumerable<ReadingElementTypes> values )
        {
            var result = default( ReadingElementTypes );
            foreach ( ReadingElementTypes v in values )
                result |= v;
            return result;
        }

        /// <summary> Type of the measured element. </summary>
        private ReadingElementTypes _MeasuredElementType;

        /// <summary> Gets or sets the type of the measured element. </summary>
        /// <value> The type of the measured element. </value>
        public ReadingElementTypes MeasuredElementType
        {
            get => this._MeasuredElementType;

            set {
                if ( this.MeasuredElementType != value )
                {
                    this._MeasuredElementType = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> List of types of the reading elements. </summary>
        private ReadingElementTypes _ReadingElementTypes;

        /// <summary> Gets or sets a list of types of the reading elements. </summary>
        /// <value> A list of types of the reading elements. </value>
        public ReadingElementTypes ReadingElementTypes
        {
            get => this._ReadingElementTypes;

            set {
                if ( this.ReadingElementTypes != value )
                {
                    this._ReadingElementTypes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Enumerates the buffer readings in this collection. </summary>
        /// <param name="commaSeparatedValues"> The comma separated values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process the buffer readings in this
        /// collection.
        /// </returns>
        public IList<BufferReading> EnumerateBufferReadings( string commaSeparatedValues )
        {
            var l = new List<BufferReading>();
            if ( !string.IsNullOrWhiteSpace( commaSeparatedValues ) )
            {
                var q = new Queue<string>( commaSeparatedValues.Split( ',' ) );
                while ( q.Any() )
                {
                    var reading = new BufferReading( this.MeasuredElementType, q, this.OrderedReadingElementTypes );
                    if ( !this.OrderedReadingElementTypes.Contains( ReadingElementTypes.Units ) )
                    {
                        reading.BuildAmount( this.BufferReadingUnit );
                    }

                    l.Add( reading );
                }
            }

            return l;
        }

        /// <summary> Gets a queue of new buffer readings. </summary>
        /// <value> A thread safe Queue of buffer readings. </value>
        public System.Collections.Concurrent.ConcurrentQueue<BufferReading> NewBufferReadingsQueue { get; private set; }

        /// <summary> Gets the buffer reading binding list. </summary>
        /// <value> The buffer readings. </value>
        public BufferReadingBindingList BufferReadingsBindingList { get; private set; } = new BufferReadingBindingList();

        /// <summary> Gets the number of buffer readings. </summary>
        /// <value> The number of buffer readings. </value>
        public int BufferReadingsCount => this.BufferReadingsBindingList.Count;

        /// <summary> The buffer reading unit. </summary>
        private Arebis.TypedUnits.Unit _BufferReadingUnit;

        /// <summary> Gets or sets the buffer reading unit. </summary>
        /// <value> The buffer reading unit. </value>
        public Arebis.TypedUnits.Unit BufferReadingUnit
        {
            get => this._BufferReadingUnit;

            set {
                if ( !Equals( value, this.BufferReadingUnit ) )
                {
                    this._BufferReadingUnit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last reading. </summary>
        private BufferReading _LastReading;

        /// <summary> Gets or sets the last buffer reading. </summary>
        /// <value> The last buffer reading. </value>
        public BufferReading LastReading
        {
            get => this._LastReading;

            set {
                this._LastReading = value;
                if ( value is null )
                {
                    this.LastReadingCaption = "-.---";
                    this.LastRawReading = string.Empty;
                    this.LastReadingStatus = string.Empty;
                }
                else
                {
                    this.LastReadingCaption = this.LastReading.ReadingCaption;
                    this.LastRawReading = this.LastReading.Reading;
                    this.LastReadingStatus = this.LastReading.StatusReading;
                }
            }
        }

        /// <summary> The last reading status. </summary>
        private string _LastReadingStatus;

        /// <summary> Gets or sets the last buffer reading readings. </summary>
        /// <value> The last buffer reading. </value>
        public string LastReadingStatus
        {
            get => this._LastReadingStatus;

            set {
                if ( !string.Equals( value, this.LastReadingStatus ) )
                {
                    this._LastReadingStatus = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last raw reading. </summary>
        private string _LastRawReading;

        /// <summary> Gets or sets the last buffer reading readings. </summary>
        /// <value> The last buffer reading. </value>
        public string LastRawReading
        {
            get => this._LastRawReading;

            set {
                if ( !string.Equals( value, this.LastRawReading ) )
                {
                    this._LastRawReading = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last reading caption. </summary>
        private string _LastReadingCaption;

        /// <summary> Gets or sets the last buffer reading. </summary>
        /// <value> The last buffer reading. </value>
        public string LastReadingCaption
        {
            get => this._LastReadingCaption;

            set {
                if ( !string.Equals( value, this.LastReadingCaption ) )
                {
                    this._LastReadingCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the items locker. </summary>
        /// <value> The items locker. </value>
        protected ReaderWriterLockSlim ItemsLocker { get; private set; } = new ReaderWriterLockSlim();

        /// <summary> Enqueue range. </summary>
        /// <param name="items"> The items. </param>
        public void EnqueueRange( IList<BufferReading> items )
        {
            if ( items is null || !items.Any() )
                return;
            this.ItemsLocker.EnterWriteLock();
            try
            {
                foreach ( BufferReading item in items )
                    this.NewBufferReadingsQueue.Enqueue( item );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
        }

        /// <summary> Enumerates dequeue range in this collection. </summary>
        /// <returns>
        /// An enumerator that allows for each to be used to process dequeue range in this collection.
        /// </returns>
        public IList<BufferReading> DequeueRange()
        {
            var result = new List<BufferReading>();
            while ( this.NewBufferReadingsQueue.Any() )
            {
                if ( this.NewBufferReadingsQueue.TryDequeue( out BufferReading value ) )
                {
                    result.Add( value );
                }
            }

            return result;
        }

        /// <summary> The buffer streaming enabled. </summary>
        private readonly Core.Concurrent.ConcurrentToken<bool> _BufferStreamingEnabled = new();

        /// <summary> Gets or sets the buffer streaming enabled. </summary>
        /// <value> The buffer streaming enabled. </value>
        public bool BufferStreamingEnabled
        {
            get => this._BufferStreamingEnabled.Value;

            set {
                if ( this.BufferStreamingEnabled != value )
                {
                    this._BufferStreamingEnabled.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The buffer streaming active. </summary>
        private readonly Core.Concurrent.ConcurrentToken<bool> _BufferStreamingActive = new();

        /// <summary> Gets or sets the buffer streaming Active. </summary>
        /// <value> The buffer streaming Active. </value>
        public bool BufferStreamingActive
        {
            get => this._BufferStreamingActive.Value;

            set {
                if ( this.BufferStreamingActive != value )
                {
                    this._BufferStreamingActive.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The buffer streaming alert. </summary>
        private readonly Core.Concurrent.ConcurrentToken<bool> _BufferStreamingAlert = new();

        /// <summary> Gets or sets the buffer streaming Alert. </summary>
        /// <value> The buffer streaming Alert. </value>
        public bool BufferStreamingAlert
        {
            get => this._BufferStreamingAlert.Value;

            set {
                if ( this.BufferStreamingAlert != value )
                {
                    this._BufferStreamingAlert.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Stream buffer. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="triggerSubsystem"> The trigger subsystem. </param>
        /// <param name="pollPeriod">       The poll period. </param>
        private void StreamBufferThis( TriggerSubsystemBase triggerSubsystem, TimeSpan pollPeriod )
        {
            int first = 0;
            this.BufferStreamingEnabled = true;
            this.BufferReadingsBindingList.Clear();
            this.NewBufferReadingsQueue = new System.Collections.Concurrent.ConcurrentQueue<BufferReading>();
            _ = triggerSubsystem.QueryTriggerState();
            this.BufferStreamingActive = true;
            var pollStopwatch = Stopwatch.StartNew();
            while ( triggerSubsystem.IsTriggerStateActive() && this.BufferStreamingEnabled )
            {
                if ( first == 0 )
                    first = this.QueryFirstPointNumber().GetValueOrDefault( 0 );
                if ( first > 0 )
                {
                    int last = this.QueryLastPointNumber().GetValueOrDefault( 0 );
                    if ( last - first + 1 > this.BufferReadingsCount )
                    {
                        var newReadings = this.QueryBufferReadings( this.BufferReadingsCount + 1, last );
                        this.BufferReadingsBindingList.Add( newReadings );
                        this.EnqueueRange( newReadings );
                        this.SyncNotifyPropertyChanged( nameof( this.BufferReadingsCount ) );
                        Core.ApplianceBase.DoEvents();
                        pollStopwatch.Restart();
                    }
                }

                pollPeriod.Subtract( pollStopwatch.Elapsed ).SpinWait();
                pollStopwatch.Restart();
                Core.ApplianceBase.DoEvents();
                _ = triggerSubsystem.QueryTriggerState();
            }

            this.BufferStreamingActive = false;
        }

        /// <summary> Stream buffer. </summary>
        /// <remarks> David, 2020-07-23. </remarks>
        /// <param name="triggerSubsystem"> The trigger subsystem. </param>
        /// <param name="pollPeriod">       The poll period. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public virtual void StreamBuffer( TriggerSubsystemBase triggerSubsystem, TimeSpan pollPeriod )
        {
            try
            {
                this.StreamBufferThis( triggerSubsystem, pollPeriod );
            }
            catch
            {
                // stop buffer streaming; the exception is handled in the Async Completed event handler
                this.BufferStreamingActive = false;
                this.BufferStreamingAlert = true;
                this.BufferStreamingEnabled = false;
            }
        }

        /// <summary> Gets or sets the buffer stream tasker. </summary>
        /// <value> The buffer stream tasker. </value>
        public Core.Tasker BufferStreamTasker { get; private set; }

        /// <summary> Starts buffer stream. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="triggerSubsystem"> The trigger subsystem. </param>
        /// <param name="pollPeriod">       The poll period. </param>
        public void StartBufferStream( TriggerSubsystemBase triggerSubsystem, TimeSpan pollPeriod )
        {
            if ( triggerSubsystem is null )
                throw new ArgumentNullException( nameof( triggerSubsystem ) );
            this.BufferStreamTasker.StartAction( () => this.StreamBuffer( triggerSubsystem, pollPeriod ) );
        }

        /// <summary>
        /// Estimates the stream stop timeout interval as scale margin times the cycle and poll intervals.
        /// </summary>
        /// <remarks> David, 2020-04-13. </remarks>
        /// <param name="streamCycleDuration"> Duration of the stream cycle. </param>
        /// <param name="pollInterval">        The poll interval. </param>
        /// <param name="scaleMargin">         The scale margin. </param>
        /// <returns> A TimeSpan. </returns>
        public static TimeSpan EstimateStreamStopTimeoutInterval( TimeSpan streamCycleDuration, TimeSpan pollInterval, double scaleMargin )
        {
            return TimeSpan.FromTicks( ( long ) (scaleMargin * (streamCycleDuration.Ticks + pollInterval.Ticks)) );
        }

        /// <summary> Stops buffer stream and wait for completion or timeout. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) StopBufferStream( TimeSpan timeout )
        {
            (bool Success, string Details) result;
            this.BufferStreamingEnabled = false;
            var status = this.BufferStreamTasker.AwaitCompletion( timeout );
            result = System.Threading.Tasks.TaskStatus.RanToCompletion == status ? this.BufferStreamingActive ? (false, $"Failed stopping buffer streaming; still active") : (true, "buffer streaming completed") : (false, $"buffer streaming task existed with a {status} {nameof( System.Threading.Tasks.TaskStatus )}");
            return result;
        }

        #endregion

    }
}
