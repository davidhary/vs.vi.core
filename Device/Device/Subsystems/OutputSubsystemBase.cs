using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by an Output Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class OutputSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="OutputSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected OutputSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DefineOutputOffModeReadWrites();
            this.DefineOutputTerminalsModeReadWrites();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.OutputOnState = false;
            this.OutputOffMode = OutputOffModes.Normal;
            this.OutputTerminalsMode = OutputTerminalsModes.Front;
        }

        #endregion

        #region " ON/OFF STATE "

        /// <summary> State of the output on. </summary>
        private bool? _OutputOnState;

        /// <summary> Gets or sets the cached output on/off state. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? OutputOnState
        {
            get => this._OutputOnState;

            protected set {
                if ( !Equals( this.OutputOnState, value ) )
                {
                    this._OutputOnState = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the output on/off state. </summary>
        /// <param name="value"> if set to <c>True</c> if turning on; False if turning output off. </param>
        /// <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        public bool? ApplyOutputOnState( bool value )
        {
            _ = this.WriteOutputOnState( value );
            return this.QueryOutputOnState();
        }

        /// <summary> Gets or sets the output on state query command. </summary>
        /// <value> The output on state query command. </value>
        protected virtual string OutputOnStateQueryCommand { get; set; }

        /// <summary>
        /// Queries the output on/off state. Also sets the <see cref="OutputOnState">output on</see>
        /// sentinel.
        /// </summary>
        /// <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        public virtual bool? QueryOutputOnState()
        {
            this.OutputOnState = this.StatusSubsystem.Query( this.OutputOnState.GetValueOrDefault( true ), this.OutputOnStateQueryCommand );
            return this.OutputOnState;
        }

        /// <summary> Gets or sets the output on state command format. </summary>
        /// <value> The output on state command format. </value>
        protected virtual string OutputOnStateCommandFormat { get; set; }

        /// <summary> Writes the output on/off state. Does not read back from the instrument. </summary>
        /// <param name="value"> if set to <c>True</c> [is on]. </param>
        /// <returns> <c>True</c> if on; otherwise <c>False</c>. </returns>
        public virtual bool? WriteOutputOnState( bool value )
        {
            _ = this.Session.WriteLine( this.OutputOnStateQueryCommand, ( object ) Conversions.ToInteger( value ) );
            this.OutputOnState = value;
            return this.OutputOnState;
        }

        #endregion

    }
}
