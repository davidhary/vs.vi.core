using System;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by an Output Subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-12-12, 3.0.5093. </para>
    /// </remarks>
    public abstract class AccessSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="OutputSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected AccessSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.CertifyTimeout = TimeSpan.FromMilliseconds( 20000d );
            this.Certified = new bool?();
        }

        #endregion

        #region " CERTIFY "

        /// <summary> The certify timeout. </summary>
        private TimeSpan _CertifyTimeout;

        /// <summary> Gets or sets the timeout time span allowed for certifying the device. </summary>
        /// <value> The certify timeout. </value>
        public TimeSpan CertifyTimeout
        {
            get => this._CertifyTimeout;

            set {
                if ( !this.CertifyTimeout.Equals( value ) )
                {
                    this._CertifyTimeout = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> State of the output on. </summary>
        private bool? _Certified;

        /// <summary> Gets or sets the cached certification sentinel. </summary>
        /// <value>
        /// <c>null</c> if not known; <c>True</c> if certified on; otherwise, <c>False</c>.
        /// </value>
        public bool? Certified
        {
            get => this._Certified;

            protected set {
                if ( !Equals( this.Certified, value ) )
                {
                    this._Certified = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Certifies. </summary>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>null</c> if not known; <c>True</c> if certified on; otherwise, <c>False</c>.
        /// </returns>
        public abstract bool? Certify( string value );

        /// <summary> Tries to certify. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> <c>True</c> if certified on; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool TryCertify( string value )
        {
            try
            {
                return this.Certify( value ).GetValueOrDefault( false );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( "certifying access", ex );
                return false;
            }
        }

        #endregion

        #region " RESOURCES "

        /// <summary> The certified instruments. </summary>
        private string _CertifiedInstruments;

        /// <summary> Gets or sets the list of certified instruments. </summary>
        /// <value> The certified instruments. </value>
        public string CertifiedInstruments
        {
            get => this._CertifiedInstruments;

            protected set {
                this._CertifiedInstruments = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The released instruments. </summary>
        private string _ReleasedInstruments;

        /// <summary> Gets or sets the list of released instruments. </summary>
        /// <value> The released instruments. </value>
        public string ReleasedInstruments
        {
            get => this._ReleasedInstruments;

            set {
                this._ReleasedInstruments = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The salt. </summary>
        private string _Salt;

        /// <summary> Gets or sets the list of released instruments. </summary>
        /// <value> The released instruments. </value>
        public string Salt
        {
            get => this._Salt;

            set {
                this._Salt = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion

        #region " ACCESS MANAGEMENT "

        /// <summary> Checks if the custom scripts loaded successfully. </summary>
        /// <returns> <c>True</c> if loaded; otherwise, <c>False</c>. </returns>
        public virtual bool Loaded()
        {
            return false;
        }

        /// <summary> Gets the release code for the controller instrument. </summary>
        /// <param name="serialNumber"> The serial number. </param>
        /// <param name="salt">         The released instruments. </param>
        /// <returns> The release value of the controller instrument. </returns>
        public virtual string ReleaseValue( string serialNumber, string salt )
        {
            return string.Empty;
        }

        #endregion

    }
}
