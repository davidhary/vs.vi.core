using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI
{
    public partial class ArmLayerSubsystemBase
    {

        #region " ARM LAYER BYPASS MODE "

        /// <summary> Define arm layer bypass mode read writes. </summary>
        private void DefineArmLayerBypassModeReadWrites()
        {
            this.ArmLayerBypassModeReadWrites = new Pith.EnumReadWriteCollection();
            foreach ( TriggerLayerBypassModes enumValue in Enum.GetValues( typeof( TriggerLayerBypassModes ) ) )
                this.ArmLayerBypassModeReadWrites.Add( enumValue );
        }

        /// <summary> Gets a dictionary of trigger layer bypass mode parses. </summary>
        /// <value> A Dictionary of trigger layer bypass mode parses. </value>
        public Pith.EnumReadWriteCollection ArmLayerBypassModeReadWrites { get; private set; }

        /// <summary>
        /// Returns true if bypassing the source Arm using <see cref="TriggerLayerBypassModes.Acceptor"/>
        /// .
        /// </summary>
        /// <value> True if Arm layer is bypassed. </value>
        public bool? IsArmLayerBypass => this.ArmLayerBypassMode.HasValue ? this.ArmLayerBypassMode.Value == TriggerLayerBypassModes.Acceptor : new bool?();

        /// <summary> The arm layer bypass mode. </summary>
        private TriggerLayerBypassModes? _ArmLayerBypassMode;

        /// <summary> Gets or sets the cached arm layer bypass mode. </summary>
        /// <value>
        /// The <see cref="ArmLayerBypassMode">Arm Layer Bypass Mode</see> or none if not set or unknown.
        /// </value>
        public TriggerLayerBypassModes? ArmLayerBypassMode
        {
            get => this._ArmLayerBypassMode;

            protected set {
                if ( !this.ArmLayerBypassMode.Equals( value ) )
                {
                    this._ArmLayerBypassMode = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.IsArmLayerBypass ) );
                }
            }
        }

        /// <summary> Writes and reads back the Arm Layer Bypass Mode. </summary>
        /// <param name="value"> The Arm Layer Bypass Mode. </param>
        /// <returns>
        /// The <see cref="ArmLayerBypassMode">source  Arm Layer Bypass Mode</see> or none if unknown.
        /// </returns>
        public TriggerLayerBypassModes? ApplyArmLayerBypassMode( TriggerLayerBypassModes value )
        {
            _ = this.WriteArmLayerBypassMode( value );
            return this.QueryArmLayerBypassMode();
        }

        /// <summary> Gets or sets the Arm Layer Bypass Mode query command. </summary>
        /// <remarks> SCPI: ":ARM:LAYx:DIR?". </remarks>
        /// <value> The Arm Layer Bypass Mode query command. </value>
        protected virtual string ArmLayerBypassModeQueryCommand { get; set; }

        /// <summary> Queries the Arm Layer Bypass Mode. </summary>
        /// <returns>
        /// The <see cref="ArmLayerBypassMode"> Arm Layer Bypass Mode</see> or none if unknown.
        /// </returns>
        public TriggerLayerBypassModes? QueryArmLayerBypassMode()
        {
            this.ArmLayerBypassMode = this.Query( this.ArmLayerBypassModeQueryCommand, this.ArmLayerBypassMode );
            return this.ArmLayerBypassMode;
        }

        /// <summary> Gets or sets the Arm Layer Bypass Mode command format. </summary>
        /// <remarks> SCPI: ":ARM:LAYx:DIR {0}". </remarks>
        /// <value> The Arm Layer Bypass Mode command format. </value>
        protected virtual string ArmLayerBypassModeCommandFormat { get; set; }

        /// <summary>
        /// Writes the Arm Layer Bypass Mode without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The Arm Layer Bypass Mode. </param>
        /// <returns>
        /// The <see cref="ArmLayerBypassMode"> ARM Directio`n</see> or none if unknown.
        /// </returns>
        public TriggerLayerBypassModes? WriteArmLayerBypassMode( TriggerLayerBypassModes value )
        {
            this.ArmLayerBypassMode = this.Write( this.ArmLayerBypassModeCommandFormat, value );
            return this.ArmLayerBypassMode;
        }

        #region " OVERRIDES "

        /// <summary> Arm Layer Bypass Mode getter. </summary>
        /// <returns> A Nullable(Of T) </returns>
        public int? ArmLayerBypassModeGetter()
        {
            return ( int? ) this.ArmLayerBypassMode;
        }

        /// <summary> Arm Layer Bypass Mode setter. </summary>
        /// <param name="value"> The current ArmCount. </param>
        public void ArmLayerBypassModeSetter( int? value )
        {
            this.ArmLayerBypassMode = ( TriggerLayerBypassModes ) Conversions.ToInteger( value );
        }

        /// <summary> Arm Layer Bypass Mode reader. </summary>
        /// <returns> An Integer? </returns>
        public int? ArmLayerBypassModeReader()
        {
            return ( int? ) this.QueryArmLayerBypassMode();
        }

        /// <summary> Arm Layer Bypass Mode writer. </summary>
        /// <param name="value"> The Arm Layer Bypass Mode. </param>
        /// <returns> An Integer? </returns>
        public int? ArmLayerBypassModeWriter( int value )
        {
            return ( int? ) this.WriteArmLayerBypassMode( ( TriggerLayerBypassModes ) Conversions.ToInteger( value ) );
        }

        #endregion

        #endregion

        #region " ARM SOURCE "

        /// <summary> Define arm source read writes. </summary>
        private void DefineArmSourceReadWrites()
        {
            this.ArmSourceReadWrites = new Pith.EnumReadWriteCollection();
            foreach ( ArmSources enumValue in Enum.GetValues( typeof( ArmSources ) ) )
                this.ArmSourceReadWrites.Add( enumValue );
        }

        /// <summary> Gets or sets a dictionary of Arm source parses. </summary>
        /// <value> A Dictionary of Arm source parses. </value>
        public Pith.EnumReadWriteCollection ArmSourceReadWrites { get; private set; }

        /// <summary> The supported arm sources. </summary>
        private ArmSources _SupportedArmSources;

        /// <summary> Gets or sets the supported Arm sources. </summary>
        /// <value> The supported Arm sources. </value>
        public ArmSources SupportedArmSources
        {
            get => this._SupportedArmSources;

            set {
                if ( !this.SupportedArmSources.Equals( value ) )
                {
                    this._SupportedArmSources = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The arm source. </summary>
        private ArmSources? _ArmSource;

        /// <summary> Gets or sets the cached source ArmSource. </summary>
        /// <value>
        /// The <see cref="ArmSource">source Arm Source</see> or none if not set or unknown.
        /// </value>
        public ArmSources? ArmSource
        {
            get => this._ArmSource;

            protected set {
                if ( !this.ArmSource.Equals( value ) )
                {
                    this._ArmSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Arm Source. </summary>
        /// <param name="value"> The  Source Arm Source. </param>
        /// <returns> The <see cref="ArmSource">source Arm Source</see> or none if unknown. </returns>
        public ArmSources? ApplyArmSource( ArmSources value )
        {
            _ = this.WriteArmSource( value );
            return this.QueryArmSource();
        }

        /// <summary> Gets or sets the Arm source query command. </summary>
        /// <remarks> SCPI: ":ARM:SOUR?". </remarks>
        /// <value> The Arm source query command. </value>
        protected virtual string ArmSourceQueryCommand { get; set; }

        /// <summary> Queries the Arm source. </summary>
        /// <returns> The <see cref="ArmSource">Arm source</see> or none if unknown. </returns>
        public ArmSources? QueryArmSource()
        {
            this.ArmSource = this.Query( this.ArmSourceQueryCommand, this.ArmSource.GetValueOrDefault( ArmSources.None ), this.ArmSourceReadWrites );
            return this.ArmSource;
        }

        /// <summary> Gets or sets the Arm source command format. </summary>
        /// <remarks> SCPI: ":ARM:SOUR {0}". </remarks>
        /// <value> The write Arm source command format. </value>
        protected virtual string ArmSourceCommandFormat { get; set; }

        /// <summary> Writes the Arm Source without reading back the value from the device. </summary>
        /// <param name="value"> The Arm Source. </param>
        /// <returns> The <see cref="ArmSource">Arm Source</see> or none if unknown. </returns>
        public ArmSources? WriteArmSource( ArmSources value )
        {
            this.ArmSource = this.Write( this.ArmSourceCommandFormat, value, this.ArmSourceReadWrites );
            return this.ArmSource;
        }

        #endregion

    }

    /// <summary> Enumerates the arm layer control sources. </summary>
    [Flags]
    public enum ArmSources
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined ()" )]
        None = 0,

        /// <summary> An enum constant representing the bus option. </summary>
        [System.ComponentModel.Description( "Bus (BUS)" )]
        Bus = 1,

        /// <summary> An enum constant representing the external option. </summary>
        [System.ComponentModel.Description( "External (EXT)" )]
        External = 2,

        /// <summary> An enum constant representing the hold option. </summary>
        [System.ComponentModel.Description( "Hold operation (HOLD)" )]
        Hold = 4,

        /// <summary> An enum constant representing the immediate option. </summary>
        [System.ComponentModel.Description( "Immediate (IMM)" )]
        Immediate = 8,

        /// <summary> An enum constant representing the manual option. </summary>
        [System.ComponentModel.Description( "Manual (MAN)" )]
        Manual = 16,

        /// <summary> An enum constant representing the timer option. </summary>
        [System.ComponentModel.Description( "Timer (TIM)" )]
        Timer = 32,

        /// <summary> Event detection for the arm layer is satisfied when either a positive-going or
        /// a negative-going pulse (via the SOT line of the Digital I/O) is received. </summary>
        [System.ComponentModel.Description( "SOT Pulsed High or Low (BSTES)" )]
        StartTestBoth = 64,

        /// <summary> Event detection for the arm layer is satisfied when a positive-going pulse
        /// (via the SOT line of the Digital I/O) is received.  </summary>
        [System.ComponentModel.Description( "SOT Pulsed High (PSTES)" )]
        StartTestHigh = 128,

        /// <summary> Event detection for the arm layer is satisfied when a negative-going pulse
        /// (via the SOT line of the Digital I/O) is received. </summary>
        [System.ComponentModel.Description( "SOT Pulsed High (NSTES)" )]
        StartTestLow = 256,

        /// <summary> Event detection occurs when an input trigger via the Trigger Link input line is
        /// received. See “Trigger link,” 2400 manual page 11-19, For more information. With TLINk selected, you
        /// can Loop around the Arm Event Detector by setting the Event detector bypass. </summary>
        [System.ComponentModel.Description( "Trigger Link (TLIN)" )]
        TriggerLink = 512,

        /// <summary> An enum constant representing all option. </summary>
        [System.ComponentModel.Description( "All" )]
        All = 1023
    }
}
