
namespace isr.VI
{

    /// <summary> Defines a Scpi Instrument Subsystem. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-07-01, 4.0.6026. </para>
    /// </remarks>
    public abstract class InstrumentSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="InstrumentSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected InstrumentSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " DMM Installed "

        /// <summary> DMM Installed. </summary>
        private bool? _DmmInstalled;

        /// <summary> Gets or sets the cached DMM Installed sentinel. </summary>
        /// <value>
        /// <c>null</c> if DMM Installed is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? DmmInstalled
        {
            get => this._DmmInstalled;

            protected set {
                if ( !Equals( this.DmmInstalled, value ) )
                {
                    this._DmmInstalled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the DMM Installed query command. </summary>
        /// <value> The DMM Installed query command. </value>
        protected virtual string DmmInstalledQueryCommand { get; set; }

        /// <summary>
        /// Queries the DMM Installed sentinel. Also sets the
        /// <see cref="DmmInstalled">DMM Installed</see> sentinel.
        /// </summary>
        /// <returns>
        /// <c>null</c> Instrument status is not known; <c>True</c> if DmmInstalled; otherwise,
        /// <c>False</c>.
        /// </returns>
        public bool? QueryDmmInstalled()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.DmmInstalled.GetValueOrDefault( true ) );
            if ( !string.IsNullOrWhiteSpace( this.DmmInstalledQueryCommand ) )
            {
                this.DmmInstalled = this.Session.Query( this.DmmInstalled.GetValueOrDefault( true ), this.DmmInstalledQueryCommand );
            }

            return this.DmmInstalled;
        }

        #endregion

    }
}
