using System;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by a Prober Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract class ProberSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="OutputSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected ProberSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.ErrorRead = false;
            this.LastReading = string.Empty;
            this.IdentityRead = false;
            this.MessageCompleted = false;
            this.MessageFailed = false;
            this.PatternCompleteReceived = new bool?();
            this.SetModeSent = false;
            this.IsFirstTestStart = new bool?();
            this.RetestRequested = new bool?();
            this.TestAgainRequested = new bool?();
            this.TestCompleteSent = new bool?();
            this.TestStartReceived = new bool?();
            this.WaferStartReceived = new bool?();
        }

        #endregion

        #region " ERROR READ "

        /// <summary> True to error read. </summary>
        private bool _ErrorRead;

        /// <summary> Gets or sets the Error Read sentinel. </summary>
        /// <value> The Error Read. </value>
        public bool ErrorRead
        {
            get => this._ErrorRead;

            set {
                this._ErrorRead = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " IDENTITY READ "

        /// <summary> True to identity read. </summary>
        private bool _IdentityRead;

        /// <summary> Gets or sets the Identity Read sentinel. </summary>
        /// <value> The Identity Read. </value>
        public bool IdentityRead
        {
            get => this._IdentityRead;

            set {
                this._IdentityRead = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " MESSAGE FAILED "

        /// <summary> True if message failed. </summary>
        private bool _MessageFailed;

        /// <summary> Gets or sets the message failed. </summary>
        /// <value> The message failed. </value>
        public bool MessageFailed
        {
            get => this._MessageFailed;

            set {
                this._MessageFailed = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " MESSAGE COMPLETED "

        /// <summary> True if message completed. </summary>
        private bool _MessageCompleted;

        /// <summary> Gets or sets the message Completed. </summary>
        /// <value> The message Completed. </value>
        public bool MessageCompleted
        {
            get => this._MessageCompleted;

            set {
                this._MessageCompleted = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " PATTERN COMPLETE "

        /// <summary> A sentinel indicating that the Pattern Complete message was received. </summary>
        private bool? _PatternCompleteReceived;

        /// <summary> Gets or sets the cached Pattern Complete message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if Pattern Complete was received; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? PatternCompleteReceived
        {
            get => this._PatternCompleteReceived;

            protected set {
                this._PatternCompleteReceived = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
                if ( value.GetValueOrDefault( false ) )
                {
                    // if pattern completed, turn off all other flags.
                    this.UnhandledMessageReceived = false;
                    this.TestCompleteSent = false;
                    this.TestStartReceived = false;
                    this.WaferStartReceived = false;
                }
            }
        }

        #endregion

        #region " SET MODE SENT "

        /// <summary> A sentinel indicating that the Set Mode message was Sent. </summary>
        private bool _SetModeSent;

        /// <summary> Gets or sets the cached Set Mode message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if Set Mode was Sent; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool SetModeSent
        {
            get => this._SetModeSent;

            protected set {
                this._SetModeSent = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " TEST COMPLETE SENT "

        /// <summary> A sentinel indicating that the Test Complete message was Sent. </summary>
        private bool? _TestCompleteSent;

        /// <summary> Gets or sets the cached Test Complete message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if Test Complete was Sent; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? TestCompleteSent
        {
            get => this._TestCompleteSent;

            set {
                this._TestCompleteSent = value;
                if ( value.GetValueOrDefault( false ) )
                {
                    // if pattern completed, turn off all other flags.
                    this.UnhandledMessageReceived = false;
                    this.TestStartReceived = false;
                }

                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " TEST START "

        /// <summary> A sentinel indicating that the test start message was received. </summary>
        private bool? _TestStartReceived;

        /// <summary> Gets or sets the cached test start message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if test start was received; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? TestStartReceived
        {
            get => this._TestStartReceived;

            protected set {
                this._TestStartReceived = value;
                if ( value.GetValueOrDefault( false ) )
                {
                    // turn off relevant sentinels.
                    this.UnhandledMessageReceived = false;
                    this.PatternCompleteReceived = false;
                    this.TestCompleteSent = false;
                }

                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> A sentinel indicating that the retest is requested. </summary>
        private bool? _RetestRequested;

        /// <summary> Gets or sets the cached retest requested sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if retest is request; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? RetestRequested
        {
            get => this._RetestRequested;

            protected set {
                this._RetestRequested = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> A sentinel indicating that the Test Again is requested. </summary>
        private bool? _TestAgainRequested;

        /// <summary> Gets or sets the cached Test Again requested sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if TestAgain is request; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? TestAgainRequested
        {
            get => this._TestAgainRequested;

            protected set {
                this._TestAgainRequested = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " FIRST TEST START "

        /// <summary> A sentinel indicating that the test start message was received. </summary>
        private bool? _IsFirstTestStart;

        /// <summary> Gets or sets the cached first test start message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if test start was received; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? IsFirstTestStart
        {
            get => this._IsFirstTestStart;

            protected set {
                this._IsFirstTestStart = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " WAFER START "

        /// <summary> A sentinel indicating that the Wafer start message was received. </summary>
        private bool? _WaferStartReceived;

        /// <summary> Gets or sets the cached Wafer start message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if Wafer start was received; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? WaferStartReceived
        {
            get => this._WaferStartReceived;

            protected set {
                this._WaferStartReceived = value;
                if ( value.GetValueOrDefault( false ) )
                {
                    // turn off relevant sentinels.
                    this.UnhandledMessageReceived = false;
                    this.PatternCompleteReceived = false;
                    this.TestCompleteSent = false;
                    this.TestStartReceived = false;
                }

                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " UNHANDLED MESSAGE RECEIVED "

        /// <summary> A sentinel indicating that the Unhandled Message was received. </summary>
        private bool? _UnhandledMessageReceived;

        /// <summary> Gets or sets the cached Unhandled Message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if Unhandled Message was received; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? UnhandledMessageReceived
        {
            get => this._UnhandledMessageReceived;

            protected set {
                this._UnhandledMessageReceived = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " UNHANDLED MESSAGE SENT "

        /// <summary> A sentinel indicating that the Unhandled Message was Sent. </summary>
        private bool? _UnhandledMessageSent;

        /// <summary> Gets or sets the cached Unhandled Message sentinel. </summary>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if Unhandled Message was Sent; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? UnhandledMessageSent
        {
            get => this._UnhandledMessageSent;

            protected set {
                this._UnhandledMessageSent = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " FETCH "

        /// <summary> The last reading. </summary>
        private string _LastReading;

        /// <summary> Gets or sets the last reading. </summary>
        /// <value> The last reading. </value>
        public string LastReading
        {
            get => this._LastReading;

            set {
                if ( string.IsNullOrWhiteSpace( this.LastReading ) )
                {
                    if ( string.IsNullOrWhiteSpace( value ) )
                    {
                        return;
                    }
                    else
                    {
                        this._LastReading = string.Empty;
                    }
                }

                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                this._LastReading = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Parses the message. </summary>
        /// <param name="reading"> The reading. </param>
        public abstract void ParseReading( string reading );

        /// <summary>
        /// Fetches and parses a message from the instrument. The message must already be present.
        /// </summary>
        /// <param name="readStatusDelay"> The read status delay. </param>
        /// <param name="readDelay">       The read delay. </param>
        public void FetchAndParse( TimeSpan readStatusDelay, TimeSpan readDelay )
        {
            _ = this.PublishVerbose( $"waiting read status delay {readStatusDelay:ss\\.fff};. " );
            Core.ApplianceBase.DoEventsWait( readStatusDelay );
            _ = this.PublishVerbose( $"reading status;. " );
            _ = this.Session.ReadStatusRegister();
            _ = this.PublishVerbose( $"waiting read delay {readDelay:ss\\.fff};. " );
            Core.ApplianceBase.DoEventsWait( readDelay );
            _ = this.PublishVerbose( "Fetching;. " );
            this.LastReading = this.Session.ReadLineTrimEnd();
            _ = this.PublishVerbose( "Parsing;. {0}", this.LastReading );
            this.ParseReading( this.LastReading );
        }

        /// <summary>
        /// Fetches and parses a message from the instrument. The message must already be present.
        /// </summary>
        public void FetchAndParse()
        {
            _ = this.PublishVerbose( "Fetching;. " );
            this.LastReading = this.Session.ReadLineTrimEnd();
            _ = this.PublishVerbose( "Parsing;. {0}", this.LastReading );
            this.ParseReading( this.LastReading );
        }

        /// <summary>
        /// Fetches and parses a message from the instrument. The message must already be present.
        /// </summary>
        /// <param name="readDelay"> The read delay. </param>
        public void FetchAndParse( TimeSpan readDelay )
        {
            _ = this.PublishVerbose( $"waiting read delay {readDelay:ss\\.fff};. " );
            Core.ApplianceBase.DoEventsWait( readDelay );
            _ = this.PublishVerbose( "Fetching;. " );
            this.LastReading = this.Session.ReadLineTrimEnd();
            _ = this.PublishVerbose( $"Parsing;. {this.LastReading};. " );
            this.ParseReading( this.LastReading );
        }

        #endregion

    }
}
