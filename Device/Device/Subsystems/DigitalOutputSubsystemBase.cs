using System;

using isr.Core.Primitives;

namespace isr.VI
{

    /// <summary> Defines the SCPI Digital Output subsystem. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-11-05. Created based on SCPI 5.1 library.  </para><para>
    /// David, 2008-03-25, 5.0.3004. Port to new SCPI library. </para>
    /// </remarks>
    public abstract partial class DigitalOutputSubsystemBase : SubsystemPlusStatusBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalOutputSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected DigitalOutputSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DefineOutputModeReadWrites();
            this.DefineDigitalActiveLevelReadWrites();
            this.DigitalOutputLines = new DigitalOutputLineCollection();
            this.BinningStrobeTasker = new Core.Tasker();
            // make sure the dispatcher is instantiated. This takes a bit of time the first time around
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> True to disposed value. </summary>
        private bool _DisposedValue;

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        /// <param name="disposing"> True to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( !this._DisposedValue )
            {
                if ( disposing )
                {
                    this.BinningStrobeTasker?.Dispose();
                }

                this._DisposedValue = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        public void Dispose()
        {
            this.Dispose( disposing: true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.BitSize = 4;
            this.Level = 15;
            this.AutoClearEnabled = false;
            this._DelayRange = new Range<TimeSpan>( TimeSpan.Zero, TimeSpan.FromSeconds( 60d ) );
            this.NotifyPropertyChanged( nameof( this.DelayRange ) );
            this.Delay = TimeSpan.FromSeconds( 0.0001d );
            this.OutputMode = OutputModes.EndTest;
            this.CurrentDigitalActiveLevel = DigitalActiveLevels.Low;
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the Digital Outputs clear command. </summary>
        /// <remarks> SCPI: ":SOUR2:CLE". </remarks>
        /// <value> The Digital Outputs clear command. </value>
        protected virtual string ClearCommand { get; set; }

        /// <summary> Clears Digital Outputs. </summary>
        public void ClearOutput()
        {
            if ( !string.IsNullOrWhiteSpace( this.ClearCommand ) )
                this.Session.Execute( this.ClearCommand );
        }

        #endregion

        #region " AUTO CLEAR ENABLED "

        /// <summary> The automatic clear enabled. </summary>
        private bool? _AutoClearEnabled;

        /// <summary> Gets or sets the cached Digital Outputs Auto Clear enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Digital Outputs Auto Clear enabled is not known; <c>True</c> if output is on;
        /// otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoClearEnabled
        {
            get => this._AutoClearEnabled;

            protected set {
                if ( !Equals( this.AutoClearEnabled, value ) )
                {
                    this._AutoClearEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Digital Outputs Auto Clear enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if Enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoClearEnabled( bool value )
        {
            _ = this.WriteAutoClearEnabled( value );
            return this.QueryAutoClearEnabled();
        }

        /// <summary> Gets or sets the Digital Outputs Auto Clear enabled query command. </summary>
        /// <remarks> SCPI: ":SOUR2:CLE:AUTO?". </remarks>
        /// <value> The Digital Outputs Auto Clear enabled query command. </value>
        protected virtual string AutoClearEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Delay Enabled sentinel. Also sets the
        /// <see cref="AutoClearEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoClearEnabled()
        {
            this.AutoClearEnabled = this.Query( this.AutoClearEnabled, this.AutoClearEnabledQueryCommand );
            return this.AutoClearEnabled;
        }

        /// <summary> Gets or sets the Digital Outputs Auto Clear enabled command Format. </summary>
        /// <remarks> SCPI: ":SOUR2:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Digital Outputs Auto Clear enabled query command. </value>
        protected virtual string AutoClearEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoClearEnabled( bool value )
        {
            this.AutoClearEnabled = this.Write( value, this.AutoClearEnabledCommandFormat );
            return this.AutoClearEnabled;
        }

        #endregion

        #region " BIT SIZE "

        /// <summary> Size of the bit. </summary>
        private int? _BitSize;

        /// <summary> Gets or sets the cached Digital Outputs Bit Size. </summary>
        /// <value> The Digital Outputs Bit Size or none if not set or unknown. </value>
        public int? BitSize
        {
            get => this._BitSize;

            protected set {
                if ( !Nullable.Equals( this.BitSize, value ) )
                {
                    this._BitSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Digital Outputs Bit Size. </summary>
        /// <param name="value"> The current Digital Outputs Bit Size. </param>
        /// <returns> The Digital Outputs Bit Size or none if unknown. </returns>
        public int? ApplyBitSize( int value )
        {
            _ = this.WriteBitSize( value );
            return this.QueryBitSize();
        }

        /// <summary> Gets or sets the Lower Limit Bit Size query command. </summary>
        /// <remarks> SCPI: ":SOUR2:BSIZ?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string BitSizeQueryCommand { get; set; }

        /// <summary> Queries the current Lower Limit Bit Size. </summary>
        /// <returns> The Lower Limit Bit Size or none if unknown. </returns>
        public int? QueryBitSize()
        {
            this.BitSize = this.Query( this.BitSize, this.BitSizeQueryCommand );
            return this.BitSize;
        }

        /// <summary> Gets or sets the Lower Limit Bit Size query command. </summary>
        /// <remarks> SCPI: "::SOUR2:BSIZ {0}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string BitSizeCommandFormat { get; set; }

        /// <summary>
        /// Sets back the Lower Limit Bit Size without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current Lower Limit Bit Size. </param>
        /// <returns> The Lower Limit Bit Size or none if unknown. </returns>
        public int? WriteBitSize( int value )
        {
            this.BitSize = this.Write( value, this.BitSizeCommandFormat );
            return this.BitSize;
        }

        #endregion

        #region " DELAY (PULSE WIDTH) "

        /// <summary> The delay range. </summary>
        private Range<TimeSpan> _DelayRange;

        /// <summary> Gets or sets the delay (pulse width) range. </summary>
        /// <value> The delay range. </value>
        public virtual Range<TimeSpan> DelayRange
        {
            get => this._DelayRange;

            set {
                if ( this.DelayRange != value )
                {
                    this._DelayRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The delay. </summary>
        private TimeSpan? _Delay;

        /// <summary> Gets or sets the cached output delay (pulse width). </summary>
        /// <remarks>
        /// The delay is used to delay operation in the Source layer. After the programmed Source event
        /// occurs, the instrument waits until the delay period expires before performing the Device
        /// Action.
        /// </remarks>
        /// <value> The output delay (pulse width) or none if not set or unknown. </value>
        public TimeSpan? Delay
        {
            get => this._Delay;

            protected set {
                if ( !Nullable.Equals( this.Delay, value ) )
                {
                    this._Delay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the output delay (pulse width). </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The output delay (pulse width) or none if unknown. </returns>
        public TimeSpan? ApplyDelay( TimeSpan value )
        {
            _ = this.WriteDelay( value );
            _ = this.QueryDelay();
            return default;
        }

        /// <summary> Gets or sets the delay query command. </summary>
        /// <remarks> SCPI: ":SOUR:DEL?". </remarks>
        /// <value> The delay query command. </value>
        protected virtual string DelayQueryCommand { get; set; }

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
        /// <value> The Delay query command. </value>
        protected virtual string DelayFormat { get; set; }

        /// <summary> Queries the Delay. </summary>
        /// <returns> The Delay or none if unknown. </returns>
        public TimeSpan? QueryDelay()
        {
            this.Delay = this.Query( this.Delay, this.DelayFormat, this.DelayQueryCommand );
            return this.Delay;
        }

        /// <summary> Gets or sets the delay command format. </summary>
        /// <remarks> SCPI: ":SOUR:DEL {0:s\.FFFFFFF}". </remarks>
        /// <value> The delay command format. </value>
        protected virtual string DelayCommandFormat { get; set; }

        /// <summary>
        /// Writes the output delay (pulse width) without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The output delay (pulse width) or none if unknown. </returns>
        public TimeSpan? WriteDelay( TimeSpan value )
        {
            this.Delay = this.Write( value, this.DelayCommandFormat );
            return this.Delay;
        }

        #endregion

        #region " LEVEL "

        /// <summary> The level. </summary>
        private int? _Level;

        /// <summary> Gets or sets the cached Digital Outputs Level. </summary>
        /// <value> The Digital Outputs Level or none if not set or unknown. </value>
        public int? Level
        {
            get => this._Level;

            protected set {
                if ( !Nullable.Equals( this.Level, value ) )
                {
                    this._Level = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Digital Outputs Level. </summary>
        /// <param name="value"> The current Digital Outputs Level. </param>
        /// <returns> The Digital Outputs Level or none if unknown. </returns>
        public int? ApplyLevel( int value )
        {
            _ = this.WriteLevel( value );
            return this.QueryLevel();
        }

        /// <summary> Gets or sets the Level query command. </summary>
        /// <remarks> SCPI: ":SOUR2:TTL:ACT?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string LevelQueryCommand { get; set; }

        /// <summary> Queries the current Lower Limit Level. </summary>
        /// <returns> The Level or none if unknown. </returns>
        public int? QueryLevel()
        {
            this.Level = this.Query( this.Level, this.LevelQueryCommand );
            return this.Level;
        }

        /// <summary> Gets or sets the Level query command. </summary>
        /// <remarks> SCPI: "::SOUR2:TTL:LEVEL {0}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string LevelCommandFormat { get; set; }

        /// <summary> Sets back the Level without reading back the value from the device. </summary>
        /// <param name="value"> The current Lower Limit Level. </param>
        /// <returns> The Level or none if unknown. </returns>
        public int? WriteLevel( int value )
        {
            this.Level = this.Write( value, this.LevelCommandFormat );
            return this.Level;
        }

        #endregion

        #region " LINE LEVEL "

        /// <summary> Writes and reads back the Digital Outputs LineLevel. </summary>
        /// <param name="lineNumber"> The line number. </param>
        /// <param name="value">      The current Digital Outputs LineLevel. </param>
        /// <returns> The Digital Outputs Line Level or none if unknown. </returns>
        public int? ApplyLineLevel( int lineNumber, int value )
        {
            _ = this.WriteLineLevel( lineNumber, value );
            return this.QueryLineLevel( lineNumber );
        }

        /// <summary> Gets or sets the Line Level query command. </summary>
        /// <remarks> SCPI: ":SOUR:TTL{0}:LEV?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string LineLevelQueryCommand { get; set; }

        /// <summary> Queries the current Line Level. </summary>
        /// <param name="lineNumber"> The line number. </param>
        /// <returns> The Line Level or none if unknown. </returns>
        public int? QueryLineLevel( int lineNumber )
        {
            var value = this.Query( lineNumber, string.Format( this.LineLevelQueryCommand, lineNumber ) );
            this.DigitalOutputLines.Update( lineNumber, value );
            this.NotifyPropertyChanged( nameof( this.DigitalOutputLines ) );
            return value;
        }

        /// <summary> Gets or sets the Line Level query command. </summary>
        /// <remarks> SCPI: "::SOUR2:TTL{0}:LEV {{0}}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected virtual string LineLevelCommandFormat { get; set; }

        /// <summary> Sets back the Line Level without reading back the value from the device. </summary>
        /// <param name="lineNumber"> The line number. </param>
        /// <param name="value">      The current Line Level. </param>
        /// <returns> The Line Level or none if unknown. </returns>
        public int? WriteLineLevel( int lineNumber, int value )
        {
            var result = this.Write( value, string.Format( this.LineLevelCommandFormat, lineNumber ) );
            this.DigitalOutputLines.Update( lineNumber, result );
            this.NotifyPropertyChanged( nameof( this.DigitalOutputLines ) );
            return result;
        }

        /// <summary>   Outputs a pulse. </summary>
        /// <remarks>   David, 2020-07-17. </remarks>
        /// <param name="lineNumber">   The strobe line. </param>
        /// <param name="lineLevel">    The line level. </param>
        /// <param name="duration">     Duration of the strobe. </param>
        public void Pulse( int lineNumber, int lineLevel, TimeSpan duration )
        {
            _ = this.WriteLineLevel( lineNumber, lineLevel );
            Core.ApplianceBase.Delay( duration );
            _ = this.WriteLineLevel( lineNumber, lineLevel == 0 ? 1 : 0 );
        }

        /// <summary> Outputs to bin line and strobes the strobe line. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        /// <param name="strobeLine">     The strobe line. </param>
        /// <param name="strobeDuration"> Duration of the strobe. </param>
        /// <param name="binLine">        The bin line. </param>
        /// <param name="binDuration">    Duration of the bin. </param>
        public void Strobe( int strobeLine, TimeSpan strobeDuration, int binLine, TimeSpan binDuration )
        {
            if ( binDuration <= strobeDuration )
                binDuration = strobeDuration.Add( TimeSpan.FromMilliseconds( 1d ) );
            _ = this.WriteLineLevel( binLine, 1 );
            Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 0.5d * (binDuration - strobeDuration).TotalMilliseconds ) );
            _ = this.WriteLineLevel( strobeLine, 1 );
            Core.ApplianceBase.DoEventsWait( strobeDuration );
            _ = this.WriteLineLevel( strobeLine, 0 );
            Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 0.5d * (binDuration - strobeDuration).TotalMilliseconds ) );
            _ = this.WriteLineLevel( binLine, 0 );
        }

        /// <summary> Gets or sets the tasker. </summary>
        /// <value> The tasker. </value>
        public Core.Tasker BinningStrobeTasker { get; private set; }

        /// <summary> Starts strobe task. </summary>
        /// <remarks> David, 2020-07-17. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="strobeLine">     The strobe line. </param>
        /// <param name="strobeDuration"> Duration of the strobe. </param>
        /// <param name="binLine">        The bin line. </param>
        /// <param name="binDuration">    Duration of the bin. </param>
        public void StartStrobeTask( int strobeLine, TimeSpan strobeDuration, int binLine, TimeSpan binDuration )
        {
            if ( this.BinningStrobeTasker?.IsBusy() == true )
                throw new InvalidOperationException( $"{nameof( Core.Tasker )} is busy" );
            this.BinningStrobeTasker.StartAction( () => this.Strobe( strobeLine, strobeDuration, binLine, binDuration ) );
        }

        #endregion

    }
}
