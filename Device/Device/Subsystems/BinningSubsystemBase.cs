using System;

namespace isr.VI
{

    /// <summary> Defines a Scpi Binning Subsystem. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-01-15, 1.0.1841.x. </para>
    /// </remarks>
    public abstract class BinningSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="BinningSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected BinningSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DefineFeedSourceReadWrites();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.BinningStrobeDuration = TimeSpan.Zero;
            this.FeedSource = FeedSources.Voltage;
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the Immediate command. </summary>
        /// <remarks> SCPI: ":CALC3:IMM". </remarks>
        /// <value> The Immediate command. </value>
        protected virtual string ImmediateCommand { get; set; }

        /// <summary> Immediately calculate limits. </summary>
        public void Immediate()
        {
            if ( !string.IsNullOrWhiteSpace( this.ImmediateCommand ) )
                this.Session.Execute( this.ImmediateCommand );
        }

        /// <summary> Gets or sets the limit 1 clear command. </summary>
        /// <value> The limit 1 clear command. </value>
        protected virtual string Limit1ClearCommand { get; set; }

        /// <summary> Clear limit 1. </summary>
        public void Limit1Clear()
        {
            if ( !string.IsNullOrWhiteSpace( this.Limit1ClearCommand ) )
                this.Session.Execute( this.Limit1ClearCommand );
        }

        /// <summary> Gets or sets the limit 2 clear command. </summary>
        /// <value> The limit 2 clear command. </value>
        protected virtual string Limit2ClearCommand { get; set; }

        /// <summary> Clear limit 2. </summary>
        public void Limit2Clear()
        {
            if ( !string.IsNullOrWhiteSpace( this.Limit2ClearCommand ) )
                this.Session.Execute( this.Limit2ClearCommand );
        }

        #endregion

        #region " DIGITAL I/O: FORCED DIGITAL OUTPUT PATTERN ENABLED "

        /// <summary> The forced digital output pattern enabled. </summary>
        private bool? _ForcedDigitalOutputPatternEnabled;

        /// <summary> Gets or sets the cached Forced Digital Output Pattern Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Forced Digital Output Pattern Enabled is not known; <c>True</c> if output is
        /// on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ForcedDigitalOutputPatternEnabled
        {
            get => this._ForcedDigitalOutputPatternEnabled;

            protected set {
                if ( !Equals( this.ForcedDigitalOutputPatternEnabled, value ) )
                {
                    this._ForcedDigitalOutputPatternEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Forced Digital Output Pattern Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyForcedDigitalOutputPatternEnabled( bool value )
        {
            _ = this.WriteForcedDigitalOutputPatternEnabled( value );
            return this.QueryForcedDigitalOutputPatternEnabled();
        }

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <remarks> SCPI: ":CALC3:FORC:STAT?". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string ForcedDigitalOutputPatternEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Forced Digital Output Pattern Enabled sentinel. Also sets the
        /// <see cref="ForcedDigitalOutputPatternEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryForcedDigitalOutputPatternEnabled()
        {
            this.ForcedDigitalOutputPatternEnabled = this.Query( this.ForcedDigitalOutputPatternEnabled, this.ForcedDigitalOutputPatternEnabledQueryCommand );
            return this.ForcedDigitalOutputPatternEnabled;
        }

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <remarks> SCPI: ":CALC3:FORC:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string ForcedDigitalOutputPatternEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Forced Digital Output Pattern Enabled sentinel. Does not read back from the
        /// instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteForcedDigitalOutputPatternEnabled( bool value )
        {
            this.ForcedDigitalOutputPatternEnabled = this.Write( value, this.ForcedDigitalOutputPatternEnabledCommandFormat );
            return this.ForcedDigitalOutputPatternEnabled;
        }

        #endregion

        #region " DIGITAL I/O: FORCED DIGITAL OUTPUT PATTERN "

        /// <summary> A pattern specifying the forced digital output. </summary>
        private int? _ForcedDigitalOutputPattern;

        /// <summary> Gets or sets the cached Forced Digital Output Pattern. </summary>
        /// <value> The Forced Digital Output Pattern or none if not set or unknown. </value>
        public int? ForcedDigitalOutputPattern
        {
            get => this._ForcedDigitalOutputPattern;

            protected set {
                if ( !Nullable.Equals( this.ForcedDigitalOutputPattern, value ) )
                {
                    this._ForcedDigitalOutputPattern = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Forced Digital Output Pattern. </summary>
        /// <param name="value"> The current Forced Digital Output Pattern. </param>
        /// <returns> The Forced Digital Output Pattern or none if unknown. </returns>
        public int? ApplyForcedDigitalOutputPattern( int value )
        {
            _ = this.WriteForcedDigitalOutputPattern( value );
            return this.QueryForcedDigitalOutputPattern();
        }

        /// <summary> Gets or sets the forced digital output Pattern query command. </summary>
        /// <remarks> SCPI ":CALC3:FORC:PATT". </remarks>
        /// <value> The forced digital output Pattern query command. </value>
        protected virtual string ForcedDigitalOutputPatternQueryCommand { get; set; }

        /// <summary> Queries the current Forced Digital Output Pattern. </summary>
        /// <returns> The Forced Digital Output Pattern or none if unknown. </returns>
        public int? QueryForcedDigitalOutputPattern()
        {
            this.ForcedDigitalOutputPattern = this.Query( this.ForcedDigitalOutputPattern, this.ForcedDigitalOutputPatternQueryCommand );
            return this.ForcedDigitalOutputPattern;
        }

        /// <summary> Gets or sets the forced digital output Pattern command format. </summary>
        /// <remarks> SCPI ":CALC3:FORC:PATT". </remarks>
        /// <value> The forced digital output Pattern command format. </value>
        protected virtual string ForcedDigitalOutputPatternCommandFormat { get; set; }

        /// <summary>
        /// Sets back the Forced Digital Output Pattern without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current Forced Digital Output Pattern. </param>
        /// <returns> The Forced Digital Output Pattern or none if unknown. </returns>
        public int? WriteForcedDigitalOutputPattern( int value )
        {
            this.ForcedDigitalOutputPattern = this.Write( value, this.ForcedDigitalOutputPatternCommandFormat );
            return this.ForcedDigitalOutputPattern;
        }

        #endregion

        #region " BINNING STROBE "

        /// <summary> Duration of the binning strobe. </summary>
        private TimeSpan _BinningStrobeDuration;

        /// <summary> Gets or sets the duration of the binning strobe. </summary>
        /// <value> The binning strobe duration. </value>
        public TimeSpan BinningStrobeDuration
        {
            get => this._BinningStrobeDuration;

            set {
                if ( value != this.BinningStrobeDuration )
                {
                    this._BinningStrobeDuration = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Binning Strobe Enabled status. </summary>
        private bool? _BinningStrobeEnabled;

        /// <summary> Gets or sets the cached status of outputting a Binning strobe. </summary>
        /// <value>
        /// <c>null</c> if a Binning Strobe Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? BinningStrobeEnabled
        {
            get => this._BinningStrobeEnabled;

            protected set {
                if ( !Equals( this.BinningStrobeEnabled, value ) )
                {
                    this._BinningStrobeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Binning Strobe Enabled status. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyBinningStrobeEnabled( bool value )
        {
            _ = this.WriteBinningStrobeEnabled( value );
            return this.QueryBinningStrobeEnabled();
        }

        /// <summary> Gets or sets the Binning Strobe Enabled status query command. </summary>
        /// <remarks> SCPI: ":CALC3:BSTR:STAT?". </remarks>
        /// <value> The Binning enabled query command. </value>
        protected virtual string BinningStrobeEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Binning Strobe Enabled status. Also sets the
        /// <see cref="BinningStrobeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryBinningStrobeEnabled()
        {
            this.BinningStrobeEnabled = this.Query( this.BinningStrobeEnabled, this.BinningStrobeEnabledQueryCommand );
            return this.BinningStrobeEnabled;
        }

        /// <summary> Gets or sets the Binning Strobe Enabled status command Format. </summary>
        /// <remarks> SCPI: ":CALC3:BSTR:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Binning enabled query command. </value>
        protected virtual string BinningStrobeEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Binning Strobe Enabled status. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteBinningStrobeEnabled( bool value )
        {
            this.BinningStrobeEnabled = this.Write( value, this.BinningStrobeEnabledCommandFormat );
            return this.BinningStrobeEnabled;
        }

        #endregion

        #region " FEED SOURCE "

        /// <summary> Define feed source read writes. </summary>
        private void DefineFeedSourceReadWrites()
        {
            this.FeedSourceReadWrites = new Pith.EnumReadWriteCollection();
            foreach ( FeedSources enumValue in Enum.GetValues( typeof( FeedSources ) ) )
                this.FeedSourceReadWrites.Add( enumValue );
        }

        /// <summary> Gets or sets a dictionary of Feed source parses. </summary>
        /// <value> A Dictionary of Feed source parses. </value>
        public Pith.EnumReadWriteCollection FeedSourceReadWrites { get; private set; }

        /// <summary> The supported feed sources. </summary>
        private FeedSources _SupportedFeedSources;

        /// <summary> Gets or sets the supported Feed sources. </summary>
        /// <value> The supported Feed sources. </value>
        public FeedSources SupportedFeedSources
        {
            get => this._SupportedFeedSources;

            set {
                if ( !this.SupportedFeedSources.Equals( value ) )
                {
                    this._SupportedFeedSources = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The feed source. </summary>
        private FeedSources? _FeedSource;

        /// <summary> Gets or sets the cached source FeedSource. </summary>
        /// <value>
        /// The <see cref="FeedSource">source Feed Source</see> or none if not set or unknown.
        /// </value>
        public FeedSources? FeedSource
        {
            get => this._FeedSource;

            protected set {
                if ( !this.FeedSource.Equals( value ) )
                {
                    this._FeedSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Feed Source. </summary>
        /// <param name="value"> The  Source Feed Source. </param>
        /// <returns> The <see cref="FeedSource">source Feed Source</see> or none if unknown. </returns>
        public FeedSources? ApplyFeedSource( FeedSources value )
        {
            _ = this.WriteFeedSource( value );
            return this.QueryFeedSource();
        }

        /// <summary> Gets or sets the Feed source query command. </summary>
        /// <remarks> SCPI: "?". </remarks>
        /// <value> The Feed source query command. </value>
        protected virtual string FeedSourceQueryCommand { get; set; }

        /// <summary> Queries the Feed source. </summary>
        /// <returns> The <see cref="FeedSource">Feed source</see> or none if unknown. </returns>
        public FeedSources? QueryFeedSource()
        {
            this.FeedSource = this.Query( this.FeedSourceQueryCommand, this.FeedSource.GetValueOrDefault( FeedSources.None ), this.FeedSourceReadWrites );
            return this.FeedSource;
        }

        /// <summary> Gets or sets the Feed source command format. </summary>
        /// <remarks> SCPI: " {0}". </remarks>
        /// <value> The write Feed source command format. </value>
        protected virtual string FeedSourceCommandFormat { get; set; }

        /// <summary> Writes the Feed Source without reading back the value from the device. </summary>
        /// <param name="value"> The Feed Source. </param>
        /// <returns> The <see cref="FeedSource">Feed Source</see> or none if unknown. </returns>
        public FeedSources? WriteFeedSource( FeedSources value )
        {
            this.FeedSource = this.Write( this.FeedSourceCommandFormat, value, this.FeedSourceReadWrites );
            return this.FeedSource;
        }

        #endregion

        #region " LIMITS FAILED "

        /// <summary> Limits Failed. </summary>
        private bool? _LimitsFailed;

        /// <summary> Gets or sets the cached Limits Failed sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limits Failed is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? LimitsFailed
        {
            get => this._LimitsFailed;

            protected set {
                if ( !Equals( this.LimitsFailed, value ) )
                {
                    this._LimitsFailed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Limits Failed query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM1:FAIL?". </remarks>
        /// <value> The Limits Failed query command. </value>
        protected virtual string LimitsFailedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limits Failed sentinel. Also sets the
        /// <see cref="LimitsFailed">Failed</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
        public bool? QueryLimitsFailed()
        {
            this.LimitsFailed = this.Query( this.LimitsFailed, this.LimitsFailedQueryCommand );
            return this.LimitsFailed;
        }

        #endregion

        #region " PASS SOURCE "

        /// <summary> The Pass Source. </summary>
        private int? _PassSource;

        /// <summary> Gets or sets the cached Pass Source. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? PassSource
        {
            get => this._PassSource;

            protected set {
                if ( !Nullable.Equals( this.PassSource, value ) )
                {
                    this._PassSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Pass Source. </summary>
        /// <param name="value"> The Pass Source. </param>
        /// <returns> The Pass Source. </returns>
        public int? ApplyPassSource( int value )
        {
            _ = this.WritePassSource( value );
            return this.QueryPassSource();
        }

        /// <summary> Gets or sets The Pass Source query command. </summary>
        /// <value> The Pass Source query command. </value>
        protected virtual string PassSourceQueryCommand { get; set; }

        /// <summary> Queries The Pass Source. </summary>
        /// <returns> The Pass Source or none if unknown. </returns>
        public int? QueryPassSource()
        {
            this.PassSource = this.Query( this.PassSource, this.PassSourceQueryCommand );
            return this.PassSource;
        }

        /// <summary> Gets or sets The Pass Source command format. </summary>
        /// <value> The Pass Source command format. </value>
        protected virtual string PassSourceCommandFormat { get; set; }

        /// <summary> Writes The Pass Source without reading back the value from the device. </summary>
        /// <remarks> This command sets The Pass Source. </remarks>
        /// <param name="value"> The Pass Source. </param>
        /// <returns> The Pass Source. </returns>
        public int? WritePassSource( int value )
        {
            this.PassSource = this.Write( value, this.PassSourceCommandFormat );
            return this.PassSource;
        }

        #endregion

        #region " LIMIT 1 "

        #region " LIMIT1 ENABLED "

        /// <summary> Limit1 enabled. </summary>
        private bool? _Limit1Enabled;

        /// <summary> Gets or sets the cached Limit1 Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit1 Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit1Enabled
        {
            get => this._Limit1Enabled;

            protected set {
                if ( !Equals( this.Limit1Enabled, value ) )
                {
                    this._Limit1Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit1Enabled( bool value )
        {
            _ = this.WriteLimit1Enabled( value );
            return this.QueryLimit1Enabled();
        }

        /// <summary> Gets or sets the Limit1 enabled query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM1:STAT?". </remarks>
        /// <value> The Limit1 enabled query command. </value>
        protected virtual string Limit1EnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit1 Enabled sentinel. Also sets the
        /// <see cref="Limit1Enabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryLimit1Enabled()
        {
            this.Limit1Enabled = this.Query( this.Limit1Enabled, this.Limit1EnabledQueryCommand );
            return this.Limit1Enabled;
        }

        /// <summary> Gets or sets the Limit1 enabled command Format. </summary>
        /// <remarks> SCPI: ":CALC3:LIM1:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Limit1 enabled query command. </value>
        protected virtual string Limit1EnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit1 Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteLimit1Enabled( bool value )
        {
            this.Limit1Enabled = this.Write( value, this.Limit1EnabledCommandFormat );
            return this.Limit1Enabled;
        }

        #endregion

        #region " LIMIT1 FAILED "

        /// <summary> Limit1 Failed. </summary>
        private bool? _Limit1Failed;

        /// <summary> Gets or sets the cached Limit1 Failed sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit1 Failed is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit1Failed
        {
            get => this._Limit1Failed;

            protected set {
                if ( !Equals( this.Limit1Failed, value ) )
                {
                    this._Limit1Failed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Limit1 Failed query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM1:FAIL?". </remarks>
        /// <value> The Limit1 Failed query command. </value>
        protected virtual string Limit1FailedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit1 Failed sentinel. Also sets the
        /// <see cref="Limit1Failed">Failed</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
        public bool? QueryLimit1Failed()
        {
            this.Limit1Failed = this.Query( this.Limit1Failed, this.Limit1FailedQueryCommand );
            return this.Limit1Failed;
        }

        #endregion

        #region " LIMIT1 LOWER LEVEL "

        /// <summary> The Limit1 Lower Level. </summary>
        private double? _Limit1LowerLevel;

        /// <summary>
        /// Gets or sets the cached Limit1 Lower Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit1LowerLevel
        {
            get => this._Limit1LowerLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit1LowerLevel, value ) )
                {
                    this._Limit1LowerLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Lower Level. </summary>
        /// <param name="value"> The Limit1 Lower Level. </param>
        /// <returns> The Limit1 Lower Level. </returns>
        public double? ApplyLimit1LowerLevel( double value )
        {
            _ = this.WriteLimit1LowerLevel( value );
            return this.QueryLimit1LowerLevel();
        }

        /// <summary> Gets or sets The Limit1 Lower Level query command. </summary>
        /// <value> The Limit1 Lower Level query command. </value>
        protected virtual string Limit1LowerLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit1 Lower Level. </summary>
        /// <returns> The Limit1 Lower Level or none if unknown. </returns>
        public double? QueryLimit1LowerLevel()
        {
            this.Limit1LowerLevel = this.Query( this.Limit1LowerLevel, this.Limit1LowerLevelQueryCommand );
            return this.Limit1LowerLevel;
        }

        /// <summary> Gets or sets The Limit1 Lower Level command format. </summary>
        /// <value> The Limit1 Lower Level command format. </value>
        protected virtual string Limit1LowerLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit1 Lower Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit1 Lower Level. </remarks>
        /// <param name="value"> The Limit1 Lower Level. </param>
        /// <returns> The Limit1 Lower Level. </returns>
        public double? WriteLimit1LowerLevel( double value )
        {
            this.Limit1LowerLevel = this.Write( value, this.Limit1LowerLevelCommandFormat );
            return this.Limit1LowerLevel;
        }

        #endregion

        #region " LIMIT1 UPPER LEVEL "

        /// <summary> The Limit1 Upper Level. </summary>
        private double? _Limit1UpperLevel;

        /// <summary>
        /// Gets or sets the cached Limit1 Upper Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit1UpperLevel
        {
            get => this._Limit1UpperLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit1UpperLevel, value ) )
                {
                    this._Limit1UpperLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Upper Level. </summary>
        /// <param name="value"> The Limit1 Upper Level. </param>
        /// <returns> The Limit1 Upper Level. </returns>
        public double? ApplyLimit1UpperLevel( double value )
        {
            _ = this.WriteLimit1UpperLevel( value );
            return this.QueryLimit1UpperLevel();
        }

        /// <summary> Gets or sets The Limit1 Upper Level query command. </summary>
        /// <value> The Limit1 Upper Level query command. </value>
        protected virtual string Limit1UpperLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit1 Upper Level. </summary>
        /// <returns> The Limit1 Upper Level or none if unknown. </returns>
        public double? QueryLimit1UpperLevel()
        {
            this.Limit1UpperLevel = this.Query( this.Limit1UpperLevel, this.Limit1UpperLevelQueryCommand );
            return this.Limit1UpperLevel;
        }

        /// <summary> Gets or sets The Limit1 Upper Level command format. </summary>
        /// <value> The Limit1 Upper Level command format. </value>
        protected virtual string Limit1UpperLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit1 Upper Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit1 Upper Level. </remarks>
        /// <param name="value"> The Limit1 Upper Level. </param>
        /// <returns> The Limit1 Upper Level. </returns>
        public double? WriteLimit1UpperLevel( double value )
        {
            this.Limit1UpperLevel = this.Write( value, this.Limit1UpperLevelCommandFormat );
            return this.Limit1UpperLevel;
        }

        #endregion

        #region " LIMIT1 AUTO CLEAR "

        /// <summary> Limit1 Auto Clear. </summary>
        private bool? _Limit1AutoClear;

        /// <summary> Gets or sets the cached Limit1 Auto Clear sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit1 Auto Clear is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit1AutoClear
        {
            get => this._Limit1AutoClear;

            protected set {
                if ( !Equals( this.Limit1AutoClear, value ) )
                {
                    this._Limit1AutoClear = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Auto Clear sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit1AutoClear( bool value )
        {
            _ = this.WriteLimit1AutoClear( value );
            return this.QueryLimit1AutoClear();
        }

        /// <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM1:STAT?". </remarks>
        /// <value> The Limit1 Auto Clear query command. </value>
        protected virtual string Limit1AutoClearQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit1 Auto Clear sentinel. Also sets the
        /// <see cref="Limit1AutoClear">AutoClear</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? QueryLimit1AutoClear()
        {
            this.Limit1AutoClear = this.Query( this.Limit1AutoClear, this.Limit1AutoClearQueryCommand );
            return this.Limit1AutoClear;
        }

        /// <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
        /// <remarks> SCPI: ":CALC3:LIM1:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Limit1 Auto Clear query command. </value>
        protected virtual string Limit1AutoClearCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit1 Auto Clear sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is Auto Clear. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? WriteLimit1AutoClear( bool value )
        {
            this.Limit1AutoClear = this.Write( value, this.Limit1AutoClearCommandFormat );
            return this.Limit1AutoClear;
        }

        #endregion

        #region " LIMIT1 LOWER SOURCE "

        /// <summary> The Limit1 Lower Source. </summary>
        private int? _Limit1LowerSource;

        /// <summary> Gets or sets the cached Limit1 Lower Source. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? Limit1LowerSource
        {
            get => this._Limit1LowerSource;

            protected set {
                if ( !Nullable.Equals( this.Limit1LowerSource, value ) )
                {
                    this._Limit1LowerSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Lower Source. </summary>
        /// <param name="value"> The Limit1 Lower Source. </param>
        /// <returns> The Limit1 Lower Source. </returns>
        public int? ApplyLimit1LowerSource( int value )
        {
            _ = this.WriteLimit1LowerSource( value );
            return this.QueryLimit1LowerSource();
        }

        /// <summary> Gets or sets The Limit1 Lower Source query command. </summary>
        /// <value> The Limit1 Lower Source query command. </value>
        protected virtual string Limit1LowerSourceQueryCommand { get; set; }

        /// <summary> Queries The Limit1 Lower Source. </summary>
        /// <returns> The Limit1 Lower Source or none if unknown. </returns>
        public int? QueryLimit1LowerSource()
        {
            this.Limit1LowerSource = this.Query( this.Limit1LowerSource, this.Limit1LowerSourceQueryCommand );
            return this.Limit1LowerSource;
        }

        /// <summary> Gets or sets The Limit1 Lower Source command format. </summary>
        /// <value> The Limit1 Lower Source command format. </value>
        protected virtual string Limit1LowerSourceCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit1 Lower Source without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit1 Lower Source. </remarks>
        /// <param name="value"> The Limit1 Lower Source. </param>
        /// <returns> The Limit1 Lower Source. </returns>
        public int? WriteLimit1LowerSource( int value )
        {
            this.Limit1LowerSource = this.Write( value, this.Limit1LowerSourceCommandFormat );
            return this.Limit1LowerSource;
        }

        #endregion

        #region " LIMIT1 UPPER SOURCE "

        /// <summary> The Limit1 Upper Source. </summary>
        private int? _Limit1UpperSource;

        /// <summary> Gets or sets the cached Limit1 Upper Source. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? Limit1UpperSource
        {
            get => this._Limit1UpperSource;

            protected set {
                if ( !Nullable.Equals( this.Limit1UpperSource, value ) )
                {
                    this._Limit1UpperSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Upper Source. </summary>
        /// <param name="value"> The Limit1 Upper Source. </param>
        /// <returns> The Limit1 Upper Source. </returns>
        public int? ApplyLimit1UpperSource( int value )
        {
            _ = this.WriteLimit1UpperSource( value );
            return this.QueryLimit1UpperSource();
        }

        /// <summary> Gets or sets The Limit1 Upper Source query command. </summary>
        /// <value> The Limit1 Upper Source query command. </value>
        protected virtual string Limit1UpperSourceQueryCommand { get; set; }

        /// <summary> Queries The Limit1 Upper Source. </summary>
        /// <returns> The Limit1 Upper Source or none if unknown. </returns>
        public int? QueryLimit1UpperSource()
        {
            this.Limit1UpperSource = this.Query( this.Limit1UpperSource, this.Limit1UpperSourceQueryCommand );
            return this.Limit1UpperSource;
        }

        /// <summary> Gets or sets The Limit1 Upper Source command format. </summary>
        /// <value> The Limit1 Upper Source command format. </value>
        protected virtual string Limit1UpperSourceCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit1 Upper Source without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit1 Upper Source. </remarks>
        /// <param name="value"> The Limit1 Upper Source. </param>
        /// <returns> The Limit1 Upper Source. </returns>
        public int? WriteLimit1UpperSource( int value )
        {
            this.Limit1UpperSource = this.Write( value, this.Limit1UpperSourceCommandFormat );
            return this.Limit1UpperSource;
        }

        #endregion

        #endregion

        #region " LIMIT 2 "

        #region " LIMIT2 ENABLED "

        /// <summary> Limit2 enabled. </summary>
        private bool? _Limit2Enabled;

        /// <summary> Gets or sets the cached Limit2 Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit2 Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit2Enabled
        {
            get => this._Limit2Enabled;

            protected set {
                if ( !Equals( this.Limit2Enabled, value ) )
                {
                    this._Limit2Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit2Enabled( bool value )
        {
            _ = this.WriteLimit2Enabled( value );
            return this.QueryLimit2Enabled();
        }

        /// <summary> Gets or sets the Limit2 enabled query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM2:STAT?". </remarks>
        /// <value> The Limit2 enabled query command. </value>
        protected virtual string Limit2EnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit2 Enabled sentinel. Also sets the
        /// <see cref="Limit2Enabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryLimit2Enabled()
        {
            this.Limit2Enabled = this.Query( this.Limit2Enabled, this.Limit2EnabledQueryCommand );
            return this.Limit2Enabled;
        }

        /// <summary> Gets or sets the Limit2 enabled command Format. </summary>
        /// <remarks> SCPI: ":CALC3:LIM2:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Limit2 enabled query command. </value>
        protected virtual string Limit2EnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit2 Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteLimit2Enabled( bool value )
        {
            this.Limit2Enabled = this.Write( value, this.Limit2EnabledCommandFormat );
            return this.Limit2Enabled;
        }

        #endregion

        #region " LIMIT2 FAILED "

        /// <summary> Limit2 Failed. </summary>
        private bool? _Limit2Failed;

        /// <summary> Gets or sets the cached Limit2 Failed sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit2 Failed is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit2Failed
        {
            get => this._Limit2Failed;

            protected set {
                if ( !Equals( this.Limit2Failed, value ) )
                {
                    this._Limit2Failed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Limit2 Failed query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM2:FAIL?". </remarks>
        /// <value> The Limit2 Failed query command. </value>
        protected virtual string Limit2FailedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit2 Failed sentinel. Also sets the
        /// <see cref="Limit2Failed">Failed</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if Failed; otherwise <c>False</c>. </returns>
        public bool? QueryLimit2Failed()
        {
            this.Limit2Failed = this.Query( this.Limit2Failed, this.Limit2FailedQueryCommand );
            return this.Limit2Failed;
        }

        #endregion

        #region " LIMIT2 LOWER LEVEL "

        /// <summary> The Limit2 Lower Level. </summary>
        private double? _Limit2LowerLevel;

        /// <summary>
        /// Gets or sets the cached Limit2 Lower Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit2LowerLevel
        {
            get => this._Limit2LowerLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit2LowerLevel, value ) )
                {
                    this._Limit2LowerLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Lower Level. </summary>
        /// <param name="value"> The Limit2 Lower Level. </param>
        /// <returns> The Limit2 Lower Level. </returns>
        public double? ApplyLimit2LowerLevel( double value )
        {
            _ = this.WriteLimit2LowerLevel( value );
            return this.QueryLimit2LowerLevel();
        }

        /// <summary> Gets or sets The Limit2 Lower Level query command. </summary>
        /// <value> The Limit2 Lower Level query command. </value>
        protected virtual string Limit2LowerLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit2 Lower Level. </summary>
        /// <returns> The Limit2 Lower Level or none if unknown. </returns>
        public double? QueryLimit2LowerLevel()
        {
            this.Limit2LowerLevel = this.Query( this.Limit2LowerLevel, this.Limit2LowerLevelQueryCommand );
            return this.Limit2LowerLevel;
        }

        /// <summary> Gets or sets The Limit2 Lower Level command format. </summary>
        /// <value> The Limit2 Lower Level command format. </value>
        protected virtual string Limit2LowerLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit2 Lower Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit2 Lower Level. </remarks>
        /// <param name="value"> The Limit2 Lower Level. </param>
        /// <returns> The Limit2 Lower Level. </returns>
        public double? WriteLimit2LowerLevel( double value )
        {
            this.Limit2LowerLevel = this.Write( value, this.Limit2LowerLevelCommandFormat );
            return this.Limit2LowerLevel;
        }

        #endregion

        #region " LIMIT2 UPPER LEVEL "

        /// <summary> The Limit2 Upper Level. </summary>
        private double? _Limit2UpperLevel;

        /// <summary>
        /// Gets or sets the cached Limit2 Upper Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit2UpperLevel
        {
            get => this._Limit2UpperLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit2UpperLevel, value ) )
                {
                    this._Limit2UpperLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Upper Level. </summary>
        /// <param name="value"> The Limit2 Upper Level. </param>
        /// <returns> The Limit2 Upper Level. </returns>
        public double? ApplyLimit2UpperLevel( double value )
        {
            _ = this.WriteLimit2UpperLevel( value );
            return this.QueryLimit2UpperLevel();
        }

        /// <summary> Gets or sets The Limit2 Upper Level query command. </summary>
        /// <value> The Limit2 Upper Level query command. </value>
        protected virtual string Limit2UpperLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit2 Upper Level. </summary>
        /// <returns> The Limit2 Upper Level or none if unknown. </returns>
        public double? QueryLimit2UpperLevel()
        {
            this.Limit2UpperLevel = this.Query( this.Limit2UpperLevel, this.Limit2UpperLevelQueryCommand );
            return this.Limit2UpperLevel;
        }

        /// <summary> Gets or sets The Limit2 Upper Level command format. </summary>
        /// <value> The Limit2 Upper Level command format. </value>
        protected virtual string Limit2UpperLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit2 Upper Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit2 Upper Level. </remarks>
        /// <param name="value"> The Limit2 Upper Level. </param>
        /// <returns> The Limit2 Upper Level. </returns>
        public double? WriteLimit2UpperLevel( double value )
        {
            this.Limit2UpperLevel = this.Write( value, this.Limit2UpperLevelCommandFormat );
            return this.Limit2UpperLevel;
        }

        #endregion

        #region " LIMIT2 AUTO CLEAR "

        /// <summary> Limit2 Auto Clear. </summary>
        private bool? _Limit2AutoClear;

        /// <summary> Gets or sets the cached Limit2 Auto Clear sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit2 Auto Clear is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit2AutoClear
        {
            get => this._Limit2AutoClear;

            protected set {
                if ( !Equals( this.Limit2AutoClear, value ) )
                {
                    this._Limit2AutoClear = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Auto Clear sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit2AutoClear( bool value )
        {
            _ = this.WriteLimit2AutoClear( value );
            return this.QueryLimit2AutoClear();
        }

        /// <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
        /// <remarks> SCPI: ":CALC3:LIM2:STAT?". </remarks>
        /// <value> The Limit2 Auto Clear query command. </value>
        protected virtual string Limit2AutoClearQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit2 Auto Clear sentinel. Also sets the
        /// <see cref="Limit2AutoClear">AutoClear</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? QueryLimit2AutoClear()
        {
            this.Limit2AutoClear = this.Query( this.Limit2AutoClear, this.Limit2AutoClearQueryCommand );
            return this.Limit2AutoClear;
        }

        /// <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
        /// <remarks> SCPI: ":CALC3:LIM2:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Limit2 Auto Clear query command. </value>
        protected virtual string Limit2AutoClearCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit2 Auto Clear sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is Auto Clear. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? WriteLimit2AutoClear( bool value )
        {
            this.Limit2AutoClear = this.Write( value, this.Limit2AutoClearCommandFormat );
            return this.Limit2AutoClear;
        }

        #endregion

        #region " LIMIT2 LOWER SOURCE "

        /// <summary> The Limit2 Lower Source. </summary>
        private int? _Limit2LowerSource;

        /// <summary> Gets or sets the cached Limit2 Lower Source. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? Limit2LowerSource
        {
            get => this._Limit2LowerSource;

            protected set {
                if ( !Nullable.Equals( this.Limit2LowerSource, value ) )
                {
                    this._Limit2LowerSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Lower Source. </summary>
        /// <param name="value"> The Limit2 Lower Source. </param>
        /// <returns> The Limit2 Lower Source. </returns>
        public int? ApplyLimit2LowerSource( int value )
        {
            _ = this.WriteLimit2LowerSource( value );
            return this.QueryLimit2LowerSource();
        }

        /// <summary> Gets or sets The Limit2 Lower Source query command. </summary>
        /// <value> The Limit2 Lower Source query command. </value>
        protected virtual string Limit2LowerSourceQueryCommand { get; set; }

        /// <summary> Queries The Limit2 Lower Source. </summary>
        /// <returns> The Limit2 Lower Source or none if unknown. </returns>
        public int? QueryLimit2LowerSource()
        {
            this.Limit2LowerSource = this.Query( this.Limit2LowerSource, this.Limit2LowerSourceQueryCommand );
            return this.Limit2LowerSource;
        }

        /// <summary> Gets or sets The Limit2 Lower Source command format. </summary>
        /// <value> The Limit2 Lower Source command format. </value>
        protected virtual string Limit2LowerSourceCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit2 Lower Source without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit2 Lower Source. </remarks>
        /// <param name="value"> The Limit2 Lower Source. </param>
        /// <returns> The Limit2 Lower Source. </returns>
        public int? WriteLimit2LowerSource( int value )
        {
            this.Limit2LowerSource = this.Write( value, this.Limit2LowerSourceCommandFormat );
            return this.Limit2LowerSource;
        }

        #endregion

        #region " LIMIT2 UPPER SOURCE "

        /// <summary> The Limit2 Upper Source. </summary>
        private int? _Limit2UpperSource;

        /// <summary> Gets or sets the cached Limit2 Upper Source. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? Limit2UpperSource
        {
            get => this._Limit2UpperSource;

            protected set {
                if ( !Nullable.Equals( this.Limit2UpperSource, value ) )
                {
                    this._Limit2UpperSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Upper Source. </summary>
        /// <param name="value"> The Limit2 Upper Source. </param>
        /// <returns> The Limit2 Upper Source. </returns>
        public int? ApplyLimit2UpperSource( int value )
        {
            _ = this.WriteLimit2UpperSource( value );
            return this.QueryLimit2UpperSource();
        }

        /// <summary> Gets or sets The Limit2 Upper Source query command. </summary>
        /// <value> The Limit2 Upper Source query command. </value>
        protected virtual string Limit2UpperSourceQueryCommand { get; set; }

        /// <summary> Queries The Limit2 Upper Source. </summary>
        /// <returns> The Limit2 Upper Source or none if unknown. </returns>
        public int? QueryLimit2UpperSource()
        {
            this.Limit2UpperSource = this.Query( this.Limit2UpperSource, this.Limit2UpperSourceQueryCommand );
            return this.Limit2UpperSource;
        }

        /// <summary> Gets or sets The Limit2 Upper Source command format. </summary>
        /// <value> The Limit2 Upper Source command format. </value>
        protected virtual string Limit2UpperSourceCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit2 Upper Source without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit2 Upper Source. </remarks>
        /// <param name="value"> The Limit2 Upper Source. </param>
        /// <returns> The Limit2 Upper Source. </returns>
        public int? WriteLimit2UpperSource( int value )
        {
            this.Limit2UpperSource = this.Write( value, this.Limit2UpperSourceCommandFormat );
            return this.Limit2UpperSource;
        }

        #endregion

        #endregion


    }
}
