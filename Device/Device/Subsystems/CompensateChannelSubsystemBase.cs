using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.VI
{

    /// <summary> Defines the Compensation Channel subsystem. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public abstract partial class CompensateChannelSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="CompensateChannelSubsystemBase" /> class.
        /// </summary>
        /// <param name="channelNumber">   The channel number. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected CompensateChannelSubsystemBase( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.ChannelNumber = channelNumber;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CompensateChannelSubsystemBase" /> class.
        /// </summary>
        /// <param name="compensationType"> Type of the compensation. </param>
        /// <param name="channelNumber">    A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="statusSubsystem">  The status subsystem. </param>
        protected CompensateChannelSubsystemBase( CompensationTypes compensationType, int channelNumber, StatusSubsystemBase statusSubsystem ) : this( channelNumber, statusSubsystem )
        {
            this.ApplyCompensationType( compensationType );
            this.DefineCompensationTypeReadWrites();
        }

        #endregion

        #region " CHANNEL "

        /// <summary> Gets or sets the channel number. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidCastException">      Thrown when an object cannot be cast to a
        /// required type. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The channel number. </value>
        public int ChannelNumber { get; private set; }

        #endregion

        #region " ARRAY <> STRING "

        /// <summary> Parses an impedance array string. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <exception cref="InvalidCastException">  Thrown when an object cannot be cast to a required
        /// type. </exception>
        /// <param name="values"> The values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process parse in this collection.
        /// </returns>
        public static IList<double> Parse( string values )
        {
            if ( string.IsNullOrEmpty( values ) )
                throw new ArgumentNullException( nameof( values ) );
            var v = new Queue<string>( values.Split( ',' ) );
            var data = new List<double>();
            while ( v.Any() )
            {
                string pop = v.Dequeue();
                if ( !double.TryParse( pop, out double value ) )
                {
                    throw new InvalidCastException( $"Parse failed for value {pop}" );
                }

                data.Add( value );
            }

            return data;
        }

        /// <summary> Builds an impedance array string. </summary>
        /// <param name="values"> The values. </param>
        /// <returns> A String. </returns>
        public static string Build( IList<double> values )
        {
            var builder = new System.Text.StringBuilder();
            if ( values?.Any() == true )
            {
                foreach ( double v in values )
                {
                    if ( builder.Length > 0 )
                        _ = builder.Append( "," );
                    _ = builder.Append( v.ToString() );
                }
            }

            return builder.ToString();
        }

        /// <summary> Builds impedance array string. </summary>
        /// <param name="includesFrequency">      true if impedance array includes. </param>
        /// <param name="lowFrequencyImpedance">  The low frequency impedance values. </param>
        /// <param name="highFrequencyImpedance"> The high frequency values. </param>
        /// <returns> A String. </returns>
        public static string Build( bool includesFrequency, IList<double> lowFrequencyImpedance, IList<double> highFrequencyImpedance )
        {
            return Build( Merge( includesFrequency, lowFrequencyImpedance, highFrequencyImpedance ) );
        }

        /// <summary> Merges the impedance arrays into a single array. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="includesFrequency">      true if impedance array includes. </param>
        /// <param name="lowFrequencyImpedance">  The low frequency impedance values. </param>
        /// <param name="highFrequencyImpedance"> The high frequency values. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process merge in this collection.
        /// </returns>
        public static IList<double> Merge( bool includesFrequency, IList<double> lowFrequencyImpedance, IList<double> highFrequencyImpedance )
        {
            if ( lowFrequencyImpedance is null )
                throw new ArgumentNullException( nameof( lowFrequencyImpedance ) );
            if ( highFrequencyImpedance is null )
                throw new ArgumentNullException( nameof( highFrequencyImpedance ) );
            int startIndex = includesFrequency ? 1 : 0;
            if ( lowFrequencyImpedance.Count != startIndex + 2 )
                throw new InvalidOperationException( $"Low frequency array has {lowFrequencyImpedance.Count} values instead of {startIndex + 2}" );
            if ( highFrequencyImpedance.Count != startIndex + 2 )
                throw new InvalidOperationException( $"High frequency array has {highFrequencyImpedance.Count} values instead of {startIndex + 2}" );
            var l = new List<double>() { lowFrequencyImpedance[startIndex], lowFrequencyImpedance[startIndex + 1], highFrequencyImpedance[startIndex], highFrequencyImpedance[startIndex + 1] };
            return l.ToArray();
        }

        /// <summary> Merge frequency and impedance arrays. </summary>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        /// null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="frequencies"> The frequencies. </param>
        /// <param name="impedances">  The impedances. </param>
        /// <returns> A String. </returns>
        public static string Merge( string frequencies, string impedances )
        {
            if ( string.IsNullOrEmpty( frequencies ) )
                throw new ArgumentNullException( nameof( frequencies ) );
            if ( string.IsNullOrEmpty( impedances ) )
                throw new ArgumentNullException( nameof( impedances ) );
            var builder = new System.Text.StringBuilder();
            var f = new Queue<string>( frequencies.Split( ',' ) );
            var v = new Queue<string>( impedances.Split( ',' ) );
            if ( 2 * f.Count != v.Count )
            {
                throw new InvalidOperationException( $"Number of values {v.Count} must be twice the number of frequencies {f.Count}" );
            }

            while ( f.Any() )
            {
                _ = builder.Length > 0 ? builder.AppendFormat( "{0}", f.Dequeue() ) : builder.AppendFormat( ",{0}", f.Dequeue() );

                if ( v.Any() )
                    _ = builder.AppendFormat( ",{0}", v.Dequeue() );
                if ( v.Any() )
                    _ = builder.AppendFormat( ",{0}", v.Dequeue() );
            }

            return builder.ToString();
        }


        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the clear measurements command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:{1}:COLL:CLE". </remarks>
        /// <value> The clear measurements command. </value>
        protected virtual string ClearMeasurementsCommand { get; set; }

        /// <summary> Clears the measured data. </summary>
        public void ClearMeasurements()
        {
            _ = this.WriteLine( this.ClearMeasurementsCommand, this.ChannelNumber, this.CompensationTypeCode );
            this.FrequencyArray = Array.Empty<double>();
            this.FrequencyArrayReading = string.Empty;
            this.ImpedanceArray = Array.Empty<double>();
            this.ImpedanceArrayReading = string.Empty;
            this.Enabled = false;
            this.FrequencyStimulusPoints = new int?();
        }

        /// <summary> Gets or sets the Acquire measurements command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:COLL:ACQ:{1}". </remarks>
        /// <value> The clear measurements command. </value>
        protected virtual string AcquireMeasurementsCommand { get; set; }

        /// <summary> Acquires the measured data. </summary>
        public void AcquireMeasurements()
        {
            _ = this.WriteLine( this.ClearMeasurementsCommand, this.ChannelNumber, this.CompensationTypeCode );
        }


        #endregion

        #region " ENABLED "

        /// <summary> The enabled. </summary>
        private bool? _Enabled;

        /// <summary> Gets or sets the cached Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if  Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Enabled
        {
            get => this._Enabled;

            protected set {
                if ( !Equals( this.Enabled, value ) )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the  Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyEnabled( bool value )
        {
            _ = this.WriteEnabled( value );
            return this.QueryEnabled();
        }

        /// <summary> Gets or sets the compensation enabled query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:{1}:STAT?". </remarks>
        /// <value> The compensation enabled query command. </value>
        protected virtual string EnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the compensation Enabled sentinel. Also sets the
        /// <see cref="Enabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryEnabled()
        {
            this.Enabled = this.Query( this.Enabled, string.Format( this.EnabledQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.Enabled;
        }

        /// <summary> Gets or sets the enabled command Format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:{1}:STAT {0:1;1;0}". </remarks>
        /// <value> The compensation enabled query command. </value>
        protected virtual string EnabledCommandFormat { get; set; }

        /// <summary> Writes the  Enabled sentinel. Does not read back from the instrument. </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteEnabled( bool value )
        {
            _ = this.WriteLine( this.EnabledCommandFormat, this.ChannelNumber, this.CompensationTypeCode, value.GetHashCode() );
            this.Enabled = value;
            return this.Enabled;
        }

        #endregion

        #region " FREQUENCY STIMULUS POINTS "

        /// <summary> The Frequency Stimulus Points. </summary>
        private int? _FrequencyStimulusPoints;

        /// <summary>
        /// Gets or sets the cached Frequency Stimulus Points. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? FrequencyStimulusPoints
        {
            get => this._FrequencyStimulusPoints;

            protected set {
                if ( !Nullable.Equals( this.FrequencyStimulusPoints, value ) )
                {
                    this._FrequencyStimulusPoints = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the Frequency Stimulus Points query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:ZME:{1}:POIN?". </remarks>
        /// <value> The Frequency Stimulus Points query command. </value>
        protected virtual string FrequencyStimulusPointsQueryCommand { get; set; }

        /// <summary> Queries the Frequency Stimulus Points. </summary>
        /// <returns> The Frequency Stimulus Points or none if unknown. </returns>
        public int? QueryFrequencyStimulusPoints()
        {
            this.FrequencyStimulusPoints = this.Query( this.FrequencyStimulusPoints,
                                                       string.Format( this.FrequencyStimulusPointsQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.FrequencyStimulusPoints;
        }

        #endregion

        #region " FREQUENCY ARRAY "

        /// <summary> Gets the has complete compensation values. </summary>
        /// <value> The has complete compensation values. </value>
        public bool HasCompleteCompensationValues => !(string.IsNullOrWhiteSpace( this.FrequencyArrayReading ) | string.IsNullOrWhiteSpace( this.ImpedanceArrayReading ));

        /// <summary> The frequency array reading. </summary>
        private string _FrequencyArrayReading;

        /// <summary> Gets or sets the frequency array reading. </summary>
        /// <value> The frequency array reading. </value>
        public string FrequencyArrayReading
        {
            get => this._FrequencyArrayReading;

            protected set {
                if ( !string.Equals( value, this.FrequencyArrayReading ) )
                {
                    this._FrequencyArrayReading = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.HasCompleteCompensationValues ) );
                }
            }
        }

        /// <summary> The Frequency Array. </summary>
        private IList<double> _FrequencyArray;

        /// <summary>
        /// Gets or sets the cached Frequency Array. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public IList<double> FrequencyArray
        {
            get => this._FrequencyArray;

            protected set {
                if ( !Equals( this.FrequencyArray, value ) )
                {
                    this._FrequencyArray = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Frequency Array. </summary>
        /// <param name="value"> The Frequency Array. </param>
        /// <returns> The Frequency Array. </returns>
        public IList<double> ApplyFrequencyArray( IList<double> value )
        {
            _ = this.WriteFrequencyArray( value );
            return this.QueryFrequencyArray();
        }

        /// <summary> Gets or sets the Frequency Array query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:ZME:{1}:FREQ?". </remarks>
        /// <value> The Frequency Array query command. </value>
        protected virtual string FrequencyArrayQueryCommand { get; set; }

        /// <summary> Queries the Frequency Array. </summary>
        /// <returns> The Frequency Array or none if unknown. </returns>
        public IList<double> QueryFrequencyArray()
        {
            this.FrequencyArrayReading = this.QueryTrimEnd( string.Empty, string.Format( this.FrequencyArrayQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            this.FrequencyArray = Parse( this._FrequencyArrayReading );
            return this.FrequencyArray;
        }

        /// <summary> Gets or sets the Frequency Array command format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:ZME:{1}:FREQ {0}". </remarks>
        /// <value> The Frequency Array command format. </value>
        protected virtual string FrequencyArrayCommandFormat { get; set; }

        /// <summary> Writes the Frequency Array without reading back the value from the device. </summary>
        /// <remarks> This command sets the Frequency Array. </remarks>
        /// <param name="values"> The Frequency Array. </param>
        /// <returns> The Frequency Array. </returns>
        public IList<double> WriteFrequencyArray( IList<double> values )
        {
            this.FrequencyArrayReading = Build( values );
            _ = this.WriteLine( this.FrequencyArrayCommandFormat, this._FrequencyArrayReading );
            this.FrequencyArray = values.ToArray();
            return this.FrequencyArray;
        }

        #endregion

        #region " IMPEDANCE ARRAY "

        /// <summary> The impedance array reading. </summary>
        private string _ImpedanceArrayReading;

        /// <summary> Gets or sets the impedance array reading. </summary>
        /// <value> The impedance array reading. </value>
        public string ImpedanceArrayReading
        {
            get => this._ImpedanceArrayReading;

            protected set {
                if ( !string.Equals( value, this.ImpedanceArrayReading ) )
                {
                    this._ImpedanceArrayReading = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.HasCompleteCompensationValues ) );
                }
            }
        }

        /// <summary> The Impedance Array. </summary>
        private IList<double> _ImpedanceArray;

        /// <summary>
        /// Gets or sets the cached Impedance Array. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public IList<double> ImpedanceArray
        {
            get => this._ImpedanceArray;

            protected set {
                if ( !Equals( this.ImpedanceArray, value ) )
                {
                    this._ImpedanceArray = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Impedance Array. </summary>
        /// <param name="reading"> The reading. </param>
        /// <returns> The Impedance Array. </returns>
        public IList<double> ApplyImpedanceArray( string reading )
        {
            _ = this.WriteImpedanceArray( reading );
            return this.QueryImpedanceArray();
        }

        /// <summary> Writes and reads back the Impedance Array. </summary>
        /// <param name="value"> The Impedance Array. </param>
        /// <returns> The Impedance Array. </returns>
        public IList<double> ApplyImpedanceArray( IList<double> value )
        {
            _ = this.WriteImpedanceArray( value );
            return this.QueryImpedanceArray();
        }

        /// <summary> Gets or sets the Impedance Array query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:ZME:{1}:DATA?". </remarks>
        /// <value> The Impedance Array query command. </value>
        protected virtual string ImpedanceArrayQueryCommand { get; set; }

        /// <summary> Queries the Impedance Array. </summary>
        /// <returns> The Impedance Array or none if unknown. </returns>
        public IList<double> QueryImpedanceArray()
        {
            this.ImpedanceArrayReading = this.QueryTrimEnd( "", string.Format( this.ImpedanceArrayQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            this.ImpedanceArray = Parse( this._ImpedanceArrayReading );
            return this.ImpedanceArray;
        }

        /// <summary> Gets or sets the Impedance Array command format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:ZME:{1}:DATA {0}". </remarks>
        /// <value> The Impedance Array command format. </value>
        protected virtual string ImpedanceArrayCommandFormat { get; set; }

        /// <summary> Writes the Impedance Array without reading back the value from the device. </summary>
        /// <remarks> This command sets the Impedance Array. </remarks>
        /// <param name="reading"> The reading. </param>
        /// <returns> The Impedance Array. </returns>
        public IList<double> WriteImpedanceArray( string reading )
        {
            this.ImpedanceArrayReading = reading;
            _ = this.WriteLine( this.ImpedanceArrayCommandFormat, this.ChannelNumber, this.CompensationTypeCode, this._ImpedanceArrayReading );
            this.ImpedanceArray = Parse( reading );
            return this.ImpedanceArray;
        }

        /// <summary> Writes the Impedance Array without reading back the value from the device. </summary>
        /// <remarks> This command sets the Impedance Array. </remarks>
        /// <param name="values"> The Impedance Array. </param>
        /// <returns> The Impedance Array. </returns>
        public IList<double> WriteImpedanceArray( IList<double> values )
        {
            return this.WriteImpedanceArray( Build( values ) );
        }

        /// <summary> Writes the Impedance Array without reading back the value from the device. </summary>
        /// <remarks> This command sets the Impedance Array. </remarks>
        /// <param name="includesFrequency">      true if impedance array includes. </param>
        /// <param name="lowFrequencyImpedance">  The low frequency impedance values. </param>
        /// <param name="highFrequencyImpedance"> The high frequency values. </param>
        /// <returns> The Impedance Array. </returns>
        public IList<double> WriteImpedanceArray( bool includesFrequency, IList<double> lowFrequencyImpedance, IList<double> highFrequencyImpedance )
        {
            return this.WriteImpedanceArray( Merge( includesFrequency, lowFrequencyImpedance, highFrequencyImpedance ) );
        }

        #endregion

        #region " MODEL RESISTANCE "

        /// <summary> The Model Resistance. </summary>
        private double? _ModelResistance;

        /// <summary>
        /// Gets or sets the cached Model Resistance. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ModelResistance
        {
            get => this._ModelResistance;

            protected set {
                if ( !Nullable.Equals( this.ModelResistance, value ) )
                {
                    this._ModelResistance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Model Resistance. </summary>
        /// <param name="value"> The Model Resistance. </param>
        /// <returns> The Model Resistance. </returns>
        public double? ApplyModelResistance( double value )
        {
            _ = this.WriteModelResistance( value );
            return this.QueryModelResistance();
        }

        /// <summary> Gets or sets the Model Resistance query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:R?". </remarks>
        /// <value> The Model Resistance query command. </value>
        protected virtual string ModelResistanceQueryCommand { get; set; }

        /// <summary> Queries the Model Resistance. </summary>
        /// <returns> The Model Resistance or none if unknown. </returns>
        public double? QueryModelResistance()
        {
            this.ModelResistance = this.Query( this.ModelResistance, string.Format( this.ModelResistanceQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelResistance;
        }

        /// <summary> Gets or sets the Model Resistance command format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:R {0}". </remarks>
        /// <value> The Model Resistance command format. </value>
        protected virtual string ModelResistanceCommandFormat { get; set; }

        /// <summary>
        /// Writes the Model Resistance without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the Model Resistance. </remarks>
        /// <param name="value"> The Model Resistance. </param>
        /// <returns> The Model Resistance. </returns>
        public double? WriteModelResistance( double value )
        {
            this.ModelResistance = this.Write( value, string.Format( this.ModelResistanceCommandFormat, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelResistance;
        }

        #endregion

        #region " MODEL CAPACITANCE "

        /// <summary> The Model Capacitance. </summary>
        private double? _ModelCapacitance;

        /// <summary>
        /// Gets or sets the cached Model Capacitance. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ModelCapacitance
        {
            get => this._ModelCapacitance;

            protected set {
                if ( !Nullable.Equals( this.ModelCapacitance, value ) )
                {
                    this._ModelCapacitance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Model Capacitance. </summary>
        /// <param name="value"> The Model Capacitance. </param>
        /// <returns> The Model Capacitance. </returns>
        public double? ApplyModelCapacitance( double value )
        {
            _ = this.WriteModelCapacitance( value );
            return this.QueryModelCapacitance();
        }

        /// <summary> Gets or sets the Model Capacitance query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:C?". </remarks>
        /// <value> The Model Capacitance query command. </value>
        protected virtual string ModelCapacitanceQueryCommand { get; set; }

        /// <summary> Queries the Model Capacitance. </summary>
        /// <returns> The Model Capacitance or none if unknown. </returns>
        public double? QueryModelCapacitance()
        {
            this.ModelCapacitance = this.Query( this.ModelCapacitance, string.Format( this.ModelCapacitanceQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelCapacitance;
        }

        /// <summary> Gets or sets the Model Capacitance command format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:C {0}". </remarks>
        /// <value> The Model Capacitance command format. </value>
        protected virtual string ModelCapacitanceCommandFormat { get; set; }

        /// <summary>
        /// Writes the Model Capacitance without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the Model Capacitance. </remarks>
        /// <param name="value"> The Model Capacitance. </param>
        /// <returns> The Model Capacitance. </returns>
        public double? WriteModelCapacitance( double value )
        {
            this.ModelCapacitance = this.Write( value, string.Format( this.ModelCapacitanceCommandFormat, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelCapacitance;
        }

        #endregion

        #region " MODEL Conductance "

        /// <summary> The Model Conductance. </summary>
        private double? _ModelConductance;

        /// <summary>
        /// Gets or sets the cached Model Conductance. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ModelConductance
        {
            get => this._ModelConductance;

            protected set {
                if ( !Nullable.Equals( this.ModelConductance, value ) )
                {
                    this._ModelConductance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Model Conductance. </summary>
        /// <param name="value"> The Model Conductance. </param>
        /// <returns> The Model Conductance. </returns>
        public double? ApplyModelConductance( double value )
        {
            _ = this.WriteModelConductance( value );
            return this.QueryModelConductance();
        }

        /// <summary> Gets or sets the Model Conductance query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:G?". </remarks>
        /// <value> The Model Conductance query command. </value>
        protected virtual string ModelConductanceQueryCommand { get; set; }

        /// <summary> Queries the Model Conductance. </summary>
        /// <returns> The Model Conductance or none if unknown. </returns>
        public double? QueryModelConductance()
        {
            this.ModelConductance = this.Query( this.ModelConductance, string.Format( this.ModelConductanceQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelConductance;
        }

        /// <summary> Gets or sets the Model Conductance command format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:G {0}". </remarks>
        /// <value> The Model Conductance command format. </value>
        protected virtual string ModelConductanceCommandFormat { get; set; }

        /// <summary>
        /// Writes the Model Conductance without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the Model Conductance. </remarks>
        /// <param name="value"> The Model Conductance. </param>
        /// <returns> The Model Conductance. </returns>
        public double? WriteModelConductance( double value )
        {
            this.ModelConductance = this.Write( value, String.Format( this.ModelConductanceCommandFormat, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelConductance;
        }

        #endregion

        #region " MODEL INDUCTANCE "

        /// <summary> The Model Inductance. </summary>
        private double? _ModelInductance;

        /// <summary>
        /// Gets or sets the cached Model Inductance. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ModelInductance
        {
            get => this._ModelInductance;

            protected set {
                if ( !Nullable.Equals( this.ModelInductance, value ) )
                {
                    this._ModelInductance = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Model Inductance. </summary>
        /// <param name="value"> The Model Inductance. </param>
        /// <returns> The Model Inductance. </returns>
        public double? ApplyModelInductance( double value )
        {
            _ = this.WriteModelInductance( value );
            return this.QueryModelInductance();
        }

        /// <summary> Gets or sets the Model Inductance query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:L?". </remarks>
        /// <value> The Model Inductance query command. </value>
        protected virtual string ModelInductanceQueryCommand { get; set; }

        /// <summary> Queries the Model Inductance. </summary>
        /// <returns> The Model Inductance or none if unknown. </returns>
        public double? QueryModelInductance()
        {
            this.ModelInductance = this.Query( this.ModelInductance, string.Format( this.ModelInductanceQueryCommand, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelInductance;
        }

        /// <summary> Gets or sets the Model Inductance command format. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CKIT:{1}:L {0}". </remarks>
        /// <value> The Model Inductance command format. </value>
        protected virtual string ModelInductanceCommandFormat { get; set; }

        /// <summary>
        /// Writes the Model Inductance without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the Model Inductance. </remarks>
        /// <param name="value"> The Model Inductance. </param>
        /// <returns> The Model Inductance. </returns>
        public double? WriteModelInductance( double value )
        {
            this.ModelInductance = this.Write( value, string.Format( this.ModelInductanceCommandFormat, this.ChannelNumber, this.CompensationTypeCode ) );
            return this.ModelInductance;
        }

        #endregion

    }
}
