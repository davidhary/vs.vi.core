using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core;
using isr.Core.TimeSpanExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI
{
    /// <summary> Defines the contract that must be implemented by a Trigger Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class TriggerSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="TriggerSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected TriggerSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DefineTriggerSourceReadWrites();
            this.DefineTriggerLayerBypassReadWrites();
            this.DefineTriggerEventReadWrites();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.AutoDelayEnabled = false;
            this.TriggerCount = 1;
            this.Delay = TimeSpan.Zero;
            this.InputLineNumber = 1;
            this.OutputLineNumber = 2;
            this.TimerInterval = TimeSpan.FromSeconds( 0.1d );
            this.ContinuousEnabled = false;
            this.TriggerState = VI.TriggerState.None;
            this.MaximumTriggerCount = 99999;
            this.MaximumDelay = TimeSpan.FromSeconds( 999999.999d );
            this.ResetScpiKnownState();
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        private void ResetScpiKnownState()
        {
            this.AutoDelayEnabled = false;
            this.TriggerCount = 1;
            this.Delay = TimeSpan.Zero;
            this.TriggerLayerBypassMode = TriggerLayerBypassModes.Acceptor;
            this.InputLineNumber = 1;
            this.OutputLineNumber = 2;
            this.TriggerSource = TriggerSources.Immediate;
            this.TimerInterval = TimeSpan.FromSeconds( 0.1d );
            this.SupportedTriggerSources = TriggerSources.Bus | TriggerSources.External | TriggerSources.Immediate;
            this.ContinuousEnabled = false;
            this.TriggerState = VI.TriggerState.None;
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the Abort command. </summary>
        /// <remarks> SCPI: ":ABOR". </remarks>
        /// <value> The Abort command. </value>
        protected virtual string AbortCommand { get; set; }

        /// <summary> Aborts operations. </summary>
        /// <remarks>
        /// When this action command is sent, the SourceMeter aborts operation and returns to the idle
        /// state. A faster way to return to idle is to use the DCL or SDC command. With auto output- off
        /// enabled (:SOURce1:CLEar:AUTO ON), the output will remain on if operation is terminated before
        /// the output has a chance to automatically turn off.
        /// </remarks>
        public void Abort()
        {
            if ( !string.IsNullOrWhiteSpace( this.AbortCommand ) )
            {
                _ = this.Session.WriteLine( this.AbortCommand );
            }
        }

        /// <summary> Gets or sets the clear command. </summary>
        /// <remarks> SCPI: ":TRIG:CLE". </remarks>
        /// <value> The clear command. </value>
        protected virtual string ClearCommand { get; set; }

        /// <summary> Clears the triggers. </summary>
        public void ClearTriggers()
        {
            _ = this.WriteLine( this.ClearCommand );
        }

        /// <summary> Gets or sets the clear  trigger model command. </summary>
        /// <remarks> SCPI: ":TRIG:LOAD 'EMPTY'". </remarks>
        /// <value> The clear command. </value>
        protected virtual string ClearTriggerModelCommand { get; set; }

        /// <summary> Clears the trigger model. </summary>
        public void ClearTriggerModel()
        {
            _ = this.WriteLine( this.ClearTriggerModelCommand );
        }

        /// <summary> Gets or sets the initiate command. </summary>
        /// <remarks> SCPI: ":INIT". </remarks>
        /// <value> The initiate command. </value>
        protected virtual string InitiateCommand { get; set; }

        /// <summary> Initiates operations. </summary>
        /// <remarks>
        /// This command is used to initiate source-measure operation by taking the SourceMeter out of
        /// idle. The :READ? and :MEASure? commands also perform an initiation. Note that if auto output-
        /// off is disabled (SOURce1:CLEar:AUTO OFF), the source output must first be turned on before an
        /// initiation can be performed. The :MEASure? command automatically turns the output source on
        /// before performing the initiation.
        /// </remarks>
        public void Initiate()
        {
            _ = this.WriteLine( this.InitiateCommand );
        }

        /// <summary> Gets or sets the Immediate command. </summary>
        /// <remarks> SCPI: ":TRIG:IMM". </remarks>
        /// <value> The Immediate command. </value>
        protected virtual string ImmediateCommand { get; set; }

        /// <summary> Immediately move tot he next layer. </summary>
        public void Immediate()
        {
            if ( !string.IsNullOrWhiteSpace( this.ImmediateCommand ) )
                this.Session.Execute( this.ImmediateCommand );
        }

        #endregion

        #region " AUTO DELAY ENABLED "

        /// <summary> The automatic delay enabled. </summary>
        private bool? _AutoDelayEnabled;

        /// <summary> Gets or sets the cached Auto Delay Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Delay Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoDelayEnabled
        {
            get => this._AutoDelayEnabled;

            protected set {
                if ( !Equals( this.AutoDelayEnabled, value ) )
                {
                    this._AutoDelayEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Delay Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoDelayEnabled( bool value )
        {
            _ = this.WriteAutoDelayEnabled( value );
            return this.QueryAutoDelayEnabled();
        }

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <remarks> SCPI: ":TRIG:DEL:AUTO?". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string AutoDelayEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Delay Enabled sentinel. Also sets the
        /// <see cref="AutoDelayEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoDelayEnabled()
        {
            this.AutoDelayEnabled = this.Query( this.AutoDelayEnabled, this.AutoDelayEnabledQueryCommand );
            return this.AutoDelayEnabled;
        }

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <remarks> SCPI: ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string AutoDelayEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoDelayEnabled( bool value )
        {
            this.AutoDelayEnabled = this.Write( value, this.AutoDelayEnabledCommandFormat );
            return this.AutoDelayEnabled;
        }

        #endregion

        #region " AVERAGING ENABLED "

        /// <summary> The averaging enabled. </summary>
        private bool? _AveragingEnabled;

        /// <summary> Gets or sets the cached Averaging Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Averaging Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AveragingEnabled
        {
            get => this._AveragingEnabled;

            protected set {
                if ( !Equals( this.AveragingEnabled, value ) )
                {
                    this._AveragingEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Averaging Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAveragingEnabled( bool value )
        {
            _ = this.WriteAveragingEnabled( value );
            return this.QueryAveragingEnabled();
        }

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <remarks> SCPI: ":TRIG:AVER?". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string AveragingEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Averaging Enabled sentinel. Also sets the
        /// <see cref="AveragingEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAveragingEnabled()
        {
            this.AveragingEnabled = this.Query( this.AveragingEnabled, this.AveragingEnabledQueryCommand );
            return this.AveragingEnabled;
        }

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <remarks> SCPI: ":TRIG:AVER {0:1;1;0}". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string AveragingEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Averaging Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAveragingEnabled( bool value )
        {
            this.AveragingEnabled = this.Write( value, this.AveragingEnabledCommandFormat );
            return this.AveragingEnabled;
        }

        #endregion

        #region " CONTINUOUS ENABLED "

        /// <summary> The continuous enabled. </summary>
        private bool? _ContinuousEnabled;

        /// <summary> Gets or sets the cached Continuous Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Continuous Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ContinuousEnabled
        {
            get => this._ContinuousEnabled;

            protected set {
                if ( !Equals( this.ContinuousEnabled, value ) )
                {
                    this._ContinuousEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Continuous Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyContinuousEnabled( bool value )
        {
            _ = this.WriteContinuousEnabled( value );
            return this.QueryContinuousEnabled();
        }

        /// <summary> Gets or sets the Continuous enabled query command. </summary>
        /// <remarks> SCPI: ":INIT:CONT?". </remarks>
        /// <value> The Continuous enabled query command. </value>
        protected virtual string ContinuousEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Continuous Enabled sentinel. Also sets the
        /// <see cref="ContinuousEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryContinuousEnabled()
        {
            this.ContinuousEnabled = this.Query( this.ContinuousEnabled, this.ContinuousEnabledQueryCommand );
            return this.ContinuousEnabled;
        }

        /// <summary> Gets or sets the Continuous enabled command Format. </summary>
        /// <remarks> SCPI: ":INIT:CONT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Continuous enabled query command. </value>
        protected virtual string ContinuousEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Continuous Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteContinuousEnabled( bool value )
        {
            this.ContinuousEnabled = this.Write( value, this.ContinuousEnabledCommandFormat );
            return this.ContinuousEnabled;
        }

        #endregion

        #region " TRIGGER COUNT "

        /// <summary> Number of maximum triggers. </summary>
        private int _MaximumTriggerCount;

        /// <summary> Gets or sets the cached Maximum Trigger Count. </summary>
        /// <remarks>
        /// Specifies how many times an operation is performed in the specified layer of the Trigger
        /// model.
        /// </remarks>
        /// <value> The Trigger MaximumTriggerCount or none if not set or unknown. </value>
        public int MaximumTriggerCount
        {
            get => this._MaximumTriggerCount;

            protected set {
                if ( !Equals( this.MaximumTriggerCount, value ) )
                {
                    this._MaximumTriggerCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Returns <c>True</c> if using an infinite trigger count. </summary>
        /// <value> The number of is infinite triggers. </value>
        public bool? IsTriggerCountInfinite => this.TriggerCount.HasValue ? this.TriggerCount >= int.MaxValue : new bool?();

        /// <summary> Number of triggers. </summary>
        private int? _TriggerCount;

        /// <summary> Gets or sets the cached Trigger Count. </summary>
        /// <remarks>
        /// Specifies how many times an operation is performed in the specified layer of the trigger
        /// model.
        /// </remarks>
        /// <value> The trigger count or none if not set or unknown. </value>
        public int? TriggerCount
        {
            get => this._TriggerCount;

            protected set {
                if ( !Nullable.Equals( this.TriggerCount, value ) )
                {
                    this._TriggerCount = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.IsTriggerCountInfinite ) );
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Count. </summary>
        /// <param name="value"> The current Trigger Count. </param>
        /// <returns> The TriggerCount or none if unknown. </returns>
        public int? ApplyTriggerCount( int value )
        {
            _ = this.WriteTriggerCount( value );
            return this.QueryTriggerCount();
        }

        /// <summary> Gets or sets trigger count query command. </summary>
        /// <remarks> SCPI: ":TRIG:COUN?". </remarks>
        /// <value> The trigger count query command. </value>
        protected virtual string TriggerCountQueryCommand { get; set; }

        /// <summary> Queries the current Trigger Count. </summary>
        /// <returns> The Trigger Count or none if unknown. </returns>
        public int? QueryTriggerCount()
        {
            if ( !string.IsNullOrWhiteSpace( this.TriggerCountQueryCommand ) )
            {
                double candidateTriggerCount = this.Session.Query( 0d, this.TriggerCountQueryCommand );
                this.TriggerCount = candidateTriggerCount < int.MaxValue ? ( int ) candidateTriggerCount : int.MaxValue;
            }

            return this.TriggerCount;
        }

        /// <summary> Gets or sets trigger count command format. </summary>
        /// <remarks> SCPI: ":TRIG:COUN {0}". </remarks>
        /// <value> The trigger count command format. </value>
        protected virtual string TriggerCountCommandFormat { get; set; }

        /// <summary> Write the Trigger Count without reading back the value from the device. </summary>
        /// <param name="value"> The current PointsTriggerCount. </param>
        /// <returns> The Trigger Count or none if unknown. </returns>
        public int? WriteTriggerCount( int value )
        {
            if ( !string.IsNullOrWhiteSpace( this.TriggerCountCommandFormat ) )
            {
                _ = value == int.MaxValue
                    ? this.Session.WriteLine( this.TriggerCountCommandFormat, "INF" )
                    : this.Session.WriteLine( this.TriggerCountCommandFormat, ( object ) value );
            }

            this.TriggerCount = value;
            return this.TriggerCount;
        }

        #endregion

        #region " DELAY "

        /// <summary> The maximum delay. </summary>
        private TimeSpan _MaximumDelay;

        /// <summary> Gets or sets the maximum delay. </summary>
        /// <value> The maximum delay. </value>
        public TimeSpan MaximumDelay
        {
            get => this._MaximumDelay;

            protected set {
                if ( !Equals( this.MaximumDelay, value ) )
                {
                    this._MaximumDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The delay. </summary>
        private TimeSpan? _Delay;

        /// <summary> Gets or sets the cached Trigger Delay. </summary>
        /// <remarks>
        /// The delay is used to delay operation in the trigger layer. After the programmed trigger event
        /// occurs, the instrument waits until the delay period expires before performing the Device
        /// Action.
        /// </remarks>
        /// <value> The Trigger Delay or none if not set or unknown. </value>
        public TimeSpan? Delay
        {
            get => this._Delay;

            protected set {
                if ( !Nullable.Equals( this.Delay, value ) )
                {
                    this._Delay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Delay. </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The Trigger Delay or none if unknown. </returns>
        public TimeSpan? ApplyDelay( TimeSpan value )
        {
            _ = this.WriteDelay( value );
            return this.QueryDelay();
        }

        /// <summary> Gets or sets the delay query command. </summary>
        /// <remarks> SCPI: ":TRIG:DEL?". </remarks>
        /// <value> The delay query command. </value>
        protected virtual string DelayQueryCommand { get; set; }

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
        /// <value> The Delay query command. </value>
        protected virtual string DelayFormat { get; set; }

        /// <summary> Queries the Delay. </summary>
        /// <returns> The Delay or none if unknown. </returns>
        public TimeSpan? QueryDelay()
        {
            this.Delay = this.Query( this.Delay, this.DelayFormat, this.DelayQueryCommand );
            return this.Delay;
        }

        /// <summary> Gets or sets the delay command format. </summary>
        /// <remarks> SCPI: ":TRIG:DEL {0:s\.FFFFFFF}". </remarks>
        /// <value> The delay command format. </value>
        protected virtual string DelayCommandFormat { get; set; }

        /// <summary> Writes the Trigger Delay without reading back the value from the device. </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The Trigger Delay or none if unknown. </returns>
        public TimeSpan? WriteDelay( TimeSpan value )
        {
            this.Delay = this.Write( value, this.DelayCommandFormat );
            return this.Delay;
        }

        #endregion

        #region " INPUT LINE NUMBER "

        /// <summary> The input line number. </summary>
        private int? _InputLineNumber;

        /// <summary> Gets or sets the cached Trigger Input Line Number. </summary>
        /// <value> The Trigger InputLineNumber or none if not set or unknown. </value>
        public int? InputLineNumber
        {
            get => this._InputLineNumber;

            protected set {
                if ( !Nullable.Equals( this.InputLineNumber, value ) )
                {
                    this._InputLineNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Input Line Number. </summary>
        /// <param name="value"> The current Input Line Number. </param>
        /// <returns> The Input Line Number or none if unknown. </returns>
        public int? ApplyInputLineNumber( int value )
        {
            _ = this.WriteInputLineNumber( value );
            return this.QueryInputLineNumber();
        }

        /// <summary> Gets or sets the Input Line Number query command. </summary>
        /// <remarks>
        /// SCPI: :TRIG:ILIN?
        /// 2002: :TRIG:TCON:ASYN:ILIN?
        /// </remarks>
        /// <value> The Input Line Number query command. </value>
        protected virtual string InputLineNumberQueryCommand { get; set; }

        /// <summary> Queries the InputLineNumber. </summary>
        /// <returns> The Input Line Number or none if unknown. </returns>
        public int? QueryInputLineNumber()
        {
            this.InputLineNumber = this.Query( this.InputLineNumber, this.InputLineNumberQueryCommand );
            return this.InputLineNumber;
        }

        /// <summary> Gets or sets the Input Line Number command format. </summary>
        /// <remarks>
        /// SCPI: :TRIG:ILIN {0}
        /// 2002: :TRIG:TCON:ASYN:ILIN {0}
        /// </remarks>
        /// <value> The Input Line Number command format. </value>
        protected virtual string InputLineNumberCommandFormat { get; set; }

        /// <summary>
        /// Writes the Trigger Input Line Number without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current InputLineNumber. </param>
        /// <returns> The Trigger Input Line Number or none if unknown. </returns>
        public int? WriteInputLineNumber( int value )
        {
            this.InputLineNumber = this.Write( value, this.InputLineNumberCommandFormat );
            return this.InputLineNumber;
        }

        #endregion

        #region " OUTPUT LINE NUMBER "

        /// <summary> The output line number. </summary>
        private int? _OutputLineNumber;

        /// <summary> Gets or sets the cached Trigger Output Line Number. </summary>
        /// <value> The Trigger OutputLineNumber or none if not set or unknown. </value>
        public int? OutputLineNumber
        {
            get => this._OutputLineNumber;

            protected set {
                if ( !Nullable.Equals( this.OutputLineNumber, value ) )
                {
                    this._OutputLineNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Output Line Number. </summary>
        /// <param name="value"> The current Output Line Number. </param>
        /// <returns> The Output Line Number or none if unknown. </returns>
        public int? ApplyOutputLineNumber( int value )
        {
            _ = this.WriteOutputLineNumber( value );
            return this.QueryOutputLineNumber();
        }

        /// <summary> Gets or sets the Output Line Number query command. </summary>
        /// <remarks>
        /// SCPI: :TRIG:OLIN?
        /// 2002: :TRIG:TCON:ASYN:OLIN?
        /// </remarks>
        /// <value> The Output Line Number query command. </value>
        protected virtual string OutputLineNumberQueryCommand { get; set; }

        /// <summary> Queries the OutputLineNumber. </summary>
        /// <returns> The Output Line Number or none if unknown. </returns>
        public int? QueryOutputLineNumber()
        {
            this.OutputLineNumber = this.Query( this.OutputLineNumber, this.OutputLineNumberQueryCommand );
            return this.OutputLineNumber;
        }

        /// <summary> Gets or sets the Output Line Number command format. </summary>
        /// <remarks>
        /// SCPI: :TRIG:OLIN {0}
        /// 2002: :TRIG:TCON:ASYN:OLIN {0}
        /// </remarks>
        /// <value> The Output Line Number command format. </value>
        protected virtual string OutputLineNumberCommandFormat { get; set; }

        /// <summary>
        /// Writes the Trigger Output Line Number without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current OutputLineNumber. </param>
        /// <returns> The Trigger Output Line Number or none if unknown. </returns>
        public int? WriteOutputLineNumber( int value )
        {
            this.OutputLineNumber = this.Write( value, this.OutputLineNumberCommandFormat );
            return this.OutputLineNumber;
        }

        #endregion

        #region " TIMER TIME SPAN "

        /// <summary> The timer interval. </summary>
        private TimeSpan? _TimerInterval;

        /// <summary> Gets or sets the cached Trigger Timer Interval. </summary>
        /// <remarks>
        /// The Timer Interval is used to Timer Interval operation in the trigger layer. After the
        /// programmed trigger event occurs, the instrument waits until the Timer Interval period expires
        /// before performing the Device Action.
        /// </remarks>
        /// <value> The Trigger Timer Interval or none if not set or unknown. </value>
        public TimeSpan? TimerInterval
        {
            get => this._TimerInterval;

            protected set {
                if ( !this.TimerInterval.Equals( value ) )
                {
                    this._TimerInterval = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Timer Interval. </summary>
        /// <param name="value"> The current TimerTimeSpan. </param>
        /// <returns> The Trigger Timer Interval or none if unknown. </returns>
        public TimeSpan? ApplyTimerTimeSpan( TimeSpan value )
        {
            _ = this.WriteTimerTimeSpan( value );
            return this.QueryTimerTimeSpan();
        }

        /// <summary> Gets the Timer Interval query command. </summary>
        /// <remarks> SCPI: ":TRIG:TIM?". </remarks>
        /// <value> The Timer Interval query command. </value>
        protected virtual string TimerIntervalQueryCommand { get; set; }

        /// <summary> Gets the Timer Interval format for converting the query to time span. </summary>
        /// <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
        /// <value> The Timer Interval query command. </value>
        protected virtual string TimerIntervalFormat { get; set; }

        /// <summary> Queries the Timer Interval. </summary>
        /// <returns> The Timer Interval or none if unknown. </returns>
        public TimeSpan? QueryTimerTimeSpan()
        {
            this.TimerInterval = this.Query( this.TimerInterval, this.TimerIntervalFormat, this.TimerIntervalQueryCommand );
            return this.TimerInterval;
        }

        /// <summary> Gets the Timer Interval command format. </summary>
        /// <remarks> SCPI: ":TRIG:TIM {0:s\.FFFFFFF}". </remarks>
        /// <value> The query command format. </value>
        protected virtual string TimerIntervalCommandFormat { get; set; }

        /// <summary>
        /// Writes the Trigger Timer Interval without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The current TimerTimeSpan. </param>
        /// <returns> The Trigger Timer Interval or none if unknown. </returns>
        public TimeSpan? WriteTimerTimeSpan( TimeSpan value )
        {
            this.TimerInterval = this.Write( value, this.TimerIntervalCommandFormat );
            return default;
        }

        #endregion

        #region " TRIGGER STATE "

        /// <summary> Monitor active trigger state. </summary>
        /// <param name="pollPeriod"> The poll period. </param>
        public void MonitorActiveTriggerState( TimeSpan pollPeriod )
        {
            _ = this.QueryTriggerState();
            while ( this.IsTriggerStateActive() )
            {
                pollPeriod.SpinWait();
                ApplianceBase.DoEvents();
                _ = this.QueryTriggerState();
            }
        }

        /// <summary> Gets the asynchronous task. </summary>
        /// <value> The asynchronous task. </value>
        public System.Threading.Tasks.Task AsyncTask { get; private set; }

        /// <summary> Gets the action task. </summary>
        /// <value> The action task. </value>
        public System.Threading.Tasks.Task ActionTask { get; private set; }

        /// <summary> Asynchronous monitor trigger state. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="pollPeriod"> The poll period. </param>
        /// <returns> A Threading.Tasks.Task. </returns>
        public System.Threading.Tasks.Task AsyncMonitorTriggerState( TimeSpan pollPeriod )
        {
            this.AsyncTask = this.AsyncAwaitMonitorTriggerState( pollPeriod );
            return this.ActionTask;
        }

        /// <summary> Asynchronous await monitor trigger state. </summary>
        /// <param name="pollPeriod"> The poll period. </param>
        /// <returns> A Threading.Tasks.Task. </returns>
        private async System.Threading.Tasks.Task AsyncAwaitMonitorTriggerState( TimeSpan pollPeriod )
        {
            this.ActionTask = System.Threading.Tasks.Task.Run( () => this.MonitorActiveTriggerState( pollPeriod ) );
            await this.ActionTask;
        }

        /// <summary> Gets the trigger state caption. </summary>
        /// <value> The trigger state caption. </value>
        public string TriggerStateCaption => this.TriggerState.HasValue ? this.TriggerState.ToString() : string.Empty;

        /// <summary> State of the trigger. </summary>
        private TriggerState? _TriggerState;

        /// <summary> Gets or sets the cached State TriggerState. </summary>
        /// <value>
        /// The <see cref="TriggerState">State Trigger State</see> or none if not set or unknown.
        /// </value>
        public TriggerState? TriggerState
        {
            get => this._TriggerState;

            protected set {
                if ( !this.TriggerState.Equals( value ) )
                {
                    this._TriggerState = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.TriggerStateCaption ) );
                }
            }
        }

        /// <summary> State of the trigger block. </summary>
        private TriggerState? _TriggerBlockState;

        /// <summary> Gets or sets the cached trigger block <see cref="TriggerState"/>. </summary>
        /// <value>
        /// The <see cref="TriggerBlockState">State Trigger State</see> or none if not set or unknown.
        /// </value>
        public TriggerState? TriggerBlockState
        {
            get => this._TriggerBlockState;

            protected set {
                if ( !this.TriggerBlockState.Equals( value ) )
                {
                    this._TriggerBlockState = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The trigger state block number. </summary>
        private int? _TriggerStateBlockNumber;

        /// <summary> Gets or sets the cached trigger state block number. </summary>
        /// <value> The block number of the trigger state. </value>
        public int? TriggerStateBlockNumber
        {
            get => this._TriggerStateBlockNumber;

            protected set {
                if ( !this.TriggerStateBlockNumber.Equals( value ) )
                {
                    this._TriggerStateBlockNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Query if this object is trigger state done. </summary>
        /// <returns> <c>true</c> if trigger state done; otherwise <c>false</c> </returns>
        public bool IsTriggerStateDone()
        {
            bool result;
            if ( this.TriggerState.HasValue )
            {
                var value = this.TriggerState.Value;
                result = value == VI.TriggerState.Aborted || value == VI.TriggerState.Failed || value == VI.TriggerState.Empty || value == VI.TriggerState.Idle || value == VI.TriggerState.None;
            }
            else
            {
                return false;
            }

            return result;
        }

        /// <summary> Queries if a trigger state is active. </summary>
        /// <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
        public bool IsTriggerStateActive()
        {
            bool result;
            if ( this.TriggerState.HasValue )
            {
                var value = this.TriggerState.Value;
                result = value == VI.TriggerState.Running || value == VI.TriggerState.Waiting;
            }
            else
            {
                return false;
            }

            return result;
        }

        /// <summary> Queries if a trigger state is aborting. </summary>
        /// <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
        public bool IsTriggerStateAborting()
        {
            bool result;
            if ( this.TriggerState.HasValue )
            {
                var value = this.TriggerState.Value;
                result = value == VI.TriggerState.Aborting;
            }
            else
            {
                return false;
            }

            return result;
        }

        /// <summary> Queries if a trigger state is Failed. </summary>
        /// <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
        public bool IsTriggerStateFailed()
        {
            bool result;
            if ( this.TriggerState.HasValue )
            {
                var value = this.TriggerState.Value;
                result = value == VI.TriggerState.Failed;
            }
            else
            {
                return false;
            }

            return result;
        }

        /// <summary> Queries if a trigger state is Idle. </summary>
        /// <returns> <c>true</c> if a trigger state is active; otherwise <c>false</c> </returns>
        public bool IsTriggerStateIdle()
        {
            bool result;
            if ( this.TriggerState.HasValue )
            {
                var value = this.TriggerState.Value;
                result = value == VI.TriggerState.Idle;
            }
            else
            {
                return false;
            }

            return result;
        }

        /// <summary>   Clears the trigger state, setting it to <see cref="VI.TriggerState.None"/> </summary>
        /// <remarks>   David, 2021-07-10. </remarks>
        public void ClearTriggerState()
        {
            this.TriggerState = VI.TriggerState.None;
        }

        /// <summary>   Queries if the trigger plan is inactive. </summary>
        /// <remarks>   David, 2021-07-09. </remarks>
        /// <returns>   True if the trigger plan is inactive, false if not. </returns>
        public bool IsTriggerPlanInactive()
        {
            return !this.TriggerState.HasValue || this.TriggerState.Value == VI.TriggerState.None;
        }

        /// <summary> Gets the Trigger State query command. </summary>
        /// <remarks>
        /// <c>SCPI: :TRIG:STAT? <para>
        /// TSP2: trigger.model.state()
        /// </para></c>
        /// </remarks>
        /// <value> The Trigger State query command. </value>
        protected virtual string TriggerStateQueryCommand { get; set; }

        /// <summary> Queries the Trigger State. </summary>
        /// <returns> The <see cref="TriggerState">Trigger State</see> or none if unknown. </returns>
        public virtual TriggerState? QueryTriggerState()
        {
            string currentValue = this.TriggerState.ToString();
            if ( string.IsNullOrEmpty( this.Session.EmulatedReply ) )
                this.Session.EmulatedReply = currentValue;
            if ( !string.IsNullOrWhiteSpace( this.TriggerStateQueryCommand ) )
            {
                currentValue = this.Session.QueryTrimEnd( this.TriggerStateQueryCommand );
            }

            if ( string.IsNullOrWhiteSpace( currentValue ) )
            {
                this.TriggerState = new TriggerState?();
            }
            else
            {
                var values = new Queue<string>( currentValue.Split( ';' ) );
                if ( values.Any() )
                {
                    this.TriggerState = ( TriggerState ) Conversions.ToInteger( Enum.Parse( typeof( TriggerState ), values.Dequeue(), true ) );
                }

                if ( values.Any() )
                {
                    this.TriggerBlockState = ( TriggerState ) Conversions.ToInteger( Enum.Parse( typeof( TriggerState ), values.Dequeue(), true ) );
                }

                if ( values.Any() )
                {
                    this.TriggerStateBlockNumber = int.Parse( values.Dequeue() );
                }
            }

            return this.TriggerState;
        }

        /// <summary> Gets the state of the supports trigger. </summary>
        /// <value> The supports trigger state. </value>
        public bool SupportsTriggerState => !string.IsNullOrEmpty( this.TriggerStateQueryCommand );

        #endregion

    }

    /// <summary> Values that represent trigger status. </summary>
    public enum TriggerState
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Idle (trigger.STATE_NONE)" )]
        None,

        /// <summary> An enum constant representing the idle option. </summary>
        [System.ComponentModel.Description( "Idle (trigger.STATE_IDLE)" )]
        Idle,

        /// <summary> An enum constant representing the running option. </summary>
        [System.ComponentModel.Description( "Running (trigger.STATE_RUNNING)" )]
        Running,

        /// <summary> An enum constant representing the waiting option. </summary>
        [System.ComponentModel.Description( "Waiting (trigger.STATE_WAITING)" )]
        Waiting,

        /// <summary> An enum constant representing the empty option. </summary>
        [System.ComponentModel.Description( "Empty (trigger.STATE_EMPTY)" )]
        Empty,

        /// <summary> An enum constant representing the building option. </summary>
        [System.ComponentModel.Description( "Building (trigger.STATE_BUILDING)" )]
        Building,

        /// <summary> An enum constant representing the failed option. </summary>
        [System.ComponentModel.Description( "Failed (trigger.STATE_FAILED)" )]
        Failed,

        /// <summary> An enum constant representing the aborting option. </summary>
        [System.ComponentModel.Description( "Aborting (trigger.STATE_ABORTING)" )]
        Aborting,

        /// <summary> An enum constant representing the aborted option. </summary>
        [System.ComponentModel.Description( "Aborted (trigger.STATE_ABORTED)" )]
        Aborted
    }
}
