using System;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI
{

    /// <summary> An insulation test configuration. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-03-09 </para>
    /// </remarks>
    public class InsulationResistance : Core.Models.ViewModelTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public InsulationResistance() : base()
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Defines values for the known clear state. </summary>
        public virtual void DefineClearExecutionState()
        {
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public virtual void InitKnownState()
        {
        }

        /// <summary> Sets the known preset state. </summary>
        public virtual void PresetKnownState()
        {
        }

        /// <summary> Sets the known reset (default) state. </summary>
        public virtual void ResetKnownState()
        {
            this.DwellTime = TimeSpan.FromSeconds( 2d );
            this.CurrentLimit = 0.00001d;
            this.PowerLineCycles = 1d;
            this.VoltageLevel = 10d;
            this.ResistanceLowLimit = 10000000d;
            this.ResistanceRange = 1000000000d;
            this.ContactCheckEnabled = true;
        }

        #endregion

        #region " FIELDS "

        /// <summary> The dwell time. </summary>
        private TimeSpan _DwellTime;

        /// <summary> Gets or sets the dwell time. </summary>
        /// <value> The dwell time. </value>
        public TimeSpan DwellTime
        {
            get => this._DwellTime;

            set {
                if ( this.DwellTime != value )
                {
                    this._DwellTime = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The current limit. </summary>
        private double _CurrentLimit;

        /// <summary> Gets or sets the current limit. </summary>
        /// <value> The current limit. </value>
        public double CurrentLimit
        {
            get => this._CurrentLimit;

            set {
                if ( value != this.CurrentLimit )
                {
                    this._CurrentLimit = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.CurrentRange ) );
                }
            }
        }

        /// <summary> The power line cycles. </summary>
        private double _PowerLineCycles;

        /// <summary> Gets or sets the power line cycles. </summary>
        /// <value> The power line cycles. </value>
        public double PowerLineCycles
        {
            get => this._PowerLineCycles;

            set {
                if ( value != this.PowerLineCycles )
                {
                    this._PowerLineCycles = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The voltage level. </summary>
        private double _VoltageLevel;

        /// <summary> Gets or sets the voltage level. </summary>
        /// <value> The voltage level. </value>
        public double VoltageLevel
        {
            get => this._VoltageLevel;

            set {
                if ( value != this.VoltageLevel )
                {
                    this._VoltageLevel = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.CurrentRange ) );
                }
            }
        }

        /// <summary> The resistance low limit. </summary>
        private double _ResistanceLowLimit;

        /// <summary> Gets or sets the resistance low limit. </summary>
        /// <value> The resistance low limit. </value>
        public double ResistanceLowLimit
        {
            get => this._ResistanceLowLimit;

            set {
                if ( value != this.ResistanceLowLimit )
                {
                    this._ResistanceLowLimit = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( this.CurrentRange ) );
                }
            }
        }

        /// <summary> The resistance range. </summary>
        private double _ResistanceRange;

        /// <summary> Gets or sets the resistance range. </summary>
        /// <value> The resistance range. </value>
        public double ResistanceRange
        {
            get => this._ResistanceRange;

            set {
                if ( value != this.ResistanceRange )
                {
                    this._ResistanceRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True to enable, false to disable the contact check. </summary>
        private bool _ContactCheckEnabled;

        /// <summary> Gets or sets the contact check enabled. </summary>
        /// <value> The contact check enabled. </value>
        public bool ContactCheckEnabled
        {
            get => this._ContactCheckEnabled;

            set {
                if ( value != this.ContactCheckEnabled )
                {
                    this._ContactCheckEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the current range. Based on the ratio of the voltage to the minimum resistance. If the
        /// resistance is zero, returns the current limit.
        /// </summary>
        /// <value> The current range. </value>
        public double CurrentRange => this.ResistanceLowLimit > 0d ? this.VoltageLevel / this.ResistanceLowLimit : 1.01d * this.CurrentLimit;

        #endregion

        #region " I TALKER "

        /// <summary> Identifies talkers. </summary>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        #endregion

        #region " TALKER PUBLISH "

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
