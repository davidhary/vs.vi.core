using System;
using System.Collections.Generic;

namespace isr.VI
{

    /// <summary> Parses and holds the instrument version information. </summary>
    /// <remarks>
    /// (c) 2008 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2008-01-15, 2.0.2936. Derived from previous Scpi Instrument implementation. </para>
    /// </remarks>
    public abstract class VersionInfoBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Specialized default constructor for use only by derived classes. </summary>
        protected VersionInfoBase() : base()
        {
            this.ClearThis();
        }

        #endregion

        #region " INFORMATION PROPERTIES "

        /// <summary>
        /// Gets or sets the identity information that was used to parse the version information.
        /// </summary>
        /// <value> The identity. </value>
        public string Identity { get; set; }

        /// <summary> Gets or sets the extended version information. </summary>
        /// <value> The firmware revision. </value>
        public string FirmwareRevision { get; set; }

        /// <summary> Gets or sets the instrument manufacturer name . </summary>
        /// <value> The name of the manufacturer. </value>
        public string ManufacturerName { get; set; }

        /// <summary> Gets or sets the instrument model number. </summary>
        /// <value>
        /// A string property that may include additional precursors such as 'Model' to the relevant
        /// information.
        /// </value>
        public string Model { get; set; }

        /// <summary> Gets or sets the serial number. </summary>
        /// <value> The serial number. </value>
        public string SerialNumber { get; set; }

        /// <summary> Gets or sets the list of revision elements. </summary>
        /// <value> The firmware revision elements. </value>
        public System.Collections.Specialized.StringDictionary FirmwareRevisionElements { get; set; }

        #endregion

        #region " PARSE "

        /// <summary> Clears this object to its blank/initial state. </summary>
        private void ClearThis()
        {
            this.FirmwareRevisionElements = new System.Collections.Specialized.StringDictionary();
            this.Identity = string.Empty;
            this.FirmwareRevision = string.Empty;
            this.ManufacturerName = string.Empty;
            this.Model = string.Empty;
            this.SerialNumber = string.Empty;
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        public virtual void Clear()
        {
            this.ClearThis();
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <param name="revision"> Specifies the instrument firmware revision. </param>
        protected virtual void ParseFirmwareRevision( string revision )
        {
            this.FirmwareRevisionElements = new System.Collections.Specialized.StringDictionary();
        }

        /// <summary> Builds the identity. </summary>
        /// <returns> A String. </returns>
        public string BuildIdentity()
        {
            var builder = new System.Text.StringBuilder();
            _ = string.IsNullOrWhiteSpace( this.ManufacturerName )
                ? builder.Append( "Manufacturer," )
                : builder.Append( $"{this.ManufacturerName}," );

            _ = string.IsNullOrWhiteSpace( this.Model ) ? builder.Append( "Model," ) : builder.Append( $"{this.Model}," );

            _ = string.IsNullOrWhiteSpace( this.SerialNumber ) ? builder.Append( "1," ) : builder.Append( $"{this.SerialNumber}," );

            _ = string.IsNullOrWhiteSpace( this.FirmwareRevision )
                ? builder.Append( "Oct 10 1997" )
                : builder.Append( $"{this.FirmwareRevision}" );

            return builder.ToString();
        }

        /// <summary> Parses the instrument identity string. </summary>
        /// <remarks> The firmware revision can be further interpreted by the child instruments. </remarks>
        /// <param name="value"> Specifies the instrument identity string, which includes at a minimum the
        /// following information:
        /// <see cref="ManufacturerName">manufacturer</see>,
        /// <see cref="Model">model</see>,
        /// <see cref="SerialNumber">serial number</see>, e.g., <para>
        /// <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997
        /// 09:51:36/A02
        /// D/B/E.</c>.</para> </param>
        public virtual void Parse( string value )
        {
            if ( string.IsNullOrEmpty( value ) )
            {
                this.Clear();
            }
            else
            {

                // save the identity.
                this.Identity = value;

                // Parse the id to get the revision number
                var idItems = new Queue<string>( value.Split( ',' ) );

                // company, e.g., KEITHLEY INSTRUMENTS,
                this.ManufacturerName = idItems.Dequeue();

                // 24xx: MODEL 2420
                // 7510: MODEL DMM7510
                this.Model = idItems.Dequeue();
                string stripCandidate = "MODEL ";
                if ( this.Model.StartsWith( stripCandidate, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.Model = this.Model.Substring( stripCandidate.Length );
                }

                // Serial Number: 0669977
                this.SerialNumber = idItems.Dequeue();

                // 2002: C11 Oct 10 1997 09:51:36/A02 /D/B/E
                // 7510: 1.0.0i
                this.FirmwareRevision = idItems.Dequeue();

                // parse thee firmware revision
                this.ParseFirmwareRevision( this.FirmwareRevision );
            }
        }

        #endregion

    }

    /// <summary>
    /// Enumerates the instrument board types as defined by the instrument identity.
    /// </summary>
    public enum FirmwareRevisionElement
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary> An enum constant representing the analog option. </summary>
        [System.ComponentModel.Description( "Analog" )]
        Analog,

        /// <summary> An enum constant representing the digital option. </summary>
        [System.ComponentModel.Description( "Digital" )]
        Digital,

        /// <summary> An enum constant representing the display option. </summary>
        [System.ComponentModel.Description( "Display" )]
        Display,

        /// <summary> An enum constant representing the contact check option. </summary>
        [System.ComponentModel.Description( "Contact Check" )]
        ContactCheck,

        /// <summary> An enum constant representing the LED display option. </summary>
        [System.ComponentModel.Description( "LED Display" )]
        LedDisplay,

        /// <summary> An enum constant representing the primary option. </summary>
        [System.ComponentModel.Description( "Primary" )]
        Primary,

        /// <summary> An enum constant representing the secondary option. </summary>
        [System.ComponentModel.Description( "Secondary" )]
        Secondary,

        /// <summary> An enum constant representing the ternary option. </summary>
        [System.ComponentModel.Description( "Ternary" )]
        Ternary,

        /// <summary> An enum constant representing the quaternary option. </summary>
        [System.ComponentModel.Description( "Quaternary" )]
        Quaternary,

        /// <summary> An enum constant representing the mainframe option. </summary>
        [System.ComponentModel.Description( "Mainframe" )]
        Mainframe,

        /// <summary> An enum constant representing the boot code option. </summary>
        [System.ComponentModel.Description( "Boot code" )]
        BootCode,

        /// <summary> An enum constant representing the front panel option. </summary>
        [System.ComponentModel.Description( "Front panel" )]
        FrontPanel,

        /// <summary> An enum constant representing the internal meter option. </summary>
        [System.ComponentModel.Description( "Internal Meter" )]
        InternalMeter
    }
}
