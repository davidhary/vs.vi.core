using System.Collections.Generic;

namespace isr.VI
{

    /// <summary>
    /// Dictionary of ranges ordered by integer key, which could come from function modes.
    /// </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-16 </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable",
        Justification = "[Serializable] relates essentially just to BinaryFormatter, which usually isn't a good choice." )]
    public class RangeDictionary : Dictionary<int, Core.Primitives.RangeR>
    {
    }

    /// <summary> Dictionary of boolean values, which could be searched by function modes. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-02-08 </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable",
        Justification = "[Serializable] relates essentially just to BinaryFormatter, which usually isn't a good choice." )]
    public class BooleanDictionary : Dictionary<int, bool>
    {
    }

    /// <summary>
    /// Dictionary of <see cref="Arebis.TypedUnits.Unit"/> which could be searched by function modes.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-30 </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable",
        Justification = "[Serializable] relates essentially just to BinaryFormatter, which usually isn't a good choice." )]
    public class UnitDictionary : Dictionary<int, Arebis.TypedUnits.Unit>
    {
    }

    /// <summary> Dictionary of integer values, which could be searched by function modes. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-30 </para>
    /// </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Usage", "CA2237:Mark ISerializable types with serializable",
        Justification = "[Serializable] relates essentially just to BinaryFormatter, which usually isn't a good choice." )]
    public class IntegerDictionary : Dictionary<int, int>
    {
    }
}
