using System;

using isr.Core.Models;

namespace isr.VI
{

    /// <summary> Defines the binning information . </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-11-05, . based on SCPI 5.1 library. </para>
    /// </remarks>
    public class BinningInfo : ViewModelBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        public BinningInfo() : base()
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Defines values for the known clear state. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void DefineClearExecutionState()
        {
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void InitKnownState()
        {
        }

        /// <summary> Sets the known preset state. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void PresetKnownState()
        {
        }

        /// <summary> Sets the known reset (default) state. </summary>
        private void ResetKnownStateThis()
        {
            this._LimitFailed = new bool?();
            this._Enabled = false;
            this._UpperLimit = 1d;
            this._UpperLimitFailureBits = 15;
            this._LowerLimit = -1;
            this._LowerLimitFailureBits = 15;
            this._PassBits = 15;
            this._StrobePulseWidth = TimeSpan.FromTicks( ( long ) (0.01d * TimeSpan.TicksPerMillisecond) );
            this._InputLineNumber = 1;
            this._OutputLineNumber = 2;
            this._ArmCount = 0;
            this._ArmDirection = TriggerLayerBypassModes.Acceptor;
            this._ArmSource = ArmSources.Immediate;
            this._TriggerDirection = TriggerLayerBypassModes.Acceptor;
            this._TriggerSource = TriggerSources.Immediate;
        }

        /// <summary> Sets the known reset (default) state. </summary>
        public void ResetKnownState()
        {
            this.ResetKnownStateThis();
        }

        #endregion

        #region " ARM SOURCE "

        /// <summary> The arm source mode. </summary>
        private ArmSources _ArmSource;

        /// <summary> Gets or sets the arm Source. </summary>
        /// <value>
        /// The <see cref="ArmSource">source Function Mode</see> or none if not set or unknown.
        /// </value>
        public ArmSources ArmSource
        {
            get => this._ArmSource;

            set {
                if ( !this.ArmSource.Equals( value ) )
                {
                    this._ArmSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " COUNT "

        /// <summary> Number of arms. </summary>
        private int _ArmCount;

        /// <summary> Gets or sets the arm Count. </summary>
        /// <value> The source Count or none if not set or unknown. </value>
        public int ArmCount
        {
            get => this._ArmCount;

            set {
                if ( !Equals( this.ArmCount, value ) )
                {
                    this._ArmCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ARM LAYER BYPASS MODE "

        /// <summary> The Arm Layer Bypass Mode. </summary>
        private TriggerLayerBypassModes _ArmDirection;

        /// <summary> Gets or sets the Arm Layer Bypass Mode. </summary>
        /// <value>
        /// The <see cref="ArmDirection">Arm Layer Bypass Mode</see> or none if not set or unknown.
        /// </value>
        public TriggerLayerBypassModes ArmDirection
        {
            get => this._ArmDirection;

            set {
                if ( !this.ArmDirection.Equals( value ) )
                {
                    this._ArmDirection = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " ENABLED "

        /// <summary> The on/off state. </summary>
        private bool _Enabled;

        /// <summary> Gets or sets the enabled state. </summary>
        /// <value>
        /// <c>True</c> if the Enabled; <c>False</c> if not, or none if unknown or not set.
        /// </value>
        public bool Enabled
        {
            get => this._Enabled;

            set {
                if ( this.Enabled != value )
                {
                    this._Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FAILURE BITS "

        /// <summary> The Failure Bits. </summary>
        private int _FailureBits;

        /// <summary> Gets or sets the Output fail bit pattern (15). </summary>
        /// <value> The Failure Bits or none if not set or unknown. </value>
        public int FailureBits
        {
            get => this._FailureBits;

            set {
                if ( this.FailureBits != value )
                {
                    this._FailureBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " INPUT LINE NUMBER "

        /// <summary> The Input Line Number. </summary>
        private int _InputLineNumber;

        /// <summary> Gets or sets the arm Input Line Number. </summary>
        /// <value> The source Input Line Number or none if not set or unknown. </value>
        public int InputLineNumber
        {
            get => this._InputLineNumber;

            set {
                if ( !Equals( this.InputLineNumber, value ) )
                {
                    this._InputLineNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LIMIT FAILED "

        /// <summary> The Limit Failed. </summary>
        private bool? _LimitFailed;

        /// <summary> Gets or sets the fail condition (False) state. </summary>
        /// <value>
        /// <c>True</c> if the Limit Failed; <c>False</c> if not, or none if unknown or not set.
        /// </value>
        public bool? LimitFailed
        {
            get => this._LimitFailed;

            set {
                if ( !Equals( this.LimitFailed, value ) )
                {
                    this._LimitFailed = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOWER LIMIT "

        /// <summary> The Lower Limit. </summary>
        private double _LowerLimit;

        /// <summary> Gets or sets the lower limit. </summary>
        /// <value> The Lower Limit or none if not set or unknown. </value>
        public double LowerLimit
        {
            get => this._LowerLimit;

            set {
                if ( this.LowerLimit != value )
                {
                    this._LowerLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " LOWER LIMIT FAILURE BITS "

        /// <summary> The Lower Limit Failure Bits. </summary>
        private int _LowerLimitFailureBits;

        /// <summary> Gets or sets the lower limit failure bit pattern (15). </summary>
        /// <value> The Lower Limit FailureBits or none if not set or unknown. </value>
        public int LowerLimitFailureBits
        {
            get => this._LowerLimitFailureBits;

            set {
                if ( this.LowerLimitFailureBits != value )
                {
                    this._LowerLimitFailureBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OUTPUT LINE NUMBER "

        /// <summary> The Output Line Number. </summary>
        private int _OutputLineNumber;

        /// <summary> Gets or sets the Output Line Number. </summary>
        /// <value> The source Output Line Number or none if not set or unknown. </value>
        public int OutputLineNumber
        {
            get => this._OutputLineNumber;

            set {
                if ( !Equals( this.OutputLineNumber, value ) )
                {
                    this._OutputLineNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " PASS BITS "

        /// <summary> The Pass Bits. </summary>
        private int _PassBits;

        /// <summary> Gets or sets the Output pass bit pattern (15). </summary>
        /// <value> The Pass Bits or none if not set or unknown. </value>
        public int PassBits
        {
            get => this._PassBits;

            set {
                if ( this.PassBits != value )
                {
                    this._PassBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " STROBE PULSE WIDTH "

        /// <summary> The Strobe Pulse Width. </summary>
        private TimeSpan _StrobePulseWidth;

        /// <summary> Gets or sets the end of test strobe pulse width. </summary>
        /// <value> The Strobe Pulse Width or none if not set or unknown. </value>
        public TimeSpan StrobePulseWidth
        {
            get => this._StrobePulseWidth;

            set {
                if ( this.StrobePulseWidth != value )
                {
                    this._StrobePulseWidth = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TRIGGER SOURCE "

        /// <summary> The Trigger source mode. </summary>
        private TriggerSources _TriggerSource;

        /// <summary> Gets or sets the Trigger Source. </summary>
        /// <value>
        /// The <see cref="TriggerSource">source Function Mode</see> or none if not set or unknown.
        /// </value>
        public TriggerSources TriggerSource
        {
            get => this._TriggerSource;

            set {
                if ( !this.TriggerSource.Equals( value ) )
                {
                    this._TriggerSource = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " TRIGGER DIRECTION "

        /// <summary> The Trigger Direction. </summary>
        private TriggerLayerBypassModes _TriggerDirection;

        /// <summary> Gets or sets the trigger Direction. </summary>
        /// <value>
        /// The <see cref="TriggerDirection">Trigger Direction</see> or none if not set or unknown.
        /// </value>
        public TriggerLayerBypassModes TriggerDirection
        {
            get => this._TriggerDirection;

            set {
                if ( !this.TriggerDirection.Equals( value ) )
                {
                    this._TriggerDirection = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " UPPER LIMIT "

        /// <summary> The Upper Limit. </summary>
        private double _UpperLimit;

        /// <summary> Gets or sets the Upper limit. </summary>
        /// <value> The Upper Limit or none if not set or unknown. </value>
        public double UpperLimit
        {
            get => this._UpperLimit;

            set {
                if ( this.UpperLimit != value )
                {
                    this._UpperLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " UPPER LIMIT FAILURE BITS "

        /// <summary> The Upper Limit Failure Bits. </summary>
        private int _UpperLimitFailureBits;

        /// <summary> Gets or sets the Upper limit failure bit pattern (15). </summary>
        /// <value> The Upper Limit FailureBits or none if not set or unknown. </value>
        public int UpperLimitFailureBits
        {
            get => this._UpperLimitFailureBits;

            set {
                if ( this.UpperLimitFailureBits != value )
                {
                    this._UpperLimitFailureBits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
