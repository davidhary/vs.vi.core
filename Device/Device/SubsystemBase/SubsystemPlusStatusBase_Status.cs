using System;

namespace isr.VI
{
    public partial class SubsystemPlusStatusBase
    {

        /// <summary>   Await query result status ready. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="readyToQueryTimeout">  The ready to query timeout. </param>
        /// <returns>   A Tuple: (bool TimedOut, int StatusByte, TimeSpan Elapsed). </returns>
        public (bool TimedOut, int StatusByte, TimeSpan Elpased) AwaitQueryResultStatusReady( TimeSpan readyToQueryTimeout )
        {
            return this.Session.AwaitStatusReady( readyToQueryTimeout, this.StatusSubsystem.IsStatusQueryResultReady, this.StatusSubsystem.IsStatusError );
        }

        /// <summary>   Executes the command after waiting for status ready. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="readyToQueryTimeout">  The ready to query timeout. </param>
        /// <param name="dataToWrite">          The data to write. </param>
        /// <returns>   <see cref="ExecuteInfo"/> </returns>
        public ExecuteInfo ExecuteStatusReady( TimeSpan readyToQueryTimeout, string dataToWrite )
        {
            return string.IsNullOrWhiteSpace( dataToWrite )
                ? ExecuteInfo.Empty
                : this.Session.ExecuteStatusReady( readyToQueryTimeout, dataToWrite,
                    ( statusByte ) => !this.StatusSubsystem.IsStatusBusy( statusByte ), this.StatusSubsystem.IsStatusError );
        }

        /// <summary>   Queries if status data ready. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="readyToQueryTimeout">  The ready to query timeout. </param>
        /// <param name="readyToReadTimeout">   The ready to read timeout. </param>
        /// <param name="dataToWrite">          The data to write. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, QueryInfo QueryInfo). </returns>
        public (string ReceivedMessage, QueryInfo QueryInfo) QueryIfStatusDataReady( TimeSpan readyToQueryTimeout, TimeSpan readyToReadTimeout, string dataToWrite )
        {
            return this.Session.QueryIfStatusReady( readyToQueryTimeout, readyToReadTimeout, dataToWrite,
                ( statusByte ) => this.StatusSubsystem.IsStatusDataReady( statusByte ), this.StatusSubsystem.IsStatusError );
        }

        /// <summary>   Reads if status data ready. </summary>
        /// <remarks>   David, 2021-04-16. </remarks>
        /// <param name="readyToReadTimeout">   The ready to read timeout. </param>
        /// <returns>   if status data ready. </returns>
        public (string ReceivedMessage, ReadInfo ReadInfo) ReadIfStatusDataReady( TimeSpan readyToReadTimeout )
        {
            return this.Session.ReadIfStatusReady( readyToReadTimeout, ( statusByte ) => this.StatusSubsystem.IsStatusDataReady( statusByte ), this.StatusSubsystem.IsStatusError );
        }

        /// <summary>   Queries if status data ready. </summary>
        /// <remarks>   David, 2021-04-06. </remarks>
        /// <param name="statusByte">           The status byte. </param>
        /// <param name="readyToReadTimeout">   The ready to read timeout. </param>
        /// <param name="dataToWrite">          The data to write. </param>
        /// <returns> A Tuple: (bool MessageFetched, string ReceivedMessage, QueryInfo QueryInfo) </returns>
        public (bool MessageFetched, string ReceivedMessage, QueryInfo QueryInfo) QueryIfStatusDataReady( int statusByte, TimeSpan readyToReadTimeout, string dataToWrite )
        {
            return this.Session.QueryIfStatusReady( statusByte, readyToReadTimeout, dataToWrite, ( statusByte ) => !this.StatusSubsystem.IsStatusBusy( statusByte ),
                                                                ( statusByte ) => this.StatusSubsystem.IsStatusDataReady( statusByte ), this.StatusSubsystem.IsStatusError );
        }

        /// <summary>   Queries the specified data to write checking for status ready. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="readyToQueryTimeout">  The ready to query timeout. </param>
        /// <param name="readyToReadTimeout">   The ready to read timeout. </param>
        /// <param name="dataToWrite">          The data to write. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, QueryInfo QueryInfo) . </returns>
        public (string ReceivedMessage, QueryInfo QueryInfo) QueryStatusReady( TimeSpan readyToQueryTimeout, TimeSpan readyToReadTimeout, string dataToWrite )
        {
            return this.Session.QueryStatusReady( readyToQueryTimeout, readyToReadTimeout, dataToWrite, ( statusByte ) => !this.StatusSubsystem.IsStatusBusy( statusByte ), this.StatusSubsystem.IsStatusError );
        }



        /// <summary>   Writes the specified data to write checking for status ready. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="readyToWriteTimeout">  The ready to write timeout. </param>
        /// <param name="dataToWrite">          The data to write. </param>
        /// <returns>   <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo WriteStatusReady( TimeSpan readyToWriteTimeout, string dataToWrite )
        {
            return this.Session.WriteStatusReady( readyToWriteTimeout, dataToWrite,
                ( statusByte ) => !this.StatusSubsystem.IsStatusBusy( statusByte ), this.StatusSubsystem.IsStatusError );
        }

        /// <summary>   Await status. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <param name="timeout">  The timeout. </param>
        /// <returns>   A Tuple. </returns>
        public virtual (bool TimedOut, int StatusByte, TimeSpan Elapsed) AwaitStatus( TimeSpan timeout )
        {
            return this.Session.AwaitStatus( timeout, ( statusByte ) => !this.StatusSubsystem.IsStatusBusy( statusByte ) );
        }

    }
}
