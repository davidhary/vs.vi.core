using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core;

namespace isr.VI
{
    public partial class SubsystemCollection : ITalker
    {

        /// <summary> The talker. </summary>
        private ITraceMessageTalker _Talker;

        /// <summary> Gets or sets the trace message talker. </summary>
        /// <value> The trace message talker. </value>
        public ITraceMessageTalker Talker
        {
            get => this._Talker;

            private set {
                if ( this._Talker is object )
                {
                    this.RemoveListeners();
                }

                this._Talker = value;
            }
        }

        /// <summary> True if is assigned talker, false if not. </summary>
        private bool _IsAssignedTalker;

        /// <summary> Assigns a talker. </summary>
        /// <param name="talker"> The talker. </param>
        public virtual void AssignTalker( ITraceMessageTalker talker )
        {
            this._IsAssignedTalker = talker is object;
            this.Talker = this._IsAssignedTalker ? talker : new TraceMessageTalker();
            this.IdentifyTalkers();
            foreach ( SubsystemBase subsystem in this )
                subsystem.AssignTalker( talker );
        }

        /// <summary> Identifies talkers. </summary>
        public virtual void IdentifyTalkers()
        {
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Removes the private listeners. Removes all listeners if the talker was not assigned.
        /// </summary>
        public virtual void RemoveListeners()
        {
            this.RemovePrivateListeners();
            if ( !this._IsAssignedTalker )
                this.Talker.RemoveListeners();
        }

        /// <summary> The private listeners. </summary>
        private List<IMessageListener> _PrivateListeners;

        /// <summary> Gets the private listeners. </summary>
        /// <value> The private listeners. </value>
        public virtual IList<IMessageListener> PrivateListeners
        {
            get {
                if ( this._PrivateListeners is null )
                    this._PrivateListeners = new List<IMessageListener>();
                return this._PrivateListeners;
            }
        }

        /// <summary> Adds a private listener. </summary>
        /// <param name="listener"> The listener. </param>
        public virtual void AddPrivateListener( IMessageListener listener )
        {
            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.Add( listener );
            }

            this.AddListener( listener );
        }

        /// <summary> Adds private listeners. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void AddPrivateListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.AddRange( listeners );
            }

            this.AddListeners( listeners );
        }

        /// <summary> Adds private listeners. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public virtual void AddPrivateListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
                throw new ArgumentNullException( nameof( talker ) );
            this.AddPrivateListeners( talker.Listeners );
        }

        /// <summary> Removes the private listener described by listener. </summary>
        /// <param name="listener"> The listener. </param>
        public virtual void RemovePrivateListener( IMessageListener listener )
        {
            this.RemoveListener( listener );
            if ( this.PrivateListeners is object )
            {
                _ = this._PrivateListeners.Remove( listener );
            }
        }

        /// <summary> Removes the private listeners. </summary>
        public virtual void RemovePrivateListeners()
        {
            foreach ( IMessageListener listener in this.PrivateListeners )
                this.RemoveListener( listener );
            this._PrivateListeners.Clear();
        }

        /// <summary> Removes the listener described by listener. </summary>
        /// <param name="listener"> The listener. </param>
        public virtual void RemoveListener( IMessageListener listener )
        {
            this.Talker.RemoveListener( listener );
        }

        /// <summary> Removes the specified listeners. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void RemoveListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            foreach ( IMessageListener listener in listeners )
                this.RemoveListener( listener );
        }

        /// <summary> Adds a listener. </summary>
        /// <param name="listener"> The listener. </param>
        public virtual void AddListener( IMessageListener listener )
        {
            this.Talker.AddListener( listener );
            foreach ( SubsystemBase element in this.Items )
                element.Talker.AddListener( listener );
            this.IdentifyTalkers();
        }

        /// <summary> Adds the listeners. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void AddListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            foreach ( IMessageListener listener in listeners )
                this.AddListener( listener );
        }

        /// <summary> Adds the listeners. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public virtual void AddListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
                throw new ArgumentNullException( nameof( talker ) );
            this.AddListeners( talker.Listeners );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public virtual void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            // this should apply only to the listeners associated with this form
            // Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
            foreach ( ITalker element in this.Items )
                element.ApplyListenerTraceLevel( listenerType, value );
            this.IdentifyTalkers();
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public virtual void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this.Talker.ApplyTalkerTraceLevel( listenerType, value );
            foreach ( ITalker element in this.Items )
                element.ApplyTalkerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <param name="talker"> The talker. </param>
        public virtual void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            this.Talker.ApplyTalkerTraceLevels( talker );
            foreach ( ITalker element in this.Items )
                element.ApplyTalkerTraceLevels( talker );
        }

        /// <summary> Applies the talker listeners trace levels described by talker. </summary>
        /// <param name="talker"> The talker. </param>
        public virtual void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            this.Talker.ApplyListenerTraceLevels( talker );
            foreach ( ITalker element in this.Items )
                element.ApplyListenerTraceLevels( talker );
        }
    }
}
