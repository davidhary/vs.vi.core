using System;
using System.Diagnostics;

using isr.Core;
using isr.VI.ExceptionExtensions;

namespace isr.VI
{

    /// <summary> Collection of subsystems. </summary>
    /// <remarks>
    /// The synchronization context is captured as part of the property change and other event
    /// handlers and is no longer needed here. <para>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved., Inc. All rights
    /// reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-12-21 </para>
    /// </remarks>
    public partial class SubsystemCollection : System.Collections.ObjectModel.Collection<SubsystemBase>, ITalker
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        public SubsystemCollection() : base()
        {
            this._Talker = new TraceMessageTalker();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks> Clears the queues and sets all registers to zero. </remarks>
        public void DefineClearExecutionState()
        {
            foreach ( SubsystemBase element in this.Items )
                element.DefineClearExecutionState();
        }

        /// <summary>
        /// Performs a reset and additional custom setting for the subsystem:<para>
        /// </para>
        /// </summary>
        public void InitKnownState()
        {
            foreach ( SubsystemBase element in this.Items )
                element.InitKnownState();
        }

        /// <summary>
        /// Gets subsystem to the following default system preset values:<para>
        /// </para>
        /// </summary>
        public void PresetKnownState()
        {
            foreach ( SubsystemBase element in this.Items )
                element.PresetKnownState();
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> Clears the queues and sets all registers to zero. </remarks>
        public void DefineKnownResetState()
        {
            foreach ( SubsystemBase element in this.Items )
                element.DefineKnownResetState();
        }

        #endregion

        #region " ADD "

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public new void Add( SubsystemBase item )
        {
            if ( item is null )
                throw new ArgumentNullException( nameof( item ) );
            base.Add( item );
            if ( this.Talker is object )
                item.AssignTalker( this.Talker );
        }

        #endregion

        #region " CLEAR/DISPOSE "

        /// <summary> Disposes items and clear. </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public new void Clear()
        {
            foreach ( SubsystemBase element in this.Items )
            {
                try
                {
                    (element as IDisposable)?.Dispose();
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
                }
            }

            base.Clear();
        }

        #endregion

    }
}
