using System;
using System.Diagnostics;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by Subsystems. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-01-15, 1.0.1841.x. </para>
    /// </remarks>
    public abstract partial class SubsystemBase : Core.Models.ViewModelTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SubsystemBase" /> class. </summary>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        protected SubsystemBase( Pith.SessionBase session ) : base()
        {
            this.ApplySessionThis( session );
            this.ElapsedTimeStopwatch = new Stopwatch();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks> Clears the queues and sets all registers to zero. </remarks>
        public virtual void DefineClearExecutionState()
        {
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public virtual void InitKnownState()
        {
        }

        /// <summary> Sets the subsystem known preset state. </summary>
        public virtual void PresetKnownState()
        {
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> Clears the queues and sets all registers to zero. </remarks>
        public virtual void DefineKnownResetState()
        {
        }

        #endregion

        #region " SESSION "

        /// <summary> Gets the session. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The session. </value>
        public Pith.SessionBase Session { get; private set; }

        /// <summary> Applies the session described by value. </summary>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        public void ApplySession( Pith.SessionBase session )
        {
            this.ApplySessionThis( session );
            this.NotifyPropertyChanged( nameof( this.ResourceNameCaption ) );
            this.NotifyPropertyChanged( nameof( this.ResourceTitleCaption ) );
            this.NotifyPropertyChanged( nameof( this.IsDeviceOpen ) );
        }

        /// <summary> Applies the session described by value. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        private void ApplySessionThis( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            this.Session = session;
            session.PropertyChanged += this.SessionPropertyChanged;
        }

        /// <summary> Handles the session property changed action. </summary>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( Pith.SessionBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Pith.SessionBase.ResourceTitleCaption ):
                    {
                        this.NotifyPropertyChanged( nameof( this.ResourceTitleCaption ) );
                        break;
                    }

                case nameof( Pith.SessionBase.ResourceNameCaption ):
                    {
                        this.NotifyPropertyChanged( nameof( this.ResourceNameCaption ) );
                        break;
                    }
            }
        }

        /// <summary> Handles the Session property changed event. </summary>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( sender is null || e is null )
                return;
            string activity = $"handling {nameof( Pith.SessionBase )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as Pith.SessionBase, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Gets a value indicating whether a session to the device is open. See also
        /// <see cref="VI.Pith.SessionBase.IsDeviceOpen"/>.
        /// </summary>
        /// <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
        public bool IsDeviceOpen => this.Session is object && this.Session.IsDeviceOpen;

        /// <summary> Gets the resource title. </summary>
        /// <value> The resource title. </value>
        public string ResourceTitleCaption => this.Session?.ResourceTitleCaption;

        /// <summary> Gets the resource name caption. </summary>
        /// <value> The resource name caption. </value>
        public string ResourceNameCaption => this.Session?.ResourceNameCaption;

        #endregion

        #region " PARSE SUBSYSTEM MESSAGE "

        /// <summary>   Parses a subsystem message assigning value to a subsystem property. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="messageReceived">  The message received. </param>
        /// <returns>   True if it message was parsed by the subsystem; otherwise, false. </returns>
        public virtual bool ParseSubsystemMessage( string messageReceived )
        {
            return false;
        }

        #endregion

        #region " QUERY / WRITE / EXECUTE "

        #region " ENUMERATION "

        /// <summary> Queries first value returned from the instrument. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <param name="value">        The value. </param>
        /// <returns> The first value. </returns>
        public T? QueryFirstValue<T>( string queryCommand, T? value ) where T : struct
        {
            return !string.IsNullOrWhiteSpace( queryCommand ) ? this.Session.QueryFirstEnumValue( value, queryCommand ) : value;
        }

        /// <summary> Issues the query command and parses the returned enum value into an Enum. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <param name="value">        The value. </param>
        /// <returns> The parsed value or none if unknown. </returns>
        public T? QueryValue<T>( string queryCommand, T? value ) where T : struct
        {
            return !string.IsNullOrWhiteSpace( queryCommand ) ? this.Session.QueryEnumValue( value, queryCommand ) : value;
        }

        /// <summary> Writes the Enum value without reading back the value from the device. </summary>
        /// <param name="commandFormat"> The command format. </param>
        /// <param name="value">         The value. </param>
        /// <returns> The value or none if unknown. </returns>
        public T? WriteValue<T>( string commandFormat, T value ) where T : struct
        {
            return this.Session.WriteEnumValue( value, commandFormat );
        }

        /// <summary>
        /// Issues the query command and parses the returned enum value name into an Enum.
        /// </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <param name="value">        The value. </param>
        /// <returns> The parsed value or none if unknown. </returns>
        public T? Query<T>( string queryCommand, T? value ) where T : struct
        {
            return !string.IsNullOrWhiteSpace( queryCommand ) ? this.Session.QueryEnum( value, queryCommand ) : value;
        }

        /// <summary>
        /// Issues the query command and parses the returned enum value name into an Enum.
        /// </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <param name="value">        The value. </param>
        /// <param name="readWrites">   The read writes. </param>
        /// <returns> The parsed value or none if unknown. </returns>
        public T? Query<T>( string queryCommand, T value, Pith.EnumReadWriteCollection readWrites ) where T : struct
        {
            return !string.IsNullOrWhiteSpace( queryCommand ) ? this.Session.Query( value, readWrites, queryCommand ) : value;
        }

        /// <summary> Queries first value returned from the instrument. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <param name="value">        The value. </param>
        /// <param name="readWrites">   The read writes. </param>
        /// <returns> The first value. </returns>
        public T? QueryFirstValue<T>( string queryCommand, T value, Pith.EnumReadWriteCollection readWrites ) where T : struct
        {
            return !string.IsNullOrWhiteSpace( queryCommand ) ? this.Session.QueryFirst( value, readWrites, queryCommand ) : value;
        }

        /// <summary> Writes the Enum value name without reading back the value from the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="commandFormat"> The command format. </param>
        /// <param name="value">         The value. </param>
        /// <param name="readWrites">    Dictionary of parses. </param>
        /// <returns> The value or none if unknown. </returns>
        public T? Write<T>( string commandFormat, T value, Pith.EnumReadWriteCollection readWrites ) where T : struct
        {
            return this.Session.Write( value, commandFormat, readWrites );
        }

        /// <summary> Writes the Enum value name without reading back the value from the device. </summary>
        /// <param name="commandFormat"> The command format. </param>
        /// <param name="value">         The value. </param>
        /// <returns> The value or none if unknown. </returns>
        public T? Write<T>( string commandFormat, T value ) where T : struct
        {
            return this.Session.Write( value, commandFormat );
        }

        #endregion

        #region " BOOLEAN "

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">        The value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The value. </returns>
        public bool? Query( bool? value, string queryCommand )
        {
            return string.IsNullOrWhiteSpace( queryCommand )
                ? value
                : this.Session.Query( value.GetValueOrDefault( default ), queryCommand );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        The value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns>   A Tuple: (bool? ParsedValue, <see cref="QueryParseBooleanInfo"/>) . </returns>
        public (bool? ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryElapsed( bool? value, string queryCommand )
        {
            return string.IsNullOrWhiteSpace( queryCommand )
                ? (value, QueryParseBooleanInfo.Empty)
                : this.Session.QueryElapsed( value.GetValueOrDefault( default ), queryCommand );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">                    The value. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="queryCommand">             The query command. </param>
        /// <returns>   The value. </returns>
        public bool? Query( bool? value, int receivedMessageItemIndex, string queryCommand )
        {
            return string.IsNullOrWhiteSpace( queryCommand )
                ? value
                : this.Session.Query( value.GetValueOrDefault( default ), receivedMessageItemIndex, queryCommand );
        }

        /// <summary>   Queries and parses the indexed value from the instrument. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">                    The value. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="queryCommand">             The query command. </param>
        /// <returns>   A Tuple: (bool? ParsedValue, <see cref="QueryParseBooleanInfo"/>) . </returns>
        public (bool? ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryElapsed( bool? value, int receivedMessageItemIndex, string queryCommand )
        {
            return !string.IsNullOrWhiteSpace( queryCommand )
                ? this.Session.QueryElapsed( value.GetValueOrDefault( default ), receivedMessageItemIndex, queryCommand )
                : (value, QueryParseBooleanInfo.Empty);
        }

        /// <summary> Writes the value without reading back the value from the device. </summary>
        /// <param name="value">         The value. </param>
        /// <param name="commandFormat"> The command format. </param>
        /// <returns> The value. </returns>
        public bool Write( bool value, string commandFormat )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                _ = this.Session.WriteLine( commandFormat, ( object ) value.GetHashCode() );
            }
            return value;
        }

        /// <summary>
        /// Writes the value without reading back the value from the device and returns the elapsed time.
        /// Synchronously writes ASCII-encoded string data to the device or interface. Converts the
        /// specified string to an ASCII string and appends it to the formatted I/O write buffer. Appends
        /// a newline (0xA) to the formatted I/O write buffer, flushes the buffer, and sends an END with
        /// the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">            The value. </param>
        /// <param name="commandFormat">    The command format. </param>
        /// <returns>   A Tuple: ( bool vSentValue, ExecuteInfo ExecuteInfo ). </returns>
        public (bool SentValue, WriteBooleanInfo WriteBooleanInfo) WriteLineElapsed( bool value, string commandFormat )
        {
            return string.IsNullOrWhiteSpace( commandFormat )
                ? (value, WriteBooleanInfo.Empty)
                : (value, new WriteBooleanInfo( value, this.Session.WriteLineElapsed( commandFormat, ( object ) value.GetHashCode() ) ));
        }

        #endregion

        #region " INTEGER "

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">        The value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The value. </returns>
        public int? Query( int? value, string queryCommand )
        {
            if ( !string.IsNullOrWhiteSpace( queryCommand ) )
            {
                value = this.Session.Query( value.GetValueOrDefault( default ), queryCommand );
            }
            return value;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        The value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns>   A tuple: (int? ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) . </returns>
        public (int? ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryElapsed( int? value, string queryCommand )
        {
            return !string.IsNullOrWhiteSpace( queryCommand )
                ? this.Session.QueryElapsed( value.GetValueOrDefault( default ), queryCommand )
                : (value, QueryParseInfo<int>.Empty);
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">                    The value. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="queryCommand">             The query command. </param>
        /// <returns>   The value. </returns>
        public int? Query( int? value, int receivedMessageItemIndex, string queryCommand )
        {
            if ( !string.IsNullOrWhiteSpace( queryCommand ) )
            {
                value = this.Session.Query( value.GetValueOrDefault( default ), receivedMessageItemIndex, queryCommand );
            }
            return value;
        }

        /// <summary>   Queries and parses the indexed value from the instrument. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">                    The value. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="queryCommand">             The query command. </param>
        /// <returns>   A tuple: (int? ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo) . </returns>
        public (int? ParsedValue, QueryParseInfo<int> QueryParseInfo) QueryElapsed( int? value, int receivedMessageItemIndex, string queryCommand )
        {
            return !string.IsNullOrWhiteSpace( queryCommand )
                ? this.Session.QueryElapsed( value.GetValueOrDefault( default ), receivedMessageItemIndex, queryCommand )
                : (value, QueryParseInfo<int>.Empty);
        }

        /// <summary> Writes the value without reading back the value from the device. </summary>
        /// <param name="value">         The value. </param>
        /// <param name="commandFormat"> The command format. </param>
        /// <returns> The value. </returns>
        public int Write( int value, string commandFormat )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                _ = this.Session.WriteLine( commandFormat, ( object ) value );
            }
            return value;
        }

        /// <summary>
        /// Writes the value without reading back the value from the device and returns the elapsed time.
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">            The value. </param>
        /// <param name="commandFormat">    The command format. </param>
        /// <returns>   A Tuple: (int SentValue, <see cref="WriteInfo{T}"/> WriteInfo ) . </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteLineElapsed( int value, string commandFormat )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                ExecuteInfo executeInfo = this.Session.WriteLineElapsed( commandFormat, ( object ) value );
                return (value, new WriteInfo<int>( value, commandFormat, executeInfo.SentMessage, executeInfo.GetElapsedTimes() ));
            }
            return (value, WriteInfo<int>.Empty);
        }

        #endregion

        #region " DOUBLE "

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <param name="value">        The value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The value. </returns>
        public double? Query( double? value, string queryCommand )
        {
            if ( !string.IsNullOrWhiteSpace( queryCommand ) )
            {
                value = this.Session.Query( value.GetValueOrDefault( default ), queryCommand );
            }
            return value;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        The value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns>   A Tuple: (double? ReceivedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo). </returns>
        public (double? ReceivedValue, QueryParseInfo<double> QueryParseInfo) QueryElapsed( double? value, string queryCommand )
        {
            return !string.IsNullOrWhiteSpace( queryCommand )
                ? this.Session.QueryElapsed( value.GetValueOrDefault( default ), queryCommand )
                : (value, QueryParseInfo<double>.Empty);
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">                    The value. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="queryCommand">             The query command. </param>
        /// <returns>   The value. </returns>
        public double? Query( double? value, int receivedMessageItemIndex, string queryCommand )
        {
            if ( !string.IsNullOrWhiteSpace( queryCommand ) )
            {
                value = this.Session.Query( value.GetValueOrDefault( default ), receivedMessageItemIndex, queryCommand );
            }
            return value;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read and
        /// parses the item identified by the item index from a comma-separated received message.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">                    The value. </param>
        /// <param name="receivedMessageItemIndex"> Zero-based index of the received message item. </param>
        /// <param name="queryCommand">             The query command. </param>
        /// <returns>   A Tuple: (double? ReceivedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo). </returns>
        public (double? ReceivedValue, QueryParseInfo<double> QueryParseInfo) QueryElapsed( double? value, int receivedMessageItemIndex, string queryCommand )
        {
            return !string.IsNullOrWhiteSpace( queryCommand )
                ? this.Session.QueryElapsed( value.GetValueOrDefault( default ), receivedMessageItemIndex, queryCommand )
                : (value, QueryParseInfo<double>.Empty);
        }

        /// <summary> Write the value without reading back the value from the device. </summary>
        /// <param name="value">         The value. </param>
        /// <param name="commandFormat"> The command format. </param>
        /// <returns> The value. </returns>
        public double Write( double value, string commandFormat )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                if ( value >= Pith.Scpi.Syntax.Infinity - 1d )
                {
                    _ = this.Session.WriteLine( commandFormat, "MAX" );
                    value = Pith.Scpi.Syntax.Infinity;
                }
                else if ( value <= Pith.Scpi.Syntax.NegativeInfinity + 1d )
                {
                    _ = this.Session.WriteLine( commandFormat, "MIN" );
                    value = Pith.Scpi.Syntax.NegativeInfinity;
                }
                else
                {
                    _ = this.Session.WriteLine( commandFormat, ( object ) value );
                }
            }
            return value;
        }

        /// <summary> Write the value without reading back the value from the device and return the elapsed time. </summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">            The value. </param>
        /// <param name="commandFormat">    The command format. </param>
        /// <returns>   A Tuple: (int SentValue, <see cref="WriteInfo{T}"/> WriteInfo ) . </returns>
        public (double SentValue, WriteInfo<double> WriteInfo) WriteLineElapsed( double value, string commandFormat )
        {
            Stopwatch sw = Stopwatch.StartNew();
            string sentMessage = string.Empty;
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                if ( value >= Pith.Scpi.Syntax.Infinity - 1d )
                {
                    sentMessage = this.Session.WriteLine( commandFormat, "MAX" );
                    value = Pith.Scpi.Syntax.Infinity;
                }
                else if ( value <= Pith.Scpi.Syntax.NegativeInfinity + 1d )
                {
                    sentMessage = this.Session.WriteLine( commandFormat, "MIN" );
                    value = Pith.Scpi.Syntax.NegativeInfinity;
                }
                else
                {
                    sentMessage = this.Session.WriteLine( commandFormat, ( object ) value );
                }
            }
            return (value, new WriteInfo<double>( value, commandFormat, sentMessage, sw.Elapsed ));
        }

        #endregion

        #region " STRING "

        /// <summary>   Reads the elapsed. </summary>
        /// <remarks>   David, 2021-04-16. </remarks>
        /// <returns>   The elapsed. </returns>
        public (string ReceivedMessage, ReadInfo ReadInfo) ReadElapsed()
        {
            return this.Session.ReadElapsed();
        }

        /// <summary>   Queries a string returning the elapsed time. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/>) . </returns>
        public (string ReceivedMessage, QueryInfo QueryInfo) QueryElapsed( string queryCommand )
        {
            return string.IsNullOrWhiteSpace( queryCommand )
                ? (queryCommand, QueryInfo.Empty)
                : this.Session.QueryElapsed( queryCommand );
        }

        /// <summary> Queries a <see cref="T:String">String</see> value. </summary>
        /// <param name="value">        The present value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The value. </returns>
        public string QueryTrimEnd( string value, string queryCommand )
        {
            return !string.IsNullOrWhiteSpace( queryCommand ) ? this.Session.QueryTrimEnd( queryCommand ) : value;
        }

        /// <summary> Queries a <see cref="T:String">String</see> value. </summary>
        /// <param name="value">         The present value. </param>
        /// <param name="commandFormat"> The command format. </param>
        /// <param name="args">          A variable-length parameters list containing arguments. </param>
        /// <returns> The value. </returns>
        public string QueryTrimEnd( string value, string commandFormat, params object[] args )
        {
            return this.QueryTrimEnd( value, string.Format( commandFormat, args ) );
        }

        /// <summary>   Queries a string returning the elapsed time. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">        The present value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/>) . </returns>
        public (string ReceivedMessage, QueryInfo) QueryElapsed( string value, string queryCommand )
        {
            return string.IsNullOrWhiteSpace( queryCommand )
                ? (value, QueryInfo.Empty)
                : this.Session.QueryElapsed( queryCommand );
        }

        /// <summary>   Queries a string returning the elapsed time. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="value">            The present value. </param>
        /// <param name="commandFormat">    The command format. </param>
        /// <param name="args">             A variable-length parameters list containing arguments. </param>
        /// <returns>   A Tuple: (string receivedMessage, TimeSpan Elapsed ) . </returns>
        public (string ReceivedMessage, QueryInfo) QueryElapsed( string value, string commandFormat, params object[] args )
        {
            return string.IsNullOrWhiteSpace( commandFormat )
                ? (value, QueryInfo.Empty)
                : this.Session.QueryElapsed( string.Format( System.Globalization.CultureInfo.InvariantCulture, commandFormat, args ) );
        }

        /// <summary>
        /// Write the value without reading back the value from the device. Synchronously writes ASCII-
        /// encoded string data to the device or interface. Converts the specified string to an ASCII
        /// string and appends it to the formatted I/O write buffer. Appends a newline (0xA) to the
        /// formatted I/O write buffer, flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <param name="commandFormat">    The command format. </param>
        /// <param name="args">             A variable-length parameters list containing arguments. </param>
        /// <returns>   The value. </returns>
        public string WriteLine( string commandFormat, params object[] args )
        {
            return this.WriteLine( string.Format( commandFormat, args ) );
        }

        /// <summary>
        /// Write the value without reading back the value from the device. Synchronously writes ASCII-
        /// encoded string data to the device or interface. Converts the specified string to an ASCII
        /// string and appends it to the formatted I/O write buffer. Appends a newline (0xA) to the
        /// formatted I/O write buffer, flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <param name="value">    The value. </param>
        /// <returns>   The value. </returns>
        public string WriteLine( string value )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
                _ = this.Session.WriteLine( value );
            return value;
        }

        /// <summary>
        /// Write the value without reading back the value from the device and return the elapsed time.
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="dataToWrite">  The data to write. </param>
        /// <returns>   <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo WriteLineElapsed( string dataToWrite )
        {
            return string.IsNullOrWhiteSpace( dataToWrite )
                ? ExecuteInfo.Empty
                : this.Session.WriteLineElapsed( dataToWrite );
        }

        /// <summary>
        /// Write the value without reading back the value from the device and return the elapsed time.
        /// Synchronously writes ASCII-encoded string data to the device or interface.
        /// Converts the specified string to an ASCII string and appends it to the formatted
        /// I/O write buffer. Appends a newline (0xA) to the formatted I/O write buffer,
        /// flushes the buffer, and sends an END with the buffer if required.
        /// </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="commandFormat">    The command format. </param>
        /// <param name="args">             A variable-length parameters list containing arguments. </param>
        /// <returns>   A Tuple: ( double SentValue, TimeSpan Elapsed ). </returns>
        public ExecuteInfo WriteLineElapsed( string commandFormat, params object[] args )
        {
            return string.IsNullOrWhiteSpace( commandFormat )
                ? ExecuteInfo.Empty
                : this.Session.WriteLineElapsed( string.Format( System.Globalization.CultureInfo.InvariantCulture, commandFormat, args ) );
        }

        #endregion

        #region " TIME SPAN "

        /// <summary> Queries an <see cref="T:TimeSpan">TimeSpan</see> value. </summary>
        /// <param name="value">        The value. </param>
        /// <param name="format">       Describes the format to use. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The value. </returns>
        public TimeSpan? Query( TimeSpan? value, string format, string queryCommand )
        {
            if ( !string.IsNullOrWhiteSpace( format ) && !string.IsNullOrWhiteSpace( queryCommand ) )
            {
                value = this.Session.Query( format, queryCommand );
            }

            return value;
        }

        /// <summary> Write the value without reading back the value from the device. </summary>
        /// <param name="value">         The value. </param>
        /// <param name="commandFormat"> The command format. </param>
        /// <returns> The value. </returns>
        public TimeSpan? Write( TimeSpan value, string commandFormat )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
            {
                _ = this.Session.WriteLine( commandFormat, ( object ) value );
            }

            return value;
        }

        #endregion

        #region " PAYLOAD "

        /// <summary> Issues the query command and parses the returned payload. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="payload"> The payload. </param>
        /// <returns>
        /// <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
        /// <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
        /// </returns>
        public bool Query( Pith.PayloadBase payload )
        {
            if ( payload is null )
                throw new ArgumentNullException( nameof( payload ) );
            bool result = true;
            if ( !string.IsNullOrWhiteSpace( payload.QueryCommand ) )
            {
                result = this.Session.Query( payload );
            }

            return result;
        }

        /// <summary>
        /// Write the payload. A <see cref="Query(VI.Pith.PayloadBase)"/> must be issued to get the value
        /// from the device.
        /// </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="payload"> The payload. </param>
        /// <returns>
        /// <c>True</c> if <see cref="VI.Pith.PayloadStatus"/> is
        /// <see cref="VI.Pith.PayloadStatus.Okay"/>; otherwise <c>False</c>.
        /// </returns>
        public bool Write( Pith.PayloadBase payload )
        {
            if ( payload is null )
                throw new ArgumentNullException( nameof( payload ) );
            bool result = true;
            if ( !string.IsNullOrWhiteSpace( payload.CommandFormat ) )
            {
                result = this.Session.Write( payload );
            }

            return result;
        }

        #endregion

        #region " EXECUTE "

        /// <summary> Executes the command. </summary>
        /// <param name="command"> The command. </param>
        public void Execute( string command )
        {
            if ( !string.IsNullOrWhiteSpace( command ) )
                this.Session.Execute( command );
        }

        /// <summary> Executes the command. </summary>
        /// <param name="commandFormat"> The command format. </param>
        /// <param name="args">          A variable-length parameters list containing arguments. </param>
        public void Execute( string commandFormat, params object[] args )
        {
            if ( !string.IsNullOrWhiteSpace( commandFormat ) )
                this.Session.Execute( string.Format( System.Globalization.CultureInfo.InvariantCulture, commandFormat, args ) );
        }

        /// <summary>   Executes the 'elapsed' operation. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="command">  The command. </param>
        /// <returns>   <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo ExecuteElapsed( string command )
        {
            return (string.IsNullOrWhiteSpace( command ))
                ? ExecuteInfo.Empty
                : this.Session.ExecuteElapsed( command );
        }

        /// <summary>   Executes the 'elapsed' operation. </summary>
        /// <remarks>   David, 2021-04-10. </remarks>
        /// <param name="commandFormat">    The command format. </param>
        /// <param name="args">             A variable-length parameters list containing arguments. </param>
        /// <returns>   <see cref="ExecuteInfo"/>. </returns>
        public ExecuteInfo ExecuteElapsed( string commandFormat, params object[] args )
        {
            return (string.IsNullOrWhiteSpace( commandFormat ))
                ? ExecuteInfo.Empty
                : this.Session.ExecuteElapsed( string.Format( System.Globalization.CultureInfo.InvariantCulture, commandFormat, args ) );
        }

        #endregion

        #region " QUERY WITH STATUS READ "

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
        /// after the specified delay. A final read is performed after a delay provided no error occurred.
        /// </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="readReadyDelay">   The read ready delay. </param>
        /// <param name="readDelay">        The read delay. </param>
        /// <param name="dataToWrite">      The data to write. </param>
        /// <param name="queryStatusError"> The query status error. </param>
        /// <returns>   A Tuple: (string ReceivedMessage, <see cref="QueryInfo"/> QueryInfo). </returns>
        public (string ReceivedMessage, QueryInfo QueryInfo) QueryStatusRead( TimeSpan readReadyDelay, TimeSpan readDelay, string dataToWrite,
            Func<(bool, string, int)> queryStatusError )
        {
            return this.Session.QueryIfNoStatusError( readReadyDelay, readDelay, dataToWrite, queryStatusError );
        }


        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
        /// after the specified delay. A final read is performed after a delay provided no error occurred.
        /// </summary>
        /// <param name="statusReadDelay"> The read delay. </param>
        /// <param name="readDelay">       The read delay. </param>
        /// <param name="dataToWrite">     The data to write. </param>
        /// <returns>
        /// The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
        /// </returns>
        public string Query( TimeSpan statusReadDelay, TimeSpan readDelay, string dataToWrite )
        {
            return string.IsNullOrWhiteSpace( dataToWrite )
                ? string.Empty
                : this.Session.QueryIfNoStatusError( statusReadDelay, readDelay, dataToWrite ).ReceivedMessage;
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous status
        /// after the specified delay. A final read is performed after a delay provided no error occurred.
        /// </summary>
        /// <param name="statusReadDelay"> The read delay. </param>
        /// <param name="readDelay">       The read delay. </param>
        /// <param name="dataToWrite">     The data to write. </param>
        /// <returns> The trim end. </returns>
        public string QueryTrimEnd( TimeSpan statusReadDelay, TimeSpan readDelay, string dataToWrite )
        {
            return string.IsNullOrWhiteSpace( dataToWrite )
                ? string.Empty
                : this.Session.QueryTrimEnd( statusReadDelay, readDelay, dataToWrite );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read
        /// after the specified delay.
        /// </summary>
        /// <param name="readDelay">   The read delay. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns>
        /// The  <see cref="VI.Pith.SessionBase.LastMessageReceived">last received data</see>.
        /// </returns>
        public string Query( TimeSpan readDelay, string dataToWrite )
        {
            return this.Session.Query( readDelay, dataToWrite );
        }

        /// <summary>
        /// Performs a synchronous write of ASCII-encoded string data, followed by a synchronous read
        /// after the specified delay.
        /// </summary>
        /// <param name="readDelay">   The read delay. </param>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The trim end. </returns>
        public string QueryTrimEnd( TimeSpan readDelay, string dataToWrite )
        {
            return this.Session.QueryTrimEnd( readDelay, dataToWrite );
        }

        #endregion

        #endregion

        #region " ELAPSED TIME "

        /// <summary> The last action elapsed time. </summary>
        private TimeSpan _LastActionElapsedTime;

        /// <summary> Gets or sets the last reading. </summary>
        /// <value> The last reading. </value>
        public TimeSpan LastActionElapsedTime
        {
            get => this._LastActionElapsedTime;

            set {
                if ( value != this.LastActionElapsedTime )
                {
                    this._LastActionElapsedTime = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SERVICE REQUEST HANDLING OPTION FLAGS "

        /// <summary>   (Immutable) the service request handling options token. </summary>
        private readonly Lazy<Core.Concurrent.ConcurrentToken<ServiceRequestHandlingOption>> _ServiceRequestHandlingOptionsToken = new( () => new Core.Concurrent.ConcurrentToken<ServiceRequestHandlingOption>() );

        /// <summary>   Gets or sets the Service Request Fetch Reading enabled service request option. </summary>
        /// <value> The Service Request Fetch Reading service request option enabled. </value>
        public bool ServiceRequestFetchReadingEnabled
        {
            get => ServiceRequestHandlingOption.AutoFetchReading == this._ServiceRequestHandlingOptionsToken.Value.Value;

            set {
                if ( this.ServiceRequestFetchReadingEnabled != value )
                {
                    this._ServiceRequestHandlingOptionsToken.Value.Value = value ? ServiceRequestHandlingOption.AutoFetchReading : ServiceRequestHandlingOption.None;
                    this.NotifyPropertyChanged();
                }
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether the Service Request Stream Readings option is enabled.
        /// </summary>
        /// <value> True if Service Request Stream Readings is enabled, false if not. </value>
        public bool ServiceRequestStreamReadingsEnabled
        {
            get => ServiceRequestHandlingOption.StreamReadings == this._ServiceRequestHandlingOptionsToken.Value.Value;

            set {
                if ( this.ServiceRequestStreamReadingsEnabled != value )
                {
                    this._ServiceRequestHandlingOptionsToken.Value.Value = value ? ServiceRequestHandlingOption.StreamReadings : ServiceRequestHandlingOption.None;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>   Gets or sets a value indicating whether the Service Request subsystem parse message is enabled. </summary>
        /// <value> True if Service Request subsystem parse message enabled, false if not. </value>
        public bool ServiceRequestSubsystemParseMessageEnabled
        {
            get => ServiceRequestHandlingOption.ParseMessage == this._ServiceRequestHandlingOptionsToken.Value.Value;

            set {
                if ( this.ServiceRequestSubsystemParseMessageEnabled != value )
                {
                    this._ServiceRequestHandlingOptionsToken.Value.Value = value ? ServiceRequestHandlingOption.ParseMessage : ServiceRequestHandlingOption.None;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }

    /// <summary>   Values that represent service request handling options. </summary>
    /// <remarks>   David, 2021-04-08. </remarks>
    public enum ServiceRequestHandlingOption
    {
        /// <summary>   An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None,

        /// <summary>   An enum constant representing the parse message option. </summary>
        [System.ComponentModel.Description( "Parse Message" )]
        ParseMessage,

        /// <summary>   An enum constant representing the Automatic fetch reading option. </summary>
        [System.ComponentModel.Description( "AutoFetch Reading" )]
        AutoFetchReading,

        /// <summary>   An enum constant representing the stream readings option. </summary>
        [System.ComponentModel.Description( "Stream Readings" )]
        StreamReadings
    }

}


