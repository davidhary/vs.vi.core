using System;

namespace isr.VI
{

    /// <summary>
    /// Defines the contract that must be implemented by Subsystems that report status.
    /// </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-01-15, 1.0.1841.x. </para>
    /// </remarks>
    public abstract partial class SubsystemPlusStatusBase : SubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SubsystemPlusStatusBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected SubsystemPlusStatusBase( StatusSubsystemBase statusSubsystem ) : base( StatusSubsystemBase.Validated( statusSubsystem ).Session )
        {
            this.StatusSubsystem = statusSubsystem;
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets or sets the status subsystem. </summary>
        /// <value> The status subsystem. </value>
        protected StatusSubsystemBase StatusSubsystem { get; private set; }

        #endregion

        #region " CHECK AND THROW "

        /// <summary>
        /// Checks and throws an exception if device errors occurred. Can only be used after receiving a
        /// full reply from the device.
        /// </summary>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="args">           A variable-length parameters list containing arguments. </param>
        public void CheckThrowDeviceException( bool flushReadFirst, string format, params object[] args )
        {
            this.StatusSubsystem.CheckThrowDeviceException( flushReadFirst, format, args );
        }

        #endregion

        #region " CHECK AND REPORT "

        /// <summary>
        /// Check and reports visa or device error occurred. Can only be used after receiving a full
        /// reply from the device.
        /// </summary>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Specifies the report format. </param>
        /// <param name="args">       Specifies the report arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public virtual bool TraceVisaDeviceOperationOkay( int nodeNumber, string format, params object[] args )
        {
            return this.StatusSubsystem.TraceVisaDeviceOperationOkay( nodeNumber, format, args );
        }

        /// <summary>
        /// Checks and reports if a visa or device error occurred. Can only be used after receiving a
        /// full reply from the device.
        /// </summary>
        /// <param name="nodeNumber">     Specifies the remote node number to validate. </param>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="args">           A variable-length parameters list containing arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool TraceVisaDeviceOperationOkay( int nodeNumber, bool flushReadFirst, string format, params object[] args )
        {
            return this.StatusSubsystem.TraceVisaDeviceOperationOkay( nodeNumber, flushReadFirst, format, args );
        }

        /// <summary>
        /// Checks and reports if a visa or device error occurred. Can only be used after receiving a
        /// full reply from the device.
        /// </summary>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="format">         Describes the format to use. </param>
        /// <param name="args">           A variable-length parameters list containing arguments. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public bool TraceVisaDeviceOperationOkay( bool flushReadFirst, string format, params object[] args )
        {
            return this.StatusSubsystem.TraceVisaDeviceOperationOkay( flushReadFirst, format, args );
        }

        /// <summary> Reports if a visa error occurred. Can be used with queries. </summary>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( string format, params object[] args )
        {
            this.StatusSubsystem.TraceVisaOperation( format, args );
        }

        /// <summary> Reports if a visa error occurred. Can be used with queries. </summary>
        /// <param name="ex">     The exception. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( Pith.NativeException ex, string format, params object[] args )
        {
            this.StatusSubsystem.TraceVisaOperation( ex, format, args );
        }

        /// <summary> Reports if a operation error occurred. Can be used with queries. </summary>
        /// <param name="ex">     The exception. </param>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void TraceOperation( Exception ex, string format, params object[] args )
        {
            this.StatusSubsystem.TraceOperation( ex, format, args );
        }

        /// <summary> Reports if a visa error occurred. Can be used with queries. </summary>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Describes the format to use. </param>
        /// <param name="args">       A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( int nodeNumber, string format, params object[] args )
        {
            this.StatusSubsystem.TraceVisaOperation( nodeNumber, format, args );
        }

        /// <summary> Reports if a visa error occurred. Can be used with queries. </summary>
        /// <param name="ex">         The exception. </param>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Describes the format to use. </param>
        /// <param name="args">       A variable-length parameters list containing arguments. </param>
        public void TraceVisaOperation( Pith.NativeException ex, int nodeNumber, string format, params object[] args )
        {
            this.StatusSubsystem.TraceVisaOperation( ex, nodeNumber, format, args );
        }

        /// <summary> Reports if an operation error occurred. Can be used with queries. </summary>
        /// <param name="ex">         The exception. </param>
        /// <param name="nodeNumber"> Specifies the remote node number to validate. </param>
        /// <param name="format">     Describes the format to use. </param>
        /// <param name="args">       A variable-length parameters list containing arguments. </param>
        public void TraceOperation( Exception ex, int nodeNumber, string format, params object[] args )
        {
            this.StatusSubsystem.TraceOperation( ex, nodeNumber, format, args );
        }

        #endregion

    }
}
