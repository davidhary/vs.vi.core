using System;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by a Measure Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class MeasureSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="MeasureSubsystemBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="readingAmounts">  The reading amounts. </param>
        protected MeasureSubsystemBase( StatusSubsystemBase statusSubsystem, ReadingAmounts readingAmounts ) : base( statusSubsystem )
        {
            this.ReadingAmounts = readingAmounts;
            this.DefaultMeasurementUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.DefaultFunctionModeDecimalPlaces = 3;
            this.ApertureRange = Pith.Ranges.StandardApertureRange;
            this.FilterCountRange = Pith.Ranges.StandardFilterCountRange;
            this.FilterWindowRange = Pith.Ranges.StandardFilterWindowRange;
            this.PowerLineCyclesRange = Pith.Ranges.StandardPowerLineCyclesRange;
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
            this.DefineFunctionModeDecimalPlaces();
            this.DefineFunctionModeReadWrites();
            this.DefineFunctionModeRanges();
            this.DefineFunctionModeUnits();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.DefineFunctionClearKnownState();
            this.NotifyPropertyChanged( nameof( this.ReadingAmounts ) );
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            _ = this.ParsePrimaryReading( string.Empty );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.ApertureRange = Pith.Ranges.StandardApertureRange;
            this.FilterCountRange = Pith.Ranges.StandardFilterCountRange;
            this.FilterWindowRange = Pith.Ranges.StandardFilterWindowRange;
            this.PowerLineCyclesRange = Pith.Ranges.StandardPowerLineCyclesRange;
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
        }

        #endregion

        #region " INIT, READ, FETCH, MEASURE "

        /// <summary> Gets or sets the fetch command. </summary>
        /// <remarks> SCPI: 'FETCh?'. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The fetch command. </value>
        protected virtual string FetchCommand { get; set; }

        /// <summary> Fetches the data. </summary>
        /// <remarks>
        /// Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
        /// there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
        /// the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
        /// Buffer. Thus, subsequent executions of FETCh? acquire the same data.
        /// </remarks>
        /// <returns> The reading. </returns>
        public virtual string FetchReading()
        {
            return this.FetchReading( this.FetchCommand );
        }

        /// <summary> Fetches the data. </summary>
        /// <remarks>
        /// Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
        /// there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
        /// the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
        /// Buffer. Thus, subsequent executions of FETCh? acquire the same data.
        /// </remarks>
        /// <returns> A Double? </returns>
        public virtual double? Fetch()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return this.MeasureReadingAmounts( this.FetchCommand );
        }

        /// <summary> Gets or sets the read command. </summary>
        /// <remarks> SCPI: 'READ'. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The read command. </value>
        protected virtual string ReadCommand { get; set; }

        /// <summary> Initiates an operation and then fetches the data. </summary>
        /// <remarks>
        /// Issues the 'READ?' query, which performs a trigger initiation and then a
        /// <see cref="Fetch">FETCh? </see>
        /// The initiate triggers a new measurement cycle which puts new data in the Sample Buffer. Fetch
        /// reads that new data. The
        /// <see cref="VI.MeasureSubsystemBase.MeasureReadingAmounts(string)">Measure</see> command places
        /// the instrument in a “one-shot” mode and then performs a read.
        /// </remarks>
        /// <returns> A Double? </returns>
        public virtual double? Read()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return this.MeasureReadingAmounts( this.ReadCommand );
        }

        /// <summary> Gets or sets The Measure query command. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The Measure query command. </value>
        protected virtual string MeasureQueryCommand { get; set; }

        /// <summary> Queries readings into the reading amounts. </summary>
        /// <returns> The reading or none if unknown. </returns>
        public virtual double? MeasureReadingAmounts()
        {
            return this.MeasureReadingAmounts( this.MeasureQueryCommand );
        }

        /// <summary>
        /// Query a measured value from the instrument. Does not use
        /// <see cref="MeasureSubsystemBase.ReadingAmounts"/>.
        /// </summary>
        /// <returns> The reading or none if unknown. </returns>
        public virtual double? MeasurePrimaryReading()
        {
            return this.MeasurePrimaryReading( this.MeasureQueryCommand );
        }

        /// <summary> Estimates the lower bound on measurement time. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <returns> A TimeSpan. </returns>
        public virtual TimeSpan EstimateMeasurementTime()
        {
            if ( !this.PowerLineCycles.HasValue )
                throw new InvalidOperationException( $"{nameof( MultimeterSubsystemBase.PowerLineCycles )} value not set" );
            if ( !this.FilterEnabled.HasValue )
                throw new InvalidOperationException( $"{nameof( MultimeterSubsystemBase.FilterEnabled )} value not set" );
            double aperture = this.PowerLineCycles.Value / this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d );
            double timeSeconds = 0d;
            if ( this.FilterEnabled.Value )
            {
                if ( !this.FilterCount.HasValue )
                    throw new InvalidOperationException( $"{nameof( MultimeterSubsystemBase.FilterCount )} value not set" );
                if ( this.FilterCount.Value > 0 )
                {
                    // if auto zero once is included the time maybe too long
                    timeSeconds = aperture * this.FilterCount.Value;
                }
                else
                {
                    // assumes auto zero
                    timeSeconds = aperture * 2d;
                }
            }

            return TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerSecond * timeSeconds) );
        }

        #endregion

        #region " APERTURE "

        /// <summary> The aperture range. </summary>
        private Core.Primitives.RangeR _ApertureRange;

        /// <summary> The aperture range in seconds. </summary>
        /// <value> The aperture range. </value>
        public Core.Primitives.RangeR ApertureRange
        {
            get => this._ApertureRange;

            set {
                if ( this.ApertureRange != value )
                {
                    this._ApertureRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The Aperture. </summary>
        private double? _Aperture;

        /// <summary>
        /// Gets or sets the cached sense Aperture. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <remarks>
        /// The aperture sets the amount of time the ADC takes when making a measurement, which is the
        /// integration period For the selected measurement Function. The integration period Is specified
        /// In seconds. In general, a short integration period provides a fast reading rate, while a long
        /// integration period provides better accuracy. The selected integration period Is a compromise
        /// between speed And accuracy. During the integration period, If an external trigger With a
        /// count Of 1 Is sent, the trigger Is ignored. If the count Is Set To more than 1, the first
        /// reading Is initialized by this trigger. Subsequent readings occur as rapidly as the
        /// instrument can make them. If a trigger occurs during the group measurement, the trigger Is
        /// latched And another group Of measurements With the same count will be triggered after the
        /// current group completes. You can also Set the integration rate by setting the number Of power
        /// line cycles (NPLCs). Changing the NPLC value changes the aperture time And changing the
        /// aperture time changes the NPLC value.
        /// </remarks>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Aperture
        {
            get => this._Aperture;

            protected set {
                if ( !Nullable.Equals( this.Aperture, value ) )
                {
                    this._Aperture = value;
                    this.PowerLineCycles = value.HasValue ? this.Aperture.Value * this.StatusSubsystem.LineFrequency : new double?();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense Aperture. </summary>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Aperture. </returns>
        public double? ApplyAperture( double value )
        {
            _ = this.WriteAperture( value );
            return this.QueryAperture();
        }

        /// <summary> Gets or sets The Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected virtual string ApertureQueryCommand { get; set; }

        /// <summary> Queries The Aperture. </summary>
        /// <returns> The Aperture or none if unknown. </returns>
        public double? QueryAperture()
        {
            this.Aperture = this.Query( this.Aperture, this.ApertureQueryCommand );
            return this.Aperture;
        }

        /// <summary> Gets or sets The Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected virtual string ApertureCommandFormat { get; set; }

        /// <summary> Writes The Aperture without reading back the value from the device. </summary>
        /// <remarks> This command sets The Aperture. </remarks>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Aperture. </returns>
        public double? WriteAperture( double value )
        {
            this.Aperture = this.Write( value, this.ApertureCommandFormat );
            return this.Aperture;
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Auto Range enabled. </summary>
        private bool? _AutoRangeEnabled;

        /// <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <remarks>
        /// When this command is set to off, you must set the range. If you do not set the range, the
        /// instrument remains at the range that was selected by auto range. When this command Is set to
        /// on, the instrument automatically goes to the most sensitive range to perform the measurement.
        /// If a range Is manually selected through the front panel Or a remote command, this command Is
        /// automatically set to off. Auto range selects the best range In which To measure the signal
        /// that Is applied To the input terminals of the instrument. When auto range Is enabled, the
        /// range increases at 120 percent of range And decreases occurs When the reading Is less than 10
        /// percent Of nominal range. For example, If you are On the 1 volt range And auto range Is
        /// enabled, the instrument auto ranges up To the 10 volt range When the measurement exceeds 1.2
        /// volts. It auto ranges down To the 100 mV range When the measurement falls below 1 volt.
        /// </remarks>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public virtual bool? AutoRangeEnabled
        {
            get => this._AutoRangeEnabled;

            protected set {
                if ( !Equals( this.AutoRangeEnabled, value ) )
                {
                    this._AutoRangeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoRangeEnabled( bool value )
        {
            _ = this.WriteAutoRangeEnabled( value );
            return this.QueryAutoRangeEnabled();
        }

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <remarks> SCPI: ":RANG:AUTO?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="AutoRangeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoRangeEnabled()
        {
            this.AutoRangeEnabled = this.Query( this.AutoRangeEnabled, this.AutoRangeEnabledQueryCommand );
            return this.AutoRangeEnabled;
        }

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <remarks> SCPI: ":RANGE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoRangeEnabled( bool value )
        {
            this.AutoRangeEnabled = this.Write( value, this.AutoRangeEnabledCommandFormat );
            return this.AutoRangeEnabled;
        }

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Auto Zero enabled. </summary>
        private bool? _AutoZeroEnabled;

        /// <summary> Gets or sets the cached Auto Zero Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Zero Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoZeroEnabled
        {
            get => this._AutoZeroEnabled;

            protected set {
                if ( !Equals( this.AutoZeroEnabled, value ) )
                {
                    this._AutoZeroEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Zero Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoZeroEnabled( bool value )
        {
            _ = this.WriteAutoZeroEnabled( value );
            return this.QueryAutoZeroEnabled();
        }

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected virtual string AutoZeroEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Zero Enabled sentinel. Also sets the
        /// <see cref="AutoZeroEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoZeroEnabled()
        {
            this.AutoZeroEnabled = this.Query( this.AutoZeroEnabled, this.AutoZeroEnabledQueryCommand );
            return this.AutoZeroEnabled;
        }

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <remarks> SCPI: ":SENSE:Zero:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Zero enabled query command. </value>
        protected virtual string AutoZeroEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Zero Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoZeroEnabled( bool value )
        {
            this.AutoZeroEnabled = this.Write( value, this.AutoZeroEnabledCommandFormat );
            return this.AutoZeroEnabled;
        }

        #endregion

        #region " AUTO ZERO ONCE "

        /// <summary> Gets or sets the 'automatic zero once' command. </summary>
        /// <value> The 'automatic zero once' command. </value>
        protected virtual string AutoZeroOnceCommand { get; set; }

        /// <summary> Request a single auto zero. </summary>
        public void AutoZeroOnce()
        {
            if ( !string.IsNullOrWhiteSpace( this.AutoZeroOnceCommand ) )
                this.Session.Execute( this.AutoZeroOnceCommand );
        }

        #endregion

        #region " FILTER "

        #region " FILTER COUNT "

        /// <summary> The filter count range. </summary>
        private Core.Primitives.RangeI _FilterCountRange;

        /// <summary> The Filter Count range in seconds. </summary>
        /// <value> The filter count range. </value>
        public Core.Primitives.RangeI FilterCountRange
        {
            get => this._FilterCountRange;

            set {
                if ( this.FilterCountRange != value )
                {
                    this._FilterCountRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The FilterCount. </summary>
        private int? _FilterCount;

        /// <summary>
        /// Gets or sets the cached sense Filter Count. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? FilterCount
        {
            get => this._FilterCount;

            protected set {
                if ( !Nullable.Equals( this.FilterCount, value ) )
                {
                    this._FilterCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense Filter Count. </summary>
        /// <param name="value"> The Filter Count. </param>
        /// <returns> The Filter Count. </returns>
        public int? ApplyFilterCount( int value )
        {
            _ = this.WriteFilterCount( value );
            return this.QueryFilterCount();
        }

        /// <summary> Gets or sets The Filter Count query command. </summary>
        /// <value> The FilterCount query command. </value>
        protected virtual string FilterCountQueryCommand { get; set; }

        /// <summary> Queries The Filter Count. </summary>
        /// <returns> The Filter Count or none if unknown. </returns>
        public int? QueryFilterCount()
        {
            this.FilterCount = this.Query( this.FilterCount, this.FilterCountQueryCommand );
            return this.FilterCount;
        }

        /// <summary> Gets or sets The Filter Count command format. </summary>
        /// <value> The FilterCount command format. </value>
        protected virtual string FilterCountCommandFormat { get; set; }

        /// <summary> Writes The Filter Count without reading back the value from the device. </summary>
        /// <remarks> This command sets The Filter Count. </remarks>
        /// <param name="value"> The Filter Count. </param>
        /// <returns> The Filter Count. </returns>
        public int? WriteFilterCount( int value )
        {
            this.FilterCount = this.Write( value, this.FilterCountCommandFormat );
            return this.FilterCount;
        }

        #endregion

        #region " FILTER ENABLED "

        /// <summary> Filter enabled. </summary>
        private bool? _FilterEnabled;

        /// <summary> Gets or sets the cached Filter Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Filter Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? FilterEnabled
        {
            get => this._FilterEnabled;

            protected set {
                if ( !Equals( this.FilterEnabled, value ) )
                {
                    this._FilterEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Filter Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyFilterEnabled( bool value )
        {
            _ = this.WriteFilterEnabled( value );
            return this.QueryFilterEnabled();
        }

        /// <summary> Gets or sets the Filter enabled query command. </summary>
        /// <remarks> TSP: _G.print(dmm.filter.enable==1) </remarks>
        /// <value> The Filter enabled query command. </value>
        protected virtual string FilterEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Filter Enabled sentinel. Also sets the
        /// <see cref="FilterEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryFilterEnabled()
        {
            this.FilterEnabled = this.Query( this.FilterEnabled, this.FilterEnabledQueryCommand );
            return this.FilterEnabled;
        }

        /// <summary> Gets or sets the Filter enabled command Format. </summary>
        /// <remarks> TSP "dmm.filter.enable={0:'1';'1';'0'}". </remarks>
        /// <value> The Filter enabled query command. </value>
        protected virtual string FilterEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Filter Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteFilterEnabled( bool value )
        {
            this.FilterEnabled = this.Write( value, this.FilterEnabledCommandFormat );
            return this.FilterEnabled;
        }

        #endregion

        #region " MOVING AVERAGE FILTER ENABLED "

        /// <summary> Moving Average Filter enabled. </summary>
        private bool? _MovingAverageFilterEnabled;

        /// <summary> Gets or sets the cached Moving Average Filter Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Moving Average Filter Enabled is not known; <c>True</c> if output is on;
        /// otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? MovingAverageFilterEnabled
        {
            get => this._MovingAverageFilterEnabled;

            protected set {
                if ( !Equals( this.MovingAverageFilterEnabled, value ) )
                {
                    this._MovingAverageFilterEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Moving Average Filter Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyMovingAverageFilterEnabled( bool value )
        {
            _ = this.WriteMovingAverageFilterEnabled( value );
            return this.QueryMovingAverageFilterEnabled();
        }

        /// <summary> Gets or sets the Moving Average Filter enabled query command. </summary>
        /// <remarks> TSP: _G.print(dmm.filter.type=0) </remarks>
        /// <value> The Moving Average Filter enabled query command. </value>
        protected virtual string MovingAverageFilterEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Moving Average Filter Enabled sentinel. Also sets the
        /// <see cref="MovingAverageFilterEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryMovingAverageFilterEnabled()
        {
            this.MovingAverageFilterEnabled = this.Query( this.MovingAverageFilterEnabled, this.MovingAverageFilterEnabledQueryCommand );
            return this.MovingAverageFilterEnabled;
        }

        /// <summary> Gets or sets the Moving Average Filter enabled command Format. </summary>
        /// <remarks> TSP: "dmm.filter.type={0:'0';'0';'1'}". </remarks>
        /// <value> The Moving Average Filter enabled query command. </value>
        protected virtual string MovingAverageFilterEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Moving Average Filter Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteMovingAverageFilterEnabled( bool value )
        {
            this.MovingAverageFilterEnabled = this.Write( value, this.MovingAverageFilterEnabledCommandFormat );
            return this.MovingAverageFilterEnabled;
        }

        #endregion

        #region " FILTER WINDOW "

        /// <summary> The filter window range. </summary>
        private Core.Primitives.RangeR _FilterWindowRange;

        /// <summary> The Filter Window range. </summary>
        /// <value> The filter window range. </value>
        public Core.Primitives.RangeR FilterWindowRange
        {
            get => this._FilterWindowRange;

            set {
                if ( this.FilterWindowRange != value )
                {
                    this._FilterWindowRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The FilterWindow. </summary>
        private double? _FilterWindow;

        /// <summary>
        /// Gets or sets the cached sense Filter Window. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? FilterWindow
        {
            get => this._FilterWindow;

            protected set {
                if ( !Nullable.Equals( this.FilterWindow, value ) )
                {
                    this._FilterWindow = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense Filter Window. </summary>
        /// <param name="value"> The Filter Window. </param>
        /// <returns> The Filter Window. </returns>
        public double? ApplyFilterWindow( double value )
        {
            _ = this.WriteFilterWindow( value );
            return this.QueryFilterWindow();
        }

        /// <summary> Gets The Filter Window query command. </summary>
        /// <value> The FilterWindow query command. </value>
        protected virtual string FilterWindowQueryCommand { get; set; }

        /// <summary> Queries The Filter Window. </summary>
        /// <returns> The Filter Window or none if unknown. </returns>
        public double? QueryFilterWindow()
        {
            var value = this.Query( this.FilterWindow, this.FilterWindowQueryCommand );
            this.FilterWindow = value.HasValue ? 100d * value.Value : new double?();
            return this.FilterWindow;
        }

        /// <summary> Gets The Filter Window command format. </summary>
        /// <value> The FilterWindow command format. </value>
        protected virtual string FilterWindowCommandFormat { get; set; }

        /// <summary> Writes The Filter Window without reading back the value from the device. </summary>
        /// <remarks> This command sets The Filter Window. </remarks>
        /// <param name="value"> The Filter Window. </param>
        /// <returns> The Filter Window. </returns>
        public double? WriteFilterWindow( double value )
        {
            this.FilterWindow = this.Write( 100d * value, this.FilterWindowCommandFormat );
            return this.FilterWindow;
        }

        #endregion

        #endregion

        #region " FRONT TERMINALS SELECTED "

        /// <summary> Gets the front terminal label. </summary>
        /// <value> The front terminal label. </value>
        public string FrontTerminalLabel { get; set; } = "F";

        /// <summary> Gets the rear terminal label. </summary>
        /// <value> The rear terminal label. </value>
        public string RearTerminalLabel { get; set; } = "R";

        /// <summary> Gets the unknown terminal label. </summary>
        /// <value> The unknown terminal label. </value>
        public string UnknownTerminalLabel { get; set; } = string.Empty;

        /// <summary> Gets the terminals caption. </summary>
        /// <value> The terminals caption. </value>
        public string TerminalsCaption => this.FrontTerminalsSelected.HasValue ? this.FrontTerminalsSelected.Value ? this.FrontTerminalLabel : this.RearTerminalLabel : this.UnknownTerminalLabel;

        /// <summary> Gets true if the subsystem supports front terminals selection query. </summary>
        /// <value>
        /// The value indicating if the subsystem supports front terminals selection query.
        /// </value>
        public bool SupportsFrontTerminalsSelectionQuery => !string.IsNullOrWhiteSpace( this.FrontTerminalsSelectedQueryCommand );

        /// <summary> Front Terminals Selected. </summary>
        private bool? _FrontTerminalsSelected;

        /// <summary> Gets or sets the cached Front Terminals Selected sentinel. </summary>
        /// <value>
        /// <c>null</c> if Front Terminals Selected is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? FrontTerminalsSelected
        {
            get => this._FrontTerminalsSelected;

            protected set {
                if ( !Equals( this.FrontTerminalsSelected, value ) )
                {
                    this._FrontTerminalsSelected = value;
                    this.NotifyPropertyChanged();
                    this.NotifyPropertyChanged( nameof( MultimeterSubsystemBase.TerminalsCaption ) );
                }
            }
        }

        /// <summary> Writes and reads back the Front Terminals Selected sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyFrontTerminalsSelected( bool value )
        {
            _ = this.WriteFrontTerminalsSelected( value );
            return this.QueryFrontTerminalsSelected();
        }

        /// <summary> Gets or sets the front terminals selected query command. </summary>
        /// <value> The front terminals selected query command. </value>
        protected virtual string FrontTerminalsSelectedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Front Terminals Selected sentinel. Also sets the
        /// <see cref="FrontTerminalsSelected">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryFrontTerminalsSelected()
        {
            this.FrontTerminalsSelected = this.Query( this.FrontTerminalsSelected, this.FrontTerminalsSelectedQueryCommand );
            return this.FrontTerminalsSelected;
        }

        /// <summary> Gets or sets the front terminals selected command format. </summary>
        /// <value> The front terminals selected command format. </value>
        protected virtual string FrontTerminalsSelectedCommandFormat { get; set; }

        /// <summary>
        /// Writes the Front Terminals Selected sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteFrontTerminalsSelected( bool value )
        {
            this.FrontTerminalsSelected = this.Write( value, this.FrontTerminalsSelectedCommandFormat );
            return this.FrontTerminalsSelected;
        }

        #endregion

        #region " LIMIT 1 "

        #region " LIMIT1 AUTO CLEAR "

        /// <summary> Limit1 Auto Clear. </summary>
        private bool? _Limit1AutoClear;

        /// <summary> Gets or sets the cached Limit1 Auto Clear sentinel. </summary>
        /// <remarks>
        /// When auto clear is set to on for a measure function, limit conditions are cleared
        /// automatically after each measurement. If you are making a series of measurements, the
        /// instrument shows the limit test result of the last measurement for the pass Or fail
        /// indication for the limit. If you want To know If any Of a series Of measurements failed the
        /// limit, Set the auto clear setting To off. When this set to off, a failed indication Is Not
        /// cleared automatically. It remains set until it Is cleared With the clear command. The auto
        /// clear setting affects both the high And low limits.
        /// </remarks>
        /// <value>
        /// <c>null</c> if Limit1 Auto Clear is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit1AutoClear
        {
            get => this._Limit1AutoClear;

            protected set {
                if ( !Equals( this.Limit1AutoClear, value ) )
                {
                    this._Limit1AutoClear = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Auto Clear sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit1AutoClear( bool value )
        {
            _ = this.WriteLimit1AutoClear( value );
            return this.QueryLimit1AutoClear();
        }

        /// <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
        /// <remarks> TSP: _G.print(_G.dmm.measure.limit1.autoclear==dmm.ON) </remarks>
        /// <value> The Limit1 Auto Clear query command. </value>
        protected virtual string Limit1AutoClearQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit1 Auto Clear sentinel. Also sets the
        /// <see cref="Limit1AutoClear">AutoClear</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? QueryLimit1AutoClear()
        {
            this.Limit1AutoClear = this.Query( this.Limit1AutoClear, this.Limit1AutoClearQueryCommand );
            return this.Limit1AutoClear;
        }

        /// <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
        /// <remarks> TSP: "_G.dmm.measure.limit1.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}". </remarks>
        /// <value> The Limit1 Auto Clear query command. </value>
        protected virtual string Limit1AutoClearCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit1 Auto Clear sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is Auto Clear. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? WriteLimit1AutoClear( bool value )
        {
            this.Limit1AutoClear = this.Write( value, this.Limit1AutoClearCommandFormat );
            return this.Limit1AutoClear;
        }

        #endregion

        #region " LIMIT1 ENABLED "

        /// <summary> Limit1 enabled. </summary>
        private bool? _Limit1Enabled;

        /// <summary> Gets or sets the cached Limit1 Enabled sentinel. </summary>
        /// <remarks>
        /// This command enables or disables a limit test for the selected measurement function. When
        /// this attribute Is enabled, the limit 1 testing occurs on each measurement made by the
        /// instrument. Limit 1 testing compares the measurements To the high And low limit values. If a
        /// measurement falls outside these limits, the test fails.
        /// </remarks>
        /// <value>
        /// <c>null</c> if Limit1 Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit1Enabled
        {
            get => this._Limit1Enabled;

            protected set {
                if ( !Equals( this.Limit1Enabled, value ) )
                {
                    this._Limit1Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit1Enabled( bool value )
        {
            _ = this.WriteLimit1Enabled( value );
            return this.QueryLimit1Enabled();
        }

        /// <summary> Gets or sets the Limit1 enabled query command. </summary>
        /// <remarks> TSP: _G.print(_G.dmm.measure.limit1.autoclear==dmm.ON) </remarks>
        /// <value> The Limit1 enabled query command. </value>
        protected virtual string Limit1EnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit1 Enabled sentinel. Also sets the
        /// <see cref="Limit1Enabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryLimit1Enabled()
        {
            this.Limit1Enabled = this.Query( this.Limit1Enabled, this.Limit1EnabledQueryCommand );
            return this.Limit1Enabled;
        }

        /// <summary> Gets or sets the Limit1 enabled command Format. </summary>
        /// <remarks> TSP _G.dmm.measure.limit1.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'} </remarks>
        /// <value> The Limit1 enabled query command. </value>
        protected virtual string Limit1EnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit1 Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteLimit1Enabled( bool value )
        {
            this.Limit1Enabled = this.Write( value, this.Limit1EnabledCommandFormat );
            return this.Limit1Enabled;
        }

        #endregion

        #region " LIMIT1 LOWER LEVEL "

        /// <summary> The Limit1 Lower Level. </summary>
        private double? _Limit1LowerLevel;

        /// <summary>
        /// Gets or sets the cached Limit1 Lower Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <remarks>
        /// This command sets the lower limit for the limit 1 test for the selected measure function.
        /// When limit 1 testing Is enabled, this causes a fail indication to occur when the measurement
        /// value Is less than this value.  Default Is 0.3 For limit 1 When the diode Function Is
        /// selected. The Default For limit 2 For the diode Function is() –1.
        /// </remarks>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit1LowerLevel
        {
            get => this._Limit1LowerLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit1LowerLevel, value ) )
                {
                    this._Limit1LowerLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Lower Level. </summary>
        /// <param name="value"> The Limit1 Lower Level. </param>
        /// <returns> The Limit1 Lower Level. </returns>
        public double? ApplyLimit1LowerLevel( double value )
        {
            _ = this.WriteLimit1LowerLevel( value );
            return this.QueryLimit1LowerLevel();
        }

        /// <summary> Gets or sets The Limit1 Lower Level query command. </summary>
        /// <value> The Limit1 Lower Level query command. </value>
        protected virtual string Limit1LowerLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit1 Lower Level. </summary>
        /// <returns> The Limit1 Lower Level or none if unknown. </returns>
        public double? QueryLimit1LowerLevel()
        {
            this.Limit1LowerLevel = this.Query( this.Limit1LowerLevel, this.Limit1LowerLevelQueryCommand );
            return this.Limit1LowerLevel;
        }

        /// <summary> Gets or sets The Limit1 Lower Level command format. </summary>
        /// <value> The Limit1 Lower Level command format. </value>
        protected virtual string Limit1LowerLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit1 Lower Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit1 Lower Level. </remarks>
        /// <param name="value"> The Limit1 Lower Level. </param>
        /// <returns> The Limit1 Lower Level. </returns>
        public double? WriteLimit1LowerLevel( double value )
        {
            this.Limit1LowerLevel = this.Write( value, this.Limit1LowerLevelCommandFormat );
            return this.Limit1LowerLevel;
        }

        #endregion

        #region " LIMIT1 UPPER LEVEL "

        /// <summary> The Limit1 Upper Level. </summary>
        private double? _Limit1UpperLevel;

        /// <summary>
        /// Gets or sets the cached Limit1 Upper Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <remarks>
        /// This command sets the high limit for the limit 2 test for the selected measurement function.
        /// When limit 2 testing Is enabled, the instrument generates a fail indication When the
        /// measurement value Is more than this value. Default Is 0.8 For limit 1 When the diode Function
        /// Is selected; 10 When the continuity Function Is selected. The default for limit 2 for the
        /// diode And continuity functions Is 1.
        /// </remarks>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit1UpperLevel
        {
            get => this._Limit1UpperLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit1UpperLevel, value ) )
                {
                    this._Limit1UpperLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit1 Upper Level. </summary>
        /// <param name="value"> The Limit1 Upper Level. </param>
        /// <returns> The Limit1 Upper Level. </returns>
        public double? ApplyLimit1UpperLevel( double value )
        {
            _ = this.WriteLimit1UpperLevel( value );
            return this.QueryLimit1UpperLevel();
        }

        /// <summary> Gets or sets The Limit1 Upper Level query command. </summary>
        /// <value> The Limit1 Upper Level query command. </value>
        protected virtual string Limit1UpperLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit1 Upper Level. </summary>
        /// <returns> The Limit1 Upper Level or none if unknown. </returns>
        public double? QueryLimit1UpperLevel()
        {
            this.Limit1UpperLevel = this.Query( this.Limit1UpperLevel, this.Limit1UpperLevelQueryCommand );
            return this.Limit1UpperLevel;
        }

        /// <summary> Gets or sets The Limit1 Upper Level command format. </summary>
        /// <value> The Limit1 Upper Level command format. </value>
        protected virtual string Limit1UpperLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit1 Upper Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit1 Upper Level. </remarks>
        /// <param name="value"> The Limit1 Upper Level. </param>
        /// <returns> The Limit1 Upper Level. </returns>
        public double? WriteLimit1UpperLevel( double value )
        {
            this.Limit1UpperLevel = this.Write( value, this.Limit1UpperLevelCommandFormat );
            return this.Limit1UpperLevel;
        }

        #endregion

        #endregion

        #region " LIMIT 2 "

        #region " LIMIT2 AUTO CLEAR "

        /// <summary> Limit2 Auto Clear. </summary>
        private bool? _Limit2AutoClear;

        /// <summary> Gets or sets the cached Limit2 Auto Clear sentinel. </summary>
        /// <remarks>
        /// When auto clear is set to on for a measure function, limit conditions are cleared
        /// automatically after each measurement. If you are making a series of measurements, the
        /// instrument shows the limit test result of the last measurement for the pass Or fail
        /// indication for the limit. If you want To know If any Of a series Of measurements failed the
        /// limit, Set the auto clear setting To off. When this set to off, a failed indication Is Not
        /// cleared automatically. It remains set until it Is cleared With the clear command. The auto
        /// clear setting affects both the high And low limits.
        /// </remarks>
        /// <value>
        /// <c>null</c> if Limit2 Auto Clear is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit2AutoClear
        {
            get => this._Limit2AutoClear;

            protected set {
                if ( !Equals( this.Limit2AutoClear, value ) )
                {
                    this._Limit2AutoClear = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Auto Clear sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit2AutoClear( bool value )
        {
            _ = this.WriteLimit2AutoClear( value );
            return this.QueryLimit2AutoClear();
        }

        /// <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
        /// <remarks> TSP: _G.print(_G.dmm.measure.limit1.autoclear==dmm.ON) </remarks>
        /// <value> The Limit2 Auto Clear query command. </value>
        protected virtual string Limit2AutoClearQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit2 Auto Clear sentinel. Also sets the
        /// <see cref="Limit2AutoClear">AutoClear</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? QueryLimit2AutoClear()
        {
            this.Limit2AutoClear = this.Query( this.Limit2AutoClear, this.Limit2AutoClearQueryCommand );
            return this.Limit2AutoClear;
        }

        /// <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
        /// <remarks> TSP: "_G.dmm.measure.limit1.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}". </remarks>
        /// <value> The Limit2 Auto Clear query command. </value>
        protected virtual string Limit2AutoClearCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit2 Auto Clear sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is Auto Clear. </param>
        /// <returns> <c>True</c> if AutoClear; otherwise <c>False</c>. </returns>
        public bool? WriteLimit2AutoClear( bool value )
        {
            this.Limit2AutoClear = this.Write( value, this.Limit2AutoClearCommandFormat );
            return this.Limit2AutoClear;
        }

        #endregion

        #region " LIMIT2 ENABLED "

        /// <summary> Limit2 enabled. </summary>
        private bool? _Limit2Enabled;

        /// <summary> Gets or sets the cached Limit2 Enabled sentinel. </summary>
        /// <remarks>
        /// This command enables or disables a limit test for the selected measurement function. When
        /// this attribute Is enabled, the limit 2 testing occurs on each measurement made by the
        /// instrument. Limit 2 testing compares the measurements To the high And low limit values. If a
        /// measurement falls outside these limits, the test fails.
        /// </remarks>
        /// <value>
        /// <c>null</c> if Limit2 Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? Limit2Enabled
        {
            get => this._Limit2Enabled;

            protected set {
                if ( !Equals( this.Limit2Enabled, value ) )
                {
                    this._Limit2Enabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyLimit2Enabled( bool value )
        {
            _ = this.WriteLimit2Enabled( value );
            return this.QueryLimit2Enabled();
        }

        /// <summary> Gets or sets the Limit2 enabled query command. </summary>
        /// <remarks> TSP: _G.print(_G.dmm.measure.limit2.autoclear==dmm.ON) </remarks>
        /// <value> The Limit2 enabled query command. </value>
        protected virtual string Limit2EnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit2 Enabled sentinel. Also sets the
        /// <see cref="Limit2Enabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryLimit2Enabled()
        {
            this.Limit2Enabled = this.Query( this.Limit2Enabled, this.Limit2EnabledQueryCommand );
            return this.Limit2Enabled;
        }

        /// <summary> Gets or sets the Limit2 enabled command Format. </summary>
        /// <remarks> TSP: _G.dmm.measure.limit2.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'} </remarks>
        /// <value> The Limit2 enabled query command. </value>
        protected virtual string Limit2EnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Limit2 Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteLimit2Enabled( bool value )
        {
            this.Limit2Enabled = this.Write( value, this.Limit2EnabledCommandFormat );
            return this.Limit2Enabled;
        }

        #endregion

        #region " LIMIT2 LOWER LEVEL "

        /// <summary> The Limit2 Lower Level. </summary>
        private double? _Limit2LowerLevel;

        /// <summary>
        /// Gets or sets the cached Limit2 Lower Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <remarks>
        /// This command sets the lower limit for the limit 1 test for the selected measure function.
        /// When limit 1 testing Is enabled, this causes a fail indication to occur when the measurement
        /// value Is less than this value.  Default Is 0.3 For limit 1 When the diode Function Is
        /// selected. The Default For limit 2 For the diode Function is() –1.
        /// </remarks>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit2LowerLevel
        {
            get => this._Limit2LowerLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit2LowerLevel, value ) )
                {
                    this._Limit2LowerLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Lower Level. </summary>
        /// <param name="value"> The Limit2 Lower Level. </param>
        /// <returns> The Limit2 Lower Level. </returns>
        public double? ApplyLimit2LowerLevel( double value )
        {
            _ = this.WriteLimit2LowerLevel( value );
            return this.QueryLimit2LowerLevel();
        }

        /// <summary> Gets or sets The Limit2 Lower Level query command. </summary>
        /// <value> The Limit2 Lower Level query command. </value>
        protected virtual string Limit2LowerLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit2 Lower Level. </summary>
        /// <returns> The Limit2 Lower Level or none if unknown. </returns>
        public double? QueryLimit2LowerLevel()
        {
            this.Limit2LowerLevel = this.Query( this.Limit2LowerLevel, this.Limit2LowerLevelQueryCommand );
            return this.Limit2LowerLevel;
        }

        /// <summary> Gets or sets The Limit2 Lower Level command format. </summary>
        /// <value> The Limit2 Lower Level command format. </value>
        protected virtual string Limit2LowerLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit2 Lower Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit2 Lower Level. </remarks>
        /// <param name="value"> The Limit2 Lower Level. </param>
        /// <returns> The Limit2 Lower Level. </returns>
        public double? WriteLimit2LowerLevel( double value )
        {
            this.Limit2LowerLevel = this.Write( value, this.Limit2LowerLevelCommandFormat );
            return this.Limit2LowerLevel;
        }

        #endregion

        #region " LIMIT2 UPPER LEVEL "

        /// <summary> The Limit2 Upper Level. </summary>
        private double? _Limit2UpperLevel;

        /// <summary>
        /// Gets or sets the cached Limit2 Upper Level. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <remarks>
        /// This command sets the high limit for the limit 2 test for the selected measurement function.
        /// When limit
        /// 2 testing Is enabled, the instrument generates a fail indication When the measurement value
        /// Is more than this value. Default Is 0.8 For limit 1 When the diode Function Is selected; 10
        /// When the continuity Function Is selected. The default for limit 2 for the diode And
        /// continuity functions Is 1.
        /// </remarks>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit2UpperLevel
        {
            get => this._Limit2UpperLevel;

            protected set {
                if ( !Nullable.Equals( this.Limit2UpperLevel, value ) )
                {
                    this._Limit2UpperLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Limit2 Upper Level. </summary>
        /// <param name="value"> The Limit2 Upper Level. </param>
        /// <returns> The Limit2 Upper Level. </returns>
        public double? ApplyLimit2UpperLevel( double value )
        {
            _ = this.WriteLimit2UpperLevel( value );
            return this.QueryLimit2UpperLevel();
        }

        /// <summary> Gets or sets The Limit2 Upper Level query command. </summary>
        /// <value> The Limit2 Upper Level query command. </value>
        protected virtual string Limit2UpperLevelQueryCommand { get; set; }

        /// <summary> Queries The Limit2 Upper Level. </summary>
        /// <returns> The Limit2 Upper Level or none if unknown. </returns>
        public double? QueryLimit2UpperLevel()
        {
            this.Limit2UpperLevel = this.Query( this.Limit2UpperLevel, this.Limit2UpperLevelQueryCommand );
            return this.Limit2UpperLevel;
        }

        /// <summary> Gets or sets The Limit2 Upper Level command format. </summary>
        /// <value> The Limit2 Upper Level command format. </value>
        protected virtual string Limit2UpperLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes The Limit2 Upper Level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Limit2 Upper Level. </remarks>
        /// <param name="value"> The Limit2 Upper Level. </param>
        /// <returns> The Limit2 Upper Level. </returns>
        public double? WriteLimit2UpperLevel( double value )
        {
            this.Limit2UpperLevel = this.Write( value, this.Limit2UpperLevelCommandFormat );
            return this.Limit2UpperLevel;
        }

        #endregion

        #endregion

        #region " MEASURE UNIT "

        /// <summary> Gets or sets the default measurement unit. </summary>
        /// <value> The default measure unit. </value>
        public Arebis.TypedUnits.Unit DefaultMeasurementUnit { get; set; }

        /// <summary> Gets or sets the function unit. </summary>
        /// <value> The function unit. </value>
        public Arebis.TypedUnits.Unit MeasurementUnit
        {
            get => this.PrimaryReading.Amount.Unit;

            set {
                if ( this.MeasurementUnit != value )
                {
                    this.PrimaryReading.ApplyUnit( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " OPEN DETECTOR ENABLED "

        /// <summary> Gets or sets a list of states of the open detector knowns. </summary>
        /// <value> The open detector known states. </value>
        public BooleanDictionary OpenDetectorKnownStates { get; private set; }

        /// <summary> Open Detector enabled. </summary>
        private bool? _OpenDetectorEnabled;

        /// <summary> Gets or sets the cached Open Detector Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Open Detector Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? OpenDetectorEnabled
        {
            get => this._OpenDetectorEnabled;

            protected set {
                if ( !Equals( this.OpenDetectorEnabled, value ) )
                {
                    this._OpenDetectorEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Open Detector Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyOpenDetectorEnabled( bool value )
        {
            _ = this.WriteOpenDetectorEnabled( value );
            return this.QueryOpenDetectorEnabled();
        }

        /// <summary> Gets the automatic Zero enabled query command. </summary>
        /// <remarks> TSP: _G.print(_G.dmm.opendetector==1) </remarks>
        /// <value> The automatic Zero enabled query command. </value>
        protected virtual string OpenDetectorEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Open Detector Enabled sentinel. Also sets the
        /// <see cref="OpenDetectorEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryOpenDetectorEnabled()
        {
            this.OpenDetectorEnabled = this.Query( this.OpenDetectorEnabled, this.OpenDetectorEnabledQueryCommand );
            return this.OpenDetectorEnabled;
        }

        /// <summary> Gets the automatic Zero enabled command Format. </summary>
        /// <remarks> TSP: _G.opendetector={0:'1';'1';'0'}". </remarks>
        /// <value> The automatic Zero enabled query command. </value>
        protected virtual string OpenDetectorEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Open Detector Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteOpenDetectorEnabled( bool value )
        {
            this.OpenDetectorEnabled = this.Write( value, this.OpenDetectorEnabledCommandFormat );
            return this.OpenDetectorEnabled;
        }

        #endregion

        #region " POWER LINE CYCLES (NPLC) "

        /// <summary> Gets the power line cycles decimal places. </summary>
        /// <value> The power line decimal places. </value>
        public int PowerLineCyclesDecimalPlaces => ( int ) Math.Max( 0d, 1d - Math.Log10( this.PowerLineCyclesRange.Min ) );

        /// <summary> The power line cycles range. </summary>
        private Core.Primitives.RangeR _PowerLineCyclesRange;

        /// <summary> The power line cycles range in units. </summary>
        /// <value> The power line cycles range. </value>
        public Core.Primitives.RangeR PowerLineCyclesRange
        {
            get => this._PowerLineCyclesRange;

            set {
                if ( this.PowerLineCyclesRange != value )
                {
                    this._PowerLineCyclesRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The Power Line Cycles. </summary>
        private double? _PowerLineCycles;

        /// <summary>
        /// Gets or sets the cached sense PowerLineCycles. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? PowerLineCycles
        {
            get => this._PowerLineCycles;

            protected set {
                if ( !Nullable.Equals( this.PowerLineCycles, value ) )
                {
                    this._PowerLineCycles = value;
                    this.Aperture = StatusSubsystemBase.FromPowerLineCycles( this._PowerLineCycles.Value ).TotalSeconds;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense PowerLineCycles. </summary>
        /// <param name="value"> The Power Line Cycles. </param>
        /// <returns> The Power Line Cycles. </returns>
        public double? ApplyPowerLineCycles( double value )
        {
            _ = this.WritePowerLineCycles( value );
            return this.QueryPowerLineCycles();
        }

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected virtual string PowerLineCyclesQueryCommand { get; set; }

        /// <summary> Queries The Power Line Cycles. </summary>
        /// <returns> The Power Line Cycles or none if unknown. </returns>
        public double? QueryPowerLineCycles()
        {
            this.PowerLineCycles = this.Query( this.PowerLineCycles, this.PowerLineCyclesQueryCommand );
            return this.PowerLineCycles;
        }

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected virtual string PowerLineCyclesCommandFormat { get; set; }

        /// <summary>
        /// Writes The Power Line Cycles without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Power Line Cycles. </remarks>
        /// <param name="value"> The Power Line Cycles. </param>
        /// <returns> The Power Line Cycles. </returns>
        public double? WritePowerLineCycles( double value )
        {
            this.PowerLineCycles = this.Write( value, this.PowerLineCyclesCommandFormat );
            return this.PowerLineCycles;
        }

        #endregion

        #region " RANGE "

        /// <summary> The Range. </summary>
        private double? _Range;

        /// <summary>
        /// Gets or sets the cached sense Range. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Range
        {
            get => this._Range;

            protected set {
                if ( !Nullable.Equals( this.Range, value ) )
                {
                    this._Range = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense Range. </summary>
        /// <param name="value"> The Range. </param>
        /// <returns> The Range. </returns>
        public double? ApplyRange( double value )
        {
            _ = this.WriteRange( value );
            return this.QueryRange();
        }

        /// <summary> Gets or sets The Range query command. </summary>
        /// <value> The Range query command. </value>
        protected virtual string RangeQueryCommand { get; set; }

        /// <summary> Queries The Range. </summary>
        /// <returns> The Range or none if unknown. </returns>
        public double? QueryRange()
        {
            this.Range = this.Query( this.Range, this.RangeQueryCommand );
            return this.Range;
        }

        /// <summary> Gets or sets The Range command format. </summary>
        /// <value> The Range command format. </value>
        protected virtual string RangeCommandFormat { get; set; }

        /// <summary> Writes The Range without reading back the value from the device. </summary>
        /// <remarks> This command sets The Range. </remarks>
        /// <param name="value"> The Range. </param>
        /// <returns> The Range. </returns>
        public double? WriteRange( double value )
        {
            this.Range = this.Write( value, this.RangeCommandFormat );
            return this.Range;
        }

        #endregion

        #region " REMOTE SENSE SELECTED "

        /// <summary> Remote Sense Selected. </summary>
        private bool? _RemoteSenseSelected;

        /// <summary> Gets or sets the cached Remote Sense Selected sentinel. </summary>
        /// <value>
        /// <c>null</c> if Remote Sense Selected is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? RemoteSenseSelected
        {
            get => this._RemoteSenseSelected;

            protected set {
                if ( !Equals( this.RemoteSenseSelected, value ) )
                {
                    this._RemoteSenseSelected = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Remote Sense Selected sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyRemoteSenseSelected( bool value )
        {
            _ = this.WriteRemoteSenseSelected( value );
            return this.QueryRemoteSenseSelected();
        }

        /// <summary> Gets or sets the remote sense selected query command. </summary>
        /// <value> The remote sense selected query command. </value>
        protected virtual string RemoteSenseSelectedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Remote Sense Selected sentinel. Also sets the
        /// <see cref="RemoteSenseSelected">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryRemoteSenseSelected()
        {
            this.RemoteSenseSelected = this.Query( this.RemoteSenseSelected, this.RemoteSenseSelectedQueryCommand );
            return this.RemoteSenseSelected;
        }

        /// <summary> Gets or sets the remote sense selected command format. </summary>
        /// <value> The remote sense selected command format. </value>
        protected virtual string RemoteSenseSelectedCommandFormat { get; set; }

        /// <summary>
        /// Writes the Remote Sense Selected sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteRemoteSenseSelected( bool value )
        {
            this.RemoteSenseSelected = this.Write( value, this.RemoteSenseSelectedCommandFormat );
            return this.RemoteSenseSelected;
        }

        #endregion

    }
}
