using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI
{
    public abstract partial class MeasureSubsystemBase
    {

        #region " RANGE "

        /// <summary> Define function mode ranges. </summary>
        private void DefineFunctionModeRanges()
        {
            this.FunctionModeRanges = new RangeDictionary();
            SenseSubsystemBase.DefineFunctionModeRanges( this.FunctionModeRanges, this.DefaultFunctionRange );
        }

        /// <summary> Gets or sets the function mode ranges. </summary>
        /// <value> The function mode ranges. </value>
        public RangeDictionary FunctionModeRanges { get; private set; }

        /// <summary> Gets or sets the default function range. </summary>
        /// <value> The default function range. </value>
        public Core.Primitives.RangeR DefaultFunctionRange { get; set; }

        /// <summary> Converts a functionMode to a range. </summary>
        /// <param name="functionMode"> The function mode. </param>
        /// <returns> FunctionMode as an isr.Core.Primitives.RangeR. </returns>
        public virtual Core.Primitives.RangeR ToRange( int functionMode )
        {
            return this.FunctionModeRanges[functionMode];
        }

        /// <summary> The function range. </summary>
        private Core.Primitives.RangeR _FunctionRange;

        /// <summary> The Range of the range. </summary>
        /// <value> The function range. </value>
        public Core.Primitives.RangeR FunctionRange
        {
            get => this._FunctionRange;

            set {
                if ( this.FunctionRange != value )
                {
                    this._FunctionRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " DECIMAL PLACES "

        /// <summary> Gets or sets the default decimal places. </summary>
        /// <value> The default decimal places. </value>
        public int DefaultFunctionModeDecimalPlaces { get; set; } = 3;

        /// <summary> Define function mode decimal places. </summary>
        private void DefineFunctionModeDecimalPlaces()
        {
            this.FunctionModeDecimalPlaces = new IntegerDictionary();
            SenseSubsystemBase.DefineFunctionModeDecimalPlaces( this.FunctionModeDecimalPlaces, this.DefaultFunctionModeDecimalPlaces );
        }

        /// <summary> Gets or sets the function mode decimal places. </summary>
        /// <value> The function mode decimal places. </value>
        public IntegerDictionary FunctionModeDecimalPlaces { get; private set; }

        /// <summary> Converts a function Mode to a decimal places. </summary>
        /// <param name="functionMode"> The function mode. </param>
        /// <returns> FunctionMode as an Integer. </returns>
        public virtual int ToDecimalPlaces( int functionMode )
        {
            return this.FunctionModeDecimalPlaces[functionMode];
        }

        /// <summary> The function range decimal places. </summary>
        private int _FunctionRangeDecimalPlaces;

        /// <summary> Gets or sets the function range decimal places. </summary>
        /// <value> The function range decimal places. </value>
        public int FunctionRangeDecimalPlaces
        {
            get => this._FunctionRangeDecimalPlaces;

            set {
                if ( this.FunctionRangeDecimalPlaces != value )
                {
                    this._FunctionRangeDecimalPlaces = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " UNITS "

        /// <summary> Gets or sets the default unit. </summary>
        /// <value> The default unit. </value>
        public Arebis.TypedUnits.Unit DefaultFunctionUnit { get; set; } = Arebis.StandardUnits.ElectricUnits.Volt;

        /// <summary> Defines function mode units. </summary>
        private void DefineFunctionModeUnits()
        {
            this.FunctionModeUnits = new UnitDictionary();
            SenseSubsystemBase.DefineFunctionModeUnits( this.FunctionModeUnits );
        }

        /// <summary> Gets or sets the function mode decimal places. </summary>
        /// <value> The function mode decimal places. </value>
        public UnitDictionary FunctionModeUnits { get; private set; }

        /// <summary> Parse units. </summary>
        /// <param name="functionMode"> The  Multimeter Function Mode. </param>
        /// <returns> An Arebis.TypedUnits.Unit. </returns>
        public virtual Arebis.TypedUnits.Unit ToUnit( int functionMode )
        {
            return this.FunctionModeUnits[functionMode];
        }

        /// <summary> Gets or sets the function unit. </summary>
        /// <value> The function unit. </value>
        public Arebis.TypedUnits.Unit FunctionUnit
        {
            get => this.PrimaryReading.Amount.Unit;

            set {
                if ( this.FunctionUnit != value )
                {
                    this.PrimaryReading.ApplyUnit( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Define function clear known state. </summary>
        protected virtual void DefineFunctionClearKnownState()
        {
            if ( this.ReadingAmounts.HasReadingElements() )
            {
                this.ReadingAmounts.ActiveReadingAmount().ApplyUnit( this.FunctionUnit );
            }

            this.ReadingAmounts.Reset();
            _ = this.ParsePrimaryReading( string.Empty );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( this.FunctionUnit );
            _ = this.ReadingAmounts.TryParse( string.Empty );
            this.NotifyPropertyChanged( nameof( this.ReadingAmounts ) );
        }

        /// <summary> Define function mode read writes. </summary>
        private void DefineFunctionModeReadWrites()
        {
            this.FunctionModeReadWrites = new Pith.EnumReadWriteCollection();
            SenseSubsystemBase.DefineFunctionModeReadWrites( this.FunctionModeReadWrites );
        }

        /// <summary> Define function mode read writes. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="readValueDecorator">  The read value decorator. e.g., """{0}""". </param>
        /// <param name="writeValueDecorator"> The write value decorator, e.g., "'{0}'". </param>
        public void DefineFunctionModeReadWrites( string readValueDecorator, string writeValueDecorator )
        {
            this.FunctionModeReadWrites = new Pith.EnumReadWriteCollection() {
                ReadValueDecorator = readValueDecorator,
                WriteValueDecorator = writeValueDecorator
            };
            SenseSubsystemBase.DefineFunctionModeReadWrites( this.FunctionModeReadWrites );
        }

        /// <summary> Gets or sets a dictionary of Sense function mode parses. </summary>
        /// <value> A Dictionary of Sense function mode parses. </value>
        public Pith.EnumReadWriteCollection FunctionModeReadWrites { get; private set; }

        /// <summary> The supported function modes. </summary>
        private SenseFunctionModes _SupportedFunctionModes;

        /// <summary>
        /// Gets or sets the supported Function Modes. This is a subset of the functions supported by the
        /// instrument.
        /// </summary>
        /// <value> The supported Sense function modes. </value>
        public SenseFunctionModes SupportedFunctionModes
        {
            get => this._SupportedFunctionModes;

            set {
                if ( !this.SupportedFunctionModes.Equals( value ) )
                {
                    this._SupportedFunctionModes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The function mode. </summary>
        private SenseFunctionModes? _FunctionMode;

        /// <summary> Gets or sets the cached Sense function mode. </summary>
        /// <value>
        /// The <see cref="FunctionMode">Sense function mode</see> or none if not set or unknown.
        /// </value>
        public SenseFunctionModes? FunctionMode
        {
            get => this._FunctionMode;

            set {
                if ( !this.FunctionMode.Equals( value ) )
                {
                    this._FunctionMode = value;
                    if ( value.HasValue )
                    {
                        this.FunctionRange = this.ToRange( ( int ) value.Value );
                        this.FunctionUnit = this.ToUnit( ( int ) value.Value );
                        this.FunctionRangeDecimalPlaces = this.ToDecimalPlaces( ( int ) value.Value );
                    }
                    else
                    {
                        this.FunctionRange = this.DefaultFunctionRange;
                        this.FunctionUnit = this.DefaultFunctionUnit;
                        this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
                    }

                    this.DefineFunctionClearKnownState();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Sense function mode. </summary>
        /// <param name="value"> The  Sense function mode. </param>
        /// <returns>
        /// The <see cref="FunctionMode">source Sense function mode</see> or none if unknown.
        /// </returns>
        public virtual SenseFunctionModes? ApplyFunctionMode( SenseFunctionModes value )
        {
            _ = this.WriteFunctionMode( value );
            return this.QueryFunctionMode();
        }

        /// <summary> Gets or sets the Sense function mode query command. </summary>
        /// <value> The Sense function mode query command. </value>
        protected abstract string FunctionModeQueryCommand { get; set; }

        /// <summary> Queries the Sense function mode. </summary>
        /// <returns> The <see cref="FunctionMode">Sense function mode</see> or none if unknown. </returns>
        public virtual SenseFunctionModes? QueryFunctionMode()
        {
            return this.QueryFunctionMode( this.FunctionModeQueryCommand );
        }

        /// <summary> Queries the Sense function mode. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns> The <see cref="FunctionMode">Sense function mode</see> or none if unknown. </returns>
        public virtual SenseFunctionModes? QueryFunctionMode( string queryCommand )
        {
            this.FunctionMode = this.Query( queryCommand, this.FunctionMode.GetValueOrDefault( SenseFunctionModes.None ), this.FunctionModeReadWrites );
            return this.FunctionMode;
        }

        /// <summary>
        /// Queries the Sense Function Mode. Also sets the <see cref="FunctionMode"></see> cached value.
        /// </summary>
        /// <returns> The Sense Function Mode or null if unknown. </returns>
        [Obsolete( "Now using double quests for both read and writes" )]
        public SenseFunctionModes? QueryFunctionModeTrimQuotes()
        {
            // the instrument expects single quotes when writing the value but sends back items delimited with double quotes.
            string reading = this.Session.QueryTrimEnd( this.FunctionModeQueryCommand ).Trim( '"' );
            long v = this.Session.Parse( this.FunctionModeReadWrites, reading );
            this.FunctionMode = Enum.IsDefined( typeof( SenseFunctionModes ), v ) ? ( SenseFunctionModes ) Conversions.ToInteger( Enum.ToObject( typeof( SenseFunctionModes ), v ) ) : new SenseFunctionModes?();
            return this.FunctionMode;
        }

        /// <summary> Gets or sets the Sense function mode command format. </summary>
        /// <value> The Sense function mode command format. </value>
        protected abstract string FunctionModeCommandFormat { get; set; }

        /// <summary>
        /// Writes the Sense function mode without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The Sense function mode. </param>
        /// <returns> The <see cref="FunctionMode">Sense function mode</see> or none if unknown. </returns>
        public virtual SenseFunctionModes? WriteFunctionMode( SenseFunctionModes value )
        {
            this.FunctionMode = this.Write( this.FunctionModeCommandFormat, value, this.FunctionModeReadWrites );
            return this.FunctionMode;
        }

        #endregion

    }
}
