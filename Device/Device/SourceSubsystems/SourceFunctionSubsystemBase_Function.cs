using isr.Core.EnumExtensions;

namespace isr.VI
{
    public partial class SourceFunctionSubsystemBase
    {

        #region " RANGE "

        /// <summary> Define function mode ranges. </summary>
        private void DefineFunctionModeRanges()
        {
            this.FunctionModeRanges = new RangeDictionary();
            SourceSubsystemBase.DefineFunctionModeRanges( this.FunctionModeRanges, this.DefaultFunctionRange );
        }

        /// <summary> Gets or sets the function mode ranges. </summary>
        /// <value> The function mode ranges. </value>
        public RangeDictionary FunctionModeRanges { get; private set; }

        /// <summary> Gets or sets the default function range. </summary>
        /// <value> The default function range. </value>
        public Core.Primitives.RangeR DefaultFunctionRange { get; set; }

        /// <summary> Converts a functionMode to a range. </summary>
        /// <param name="functionMode"> The function mode. </param>
        /// <returns> FunctionMode as an isr.Core.Primitives.RangeR. </returns>
        public virtual Core.Primitives.RangeR ToRange( int functionMode )
        {
            return this.FunctionModeRanges[functionMode];
        }

        /// <summary> The function range. </summary>
        private Core.Primitives.RangeR _FunctionRange;

        /// <summary> The Range of the range. </summary>
        /// <value> The function range. </value>
        public Core.Primitives.RangeR FunctionRange
        {
            get => this._FunctionRange;

            set {
                if ( this.FunctionRange != value )
                {
                    this._FunctionRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " DECIMAL PLACES "

        /// <summary> Gets or sets the default decimal places. </summary>
        /// <value> The default decimal places. </value>
        public int DefaultFunctionModeDecimalPlaces { get; set; }

        /// <summary> Define function mode decimal places. </summary>
        private void DefineFunctionModeDecimalPlaces()
        {
            this.FunctionModeDecimalPlaces = new IntegerDictionary();
            MultimeterSubsystemBase.DefineFunctionModeDecimalPlaces( this.FunctionModeDecimalPlaces, this.DefaultFunctionModeDecimalPlaces );
        }

        /// <summary> Gets or sets the function mode decimal places. </summary>
        /// <value> The function mode decimal places. </value>
        public IntegerDictionary FunctionModeDecimalPlaces { get; private set; }

        /// <summary> Converts a function Mode to a decimal places. </summary>
        /// <param name="functionMode"> The function mode. </param>
        /// <returns> FunctionMode as an Integer. </returns>
        public virtual int ToDecimalPlaces( int functionMode )
        {
            return this.FunctionModeDecimalPlaces[functionMode];
        }

        /// <summary> The function range decimal places. </summary>
        private int _FunctionRangeDecimalPlaces;

        /// <summary> Gets or sets the function range decimal places. </summary>
        /// <value> The function range decimal places. </value>
        public int FunctionRangeDecimalPlaces
        {
            get => this._FunctionRangeDecimalPlaces;

            set {
                if ( this.FunctionRangeDecimalPlaces != value )
                {
                    this._FunctionRangeDecimalPlaces = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " UNIT "

        /// <summary> Gets or sets the default unit. </summary>
        /// <value> The default unit. </value>
        public Arebis.TypedUnits.Unit DefaultFunctionUnit { get; set; }

        /// <summary> Define function mode units. </summary>
        private void DefineFunctionModeUnits()
        {
            this.FunctionModeUnits = new UnitDictionary();
            SourceSubsystemBase.DefineFunctionModeUnits( this.FunctionModeUnits );
        }

        /// <summary> Gets or sets the function mode decimal places. </summary>
        /// <value> The function mode decimal places. </value>
        public UnitDictionary FunctionModeUnits { get; private set; }

        /// <summary> Parse units. </summary>
        /// <param name="functionMode"> The  Multimeter Function Mode. </param>
        /// <returns> An Arebis.TypedUnits.Unit. </returns>
        public virtual Arebis.TypedUnits.Unit ToUnit( int functionMode )
        {
            return this.FunctionModeUnits[functionMode];
        }

        /// <summary> Gets or sets the function unit. </summary>
        /// <value> The function unit. </value>
        public Arebis.TypedUnits.Unit FunctionUnit
        {
            get => this.Amount.Unit;

            set {
                if ( this.FunctionUnit != value )
                {
                    this.NewAmount( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Define function mode read writes. </summary>
        private void DefineFunctionModeReadWrites()
        {
            this.FunctionModeReadWrites = new Pith.EnumReadWriteCollection();
            SourceSubsystemBase.DefineFunctionModeReadWrites( this.FunctionModeReadWrites );
        }

        /// <summary> Gets or sets a dictionary of Source function mode parses. </summary>
        /// <value> A Dictionary of Source function mode parses. </value>
        public Pith.EnumReadWriteCollection FunctionModeReadWrites { get; private set; }

        /// <summary> The supported function modes. </summary>
        private SourceFunctionModes _SupportedFunctionModes;

        /// <summary>
        /// Gets or sets the supported Function Modes. This is a subset of the functions supported by the
        /// instrument.
        /// </summary>
        /// <value> The supported Source function modes. </value>
        public SourceFunctionModes SupportedFunctionModes
        {
            get => this._SupportedFunctionModes;

            set {
                if ( !this.SupportedFunctionModes.Equals( value ) )
                {
                    this._SupportedFunctionModes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the function code. </summary>
        /// <value> The function code. </value>
        protected string FunctionCode { get; private set; }

        /// <summary> Define function clear known state. </summary>
        protected virtual void DefineFunctionClearKnownState()
        {
            this.NewAmount( this.FunctionUnit );
            this.FunctionCode = this.FunctionMode.HasValue ? this.FunctionMode.Value.ExtractBetween() : string.Empty;
        }

        /// <summary> The function mode. </summary>
        private SourceFunctionModes? _FunctionMode;

        /// <summary> Gets or sets the cached Source function mode. </summary>
        /// <value>
        /// The <see cref="FunctionMode">Source function mode</see> or none if not set or unknown.
        /// </value>
        public SourceFunctionModes? FunctionMode
        {
            get => this._FunctionMode;

            protected set {
                if ( !this.FunctionMode.Equals( value ) )
                {
                    this._FunctionMode = value;
                    if ( value.HasValue )
                    {
                        this.FunctionRange = this.ToRange( ( int ) value.Value );
                        this.FunctionUnit = this.ToUnit( ( int ) value.Value );
                        this.FunctionRangeDecimalPlaces = this.ToDecimalPlaces( ( int ) value.Value );
                    }
                    else
                    {
                        this.FunctionRange = this.DefaultFunctionRange;
                        this.FunctionUnit = this.DefaultFunctionUnit;
                        this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
                    }

                    this.DefineFunctionClearKnownState();
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Source function mode. </summary>
        /// <param name="value"> The  Source function mode. </param>
        /// <returns>
        /// The <see cref="FunctionMode">source Source function mode</see> or none if unknown.
        /// </returns>
        public virtual SourceFunctionModes? ApplyFunctionMode( SourceFunctionModes value )
        {
            _ = this.WriteFunctionMode( value );
            return this.QueryFunctionMode();
        }

        /// <summary> Gets or sets the Source function mode query command. </summary>
        /// <value> The Source function mode query command. </value>
        protected abstract string FunctionModeQueryCommand { get; set; }

        /// <summary> Queries the Source function mode. </summary>
        /// <returns>
        /// The <see cref="FunctionMode">Source function mode</see> or none if unknown.
        /// </returns>
        public virtual SourceFunctionModes? QueryFunctionMode()
        {
            return this.QueryFunctionMode( this.FunctionModeQueryCommand );
        }

        /// <summary> Queries the Source function mode. </summary>
        /// <param name="queryCommand"> The query command. </param>
        /// <returns>
        /// The <see cref="FunctionMode">Source function mode</see> or none if unknown.
        /// </returns>
        public virtual SourceFunctionModes? QueryFunctionMode( string queryCommand )
        {
            this.FunctionMode = this.Query( queryCommand, this.FunctionMode.GetValueOrDefault( SourceFunctionModes.None ), this.FunctionModeReadWrites );
            return this.FunctionMode;
        }

        /// <summary> Gets or sets the Source function mode command format. </summary>
        /// <value> The Source function mode command format. </value>
        protected abstract string FunctionModeCommandFormat { get; set; }

        /// <summary>
        /// Writes the Source function mode without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The Source function mode. </param>
        /// <returns>
        /// The <see cref="FunctionMode">Source function mode</see> or none if unknown.
        /// </returns>
        public virtual SourceFunctionModes? WriteFunctionMode( SourceFunctionModes value )
        {
            this.FunctionMode = this.Write( this.FunctionModeCommandFormat, value, this.FunctionModeReadWrites );
            return this.FunctionMode;
        }

        #endregion

    }
}
