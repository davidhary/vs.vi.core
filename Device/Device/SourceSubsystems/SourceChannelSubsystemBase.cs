namespace isr.VI
{

    /// <summary>
    /// Defines the contract that must be implemented by a Source Channel Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public abstract class SourceChannelSubsystemBase : SourceFunctionSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceSubsystemBase" /> class.
        /// </summary>
        /// <param name="channelNumber">   The channel number. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected SourceChannelSubsystemBase( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.ChannelNumber = channelNumber;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets values to their known clear execution state. </summary>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.DefineFunctionClearKnownState();
        }

        #endregion

        #region " CHANNEL "

        /// <summary> Gets or sets the channel number. </summary>
        /// <value> The channel number. </value>
        public int ChannelNumber { get; private set; }

        #endregion

    }
}
