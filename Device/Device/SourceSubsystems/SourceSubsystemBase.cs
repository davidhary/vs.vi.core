using System;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by a Source Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class SourceSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceSubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        protected SourceSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.DefaultFunctionModeDecimalPlaces = 3;
            this._Amount = new Arebis.TypedUnits.Amount( 0d, this.DefaultFunctionUnit );
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
            this.DefineFunctionModeDecimalPlaces();
            this.DefineFunctionModeReadWrites();
            this.DefineFunctionModeRanges();
            this.DefineFunctionModeUnits();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.Level = new double?();
            this.DefineFunctionClearKnownState();
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Amount = new Arebis.TypedUnits.Amount( 0d, this.DefaultFunctionUnit );
            this.FunctionUnit = this.DefaultFunctionUnit;
            this._FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
            this.AutoClearEnabled = false;
            this.AutoDelayEnabled = true;
            this.Delay = TimeSpan.Zero;
            this.SweepPoints = 2500;
        }

        #endregion

        #region " AUTO CLEAR ENABLED "

        /// <summary> The automatic clear enabled. </summary>
        private bool? _AutoClearEnabled;

        /// <summary> Gets or sets the cached Auto Clear Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Clear Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoClearEnabled
        {
            get => this._AutoClearEnabled;

            protected set {
                if ( !Equals( this.AutoClearEnabled, value ) )
                {
                    this._AutoClearEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Clear Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoClearEnabled( bool value )
        {
            _ = this.WriteAutoClearEnabled( value );
            return this.QueryAutoClearEnabled();
        }

        /// <summary> Gets or sets the automatic Clear enabled query command. </summary>
        /// <remarks> SCPI: ":SOUR:CLE:AUTO?". </remarks>
        /// <value> The automatic Clear enabled query command. </value>
        protected virtual string AutoClearEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Clear Enabled sentinel. Also sets the
        /// <see cref="AutoClearEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoClearEnabled()
        {
            this.AutoClearEnabled = this.Query( this.AutoClearEnabled, this.AutoClearEnabledQueryCommand );
            return this.AutoClearEnabled;
        }

        /// <summary> Gets or sets the automatic Clear enabled command Format. </summary>
        /// <remarks> SCPI: ":SOU:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Clear enabled query command. </value>
        protected virtual string AutoClearEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Clear Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoClearEnabled( bool value )
        {
            this.AutoClearEnabled = this.Write( value, this.AutoClearEnabledCommandFormat );
            return this.AutoClearEnabled;
        }

        #endregion

        #region " AUTO DELAY ENABLED "

        /// <summary> The automatic delay enabled. </summary>
        private bool? _AutoDelayEnabled;

        /// <summary> Gets or sets the cached Auto Delay Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Delay Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoDelayEnabled
        {
            get => this._AutoDelayEnabled;

            protected set {
                if ( !Equals( this.AutoDelayEnabled, value ) )
                {
                    this._AutoDelayEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Delay Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoDelayEnabled( bool value )
        {
            _ = this.WriteAutoDelayEnabled( value );
            return this.QueryAutoDelayEnabled();
        }

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <remarks> SCPI: ":SOUR:DEL:AUTO?". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string AutoDelayEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Delay Enabled sentinel. Also sets the
        /// <see cref="AutoDelayEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoDelayEnabled()
        {
            this.AutoDelayEnabled = this.Query( this.AutoDelayEnabled, this.AutoDelayEnabledQueryCommand );
            return this.AutoDelayEnabled;
        }

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <remarks> SCPI: ":SOUR:DEL:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic delay enabled query command. </value>
        protected virtual string AutoDelayEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Delay Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoDelayEnabled( bool value )
        {
            this.AutoDelayEnabled = this.Write( value, this.AutoDelayEnabledCommandFormat );
            return this.AutoDelayEnabled;
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Auto Range enabled. </summary>
        private bool? _AutoRangeEnabled;

        /// <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public virtual bool? AutoRangeEnabled
        {
            get => this._AutoRangeEnabled;

            protected set {
                if ( !Equals( this.AutoRangeEnabled, value ) )
                {
                    this._AutoRangeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoRangeEnabled( bool value )
        {
            _ = this.WriteAutoRangeEnabled( value );
            return this.QueryAutoRangeEnabled();
        }

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <remarks> SCPI: "SOUR:RANG:AUTO?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="AutoRangeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoRangeEnabled()
        {
            this.AutoRangeEnabled = this.Query( this.AutoRangeEnabled, this.AutoRangeEnabledQueryCommand );
            return this.AutoRangeEnabled;
        }

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <remarks> SCPI: "SOUR:RANGE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoRangeEnabled( bool value )
        {
            this.AutoRangeEnabled = this.Write( value, this.AutoRangeEnabledCommandFormat );
            return this.AutoRangeEnabled;
        }

        #endregion

        #region " DELAY "

        /// <summary> The delay. </summary>
        private TimeSpan? _Delay;

        /// <summary> Gets or sets the cached Source Delay. </summary>
        /// <remarks>
        /// The delay is used to delay operation in the Source layer. After the programmed Source event
        /// occurs, the instrument waits until the delay period expires before performing the Device
        /// Action.
        /// </remarks>
        /// <value> The Source Delay or none if not set or unknown. </value>
        public TimeSpan? Delay
        {
            get => this._Delay;

            protected set {
                if ( !Nullable.Equals( this.Delay, value ) )
                {
                    this._Delay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Source Delay. </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The Source Delay or none if unknown. </returns>
        public TimeSpan? ApplyDelay( TimeSpan value )
        {
            _ = this.WriteDelay( value );
            _ = this.QueryDelay();
            return default;
        }

        /// <summary> Gets or sets the delay query command. </summary>
        /// <remarks> SCPI: ":SOUR:DEL?". </remarks>
        /// <value> The delay query command. </value>
        protected virtual string DelayQueryCommand { get; set; }

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
        /// <value> The Delay query command. </value>
        protected virtual string DelayFormat { get; set; }

        /// <summary> Queries the Delay. </summary>
        /// <returns> The Delay or none if unknown. </returns>
        public TimeSpan? QueryDelay()
        {
            this.Delay = this.Query( this.Delay, this.DelayFormat, this.DelayQueryCommand );
            return this.Delay;
        }

        /// <summary> Gets or sets the delay command format. </summary>
        /// <remarks> SCPI: ":SOUR:DEL {0:s\.FFFFFFF}". </remarks>
        /// <value> The delay command format. </value>
        protected virtual string DelayCommandFormat { get; set; }

        /// <summary> Writes the Source Delay without reading back the value from the device. </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The Source Delay or none if unknown. </returns>
        public TimeSpan? WriteDelay( TimeSpan value )
        {
            this.Delay = this.Write( value, this.DelayCommandFormat );
            return this.Delay;
        }

        #endregion

        #region " LEVEL "

        /// <summary> The amount. </summary>
        private Arebis.TypedUnits.Amount _Amount;

        /// <summary> Gets or sets the amount. </summary>
        /// <value> The amount. </value>
        public Arebis.TypedUnits.Amount Amount
        {
            get => this._Amount;

            set {
                this._Amount = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Creates a new amount. </summary>
        /// <param name="unit"> The unit. </param>
        private void NewAmount( Arebis.TypedUnits.Unit unit )
        {
            if ( this.Level.HasValue )
            {
                this._Amount = new Arebis.TypedUnits.Amount( this.Level.Value, unit );
                this.LevelCaption = $"{this.Amount} {this.Amount.Unit}";
            }
            else
            {
                this._Amount = new Arebis.TypedUnits.Amount( 0d, unit );
                this.LevelCaption = $"-.---- {this.Amount.Unit}";
            }

            this.NotifyPropertyChanged( nameof( this.Amount ) );
            this.NotifyPropertyChanged( nameof( this.FunctionUnit ) );
        }

        /// <summary> The level caption. </summary>
        private string _LevelCaption;

        /// <summary> Gets or sets the Level caption. </summary>
        /// <value> The Level caption. </value>
        public string LevelCaption
        {
            get => this._LevelCaption;

            set {
                if ( !string.Equals( value, this.LevelCaption, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LevelCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The level. </summary>
        private double? _Level;

        /// <summary> Gets or sets the cached Source Current Level. </summary>
        /// <value> The Source Current Level. Actual current depends on the power supply mode. </value>
        public double? Level
        {
            get => this._Level;

            protected set {
                if ( !Nullable.Equals( this.Level, value ) )
                {
                    this._Level = value;
                    this.NewAmount( this.FunctionUnit );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source current level. </summary>
        /// <remarks>
        /// This command set the immediate output current level. The value is in Amperes. The immediate
        /// level is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <param name="value"> The current level. </param>
        /// <returns> The Source Current Level. </returns>
        public double? ApplyLevel( double value )
        {
            _ = this.WriteLevel( value );
            return this.QueryLevel();
        }

        /// <summary> Gets or sets The Level query command. </summary>
        /// <value> The Level query command. </value>
        protected virtual string LevelQueryCommand { get; set; }

        /// <summary> Queries the current level. </summary>
        /// <returns> The current level or none if unknown. </returns>
        public double? QueryLevel()
        {
            this.Level = this.Query( this.Level.GetValueOrDefault( 0d ), this.LevelQueryCommand );
            return this.Level;
        }

        /// <summary> Gets or sets The Level command format. </summary>
        /// <value> The Level command format. </value>
        protected virtual string LevelCommandFormat { get; set; }

        /// <summary>
        /// Writes the source current level without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output current level. The value is in Amperes. The immediate
        /// level is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <param name="value"> The current level. </param>
        /// <returns> The Source Current Level. </returns>
        public double? WriteLevel( double value )
        {
            _ = this.Session.WriteLine( this.LevelCommandFormat, ( object ) value );
            this.Level = value;
            return this.Level;
        }

        #endregion

        #region " LIMIT "

        /// <summary> The Limit. </summary>
        private double? _Limit;

        /// <summary>
        /// Gets or sets the cached source Limit for a Current Source. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Limit
        {
            get => this._Limit;

            protected set {
                if ( !Nullable.Equals( this.Limit, value ) )
                {
                    this._Limit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Limit. </summary>
        /// <remarks>
        /// This command set the immediate output Limit. The value is in Amperes. The immediate Limit is
        /// the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Limit. </param>
        /// <returns> The Source Limit. </returns>
        public double? ApplyLimit( double value )
        {
            _ = this.WriteLimit( value );
            return this.QueryLimit();
        }

        /// <summary> Gets or sets the limit query command. </summary>
        /// <value> The limit query command. </value>
        protected virtual string ModalityLimitQueryCommandFormat { get; set; }

        /// <summary> Queries the Limit. </summary>
        /// <returns> The Limit or none if unknown. </returns>
        public virtual double? QueryLimit()
        {
            this.Limit = this.Query( this.Limit, this.ModalityLimitQueryCommandFormat );
            return this.Limit;
        }

        /// <summary> Gets or sets the modality limit command format. </summary>
        /// <value> The modality limit command format. </value>
        protected virtual string ModalityLimitCommandFormat { get; set; }

        /// <summary> Writes the source Limit without reading back the value from the device. </summary>
        /// <remarks>
        /// This command set the immediate output Limit. The value is in Amperes. The immediate Limit is
        /// the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Limit. </param>
        /// <returns> The Source Limit. </returns>
        public virtual double? WriteLimit( double value )
        {
            _ = this.Write( value, this.ModalityLimitCommandFormat );
            this.Limit = value;
            return this.Limit;
        }

        #endregion

        #region " LIMIT TRIPPED "

        /// <summary> Limit Tripped. </summary>
        private bool? _LimitTripped;

        /// <summary> Gets or sets the cached Limit Tripped sentinel. </summary>
        /// <value>
        /// <c>null</c> if Limit Tripped is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? LimitTripped
        {
            get => this._LimitTripped;

            protected set {
                if ( !Equals( this.LimitTripped, value ) )
                {
                    this._LimitTripped = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the limit tripped command format. </summary>
        /// <value> The limit tripped command format. </value>
        protected virtual string LimitTrippedQueryCommand { get; set; }

        /// <summary>
        /// Queries the Limit Tripped sentinel. Also sets the
        /// <see cref="LimitTripped">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryLimitTripped()
        {
            this.LimitTripped = this.Query( this.LimitTripped, this.LimitTrippedQueryCommand );
            return this.LimitTripped;
        }

        #endregion

        #region " OUTPUT ENABLED "

        /// <summary> Output enabled. </summary>
        private bool? _OutputEnabled;

        /// <summary> Gets or sets the cached Output Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Output Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? OutputEnabled
        {
            get => this._OutputEnabled;

            protected set {
                if ( !Equals( this.OutputEnabled, value ) )
                {
                    this._OutputEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Output Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyOutputEnabled( bool value )
        {
            _ = this.WriteOutputEnabled( value );
            return this.QueryOutputEnabled();
        }

        /// <summary> Gets or sets the Output enabled query command. </summary>
        /// <value> The Output enabled query command. </value>
        protected virtual string OutputEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Output Enabled sentinel. Also sets the
        /// <see cref="OutputEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryOutputEnabled()
        {
            this.OutputEnabled = this.Query( this.OutputEnabled, this.OutputEnabledQueryCommand );
            return this.OutputEnabled;
        }

        /// <summary> Gets or sets the Output enabled command Format. </summary>
        /// <value> The Output enabled query command. </value>
        protected virtual string OutputEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Output Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteOutputEnabled( bool value )
        {
            this.OutputEnabled = this.Write( value, this.OutputEnabledCommandFormat );
            return this.OutputEnabled;
        }

        #endregion

        #region " RANGE "

        /// <summary> The Range. </summary>
        private double? _Range;

        /// <summary>
        /// Gets or sets the cached sense Range. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Range
        {
            get => this._Range;

            protected set {
                if ( !Nullable.Equals( this.Range, value ) )
                {
                    this._Range = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense Range. </summary>
        /// <param name="value"> The Range. </param>
        /// <returns> The Range. </returns>
        public double? ApplyRange( double value )
        {
            _ = this.WriteRange( value );
            return this.QueryRange();
        }

        /// <summary> Gets or sets The Range query command. </summary>
        /// <value> The Range query command. </value>
        protected virtual string RangeQueryCommand { get; set; }

        /// <summary> Queries The Range. </summary>
        /// <returns> The Range or none if unknown. </returns>
        public double? QueryRange()
        {
            this.Range = this.Query( this.Range, this.RangeQueryCommand );
            return this.Range;
        }

        /// <summary> Gets or sets The Range command format. </summary>
        /// <value> The Range command format. </value>
        protected virtual string RangeCommandFormat { get; set; }

        /// <summary> Writes The Range without reading back the value from the device. </summary>
        /// <remarks> This command sets The Range. </remarks>
        /// <param name="value"> The Range. </param>
        /// <returns> The Range. </returns>
        public double? WriteRange( double value )
        {
            this.Range = this.Write( value, this.RangeCommandFormat );
            return this.Range;
        }

        #endregion

        #region " READ BACK ENABLED "

        /// <summary> The last read back. </summary>
        private string _LastReadBack;

        /// <summary> Gets or sets the last ReadBack. </summary>
        /// <value> The last ReadBack. </value>
        public string LastReadBack
        {
            get => this._LastReadBack;

            set {
                if ( !string.Equals( value, this.LastReadBack, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LastReadBack = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The read back caption. </summary>
        private string _ReadBackCaption;

        /// <summary> Gets or sets the ReadBack caption. </summary>
        /// <value> The ReadBack caption. </value>
        public string ReadBackCaption
        {
            get => this._ReadBackCaption;

            set {
                if ( !string.Equals( value, this.ReadBackCaption, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ReadBackCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The read back amount. </summary>
        private Arebis.TypedUnits.Amount _ReadBackAmount;

        /// <summary> Gets or sets the read back amount. </summary>
        /// <value> The amount. </value>
        public Arebis.TypedUnits.Amount ReadBackAmount
        {
            get => this._ReadBackAmount;

            set {
                this._ReadBackAmount = value;
                this.NotifyPropertyChanged();
                this.HasReadBackAmount = value is object;
            }
        }

        /// <summary> True if has read back amount, false if not. </summary>
        private bool _HasReadBackAmount;

        /// <summary> Gets or sets the has read back amount. </summary>
        /// <value> The has read back amount. </value>
        public bool HasReadBackAmount
        {
            get => this._HasReadBackAmount;

            set {
                this._HasReadBackAmount = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Parse read back amount. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> A Double. </returns>
        public double ParseReadBackAmount( string value )
        {
            double result = 0d;
            string caption;
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                this.ReadBackAmount = null;
                caption = $"-.---- {this.Amount.Unit}";
            }
            else if ( double.TryParse( value, out result ) )
            {
                this.ReadBackAmount = new Arebis.TypedUnits.Amount( result, this.Amount.Unit );
                caption = $"{this.ReadBackAmount} {this.ReadBackAmount.Unit}";
            }
            else
            {
                this.ReadBackAmount = null;
                caption = $"-NAN- {this.Amount.Unit}";
            }

            this.ReadBackCaption = caption;
            this.LastReadBack = value;
            return result;
        }

        /// <summary> Gets or sets the query source value command format. </summary>
        /// <value> The query source value command format. </value>
        protected virtual string QuerySourceValueCommandFormat { get; set; } = "_G.print(defbuffer1.sourcevalues[{0}])";

        /// <summary> Parse read back buffer amount. </summary>
        /// <param name="index"> Zero-based index of the. </param>
        /// <returns> A Double. </returns>
        public double ParseReadBackBufferAmount( int index )
        {
            string value = this.QueryTrimEnd( "", this.QuerySourceValueCommandFormat, ( object ) index );
            return this.ParseReadBackAmount( value );
        }

        /// <summary> Parse read back buffer amount. </summary>
        /// <returns> A Double. </returns>
        public double ParseReadBackBufferAmount()
        {
            string value = this.QueryTrimEnd( "", this.QuerySourceValueCommandFormat, "defbuffer1.n" );
            return this.ParseReadBackAmount( value );
        }

        #endregion

        #region " READ BACK ENABLED "

        /// <summary> The read back enabled. </summary>
        private bool? _ReadBackEnabled;

        /// <summary> Gets or sets the cached Read Back Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Read Back Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ReadBackEnabled
        {
            get => this._ReadBackEnabled;

            protected set {
                if ( !Equals( this.ReadBackEnabled, value ) )
                {
                    this._ReadBackEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Read Back Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyReadBackEnabled( bool value )
        {
            _ = this.WriteReadBackEnabled( value );
            return this.QueryReadBackEnabled();
        }

        /// <summary> Gets or sets the Read Back enabled query command. </summary>
        /// <remarks> SCPI: :SOUR:function:READ:BACK? TSP:  smu.source.readback. </remarks>
        /// <value> The Read Back enabled query command. </value>
        protected virtual string ReadBackEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Read Back Enabled sentinel. Also sets the
        /// <see cref="ReadBackEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryReadBackEnabled()
        {
            this.ReadBackEnabled = this.Query( this.ReadBackEnabled, this.ReadBackEnabledQueryCommand );
            return this.ReadBackEnabled;
        }

        /// <summary> Gets or sets the Read Back enabled command Format. </summary>
        /// <remarks>
        /// SCPI: :SOU:function:READ:BACK {0:'ON';'ON';'OFF'}
        /// TSP: _G.smu.source.readback={0:'smu.ON';'smu.ON';'smu.OFF'}
        /// </remarks>
        /// <value> The Read Back enabled query command. </value>
        protected virtual string ReadBackEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Read Back Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteReadBackEnabled( bool value )
        {
            this.ReadBackEnabled = this.Write( value, this.ReadBackEnabledCommandFormat );
            return this.ReadBackEnabled;
        }

        #endregion

        #region " SWEEP POINTS "

        /// <summary> The sweep point. </summary>
        private int? _SweepPoint;

        /// <summary> Gets or sets the cached Sweep Points. </summary>
        /// <value> The Sweep Points or none if not set or unknown. </value>
        public int? SweepPoints
        {
            get => this._SweepPoint;

            protected set {
                if ( !Nullable.Equals( this.SweepPoints, value ) )
                {
                    this._SweepPoint = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Sweep Points. </summary>
        /// <param name="value"> The current SweepPoints. </param>
        /// <returns> The SweepPoints or none if unknown. </returns>
        public int? ApplySweepPoints( int value )
        {
            _ = this.WriteSweepPoints( value );
            return this.QuerySweepPoints();
        }

        /// <summary> Gets or sets Sweep Points query command. </summary>
        /// <remarks> SCPI: ":SOUR:SWE:POIN?". </remarks>
        /// <value> The Sweep Points query command. </value>
        protected virtual string SweepPointsQueryCommand { get; set; }

        /// <summary> Queries the current Sweep Points. </summary>
        /// <returns> The Sweep Points or none if unknown. </returns>
        public int? QuerySweepPoints()
        {
            if ( !string.IsNullOrWhiteSpace( this.SweepPointsQueryCommand ) )
            {
                this.SweepPoints = this.Session.Query( 0, this.SweepPointsQueryCommand );
            }

            return this.SweepPoints;
        }

        /// <summary> Gets or sets Sweep Points command format. </summary>
        /// <remarks> SCPI: ":SOUR:SWE:POIN {0}". </remarks>
        /// <value> The Sweep Points command format. </value>
        protected virtual string SweepPointsCommandFormat { get; set; }

        /// <summary> Write the Sweep Points without reading back the value from the device. </summary>
        /// <param name="value"> The current Sweep Points. </param>
        /// <returns> The Sweep Points or none if unknown. </returns>
        public int? WriteSweepPoints( int value )
        {
            if ( !string.IsNullOrWhiteSpace( this.SweepPointsCommandFormat ) )
            {
                _ = this.Session.WriteLine( this.SweepPointsCommandFormat, ( object ) value );
            }

            this.SweepPoints = value;
            return this.SweepPoints;
        }

        #endregion

    }
}
