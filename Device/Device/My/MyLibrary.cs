namespace isr.VI.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.Device;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI Device Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Virtual Instrument Device Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.Device";
    }
}
