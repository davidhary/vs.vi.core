## ISR VI Device<sub>&trade;</sub>: VISA Device Library
\(C\) 2012 Integrated Scientific Resources, Inc. All rights reserved.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*7.3.7640 2020-12-01*  
Converted to C#.

*7.3.7464 2020-06-08*  
Updates resource names storage upon opening a new session.

*7.2.7445 2020-05-20*  
Relaxes resource name validations to allow opening resources 
not registered with the VISA resource manager.

*7.2.7411 2020-04-16*  
Uses IVI Foundation VISA--Keysight implementation.

*7.2.7367 2020-03-03*  
Visa 19.5. Digital I/O: Adds digital line class and sets digital line polarities.

*7.1.7257 2019-11-12*  
Visa 19. Renames service request enable commands.

*6.1.6937 2018-12-27*  
adds Visa Session and visa session base (device base)
with support for view model for selection and opening resources.

*5.0.6912 2018-12-04*  
Removes reading of the status byte when reading errors
for existing error status.

*5.0.6897 2018-11-19*  
Adds read and status read delay to the status
subsystem.

*5.0.6892 2018-11-14*  
Defaults to using Keep Alive attributes. Clears keep
alive commands and sets keep alive time interval to zero.

*5.0.6657 2018-03-24*  
Re-factor for better model/view separation.

*4.2.6636 2018-03-03*  
Adds methods to open a status session. Uses refractory
initialization periods on all derived devices. Adds status methods to
address changes to the device language (SCPI/TSP).

*4.2.6633 2018-02-28*  
Adds real value payload and subsystem base query and
write commands for a payload base.

*4.2.6591 2018-01-17*  
Adds a device clear active state, reset and clear on
device opened to clear existing error before raising error warnings.

*4.2.6583 2018-01-09*  
Applies settings to set refractory periods when
opening the device. Moves refractory periods to the status and system
sub systems.

*4.1.6440 2017-08-19*  
Uses modified trace message and logger.

*4.1.6438 2017-08-17*  
Updates trace message talkers to ensure trace levels
are correctly updated.

*4.0.6264 2017-02-25*  
Adds units and statuses to buffer readings.

*4.0.6263 2017-02-24*  
Adds buffer streaming to the trace subsystem.

*4.0.6256 2017-02-16*  
Adds meter complete first grading and binning mode.

*4.0.6243 2017-02-03*  
Adds trigger plan monitoring and buffer reading.

*4.0.6214 2017-01-05*  
Adds Open Lead detection. Adds Limit 2 to the
Calculate 2 subsystem.

*4.0.6211 2017-01-02*  
Adds Calculate 2 limit functions. Adds sense function
filtering commands.

*4.0.5890 2016-02-16*  
Adds interlock info class.

*4.0.5868 2016-01-25*  
Allows creating a session without a resource name.

*4.0.5812 2015-11-30 Splits R2D2 and SCPI devices to associated libraries.
Uses factory class to create instrument and interface sessions.

*4.0.5803 2015-11-21 New structures and name spaces.

*3.0.5226 2014-05-23*  
Uses new stack trace parser. Fixes the checking for
visa and device errors.

*3.0.5189 2014-03-17*  
Applies the session status elements when calling the
resetting the status subsystem known state even if the status subsystem
does not have a defined reset command.

*3.0.5181 2014-03-09*  
Uses abstract properties to define instrument
commands.

*3.0.5163 2014-02-19*  
Uses local sync and async safe event handlers.

*3.0.5145 2014-02-01*  
Use diagnosis base classes for property changes. Bug
fix: disable service request when closing (was enabling). Support
enabling synchronous property change with last message received.

*3.0.5126 2014-01-13*  
Tagged as 2014.

*3.0.5067 2013-11-15*  
Overrides Device Open in the Device class and set
Device Open in the Status Subsystem. Adds Connect Timeout to the base
device.

*3.0.5065 2013-11-13*  
Changes \'Read\' to \'Query\' for all query methods
\-- read is used for reading existing data; query asks for the data and
then reads. Adds tracing to the devices and subsystems. Makes status to
main subsystem. Moves methods from System to Status. Adds a subsystem
base with status. Add check and Throw and Check and Report.

*3.0.5056 2013-11-04*  
Adds a Presettable Publisher Base and uses this in
place of the Subsystems collection.

*3.0.5054 2013-11-02*  
Uses Arebis units in place for core primitive units.

*3.0.5050 2013-10-29*  
Prober: Emulates sending messages. Includes emulation
in the Send command.

*3.0.5045 2013-10-24*  
Moves setting default status bits to the constructors.

*3.0.5014 2013-09-24*  
Adds code for extracting delimited strings for them
enumeration descriptions. Requires parsing version info. Adds String
Enumerator and use to parse query results. Adds extensions to use
delimited values from the enumerations.

*3.0.5012 2013-09-22*  
Add Multi Meter and Prober Libraries. Move Source
Measure Units project to the Units folder. Copy the VISA 3.0 instrument
panel to the library. Add the instrument elements to the project. Change
Readings to Not Disposable. Remove the Is Visible Property from the
Resource Panel Base. Rename Readings to Readings base and document. Use
reflection to publish properties. Add presettable publisher and use for
the collection of subsystems. Add sense subsystem and use it to parse a
reading Core: Copy publisher base to Core Library. Use reflection to
publish properties. Add publisher base and publisher interface.

*3.0.5011 2013-09-21*  
Adds Interface Panel. Session: Convert write functions
to subroutines because the base functions are subroutines. Convert
Presettable functions to subroutines. Device Base: Remove Session With
Events directive and replace with Add and remove handlers. Encapsulate
enabling and disabling the service request event. Set session to nothing
when closing or disposing \-- now works ok. Remove session Last VISA
status properties Tester: Adds Interface Panel. Adds resource event
handlers to the interface panel. Adds interface and resource name
properties to the interface panel. Add disposing of managed resources to
the NI derived panels. Add Is Open sentinel because we cannot null the
Visa Interface. Document classes. Shared: Document some classes. Add
IEEE488 Syntax standard commands. Move SCPI Device and Version Info
under the SCPI folder. Instrument: Change fonts to Segoe UI 10. Enable
clear only after selecting all resources and seeing that we have
resources. Enable connection only if the resource exists. Update the
selector connector to validate the selected resource. Remove the
selected event and use property changed instead Tester: Turn on Show
Tool tip property of tool strip. Add messages. Add tracing. Use ISR
Message Based Session; Core.Diagnostics: Update the observer interface.
Have the publisher interface inherit the observer interface. Change the
synopsis delimiter to \';. \' Forms: Change fonts to Segoe UI 10. Share:
Add my library trace. Update Trace Log Info to return the default file
trace listener when replacing the listener.

*3.0.5010 2013-09-20*  
Debugged VISA session or Interface with With events
causes object disposed exception.

*3.0.5009 2013-09-19*  
Add Property Attributes to controls. Handle the
display of resource names in the selector connector control. Includes
the resource panel. Diagnostics: Add trace message interfaces for
observer and publisher. Update copyright and creation dates.

*3.0.5008 2013-09-18*  
Restores links to the solution info and key pair fails
that broke when renaming the library folder. Adds write and write line
and query line dealing with termination characters. Creates service
register values. Adds GPIB interface extensions.

*3.0.5007 2013-09-17*  
Add handling of service request; Read registers when
servicing request if no message; Update error, message and measurement
available flags when reading service register; Update standard status
when reading the standard register; Updates error, message and
measurement available when reading the status register. Updates the
device error when reading the standard register. Process registers on
service requests. Adds Reset and Clear to the base device. Adds version
info. Parses version info when reading the identity. Closes session if
failed to open. Explicitly enables and disables session service
requests. Adds resource manager extensions to find interfaces and
instruments. Add readings and measurands. Append Library to all
projects. Change main visa library to Visa Core. 3.0.5007 2013-09-16*  
Adds
Enabled property to the device. Adds sub system enabled and opened.
Access session only if session is open \-- allows working without
hardware. Removes Session status from the device. Conditions hardware
I/O on having an open session\--session is open only if hardware is
defined; otherwise session I/O is ignored. Adds reset and clear.
Terminates Presettable collection action on first failure. Adds code to
discard unread data waiting for all messages to end. Adds Version Info
base and implement in the system subsystem.

*3.0.5002 2013-09-11*  
Debugs undefined header on :SYST:ERR command. Changes
open session using must override method for adding sub stations.
Replaces parser base of Boolean with Boolean parser. Removes shared
methods from Power Supply and Source Measure. Requires implementing To
Text function in all parsers. Uses Equals in place of equality or non
equality operator on nullable types and all types for that matter as
equality operator does not work if nullable is not set. Adds
approximation functions for nullable double and default methods for
using Single Epsilon as the delta. Adds sense and route subsystems to
the source measure instrument. Uses Unequal for doubles. Adds measure
subsystem to source measure and parse readings. Parses reading on read
and fetch.

*3.0.5001 2013-09-10*  
Figures out how to set the session to nothing. Sets
released history dates to 2012. Corrects copyright dates. Adds extension
to try parse the resource. Publishes interface type and number. Removes
public open session methods. Discards all events when closing the
session; Uses session not nothing and not disposed to determine if
session is open \-- setting session to nothing raises an Object Disposed
exception. Makes error queue read only. Clears Error Queue in the base
class. Updates NI folder from Visa 3.0. Moves NI.VS10 to Visa folder.
Adds Device to Switch and Source measure. Adds unit tests for source
measure. Removes session from session writer. Removes shared session
methods. Moves interface type and number from the device to the session;
Removes the resource name from the device. Enumerates subsystems when
opening the session; Uses Boolean Parser when writing and querying.

*3.0.5000 2013-09-09*  
Documents status subsystem base. Processes subsystems:
Moves setters to base classes. Creates generic base classes. Removes
shared methods. Updates all writers and setters to return null if Visa
status failed. Changes Getters (query), Setters (apply), Writers
(write). Changes Read to Query. Changes enabled to value.

*3.0.4998 2013-09-07*  
Removes interfaces from instrument classes. Uses
regions for base SCPI Code (shared). Removes System Subsystem Base
commands exception from the System subsystem. Uses Reset Known State
when initializing. Removes base system and status from the base device.
Removes SCPI name space from Presettable and constants. Changes
constants to read only values. Removes shared methods: System and Status
subsystems. Device: Removes SCPI name space. Changes Subsystems to
Presettable Elements. Adds function to Add Subsystems. Creates Generic
Base Classes (to allow using TSP).

*3.0.4996 2013-09-05*  
Uses shared Visa and instrument exceptions. Adds
custom constructors to Visa Exception. Moves units under the units
folder. Adds Power Supply Units. Deletes Instrument code from the base
library. Shared: Adds custom constructors to Visa Exception. Exposes
timeout as timespan. Uses Property publisher base class. Converts
parsers to classes. Removes parser interface. Creates parser base.
Replaces parser interface with parser base. Uses new font for the RTF
license statement. Removes all SCPI interfaces. Makes all SCPI
subsystems base classes. Removes settings and resources from the VISA
library.

*2.0.4995 2013-09-04*  
Tagged in preparation of version 3.0.

*2.0.4695 2012-11-08*  
Adds status subsystem. Adds functionality to the
session; Uses value, value getter, setter and writer.

*2.0.4693 2012-11-06*  
Adds source measure source and output subsystem
functionality.

*2.0.4692 2012-11-05*  
Uses Session, Session Read and Session Write from
Version 3.0. Adds subsystems. Increments breaking version to 2.0

*1.0.4657 2012-10-01*  
Adds is Session Open. Adds wait for minimum current
and voltage

*1.0.4654 2012-09-28*  
Adds Initialize to the VISA SCPI instrument to
encapsulate custom resetting. Documents the function calls. Adds On and
Off constants. Adds over-voltage and current protection commands. Adds
voltage and current source commands. Adds property Publishing.

*1.0.4653 2012-09-27*  
Saves Oster projects as Oster Visa Library and Oster
Visa Units projects. Renames turn off and on to output off. Adds output
state.

*1.0.4652 2012-09-26*  
Created.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
