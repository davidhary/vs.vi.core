using System;

namespace isr.VI.ExceptionExtensions
{

    /// <summary> Adds exception data for building the exception full blown report. </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary> Adds exception data from the specified exception. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static bool AddExceptionData( Exception exception )
        {
            // this project adds no project-specific extensions.
            return false;
        }
    }

    /// <summary> Builds the full blown exception report. </summary>
    /// <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class PublicMethods
    {

        /// <summary> Adds an exception data. </summary>
        /// <param name="exception"> The exception. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private static bool AddExceptionDataThis( Exception exception )
        {
            return Methods.AddExceptionData( exception ) || Pith.ExceptionExtensions.Methods.AddExceptionData( exception ) || Foundation.ExceptionExtensions.Methods.AddExceptionData( exception ) || Core.ExceptionExtensions.ExceptionExtensionMethods.AddExceptionData( exception );
        }

        /// <summary> Converts a value to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        public static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString( this Exception value, int level )
        {
            return Core.ExceptionExtensions.ExceptionExtensionMethods.ToFullBlownString( value, level, AddExceptionDataThis );
        }
    }
}
