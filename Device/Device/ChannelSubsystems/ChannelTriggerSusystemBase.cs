namespace isr.VI
{
    /// <summary> Defines the Calculate Channel SCPI subsystem. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public abstract class ChannelTriggerSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="ChannelTriggerSubsystemBase" /> class.
        /// </summary>
        /// <param name="channelNumber">   The channel number. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected ChannelTriggerSubsystemBase( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.ChannelNumber = channelNumber;
        }

        #endregion

        #region " CHANNEL "

        /// <summary> Gets or sets the channel number. </summary>
        /// <value> The channel number. </value>
        public int ChannelNumber { get; private set; }

        #endregion

        #region " IMMEDIATE "

        /// <summary> Gets or sets the initiate command. </summary>
        /// <remarks> SCPI: ":INIT&lt;c#&gt;:IMM". </remarks>
        /// <value> The initiate command. </value>
        protected virtual string InitiateCommand { get; set; }

        /// <summary>
        /// Changes the state of the channel to the initiation state of the trigger system.
        /// </summary>
        public void Initiate()
        {
            _ = this.Write( this.InitiateCommand, this.ChannelNumber );
        }

        #endregion

        #region " CONTINUOUS ENABLED "

        /// <summary> The continuous enabled. </summary>
        private bool? _ContinuousEnabled;

        /// <summary> Gets or sets the cached Continuous Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Continuous Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ContinuousEnabled
        {
            get => this._ContinuousEnabled;

            protected set {
                if ( !Equals( this.ContinuousEnabled, value ) )
                {
                    this._ContinuousEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Continuous Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyContinuousEnabled( bool value )
        {
            _ = this.WriteContinuousEnabled( value );
            return this.QueryContinuousEnabled();
        }

        /// <summary> Gets or sets the continuous trigger enabled query command. </summary>
        /// <remarks> SCPI: ":INIT{0}:CONT?". </remarks>
        /// <value> The continuous trigger enabled query command. </value>
        protected virtual string ContinuousEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Continuous Enabled sentinel. Also sets the
        /// <see cref="ContinuousEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryContinuousEnabled()
        {
            this.ContinuousEnabled = this.Query( this.ContinuousEnabled, string.Format( this.ContinuousEnabledQueryCommand, this.ChannelNumber ) );
            return this.ContinuousEnabled;
        }

        /// <summary> Gets or sets the continuous trigger enabled command Format. </summary>
        /// <remarks> SCPI: ":INIT{0}:CONT {1:1;1;0}". </remarks>
        /// <value> The continuous trigger enabled query command. </value>
        protected virtual string ContinuousEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Continuous Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteContinuousEnabled( bool value )
        {
            _ = this.WriteLine( string.Format( this.ContinuousEnabledCommandFormat, this.ChannelNumber, value.GetHashCode() ) );
            this.ContinuousEnabled = value;
            return this.ContinuousEnabled;
        }

        #endregion

    }
}
