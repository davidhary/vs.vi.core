using System;

namespace isr.VI
{

    /// <summary> Defines a System Subsystem for a TSP System. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2016-01-13 </para>
    /// </remarks>
    public abstract class ChannelSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplaySubsystemBase" /> class.
        /// </summary>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.StatusSubsystemBase">status
        /// Subsystem</see>. </param>
        protected ChannelSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            // clear values to force update.
            this._ClosedChannels = null;
            this._ClosedChannelsCaption = null;
            this.ClosedChannels = string.Empty;
        }

        #endregion

        #region " CLOSED CHANNELS "

        /// <summary> The closed channels. </summary>
        private string _ClosedChannels;

        /// <summary> Gets or sets the closed channels. </summary>
        /// <value> The closed channels. </value>
        public string ClosedChannels
        {
            get => this._ClosedChannels;

            protected set {
                if ( !string.Equals( value, this.ClosedChannels ) )
                {
                    this._ClosedChannels = value;
                    this.ClosedChannelsCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The closed channels caption. </summary>
        private string _ClosedChannelsCaption;

        /// <summary> Gets or sets the closed channels Caption. </summary>
        /// <value> The closed channels. </value>
        public string ClosedChannelsCaption
        {
            get => this._ClosedChannelsCaption;

            protected set {
                if ( !string.Equals( value, this.ClosedChannelsCaption ) )
                {
                    this._ClosedChannelsCaption = value is null ? "nil" : string.IsNullOrWhiteSpace( value ) ? "all open" : value;

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Applies the closed channels described by value. </summary>
        /// <param name="value">   The scan list. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A String. </returns>
        public string ApplyClosedChannels( string value, TimeSpan timeout )
        {
            _ = this.WriteClosedChannels( value, timeout );
            return this.QueryClosedChannels();
        }

        /// <summary> Gets or sets the closed channels query command. </summary>
        /// <remarks> :ROUT:CLOS. </remarks>
        /// <value> The closed channels query command. </value>
        protected virtual string ClosedChannelsQueryCommand { get; set; }

        /// <summary> Queries closed channels. </summary>
        /// <returns> The closed channels. </returns>
        public string QueryClosedChannels()
        {
            string value = this.QueryTrimEnd( this.ClosedChannels, this.ClosedChannelsQueryCommand );
            this.ClosedChannels = Pith.SessionBase.EqualsNil( value ) ? string.Empty : value;
            return this.ClosedChannels;
        }

        /// <summary> Gets or sets the closed channels command format. </summary>
        /// <remarks> :ROUT:CLOS {0} </remarks>
        /// <value> The closed channels command format. </value>
        protected virtual string ClosedChannelsCommandFormat { get; set; }

        /// <summary> Writes a closed channels. </summary>
        /// <param name="value">   The scan list. </param>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A String. </returns>
        public string WriteClosedChannels( string value, TimeSpan timeout )
        {
            if ( !string.IsNullOrWhiteSpace( this.ClosedChannelsCommandFormat ) )
                this.Session.Execute( $"{string.Format( this.ClosedChannelsCommandFormat, value )}; {this.Session.OperationCompleteCommand}" );
            _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( timeout ).Status );
            this.ClosedChannels = value;
            return this.ClosedChannels;
        }

        /// <summary> Gets or sets the open channels command format. </summary>
        /// <remarks> SCPI: :ROUT:OPEN:ALL. </remarks>
        /// <value> The open channels command format. </value>
        protected virtual string OpenChannelsCommandFormat { get; set; }

        /// <summary> Open the specified channels in the list and read back the closed channels. </summary>
        /// <param name="channelList"> List of channels. </param>
        /// <param name="timeout">     The timeout. </param>
        /// <returns> A String. </returns>
        public string ApplyOpenChannels( string channelList, TimeSpan timeout )
        {
            _ = this.WriteOpenChannels( channelList, timeout );
            return this.QueryClosedChannels();
        }

        /// <summary> Opens the specified channels in the list. </summary>
        /// <param name="channelList"> List of channels. </param>
        /// <param name="timeout">     The timeout. </param>
        /// <returns> A String. </returns>
        public string WriteOpenChannels( string channelList, TimeSpan timeout )
        {
            if ( !string.IsNullOrWhiteSpace( this.OpenChannelsCommandFormat ) )
                this.Session.Execute( $"{string.Format( this.OpenChannelsCommandFormat, channelList )}; {this.Session.OperationCompleteCommand}" );
            _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( timeout ).Status );
            // set to nothing to indicate that the value is not known -- requires reading.
            this.ClosedChannels = null;
            return this.ClosedChannels;
        }

        /// <summary> Gets or sets the open channels command. </summary>
        /// <value> The open channels command. </value>
        protected virtual string OpenChannelsCommand { get; set; }

        /// <summary> Opens all channels and reads back the closed channels. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A String. </returns>
        public string ApplyOpenAll( TimeSpan timeout )
        {
            _ = this.WriteOpenAll( timeout );
            return this.QueryClosedChannels();
        }

        /// <summary> Opens all channels. </summary>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> A String. </returns>
        public string WriteOpenAll( TimeSpan timeout )
        {
            if ( !string.IsNullOrWhiteSpace( this.OpenChannelsCommand ) )
                this.Session.Execute( $"{this.OpenChannelsCommand}; {this.Session.OperationCompleteCommand}" );
            _ = this.Session.ApplyServiceRequest( this.Session.AwaitOperationCompleted( timeout ).Status );
            // set to nothing to indicate that the value is not known -- requires reading.
            this.ClosedChannels = null;
            return this.ClosedChannels;
        }

        #endregion

    }
}
