using System;

namespace isr.VI
{

    /// <summary> Defines the contract that must be implemented by a Sense Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific ReSenses, Inc.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class SenseSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="readingAmounts">  The reading amounts. </param>
        protected SenseSubsystemBase( StatusSubsystemBase statusSubsystem, ReadingAmounts readingAmounts ) : base( statusSubsystem )
        {
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.DefaultFunctionModeDecimalPlaces = 3;
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.DefaultFunctionModeDecimalPlaces = 3;
            this.ReadingAmounts = readingAmounts;
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
            this.DefineFunctionModeDecimalPlaces();
            this.DefineFunctionModeReadWrites();
            this.DefineFunctionModeRanges();
            this.DefineFunctionModeUnits();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.DefineFunctionClearKnownState();
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Auto Range enabled. </summary>
        private bool? _AutoRangeEnabled;

        /// <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoRangeEnabled
        {
            get => this._AutoRangeEnabled;

            protected set {
                if ( !Equals( this.AutoRangeEnabled, value ) )
                {
                    this._AutoRangeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoRangeEnabled( bool value )
        {
            _ = this.WriteAutoRangeEnabled( value );
            return this.QueryAutoRangeEnabled();
        }

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <remarks> SCPI: ":SENSE:RANG:AUTO?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected abstract string AutoRangeEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="AutoRangeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoRangeEnabled()
        {
            this.AutoRangeEnabled = this.Query( this.AutoRangeEnabled, this.AutoRangeEnabledQueryCommand );
            return this.AutoRangeEnabled;
        }

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <remarks> SCPI: ":SENSE:RANGE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected abstract string AutoRangeEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoRangeEnabled( bool value )
        {
            this.AutoRangeEnabled = this.Write( value, this.AutoRangeEnabledCommandFormat );
            return this.AutoRangeEnabled;
        }

        #endregion

        #region " CONCURRENT SENSE ENABLED "

        /// <summary> Auto Range enabled. </summary>
        private bool? _ConcurrentSenseEnabled;

        /// <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ConcurrentSenseEnabled
        {
            get => this._ConcurrentSenseEnabled;

            protected set {
                if ( !Equals( this.ConcurrentSenseEnabled, value ) )
                {
                    this._ConcurrentSenseEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyConcurrentSenseEnabled( bool value )
        {
            _ = this.WriteConcurrentSenseEnabled( value );
            return this.QueryConcurrentSenseEnabled();
        }

        /// <summary> Gets the automatic Range enabled query command. </summary>
        /// <remarks> SCPI: ":SENS:CONC:STAT?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string ConcurrentSenseEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="ConcurrentSenseEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryConcurrentSenseEnabled()
        {
            this.ConcurrentSenseEnabled = this.Query( this.ConcurrentSenseEnabled, this.ConcurrentSenseEnabledQueryCommand );
            return this.ConcurrentSenseEnabled;
        }

        /// <summary> Gets the automatic Range enabled command Format. </summary>
        /// <remarks> SCPI: ":SENSE:CONC:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string ConcurrentSenseEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteConcurrentSenseEnabled( bool value )
        {
            this.ConcurrentSenseEnabled = this.Write( value, this.ConcurrentSenseEnabledCommandFormat );
            return this.ConcurrentSenseEnabled;
        }

        #endregion

        #region " POWER LINE CYCLES (NPLC) "

        /// <summary> The Power Line Cycles. </summary>
        private double? _PowerLineCycles;

        /// <summary> Gets the integration period. </summary>
        /// <value> The integration period. </value>
        public TimeSpan? IntegrationPeriod => this.PowerLineCycles.HasValue ? StatusSubsystemBase.FromPowerLineCycles( this.PowerLineCycles.Value ) : new TimeSpan?();

        /// <summary> Gets or sets the cached sense PowerLineCycles. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? PowerLineCycles
        {
            get => this._PowerLineCycles;

            protected set {
                if ( !Nullable.Equals( this.PowerLineCycles, value ) )
                {
                    this._PowerLineCycles = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense PowerLineCycles. </summary>
        /// <param name="value"> The Power Line Cycles. </param>
        /// <returns> The Power Line Cycles. </returns>
        public double? ApplyPowerLineCycles( double value )
        {
            _ = this.WritePowerLineCycles( value );
            return this.QueryPowerLineCycles();
        }

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected abstract string PowerLineCyclesQueryCommand { get; set; }

        /// <summary> Queries The Power Line Cycles. </summary>
        /// <returns> The Power Line Cycles or none if unknown. </returns>
        public double? QueryPowerLineCycles()
        {
            this.PowerLineCycles = this.Query( this.PowerLineCycles, this.PowerLineCyclesQueryCommand );
            return this.PowerLineCycles;
        }

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected abstract string PowerLineCyclesCommandFormat { get; set; }

        /// <summary>
        /// Writes The Power Line Cycles without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Power Line Cycles. </remarks>
        /// <param name="value"> The Power Line Cycles. </param>
        /// <returns> The Power Line Cycles. </returns>
        public double? WritePowerLineCycles( double value )
        {
            this.PowerLineCycles = this.Write( value, this.PowerLineCyclesCommandFormat );
            return this.PowerLineCycles;
        }

        #endregion

        #region " PROTECTION LEVEL "

        /// <summary> The Current Limit. </summary>
        private double? _ProtectionLevel;

        /// <summary>
        /// Gets or sets the cached source current Limit for a voltage source. Set to
        /// <see cref="isr.VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="isr.VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ProtectionLevel
        {
            get => this._ProtectionLevel;

            protected set {
                if ( !Nullable.Equals( this.ProtectionLevel, value ) )
                {
                    this._ProtectionLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the protection level. </summary>
        /// <param name="value"> the protection level. </param>
        /// <returns> the protection level. </returns>
        public double? ApplyProtectionLevel( double value )
        {
            _ = this.WriteProtectionLevel( value );
            return this.QueryProtectionLevel();
        }

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected abstract string ProtectionLevelQueryCommand { get; set; }

        /// <summary> Queries the protection level. </summary>
        /// <returns> the protection level or none if unknown. </returns>
        public double? QueryProtectionLevel()
        {
            this.ProtectionLevel = this.Query( this.ProtectionLevel, this.ProtectionLevelQueryCommand );
            return this.ProtectionLevel;
        }

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected abstract string ProtectionLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes the protection level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the protection level. </remarks>
        /// <param name="value"> the protection level. </param>
        /// <returns> the protection level. </returns>
        public double? WriteProtectionLevel( double value )
        {
            this.ProtectionLevel = this.Write( value, this.ProtectionLevelCommandFormat );
            return this.ProtectionLevel;
        }

        #endregion

        #region " RANGE "

        /// <summary> The Range. </summary>
        private double? _Range;

        /// <summary>
        /// Gets or sets the cached sense Range. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <remarks>
        /// You can assign any real number using this command. The instrument selects the closest fixed
        /// range that Is large enough to measure the entered number. For example, for current
        /// measurements, if you expect a reading Of approximately 9 mA, Set the range To 9 mA To Select
        /// the 10 mA range. When you read this setting, you see the positive full-scale value Of the
        /// measurement range that the instrument Is presently using. This command Is primarily intended
        /// To eliminate the time that Is required by the instrument To automatically search For a range.
        /// When a range Is fixed, any signal greater than the entered range generates an overrange
        /// condition. When an over-range condition occurs, the front panel displays "Overflow" And the
        /// remote interface returns 9.9e+37.
        /// </remarks>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Range
        {
            get => this._Range;

            protected set {
                if ( !Nullable.Equals( this.Range, value ) )
                {
                    this._Range = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the sense Range. </summary>
        /// <param name="value"> The Range. </param>
        /// <returns> The Range. </returns>
        public double? ApplyRange( double value )
        {
            _ = this.WriteRange( value );
            return this.QueryRange();
        }

        /// <summary> Gets or sets The Range query command. </summary>
        /// <value> The Range query command. </value>
        protected virtual string RangeQueryCommand { get; set; }

        /// <summary> Queries The Range. </summary>
        /// <returns> The Range or none if unknown. </returns>
        public double? QueryRange()
        {
            this.Range = this.Query( this.Range, this.RangeQueryCommand );
            return this.Range;
        }

        /// <summary> Gets or sets The Range command format. </summary>
        /// <value> The Range command format. </value>
        protected virtual string RangeCommandFormat { get; set; }

        /// <summary> Writes The Range without reading back the value from the device. </summary>
        /// <remarks> This command sets The Range. </remarks>
        /// <param name="value"> The Range. </param>
        /// <returns> The Range. </returns>
        public double? WriteRange( double value )
        {
            this.Range = this.Write( value, this.RangeCommandFormat );
            return this.Range;
        }

        #endregion

        #region " FETCH; DATA; READ "

        /// <summary> Gets or sets the latest data query command. </summary>
        /// <remarks> SCPI: ":SENSE:DATA:LAT?". </remarks>
        /// <value> The latest data query command. </value>
        protected virtual string LatestDataQueryCommand { get; set; }

        /// <summary> Fetches the latest data and parses it. </summary>
        /// <remarks>
        /// Issues the ':SENSE:DATA:LAT?' query, which reads data stored in the Sample Buffer.
        /// </remarks>
        /// <returns> The latest data. </returns>
        public virtual double? FetchLatestData()
        {
            return this.MeasureReadingAmounts( this.LatestDataQueryCommand );
        }

        #endregion

    }
}
