using System;

namespace isr.VI
{

    /// <summary>
    /// Defines the contract that must be implemented by a Sense function Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific ReSenses, Inc.<para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract partial class SenseFunctionSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SenseFunctionSubsystemBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">status
        /// subsystem</see>. </param>
        /// <param name="readingAmounts">  The reading amounts. </param>
        protected SenseFunctionSubsystemBase( StatusSubsystemBase statusSubsystem, ReadingAmounts readingAmounts ) : base( statusSubsystem )
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.DefaultFunctionModeDecimalPlaces = 3;
            this.ReadingAmounts = readingAmounts;
            this.PowerLineCyclesRange = Pith.Ranges.StandardPowerLineCyclesRange;
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.DefaultFunctionRange = Pith.Ranges.NonnegativeFullRange;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
            this.DefineFunctionModeDecimalPlaces();
            this.DefineFunctionModeReadWrites();
            this.DefineFunctionModeRanges();
            this.DefineFunctionModeUnits();
            this.DefineAverageFilterTypesReadWrites();
            this.DefineConfigurationModeReadWrites();
            this.AverageCountRange = new Core.Primitives.RangeI( 1, 100 );
            this.AveragePercentWindowRange = new Core.Primitives.RangeR( 0.0d, 1d );
            this.ResolutionDigitsRange = new Core.Primitives.RangeI( 4, 9 );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.DefineFunctionClearKnownState();
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.AutoZeroEnabled = true;
            this.AutoRangeEnabled = true;
            this.PowerLineCycles = 1;
            this.PowerLineCyclesRange = Pith.Ranges.StandardPowerLineCyclesRange;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRangeDecimalPlaces = this.DefaultFunctionModeDecimalPlaces;
            this.AverageCount = 10;
            this.AveragePercentWindow = 1;
        }

        #endregion

        #region " APERTURE (NPLC) "

        /// <summary> The aperture range. </summary>
        private Core.Primitives.RangeR _ApertureRange;

        /// <summary> The Range of the Aperture. </summary>
        /// <value> The aperture range. </value>
        public Core.Primitives.RangeR ApertureRange
        {
            get => this._ApertureRange;

            set {
                if ( this.ApertureRange != value )
                {
                    this._ApertureRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The Aperture. </summary>
        private double? _Aperture;

        /// <summary>
        /// Gets or sets the cached sense Aperture. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Aperture
        {
            get => this._Aperture;

            protected set {
                if ( !Nullable.Equals( this.Aperture, value ) )
                {
                    this._Aperture = value;
                    this.NotifyPropertyChanged();
                    this.PowerLineCycles = value.HasValue ? StatusSubsystemBase.ToPowerLineCycles( TimeSpan.FromTicks( ( long ) (TimeSpan.TicksPerSecond * value.Value) ) ) : new double?();
                }
            }
        }

        /// <summary> Writes and reads back the sense Aperture. </summary>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Aperture. </returns>
        public double? ApplyAperture( double value )
        {
            _ = this.WriteAperture( value );
            return this.QueryAperture();
        }

        /// <summary> Gets or sets The Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected virtual string ApertureQueryCommand { get; set; }

        /// <summary> Queries The Aperture. </summary>
        /// <returns> The Aperture or none if unknown. </returns>
        public double? QueryAperture()
        {
            this.Aperture = this.Query( this.Aperture, this.ApertureQueryCommand );
            return this.Aperture;
        }

        /// <summary> Gets or sets The Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected virtual string ApertureCommandFormat { get; set; }

        /// <summary> Writes The Aperture without reading back the value from the device. </summary>
        /// <remarks> This command sets The Aperture. </remarks>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Aperture. </returns>
        public double? WriteAperture( double value )
        {
            this.Aperture = this.Write( value, this.ApertureCommandFormat );
            return this.Aperture;
        }

        #endregion

        #region " AUTO DELAY "

        /// <summary> Gets or sets the supports automatic Delay. </summary>
        /// <value> The supports automatic Delay. </value>
        public bool SupportsAutoDelay { get; private set; } = false;

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Auto Range enabled. </summary>
        private bool? _AutoRangeEnabled;

        /// <summary> Gets or sets the cached Auto Range Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Range Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoRangeEnabled
        {
            get => this._AutoRangeEnabled;

            protected set {
                if ( !Equals( this.AutoRangeEnabled, value ) )
                {
                    this._AutoRangeEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Range Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoRangeEnabled( bool value )
        {
            _ = this.WriteAutoRangeEnabled( value );
            return this.QueryAutoRangeEnabled();
        }

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <remarks> SCPI: "CURR:RANG:AUTO?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="AutoRangeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoRangeEnabled()
        {
            this.AutoRangeEnabled = this.Query( this.AutoRangeEnabled, this.AutoRangeEnabledQueryCommand );
            return this.AutoRangeEnabled;
        }

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <remarks> SCPI: "CURR:RANG:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected virtual string AutoRangeEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoRangeEnabled( bool value )
        {
            this.AutoRangeEnabled = this.Write( value, this.AutoRangeEnabledCommandFormat );
            return this.AutoRangeEnabled;
        }

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Auto Zero enabled. </summary>
        private bool? _AutoZeroEnabled;

        /// <summary> Gets or sets the cached Auto Zero Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auto Zero Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AutoZeroEnabled
        {
            get => this._AutoZeroEnabled;

            protected set {
                if ( !Equals( this.AutoZeroEnabled, value ) )
                {
                    this._AutoZeroEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Zero Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAutoZeroEnabled( bool value )
        {
            _ = this.WriteAutoZeroEnabled( value );
            return this.QueryAutoZeroEnabled();
        }

        /// <summary> Gets the automatic Zero enabled query command. </summary>
        /// <remarks> SCPI: "CURR:AZER?". </remarks>
        /// <value> The automatic Zero enabled query command. </value>
        protected virtual string AutoZeroEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Auto Zero Enabled sentinel. Also sets the
        /// <see cref="AutoZeroEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAutoZeroEnabled()
        {
            this.AutoZeroEnabled = this.Query( this.AutoZeroEnabled, this.AutoZeroEnabledQueryCommand );
            return this.AutoZeroEnabled;
        }

        /// <summary> Gets the automatic Zero enabled command Format. </summary>
        /// <remarks> SCPI: "CURR:AZER {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Zero enabled query command. </value>
        protected virtual string AutoZeroEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Auto Zero Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAutoZeroEnabled( bool value )
        {
            this.AutoZeroEnabled = this.Write( value, this.AutoZeroEnabledCommandFormat );
            return this.AutoZeroEnabled;
        }

        /// <summary> Gets the supports automatic zero. </summary>
        /// <value> The supports automatic zero. </value>
        public bool SupportsAutoZero => !string.IsNullOrEmpty( this.AutoZeroEnabledCommandFormat );

        #endregion

        #region " CONFIGURATION MODE "

        /// <summary> Define configuration mode read writes. </summary>
        private void DefineConfigurationModeReadWrites()
        {
            this.ConfigurationModeReadWrites = new Pith.EnumReadWriteCollection();
            foreach ( ConfigurationModes enumValue in Enum.GetValues( typeof( ConfigurationModes ) ) )
                this.ConfigurationModeReadWrites.Add( enumValue );
        }

        /// <summary> Gets or sets a dictionary of Configuration Mode parses. </summary>
        /// <value> A Dictionary of Configuration Mode parses. </value>
        public Pith.EnumReadWriteCollection ConfigurationModeReadWrites { get; private set; }

        /// <summary> The supported configuration modes. </summary>
        private ConfigurationModes _SupportedConfigurationModes;

        /// <summary> Gets or sets the supported Configuration Modes. </summary>
        /// <value> The supported Configuration Modes. </value>
        public ConfigurationModes SupportedConfigurationModes
        {
            get => this._SupportedConfigurationModes;

            set {
                if ( !this.SupportedConfigurationModes.Equals( value ) )
                {
                    this._SupportedConfigurationModes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The configuration mode. </summary>
        private ConfigurationModes? _ConfigurationMode;

        /// <summary> Gets or sets the cached source ConfigurationMode. </summary>
        /// <value>
        /// The <see cref="ConfigurationMode">source Configuration Mode</see> or none if not set or
        /// unknown.
        /// </value>
        public ConfigurationModes? ConfigurationMode
        {
            get => this._ConfigurationMode;

            protected set {
                if ( !this.ConfigurationMode.Equals( value ) )
                {
                    this._ConfigurationMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Configuration Mode. </summary>
        /// <param name="value"> The  Source Configuration Mode. </param>
        /// <returns>
        /// The <see cref="ConfigurationMode">source Configuration Mode</see> or none if unknown.
        /// </returns>
        public ConfigurationModes? ApplyConfigurationMode( ConfigurationModes value )
        {
            _ = this.WriteConfigurationMode( value );
            return this.QueryConfigurationMode();
        }

        /// <summary> Gets or sets the Configuration Mode query command. </summary>
        /// <remarks> SCPI: :SENS:RES:MODE? </remarks>
        /// <value> The Configuration Mode query command. </value>
        protected virtual string ConfigurationModeQueryCommand { get; set; }

        /// <summary> Queries the Configuration Mode. </summary>
        /// <returns>
        /// The <see cref="ConfigurationMode">Configuration Mode</see> or none if unknown.
        /// </returns>
        public ConfigurationModes? QueryConfigurationMode()
        {
            this.ConfigurationMode = this.Query( this.ConfigurationModeQueryCommand, this.ConfigurationMode.GetValueOrDefault( ConfigurationModes.None ), this.ConfigurationModeReadWrites );
            return this.ConfigurationMode;
        }

        /// <summary> Gets or sets the Configuration Mode command format. </summary>
        /// <remarks> SCPI: :SENS:RES:MODE {0}. </remarks>
        /// <value> The write Configuration Mode command format. </value>
        protected virtual string ConfigurationModeCommandFormat { get; set; }

        /// <summary>
        /// Writes the Configuration Mode without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The Configuration Mode. </param>
        /// <returns>
        /// The <see cref="ConfigurationMode">Configuration Mode</see> or none if unknown.
        /// </returns>
        public ConfigurationModes? WriteConfigurationMode( ConfigurationModes value )
        {
            this.ConfigurationMode = this.Write( this.ConfigurationModeCommandFormat, value, this.ConfigurationModeReadWrites );
            return this.ConfigurationMode;
        }

        #endregion

        #region " DELAY "

        /// <summary> The delay. </summary>
        private TimeSpan? _Delay;

        /// <summary> Gets or sets the cached Trigger Delay. </summary>
        /// <remarks>
        /// The delay is used to delay operation in the trigger layer. After the programmed trigger event
        /// occurs, the instrument waits until the delay period expires before performing the Device
        /// Action.
        /// </remarks>
        /// <value> The Trigger Delay or none if not set or unknown. </value>
        public TimeSpan? Delay
        {
            get => this._Delay;

            protected set {
                if ( !Nullable.Equals( this.Delay, value ) )
                {
                    this._Delay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Delay. </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The Trigger Delay or none if unknown. </returns>
        public TimeSpan? ApplyDelay( TimeSpan value )
        {
            _ = this.WriteDelay( value );
            return this.QueryDelay();
        }

        /// <summary> Gets or sets the delay query command. </summary>
        /// <remarks> SCPI: ":SENS:FRES:DEL?". </remarks>
        /// <value> The delay query command. </value>
        protected virtual string DelayQueryCommand { get; set; }

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <remarks> For example: "s\.FFFFFFF" will convert the result from seconds. </remarks>
        /// <value> The Delay query command. </value>
        protected virtual string DelayFormat { get; set; }

        /// <summary> Queries the Delay. </summary>
        /// <returns> The Delay or none if unknown. </returns>
        public TimeSpan? QueryDelay()
        {
            this.Delay = this.Query( this.Delay, this.DelayFormat, this.DelayQueryCommand );
            return this.Delay;
        }

        /// <summary> Gets or sets the delay command format. </summary>
        /// <remarks> SCPI: ":SENS:FRES:DEL {0:s\.FFFFFFF}". </remarks>
        /// <value> The delay command format. </value>
        protected virtual string DelayCommandFormat { get; set; }

        /// <summary> Writes the Trigger Delay without reading back the value from the device. </summary>
        /// <param name="value"> The current Delay. </param>
        /// <returns> The Trigger Delay or none if unknown. </returns>
        public TimeSpan? WriteDelay( TimeSpan value )
        {
            this.Delay = this.Write( value, this.DelayCommandFormat );
            return this.Delay;
        }

        #endregion

        #region " OPEN LEAD DETECTOR ENABLED "

        /// <summary> Open Lead Detector enabled. </summary>
        private bool? _OpenLeadDetectorEnabled;

        /// <summary> Gets or sets the cached Open Lead Detector Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Open Lead Detector Enabled is not known; <c>True</c> if output is on;
        /// otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? OpenLeadDetectorEnabled
        {
            get => this._OpenLeadDetectorEnabled;

            protected set {
                if ( !Equals( this.OpenLeadDetectorEnabled, value ) )
                {
                    this._OpenLeadDetectorEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Open Lead Detector Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyOpenLeadDetectorEnabled( bool value )
        {
            _ = this.WriteOpenLeadDetectorEnabled( value );
            return this.QueryOpenLeadDetectorEnabled();
        }

        /// <summary> Gets the Open Lead Detector enabled query command. </summary>
        /// <remarks> SCPI: ":FRES:ODET?". </remarks>
        /// <value> The Open Lead Detector enabled query command. </value>
        protected virtual string OpenLeadDetectorEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Open Lead Detector Enabled sentinel. Also sets the
        /// <see cref="OpenLeadDetectorEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryOpenLeadDetectorEnabled()
        {
            this.OpenLeadDetectorEnabled = this.Query( this.OpenLeadDetectorEnabled, this.OpenLeadDetectorEnabledQueryCommand );
            return this.OpenLeadDetectorEnabled;
        }

        /// <summary> Gets the Open Lead Detector enabled command Format. </summary>
        /// <remarks> SCPI: ":FRES:ODET {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Open Lead Detector enabled query command. </value>
        protected virtual string OpenLeadDetectorEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Open Lead Detector Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteOpenLeadDetectorEnabled( bool value )
        {
            this.OpenLeadDetectorEnabled = this.Write( value, this.OpenLeadDetectorEnabledCommandFormat );
            return this.OpenLeadDetectorEnabled;
        }

        /// <summary> Gets the supports open lead detector. </summary>
        /// <value> The supports open lead detector. </value>
        public bool SupportsOpenLeadDetector => !string.IsNullOrEmpty( this.OpenLeadDetectorEnabledCommandFormat );

        #endregion

        #region " POWER LINE CYCLES (NPLC) "

        /// <summary> Gets the power line cycles decimal places. </summary>
        /// <value> The power line decimal places. </value>
        public int PowerLineCyclesDecimalPlaces => ( int ) Math.Max( 0d, 1d - Math.Log10( this.PowerLineCyclesRange.Min ) );

        /// <summary> The power line cycles range. </summary>
        private Core.Primitives.RangeR _PowerLineCyclesRange;

        /// <summary> The Range of the power line cycles. </summary>
        /// <value> The power line cycles range. </value>
        public Core.Primitives.RangeR PowerLineCyclesRange
        {
            get => this._PowerLineCyclesRange;

            set {
                // force a unit change as the value needs to be updated when the subsystem is switched.
                this._PowerLineCyclesRange = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The Power Line Cycles. </summary>
        private double? _PowerLineCycles;

        /// <summary> Gets the integration period. </summary>
        /// <value> The integration period. </value>
        public TimeSpan? IntegrationPeriod => this.PowerLineCycles.HasValue ? StatusSubsystemBase.FromPowerLineCycles( this.PowerLineCycles.Value ) : new TimeSpan?();

        /// <summary>
        /// Gets or sets the cached sense PowerLineCycles. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? PowerLineCycles
        {
            get => this._PowerLineCycles;

            protected set {
                // force a unit change as the value needs to be updated when the subsystem is switched.
                this._PowerLineCycles = value;
                this.Aperture = value.HasValue ? StatusSubsystemBase.FromPowerLineCycles( this.PowerLineCycles.Value ).TotalSeconds : new double?();
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Writes and reads back the sense PowerLineCycles. </summary>
        /// <param name="value"> The Power Line Cycles. </param>
        /// <returns> The Power Line Cycles. </returns>
        public double? ApplyPowerLineCycles( double value )
        {
            _ = this.WritePowerLineCycles( value );
            return this.QueryPowerLineCycles();
        }

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected virtual string PowerLineCyclesQueryCommand { get; set; }

        /// <summary> Queries The Power Line Cycles. </summary>
        /// <returns> The Power Line Cycles or none if unknown. </returns>
        public double? QueryPowerLineCycles()
        {
            this.PowerLineCycles = this.Query( this.PowerLineCycles, this.PowerLineCyclesQueryCommand );
            return this.PowerLineCycles;
        }

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected virtual string PowerLineCyclesCommandFormat { get; set; }

        /// <summary>
        /// Writes The Power Line Cycles without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Power Line Cycles. </remarks>
        /// <param name="value"> The Power Line Cycles. </param>
        /// <returns> The Power Line Cycles. </returns>
        public double? WritePowerLineCycles( double value )
        {
            this.PowerLineCycles = this.Write( value, this.PowerLineCyclesCommandFormat );
            return this.PowerLineCycles;
        }

        #endregion

        #region " PROTECTION LEVEL "

        /// <summary> The Current Limit. </summary>
        private double? _ProtectionLevel;

        /// <summary>
        /// Gets or sets the cached source current Limit for a voltage source. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ProtectionLevel
        {
            get => this._ProtectionLevel;

            protected set {
                if ( !Nullable.Equals( this.ProtectionLevel, value ) )
                {
                    this._ProtectionLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the protection level. </summary>
        /// <param name="value"> the protection level. </param>
        /// <returns> the protection level. </returns>
        public double? ApplyProtectionLevel( double value )
        {
            _ = this.WriteProtectionLevel( value );
            return this.QueryProtectionLevel();
        }

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected virtual string ProtectionLevelQueryCommand { get; set; }

        /// <summary> Queries the protection level. </summary>
        /// <returns> the protection level or none if unknown. </returns>
        public double? QueryProtectionLevel()
        {
            this.ProtectionLevel = this.Query( this.ProtectionLevel, this.ProtectionLevelQueryCommand );
            return this.ProtectionLevel;
        }

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected virtual string ProtectionLevelCommandFormat { get; set; }

        /// <summary>
        /// Writes the protection level without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the protection level. </remarks>
        /// <param name="value"> the protection level. </param>
        /// <returns> the protection level. </returns>
        public double? WriteProtectionLevel( double value )
        {
            this.ProtectionLevel = this.Write( value, this.ProtectionLevelCommandFormat );
            return this.ProtectionLevel;
        }

        #endregion

        #region " PROTECTION ENABLED "

        /// <summary> Protection enabled. </summary>
        private bool? _ProtectionEnabled;

        /// <summary>
        /// Gets or sets a cached value indicating whether Sense Voltage protection is enabled.
        /// </summary>
        /// <remarks>
        /// :SENSE:VOLT:PROT:STAT The setter enables or disables the over-Voltage protection (OCP)
        /// function. The enabled state is On (1); the disabled state is Off (0). If the over-Voltage
        /// protection function is enabled and the output goes into constant Voltage operation, the
        /// output is disabled and OCP is set in the Questionable Condition status register. The *RST
        /// value = Off.
        /// </remarks>
        /// <value>
        /// <c>null</c> if state is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? ProtectionEnabled
        {
            get => this._ProtectionEnabled;

            protected set {
                if ( !Equals( this.ProtectionEnabled, value ) )
                {
                    this._ProtectionEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Protection Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyProtectionEnabled( bool value )
        {
            _ = this.WriteProtectionEnabled( value );
            return this.QueryProtectionEnabled();
        }

        /// <summary> Gets or sets the Protection enabled query command. </summary>
        /// <remarks> SCPI: ":SENSE:PROT:STAT?". </remarks>
        /// <value> The Protection enabled query command. </value>
        protected virtual string ProtectionEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Protection Enabled sentinel. Also sets the
        /// <see cref="ProtectionEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryProtectionEnabled()
        {
            this.ProtectionEnabled = this.Query( this.ProtectionEnabled, this.ProtectionEnabledQueryCommand );
            return this.ProtectionEnabled;
        }

        /// <summary> Gets or sets the Protection enabled command Format. </summary>
        /// <remarks> SCPI: ""SENSE:PROT:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Protection enabled query command. </value>
        protected virtual string ProtectionEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Protection Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteProtectionEnabled( bool value )
        {
            this.ProtectionEnabled = this.Write( value, this.ProtectionEnabledCommandFormat );
            return this.ProtectionEnabled;
        }

        #endregion

        #region " RANGE "

        /// <summary> The range. </summary>
        private double? _Range;

        /// <summary>
        /// Gets or sets the cached range. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? Range
        {
            get => this._Range;

            protected set {
                // force a unit change as the value needs to be updated when the subsystem is switched.
                this._Range = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Writes and reads back the range. </summary>
        /// <param name="value"> The range. </param>
        /// <returns> The range. </returns>
        public double? ApplyRange( double value )
        {
            _ = this.WriteRange( value );
            return this.QueryRange();
        }

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected virtual string RangeQueryCommand { get; set; }

        /// <summary> Queries the range. </summary>
        /// <returns> The range or none if unknown. </returns>
        public double? QueryRange()
        {
            this.Range = this.Query( this.Range, this.RangeQueryCommand );
            return this.Range;
        }

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected virtual string RangeCommandFormat { get; set; }

        /// <summary> Writes the range without reading back the value from the device. </summary>
        /// <remarks> This command sets the range. </remarks>
        /// <param name="value"> The range. </param>
        /// <returns> The range. </returns>
        public double? WriteRange( double value )
        {
            this.Range = this.Write( value, this.RangeCommandFormat );
            return this.Range;
        }

        #endregion

        #region " RESOLUTION DIGITS "

        /// <summary> The resolution digits range. </summary>
        private Core.Primitives.RangeI _ResolutionDigitsRange;

        /// <summary> The resolution digits range in seconds. </summary>
        /// <value> The resolution digits range. </value>
        public Core.Primitives.RangeI ResolutionDigitsRange
        {
            get => this._ResolutionDigitsRange;

            set {
                if ( this.ResolutionDigitsRange != value )
                {
                    this._ResolutionDigitsRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The ResolutionDigits. </summary>
        private double? _ResolutionDigits;

        /// <summary> Gets or sets the cached ResolutionDigits. </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? ResolutionDigits
        {
            get => this._ResolutionDigits;

            protected set {
                if ( !Nullable.Equals( this.ResolutionDigits, value ) )
                {
                    this._ResolutionDigits = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the ResolutionDigits. </summary>
        /// <param name="value"> The ResolutionDigits. </param>
        /// <returns> The ResolutionDigits. </returns>
        public double? ApplyResolutionDigits( double value )
        {
            _ = this.WriteResolutionDigits( value );
            return this.QueryResolutionDigits();
        }

        /// <summary> Gets or sets the ResolutionDigits query command. </summary>
        /// <value> The ResolutionDigits query command. </value>
        protected virtual string ResolutionDigitsQueryCommand { get; set; }

        /// <summary> Queries the ResolutionDigits. </summary>
        /// <returns> The ResolutionDigits or none if unknown. </returns>
        public double? QueryResolutionDigits()
        {
            this.ResolutionDigits = this.Query( this.ResolutionDigits, this.ResolutionDigitsQueryCommand );
            return this.ResolutionDigits;
        }

        /// <summary> Gets or sets the ResolutionDigits command format. </summary>
        /// <value> The ResolutionDigits command format. </value>
        protected virtual string ResolutionDigitsCommandFormat { get; set; }

        /// <summary>
        /// Writes the ResolutionDigits without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets the ResolutionDigits. </remarks>
        /// <param name="value"> The ResolutionDigits. </param>
        /// <returns> The ResolutionDigits. </returns>
        public double? WriteResolutionDigits( double value )
        {
            this.ResolutionDigits = this.Write( value, this.ResolutionDigitsCommandFormat );
            return this.ResolutionDigits;
        }

        #endregion

    }

    /// <summary> Enumerates the configuration mode. </summary>
    [Flags]
    public enum ConfigurationModes
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined ()" )]
        None = 0,

        /// <summary> An enum constant representing the auto] option. </summary>
        [System.ComponentModel.Description( "Auto (AUTO)" )]
        Auto = 1,

        /// <summary> An enum constant representing the manual] option. </summary>
        [System.ComponentModel.Description( "Manual (MAN)" )]
        Manual = 2
    }
}
