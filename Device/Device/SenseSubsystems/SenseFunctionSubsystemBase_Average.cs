using System;

namespace isr.VI
{
    public partial class SenseFunctionSubsystemBase
    {

        #region " AVERAGE COUNT "

        /// <summary> The average count range. </summary>
        private Core.Primitives.RangeI _AverageCountRange;

        /// <summary> The Average Count range in seconds. </summary>
        /// <value> The average count range. </value>
        public Core.Primitives.RangeI AverageCountRange
        {
            get => this._AverageCountRange;

            set {
                if ( this.AverageCountRange != value )
                {
                    this._AverageCountRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The average count. </summary>
        private int? _AverageCount;

        /// <summary>
        /// Gets or sets the cached average count. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public int? AverageCount
        {
            get => this._AverageCount;

            protected set {
                if ( !Nullable.Equals( this.AverageCount, value ) )
                {
                    this._AverageCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the average count. </summary>
        /// <param name="value"> The average count. </param>
        /// <returns> The average count. </returns>
        public int? ApplyAverageCount( int value )
        {
            _ = this.WriteAverageCount( value );
            return this.QueryAverageCount();
        }

        /// <summary> Gets or sets The average count query command. </summary>
        /// <value> The average count query command. </value>
        protected virtual string AverageCountQueryCommand { get; set; }

        /// <summary> Queries The average count. </summary>
        /// <returns> The average count or none if unknown. </returns>
        public int? QueryAverageCount()
        {
            this.AverageCount = this.Query( this.AverageCount, this.AverageCountQueryCommand );
            return this.AverageCount;
        }

        /// <summary> Gets or sets The average count command format. </summary>
        /// <value> The average count command format. </value>
        protected virtual string AverageCountCommandFormat { get; set; }

        /// <summary> Writes The average count without reading back the value from the device. </summary>
        /// <remarks> This command sets The average count. </remarks>
        /// <param name="value"> The average count. </param>
        /// <returns> The average count. </returns>
        public int? WriteAverageCount( int value )
        {
            this.AverageCount = this.Write( value, this.AverageCountCommandFormat );
            return this.AverageCount;
        }

        #endregion

        #region " AVERAGE ENABLED "

        /// <summary> Average enabled. </summary>
        private bool? _AverageEnabled;

        /// <summary> Gets or sets the cached Average Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Average Enabled is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? AverageEnabled
        {
            get => this._AverageEnabled;

            protected set {
                if ( !Equals( this.AverageEnabled, value ) )
                {
                    this._AverageEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Average Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyAverageEnabled( bool value )
        {
            _ = this.WriteAverageEnabled( value );
            return this.QueryAverageEnabled();
        }

        /// <summary> Gets or sets the Average enabled query command. </summary>
        /// <remarks> SCPI: "CURR:AVER:STAT?". </remarks>
        /// <value> The Average enabled query command. </value>
        protected virtual string AverageEnabledQueryCommand { get; set; }

        /// <summary>
        /// Queries the Average Enabled sentinel. Also sets the
        /// <see cref="AverageEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryAverageEnabled()
        {
            this.AverageEnabled = this.Query( this.AverageEnabled, this.AverageEnabledQueryCommand );
            return this.AverageEnabled;
        }

        /// <summary> Gets or sets the Average enabled command Format. </summary>
        /// <remarks> SCPI: "CURR:AVER:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Average enabled query command. </value>
        protected virtual string AverageEnabledCommandFormat { get; set; }

        /// <summary>
        /// Writes the Average Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteAverageEnabled( bool value )
        {
            this.AverageEnabled = this.Write( value, this.AverageEnabledCommandFormat );
            return this.AverageEnabled;
        }

        #endregion

        #region " AVERAGE FILTER TYPE "

        /// <summary> Define average filter types read writes. </summary>
        private void DefineAverageFilterTypesReadWrites()
        {
            this.AverageFilterTypeReadWrites = new Pith.EnumReadWriteCollection();
            foreach ( AverageFilterTypes enumValue in Enum.GetValues( typeof( AverageFilterTypes ) ) )
                this.AverageFilterTypeReadWrites.Add( enumValue );
        }

        /// <summary> Gets or sets a dictionary of Average Filter Type parses. </summary>
        /// <value> A Dictionary of Average Filter Type parses. </value>
        public Pith.EnumReadWriteCollection AverageFilterTypeReadWrites { get; private set; }

        /// <summary> List of types of the supported average filters. </summary>
        private AverageFilterTypes _SupportedAverageFilterTypes;

        /// <summary> Gets or sets the supported Average Filter Types. </summary>
        /// <value> The supported Average Filter Types. </value>
        public AverageFilterTypes SupportedAverageFilterTypes
        {
            get => this._SupportedAverageFilterTypes;

            set {
                if ( !this.SupportedAverageFilterTypes.Equals( value ) )
                {
                    this._SupportedAverageFilterTypes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Type of the average filter. </summary>
        private AverageFilterTypes? _AverageFilterType;

        /// <summary> Gets or sets the cached source AverageFilterType. </summary>
        /// <value>
        /// The <see cref="AverageFilterType">source Average Filter Type</see> or none if not set or
        /// unknown.
        /// </value>
        public AverageFilterTypes? AverageFilterType
        {
            get => this._AverageFilterType;

            protected set {
                if ( !this.AverageFilterType.Equals( value ) )
                {
                    this._AverageFilterType = value;
                    this.NotifyPropertyChanged();
                    this.MovingAverageFilterEnabled = Nullable.Equals( value, AverageFilterTypes.Moving );
                }
            }
        }

        /// <summary> Writes and reads back the source Average Filter Type. </summary>
        /// <param name="value"> The  Source Average Filter Type. </param>
        /// <returns>
        /// The <see cref="AverageFilterType">source Average Filter Type</see> or none if unknown.
        /// </returns>
        public AverageFilterTypes? ApplyAverageFilterType( AverageFilterTypes value )
        {
            _ = this.WriteAverageFilterType( value );
            return this.QueryAverageFilterType();
        }

        /// <summary> Gets or sets the Average Filter Type query command. </summary>
        /// <remarks> SCPI: SENS:CURR:DC:AVER:TCON?". </remarks>
        /// <value> The Average Filter Type query command. </value>
        protected virtual string AverageFilterTypeQueryCommand { get; set; }

        /// <summary> Queries the Average Filter Type. </summary>
        /// <returns>
        /// The <see cref="AverageFilterType">Average Filter Type</see> or none if unknown.
        /// </returns>
        public AverageFilterTypes? QueryAverageFilterType()
        {
            this.AverageFilterType = this.Query( this.AverageFilterTypeQueryCommand, this.AverageFilterType.GetValueOrDefault( AverageFilterTypes.None ), this.AverageFilterTypeReadWrites );
            return this.AverageFilterType;
        }

        /// <summary> Gets or sets the Average Filter Type command format. </summary>
        /// <remarks> SCPI: SENS:CURR:DC:AVER:TCON {0}. </remarks>
        /// <value> The write Average Filter Type command format. </value>
        protected virtual string AverageFilterTypeCommandFormat { get; set; }

        /// <summary>
        /// Writes the Average Filter Type without reading back the value from the device.
        /// </summary>
        /// <param name="value"> The Average Filter Type. </param>
        /// <returns>
        /// The <see cref="AverageFilterType">Average Filter Type</see> or none if unknown.
        /// </returns>
        public AverageFilterTypes? WriteAverageFilterType( AverageFilterTypes value )
        {
            this.AverageFilterType = this.Write( this.AverageFilterTypeCommandFormat, value, this.AverageFilterTypeReadWrites );
            return this.AverageFilterType;
        }

        #endregion

        #region " AVERAGE MOVING FILTER ENABLED "

        /// <summary> Moving Average Filter enabled. </summary>
        private bool? _MovingAverageFilterEnabled;

        /// <summary> Gets or sets the cached Moving Average Filter Enabled sentinel. </summary>
        /// <value>
        /// <c>null</c> if Moving Average Filter Enabled is not known; <c>True</c> if output is on;
        /// otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? MovingAverageFilterEnabled
        {
            get => this._MovingAverageFilterEnabled;

            protected set {
                if ( !Equals( this.MovingAverageFilterEnabled, value ) )
                {
                    this._MovingAverageFilterEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Moving Average Filter Enabled sentinel. </summary>
        /// <param name="value"> if set to <c>True</c> if enabling; False if disabling. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? ApplyMovingAverageFilterEnabled( bool value )
        {
            _ = this.WriteMovingAverageFilterEnabled( value );
            return this.QueryMovingAverageFilterEnabled();
        }

        /// <summary>
        /// Queries the Moving Average Filter Enabled sentinel. Also sets the
        /// <see cref="MovingAverageFilterEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryMovingAverageFilterEnabled()
        {
            this.MovingAverageFilterEnabled = Nullable.Equals( AverageFilterTypes.Moving, this.QueryAverageFilterType() );
            return this.MovingAverageFilterEnabled;
        }

        /// <summary>
        /// Writes the Moving Average Filter Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <param name="value"> if set to <c>True</c> is enabled. </param>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? WriteMovingAverageFilterEnabled( bool value )
        {
            this.MovingAverageFilterEnabled = Nullable.Equals( AverageFilterTypes.Moving, this.WriteAverageFilterType( value ? AverageFilterTypes.Moving : AverageFilterTypes.Repeat ) );
            return this.MovingAverageFilterEnabled;
        }

        #endregion

        #region " AVERAGE PERCENT WINDOW "

        /// <summary> The average percent window range. </summary>
        private Core.Primitives.RangeR _AveragePercentWindowRange;

        /// <summary> The Average Percent Window range. </summary>
        /// <value> The average percent window range. </value>
        public Core.Primitives.RangeR AveragePercentWindowRange
        {
            get => this._AveragePercentWindowRange;

            set {
                if ( this.AveragePercentWindowRange != value )
                {
                    this._AveragePercentWindowRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The Average Percent Window. </summary>
        private double? _AveragePercentWindow;

        /// <summary>
        /// Gets or sets the cached Average Percent Window. Set to
        /// <see cref="VI.Pith.Scpi.Syntax.Infinity">infinity</see> to set to maximum or to
        /// <see cref="VI.Pith.Scpi.Syntax.NegativeInfinity">negative infinity</see> for minimum.
        /// </summary>
        /// <value> <c>null</c> if value is not known. </value>
        public double? AveragePercentWindow
        {
            get => this._AveragePercentWindow;

            protected set {
                if ( !Nullable.Equals( this.AveragePercentWindow, value ) )
                {
                    this._AveragePercentWindow = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Average Percent Window. </summary>
        /// <param name="value"> The Average Percent Window. </param>
        /// <returns> The Average Percent Window. </returns>
        public double? ApplyAveragePercentWindow( double value )
        {
            _ = this.WriteAveragePercentWindow( value );
            return this.QueryAveragePercentWindow();
        }

        /// <summary> Gets or sets The Average Percent Window query command. </summary>
        /// <value> The Average Percent Window query command. </value>
        protected virtual string AveragePercentWindowQueryCommand { get; set; }

        /// <summary> Queries The Average Percent Window. </summary>
        /// <returns> The Average Percent Window or none if unknown. </returns>
        public double? QueryAveragePercentWindow()
        {
            this.AveragePercentWindow = this.Query( this.AveragePercentWindow, this.AveragePercentWindowQueryCommand );
            return this.AveragePercentWindow;
        }

        /// <summary> Gets or sets The Average Percent Window command format. </summary>
        /// <value> The Average Percent Window command format. </value>
        protected virtual string AveragePercentWindowCommandFormat { get; set; }

        /// <summary>
        /// Writes The Average Percent Window without reading back the value from the device.
        /// </summary>
        /// <remarks> This command sets The Average Percent Window. </remarks>
        /// <param name="value"> The Average Percent Window. </param>
        /// <returns> The Average Percent Window. </returns>
        public double? WriteAveragePercentWindow( double value )
        {
            this.AveragePercentWindow = this.Write( value, this.AveragePercentWindowCommandFormat );
            return this.AveragePercentWindow;
        }

        #endregion

    }

    /// <summary> Values that represent average filter types. </summary>
    [Flags]
    public enum AverageFilterTypes
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined ()" )]
        None = 0,

        /// <summary> An enum constant representing the repeat option. </summary>
        [System.ComponentModel.Description( "Repeat (REP)" )]
        Repeat = 1,

        /// <summary> An enum constant representing the moving option. </summary>
        [System.ComponentModel.Description( "Moving (MOV)" )]
        Moving = 2
    }
}
