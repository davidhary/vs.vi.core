﻿using System.Diagnostics;

namespace isr.VI.Tsp2.K2450.Forms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class SourceView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Location = new System.Drawing.Point(13, 17);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(352, 287);
            _Panel.TabIndex = 0;
            // 
            // SourceView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "SourceView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TableLayoutPanel _Layout;
    }
}