using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

namespace isr.VI.Tsp2.K2450.Forms
{

    /// <summary> A measure view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class MeasureView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MeasureView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__OpenDetectorCheckBox.Name = "_OpenDetectorCheckBox";
            this.__FilterEnabledCheckBox.Name = "_FilterEnabledCheckBox";
            this.__SenseFunctionComboBox.Name = "_SenseFunctionComboBox";
            this.__ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton";
        }

        /// <summary> Creates a new <see cref="MeasureView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static MeasureView Create()
        {
            MeasureView view = null;
            try
            {
                view = new MeasureView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( default );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2450Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2450Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindMeasureSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2450Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MEASURE "

        /// <summary> Gets the resistance sense subsystem . </summary>
        /// <value> The Resistance Sense subsystem . </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public MeasureSubsystem MeasureSubsystem { get; private set; }

        /// <summary> Bind Measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindMeasureSubsystem( K2450Device device )
        {
            if ( this.MeasureSubsystem is object )
            {
                this.BindSubsystem( false, this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            if ( device is object )
            {
                this.MeasureSubsystem = device.MeasureSubsystem;
                this.BindSubsystem( true, this.MeasureSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MeasureSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.MeasureSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.SupportedFunctionModes ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.AutoRangeEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.AutoZeroEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FilterCount ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FilterCountRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FilterWindow ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FilterWindowRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.MovingAverageFilterEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FunctionMode ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FunctionRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FunctionRangeDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.FunctionUnit ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.OpenDetectorEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.PowerLineCycles ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.PowerLineCyclesDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.PowerLineCyclesRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K2450.MeasureSubsystem.Range ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.MeasureSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the supported function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void HandleSupportedFunctionModesChanged( MeasureSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.SupportedFunctionModes != SenseFunctionModes.None )
            {
                bool init = this.InitializingComponents;
                try
                {
                    this.InitializingComponents = true;
                    this._SenseFunctionComboBox.ListSupportedSenseFunctionModes( subsystem.SupportedFunctionModes );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    this.InitializingComponents = init;
                }
            }
        }

        /// <summary> Handles the function modes changed described by subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void HandleFunctionModesChanged( MeasureSubsystem subsystem )
        {
            _ = this._SenseFunctionComboBox.SelectSenseFunctionModes( subsystem.FunctionMode.GetValueOrDefault( SenseFunctionModes.VoltageDC ) );
            _ = this.Device.MeasureSubsystem.QueryMeasureUnit();
            _ = this.Device.MeasureSubsystem.QueryOpenDetectorEnabled();
        }

        /// <summary> Handles the measure subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MeasureSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2450.MeasureSubsystem.AutoRangeEnabled ):
                    {
                        if ( subsystem.AutoRangeEnabled.HasValue )
                            this._AutoRangeCheckBox.Checked = subsystem.AutoRangeEnabled.Value;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.AutoZeroEnabled ):
                    {
                        if ( subsystem.AutoZeroEnabled.HasValue )
                            this._AutoZeroCheckBox.Checked = subsystem.AutoZeroEnabled.Value;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FilterCount ):
                    {
                        if ( subsystem.FilterCount.HasValue )
                            this._FilterCountNumeric.Value = subsystem.FilterCount.Value;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FilterCountRange ):
                    {
                        this._FilterCountNumeric.Maximum = subsystem.FilterCountRange.Max;
                        this._FilterCountNumeric.Minimum = subsystem.FilterCountRange.Min;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FilterEnabled ):
                    {
                        if ( subsystem.FilterEnabled.HasValue )
                            this._FilterEnabledCheckBox.Checked = subsystem.FilterEnabled.Value;
                        if ( this._FilterEnabledCheckBox.Checked != this._FilterGroupBox.Enabled )
                            this._FilterGroupBox.Enabled = this._FilterEnabledCheckBox.Checked;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FilterWindow ):
                    {
                        if ( subsystem.FilterWindow.HasValue )
                            this._FilterWindowNumeric.Value = ( decimal ) (100 * subsystem.FilterWindow.Value);
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FilterWindowRange ):
                    {
                        this._FilterWindowNumeric.Maximum = ( decimal ) (100 * subsystem.FilterWindowRange.Max);
                        this._FilterWindowNumeric.Minimum = ( decimal ) (100 * subsystem.FilterWindowRange.Min);
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.MovingAverageFilterEnabled ):
                    {
                        if ( subsystem.MovingAverageFilterEnabled.HasValue )
                            this._MovingAverageRadioButton.Checked = subsystem.MovingAverageFilterEnabled.Value;
                        if ( subsystem.MovingAverageFilterEnabled.HasValue )
                            this._RepeatingAverageRadioButton.Checked = !subsystem.MovingAverageFilterEnabled.Value;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.SupportedFunctionModes ):
                    {
                        this.HandleSupportedFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FunctionMode ):
                    {
                        this.HandleFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FunctionRange ):
                    {
                        _ = this._SenseRangeNumeric.RangeSetter( subsystem.FunctionRange.Min, subsystem.FunctionRange.Max );
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FunctionRangeDecimalPlaces ):
                    {
                        this._SenseRangeNumeric.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.FunctionUnit ):
                    {
                        this._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:";
                        this._SenseRangeNumericLabel.Left = this._SenseRangeNumeric.Left - this._SenseRangeNumericLabel.Width;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.OpenDetectorEnabled ):
                    {
                        if ( subsystem.OpenDetectorEnabled.HasValue )
                            this._OpenDetectorCheckBox.Checked = subsystem.OpenDetectorEnabled.Value;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.PowerLineCycles ):
                    {
                        if ( subsystem.PowerLineCycles.HasValue )
                            this._PowerLineCyclesNumeric.Value = ( decimal ) (subsystem.PowerLineCycles.Value);
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.PowerLineCyclesRange ):
                    {
                        this._PowerLineCyclesNumeric.Maximum = ( decimal ) (subsystem.PowerLineCyclesRange.Max);
                        this._PowerLineCyclesNumeric.Minimum = ( decimal ) (subsystem.PowerLineCyclesRange.Min);
                        this._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces;
                        break;
                    }

                case nameof( K2450.MeasureSubsystem.Range ):
                    {
                        if ( subsystem.Range.HasValue )
                            this.SenseRangeSetter( subsystem.Range.Value );
                        break;
                    }
            }
        }

        /// <summary> measure subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    activity = $"invoking {nameof( this.MeasureSubsystem )}.{e.PropertyName} change";
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeasureSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"handling {nameof( this.MeasureSubsystem )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as MeasureSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE SETTINGS: FUNCTION MODE "

        /// <summary> Gets the selected function mode. </summary>
        /// <value> The selected function mode. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SenseFunctionModes SelectedFunctionMode => this._SenseFunctionComboBox.SelectedSenseFunctionModes();

        #endregion

        #region " CONTROL EVENT HANDLERS: MEASURE "

        /// <summary> Sense range setter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void SenseRangeSetter( double value )
        {
            if ( value <= ( double ) this._SenseRangeNumeric.Maximum && value >= ( double ) this._SenseRangeNumeric.Minimum )
                this._SenseRangeNumeric.Value = ( decimal ) value;
        }

        /// <summary>
        /// Event handler. Called by _SenseFunctionComboBox for selected index changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseFunctionComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} checking function mode";
                var mode = this.SelectedFunctionMode;
                if ( mode == SenseFunctionModes.None )
                    return;
                activity = $"{this.Device.ResourceNameCaption} applying selected function mode {mode}";
                _ = this.Device.MeasureSubsystem.ApplyFunctionMode( mode );
                activity = $"{this.Device.ResourceNameCaption} reading ";
                _ = this.Device.MeasureSubsystem.QueryPowerLineCycles();
                _ = this.Device.MeasureSubsystem.QueryAutoRangeEnabled();
                _ = this.Device.MeasureSubsystem.QueryRange();
                _ = this.Device.MeasureSubsystem.QueryMeasureUnit();
                _ = this.Device.MeasureSubsystem.QueryAutoZeroEnabled();
                _ = this.Device.MeasureSubsystem.QueryFilterEnabled();
                _ = this.Device.MeasureSubsystem.QueryFilterCount();
                _ = this.Device.MeasureSubsystem.QueryFilterWindow();
                _ = this.Device.MeasureSubsystem.QueryMovingAverageFilterEnabled();
                _ = this.Device.MeasureSubsystem.QueryOpenDetectorEnabled();
                this.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the selected measurements settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplySenseSettings()
        {
            if ( !object.Equals( this.Device.MeasureSubsystem.PowerLineCycles, this._PowerLineCyclesNumeric.Value ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyPowerLineCycles( ( double ) this._PowerLineCyclesNumeric.Value );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.AutoRangeEnabled, this._AutoRangeCheckBox.Checked ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyAutoRangeEnabled( this._AutoRangeCheckBox.Checked );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.AutoZeroEnabled, this._AutoZeroCheckBox.Checked ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyAutoZeroEnabled( this._AutoZeroCheckBox.Checked );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.FilterEnabled, this._FilterEnabledCheckBox.Checked ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyFilterEnabled( this._FilterEnabledCheckBox.Checked );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.FilterCount, this._FilterCountNumeric.Value ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyFilterCount( ( int ) Math.Round( this._FilterCountNumeric.Value ) );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.MovingAverageFilterEnabled, this._MovingAverageRadioButton.Checked ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyMovingAverageFilterEnabled( this._MovingAverageRadioButton.Checked );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.OpenDetectorEnabled, this._OpenDetectorCheckBox.Checked ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyOpenDetectorEnabled( this._OpenDetectorCheckBox.Checked );
            }

            if ( (this.Device.MeasureSubsystem.AutoRangeEnabled).GetValueOrDefault( false ) )
            {
                _ = this.Device.MeasureSubsystem.QueryRange();
            }
            else if ( !object.Equals( this.Device.MeasureSubsystem.Range, this._SenseRangeNumeric.Value ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyRange( ( int ) Math.Round( this._SenseRangeNumeric.Value ) );
            }

            if ( !object.Equals( this.Device.MeasureSubsystem.FilterWindow, 0.01d * ( double ) this._FilterWindowNumeric.Value ) )
            {
                _ = this.Device.MeasureSubsystem.ApplyFilterWindow( 0.01d * ( double ) this._FilterWindowNumeric.Value );
            }
        }

        /// <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySenseSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying sense settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.SelectedFunctionMode != this.Device.MeasureSubsystem.FunctionMode.Value )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Info, "Set function first" );
                }
                else
                {
                    this.ApplySenseSettings();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Filter enabled check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FilterEnabledCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._FilterGroupBox.Enabled = this._FilterEnabledCheckBox.Checked;
        }

        /// <summary> Opens detector check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDetectorCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._OpenDetectorCheckBox.Text = $"Open Detector {this._OpenDetectorCheckBox.Checked.GetHashCode():'ON';'ON';'OFF'}";
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
