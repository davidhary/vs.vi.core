using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp2.K2450.Forms
{

    /// <summary> Keithley 2450 Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "K2450 User Interface" )]
    [Description( "Keithley 2450 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( K2450TreeView ) )]
    public class K2450TreeView : Facade.VisaTreeView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K2450TreeView() : base()
        {
            this.InitializingComponents = true;
            this.ReadingView = ReadingView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Read", "Read", this.ReadingView );
            this.MeasureView = MeasureView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Measure", "Measure", this.MeasureView );
            this.SourceView = SourceView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Source", "Source", this.SourceView );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public K2450TreeView( K2450Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary> Gets source view. </summary>
        /// <value> The source view. </value>
        private SourceView SourceView { get; set; }

        /// <summary> Gets the reading view. </summary>
        /// <value> The reading view. </value>
        private ReadingView ReadingView { get; set; }

        /// <summary> Gets the measure view. </summary>
        /// <value> The measure view. </value>
        private MeasureView MeasureView { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the K2450 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.SourceView is object )
                    {
                        this.SourceView.Dispose();
                        this.SourceView = null;
                    }

                    if ( this.ReadingView is object )
                    {
                        this.ReadingView.Dispose();
                        this.ReadingView = null;
                    }

                    if ( this.MeasureView is object )
                    {
                        this.MeasureView.Dispose();
                        this.MeasureView = null;
                    }

                    this.AssignDeviceThis( default );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2450Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( K2450Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = K2450.My.MySettings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( K2450Device value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.ReadingView.AssignDevice( default );
                this.MeasureView.AssignDevice( default );
                this.SourceView.AssignDevice( default );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            this.DisplayView.BindMeasureToolStrip( this.Device.MeasureSubsystem );
            this.DisplayView.BindSubsystemToolStrip( this.Device.SourceSubsystem );
            this.StatusView.ReadTerminalsState = null;
            this.DisplayView.BindTerminalsDisplay( this.Device.MeasureSubsystem );
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.ReadingView.AssignDevice( this.Device );
            this.MeasureView.AssignDevice( this.Device );
            this.SourceView.AssignDevice( this.Device );
            this.DisplayView.BindMeasureToolStrip( this.Device.MeasureSubsystem );
            this.DisplayView.BindSubsystemToolStrip( this.Device.SourceSubsystem );
            this.StatusView.ReadTerminalsState = this.Device.MeasureSubsystem.QueryFrontTerminalsSelected;
            this.DisplayView.BindTerminalsDisplay( this.Device.MeasureSubsystem );
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
