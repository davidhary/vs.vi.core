﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp2.K2450.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadingView));
            _ReadingsDataGridView = new System.Windows.Forms.DataGridView();
            _ReadingToolStrip = new System.Windows.Forms.ToolStrip();
            __ReadButton = new System.Windows.Forms.ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __InitiateButton = new System.Windows.Forms.ToolStripButton();
            __InitiateButton.Click += new EventHandler(InitiateButton_Click);
            _TraceButton = new System.Windows.Forms.ToolStripButton();
            _ReadingsCountLabel = new System.Windows.Forms.ToolStripLabel();
            __ReadingComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReadingComboBox.SelectedIndexChanged += new EventHandler(ReadingComboBox_SelectedIndexChanged);
            _AbortButton = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)_ReadingsDataGridView).BeginInit();
            _ReadingToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ReadingsDataGridView
            // 
            _ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReadingsDataGridView.Location = new System.Drawing.Point(1, 26);
            _ReadingsDataGridView.Name = "_ReadingsDataGridView";
            _ReadingsDataGridView.Size = new System.Drawing.Size(345, 341);
            _ReadingsDataGridView.TabIndex = 25;
            // 
            // _ReadingToolStrip
            // 
            _ReadingToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ReadButton, __InitiateButton, _TraceButton, _ReadingsCountLabel, __ReadingComboBox, _AbortButton });
            _ReadingToolStrip.Location = new System.Drawing.Point(1, 1);
            _ReadingToolStrip.Name = "_ReadingToolStrip";
            _ReadingToolStrip.Size = new System.Drawing.Size(345, 25);
            _ReadingToolStrip.TabIndex = 24;
            _ReadingToolStrip.Text = "ToolStrip1";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(37, 22);
            __ReadButton.Text = "Read";
            __ReadButton.ToolTipText = "Read single reading";
            // 
            // _InitiateButton
            // 
            __InitiateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __InitiateButton.Image = (System.Drawing.Image)resources.GetObject("_InitiateButton.Image");
            __InitiateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __InitiateButton.Name = "__InitiateButton";
            __InitiateButton.Size = new System.Drawing.Size(47, 22);
            __InitiateButton.Text = "Initiate";
            // 
            // _TraceButton
            // 
            _TraceButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TraceButton.Image = (System.Drawing.Image)resources.GetObject("_TraceButton.Image");
            _TraceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _TraceButton.Name = "_TraceButton";
            _TraceButton.Size = new System.Drawing.Size(38, 22);
            _TraceButton.Text = "Trace";
            _TraceButton.ToolTipText = "Reads the buffer";
            // 
            // _ReadingsCountLabel
            // 
            _ReadingsCountLabel.Name = "_ReadingsCountLabel";
            _ReadingsCountLabel.Size = new System.Drawing.Size(13, 22);
            _ReadingsCountLabel.Text = "0";
            _ReadingsCountLabel.ToolTipText = "Buffer count";
            // 
            // _ReadingComboBox
            // 
            __ReadingComboBox.Name = "__ReadingComboBox";
            __ReadingComboBox.Size = new System.Drawing.Size(121, 25);
            __ReadingComboBox.ToolTipText = "Select reading type";
            // 
            // _AbortButton
            // 
            _AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            _AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _AbortButton.Name = "_AbortButton";
            _AbortButton.Size = new System.Drawing.Size(41, 22);
            _AbortButton.Text = "Abort";
            _AbortButton.ToolTipText = "Aborts active trigger";
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_ReadingsDataGridView);
            Controls.Add(_ReadingToolStrip);
            Name = "ReadingView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(347, 368);
            ((System.ComponentModel.ISupportInitialize)_ReadingsDataGridView).EndInit();
            _ReadingToolStrip.ResumeLayout(false);
            _ReadingToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.DataGridView _ReadingsDataGridView;
        private System.Windows.Forms.ToolStrip _ReadingToolStrip;
        private System.Windows.Forms.ToolStripButton __ReadButton;

        private System.Windows.Forms.ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __InitiateButton;

        private System.Windows.Forms.ToolStripButton _InitiateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click -= InitiateButton_Click;
                }

                __InitiateButton = value;
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click += InitiateButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton _TraceButton;
        private System.Windows.Forms.ToolStripLabel _ReadingsCountLabel;
        private System.Windows.Forms.ToolStripComboBox __ReadingComboBox;

        private System.Windows.Forms.ToolStripComboBox _ReadingComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged -= ReadingComboBox_SelectedIndexChanged;
                }

                __ReadingComboBox = value;
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged += ReadingComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton _AbortButton;
    }
}