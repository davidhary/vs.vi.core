﻿
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Tsp2.K2450.Device.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Tsp2.Forms.K2450FormsTests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.K2450Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.Cinco.K2450Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.Morphe.K2450Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Verrzzano.K2450Tests,PublicKey=" + isr.VI.My.SolutionInfo.StrainPublicKey )]
