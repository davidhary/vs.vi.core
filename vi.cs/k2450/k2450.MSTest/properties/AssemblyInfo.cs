﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K2450 VI Tests" )]
[assembly: AssemblyDescription( "K2450 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K2450.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
