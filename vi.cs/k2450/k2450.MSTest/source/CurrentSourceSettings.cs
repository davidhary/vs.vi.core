namespace isr.VI.Tsp2.K2450.Device.MSTest
{

    /// <summary> Current Source Test Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class CurrentSourceSettings : VI.DeviceTests.TestSettingsBase
    {

        #region " SINGLETON "

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private CurrentSourceSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(CurrentSourceSettings)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static CurrentSourceSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static CurrentSourceSettings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (CurrentSourceSettings)Synchronized(new CurrentSourceSettings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " MEASURE SUBSYSTEM INFORMATION "

        /// <summary> Gets or sets the auto zero Enabled settings. </summary>
        /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoZeroEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the automatic range enabled. </summary>
        /// <value> The automatic range enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoRangeEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the Sense Function settings. </summary>
        /// <value> The Sense Function settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("VoltageDC")]
        public SenseFunctionModes SenseFunction
        {
            get
            {
                return AppSettingEnum<SenseFunctionModes>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets the power line cycles settings. </summary>
        /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public double PowerLineCycles
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the remote sense selected. </summary>
        /// <value> The remote sense selected. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool RemoteSenseSelected
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " SOURCE SUBSYSTEM INFORMATION "

        /// <summary> Gets or sets source read back enabled. </summary>
        /// <value> The source read back enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool SourceReadBackEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the front terminals selected. </summary>
        /// <value> The front terminals selected. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool FrontTerminalsSelected
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets source function. </summary>
        /// <value> The source function. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("CurrentDC")]
        public SourceFunctionModes SourceFunction
        {
            get
            {
                return AppSettingEnum<SourceFunctionModes>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets source level. </summary>
        /// <value> The source level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double SourceLevel
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " RESISTANCE INFORMATION "

        /// <summary> Gets or sets the load resistance. </summary>
        /// <value> The load resistance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("100")]
        public double LoadResistance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the measurement tolerance. </summary>
        /// <value> The measurement tolerance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double MeasurementTolerance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}
