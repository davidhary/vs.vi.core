using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp2.K2450.Device.MSTest
{

    /// <summary> K2450 Current Source unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2450" )]
    public class CurrentSourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( testContext.FullyQualifiedTestClassName );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( CurrentSourceSettings.Get().Exists, $"{typeof( CurrentSourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " SOURCE CURRENT MEASURE VOLTAGE "

        /// <summary> Assert source current measure voltage should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertSourceCurrentMeasureVoltageShouldPass( isr.VI.Tsp2.K2450.K2450Device device )
        {
            double expectedPowerLineCycles = CurrentSourceSettings.Get().PowerLineCycles;
            double actualPowerLineCycles = device.MeasureSubsystem.ApplyPowerLineCycles( expectedPowerLineCycles ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, device.StatusSubsystem.LineFrequency.Value / TimeSpan.TicksPerSecond, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.PowerLineCycles )} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}" );
            bool expectedBoolean = CurrentSourceSettings.Get().AutoRangeEnabled;
            bool actualBoolean = device.MeasureSubsystem.ApplyAutoRangeEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.AutoRangeEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            expectedBoolean = CurrentSourceSettings.Get().AutoZeroEnabled;
            actualBoolean = device.MeasureSubsystem.ApplyAutoZeroEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.AutoZeroEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            expectedBoolean = CurrentSourceSettings.Get().FrontTerminalsSelected;
            actualBoolean = device.MeasureSubsystem.ApplyFrontTerminalsSelected( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.FrontTerminalsSelected )} is {actualBoolean}; expected {expectedBoolean}" );
            var expectedFunctionMode = CurrentSourceSettings.Get().SourceFunction;
            SourceFunctionModes SourceFunction = device.SourceSubsystem.ApplyFunctionMode( expectedFunctionMode ).GetValueOrDefault( SourceFunctionModes.None );
            Assert.AreEqual( expectedFunctionMode, SourceFunction, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.FunctionMode )} is {SourceFunction} ; expected {expectedFunctionMode}" );
            var expectedMeasureFunctionMode = CurrentSourceSettings.Get().SenseFunction;
            SenseFunctionModes measureFunction = device.MeasureSubsystem.ApplyFunctionMode( expectedMeasureFunctionMode ).GetValueOrDefault( SenseFunctionModes.Resistance );
            Assert.AreEqual( expectedMeasureFunctionMode, measureFunction, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.FunctionMode )} is {measureFunction} ; expected {expectedMeasureFunctionMode}" );
            expectedBoolean = CurrentSourceSettings.Get().RemoteSenseSelected;
            actualBoolean = device.MeasureSubsystem.ApplyRemoteSenseSelected( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.RemoteSenseSelected )} is {actualBoolean}; expected {expectedBoolean}" );
            double expectedSourceLevel = CurrentSourceSettings.Get().SourceLevel;
            double actualSourceLevel = device.SourceSubsystem.ApplyLevel( expectedSourceLevel ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedSourceLevel, actualSourceLevel, 0.000001d, $"{typeof( MeasureSubsystemBase )}.{nameof( SourceSubsystemBase.Level )} is {actualSourceLevel:G5}; expected {expectedSourceLevel:G5}" );
            double expectedSourceLimit = 2d * CurrentSourceSettings.Get().SourceLevel * CurrentSourceSettings.Get().LoadResistance;
            double actualSourceLimit = device.SourceSubsystem.ApplyLimit( expectedSourceLimit ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedSourceLimit, actualSourceLimit, 0.000001d, $"{typeof( MeasureSubsystemBase )}.{nameof( SourceSubsystemBase.Limit )} is {actualSourceLimit:G5}; expected {expectedSourceLimit:G5}" );
            expectedBoolean = CurrentSourceSettings.Get().SourceReadBackEnabled;
            actualBoolean = device.SourceSubsystem.ApplyReadBackEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.ReadBackEnabled )} is {actualBoolean}; expected {expectedBoolean}" );

            // set the reading to display voltage units
            device.MeasureSubsystem.ReadingAmounts.PrimaryReading.ApplyUnit( device.MeasureSubsystem.FunctionUnit );

            // turn on the output
            VI.DeviceTests.DeviceManager.AssertOutputEnabledSouldToggle( device.SourceSubsystem, true );
            for ( int i = 1; i <= 2; i++ )
            {
                double measuredVoltage = device.MeasureSubsystem.MeasurePrimaryReading().GetValueOrDefault( -1 );
                double expectedVoltage = CurrentSourceSettings.Get().SourceLevel * CurrentSourceSettings.Get().LoadResistance;
                double epsilon = expectedVoltage * CurrentSourceSettings.Get().MeasurementTolerance;
                Assert.AreEqual( expectedVoltage, measuredVoltage, epsilon, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.PrimaryReadingValue )} is {measuredVoltage}; expected {expectedVoltage} within {epsilon}" );
                if ( CurrentSourceSettings.Get().SourceReadBackEnabled )
                {
                    // read back the last source value
                    double measuredCurrent = device.SourceSubsystem.ParseReadBackBufferAmount();
                    double expectedCurrent = CurrentSourceSettings.Get().SourceLevel;
                    epsilon = expectedCurrent * CurrentSourceSettings.Get().MeasurementTolerance;
                    Assert.AreEqual( expectedCurrent, measuredCurrent, epsilon, $"{typeof( MeasureSubsystemBase )}.{nameof( SourceSubsystemBase.ReadBackAmount )} is {measuredCurrent}; expected {expectedCurrent} within {epsilon}" );
                }
            }

            // turn off the output
            VI.DeviceTests.DeviceManager.AssertOutputEnabledSouldToggle( device.SourceSubsystem, false );
        }

        /// <summary> (Unit Test Method) source current measure voltage should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SourceCurrentMeasureVoltageShouldPass()
        {
            using isr.VI.Tsp2.K2450.K2450Device device = K2450.K2450Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
            try
            {
                AssertSourceCurrentMeasureVoltageShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertOutputEnabledSouldToggle( device.SourceSubsystem, false );
            }

            DeviceManager.CloseSession( TestInfo, device );
        }

        #endregion

    }
}
