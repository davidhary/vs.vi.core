using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp2.K2450.Device.MSTest
{

    /// <summary> K2450 Subsystems unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2450" )]
    public class SubsystemsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( testContext.FullyQualifiedTestClassName );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( SubsystemsSettings.Get().Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " STATUS SUSBSYSTEM "

        /// <summary> Assert session open check status should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
        /// <param name="resourceInfo">     Information describing the resource. </param>
        /// <param name="subsystemsInfo">   Information describing the subsystems. </param>
        private static void AssertSessionOpenCheckStatusShouldPass( bool readErrorEnabled, ResourceSettings resourceInfo, SubsystemsSettings subsystemsInfo )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using isr.VI.Tsp2.K2450.K2450Device device = K2450.K2450Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            VI.DeviceTests.DeviceManager.AssertSessionInitialValuesShouldMatch( device.Session, resourceInfo, subsystemsInfo );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertDeviceModelShouldMatch( device.StatusSubsystemBase, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldMatch( device.StatusSubsystemBase, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertTerminationValuesShouldMatch( device.Session, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertLineFrequencyShouldMatch( device.StatusSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertIntegrationPeriodShouldMatch( device.StatusSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.MeasureSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.SourceSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSessionDeviceErrorsShouldClear( device, subsystemsInfo );
                if ( readErrorEnabled )
                    VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldRead( device, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertOrphanMessagesShouldBeEmpty( device.StatusSubsystemBase );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) session open check status should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionOpenCheckStatusShouldPass()
        {
            AssertSessionOpenCheckStatusShouldPass( false, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        /// <summary>
        /// (Unit Test Method) session open check status device errors should pass.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionOpenCheckStatusDeviceErrorsShouldPass()
        {
            AssertSessionOpenCheckStatusShouldPass( true, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        #endregion

        #region " MEASURE SUBSYSTEM TEST "

        /// <summary> Assert measure subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertMeasureSubsystemInfoShouldPass( isr.VI.Tsp2.K2450.K2450Device device )
        {
            double expectedPowerLineCycles = SubsystemsSettings.Get().InitialPowerLineCycles;
            double actualPowerLineCycles = device.MeasureSubsystem.QueryPowerLineCycles().GetValueOrDefault( 0 );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, SubsystemsSettings.Get().LineFrequency / TimeSpan.TicksPerSecond, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.PowerLineCycles )} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}" );
            _ = SubsystemsSettings.Get().InitialAutoRangeEnabled;
            bool actualBoolean = device.MeasureSubsystem.QueryAutoRangeEnabled().GetValueOrDefault( false );
            Assert.IsTrue( actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.AutoRangeEnabled )} is {actualBoolean}; expected {true}" );
            _ = SubsystemsSettings.Get().InitialAutoZeroEnabled;
            actualBoolean = device.MeasureSubsystem.QueryAutoZeroEnabled().GetValueOrDefault( false );
            Assert.IsTrue( actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.AutoZeroEnabled )} is {actualBoolean}; expected {true}" );
            bool expectedBoolean = SubsystemsSettings.Get().InitialFrontTerminalsSelected;
            actualBoolean = device.MeasureSubsystem.QueryFrontTerminalsSelected().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.FrontTerminalsSelected )} is {actualBoolean}; expected {expectedBoolean}" );
            expectedBoolean = SubsystemsSettings.Get().InitialRemoteSenseSelected;
            actualBoolean = device.MeasureSubsystem.QueryRemoteSenseSelected().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.RemoteSenseSelected )} is {actualBoolean}; expected {expectedBoolean}" );
            SenseFunctionModes senseFn = device.MeasureSubsystem.QueryFunctionMode().GetValueOrDefault( SenseFunctionModes.Resistance );
            var expectedFunctionMode = SubsystemsSettings.Get().InitialSenseFunction;
            Assert.AreEqual( expectedFunctionMode, senseFn, $"{typeof( MeasureSubsystemBase )}.{nameof( MeasureSubsystemBase.FunctionMode )} is {senseFn} ; expected {expectedFunctionMode}" );
        }

        /// <summary> (Unit Test Method) measure subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MeasureSubsystemInfoShouldPass()
        {
            using isr.VI.Tsp2.K2450.K2450Device device = K2450.K2450Device.Create();
            try
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertMeasureSubsystemInfoShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " SOURCE SUBSYSTEM TEST "

        /// <summary> Assert source subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertSourceSubsystemInfoShouldPass( isr.VI.Tsp2.K2450.K2450Device device )
        {
            bool expectedBoolean = SubsystemsSettings.Get().InitialAutoRangeEnabled;
            bool actualBoolean = device.SourceSubsystem.QueryAutoRangeEnabled().GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.AutoRangeEnabled )} is {actualBoolean}; expected {true}" );
            expectedBoolean = SubsystemsSettings.Get().InitialAutoDelayEnabled;
            actualBoolean = device.SourceSubsystem.QueryAutoDelayEnabled().GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.AutoDelayEnabled )} is {actualBoolean}; expected {true}" );
            expectedBoolean = false;
            actualBoolean = device.SourceSubsystem.QueryOutputEnabled().GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.OutputEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            SourceFunctionModes functionMode = device.SourceSubsystem.QueryFunctionMode().GetValueOrDefault( SourceFunctionModes.None );
            var expectedFunctionMode = SubsystemsSettings.Get().InitialSourceFunction;
            Assert.AreEqual( expectedFunctionMode, functionMode, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.FunctionMode )} is {functionMode} ; expected {expectedFunctionMode}" );
            expectedBoolean = false;
            actualBoolean = device.SourceSubsystem.QueryLimitTripped().GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.OutputEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            double expectedDouble = SubsystemsSettings.Get().InitialSourceLevel;
            double actualDouble = device.SourceSubsystem.QueryLevel().GetValueOrDefault( -1 );
            Assert.AreEqual( expectedDouble, actualDouble, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.Level )} is {actualDouble}; expected {expectedDouble}" );
            expectedDouble = SubsystemsSettings.Get().InitialSourceLimit;
            actualDouble = device.SourceSubsystem.QueryLimit().GetValueOrDefault( -1 );
            Assert.AreEqual( expectedDouble, actualDouble, $"{typeof( SourceSubsystemBase )}.{nameof( SourceSubsystemBase.Limit )} is {actualDouble}; expected {expectedDouble}" );
        }

        /// <summary> (Unit Test Method) source subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SourceSubsystemInfoShouldPass()
        {
            using isr.VI.Tsp2.K2450.K2450Device device = K2450.K2450Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertSourceSubsystemInfoShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
