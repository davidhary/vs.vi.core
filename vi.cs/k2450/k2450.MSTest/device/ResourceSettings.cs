namespace isr.VI.Tsp2.K2450.Device.MSTest
{

    /// <summary> The Keithley 2450 Resource Settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode( "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0" )]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
    internal class ResourceSettings : VI.DeviceTests.ResourceSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private ResourceSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( $"{typeof( ResourceSettings )} Editor", Get() );
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static ResourceSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static ResourceSettings Get()
        {
            if ( _Instance is null )
            {
                lock ( _SyncLocker )
                    _Instance = ( ResourceSettings ) Synchronized( new ResourceSettings() );
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( _SyncLocker )
                    return _Instance is object;
            }
        }

        #endregion


        #region " DEVICE RESOURCE INFORMATION "

        /// <summary> Gets or sets the Model of the resource. </summary>
        /// <value> The Model of the resource. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "2450" )]
        public override string ResourceModel
        {
            get {
                return base.ResourceModel;
            }

            set {
                base.ResourceModel = value;
            }
        }

        /// <summary> Gets or sets the names of the candidate resources. </summary>
        /// <value> The names of the candidate resources. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( @"TCPIP0::192.168.0.152::inst0::INSTR|TCPIP0::192.168.0.156::inst0::INSTR|
TCPIP0::10.1.1.25::inst0::INSTR|TCPIP0::10.1.1.26::inst0::INSTR" )]
        public override string ResourceNames
        {
            get {
                return base.ResourceNames;
            }

            set {
                base.ResourceNames = value;
            }
        }

        /// <summary> Gets or sets the Title of the resource. </summary>
        /// <value> The Title of the resource. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "2450" )]
        public override string ResourceTitle
        {
            get {
                return base.ResourceTitle;
            }

            set {
                base.ResourceTitle = value;
            }
        }

        /// <summary> Gets or sets the language. </summary>
        /// <value> The language. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "TSP" )]
        public override string Language
        {
            get {
                return base.Language;
            }

            set {
                base.Language = value;
            }
        }

        #endregion

    }

    internal static class K2450ResourceSettingsProperty
    {

        /// <summary> Gets the 2450 resource settings. </summary>
        /// <value> The k 2450 resource settings. </value>
        internal static ResourceSettings K2450ResourceSettings => ResourceSettings.Get();
    }
}
