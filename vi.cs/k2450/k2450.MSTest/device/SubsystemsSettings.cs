namespace isr.VI.Tsp2.K2450.Device.MSTest
{

    /// <summary> The Subsystems Test Information. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode( "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0" )]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
    internal class SubsystemsSettings : VI.DeviceTests.SubsystemsSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private SubsystemsSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( $"{typeof( SubsystemsSettings )} Editor", Get() );
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static SubsystemsSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static SubsystemsSettings Get()
        {
            if ( _Instance is null )
            {
                lock ( _SyncLocker )
                    _Instance = ( SubsystemsSettings ) Synchronized( new SubsystemsSettings() );
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( _SyncLocker )
                    return _Instance is object;
            }
        }

        #endregion

        #region " DEVICE SESSION INFORMATION "

        /// <summary> Gets or sets the keep alive query command. </summary>
        /// <value> The keep alive query command. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "*OPC?" )]
        public override string KeepAliveQueryCommand
        {
            get {
                return base.KeepAliveQueryCommand;
            }

            set {
                base.KeepAliveQueryCommand = value;
            }
        }

        /// <summary> Gets or sets the keep-alive command. </summary>
        /// <value> The keep-alive command. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "*OPC" )]
        public override string KeepAliveCommand
        {
            get {
                return base.KeepAliveCommand;
            }

            set {
                base.KeepAliveCommand = value;
            }
        }

        /// <summary> Gets or sets the initial read termination enabled. </summary>
        /// <value> The initial read termination enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public override bool InitialReadTerminationEnabled
        {
            get {
                return base.InitialReadTerminationEnabled;
            }

            set {
                base.InitialReadTerminationEnabled = value;
            }
        }

        /// <summary> Gets or sets the initial read termination character. </summary>
        /// <value> The initial read termination character. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public override int InitialReadTerminationCharacter
        {
            get {
                return base.InitialReadTerminationCharacter;
            }

            set {
                base.InitialReadTerminationCharacter = value;
            }
        }

        /// <summary> Gets or sets the read termination enabled. </summary>
        /// <value> The read termination enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public override bool ReadTerminationEnabled
        {
            get {
                return base.ReadTerminationEnabled;
            }

            set {
                base.ReadTerminationEnabled = value;
            }
        }

        /// <summary> Gets or sets the read termination character. </summary>
        /// <value> The read termination character. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public override int ReadTerminationCharacter
        {
            get {
                return base.ReadTerminationCharacter;
            }

            set {
                base.ReadTerminationCharacter = value;
            }
        }

        #endregion

        #region " STATUS SUBSYSTEM INFORMATION "

        /// <summary> Gets or sets the Initial power line cycles settings. </summary>
        /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1" )]
        public override double InitialPowerLineCycles
        {
            get {
                return base.InitialPowerLineCycles;
            }

            set {
                base.InitialPowerLineCycles = value;
            }
        }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the erroneous command. </summary>
        /// <value> The erroneous command. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "*CLL" )]
        public override string ErroneousCommand
        {
            get {
                return base.ErroneousCommand;
            }

            set {
                base.ErroneousCommand = value;
            }
        }

        /// <summary> Gets or sets the error available milliseconds delay. </summary>
        /// <value> The error available milliseconds delay. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public override int ErrorAvailableMillisecondsDelay
        {
            get {
                return base.ErrorAvailableMillisecondsDelay;
            }

            set {
                base.ErrorAvailableMillisecondsDelay = value;
            }
        }

        /// <summary> Gets or sets a message describing the expected compound error. </summary>
        /// <value> A message describing the expected compound error. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "-285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20" )]
        public override string ExpectedCompoundErrorMessage
        {
            get {
                return base.ExpectedCompoundErrorMessage;
            }

            set {
                base.ExpectedCompoundErrorMessage = value;
            }
        }

        /// <summary> Gets or sets a message describing the expected error. </summary>
        /// <value> A message describing the expected error. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "-285" )]
        public override string ExpectedErrorMessage
        {
            get {
                return base.ExpectedErrorMessage;
            }

            set {
                base.ExpectedErrorMessage = value;
            }
        }

        /// <summary> Gets or sets the expected error number. </summary>
        /// <value> The expected error number. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "TSP Syntax error at line 1: unexpected symbol near `*'" )]
        public override int ExpectedErrorNumber
        {
            get {
                return base.ExpectedErrorNumber;
            }

            set {
                base.ExpectedErrorNumber = value;
            }
        }

        /// <summary> Gets or sets the expected error level. </summary>
        /// <value> The expected error level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "20" )]
        public override int ExpectedErrorLevel
        {
            get {
                return base.ExpectedErrorLevel;
            }

            set {
                base.ExpectedErrorLevel = value;
            }
        }

        #endregion

        #region " SOURCE MEASURE UNIT INFORMATION "

        /// <summary> Gets or sets the maximum output power of the instrument. </summary>
        /// <value> The maximum output power . </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public override double MaximumOutputPower
        {
            get {
                return base.MaximumOutputPower;
            }

            set {
                base.MaximumOutputPower = value;
            }
        }

        /// <summary> Gets or sets the line frequency. </summary>
        /// <value> The line frequency. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "60" )]
        public override double LineFrequency
        {
            get {
                return base.LineFrequency;
            }

            set {
                base.LineFrequency = value;
            }
        }

        #endregion

        #region " SENSE SUBSYSTEM INFORMATION "

        /// <summary> Gets or sets the Initial auto Range enabled settings. </summary>
        /// <value> The auto Range settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public override bool InitialAutoRangeEnabled
        {
            get {
                return base.InitialAutoRangeEnabled;
            }

            set {
                base.InitialAutoRangeEnabled = value;
            }
        }

        /// <summary> Gets or sets the Initial auto zero Enabled settings. </summary>
        /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public override bool InitialAutoZeroEnabled
        {
            get {
                return base.InitialAutoZeroEnabled;
            }

            set {
                base.InitialAutoZeroEnabled = value;
            }
        }

        /// <summary> Gets or sets the initial multimeter sense function. </summary>
        /// <value> The initial multimeter sense function. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "VoltageDC" )]
        public override MultimeterFunctionModes InitialMultimeterFunction
        {
            get {
                return base.InitialMultimeterFunction;
            }

            set {
                base.InitialMultimeterFunction = value;
            }
        }

        /// <summary> Gets or sets the initial sense function. </summary>
        /// <value> The initial sense function. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "CurrentDC" )]
        public override SenseFunctionModes InitialSenseFunction
        {
            get {
                return base.InitialSenseFunction;
            }

            set {
                base.InitialSenseFunction = value;
            }
        }

        /// <summary> Gets or sets the initial filter enabled. </summary>
        /// <value> The initial filter enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public override bool InitialFilterEnabled
        {
            get {
                return base.InitialFilterEnabled;
            }

            set {
                base.InitialFilterEnabled = value;
            }
        }

        /// <summary> Gets or sets the initial moving average filter enabled. </summary>
        /// <value> The initial moving average filter enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "False" )]
        public override bool InitialMovingAverageFilterEnabled
        {
            get {
                return base.InitialMovingAverageFilterEnabled;
            }

            set {
                base.InitialMovingAverageFilterEnabled = value;
            }
        }

        /// <summary> Gets or sets the number of initial filters. </summary>
        /// <value> The number of initial filters. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public override int InitialFilterCount
        {
            get {
                return base.InitialFilterCount;
            }

            set {
                base.InitialFilterCount = value;
            }
        }

        /// <summary> Gets or sets the initial filter window. </summary>
        /// <value> The initial filter window. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0.001" )]
        public override double InitialFilterWindow
        {
            get {
                return base.InitialFilterWindow;
            }

            set {
                base.InitialFilterWindow = value;
            }
        }

        /// <summary> Gets or sets the initial remote sense selected. </summary>
        /// <value> The initial remote sense selected. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public override bool InitialRemoteSenseSelected
        {
            get {
                return base.InitialRemoteSenseSelected;
            }

            set {
                base.InitialRemoteSenseSelected = value;
            }
        }
        #endregion

        #region " SOURCE SUBSYSTEM INFORMATION "

        /// <summary> Gets or sets the Initial auto Delay Enabled settings. </summary>
        /// <value> The auto Delay settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public override bool InitialAutoDelayEnabled
        {
            get {
                return base.InitialAutoDelayEnabled;
            }

            set {
                base.InitialAutoDelayEnabled = value;
            }
        }

        /// <summary> Gets or sets the initial source function mode. </summary>
        /// <value> The initial source function mode. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "VoltageDC" )]
        public override SourceFunctionModes InitialSourceFunction
        {
            get {
                return base.InitialSourceFunction;
            }

            set {
                base.InitialSourceFunction = value;
            }
        }

        /// <summary> Gets or sets the initial front terminals selected. </summary>
        /// <value> The initial front terminals selected. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public override bool InitialFrontTerminalsSelected
        {
            get {
                return base.InitialFrontTerminalsSelected;
            }

            set {
                base.InitialFrontTerminalsSelected = value;
            }
        }

        /// <summary> Gets or sets the initial source level. </summary>
        /// <value> The initial source level. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0" )]
        public override double InitialSourceLevel
        {
            get {
                return base.InitialSourceLevel;
            }

            set {
                base.InitialSourceLevel = value;
            }
        }

        /// <summary> Gets or sets the initial source limit. </summary>
        /// <value> The initial source limit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "0.000105" )]
        public override double InitialSourceLimit
        {
            get {
                return base.InitialSourceLimit;
            }

            set {
                base.InitialSourceLimit = value;
            }
        }


        #endregion

    }

    internal static class K2450SubsystemSettingsProperty
    {

        /// <summary> Gets the 2405 subsystems settings. </summary>
        /// <value> The k 2405 subsystems settings. </value>
        internal static SubsystemsSettings K2405SubsystemsSettings => SubsystemsSettings.Get();
    }
}
