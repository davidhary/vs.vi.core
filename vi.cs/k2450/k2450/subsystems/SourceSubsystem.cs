namespace isr.VI.Tsp2.K2450
{

    /// <summary> Source subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-03 </para>
    /// </remarks>
    public class SourceSubsystem : SourceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SourceSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public SourceSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.FunctionModeRanges[( int ) SourceFunctionModes.CurrentDC].SetRange( -1.05d, 1.05d );
            this.FunctionModeRanges[( int ) SourceFunctionModes.VoltageDC].SetRange( -210, 210d );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Level = 0;
            this.Limit = 0.000105d;
            this.Range = new double?();
            this.AutoRangeEnabled = true;
            this.AutoDelayEnabled = true;
            this.FunctionMode = SourceFunctionModes.VoltageDC;
            this.Range = 0.02d;
            this.LimitTripped = false;
            this.OutputEnabled = false;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO DELAY ENABLED "

        /// <summary> Gets or sets the automatic Delay enabled command Format. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected override string AutoDelayEnabledCommandFormat { get; set; } = "_G.smu.source.autodelay={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the automatic Delay enabled query command. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected override string AutoDelayEnabledQueryCommand { get; set; } = "_G.print(_G.smu.source.autodelay==smu.ON)";

        #endregion

        #region " AUTO RANGE STATE "

        /// <summary> The Auto Range state command format. </summary>
        /// <value> The automatic range state command format. </value>
        protected override string AutoRangeStateCommandFormat { get; set; } = "_G.smu.source.autorange={0}";

        /// <summary> Gets or sets the Auto Range state query command. </summary>
        /// <value> The AutoRange state query command. </value>
        protected override string AutoRangeStateQueryCommand { get; set; } = Tsp.Syntax.Lua.PrintCommand( "_G.smu.source.autorange" );

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = "_G.smu.source.autorange={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = "_G.print(_G.smu.source.autorange==smu.ON)";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the function mode query command. </summary>
        /// <value> The function mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = Tsp.Syntax.Lua.PrintCommand( "_G.smu.source.func" );

        /// <summary> Gets or sets the function mode command format. </summary>
        /// <value> The function mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = "_G.smu.source.func={0}";

        #endregion

        #region " LEVEL "

        /// <summary> Gets or sets the level query command. </summary>
        /// <value> The level query command. </value>
        protected override string LevelQueryCommand { get; set; } = "_G.print(_G.smu.source.level)";

        /// <summary> Gets or sets the level command format. </summary>
        /// <value> The level command format. </value>
        protected override string LevelCommandFormat { get; set; } = "_G.smu.source.level={0}";

        #endregion

        #region " LIMIT "

        /// <summary> Gets or sets the limit query format. </summary>
        /// <value> The limit query format. </value>
        protected override string LimitQueryFormat { get; set; } = "_G.smu.source.{0}limit.level";

        /// <summary> Gets or sets the limit command format. </summary>
        /// <value> The limit command format. </value>
        protected override string LimitCommandFormat { get; set; } = "_G.smu.source.{0}limit.level={1}";

        #endregion

        #region " LIMIT TRIPPED "

        /// <summary> Gets or sets the limit tripped query command format. </summary>
        /// <value> The limit tripped query command format. </value>
        protected override string LimitTrippedQueryCommandFormat { get; set; } = "_G.print(_G.smu.source.{0}limit.tripped==smu.ON)";

        #endregion

        #region " OUTPUT ENABLED "

        /// <summary> Gets or sets the Output enabled command Format. </summary>
        /// <value> The Output enabled query command. </value>
        protected override string OutputEnabledCommandFormat { get; set; } = "_G.smu.source.output={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the Output enabled query print command. </summary>
        /// <value> The Output enabled query command. </value>
        protected override string OutputEnabledQueryCommand { get; set; } = "_G.print(_G.smu.source.output==smu.ON)";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the Range query command. </summary>
        /// <value> The Range query command. </value>
        protected override string RangeQueryCommand { get; set; } = "_G.print(_G.smu.source.range)";

        /// <summary> Gets or sets the Range command format. </summary>
        /// <value> The Range command format. </value>
        protected override string RangeCommandFormat { get; set; } = "_G.smu.source.range={0}";

        #endregion

        #region " READ BACK ENABLED "

        /// <summary> Gets or sets the Read Back enabled command Format. </summary>
        /// <value> The Read Back enabled query command. </value>
        protected override string ReadBackEnabledCommandFormat { get; set; } = "_G.smu.source.readback={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the Read Back enabled query command. </summary>
        /// <value> The Read Back enabled query command. </value>
        protected override string ReadBackEnabledQueryCommand { get; set; } = "_G.print(_G.smu.source.readback==smu.ON)";

        #endregion

        #endregion

    }
}
