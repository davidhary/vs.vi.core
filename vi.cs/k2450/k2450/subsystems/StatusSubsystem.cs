using System;

namespace isr.VI.Tsp2.K2450
{

    /// <summary> Status subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> The session. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( session )
        {
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Define measurement event bitmasks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( MeasurementEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            int failuresSummaryBitmask = 0;
            int bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.Questionable, 1 << 0 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.Origin, 1 << 1 | 1 << 2 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.MeasurementTerminal, 1 << 3 );
            bitmask = 1 << 4;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit2, bitmask );
            bitmask = 1 << 5;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit2, bitmask );
            bitmask = 1 << 6;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit1, bitmask );
            bitmask = 1 << 7;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit1, bitmask );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.FirstReadingInGroup, 1 << 8 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.FailuresSummary, failuresSummaryBitmask );
        }

        /// <summary> Define measurement event bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineMeasurementEventsBitmasks()
        {
            base.DefineMeasurementEventsBitmasks();
            DefineBitmasks( this.MeasurementEventsBitmasks );
        }

        #endregion

    }
}
