namespace isr.VI.Tsp2.K2450
{

    /// <summary> Measure subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-03 </para>
    /// </remarks>
    public class MeasureSubsystem : MeasureSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MeasureSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ampere );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere;
            this.SupportedFunctionModes = SenseFunctionModes.CurrentDC | SenseFunctionModes.VoltageDC | SenseFunctionModes.Resistance;
            this.FunctionModeRanges[( int ) SenseFunctionModes.CurrentDC].SetRange( 0d, 1d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.VoltageDC].SetRange( 0d, 200d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.Resistance].SetRange( 0d, 200000000.0d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.ResistanceFourWire].SetRange( 0d, 200000000.0d );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.ApertureRange = new Core.Primitives.RangeR( 0.000166667d, 0.166667d );
            this.FilterCountRange = new Core.Primitives.RangeI( 1, 100 );
            this.FilterWindowRange = new Core.Primitives.RangeR( 0d, 0.1d );
            this.PowerLineCyclesRange = new Core.Primitives.RangeR( 0.01d, 10d );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.PowerLineCycles = 1;
            this.AutoRangeState = OnOffState.On;
            this.AutoZeroEnabled = true;
            this.FilterCount = 10;
            this.FilterEnabled = false;
            this.MovingAverageFilterEnabled = false;
            this.OpenDetectorEnabled = false;
            this.FilterWindow = 0.001d;
            this.FunctionMode = SenseFunctionModes.CurrentDC;
            this.Range = 0.000001d;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " APERTURE "

        /// <summary> Gets or sets the Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string ApertureQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected override string ApertureCommandFormat { get; set; } = string.Empty;

        #endregion

        #region " AUTO RANGE STATE "

        /// <summary> The Auto Range state command format. </summary>
        /// <value> The automatic range state command format. </value>
        protected override string AutoRangeStateCommandFormat { get; set; } = "_G.smu.measure.autorange={0}";

        /// <summary> Gets or sets the Auto Range state query command. </summary>
        /// <value> The AutoRange state query command. </value>
        protected override string AutoRangeStateQueryCommand { get; set; } = "_G.smu.measure.autorange";

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = "_G.smu.measure.autorange={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = "_G.print(_G.smu.measure.autorange==smu.ON)";

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = "_G.smu.measure.autozero.enable={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = "_G.print(_G.smu.measure.autozero.enable==smu.ON)";

        #endregion

        #region " AUTO ZERO ONCE "

        /// <summary> Gets or sets the automatic zero once command. </summary>
        /// <value> The automatic zero once command. </value>
        protected override string AutoZeroOnceCommand { get; set; } = "_G.smu.measure.autozero.once()";

        #endregion

        #region " FILTER "

        #region " FILTER COUNT "

        /// <summary> Gets or sets the Filter Count query command. </summary>
        /// <value> The FilterCount query command. </value>
        protected override string FilterCountQueryCommand { get; set; } = "_G.print(_G.smu.measure.filter.count)";

        /// <summary> Gets or sets the Filter Count command format. </summary>
        /// <value> The FilterCount command format. </value>
        protected override string FilterCountCommandFormat { get; set; } = "_G.smu.measure.filter.count={0}";

        #endregion

        #region " FILTER ENABLED "

        /// <summary> Gets or sets the Filter enabled command Format. </summary>
        /// <value> The Filter enabled query command. </value>
        protected override string FilterEnabledCommandFormat { get; set; } = "_G.smu.measure.filter.enable={0:'smu.ON';'smu.ON';'smu.OFF'}";

        /// <summary> Gets or sets the Filter enabled query command. </summary>
        /// <value> The Filter enabled query command. </value>
        protected override string FilterEnabledQueryCommand { get; set; } = "_G.print(_G.smu.measure.filter.enable==smu.ON)";

        #endregion

        #region " MOVING AVERAGE ENABLED "

        /// <summary> Gets or sets the moving average filter enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string MovingAverageFilterEnabledCommandFormat { get; set; } = "_G.smu.measure.filter.type={0:'smu.FILTER_MOVING_AVG';'smu.FILTER_MOVING_AVG';'smu.FILTER_REPEAT_AVG'}";

        /// <summary> Gets or sets the moving average filter enabled query command. </summary>
        /// <value> The moving average filter enabled query command. </value>
        protected override string MovingAverageFilterEnabledQueryCommand { get; set; } = "_G.print(_G.smu.measure.filter.type==smu.FILTER_MOVING_AVG)";

        #endregion

        #region " FILTER Window "

        /// <summary> Gets or sets the Filter Window query command. </summary>
        /// <value> The FilterWindow query command. </value>
        protected override string FilterWindowQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the Filter Window command format. </summary>
        /// <value> The FilterWindow command format. </value>
        protected override string FilterWindowCommandFormat { get; set; } = string.Empty;

        #endregion

        #endregion

        #region " FRONT TERMINALS SELECTED "

        /// <summary> Gets or sets the front terminals selected command format. </summary>
        /// <value> The front terminals selected command format. </value>
        protected override string FrontTerminalsSelectedCommandFormat { get; set; } = "_G.smu.measure.terminals={0:'smu.TERMINALS_FRONT';'smu.TERMINALS_FRONT';'smu.TERMINALS_REAR'}";

        /// <summary> Gets or sets the front terminals selected query command. </summary>
        /// <value> The front terminals selected query command. </value>
        protected override string FrontTerminalsSelectedQueryCommand { get; set; } = "_G.print(_G.smu.measure.terminals==smu.TERMINALS_FRONT)";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the function mode query command. </summary>
        /// <value> The function mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = "_G.print(_G.smu.measure.func)";

        /// <summary> Gets or sets the function mode command format. </summary>
        /// <value> The function mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = "_G.smu.measure.func={0}";

        #endregion

        #region " MEASURE "

        /// <summary> Gets or sets the Measure query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string MeasureQueryCommand { get; set; } = "_G.print(_G.smu.measure.read())";

        #endregion

        #region " OPEN DETECTOR ENABLED "

        /// <summary> Gets or sets the open detector enabled command Format. </summary>
        /// <value> The open detector enabled query command. </value>
        protected override string OpenDetectorEnabledCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the open detector enabled query command. </summary>
        /// <value> The open detector enabled query command. </value>
        protected override string OpenDetectorEnabledQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = "_G.smu.measure.nplc={0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = "_G.print(_G.smu.measure.nplc)";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the Range query command. </summary>
        /// <value> The Range query command. </value>
        protected override string RangeQueryCommand { get; set; } = "_G.print(_G.smu.measure.range)";

        /// <summary> Gets or sets the Range command format. </summary>
        /// <value> The Range command format. </value>
        protected override string RangeCommandFormat { get; set; } = "_G.smu.measure.range={0}";

        #endregion

        #region " REMOTE SENSE SELECTED "

        /// <summary> Gets or sets the remote sense selected command format. </summary>
        /// <value> The remote sense selected command format. </value>
        protected override string RemoteSenseSelectedCommandFormat { get; set; } = "_G.smu.measure.sense={0:'smu.SENSE_4WIRE';'smu.SENSE_4WIRE';'smu.SENSE_2WIRE'}";

        /// <summary> Gets or sets the remote sense selected query command. </summary>
        /// <value> The remote sense selected query command. </value>
        protected override string RemoteSenseSelectedQueryCommand { get; set; } = "_G.print(_G.smu.measure.sense==smu.SENSE_4WIRE)";

        #endregion

        #region " UNIT "

        /// <summary> Gets or sets the unit command. </summary>
        /// <value> The unit query command. </value>
        protected override string MeasureUnitQueryCommand { get; set; } = "_G.print(_G.smu.measure.unit)";

        /// <summary> Gets or sets the unit command format. </summary>
        /// <value> The unit command format. </value>
        protected override string MeasureUnitCommandFormat { get; set; } = "_G.smu.measure.unit={0}";

        #endregion

        #endregion

        #region " MEASURE "

        /// <summary> Queries readings into the reading amounts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The reading or none if unknown. </returns>
        public override double? MeasureReadingAmounts()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return base.MeasureReadingAmounts();
        }

        #endregion

    }
}
