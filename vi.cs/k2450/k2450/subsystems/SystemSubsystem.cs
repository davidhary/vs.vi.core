
namespace isr.VI.Tsp2.K2450
{

    /// <summary> System subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="P:isr.VI.SubsystemPlusStatusBase.StatusSubsystem">TSP
        ///                                status Subsystem</see>. </param>
        public SystemSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "



        #endregion


    }
}
