namespace isr.VI.Tsp2.K2450
{

    /// <summary> Digital Input Output subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-06-29 </para>
    /// </remarks>
    public class DigitalInputOutputSubsystem : DigitalInputOutputSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalInputOutputSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.Tsp2.StatusSubsystemBase">status
        ///                                Subsystem</see>. </param>
        public DigitalInputOutputSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Resets the known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void DefineKnownResetStateThis()
        {
            this.DigitalLines = new DigitalLineCollection( 6, false );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.DefineKnownResetStateThis();
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " DIGITAL LINE MODE "

        /// <summary> Get the Digital Line Mode query command. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lineNumber"> The line n umber. </param>
        /// <returns> A String. </returns>
        protected override string DigitalLineModeQueryCommand( int lineNumber )
        {
            return $"_G.print(_G.digio.line[{lineNumber}].mode)";
        }

        /// <summary> Gets Digital Line Mode command format. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lineNumber"> The line n umber. </param>
        /// <returns> A String. </returns>
        protected override string DigitalLineModeCommandFormat( int lineNumber )
        {
            return $"_G.digio.line[{lineNumber}].mode={{0}}";
        }

        #endregion

        #region " DIGITAL LINE RESET "

        /// <summary> Digital line reset command. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lineNumber"> The line number. </param>
        /// <returns> A String. </returns>
        protected override string DigitalLineResetCommand( int lineNumber )
        {
            return $"_G.digio.line[{lineNumber}].reset()";
        }

        #endregion

        #region " DIGITAL LINE STATE "

        /// <summary> Get the Digital Line State query command. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lineNumber"> The line n umber. </param>
        /// <returns> A String. </returns>
        protected override string DigitalLineStateQueryCommand( int lineNumber )
        {
            return $"_G.print(_G.digio.line[{lineNumber}].state)";
        }

        /// <summary> Gets Digital Line State command format. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lineNumber"> The line n umber. </param>
        /// <returns> A String. </returns>
        protected override string DigitalLineStateCommandFormat( int lineNumber )
        {
            return $"_G.digio.line[{lineNumber}].state={{0}}";
        }

        #endregion

        #region " READ PORT "

        /// <summary> Gets or sets the level command format. </summary>
        /// <value> The level command format. </value>
        protected override string LevelCommandFormat { get; set; } = "_G.digio.writeport({0})";

        /// <summary> Gets or sets the level query command. </summary>
        /// <value> The level query command. </value>
        protected override string LevelQueryCommand { get; set; } = "_G.print(_G.digio.readport())";

        #endregion

        #endregion
    }
}
