namespace isr.VI.Tsp2.K2450
{

    /// <summary> Source Measure Unit subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class SourceMeasureUnit : SourceMeasureUnitBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SourceMeasureUnit" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="P:isr.VI.SubsystemPlusStatusBase.StatusSubsystem">TSP
        ///                                status Subsystem</see>. </param>
        public SourceMeasureUnit( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="SourceMeasureUnit" /> class. </summary>
        /// <remarks>
        /// Note that the local node status clear command only clears the SMU status.  So, issue a CLS
        /// and RST as necessary when adding an SMU.
        /// </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystem">TSP status
        ///                                Subsystem</see>. </param>
        /// <param name="nodeNumber">      Specifies the node number. </param>
        /// <param name="smuNumber">       Specifies the SMU (either 'a' or 'b'. </param>
        public SourceMeasureUnit( StatusSubsystemBase statusSubsystem, int nodeNumber, string smuNumber ) : base( statusSubsystem, nodeNumber, smuNumber )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.MaximumOutputPower = 22d;
        }

        #endregion

    }
}
