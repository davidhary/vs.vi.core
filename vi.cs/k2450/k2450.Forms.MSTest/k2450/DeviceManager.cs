
namespace isr.VI.Tsp2.K2450.Forms.MSTest
{

    /// <summary>
    /// Static class for managing the common functions.
    /// </summary>
    internal sealed partial class DeviceManager
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private DeviceManager() : base()
        {
        }

        #endregion

    }
}
