﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K2450 TSP2 VI Forms Tests" )]
[assembly: AssemblyDescription( "K2450 TSP2 Virtual Instrument Forms Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Tsp2.K2450.Forms.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
