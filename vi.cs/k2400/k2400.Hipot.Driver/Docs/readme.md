**Sample Source Code for ISR HIPOT 1.0**

**Installation Information**

This program installs sample source code for the ISR High Potential
Meter 1.0

**Table of Contents**

-   Installation and Files
-   Required Development Studio Components
-   Copyrights and Trademarks

**Installation and Files**

The installation program copies files to the destination directory
chosen during installation. Source code is copied to the user documents
folder.

**Required Development Studio Components**


The following components are required for compiling the program.
-   Microsoft Visual Basic 6.0 SP6;
-   Microsoft Visual Studio 2008 Development Environment 9.0.30729.1;
-   National Instruments VISA 4.3 and up.
-   NI-488.2 compatible drivers for NI-VISA

**Copyrights and Trademarks**

The ISR logo is a trademark of Integrated Scientific Resources, Inc..
Windows and .NET Framework are trademarks of Microsoft Corporation.

\(c\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

<a name="The-MIT-License"></a>
### The MIT License
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Open source used by this software is described and licensed on the
following sites:

Core VB Library:  
[Easy String Compression and Encryption](http://www.codeproject.com/KB/string/string_compression.aspx)  
[Enumerations, Flags and C\#](http://www.codeproject.com/Articles/37921/Enums-Flags-and-Csharp-Oh-my-bad-pun.aspx)  
[Generic Event Arguments](http://www.codeproject.com/KB/cs/GenericEventArgs.aspx)  
[Register](http://www.codeproject.com/KB/cs/Register.aspx)  
[String Extensions](http://www.codeproject.com/KB/string/stringconversion.aspx)  
[Strong-Type and Efficient Enumerations](http://www.codeproject.com/KB/cs/efficient_strong_enum.aspx)

This software uses Closed software described and licensed on the
following sites:

VISA VB Library:  
[NI-VISA](http:\www.ni.com)
