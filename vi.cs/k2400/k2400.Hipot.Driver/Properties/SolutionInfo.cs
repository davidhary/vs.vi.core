﻿using System.Reflection;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyCompany( "Integrated Scientific Resources" )]
[assembly: AssemblyCopyright( "(c) 2010 Integrated Scientific Resources, Inc. All rights reserved." )]
[assembly: AssemblyTrademark( "Licensed under ISR Fair End User Use License Version 1.0 http://www.integratedscientificresources.com/licenses/FairEndUserUseLicense.pdf" )]
[assembly: System.Resources.NeutralResourcesLanguage( "en-US", System.Resources.UltimateResourceFallbackLocation.MainAssembly )]
[assembly: AssemblyVersion( "1.3.*" )]
