﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// TO_DO: Review the values of the assembly attributes

[assembly: AssemblyTitle( "HIPOT Driver" )]
[assembly: AssemblyDescription( "HIPOT Driver" )]
[assembly: AssemblyProduct( "HIPOT.Driver" )]
[assembly: CLSCompliant( true )]

// Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
[assembly: ComVisible( false )]
