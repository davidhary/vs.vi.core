using System;
using System.Diagnostics;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.K2400.Hipot.Driver
{

    /// <summary> Implements the high potential insulation resistance meter device. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-12-25, 1.0.4011. Created </para>
    /// </remarks>
    public partial class Device : IDevice
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Primary Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public Device() : base()
        {
            this.ResourceName = "GPIB0::24::INSTR";
            this._MessageAvailableBits = 16;
            this._ErrorAvailableBits = 4;
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> The is disposed. </value>
        protected bool IsDisposed { get; private set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
        /// method has been called directly or indirectly by a user's code--managed and unmanaged
        /// resources can be disposed. If disposing equals False, the method has been called by the
        /// runtime from inside the finalizer and you should not reference other objects--only unmanaged
        /// resources can be disposed.
        /// </remarks>
        /// <param name="disposing"> True if this method releases both managed and unmanaged resources;
        ///                          False if this method releases only unmanaged resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this._Identity = string.Empty;
                    if ( ExceptionAvailable is object )
                    {
                        foreach ( Delegate d in ExceptionAvailable.GetInvocationList() )
                            ExceptionAvailable -= ( EventHandler<System.Threading.ThreadExceptionEventArgs> ) d;
                    }

                    if ( MessageAvailable is object )
                    {
                        foreach ( Delegate d in MessageAvailable.GetInvocationList() )
                            MessageAvailable -= ( EventHandler<MessageEventArgs> ) d;
                    }

                    this.Session?.Dispose();
                    this.Session = null;
                }
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// This destructor will run only if the Dispose method does not get called. It gives the base
        /// class the opportunity to finalize. Do not provide destructors in types derived from this
        /// class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        ~Device()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal for readability and maintainability.
            this.Dispose( false );
        }

        #endregion

        #region " I DEVICE: CONNECT "

        /// <summary> Gets the last error status. </summary>
        /// <exception cref="VisaException">         Thrown when a Visa error condition occurs. </exception>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The last error status. </value>
        public int LastErrorStatus { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NationalInstruments.Visa.Session" /> class.
        /// </summary>
        /// <remarks>
        /// This method does not lock the resource. Rev 4.1 and 5.0 of VISA did not support this call and
        /// could not verify the resource.
        /// </remarks>
        /// <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <param name="timeout">      The open timeout. </param>
        protected void CreateSession( string resourceName, TimeSpan timeout )
        {
            try
            {
                this.LastErrorStatus = 0;
                var result = Ivi.Visa.ResourceOpenStatus.Success;
                string activity = "creating visa session";
                using ( var resMgr = new NationalInstruments.Visa.ResourceManager() )
                {
                    result = Ivi.Visa.ResourceOpenStatus.Unknown;
                    this.Session = ( Ivi.Visa.IMessageBasedSession ) resMgr.Open( resourceName, Ivi.Visa.AccessModes.None, ( int ) Math.Round( timeout.TotalMilliseconds ), out result );
                }

                if ( result != Ivi.Visa.ResourceOpenStatus.Success )
                    throw new VisaException( $"Failed {activity}; '{result}';. {resourceName}" );
                if ( this.Session is null )
                    throw new VisaException( $"Failed {activity};. {resourceName}" );
            }
            catch ( Ivi.Visa.NativeVisaException ex )
            {
                if ( this.Session is object )
                {
                    this.Session.Dispose();
                    this.Session = null;
                }

                this.LastErrorStatus = ex.ErrorCode;
                throw new VisaException( this.LastErrorStatus, ex );
            }
            catch
            {
                if ( this.Session is object )
                {
                    this.Session.Dispose();
                    this.Session = null;
                }

                throw;
            }
        }

        /// <summary> Connects to the instrument and clears its known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resourceName"> . </param>
        /// <returns> <c>True</c> if the device connected. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool Connect( string resourceName )
        {
            if ( resourceName is null )
            {
                throw new ArgumentNullException( nameof( resourceName ) );
            }

            this.ResourceName = resourceName;
            try
            {
                this.OnMessageAvailable( TraceEventType.Verbose, "OPENING VISA SESSION", "Opening VISA session to {0}", resourceName );
                this.CreateSession( resourceName, TimeSpan.FromSeconds( 3d ) );
                this.OnMessageAvailable( TraceEventType.Information, "VISA SESSION OPEN", "VISA session opened to {0}", resourceName );
                this.OnMessageAvailable( TraceEventType.Verbose, "READING STATUS BYTE", "Reading {0} status byte", resourceName );
                _ = this.ReadStatusByte();
                this.DiscardUnreadData();
                _ = this.ResetAndClear();
                this.IsConnected = true;
                return true;
            }
            catch ( Exception ex )
            {
                this.OnExceptionAvailable( ex, "Exception occurred opening a VISA Session to {0}. Disconnecting.", resourceName );
                try
                {
                    _ = this.Disconnect();
                }
                finally
                {
                }

                return false;
            }
            finally
            {
            }
        }

        /// <summary> Disconnects form the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if the device  disconnected. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool Disconnect()
        {
            try
            {
                this._Identity = string.Empty;
                this.OnMessageAvailable( TraceEventType.Verbose, "DISCONNECTING", "disconnecting {0}", this.ResourceName );
                if ( this.Session is object )
                {
                    this.Session.Dispose();
                    this.Session = null;
                }

                this.IsConnected = false;
                return !this.IsConnected;
            }
            catch ( Exception ex )
            {
                this.OnExceptionAvailable( ex, "Exception occurred closing the VISA Session to {0}.", this.ResourceName );
                return false;
            }
        }

        /// <summary> Clears the read buffers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool FlushRead()
        {
            this.DiscardUnreadData();
            return default;
        }

        /// <summary> The identity. </summary>
        private string _Identity;

        /// <summary>
        /// Returns the instrument identification code. KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11
        /// Oct 10 1997 09:51:36/A02 /D/B/E.
        /// </summary>
        /// <value> The identity. </value>
        public string Identity
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._Identity ) )
                {
                    this._Identity = this.Session.Query( IdentifyQueryCommand );
                }

                return this._Identity;
            }
        }

        /// <summary> Gets the connection status. </summary>
        /// <value> The is connected. </value>
        public bool IsConnected { get; private set; }

        /// <summary> Reseting and clearing the instrument to its known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public bool ResetAndClear()
        {
            var timeout = default( int );
            try
            {
                timeout = this.Session.ReadTimeout();
                this.OnMessageAvailable( TraceEventType.Verbose, "RESETTING AND CLEARING TO KNOWN STATE", "Resetting and clearing {0} to its known state", this.ResourceName );
                _ = this.ClearActiveState();
                _ = this.ResetKnownState();
                _ = this.ClearExecutionState();
                return this.LastErrorStatus == 0;
            }
            catch ( Exception ex )
            {
                this.OnExceptionAvailable( ex, "Exception occurred resetting the {0} to its known state.", this.ResourceName );
            }
            finally
            {

                // restore timeout.
                this.Session.WriteTimeout( timeout );
            }

            return default;
        }

        /// <summary> Gets the instrument resource name. </summary>
        /// <value> The name of the resource. </value>
        public string ResourceName { get; set; }

        #endregion

        #region " I DEVICE: CONFIGURE "

        /// <summary> Selects the voltage range. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.String. </returns>
        public static string SelectVoltageRange( float value )
        {
            switch ( value )
            {
                case var @case when @case <= 20f:
                    {
                        return "20";
                    }

                case var case1 when case1 <= 40f:
                    {
                        return "40";
                    }

                case var case2 when case2 <= 100f:
                    {
                        return "100";
                    }

                case var case3 when case3 <= 200f:
                    {
                        return "200";
                    }

                case var case4 when case4 <= 300f:
                    {
                        return "300";
                    }

                case var case5 when case5 <= 400f:
                    {
                        return "400";
                    }

                case var case6 when case6 <= 500f:
                    {
                        return "500";
                    }

                default:
                    {
                        return "MAX";
                    }
            }
        }

        /// <summary> Selects the resistance range. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> System.String. </returns>
        public static string SelectResistanceRange( float value )
        {
            switch ( value )
            {
                case var @case when @case <= 21f:
                    {
                        return "21";
                    }

                case var case1 when case1 <= 210f:
                    {
                        return "210";
                    }

                case var case2 when case2 <= 2100f:
                    {
                        return "2100";
                    }

                case var case3 when case3 <= 21000f:
                    {
                        return "21000";
                    }

                case var case4 when case4 <= 210000f:
                    {
                        return "210000";
                    }

                case var case5 when case5 <= 2100000f:
                    {
                        return "2100000";
                    }

                case var case6 when case6 <= 21000000f:
                    {
                        return "21000000";
                    }

                case var case7 when case7 <= 210000000f:
                    {
                        return "210000000";
                    }

                default:
                    {
                        return "MAX";
                    }
            }
        }

        /// <summary> Configures the measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> True if success or False if failed. </returns>
        public bool Configure()
        {
            if ( this.Aperture < 0.001d || this.CurrentLimit < 0.000001d || this.VoltageLevel < 0.001d )
            {
                // ignores configuration if settings were not applied to instrument cached values.
                return true;
            }

            // make sure the 2400 output is off
            this.Session.Write( ":OUTP OFF" );

            // Me.Session.Write("*RST")
            // Me.Session.Write(":SENS:FUNC ""RES""")
            // Me.Session.Write(":SENS:RES:MODE MAN")
            // Me.Session.Write(":SYST:RSEN ON")
            // Me.Session.Write(":SOUR:FUNC VOLT")
            // Me.Session.Write(":SOUR:VOLT:RANG 5")
            // Me.Session.Write(":SOUR:VOLT 5")
            // Me.Session.Write(":SENS:CURR:RANG 10e-6")
            // Me.Session.Write(":OUTPut ON")
            // Dim value As String = Me.Session.Query(":READ?")
            // Me.Session.Write(":OUTPut OFF")

            // reset the instrument to its know state. 
            _ = this.ResetKnownState();

            // Use rear terminals
            this.Session.Write( ":ROUT:TERM REAR" );
            this.Session.Write( ":SENS:FUNC \"RES\"" );

            // no need to set in manual mode.
            // Me.Session.Write(":SENS:RES:RANG " & selectResistanceRange(Me.ResistanceRange))
            // Me.ResistanceRange = CSng(Me.Session.Query(":SENS:RES:RANG?"))
            this.Session.Write( ":SENS:RES:MODE MAN" );

            // the source must be set first.
            this.Session.Write( ":SOUR:CLE:AUTO ON" );
            this.Session.Write( ":SOUR:DEL " + this.DwellTime.ToString() );
            this.DwellTime = Conversions.ToSingle( this.Session.Query( ":SOUR:DEL?" ) );
            this.Session.Write( ":SOUR:FUNC VOLT" );
            // default: Me.Session.Write(":SOUR:VOLT:MODE FIX")
            this.Session.Write( ":SOUR:VOLT:RANG " + SelectVoltageRange( this.VoltageLevel ) );
            this.Session.Write( ":SOUR:VOLT:LEV " + this.VoltageLevel.ToString() );
            this.VoltageLevel = Conversions.ToSingle( this.Session.Query( ":SOUR:VOLT:LEV?" ) );

            // Me.Session.Write(":SENS:FUNC <function>")
            // Me.Session.Write(":SENS:CURR:PROT " & CStr(Me.CurrentLimit))
            // Me.Session.Write(":SENS:CURR:RANG " & CStr(Me.CurrentLimit))
            // Me.Session.Write(":SENS:RES:RANG <n>")

            this.Session.Write( ":SENS:CURR:RANG " + this.CurrentRange.ToString() );
            // Debug.Write(Me.Session.Query(":SENS:CURR:RANG?"))
            this.Session.Write( ":SENS:CURR:PROT " + this.CurrentLimit.ToString() );
            this.CurrentLimit = Conversions.ToSingle( this.Session.Query( ":SENS:CURR:PROT?" ) );
            this.Session.Write( ":SENS:VOLT:NPLC " + this.Aperture.ToString() );
            this.Session.Write( ":SENS:CURR:NPLC " + this.Aperture.ToString() );
            this.Aperture = Conversions.ToSingle( this.Session.Query( ":SENS:CURR:NPLC?" ) );

            // Enable four wire connection
            this.Session.Write( ":SYST:RSEN ON" );

            // force immediate update of auto zero
            this.Session.Write( ":SYST:AZER ONCE" );

            // default is cable: Me.Session.Write(":SYST:GUAR CABL") ' or OHMS

            // =================
            // ARM MODEL:

            // arm to immediate mode.
            // default is IMM:  Me.Session.Write(":ARM:SOUR IMM")

            // Set ARM counter to 1
            // default is 1: Me.Session.Write(":ARM:COUN 1")

            // Define Trigger layer to interface with the
            // Trigger Master board:

            // clear any pending triggers.
            this.Session.Write( ":TRIG:CLE" );

            // Set TRIGGER input line to Trigger Link line number
            // default. to be set when starting: Me.Session.Write(":TRIG:ILIN 1")

            // Set TRIGGER output line to Trigger Link line number
            // default: Me.Session.Write(":TRIG:OLIN 2")

            // Set Input trigger to Acceptor
            this.Session.Write( ":TRIG:DIR ACC" );
            this.Session.Write( ":TRIG:DIR SOUR" );

            // start with immediate trigger allowing non-triggered measurements.
            this.Session.Write( ":TRIG:SOUR IMM" );

            // no outputs triggers
            // default is none: Me.Session.Write(":TRIG:OUTP NONE")

            // A single trigger.
            // default is 1: Me.Session.Write(":TRIG:COUN 1")

            // default: Me.Session.Write(":TRIG:INP NONE")

            // ======
            // FORMAT

            // Set time stamp to start from when first items stored to memory
            // not using timestamps. Me.Session.Write(":TRAC:TST:FORM ABS")

            // Define data transfer format
            // default is ASC: Me.Session.Write(":FORM:DATA ASC")

            this.Session.Write( ":FORM:ELEM VOLT,CURR,RES,STATUS" );

            // ======
            // CALC2

            // default: Me.Session.Write(":CALC2:CLIM:BCON IMM")
            this.Session.Write( ":CALC2:CLIM:CLE" );
            this.Session.Write( ":CALC2:CLIM:MODE GRAD" );
            // default Me.Session.Write(":CALC2:CLIM:CLE:AUTO ON")

            // Make limit comparisons on resistance reading
            this.Session.Write( ":CALC2:FEED RES" );

            // Limit 1: send BAD code if measurement is in
            // compliance
            // default: Me.Session.Write(":CALC2:LIM1:COMP:FAIL IN")

            // Set fail bin
            this.Session.Write( string.Format( System.Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM1:COMP:SOUR2 {0}", this.FailBitPattern ) );
            this.Session.Write( ":CALC2:LIM1:STAT 1" );

            // Limit 2: send BAD code if out-of-range

            // Set upper limit
            // Me.Session.Write(":CALC2:LIM2:UPP " & CStr(0.995 * Me.ResistanceRange))
            // Me.Session.Write(":CALC2:LIM2:UPP:SOUR2 " & CStr(7 - 2 ^ (3 - 1)))  ' Bit = 3

            // Set lower limit
            // TO_DO:  Check why I cannot set a limit of 0.1 on
            // the current test?  0 or -10 will work!
            this.Session.Write( ":CALC2:LIM2:UPP 1E+19" );
            this.Session.Write( string.Format( System.Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM2:LOW {0}", this.ResistanceLowLimit ) );
            this.Session.Write( string.Format( System.Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM2:UPP:SOUR2 {0}", this.FailBitPattern ) );
            this.Session.Write( string.Format( System.Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM2:LOW:SOUR2 {0}", this.FailBitPattern ) );
            this.Session.Write( ":CALC2:LIM2:STAT 1" );

            // Send GOOD code if measurement is in range
            this.Session.Write( string.Format( System.Globalization.CultureInfo.InvariantCulture, ":CALC2:CLIM:PASS:SOUR2 {0}", this.PassBitPattern ) );
            this.Session.Write( ":CALC2:CLIM:BCON IMM" );

            // enable contact check
            if ( this.ContactCheckSupported )
            {
                if ( this.ContactCheckEnabled )
                {
                    // Enable contact check failure detection
                    this.Session.Write( ":CALC2:LIM4:STAT ON" );
                    if ( this.ContactCheckBitPattern > 0 )
                    {
                        // set the contact check failure bit pattern
                        this.Session.Write( string.Format( System.Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM4:SOUR2 {0}", this.ContactCheckBitPattern ) );
                        // Enable contact check event detection
                        this.Session.Write( ":TRIG:SEQ2:SOUR CCH" );
                        // Set 2 second contact check timeout
                        this.Session.Write( ":TRIG:SEQ2:TOUT 1" );
                    }
                    // set contact check resistance
                    this.Session.Write( ":SYST:CCH:RES 2" );
                    // enable contact check
                    this.Session.Write( ":SYST:CCH ON" );
                }
                else
                {
                    this.Session.Write( ":CALC2:LIM4:STAT OFF" );
                    this.Session.Write( ":SYST:CCH OFF" );
                }
            }

            // ======
            // EOT
            // Set byte size to 3 enabling EOT mode.
            this.Session.Write( ":SOUR2:BSIZ 3" );

            // Set the output logic to high
            this.Session.Write( ":SOUR2:TTL #b000" );

            // Set Digital I/O Mode to EOT
            this.Session.Write( ":SOUR2:TTL4:MODE EOT" );

            // Set EOT polarity to HI
            this.Session.Write( ":SOUR2:TTL4:BST HI" );

            // Set the output level to set automatically to the
            // :TTL level after the pass or fail output bit
            // pattern or a limit test is sent to the handler.
            // this can be turned off and the clear command issued on each start.
            this.Session.Write( ":SOUR2:CLE" );
            this.Session.Write( ":SOUR2:CLE:AUTO ON" );

            // Set the duration of the EOT strobe
            this.Session.Write( ":SOUR2:CLE:AUTO:DEL " + this.EotStrobeDuration.ToString() );
            return true;
        }

        /// <summary> Gets the aperture in power line cycles. </summary>
        /// <value> The aperture. </value>
        public float Aperture { get; set; }

        /// <summary> The contact check option. </summary>
        private const string _ContactCheckOption = "CONTACT-CHECK";

        /// <summary> Query if this object is contact check supported. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if contact check supported, false if not. </returns>
        private bool? IsContactCheckSupported()
        {
            if ( this.IsConnected )
            {
                string options = this.Session.Query( "*OPT?" );
                return string.IsNullOrWhiteSpace( options ) ? new bool?() : options.Contains( _ContactCheckOption );
            }
            else
            {
                return new bool?();
            }
        }

        /// <summary> The contact check supported. </summary>
        private bool? _ContactCheckSupported;

        /// <summary> Gets the sentinel indicating that contact check is supported. </summary>
        /// <value> The contact check supported. </value>
        public bool ContactCheckSupported
        {
            get {
                if ( !this._ContactCheckSupported.HasValue )
                {
                    this._ContactCheckSupported = this.IsContactCheckSupported();
                }

                return this._ContactCheckSupported.Value;
            }
        }

        /// <summary> Gets the contact check enabled. </summary>
        /// <value> The contact check enabled. </value>
        public bool ContactCheckEnabled { get; set; }

        /// <summary> Gets the contact check bit pattern. </summary>
        /// <value> The contact check bit pattern. </value>
        public int ContactCheckBitPattern { get; set; }

        /// <summary>
        /// Gets the current range. Based on the ratio of the voltage to the minimum resistance. If the
        /// resistance is zero, returns the current limit.
        /// </summary>
        /// <value> The current range. </value>
        private float CurrentRange => this.ResistanceLowLimit > 0f ? this.VoltageLevel / this.ResistanceLowLimit : this.CurrentLimit;

        /// <summary> Gets the current limit in Amperes. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The current limit. </value>
        public float CurrentLimit { get; set; }

        /// <summary> Gets the dwell time in seconds. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The dwell time. </value>
        public float DwellTime { get; set; }

        /// <summary> Gets the resistance limit in ohms. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The resistance low limit. </value>
        public float ResistanceLowLimit { get; set; }

        /// <summary> Gets the resistance range in ohms. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The resistance range. </value>
        public float ResistanceRange { get; set; }

        /// <summary> Gets the voltage level in volts. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The voltage level. </value>
        public float VoltageLevel { get; set; }


        #endregion

        #region " I DEVICE: BINNING "

        /// <summary> Gets the duration of the end-of=test strobe. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The duration of the end-of-test strobe. </value>
        public float EotStrobeDuration { get; set; }

        /// <summary> Gets the pass bit pattern. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The pass bit pattern. </value>
        public int PassBitPattern { get; set; }

        /// <summary> Gets the fail bit pattern. </summary>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <value> The fail bit pattern. </value>
        public int FailBitPattern { get; set; }

        #endregion

        #region " I DEVICE: EVENTS "

        /// <summary>
        /// Sends an Exception message to the client.
        /// </summary>
        public event EventHandler<System.Threading.ThreadExceptionEventArgs> ExceptionAvailable;

        /// <summary> Raises the exception available event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
        /// <param name="ex"> Specifies the exception. </param>
        public void OnExceptionAvailable( Exception ex )
        {
            if ( ExceptionAvailable is null )
            {
                throw new DriverException( "An exception occurred but no handlers were set to receive the exception message. Check inner exception for details.", ex );
            }
            else
            {
                ExceptionAvailable( this, new System.Threading.ThreadExceptionEventArgs( ex ) );
            }
        }

        /// <summary> Raises the exception available event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="ex">     Specifies the exception. </param>
        /// <param name="format"> Specifies the event message format. </param>
        /// <param name="args">   Specifies the event message format arguments. </param>
        public void OnExceptionAvailable( Exception ex, string format, params object[] args )
        {
            if ( ex is object )
            {
                ex.Data.Add( "Details", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
                this.OnExceptionAvailable( ex );
            }
        }

        /// <summary>Sends a message to the client.
        /// </summary>
        public event EventHandler<MessageEventArgs> MessageAvailable;

        /// <summary> Raises the Message Available event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> The message event arguments. </param>
        public void OnMessageAvailable( MessageEventArgs e )
        {
            if ( MessageAvailable is object )
            {
                MessageAvailable( this, e );
            }
        }

        /// <summary> Raises the Message Available event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="traceLevel"> Specifies the event arguments message
        ///                           <see cref="System.Diagnostics.TraceEventType">trace level</see>. </param>
        /// <param name="synopsis">   Specifies the message Synopsis. </param>
        /// <param name="format">     Specifies the message details. </param>
        /// <param name="args">       Arguments to use in the format statement. </param>
        public virtual void OnMessageAvailable( TraceEventType traceLevel, string synopsis, string format, params object[] args )
        {
            this.OnMessageAvailable( new MessageEventArgs( traceLevel, synopsis, format, args ) );
        }

        #endregion

        #region " I DEVICE MEASURE "

        /// <summary> Makes the HIPOT measurements manually. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool Measure()
        {
            this.Outcome = MeasurementOutcomes.None;
            if ( int.TryParse( this.Session.Query( ":SOUR:CLE:AUTO?" ), out int autoMeasureState ) )
            {
            }

            int openCheckFailed = 0;
            if ( autoMeasureState == 0 )
                this.Session.Write( ":OUTP ON" );
            int timeout = this.Session.ReadTimeout();
            this.Session.WriteTimeout( 10000 );
            this.Session.Write( ":INIT" );
            if ( this.ContactCheckEnabled )
            {
                if ( int.TryParse( this.Session.Query( ":CALC2:LIM4:FAIL?" ), out openCheckFailed ) )
                {
                }
            }

            if ( openCheckFailed == 0 )
            {
                this.Reading = this.Session.Query( ":FETCH?" );
            }
            else
            {
            }

            if ( autoMeasureState == 0 )
                this.Session.Write( ":OUTP OFF" );
            this.Session.WriteTimeout( timeout );
            return openCheckFailed == 0 ? this.ParseReading( 0 ) : this.ParseReading( ( int ) Math.Round( Math.Pow( 2d, ( double ) StatusBit.FailedContactCheck ) ) );
        }

        /// <summary> Gets the measurement outcome for the measurements. </summary>
        /// <value> The outcome. </value>
        public MeasurementOutcomes Outcome { get; private set; }

        /// <summary> Parse status. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ParseStatus()
        {

            // Bit 0 (OFLO) — Set to 1 if measurement was made while in over-range.
            // Bit 1 (Filter) — Set to 1 if measurement was made with the filter enabled. 
            // Bit 3 (Compliance) — Set to 1 if in real compliance. 
            // Bit 4 (OVP) — Set to 1 if the over voltage protection limit was reached. '
            // Bit 16 (Range Compliance) — Set to 1 if in range compliance. 
            // Bit 18 — Contact check failure (see Appendix F). 
            if ( (this.Status & ( long ) Math.Round( Math.Pow( 2d, ( double ) StatusBit.HitCompliance ) + Math.Pow( 2d, ( double ) StatusBit.HitRangeCompliance ) )) != 0L )
                this.Outcome |= MeasurementOutcomes.HitCompliance;
            if ( (this.Status & ( long ) Math.Round( Math.Pow( 2d, ( double ) StatusBit.FailedContactCheck ) )) != 0L )
                this.Outcome |= MeasurementOutcomes.FailedContactCheck;


            // TO_DO: Parse status to get the compliance issue. 
            // Parse the limit to get the level compliance. 
            if ( this.Outcome == MeasurementOutcomes.None )
            {
                this.Outcome = this._Resistance < this.ResistanceLowLimit ? MeasurementOutcomes.PartFailed : MeasurementOutcomes.PartPassed;
            }
        }

        /// <summary> Parses the reading. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool ParseReading( int status )
        {
            this.VoltageReading = string.Empty;
            this._Voltage = 0f;
            this._Current = 0f;
            this.CurrentReading = string.Empty;
            this._Resistance = 0f;
            if ( false )
            {
            }
            else if ( status > 0 )
            {
                this.Status = status;
                this.ParseStatus();
            }
            else if ( string.IsNullOrWhiteSpace( this.Reading ) )
            {
                this.Status = -1;
                return false;
            }
            else
            {
                var values = this.Reading.Split( ',' );
                this.VoltageReading = values[0];
                var numberStyle = System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowDecimalPoint | System.Globalization.NumberStyles.AllowExponent | System.Globalization.NumberStyles.AllowLeadingSign;
                if ( !float.TryParse( this.VoltageReading, numberStyle, System.Globalization.CultureInfo.CurrentCulture, out this._Voltage ) )
                {
                    this._Voltage = -1.0f;
                    this.Outcome |= MeasurementOutcomes.UnexpectedReadingFormat;
                }

                this.CurrentReading = values[1];
                if ( !float.TryParse( this.CurrentReading, numberStyle, System.Globalization.CultureInfo.CurrentCulture, out this._Current ) )
                {
                    this._Current = -1.0f;
                    this.Outcome |= MeasurementOutcomes.UnexpectedReadingFormat;
                }

                this.ResistanceReading = values[2];
                if ( !float.TryParse( this.ResistanceReading, numberStyle, System.Globalization.CultureInfo.CurrentCulture, out this._Resistance ) )
                {
                    this._Resistance = -1.0f;
                    this.Outcome |= MeasurementOutcomes.UnexpectedReadingFormat;
                }

                if ( !float.TryParse( values[3], numberStyle, System.Globalization.CultureInfo.CurrentCulture, out float temp ) )
                {
                    temp = -1.0f;
                    this.Outcome |= MeasurementOutcomes.UnexpectedReadingFormat;
                }

                this.Status = ( long ) Math.Round( temp );
                this.ParseStatus();
            }

            return true;
        }

        /// <summary> Read the measurements from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool ReadMeasurements()
        {
            this.Reading = this.Session.Query( ":FETCH?" );
            return this.ParseReading( 0 );
        }

        /// <summary> The current. </summary>
        private float _Current;

        /// <summary> Gets the measured Current. </summary>
        /// <value> The current. </value>
        public float Current => this._Current;

        /// <summary> Gets the measured Current reading. </summary>
        /// <value> The current reading. </value>
        public string CurrentReading { get; private set; }

        /// <summary> Gets the reading as fetched from the instrument. </summary>
        /// <value> The reading. </value>
        public string Reading { get; private set; }

        /// <summary> The resistance. </summary>
        private float _Resistance;

        /// <summary> Gets the measurement Resistance. </summary>
        /// <value> The resistance. </value>
        public float Resistance => this._Resistance;

        /// <summary> Gets the measurement Resistance reading. </summary>
        /// <value> The resistance reading. </value>
        public string ResistanceReading { get; private set; }

        /// <summary> Gets the parsed measurement status. </summary>
        /// <value> The status. </value>
        public long Status { get; private set; }

        /// <summary> The voltage. </summary>
        private float _Voltage;

        /// <summary> Gets the measured voltage. </summary>
        /// <value> The voltage. </value>
        public float Voltage => this._Voltage;

        /// <summary> Gets the measured voltage reading. </summary>
        /// <value> The voltage reading. </value>
        public string VoltageReading { get; private set; }

        #endregion

        #region " I DEVICE SETTINGS "

        /// <summary> Saves the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void SaveSettings()
        {
            My.MySettings.Default.Save();
        }

        /// <summary>
        /// Reads the settings and set the device values -- this does not send the settings to the
        /// instrument.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ReadSettings()
        {
            My.MyProject.Application.Log.WriteEntry( "Storing configuration settings in the device.", TraceEventType.Verbose );
            this.Aperture = ( float ) My.MySettings.Default.Aperture;
            this.CurrentLimit = ( float ) My.MySettings.Default.CurrentLimit;
            this.DwellTime = ( float ) My.MySettings.Default.DwellTime;
            this.ResistanceLowLimit = ( float ) My.MySettings.Default.ResistanceLowLimit;
            this.ResistanceRange = ( float ) My.MySettings.Default.ResistanceRange;
            this.VoltageLevel = ( float ) My.MySettings.Default.VoltageLevel;
            this.EotStrobeDuration = ( float ) My.MySettings.Default.EotStrobeDuration;
            this.FailBitPattern = My.MySettings.Default.FailBitPattern;
            this.PassBitPattern = My.MySettings.Default.PassBitPattern;
            if ( this._ContactCheckSupported.GetValueOrDefault( false ) )
            {
                this.ContactCheckEnabled = My.MySettings.Default.ContactCheckEnabled;
                this.ContactCheckBitPattern = My.MySettings.Default.ContactCheckBitPattern;
            }
        }

        /// <summary>
        /// Updates the current settings from the device -- this does not read the settings from the
        /// instrument.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void UpdateSettings()
        {
            My.MyProject.Application.Log.WriteEntry( "Updating configuration settings from the device.", TraceEventType.Verbose );
            My.MySettings.Default.Aperture = ( decimal ) this.Aperture;
            My.MySettings.Default.CurrentLimit = ( decimal ) this.CurrentLimit;
            My.MySettings.Default.DwellTime = ( decimal ) this.DwellTime;
            My.MySettings.Default.ResistanceLowLimit = ( decimal ) this.ResistanceLowLimit;
            My.MySettings.Default.ResistanceRange = ( decimal ) this.ResistanceRange;
            My.MySettings.Default.VoltageLevel = ( decimal ) this.VoltageLevel;
            My.MySettings.Default.EotStrobeDuration = ( decimal ) this.EotStrobeDuration;
            My.MySettings.Default.FailBitPattern = this.FailBitPattern;
            My.MySettings.Default.PassBitPattern = this.PassBitPattern;
            if ( this._ContactCheckSupported.GetValueOrDefault( false ) )
            {
                My.MySettings.Default.ContactCheckEnabled = this.ContactCheckEnabled;
                My.MySettings.Default.ContactCheckBitPattern = this.ContactCheckBitPattern;
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Aborts waiting for a triggered measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool AbortMeasurement()
        {
            this.Session.Write( ":ABORT" );
            this.Session.Clear();
            return this.LastErrorStatus == 0;
        }

        /// <summary> Asserts a trigger for making a triggered measurement. </summary>
        /// <remarks>
        /// This works with the 2600 instrument but not with the 2400 instrument. This method was devised
        /// for testing. It is replaced with Manual Trigger from the front panel of the 2400.
        /// </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool TriggerMeasurement()
        {
            this.Session.AssertTrigger();
            return this.LastErrorStatus == 0;
        }

        /// <summary> Returns true if measurement completed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if measurement completed, false if not. </returns>
        public bool IsMeasurementCompleted()
        {
            return (this.ReadStatusByte() & ( int ) ServiceRequests.StandardEvent) != 0;
        }

        /// <summary>
        /// Prime the instrument to wait for a trigger. This clears the digital outputs and loops until
        /// trigger or external TRG command.
        /// </summary>
        /// <remarks>
        /// Using manual trigger was required because the 2400 instrument fails to respond to the bus
        /// trigger.
        /// </remarks>
        /// <param name="useManualTrigger"> True if using manual trigger. </param>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool PrepareForTrigger( bool useManualTrigger )
        {
            this.Session.Write( ":TRIG:SOUR IMM" );
            if ( useManualTrigger )
            {
                // start when the bus trigger line is asserted.
                // Me.Session.Write(":ARM:SOUR BUS")
                this.Session.Write( ":ARM:SOUR MAN" );
                this.Session.Write( ":ARM:COUNT 1" );
            }
            else
            {
                // start when the start of test is pulsed high
                this.Session.Write( ":ARM:SOUR PST" );
            }

            if ( this.EnableServiceRequest( ( int ) (StandardEvents.All & ~StandardEvents.RequestControl), ( int ) (ServiceRequests.StandardEvent | ServiceRequests.OperationEvent) ) )
            {
                this.Session.Write( ":INIT; *OPC" );
                return this.LastErrorStatus == 0;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region " SYNCHRONIZER "

        /// <summary>
        /// Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see>
        /// to use for marshaling events.
        /// </summary>
        /// <value> The synchronizer. </value>
        public System.ComponentModel.ISynchronizeInvoke Synchronizer { get; set; }

        /// <summary>
        /// Sets the <see cref="System.ComponentModel.ISynchronizeInvoke">synchronization object</see>
        /// to use for marshaling events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void SynchronizerSetter( System.ComponentModel.ISynchronizeInvoke value )
        {
            this.Synchronizer = value;
        }

        #endregion

    }
}
