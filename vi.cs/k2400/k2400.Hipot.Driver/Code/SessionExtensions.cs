using System;

namespace isr.K2400.Hipot.Driver
{
    /// <summary>   A methods. </summary>
    /// <remarks>   David, 2021-09-04. </remarks>
    public static class Methods
    {

        /// <summary> Queries. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <param name="dataToWrite">         The data to write. </param>
        /// <returns> A String. </returns>
        [CLSCompliant( false )]
        public static string Query( this Ivi.Visa.IMessageBasedSession messageBasedSession, string dataToWrite )
        {
            if ( messageBasedSession is null )
                throw new ArgumentNullException( nameof( messageBasedSession ) );
            messageBasedSession.Write( dataToWrite );
            return messageBasedSession.Read();
        }

        /// <summary>
        /// Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
        /// data with the read termination character.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <param name="dataToWrite">         The data to write. </param>
        [CLSCompliant( false )]
        public static void Write( this Ivi.Visa.IMessageBasedSession messageBasedSession, string dataToWrite )
        {
            if ( messageBasedSession is object )
                messageBasedSession.RawIO.Write( dataToWrite );
        }

        /// <summary> Reads the given message based session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <returns> A String. </returns>
        [CLSCompliant( false )]
        public static string Read( this Ivi.Visa.IMessageBasedSession messageBasedSession )
        {
            string result = string.Empty;
            if ( messageBasedSession is object )
                result = messageBasedSession.RawIO.ReadString();
            return result;
        }

        /// <summary> Reads an attribute. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <param name="attribute">           The attribute. </param>
        /// <param name="defaultValue">        The default value. </param>
        /// <returns> The attribute. </returns>
        [CLSCompliant( false )]
        public static int ReadAttribute( this Ivi.Visa.IMessageBasedSession messageBasedSession, Ivi.Visa.NativeVisaAttribute attribute, int defaultValue )
        {
            return messageBasedSession is Ivi.Visa.INativeVisaSession s ? s.GetAttributeInt32( attribute ) : defaultValue;
        }

        /// <summary> Writes an attribute. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <param name="attribute">           The attribute. </param>
        /// <param name="value">               The value. </param>
        [CLSCompliant( false )]
        public static void WriteAttribute( this Ivi.Visa.IMessageBasedSession messageBasedSession, Ivi.Visa.NativeVisaAttribute attribute, int value )
        {
            if ( messageBasedSession is Ivi.Visa.INativeVisaSession s )
            {
                s.SetAttributeInt32( attribute, value );
            }
        }

        /// <summary> Reads a timeout. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <returns> The timeout. </returns>
        [CLSCompliant( false )]
        public static int ReadTimeout( this Ivi.Visa.IMessageBasedSession messageBasedSession )
        {
            return messageBasedSession.ReadAttribute( Ivi.Visa.NativeVisaAttribute.TimeoutValue, 2000 );
        }

        /// <summary> Writes a timeout. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="messageBasedSession"> The message based session. </param>
        /// <param name="timeout">             The timeout. </param>
        [CLSCompliant( false )]
        public static void WriteTimeout( this Ivi.Visa.IMessageBasedSession messageBasedSession, int timeout )
        {
            messageBasedSession.WriteAttribute( Ivi.Visa.NativeVisaAttribute.TimeoutValue, timeout );
        }
    }
}
