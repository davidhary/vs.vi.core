using System;
using System.Diagnostics;

namespace isr.K2400.Hipot.Driver
{

    /// <summary>
    /// Defines an interface for the high potential insulation resistance meter device.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-12-25, 1.0.4011.x. </para>
    /// </remarks>
    public interface IDevice : IDisposable
    {

        #region " CONNECT "

        /// <summary> Connects to the thermal transient device. </summary>
        /// <param name="resourceName"> The name of the resource. </param>
        /// <returns> <c>True</c> if the device connected. </returns>
        bool Connect( string resourceName );

        /// <summary> Disconnects from the thermal transient meter. </summary>
        /// <returns> <c>True</c> if the device  disconnected. </returns>
        bool Disconnect();

        /// <summary> Flushes the read buffers. </summary>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool FlushRead();

        /// <summary> Gets or sets the device identity. </summary>
        /// <value> The identity. </value>
        string Identity { get; }

        /// <summary>
        /// Gets or sets the sentinel indicating if the thermal transient meter is connected.
        /// </summary>
        /// <value> The is connected. </value>
        bool IsConnected { get; }

        /// <summary>
        /// Aborts execution, resets the device to known state and clears queues and register.
        /// </summary>
        /// <remarks> Typically implements SDC, RST, CLS and clearing error queue. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool ResetAndClear();

        /// <summary> Gets or sets the instrument resource address. </summary>
        /// <value> The name of the resource. </value>
        string ResourceName { get; set; }

        #endregion

        #region " CONFIGURE "

        /// <summary> Configures the HIPOT Meter for making measurements. </summary>
        /// <returns> True if success or False if failed. </returns>
        bool Configure();

        /// <summary> Gets or sets the dwell time in seconds. </summary>
        /// <value> The dwell time. </value>
        float DwellTime { get; set; }

        /// <summary> Gets or sets the aperture in power line cycles. </summary>
        /// <value> The aperture. </value>
        float Aperture { get; set; }

        /// <summary> Gets or sets the voltage level in volts. </summary>
        /// <value> The voltage level. </value>
        float VoltageLevel { get; set; }

        /// <summary> Gets or sets the current limit in Amperes. </summary>
        /// <value> The current limit. </value>
        float CurrentLimit { get; set; }

        /// <summary> Gets or sets the resistance limit in ohms. </summary>
        /// <value> The resistance low limit. </value>
        float ResistanceLowLimit { get; set; }

        /// <summary> Gets or sets the resistance range in ohms. </summary>
        /// <value> The resistance range. </value>
        float ResistanceRange { get; set; }

        /// <summary> Gets or sets the sentinel indicating that contact check is supported. </summary>
        /// <value> The contact check supported. </value>
        bool ContactCheckSupported { get; }

        /// <summary> Gets or sets the contact check enabled. </summary>
        /// <value> The contact check enabled. </value>
        bool ContactCheckEnabled { get; set; }

        /// <summary> Gets or sets the contact check bit pattern. </summary>
        /// <value> The contact check bit pattern. </value>
        int ContactCheckBitPattern { get; set; }

        #endregion

        #region " BINNING "

        /// <summary> Gets or sets the duration of the end-of=test strobe. </summary>
        /// <value> The duration of the end-of-test strobe. </value>
        float EotStrobeDuration { get; set; }

        /// <summary> Gets or sets the pass bit pattern. </summary>
        /// <value> The pass bit pattern. </value>
        int PassBitPattern { get; set; }

        /// <summary> Gets or sets the fail bit pattern. </summary>
        /// <value> The fail bit pattern. </value>
        int FailBitPattern { get; set; }

        #endregion

        #region " EVENTS "

        /// <summary>
        /// Sends an Exception message to the client.
        /// </summary>
        event EventHandler<System.Threading.ThreadExceptionEventArgs> ExceptionAvailable;

        /// <summary> Raises the exception available event. </summary>
        /// <remarks>
        /// This method raises the exception if the exception messaging is not handled.
        /// </remarks>
        /// <param name="ex"> Specifies the exception. </param>
        void OnExceptionAvailable( Exception ex );

        /// <summary> Raises the exception available event. </summary>
        /// <remarks>
        /// This method raises the exception if the exception messaging is not handled.
        /// </remarks>
        /// <param name="ex">     Specifies the exception. </param>
        /// <param name="format"> Specifies the event message format. </param>
        /// <param name="args">   Specifies the event message format arguments. </param>
        void OnExceptionAvailable( Exception ex, string format, params object[] args );

        /// <summary>Sends a message to the client.
        /// </summary>
        event EventHandler<MessageEventArgs> MessageAvailable;

        /// <summary> Raises the Message Available event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> The message event arguments. </param>
        void OnMessageAvailable( MessageEventArgs e );

        /// <summary> Raises the Message Available event. </summary>
        /// <param name="traceLevel"> Specifies the event arguments message
        ///                           <see cref="System.Diagnostics.TraceEventType">trace level</see>. </param>
        /// <param name="synopsis">   Specifies the message Synopsis. </param>
        /// <param name="format">     Specifies the message details. </param>
        /// <param name="args">       Arguments to use in the format statement. </param>
        void OnMessageAvailable( TraceEventType traceLevel, string synopsis, string format, params object[] args );

        #endregion

        #region " MEASURE "

        /// <summary> Gets or sets the measured Current. </summary>
        /// <value> The current. </value>
        float Current { get; }

        /// <summary> Gets or sets the measured Current reading. </summary>
        /// <value> The current reading. </value>
        string CurrentReading { get; }

        /// <summary> Makes the HIPOT measurements manually. </summary>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool Measure();

        /// <summary> Gets or sets the measurement outcome for the measurements. </summary>
        /// <value> The outcome. </value>
        MeasurementOutcomes Outcome { get; }

        /// <summary> Gets or sets the measurement Resistance. </summary>
        /// <value> The resistance. </value>
        float Resistance { get; }

        /// <summary> Gets or sets the measurement Resistance reading. </summary>
        /// <value> The resistance reading. </value>
        string ResistanceReading { get; }

        /// <summary> Gets or sets the reading as fetched from the instrument. </summary>
        /// <value> The reading. </value>
        string Reading { get; }

        /// <summary> Gets or sets the parsed measurement status. </summary>
        /// <value> The status. </value>
        long Status { get; }

        /// <summary> Gets or sets the measured voltage. </summary>
        /// <value> The voltage. </value>
        float Voltage { get; }

        /// <summary> Gets or sets the measured voltage reading. </summary>
        /// <value> The voltage reading. </value>
        string VoltageReading { get; }

        #endregion

        #region " Settings "

        /// <summary> Saves the settings. </summary>
        void SaveSettings();

        /// <summary>
        /// Reads the settings and set the device values -- this does not send the settings to the
        /// instrument.
        /// </summary>
        void ReadSettings();

        /// <summary>
        /// Updates the current settings from the device -- this does not read the settings from the
        /// instrument.
        /// </summary>
        void UpdateSettings();

        #endregion

        #region " SYNCHRNIZER "

        /// <summary>
        /// Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see>
        /// to use for marshaling events.
        /// </summary>
        /// <value> The synchronizer. </value>
        System.ComponentModel.ISynchronizeInvoke Synchronizer { get; set; }

        /// <summary>
        /// Sets the <see cref="System.ComponentModel.ISynchronizeInvoke">synchronization object</see>
        /// to use for marshaling events.
        /// </summary>
        /// <param name="value"> The value. </param>
        void SynchronizerSetter( System.ComponentModel.ISynchronizeInvoke value );
        #endregion

        #region " TRIGGER "

        /// <summary> Aborts waiting for a triggered measurement. </summary>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool AbortMeasurement();

        /// <summary> Asserts a trigger for making a triggered measurement. </summary>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool TriggerMeasurement();

        /// <summary> Returns true if measurement completed. </summary>
        /// <returns> true if measurement completed, false if not. </returns>
        bool IsMeasurementCompleted();

        /// <summary>
        /// Prime the instrument to wait for a trigger. This clears the digital outputs and loops until
        /// trigger or external TRG command.
        /// </summary>
        /// <remarks>
        /// Using manual trigger was required because the i2400 instrument fails to respond to the bus
        /// trigger.
        /// </remarks>
        /// <param name="useManualTrigger"> True if using manual trigger. </param>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool PrepareForTrigger( bool useManualTrigger );

        /// <summary> Read the measurements from the instrument. </summary>
        /// <returns> true if it succeeds, false if it fails. </returns>
        bool ReadMeasurements();

        #endregion

    }

    /// <summary> Enumerates the measurement outcomes. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum StatusBit
    {

        /// <summary> An enum constant representing the overflow option. </summary>
        [System.ComponentModel.Description( "Measurement was made while in over-range (OFLO)" )]
        Overflow = 0,

        /// <summary> An enum constant representing the filter enabled option. </summary>
        [System.ComponentModel.Description( "Measurement was made with the filter enabled (Filter)" )]
        FilterEnabled = 1,

        /// <summary> An enum constant representing the hit compliance option. </summary>
        [System.ComponentModel.Description( "Hit real compliance (Compliance)" )]
        HitCompliance = 3,

        /// <summary> An enum constant representing the hit overvoltage protection option. </summary>
        [System.ComponentModel.Description( "Hit over voltage protection limit (OVP)" )]
        HitOvervoltageProtection = 4,

        /// <summary> An enum constant representing the hit range compliance option. </summary>
        [System.ComponentModel.Description( "Hit range compliance (Range Compliance)" )]
        HitRangeCompliance = 16,

        /// <summary> An enum constant representing the failed contact check option. </summary>
        [System.ComponentModel.Description( "Failed contact check (CCH)" )]
        FailedContactCheck = 18
    }

    /// <summary> Enumerates the measurement outcomes. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags()]
    public enum MeasurementOutcomes
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not Defined" )]
        None,

        /// <summary> An enum constant representing the part passed option. </summary>
        [System.ComponentModel.Description( "Measured value is in range" )]
        PartPassed = 1,

        /// <summary>Out of range.</summary>
        [System.ComponentModel.Description( "Measured values is out of range" )]
        PartFailed = 2,

        /// <summary>Measurement Failed.</summary>
        [System.ComponentModel.Description( "Measurement Failed" )]
        MeasurementFailed = 4,

        /// <summary>Measurement Not made.</summary>
        [System.ComponentModel.Description( "Measurement Not Made" )]
        MeasurementNotMade = 8,

        /// <summary>Instrument failed to measure because it hit the voltage limit.</summary>
        [System.ComponentModel.Description( "Compliance" )]
        HitCompliance = 16,

        /// <summary>The message received from the instrument did not parse.</summary>
        [System.ComponentModel.Description( "Unexpected Reading format" )]
        UnexpectedReadingFormat = 32,

        /// <summary>The message received from the instrument did not parse.</summary>
        [System.ComponentModel.Description( "Unexpected Outcome format" )]
        UnexpectedOutcomeFormat = 64,

        /// <summary>One of a number of failure occurred at the device level.</summary>
        [System.ComponentModel.Description( "Unspecified Status Exception" )]
        UnspecifiedStatusException = 128,

        /// <summary>One of a number of failure occurred at the program level.</summary>
        [System.ComponentModel.Description( "Unspecified Program Failure" )]
        UnspecifiedProgramFailure = 256,

        /// <summary>Device Error.</summary>
        [System.ComponentModel.Description( "Device Error" )]
        DeviceError = 512,

        /// <summary> An enum constant representing the failed contact check option. </summary>
        [System.ComponentModel.Description( "Contact Check" )]
        FailedContactCheck = 1024,

        /// <summary> An enum constant representing the undefined 4 option. </summary>
        [System.ComponentModel.Description( "Undefined4" )]
        Undefined4 = 2048,

        /// <summary> An enum constant representing the undefined 5 option. </summary>
        [System.ComponentModel.Description( "Undefined5" )]
        Undefined5 = 4096,

        /// <summary> An enum constant representing the undefined 6 option. </summary>
        [System.ComponentModel.Description( "Undefined6" )]
        Undefined6 = 8192,

        /// <summary>Unknown outcome - assert.</summary>
        [System.ComponentModel.Description( "Unknown outcome - assert" )]
        UnknownOutcome = Undefined4 + Undefined5 + Undefined6
    }
}
