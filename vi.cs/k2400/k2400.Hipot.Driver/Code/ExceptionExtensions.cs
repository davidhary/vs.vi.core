﻿using System;
using System.Collections;
using System.Text;

namespace isr.K2400.Hipot.Driver.ExceptionExtensions
{

    /// <summary> Includes extensions for <see cref="Exception">Exceptions</see>. </summary>
    /// <remarks> (c) 2017 Marco Bertschi.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-03-30, 3.1.6298.x">
    /// https://www.codeproject.com/script/Membership/View.aspx?mid=8888914
    /// https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
    /// </para></remarks>
    public static class Methods
    {

        /// <summary> Converts a value to a full blown string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> Value as a String. </returns>
        internal static string ToFullBlownString( this Exception value )
        {
            return value.ToFullBlownString( int.MaxValue );
        }

        /// <summary> Converts this object to a full blown string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="level"> The level. </param>
        /// <returns> The given data converted to a String. </returns>
        internal static string ToFullBlownString( this Exception value, int level )
        {
            var builder = new StringBuilder();
            int counter = 1;
            while ( value is object && counter <= level )
            {
                _ = builder.AppendLine( $"{counter}-> Level: {counter}" );
                if ( !string.IsNullOrEmpty( value.Message ) )
                    _ = builder.AppendLine( $"{counter}-> Message: {value.Message}" );
                if ( !string.IsNullOrEmpty( value.Source ) )
                    _ = builder.AppendLine( $"{counter}-> Source: {value.Source}" );
                if ( value.TargetSite is object )
                    _ = builder.AppendLine( $"{counter}-> Target Site: {value.TargetSite}" );
                if ( !string.IsNullOrEmpty( value.StackTrace ) )
                    _ = builder.AppendLine( $"{counter}-> Stack Trace: {value.StackTrace}" );
                if ( value.Data is object )
                {
                    foreach ( DictionaryEntry keyValuePair in value.Data )
                        _ = builder.AppendLine( $"{counter}-> {keyValuePair.Key}: {keyValuePair.Value}" );
                }

                value = value.InnerException;
                counter += 1;
            }

            return builder.ToString();
        }
    }
}