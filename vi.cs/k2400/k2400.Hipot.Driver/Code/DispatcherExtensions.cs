using System;
using System.Windows.Threading;

namespace isr.K2400.Hipot.Driver.DispatcherExtensions
{

    /// <summary> Dispatcher extension methods. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class Methods
    {

        /// <summary> Lets Windows process all the messages currently in the message queue. </summary>
        /// <remarks>
        /// https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        /// just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        /// needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        /// Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        /// https://goo.gl/WGFZEU
        /// <code>
        /// Imports System.Windows.Threading
        /// Imports isr.Core.DispatcherExtensions
        /// isr.Core.ApplianceBase.DoEvents()
        /// </code>
        /// <list type="bullet">Benchmarks<item>
        /// 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
        /// 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
        /// </remarks>
        /// <param name="dispatcher"> The dispatcher. </param>
        public static void DoEvents( this Dispatcher dispatcher )
        {
            var frame = new DispatcherFrame();
            _ = dispatcher.BeginInvoke( DispatcherPriority.Background, new DispatcherOperationCallback( ( f ) => {
                (f as DispatcherFrame).Continue = false;
                return null;
            } ), frame );
            Dispatcher.PushFrame( frame );
        }

        /// <summary>
        /// Executes the events operation before and after a  <see cref="System.Threading.Tasks.Task"/> delay.
        /// </summary>
        /// <remarks>
        /// <code>
        /// Imports System.Windows.Threading
        /// 
        /// Windows.Forms.Application.DoEvents()(Timespan.FromMilliseconds(10))
        /// </code>
        /// </remarks>
        /// <param name="dispatcher"> The dispatcher. </param>
        /// <param name="delay">      The delay. </param>
        public static void DoEventsTaskDelay( this Dispatcher dispatcher, TimeSpan delay )
        {
            dispatcher.DoEvents();
            System.Threading.Tasks.Task.Delay( delay ).Wait();
            dispatcher.DoEvents();
        }
    }
}

namespace isr.K2400.Hipot.Driver.My
{

    /// <summary> my library. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary> Lets Windows process all the messages currently in the message queue. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void DoEvents()
        {
            DispatcherExtensions.Methods.DoEvents( Dispatcher.CurrentDispatcher );
        }

        /// <summary> Executes the events task delay operation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="delayTime"> The delay time. </param>
        public static void DoEventsTaskDelay( TimeSpan delayTime )
        {
            DispatcherExtensions.Methods.DoEventsTaskDelay( Dispatcher.CurrentDispatcher, delayTime );
        }
    }
}
