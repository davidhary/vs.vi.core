using System;

namespace isr.K2400.Hipot.Driver
{

    /// <summary>
    /// Parses the stack trace as provided by <see cref="Environment.StackTrace">the
    /// environment</see>.
    /// </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2009-07-10, 1.2.3478. Created
    /// </para>
    /// </remarks>
    public class StackTraceParser
    {

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="stackTrace"> Normally formatted Stack Trace. </param>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        public StackTraceParser( string stackTrace, int skipLines, int totalLines ) : base()
        {
            this._UserCallStack = new System.Text.StringBuilder( 0xFF );
            this._CallStack = new System.Text.StringBuilder( 0xFF );
            if ( _FrameworkLocations is null )
            {
                _FrameworkLocations = new string[] { "at System", "at Microsoft" };
            }

            this.ParseThis( stackTrace, skipLines, totalLines );
        }

        /// <summary>
        /// Constructs this class using the <see cref="Environment.StackTrace">environment stack
        /// trace</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        public StackTraceParser( int skipLines, int totalLines ) : this( Environment.StackTrace, skipLines, totalLines )
        {
        }

        /// <summary> Stack of calls. </summary>
        private System.Text.StringBuilder _CallStack;

        /// <summary> Gets a normally formatted Call Stack. </summary>
        /// <value> A stack of calls. </value>
        public string CallStack => this._CallStack.ToString().TrimEnd( Environment.NewLine.ToCharArray() );

        /// <summary> The framework locations. </summary>
        private static string[] _FrameworkLocations;

        /// <summary> Returns true if the line is a framework line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> True if framework line, false if not. </returns>
        public static bool IsFrameworkLine( string value )
        {
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                foreach ( string locationName in _FrameworkLocations )
                {
                    if ( value.TrimStart().StartsWith( locationName, StringComparison.OrdinalIgnoreCase ) )
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary> Parses the stack trace into the user and full call stacks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="stackTrace"> Normally formatted Stack Trace. </param>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        public void Parse( string stackTrace, int skipLines, int totalLines )
        {
            this.ParseThis( stackTrace, skipLines, totalLines );
        }

        /// <summary>
        /// Parses the <see cref="Environment.StackTrace">environment stack trace</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        public void Parse( int skipLines, int totalLines )
        {
            this.ParseThis( skipLines, totalLines );
        }

        /// <summary>
        /// Parses the <see cref="Environment.StackTrace">environment stack trace</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        private void ParseThis( int skipLines, int totalLines )
        {
            this.ParseThis( Environment.StackTrace, skipLines, totalLines );
        }

        /// <summary> Parses the stack trace into the user and full call stacks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="stackTrace"> Normally formatted Stack Trace. </param>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        private void ParseThis( string stackTrace, int skipLines, int totalLines )
        {
            this._UserCallStack = new System.Text.StringBuilder( 0xFF );
            this._CallStack = new System.Text.StringBuilder( 0xFF );
            if ( string.IsNullOrWhiteSpace( stackTrace ) )
            {
                return;
            }

            var callStack = stackTrace.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            if ( callStack.Length <= 0 )
            {
                return;
            }

            bool isFrameworkBlock = false;
            int lineNumber = 0;
            int lineCount = 0;
            foreach ( string line in callStack )
            {
                if ( totalLines == 0 || lineCount <= totalLines )
                {
                    lineNumber += 1;
                    if ( lineNumber > skipLines )
                    {
                        if ( IsFrameworkLine( line ) )
                        {
                            isFrameworkBlock = true;
                            _ = this._CallStack.AppendLine( line );
                        }
                        else
                        {
                            if ( isFrameworkBlock )
                            {
                                // if previously had external code, append
                                _ = this._UserCallStack.AppendLine( "   at [External Code]" );
                                isFrameworkBlock = false;
                            }

                            _ = this._CallStack.AppendLine( line );
                            _ = this._UserCallStack.AppendLine( line );
                            lineCount += 1;
                        }
                    }
                }
            }
        }

        /// <summary> Stack of user calls. </summary>
        private System.Text.StringBuilder _UserCallStack;

        /// <summary> Gets the user call stack, which includes no .NET Framework functions. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A String. </returns>
        public string UserCallStack()
        {
            return this._UserCallStack.ToString().TrimEnd( Environment.NewLine.ToCharArray() );
        }

        /// <summary> Parses and returns the user call stack. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="stackTrace"> Normally formatted Stack Trace. </param>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        /// <returns> A String. </returns>
        public static string UserCallStack( string stackTrace, int skipLines, int totalLines )
        {
            var parser = new StackTraceParser( stackTrace, skipLines, totalLines );
            return parser.UserCallStack();
        }

        /// <summary>
        /// Parses and returns the user call stack using the
        /// <see cref="Environment.StackTrace">environment stack trace</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="skipLines">  Specifies the number of lines to skip. Two lines include the
        ///                           environment methods. </param>
        /// <param name="totalLines"> Specifies the maximum number of lines to include in the trace. Use 0
        ///                           to include all lines. </param>
        /// <returns> A String. </returns>
        public static string UserCallStack( int skipLines, int totalLines )
        {
            var parser = new StackTraceParser( skipLines, totalLines );
            return parser.UserCallStack();
        }
    }
}
