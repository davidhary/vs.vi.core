using System;
using System.Diagnostics;

namespace isr.K2400.Hipot.Driver
{

    /// <summary> Defines an event arguments class for messages. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2009-05-01, 1.1.3408. Created </para>
    /// </remarks>
    public class MessageEventArgs : EventArgs
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="broadcast"> The broadcast. </param>
        /// <param name="trace">     The trace. </param>
        /// <param name="synopsis">  The synopsis. </param>
        /// <param name="details">   The details. </param>
        public MessageEventArgs( TraceEventType broadcast, TraceEventType trace, string synopsis, string details ) : base()
        {
            this.Timestamp = DateTimeOffset.Now;
            this.BroadcastLevel = broadcast;
            this.TraceLevel = trace;
            this.Synopsis = synopsis;
            this.Details = details;
            this._Format = "{0:HH:mm:ss.fff}, {1}, {2}, {3}";
        }

        /// <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="synopsis"> The synopsis. </param>
        /// <param name="format">   The format. </param>
        /// <param name="args">     The arguments. </param>
        public MessageEventArgs( string synopsis, string format, params object[] args ) : this( TraceEventType.Information, TraceEventType.Information, synopsis, format, args )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trace">    The trace. </param>
        /// <param name="synopsis"> The synopsis. </param>
        /// <param name="format">   The format. </param>
        /// <param name="args">     The arguments. </param>
        public MessageEventArgs( TraceEventType trace, string synopsis, string format, params object[] args ) : this( trace, trace, synopsis, format, args )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="broadcast"> The broadcast. </param>
        /// <param name="trace">     The trace. </param>
        /// <param name="synopsis">  The synopsis. </param>
        /// <param name="format">    The format. </param>
        /// <param name="args">      The arguments. </param>
        public MessageEventArgs( TraceEventType broadcast, TraceEventType trace, string synopsis, string format, params object[] args ) : this( broadcast, trace, synopsis, string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="synopsis"> The synopsis. </param>
        /// <param name="details">  The details. </param>
        public MessageEventArgs( string synopsis, string details ) : this( TraceEventType.Information, TraceEventType.Information, synopsis, details )
        {
        }


        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the message details. </summary>
        /// <value> The details. </value>
        public string Details { get; private set; }

        /// <summary> Gets or sets the event synopsis. </summary>
        /// <value> The synopsis. </value>
        public string Synopsis { get; private set; }

        /// <summary> Gets or sets the event time stamp. </summary>
        /// <value> The timestamp. </value>
        public DateTimeOffset Timestamp { get; private set; }

        /// <summary> Gets or sets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel { get; private set; }

        /// <summary> Gets or sets the Broadcast level. </summary>
        /// <value> The broadcast level. </value>
        public TraceEventType BroadcastLevel { get; private set; }

        #endregion

        #region " METHODS "

        /// <summary> Describes the format to use. </summary>
        private readonly string _Format;

        /// <summary> Returns a message based on the default format. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A string that represents the current object. </returns>
        public override string ToString()
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, this._Format, this.Timestamp, this.TraceLevel, this.Synopsis, this.Details );
        }

        #endregion

    }
}
