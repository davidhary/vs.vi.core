using System;
using System.Diagnostics;

using isr.K2400.Hipot.Driver.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.K2400.Hipot.Driver
{
    public partial class Device
    {

        #region " SCPI SYNTAX "

        /// <summary> Gets the Clear Status (CLS) command. </summary>
        public const string ClearExecutionStateCommand = "*CLS";

        /// <summary> The default error queue clear command. </summary>
        public const string ClearErrorQueueCommand = ":STAT:QUE:CLEAR";

        /// <summary> The default error queue query command. </summary>
        public const string ErrorQueueQueryCommand = ":STAT:QUE?";

        /// <summary> Gets the Identify query (*IDN?) command. </summary>
        public const string IdentifyQueryCommand = "*IDN?";

        /// <summary> Gets the reset to know state (*RST) command. </summary>
        public const string ResetKnownStateCommand = "*RST";

        /// <summary> Gets the Service Request Enable (*SRE) enable command. </summary>
        public const string ServiceRequestEnableCommand = "*SRE {0:D}";

        /// <summary> Gets the Standard Event Enable (*ESE) command. </summary>
        public const string StandardEventEnableCommand = "*ESE {0:D}";

        /// <summary> Gets the Standard Event Enable (*ESR?) command. </summary>
        public const string StandardEventStatusQueryCommand = "*ESR?";

        #endregion

        #region " STATUS REGISTER "

        /// <summary>
        /// Sets the device to issue an SRQ upon any of the SCPI events. Uses *ESE to select (mask) the
        /// events that will issue SRQ and  *SRE to select (mask) the event registers to be included in
        /// the bits that will issue an SRQ.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="standardEventEnableBits">  Specifies standard events will issue an SRQ. </param>
        /// <param name="serviceRequestEnableBits"> Specifies which status registers will issue an SRQ. </param>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public virtual bool EnableServiceRequest( int standardEventEnableBits, int serviceRequestEnableBits )
        {
            var commandBuilder = new System.Text.StringBuilder();
            _ = commandBuilder.Append( ClearExecutionStateCommand );
            _ = commandBuilder.Append( "; " );
            _ = commandBuilder.Append( string.Format( System.Globalization.CultureInfo.InvariantCulture, StandardEventEnableCommand, standardEventEnableBits ) );
            _ = commandBuilder.Append( "; " );
            _ = commandBuilder.Append( string.Format( System.Globalization.CultureInfo.InvariantCulture, ServiceRequestEnableCommand, serviceRequestEnableBits ) );

            // Check if necessary; clear the service request bit
            _ = this.ReadStatusByte();
            this.Session.FormattedIO.WriteLine( commandBuilder.ToString() );
            return this.IsLastVisaOperationSuccess;
        }

        /// <summary> Gets the last read status byte. </summary>
        /// <value> The cached status byte. </value>
        public int CachedStatusByte { get; set; }

        /// <summary> Returns the service request register status byte. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The status byte. </returns>
        public int ReadStatusByte()
        {
            if ( this.Session is object )
            {
                this.CachedStatusByte = Conversions.ToInteger( this.Session.ReadStatusByte() );
                return this.CachedStatusByte >= 0 ? this.CachedStatusByte : 256 + this.CachedStatusByte;
            }

            return this.CachedStatusByte;
        }

        #endregion

        #region " STANDARD EVENTS "

        /// <summary>
        /// Returns a True is the service request register status byte reports error available.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="standardErrorBits"> Specifies the standard device register error bits. </param>
        /// <returns> true if standard error, false if not. </returns>
        public virtual bool HasStandardError( int standardErrorBits )
        {
            return (this.ReadStandardEventStatus() & standardErrorBits) != 0;
        }

        /// <summary> Reads the standard event status from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The standard event status. </returns>
        public virtual int ReadStandardEventStatus()
        {
            this.Session.FormattedIO.WriteLine( StandardEventStatusQueryCommand );
            this.StandardEventStatus = ( int ) this.Session.FormattedIO.ReadInt64();
            return this.StandardEventStatus;
        }

        /// <summary> Gets the standard event status. </summary>
        /// <value> The standard event status. </value>
        public int StandardEventStatus { get; set; }


        #endregion

        #region " IEEE 488.2: VISA AND DEVICE ERROR MANAGEMENT "

        /// <summary>
        /// Gets a report of the error stored in the cached error queue event status register (ESR)
        /// information.
        /// </summary>
        /// <value> The device errors. </value>
        public virtual string DeviceErrors
        {
            get {
                var message = new System.Text.StringBuilder();
                if ( this._ErrorQueue.Length > 0 )
                {
                    if ( message.Length > 0 )
                    {
                        _ = message.AppendLine();
                    }

                    _ = message.AppendLine( "The device reported the following error(s) from the error queue:" );
                    _ = message.Append( this._ErrorQueue.ToString() );
                }

                return message.ToString();
            }
        }

        /// <summary> Queue of errors. </summary>
        private System.Text.StringBuilder _ErrorQueue;

        /// <summary> Gets the error queue cache. </summary>
        /// <value> The error queue cache. </value>
        public string ErrorQueueCache => this._ErrorQueue is null || this._ErrorQueue.Length == 0 ? string.Empty : this._ErrorQueue.ToString();

        /// <summary> true to had error. </summary>
        private bool _HadError;

        /// <summary>
        /// Returns True if the last operation reported an error by way of a service request. The error
        /// is reported if the cached error indicator is true or a new error is reported by the service
        /// request register.
        /// </summary>
        /// <value> The had error. </value>
        public bool HadError
        {
            get {
                this._HadError = this._HadError || this.HasError();
                return this._HadError;
            }

            set => this._HadError = value;
        }

        /// <summary> The error available bits. </summary>
        private readonly int _ErrorAvailableBits;

        /// <summary>
        /// Returns a True is the service request register status byte reports error available.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if error, false if not. </returns>
        public bool HasError()
        {
            return this.HasError( this._ErrorAvailableBits );
        }

        /// <summary>
        /// Returns a True is the service request register status byte reports error available.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="errorBits"> The error bits. </param>
        /// <returns> true if error, false if not. </returns>
        public bool HasError( int errorBits )
        {
            return this.Session is object && (( int ) this.Session.ReadStatusByte() & errorBits) != 0;
        }

        /// <summary> Check the status byte for error bits and latches the last error sentinel. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if an error is available, false if not. </returns>
        public virtual bool IsErrorAvailable()
        {
            this.HadError = false;
            return this.HadError;
        }

        /// <summary>
        /// Returns true if the device had an error include standard query or execution errors. Clears
        /// the error queue and status by reading the error queue.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <returns> True if error occurred. </returns>
        public virtual bool HasDeviceError( bool flushReadFirst )
        {
            if ( !this.IsErrorAvailable() && (this.CachedStatusByte & ( int ) ServiceRequests.StandardEvent) == 0 )
            {
                return true;
            }

            if ( flushReadFirst )
            {
                this.DiscardUnreadData();
            }

            // In order to keep compatibility, we clear the error here by reading the error status.
            _ = this.ReadErrorQueue();

            // check for query and other errors reported by the standard event register
            var standardErrorBits = StandardEvents.CommandError | StandardEvents.DeviceDependentError | StandardEvents.ExecutionError | StandardEvents.QueryError;
            return this.HadError || this.HasStandardError( ( int ) standardErrorBits );
        }

        /// <summary>
        /// Gets or sets the last read error queue. Setting the error allows emulating or clearing errors
        /// when the error queue command is not supported or not using devices.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The error queue. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public string ReadErrorQueue()
        {
            try
            {

                // clear the queue string
                this._ErrorQueue = new System.Text.StringBuilder();

                // loop while we have an error
                while ( (this.ReadStatusByte() & this._ErrorAvailableBits) != 0 )
                {
                    if ( this._ErrorQueue.Length > 0 )
                    {
                        _ = this._ErrorQueue.Append( Environment.NewLine );
                    }

                    // save the error status
                    this.HadError = true;

                    // send the command to get the queue
                    this.Session.Write( ErrorQueueQueryCommand );

                    // Add the error string
                    _ = this._ErrorQueue.AppendFormat( "Device error {0}", this.Session.RawIO.ReadString() );
                }

                // return the queue
                return this._ErrorQueue.ToString();
            }
            catch ( Exception ex )
            {

                // if we have an error, return a note of it
                return "Exception occurred getting the error queue. " + ex.ToFullBlownString();
            }
        }

        /// <summary>
        /// Reports on VISA or device errors. Can be used with queries because the report does not
        /// require querying the instrument.
        /// </summary>
        /// <remarks>
        /// This method is called after a VISA operation, such as a query, that could cause a query
        /// terminated failure if accessing the instrument.
        /// </remarks>
        /// <param name="synopsis"> The report synopsis. </param>
        /// <param name="format">   The report details format. </param>
        /// <param name="args">     The report details arguments. </param>
        /// <returns> True if no error occurred. </returns>
        public bool ReportVisaOperationOkay( string synopsis, string format, params object[] args )
        {
            if ( this.IsLastVisaOperationFailed )
            {
                this.OnMessageAvailable( TraceEventType.Verbose, synopsis, "{0}. Instrument {1} encountered VISA errors: {2}{3}{4}", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), this.ResourceName, BuildVisaStatusDetails( this.LastErrorStatus ), Environment.NewLine, StackTraceParser.UserCallStack( 4, 10 ) );
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reports on device errors. Can only be used after receiving a full reply from the instrument.
        /// Use <see cref="ReportVisaOperationOkay">report visa operation</see> to avoid accessing the
        /// information from the instrument.
        /// </summary>
        /// <remarks>
        /// This method is called after a VISA operation, such as a query, that could cause a query
        /// terminated failure if accessing the instrument.
        /// </remarks>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="synopsis">       The report synopsis. </param>
        /// <param name="format">         The report details format. </param>
        /// <param name="args">           The report details arguments. </param>
        /// <returns> True if no error occurred. </returns>
        public bool ReportDeviceOperationOkay( bool flushReadFirst, string synopsis, string format, params object[] args )
        {
            if ( this.HasDeviceError( flushReadFirst ) )
            {
                this.OnMessageAvailable( TraceEventType.Verbose, synopsis, "{0}. Instrument {1} encountered errors: {2}{3}{4}", string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), this.ResourceName, this.DeviceErrors, Environment.NewLine, StackTraceParser.UserCallStack( 4, 10 ) );
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reports on VISA or device errors. Can only be used after receiving a full reply from the
        /// instrument. Use <see cref="ReportVisaOperationOkay">report visa operation</see> to avoid
        /// accessing the information from the instrument.
        /// </summary>
        /// <remarks>
        /// This method is called after a VISA operation. It checked for error and sends
        /// <see cref="MessageAvailable">message available</see> if an error occurred.
        /// </remarks>
        /// <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
        /// <param name="synopsis">       The report synopsis. </param>
        /// <param name="format">         The report details format. </param>
        /// <param name="args">           The report details arguments. </param>
        /// <returns> True if no error occurred. </returns>
        public bool ReportVisaDeviceOperationOkay( bool flushReadFirst, string synopsis, string format, params object[] args )
        {
            return this.ReportVisaOperationOkay( synopsis, format, args ) && this.ReportDeviceOperationOkay( flushReadFirst, synopsis, format, args );
        }

        /// <summary>
        /// Throws a <see cref="VisaException">VISA exception</see>
        /// Can be used with queries.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
        /// <param name="format"> The report details format. </param>
        /// <param name="args">   The report details arguments. </param>
        /// <returns> True if success. </returns>
        public bool ThrowVisaExceptionIfError( string format, params object[] args )
        {
            if ( this.IsLastVisaOperationFailed && this.Session is object )
            {
                var builder = new System.Text.StringBuilder();
                _ = builder.AppendFormat( format, args );
                _ = builder.AppendFormat( ". Visa Status: {0}", this.LastErrorStatus );
                throw new VisaException( builder.ToString() );
            }

            return true;
        }

        /// <summary>
        /// Throws a <see cref="isr.K2400.Hipot.Driver.DeviceException">device exceptions</see>. Flushes the read buffer if an
        /// error occurred. Cannot be used before a query is completed.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.K2400.Hipot.Driver.DeviceException"> Thrown when a Device error condition occurs. </exception>
        /// <param name="format"> The report details format. </param>
        /// <param name="args">   The report details arguments. </param>
        /// <returns> True if success. </returns>
        public bool ThrowDeviceExceptionIfError( string format, params object[] args )
        {
            if ( this.HasDeviceError( true ) )
            {
                var builder = new System.Text.StringBuilder();
                _ = builder.AppendFormat( format, args );
                _ = builder.AppendFormat( ". The device '{0}' reported the following errors: {1}", this.ResourceName, this.DeviceErrors );
                throw new DeviceException( builder.ToString() );
            }

            return true;
        }

        /// <summary>
        /// Throws an <see cref="isr.K2400.Hipot.Driver.DeviceException">instrument exceptions</see>. Flushes the read buffer if
        /// an error occurred.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> The report details format. </param>
        /// <param name="args">   The report details arguments. </param>
        /// <returns> True if success. </returns>
        public bool RaiseVisaOrDeviceExceptionIfError( string format, params object[] args )
        {
            return this.ThrowVisaExceptionIfError( format, args ) && this.ThrowDeviceExceptionIfError( format, args );
        }

        #endregion

        #region " VISA "

        /// <summary>
        /// Builds a VISA warning or error message. This method does not require additional information
        /// from the instrument.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The state code from the last operation. </param>
        /// <returns> A String. </returns>
        public static string BuildVisaStatusDetails( int value )
        {
            if ( value == 0 )
            {
                return "OK";
            }
            else
            {
                var visaMessage = new System.Text.StringBuilder();
                _ = value > 0 ? visaMessage.Append( "VISA Warning" ) : visaMessage.Append( "VISA Error" );

                string description = Ivi.Visa.NativeErrorCode.GetMacroNameFromStatusCode( value );
                _ = string.Equals( value.ToString(), description, StringComparison.CurrentCultureIgnoreCase )
                    ? visaMessage.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, ": {0}.", value )
                    : visaMessage.AppendFormat( System.Globalization.CultureInfo.CurrentCulture, " {0}: {1}.", value, description );

                return visaMessage.ToString();
            }
        }

        /// <summary> Selectively clears the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool ClearActiveState()
        {
            if ( this.Session is object )
            {
                this.Session.Clear();
            }

            return !this.HasError();
        }

        /// <summary>
        /// Clears the error Queue and resets all event registers to zero by issuing the IEEE488.2 common
        /// command.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool ClearExecutionState()
        {
            return this.Execute( ClearErrorQueueCommand );
        }

        /// <summary> The message available bits. </summary>
        private readonly int _MessageAvailableBits;

        /// <summary>
        /// Reads and discards all data from the VISA session until the END indicator is read.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void DiscardUnreadData()
        {
            if ( this.Session is object )
            {
                while ( this.IsMessageAvailable() )
                    _ = this.Session.RawIO.ReadString();
            }
        }

        /// <summary> Executes the specified command. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool Execute( string value )
        {
            if ( this.Session is object && !string.IsNullOrWhiteSpace( value ) )
            {
                this.Session.FormattedIO.WriteLine( value );
            }

            return this.IsLastVisaOperationSuccess;
        }

        /// <summary> Returns true if the last VISA operation ended with a warning. </summary>
        /// <value> The is last visa operation success. </value>
        public bool IsLastVisaOperationSuccess => this.Session is null || this.LastErrorStatus == 0;

        /// <summary> Returns true if the last VISA operation ended with a warning. </summary>
        /// <value> The is last visa operation failed. </value>
        public bool IsLastVisaOperationFailed => this.Session is object && this.LastErrorStatus < 0;

        /// <summary>
        /// Returns the device to its default known state by issuing the *RST IEEE488.2 common command.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> true if it succeeds, false if it fails. </returns>
        public bool ResetKnownState()
        {
            return this.Execute( ResetKnownStateCommand );
        }

        /// <summary>
        /// Reads the device status data byte and returns True if the massage available bits were set.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> True if the device has data in the queue. </returns>
        public virtual bool IsMessageAvailable()
        {
            return this.Session is object && (( int ) this.Session.ReadStatusByte() & this._MessageAvailableBits) != 0;
        }

        /// <summary> Gets or sets reference to the VISA session. </summary>
        /// <value> The session. </value>
        private Ivi.Visa.IMessageBasedSession Session { get; set; }

        #endregion

    }

    #region " IEEE 488.2 EVENT FLAGS "

    /// <summary> Gets or sets the status byte bits of the service request register. </summary>
    /// <remarks>
    /// Extends the VISA event status flags. Because the VISA status returns a signed byte, we need
    /// to use Int16. Enumerates the Status Byte Register Bits. Use STB? or status.request_event to
    /// read this register. Use *SRE or status.request_enable to enable these services. This
    /// attribute is used to read the status byte, which is returned as a numeric value. The binary
    /// equivalent of the returned value indicates which register bits are set. (c) 2010 Integrated
    /// Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [Flags()]
    public enum ServiceRequests
    {

        /// <summary> Not specified. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> Bit B0, Measurement Summary Bit (MSB). Set summary bit indicates
        /// that an enabled measurement event has occurred. </summary>
        [System.ComponentModel.Description( "Measurement Event (MSB)" )]
        MeasurementEvent = 0x1,

        /// <summary> Bit B1, System Summary Bit (SSB). Set summary bit indicates
        /// that an enabled system event has occurred. </summary>
        [System.ComponentModel.Description( "System Event (SSB)" )]
        SystemEvent = 0x2,

        /// <summary> Bit B2, Error Available (EAV). Set summary bit indicates that
        /// an error or status message is present in the Error Queue. </summary>
        [System.ComponentModel.Description( "Error Available (EAV)" )]
        ErrorAvailable = 0x4,

        /// <summary> Bit B3, Questionable Summary Bit (QSB). Set summary bit indicates
        /// that an enabled questionable event has occurred. </summary>
        [System.ComponentModel.Description( "Questionable Event (QSB)" )]
        QuestionableEvent = 0x8,

        /// <summary> Bit B4 (16), Message Available (MAV). Set summary bit indicates that
        /// a response message is present in the Output Queue. </summary>
        [System.ComponentModel.Description( "Message Available (MAV)" )]
        MessageAvailable = 0x10,

        /// <summary> Bit B5, Event Summary Bit (ESB). Set summary bit indicates 
        /// that an enabled standard event has occurred. </summary>
        [System.ComponentModel.Description( "Standard Event (ESB)" )]
        StandardEvent = 0x20, // (32) ESB

        /// <summary> Bit B6 (64), Request Service (RQS)/Master Summary Status (MSS).
        /// Set bit indicates that an enabled summary bit of the Status Byte Register
        /// is set. Depending on how it is used, Bit B6 of the Status Byte Register
        /// is either the Request for Service (RQS) bit or the Master Summary Status
        /// (MSS) bit: When using the GPIB serial poll sequence of the unit to obtain
        /// the status byte (serial poll byte), B6 is the RQS bit. When using
        /// status.condition or the *STB? common command to read the status byte,
        /// B6 is the MSS bit. </summary>
        [System.ComponentModel.Description( "Request Service (RQS)/Master Summary Status (MSS)" )]
        RequestingService = 0x40,

        /// <summary> Bit B7 (128), Operation Summary (OSB). Set summary bit indicates that
        /// an enabled operation event has occurred. </summary>
        [System.ComponentModel.Description( "Operation Event (OSB)" )]
        OperationEvent = 0x80,

        /// <summary> Includes all bits. </summary>
        [System.ComponentModel.Description( "All" )]
        All = 0xFF, // 255

        /// <summary> Unknown value due to, for example, error trying to get value from the
        /// instrument. </summary>
        [System.ComponentModel.Description( "Unknown" )]
        Unknown = 0x100
    }

    /// <summary> Enumerates the status byte flags of the standard event register. </summary>
    /// <remarks>
    /// Enumerates the Standard Event Status Register Bits. Read this information using ESR? or
    /// status.standard.event. Use *ESE or status.standard.enable or event status enable to enable
    /// this register. These values are used when reading or writing to the standard event registers.
    /// Reading a status register returns a value. The binary equivalent of the returned value
    /// indicates which register bits are set. The least significant bit of the binary number is bit
    /// 0, and the most significant bit is bit 15. For example, assume value 9 is returned for the
    /// enable register. The binary equivalent is 0000000000001001. This value indicates that bit 0
    /// (OPC) and bit 3 (DDE)
    /// are set.
    /// </remarks>
    [Flags()]
    public enum StandardEvents
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> Bit B0, Operation Complete (OPC). Set bit indicates that all
        /// pending selected device operations are completed and the unit is ready to
        /// accept new commands. The bit is set in response to an *OPC command.
        /// The ICL function opc() can be used in place of the *OPC command. </summary>
        [System.ComponentModel.Description( "Operation Complete (OPC)" )]
        OperationComplete = 1,

        /// <summary> Bit B1, Request Control (RQC). Set bit indicates that.... </summary>
        [System.ComponentModel.Description( "Request Control (RQC)" )]
        RequestControl = 0x2,

        /// <summary> Bit B2, Query Error (QYE). Set bit indicates that you attempted
        /// to read data from an empty Output Queue. </summary>
        [System.ComponentModel.Description( "Query Error (QYE)" )]
        QueryError = 0x4,

        /// <summary> Bit B3, Device-Dependent Error (DDE). Set bit indicates that an
        /// instrument operation did not execute properly due to some internal
        /// condition. </summary>
        [System.ComponentModel.Description( "Device Dependent Error (DDE)" )]
        DeviceDependentError = 0x8,

        /// <summary> Bit B4 (16), Execution Error (EXE). Set bit indicates that the unit
        /// detected an error while trying to execute a command. 
        /// This is used by Quatech to report No Contact. </summary>
        [System.ComponentModel.Description( "Execution Error (EXE)" )]
        ExecutionError = 0x10,

        /// <summary> Bit B5 (32), Command Error (CME). Set bit indicates that a
        /// command error has occurred. Command errors include:<p>
        /// IEEE-488.2 syntax error — unit received a message that does not follow
        /// the defined syntax of the IEEE-488.2 standard.  </p><p>
        /// Semantic error — unit received a command that was misspelled or received
        /// an optional IEEE-488.2 command that is not implemented.  </p><p>
        /// The instrument received a Group Execute Trigger (GET) inside a program
        /// message.  </p> </summary>
        [System.ComponentModel.Description( "Command Error (CME)" )]
        CommandError = 0x20,

        /// <summary> Bit B6 (64), User Request (URQ). Set bit indicates that the LOCAL
        /// key on the SourceMeter front panel was pressed. </summary>
        [System.ComponentModel.Description( "User Request (URQ)" )]
        UserRequest = 0x40,

        /// <summary> Bit B7 (128), Power ON (PON). Set bit indicates that the instrument
        /// has been turned off and turned back on since the last time this register
        /// has been read. </summary>
        [System.ComponentModel.Description( "Power Toggled (PON)" )]
        PowerToggled = 0x80,

        /// <summary> Unknown value due to, for example, error trying to get value from the
        /// instrument. </summary>
        [System.ComponentModel.Description( "Unknown" )]
        Unknown = 0x100,

        /// <summary> Includes all bits. </summary>
        [System.ComponentModel.Description( "All" )]
        All = 0xFF // 255
    }

    #endregion

    /// <summary>   An extensions. </summary>
    /// <remarks>   David, 2021-09-04. </remarks>
    public static class Extensions
    {

        /// <summary>
        /// Gets the <see cref="System.ComponentModel.DescriptionAttribute"/> of an
        /// <see cref="System.Enum"/> type value.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="System.Enum"/> type value. </param>
        /// <returns>
        /// A string containing the text of the
        /// <see cref="System.ComponentModel.DescriptionAttribute"/>.
        /// </returns>
        public static string Description( this Enum value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else
            {
                string candidate = value.ToString();
                var fieldInfo = value.GetType().GetField( candidate );
                System.ComponentModel.DescriptionAttribute[] attributes = ( System.ComponentModel.DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( System.ComponentModel.DescriptionAttribute ), false );
                if ( attributes is object && attributes.Length > 0 )
                {
                    candidate = attributes[0].Description;
                }

                return candidate;
            }
        }
    }
}
