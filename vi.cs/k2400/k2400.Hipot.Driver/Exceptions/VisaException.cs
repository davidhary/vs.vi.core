using System;

namespace isr.K2400.Hipot.Driver
{

    /// <summary> Handles General VISA exceptions. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-12-25, 1.0.4011. Created </para>
    /// </remarks>
    [Serializable()]
    public class VisaException : BaseException
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VisaException() : this( UnknownErrorCode, "Unknown VISA Exception" )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="VisaException"/> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        public VisaException( string message ) : base( message )
        {
            this.ErrorCode = UnknownErrorCode;
        }

        /// <summary> The unknown error code. </summary>
        public const int UnknownErrorCode = -9999;

        /// <summary> Gets or sets the error code. </summary>
        /// <value> The error code. </value>
        public int ErrorCode { get; private set; }

        /// <summary> Initializes a new instance of the <see cref="VisaException"/> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="errorCode"> The error code. </param>
        public VisaException( int errorCode ) : this( errorCode, "VISA Exception" )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="errorCode"> The error code. </param>
        /// <param name="message">   The message. </param>
        public VisaException( int errorCode, string message ) : base( message )
        {
            this.ErrorCode = errorCode;
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        public VisaException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="errorCode">      The error code. </param>
        /// <param name="innerException"> The inner exception. </param>
        public VisaException( int errorCode, Exception innerException ) : base( "Visa Exception", innerException )
        {
            this.ErrorCode = errorCode;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
        /// data.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        ///                        that holds the serialized object data about the exception being
        ///                        thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        ///                        that contains contextual information about the source or destination. 
        /// </param>
        protected VisaException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        /// <summary>
        /// Overrides the <see cref="GetObjectData" /> method to serialize custom values.
        /// </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        /// <param name="info">     The
        ///                         <see cref="System.Runtime.Serialization.SerializationInfo">serialization
        ///                         information</see>. </param>
        /// <param name="context">  The
        ///                         <see cref="System.Runtime.Serialization.StreamingContext">streaming
        ///                         context</see> for the exception. </param>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            if ( info is null )
            {
                return;
            }

            info.AddValue( nameof( this.ErrorCode ), this.ErrorCode, typeof( int ) );
            base.GetObjectData( info, context );
        }

        #endregion

    }
}
