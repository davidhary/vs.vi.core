using System;

namespace isr.K2400.Hipot.Driver
{

    /// <summary>
    /// An inheritable exception for use by ISR framework classes and applications.
    /// </summary>
    /// <remarks>
    /// Inherits from System.Exception per FxCop design rule CA1958 which specifies that "Types do
    /// not extend inheritance vulnerable types" and further explains that "This [Application
    /// Exception] base exception type does not provide any additional value for framework classes."
    /// <para>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [Serializable()]
    public class BaseException : Exception
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public BaseException() : base()
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        public BaseException( string message ) : base( message )
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        public BaseException( string message, Exception innerException ) : base( message, innerException )
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> The format. </param>
        /// <param name="args">   The args. </param>
        public BaseException( string format, params object[] args ) : base( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) )
        {
            this.ObtainEnvironmentInformation();
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="innerException"> Specifies the InnerException. </param>
        /// <param name="format">         Specifies the exception formatting. </param>
        /// <param name="args">           Specifies the message arguments. </param>
        public BaseException( Exception innerException, string format, params object[] args ) : base( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), innerException )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
        /// data.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        ///                        that holds the serialized object data about the exception being
        ///                        thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        ///                        that contains contextual information about the source or destination. 
        /// </param>
        protected BaseException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
            if ( info is null )
            {
                return;
            }

            this._MachineName = info.GetString( "machineName" );
            this._CreatedDateTime = info.GetDateTime( "createdDateTime" );
            this._AppDomainName = info.GetString( "appDomainName" );
            this._ThreadIdentityName = info.GetString( "threadIdentity" );
            this._WindowsIdentityName = info.GetString( "windowsIdentity" );
            this._OSVersion = info.GetString( "OSVersion" );
            this._AdditionalInformation = ( System.Collections.Specialized.NameValueCollection ) info.GetValue( "additionalInformation", typeof( System.Collections.Specialized.NameValueCollection ) );
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Overrides the <see cref="GetObjectData" /> method to serialize custom values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="info">    The <see cref="System.Runtime.Serialization.SerializationInfo">serialization
        ///                        information</see>. </param>
        /// <param name="context"> The <see cref="System.Runtime.Serialization.StreamingContext">streaming
        ///                        context</see> for the exception. </param>
        [System.Security.Permissions.SecurityPermission( System.Security.Permissions.SecurityAction.Demand, SerializationFormatter = true )]
        public override void GetObjectData( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context )
        {
            if ( info is null )
            {
                return;
            }

            info.AddValue( "machineName", this.MachineName, typeof( string ) );
            info.AddValue( "createdDateTime", this.CreatedDateTime );
            info.AddValue( "appDomainName", this.AppDomainName, typeof( string ) );
            info.AddValue( "threadIdentity", this.ThreadIdentityName, typeof( string ) );
            info.AddValue( "windowsIdentity", this.WindowsIdentityName, typeof( string ) );
            info.AddValue( "OSVersion", this.OSVersion, typeof( string ) );
            info.AddValue( "additionalInformation", this.AdditionalInformation, typeof( System.Collections.Specialized.NameValueCollection ) );
            base.GetObjectData( info, context );
        }

        #endregion

        #region " ADDITIONAL INFORMATION "

        /// <summary> Information describing the additional. </summary>
        private System.Collections.Specialized.NameValueCollection _AdditionalInformation = new ();

        /// <summary> Collection allowing additional information to be added to the exception. </summary>
        /// <value> The additional information. </value>
        public System.Collections.Specialized.NameValueCollection AdditionalInformation => this._AdditionalInformation;

        /// <summary> Name of the application domain. </summary>
        private string _AppDomainName;

        /// <summary> AppDomain name where the Exception occurred. </summary>
        /// <value> The name of the application domain. </value>
        public string AppDomainName => this._AppDomainName;

        /// <summary> The created date time. </summary>
        private DateTimeOffset _CreatedDateTime = DateTimeOffset.Now;

        /// <summary> Date and Time the exception was created. </summary>
        /// <value> The created date time. </value>
        public DateTimeOffset CreatedDateTime => this._CreatedDateTime;

        /// <summary> Name of the machine. </summary>
        private string _MachineName;

        /// <summary> Machine name where the Exception occurred. </summary>
        /// <value> The name of the machine. </value>
        public string MachineName => this._MachineName;

        /// <summary> The operating system version. </summary>
        private string _OSVersion;

        /// <summary> Gets the OS version. </summary>
        /// <value> The OS version. </value>
        public string OSVersion => this._OSVersion;

        /// <summary> Name of the thread identity. </summary>
        private string _ThreadIdentityName;

        /// <summary> Identity of the executing thread on which the exception was created. </summary>
        /// <value> The name of the thread identity. </value>
        public string ThreadIdentityName => this._ThreadIdentityName;

        /// <summary> Name of the windows identity. </summary>
        private string _WindowsIdentityName;

        /// <summary> Windows identity under which the code was running. </summary>
        /// <value> The name of the windows identity. </value>
        public string WindowsIdentityName => this._WindowsIdentityName;

        /// <summary> Function That gathers environment information safely. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ObtainEnvironmentInformation()
        {
            const string unknown = "N/A";
            this._CreatedDateTime = DateTimeOffset.Now;
            this._CreatedDateTime = DateTimeOffset.Now;
            this._MachineName = Environment.MachineName;
            if ( this._MachineName.Length == 0 )
            {
                this._MachineName = unknown;
            }

            this._ThreadIdentityName = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            if ( this._ThreadIdentityName.Length == 0 )
            {
                this._ThreadIdentityName = unknown;
            }

            this._WindowsIdentityName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            if ( this._WindowsIdentityName.Length == 0 )
            {
                this._WindowsIdentityName = unknown;
            }

            this._AppDomainName = AppDomain.CurrentDomain.FriendlyName;
            if ( this._AppDomainName.Length == 0 )
            {
                this._AppDomainName = unknown;
            }

            this._OSVersion = Environment.OSVersion.ToString();
            if ( this._OSVersion.Length == 0 )
            {
                this._OSVersion = unknown;
            }

            this._AdditionalInformation = new System.Collections.Specialized.NameValueCollection() { { AdditionalInfoItem.MachineName.ToString(), My.MyProject.Computer.Name }, { AdditionalInfoItem.Timestamp.ToString(), DateTimeOffset.Now.ToString( System.Globalization.CultureInfo.CurrentCulture ) }, { AdditionalInfoItem.FullName.ToString(), System.Reflection.Assembly.GetExecutingAssembly().FullName }, { AdditionalInfoItem.AppDomainName.ToString(), AppDomain.CurrentDomain.FriendlyName }, { AdditionalInfoItem.ThreadIdentity.ToString(), My.MyProject.User.Name }, { AdditionalInfoItem.WindowsIdentity.ToString(), My.MyProject.Computer.Info.OSFullName }, { AdditionalInfoItem.OSVersion.ToString(), My.MyProject.Computer.Info.OSVersion } }; // Thread.CurrentPrincipal.Identity.Name
        }

        #endregion

        /// <summary> Specifies the contents of the additional information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private enum AdditionalInfoItem
        {

            /// <summary>
            /// The none
            /// </summary>
            None,

            /// <summary>
            /// The machine name
            /// </summary>
            MachineName,

            /// <summary>
            /// The timestamp
            /// </summary>
            Timestamp,

            /// <summary>
            /// The full name
            /// </summary>
            FullName,

            /// <summary>
            /// The application domain name
            /// </summary>
            AppDomainName,

            /// <summary>
            /// The thread identity
            /// </summary>
            ThreadIdentity,

            /// <summary>
            /// The windows identity
            /// </summary>
            WindowsIdentity,

            /// <summary>
            /// The OS version
            /// </summary>
            OSVersion
        }
    }
}
