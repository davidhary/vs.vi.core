using System;

namespace isr.K2400.Hipot.Driver
{

    /// <summary> Handles device exception including errors retrieved from the device. </summary>
    /// <remarks>
    /// David, 2010-12-25, 1.0.4011. Created <para>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    [Serializable()]
    public class DeviceException : BaseException
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="isr.K2400.Hipot.Driver.DeviceException"/> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public DeviceException() : this( "Device Exception" )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        public DeviceException( string message ) : base( message )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        public DeviceException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
        /// data.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        ///                        that holds the serialized object data about the exception being
        ///                        thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        ///                        that contains contextual information about the source or destination. 
        /// </param>
        protected DeviceException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }
}
