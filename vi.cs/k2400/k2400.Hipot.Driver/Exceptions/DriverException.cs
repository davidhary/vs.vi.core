using System;

namespace isr.K2400.Hipot.Driver
{

    /// <summary> Handles device exceptions. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2011-02-15, 1.0.4063.  Created  </para>
    /// </remarks>
    [Serializable()]
    public class DriverException : BaseException
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="DriverException"/> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public DriverException() : this( "Device Exception" )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        public DriverException( string message ) : base( message )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message">        The message. </param>
        /// <param name="innerException"> The inner exception. </param>
        public DriverException( string message, Exception innerException ) : base( message, innerException )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
        /// data.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
        ///                        that holds the serialized object data about the exception being
        ///                        thrown. </param>
        /// <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
        ///                        that contains contextual information about the source or destination. 
        /// </param>
        protected DriverException( System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context ) : base( info, context )
        {
        }

        #endregion

    }
}
