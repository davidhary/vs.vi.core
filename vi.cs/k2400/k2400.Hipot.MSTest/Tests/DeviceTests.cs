using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.K2400.Hipot.MSTest
{

    /// <summary>
    /// This is a test class for DeviceTest and is intended to contain all DeviceTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [TestClass()]
    [TestCategory( "k2400" )]
    public class DeviceTests
    {

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> A test for Open Session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionTest()
        {
            bool expectedBoolean = true;
            bool actualBoolean;
            using var device = new Driver.Device();
            actualBoolean = device.Connect( Driver.My.MySettings.Default.ResourceName );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"Open Session to {Driver.My.MySettings.Default.ResourceName}" );
            _ = device.Disconnect();
        }
    }
}
