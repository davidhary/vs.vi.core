using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400
{

    /// <summary> Implements a Keithley 2400 source meter. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-10, 3.0.5001. </para>
    /// </remarks>
    public class K2400Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="K2400Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K2400Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected K2400Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            My.MySettings.Default.PropertyChanged += this.MySettings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static K2400Device Create()
        {
            K2400Device device = null;
            try
            {
                device = new K2400Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }


        #region " IDisposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( K2400Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindMeasureSubsystem( null );
                this.BindFormatSubsystem( null );
                this.BindOutputSubsystem( null );
                this.BindRouteSubsystem( null );
                this.BindSenseCurrentSubsystem( null );
                this.BindSenseVoltageSubsystem( null );
                this.BindSenseResistanceSubsystem( null );
                this.BindSenseSubsystem( null );
                this.BindSourceCurrentSubsystem( null );
                this.BindSourceVoltageSubsystem( null );
                this.BindSourceSubsystem( null );
                this.BindArmLayerSubsystem( null );
                this.BindTriggerSubsystem( null );
                this.BindBinningSubsystem( null );
                this.BindContactCheckLimit( null );
                this.BindComplianceLimit( null );
                this.BindCompositeLimit( null );
                this.BindDigitalOutput( null );
                this.BindUpperLowerLimit( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                // better add before the format subsystem, which reset initializes the readings.
                this.BindMeasureSubsystem( new MeasureSubsystem( this.StatusSubsystem ) );
                // the measure subsystem reading are initialized when the format system is reset
                this.BindFormatSubsystem( new FormatSubsystem( this.StatusSubsystem ) );
                this.BindOutputSubsystem( new OutputSubsystem( this.StatusSubsystem ) );
                this.BindRouteSubsystem( new RouteSubsystem( this.StatusSubsystem ) );
                this.BindSenseCurrentSubsystem( new SenseCurrentSubsystem( this.StatusSubsystem ) );
                this.BindSenseVoltageSubsystem( new SenseVoltageSubsystem( this.StatusSubsystem ) );
                this.BindSenseResistanceSubsystem( new SenseResistanceSubsystem( this.StatusSubsystem ) );
                this.BindSenseSubsystem( new SenseSubsystem( this.StatusSubsystem ) );
                this.BindSourceCurrentSubsystem( new SourceCurrentSubsystem( this.StatusSubsystem ) );
                this.BindSourceVoltageSubsystem( new SourceVoltageSubsystem( this.StatusSubsystem ) );
                this.BindSourceSubsystem( new SourceSubsystem( this.StatusSubsystem ) );
                this.BindArmLayerSubsystem( new ArmLayerSubsystem( this.StatusSubsystem ) );
                this.BindTriggerSubsystem( new TriggerSubsystem( this.StatusSubsystem ) );
                this.BindBinningSubsystem( new BinningSubsystem( this.StatusSubsystem ) );
                this.BindContactCheckLimit( new ContactCheckLimit( this.StatusSubsystem ) );
                this.BindComplianceLimit( new ComplianceLimit( this.StatusSubsystem ) );
                this.BindCompositeLimit( new CompositeLimit( this.StatusSubsystem ) );
                this.BindDigitalOutput( new DigitalOutputSubsystem( this.StatusSubsystem ) );
                this.BindUpperLowerLimit( new UpperLowerLimit( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " LIMITS "

        /// <summary> Gets or sets the composite limit. </summary>
        /// <value> The composite limit. </value>
        public CompositeLimit CompositeLimit { get; private set; }

        /// <summary> Binds the composite limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="limit"> The limit. </param>
        private void BindCompositeLimit( CompositeLimit limit )
        {
            if ( this.CompositeLimit is object )
            {
                this.CompositeLimit = null;
            }

            this.CompositeLimit = limit;
        }

        /// <summary> Gets or sets the Compliance limit. </summary>
        /// <value> The Compliance limit. </value>
        public ComplianceLimit ComplianceLimit { get; private set; }

        /// <summary> Binds the Compliance limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="limit"> The limit. </param>
        private void BindComplianceLimit( ComplianceLimit limit )
        {
            if ( this.ComplianceLimit is object )
            {
                this.ComplianceLimit = null;
            }

            this.ComplianceLimit = limit;
        }

        /// <summary> Gets or sets the Contact Check limit. </summary>
        /// <value> The Contact Check limit. </value>
        public ContactCheckLimit ContactCheckLimit { get; private set; }

        /// <summary> Binds the Contact Check limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="limit"> The limit. </param>
        private void BindContactCheckLimit( ContactCheckLimit limit )
        {
            if ( this.ContactCheckLimit is object )
            {
                this.ContactCheckLimit = null;
            }

            this.ContactCheckLimit = limit;
        }

        /// <summary> Gets or sets the Upper-Lower limit. </summary>
        /// <value> The Upper-Lower limit. </value>
        public UpperLowerLimit UpperLowerLimit { get; private set; }

        /// <summary> Binds the Upper-Lower limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="limit"> The limit. </param>
        private void BindUpperLowerLimit( UpperLowerLimit limit )
        {
            if ( this.UpperLowerLimit is object )
            {
                this.UpperLowerLimit = null;
            }

            this.UpperLowerLimit = limit;
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Gets or sets the arm layer subsystem. </summary>
        /// <value> The arm layer subsystem. </value>
        public ArmLayerSubsystem ArmLayerSubsystem { get; private set; }

        /// <summary> Binds the Arm Layer subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindArmLayerSubsystem( ArmLayerSubsystem subsystem )
        {
            if ( this.ArmLayerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.ArmLayerSubsystem );
                this.ArmLayerSubsystem = null;
            }

            this.ArmLayerSubsystem = subsystem;
            if ( this.ArmLayerSubsystem is object )
            {
                this.Subsystems.Add( this.ArmLayerSubsystem );
            }
        }

        /// <summary> Gets or sets the Binning subsystem. </summary>
        /// <value> The Binning subsystem. </value>
        public BinningSubsystem BinningSubsystem { get; private set; }

        /// <summary> Binds the Binning subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindBinningSubsystem( BinningSubsystem subsystem )
        {
            if ( this.BinningSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.BinningSubsystem );
                this.BinningSubsystem = null;
            }

            this.BinningSubsystem = subsystem;
            if ( this.BinningSubsystem is object )
            {
                this.Subsystems.Add( this.BinningSubsystem );
            }
        }

        /// <summary> Gets or sets the Digital Output Subsystem. </summary>
        /// <value> The Digital Output Subsystem. </value>
        public DigitalOutputSubsystem DigitalOutput { get; private set; }

        /// <summary> Binds the Digital Output subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDigitalOutput( DigitalOutputSubsystem subsystem )
        {
            if ( this.DigitalOutput is object )
            {
                _ = this.Subsystems.Remove( this.DigitalOutput );
                this.DigitalOutput = null;
            }

            this.DigitalOutput = subsystem;
            if ( this.DigitalOutput is object )
            {
                this.Subsystems.Add( this.DigitalOutput );
            }
        }

        /// <summary> Gets or sets the Measure Subsystem. </summary>
        /// <value> The Measure Subsystem. </value>
        public MeasureSubsystem MeasureSubsystem { get; private set; }

        /// <summary> Binds the Measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureSubsystem( MeasureSubsystem subsystem )
        {
            if ( this.MeasureSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            this.MeasureSubsystem = subsystem;
            if ( this.MeasureSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureSubsystem );
            }
        }

        /// <summary> Gets or sets the Output Subsystem. </summary>
        /// <value> The Output Subsystem. </value>
        public OutputSubsystem OutputSubsystem { get; private set; }

        /// <summary> Binds the Output subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindOutputSubsystem( OutputSubsystem subsystem )
        {
            if ( this.OutputSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.OutputSubsystem );
                this.OutputSubsystem = null;
            }

            this.OutputSubsystem = subsystem;
            if ( this.OutputSubsystem is object )
            {
                this.Subsystems.Add( this.OutputSubsystem );
            }
        }

        /// <summary> Gets or sets the Route Subsystem. </summary>
        /// <value> The Route Subsystem. </value>
        public RouteSubsystem RouteSubsystem { get; private set; }

        /// <summary> Binds the Route subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindRouteSubsystem( RouteSubsystem subsystem )
        {
            if ( this.RouteSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.RouteSubsystem );
                this.RouteSubsystem = null;
            }

            this.RouteSubsystem = subsystem;
            if ( this.RouteSubsystem is object )
            {
                this.Subsystems.Add( this.RouteSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Current Subsystem. </summary>
        /// <value> The Sense Current Subsystem. </value>
        public SenseCurrentSubsystem SenseCurrentSubsystem { get; private set; }

        /// <summary> Binds the Sense Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseCurrentSubsystem( SenseCurrentSubsystem subsystem )
        {
            if ( this.SenseCurrentSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseCurrentSubsystem );
                this.SenseCurrentSubsystem = null;
            }

            this.SenseCurrentSubsystem = subsystem;
            if ( this.SenseCurrentSubsystem is object )
            {
                this.Subsystems.Add( this.SenseCurrentSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Resistance Subsystem. </summary>
        /// <value> The Sense Resistance Subsystem. </value>
        public SenseResistanceSubsystem SenseResistanceSubsystem { get; private set; }

        /// <summary> Binds the Sense Resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseResistanceSubsystem( SenseResistanceSubsystem subsystem )
        {
            if ( this.SenseResistanceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseResistanceSubsystem );
                this.SenseResistanceSubsystem = null;
            }

            this.SenseResistanceSubsystem = subsystem;
            if ( this.SenseResistanceSubsystem is object )
            {
                this.Subsystems.Add( this.SenseResistanceSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Voltage Subsystem. </summary>
        /// <value> The Sense Voltage Subsystem. </value>
        public SenseVoltageSubsystem SenseVoltageSubsystem { get; private set; }

        /// <summary> Binds the Sense Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseVoltageSubsystem( SenseVoltageSubsystem subsystem )
        {
            if ( this.SenseVoltageSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseVoltageSubsystem );
                this.SenseVoltageSubsystem = null;
            }

            this.SenseVoltageSubsystem = subsystem;
            if ( this.SenseVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.SenseVoltageSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Subsystem. </summary>
        /// <value> The Sense Subsystem. </value>
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Binds the Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseSubsystem( SenseSubsystem subsystem )
        {
            if ( this.SenseSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            this.SenseSubsystem = subsystem;
            if ( this.SenseSubsystem is object )
            {
                this.Subsystems.Add( this.SenseSubsystem );
            }
        }

        /// <summary> Gets or sets the Source Current Subsystem. </summary>
        /// <value> The Source Current Subsystem. </value>
        public SourceCurrentSubsystem SourceCurrentSubsystem { get; private set; }

        /// <summary> Binds the Source Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceCurrentSubsystem( SourceCurrentSubsystem subsystem )
        {
            if ( this.SourceCurrentSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SourceCurrentSubsystem );
                this.SourceCurrentSubsystem = null;
            }

            this.SourceCurrentSubsystem = subsystem;
            if ( this.SourceCurrentSubsystem is object )
            {
                this.Subsystems.Add( this.SourceCurrentSubsystem );
            }
        }

        /// <summary> Gets or sets the Source Voltage Subsystem. </summary>
        /// <value> The Source Voltage Subsystem. </value>
        public SourceVoltageSubsystem SourceVoltageSubsystem { get; private set; }

        /// <summary> Binds the Source Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceVoltageSubsystem( SourceVoltageSubsystem subsystem )
        {
            if ( this.SourceVoltageSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SourceVoltageSubsystem );
                this.SourceVoltageSubsystem = null;
            }

            this.SourceVoltageSubsystem = subsystem;
            if ( this.SourceVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.SourceVoltageSubsystem );
            }
        }

        /// <summary> Gets or sets the Source Subsystem. </summary>
        /// <value> The Source Subsystem. </value>
        public SourceSubsystem SourceSubsystem { get; private set; }

        /// <summary> Binds the Source subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceSubsystem( SourceSubsystem subsystem )
        {
            if ( this.SourceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SourceSubsystem );
                this.SourceSubsystem = null;
            }

            this.SourceSubsystem = subsystem;
            if ( this.SourceSubsystem is object )
            {
                this.Subsystems.Add( this.SourceSubsystem );
            }
        }

        /// <summary> Gets or sets the Trigger Subsystem. </summary>
        /// <value> The Trigger Subsystem. </value>
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Binds the Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTriggerSubsystem( TriggerSubsystem subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( this.TriggerSubsystem is object )
            {
                this.Subsystems.Add( this.TriggerSubsystem );
            }
        }

        #region " FORMAT "

        /// <summary> Gets or sets the Format Subsystem. </summary>
        /// <value> The Format Subsystem. </value>
        public FormatSubsystem FormatSubsystem { get; private set; }

        /// <summary> Binds the Format subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindFormatSubsystem( FormatSubsystem subsystem )
        {
            if ( this.FormatSubsystem is object )
            {
                this.FormatSubsystem.PropertyChanged -= this.FormatSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.FormatSubsystem );
                this.FormatSubsystem = null;
            }

            this.FormatSubsystem = subsystem;
            if ( this.FormatSubsystem is object )
            {
                this.FormatSubsystem.PropertyChanged += this.FormatSubsystemPropertyChanged;
                this.Subsystems.Add( this.FormatSubsystem );
                this.HandlePropertyChanged( this.FormatSubsystem, nameof( FormatSubsystemBase.Elements ) );
            }
        }

        /// <summary> Handle the Format subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( FormatSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( FormatSubsystemBase.Elements ):
                    {
                        this.MeasureSubsystem.ReadingAmounts.Initialize( subsystem.Elements );
                        break;
                    }
            }
        }

        /// <summary> Format subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FormatSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.FormatSubsystem )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as FormatSubsystem, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Binds the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                this.SystemSubsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.SystemSubsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
                this.Subsystems.Add( this.SystemSubsystem );
                this.HandlePropertyChanged( this.SystemSubsystem, nameof( K2400.SystemSubsystem.Options ) );
            }
        }

        /// <summary> Handles the Status subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SystemSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SystemSubsystem.Options ):
                    {
                        // read the contact check option after reading options.
                        _ = subsystem.QueryContactCheckSupported();
                        break;
                    }
            }
        }

        /// <summary> System subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            try
            {
                this.HandlePropertyChanged( sender as SystemSubsystem, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, "Exception handling property '{0}' changed event;. {1}", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        #endregion

        #endregion

        #region " MIXED SUBSYSTEMS: OUTPUT / MEASURE "

        /// <summary> Sets the specified voltage and current limit and turns on the output. </summary>
        /// <remarks> See 2400 system manual page 102 of 592. </remarks>
        /// <param name="voltage">          The voltage. </param>
        /// <param name="currentLimit">     The current limit. </param>
        /// <param name="overvoltageLimit"> The over voltage limit. </param>
        public void OutputOn( double voltage, double currentLimit, double overvoltageLimit )
        {

            // turn output off.
            _ = this.OutputSubsystem.ApplyOutputOnState( false );
            _ = this.SourceSubsystem.WriteFunctionMode( SourceFunctionModes.VoltageDC );

            // Set the voltage
            _ = this.SourceVoltageSubsystem.ApplyLevel( voltage );

            // Set the over voltage level
            _ = this.SourceVoltageSubsystem.ApplyProtectionLevel( overvoltageLimit );

            // Turn on over current protection
            _ = this.SenseCurrentSubsystem.ApplyProtectionLevel( currentLimit );
            this.SenseSubsystem.FunctionModes = SenseFunctionModes.Current | SenseFunctionModes.Voltage;
            this.SenseSubsystem.SupportsMultiFunctions = true;
            _ = this.SenseSubsystem.ApplyConcurrentSenseEnabled( true );

            // turn output on.
            _ = this.OutputSubsystem.ApplyOutputOnState( true );
            _ = this.Session.QueryOperationCompleted();

            // operation completion is not sufficient to ensure that the Device has hit the correct voltage.

        }

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "K2400 Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.ReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.StatusReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime;
                        break;
                    }

            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
