namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Trigger Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TriggerSubsystem : TriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TriggerSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.SupportedTriggerSources = TriggerSources.Immediate | TriggerSources.TriggerLink;
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.TriggerSource = TriggerSources.Immediate;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " COMMANDS "

        /// <summary> Gets or sets the Abort command. </summary>
        /// <value> The Abort command. </value>
        protected override string AbortCommand { get; set; } = ":ABORT";

        /// <summary> Gets or sets the initiate command. </summary>
        /// <value> The initiate command. </value>
        protected override string InitiateCommand { get; set; } = ":INIT";

        /// <summary> Gets or sets the clear command. </summary>
        /// <remarks> SCPI: ":TRIG:CLE". </remarks>
        /// <value> The clear command. </value>
        protected override string ClearCommand { get; set; } = "TRIG:CLE";

        /// <summary> Gets or sets the Immediate command. </summary>
        /// <value> The Immediate command. </value>
        protected override string ImmediateCommand { get; set; } = string.Empty; // ":TRIG:IMM"

        #endregion

        #region " AUTO DELAY "

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledCommandFormat { get; set; } = ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledQueryCommand { get; set; } = ":TRIG:DEL:AUTO?";

        #endregion

        #region " TRIGGER COUNT "

        /// <summary> Gets or sets trigger count query command. </summary>
        /// <value> The trigger count query command. </value>
        protected override string TriggerCountQueryCommand { get; set; } = ":TRIG:COUN?";

        /// <summary> Gets or sets trigger count command format. </summary>
        /// <value> The trigger count command format. </value>
        protected override string TriggerCountCommandFormat { get; set; } = ":TRIG:COUN {0}";

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = @":TRIG:DEL {0:s\.FFFFFFF}";

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = ":TRIG:DEL?";

        #endregion

        #region " DIRECTION (BYPASS) "

        /// <summary> Gets or sets the Trigger Direction query command. </summary>
        /// <value> The Trigger Direction query command. </value>
        protected override string TriggerLayerBypassModeQueryCommand { get; set; } = ":TRIG:DIR?";

        /// <summary> Gets or sets the Trigger Direction command format. </summary>
        /// <value> The Trigger Direction command format. </value>
        protected override string TriggerLayerBypassModeCommandFormat { get; set; } = ":TRIG:DIR {0}";

        #endregion

        #region " INPUT LINE NUMBER "

        /// <summary> Gets or sets the Input Line Number command format. </summary>
        /// <value> The Input Line Number command format. </value>
        protected override string InputLineNumberCommandFormat { get; set; } = ":TRIG:ILIN {0}";

        /// <summary> Gets or sets the Input Line Number query command. </summary>
        /// <value> The Input Line Number query command. </value>
        protected override string InputLineNumberQueryCommand { get; set; } = ":TRIG:ILIN?";

        #endregion

        #region " OUTPUT LINE NUMBER "

        /// <summary> Gets or sets the Output Line Number command format. </summary>
        /// <value> The Output Line Number command format. </value>
        protected override string OutputLineNumberCommandFormat { get; set; } = ":TRIG:OLIN {0}";

        /// <summary> Gets or sets the Output Line Number query command. </summary>
        /// <value> The Output Line Number query command. </value>
        protected override string OutputLineNumberQueryCommand { get; set; } = ":TRIG:OLIN?";

        #endregion

        #region " TRIGGER SOURCE "

        /// <summary> Gets or sets the Trigger source command format. </summary>
        /// <value> The write Trigger source command format. </value>
        protected override string TriggerSourceCommandFormat { get; set; } = ":TRIG:SOUR {0}";

        /// <summary> Gets or sets the Trigger source query command. </summary>
        /// <value> The Trigger source query command. </value>
        protected override string TriggerSourceQueryCommand { get; set; } = ":TRIG:SOUR?";

        #endregion

        #region " TIMER TIME SPAN "

        /// <summary> Gets or sets the Timer Interval command format. </summary>
        /// <value> The query command format. </value>
        protected override string TimerIntervalCommandFormat { get; set; } = @":TRIG:TIM {0:s\.FFFFFFF}";

        /// <summary>
        /// Gets or sets the Timer Interval format for converting the query to time span.
        /// </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the Timer Interval query command. </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalQueryCommand { get; set; } = ":TRIG:TIM?";

        #endregion

        #endregion

    }
}
