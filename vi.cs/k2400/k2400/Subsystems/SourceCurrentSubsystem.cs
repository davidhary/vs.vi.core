namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Source Current Subsystem . </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class SourceCurrentSubsystem : SourceCurrentSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceCurrentSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public SourceCurrentSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SOUR:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SOUR:CURR:RANG:AUTO?";

        #endregion

        #region " PROTECTION "

        /// <summary> Gets or sets the Protection enabled command Format. </summary>
        /// <value> The Protection enabled query command. </value>
        protected override string ProtectionEnabledCommandFormat { get; set; } = string.Empty; // ":SOUR:CURR:PROT:STAT {0:'ON';'ON';'OFF'}"

        /// <summary> Gets or sets the Protection enabled query command. </summary>
        /// <value> The Protection enabled query command. </value>
        protected override string ProtectionEnabledQueryCommand { get; set; } = string.Empty; // ":SOUR:CURR:PROT:STAT?"

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SOUR:CURR:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SOUR:CURR:RANG?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SOUR:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SOUR:FUNC?";

        #endregion

        #endregion

    }
}
