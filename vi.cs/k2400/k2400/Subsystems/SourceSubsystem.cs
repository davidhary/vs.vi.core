namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Source Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public partial class SourceSubsystem : SourceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SourceSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public SourceSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            // TO_DO: Implements elements from Source Subsystem base to define the range and supported function and use the 
            // proper function calls to write and query the function mode.
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            base.DefineKnownResetState();
            this.FunctionMode = SourceFunctionModes.Voltage;
            this.SupportedFunctionModes = SourceFunctionModes.Current | SourceFunctionModes.Voltage;
        }

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Clears the function state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineFunctionClearKnownState()
        {
            base.DefineFunctionClearKnownState();
            _ = this.ParseReadBackAmount( string.Empty );
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SOUR:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SOUR:FUNC?";

        #endregion

        #endregion

    }
}
