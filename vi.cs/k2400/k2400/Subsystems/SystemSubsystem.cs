using System;

namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI System Subsystem such as the Keithley 2400. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks>
        /// Additional Actions: <para>
        /// Clears Error Queue.
        /// </para>
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override void InitKnownState()
        {
            base.InitKnownState();
            string activity = string.Empty;
            try
            {
                activity = "Reading options";
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.QueryOptions();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.FourWireSenseEnabled = false;
            this.AutoZeroEnabled = true;
            this.ContactCheckEnabled = false;
            this.ContactCheckResistance = 50;
        }

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Gets or sets the Auto Zero enabled query command. </summary>
        /// <value> The Auto Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = ":SYST:AZER?";

        /// <summary> Gets or sets the Auto Zero enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:AZERO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Auto Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = ":SYST:AZER {0:'ON';'ON';'OFF'}";

        #endregion

        #region " CONTACT CHECK ENABLED "

        /// <summary> Gets or sets the Contact Check enabled query command. </summary>
        /// <value> The Contact Check enabled query command. </value>
        protected override string ContactCheckEnabledQueryCommand { get; set; } = ":SYST:CCH?";

        /// <summary> Gets or sets the Contact Check enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:CCH {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Contact Check enabled query command. </value>
        protected override string ContactCheckEnabledCommandFormat { get; set; } = ":SYST:CCH {0:'ON';'ON';'OFF'}";

        #endregion

        #region " CONTACT CHECK RESISTANCE "

        /// <summary> Gets or sets the ContactCheck Resistance query command. </summary>
        /// <value> the ContactCheck Resistance query command. </value>
        protected override string ContactCheckResistanceQueryCommand { get; set; } = "SYST:CCH:RES?";

        /// <summary> Gets or sets the ContactCheck Resistance command format. </summary>
        /// <value> the ContactCheck Resistance command format. </value>
        protected override string ContactCheckResistanceCommandFormat { get; set; } = "SYST:CCH:RES {0}";

        #endregion

        #region " CONTACT CHECK SUPPORTED "

        /// <summary> Gets or sets the contact check option value. </summary>
        /// <value> The contact check option value. </value>
        protected override string ContactCheckOptionValue { get; set; } = "CONTACT-CHECK";

        #endregion

        #region " FOUR WIRE SENSE ENABLED "

        /// <summary> Gets or sets the Four Wire Sense enabled query command. </summary>
        /// <value> The Four Wire Sense enabled query command. </value>
        protected override string FourWireSenseEnabledQueryCommand { get; set; } = ":SYST:RSEN?";

        /// <summary> Gets or sets the Four Wire Sense enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:RSEN {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Four Wire Sense enabled query command. </value>
        protected override string FourWireSenseEnabledCommandFormat { get; set; } = ":SYST:RSEN {0:'ON';'ON';'OFF'}";

        #endregion

        #region " OPTIONS "

        /// <summary> Gets or sets the option query command. </summary>
        /// <value> The option query command. </value>
        protected override string OptionQueryCommand { get; set; } = "*OPT?";

        #endregion

    }
}
