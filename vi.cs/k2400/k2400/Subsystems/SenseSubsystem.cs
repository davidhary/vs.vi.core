namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Sense Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class SenseSubsystem : SenseSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        public SenseSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.SupportsMultiFunctions = true;
            this.SupportedFunctionModes = SenseFunctionModes.Current | SenseFunctionModes.Voltage | SenseFunctionModes.Resistance;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere;
            base.DefineKnownResetState();
            this.ConcurrentSenseEnabled = true;
            this.FunctionModes = SenseFunctionModes.Current | SenseFunctionModes.Voltage;
            this.ConcurrentSenseEnabled = true;
            this.Range = 0.105d;
            this.PowerLineCycles = 5;
            this.FunctionMode = SenseFunctionModes.Current;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:RANG:AUTO?";

        #endregion

        #region " LATEST DATA "

        /// <summary> Gets or sets the latest data query command. </summary>
        /// <value> The latest data query command. </value>
        protected override string LatestDataQueryCommand { get; set; } = ":SENSE:DATA:LAT?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:NPLC?";

        #endregion

        #region " PROTECTION LEVEL "

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected override string ProtectionLevelCommandFormat { get; set; } = ":SENS:PROT {0}";

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected override string ProtectionLevelQueryCommand { get; set; } = ":SENS:PROT?";

        #endregion

        #endregion

    }
}
