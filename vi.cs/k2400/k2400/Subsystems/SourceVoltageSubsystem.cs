using System;

namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Source Voltage Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class SourceVoltageSubsystem : SourceVoltageSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceVoltageSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        public SourceVoltageSubsystem( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            string model = (( StatusSubsystem ) this.StatusSubsystem).VersionInfo.Model;
            Core.Primitives.RangeR voltageRange;
            Core.Primitives.RangeR currentRange;
            switch ( true )
            {
                case object _ when model.StartsWith( "2400", StringComparison.OrdinalIgnoreCase ):
                    {
                        voltageRange = new Core.Primitives.RangeR( -210, +210 );
                        currentRange = new Core.Primitives.RangeR( -1.05d, +1.05d );
                        break;
                    }

                case object _ when model.StartsWith( "2410", StringComparison.OrdinalIgnoreCase ):
                    {
                        voltageRange = new Core.Primitives.RangeR( -1100, +1100 );
                        currentRange = new Core.Primitives.RangeR( -1.05d, +1.05d );
                        break;
                    }

                case object _ when model.StartsWith( "2420", StringComparison.OrdinalIgnoreCase ):
                    {
                        voltageRange = new Core.Primitives.RangeR( -63, +63 );
                        currentRange = new Core.Primitives.RangeR( -3.15d, +3.05d );
                        break;
                    }

                case object _ when model.StartsWith( "2425", StringComparison.OrdinalIgnoreCase ):
                    {
                        voltageRange = new Core.Primitives.RangeR( -105, +105 );
                        currentRange = new Core.Primitives.RangeR( -1.05d, +1.05d );
                        break;
                    }

                case object _ when model.StartsWith( "243", StringComparison.OrdinalIgnoreCase ):
                    {
                        voltageRange = new Core.Primitives.RangeR( -105, +105 );
                        currentRange = new Core.Primitives.RangeR( -3.15d, +3.15d );
                        break;
                    }

                case object _ when model.StartsWith( "244", StringComparison.OrdinalIgnoreCase ):
                    {
                        voltageRange = new Core.Primitives.RangeR( -42, +42 );
                        currentRange = new Core.Primitives.RangeR( -5.25d, +5.25d );
                        break;
                    }

                default:
                    {
                        voltageRange = new Core.Primitives.RangeR( -210, +210 );
                        currentRange = new Core.Primitives.RangeR( -1.05d, +1.05d );
                        break;
                    }
            }

            this.FunctionModeRanges[( int ) SourceFunctionModes.Voltage] = voltageRange;
            this.FunctionModeRanges[( int ) SourceFunctionModes.VoltageDC] = voltageRange;
            this.FunctionModeRanges[( int ) SourceFunctionModes.Current] = currentRange;
            this.FunctionModeRanges[( int ) SourceFunctionModes.CurrentDC] = currentRange;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " PROTECTION LEVEL "

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected override string ProtectionLevelCommandFormat { get; set; } = ":SOUR:VOLT:PROT:LEV {0}";

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected override string ProtectionLevelQueryCommand { get; set; } = ":SOUR:VOLT:PROT:LEV?";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SOUR:VOLT:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SOUR:VOLT:RANG?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SOUR:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SOUR:FUNC?";

        #endregion

        #endregion

    }
}
