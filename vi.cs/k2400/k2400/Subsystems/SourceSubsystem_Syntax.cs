namespace isr.VI.K2400
{

    /// <summary>   A source subsystem. </summary>
    /// <remarks>   David, 2021-09-03. </remarks>
    public partial class SourceSubsystem
    {

        #region " AUTO CLEAR ENABLED "

        /// <summary> Gets or sets the automatic Clear enabled query command. </summary>
        /// <remarks> SCPI: ":SOUR:CLE:AUTO?". </remarks>
        /// <value> The automatic Clear enabled query command. </value>
        protected override string AutoClearEnabledQueryCommand { get; set; } = ":SOUR:CLE:AUTO?";

        /// <summary> Gets or sets the automatic Clear enabled command Format. </summary>
        /// <value> The automatic Clear enabled query command. </value>
        protected override string AutoClearEnabledCommandFormat { get; set; } = ":SOUR:CLE:AUTO {0:'ON';'ON';'OFF'}";

        #endregion

        #region " AUTO DELAY ENABLED "

        /// <summary> Gets or sets the automatic Delay enabled query command. </summary>
        /// <remarks> SCPI: ":SOUR:CLE:AUTO?". </remarks>
        /// <value> The automatic Delay enabled query command. </value>
        protected override string AutoDelayEnabledQueryCommand { get; set; } = ":SOUR:DEL:AUTO?";

        /// <summary> Gets or sets the automatic Delay enabled command Format. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected override string AutoDelayEnabledCommandFormat { get; set; } = ":SOUR:DEL:AUTO {0:'ON';'ON';'OFF'}";

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = @":SOUR:DEL {0:s\.FFFFFFF}";

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = ":SOUR:DEL?";

        #endregion

        #region " FUNCTION MODE "
        // 2018 12 27 not sure why these are commented out

        // <summary> Gets or sets the function mode query command. </summary>
        // <value> The function mode query command, e.g., :SOUR:FUNC? </value>
        // Protected Overrides Property FunctionModeQueryCommand As String = ":SOUR:FUNC?"

        // <summary> Gets or sets the function mode command. </summary>
        // <value> The function mode command, e.g., :SOUR:FUNC {0}. </value>
        // Protected Overrides Property FunctionModeCommandFormat As String = ":SOUR:FUNC {0}"

        #endregion

        #region " SWEEP POINTS "

        /// <summary> Gets or sets Sweep Points query command. </summary>
        /// <value> The Sweep Points query command. </value>
        protected override string SweepPointsQueryCommand { get; set; } = ":SOUR:SWE:POIN?";

        /// <summary> Gets or sets Sweep Points command format. </summary>
        /// <value> The Sweep Points command format. </value>
        protected override string SweepPointsCommandFormat { get; set; } = ":SOUR:SWE:POIN {0}";

        #endregion

    }
}
