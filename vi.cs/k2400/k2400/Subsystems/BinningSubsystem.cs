namespace isr.VI.K2400
{

    /// <summary> Defines the instrument Binning Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class BinningSubsystem : BinningSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="BinningSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public BinningSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " SYNTAX "

        #region " FEED SOURCE "

        /// <summary> Gets or sets the feed source query command. </summary>
        /// <value> The feed source query command. </value>
        protected override string FeedSourceQueryCommand { get; set; } = ":CALC2:FEED?";

        /// <summary> Gets or sets the feed source command format. </summary>
        /// <value> The write feed source command format. </value>
        protected override string FeedSourceCommandFormat { get; set; } = "CALC2:FEED {0}";

        #endregion

        #endregion

    }
}
