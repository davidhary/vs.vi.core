using System;

namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Route Subsystem for Source Measure SCPI devices. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class RouteSubsystem : RouteSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public RouteSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "



        #endregion

        #region " COMMAND SYNTAX "

        #region " CLOSED CHANNEL "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the closed Channel query command. </summary>
        /// <value> The closed Channel query command. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string ClosedChannelQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the closed Channel command format. </summary>
        /// <value> The closed Channel command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string ClosedChannelCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string OpenChannelCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " CLOSED CHANNELS "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the closed channels query command. </summary>
        /// <value> The closed channels query command. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string ClosedChannelsQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the closed channels command format. </summary>
        /// <value> The closed channels command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string ClosedChannelsCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string OpenChannelsCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " CHANNELS "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the recall channel pattern command format. </summary>
        /// <value> The recall channel pattern command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string RecallChannelPatternCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the save channel pattern command format. </summary>
        /// <value> The save channel pattern command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string SaveChannelPatternCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the open channels command. </summary>
        /// <value> The open channels command. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string OpenChannelsCommand { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " SCAN LIST "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets or sets the scan list command query. </summary>
        /// <value> The scan list query command. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string ScanListQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the scan list command format. </summary>
        /// <value> The scan list command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string ScanListCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " SLOT CARD TYPE "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the slot card type command format. </summary>
        /// <value> The slot card type command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string SlotCardTypeCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the slot card type query command format. </summary>
        /// <value> The slot card type query command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string SlotCardTypeQueryCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " SLOT CARD SETTLING TIME "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the slot card settling time command format. </summary>
        /// <value> The slot card settling time command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string SlotCardSettlingTimeCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the slot card settling time query command format. </summary>
        /// <value> The slot card settling time query command format. </value>
        [Obsolete( "Not supported for the Source Meters." )]
        protected override string SlotCardSettlingTimeQueryCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " TERMINAL MODE "

        /// <summary> Gets or sets the terminals mode query command. </summary>
        /// <value> The terminals mode command. </value>
        protected override string TerminalsModeQueryCommand { get; set; } = ":ROUT:TERM?";

        /// <summary> Gets or sets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        protected override string TerminalsModeCommandFormat { get; set; } = ":ROUT:TERM {0}";

        #endregion

        #endregion

    }
}
