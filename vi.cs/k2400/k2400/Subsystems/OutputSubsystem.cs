namespace isr.VI.K2400
{

    /// <summary> Defines a SCPI Output Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class OutputSubsystem : OutputSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="OutputSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public OutputSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "



        #endregion

        #region " COMMAND SYNTAX "

        #region " OFF MODE "

        /// <summary> Gets or sets the Output Off Mode query command. </summary>
        /// <value> The Off Mode query command. </value>
        protected override string OutputOffModeQueryCommand { get; set; } = ":OUTP:SMOD?";

        /// <summary> Gets or sets the output Off Mode command format. </summary>
        /// <value> The write Off Mode command format. </value>
        protected override string OutputOffModeCommandFormat { get; set; } = ":OUTP:SMOD {0}";

        #endregion

        #region " ON/OFF STATE "

        /// <summary> Gets or sets the output on state query command. </summary>
        /// <value> The output on state query command. </value>
        protected override string OutputOnStateQueryCommand { get; set; } = ":OUTP:STAT?";

        /// <summary> Gets or sets the output on state command format. </summary>
        /// <value> The output on state command format. </value>
        protected override string OutputOnStateCommandFormat { get; set; } = ":OUTP:STAT {0:'1';'1';'0'}";

        #endregion

        #region " TERMINAL MODE "

        /// <summary> Gets or sets the terminals mode query command. </summary>
        /// <value> The terminals mode query command. </value>
        protected override string OutputTerminalsModeQueryCommand { get; set; } = ":OUTP:ROUT:TERM?";

        /// <summary> Gets or sets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        protected override string OutputTerminalsModeCommandFormat { get; set; } = ":OUTP:ROUT:TERM {0}";

        #endregion

        #endregion

    }
}
