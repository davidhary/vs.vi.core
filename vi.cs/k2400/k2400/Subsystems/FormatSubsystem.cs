namespace isr.VI.K2400
{

    /// <summary> Defines a Format Subsystem for a Keithley 2400 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class FormatSubsystem : FormatSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="FormatSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public FormatSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Elements = ReadingElementTypes.Voltage | ReadingElementTypes.Current | ReadingElementTypes.Resistance | ReadingElementTypes.Time | ReadingElementTypes.Status;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " ELEMENTS "

        /// <summary> Gets or sets the elements query command. </summary>
        /// <value> The elements query command. </value>
        protected override string ElementsQueryCommand { get; set; } = ":FORM:ELEM?";

        /// <summary> Gets or sets the elements command format. </summary>
        /// <value> The elements command format. </value>
        protected override string ElementsCommandFormat { get; set; } = ":FORM:ELEM {0}";

        #endregion

        #endregion

    }
}
