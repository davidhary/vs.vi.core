namespace isr.VI.K2400
{

    /// <summary>
    /// Defines the contract that must be implemented by a SCPI Digital Output Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class DigitalOutputSubsystem : DigitalOutputSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalOutputSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public DigitalOutputSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " SYNTAX "

        /// <summary> Gets or sets the digital output clear command. </summary>
        /// <remarks> SCPI: ":SOUR2:CLE". </remarks>
        /// <value> The digital output clear command. </value>
        protected override string ClearCommand { get; set; } = ":SOUR2:CLE";

        /// <summary> Gets or sets the digital output Auto Clear enabled query command. </summary>
        /// <remarks> SCPI: ":SOUR2:CLE:AUTO?". </remarks>
        /// <value> The digital output Auto Clear enabled query command. </value>
        protected override string AutoClearEnabledQueryCommand { get; set; } = ":SOUR2:CLE:AUTO?";

        /// <summary> Gets or sets the digital output Auto Clear enabled command Format. </summary>
        /// <remarks> SCPI: ":SOUR2:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The digital output Auto Clear enabled query command. </value>
        protected override string AutoClearEnabledCommandFormat { get; set; } = ":SOUR2:CLE:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the digital output Delay query command. </summary>
        /// <value> The Limit enabled query command. </value>
        protected override string DelayQueryCommand { get; set; } = ":SOUR2:CLE:AUTO:DEL?";

        /// <summary> Gets or sets the digital output Delay query command. </summary>
        /// <value> The Limit enabled query command. </value>
        protected override string DelayCommandFormat { get; set; } = ":SOUR2:CLE:AUTO:DEL {0}";

        /// <summary> Gets or sets the digital output Bit Size query command. </summary>
        /// <value> The 'bit size query' command. </value>
        protected override string BitSizeQueryCommand { get; set; } = ":SOUR2:BSIZ?";

        /// <summary> Gets or sets the digital output Bit Size query command. </summary>
        /// <value> The bit size command format. </value>
        protected override string BitSizeCommandFormat { get; set; } = ":SOUR2:BSIZ {0}";

        /// <summary> Gets or sets the digital output Level query command. </summary>
        /// <value> The Limit enabled query command. </value>
        protected override string LevelQueryCommand { get; set; } = ":SOUR2:TTL:ACT?";

        /// <summary> Gets or sets the digital output Level query command. </summary>
        /// <value> The Limit enabled query command. </value>
        protected override string LevelCommandFormat { get; set; } = ":SOUR2:TTL:LEV {0}";

        /// <summary> Gets or sets the Output Mode query command. </summary>
        /// <remarks> SCPI: ":SOUR2:TTL4:MODE?". </remarks>
        /// <value> The Output Mode query command. </value>
        protected override string OutputModeQueryCommand { get; set; } = ":SOUR2:TTL4:MODE?";

        /// <summary> Gets or sets the Output Mode command format. </summary>
        /// <remarks> SCPI: ":SOUR2:TTL4:MODE {0}". </remarks>
        /// <value> The Output Mode query command format. </value>
        protected override string OutputModeCommandFormat { get; set; } = ":SOUR2:TTL4:MODE {0}";

        /// <summary> Gets or sets the Output Polarity query command. </summary>
        /// <remarks> SCPI: "SOUR2:TTL4:BST?". </remarks>
        /// <value> The Output Polarity query command. </value>
        protected override string DigitalActiveLevelQueryCommand { get; set; } = "SOUR2:TTL4:BST?";

        /// <summary> Gets or sets the Output Polarity command format. </summary>
        /// <remarks> SCPI: "SOUR2:TTL4:BST {0}". </remarks>
        /// <value> The Output Polarity command format. </value>
        protected override string DigitalActiveLevelCommandFormat { get; set; } = "SOUR2:TTL4:BST {0}";

        #endregion

    }
}
