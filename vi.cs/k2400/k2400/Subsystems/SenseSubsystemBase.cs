using System;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400
{

    /// <summary> Defines the contract that must be implemented by a Sense Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific ReSenses, Inc.<para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public abstract class SenseSubsystemBase : VI.SenseSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected SenseSubsystemBase( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading | ReadingElementTypes.Voltage | ReadingElementTypes.Current );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ampere );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere;
        }

        #endregion

        #region " CONCURRENT SENSE FUNCTION MODE "

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <remarks> SCPI: ":SENS:FUNC:CONC?". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected override string ConcurrentSenseEnabledQueryCommand { get; set; } = ":SENS:FUNC:CONC?";

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <remarks> SCPI: ":SENS:FUNC:CONC {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Range enabled query command. </value>
        protected override string ConcurrentSenseEnabledCommandFormat { get; set; } = ":SENS:FUNC:CONC {0:'ON';'ON';'OFF'}";

        #endregion

        #region " LATEST DATA "

        /// <summary> Gets or sets the latest data query command. </summary>
        /// <remarks> SCPI: ":SENSE:DATA:LAT?". </remarks>
        /// <value> The latest data query command. </value>
        protected override string LatestDataQueryCommand { get; set; } = ":SENSE:DATA:LAT?";

        #endregion

        #region " FUNCTION MODES "

        /// <summary> Builds the Modes record for the specified Modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="modes"> Sense Function Modes. </param>
        /// <returns> The record. </returns>
        public static string BuildRecord( SenseFunctionModes modes )
        {
            if ( modes == SenseFunctionModes.None )
            {
                return string.Empty;
            }
            else
            {
                var reply = new System.Text.StringBuilder();
                foreach ( int code in Enum.GetValues( typeof( SenseFunctionModes ) ) )
                {
                    if ( (( int ) modes & code) != 0 )
                    {
                        string value = (( SenseFunctionModes ) Conversions.ToInteger( code )).ExtractBetween();
                        if ( !string.IsNullOrWhiteSpace( value ) )
                        {
                            if ( reply.Length > 0 )
                            {
                                _ = reply.Append( "," );
                            }

                            _ = reply.Append( value );
                        }
                    }
                }

                return reply.ToString();
            }
        }

        /// <summary>
        /// Get the composite Sense Function Modes based on the message from the instrument.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="record"> Specifies the comma delimited Modes record. </param>
        /// <returns> The sense function modes. </returns>
        public static SenseFunctionModes ParseSenseFunctionModes( string record )
        {
            var parsed = SenseFunctionModes.None;
            if ( !string.IsNullOrWhiteSpace( record ) )
            {
                foreach ( string modeValue in record.Split( ',' ) )
                    parsed |= ParseSenseFunctionMode( modeValue );
            }

            return parsed;
        }

        /// <summary>
        /// Returns the <see cref="SenseFunctionModes"></see> from the specified value.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The Modes. </param>
        /// <returns> The sense function mode. </returns>
        public static SenseFunctionModes ParseSenseFunctionMode( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return SenseFunctionModes.None;
            }
            else
            {
                var se = new Core.StringEnumerator<SenseFunctionModes>();
                return se.ParseContained( value.BuildDelimitedValue() );
            }
        }

        /// <summary> The Sense Function Modes. </summary>
        private SenseFunctionModes? _FunctionModes;

        /// <summary> Gets or sets the cached Sense Function Modes. </summary>
        /// <value> The Function Modes or null if unknown. </value>
        public SenseFunctionModes? FunctionModes
        {
            get => this._FunctionModes;

            set {
                if ( !Nullable.Equals( this.FunctionModes, value ) )
                {
                    this._FunctionModes = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Sense function mode query command. </summary>
        /// <value> The Sense function mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = string.Empty; // not supported

        /// <summary> Gets or sets the Sense function mode command format. </summary>
        /// <value> The Sense function mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = string.Empty; // not supported

        /// <summary> True to supports multi functions. </summary>
        private bool _SupportsMultiFunctions;

        /// <summary>
        /// Gets or sets the condition telling if the instrument supports multi-functions. For example,
        /// the 2400 source-measure instrument support measuring voltage, current, and resistance
        /// concurrently whereas the 2700 supports a single function at a time.
        /// </summary>
        /// <value> The supports multi functions. </value>
        public bool SupportsMultiFunctions
        {
            get => this._SupportsMultiFunctions;

            set {
                if ( !this.SupportsMultiFunctions.Equals( value ) )
                {
                    this._SupportsMultiFunctions = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets The Range query command. </summary>
        /// <value> The Range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SOUR:RANG?";

        /// <summary> Gets or sets The Range command format. </summary>
        /// <value> The Range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SENS:RANG {0}";

        #endregion

    }
}
