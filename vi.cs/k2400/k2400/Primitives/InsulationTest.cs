using System;

using isr.Core.Models;

namespace isr.VI.K2400
{

    /// <summary> An insulation test. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-03-09 </para>
    /// </remarks>
    public class InsulationTest : ViewModelBase
    {

        #region " CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public InsulationTest() : base()
        {
            this.ResetKnownStateThis();
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets values to their known clear execution state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void DefineClearExecutionState()
        {
        }

        /// <summary> Initializes the known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void InitKnownState()
        {
            this.ResetKnownState();
            this.DefineClearExecutionState();
        }

        /// <summary> Preset known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void PresetKnownState()
        {
        }

        /// <summary> Resets the known state this. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ResetKnownStateThis()
        {
            this.Binning = new BinningInfo();
            this.Insulation = new InsulationResistance();
            // not using upper limit
            this.Binning.UpperLimit = Pith.Scpi.Syntax.Infinity;
            this.Binning.UpperLimitFailureBits = 4;
            // using lower limit
            this.Binning.LowerLimit = 10000000d;
            this.Binning.LowerLimitFailureBits = 4;
            this.Binning.PassBits = 1;
            this.Binning.ArmCount = 0;
            this.Binning.ArmDirection = TriggerLayerBypassModes.Source;
            // using SOT Line
            this.Binning.InputLineNumber = 1;
            // USING EOT Line
            this.Binning.OutputLineNumber = 2;
            this.Binning.ArmSource = ArmSources.StartTestBoth;
            this.Binning.ArmDirection = TriggerLayerBypassModes.Acceptor;
            this.Binning.TriggerDirection = TriggerLayerBypassModes.Source;
            this.Binning.TriggerSource = TriggerSources.Immediate;
            this.Insulation.DwellTime = TimeSpan.FromSeconds( 2d );
            this.Insulation.CurrentLimit = 0.00001d;
            this.Insulation.PowerLineCycles = 1d;
            this.Insulation.VoltageLevel = 10d;
            this.Insulation.ResistanceLowLimit = 10000000d;
            this.Insulation.ResistanceRange = 1000000000d;
            this.Insulation.ContactCheckEnabled = true;
        }

        /// <summary> Resets the known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ResetKnownState()
        {
            this.ResetKnownStateThis();
        }


        #endregion

        #region " FIELDS "

        /// <summary> Gets or sets the binning. </summary>
        /// <value> The binning. </value>
        public BinningInfo Binning { get; set; }

        /// <summary> Gets or sets the insulation. </summary>
        /// <value> The insulation. </value>
        public InsulationResistance Insulation { get; set; }

        #endregion

    }
}
