﻿using isr.Core.NumericExtensions;

namespace isr.VI.K2400
{

    /// <summary> Holds a single set of 27xx instrument reading elements. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2008-01-15, 2.0.2936 Create based on the 24xx system classes. </para>
    /// </remarks>
    public class Readings : ReadingAmounts
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>

        // instantiate the base class
        public Readings() : base()
        {
            this.PrimaryReading = new MeasuredAmount( ReadingElementTypes.Reading, Arebis.StandardUnits.ElectricUnits.Ampere ) {
                ComplianceLimit = Pith.Scpi.Syntax.Infinity,
                HighLimit = Pith.Scpi.Syntax.Infinity,
                LowLimit = Pith.Scpi.Syntax.NegativeInfinity,
                ReadingLength = 13
            };
            this.BaseReadings.Add( this.PrimaryReading );
            this.VoltageReading = new MeasuredAmount( ReadingElementTypes.Voltage, Arebis.StandardUnits.ElectricUnits.Volt ) {
                ComplianceLimit = Pith.Scpi.Syntax.Infinity,
                HighLimit = Pith.Scpi.Syntax.Infinity,
                LowLimit = Pith.Scpi.Syntax.NegativeInfinity,
                ReadingLength = 13
            };
            this.BaseReadings.Add( this.VoltageReading );
            this.CurrentReading = new MeasuredAmount( ReadingElementTypes.Current, Arebis.StandardUnits.ElectricUnits.Ampere ) {
                ComplianceLimit = Pith.Scpi.Syntax.Infinity,
                HighLimit = Pith.Scpi.Syntax.Infinity,
                LowLimit = Pith.Scpi.Syntax.NegativeInfinity,
                ReadingLength = 13
            };
            this.BaseReadings.Add( this.CurrentReading );
            this.ResistanceReading = new MeasuredAmount( ReadingElementTypes.Resistance, Arebis.StandardUnits.ElectricUnits.Ohm ) {
                ComplianceLimit = Pith.Scpi.Syntax.Infinity,
                HighLimit = Pith.Scpi.Syntax.Infinity,
                LowLimit = Pith.Scpi.Syntax.NegativeInfinity,
                ReadingLength = 13
            };
            this.BaseReadings.Add( this.ResistanceReading );
            this.Timestamp = new ReadingAmount( ReadingElementTypes.Time, Arebis.StandardUnits.TimeUnits.Second ) { ReadingLength = 13 };
            this.BaseReadings.Add( this.Timestamp );
            this.StatusReading = new ReadingStatus( ReadingElementTypes.Status ) { ReadingLength = 13 };
            this.BaseReadings.Add( this.StatusReading );
        }

        /// <summary> Create a copy of the model. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="model"> The model. </param>
        public Readings( Readings model ) : base( model )
        {
            if ( model is object )
            {
                this.PrimaryReading = new MeasuredAmount( model.PrimaryReading );
                this.CurrentReading = new MeasuredAmount( model.CurrentReading );
                this.ResistanceReading = new MeasuredAmount( model.ResistanceReading );
                this.VoltageReading = new MeasuredAmount( model.VoltageReading );
                this.Timestamp = new ReadingAmount( model.Timestamp );
                this.StatusReading = new ReadingStatus( model.StatusReading );
            }
        }

        /// <summary> Create a copy of the model. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="model"> The model. </param>
        public Readings( ReadingAmounts model ) : base( model )
        {
        }

        /// <summary> Clones this class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="model"> The value. </param>
        /// <returns> A copy of this object. </returns>
        public override ReadingAmounts Clone( ReadingAmounts model )
        {
            return new Readings( model );
        }

        #endregion

        #region " PARSE "

        /// <summary> Builds meta status. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns>
        /// The <see cref="T:isr.VI.MetaStatus" /><see cref="P:isr.VI.MetaStatus.StatusValue" /> .
        /// </returns>
        protected override long BuildMetaStatus( long status )
        {
            var metaStatus = new MetaStatus();
            metaStatus.Preset( status );
            if ( status != 0L )
            {
                // update the meta status based on the status reading.
                if ( status.IsBit( ( int ) StatusWordBit.FailedContactCheck ) )
                {
                    metaStatus.FailedContactCheck = true;
                }

                if ( status.IsBit( ( int ) StatusWordBit.HitCompliance ) )
                {
                    metaStatus.HitStatusCompliance = true;
                }

                if ( status.IsBit( ( int ) StatusWordBit.HitRangeCompliance ) )
                {
                    metaStatus.HitRangeCompliance = true;
                }

                if ( status.IsBit( ( int ) StatusWordBit.HitVoltageProtection ) )
                {
                    metaStatus.HitVoltageProtection = true;
                }

                if ( status.IsBit( ( int ) StatusWordBit.OverRange ) )
                {
                    metaStatus.HitOverRange = true;
                }
            }

            return metaStatus.StatusValue;
        }

        #endregion

    }
}