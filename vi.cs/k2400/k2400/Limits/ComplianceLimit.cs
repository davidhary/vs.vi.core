namespace isr.VI.K2400
{

    /// <summary>
    /// Defines the contract that must be implemented by a SCPI Compliance Limit Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class ComplianceLimit : ComplianceLimitBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ComplianceLimit" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public ComplianceLimit( StatusSubsystemBase statusSubsystem ) : base( 1, statusSubsystem )
        {
        }

        #endregion

        #region " SYNTAX "

        #region " COMPLIANCE FAILURE BITS "

        /// <summary> Gets or sets Compliance Failure Bits query command. </summary>
        /// <value> The Compliance Failure Bits query command. </value>
        protected override string FailureBitsQueryCommand { get; set; } = ":CALC2:LIM{0}:COMP:SOUR2?";

        /// <summary> Gets or sets Compliance Failure Bits command format. </summary>
        /// <value> The Compliance Failure Bits command format. </value>
        protected override string FailureBitsCommandFormat { get; set; } = ":CALC2:LIM{0}:COMP:SOUR2 {{0}}";

        #endregion

        #region " IN COMPLAINCE FAIL CONDITION "

        /// <summary>
        /// Gets or sets the In-compliance Condition command Format.
        /// <see cref="P:isr.VI.ComplianceLimitBase.IncomplianceCondition">Condition</see> sentinel.
        /// </summary>
        /// <value> The in-compliance condition command format. </value>
        protected override string IncomplianceConditionCommandFormat { get; set; } = ":CALC2:LIM{0}:COMP:FAIL {{0:'IN';'IN';'OUT'}}";

        /// <summary> Gets or sets the In compliance Condition query command. </summary>
        /// <remarks> SCPI: ":CALC2:LIM:COMP:FAIL?". </remarks>
        /// <value> The In-compliance Condition query command. </value>
        protected override string IncomplianceConditionQueryCommand { get; set; } = ":CALC2:LIM{0}:COMP:FAIL?";

        #endregion

        #region " LIMIT FAILED "

        /// <summary> Gets or sets the Limit Failed query command. </summary>
        /// <value> The Limit Failed query command. </value>
        protected override string FailedQueryCommand { get; set; } = ":CALC2:LIM{0}:FAIL?";

        #endregion

        #region " LIMIT ENABLED "

        /// <summary> Gets or sets the Limit enabled command Format. </summary>
        /// <remarks> SCPI: "CALC2:LIM[#]:STAT {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected override string EnabledCommandFormat { get; set; } = "CALC2:LIM{0}:STAT {{0:'ON';'ON';'OFF'}}";

        /// <summary> Gets or sets the Limit enabled query command. </summary>
        /// <remarks> SCPI: "CALC2:LIM[#]:STAT?". </remarks>
        /// <value> The Limit enabled query command. </value>
        protected override string EnabledQueryCommand { get; set; } = ":CALC2:LIM{0}:STAT?";

        #endregion

        #endregion

    }
}
