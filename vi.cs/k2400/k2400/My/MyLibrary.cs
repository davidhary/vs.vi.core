﻿
namespace isr.VI.K2400.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.K2400;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI K2400 Source Meter Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "K2400 Source Meter Virtual Instrument Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.K2400.Source.Meter";
    }
}