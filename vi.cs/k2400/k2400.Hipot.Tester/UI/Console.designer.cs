﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.K2400.Hipot.Tester
{
    [DesignerGenerated()]
    public partial class Console
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(Console));
            _MainLayout = new TableLayoutPanel();
            _ConfigurationLayout = new TableLayoutPanel();
            _ConfigGroupBox = new GroupBox();
            _CurrentLimitNumeric = new NumericUpDown();
            _CurrentLimitNumericLabel = new Label();
            _VoltageLevelNumericLabel = new Label();
            _VoltageLevelNumeric = new NumericUpDown();
            _ResistanceRangeNumericLabel = new Label();
            _ResistanceLowLimitNumericLabel = new Label();
            _ResistanceRangeNumeric = new NumericUpDown();
            _ResistanceLowLimitNumeric = new NumericUpDown();
            _DwellTimeNumericLabel = new Label();
            _DwellTimeNumeric = new NumericUpDown();
            _ApertureNumeric = new NumericUpDown();
            _ApertureNumericLabel = new Label();
            _BinningGroupBox = new GroupBox();
            _ContactCheckSupportLabel = new Label();
            _FailBitPatternNumeric = new NumericUpDown();
            _PassBitPatternNumeric = new NumericUpDown();
            _ContactCheckBitPatternNumeric = new NumericUpDown();
            _FailBitPatternNumericLabel = new Label();
            _PassBitPatternNumericLabel = new Label();
            _ContactCheckBitPatternNumericLabel = new Label();
            _ContactCheckToggle = new CheckBox();
            _EotStrobeDurationNumericLabel = new Label();
            _EotStrobeDurationNumeric = new NumericUpDown();
            _ConnectGroupBox = new GroupBox();
            __ConnectToggle = new CheckBox();
            __ConnectToggle.CheckedChanged += new EventHandler(ConnectToggle_CheckedChanged);
            _ResourceNameTextBoxLabel = new Label();
            _IdentityTextBox = new TextBox();
            _ResourceNameTextBox = new TextBox();
            _OutcomeLayout = new TableLayoutPanel();
            _CurrentTextBox = new TextBox();
            _CurrentTextBoxLabel = new Label();
            _ResistanceTextBox = new TextBox();
            _VoltageTextBox = new TextBox();
            _ResistanceTextBoxLabel = new Label();
            _VoltageTextBoxLabel = new Label();
            _ManualGroupBox = new GroupBox();
            _MeasureControlsLayout = new TableLayoutPanel();
            __MeasureButton = new Button();
            __MeasureButton.Click += new EventHandler(MeasureButton_Click);
            _ApplyLayout = new TableLayoutPanel();
            __ApplyConfigButton = new Button();
            __ApplyConfigButton.Click += new EventHandler(ApplyConfigButton_Click);
            _TriggerToolStrip = new ToolStrip();
            __AwaitTriggerToolStripButton = new ToolStripButton();
            __AwaitTriggerToolStripButton.CheckedChanged += new EventHandler(AwaitTriggerToolStripButton_CheckedChanged);
            _WaitHourglassLabel = new ToolStripLabel();
            _TriggersDropDownButton = new ToolStripDropDownButton();
            __ManualTriggerMenuItem = new ToolStripMenuItem();
            __ManualTriggerMenuItem.CheckStateChanged += new EventHandler(ManualTriggerMenuItem_CheckStateChanged);
            __SotTriggerMenuItem = new ToolStripMenuItem();
            __SotTriggerMenuItem.CheckStateChanged += new EventHandler(SotTriggerMenuItem_CheckStateChanged);
            __AssertTriggerToolStripButton = new ToolStripButton();
            __AssertTriggerToolStripButton.Click += new EventHandler(AssertTriggerToolStripButton_Click);
            _TriggerActionToolStripLabel = new ToolStripLabel();
            _ToolTip = new ToolTip(components);
            _ErrorProvider = new ErrorProvider(components);
            __MeterTimer = new Timer(components);
            __MeterTimer.Tick += new EventHandler(MeterTimer_Tick);
            _StatusStrip = new StatusStrip();
            _StatusLabel = new ToolStripStatusLabel();
            _MainLayout.SuspendLayout();
            _ConfigurationLayout.SuspendLayout();
            _ConfigGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_CurrentLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_VoltageLevelNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceLowLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_DwellTimeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).BeginInit();
            _BinningGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_FailBitPatternNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_PassBitPatternNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ContactCheckBitPatternNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_EotStrobeDurationNumeric).BeginInit();
            _ConnectGroupBox.SuspendLayout();
            _OutcomeLayout.SuspendLayout();
            _ManualGroupBox.SuspendLayout();
            _MeasureControlsLayout.SuspendLayout();
            _ApplyLayout.SuspendLayout();
            _TriggerToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).BeginInit();
            _StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _mainLayout
            // 
            _MainLayout.ColumnCount = 3;
            _MainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _MainLayout.ColumnStyles.Add(new ColumnStyle());
            _MainLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _MainLayout.Controls.Add(_ConfigurationLayout, 1, 5);
            _MainLayout.Controls.Add(_ConnectGroupBox, 1, 3);
            _MainLayout.Controls.Add(_OutcomeLayout, 1, 1);
            _MainLayout.Controls.Add(_ManualGroupBox, 1, 8);
            _MainLayout.Controls.Add(_ApplyLayout, 1, 6);
            _MainLayout.Controls.Add(_TriggerToolStrip, 1, 10);
            _MainLayout.Dock = DockStyle.Fill;
            _MainLayout.Location = new Point(0, 0);
            _MainLayout.Name = "_mainLayout";
            _MainLayout.RowCount = 12;
            _MainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 20.0f));
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 20.0f));
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 20.0f));
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 20.0f));
            _MainLayout.RowStyles.Add(new RowStyle());
            _MainLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 20.0f));
            _MainLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _MainLayout.Size = new Size(660, 683);
            _MainLayout.TabIndex = 0;
            // 
            // _configurationLayout
            // 
            _ConfigurationLayout.ColumnCount = 4;
            _ConfigurationLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ConfigurationLayout.ColumnStyles.Add(new ColumnStyle());
            _ConfigurationLayout.ColumnStyles.Add(new ColumnStyle());
            _ConfigurationLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ConfigurationLayout.Controls.Add(_ConfigGroupBox, 1, 0);
            _ConfigurationLayout.Controls.Add(_BinningGroupBox, 2, 0);
            _ConfigurationLayout.Dock = DockStyle.Top;
            _ConfigurationLayout.Location = new Point(15, 233);
            _ConfigurationLayout.Name = "_configurationLayout";
            _ConfigurationLayout.RowCount = 1;
            _ConfigurationLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _ConfigurationLayout.Size = new Size(629, 197);
            _ConfigurationLayout.TabIndex = 1;
            // 
            // _ConfigGroupBox
            // 
            _ConfigGroupBox.Controls.Add(_CurrentLimitNumeric);
            _ConfigGroupBox.Controls.Add(_CurrentLimitNumericLabel);
            _ConfigGroupBox.Controls.Add(_VoltageLevelNumericLabel);
            _ConfigGroupBox.Controls.Add(_VoltageLevelNumeric);
            _ConfigGroupBox.Controls.Add(_ResistanceRangeNumericLabel);
            _ConfigGroupBox.Controls.Add(_ResistanceLowLimitNumericLabel);
            _ConfigGroupBox.Controls.Add(_ResistanceRangeNumeric);
            _ConfigGroupBox.Controls.Add(_ResistanceLowLimitNumeric);
            _ConfigGroupBox.Controls.Add(_DwellTimeNumericLabel);
            _ConfigGroupBox.Controls.Add(_DwellTimeNumeric);
            _ConfigGroupBox.Controls.Add(_ApertureNumeric);
            _ConfigGroupBox.Controls.Add(_ApertureNumericLabel);
            _ConfigGroupBox.Location = new Point(15, 3);
            _ConfigGroupBox.Name = "_ConfigGroupBox";
            _ConfigGroupBox.Size = new Size(296, 191);
            _ConfigGroupBox.TabIndex = 1;
            _ConfigGroupBox.TabStop = false;
            _ConfigGroupBox.Text = "TEST SETTINGS";
            // 
            // _CurrentLimitNumeric
            // 
            _CurrentLimitNumeric.DecimalPlaces = 7;
            _CurrentLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 327680 });
            _CurrentLimitNumeric.Location = new Point(178, 106);
            _CurrentLimitNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 131072 });
            _CurrentLimitNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 393216 });
            _CurrentLimitNumeric.Name = "_CurrentLimitNumeric";
            _CurrentLimitNumeric.RightToLeft = RightToLeft.No;
            _CurrentLimitNumeric.Size = new Size(87, 20);
            _CurrentLimitNumeric.TabIndex = 7;
            _CurrentLimitNumeric.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_CurrentLimitNumeric, "Maximum allowed current in Amperes. If measured current hits this limit, the " + "measurement is said to hit compliance.");
            _CurrentLimitNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _CurrentLimitNumericLabel
            // 
            _CurrentLimitNumericLabel.AutoSize = true;
            _CurrentLimitNumericLabel.Location = new Point(66, 110);
            _CurrentLimitNumericLabel.Name = "_CurrentLimitNumericLabel";
            _CurrentLimitNumericLabel.Size = new Size(110, 13);
            _CurrentLimitNumericLabel.TabIndex = 2;
            _CurrentLimitNumericLabel.Text = "CURRENT LIMIT [A]:";
            // 
            // _VoltageLevelNumericLabel
            // 
            _VoltageLevelNumericLabel.AutoSize = true;
            _VoltageLevelNumericLabel.Location = new Point(64, 83);
            _VoltageLevelNumericLabel.Name = "_VoltageLevelNumericLabel";
            _VoltageLevelNumericLabel.Size = new Size(112, 13);
            _VoltageLevelNumericLabel.TabIndex = 2;
            _VoltageLevelNumericLabel.Text = "VOLTAGE LEVEL [V]:";
            // 
            // _VoltageLevelNumeric
            // 
            _VoltageLevelNumeric.DecimalPlaces = 3;
            _VoltageLevelNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _VoltageLevelNumeric.Location = new Point(178, 79);
            _VoltageLevelNumeric.Maximum = new decimal(new int[] { 1100, 0, 0, 0 });
            _VoltageLevelNumeric.Name = "_VoltageLevelNumeric";
            _VoltageLevelNumeric.RightToLeft = RightToLeft.No;
            _VoltageLevelNumeric.Size = new Size(87, 20);
            _VoltageLevelNumeric.TabIndex = 5;
            _VoltageLevelNumeric.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_VoltageLevelNumeric, "Applied voltage in volts");
            _VoltageLevelNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _ResistanceRangeNumericLabel
            // 
            _ResistanceRangeNumericLabel.AutoSize = true;
            _ResistanceRangeNumericLabel.Location = new Point(39, 164);
            _ResistanceRangeNumericLabel.Name = "_ResistanceRangeNumericLabel";
            _ResistanceRangeNumericLabel.Size = new Size(137, 13);
            _ResistanceRangeNumericLabel.TabIndex = 4;
            _ResistanceRangeNumericLabel.Text = "RESISTANCE RANGE [Ω]:";
            // 
            // _resistanceLowLimitNumericLabel
            // 
            _ResistanceLowLimitNumericLabel.AutoSize = true;
            _ResistanceLowLimitNumericLabel.Location = new Point(21, 137);
            _ResistanceLowLimitNumericLabel.Name = "_resistanceLowLimitNumericLabel";
            _ResistanceLowLimitNumericLabel.Size = new Size(155, 13);
            _ResistanceLowLimitNumericLabel.TabIndex = 4;
            _ResistanceLowLimitNumericLabel.Text = "RESISTANCE LOW LIMIT [Ω]:";
            // 
            // _ResistanceRangeNumeric
            // 
            _ResistanceRangeNumeric.Increment = new decimal(new int[] { 1000000, 0, 0, 0 });
            _ResistanceRangeNumeric.Location = new Point(178, 160);
            _ResistanceRangeNumeric.Maximum = new decimal(new int[] { 1000000000, 0, 0, 0 });
            _ResistanceRangeNumeric.Minimum = new decimal(new int[] { 1000, 0, 0, 0 });
            _ResistanceRangeNumeric.Name = "_ResistanceRangeNumeric";
            _ResistanceRangeNumeric.Size = new Size(87, 20);
            _ResistanceRangeNumeric.TabIndex = 3;
            _ResistanceRangeNumeric.TextAlign = HorizontalAlignment.Center;
            _ResistanceRangeNumeric.ThousandsSeparator = true;
            _ToolTip.SetToolTip(_ResistanceRangeNumeric, "Range of insulation resistance. Also high limit for overflow indication.");
            _ResistanceRangeNumeric.Value = new decimal(new int[] { 200000000, 0, 0, 0 });
            // 
            // _ResistanceLowLimitNumeric
            // 
            _ResistanceLowLimitNumeric.Increment = new decimal(new int[] { 1000000, 0, 0, 0 });
            _ResistanceLowLimitNumeric.Location = new Point(178, 133);
            _ResistanceLowLimitNumeric.Maximum = new decimal(new int[] { 100000000, 0, 0, 0 });
            _ResistanceLowLimitNumeric.Minimum = new decimal(new int[] { 1000, 0, 0, 0 });
            _ResistanceLowLimitNumeric.Name = "_ResistanceLowLimitNumeric";
            _ResistanceLowLimitNumeric.Size = new Size(87, 20);
            _ResistanceLowLimitNumeric.TabIndex = 3;
            _ResistanceLowLimitNumeric.TextAlign = HorizontalAlignment.Center;
            _ResistanceLowLimitNumeric.ThousandsSeparator = true;
            _ToolTip.SetToolTip(_ResistanceLowLimitNumeric, "Low limit of insulation resistance");
            _ResistanceLowLimitNumeric.Value = new decimal(new int[] { 10000000, 0, 0, 0 });
            // 
            // _dwellTimeNumericLabel
            // 
            _DwellTimeNumericLabel.AutoSize = true;
            _DwellTimeNumericLabel.Location = new Point(83, 56);
            _DwellTimeNumericLabel.Name = "_dwellTimeNumericLabel";
            _DwellTimeNumericLabel.Size = new Size(93, 13);
            _DwellTimeNumericLabel.TabIndex = 2;
            _DwellTimeNumericLabel.Text = "DWELL TIME [S]:";
            // 
            // _dwellTimeNumeric
            // 
            _DwellTimeNumeric.DecimalPlaces = 2;
            _DwellTimeNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _DwellTimeNumeric.Location = new Point(178, 52);
            _DwellTimeNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _DwellTimeNumeric.Name = "_dwellTimeNumeric";
            _DwellTimeNumeric.Size = new Size(87, 20);
            _DwellTimeNumeric.TabIndex = 3;
            _DwellTimeNumeric.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_DwellTimeNumeric, "Dwell time. Time the source is on before measurement is made.");
            _DwellTimeNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _ApertureNumeric
            // 
            _ApertureNumeric.DecimalPlaces = 2;
            _ApertureNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ApertureNumeric.Location = new Point(178, 25);
            _ApertureNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _ApertureNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 131072 });
            _ApertureNumeric.Name = "_ApertureNumeric";
            _ApertureNumeric.Size = new Size(87, 20);
            _ApertureNumeric.TabIndex = 1;
            _ApertureNumeric.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_ApertureNumeric, "Aperture in power line cycles");
            _ApertureNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _ApertureNumericLabel
            // 
            _ApertureNumericLabel.AutoSize = true;
            _ApertureNumericLabel.Location = new Point(73, 29);
            _ApertureNumericLabel.Name = "_ApertureNumericLabel";
            _ApertureNumericLabel.Size = new Size(103, 13);
            _ApertureNumericLabel.TabIndex = 0;
            _ApertureNumericLabel.Text = "APERTURE [NPLC]";
            // 
            // _BinningGroupBox
            // 
            _BinningGroupBox.Controls.Add(_ContactCheckSupportLabel);
            _BinningGroupBox.Controls.Add(_FailBitPatternNumeric);
            _BinningGroupBox.Controls.Add(_PassBitPatternNumeric);
            _BinningGroupBox.Controls.Add(_ContactCheckBitPatternNumeric);
            _BinningGroupBox.Controls.Add(_FailBitPatternNumericLabel);
            _BinningGroupBox.Controls.Add(_PassBitPatternNumericLabel);
            _BinningGroupBox.Controls.Add(_ContactCheckBitPatternNumericLabel);
            _BinningGroupBox.Controls.Add(_ContactCheckToggle);
            _BinningGroupBox.Controls.Add(_EotStrobeDurationNumericLabel);
            _BinningGroupBox.Controls.Add(_EotStrobeDurationNumeric);
            _BinningGroupBox.Location = new Point(317, 3);
            _BinningGroupBox.Name = "_BinningGroupBox";
            _BinningGroupBox.Size = new Size(296, 191);
            _BinningGroupBox.TabIndex = 1;
            _BinningGroupBox.TabStop = false;
            _BinningGroupBox.Text = "SOT, EOT, BINNING";
            // 
            // _ContactCheckSupportLabel
            // 
            _ContactCheckSupportLabel.AutoSize = true;
            _ContactCheckSupportLabel.Dock = DockStyle.Bottom;
            _ContactCheckSupportLabel.Location = new Point(3, 175);
            _ContactCheckSupportLabel.Name = "_ContactCheckSupportLabel";
            _ContactCheckSupportLabel.Size = new Size(0, 13);
            _ContactCheckSupportLabel.TabIndex = 9;
            _ContactCheckSupportLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // _FailBitPatternNumeric
            // 
            _FailBitPatternNumeric.Location = new Point(186, 79);
            _FailBitPatternNumeric.Maximum = new decimal(new int[] { 7, 0, 0, 0 });
            _FailBitPatternNumeric.Name = "_FailBitPatternNumeric";
            _FailBitPatternNumeric.Size = new Size(35, 20);
            _FailBitPatternNumeric.TabIndex = 5;
            _FailBitPatternNumeric.TextAlign = HorizontalAlignment.Center;
            _FailBitPatternNumeric.Value = new decimal(new int[] { 4, 0, 0, 0 });
            // 
            // _PassBitPatternNumeric
            // 
            _PassBitPatternNumeric.Location = new Point(186, 52);
            _PassBitPatternNumeric.Maximum = new decimal(new int[] { 7, 0, 0, 0 });
            _PassBitPatternNumeric.Name = "_PassBitPatternNumeric";
            _PassBitPatternNumeric.Size = new Size(35, 20);
            _PassBitPatternNumeric.TabIndex = 3;
            _PassBitPatternNumeric.TextAlign = HorizontalAlignment.Center;
            _PassBitPatternNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _ContactCheckBitPatternNumeric
            // 
            _ContactCheckBitPatternNumeric.Location = new Point(186, 106);
            _ContactCheckBitPatternNumeric.Maximum = new decimal(new int[] { 7, 0, 0, 0 });
            _ContactCheckBitPatternNumeric.Name = "_ContactCheckBitPatternNumeric";
            _ContactCheckBitPatternNumeric.Size = new Size(35, 20);
            _ContactCheckBitPatternNumeric.TabIndex = 7;
            _ContactCheckBitPatternNumeric.TextAlign = HorizontalAlignment.Center;
            _ContactCheckBitPatternNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _FailBitPatternNumericLabel
            // 
            _FailBitPatternNumericLabel.AutoSize = true;
            _FailBitPatternNumericLabel.Location = new Point(75, 83);
            _FailBitPatternNumericLabel.Name = "_FailBitPatternNumericLabel";
            _FailBitPatternNumericLabel.Size = new Size(106, 13);
            _FailBitPatternNumericLabel.TabIndex = 4;
            _FailBitPatternNumericLabel.Text = "FAIL BIT PATTERN:";
            _FailBitPatternNumericLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _PassBitPatternNumericLabel
            // 
            _PassBitPatternNumericLabel.AutoSize = true;
            _PassBitPatternNumericLabel.Location = new Point(69, 56);
            _PassBitPatternNumericLabel.Name = "_PassBitPatternNumericLabel";
            _PassBitPatternNumericLabel.Size = new Size(112, 13);
            _PassBitPatternNumericLabel.TabIndex = 2;
            _PassBitPatternNumericLabel.Text = "PASS BIT PATTERN:";
            _PassBitPatternNumericLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _ContactCheckBitPatternNumericLabel
            // 
            _ContactCheckBitPatternNumericLabel.AutoSize = true;
            _ContactCheckBitPatternNumericLabel.Location = new Point(9, 110);
            _ContactCheckBitPatternNumericLabel.Name = "_ContactCheckBitPatternNumericLabel";
            _ContactCheckBitPatternNumericLabel.Size = new Size(174, 13);
            _ContactCheckBitPatternNumericLabel.TabIndex = 6;
            _ContactCheckBitPatternNumericLabel.Text = "CONTACT CHECK BIT PATTERN:";
            _ContactCheckBitPatternNumericLabel.TextAlign = ContentAlignment.MiddleRight;
            // 
            // _ContactCheckToggle
            // 
            _ContactCheckToggle.AutoSize = true;
            _ContactCheckToggle.CheckAlign = ContentAlignment.MiddleRight;
            _ContactCheckToggle.Location = new Point(28, 135);
            _ContactCheckToggle.Name = "_ContactCheckToggle";
            _ContactCheckToggle.Size = new Size(172, 17);
            _ContactCheckToggle.TabIndex = 8;
            _ContactCheckToggle.Text = "CONTACT CHECK ENABLED:";
            _ContactCheckToggle.TextAlign = ContentAlignment.MiddleRight;
            _ContactCheckToggle.UseVisualStyleBackColor = true;
            // 
            // _EotStrobeDurationNumericLabel
            // 
            _EotStrobeDurationNumericLabel.AutoSize = true;
            _EotStrobeDurationNumericLabel.Location = new Point(26, 29);
            _EotStrobeDurationNumericLabel.Name = "_EotStrobeDurationNumericLabel";
            _EotStrobeDurationNumericLabel.Size = new Size(155, 13);
            _EotStrobeDurationNumericLabel.TabIndex = 0;
            _EotStrobeDurationNumericLabel.Text = "EOT STROBE DURATION [S]:";
            // 
            // _EotStrobeDurationNumeric
            // 
            _EotStrobeDurationNumeric.DecimalPlaces = 5;
            _EotStrobeDurationNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _EotStrobeDurationNumeric.Location = new Point(185, 25);
            _EotStrobeDurationNumeric.Maximum = new decimal(new int[] { 60, 0, 0, 0 });
            _EotStrobeDurationNumeric.Name = "_EotStrobeDurationNumeric";
            _EotStrobeDurationNumeric.Size = new Size(87, 20);
            _EotStrobeDurationNumeric.TabIndex = 1;
            _EotStrobeDurationNumeric.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_EotStrobeDurationNumeric, "The duration of the end-of-test strobe.");
            _EotStrobeDurationNumeric.Value = new decimal(new int[] { 1, 0, 0, 327680 });
            // 
            // _connectGroupBox
            // 
            _ConnectGroupBox.Controls.Add(__ConnectToggle);
            _ConnectGroupBox.Controls.Add(_ResourceNameTextBoxLabel);
            _ConnectGroupBox.Controls.Add(_IdentityTextBox);
            _ConnectGroupBox.Controls.Add(_ResourceNameTextBox);
            _ConnectGroupBox.Dock = DockStyle.Top;
            _ConnectGroupBox.Location = new Point(15, 155);
            _ConnectGroupBox.Name = "_connectGroupBox";
            _ConnectGroupBox.Size = new Size(629, 72);
            _ConnectGroupBox.TabIndex = 0;
            _ConnectGroupBox.TabStop = false;
            _ConnectGroupBox.Text = "CONNECT";
            // 
            // _connectToggle
            // 
            __ConnectToggle.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            __ConnectToggle.Appearance = Appearance.Button;
            __ConnectToggle.Location = new Point(485, 20);
            __ConnectToggle.Name = "__connectToggle";
            __ConnectToggle.Size = new Size(121, 23);
            __ConnectToggle.TabIndex = 3;
            __ConnectToggle.Text = "CONNECT";
            __ConnectToggle.TextAlign = ContentAlignment.MiddleCenter;
            _ToolTip.SetToolTip(__ConnectToggle, "Depress to connect or release to disconnect the meter");
            __ConnectToggle.UseVisualStyleBackColor = true;
            // 
            // _resourceNameTextBoxLabel
            // 
            _ResourceNameTextBoxLabel.AutoSize = true;
            _ResourceNameTextBoxLabel.Location = new Point(21, 25);
            _ResourceNameTextBoxLabel.Name = "_resourceNameTextBoxLabel";
            _ResourceNameTextBoxLabel.Size = new Size(87, 13);
            _ResourceNameTextBoxLabel.TabIndex = 2;
            _ResourceNameTextBoxLabel.Text = "Resource Name:";
            // 
            // _identityTextBox
            // 
            _IdentityTextBox.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            _IdentityTextBox.Location = new Point(24, 47);
            _IdentityTextBox.Name = "_identityTextBox";
            _IdentityTextBox.ReadOnly = true;
            _IdentityTextBox.Size = new Size(582, 20);
            _IdentityTextBox.TabIndex = 1;
            _ToolTip.SetToolTip(_IdentityTextBox, "Displays the meter identity information");
            // 
            // _resourceNameTextBox
            // 
            _ResourceNameTextBox.Location = new Point(108, 21);
            _ResourceNameTextBox.Name = "_resourceNameTextBox";
            _ResourceNameTextBox.Size = new Size(374, 20);
            _ResourceNameTextBox.TabIndex = 0;
            _ResourceNameTextBox.Text = "TCPIP0::192.168.1.140::gpib0,24::INSTR";
            // 
            // _outcomeLayout
            // 
            _OutcomeLayout.ColumnCount = 7;
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333f));
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 10.0f));
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333f));
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 10.0f));
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333f));
            _OutcomeLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 22.0f));
            _OutcomeLayout.Controls.Add(_CurrentTextBox, 3, 2);
            _OutcomeLayout.Controls.Add(_CurrentTextBoxLabel, 3, 1);
            _OutcomeLayout.Controls.Add(_ResistanceTextBox, 5, 2);
            _OutcomeLayout.Controls.Add(_VoltageTextBox, 1, 2);
            _OutcomeLayout.Controls.Add(_ResistanceTextBoxLabel, 5, 1);
            _OutcomeLayout.Controls.Add(_VoltageTextBoxLabel, 1, 1);
            _OutcomeLayout.Dock = DockStyle.Top;
            _OutcomeLayout.Location = new Point(15, 41);
            _OutcomeLayout.Name = "_outcomeLayout";
            _OutcomeLayout.RowCount = 4;
            _OutcomeLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 10.0f));
            _OutcomeLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 20.0f));
            _OutcomeLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 27.0f));
            _OutcomeLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _OutcomeLayout.Size = new Size(629, 70);
            _OutcomeLayout.TabIndex = 2;
            // 
            // _currentTextBox
            // 
            _CurrentTextBox.Dock = DockStyle.Top;
            _CurrentTextBox.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _CurrentTextBox.Location = new Point(222, 33);
            _CurrentTextBox.Name = "_currentTextBox";
            _CurrentTextBox.ReadOnly = true;
            _CurrentTextBox.Size = new Size(183, 22);
            _CurrentTextBox.TabIndex = 0;
            _CurrentTextBox.Text = "0.000";
            _CurrentTextBox.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_CurrentTextBox, "Measured Current");
            // 
            // _currentTextBoxLabel
            // 
            _CurrentTextBoxLabel.Dock = DockStyle.Bottom;
            _CurrentTextBoxLabel.Location = new Point(222, 10);
            _CurrentTextBoxLabel.Name = "_currentTextBoxLabel";
            _CurrentTextBoxLabel.Size = new Size(183, 20);
            _CurrentTextBoxLabel.TabIndex = 1;
            _CurrentTextBoxLabel.Text = "CURRENT [A]";
            _CurrentTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _ResistanceTextBox
            // 
            _ResistanceTextBox.Dock = DockStyle.Top;
            _ResistanceTextBox.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ResistanceTextBox.Location = new Point(421, 33);
            _ResistanceTextBox.Name = "_ResistanceTextBox";
            _ResistanceTextBox.ReadOnly = true;
            _ResistanceTextBox.Size = new Size(183, 22);
            _ResistanceTextBox.TabIndex = 2;
            _ResistanceTextBox.Text = "0.000";
            _ResistanceTextBox.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_ResistanceTextBox, "Measured insulation resistance");
            // 
            // _voltageTextBox
            // 
            _VoltageTextBox.Dock = DockStyle.Bottom;
            _VoltageTextBox.Font = new Font("Microsoft Sans Serif", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _VoltageTextBox.Location = new Point(23, 33);
            _VoltageTextBox.Name = "_voltageTextBox";
            _VoltageTextBox.ReadOnly = true;
            _VoltageTextBox.Size = new Size(183, 22);
            _VoltageTextBox.TabIndex = 2;
            _VoltageTextBox.Text = "0.000";
            _VoltageTextBox.TextAlign = HorizontalAlignment.Center;
            _ToolTip.SetToolTip(_VoltageTextBox, "Measured Voltage");
            // 
            // _ResistanceTextBoxLabel
            // 
            _ResistanceTextBoxLabel.Dock = DockStyle.Bottom;
            _ResistanceTextBoxLabel.Location = new Point(421, 10);
            _ResistanceTextBoxLabel.Name = "_ResistanceTextBoxLabel";
            _ResistanceTextBoxLabel.Size = new Size(183, 20);
            _ResistanceTextBoxLabel.TabIndex = 3;
            _ResistanceTextBoxLabel.Text = "RESISTANCE [Ω]";
            _ResistanceTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _voltageTextBoxLabel
            // 
            _VoltageTextBoxLabel.Dock = DockStyle.Bottom;
            _VoltageTextBoxLabel.Location = new Point(23, 17);
            _VoltageTextBoxLabel.Name = "_voltageTextBoxLabel";
            _VoltageTextBoxLabel.Size = new Size(183, 13);
            _VoltageTextBoxLabel.TabIndex = 3;
            _VoltageTextBoxLabel.Text = "VOLTAGE [V]";
            _VoltageTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _manualGroupBox
            // 
            _ManualGroupBox.Controls.Add(_MeasureControlsLayout);
            _ManualGroupBox.Dock = DockStyle.Top;
            _ManualGroupBox.Location = new Point(15, 511);
            _ManualGroupBox.Name = "_manualGroupBox";
            _ManualGroupBox.Size = new Size(629, 67);
            _ManualGroupBox.TabIndex = 1;
            _ManualGroupBox.TabStop = false;
            _ManualGroupBox.Text = "TAKE A MEASURMENT";
            // 
            // _measureControlsLayout
            // 
            _MeasureControlsLayout.ColumnCount = 3;
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle());
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _MeasureControlsLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20.0f));
            _MeasureControlsLayout.Controls.Add(__MeasureButton, 1, 1);
            _MeasureControlsLayout.Dock = DockStyle.Fill;
            _MeasureControlsLayout.Location = new Point(3, 16);
            _MeasureControlsLayout.Name = "_measureControlsLayout";
            _MeasureControlsLayout.RowCount = 3;
            _MeasureControlsLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 10.0f));
            _MeasureControlsLayout.RowStyles.Add(new RowStyle());
            _MeasureControlsLayout.RowStyles.Add(new RowStyle(SizeType.Absolute, 10.0f));
            _MeasureControlsLayout.Size = new Size(623, 48);
            _MeasureControlsLayout.TabIndex = 3;
            // 
            // _measureButton
            // 
            __MeasureButton.Location = new Point(231, 13);
            __MeasureButton.Name = "__measureButton";
            __MeasureButton.Size = new Size(161, 23);
            __MeasureButton.TabIndex = 0;
            __MeasureButton.Text = "MEASURE RESISTANCE";
            _ToolTip.SetToolTip(__MeasureButton, "Click to measure high potential insulation resistance");
            __MeasureButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyLayout
            // 
            _ApplyLayout.ColumnCount = 3;
            _ApplyLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ApplyLayout.ColumnStyles.Add(new ColumnStyle());
            _ApplyLayout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _ApplyLayout.Controls.Add(__ApplyConfigButton, 1, 0);
            _ApplyLayout.Dock = DockStyle.Top;
            _ApplyLayout.Location = new Point(15, 436);
            _ApplyLayout.Name = "_ApplyLayout";
            _ApplyLayout.RowCount = 1;
            _ApplyLayout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _ApplyLayout.Size = new Size(629, 31);
            _ApplyLayout.TabIndex = 4;
            // 
            // _applyConfigButton
            // 
            __ApplyConfigButton.Location = new Point(277, 3);
            __ApplyConfigButton.Name = "__applyConfigButton";
            __ApplyConfigButton.Size = new Size(75, 23);
            __ApplyConfigButton.TabIndex = 8;
            __ApplyConfigButton.Text = "APPLY";
            _ToolTip.SetToolTip(__ApplyConfigButton, "Click to apply the meter configuration");
            __ApplyConfigButton.UseVisualStyleBackColor = true;
            // 
            // _TriggerToolStrip
            // 
            _TriggerToolStrip.Items.AddRange(new ToolStripItem[] { __AwaitTriggerToolStripButton, _WaitHourglassLabel, _TriggersDropDownButton, __AssertTriggerToolStripButton, _TriggerActionToolStripLabel });
            _TriggerToolStrip.Location = new Point(12, 619);
            _TriggerToolStrip.Name = "_TriggerToolStrip";
            _TriggerToolStrip.Size = new Size(635, 25);
            _TriggerToolStrip.Stretch = true;
            _TriggerToolStrip.TabIndex = 5;
            _TriggerToolStrip.Text = "Trigger Tool Strip";
            // 
            // _AwaitTriggerToolStripButton
            // 
            __AwaitTriggerToolStripButton.CheckOnClick = true;
            __AwaitTriggerToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AwaitTriggerToolStripButton.Image = (Image)resources.GetObject("_AwaitTriggerToolStripButton.Image");
            __AwaitTriggerToolStripButton.ImageTransparentColor = Color.Magenta;
            __AwaitTriggerToolStripButton.Name = "__AwaitTriggerToolStripButton";
            __AwaitTriggerToolStripButton.Overflow = ToolStripItemOverflow.Never;
            __AwaitTriggerToolStripButton.Size = new Size(114, 22);
            __AwaitTriggerToolStripButton.Text = "WAIT FOR TRIGGER";
            __AwaitTriggerToolStripButton.TextImageRelation = TextImageRelation.ImageAboveText;
            __AwaitTriggerToolStripButton.ToolTipText = "Depress to wait for trigger or release to abort.";
            // 
            // _WaitHourglassLabel
            // 
            _WaitHourglassLabel.Name = "_WaitHourglassLabel";
            _WaitHourglassLabel.Size = new Size(20, 22);
            _WaitHourglassLabel.Text = "[-]";
            _WaitHourglassLabel.ToolTipText = "Waiting for trigger";
            // 
            // _TriggersDropDownButton
            // 
            _TriggersDropDownButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _TriggersDropDownButton.DropDownItems.AddRange(new ToolStripItem[] { __ManualTriggerMenuItem, __SotTriggerMenuItem });
            _TriggersDropDownButton.Image = (Image)resources.GetObject("_TriggersDropDownButton.Image");
            _TriggersDropDownButton.ImageTransparentColor = Color.Magenta;
            _TriggersDropDownButton.Margin = new Padding(6, 1, 0, 2);
            _TriggersDropDownButton.Name = "_TriggersDropDownButton";
            _TriggersDropDownButton.Size = new Size(62, 22);
            _TriggersDropDownButton.Text = "Triggers";
            _TriggersDropDownButton.ToolTipText = "Click to select trigger option";
            // 
            // _ManualTriggerMenuItem
            // 
            __ManualTriggerMenuItem.CheckOnClick = true;
            __ManualTriggerMenuItem.Name = "__ManualTriggerMenuItem";
            __ManualTriggerMenuItem.Size = new Size(137, 22);
            __ManualTriggerMenuItem.Text = "Manual";
            // 
            // _SotTriggerMenuItem
            // 
            __SotTriggerMenuItem.Checked = true;
            __SotTriggerMenuItem.CheckOnClick = true;
            __SotTriggerMenuItem.CheckState = CheckState.Checked;
            __SotTriggerMenuItem.Name = "__SotTriggerMenuItem";
            __SotTriggerMenuItem.Size = new Size(137, 22);
            __SotTriggerMenuItem.Text = "SOT (Line 6)";
            // 
            // _AssertTriggerToolStripButton
            // 
            __AssertTriggerToolStripButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __AssertTriggerToolStripButton.Image = (Image)resources.GetObject("_AssertTriggerToolStripButton.Image");
            __AssertTriggerToolStripButton.ImageTransparentColor = Color.Magenta;
            __AssertTriggerToolStripButton.Margin = new Padding(6, 1, 0, 2);
            __AssertTriggerToolStripButton.Name = "__AssertTriggerToolStripButton";
            __AssertTriggerToolStripButton.Size = new Size(38, 22);
            __AssertTriggerToolStripButton.Text = "*TRG";
            // 
            // _TriggerActionToolStripLabel
            // 
            _TriggerActionToolStripLabel.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _TriggerActionToolStripLabel.Name = "_TriggerActionToolStripLabel";
            _TriggerActionToolStripLabel.Size = new Size(0, 22);
            _TriggerActionToolStripLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _errorProvider
            // 
            _ErrorProvider.ContainerControl = this;
            // 
            // _meterTimer
            // 
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new ToolStripItem[] { _StatusLabel });
            _StatusStrip.Location = new Point(0, 661);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Size = new Size(660, 22);
            _StatusStrip.TabIndex = 1;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusLabel
            // 
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Size = new Size(54, 17);
            _StatusLabel.Text = "<status>";
            // 
            // Console
            // 
            AutoScaleDimensions = new SizeF(6.0f, 13.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(660, 683);
            Controls.Add(_StatusStrip);
            Controls.Add(_MainLayout);
            Name = "Console";
            Text = "HIPOT Console";
            _MainLayout.ResumeLayout(false);
            _MainLayout.PerformLayout();
            _ConfigurationLayout.ResumeLayout(false);
            _ConfigGroupBox.ResumeLayout(false);
            _ConfigGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_CurrentLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_VoltageLevelNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceLowLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_DwellTimeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).EndInit();
            _BinningGroupBox.ResumeLayout(false);
            _BinningGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_FailBitPatternNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_PassBitPatternNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ContactCheckBitPatternNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_EotStrobeDurationNumeric).EndInit();
            _ConnectGroupBox.ResumeLayout(false);
            _ConnectGroupBox.PerformLayout();
            _OutcomeLayout.ResumeLayout(false);
            _OutcomeLayout.PerformLayout();
            _ManualGroupBox.ResumeLayout(false);
            _MeasureControlsLayout.ResumeLayout(false);
            _ApplyLayout.ResumeLayout(false);
            _TriggerToolStrip.ResumeLayout(false);
            _TriggerToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ErrorProvider).EndInit();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            FormClosing += new FormClosingEventHandler(ConsolePanel_FormClosing);
            Load += new EventHandler(ConsolePanel_Load);
            Shown += new EventHandler(ConsolePanel_Shown);
            ResumeLayout(false);
            PerformLayout();
        }

        private TableLayoutPanel _MainLayout;
        private GroupBox _ConnectGroupBox;

        /// <summary> The with events control. </summary>
        private Label _ResourceNameTextBoxLabel;

        /// <summary> The with events control. </summary>
        private TextBox _IdentityTextBox;

        /// <summary> The with events control. </summary>
        private TextBox _ResourceNameTextBox;

        /// <summary> The with events control. </summary>
        private CheckBox __ConnectToggle;

        private CheckBox _ConnectToggle
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConnectToggle;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConnectToggle != null)
                {
                    __ConnectToggle.CheckedChanged -= ConnectToggle_CheckedChanged;
                }

                __ConnectToggle = value;
                if (__ConnectToggle != null)
                {
                    __ConnectToggle.CheckedChanged += ConnectToggle_CheckedChanged;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private ToolTip _ToolTip;
        private TableLayoutPanel _ConfigurationLayout;
        private GroupBox _ConfigGroupBox;

        /// <summary> The with events control. </summary>
        private NumericUpDown _CurrentLimitNumeric;

        /// <summary> The with events control. </summary>
        private Label _VoltageLevelNumericLabel;

        /// <summary> The with events control. </summary>
        private NumericUpDown _VoltageLevelNumeric;

        /// <summary> The with events control. </summary>
        private Label _ResistanceLowLimitNumericLabel;

        /// <summary> The with events control. </summary>
        private NumericUpDown _DwellTimeNumeric;

        /// <summary> The with events control. </summary>
        private Label _DwellTimeNumericLabel;

        /// <summary> The with events control. </summary>
        private NumericUpDown _ApertureNumeric;

        /// <summary> The with events control. </summary>
        private Label _ApertureNumericLabel;

        /// <summary> The with events control. </summary>
        private NumericUpDown _ResistanceLowLimitNumeric;

        /// <summary> The with events control. </summary>
        private Button __ApplyConfigButton;

        private Button _ApplyConfigButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyConfigButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyConfigButton != null)
                {
                    __ApplyConfigButton.Click -= ApplyConfigButton_Click;
                }

                __ApplyConfigButton = value;
                if (__ApplyConfigButton != null)
                {
                    __ApplyConfigButton.Click += ApplyConfigButton_Click;
                }
            }
        }

        private TableLayoutPanel _OutcomeLayout;

        /// <summary> The with events control. </summary>
        private TextBox _CurrentTextBox;

        /// <summary> The with events control. </summary>
        private Label _CurrentTextBoxLabel;

        /// <summary> The with events control. </summary>
        private TextBox _ResistanceTextBox;

        /// <summary> The with events control. </summary>
        private TextBox _VoltageTextBox;

        /// <summary> The with events control. </summary>
        private Label _ResistanceTextBoxLabel;

        /// <summary> The with events control. </summary>
        private Label _VoltageTextBoxLabel;
        private ErrorProvider _ErrorProvider;
        private GroupBox _ManualGroupBox;
        private TableLayoutPanel _MeasureControlsLayout;

        /// <summary> The with events control. </summary>
        private Button __MeasureButton;

        private Button _MeasureButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click -= MeasureButton_Click;
                }

                __MeasureButton = value;
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click += MeasureButton_Click;
                }
            }
        }

        private Timer __MeterTimer;

        private Timer _MeterTimer
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeterTimer;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeterTimer != null)
                {
                    __MeterTimer.Tick -= MeterTimer_Tick;
                }

                __MeterTimer = value;
                if (__MeterTimer != null)
                {
                    __MeterTimer.Tick += MeterTimer_Tick;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private Label _CurrentLimitNumericLabel;

        /// <summary> The with events control. </summary>
        private Label _ResistanceRangeNumericLabel;

        /// <summary> The with events control. </summary>
        private NumericUpDown _ResistanceRangeNumeric;
        private StatusStrip _StatusStrip;
        private ToolStripStatusLabel _StatusLabel;
        private GroupBox _BinningGroupBox;

        /// <summary> The with events control. </summary>
        private Label _EotStrobeDurationNumericLabel;

        /// <summary> The with events control. </summary>
        private NumericUpDown _EotStrobeDurationNumeric;
        private TableLayoutPanel _ApplyLayout;

        /// <summary> The with events control. </summary>
        private Label _ContactCheckBitPatternNumericLabel;

        /// <summary> The with events control. </summary>
        private CheckBox _ContactCheckToggle;

        /// <summary> The with events control. </summary>
        private NumericUpDown _ContactCheckBitPatternNumeric;

        /// <summary> The with events control. </summary>
        private NumericUpDown _FailBitPatternNumeric;

        /// <summary> The with events control. </summary>
        private NumericUpDown _PassBitPatternNumeric;

        /// <summary> The with events control. </summary>
        private Label _FailBitPatternNumericLabel;

        /// <summary> The with events control. </summary>
        private Label _PassBitPatternNumericLabel;

        /// <summary> The with events control. </summary>
        private Label _ContactCheckSupportLabel;
        private ToolStripButton __AwaitTriggerToolStripButton;

        private ToolStripButton _AwaitTriggerToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AwaitTriggerToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AwaitTriggerToolStripButton != null)
                {
                    __AwaitTriggerToolStripButton.CheckedChanged -= AwaitTriggerToolStripButton_CheckedChanged;
                }

                __AwaitTriggerToolStripButton = value;
                if (__AwaitTriggerToolStripButton != null)
                {
                    __AwaitTriggerToolStripButton.CheckedChanged += AwaitTriggerToolStripButton_CheckedChanged;
                }
            }
        }

        private ToolStripLabel _WaitHourglassLabel;
        private ToolStripDropDownButton _TriggersDropDownButton;
        private ToolStripMenuItem __ManualTriggerMenuItem;

        private ToolStripMenuItem _ManualTriggerMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ManualTriggerMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ManualTriggerMenuItem != null)
                {
                    __ManualTriggerMenuItem.CheckStateChanged -= ManualTriggerMenuItem_CheckStateChanged;
                }

                __ManualTriggerMenuItem = value;
                if (__ManualTriggerMenuItem != null)
                {
                    __ManualTriggerMenuItem.CheckStateChanged += ManualTriggerMenuItem_CheckStateChanged;
                }
            }
        }

        private ToolStripMenuItem __SotTriggerMenuItem;

        private ToolStripMenuItem _SotTriggerMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SotTriggerMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SotTriggerMenuItem != null)
                {
                    __SotTriggerMenuItem.CheckStateChanged -= SotTriggerMenuItem_CheckStateChanged;
                }

                __SotTriggerMenuItem = value;
                if (__SotTriggerMenuItem != null)
                {
                    __SotTriggerMenuItem.CheckStateChanged += SotTriggerMenuItem_CheckStateChanged;
                }
            }
        }

        private ToolStripButton __AssertTriggerToolStripButton;

        private ToolStripButton _AssertTriggerToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AssertTriggerToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AssertTriggerToolStripButton != null)
                {
                    __AssertTriggerToolStripButton.Click -= AssertTriggerToolStripButton_Click;
                }

                __AssertTriggerToolStripButton = value;
                if (__AssertTriggerToolStripButton != null)
                {
                    __AssertTriggerToolStripButton.Click += AssertTriggerToolStripButton_Click;
                }
            }
        }

        private ToolStripLabel _TriggerActionToolStripLabel;
        private ToolStrip _TriggerToolStrip;
    }
}