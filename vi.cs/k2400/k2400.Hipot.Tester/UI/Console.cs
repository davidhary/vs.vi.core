using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace isr.K2400.Hipot.Tester
{

    /// <summary> Thermal Transient Meter Tester Console. </summary>
    /// <remarks>
    /// (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2009-09-20, 2.3.3915. Created </para>
    /// </remarks>
    public partial class Console : Form
    {

        /// <summary>
        /// This constructor is public to allow using this form as a startup form for the project.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public Console() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializingComponents = true;
            this.InitializeComponent();
            this._ResourceNameTextBox.DataBindings.Add( new Binding( "Text", Driver.My.MySettings.Default, "ResourceName", true, DataSourceUpdateMode.OnValidation ) );
            this._DwellTimeNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "DwellTime", true, DataSourceUpdateMode.OnValidation ) );
            this._ApertureNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "Aperture", true, DataSourceUpdateMode.OnValidation ) );
            this._VoltageLevelNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "VoltageLevel", true, DataSourceUpdateMode.OnValidation ) );
            this._CurrentLimitNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "CurrentLimit", true, DataSourceUpdateMode.OnValidation ) );
            this._ResistanceLowLimitNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "ResistanceLowLimit", true, DataSourceUpdateMode.OnValidation ) );
            this._ResistanceRangeNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "ResistanceRange", true, DataSourceUpdateMode.OnValidation ) );
            this._EotStrobeDurationNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "EotStrobeDuration", true, DataSourceUpdateMode.OnValidation ) );
            this._PassBitPatternNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "PassBitPattern", true, DataSourceUpdateMode.OnValidation ) );
            this._FailBitPatternNumeric.DataBindings.Add( new Binding( "Value", Driver.My.MySettings.Default, "FailBitPattern", true, DataSourceUpdateMode.OnValidation ) );
            this._ContactCheckToggle.DataBindings.Add( new Binding( "Checked", Driver.My.MySettings.Default, "ContactCheckEnabled", true, DataSourceUpdateMode.OnValidation ) );
            this.InitializingComponents = false;
            this.Meter = new Driver.Device();
            this.__ConnectToggle.Name = "_ConnectToggle";
            this.__MeasureButton.Name = "_MeasureButton";
            this.__ApplyConfigButton.Name = "_ApplyConfigButton";
            this.__AwaitTriggerToolStripButton.Name = "_AwaitTriggerToolStripButton";
            this.__ManualTriggerMenuItem.Name = "_ManualTriggerMenuItem";
            this.__SotTriggerMenuItem.Name = "_SotTriggerMenuItem";
            this.__AssertTriggerToolStripButton.Name = "_AssertTriggerToolStripButton";
        }

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Gets or sets the initializing components. </summary>
        /// <value> The initializing components. </value>
        private bool InitializingComponents { get; set; }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
        ///                          resources; <see langword="false" /> to release only unmanaged
        ///                          resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                    }

                    if ( this.MeterInternal is object )
                    {
                        this.MeterInternal.Dispose();
                        this.MeterInternal = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion

        #region " EVENT HANDLERS:  FORM "

        /// <summary> Sets the caption for this form. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        internal void RefreshCaption()
        {
            this.Text = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0} {1}.{2}.{3}: CONSOLE", My.MyProject.Application.Info.Title, My.MyProject.Application.Info.Version.Major, My.MyProject.Application.Info.Version.Minor, My.MyProject.Application.Info.Version.Build );
        }

        /// <summary> Terminates objects before closing the form. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Form closing event information. </param>
        private void ConsolePanel_FormClosing( object sender, FormClosingEventArgs e )
        {
            this.Cursor = Cursors.WaitCursor;
            try
            {

                // dispose of the meter.
                if ( this.MeterInternal is object )
                {
                    this.MeterInternal.Dispose();
                    this.MeterInternal = null;
                }

                // flush the log.
                My.MyProject.Application.Log.DefaultFileLogWriter.Flush();

                // wait for timer to terminate all is actions
                Driver.My.MyLibrary.DoEvents();
                Driver.My.MyLibrary.DoEventsTaskDelay( TimeSpan.FromMilliseconds( 400d ) );

                // allow all events requiring the panel to execute on their thread.
                // this allows all timer events that were in progress to be consummated before closing the form.
                // this does not prevent timer exceptions in design mode.
                for ( int i = 1; i <= 1000; i++ )
                    Application.DoEvents();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Loads this module. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConsolePanel_Load( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = Cursors.AppStarting;

                // Center the form
                this.CenterToScreen();
            }
            catch
            {
                throw;
            }
            finally
            {

                // turn on the default pointer
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Displays final values after the form is shown. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ConsolePanel_Shown( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = Cursors.AppStarting;
                Application.DoEvents();
                this.RefreshCaption();
                My.MyProject.Application.Log.WriteEntry( this.Text + " shown", TraceEventType.Verbose );
                this.OnConnectionChanged();
                _ = this.DisplayStatus( "Logging to {0}", My.MyProject.Application.Log.DefaultFileLogWriter.FullLogFileName );
            }
            catch
            {
                throw;
            }
            finally
            {

                // turn on the default pointer
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONNECT "

        /// <summary> Updates the availability of the controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void OnMeasurementStatusChanged()
        {
            this._AwaitTriggerToolStripButton.Enabled = this.Meter.IsConnected;
            this._AssertTriggerToolStripButton.Enabled = this.Meter.IsConnected && this._MeterTimer.Enabled && this._ManualTriggerMenuItem.Checked;
            this._ApplyConfigButton.Enabled = this.Meter.IsConnected && !this._MeterTimer.Enabled;
            this._MeasureButton.Enabled = this.Meter.IsConnected && !this._MeterTimer.Enabled;
        }

        /// <summary> Updates the connection related controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void OnConnectionChanged()
        {
            if ( this.Meter.IsConnected )
            {
                this._ContactCheckBitPatternNumeric.Enabled = this.Meter.ContactCheckSupported;
                this._ContactCheckToggle.Enabled = this.Meter.ContactCheckSupported;
                this._ContactCheckSupportLabel.Text = this.Meter.ContactCheckSupported ? "SUPPORTS CONTACT CHECK" : "CONTACT CHECK NOT SUPPORTED";
            }
            else
            {
                this._MeterTimer.Enabled = false;
            }

            this.OnMeasurementStatusChanged();
        }

        /// <summary>
        /// Silently toggles the control checked value. Allows using the enabled state of the control to
        /// control addressing control events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="control"> The control. </param>
        /// <param name="checked"> if set to <c>True</c> [checked]. </param>
        private static void SilentToggle( CheckBox control, bool @checked )
        {
            bool wasEnabled = control.Enabled;
            control.Enabled = false;
            control.Checked = @checked;
            control.Enabled = wasEnabled;
        }

        /// <summary> Displays the status. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> The format. </param>
        /// <param name="args">   The args. </param>
        /// <returns> A String. </returns>
        private string DisplayStatus( string format, params object[] args )
        {
            string status = string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args );
            this._StatusLabel.Text = status;
            return status;
        }

        /// <summary>
        /// Connects or disconnected from the instrument using the
        /// <see cref="_ResourceNameTextBox">resource name</see>
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConnectToggle_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._ConnectToggle.Enabled )
            {
                this._ErrorProvider.SetError( this._ConnectToggle, "" );
                this._ErrorProvider.SetError( this._ApplyConfigButton, "" );
                this._ErrorProvider.SetError( this._MeasureButton, "" );
                this._ErrorProvider.SetError( this._TriggerToolStrip, "" );
                string resourceName = this._ResourceNameTextBox.Text;
                if ( this._ConnectToggle.Checked )
                {
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        if ( string.IsNullOrWhiteSpace( resourceName ) )
                        {
                            My.MyProject.Application.Log.WriteEntry( "Empty resource name", TraceEventType.Warning );
                            this._StatusLabel.Text = "EMPTY RESOURCE NAME";
                            SilentToggle( this._ConnectToggle, false );
                            return;
                        }

                        My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Connecting to {0} ", resourceName ), TraceEventType.Verbose );
                        this._IdentityTextBox.Text = "<connecting>";
                        if ( this.Meter.Connect( resourceName ) )
                        {
                            My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Connected to {0} ", resourceName ), TraceEventType.Verbose );

                            // display the device information.
                            this._IdentityTextBox.Text = this.Meter.Identity;
                        }
                        else
                        {
                            this._ErrorProvider.SetError( this._ConnectToggle, "Connection failed" );
                            this._IdentityTextBox.Text = "<failed connecting>";
                            My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Failed connecting to {0} ", resourceName ), TraceEventType.Error );
                            SilentToggle( this._ConnectToggle, false );
                        }
                    }
                    catch ( Exception ex )
                    {
                        this._ErrorProvider.SetError( this._ConnectToggle, "Connection failed" );
                        this._IdentityTextBox.Text = "<failed connecting>";
                        My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Failed connecting to {0} ", resourceName ), TraceEventType.Error );
                        My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "failed connecting" );
                        SilentToggle( this._ConnectToggle, false );
                    }
                    finally
                    {
                        this.Cursor = Cursors.Default;
                        this.OnConnectionChanged();
                    }
                }
                else
                {
                    try
                    {
                        this.Cursor = Cursors.WaitCursor;
                        My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Disconnecting from {0} ", resourceName ), TraceEventType.Verbose );
                        this._IdentityTextBox.Text = "<disconnecting>";

                        // abort any ongoing triggered measurements. Otherwise, the instrument will hang.
                        this.AbortMeasurement();
                        if ( this.Meter.Disconnect() )
                        {
                            this._IdentityTextBox.Text = "<disconnected>";
                            My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Disconnected from {0} ", resourceName ), TraceEventType.Verbose );
                        }
                        else
                        {
                            this._ErrorProvider.SetError( this._ConnectToggle, "Disconnection failed" );
                            this._IdentityTextBox.Text = "<failed disconnecting>";
                            My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Failed disconnecting from {0} ", resourceName ), TraceEventType.Warning );
                        }
                    }
                    catch ( Exception ex )
                    {
                        this._ErrorProvider.SetError( this._ConnectToggle, "Disconnection failed" );
                        this._IdentityTextBox.Text = "<failed disconnecting>";
                        My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "failed connecting" );
                    }
                    finally
                    {
                        this.Cursor = Cursors.Default;
                        this.OnConnectionChanged();
                    }
                }
            }

            this._ConnectToggle.Text = this._ConnectToggle.Checked ? "DISCONNECT" : "CONNECT";
        }

        #endregion

        #region " CONFIGURE "

        /// <summary>
        /// Read the current settings in the device -- this does not send the settings to the instrument.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ReadSettings()
        {
            My.MyProject.Application.Log.WriteEntry( "Storing configuration settings in the device.", TraceEventType.Verbose );
            this.Meter.ReadSettings();
        }

        /// <summary>
        /// Updates the current settings from the device -- this does not read the settings from the
        /// instrument.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void SaveSettings()
        {
            My.MyProject.Application.Log.WriteEntry( "Updating configuration settings from the device.", TraceEventType.Verbose );
            this.Meter.UpdateSettings();
            this.Meter.SaveSettings();
        }

        /// <summary> Saves the configuration settings and sends them to the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ApplyConfigButton_Click( object sender, EventArgs e )
        {
            this._ErrorProvider.SetError( this._TriggerToolStrip, "" );
            this._ErrorProvider.SetError( this._MeasureButton, "" );
            this._ErrorProvider.SetError( this._ApplyConfigButton, "" );
            this.ReadSettings();
            if ( this.Meter.IsConnected )
            {
                My.MyProject.Application.Log.WriteEntry( "Sending meter configuration settings to the instrument", TraceEventType.Verbose );
                if ( this.Meter.Configure() )
                {
                    My.MyProject.Application.Log.WriteEntry( "meter measurement configured successfully.", TraceEventType.Verbose );
                    this.SaveSettings();
                }
                else
                {
                    this._ErrorProvider.SetError( this._ApplyConfigButton, "Failed sending meter configuration settings to the instrument" );
                    My.MyProject.Application.Log.WriteEntry( "Failed sending meter configuration settings to the instrument", TraceEventType.Warning );
                }
            }
            else
            {
                this._ErrorProvider.SetError( this._ApplyConfigButton, "Instrument not connected" );
                My.MyProject.Application.Log.WriteEntry( "Instrument not connected", TraceEventType.Warning );
            }
        }

        #endregion

        #region " MEASURE "

        /// <summary> The device access locker. </summary>
        private readonly object _DeviceAccessLocker = new();

        /// <summary> Displays the results. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ShowResults()
        {
            this._VoltageTextBox.Text = this.Meter.VoltageReading;
            this._CurrentTextBox.Text = this.Meter.CurrentReading;
            this._ResistanceTextBox.Text = this.Meter.ResistanceReading;
            if ( (this.Meter.Outcome & Driver.MeasurementOutcomes.PartFailed) != 0 )
            {
                this._ErrorProvider.SetError( this._MeasureButton, "Resistance out of range" );
            }
            else if ( (this.Meter.Outcome & Driver.MeasurementOutcomes.MeasurementFailed) != 0 )
            {
                this._ErrorProvider.SetError( this._MeasureButton, "Measurement failed" );
            }
            else if ( (this.Meter.Outcome & Driver.MeasurementOutcomes.MeasurementNotMade) != 0 )
            {
                this._ErrorProvider.SetError( this._MeasureButton, "Measurement not made" );
            }
            else if ( (this.Meter.Outcome & Driver.MeasurementOutcomes.FailedContactCheck) != 0 )
            {
                this._ErrorProvider.SetError( this._MeasureButton, "Failed contact check" );
            }
        }

        /// <summary> Measures this object. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void Measure()
        {
            lock ( this._DeviceAccessLocker )
            {
                this._VoltageTextBox.Text = string.Empty;
                this._CurrentTextBox.Text = string.Empty;
                this._ResistanceTextBox.Text = string.Empty;
                this._ErrorProvider.SetError( this._MeasureButton, "" );
                this._ErrorProvider.SetIconPadding( this._MeasureButton, -10 );
                if ( this.Meter.Measure() )
                {
                    this.ShowResults();
                }
                else
                {
                    this._ErrorProvider.SetError( this._MeasureButton, "Failed Measuring Resistance" );
                    My.MyProject.Application.Log.WriteEntry( "Failed Measuring Resistance", TraceEventType.Warning );
                }
            }
        }

        /// <summary> Measures the thermal transient voltage. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureButton_Click( object sender, EventArgs e )
        {
            Control ctrl = sender as Control;
            try
            {
                this._ErrorProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                My.MyProject.Application.Log.WriteEntry( this.DisplayStatus( "Starting a measurements", TraceEventType.Verbose ) );
                this.Measure();
            }
            catch ( Exception ex )
            {
                this._ErrorProvider.SetError( ctrl, "Measurement failed" );
                My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "measurement failed" );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Aborts the measurement cycle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AbortMeasurementThis()
        {
            this._MeterTimer.Enabled = false;
            _ = this.Meter.AbortMeasurement();
            this._AwaitTriggerToolStripButton.Enabled = false;
            this._AwaitTriggerToolStripButton.Checked = false;
            this.OnMeasurementStatusChanged();
            _ = this.Meter.Configure();
        }

        /// <summary> Aborts the measurement cycle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AbortMeasurement()
        {
            lock ( this._DeviceAccessLocker )
                this.AbortMeasurementThis();
        }

        /// <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AssertTriggerToolStripButton_Click( object sender, EventArgs e )
        {
            this._ErrorProvider.SetError( this._TriggerToolStrip, "" );
            try
            {
                if ( this._ManualTriggerMenuItem.Checked && this._MeterTimer.Enabled )
                {
                    this.AssertTrigger();
                }
                else if ( !this._ManualTriggerMenuItem.Checked )
                {
                    this._ErrorProvider.SetError( this._TriggerToolStrip, "Manual trigger ignored -- not manual mode" );
                }
                else if ( !this._MeterTimer.Enabled )
                {
                    this._ErrorProvider.SetError( this._TriggerToolStrip, "Manual trigger ignored -- triggering is not active" );
                }
            }
            catch ( Exception ex )
            {
                this._ErrorProvider.SetError( this._TriggerToolStrip, "Exception occurred aborting" );
                My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "Exception occurred aborting." );
            }
        }

        /// <summary> Outputs a trigger to make a measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AssertTrigger()
        {
            lock ( this._DeviceAccessLocker )
            {
                if ( !this.Meter.TriggerMeasurement() )
                {
                    My.MyProject.Application.Log.WriteEntry( "Failed triggering instrument", TraceEventType.Warning );
                }
            }
        }

        /// <summary> True if abort requested. </summary>
        private bool _AbortRequested;

        /// <summary> Turns on or aborts waiting for trigger. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="enabled"> True to enable, false to disable. </param>
        /// <param name="checked"> if set to <c>True</c> [checked]. </param>
        private void ToggleAwaitTrigger( bool enabled, bool @checked )
        {
            this._ErrorProvider.SetIconPadding( this._TriggerToolStrip, -10 );
            this._ErrorProvider.SetError( this._TriggerToolStrip, "" );
            if ( enabled )
            {
                this._AbortRequested = !this._AwaitTriggerToolStripButton.Checked;
                if ( @checked )
                {
                    lock ( this._DeviceAccessLocker )
                    {
                        My.MyProject.Application.Log.WriteEntry( "Preparing instrument for waiting for trigger", TraceEventType.Verbose );
                        this._TriggerActionToolStripLabel.Text = "PREPARING";
                        if ( this.Meter.PrepareForTrigger( this._ManualTriggerMenuItem.Checked ) )
                        {
                            this._TriggerActionToolStripLabel.Text = "WAITING FOR TRIGGERED MEASUREMENT";
                            My.MyProject.Application.Log.WriteEntry( "Monitoring instrument for measurements.", TraceEventType.Verbose );
                            this._MeterTimer.Interval = 100;
                            this._MeterTimer.Enabled = true;
                            this.OnMeasurementStatusChanged();
                        }
                        else
                        {
                            this._TriggerActionToolStripLabel.Text = "FAILED PREPARING";
                            this._ErrorProvider.SetError( this._TriggerToolStrip, "Failed preparing instrument for waiting for trigger" );
                            My.MyProject.Application.Log.WriteEntry( "Failed preparing instrument for waiting for trigger", TraceEventType.Warning );
                            this.AbortMeasurementThis();
                        }
                    }
                }
                else
                {
                    this._TriggerActionToolStripLabel.Text = "ABORT REQUESTED";
                    this.AbortMeasurementThis();
                }
            }

            this._AwaitTriggerToolStripButton.Text = this._AwaitTriggerToolStripButton.Checked ? "ABORT TRIGGERING" : "WAIT FOR A TRIGGER";
        }

        /// <summary> Turns on or aborts waiting for trigger. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AwaitTriggerToolStripButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.ToggleAwaitTrigger( this._AwaitTriggerToolStripButton.Enabled, this._AwaitTriggerToolStripButton.Checked );
        }

        /// <summary> Manual trigger menu item check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ManualTriggerMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this._SotTriggerMenuItem.Checked ^ this._ManualTriggerMenuItem.Checked )
            {
                this._SotTriggerMenuItem.Checked = !this._ManualTriggerMenuItem.Checked;
            }
        }

        /// <summary> Sot trigger menu item check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SotTriggerMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this._SotTriggerMenuItem.Checked ^ this._ManualTriggerMenuItem.Checked )
            {
                this._ManualTriggerMenuItem.Checked = !this._SotTriggerMenuItem.Checked;
            }
        }

        /// <summary> The status bar. </summary>
        private string _StatusBar = "|";

        /// <summary>
        /// Monitors measurements. Once found, reads and displays and restarts the cycle.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeterTimer_Tick( object sender, EventArgs e )
        {
            if ( this._StatusBar == "|" )
            {
                this._StatusBar = "/";
            }
            else if ( this._StatusBar == "/" )
            {
                this._StatusBar = "-";
            }
            else if ( this._StatusBar == "-" )
            {
                this._StatusBar = @"\";
            }
            else if ( this._StatusBar == @"\" )
            {
                this._StatusBar = "|";
            }

            lock ( this._DeviceAccessLocker )
            {
                try
                {
                    this._MeterTimer.Enabled = false;
                    if ( this._AbortRequested )
                    {
                        this.AbortMeasurementThis();
                        return;
                    }

                    if ( this.Meter.IsMeasurementCompleted() )
                    {
                        this._TriggerActionToolStripLabel.Text = "READING...";
                        if ( this.Meter.ReadMeasurements() )
                        {
                            this._TriggerActionToolStripLabel.Text = "DATA AVAILABLE...";
                            this.ShowResults();
                        }
                        else
                        {
                            this._VoltageTextBox.Text = string.Empty;
                            this._ResistanceTextBox.Text = string.Empty;
                            this._CurrentTextBox.Text = string.Empty;
                            this._TriggerActionToolStripLabel.Text = "FAILED READING...";
                        }

                        this._TriggerActionToolStripLabel.Text = "PREPARING...";
                        if ( this.Meter.PrepareForTrigger( this._ManualTriggerMenuItem.Checked ) )
                        {
                            this._TriggerActionToolStripLabel.Text = "WAITING FOR TRIGGRED MEASUREMENT...";
                            My.MyProject.Application.Log.WriteEntry( "Monitoring instrument for measurements.", TraceEventType.Verbose );
                        }
                        else
                        {
                            this._TriggerActionToolStripLabel.Text = "FAILED PREPARING...";
                            this._ErrorProvider.SetError( this._TriggerToolStrip, "Failed preparing instrument for waiting for trigger" );
                            My.MyProject.Application.Log.WriteEntry( "Failed preparing instrument for waiting for trigger", TraceEventType.Warning );
                        }
                    }
                    else
                    {
                        this._TriggerActionToolStripLabel.Text = "WAITING FOR TRIGGERED MEASUREMENT. " + this._StatusBar;
                    }

                    this._MeterTimer.Enabled = !this._AbortRequested;
                }
                catch ( Exception ex )
                {
                    this._ErrorProvider.SetError( this._TriggerToolStrip, "Exception occurred monitoring instrument for data" );
                    My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "Exception occurred monitoring instrument for data." );
                    this._AwaitTriggerToolStripButton.Checked = false;
                }
            }
        }

        #endregion

        #region " HIPOT METER "

        private Driver.IDevice _MeterInternal;

        private Driver.IDevice MeterInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MeterInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MeterInternal != null )
                {

                    this._MeterInternal.ExceptionAvailable -= this.Meter_ExceptionAvailable;

                    this._MeterInternal.MessageAvailable -= this.Meter_MessageAvailable;
                }

                this._MeterInternal = value;
                if ( this._MeterInternal != null )
                {
                    this._MeterInternal.ExceptionAvailable += this.Meter_ExceptionAvailable;
                    this._MeterInternal.MessageAvailable += this.Meter_MessageAvailable;
                }
            }
        }

        /// <summary> Gets or sets reference to the thermal transient meter device. </summary>
        /// <value> The meter. </value>
        private Driver.IDevice Meter
        {
            get => this.MeterInternal;

            set => this.MeterInternal = value;
        }

        /// <summary> Meter exception available. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Thread exception event information. </param>
        private void Meter_ExceptionAvailable( object sender, System.Threading.ThreadExceptionEventArgs e )
        {
            My.MyProject.Application.Log.WriteException( e.Exception, TraceEventType.Error, "Meter exception occurred" );
        }

        /// <summary> Meter message available. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Message event information. </param>
        private void Meter_MessageAvailable( object sender, Driver.MessageEventArgs e )
        {
            My.MyProject.Application.Log.WriteEntry( e.Details, e.TraceLevel );
        }

        #endregion

    }
}
