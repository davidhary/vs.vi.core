﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.K2400.Hipot.Tester.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public Console m_Console;

            public Console Console
            {
                [DebuggerHidden]
                get
                {
                    m_Console = Create__Instance__(m_Console);
                    return m_Console;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Console))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Console);
                }
            }
        }
    }
}