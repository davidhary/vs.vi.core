﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K2400 Source Meter VI Tests" )]
[assembly: AssemblyDescription( "K2400 Source Meter Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K2400.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
