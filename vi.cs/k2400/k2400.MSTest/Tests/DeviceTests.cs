using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2400.MSTest
{

    /// <summary>
    /// This is a test class for DeviceTest and is intended to contain all DeviceTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [TestClass()]
    [TestCategory( "k2400" )]
    public class DeviceTests
    {

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Select resource name. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> A String. </returns>
        internal string SelectResourceName( Pith.HardwareInterfaceType interfaceType )
        {
            switch ( interfaceType )
            {
                case Pith.HardwareInterfaceType.Gpib:
                    {
                        return "GPIB0::24::INSTR";
                    }

                case Pith.HardwareInterfaceType.Tcpip:
                    {
                        return "TCPIP0::A-N5767A-K4381";
                    }

                case Pith.HardwareInterfaceType.Usb:
                    {
                        return "USB0::0x0957::0x0807::N5767A-US11K4381H::0::INSTR";
                    }

                default:
                    {
                        return "GPIB0::24::INSTR";
                    }
            }
        }

        /// <summary> (Unit Test Method) session should open and close. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionShouldOpenAndClose()
        {
            var usingInterfaceType = Pith.HardwareInterfaceType.Gpib;
            using var device = new K2400Device();
            var (success, details) = device.TryOpenSession( this.SelectResourceName( usingInterfaceType ), "Source Measure" );
            Assert.IsTrue( success, $"Open Session; {details}" );
            device.CloseSession();
        }

        /// <summary> (Unit Test Method) output on off should toggle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OutputOnOffShouldToggle()
        {
            bool actualBoolean;
            using var device = new K2400Device();
            var e = new Core.ActionEventArgs();
            var r = device.TryOpenSession( this.SelectResourceName( Pith.HardwareInterfaceType.Gpib ), "Source Measure" );
            Assert.IsTrue( r.Success, $"Open Session; {r.Details}" );

            // do a device clear and reset.
            device.ResetClearInit();
            actualBoolean = true;

            // output should be off after a device clear.
            bool expectedBoolean = false;
            actualBoolean = device.OutputSubsystem.QueryOutputOnState().GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output {0:'ON';'ON';'OFF'};", ( object ) Conversions.ToInteger( expectedBoolean ) );

            // turn it on
            expectedBoolean = true;
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output {0:'ON';'ON';'OFF'};", ( object ) Conversions.ToInteger( expectedBoolean ) );

            // turn it off
            expectedBoolean = false;
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output {0:'ON';'ON';'OFF'};", ( object ) Conversions.ToInteger( expectedBoolean ) );
            string expectedString = "no error";
            if ( device.StatusSubsystem.Session.IsErrorBitSet() )
            {
                r = device.StatusSubsystemBase.TryQueryExistingDeviceErrors();
                Assert.IsTrue( r.Success, $"Device error query failed: {r.Details}" );
            }

            string actualString = device.StatusSubsystem.CompoundErrorMessage;
            Assert.AreEqual( expectedString, actualString, true, System.Globalization.CultureInfo.CurrentCulture, "Device error mismatch" );
        }

        /// <summary> (Unit Test Method) device voltage current resistance should measure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DeviceVoltageCurrentResistanceShouldMeasure()
        {
            double voltage = 5.0d;
            double resistance = 9910d;
            double currentLimit = 0.001d;
            double voltageLimit = voltage + 1.0d;
            bool actualBoolean;
            using var device = new K2400Device();
            var r = device.TryOpenSession( this.SelectResourceName( Pith.HardwareInterfaceType.Gpib ), "Source Measure" );
            Assert.IsTrue( r.Success, $"Open Session; {r.Details}" );
            bool expectedBoolean = true;
            device.ResetClearInit();
            actualBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, "Reset;" );
            string expectedString = "Keithley";
            string actualString = device.StatusSubsystem.QueryIdentity();
            Assert.IsFalse( string.IsNullOrWhiteSpace( actualString ), "Identity is empty" );
            actualString = actualString.Substring( 0, Strings.Len( expectedString ) );
            Assert.AreEqual( expectedString, actualString, true, System.Globalization.CultureInfo.CurrentCulture );
            expectedBoolean = true;
            device.OutputOn( voltage, currentLimit, voltageLimit );
            actualBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output On;" );
            actualBoolean = device.OutputSubsystem.QueryOutputOnState().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output {0:'ON';'ON';'OFF'};", ( object ) Conversions.ToInteger( expectedBoolean ) );
            _ = device.MeasureSubsystem.Read();
            double expectedDouble = voltage;
            Assert.AreEqual( expectedDouble, device.MeasureSubsystem.ReadingAmounts.VoltageReading.Value.GetValueOrDefault( 0d ), 0.1d );
            expectedDouble = voltage / resistance;
            Assert.AreEqual( expectedDouble, device.MeasureSubsystem.ReadingAmounts.CurrentReading.Value.GetValueOrDefault( 0d ), 0.1d / resistance );
            expectedDouble = resistance;
            Assert.AreEqual( expectedDouble, device.MeasureSubsystem.ReadingAmounts.ResistanceReading.Value.GetValueOrDefault( 0d ), 0.1d / resistance );
            expectedBoolean = false;
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output {0:'ON';'ON';'OFF'};", ( object ) Conversions.ToInteger( expectedBoolean ) );
            expectedString = "no error";
            if ( device.StatusSubsystem.Session.IsErrorBitSet() )
            {
                r = device.StatusSubsystem.TryQueryExistingDeviceErrors();
                Assert.IsTrue( r.Success, $"Device error query failed: {r.Details}" );
            }

            actualString = device.StatusSubsystem.CompoundErrorMessage;
            Assert.AreEqual( expectedString, actualString, true, System.Globalization.CultureInfo.CurrentCulture, "Device error mismatch" );
        }
    }
}
