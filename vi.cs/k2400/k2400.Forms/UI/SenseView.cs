using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400.Forms
{

    /// <summary> A Sense view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SenseView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public SenseView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__SenseFunctionComboBox.Name = "_SenseFunctionComboBox";
            this.__ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton";
        }

        /// <summary> Creates a new <see cref="SenseView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="SenseView"/>. </returns>
        public static SenseView Create()
        {
            SenseView view = null;
            try
            {
                view = new SenseView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2400Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2400Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindSenseSubsystem( value );
            this.BindSystemSubsystem( value );
            this.BindTriggerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2400Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Gets the Sense subsystem. </summary>
        /// <value> The Sense subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Bind Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseSubsystem( K2400Device device )
        {
            if ( this.SenseSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseSubsystem = device.SenseSubsystem;
                this.BindSubsystem( true, this.SenseSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K2400.SenseSubsystem.SupportedFunctionModes ) );
                this.HandlePropertyChanged( subsystem, nameof( K2400.SenseSubsystem.FunctionMode ) );
                this.HandlePropertyChanged( subsystem, nameof( K2400.SenseSubsystem.FunctionRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K2400.SenseSubsystem.FunctionRangeDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K2400.SenseSubsystem.FunctionUnit ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseSubsystemPropertyChanged;
                this.BindSenseFunctionSubsystem( null );
            }
        }

        /// <summary> Gets the selected function mode. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The selected function mode. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SenseFunctionModes SelectedFunctionMode =>
                // Return Me._SenseFunctionComboBox.SelectedSenseFunctionModes
                ( SenseFunctionModes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._SenseFunctionComboBox.SelectedItem).Key );

        /// <summary> Handles the supported function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnSupportedFunctionModesChanged( SenseSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.SupportedFunctionModes != SenseFunctionModes.None )
            {
                bool wasInitComp = this.InitializingComponents;
                try
                {
                    this.InitializingComponents = true;
                    if ( subsystem is object && subsystem.SupportedFunctionModes != SenseFunctionModes.None )
                    {
                        this._SenseFunctionComboBox.DataSource = null;
                        this._SenseFunctionComboBox.Items.Clear();
                        this._SenseFunctionComboBox.DataSource = typeof( SenseFunctionModes ).EnumValues().IncludeFilter( ( long ) subsystem.SupportedFunctionModes ).ValueDescriptionPairs().ToList();
                        this._SenseFunctionComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                        this._SenseFunctionComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                        if ( this._SenseFunctionComboBox.Items.Count > 0 )
                        {
                            this._SenseFunctionComboBox.SelectedItem = SenseFunctionModes.Voltage.ValueDescriptionPair();
                        }

                        this._EnabledSenseFunctionsListBox.DataSource = null;
                        this._EnabledSenseFunctionsListBox.Items.Clear();
                        this._EnabledSenseFunctionsListBox.DataSource = typeof( SenseFunctionModes ).EnumValues().IncludeFilter( ( long ) subsystem.SupportedFunctionModes ).ValueDescriptionPairs().ToList();
                        this._EnabledSenseFunctionsListBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                        this._EnabledSenseFunctionsListBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    this.InitializingComponents = wasInitComp;
                }
            }
        }

        /// <summary> Select sense function modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="comboBox">           The combo box. </param>
        /// <param name="senseFunctionModes"> The sense function modes. </param>
        /// <returns> The VI.SenseFunctionModes. </returns>
        public static SenseFunctionModes SelectSenseFunctionModes( ComboBox comboBox, SenseFunctionModes? senseFunctionModes )
        {
            if ( comboBox is null )
                throw new ArgumentNullException( nameof( comboBox ) );
            if ( senseFunctionModes.HasValue && senseFunctionModes.Value != SenseFunctionModes.None && senseFunctionModes.Value != comboBox.SelectedSenseFunctionModes() )
            {
                comboBox.SelectedItem = senseFunctionModes.Value.ValueDescriptionPair();
            }

            return ( SenseFunctionModes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) comboBox.SelectedItem).Key );
        }

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnFunctionModesChanged( SenseSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.FunctionMode.HasValue )
            {
                var value = subsystem.FunctionMode.GetValueOrDefault( SenseFunctionModes.None );
                if ( value != SenseFunctionModes.None )
                {
                    // this is done at the device level by applying the function mode to the measure subsystem
                    // Me.Device.MeasureSubsystem.Readings.Reading.ApplyUnit(subsystem.ToUnit(value))
                    _ = SelectSenseFunctionModes( this._SenseFunctionComboBox, value );
                    switch ( subsystem.FunctionMode )
                    {
                        case SenseFunctionModes.CurrentDC:
                            {
                                this.BindSenseFunctionSubsystem( this.Device.SenseCurrentSubsystem );
                                break;
                            }

                        case SenseFunctionModes.VoltageDC:
                            {
                                this.BindSenseFunctionSubsystem( this.Device.SenseVoltageSubsystem );
                                break;
                            }

                        case SenseFunctionModes.Resistance:
                            {
                                this.BindSenseFunctionSubsystem( this.Device.SenseResistanceSubsystem );
                                break;
                            }
                    }
                }
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseSubsystem subsystem, string propertyName )
        {
            if ( this.InitializingComponents || subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SenseSubsystem.SupportedFunctionModes ):
                    {
                        this.OnSupportedFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( K2400.SenseSubsystem.FunctionMode ):
                    {
                        this.OnFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( K2400.SenseSubsystem.FunctionRange ):
                    {
                        _ = this._SenseRangeNumeric.RangeSetter( subsystem.FunctionRange.Min, subsystem.FunctionRange.Max );
                        break;
                    }

                case nameof( K2400.SenseSubsystem.FunctionRangeDecimalPlaces ):
                    {
                        this._SenseRangeNumeric.DecimalPlaces = subsystem.FunctionRangeDecimalPlaces;
                        break;
                    }

                case nameof( K2400.SenseSubsystem.FunctionUnit ):
                    {
                        this._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:";
                        this._SenseRangeNumericLabel.Left = this._SenseRangeNumeric.Left - this._SenseRangeNumericLabel.Width;
                        break;
                    }
            }
        }

        /// <summary> Sense subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SenseSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE FUNCTION "

        /// <summary> Gets or sets the sense function subsystem. </summary>
        /// <value> The sense function subsystem. </value>
        public SenseFunctionSubsystemBase SenseFunctionSubsystem { get; private set; }

        /// <summary> Bind Sense function subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseFunctionSubsystem( SenseFunctionSubsystemBase subsystem )
        {
            if ( this.SenseFunctionSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseFunctionSubsystem );
                this.SenseFunctionSubsystem = null;
            }

            this.SenseFunctionSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.SenseFunctionSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseFunctionSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseFunctionSubsystemPropertyChanged;
            }
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            // subsystem.QueryAutoRangeEnabled()
            // subsystem.QueryPowerLineCycles()
            // subsystem.QueryRange()
            else
            {
                subsystem.PropertyChanged -= this.SenseFunctionSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseFunctionSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( SenseFunctionSubsystemBase.AutoRangeEnabled ):
                    {
                        if ( this.Device is object && subsystem.AutoRangeEnabled.HasValue )
                        {
                            this._SenseAutoRangeToggle.Checked = subsystem.AutoRangeEnabled.Value;
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.PowerLineCycles ):
                    {
                        if ( this.Device is object && subsystem.PowerLineCycles.HasValue )
                        {
                            if ( subsystem.PowerLineCycles.HasValue )
                                _ = this._NplcNumeric.ValueSetter( ( decimal ) subsystem.PowerLineCycles.Value );
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.Range ):
                    {
                        if ( this.Device is object && subsystem.Range.HasValue )
                        {
                            _ = this._SenseRangeNumeric.ValueSetter( subsystem.Range.Value );
                        }

                        break;
                    }
            }
        }

        /// <summary> Sense function subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseFunctionSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SenseFunctionSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseFunctionSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseFunctionSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( K2400Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
            }
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            // subsystem.QueryDelay()
            // subsystem.QueryAutoDelayEnabled()
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.TriggerSubsystem.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                        {
                            // Me._TriggerDelayNumeric.Value = CDec(subsystem.Delay.Value.TotalSeconds)
                        }

                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: SENSE "

        /// <summary>
        /// Event handler. Called by _SenseFunctionComboBox for selected index changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SenseFunctionComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( sender is Control )
            {
                _ = this.Device.SenseSubsystem.ApplyFunctionMode( this.SelectedFunctionMode );
            }
        }

        /// <summary> Applies the selected measurements settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplySenseSettings()
        {
            // make sure output is off.
            _ = this.Device.OutputSubsystem.WriteOutputOnState( false );
            _ = this.SenseFunctionSubsystem.ApplyPowerLineCycles( ( double ) this._NplcNumeric.Value );
            _ = this.SenseFunctionSubsystem.ApplyAutoRangeEnabled( this._SenseAutoRangeToggle.Checked );
            if ( !this._SenseAutoRangeToggle.Checked )
                _ = this.SenseFunctionSubsystem.ApplyRange( ( double ) this._SenseRangeNumeric.Value );
        }

        /// <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySenseSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} applying sense settings";
                this.ApplySenseSettings();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System subsystem. </summary>
        /// <value> The System subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSystemSubsystem( K2400Device device )
        {
            if ( this.SystemSubsystem is object )
            {
                this.BindSubsystem( false, this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            if ( device is object )
            {
                this.SystemSubsystem = device.SystemSubsystem;
                this.BindSubsystem( true, this.SystemSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SystemSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the System subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SystemSubsystem.FourWireSenseEnabled ):
                    {
                        if ( subsystem.FourWireSenseEnabled.HasValue )
                            this._FourWireSenseCheckBox.Checked = subsystem.FourWireSenseEnabled.Value;
                        break;
                    }
            }

            Application.DoEvents();
        }

        /// <summary> System subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SystemSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SystemSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
