﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400.Forms
{
    [DesignerGenerated()]
    public partial class SourceView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _OutputAutoOnCheckBox = new System.Windows.Forms.CheckBox();
            __SourceAutoClearCheckBox = new System.Windows.Forms.CheckBox();
            __SourceAutoClearCheckBox.CheckStateChanged += new EventHandler(SourceAutoClearCheckBox_CheckStateChanged);
            __ApplySourceFunctionButton = new System.Windows.Forms.Button();
            __ApplySourceFunctionButton.Click += new EventHandler(ApplySourceFunctionButton_Click);
            _SourceLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _SourceLevelNumeric = new System.Windows.Forms.NumericUpDown();
            _SourceRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _SourceDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _SourceDelayTextBoxLabel = new System.Windows.Forms.Label();
            _SourceLevelNumericLabel = new System.Windows.Forms.Label();
            __ApplySourceSettingButton = new System.Windows.Forms.Button();
            __ApplySourceSettingButton.Click += new EventHandler(ApplySourceSettingButton_Click);
            _SourceRangeNumericLabel = new System.Windows.Forms.Label();
            _SourceFunctionComboBox = new System.Windows.Forms.ComboBox();
            _SourceLimitNumericLabel = new System.Windows.Forms.Label();
            _SourceFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _Panel = new System.Windows.Forms.Panel();
            _TriggerDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _OutputPanel = new System.Windows.Forms.Panel();
            __ContactCheckEnabledCheckBox = new System.Windows.Forms.CheckBox();
            __ContactCheckEnabledCheckBox.CheckStateChanged += new EventHandler(ContactCheckEnabledMenuItem_CheckStateChanged);
            ((System.ComponentModel.ISupportInitialize)_SourceLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SourceLevelNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SourceRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SourceDelayNumeric).BeginInit();
            _Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).BeginInit();
            _Layout.SuspendLayout();
            _OutputPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _OutputAutoOnCheckBox
            // 
            _OutputAutoOnCheckBox.AutoSize = true;
            _OutputAutoOnCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _OutputAutoOnCheckBox.Location = new System.Drawing.Point(70, 33);
            _OutputAutoOnCheckBox.Name = "_OutputAutoOnCheckBox";
            _OutputAutoOnCheckBox.Size = new System.Drawing.Size(122, 21);
            _OutputAutoOnCheckBox.TabIndex = 2;
            _OutputAutoOnCheckBox.Text = "Output Auto On:";
            _OutputAutoOnCheckBox.ThreeState = true;
            _OutputAutoOnCheckBox.UseVisualStyleBackColor = true;
            // 
            // _SourceAutoClearCheckBox
            // 
            __SourceAutoClearCheckBox.AutoSize = true;
            __SourceAutoClearCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            __SourceAutoClearCheckBox.Location = new System.Drawing.Point(38, 187);
            __SourceAutoClearCheckBox.Name = "__SourceAutoClearCheckBox";
            __SourceAutoClearCheckBox.Size = new System.Drawing.Size(91, 21);
            __SourceAutoClearCheckBox.TabIndex = 55;
            __SourceAutoClearCheckBox.Text = "Auto Clear:";
            __SourceAutoClearCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            __SourceAutoClearCheckBox.UseVisualStyleBackColor = true;
            // 
            // _ApplySourceFunctionButton
            // 
            __ApplySourceFunctionButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __ApplySourceFunctionButton.Location = new System.Drawing.Point(293, 5);
            __ApplySourceFunctionButton.Name = "__ApplySourceFunctionButton";
            __ApplySourceFunctionButton.Size = new System.Drawing.Size(61, 30);
            __ApplySourceFunctionButton.TabIndex = 48;
            __ApplySourceFunctionButton.Text = "Apply";
            __ApplySourceFunctionButton.UseVisualStyleBackColor = true;
            // 
            // _SourceLimitNumeric
            // 
            _SourceLimitNumeric.DecimalPlaces = 4;
            _SourceLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SourceLimitNumeric.Location = new System.Drawing.Point(115, 126);
            _SourceLimitNumeric.Name = "_SourceLimitNumeric";
            _SourceLimitNumeric.Size = new System.Drawing.Size(77, 25);
            _SourceLimitNumeric.TabIndex = 47;
            // 
            // _SourceLevelNumeric
            // 
            _SourceLevelNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SourceLevelNumeric.Location = new System.Drawing.Point(115, 96);
            _SourceLevelNumeric.Maximum = new decimal(new int[] { 1100, 0, 0, 0 });
            _SourceLevelNumeric.Name = "_SourceLevelNumeric";
            _SourceLevelNumeric.Size = new System.Drawing.Size(77, 25);
            _SourceLevelNumeric.TabIndex = 45;
            // 
            // _SourceRangeNumeric
            // 
            _SourceRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SourceRangeNumeric.Location = new System.Drawing.Point(115, 67);
            _SourceRangeNumeric.Maximum = new decimal(new int[] { 1100, 0, 0, 0 });
            _SourceRangeNumeric.Name = "_SourceRangeNumeric";
            _SourceRangeNumeric.Size = new System.Drawing.Size(77, 25);
            _SourceRangeNumeric.TabIndex = 46;
            // 
            // _SourceDelayNumeric
            // 
            _SourceDelayNumeric.DecimalPlaces = 2;
            _SourceDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SourceDelayNumeric.Location = new System.Drawing.Point(115, 38);
            _SourceDelayNumeric.Name = "_SourceDelayNumeric";
            _SourceDelayNumeric.Size = new System.Drawing.Size(77, 25);
            _SourceDelayNumeric.TabIndex = 44;
            // 
            // _SourceDelayTextBoxLabel
            // 
            _SourceDelayTextBoxLabel.AutoSize = true;
            _SourceDelayTextBoxLabel.Location = new System.Drawing.Point(40, 42);
            _SourceDelayTextBoxLabel.Name = "_SourceDelayTextBoxLabel";
            _SourceDelayTextBoxLabel.Size = new System.Drawing.Size(72, 17);
            _SourceDelayTextBoxLabel.TabIndex = 43;
            _SourceDelayTextBoxLabel.Text = "Delay [ms]:";
            _SourceDelayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SourceLevelNumericLabel
            // 
            _SourceLevelNumericLabel.AutoSize = true;
            _SourceLevelNumericLabel.Location = new System.Drawing.Point(52, 100);
            _SourceLevelNumericLabel.Name = "_SourceLevelNumericLabel";
            _SourceLevelNumericLabel.Size = new System.Drawing.Size(60, 17);
            _SourceLevelNumericLabel.TabIndex = 37;
            _SourceLevelNumericLabel.Text = "Level [V]:";
            _SourceLevelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ApplySourceSettingButton
            // 
            __ApplySourceSettingButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __ApplySourceSettingButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ApplySourceSettingButton.Location = new System.Drawing.Point(293, 176);
            __ApplySourceSettingButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __ApplySourceSettingButton.Name = "__ApplySourceSettingButton";
            __ApplySourceSettingButton.Size = new System.Drawing.Size(58, 30);
            __ApplySourceSettingButton.TabIndex = 42;
            __ApplySourceSettingButton.Text = "&Apply";
            __ApplySourceSettingButton.UseVisualStyleBackColor = true;
            // 
            // _SourceRangeNumericLabel
            // 
            _SourceRangeNumericLabel.AutoSize = true;
            _SourceRangeNumericLabel.Location = new System.Drawing.Point(44, 71);
            _SourceRangeNumericLabel.Name = "_SourceRangeNumericLabel";
            _SourceRangeNumericLabel.Size = new System.Drawing.Size(68, 17);
            _SourceRangeNumericLabel.TabIndex = 38;
            _SourceRangeNumericLabel.Text = "Range [V]:";
            _SourceRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SourceFunctionComboBox
            // 
            _SourceFunctionComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _SourceFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _SourceFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SourceFunctionComboBox.Items.AddRange(new object[] { "I", "V" });
            _SourceFunctionComboBox.Location = new System.Drawing.Point(115, 7);
            _SourceFunctionComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _SourceFunctionComboBox.Name = "_SourceFunctionComboBox";
            _SourceFunctionComboBox.Size = new System.Drawing.Size(173, 25);
            _SourceFunctionComboBox.TabIndex = 41;
            // 
            // _SourceLimitNumericLabel
            // 
            _SourceLimitNumericLabel.AutoSize = true;
            _SourceLimitNumericLabel.Location = new System.Drawing.Point(54, 130);
            _SourceLimitNumericLabel.Name = "_SourceLimitNumericLabel";
            _SourceLimitNumericLabel.Size = new System.Drawing.Size(58, 17);
            _SourceLimitNumericLabel.TabIndex = 39;
            _SourceLimitNumericLabel.Text = "Limit [A]:";
            _SourceLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SourceFunctionComboBoxLabel
            // 
            _SourceFunctionComboBoxLabel.AutoSize = true;
            _SourceFunctionComboBoxLabel.Location = new System.Drawing.Point(53, 11);
            _SourceFunctionComboBoxLabel.Name = "_SourceFunctionComboBoxLabel";
            _SourceFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SourceFunctionComboBoxLabel.TabIndex = 40;
            _SourceFunctionComboBoxLabel.Text = "Function:";
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__SourceAutoClearCheckBox);
            _Panel.Controls.Add(_TriggerDelayNumeric);
            _Panel.Controls.Add(_TriggerDelayNumericLabel);
            _Panel.Controls.Add(__ApplySourceFunctionButton);
            _Panel.Controls.Add(_SourceLimitNumeric);
            _Panel.Controls.Add(_SourceLevelNumeric);
            _Panel.Controls.Add(_SourceRangeNumeric);
            _Panel.Controls.Add(_SourceDelayNumeric);
            _Panel.Controls.Add(_SourceDelayTextBoxLabel);
            _Panel.Controls.Add(_SourceLevelNumericLabel);
            _Panel.Controls.Add(__ApplySourceSettingButton);
            _Panel.Controls.Add(_SourceRangeNumericLabel);
            _Panel.Controls.Add(_SourceFunctionComboBox);
            _Panel.Controls.Add(_SourceLimitNumericLabel);
            _Panel.Controls.Add(_SourceFunctionComboBoxLabel);
            _Panel.Location = new System.Drawing.Point(8, 15);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(363, 216);
            _Panel.TabIndex = 0;
            // 
            // _TriggerDelayNumeric
            // 
            _TriggerDelayNumeric.DecimalPlaces = 3;
            _TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerDelayNumeric.Location = new System.Drawing.Point(115, 156);
            _TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
            _TriggerDelayNumeric.Size = new System.Drawing.Size(56, 25);
            _TriggerDelayNumeric.TabIndex = 54;
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(5, 160);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(107, 17);
            _TriggerDelayNumericLabel.TabIndex = 53;
            _TriggerDelayNumericLabel.Text = "Trigger Delay [s]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Controls.Add(_OutputPanel, 1, 2);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 4;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 1;
            // 
            // _OutputPanel
            // 
            _OutputPanel.Controls.Add(_OutputAutoOnCheckBox);
            _OutputPanel.Controls.Add(__ContactCheckEnabledCheckBox);
            _OutputPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _OutputPanel.Location = new System.Drawing.Point(8, 237);
            _OutputPanel.Name = "_OutputPanel";
            _OutputPanel.Size = new System.Drawing.Size(363, 62);
            _OutputPanel.TabIndex = 1;
            // 
            // _ContactCheckEnabledCheckBox
            // 
            __ContactCheckEnabledCheckBox.AutoSize = true;
            __ContactCheckEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            __ContactCheckEnabledCheckBox.Location = new System.Drawing.Point(29, 9);
            __ContactCheckEnabledCheckBox.Name = "__ContactCheckEnabledCheckBox";
            __ContactCheckEnabledCheckBox.Size = new System.Drawing.Size(163, 21);
            __ContactCheckEnabledCheckBox.TabIndex = 0;
            __ContactCheckEnabledCheckBox.Text = "Contact Check Enabled:";
            __ContactCheckEnabledCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            __ContactCheckEnabledCheckBox.ThreeState = true;
            __ContactCheckEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // SourceView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "SourceView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            ((System.ComponentModel.ISupportInitialize)_SourceLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SourceLevelNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SourceRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SourceDelayNumeric).EndInit();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).EndInit();
            _Layout.ResumeLayout(false);
            _OutputPanel.ResumeLayout(false);
            _OutputPanel.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.Button __ApplySourceFunctionButton;

        private System.Windows.Forms.Button _ApplySourceFunctionButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySourceFunctionButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySourceFunctionButton != null)
                {
                    __ApplySourceFunctionButton.Click -= ApplySourceFunctionButton_Click;
                }

                __ApplySourceFunctionButton = value;
                if (__ApplySourceFunctionButton != null)
                {
                    __ApplySourceFunctionButton.Click += ApplySourceFunctionButton_Click;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _SourceLimitNumeric;
        private System.Windows.Forms.NumericUpDown _SourceLevelNumeric;
        private System.Windows.Forms.NumericUpDown _SourceRangeNumeric;
        private System.Windows.Forms.NumericUpDown _SourceDelayNumeric;
        private System.Windows.Forms.Label _SourceDelayTextBoxLabel;
        private System.Windows.Forms.Label _SourceLevelNumericLabel;
        private System.Windows.Forms.Button __ApplySourceSettingButton;

        private System.Windows.Forms.Button _ApplySourceSettingButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySourceSettingButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySourceSettingButton != null)
                {
                    __ApplySourceSettingButton.Click -= ApplySourceSettingButton_Click;
                }

                __ApplySourceSettingButton = value;
                if (__ApplySourceSettingButton != null)
                {
                    __ApplySourceSettingButton.Click += ApplySourceSettingButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _SourceRangeNumericLabel;
        private System.Windows.Forms.ComboBox _SourceFunctionComboBox;
        private System.Windows.Forms.Label _SourceLimitNumericLabel;
        private System.Windows.Forms.Label _SourceFunctionComboBoxLabel;
        private System.Windows.Forms.NumericUpDown _TriggerDelayNumeric;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
        private System.Windows.Forms.Panel _OutputPanel;
        private System.Windows.Forms.CheckBox _OutputAutoOnCheckBox;
        private System.Windows.Forms.CheckBox __ContactCheckEnabledCheckBox;

        private System.Windows.Forms.CheckBox _ContactCheckEnabledCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ContactCheckEnabledCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ContactCheckEnabledCheckBox != null)
                {
                    __ContactCheckEnabledCheckBox.CheckStateChanged -= ContactCheckEnabledMenuItem_CheckStateChanged;
                }

                __ContactCheckEnabledCheckBox = value;
                if (__ContactCheckEnabledCheckBox != null)
                {
                    __ContactCheckEnabledCheckBox.CheckStateChanged += ContactCheckEnabledMenuItem_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox __SourceAutoClearCheckBox;

        private System.Windows.Forms.CheckBox _SourceAutoClearCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SourceAutoClearCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SourceAutoClearCheckBox != null)
                {
                    __SourceAutoClearCheckBox.CheckStateChanged -= SourceAutoClearCheckBox_CheckStateChanged;
                }

                __SourceAutoClearCheckBox = value;
                if (__SourceAutoClearCheckBox != null)
                {
                    __SourceAutoClearCheckBox.CheckStateChanged += SourceAutoClearCheckBox_CheckStateChanged;
                }
            }
        }
    }
}