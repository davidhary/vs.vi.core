using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.K2400.Forms
{

    /// <summary> Keithley 2400 Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "K2400 User Interface" )]
    [Description( "Keithley 2400 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( K2400TreeView ) )]
    public class K2400TreeView : Facade.VisaTreeView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K2400TreeView() : base()
        {
            this.InitializingComponents = true;
            this.ReadingView = ReadingView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Read", "Read", this.ReadingView );
            this.SourceView = SourceView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Source", "Source", this.SourceView );
            this.SenseView = SenseView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Sense", "Sense", this.SenseView );
            this.HipotView = HipotView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Hipot", "Hipot", this.HipotView );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public K2400TreeView( K2400Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary> Gets the hipot view. </summary>
        /// <value> The hipot view. </value>
        private HipotView HipotView { get; set; }

        /// <summary> Gets the reading view. </summary>
        /// <value> The reading view. </value>
        private ReadingView ReadingView { get; set; }

        /// <summary> Gets the sense view. </summary>
        /// <value> The sense view. </value>
        private SenseView SenseView { get; set; }

        /// <summary> Gets source view. </summary>
        /// <value> The source view. </value>
        private SourceView SourceView { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the K2400 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    this.SourceView?.Dispose();
                    this.SourceView = null;

                    this.SenseView?.Dispose();
                    this.SenseView = null;

                    this.HipotView?.Dispose();
                    this.HipotView = null;

                    this.ReadingView?.Dispose();
                    this.ReadingView = null;

                    this.AssignDeviceThis( null );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2400Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( K2400Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = K2400.My.MySettings.Default;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( K2400Device value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.SourceView.AssignDevice( null );
                this.SenseView.AssignDevice( null );
                this.ReadingView.AssignDevice( null );
                this.HipotView.AssignDevice( null );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            this.DisplayView.BindMeasureToolStrip( this.Device.MeasureSubsystem );
            this.DisplayView.BindSubsystemToolStrip( this.Device.SourceSubsystem );
            this.StatusView.ReadTerminalsState = null;
            this.DisplayView.BindTerminalsDisplay( this.Device.SystemSubsystem );
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.SourceView.AssignDevice( this.Device );
            this.SenseView.AssignDevice( this.Device );
            this.ReadingView.AssignDevice( this.Device );
            this.HipotView.AssignDevice( this.Device );
            this.DisplayView.BindMeasureToolStrip( this.Device.MeasureSubsystem );
            this.DisplayView.BindSubsystemToolStrip( this.Device.SourceSubsystem );
            this.StatusView.ReadTerminalsState = this.Device.SystemSubsystem.QueryFrontTerminalsSelected;
            this.DisplayView.BindTerminalsDisplay( this.Device.SystemSubsystem );
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
