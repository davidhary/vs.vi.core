using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.Primitives;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400.Forms
{

    /// <summary> A Source view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SourceView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public SourceView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__SourceAutoClearCheckBox.Name = "_SourceAutoClearCheckBox";
            this.__ApplySourceFunctionButton.Name = "_ApplySourceFunctionButton";
            this.__ApplySourceSettingButton.Name = "_ApplySourceSettingButton";
            this.__ContactCheckEnabledCheckBox.Name = "_ContactCheckEnabledCheckBox";
        }

        /// <summary> Creates a new <see cref="SourceView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="SourceView"/>. </returns>
        public static SourceView Create()
        {
            SourceView view = null;
            try
            {
                view = new SourceView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2400Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2400Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindSourceSubsystem( value );
            this.BindSystemSubsystem( value );
            this.BindTriggerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2400Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SOURCE "

        /// <summary> Gets the Source subsystem. </summary>
        /// <value> The Source subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SourceSubsystem SourceSubsystem { get; private set; }

        /// <summary> Bind Source subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSourceSubsystem( K2400Device device )
        {
            if ( this.SourceSubsystem is object )
            {
                this.BindSubsystem( false, this.SourceSubsystem );
                this.SourceSubsystem = null;
            }

            if ( device is object )
            {
                this.SourceSubsystem = device.SourceSubsystem;
                this.BindSubsystem( true, this.SourceSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SourceSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SourceSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.SourceSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Source subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SourceSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SourceSubsystem.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                            _ = this._SourceDelayNumeric.ValueSetter( subsystem.Delay.Value.TotalMilliseconds );
                        break;
                    }

                case nameof( K2400.SourceSubsystem.AutoClearEnabled ):
                    {
                        if ( subsystem.AutoClearEnabled.HasValue )
                        {
                            this._SourceAutoClearCheckBox.Checked = subsystem.AutoClearEnabled.Value;
                        }

                        break;
                    }

                case var @case when @case == nameof( K2400.SourceSubsystem.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                        {
                            _ = this._SourceDelayNumeric.ValueSetter( subsystem.Delay.Value.Ticks / ( double ) TimeSpan.TicksPerMillisecond );
                        }

                        break;
                    }

                case nameof( K2400.SourceSubsystem.FunctionMode ):
                    {
                        if ( subsystem.FunctionMode.HasValue )
                        {
                            this._SourceFunctionComboBox.SelectedItem = subsystem.FunctionMode.Value.ValueDescriptionPair();
                            this.OnSourceFunctionChanged( subsystem );
                        }

                        break;
                    }

                case nameof( K2400.SourceSubsystem.SupportedFunctionModes ):
                    {
                        this.OnSupportedFunctionModesChanged( subsystem );
                        break;
                    }
            }
        }

        /// <summary> Source subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inSourceion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SourceSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SourceSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SourceSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SourceSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handles the supported function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnSupportedFunctionModesChanged( SourceSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.SupportedFunctionModes != SourceFunctionModes.None )
            {
                this._SourceFunctionComboBox.DataSource = null;
                this._SourceFunctionComboBox.Items.Clear();
                this._SourceFunctionComboBox.DataSource = typeof( SourceFunctionModes ).EnumValues().IncludeFilter( ( long ) subsystem.SupportedFunctionModes ).ValueDescriptionPairs().ToList();
                this._SourceFunctionComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                this._SourceFunctionComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                if ( this._SourceFunctionComboBox.Items.Count > 0 )
                {
                    this._SourceFunctionComboBox.SelectedItem = SourceFunctionModes.Voltage.ValueDescriptionPair();
                }
            }
        }

        /// <summary> Gets the selected source function mode. </summary>
        /// <value> The selected source function mode. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SourceFunctionModes SelectedSourceFunctionMode => ( SourceFunctionModes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._SourceFunctionComboBox.SelectedItem).Key );

        /// <summary> Executes the 'source function changed' action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnSourceFunctionChanged( SourceSubsystem subsystem )
        {
            if ( subsystem is null )
                return;
            var unit = Arebis.StandardUnits.ElectricUnits.Volt;
            var range = new RangeR( -1, 1d );
            int decimalPlaces = 3;
            decimal increment = 0m;
            var limitUnit = Arebis.StandardUnits.ElectricUnits.Ampere;
            var limitRange = new RangeR( -1, 1d );
            int limitDecimalPlaces = 3;
            decimal limitIncrement = 0m;
            switch ( subsystem.FunctionMode.Value )
            {
                case SourceFunctionModes.Current:
                    {
                        unit = Arebis.StandardUnits.ElectricUnits.Ampere;
                        range = this.Device.SourceCurrentSubsystem.FunctionRange;
                        decimalPlaces = 6;
                        increment = 0.0001m;
                        limitUnit = Arebis.StandardUnits.ElectricUnits.Volt;
                        limitRange = this.Device.SourceVoltageSubsystem.FunctionRange;
                        limitDecimalPlaces = 3;
                        limitIncrement = 1m;
                        break;
                    }

                case SourceFunctionModes.Voltage:
                    {
                        range = this.Device.SourceVoltageSubsystem.FunctionRange;
                        decimalPlaces = 3;
                        limitRange = this.Device.SourceCurrentSubsystem.FunctionRange;
                        limitDecimalPlaces = 6;
                        increment = 1m;
                        limitIncrement = 0.0001m;
                        break;
                    }
            }

            this._SourceLevelNumeric.Maximum = ( decimal ) range.Max;
            this._SourceLevelNumeric.Minimum = ( decimal ) range.Min;
            this._SourceLevelNumeric.DecimalPlaces = decimalPlaces;
            this._SourceLevelNumeric.Increment = increment;
            this._SourceRangeNumeric.Maximum = ( decimal ) range.Max;
            this._SourceRangeNumeric.Minimum = ( decimal ) range.Min;
            this._SourceRangeNumeric.DecimalPlaces = decimalPlaces;
            this._SourceRangeNumeric.Increment = increment;
            this._SourceLimitNumeric.Maximum = ( decimal ) limitRange.Max;
            this._SourceLimitNumeric.Minimum = ( decimal ) limitRange.Min;
            this._SourceLimitNumeric.DecimalPlaces = limitDecimalPlaces;
            this._SourceLimitNumeric.Increment = limitIncrement;
            this._SourceLevelNumericLabel.Text = $"Level [{unit.Symbol}]";
            this._SourceLevelNumericLabel.Left = this._SourceLevelNumeric.Left - this._SourceLevelNumericLabel.Width;
            this._SourceRangeNumericLabel.Text = $"Range [{unit.Symbol}]";
            this._SourceRangeNumericLabel.Left = this._SourceRangeNumeric.Left - this._SourceRangeNumericLabel.Width;
            this._SourceLimitNumericLabel.Text = $"Limit [{limitUnit.Symbol}]";
            this._SourceLimitNumericLabel.Left = this._SourceLimitNumeric.Left - this._SourceLimitNumericLabel.Width;
            switch ( subsystem.FunctionMode.Value )
            {
                case SourceFunctionModes.Current:
                    {
                        this.BindSourceVoltageSubsystem( null );
                        this.BindSourceCurrentSubsystem( this.Device );
                        break;
                    }

                case SourceFunctionModes.Voltage:
                    {
                        this.BindSourceCurrentSubsystem( null );
                        this.BindSourceVoltageSubsystem( this.Device );
                        break;
                    }
            }
        }

        #endregion

        #region " SOURCE CURRENT "

        /// <summary> Gets or sets the Source Current subsystem. </summary>
        /// <value> The Source Current subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SourceCurrentSubsystem SourceCurrentSubsystem { get; private set; }

        /// <summary> Bind Source Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSourceCurrentSubsystem( K2400Device device )
        {
            if ( this.SourceCurrentSubsystem is object )
            {
                this.BindSubsystem( false, this.SourceCurrentSubsystem );
                this.SourceCurrentSubsystem = null;
            }

            if ( device is object )
            {
                this.SourceCurrentSubsystem = device.SourceCurrentSubsystem;
                this.BindSubsystem( true, this.SourceCurrentSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SourceCurrentSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SourceCurrentSubsystemPropertyChanged;
            }
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            // subsystem.QueryLevel()
            // subsystem.QueryRange()
            // subsystem.QueryProtectionLevel()
            else
            {
                subsystem.PropertyChanged -= this.SourceCurrentSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Source Current subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SourceCurrentSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( this.Device.SourceSubsystem.FunctionMode.GetValueOrDefault( SourceFunctionModes.None ) != SourceFunctionModes.Current )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SourceCurrentSubsystem.Level ):
                    {
                        if ( subsystem.Level.HasValue )
                            _ = this._SourceLevelNumeric.ValueSetter( subsystem.Level.Value );
                        break;
                    }

                case nameof( K2400.SourceCurrentSubsystem.Range ):
                    {
                        if ( subsystem.Range.HasValue )
                            _ = this._SourceRangeNumeric.ValueSetter( subsystem.Range.Value );
                        break;
                    }
            }
        }

        /// <summary> Source Current subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source Current of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SourceCurrentSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SourceCurrentSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SourceCurrentSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SourceCurrentSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SOURCE VOLTAGE "

        /// <summary> Gets or sets the Source Voltage subsystem. </summary>
        /// <value> The Source Voltage subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SourceVoltageSubsystem SourceVoltageSubsystem { get; private set; }

        /// <summary> Bind Source Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSourceVoltageSubsystem( K2400Device device )
        {
            if ( this.SourceVoltageSubsystem is object )
            {
                this.BindSubsystem( false, this.SourceVoltageSubsystem );
                this.SourceVoltageSubsystem = null;
            }

            if ( device is object )
            {
                this.SourceVoltageSubsystem = device.SourceVoltageSubsystem;
                this.BindSubsystem( true, this.SourceVoltageSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SourceVoltageSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SourceVoltageSubsystemPropertyChanged;
            }
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            // subsystem.QueryLevel()
            // subsystem.QueryRange()
            // subsystem.QueryProtectionLevel()
            else
            {
                subsystem.PropertyChanged -= this.SourceVoltageSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Source Voltage subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SourceVoltageSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( this.Device.SourceSubsystem.FunctionMode.GetValueOrDefault( SourceFunctionModes.None ) != SourceFunctionModes.Voltage )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SourceVoltageSubsystem.Level ):
                    {
                        if ( subsystem.Level.HasValue )
                            _ = this._SourceLevelNumeric.ValueSetter( subsystem.Level.Value );
                        break;
                    }

                case nameof( K2400.SourceVoltageSubsystem.Range ):
                    {
                        if ( subsystem.Range.HasValue )
                            _ = this._SourceRangeNumeric.ValueSetter( subsystem.Range.Value );
                        break;
                    }
            }
        }

        /// <summary> Source Voltage subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source Voltage of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SourceVoltageSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SourceVoltageSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SourceVoltageSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SourceVoltageSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System subsystem. </summary>
        /// <value> The System subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSystemSubsystem( K2400Device device )
        {
            if ( this.SystemSubsystem is object )
            {
                this.BindSubsystem( false, this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            if ( device is object )
            {
                this.SystemSubsystem = device.SystemSubsystem;
                this.BindSubsystem( true, this.SystemSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SystemSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
            }
        }

        /// <summary> Contact check enabled menu item check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ContactCheckEnabledMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.InfoProvider.Clear();
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying enabling contact check";
                if ( !this.Device.SystemSubsystem.ContactCheckEnabled.HasValue || this.Device.SystemSubsystem.ContactCheckEnabled.Value != menuItem.Checked )
                {
                    this._ContactCheckEnabledCheckBox.CheckState = this.Device.SystemSubsystem.ContactCheckEnabled.ToCheckState();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handle the System subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SystemSubsystem.ContactCheckEnabled ):
                    {
                        this._ContactCheckEnabledCheckBox.CheckState = subsystem.ContactCheckEnabled.ToCheckState();
                        break;
                    }

                case nameof( K2400.SystemSubsystem.ContactCheckSupported ):
                    {
                        this._ContactCheckEnabledCheckBox.Enabled = subsystem.ContactCheckSupported.GetValueOrDefault( false );
                        break;
                    }
            }

            Application.DoEvents();
        }

        /// <summary> System subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SystemSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SystemSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( K2400Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.TriggerSubsystem.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                            _ = this._TriggerDelayNumeric.ValueSetter( subsystem.Delay.Value.TotalMilliseconds );
                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inTriggerion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Source automatic clear check box check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SourceAutoClearCheckBox_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.InfoProvider.Clear();
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} setting output auto on mode";
                if ( !this.Device.SourceSubsystem.AutoClearEnabled.HasValue || this.Device.SourceSubsystem.AutoClearEnabled.Value != menuItem.Checked )
                {
                    _ = this.Device.SourceSubsystem.ApplyAutoClearEnabled( menuItem.Checked );
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Applies the source function button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySourceFunctionButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying source function";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.ClearExecutionState();

                // make sure output is off.
                _ = this.Device.OutputSubsystem.WriteOutputOnState( false );
                _ = this.Device.SourceSubsystem.ApplyFunctionMode( this.SelectedSourceFunctionMode );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the source setting button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0045:Convert to conditional expression", Justification = "<Pending>" )]
        private void ApplySourceSettingButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying source settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.ClearExecutionState();

                // make sure output is off.
                _ = this.Device.OutputSubsystem.WriteOutputOnState( false );

                // get the delay time
                if ( this._TriggerDelayNumeric.Value >= 0m )
                {
                    _ = this.Device.TriggerSubsystem.ApplyDelay( TimeSpan.FromTicks( ( long ) Math.Round( TimeSpan.TicksPerSecond * this._TriggerDelayNumeric.Value ) ) );
                }
                else
                {
                    _ = this.Device.TriggerSubsystem.ApplyAutoDelayEnabled( true );
                }

                _ = this.Device.SourceSubsystem.ApplyDelay( TimeSpan.FromMilliseconds( ( double ) this._SourceDelayNumeric.Value ) );
                switch ( this.Device.SourceSubsystem.FunctionMode.Value )
                {
                    case SourceFunctionModes.Current:
                        {
                            _ = this.Device.SourceCurrentSubsystem.ApplyRange( 1.1d * ( double ) this._SourceLevelNumeric.Value );
                            _ = this.Device.SourceCurrentSubsystem.ApplyLevel( ( double ) this._SourceLevelNumeric.Value );
                            _ = this.Device.SenseVoltageSubsystem.ApplyProtectionLevel( ( double ) this._SourceLimitNumeric.Value );
                            break;
                        }

                    case SourceFunctionModes.Voltage:
                        {
                            _ = this.Device.SourceVoltageSubsystem.ApplyRange( 1.1d * ( double ) this._SourceLevelNumeric.Value );
                            _ = this.Device.SourceVoltageSubsystem.ApplyLevel( ( double ) this._SourceLevelNumeric.Value );
                            _ = this.Device.SenseCurrentSubsystem.ApplyProtectionLevel( ( double ) this._SourceLimitNumeric.Value );
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
