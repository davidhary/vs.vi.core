﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400.Forms
{
    [DesignerGenerated()]
    public partial class HipotView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(HipotView));
            _Tabs = new System.Windows.Forms.TabControl();
            _HipotTabPage = new System.Windows.Forms.TabPage();
            _HipotLayout = new System.Windows.Forms.TableLayoutPanel();
            _HipotGroupBox = new System.Windows.Forms.GroupBox();
            __ContactCheckToggle = new System.Windows.Forms.CheckBox();
            __ContactCheckToggle.CheckStateChanged += new EventHandler(ContactCheckToggle_CheckStateChanged);
            __ApplyHipotSettingsButton = new System.Windows.Forms.Button();
            __ApplyHipotSettingsButton.Click += new EventHandler(ApplyHipotSettingsButton_Click);
            _CurrentLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _CurrentLimitNumericLabel = new System.Windows.Forms.Label();
            _VoltageLevelNumericLabel = new System.Windows.Forms.Label();
            _VoltageLevelNumeric = new System.Windows.Forms.NumericUpDown();
            _ResistanceRangeNumericLabel = new System.Windows.Forms.Label();
            _ResistanceLowLimitNumericLabel = new System.Windows.Forms.Label();
            _ResistanceRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _ResistanceLowLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _DwellTimeNumericLabel = new System.Windows.Forms.Label();
            _DwellTimeNumeric = new System.Windows.Forms.NumericUpDown();
            _ApertureNumeric = new System.Windows.Forms.NumericUpDown();
            _ApertureNumericLabel = new System.Windows.Forms.Label();
            _StobeTabPage = new System.Windows.Forms.TabPage();
            _BinningLayout = new System.Windows.Forms.TableLayoutPanel();
            _TriggerToolStrip = new System.Windows.Forms.ToolStrip();
            __AwaitTriggerToolStripButton = new System.Windows.Forms.ToolStripButton();
            __AwaitTriggerToolStripButton.CheckedChanged += new EventHandler(AwaitTriggerToolStripButton_CheckedChanged);
            _WaitHourglassLabel = new System.Windows.Forms.ToolStripLabel();
            __AssertTriggerToolStripButton = new System.Windows.Forms.ToolStripButton();
            __AssertTriggerToolStripButton.Click += new EventHandler(AssertTriggerToolStripButton_Click);
            _TriggerActionToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _BinningGroupBox = new System.Windows.Forms.GroupBox();
            _ArmComboBoxLabel = new System.Windows.Forms.Label();
            _ArmSourceComboBox = new System.Windows.Forms.ComboBox();
            __ApplySotSettingsButton = new System.Windows.Forms.Button();
            __ApplySotSettingsButton.Click += new EventHandler(ApplySotSettingsButton_Click);
            _ContactCheckSupportLabel = new System.Windows.Forms.Label();
            _FailBitPatternNumeric = new System.Windows.Forms.NumericUpDown();
            _PassBitPatternNumeric = new System.Windows.Forms.NumericUpDown();
            _ContactCheckBitPatternNumeric = new System.Windows.Forms.NumericUpDown();
            _FailBitPatternNumericLabel = new System.Windows.Forms.Label();
            _PassBitPatternNumericLabel = new System.Windows.Forms.Label();
            _ContactCheckBitPatternNumericLabel = new System.Windows.Forms.Label();
            _EotStrobeDurationNumericLabel = new System.Windows.Forms.Label();
            _EotStrobeDurationNumeric = new System.Windows.Forms.NumericUpDown();
            _TriggerDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            _Tabs.SuspendLayout();
            _HipotTabPage.SuspendLayout();
            _HipotLayout.SuspendLayout();
            _HipotGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_CurrentLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_VoltageLevelNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceLowLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_DwellTimeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).BeginInit();
            _StobeTabPage.SuspendLayout();
            _BinningLayout.SuspendLayout();
            _TriggerToolStrip.SuspendLayout();
            _BinningGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_FailBitPatternNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_PassBitPatternNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ContactCheckBitPatternNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_EotStrobeDurationNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).BeginInit();
            SuspendLayout();
            // 
            // _Tabs
            // 
            _Tabs.Controls.Add(_HipotTabPage);
            _Tabs.Controls.Add(_StobeTabPage);
            _Tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            _Tabs.Location = new System.Drawing.Point(0, 0);
            _Tabs.Name = "_Tabs";
            _Tabs.SelectedIndex = 0;
            _Tabs.Size = new System.Drawing.Size(383, 326);
            _Tabs.TabIndex = 0;
            // 
            // _HipotTabPage
            // 
            _HipotTabPage.Controls.Add(_HipotLayout);
            _HipotTabPage.Location = new System.Drawing.Point(4, 26);
            _HipotTabPage.Name = "_HipotTabPage";
            _HipotTabPage.Padding = new System.Windows.Forms.Padding(3);
            _HipotTabPage.Size = new System.Drawing.Size(375, 296);
            _HipotTabPage.TabIndex = 0;
            _HipotTabPage.Text = "Hipot";
            _HipotTabPage.UseVisualStyleBackColor = true;
            // 
            // _HipotLayout
            // 
            _HipotLayout.ColumnCount = 3;
            _HipotLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _HipotLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _HipotLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _HipotLayout.Controls.Add(_HipotGroupBox, 1, 1);
            _HipotLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _HipotLayout.Location = new System.Drawing.Point(3, 3);
            _HipotLayout.Name = "_HipotLayout";
            _HipotLayout.RowCount = 3;
            _HipotLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _HipotLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _HipotLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _HipotLayout.Size = new System.Drawing.Size(369, 290);
            _HipotLayout.TabIndex = 5;
            // 
            // _HipotGroupBox
            // 
            _HipotGroupBox.Controls.Add(__ContactCheckToggle);
            _HipotGroupBox.Controls.Add(__ApplyHipotSettingsButton);
            _HipotGroupBox.Controls.Add(_CurrentLimitNumeric);
            _HipotGroupBox.Controls.Add(_CurrentLimitNumericLabel);
            _HipotGroupBox.Controls.Add(_VoltageLevelNumericLabel);
            _HipotGroupBox.Controls.Add(_VoltageLevelNumeric);
            _HipotGroupBox.Controls.Add(_ResistanceRangeNumericLabel);
            _HipotGroupBox.Controls.Add(_ResistanceLowLimitNumericLabel);
            _HipotGroupBox.Controls.Add(_ResistanceRangeNumeric);
            _HipotGroupBox.Controls.Add(_ResistanceLowLimitNumeric);
            _HipotGroupBox.Controls.Add(_DwellTimeNumericLabel);
            _HipotGroupBox.Controls.Add(_DwellTimeNumeric);
            _HipotGroupBox.Controls.Add(_ApertureNumeric);
            _HipotGroupBox.Controls.Add(_ApertureNumericLabel);
            _HipotGroupBox.Location = new System.Drawing.Point(43, 36);
            _HipotGroupBox.Name = "_HipotGroupBox";
            _HipotGroupBox.Size = new System.Drawing.Size(283, 217);
            _HipotGroupBox.TabIndex = 2;
            _HipotGroupBox.TabStop = false;
            _HipotGroupBox.Text = "HIPOT SETTINGS";
            // 
            // _ContactCheckToggle
            // 
            __ContactCheckToggle.AutoSize = true;
            __ContactCheckToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            __ContactCheckToggle.Location = new System.Drawing.Point(19, 187);
            __ContactCheckToggle.Name = "__ContactCheckToggle";
            __ContactCheckToggle.Size = new System.Drawing.Size(161, 21);
            __ContactCheckToggle.TabIndex = 13;
            __ContactCheckToggle.Text = "Contact check enabled:";
            __ContactCheckToggle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            __ContactCheckToggle.UseVisualStyleBackColor = true;
            // 
            // _ApplyHipotSettingsButton
            // 
            __ApplyHipotSettingsButton.Location = new System.Drawing.Point(197, 183);
            __ApplyHipotSettingsButton.Name = "__ApplyHipotSettingsButton";
            __ApplyHipotSettingsButton.Size = new System.Drawing.Size(75, 28);
            __ApplyHipotSettingsButton.TabIndex = 12;
            __ApplyHipotSettingsButton.Text = "Apply";
            __ApplyHipotSettingsButton.UseVisualStyleBackColor = true;
            // 
            // _CurrentLimitNumeric
            // 
            _CurrentLimitNumeric.DecimalPlaces = 1;
            _CurrentLimitNumeric.Location = new System.Drawing.Point(191, 102);
            _CurrentLimitNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _CurrentLimitNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _CurrentLimitNumeric.Name = "_CurrentLimitNumeric";
            _CurrentLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _CurrentLimitNumeric.Size = new System.Drawing.Size(79, 25);
            _CurrentLimitNumeric.TabIndex = 7;
            _CurrentLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _CurrentLimitNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _CurrentLimitNumericLabel
            // 
            _CurrentLimitNumericLabel.AutoSize = true;
            _CurrentLimitNumericLabel.Location = new System.Drawing.Point(76, 106);
            _CurrentLimitNumericLabel.Name = "_CurrentLimitNumericLabel";
            _CurrentLimitNumericLabel.Size = new System.Drawing.Size(112, 17);
            _CurrentLimitNumericLabel.TabIndex = 6;
            _CurrentLimitNumericLabel.Text = "Current Limit [uA]:";
            // 
            // _VoltageLevelNumericLabel
            // 
            _VoltageLevelNumericLabel.AutoSize = true;
            _VoltageLevelNumericLabel.Location = new System.Drawing.Point(80, 79);
            _VoltageLevelNumericLabel.Name = "_VoltageLevelNumericLabel";
            _VoltageLevelNumericLabel.Size = new System.Drawing.Size(108, 17);
            _VoltageLevelNumericLabel.TabIndex = 4;
            _VoltageLevelNumericLabel.Text = "Voltage Level [V]:";
            // 
            // _VoltageLevelNumeric
            // 
            _VoltageLevelNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _VoltageLevelNumeric.Location = new System.Drawing.Point(191, 75);
            _VoltageLevelNumeric.Maximum = new decimal(new int[] { 1100, 0, 0, 0 });
            _VoltageLevelNumeric.Name = "_VoltageLevelNumeric";
            _VoltageLevelNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _VoltageLevelNumeric.Size = new System.Drawing.Size(79, 25);
            _VoltageLevelNumeric.TabIndex = 5;
            _VoltageLevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _VoltageLevelNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _ResistanceRangeNumericLabel
            // 
            _ResistanceRangeNumericLabel.AutoSize = true;
            _ResistanceRangeNumericLabel.Location = new System.Drawing.Point(19, 160);
            _ResistanceRangeNumericLabel.Name = "_ResistanceRangeNumericLabel";
            _ResistanceRangeNumericLabel.Size = new System.Drawing.Size(169, 17);
            _ResistanceRangeNumericLabel.TabIndex = 10;
            _ResistanceRangeNumericLabel.Text = "Resistance Range [M Ohm]:";
            // 
            // _ResistanceLowLimitNumericLabel
            // 
            _ResistanceLowLimitNumericLabel.AutoSize = true;
            _ResistanceLowLimitNumericLabel.Location = new System.Drawing.Point(5, 133);
            _ResistanceLowLimitNumericLabel.Name = "_ResistanceLowLimitNumericLabel";
            _ResistanceLowLimitNumericLabel.Size = new System.Drawing.Size(183, 17);
            _ResistanceLowLimitNumericLabel.TabIndex = 8;
            _ResistanceLowLimitNumericLabel.Text = "Resistance low Limit [M Ohm]:";
            // 
            // _ResistanceRangeNumeric
            // 
            _ResistanceRangeNumeric.Increment = new decimal(new int[] { 1000000, 0, 0, 0 });
            _ResistanceRangeNumeric.Location = new System.Drawing.Point(191, 156);
            _ResistanceRangeNumeric.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            _ResistanceRangeNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _ResistanceRangeNumeric.Name = "_ResistanceRangeNumeric";
            _ResistanceRangeNumeric.Size = new System.Drawing.Size(79, 25);
            _ResistanceRangeNumeric.TabIndex = 11;
            _ResistanceRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ResistanceRangeNumeric.ThousandsSeparator = true;
            _ResistanceRangeNumeric.Value = new decimal(new int[] { 1000, 0, 0, 0 });
            // 
            // _ResistanceLowLimitNumeric
            // 
            _ResistanceLowLimitNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _ResistanceLowLimitNumeric.Location = new System.Drawing.Point(191, 129);
            _ResistanceLowLimitNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _ResistanceLowLimitNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _ResistanceLowLimitNumeric.Name = "_ResistanceLowLimitNumeric";
            _ResistanceLowLimitNumeric.Size = new System.Drawing.Size(79, 25);
            _ResistanceLowLimitNumeric.TabIndex = 9;
            _ResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ResistanceLowLimitNumeric.ThousandsSeparator = true;
            _ResistanceLowLimitNumeric.Value = new decimal(new int[] { 100, 0, 0, 0 });
            // 
            // _DwellTimeNumericLabel
            // 
            _DwellTimeNumericLabel.AutoSize = true;
            _DwellTimeNumericLabel.Location = new System.Drawing.Point(98, 52);
            _DwellTimeNumericLabel.Name = "_DwellTimeNumericLabel";
            _DwellTimeNumericLabel.Size = new System.Drawing.Size(90, 17);
            _DwellTimeNumericLabel.TabIndex = 2;
            _DwellTimeNumericLabel.Text = "Dwell time [S]:";
            // 
            // _DwellTimeNumeric
            // 
            _DwellTimeNumeric.DecimalPlaces = 2;
            _DwellTimeNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _DwellTimeNumeric.Location = new System.Drawing.Point(191, 48);
            _DwellTimeNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _DwellTimeNumeric.Name = "_DwellTimeNumeric";
            _DwellTimeNumeric.Size = new System.Drawing.Size(79, 25);
            _DwellTimeNumeric.TabIndex = 3;
            _DwellTimeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _DwellTimeNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _ApertureNumeric
            // 
            _ApertureNumeric.DecimalPlaces = 2;
            _ApertureNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ApertureNumeric.Location = new System.Drawing.Point(191, 21);
            _ApertureNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _ApertureNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 131072 });
            _ApertureNumeric.Name = "_ApertureNumeric";
            _ApertureNumeric.Size = new System.Drawing.Size(79, 25);
            _ApertureNumeric.TabIndex = 1;
            _ApertureNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ApertureNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _ApertureNumericLabel
            // 
            _ApertureNumericLabel.AutoSize = true;
            _ApertureNumericLabel.Location = new System.Drawing.Point(86, 25);
            _ApertureNumericLabel.Name = "_ApertureNumericLabel";
            _ApertureNumericLabel.Size = new System.Drawing.Size(102, 17);
            _ApertureNumericLabel.TabIndex = 0;
            _ApertureNumericLabel.Text = "Aperture [NPLC]";
            // 
            // _StobeTabPage
            // 
            _StobeTabPage.Controls.Add(_BinningLayout);
            _StobeTabPage.Location = new System.Drawing.Point(4, 26);
            _StobeTabPage.Name = "_StobeTabPage";
            _StobeTabPage.Padding = new System.Windows.Forms.Padding(3);
            _StobeTabPage.Size = new System.Drawing.Size(375, 296);
            _StobeTabPage.TabIndex = 1;
            _StobeTabPage.Text = "Strobe";
            _StobeTabPage.UseVisualStyleBackColor = true;
            // 
            // _BinningLayout
            // 
            _BinningLayout.ColumnCount = 3;
            _BinningLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _BinningLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _BinningLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _BinningLayout.Controls.Add(_TriggerToolStrip, 1, 3);
            _BinningLayout.Controls.Add(_BinningGroupBox, 1, 1);
            _BinningLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _BinningLayout.Location = new System.Drawing.Point(3, 3);
            _BinningLayout.Name = "_BinningLayout";
            _BinningLayout.RowCount = 5;
            _BinningLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
            _BinningLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _BinningLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
            _BinningLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _BinningLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333f));
            _BinningLayout.Size = new System.Drawing.Size(369, 290);
            _BinningLayout.TabIndex = 2;
            // 
            // _TriggerToolStrip
            // 
            _TriggerToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _TriggerToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __AwaitTriggerToolStripButton, _WaitHourglassLabel, __AssertTriggerToolStripButton, _TriggerActionToolStripLabel });
            _TriggerToolStrip.Location = new System.Drawing.Point(20, 243);
            _TriggerToolStrip.Name = "_TriggerToolStrip";
            _TriggerToolStrip.Size = new System.Drawing.Size(270, 25);
            _TriggerToolStrip.Stretch = true;
            _TriggerToolStrip.TabIndex = 6;
            _TriggerToolStrip.Text = "Trigger Tool Strip";
            // 
            // _AwaitTriggerToolStripButton
            // 
            __AwaitTriggerToolStripButton.CheckOnClick = true;
            __AwaitTriggerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __AwaitTriggerToolStripButton.Image = (System.Drawing.Image)resources.GetObject("_AwaitTriggerToolStripButton.Image");
            __AwaitTriggerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AwaitTriggerToolStripButton.Name = "__AwaitTriggerToolStripButton";
            __AwaitTriggerToolStripButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            __AwaitTriggerToolStripButton.Size = new System.Drawing.Size(34, 22);
            __AwaitTriggerToolStripButton.Text = "Arm";
            __AwaitTriggerToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText;
            __AwaitTriggerToolStripButton.ToolTipText = "Depress to wait for trigger or release to abort.";
            // 
            // _WaitHourglassLabel
            // 
            _WaitHourglassLabel.Name = "_WaitHourglassLabel";
            _WaitHourglassLabel.Size = new System.Drawing.Size(20, 22);
            _WaitHourglassLabel.Text = "[-]";
            _WaitHourglassLabel.ToolTipText = "Waiting for trigger";
            // 
            // _AssertTriggerToolStripButton
            // 
            __AssertTriggerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __AssertTriggerToolStripButton.Image = (System.Drawing.Image)resources.GetObject("_AssertTriggerToolStripButton.Image");
            __AssertTriggerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AssertTriggerToolStripButton.Margin = new System.Windows.Forms.Padding(6, 1, 0, 2);
            __AssertTriggerToolStripButton.Name = "__AssertTriggerToolStripButton";
            __AssertTriggerToolStripButton.Size = new System.Drawing.Size(38, 22);
            __AssertTriggerToolStripButton.Text = "*TRG";
            // 
            // _TriggerActionToolStripLabel
            // 
            _TriggerActionToolStripLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TriggerActionToolStripLabel.Name = "_TriggerActionToolStripLabel";
            _TriggerActionToolStripLabel.Size = new System.Drawing.Size(160, 22);
            _TriggerActionToolStripLabel.Text = "Click wait to 'Arm' for trigger";
            _TriggerActionToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _BinningGroupBox
            // 
            _BinningGroupBox.Controls.Add(_TriggerDelayNumeric);
            _BinningGroupBox.Controls.Add(_TriggerDelayNumericLabel);
            _BinningGroupBox.Controls.Add(_ArmComboBoxLabel);
            _BinningGroupBox.Controls.Add(_ArmSourceComboBox);
            _BinningGroupBox.Controls.Add(__ApplySotSettingsButton);
            _BinningGroupBox.Controls.Add(_ContactCheckSupportLabel);
            _BinningGroupBox.Controls.Add(_FailBitPatternNumeric);
            _BinningGroupBox.Controls.Add(_PassBitPatternNumeric);
            _BinningGroupBox.Controls.Add(_ContactCheckBitPatternNumeric);
            _BinningGroupBox.Controls.Add(_FailBitPatternNumericLabel);
            _BinningGroupBox.Controls.Add(_PassBitPatternNumericLabel);
            _BinningGroupBox.Controls.Add(_ContactCheckBitPatternNumericLabel);
            _BinningGroupBox.Controls.Add(_EotStrobeDurationNumericLabel);
            _BinningGroupBox.Controls.Add(_EotStrobeDurationNumeric);
            _BinningGroupBox.Location = new System.Drawing.Point(23, 25);
            _BinningGroupBox.Name = "_BinningGroupBox";
            _BinningGroupBox.Size = new System.Drawing.Size(323, 193);
            _BinningGroupBox.TabIndex = 2;
            _BinningGroupBox.TabStop = false;
            _BinningGroupBox.Text = "SOT, EOT, Bin";
            // 
            // _ArmComboBoxLabel
            // 
            _ArmComboBoxLabel.AutoSize = true;
            _ArmComboBoxLabel.Location = new System.Drawing.Point(73, 23);
            _ArmComboBoxLabel.Name = "_ArmComboBoxLabel";
            _ArmComboBoxLabel.Size = new System.Drawing.Size(91, 17);
            _ArmComboBoxLabel.TabIndex = 0;
            _ArmComboBoxLabel.Text = "Trigger signal:";
            // 
            // _ArmSourceComboBox
            // 
            _ArmSourceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _ArmSourceComboBox.FormattingEnabled = true;
            _ArmSourceComboBox.Location = new System.Drawing.Point(167, 19);
            _ArmSourceComboBox.Name = "_ArmSourceComboBox";
            _ArmSourceComboBox.Size = new System.Drawing.Size(149, 25);
            _ArmSourceComboBox.TabIndex = 1;
            // 
            // _ApplySotSettingsButton
            // 
            __ApplySotSettingsButton.Location = new System.Drawing.Point(253, 152);
            __ApplySotSettingsButton.Name = "__ApplySotSettingsButton";
            __ApplySotSettingsButton.Size = new System.Drawing.Size(57, 28);
            __ApplySotSettingsButton.TabIndex = 11;
            __ApplySotSettingsButton.Text = "Apply";
            __ApplySotSettingsButton.UseVisualStyleBackColor = true;
            // 
            // _ContactCheckSupportLabel
            // 
            _ContactCheckSupportLabel.AutoSize = true;
            _ContactCheckSupportLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ContactCheckSupportLabel.Location = new System.Drawing.Point(3, 173);
            _ContactCheckSupportLabel.Name = "_ContactCheckSupportLabel";
            _ContactCheckSupportLabel.Size = new System.Drawing.Size(0, 17);
            _ContactCheckSupportLabel.TabIndex = 9;
            _ContactCheckSupportLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _FailBitPatternNumeric
            // 
            _FailBitPatternNumeric.Location = new System.Drawing.Point(167, 100);
            _FailBitPatternNumeric.Maximum = new decimal(new int[] { 7, 0, 0, 0 });
            _FailBitPatternNumeric.Name = "_FailBitPatternNumeric";
            _FailBitPatternNumeric.Size = new System.Drawing.Size(35, 25);
            _FailBitPatternNumeric.TabIndex = 7;
            _FailBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _FailBitPatternNumeric.Value = new decimal(new int[] { 4, 0, 0, 0 });
            // 
            // _PassBitPatternNumeric
            // 
            _PassBitPatternNumeric.Location = new System.Drawing.Point(167, 73);
            _PassBitPatternNumeric.Maximum = new decimal(new int[] { 7, 0, 0, 0 });
            _PassBitPatternNumeric.Name = "_PassBitPatternNumeric";
            _PassBitPatternNumeric.Size = new System.Drawing.Size(35, 25);
            _PassBitPatternNumeric.TabIndex = 5;
            _PassBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _PassBitPatternNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _ContactCheckBitPatternNumeric
            // 
            _ContactCheckBitPatternNumeric.Location = new System.Drawing.Point(167, 127);
            _ContactCheckBitPatternNumeric.Maximum = new decimal(new int[] { 7, 0, 0, 0 });
            _ContactCheckBitPatternNumeric.Name = "_ContactCheckBitPatternNumeric";
            _ContactCheckBitPatternNumeric.Size = new System.Drawing.Size(35, 25);
            _ContactCheckBitPatternNumeric.TabIndex = 9;
            _ContactCheckBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ContactCheckBitPatternNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _FailBitPatternNumericLabel
            // 
            _FailBitPatternNumericLabel.AutoSize = true;
            _FailBitPatternNumericLabel.Location = new System.Drawing.Point(69, 104);
            _FailBitPatternNumericLabel.Name = "_FailBitPatternNumericLabel";
            _FailBitPatternNumericLabel.Size = new System.Drawing.Size(95, 17);
            _FailBitPatternNumericLabel.TabIndex = 6;
            _FailBitPatternNumericLabel.Text = "Fail bit pattern:";
            _FailBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PassBitPatternNumericLabel
            // 
            _PassBitPatternNumericLabel.AutoSize = true;
            _PassBitPatternNumericLabel.Location = new System.Drawing.Point(62, 77);
            _PassBitPatternNumericLabel.Name = "_PassBitPatternNumericLabel";
            _PassBitPatternNumericLabel.Size = new System.Drawing.Size(102, 17);
            _PassBitPatternNumericLabel.TabIndex = 4;
            _PassBitPatternNumericLabel.Text = "Pass bit pattern:";
            _PassBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ContactCheckBitPatternNumericLabel
            // 
            _ContactCheckBitPatternNumericLabel.AutoSize = true;
            _ContactCheckBitPatternNumericLabel.Location = new System.Drawing.Point(8, 131);
            _ContactCheckBitPatternNumericLabel.Name = "_ContactCheckBitPatternNumericLabel";
            _ContactCheckBitPatternNumericLabel.Size = new System.Drawing.Size(156, 17);
            _ContactCheckBitPatternNumericLabel.TabIndex = 8;
            _ContactCheckBitPatternNumericLabel.Text = "Contact check bit pattern:";
            _ContactCheckBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _EotStrobeDurationNumericLabel
            // 
            _EotStrobeDurationNumericLabel.AutoSize = true;
            _EotStrobeDurationNumericLabel.Location = new System.Drawing.Point(4, 50);
            _EotStrobeDurationNumericLabel.Name = "_EotStrobeDurationNumericLabel";
            _EotStrobeDurationNumericLabel.Size = new System.Drawing.Size(160, 17);
            _EotStrobeDurationNumericLabel.TabIndex = 2;
            _EotStrobeDurationNumericLabel.Text = "EOT Strobe duration [mS]:";
            // 
            // _EotStrobeDurationNumeric
            // 
            _EotStrobeDurationNumeric.DecimalPlaces = 3;
            _EotStrobeDurationNumeric.Location = new System.Drawing.Point(167, 46);
            _EotStrobeDurationNumeric.Name = "_EotStrobeDurationNumeric";
            _EotStrobeDurationNumeric.Size = new System.Drawing.Size(71, 25);
            _EotStrobeDurationNumeric.TabIndex = 3;
            _EotStrobeDurationNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _EotStrobeDurationNumeric.Value = new decimal(new int[] { 1, 0, 0, 131072 });
            // 
            // _TriggerDelayNumeric
            // 
            _TriggerDelayNumeric.DecimalPlaces = 3;
            _TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerDelayNumeric.Location = new System.Drawing.Point(167, 156);
            _TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
            _TriggerDelayNumeric.Size = new System.Drawing.Size(56, 25);
            _TriggerDelayNumeric.TabIndex = 24;
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(57, 160);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(107, 17);
            _TriggerDelayNumericLabel.TabIndex = 23;
            _TriggerDelayNumericLabel.Text = "Trigger Delay [s]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // HipotView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Tabs);
            Name = "HipotView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            _Tabs.ResumeLayout(false);
            _HipotTabPage.ResumeLayout(false);
            _HipotLayout.ResumeLayout(false);
            _HipotGroupBox.ResumeLayout(false);
            _HipotGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_CurrentLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_VoltageLevelNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ResistanceLowLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_DwellTimeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).EndInit();
            _StobeTabPage.ResumeLayout(false);
            _BinningLayout.ResumeLayout(false);
            _BinningLayout.PerformLayout();
            _TriggerToolStrip.ResumeLayout(false);
            _TriggerToolStrip.PerformLayout();
            _BinningGroupBox.ResumeLayout(false);
            _BinningGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_FailBitPatternNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_PassBitPatternNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ContactCheckBitPatternNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_EotStrobeDurationNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TabControl _Tabs;
        private System.Windows.Forms.TabPage _HipotTabPage;
        private System.Windows.Forms.TabPage _StobeTabPage;
        private System.Windows.Forms.TableLayoutPanel _BinningLayout;
        private System.Windows.Forms.ToolStrip _TriggerToolStrip;
        private System.Windows.Forms.ToolStripButton __AwaitTriggerToolStripButton;

        private System.Windows.Forms.ToolStripButton _AwaitTriggerToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AwaitTriggerToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AwaitTriggerToolStripButton != null)
                {
                    __AwaitTriggerToolStripButton.CheckedChanged -= AwaitTriggerToolStripButton_CheckedChanged;
                }

                __AwaitTriggerToolStripButton = value;
                if (__AwaitTriggerToolStripButton != null)
                {
                    __AwaitTriggerToolStripButton.CheckedChanged += AwaitTriggerToolStripButton_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _WaitHourglassLabel;
        private System.Windows.Forms.ToolStripButton __AssertTriggerToolStripButton;

        private System.Windows.Forms.ToolStripButton _AssertTriggerToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AssertTriggerToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AssertTriggerToolStripButton != null)
                {
                    __AssertTriggerToolStripButton.Click -= AssertTriggerToolStripButton_Click;
                }

                __AssertTriggerToolStripButton = value;
                if (__AssertTriggerToolStripButton != null)
                {
                    __AssertTriggerToolStripButton.Click += AssertTriggerToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _TriggerActionToolStripLabel;
        private System.Windows.Forms.GroupBox _BinningGroupBox;
        private System.Windows.Forms.Label _ArmComboBoxLabel;
        private System.Windows.Forms.ComboBox _ArmSourceComboBox;
        private System.Windows.Forms.Button __ApplySotSettingsButton;

        private System.Windows.Forms.Button _ApplySotSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySotSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySotSettingsButton != null)
                {
                    __ApplySotSettingsButton.Click -= ApplySotSettingsButton_Click;
                }

                __ApplySotSettingsButton = value;
                if (__ApplySotSettingsButton != null)
                {
                    __ApplySotSettingsButton.Click += ApplySotSettingsButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _ContactCheckSupportLabel;
        private System.Windows.Forms.NumericUpDown _FailBitPatternNumeric;
        private System.Windows.Forms.NumericUpDown _PassBitPatternNumeric;
        private System.Windows.Forms.NumericUpDown _ContactCheckBitPatternNumeric;
        private System.Windows.Forms.Label _FailBitPatternNumericLabel;
        private System.Windows.Forms.Label _PassBitPatternNumericLabel;
        private System.Windows.Forms.Label _ContactCheckBitPatternNumericLabel;
        private System.Windows.Forms.Label _EotStrobeDurationNumericLabel;
        private System.Windows.Forms.NumericUpDown _EotStrobeDurationNumeric;
        private System.Windows.Forms.TableLayoutPanel _HipotLayout;
        private System.Windows.Forms.GroupBox _HipotGroupBox;
        private System.Windows.Forms.CheckBox __ContactCheckToggle;

        private System.Windows.Forms.CheckBox _ContactCheckToggle
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ContactCheckToggle;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ContactCheckToggle != null)
                {
                    __ContactCheckToggle.CheckStateChanged -= ContactCheckToggle_CheckStateChanged;
                }

                __ContactCheckToggle = value;
                if (__ContactCheckToggle != null)
                {
                    __ContactCheckToggle.CheckStateChanged += ContactCheckToggle_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.Button __ApplyHipotSettingsButton;

        private System.Windows.Forms.Button _ApplyHipotSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyHipotSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyHipotSettingsButton != null)
                {
                    __ApplyHipotSettingsButton.Click -= ApplyHipotSettingsButton_Click;
                }

                __ApplyHipotSettingsButton = value;
                if (__ApplyHipotSettingsButton != null)
                {
                    __ApplyHipotSettingsButton.Click += ApplyHipotSettingsButton_Click;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _CurrentLimitNumeric;
        private System.Windows.Forms.Label _CurrentLimitNumericLabel;
        private System.Windows.Forms.Label _VoltageLevelNumericLabel;
        private System.Windows.Forms.NumericUpDown _VoltageLevelNumeric;
        private System.Windows.Forms.Label _ResistanceRangeNumericLabel;
        private System.Windows.Forms.Label _ResistanceLowLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ResistanceRangeNumeric;
        private System.Windows.Forms.NumericUpDown _ResistanceLowLimitNumeric;
        private System.Windows.Forms.Label _DwellTimeNumericLabel;
        private System.Windows.Forms.NumericUpDown _DwellTimeNumeric;
        private System.Windows.Forms.NumericUpDown _ApertureNumeric;
        private System.Windows.Forms.Label _ApertureNumericLabel;
        private System.Windows.Forms.NumericUpDown _TriggerDelayNumeric;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
    }
}