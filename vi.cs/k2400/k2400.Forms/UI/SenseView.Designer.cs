﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400.Forms
{
    [DesignerGenerated()]
    public partial class SenseView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _EnabledSenseFunctionsListBoxLabel = new System.Windows.Forms.Label();
            _EnabledSenseFunctionsListBox = new System.Windows.Forms.CheckedListBox();
            _ApplySenseFunctionButton = new System.Windows.Forms.Button();
            _ConcurrentSenseCheckBox = new System.Windows.Forms.CheckBox();
            _FourWireSenseCheckBox = new System.Windows.Forms.CheckBox();
            _SenseRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _NplcNumeric = new System.Windows.Forms.NumericUpDown();
            _SenseRangeNumericLabel = new System.Windows.Forms.Label();
            _NplcNumericLabel = new System.Windows.Forms.Label();
            __SenseFunctionComboBox = new System.Windows.Forms.ComboBox();
            __SenseFunctionComboBox.SelectedIndexChanged += new EventHandler(SenseFunctionComboBox_SelectedIndexChanged);
            _SenseFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _SenseAutoRangeToggle = new System.Windows.Forms.CheckBox();
            __ApplySenseSettingsButton = new System.Windows.Forms.Button();
            __ApplySenseSettingsButton.Click += new EventHandler(ApplySenseSettingsButton_Click);
            _Panel = new System.Windows.Forms.Panel();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_NplcNumeric).BeginInit();
            _Panel.SuspendLayout();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _EnabledSenseFunctionsListBoxLabel
            // 
            _EnabledSenseFunctionsListBoxLabel.AutoSize = true;
            _EnabledSenseFunctionsListBoxLabel.Location = new System.Drawing.Point(16, 5);
            _EnabledSenseFunctionsListBoxLabel.Name = "_EnabledSenseFunctionsListBoxLabel";
            _EnabledSenseFunctionsListBoxLabel.Size = new System.Drawing.Size(65, 17);
            _EnabledSenseFunctionsListBoxLabel.TabIndex = 40;
            _EnabledSenseFunctionsListBoxLabel.Text = "Functions:";
            // 
            // _EnabledSenseFunctionsListBox
            // 
            _EnabledSenseFunctionsListBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _EnabledSenseFunctionsListBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EnabledSenseFunctionsListBox.FormattingEnabled = true;
            _EnabledSenseFunctionsListBox.Location = new System.Drawing.Point(84, 5);
            _EnabledSenseFunctionsListBox.Name = "_EnabledSenseFunctionsListBox";
            _EnabledSenseFunctionsListBox.Size = new System.Drawing.Size(144, 64);
            _EnabledSenseFunctionsListBox.TabIndex = 39;
            // 
            // _ApplySenseFunctionButton
            // 
            _ApplySenseFunctionButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            _ApplySenseFunctionButton.Location = new System.Drawing.Point(291, 56);
            _ApplySenseFunctionButton.Name = "_ApplySenseFunctionButton";
            _ApplySenseFunctionButton.Size = new System.Drawing.Size(53, 30);
            _ApplySenseFunctionButton.TabIndex = 38;
            _ApplySenseFunctionButton.Text = "Apply";
            _ApplySenseFunctionButton.UseVisualStyleBackColor = true;
            // 
            // _ConcurrentSenseCheckBox
            // 
            _ConcurrentSenseCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            _ConcurrentSenseCheckBox.AutoSize = true;
            _ConcurrentSenseCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ConcurrentSenseCheckBox.Location = new System.Drawing.Point(236, 5);
            _ConcurrentSenseCheckBox.Name = "_ConcurrentSenseCheckBox";
            _ConcurrentSenseCheckBox.Size = new System.Drawing.Size(95, 21);
            _ConcurrentSenseCheckBox.TabIndex = 37;
            _ConcurrentSenseCheckBox.Text = "Concurrent";
            _ConcurrentSenseCheckBox.UseVisualStyleBackColor = true;
            // 
            // _FourWireSenseCheckBox
            // 
            _FourWireSenseCheckBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            _FourWireSenseCheckBox.AutoSize = true;
            _FourWireSenseCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FourWireSenseCheckBox.Location = new System.Drawing.Point(236, 31);
            _FourWireSenseCheckBox.Name = "_FourWireSenseCheckBox";
            _FourWireSenseCheckBox.Size = new System.Drawing.Size(88, 21);
            _FourWireSenseCheckBox.TabIndex = 36;
            _FourWireSenseCheckBox.Text = "Four Wire";
            _FourWireSenseCheckBox.UseVisualStyleBackColor = true;
            // 
            // _SenseRangeNumeric
            // 
            _SenseRangeNumeric.DecimalPlaces = 3;
            _SenseRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SenseRangeNumeric.Location = new System.Drawing.Point(119, 150);
            _SenseRangeNumeric.Maximum = new decimal(new int[] { 1010, 0, 0, 0 });
            _SenseRangeNumeric.Name = "_SenseRangeNumeric";
            _SenseRangeNumeric.Size = new System.Drawing.Size(76, 25);
            _SenseRangeNumeric.TabIndex = 33;
            _SenseRangeNumeric.Value = new decimal(new int[] { 105, 0, 0, 196608 });
            // 
            // _NplcNumeric
            // 
            _NplcNumeric.DecimalPlaces = 3;
            _NplcNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _NplcNumeric.Location = new System.Drawing.Point(119, 179);
            _NplcNumeric.Maximum = new decimal(new int[] { 50, 0, 0, 0 });
            _NplcNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 131072 });
            _NplcNumeric.Name = "_NplcNumeric";
            _NplcNumeric.Size = new System.Drawing.Size(76, 25);
            _NplcNumeric.TabIndex = 31;
            _NplcNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _SenseRangeNumericLabel
            // 
            _SenseRangeNumericLabel.AutoSize = true;
            _SenseRangeNumericLabel.Location = new System.Drawing.Point(48, 153);
            _SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel";
            _SenseRangeNumericLabel.Size = new System.Drawing.Size(68, 17);
            _SenseRangeNumericLabel.TabIndex = 32;
            _SenseRangeNumericLabel.Text = "Range [V]:";
            _SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _NplcNumericLabel
            // 
            _NplcNumericLabel.AutoSize = true;
            _NplcNumericLabel.Location = new System.Drawing.Point(11, 183);
            _NplcNumericLabel.Name = "_NplcNumericLabel";
            _NplcNumericLabel.Size = new System.Drawing.Size(105, 17);
            _NplcNumericLabel.TabIndex = 30;
            _NplcNumericLabel.Text = "Aperture [NPLC]:";
            _NplcNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseFunctionComboBox
            // 
            __SenseFunctionComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            __SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __SenseFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __SenseFunctionComboBox.Items.AddRange(new object[] { "I", "V" });
            __SenseFunctionComboBox.Location = new System.Drawing.Point(119, 119);
            __SenseFunctionComboBox.Name = "__SenseFunctionComboBox";
            __SenseFunctionComboBox.Size = new System.Drawing.Size(187, 25);
            __SenseFunctionComboBox.TabIndex = 29;
            // 
            // _SenseFunctionComboBoxLabel
            // 
            _SenseFunctionComboBoxLabel.AutoSize = true;
            _SenseFunctionComboBoxLabel.Location = new System.Drawing.Point(60, 123);
            _SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel";
            _SenseFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SenseFunctionComboBoxLabel.TabIndex = 28;
            _SenseFunctionComboBoxLabel.Text = "Function:";
            _SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseAutoRangeToggle
            // 
            _SenseAutoRangeToggle.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _SenseAutoRangeToggle.Location = new System.Drawing.Point(201, 152);
            _SenseAutoRangeToggle.Name = "_SenseAutoRangeToggle";
            _SenseAutoRangeToggle.Size = new System.Drawing.Size(103, 21);
            _SenseAutoRangeToggle.TabIndex = 34;
            _SenseAutoRangeToggle.Text = "Auto Range";
            _SenseAutoRangeToggle.UseVisualStyleBackColor = true;
            // 
            // _ApplySenseSettingsButton
            // 
            __ApplySenseSettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __ApplySenseSettingsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplySenseSettingsButton.Location = new System.Drawing.Point(293, 176);
            __ApplySenseSettingsButton.Name = "__ApplySenseSettingsButton";
            __ApplySenseSettingsButton.Size = new System.Drawing.Size(58, 30);
            __ApplySenseSettingsButton.TabIndex = 35;
            __ApplySenseSettingsButton.Text = "&Apply";
            __ApplySenseSettingsButton.UseVisualStyleBackColor = true;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_EnabledSenseFunctionsListBoxLabel);
            _Panel.Controls.Add(_EnabledSenseFunctionsListBox);
            _Panel.Controls.Add(_ApplySenseFunctionButton);
            _Panel.Controls.Add(_ConcurrentSenseCheckBox);
            _Panel.Controls.Add(_FourWireSenseCheckBox);
            _Panel.Controls.Add(_SenseRangeNumeric);
            _Panel.Controls.Add(_NplcNumeric);
            _Panel.Controls.Add(_SenseRangeNumericLabel);
            _Panel.Controls.Add(_NplcNumericLabel);
            _Panel.Controls.Add(__SenseFunctionComboBox);
            _Panel.Controls.Add(_SenseFunctionComboBoxLabel);
            _Panel.Controls.Add(_SenseAutoRangeToggle);
            _Panel.Controls.Add(__ApplySenseSettingsButton);
            _Panel.Location = new System.Drawing.Point(8, 51);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(363, 219);
            _Panel.TabIndex = 0;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 1;
            // 
            // SenseView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "SenseView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_NplcNumeric).EndInit();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.Label _EnabledSenseFunctionsListBoxLabel;
        private System.Windows.Forms.CheckedListBox _EnabledSenseFunctionsListBox;
        private System.Windows.Forms.Button _ApplySenseFunctionButton;
        private System.Windows.Forms.CheckBox _ConcurrentSenseCheckBox;
        private System.Windows.Forms.CheckBox _FourWireSenseCheckBox;
        private System.Windows.Forms.NumericUpDown _SenseRangeNumeric;
        private System.Windows.Forms.NumericUpDown _NplcNumeric;
        private System.Windows.Forms.Label _SenseRangeNumericLabel;
        private System.Windows.Forms.Label _NplcNumericLabel;
        private System.Windows.Forms.ComboBox __SenseFunctionComboBox;

        private System.Windows.Forms.ComboBox _SenseFunctionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SenseFunctionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged -= SenseFunctionComboBox_SelectedIndexChanged;
                }

                __SenseFunctionComboBox = value;
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged += SenseFunctionComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.Label _SenseFunctionComboBoxLabel;
        private System.Windows.Forms.CheckBox _SenseAutoRangeToggle;
        private System.Windows.Forms.Button __ApplySenseSettingsButton;

        private System.Windows.Forms.Button _ApplySenseSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySenseSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click -= ApplySenseSettingsButton_Click;
                }

                __ApplySenseSettingsButton = value;
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click += ApplySenseSettingsButton_Click;
                }
            }
        }
    }
}