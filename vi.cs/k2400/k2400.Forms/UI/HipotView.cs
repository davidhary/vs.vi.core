using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2400.Forms
{

    /// <summary> A Hipot view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class HipotView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public HipotView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ContactCheckToggle.Name = "_ContactCheckToggle";
            this.__ApplyHipotSettingsButton.Name = "_ApplyHipotSettingsButton";
            this.__AwaitTriggerToolStripButton.Name = "_AwaitTriggerToolStripButton";
            this.__AssertTriggerToolStripButton.Name = "_AssertTriggerToolStripButton";
            this.__ApplySotSettingsButton.Name = "_ApplySotSettingsButton";
        }

        /// <summary> Creates a new <see cref="HipotView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="HipotView"/>. </returns>
        public static HipotView Create()
        {
            HipotView view = null;
            try
            {
                view = new HipotView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2400Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2400Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.AssignInsulationTest( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
                this.AssignInsulationTest();
            }

            this.BindArmLayerSubsystem( value );
            this.BindContactCheckLimit( value );
            this.BindSystemSubsystem( value );
            this.BindTriggerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2400Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTACT CHECK LIMIT "

        /// <summary> Gets the contact check limit. </summary>
        /// <value> The contact check limit. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ContactCheckLimit ContactCheckLimit { get; private set; }

        /// <summary> Bind contact check limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindContactCheckLimit( K2400Device device )
        {
            if ( this.ContactCheckLimit is object )
            {
                this.BindLimit( false, this.ContactCheckLimit );
                this.ContactCheckLimit = null;
            }

            if ( device is object )
            {
                this.ContactCheckLimit = device.ContactCheckLimit;
                this.BindLimit( true, this.ContactCheckLimit );
            }
        }

        /// <summary> Bind limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">   True to add. </param>
        /// <param name="limit"> The limit. </param>
        private void BindLimit( bool add, ContactCheckLimit limit )
        {
            if ( add )
            {
                limit.PropertyChanged += this.ContactCheckLimitPropertyChanged;
            }
            else
            {
                limit.PropertyChanged -= this.ContactCheckLimitPropertyChanged;
            }
        }

        /// <summary> Handle the contact check property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ContactCheckLimit subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.ContactCheckLimit.FailureBits ):
                    {
                        if ( subsystem.FailureBits.HasValue )
                            _ = this._ContactCheckBitPatternNumeric.ValueSetter( ( decimal ) subsystem.FailureBits.Value );
                        break;
                    }
            }
        }

        /// <summary> Contact Check Limit subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ContactCheckLimitPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ContactCheckLimit )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ContactCheckLimitPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ContactCheckLimit, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " ARM LAYER "

        /// <summary> Gets the ArmLayer subsystem. </summary>
        /// <value> The ArmLayer subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ArmLayerSubsystem ArmLayerSubsystem { get; private set; }

        /// <summary> Bind ArmLayer subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindArmLayerSubsystem( K2400Device device )
        {
            if ( this.ArmLayerSubsystem is object )
            {
                this.BindSubsystem( false, this.ArmLayerSubsystem );
                this.ArmLayerSubsystem = null;
            }

            if ( device is object )
            {
                this.ArmLayerSubsystem = device.ArmLayerSubsystem;
                this.BindSubsystem( true, this.ArmLayerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ArmLayerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ArmLayerSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.ArmLayerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Arm subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ArmLayerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.ArmLayerSubsystem.InputLineNumber ):
                    {
                        break;
                    }

                case nameof( K2400.ArmLayerSubsystem.ArmSource ):
                    {
                        this._ArmSourceComboBox.SelectedItem = subsystem.ArmSource.Value.ValueDescriptionPair();
                        break;
                    }

                case nameof( K2400.ArmLayerSubsystem.SupportedArmSources ):
                    {
                        if ( this.Device is object && subsystem.SupportedArmSources != ArmSources.None )
                        {
                            int selectedIndex = this._ArmSourceComboBox.SelectedIndex;
                            this._ArmSourceComboBox.DataSource = null;
                            this._ArmSourceComboBox.Items.Clear();
                            this._ArmSourceComboBox.DataSource = typeof( ArmSources ).EnumValues().IncludeFilter( ( long ) subsystem.SupportedArmSources ).ValueDescriptionPairs().ToList();
                            this._ArmSourceComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                            this._ArmSourceComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                            if ( this._ArmSourceComboBox.Items.Count > 0 )
                            {
                                this._ArmSourceComboBox.SelectedIndex = Math.Max( selectedIndex, 0 );
                            }
                        }

                        break;
                    }
            }
        }

        /// <summary> Arm subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inArmion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ArmLayerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ArmLayerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ArmLayerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ArmLayerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets the selected arm source. </summary>
        /// <value> The selected arm source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private ArmSources SelectedArmSource => ( ArmSources ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ArmSourceComboBox.SelectedItem).Key );

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System subsystem. </summary>
        /// <value> The System subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSystemSubsystem( K2400Device device )
        {
            if ( this.SystemSubsystem is object )
            {
                this.BindSubsystem( false, this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            if ( device is object )
            {
                this.SystemSubsystem = device.SystemSubsystem;
                this.BindSubsystem( true, this.SystemSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SystemSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
            }
        }

        /// <summary> Contact check enabled menu item check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ContactCheckToggle_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.InfoProvider.Clear();
            ToolStripMenuItem menuItem = sender as ToolStripMenuItem;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying enabling contact check";
                if ( !this.Device.SystemSubsystem.ContactCheckEnabled.HasValue || this.Device.SystemSubsystem.ContactCheckEnabled.Value != menuItem.Checked )
                {
                    this._ContactCheckToggle.CheckState = this.Device.SystemSubsystem.ContactCheckEnabled.ToCheckState();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Handle the System subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.SystemSubsystem.ContactCheckEnabled ):
                    {
                        this._ContactCheckToggle.CheckState = subsystem.ContactCheckEnabled.ToCheckState();
                        break;
                    }

                case nameof( K2400.SystemSubsystem.ContactCheckSupported ):
                    {
                        this._ContactCheckToggle.Enabled = subsystem.ContactCheckSupported.GetValueOrDefault( false );
                        this._ContactCheckBitPatternNumeric.Enabled = subsystem.ContactCheckSupported.GetValueOrDefault( false );
                        break;
                    }
            }

            Application.DoEvents();
        }

        /// <summary> System subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SystemSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SystemSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( K2400Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2400.TriggerSubsystem.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                            _ = this._TriggerDelayNumeric.ValueSetter( subsystem.Delay.Value.TotalMilliseconds );
                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inTriggerion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> The device access locker. </summary>
        private readonly object _DeviceAccessLocker = new();

        private Timer _MeterTimerInternal;

        private Timer MeterTimerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MeterTimerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MeterTimerInternal != null )
                {
                    this._MeterTimerInternal.Tick -= this.MeterTimer_Tick;
                }

                this._MeterTimerInternal = value;
                if ( this._MeterTimerInternal != null )
                {
                    this._MeterTimerInternal.Tick += this.MeterTimer_Tick;
                }
            }
        }

        /// <summary> Aborts the measurement cycle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AbortMeasurementThis()
        {
            this.MeterTimerInternal.Enabled = false;
            this.Device.TriggerSubsystem.Abort();
        }


        /// <summary> Aborts the measurement cycle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void AbortMeasurement()
        {
            lock ( this._DeviceAccessLocker )
                this.AbortMeasurementThis();
        }

        /// <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AssertTriggerToolStripButton_Click( object sender, EventArgs e )
        {
            this.InfoProvider.Clear();
            try
            {
                var mode = this.Device.ArmLayerSubsystem.ArmSource.GetValueOrDefault( ArmSources.None );
                if ( mode == ArmSources.Manual && this.MeterTimerInternal.Enabled )
                {
                    this.AssertTrigger();
                }
                else if ( mode != ArmSources.Manual )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Info, $"Manual trigger ignored in '{mode.Description()}' mode" );
                }
                else if ( !this.MeterTimerInternal.Enabled )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Info, "Manual trigger ignored -- triggering is not active" );
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, "Exception occurred aborting" );
                My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "Exception occurred aborting." );
            }
        }

        /// <summary> Outputs a trigger to make a measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AssertTrigger()
        {
            lock ( this._DeviceAccessLocker )
                this.Device.Session.AssertTrigger();
        }

        /// <summary> True if abort requested. </summary>
        private bool _AbortRequested;

        /// <summary> Turns on or aborts waiting for trigger. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="enabled"> True to enable, false to disable. </param>
        /// <param name="checked"> True if checked. </param>
        private void ToggleAwaitTrigger( bool enabled, bool @checked )
        {
            this.InfoProvider.SetIconPadding( this._TriggerToolStrip, -10 );
            this.InfoProvider.SetError( this._TriggerToolStrip, "" );
            if ( enabled )
            {
                this._AbortRequested = !@checked;
                if ( @checked )
                {
                    lock ( this._DeviceAccessLocker )
                    {
                        _ = this.PublishVerbose( "Preparing instrument for waiting for trigger;. " );
                        this.TriggerAction = "INITIATING";
                        // clear execution state before enabling events
                        this.Device.ClearExecutionState();

                        // set the service request
                        _ = this.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask( ( int ) MeasurementEvents.All );
                        this.Device.Session.ApplyStandardServiceRequestEnableBitmasks( this.Device.Session.DefaultStandardEventEnableBitmask, this.Device.Session.DefaultOperationCompleteBitmask | Pith.ServiceRequests.OperationEvent );
                        this.Device.ClearExecutionState();
                        this.Device.TriggerSubsystem.Initiate();
                        this.TriggerAction = "WAITING FOR TRIGGERED MEASUREMENT";
                        _ = this.PublishVerbose( "Monitoring instrument for measurements;. " );
                        this.MeterTimerInternal.Interval = 100;
                        this.MeterTimerInternal.Enabled = true;
                    }
                }
                else
                {
                    this.TriggerAction = "ABORT REQUESTED";
                    this.AbortMeasurementThis();
                }
            }

            this._AwaitTriggerToolStripButton.Text = this._AwaitTriggerToolStripButton.Checked ? "ABORT TRIGGERING" : "WAIT FOR A TRIGGER";
        }

        /// <summary> Turns on or aborts waiting for trigger. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AwaitTriggerToolStripButton_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.InfoProvider.Clear();
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} initiating trigger";
                this.ToggleAwaitTrigger( this._AwaitTriggerToolStripButton.Enabled, this._AwaitTriggerToolStripButton.Checked );
            }
            catch ( Exception ex )
            {
                this.AbortMeasurementThis();
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets or sets the trigger action. </summary>
        /// <value> The trigger action. </value>
        private string TriggerAction
        {
            get => this._TriggerActionToolStripLabel.Text;

            set {
                if ( !string.Equals( this.TriggerAction, value ) )
                {
                    this._TriggerActionToolStripLabel.Text = value;
                }
            }
        }

        /// <summary> The status bar. </summary>
        private static string _StatusBar = "|";

        /// <summary> Gets the next status bar. </summary>
        /// <value> The next status bar. </value>
        private static string NextStatusBar
        {
            get {
                if ( _StatusBar == "|" )
                {
                    _StatusBar = "/";
                }
                else if ( _StatusBar == "/" )
                {
                    _StatusBar = "-";
                }
                else if ( _StatusBar == "-" )
                {
                    _StatusBar = @"\";
                }
                else if ( _StatusBar == @"\" )
                {
                    _StatusBar = "|";
                }

                return _StatusBar;
            }
        }

        /// <summary>
        /// Monitors measurements. Once found, reads and displays and restarts the cycle.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeterTimer_Tick( object sender, EventArgs e )
        {
            lock ( this._DeviceAccessLocker )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"{this.Device.ResourceNameCaption} monitoring instrument for data";
                    this.MeterTimerInternal.Enabled = false;
                    if ( this._AbortRequested )
                    {
                        this.AbortMeasurementThis();
                        return;
                    }

                    _ = this.Device.Session.ReadStatusRegister();
                    if ( this.Device.StatusSubsystem.HasMeasurementEvent )
                    {
                        this.TriggerAction = "READING...";

                        // update display modalities if changed.
                        _ = this.Device.MeasureSubsystem.Read();
                        this.TriggerAction = "DATA AVAILABLE...";
                        this._TriggerActionToolStripLabel.Text = "PREPARING...";

                        // clear execution state before enabling events
                        this.Device.ClearExecutionState();

                        // set the service request
                        _ = this.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask( ( int ) MeasurementEvents.All );
                        this.Device.Session.ApplyStandardServiceRequestEnableBitmasks( this.Device.Session.DefaultStandardEventEnableBitmask, this.Device.Session.DefaultOperationCompleteBitmask | Pith.ServiceRequests.OperationEvent );
                        this.Device.ClearExecutionState();
                        this.Device.TriggerSubsystem.Initiate();
                        this.TriggerAction = "WAITING FOR TRIGGRED MEASUREMENT...";
                        _ = this.PublishVerbose( "Monitoring instrument for measurements;. " );
                    }
                    else
                    {
                        this.TriggerAction = "WAITING FOR TRIGGERED MEASUREMENT";
                        this._WaitHourglassLabel.Text = NextStatusBar;
                    }

                    this.MeterTimerInternal.Enabled = !this._AbortRequested;
                }
                catch ( Exception ex )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                    _ = this.PublishException( activity, ex );
                    this._AwaitTriggerToolStripButton.Checked = false;
                }
            }
        }

        #endregion

        #region " INSULATION TEST "

        /// <summary> True if is insulation test owner, false if not. </summary>
        private bool _IsInsulationTestOwner;

        /// <summary> The insulation test. </summary>
        private InsulationTest _InsulationTest;

        /// <summary> Tests assign insulation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void AssignInsulationTest()
        {
            this._InsulationTest = new InsulationTest();
            this._IsInsulationTestOwner = false;
            this.AssignInsulationTest( new InsulationTest() );
            this._IsInsulationTestOwner = true;
        }

        /// <summary> Tests assign insulation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignInsulationTest( InsulationTest value )
        {
            if ( this._IsInsulationTestOwner && this._InsulationTest is object )
            {
                this._InsulationTest = null;
            }

            this._IsInsulationTestOwner = false;
            this.BindBinningInfo( value );
            this.BindActiveInsulationResistance( value );
        }

        #endregion

        #region " BINNING INFO "

        private BinningInfo _BinningInfoInternal;

        private BinningInfo BinningInfoInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._BinningInfoInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._BinningInfoInternal != null )
                {
                    this._BinningInfoInternal.PropertyChanged -= this.BinningInfoPropertyChanged;
                }

                this._BinningInfoInternal = value;
                if ( this._BinningInfoInternal != null )
                {
                    this._BinningInfoInternal.PropertyChanged += this.BinningInfoPropertyChanged;
                }
            }
        }

        /// <summary> Gets information describing the binning. </summary>
        /// <value> Information describing the binning. </value>
        public BinningInfo BinningInfo => this.BinningInfoInternal;

        /// <summary> Bind binning information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The insulation test to use for the binning info source. </param>
        private void BindBinningInfo( InsulationTest value )
        {
            if ( this.BinningInfo is object )
            {
                this.BindEntity( false, this.BinningInfo );
                this.BinningInfoInternal = null;
            }

            if ( value is object )
            {
                this.BinningInfoInternal = value.Binning;
                this.BindEntity( true, this.BinningInfo );
            }
        }

        /// <summary> Bind the entity. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">   True to add. </param>
        /// <param name="value"> The entity. </param>
        private void BindEntity( bool add, BinningInfo value )
        {
            if ( add )
            {
                value.PropertyChanged += this.BinningInfoPropertyChanged;
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.ArmSource ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.PassBits ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.LowerLimit ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.LowerLimitFailureBits ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.StrobePulseWidth ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.FailureBits ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.UpperLimitFailureBits ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.LowerLimitFailureBits ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.LowerLimit ) );
                this.OnPropertyChanged( value, nameof( VI.BinningInfo.PassBits ) );
            }
            else
            {
                value.PropertyChanged -= this.BinningInfoPropertyChanged;
            }
        }

        /// <summary> Executes the 'property changed' action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( BinningInfo sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( VI.BinningInfo.ArmSource ):
                    {
                        _ = this._ArmSourceComboBox.SelectItem( sender.ArmSource.ValueDescriptionPair() );
                        break;
                    }

                case nameof( VI.BinningInfo.PassBits ):
                    {
                        _ = this._PassBitPatternNumeric.ValueSetter( ( decimal ) sender.PassBits );
                        break;
                    }

                case nameof( VI.BinningInfo.LowerLimit ):
                    {
                        _ = this._ResistanceLowLimitNumeric.ValueSetter( 0.00001d * sender.LowerLimit );
                        break;
                    }

                case nameof( VI.BinningInfo.LowerLimitFailureBits ):
                    {
                        _ = this._FailBitPatternNumeric.ValueSetter( ( decimal ) sender.LowerLimitFailureBits );
                        break;
                    }

                case nameof( VI.BinningInfo.StrobePulseWidth ):
                    {
                        _ = this._EotStrobeDurationNumeric.ValueSetter( sender.StrobePulseWidth.Ticks / ( double ) TimeSpan.TicksPerMillisecond );
                        break;
                    }

                case nameof( VI.BinningInfo.FailureBits ):
                    {
                        this._FailBitPatternNumeric.Value = sender.FailureBits;
                        break;
                    }

                case nameof( VI.BinningInfo.UpperLimitFailureBits ):
                    {
                        this._FailBitPatternNumeric.Value = sender.UpperLimitFailureBits;
                        break;
                    }

                case var @case when @case == nameof( VI.BinningInfo.LowerLimitFailureBits ):
                    {
                        this._FailBitPatternNumeric.Value = sender.LowerLimitFailureBits;
                        break;
                    }

                case var case1 when case1 == nameof( VI.BinningInfo.LowerLimit ):
                    {
                        this._ResistanceLowLimitNumeric.Value = ( decimal ) (0.000001d * sender.LowerLimit);
                        break;
                    }

                case var case2 when case2 == nameof( VI.BinningInfo.PassBits ):
                    {
                        this._PassBitPatternNumeric.Value = sender.PassBits;
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _BinningInfo for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BinningInfoPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} handling the binning info {e?.PropertyName} changed Event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.BinningInfoPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as BinningInfo, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " INSULATION RESISTANCE "

        private InsulationResistance _ActiveInsulationResistanceInternal;

        private InsulationResistance ActiveInsulationResistanceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ActiveInsulationResistanceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ActiveInsulationResistanceInternal != null )
                {
                    this._ActiveInsulationResistanceInternal.PropertyChanged -= this.ActiveInsulationResistancePropertyChanged;
                }

                this._ActiveInsulationResistanceInternal = value;
                if ( this._ActiveInsulationResistanceInternal != null )
                {
                    this._ActiveInsulationResistanceInternal.PropertyChanged += this.ActiveInsulationResistancePropertyChanged;
                }
            }
        }

        /// <summary> Gets the active insulation resistance. </summary>
        /// <value> The active insulation resistance. </value>
        public InsulationResistance ActiveInsulationResistance => this.ActiveInsulationResistanceInternal;

        /// <summary> Bind active insulation resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void BindActiveInsulationResistance( InsulationTest value )
        {
            if ( this.ActiveInsulationResistance is object )
            {
                this.BindEntity( false, this.ActiveInsulationResistance );
                this.ActiveInsulationResistanceInternal = null;
            }

            if ( value is object )
            {
                this.ActiveInsulationResistanceInternal = value.Insulation;
                this.BindEntity( true, this.ActiveInsulationResistance );
            }
        }

        /// <summary> Bind the entity. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">   True to add. </param>
        /// <param name="value"> The entity. </param>
        private void BindEntity( bool add, InsulationResistance value )
        {
            if ( add )
            {
                value.PropertyChanged += this.ActiveInsulationResistancePropertyChanged;
                this.OnPropertyChanged( value, nameof( InsulationResistance.ContactCheckEnabled ) );
                this.OnPropertyChanged( value, nameof( InsulationResistance.CurrentLimit ) );
                this.OnPropertyChanged( value, nameof( InsulationResistance.DwellTime ) );
                this.OnPropertyChanged( value, nameof( InsulationResistance.PowerLineCycles ) );
                this.OnPropertyChanged( value, nameof( InsulationResistance.ResistanceLowLimit ) );
                this.OnPropertyChanged( value, nameof( InsulationResistance.ResistanceRange ) );
                this.OnPropertyChanged( value, nameof( InsulationResistance.VoltageLevel ) );
            }
            else
            {
                value.PropertyChanged -= this.ActiveInsulationResistancePropertyChanged;
            }
        }

        /// <summary> Executes the 'property changed' action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( InsulationResistance sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( InsulationResistance.ContactCheckEnabled ):
                    {
                        this._ContactCheckToggle.Checked = sender.ContactCheckEnabled;
                        break;
                    }

                case nameof( InsulationResistance.CurrentLimit ):
                    {
                        _ = this._CurrentLimitNumeric.ValueSetter( 0.00001d * sender.CurrentLimit );
                        break;
                    }

                case nameof( InsulationResistance.DwellTime ):
                    {
                        _ = this._DwellTimeNumeric.ValueSetter( sender.DwellTime.TotalSeconds );
                        break;
                    }

                case nameof( InsulationResistance.PowerLineCycles ):
                    {
                        _ = this._ApertureNumeric.ValueSetter( sender.PowerLineCycles );
                        break;
                    }

                case nameof( InsulationResistance.ResistanceLowLimit ):
                    {
                        _ = this._ResistanceLowLimitNumeric.ValueSetter( 0.00001d * sender.ResistanceLowLimit );
                        break;
                    }

                case nameof( InsulationResistance.ResistanceRange ):
                    {
                        _ = this._ResistanceRangeNumeric.ValueSetter( 0.000001d * sender.ResistanceRange );
                        break;
                    }

                case nameof( InsulationResistance.VoltageLevel ):
                    {
                        _ = this._VoltageLevelNumeric.ValueSetter( sender.VoltageLevel );
                        break;
                    }
            }
        }

        /// <summary>
        /// Event handler. Called by _ActiveInsulationResistance for property changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ActiveInsulationResistancePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} handling the insulation resistance {e?.PropertyName} changed Event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ActiveInsulationResistancePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as InsulationResistance, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " HIPOT TEST CONFIGURATION "

        /// <summary> Tests configure hipot start. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="binning">    The binning info. </param>
        /// <param name="insulation"> The insulation info. </param>
        private void ConfigureHipotStartTest( BinningInfo binning, InsulationResistance insulation )
        {
            if ( binning is null )
                throw new ArgumentNullException( nameof( binning ) );
            if ( insulation is null )
                throw new ArgumentNullException( nameof( insulation ) );
            this.InfoProvider.Clear();
            this.Device.ClearExecutionState();
            _ = this.Device.OutputSubsystem.WriteOutputOnState( false );

            // ======
            // CALC2

            // default: Me.Session.Write(":CALC2:CLIM:BCON IMM")
            // Me.Session.Write(":CALC2:CLIM:CLE")
            this.Device.CompositeLimit.ClearLimits();

            // default: Me.Session.Write(":CALC2:CLIM:CLE:AUTO ON")
            _ = this.Device.CompositeLimit.ApplyAutoClearEnabled( true );

            // default: Me.Session.Write(":CALC2:CLIM:MODE GRAD")
            _ = this.Device.CompositeLimit.ApplyLimitMode( LimitMode.Grading );

            // Make limit comparisons on resistance reading
            // Me.Session.Write(":CALC2:FEED RES")
            _ = this.Device.BinningSubsystem.ApplyFeedSource( FeedSources.Resistance );

            // Compliance Limit: send BAD code if measurement is in compliance
            // default: Me.Session.Write(":CALC2:LIM1:COMP:FAIL IN")
            _ = this.Device.ComplianceLimit.ApplyIncomplianceCondition( true );

            // Set fail bin
            // Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM1:COMP:SOUR2 {0}", Me._FailBitPatternNumeric.Value))
            _ = this.Device.ComplianceLimit.ApplyFailureBits( binning.FailureBits );
            binning.FailureBits = this.Device.ComplianceLimit.FailureBits.GetValueOrDefault( 0 );

            // Me.Session.Write(":CALC2:LIM1:STAT 1")
            _ = this.Device.ComplianceLimit.ApplyEnabled( binning.Enabled );
            binning.Enabled = this.Device.ComplianceLimit.Enabled.GetValueOrDefault( false );

            // Lower Limit
            // Me.Session.Write(":CALC2:LIM2:UPP 1E+19")
            _ = this.Device.UpperLowerLimit.ApplyUpperLimit( binning.UpperLimit );
            binning.UpperLimit = this.Device.UpperLowerLimit.UpperLimit.GetValueOrDefault( 0d );
            // ":CALC2:LIM2:UPP:SOUR2 {0}", Me._FailBitPatternNumeric.Value
            _ = this.Device.UpperLowerLimit.ApplyUpperLimitFailureBits( binning.UpperLimitFailureBits );
            binning.UpperLimitFailureBits = this.Device.UpperLowerLimit.UpperLimitFailureBits.GetValueOrDefault( 0 );
            // ":CALC2:LIM2:LOW {0}", Me._ResistanceLowLimitNumeric.Value
            _ = this.Device.UpperLowerLimit.ApplyLowerLimit( binning.LowerLimit );
            binning.LowerLimit = this.Device.UpperLowerLimit.LowerLimit.GetValueOrDefault( 0d );
            // ":CALC2:LIM2:LOW:SOUR2 {0}", Me._FailBitPatternNumeric.Value))
            _ = this.Device.UpperLowerLimit.ApplyLowerLimitFailureBits( binning.LowerLimitFailureBits );
            binning.LowerLimitFailureBits = this.Device.UpperLowerLimit.LowerLimitFailureBits.GetValueOrDefault( 0 );
            // Me.Session.Write(":CALC2:LIM2:STAT 1")
            _ = this.Device.UpperLowerLimit.ApplyEnabled( binning.Enabled );
            binning.Enabled = this.Device.UpperLowerLimit.Enabled.GetValueOrDefault( false );

            // Send GOOD code if measurement is in range
            // ":CALC2:CLIM:PASS:SOUR2 {0}", Me._PassBitPatternNumeric.Value
            _ = this.Device.CompositeLimit.ApplyPassBits( binning.PassBits );
            binning.PassBits = this.Device.CompositeLimit.PassBits.GetValueOrDefault( 0 );
            // Me.Session.Write(":CALC2:CLIM:BCON IMM")
            _ = this.Device.CompositeLimit.ApplyBinningControl( BinningControl.Immediate );


            // enable contact check
            if ( insulation.ContactCheckEnabled )
            {
                // Me.Session.Write(":CALC2:LIM4:STAT ON")
                _ = this.Device.ContactCheckLimit.ApplyEnabled( insulation.ContactCheckEnabled );
                insulation.ContactCheckEnabled = this.Device.ContactCheckLimit.Enabled.GetValueOrDefault( false );
                if ( this._ContactCheckBitPatternNumeric.Value > 0m )
                {
                    _ = this.Device.ContactCheckLimit.ApplyFailureBits( ( int ) Math.Round( this._ContactCheckBitPatternNumeric.Value ) );
                }
            }
            // this is done when setting the insulation test: 
            // Me.Session.Write(":SYSTem:CCH ON")
            else
            {
                // Me.Session.Write(":CALC2:LIM4:STAT OFF")
                _ = this.Device.ContactCheckLimit.ApplyEnabled( insulation.ContactCheckEnabled );
                insulation.ContactCheckEnabled = this.Device.ContactCheckLimit.Enabled.GetValueOrDefault( true );
                // this is done when setting the insulation test: 
                // Me.Session.Write(":SYSTem:CCH OFF")
            }

            // ======
            // EOT
            // Set byte size to 3 enabling EOT mode.
            // Me.Session.Write(":SOUR2:BSIZ 3")
            _ = this.Device.DigitalOutput.ApplyBitSize( 3 );

            // Set the output logic to high
            // Me.Session.Write(":SOUR2:TTL #b000")
            _ = this.Device.DigitalOutput.ApplyLevel( 0 );

            // Set Digital I/O Mode to EOT
            // Me.Session.Write(":SOUR2:TTL4:MODE EOT")
            _ = this.Device.DigitalOutput.ApplyOutputMode( OutputModes.EndTest );

            // Set EOT polarity to HI
            // Me.Session.Write(":SOUR2:TTL4:BST HI")
            _ = this.Device.DigitalOutput.ApplyDigitalActiveLevel( DigitalActiveLevels.High );

            // Set the output level to set automatically to the
            // :TTL level after the pass or fail output bit
            // pattern or a limit test is sent to the handler.
            // this can be turned off and the clear command issued on each start.
            // Me.Session.Write(":SOUR2:CLE")
            this.Device.DigitalOutput.ClearOutput();
            // Me.Session.Write(":SOUR2:CLE:AUTO ON")
            _ = this.Device.DigitalOutput.ApplyAutoClearEnabled( true );

            // Set the duration of the EOT strobe
            // Me.Session.Write(":SOUR2:CLE:AUTO:DEL " & CStr(Me._EotStrobeDurationNumeric.Value))
            _ = this.Device.DigitalOutput.ApplyDelay( TimeSpan.FromMilliseconds( ( double ) this._EotStrobeDurationNumeric.Value ) );

            // =================
            // ARM MODEL:

            // arm to immediate mode.
            // default is IMM:  Me.Session.Write(":ARM:SOUR IMM")

            // Set ARM counter to 1
            // default is 1: Me.Session.Write(":ARM:COUN 1")

            // Define Trigger layer to interface with the
            // Trigger Master board:

            // clear any pending triggers.
            // Me.Session.Write(":TRIG:CLE")
            this.Device.TriggerSubsystem.ClearTriggers();

            // Set TRIGGER input line to Trigger Link line number
            // default. to be set when starting: Me.Session.Write(":TRIG:ILIN 1")

            // Set TRIGGER output line to Trigger Link line number
            // default: Me.Session.Write(":TRIG:OLIN 2")

            // Set Input trigger to Acceptor
            // Me.Session.Write(":TRIG:DIR ACC")
            // Me.Device.TriggerSubsystem.ApplyDirection(Direction.Acceptor)

            // Me.Session.Write(":TRIG:DIR SOUR")
            // Me.Device.TriggerSubsystem.ApplyDirection(Direction.Source)
            _ = this.Device.TriggerSubsystem.ApplyTriggerLayerBypassMode( binning.TriggerDirection );
            if ( binning.TriggerDirection != this.Device.TriggerSubsystem.TriggerLayerBypassMode.GetValueOrDefault( TriggerLayerBypassModes.None ) )
            {
                throw new Core.OperationFailedException( $"Failed setting trigger direction to {binning.TriggerDirection};. Value set to {this.Device.TriggerSubsystem.TriggerLayerBypassMode}" );
            }

            // start with immediate trigger allowing non-triggered measurements.
            // Me.Session.Write(":TRIG:SOUR IMM")
            // see below: Me.Device.TriggerSubsystem.ApplyTriggerSource(TriggerSource.Immediate)

            // no outputs triggers
            // default is none: Me.Session.Write(":TRIG:OUTP NONE")

            // A single trigger.
            // default is 1: Me.Session.Write(":TRIG:COUN 1")

            // default: Me.Session.Write(":TRIG:INP NONE")

            // set trigger source to immediate
            _ = this.Device.TriggerSubsystem.ApplyTriggerSource( binning.TriggerSource );
            if ( binning.TriggerSource != this.Device.TriggerSubsystem.TriggerSource.GetValueOrDefault( TriggerSources.None ) )
            {
                throw new Core.OperationFailedException( $"Failed setting trigger source to {binning.TriggerSource};. Value set to {this.Device.TriggerSubsystem.TriggerSource}" );
            }

            _ = this.Device.ArmLayerSubsystem.ApplyArmLayerBypassMode( binning.ArmDirection );
            if ( binning.ArmDirection != this.Device.ArmLayerSubsystem.ArmLayerBypassMode.GetValueOrDefault( TriggerLayerBypassModes.None ) )
            {
                throw new Core.OperationFailedException( $"Failed setting Arm Layer Bypass Mode to {binning.ArmDirection};. Value set to {this.Device.ArmLayerSubsystem.ArmLayerBypassMode}" );
            }

            _ = this.Device.ArmLayerSubsystem.ApplyArmSource( binning.ArmSource );
            binning.ArmSource = this.Device.ArmLayerSubsystem.ArmSource.GetValueOrDefault( ArmSources.None );
            _ = this.Device.ArmLayerSubsystem.ApplyArmCount( binning.ArmCount );
            binning.ArmCount = this.Device.ArmLayerSubsystem.ArmCount.GetValueOrDefault( 0 );
        }

        /// <summary> Applies the hipot binning information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplyHipotBinningInfo()
        {
            this.BinningInfo.TriggerDirection = TriggerLayerBypassModes.Source;
            this.BinningInfo.TriggerSource = TriggerSources.Immediate;
            this.BinningInfo.ArmSource = this.SelectedArmSource;
            this.BinningInfo.ArmCount = this.BinningInfo.ArmSource == ArmSources.Manual ? 1 : 0;
            this.BinningInfo.FailureBits = ( int ) Math.Round( this._FailBitPatternNumeric.Value );
            this.BinningInfo.UpperLimit = Pith.Scpi.Syntax.Infinity;
            this.BinningInfo.UpperLimitFailureBits = ( int ) Math.Round( this._FailBitPatternNumeric.Value );
            this.BinningInfo.LowerLimit = 1000000.0d * ( double ) this._ResistanceLowLimitNumeric.Value;
            this.BinningInfo.LowerLimitFailureBits = ( int ) Math.Round( this._FailBitPatternNumeric.Value );
            this.BinningInfo.PassBits = ( int ) Math.Round( this._PassBitPatternNumeric.Value );
            this.BinningInfo.Enabled = true;
        }

        /// <summary> Configures the high potential measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="hipotSettings"> The hipot settings. </param>
        public void ConfigureHipot( InsulationResistance hipotSettings )
        {
            if ( hipotSettings is null )
                throw new ArgumentNullException( nameof( hipotSettings ) );

            // make sure the 2400 output is off
            _ = this.Device.OutputSubsystem.ApplyOutputOnState( false );

            // Me.Session.Write("*RST")
            // Me.Session.Write(":OUTP OFF")
            // Me.Session.Write(":ROUT:TERM REAR")
            // Me.Session.Write(":SENS:FUNC ""RES""")
            // Me.Session.Write(":SENS:RES:MODE MAN")
            // Me.Session.Write(":SOUR:CLE:AUTO ON")
            // Me.Session.Write(":SOUR:DEL 5")
            // Me.Session.Write(":SYST:RSEN ON")
            // Me.Session.Write(":SOUR:FUNC VOLT")
            // Me.Session.Write(":SOUR:VOLT:RANG 5")
            // Me.Session.Write(":SOUR:VOLT 5")
            // Me.Session.Write(":SENS:CURR:RANG 10e-6")
            // Me.Session.Write(":SENS:CURR:PROT 10e-6")
            // Me.Session.Write(":SENS:VOLT:NPLC 1")
            // Me.Session.Write(":SENS:CURR:NPLC 1")
            // Me.Session.Write(":SENS:RES:NPLC 1")
            // Me.Session.Write(":SYST:AZER ONCE")
            // Me.Session.Write(":FORM:ELEM VOLT,CURR,RES,STATUS")
            // Me.Session.Write(":SOUR:CLE:AUTO ON")
            // Dim value As String = Me.Session.Query(":READ?")

            // initialize the known state.
            this.Device.ResetClearInit();
            _ = this.Device.RouteSubsystem.ApplyTerminalsMode( RouteTerminalsModes.Rear );
            _ = this.Device.SenseSubsystem.ApplyFunctionMode( SenseFunctionModes.Resistance );
            this.Device.SenseSubsystem.FunctionModes = SenseFunctionModes.Resistance;
            this.Device.SenseSubsystem.SupportsMultiFunctions = false;

            // set to manual mode to require manually setting the measurement as voltage source
            _ = this.Device.SenseResistanceSubsystem.ApplyConfigurationMode( ConfigurationModes.Manual );

            // the source must be set first.
            _ = this.Device.SourceSubsystem.ApplyAutoClearEnabled( true );
            _ = this.Device.SourceSubsystem.ApplyDelay( hipotSettings.DwellTime );
            hipotSettings.DwellTime = this.Device.SourceSubsystem.Delay.GetValueOrDefault( TimeSpan.Zero );
            _ = this.Device.SourceSubsystem.ApplyFunctionMode( SourceFunctionModes.Voltage );
            _ = this.Device.SourceVoltageSubsystem.ApplyRange( hipotSettings.VoltageLevel );
            _ = this.Device.SourceVoltageSubsystem.ApplyLevel( hipotSettings.VoltageLevel );
            hipotSettings.VoltageLevel = this.Device.SourceVoltageSubsystem.Level.GetValueOrDefault( 0d );
            _ = this.Device.SenseCurrentSubsystem.ApplyRange( hipotSettings.CurrentRange );
            _ = this.Device.SenseCurrentSubsystem.ApplyProtectionLevel( hipotSettings.CurrentLimit );
            hipotSettings.CurrentLimit = this.Device.SenseCurrentSubsystem.ProtectionLevel.GetValueOrDefault( 0d );
            _ = this.Device.SenseResistanceSubsystem.ApplyPowerLineCycles( hipotSettings.PowerLineCycles );
            hipotSettings.PowerLineCycles = this.Device.SenseResistanceSubsystem.PowerLineCycles.GetValueOrDefault( 0d );

            // Enable four wire connection
            _ = this.Device.SystemSubsystem.ApplyFourWireSenseEnabled( true );

            // force immediate update of auto zero
            _ = this.Device.SystemSubsystem.ApplyAutoZeroEnabled( true );
            _ = this.Device.SystemSubsystem.ApplyContactCheckEnabled( hipotSettings.ContactCheckEnabled );
            hipotSettings.ContactCheckEnabled = this.Device.SystemSubsystem.ContactCheckEnabled.GetValueOrDefault( false );
            _ = this.Device.ContactCheckLimit.ApplyEnabled( hipotSettings.ContactCheckEnabled );
            hipotSettings.ContactCheckEnabled = this.Device.ContactCheckLimit.Enabled.GetValueOrDefault( false );
            _ = this.Device.FormatSubsystem.ApplyElements( ReadingElementTypes.Voltage | ReadingElementTypes.Current | ReadingElementTypes.Resistance | ReadingElementTypes.Status );

            // Me.Device.MeasureSubsystem.Readings.ResistanceReading.LowLimit = 1000000.0 * Me._ResistanceRangeNumeric.Value
            this.Device.MeasureSubsystem.ReadingAmounts.ResistanceReading.LowLimit = hipotSettings.ResistanceLowLimit;

            // update the timeout to reflect the dwell time.
            if ( this.Device.SourceSubsystem.Delay.HasValue )
            {
                var totalTestTime = this.Device.SourceSubsystem.Delay.Value;
                totalTestTime = totalTestTime.Add( this.Device.SenseResistanceSubsystem.IntegrationPeriod.GetValueOrDefault( TimeSpan.Zero ) );
                totalTestTime = totalTestTime.Add( this.Device.TriggerSubsystem.Delay.GetValueOrDefault( TimeSpan.Zero ) );
                // set the minimum timeout
                totalTestTime = totalTestTime.Add( TimeSpan.FromSeconds( 2d ) );
                if ( totalTestTime > this.Device.Session.CommunicationTimeout )
                    this.Device.Session.StoreCommunicationTimeout( totalTestTime );
            }
        }

        /// <summary> Configure hipot change. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="hipotSettings"> The hipot settings. </param>
        public void ConfigureHipotChange( InsulationResistance hipotSettings )
        {
            if ( hipotSettings is null )
                throw new ArgumentNullException( nameof( hipotSettings ) );

            // make sure the 2400 output is off
            _ = this.Device.OutputSubsystem.ApplyOutputOnState( false );
            if ( !Equals( this.Device.RouteSubsystem.TerminalsMode, OutputTerminalsModes.Rear ) )
            {
                _ = this.Device.RouteSubsystem.ApplyTerminalsMode( RouteTerminalsModes.Rear );
            }

            if ( !Nullable.Equals( this.Device.SenseSubsystem.FunctionMode, SenseFunctionModes.Resistance ) )
            {
                _ = this.Device.SenseSubsystem.ApplyFunctionMode( SenseFunctionModes.Resistance );
                this.Device.SenseSubsystem.FunctionModes = SenseFunctionModes.Resistance;
                this.Device.SenseSubsystem.SupportsMultiFunctions = false;
            }

            // set to manual mode to require manually setting the measurement as voltage source
            if ( !Nullable.Equals( this.Device.SenseResistanceSubsystem.ConfigurationMode, ConfigurationModes.Manual ) )
            {
                _ = this.Device.SenseResistanceSubsystem.ApplyConfigurationMode( ConfigurationModes.Manual );
            }


            // the source must be set first.
            if ( !Nullable.Equals( this.Device.SourceSubsystem.AutoClearEnabled, true ) )
            {
                _ = this.Device.SourceSubsystem.ApplyAutoClearEnabled( true );
            }

            if ( !Nullable.Equals( this.Device.SourceSubsystem.Delay, hipotSettings.DwellTime ) )
            {
                _ = this.Device.SourceSubsystem.ApplyDelay( hipotSettings.DwellTime );
                hipotSettings.DwellTime = this.Device.SourceSubsystem.Delay.GetValueOrDefault( TimeSpan.Zero );
            }

            if ( !Nullable.Equals( this.Device.SourceSubsystem.FunctionMode, SourceFunctionModes.Voltage ) )
            {
                _ = this.Device.SourceSubsystem.ApplyFunctionMode( SourceFunctionModes.Voltage );
            }

            if ( !Nullable.Equals( this.Device.SourceVoltageSubsystem.Range, hipotSettings.VoltageLevel ) )
            {
                _ = this.Device.SourceVoltageSubsystem.ApplyRange( hipotSettings.VoltageLevel );
            }

            if ( !Nullable.Equals( this.Device.SourceVoltageSubsystem.Level, hipotSettings.VoltageLevel ) )
            {
                _ = this.Device.SourceVoltageSubsystem.ApplyLevel( hipotSettings.VoltageLevel );
                hipotSettings.VoltageLevel = this.Device.SourceVoltageSubsystem.Level.GetValueOrDefault( 0d );
            }

            if ( !Nullable.Equals( this.Device.SenseCurrentSubsystem.Range, hipotSettings.CurrentRange ) )
            {
                _ = this.Device.SenseCurrentSubsystem.ApplyRange( hipotSettings.CurrentRange );
            }

            if ( !Nullable.Equals( this.Device.SenseCurrentSubsystem.ProtectionLevel, hipotSettings.CurrentRange ) )
            {
                _ = this.Device.SenseCurrentSubsystem.ApplyProtectionLevel( hipotSettings.CurrentLimit );
                hipotSettings.CurrentLimit = this.Device.SenseCurrentSubsystem.ProtectionLevel.GetValueOrDefault( 0d );
            }

            if ( !Nullable.Equals( this.Device.SenseResistanceSubsystem.PowerLineCycles, hipotSettings.PowerLineCycles ) )
            {
                _ = this.Device.SenseResistanceSubsystem.ApplyPowerLineCycles( hipotSettings.PowerLineCycles );
                hipotSettings.PowerLineCycles = this.Device.SenseResistanceSubsystem.PowerLineCycles.GetValueOrDefault( 0d );
            }

            // Enable four wire connection
            if ( !Nullable.Equals( this.Device.SystemSubsystem.FourWireSenseEnabled, true ) )
            {
                _ = this.Device.SystemSubsystem.ApplyFourWireSenseEnabled( true );
            }

            // force immediate update of auto zero
            if ( !Nullable.Equals( this.Device.SystemSubsystem.AutoZeroEnabled, true ) )
            {
                _ = this.Device.SystemSubsystem.ApplyAutoZeroEnabled( true );
            }

            if ( !Nullable.Equals( this.Device.SystemSubsystem.ContactCheckEnabled, hipotSettings.ContactCheckEnabled ) )
            {
                _ = this.Device.SystemSubsystem.ApplyContactCheckEnabled( hipotSettings.ContactCheckEnabled );
                hipotSettings.ContactCheckEnabled = this.Device.SystemSubsystem.ContactCheckEnabled.GetValueOrDefault( false );
            }

            if ( !Nullable.Equals( this.Device.ContactCheckLimit.Enabled, hipotSettings.ContactCheckEnabled ) )
            {
                _ = this.Device.ContactCheckLimit.ApplyEnabled( hipotSettings.ContactCheckEnabled );
                hipotSettings.ContactCheckEnabled = this.Device.ContactCheckLimit.Enabled.GetValueOrDefault( false );
            }

            var elements = ReadingElementTypes.Voltage | ReadingElementTypes.Current | ReadingElementTypes.Resistance | ReadingElementTypes.Status;
            if ( !Equals( this.Device.FormatSubsystem.Elements, elements ) )
            {
                _ = this.Device.FormatSubsystem.ApplyElements( elements );
            }

            if ( !Equals( this.Device.MeasureSubsystem.ReadingAmounts.ResistanceReading.LowLimit, hipotSettings.ResistanceLowLimit ) )
            {
                this.Device.MeasureSubsystem.ReadingAmounts.ResistanceReading.LowLimit = hipotSettings.ResistanceLowLimit;
            }

            // update the timeout to reflect the dwell time.
            if ( this.Device.SourceSubsystem.Delay.HasValue )
            {
                var totalTestTime = this.Device.SourceSubsystem.Delay.Value;
                totalTestTime = totalTestTime.Add( this.Device.SenseResistanceSubsystem.IntegrationPeriod.GetValueOrDefault( TimeSpan.Zero ) );
                totalTestTime = totalTestTime.Add( this.Device.TriggerSubsystem.Delay.GetValueOrDefault( TimeSpan.Zero ) );
                // set the minimum timeout
                totalTestTime = totalTestTime.Add( TimeSpan.FromSeconds( 2d ) );
                if ( totalTestTime > this.Device.Session.CommunicationTimeout )
                    this.Device.Session.StoreCommunicationTimeout( totalTestTime );
            }
        }

        /// <summary> Applies the hipot settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplyHipotSettings()
        {
            this.ActiveInsulationResistance.DwellTime = TimeSpan.FromSeconds( ( double ) this._DwellTimeNumeric.Value );
            this.ActiveInsulationResistance.VoltageLevel = ( double ) this._VoltageLevelNumeric.Value;
            this.ActiveInsulationResistance.CurrentLimit = 0.000001d * ( double ) this._CurrentLimitNumeric.Value;
            this.ActiveInsulationResistance.PowerLineCycles = ( double ) this._ApertureNumeric.Value;
            this.ActiveInsulationResistance.ContactCheckEnabled = this._ContactCheckToggle.Checked;
            this.ActiveInsulationResistance.ResistanceLowLimit = 1000000.0d * ( double ) this._ResistanceLowLimitNumeric.Value;
            this.ActiveInsulationResistance.ResistanceRange = 1000000.0d * ( double ) this._ResistanceRangeNumeric.Value;
        }

        /// <summary> Applies the hipot settings button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyHipotSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying high potential settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.ApplyHipotSettings();
                this.ConfigureHipot( this.ActiveInsulationResistance );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the sot settings button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySotSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying trigger settings";
                _ = this.PublishInfo( $"{activity};. " );
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.ApplyHipotBinningInfo();
                this.ApplyHipotSettings();
                this.ConfigureHipotStartTest( this.BinningInfo, this.ActiveInsulationResistance );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
