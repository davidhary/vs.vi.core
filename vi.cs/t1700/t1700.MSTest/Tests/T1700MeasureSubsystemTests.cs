using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.T1700.MSTest
{

    /// <summary>
    /// This is a test class for T1700.MeasureSubsystemTest and is intended to contain all
    /// MeasureSubsystemTest Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [TestClass()]
    [TestCategory( "t1700" )]
    public class T1700MeasureSubsystemTests
    {

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> A test for TryParse. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="rangeMode">       The range mode. </param>
        /// <param name="expectedCurrent"> The expected current. </param>
        /// <param name="expectedRange">   The expected range. </param>
        public static void AssertParseShouldPass( ResistanceRangeMode rangeMode, string expectedCurrent, string expectedRange )
        {
            string current = string.Empty;
            string range = string.Empty;
            bool expected = true;
            bool actual;
            actual = MeasureSubsystem.TryParse( rangeMode, ref current, ref range );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expectedCurrent, current );
            Assert.AreEqual( expectedRange, range );
        }

        /// <summary> (Unit Test Method) parse should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ParseShouldPass()
        {
            AssertParseShouldPass( ResistanceRangeMode.R10, "10 mA", "200 ohm" );
        }

        /// <summary> Assert parse from numeric should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="rangeMode">       The range mode. </param>
        /// <param name="expectedCurrent"> The expected current. </param>
        /// <param name="expectedRange">   The expected range. </param>
        public static void AssertParseFromNumericShouldPass( ResistanceRangeMode rangeMode, double expectedCurrent, double expectedRange )
        {
            var current = default( double );
            var range = default( double );
            bool expected = true;
            bool actual;
            actual = MeasureSubsystem.TryParse( rangeMode, ref current, ref range );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expectedCurrent, current, 0.00001d * current );
            Assert.AreEqual( expectedRange, range, 0.00001d * range );
        }

        /// <summary> (Unit Test Method) parse from numeric should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ParseFromNumericShouldPass()
        {
            AssertParseFromNumericShouldPass( ResistanceRangeMode.R10, 0.01d, 200d );
        }

        /// <summary> (Unit Test Method) parse from numeric r 1 should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ParseFromNumericR1ShouldPass()
        {
            AssertParseFromNumericShouldPass( ResistanceRangeMode.R1, 1d, 0.002d );
        }

        /// <summary> (Unit Test Method) parse from numeric r 5 should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ParseFromNumericR5ShouldPass()
        {
            AssertParseFromNumericShouldPass( ResistanceRangeMode.R5, 0.1d, 0.2d );
        }

        /// <summary> (Unit Test Method) parse from numeric r 14 should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ParseFromNumericR14ShouldPass()
        {
            AssertParseFromNumericShouldPass( ResistanceRangeMode.R14, 0.0001d, 2000d );
        }

        /// <summary> (Unit Test Method) parse from numeric r 19 should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ParseFromNumericR19ShouldPass()
        {
            AssertParseFromNumericShouldPass( ResistanceRangeMode.R19, 0.0000001d, 20000000.0d );
        }

        /// <summary> Assert convert should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="current">           The current. </param>
        /// <param name="range">             The range. </param>
        /// <param name="expectedRangeMode"> The expected range mode. </param>
        public static void AssertConvertShouldPass( double current, double range, ResistanceRangeMode expectedRangeMode )
        {
            var rangeMode = ResistanceRangeMode.R0;
            bool expected = true;
            bool actual;
            actual = MeasureSubsystem.TryConvert( current, range, ref rangeMode );
            Assert.AreEqual( expected, actual );
            Assert.AreEqual( expectedRangeMode, rangeMode );
        }

        /// <summary> (Unit Test Method) convert numeric r 10 should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ConvertNumericR10ShouldPass()
        {
            AssertConvertShouldPass( 0.01d, 200d, ResistanceRangeMode.R10 );
        }
    }
}
