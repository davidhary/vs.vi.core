﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

// Review the values of the assembly attributes

[assembly: AssemblyTitle( "Tegam 1700 VI Tests" )]
[assembly: AssemblyDescription( "Tegam 1700 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.T1700.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
