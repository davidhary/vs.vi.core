using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.T1700.Forms
{

    /// <summary> A Reading view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__MeasureButton.Name = "_MeasureButton";
            this.__WriteButton.Name = "_WriteButton";
            this.__ReadButton.Name = "_ReadButton";
            this.__ReadSRQButton.Name = "_ReadSRQButton";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public T1700Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( T1700Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindMeasureSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( T1700Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MEASURE "

        /// <summary> Gets or sets the resistance Measure subsystem . </summary>
        /// <value> The Resistance Measure subsystem . </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public MeasureSubsystem MeasureSubsystem { get; private set; }

        /// <summary> Bind Measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindMeasureSubsystem( T1700Device device )
        {
            if ( this.MeasureSubsystem is object )
            {
                this.BindSubsystem( false, this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            if ( device is object )
            {
                this.MeasureSubsystem = device.MeasureSubsystem;
                this.BindSubsystem( true, this.MeasureSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MeasureSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.MeasureSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.InitialDelay ) );
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.MaximumTrialsCount ) );
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.MaximumDifference ) );
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.MeasurementDelay ) );
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.OverRangeOpenWire ) );
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.PrimaryReadingValue ) );
                this.HandlePropertyChanged( subsystem, nameof( T1700.MeasureSubsystem.SupportedCommands ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.MeasureSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the measurement available action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnMeasurementAvailable( MeasureSubsystem subsystem )
        {
            if ( subsystem is null )
            {
            }
            else if ( this.Device.MeasureSubsystem.PrimaryReadingValue.HasValue && this._ReadContinuouslyCheckBox.Checked )
            {
                Core.ApplianceBase.DoEvents();
                _ = this.Device.Session.ReadStatusRegister(); // was Me.Device.ReadRegisters(), which seems unnecessary
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( ( double ) this._PostReadingDelayNumeric.Value ) );
                if ( !this.Device.StatusSubsystem.ErrorAvailable )
                {
                    _ = this.Device.MeasureSubsystem.Read();
                }
            }
            else if ( this.Device.MeasureSubsystem.OverRangeOpenWire.GetValueOrDefault( false ) )
            {
                _ = this.PublishInfo( "Measurement over range or open wire detected;. " );
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Handles the measurement available action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [Obsolete( "replaced with measurementAvailable(MeasureSubsystem)" )]
        private void OnMeasurementAvailable()
        {
            if ( this.Device.MeasureSubsystem.PrimaryReadingValue.HasValue && this._ReadContinuouslyCheckBox.Checked )
            {
                Core.ApplianceBase.DoEvents();
                _ = this.Device.Session.ReadStatusRegister(); // was Me.Device.ReadRegisters(), which seems unnecessary
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( ( double ) this._PostReadingDelayNumeric.Value ) );
                if ( !this.Device.StatusSubsystem.ErrorAvailable )
                {
                    _ = this.Device.MeasureSubsystem.Read();
                }
            }
        }

        /// <summary> Executes the over range open wire action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [Obsolete( "replaced with measurementAvailable(MeasureSubsystem)" )]
        private void OnOverRangeOpenWire()
        {
            if ( this.Device.MeasureSubsystem.OverRangeOpenWire.GetValueOrDefault( false ) )
            {
                _ = this.PublishInfo( "Measurement over range or open wire detected;. " );
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Handles the supported commands changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnSupportedCommandsChanged( MeasureSubsystem subsystem )
        {
            if ( subsystem is object )
            {
                this._CommandComboBox.DataSource = null;
                this._CommandComboBox.Items.Clear();
                this._CommandComboBox.DataSource = subsystem.SupportedCommands().ToList();
                this._CommandComboBox.SelectedIndex = 0;
            }
        }

        /// <summary> Updates the display of measurement settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnMeasureSettingsChanged( MeasureSubsystem subsystem )
        {
            if ( subsystem is object )
            {
                this._MeasureSettingsLabel.Text = string.Format( System.Globalization.CultureInfo.CurrentCulture, "Trials: {0}; Initial Delay: {1} ms; Measurement Delay: {2} ms; Delta: {3:0.0%}", subsystem.MaximumTrialsCount, subsystem.InitialDelay.TotalMilliseconds, subsystem.MeasurementDelay.TotalMilliseconds, subsystem.MaximumDifference );
            }
        }

        /// <summary> Handles the Measure subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MeasureSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( T1700.MeasureSubsystem.MaximumTrialsCount ):
                case nameof( T1700.MeasureSubsystem.InitialDelay ):
                case nameof( T1700.MeasureSubsystem.MeasurementDelay ):
                case nameof( T1700.MeasureSubsystem.MaximumDifference ):
                    {
                        this.OnMeasureSettingsChanged( subsystem );
                        break;
                    }

                case nameof( T1700.MeasureSubsystem.OverRangeOpenWire ):
                    {
                        // Me.onOverRangeOpenWire()
                        this.OnMeasurementAvailable( subsystem );
                        break;
                    }

                case nameof( T1700.MeasureSubsystem.PrimaryReadingValue ):
                    {
                        if ( (subsystem.PrimaryReadingValue.HasValue && !((subsystem?.OverRangeOpenWire).GetValueOrDefault( false ))) == true )
                        {
                            _ = this.PublishInfo( "Parsed resistance value;. Resistance = {0}", ( object ) subsystem.PrimaryReadingValue.Value );
                        }

                        this.OnMeasurementAvailable( subsystem );
                        break;
                    }

                case nameof( T1700.MeasureSubsystem.SupportedCommands ):
                    {
                        this.OnSupportedCommandsChanged( subsystem );
                        break;
                    }
            }
        }

        /// <summary> measure subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    activity = $"invoking {nameof( T1700.MeasureSubsystem )}.{e.PropertyName} change";
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeasureSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"handling {nameof( T1700.MeasureSubsystem )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as MeasureSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: READ "

        /// <summary> Initiates a reading for retrieval by way of the service request event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadSRQButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"{this.Device.ResourceNameCaption} reading status register";
                _ = this.Device.Session.ReadStatusRegister(); // was Me.Device.ReadRegisters(), which seems unnecessary
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Query the Device for a reading. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                // update display modalities if changed.
                _ = this.Device.MeasureSubsystem.Read();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by _WriteButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void WriteButton_Click( object sender, EventArgs e )
        {
            if ( !string.IsNullOrWhiteSpace( this._CommandComboBox.Text ) )
            {
                string dataToWrite = this._CommandComboBox.Text.Trim();
                string activity = string.Empty;
                try
                {
                    this.InfoProvider.Clear();
                    this.Cursor = Cursors.WaitCursor;
                    activity = $"{this.Device.ResourceNameCaption} writing {dataToWrite}";
                    if ( dataToWrite.StartsWith( "U", StringComparison.OrdinalIgnoreCase ) )
                    {
                        this.Device.MeasureSubsystem.LastReading = this.Device.Session.QueryTrimEnd( dataToWrite );
                    }
                    else
                    {
                        _ = this.Device.Session.WriteLine( dataToWrite );
                    }
                }
                catch ( Exception ex )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                    _ = this.PublishException( activity, ex );
                }
                finally
                {
                    _ = this.Device.Session.ReadStatusRegister(); // was Me.Device.ReadRegisters(), which seems unnecessary
                    this.Cursor = Cursors.Default;
                }
            }
        }

        /// <summary> Event handler. Called by _MeasureButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureButton_Click( object sender, EventArgs e )
        {
            if ( !string.IsNullOrWhiteSpace( this._CommandComboBox.Text ) )
            {
                string activity = string.Empty;
                try
                {
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.Clear();
                    activity = $"{this.Device.ResourceNameCaption} measuring";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.Device.MeasureSubsystem.Measure();
                }
                catch ( Exception ex )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                    _ = this.PublishException( activity, ex );
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
