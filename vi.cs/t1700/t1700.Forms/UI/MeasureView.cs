using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using isr.Core.EnumExtensions;
using isr.Core.TimeSpanExtensions;
using isr.Core.WinForms.ComboBoxExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.T1700.Forms
{

    /// <summary> A measure view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class MeasureView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MeasureView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;

            // Add any initialization after the InitializeComponent() call.
            // populate the range mode selector
            this.ListNonAutoRangeModes();
            // populate the emulated reply combo.
            this.ListOneShotTriggerModes();
            this.SelectMeterTrigger( TriggerMode.T3 );
            this.__MeterRangeComboBox.Name = "_MeterRangeComboBox";
            this.__MaximumTrialsCountNumeric.Name = "_MaximumTrialsCountNumeric";
            this.__MaximumDifferenceNumeric.Name = "_MaximumDifferenceNumeric";
            this.__MeasurementDelayNumeric.Name = "_MeasurementDelayNumeric";
            this.__InitialDelayNumeric.Name = "_InitialDelayNumeric";
            this.__TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
        }

        /// <summary> Creates a new <see cref="MeasureView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="MeasureView"/>. </returns>
        public static MeasureView Create()
        {
            MeasureView view = null;
            try
            {
                view = new MeasureView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public T1700Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( T1700Device value )
        {
            if ( this.Device is object )
            {
                this.Device.Initialized -= this.DeviceInitialized;
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
                this.Device.Initialized += this.DeviceInitialized;
            }

            this.BindMeasureSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( T1700Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Initializes the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void InitializeDevice()
        {
            this.DeviceInitialized( this.Device, EventArgs.Empty );
        }

        /// <summary> Device initialized. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Cancel event information. </param>
        protected void DeviceInitialized( T1700Device sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.ApplyMeterTriggerMode();
            this.ApplyMeterRangeMode();
            this.ApplyTriggerDelay();
            if ( this.Device.IsDeviceOpen )
            {
                _ = this.PublishVerbose( $"Setting initial delay to {this.InitialDelay.TotalMilliseconds}ms;. " );
                this.Device.MeasureSubsystem.InitialDelay = this.InitialDelay;
                _ = this.PublishVerbose( $"Setting measurement delay to {this.MeasurementDelay.TotalMilliseconds}ms;. " );
                this.Device.MeasureSubsystem.MeasurementDelay = this.MeasurementDelay;
                _ = this.PublishVerbose( $"Setting maximum trial count to {this.MaximumTrialsCount};. " );
                this.Device.MeasureSubsystem.MaximumTrialsCount = this.MaximumTrialsCount;
                _ = this.PublishVerbose( $"Setting maximum difference to {100m * this.MaximumDifference}%;. " );
                this.Device.MeasureSubsystem.MaximumDifference = ( double ) this.MaximumDifference;
            }
        }

        /// <summary> Device initialized. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        ///                                             <see cref="MeasureView"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeviceInitialized( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} handling device initialized event";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, EventArgs>( this.DeviceInitialized ), new object[] { sender, e } );
                }
                else
                {
                    this.DeviceInitialized( sender as T1700Device, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MEASURE SUBSYSTEM "

        /// <summary> Gets the measure subsystem. </summary>
        /// <value> The measure subsystem. </value>
        private MeasureSubsystem MeasureSubsystem { get; set; }

        /// <summary> Bind measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindMeasureSubsystem( T1700Device device )
        {
            if ( this.MeasureSubsystem is object )
            {
                this.BindSubsystem( false, this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            if ( device is object )
            {
                this.MeasureSubsystem = device.MeasureSubsystem;
                this.BindSubsystem( true, this.MeasureSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MeasureSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.MeasureSubsystemPropertyChanged;
                bool @int = this.InitializingComponents;
                this.InitializingComponents = true;
                this._MeterRangeComboBox.ListResistanceRangeCurrents( subsystem.ResistanceRangeCurrents, new int[] { 0 } );
                this.InitializingComponents = @int;
            }
            else
            {
                subsystem.PropertyChanged -= this.MeasureSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Measure subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MeasureSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( T1700.MeasureSubsystem.RangeMode ):
                    {
                        if ( subsystem.RangeMode.HasValue )
                        {
                            this.SelectMeterRange( subsystem.RangeMode.Value.Description() );
                        }

                        break;
                    }

                case nameof( T1700.MeasureSubsystem.TriggerMode ):
                    {
                        if ( subsystem.TriggerMode.HasValue )
                        {
                            this.SelectMeterTrigger( subsystem.TriggerMode.Value.Description() );
                        }

                        break;
                    }

                case nameof( T1700.MeasureSubsystem.TriggerDelay ):
                    {
                        if ( subsystem.TriggerDelay.HasValue )
                        {
                            this.TriggerDelay = subsystem.TriggerDelay.Value;
                        }

                        break;
                    }

                case nameof( T1700.MeasureSubsystem.InitialDelay ):
                    {
                        this.InitialDelay = subsystem.InitialDelay;
                        break;
                    }

                case nameof( T1700.MeasureSubsystem.MeasurementDelay ):
                    {
                        this.MeasurementDelay = subsystem.MeasurementDelay;
                        break;
                    }

                case nameof( T1700.MeasureSubsystem.MaximumTrialsCount ):
                    {
                        this.MaximumTrialsCount = subsystem.MaximumTrialsCount;
                        break;
                    }

                case nameof( T1700.MeasureSubsystem.MaximumDifference ):
                    {
                        this.MaximumDifference = ( decimal ) subsystem.MaximumDifference;
                        break;
                    }
            }
        }

        /// <summary> Measure subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.MeasureSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeasureSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as MeasureSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Applies the meter trigger mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ApplyMeterTriggerMode()
        {
            if ( this.Device?.IsDeviceOpen == true )
            {
                if ( this.Device.MeasureSubsystem is object && !Nullable.Equals( this.Device.MeasureSubsystem.TriggerMode, this.SelectedMeterTrigger ) )
                {
                    _ = this.PublishVerbose( "Applying meter trigger {0};. ", this.SelectedMeterTrigger );
                    _ = this.Device.MeasureSubsystem.ApplyTriggerMode( this.SelectedMeterTrigger );
                    _ = this.Device.Session.ReadStatusRegister();
                    // a delay is required between the two settings
                    _ = this.PublishVerbose( $"Waiting {MeasureSubsystem.RangeSettlingTimeMilliseconds}ms for meter trigger to settle;. " );
                    TimeSpan.FromMilliseconds( MeasureSubsystem.RangeSettlingTimeMilliseconds ).SpinWait();
                }
            }
        }

        /// <summary> Applies the meter range mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ApplyMeterRangeMode()
        {
            if ( this.Device?.IsDeviceOpen == true )
            {
                if ( this.Device.MeasureSubsystem is object && !Nullable.Equals( this.Device.MeasureSubsystem.RangeMode, this.SelectedMeterRange ) )
                {
                    _ = this.PublishVerbose( "Applying meter range settings;. range={0}; current={1}, mode='{2}'", this._MeterCurrentNumeric.Value, this._MeterRangeNumeric.Value, this.SelectedMeterRange );
                    _ = this.Device.MeasureSubsystem.ApplyRangeMode( this.SelectedMeterRange );
                    _ = this.Device.Session.ReadStatusRegister();
                    // a delay is required between the two settings
                    _ = this.PublishVerbose( $"Waiting {MeasureSubsystem.RangeSettlingTimeMilliseconds}ms for meter trigger to settle;. " );
                    // at this point, 2014-01-31, the first reading comes in too quickly. Trying to detect operation completion using bit 16 of the
                    // service register does not work. So we are resorting to a brute force delay.
                    TimeSpan.FromMilliseconds( MeasureSubsystem.RangeSettlingTimeMilliseconds ).SpinWait();
                }
            }
        }

        /// <summary> Applies the meter trigger delay. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ApplyTriggerDelay()
        {
            if ( this.Device?.IsDeviceOpen == true )
            {
                if ( this.Device.MeasureSubsystem is object && !Nullable.Equals( this.Device.MeasureSubsystem.TriggerDelay, this.TriggerDelay ) )
                {
                    _ = this.PublishVerbose( "Applying trigger delay {0} ms;. ", ( object ) this.TriggerDelay.TotalMilliseconds );
                    _ = this.Device.MeasureSubsystem.ApplyTriggerDelay( this.TriggerDelay );
                    _ = this.Device.Session.ReadStatusRegister();
                    // a delay is required between the two settings
                    _ = this.PublishVerbose( $"Waiting {MeasureSubsystem.RangeSettlingTimeMilliseconds}ms for meter trigger to settle;. " );
                    // at this point, 2014-01-31, the first reading comes in too quickly. Trying to detect operation completion using bit 16 of the
                    // service register does not work. So we are resorting to a brute force delay.
                    TimeSpan.FromMilliseconds( MeasureSubsystem.RangeSettlingTimeMilliseconds ).SpinWait();
                }
            }
        }

        #endregion

        #region " RANGE "

        /// <summary> List range modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ListRangeModes()
        {
            // populate the range mode selector
            this._MeterRangeComboBox.Enabled = false;
            this._MeterRangeComboBox.DataSource = null;
            this._MeterRangeComboBox.Items.Clear();
            this._MeterRangeComboBox.DataSource = typeof( ResistanceRangeMode ).ValueDescriptionPairs().ToList();
            this._MeterRangeComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._MeterRangeComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._MeterRangeComboBox.Enabled = true;
        }

        /// <summary> List range modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="values"> The values. </param>
        public void ListRangeModes( ResistanceRangeMode[] values )
        {
            var keyValuePairs = new ArrayList();
            if ( values is object && values.Count() > 0 )
            {
                foreach ( ResistanceRangeMode value in values )
                    _ = keyValuePairs.Add( value.ValueDescriptionPair() );
            }

            this._MeterRangeComboBox.Enabled = false;
            this._MeterRangeComboBox.DataSource = null;
            this._MeterRangeComboBox.Items.Clear();
            this._MeterRangeComboBox.DataSource = keyValuePairs;
            this._MeterRangeComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._MeterRangeComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._MeterRangeComboBox.Enabled = true;
        }

        /// <summary> List all range modes other than auto range. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ListNonAutoRangeModes()
        {
            var keyValuePairs = new ArrayList();
            foreach ( ResistanceRangeMode value in Enum.GetValues( typeof( ResistanceRangeMode ) ) )
            {
                if ( value != ResistanceRangeMode.R0 )
                {
                    _ = keyValuePairs.Add( value.ValueDescriptionPair() );
                }
            }

            this._MeterRangeComboBox.Enabled = false;
            this._MeterRangeComboBox.DataSource = null;
            this._MeterRangeComboBox.Items.Clear();
            this._MeterRangeComboBox.DataSource = keyValuePairs;
            this._MeterRangeComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._MeterRangeComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._MeterRangeComboBox.Enabled = true;
        }

        /// <summary> Gets the selected meter range. </summary>
        /// <value> The selected meter range. </value>
        public ResistanceRangeMode SelectedMeterRange => ( ResistanceRangeMode ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._MeterRangeComboBox.SelectedItem).Key );

        /// <summary> Selects the meter range based on the range mode description. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="description"> The description. </param>
        public void SelectMeterRange( string description )
        {
            this._MeterRangeComboBox.SilentSelectItem( description );
            this._MeterRangeComboBox.Refresh();
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Selects the meter range based on the range mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void SelectMeterRange( ResistanceRangeMode value )
        {
            _ = this._MeterRangeComboBox.SilentSelectValue( value );
            this._MeterRangeComboBox.Refresh();
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Selects the meter range based on the current and range settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="current"> The current. </param>
        /// <param name="range">   The range. </param>
        public void SelectMeterRange( double current, double range )
        {
            var rangeMode = ResistanceRangeMode.R0;
            if ( MeasureSubsystem.TryMatch( current, range, ref rangeMode ) )
            {
                _ = this._MeterRangeComboBox.SilentSelectValue( rangeMode );
                this._MeterRangeComboBox.Refresh();
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Gets or sets the meter current. </summary>
        /// <value> The meter current. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public decimal MeterCurrent
        {
            get => this._MeterCurrentNumeric.Value;

            set {
                if ( !decimal.Equals( value, this.MeterCurrent ) )
                {
                    _ = this._MeterCurrentNumeric.SilentValueSetter( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the meter range. </summary>
        /// <value> The meter range. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public decimal MeterRange
        {
            get => this._MeterRangeNumeric.Value;

            set {
                if ( !decimal.Equals( value, this.MeterRange ) )
                {
                    _ = this._MeterRangeNumeric.SilentValueSetter( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the range selection as read only. </summary>
        /// <value> The sentinel indicating if the meter range is read only. </value>
        public bool RangeReadOnly
        {
            get => this._MeterRangeComboBox.ReadOnly;

            set {
                if ( this.RangeReadOnly != value )
                {
                    this._MeterRangeComboBox.ReadOnly = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Event handler. Called by _MeterRangeComboBox for selected value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MeterRangeComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            if ( this._MeterRangeComboBox.Enabled )
            {
                ResistanceRangeMode range = ( ResistanceRangeMode ) Conversions.ToInteger( Enum.Parse( typeof( ResistanceRangeMode ), this._MeterRangeComboBox.SelectedValue.ToString() ) );
                double c = default, r = default;
                if ( MeasureSubsystem.TryParse( range, ref c, ref r ) )
                {
                    this.MeterCurrent = ( decimal ) c;
                    // if both range and current change, binding cause the range to restore to its previous value!
                    // the code below allows the first binding event to complete before issuing the change on the second
                    // binding event.
                    for ( int i = 1; i <= 10; i++ )
                        Core.ApplianceBase.DoEvents();
                    this.MeterRange = ( decimal ) r;
                }
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> List trigger modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ListTriggerModes()
        {
            // populate the emulated reply combo. 
            this._TriggerCombo.DataSource = null;
            this._TriggerCombo.Items.Clear();
            this._TriggerCombo.DataSource = typeof( TriggerMode ).ValueDescriptionPairs().ToList();
            this._TriggerCombo.SelectedIndex = 0;
            this._TriggerCombo.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._TriggerCombo.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
        }

        /// <summary> List one shot trigger modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ListOneShotTriggerModes()
        {
            this.ListTriggerModes( new TriggerMode[] { TriggerMode.T1, TriggerMode.T3 } );
        }

        /// <summary> List trigger modes. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="values"> The values. </param>
        public void ListTriggerModes( TriggerMode[] values )
        {
            var keyValuePairs = new ArrayList();
            if ( values is object && values.Count() > 0 )
            {
                foreach ( TriggerMode value in values )
                    _ = keyValuePairs.Add( value.ValueDescriptionPair() );
            }

            // populate the emulated reply combo.
            this._TriggerCombo.DataSource = null;
            this._TriggerCombo.Items.Clear();
            this._TriggerCombo.DataSource = keyValuePairs;
            this._TriggerCombo.SelectedIndex = 0;
            this._TriggerCombo.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            this._TriggerCombo.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
        }

        /// <summary> Gets the selected meter Trigger Mode. </summary>
        /// <value> The selected meter Trigger. </value>
        public TriggerMode SelectedMeterTrigger => ( TriggerMode ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._TriggerCombo.SelectedItem).Key );

        /// <summary> Selects the meter Trigger mode based on the Trigger mode description. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="description"> The description. </param>
        public void SelectMeterTrigger( string description )
        {
            this._TriggerCombo.SilentSelectItem( description );
            this._TriggerCombo.Refresh();
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Selects the meter Trigger mode based on the Trigger mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void SelectMeterTrigger( TriggerMode value )
        {
            _ = this._TriggerCombo.SilentSelectValue( value );
            this._TriggerCombo.Refresh();
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> The trigger delay. </summary>
        private TimeSpan _TriggerDelay;

        /// <summary> Gets or sets the Trigger Delay. </summary>
        /// <value> The Trigger Delay. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan TriggerDelay
        {
            get => this._TriggerDelay;

            set {
                if ( !Equals( value, this.TriggerDelay ) )
                {
                    this._TriggerDelay = value;
                    _ = this._TriggerDelayNumeric.SilentValueSetter( value.TotalMilliseconds );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Event handler. Called by _TriggerDelayNumeric for value changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TriggerDelayNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._TriggerDelayNumeric.Enabled )
            {
                this.TriggerDelay = TimeSpan.FromMilliseconds( ( double ) this._TriggerDelayNumeric.Value );
            }
        }

        #endregion

        #region " VALUES "

        /// <summary> The initial delay. </summary>
        private TimeSpan _InitialDelay;

        /// <summary> Gets or sets the initial delay. </summary>
        /// <value> The initial delay. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TimeSpan InitialDelay
        {
            get => this._InitialDelay;

            set {
                if ( !TimeSpan.Equals( value, this.InitialDelay ) )
                {
                    this._InitialDelay = value;
                    this._InitialDelayNumeric.Value = this._InitialDelayNumeric.SilentValueSetter( value.TotalMilliseconds );
                    if ( this.Device?.IsDeviceOpen == true )
                    {
                        this.Device.MeasureSubsystem.InitialDelay = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Event handler. Called by _InitialDelayNumeric for value changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void InitialDelayNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._InitialDelayNumeric.Enabled )
            {
                this.InitialDelay = TimeSpan.FromMilliseconds( ( double ) this._InitialDelayNumeric.Value );
            }
        }

        /// <summary> The measurement delay. </summary>
        private TimeSpan _MeasurementDelay;

        /// <summary> Gets or sets the Measurement delay. </summary>
        /// <value> The Measurement delay. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TimeSpan MeasurementDelay
        {
            get => this._MeasurementDelay;

            set {
                if ( !TimeSpan.Equals( value, this.MeasurementDelay ) )
                {
                    this._MeasurementDelay = value;
                    this._MeasurementDelayNumeric.Value = this._MeasurementDelayNumeric.SilentValueSetter( value.TotalMilliseconds );
                    if ( this.Device?.IsDeviceOpen == true )
                    {
                        this.Device.MeasureSubsystem.MeasurementDelay = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Event handler. Called by _MeasurementDelayNumeric for value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MeasurementDelayNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._MeasurementDelayNumeric.Enabled )
            {
                this.MeasurementDelay = TimeSpan.FromMilliseconds( ( double ) this._MeasurementDelayNumeric.Value );
            }
        }

        /// <summary> Number of maximum trials. </summary>
        private int _MaximumTrialsCount;

        /// <summary> Gets or sets the maximum trial count. </summary>
        /// <value> The maximum number of trials before giving up. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public int MaximumTrialsCount
        {
            get => this._MaximumTrialsCount;

            set {
                if ( !Equals( value, this.MaximumTrialsCount ) )
                {
                    this._MaximumTrialsCount = value;
                    this._MaximumTrialsCountNumeric.Value = this._MaximumTrialsCountNumeric.SilentValueSetter( ( decimal ) value );
                    if ( this.Device?.IsDeviceOpen == true )
                    {
                        this.Device.MeasureSubsystem.MaximumTrialsCount = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Event handler. Called by _MaximumTrialsCountNumeric for value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MaximumTrialsCountNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._MaximumTrialsCountNumeric.Enabled )
            {
                this.MaximumTrialsCount = ( int ) Math.Round( this._MaximumTrialsCountNumeric.Value );
            }
        }

        /// <summary> The maximum difference. </summary>
        private decimal _MaximumDifference;

        /// <summary> Gets or sets the maximum difference between consecutive measurements. </summary>
        /// <value> The maximum difference between consecutive measurements. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public decimal MaximumDifference
        {
            get => this._MaximumDifference;

            set {
                if ( !decimal.Equals( value, this.MaximumDifference ) )
                {
                    this._MaximumDifference = value;
                    _ = this._MaximumDifferenceNumeric.SilentValueSetter( 100m * value );
                    if ( this.Device?.IsDeviceOpen == true )
                    {
                        this.Device.MeasureSubsystem.MaximumDifference = ( double ) this.MaximumDifference;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Event handler. Called by _MaximumDifferenceNumeric for value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MaximumDifferenceNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this._MaximumDifferenceNumeric.Enabled )
            {
                this.MaximumDifference = ( decimal ) (0.01d * ( double ) this._MaximumDifferenceNumeric.Value);
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
