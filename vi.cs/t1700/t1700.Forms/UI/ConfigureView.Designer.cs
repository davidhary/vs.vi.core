﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.T1700.Forms
{
    [DesignerGenerated()]
    public partial class ConfigureView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _RangeComboBox = new System.Windows.Forms.ComboBox();
            _TriggerCombo = new System.Windows.Forms.ComboBox();
            _TriggerComboBoxLabel = new System.Windows.Forms.Label();
            __ConfigureButton = new System.Windows.Forms.Button();
            __ConfigureButton.Click += new EventHandler(ConfigureButton_Click);
            _RangeComboBoxLabel = new System.Windows.Forms.Label();
            _ConfigureGroupBox = new System.Windows.Forms.GroupBox();
            _Layout.SuspendLayout();
            _ConfigureGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001f));
            _Layout.Controls.Add(_ConfigureGroupBox, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.00001f));
            _Layout.Size = new System.Drawing.Size(383, 326);
            _Layout.TabIndex = 0;
            // 
            // _RangeComboBox
            // 
            _RangeComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _RangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _RangeComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _RangeComboBox.Location = new System.Drawing.Point(11, 41);
            _RangeComboBox.Name = "_RangeComboBox";
            _RangeComboBox.Size = new System.Drawing.Size(310, 25);
            _RangeComboBox.TabIndex = 1;
            // 
            // _TriggerCombo
            // 
            _TriggerCombo.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _TriggerCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _TriggerCombo.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _TriggerCombo.Items.AddRange(new object[] { "I", "V" });
            _TriggerCombo.Location = new System.Drawing.Point(11, 92);
            _TriggerCombo.Name = "_TriggerCombo";
            _TriggerCombo.Size = new System.Drawing.Size(310, 25);
            _TriggerCombo.TabIndex = 3;
            // 
            // _TriggerComboBoxLabel
            // 
            _TriggerComboBoxLabel.Location = new System.Drawing.Point(9, 69);
            _TriggerComboBoxLabel.Name = "_TriggerComboBoxLabel";
            _TriggerComboBoxLabel.Size = new System.Drawing.Size(62, 21);
            _TriggerComboBoxLabel.TabIndex = 2;
            _TriggerComboBoxLabel.Text = "Trigger: ";
            _TriggerComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _ConfigureButton
            // 
            __ConfigureButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __ConfigureButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ConfigureButton.Location = new System.Drawing.Point(263, 132);
            __ConfigureButton.Name = "__ConfigureButton";
            __ConfigureButton.Size = new System.Drawing.Size(58, 30);
            __ConfigureButton.TabIndex = 4;
            __ConfigureButton.Text = "&Apply";
            __ConfigureButton.UseVisualStyleBackColor = true;
            // 
            // _RangeComboBoxLabel
            // 
            _RangeComboBoxLabel.AutoSize = true;
            _RangeComboBoxLabel.Location = new System.Drawing.Point(9, 21);
            _RangeComboBoxLabel.Name = "_RangeComboBoxLabel";
            _RangeComboBoxLabel.Size = new System.Drawing.Size(52, 17);
            _RangeComboBoxLabel.TabIndex = 0;
            _RangeComboBoxLabel.Text = "Range: ";
            _RangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _ConfigureGroupBox
            // 
            _ConfigureGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _ConfigureGroupBox.Controls.Add(_RangeComboBox);
            _ConfigureGroupBox.Controls.Add(_TriggerCombo);
            _ConfigureGroupBox.Controls.Add(_TriggerComboBoxLabel);
            _ConfigureGroupBox.Controls.Add(__ConfigureButton);
            _ConfigureGroupBox.Controls.Add(_RangeComboBoxLabel);
            _ConfigureGroupBox.Location = new System.Drawing.Point(25, 78);
            _ConfigureGroupBox.Name = "_ConfigureGroupBox";
            _ConfigureGroupBox.Size = new System.Drawing.Size(332, 169);
            _ConfigureGroupBox.TabIndex = 9;
            _ConfigureGroupBox.TabStop = false;
            _ConfigureGroupBox.Text = "Configure Measurement";
            // 
            // ConfigureView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "ConfigureView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            _Layout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)InfoProvider).EndInit();
            _ConfigureGroupBox.ResumeLayout(false);
            _ConfigureGroupBox.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.GroupBox _ConfigureGroupBox;
        private System.Windows.Forms.ComboBox _RangeComboBox;
        private System.Windows.Forms.ComboBox _TriggerCombo;
        private System.Windows.Forms.Label _TriggerComboBoxLabel;
        private System.Windows.Forms.Button __ConfigureButton;

        private System.Windows.Forms.Button _ConfigureButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ConfigureButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ConfigureButton != null)
                {
                    __ConfigureButton.Click -= ConfigureButton_Click;
                }

                __ConfigureButton = value;
                if (__ConfigureButton != null)
                {
                    __ConfigureButton.Click += ConfigureButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _RangeComboBoxLabel;
    }
}