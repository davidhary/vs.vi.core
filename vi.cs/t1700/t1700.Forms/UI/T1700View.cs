using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.T1700.Forms
{

    /// <summary> Tegam 1750 Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "Tegam T1700 User Interface" )]
    [Description( "Tegam 1750 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( T1700View ) )]
    public class T1700View : Facade.VisaView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public T1700View() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            int index = 0;
            index += 1;
            this.ReadingView = ReadingView.Create();
            this.AddView( new Facade.VisaViewControl( this.ReadingView, index, "_ReadingTabPage", "Read" ) );
            index += 1;
            this.ConfigureView = ConfigureView.Create();
            this.AddView( new Facade.VisaViewControl( this.ConfigureView, index, "_ConfigureTabPage", "Configure" ) );
            index += 1;
            this.MeasureView = MeasureView.Create();
            this.AddView( new Facade.VisaViewControl( this.MeasureView, index, "_MeasureTabPage", "Measure" ) );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public T1700View( T1700Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the T1750 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    this.AssignDeviceThis( null );
                    if ( this.ReadingView is object )
                    {
                        this.ReadingView.Dispose();
                        this.ReadingView = null;
                    }

                    if ( this.MeasureView is object )
                    {
                        this.MeasureView.Dispose();
                        this.MeasureView = null;
                    }

                    if ( this.ConfigureView is object )
                    {
                        this.ConfigureView.Dispose();
                        this.ConfigureView = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public T1700Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( T1700Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = T1700.My.MySettings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( T1700Device value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.ReadingView.AssignDevice( null );
                this.ConfigureView.AssignDevice( null );
                this.MeasureView.AssignDevice( null );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            this.DisplayView.BindMeasureToolStrip( this.Device.MeasureSubsystem );
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.ReadingView.AssignDevice( this.Device );
            this.ConfigureView.AssignDevice( this.Device );
            this.MeasureView.AssignDevice( this.Device );
            this.DisplayView.BindMeasureToolStrip( this.Device.MeasureSubsystem );
        }

        #endregion

        #endregion

        #region " VIEWS  "

        /// <summary> Gets or sets the measure view. </summary>
        /// <value> The measure view. </value>
        private MeasureView MeasureView { get; set; }

        /// <summary> Gets or sets the reading view. </summary>
        /// <value> The reading view. </value>
        private ReadingView ReadingView { get; set; }

        /// <summary> Gets or sets the configure view. </summary>
        /// <value> The configure view. </value>
        private ConfigureView ConfigureView { get; set; }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // T1750View
            // 
            this.Name = "T1750View";
            this.Size = new System.Drawing.Size( 559, 450 );
            this.ResumeLayout( false );
        }

        #endregion

    }
}
