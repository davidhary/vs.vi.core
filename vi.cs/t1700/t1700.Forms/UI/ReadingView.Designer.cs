﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.T1700.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _MeasureSettingsLabel = new System.Windows.Forms.Label();
            __MeasureButton = new System.Windows.Forms.Button();
            __MeasureButton.Click += new EventHandler(MeasureButton_Click);
            _PostReadingDelayNumericLabel = new System.Windows.Forms.Label();
            _PostReadingDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _ReadContinuouslyCheckBox = new System.Windows.Forms.CheckBox();
            _CommandComboBox = new System.Windows.Forms.ComboBox();
            __WriteButton = new System.Windows.Forms.Button();
            __WriteButton.Click += new EventHandler(WriteButton_Click);
            __ReadButton = new System.Windows.Forms.Button();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __ReadSRQButton = new System.Windows.Forms.Button();
            __ReadSRQButton.Click += new EventHandler(ReadSRQButton_Click);
            _Panel = new System.Windows.Forms.Panel();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)_PostReadingDelayNumeric).BeginInit();
            _Panel.SuspendLayout();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _MeasureSettingsLabel
            // 
            _MeasureSettingsLabel.Location = new System.Drawing.Point(97, 103);
            _MeasureSettingsLabel.Name = "_MeasureSettingsLabel";
            _MeasureSettingsLabel.Size = new System.Drawing.Size(199, 41);
            _MeasureSettingsLabel.TabIndex = 26;
            _MeasureSettingsLabel.Text = "5 measurements, 150ms delays, 1% accuracy.";
            // 
            // _MeasureButton
            // 
            __MeasureButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __MeasureButton.Location = new System.Drawing.Point(4, 104);
            __MeasureButton.Name = "__MeasureButton";
            __MeasureButton.Size = new System.Drawing.Size(75, 30);
            __MeasureButton.TabIndex = 25;
            __MeasureButton.Text = "&Measure";
            __MeasureButton.UseVisualStyleBackColor = true;
            // 
            // _PostReadingDelayNumericLabel
            // 
            _PostReadingDelayNumericLabel.AutoSize = true;
            _PostReadingDelayNumericLabel.Location = new System.Drawing.Point(172, 15);
            _PostReadingDelayNumericLabel.Name = "_PostReadingDelayNumericLabel";
            _PostReadingDelayNumericLabel.Size = new System.Drawing.Size(72, 17);
            _PostReadingDelayNumericLabel.TabIndex = 20;
            _PostReadingDelayNumericLabel.Text = "Delay [ms]:";
            // 
            // _PostReadingDelayNumeric
            // 
            _PostReadingDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PostReadingDelayNumeric.Location = new System.Drawing.Point(247, 11);
            _PostReadingDelayNumeric.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            _PostReadingDelayNumeric.Name = "_PostReadingDelayNumeric";
            _PostReadingDelayNumeric.Size = new System.Drawing.Size(49, 25);
            _PostReadingDelayNumeric.TabIndex = 21;
            // 
            // _ReadContinuouslyCheckBox
            // 
            _ReadContinuouslyCheckBox.AutoSize = true;
            _ReadContinuouslyCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ReadContinuouslyCheckBox.Location = new System.Drawing.Point(68, 13);
            _ReadContinuouslyCheckBox.Name = "_ReadContinuouslyCheckBox";
            _ReadContinuouslyCheckBox.Size = new System.Drawing.Size(98, 21);
            _ReadContinuouslyCheckBox.TabIndex = 19;
            _ReadContinuouslyCheckBox.Text = "Continuous";
            _ReadContinuouslyCheckBox.UseVisualStyleBackColor = true;
            // 
            // _CommandComboBox
            // 
            _CommandComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _CommandComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _CommandComboBox.FormattingEnabled = true;
            _CommandComboBox.Location = new System.Drawing.Point(68, 55);
            _CommandComboBox.Name = "_CommandComboBox";
            _CommandComboBox.Size = new System.Drawing.Size(63, 25);
            _CommandComboBox.TabIndex = 23;
            _CommandComboBox.Text = "D111x";
            // 
            // _WriteButton
            // 
            __WriteButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __WriteButton.Location = new System.Drawing.Point(4, 52);
            __WriteButton.Name = "__WriteButton";
            __WriteButton.Size = new System.Drawing.Size(58, 30);
            __WriteButton.TabIndex = 22;
            __WriteButton.Text = "&Write";
            __WriteButton.UseVisualStyleBackColor = true;
            // 
            // _ReadButton
            // 
            __ReadButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ReadButton.Location = new System.Drawing.Point(4, 8);
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(58, 30);
            __ReadButton.TabIndex = 18;
            __ReadButton.Text = "&Read";
            __ReadButton.UseVisualStyleBackColor = true;
            // 
            // _ReadSRQButton
            // 
            __ReadSRQButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __ReadSRQButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ReadSRQButton.Location = new System.Drawing.Point(137, 55);
            __ReadSRQButton.Name = "__ReadSRQButton";
            __ReadSRQButton.Size = new System.Drawing.Size(160, 30);
            __ReadSRQButton.TabIndex = 24;
            __ReadSRQButton.Text = "Read &Service Request";
            __ReadSRQButton.UseVisualStyleBackColor = true;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_MeasureSettingsLabel);
            _Panel.Controls.Add(__MeasureButton);
            _Panel.Controls.Add(_PostReadingDelayNumericLabel);
            _Panel.Controls.Add(_PostReadingDelayNumeric);
            _Panel.Controls.Add(_ReadContinuouslyCheckBox);
            _Panel.Controls.Add(_CommandComboBox);
            _Panel.Controls.Add(__WriteButton);
            _Panel.Controls.Add(__ReadButton);
            _Panel.Controls.Add(__ReadSRQButton);
            _Panel.Location = new System.Drawing.Point(38, 85);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(307, 155);
            _Panel.TabIndex = 0;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(383, 326);
            _Layout.TabIndex = 1;
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "ReadingView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            ((System.ComponentModel.ISupportInitialize)_PostReadingDelayNumeric).EndInit();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.Label _MeasureSettingsLabel;
        private System.Windows.Forms.Button __MeasureButton;

        private System.Windows.Forms.Button _MeasureButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click -= MeasureButton_Click;
                }

                __MeasureButton = value;
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click += MeasureButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _PostReadingDelayNumericLabel;
        private System.Windows.Forms.NumericUpDown _PostReadingDelayNumeric;
        private System.Windows.Forms.CheckBox _ReadContinuouslyCheckBox;
        private System.Windows.Forms.ComboBox _CommandComboBox;
        private System.Windows.Forms.Button __WriteButton;

        private System.Windows.Forms.Button _WriteButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteButton != null)
                {
                    __WriteButton.Click -= WriteButton_Click;
                }

                __WriteButton = value;
                if (__WriteButton != null)
                {
                    __WriteButton.Click += WriteButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ReadButton;

        private System.Windows.Forms.Button _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ReadSRQButton;

        private System.Windows.Forms.Button _ReadSRQButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSRQButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSRQButton != null)
                {
                    __ReadSRQButton.Click -= ReadSRQButton_Click;
                }

                __ReadSRQButton = value;
                if (__ReadSRQButton != null)
                {
                    __ReadSRQButton.Click += ReadSRQButton_Click;
                }
            }
        }
    }
}