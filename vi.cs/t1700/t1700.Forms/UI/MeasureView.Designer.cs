﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.T1700.Forms
{
    [DesignerGenerated()]
    public partial class MeasureView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __MeterRangeComboBox = new Core.Controls.ComboBox();
            __MeterRangeComboBox.SelectedValueChanged += new EventHandler(MeterRangeComboBox_SelectedValueChanged);
            __MaximumTrialsCountNumeric = new Core.Controls.NumericUpDown();
            __MaximumTrialsCountNumeric.ValueChanged += new EventHandler(MaximumTrialsCountNumeric_ValueChanged);
            _MeterRangeNumeric = new Core.Controls.NumericUpDown();
            __MaximumDifferenceNumeric = new Core.Controls.NumericUpDown();
            __MaximumDifferenceNumeric.ValueChanged += new EventHandler(MaximumDifferenceNumeric_ValueChanged);
            __MeasurementDelayNumeric = new Core.Controls.NumericUpDown();
            __MeasurementDelayNumeric.ValueChanged += new EventHandler(MeasurementDelayNumeric_ValueChanged);
            __InitialDelayNumeric = new Core.Controls.NumericUpDown();
            __InitialDelayNumeric.ValueChanged += new EventHandler(InitialDelayNumeric_ValueChanged);
            __TriggerDelayNumeric = new Core.Controls.NumericUpDown();
            __TriggerDelayNumeric.ValueChanged += new EventHandler(TriggerDelayNumeric_ValueChanged);
            _MeterCurrentNumeric = new Core.Controls.NumericUpDown();
            _MeterRangeNumericLabel = new System.Windows.Forms.Label();
            _MeterCurrentNumericLabel = new System.Windows.Forms.Label();
            _MaximumDifferenceNumericLabel = new System.Windows.Forms.Label();
            _MeterRangeComboBoxLabel = new System.Windows.Forms.Label();
            _MaximumTrialsCountNumericLabel = new System.Windows.Forms.Label();
            _MeasurementDelayNumericLabel = new System.Windows.Forms.Label();
            _InitialDelayNumericLabel = new System.Windows.Forms.Label();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            _TriggerCombo = new System.Windows.Forms.ComboBox();
            _TriggerComboBoxLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)__MaximumTrialsCountNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_MeterRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__MaximumDifferenceNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__MeasurementDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__InitialDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__TriggerDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_MeterCurrentNumeric).BeginInit();
            SuspendLayout();
            // 
            // _MeterRangeComboBox
            // 
            __MeterRangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __MeterRangeComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __MeterRangeComboBox.FormattingEnabled = true;
            __MeterRangeComboBox.Location = new System.Drawing.Point(77, 7);
            __MeterRangeComboBox.Name = "__MeterRangeComboBox";
            __MeterRangeComboBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __MeterRangeComboBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __MeterRangeComboBox.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __MeterRangeComboBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __MeterRangeComboBox.Size = new System.Drawing.Size(457, 25);
            __MeterRangeComboBox.TabIndex = 16;
            // 
            // _MaximumTrialsCountNumeric
            // 
            __MaximumTrialsCountNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __MaximumTrialsCountNumeric.Location = new System.Drawing.Point(178, 150);
            __MaximumTrialsCountNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            __MaximumTrialsCountNumeric.Name = "__MaximumTrialsCountNumeric";
            __MaximumTrialsCountNumeric.NullValue = new decimal(new int[] { 6, 0, 0, 0 });
            __MaximumTrialsCountNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __MaximumTrialsCountNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __MaximumTrialsCountNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __MaximumTrialsCountNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __MaximumTrialsCountNumeric.Size = new System.Drawing.Size(76, 25);
            __MaximumTrialsCountNumeric.TabIndex = 30;
            __MaximumTrialsCountNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(__MaximumTrialsCountNumeric, "Maximum number of trials before giving up");
            __MaximumTrialsCountNumeric.Value = new decimal(new int[] { 6, 0, 0, 0 });
            // 
            // _MeterRangeNumeric
            // 
            _MeterRangeNumeric.BackColor = System.Drawing.SystemColors.Control;
            _MeterRangeNumeric.DecimalPlaces = 3;
            _MeterRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _MeterRangeNumeric.ForeColor = System.Drawing.SystemColors.WindowText;
            _MeterRangeNumeric.Location = new System.Drawing.Point(420, 45);
            _MeterRangeNumeric.Maximum = new decimal(new int[] { 20000000, 0, 0, 0 });
            _MeterRangeNumeric.Name = "_MeterRangeNumeric";
            _MeterRangeNumeric.NullValue = new decimal(new int[] { 2000000, 0, 0, 0 });
            _MeterRangeNumeric.ReadOnly = true;
            _MeterRangeNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            _MeterRangeNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            _MeterRangeNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            _MeterRangeNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            _MeterRangeNumeric.Size = new System.Drawing.Size(114, 25);
            _MeterRangeNumeric.TabIndex = 20;
            _MeterRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _MeterRangeNumeric.Value = new decimal(new int[] { 2000000, 0, 0, 0 });
            // 
            // _MaximumDifferenceNumeric
            // 
            __MaximumDifferenceNumeric.DecimalPlaces = 1;
            __MaximumDifferenceNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __MaximumDifferenceNumeric.Location = new System.Drawing.Point(419, 150);
            __MaximumDifferenceNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 65536 });
            __MaximumDifferenceNumeric.Name = "__MaximumDifferenceNumeric";
            __MaximumDifferenceNumeric.NullValue = new decimal(new int[] { 2, 0, 0, 0 });
            __MaximumDifferenceNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __MaximumDifferenceNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __MaximumDifferenceNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __MaximumDifferenceNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __MaximumDifferenceNumeric.Size = new System.Drawing.Size(76, 25);
            __MaximumDifferenceNumeric.TabIndex = 29;
            __MaximumDifferenceNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(__MaximumDifferenceNumeric, "Maximum difference between measurements");
            __MaximumDifferenceNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _MeasurementDelayNumeric
            // 
            __MeasurementDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __MeasurementDelayNumeric.Location = new System.Drawing.Point(420, 114);
            __MeasurementDelayNumeric.Maximum = new decimal(new int[] { 200, 0, 0, 0 });
            __MeasurementDelayNumeric.Name = "__MeasurementDelayNumeric";
            __MeasurementDelayNumeric.NullValue = new decimal(new int[] { 149, 0, 0, 0 });
            __MeasurementDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __MeasurementDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __MeasurementDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __MeasurementDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __MeasurementDelayNumeric.Size = new System.Drawing.Size(76, 25);
            __MeasurementDelayNumeric.TabIndex = 28;
            __MeasurementDelayNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(__MeasurementDelayNumeric, "Delay of measurement 2 and up");
            __MeasurementDelayNumeric.Value = new decimal(new int[] { 149, 0, 0, 0 });
            // 
            // _InitialDelayNumeric
            // 
            __InitialDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __InitialDelayNumeric.Location = new System.Drawing.Point(178, 114);
            __InitialDelayNumeric.Maximum = new decimal(new int[] { 200, 0, 0, 0 });
            __InitialDelayNumeric.Name = "__InitialDelayNumeric";
            __InitialDelayNumeric.NullValue = new decimal(new int[] { 149, 0, 0, 0 });
            __InitialDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __InitialDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __InitialDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __InitialDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __InitialDelayNumeric.Size = new System.Drawing.Size(76, 25);
            __InitialDelayNumeric.TabIndex = 31;
            __InitialDelayNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(__InitialDelayNumeric, "Delay of first measurement");
            __InitialDelayNumeric.Value = new decimal(new int[] { 149, 0, 0, 0 });
            // 
            // _TriggerDelayNumeric
            // 
            __TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TriggerDelayNumeric.Location = new System.Drawing.Point(420, 79);
            __TriggerDelayNumeric.Maximum = new decimal(new int[] { 200, 0, 0, 0 });
            __TriggerDelayNumeric.Name = "__TriggerDelayNumeric";
            __TriggerDelayNumeric.NullValue = new decimal(new int[] { 111, 0, 0, 0 });
            __TriggerDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __TriggerDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __TriggerDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __TriggerDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __TriggerDelayNumeric.Size = new System.Drawing.Size(76, 25);
            __TriggerDelayNumeric.TabIndex = 32;
            __TriggerDelayNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(__TriggerDelayNumeric, "Instrument trigger delay");
            __TriggerDelayNumeric.Value = new decimal(new int[] { 111, 0, 0, 0 });
            // 
            // _MeterCurrentNumeric
            // 
            _MeterCurrentNumeric.BackColor = System.Drawing.SystemColors.Control;
            _MeterCurrentNumeric.DecimalPlaces = 7;
            _MeterCurrentNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _MeterCurrentNumeric.ForeColor = System.Drawing.SystemColors.WindowText;
            _MeterCurrentNumeric.Location = new System.Drawing.Point(77, 45);
            _MeterCurrentNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            _MeterCurrentNumeric.Name = "_MeterCurrentNumeric";
            _MeterCurrentNumeric.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _MeterCurrentNumeric.ReadOnly = true;
            _MeterCurrentNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            _MeterCurrentNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            _MeterCurrentNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            _MeterCurrentNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            _MeterCurrentNumeric.Size = new System.Drawing.Size(98, 25);
            _MeterCurrentNumeric.TabIndex = 18;
            _MeterCurrentNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _MeterCurrentNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _MeterRangeNumericLabel
            // 
            _MeterRangeNumericLabel.AutoSize = true;
            _MeterRangeNumericLabel.Location = new System.Drawing.Point(330, 49);
            _MeterRangeNumericLabel.Name = "_MeterRangeNumericLabel";
            _MeterRangeNumericLabel.Size = new System.Drawing.Size(88, 17);
            _MeterRangeNumericLabel.TabIndex = 19;
            _MeterRangeNumericLabel.Text = "Range [Ohm]:";
            _MeterRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _MeterCurrentNumericLabel
            // 
            _MeterCurrentNumericLabel.AutoSize = true;
            _MeterCurrentNumericLabel.Location = new System.Drawing.Point(1, 49);
            _MeterCurrentNumericLabel.Name = "_MeterCurrentNumericLabel";
            _MeterCurrentNumericLabel.Size = new System.Drawing.Size(74, 17);
            _MeterCurrentNumericLabel.TabIndex = 17;
            _MeterCurrentNumericLabel.Text = "Current [A]:";
            _MeterCurrentNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _MaximumDifferenceNumericLabel
            // 
            _MaximumDifferenceNumericLabel.AutoSize = true;
            _MaximumDifferenceNumericLabel.Location = new System.Drawing.Point(263, 154);
            _MaximumDifferenceNumericLabel.Name = "_MaximumDifferenceNumericLabel";
            _MaximumDifferenceNumericLabel.Size = new System.Drawing.Size(154, 17);
            _MaximumDifferenceNumericLabel.TabIndex = 23;
            _MaximumDifferenceNumericLabel.Text = "Maximum Difference [%]:";
            // 
            // _MeterRangeComboBoxLabel
            // 
            _MeterRangeComboBoxLabel.AutoSize = true;
            _MeterRangeComboBoxLabel.Location = new System.Drawing.Point(27, 11);
            _MeterRangeComboBoxLabel.Name = "_MeterRangeComboBoxLabel";
            _MeterRangeComboBoxLabel.Size = new System.Drawing.Size(48, 17);
            _MeterRangeComboBoxLabel.TabIndex = 15;
            _MeterRangeComboBoxLabel.Text = "Range:";
            _MeterRangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _MaximumTrialsCountNumericLabel
            // 
            _MaximumTrialsCountNumericLabel.AutoSize = true;
            _MaximumTrialsCountNumericLabel.Location = new System.Drawing.Point(8, 154);
            _MaximumTrialsCountNumericLabel.Name = "_MaximumTrialsCountNumericLabel";
            _MaximumTrialsCountNumericLabel.Size = new System.Drawing.Size(170, 17);
            _MaximumTrialsCountNumericLabel.TabIndex = 24;
            _MaximumTrialsCountNumericLabel.Text = "Maximum Number of Trials:";
            // 
            // _MeasurementDelayNumericLabel
            // 
            _MeasurementDelayNumericLabel.AutoSize = true;
            _MeasurementDelayNumericLabel.Location = new System.Drawing.Point(262, 118);
            _MeasurementDelayNumericLabel.Name = "_MeasurementDelayNumericLabel";
            _MeasurementDelayNumericLabel.Size = new System.Drawing.Size(156, 17);
            _MeasurementDelayNumericLabel.TabIndex = 25;
            _MeasurementDelayNumericLabel.Text = "Measurement Delay [ms]:";
            _MeasurementDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _InitialDelayNumericLabel
            // 
            _InitialDelayNumericLabel.AutoSize = true;
            _InitialDelayNumericLabel.Location = new System.Drawing.Point(70, 118);
            _InitialDelayNumericLabel.Name = "_InitialDelayNumericLabel";
            _InitialDelayNumericLabel.Size = new System.Drawing.Size(106, 17);
            _InitialDelayNumericLabel.TabIndex = 26;
            _InitialDelayNumericLabel.Text = "Initial Delay [ms]:";
            _InitialDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(299, 83);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(118, 17);
            _TriggerDelayNumericLabel.TabIndex = 27;
            _TriggerDelayNumericLabel.Text = "Trigger Delay [ms]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _TriggerCombo
            // 
            _TriggerCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _TriggerCombo.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _TriggerCombo.Items.AddRange(new object[] { "Delay Continuous (T2)" });
            _TriggerCombo.Location = new System.Drawing.Point(77, 79);
            _TriggerCombo.Name = "_TriggerCombo";
            _TriggerCombo.Size = new System.Drawing.Size(177, 25);
            _TriggerCombo.TabIndex = 22;
            // 
            // _TriggerComboBoxLabel
            // 
            _TriggerComboBoxLabel.AutoSize = true;
            _TriggerComboBoxLabel.Location = new System.Drawing.Point(21, 83);
            _TriggerComboBoxLabel.Name = "_TriggerComboBoxLabel";
            _TriggerComboBoxLabel.Size = new System.Drawing.Size(53, 17);
            _TriggerComboBoxLabel.TabIndex = 21;
            _TriggerComboBoxLabel.Text = "Trigger:";
            _TriggerComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // MeasureView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(__MeterRangeComboBox);
            Controls.Add(__MaximumTrialsCountNumeric);
            Controls.Add(_MeterRangeNumeric);
            Controls.Add(__MaximumDifferenceNumeric);
            Controls.Add(__MeasurementDelayNumeric);
            Controls.Add(_MeterCurrentNumeric);
            Controls.Add(__InitialDelayNumeric);
            Controls.Add(_MeterRangeNumericLabel);
            Controls.Add(__TriggerDelayNumeric);
            Controls.Add(_MeterCurrentNumericLabel);
            Controls.Add(_MaximumDifferenceNumericLabel);
            Controls.Add(_MeterRangeComboBoxLabel);
            Controls.Add(_MaximumTrialsCountNumericLabel);
            Controls.Add(_MeasurementDelayNumericLabel);
            Controls.Add(_InitialDelayNumericLabel);
            Controls.Add(_TriggerDelayNumericLabel);
            Controls.Add(_TriggerCombo);
            Controls.Add(_TriggerComboBoxLabel);
            Name = "MeasureView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(534, 182);
            ((System.ComponentModel.ISupportInitialize)__MaximumTrialsCountNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_MeterRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__MaximumDifferenceNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__MeasurementDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__InitialDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__TriggerDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_MeterCurrentNumeric).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private Core.Controls.ComboBox __MeterRangeComboBox;

        private Core.Controls.ComboBox _MeterRangeComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeterRangeComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeterRangeComboBox != null)
                {
                    __MeterRangeComboBox.SelectedValueChanged -= MeterRangeComboBox_SelectedValueChanged;
                }

                __MeterRangeComboBox = value;
                if (__MeterRangeComboBox != null)
                {
                    __MeterRangeComboBox.SelectedValueChanged += MeterRangeComboBox_SelectedValueChanged;
                }
            }
        }

        private Core.Controls.NumericUpDown __MaximumTrialsCountNumeric;

        private Core.Controls.NumericUpDown _MaximumTrialsCountNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MaximumTrialsCountNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MaximumTrialsCountNumeric != null)
                {
                    __MaximumTrialsCountNumeric.ValueChanged -= MaximumTrialsCountNumeric_ValueChanged;
                }

                __MaximumTrialsCountNumeric = value;
                if (__MaximumTrialsCountNumeric != null)
                {
                    __MaximumTrialsCountNumeric.ValueChanged += MaximumTrialsCountNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.NumericUpDown _MeterRangeNumeric;
        private Core.Controls.NumericUpDown __MaximumDifferenceNumeric;

        private Core.Controls.NumericUpDown _MaximumDifferenceNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MaximumDifferenceNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MaximumDifferenceNumeric != null)
                {
                    __MaximumDifferenceNumeric.ValueChanged -= MaximumDifferenceNumeric_ValueChanged;
                }

                __MaximumDifferenceNumeric = value;
                if (__MaximumDifferenceNumeric != null)
                {
                    __MaximumDifferenceNumeric.ValueChanged += MaximumDifferenceNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.NumericUpDown __MeasurementDelayNumeric;

        private Core.Controls.NumericUpDown _MeasurementDelayNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasurementDelayNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasurementDelayNumeric != null)
                {
                    __MeasurementDelayNumeric.ValueChanged -= MeasurementDelayNumeric_ValueChanged;
                }

                __MeasurementDelayNumeric = value;
                if (__MeasurementDelayNumeric != null)
                {
                    __MeasurementDelayNumeric.ValueChanged += MeasurementDelayNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.NumericUpDown _MeterCurrentNumeric;
        private Core.Controls.NumericUpDown __InitialDelayNumeric;

        private Core.Controls.NumericUpDown _InitialDelayNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitialDelayNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitialDelayNumeric != null)
                {
                    __InitialDelayNumeric.ValueChanged -= InitialDelayNumeric_ValueChanged;
                }

                __InitialDelayNumeric = value;
                if (__InitialDelayNumeric != null)
                {
                    __InitialDelayNumeric.ValueChanged += InitialDelayNumeric_ValueChanged;
                }
            }
        }

        private System.Windows.Forms.Label _MeterRangeNumericLabel;
        private Core.Controls.NumericUpDown __TriggerDelayNumeric;

        private Core.Controls.NumericUpDown _TriggerDelayNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TriggerDelayNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TriggerDelayNumeric != null)
                {
                    __TriggerDelayNumeric.ValueChanged -= TriggerDelayNumeric_ValueChanged;
                }

                __TriggerDelayNumeric = value;
                if (__TriggerDelayNumeric != null)
                {
                    __TriggerDelayNumeric.ValueChanged += TriggerDelayNumeric_ValueChanged;
                }
            }
        }

        private System.Windows.Forms.Label _MeterCurrentNumericLabel;
        private System.Windows.Forms.Label _MaximumDifferenceNumericLabel;
        private System.Windows.Forms.Label _MeterRangeComboBoxLabel;
        private System.Windows.Forms.Label _MaximumTrialsCountNumericLabel;
        private System.Windows.Forms.Label _MeasurementDelayNumericLabel;
        private System.Windows.Forms.Label _InitialDelayNumericLabel;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
        private System.Windows.Forms.ComboBox _TriggerCombo;
        private System.Windows.Forms.Label _TriggerComboBoxLabel;
    }
}