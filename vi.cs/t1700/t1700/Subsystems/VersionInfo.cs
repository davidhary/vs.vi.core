using System;
using System.Collections.Generic;

namespace isr.VI.T1700
{

    /// <summary> Information about the version of a Tegam 1750 Resistance Measuring System. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-07 </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks> <c>Tegam 1750 D02.20  Apr 26,2000.</c>. </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument <see cref="VersionInfoBase.FirmwareRevisionElements">board
        ///                         revisions</see>
        ///                         e.g., <c>D02.20</c> for the digital and display boards. </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '.' ) );

                // Rev: D02.20
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Digital.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Display.ToString(), revSections.Dequeue().Trim() );
            }
        }

        /// <summary> Parses the instrument identity string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> Specifies the instrument identity string, which includes at a minimum the
        ///                      following information:
        ///                      <see cref="VersionInfoBase.ManufacturerName">manufacturer</see>,
        ///                      <see cref="VersionInfoBase.Model">model</see>,
        ///                      <see cref="VersionInfoBase.SerialNumber">serial number</see>, e.g., <para>
        ///                                               <c>Tegam 1750 D02.20  Apr 26,2000.</c>.</para> 
        /// </param>
        public override void Parse( string value )
        {
            this.Identity = value;
            this.ManufacturerName = "Tegam";
            this.Model = "1750";
            this.SerialNumber = "1";
            this.ParseFirmwareRevision( "D02.20" );
        }
    }
}
