using System;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.T1700
{

    /// <summary> Device error. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-10-07 </para>
    /// </remarks>
    public class DeviceError : VI.DeviceError
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public DeviceError() : base( Pith.Scpi.Syntax.NoErrorCompoundMessage )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="compoundError"> The compound error. </param>
        public override void Parse( string compoundError )
        {
            bool localTryParse() {
                _ = this.ErrorNumber;
                var ret = int.TryParse( compoundError.Trim( '0' ), out int argresult ); this.ErrorNumber = argresult; return ret; }

            if ( string.IsNullOrWhiteSpace( compoundError ) )
            {
            }
            else if ( compoundError == "000" )
            {
                this.ErrorNumber = 0;
                this.ErrorMessage = (( ErrorCodes ) Conversions.ToInteger( this.ErrorNumber )).Description();
                this.CompoundErrorMessage = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0},{1}", this.ErrorNumber, this.ErrorMessage );
            }
            else if ( localTryParse() )
            {
                if ( this.ErrorNumber == 0 )
                {
                    this.ErrorMessage = (( ErrorCodes ) Conversions.ToInteger( this.ErrorNumber )).Description();
                    this.CompoundErrorMessage = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0},{1}", this.ErrorNumber, this.ErrorMessage );
                }
                else if ( Enum.IsDefined( typeof( ErrorCodes ), this.ErrorNumber ) )
                {
                    this.ErrorMessage = (( ErrorCodes ) Conversions.ToInteger( this.ErrorNumber )).Description();
                    this.CompoundErrorMessage = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0},{1}", this.ErrorNumber, this.ErrorMessage );
                }
            }
        }
    }
}
