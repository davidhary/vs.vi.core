using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.T1700
{

    /// <summary> Defines a Measure Subsystem for a Tegam 1750 Resistance Measuring System. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-07 </para>
    /// </remarks>
    public class MeasureSubsystem : MeasureResistanceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MeasureSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ohm );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm;
            this._SupportedCommands = new string[] { "D111x", "M63x", "R1x", "T0x", "U0x", "U1x", "U2x", "Y3x" };
            this.RandomResistance = new Random( ( int ) (DateTimeOffset.Now.Ticks % int.MaxValue) );
            this.MaximumDifference = 0.01d;
            this.InitialDelay = TimeSpan.FromMilliseconds( 150d );
            this.MeasurementDelay = TimeSpan.FromMilliseconds( 150d );
            this.MaximumTrialsCount = 5;
            this.ResistanceRangeCurrents.Clear();
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0m, 0m, "Auto Range" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0.002m, 1m, $"2 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0.02m, 1m, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0.02m, 0.1m, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0.2m, 1m, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0.2m, 0.1m, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2m, 0.1m, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2m, 0.01m, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20m, 0.01m, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20m, 0.001m, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 200m, 0.01m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 200m, 0.001m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 200m, 0.0001m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2000m, 0.001m, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2000m, 0.0001m, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20000m, 0.0001m, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20000m, 0.00001m, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 200000m, 0.00001m, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2000000m, 0.000001m, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20000000m, 0.0000001m, $"20 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 nA" ) );
            this.ResistanceRangeCurrentDictionary = new Dictionary<int, ResistanceRangeCurrent>() { { ( int ) ResistanceRangeMode.R0, new ResistanceRangeCurrent( 0m, 0m, "Auto Range" ) }, { ( int ) ResistanceRangeMode.R1, new ResistanceRangeCurrent( 0.002m, 1m, $"2 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A" ) }, { ( int ) ResistanceRangeMode.R2, new ResistanceRangeCurrent( 0.02m, 1m, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A" ) }, { ( int ) ResistanceRangeMode.R3, new ResistanceRangeCurrent( 0.02m, 0.1m, $"20 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA" ) }, { ( int ) ResistanceRangeMode.R4, new ResistanceRangeCurrent( 0.2m, 1m, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 A" ) }, { ( int ) ResistanceRangeMode.R5, new ResistanceRangeCurrent( 0.2m, 0.1m, $"200 m{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA" ) }, { ( int ) ResistanceRangeMode.R6, new ResistanceRangeCurrent( 2m, 0.1m, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 mA" ) }, { ( int ) ResistanceRangeMode.R7, new ResistanceRangeCurrent( 2m, 0.01m, $"2 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA" ) }, { ( int ) ResistanceRangeMode.R8, new ResistanceRangeCurrent( 20m, 0.01m, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA" ) }, { ( int ) ResistanceRangeMode.R9, new ResistanceRangeCurrent( 20m, 0.001m, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) }, { ( int ) ResistanceRangeMode.R10, new ResistanceRangeCurrent( 200m, 0.01m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 mA" ) }, { ( int ) ResistanceRangeMode.R11, new ResistanceRangeCurrent( 200m, 0.001m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) }, { ( int ) ResistanceRangeMode.R12, new ResistanceRangeCurrent( 200m, 0.0001m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) }, { ( int ) ResistanceRangeMode.R13, new ResistanceRangeCurrent( 2000m, 0.001m, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) }, { ( int ) ResistanceRangeMode.R14, new ResistanceRangeCurrent( 2000m, 0.0001m, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) }, { ( int ) ResistanceRangeMode.R15, new ResistanceRangeCurrent( 20000m, 0.0001m, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) }, { ( int ) ResistanceRangeMode.R16, new ResistanceRangeCurrent( 20000m, 0.00001m, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A" ) }, { ( int ) ResistanceRangeMode.R17, new ResistanceRangeCurrent( 200000m, 0.00001m, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A" ) }, { ( int ) ResistanceRangeMode.R18, new ResistanceRangeCurrent( 2000000m, 0.000001m, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 {Arebis.StandardUnits.UnitSymbols.MU}A" ) }, { ( int ) ResistanceRangeMode.R19, new ResistanceRangeCurrent( 20000000m, 0.0000001m, $"20 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 nA" ) } };
            this.ResistanceRangeReadWrites = new Pith.EnumReadWriteCollection() { { ( long ) ResistanceRangeMode.R0, "R00", "R0", ResistanceRangeMode.R0.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R1, "R01", "R1", ResistanceRangeMode.R1.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R2, "R02", "R2", ResistanceRangeMode.R2.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R3, "R03", "R3", ResistanceRangeMode.R3.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R4, "R04", "R4", ResistanceRangeMode.R4.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R5, "R05", "R5", ResistanceRangeMode.R5.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R6, "R06", "R6", ResistanceRangeMode.R6.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R7, "R07", "R7", ResistanceRangeMode.R7.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R8, "R08", "R8", ResistanceRangeMode.R8.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R9, "R09", "R9", ResistanceRangeMode.R9.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R11, "R11", "R11", ResistanceRangeMode.R11.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R12, "R12", "R12", ResistanceRangeMode.R12.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R13, "R13", "R13", ResistanceRangeMode.R13.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) }, { ( long ) ResistanceRangeMode.R14, "R14", "R14", ResistanceRangeMode.R14.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ).Replace( "u", Arebis.StandardUnits.UnitSymbols.MU ) }, { ( long ) ResistanceRangeMode.R15, "R15", "R15", ResistanceRangeMode.R15.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ).Replace( "u", Arebis.StandardUnits.UnitSymbols.MU ) }, { ( long ) ResistanceRangeMode.R16, "R16", "R16", ResistanceRangeMode.R16.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ).Replace( "u", Arebis.StandardUnits.UnitSymbols.MU ) }, { ( long ) ResistanceRangeMode.R17, "R17", "R17", ResistanceRangeMode.R17.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ).Replace( "u", Arebis.StandardUnits.UnitSymbols.MU ) }, { ( long ) ResistanceRangeMode.R18, "R18", "R18", ResistanceRangeMode.R18.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ).Replace( "u", Arebis.StandardUnits.UnitSymbols.MU ) }, { ( long ) ResistanceRangeMode.R19, "R19", "R19", ResistanceRangeMode.R19.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ).Replace( "u", Arebis.StandardUnits.UnitSymbols.MU ) }, { ( long ) ResistanceRangeMode.R10, "R10", "R10", ResistanceRangeMode.R10.DescriptionUntil().Replace( " ohm", Arebis.StandardUnits.UnitSymbols.Omega ) } };
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets subsystem values to their known execution clear state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.LastReading = string.Empty;
            this.OverRangeOpenWire = new bool?();
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.LastReading = string.Empty;
            this.OverRangeOpenWire = new bool?();
        }

        /// <summary> Sets the subsystem values to their known execution reset state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            base.DefineKnownResetState();
            this.RangeMode = ResistanceRangeMode.R0;
            this.TriggerMode = T1700.TriggerMode.T2;
            this.MaximumDifference = 0.01d;
            this.InitialDelay = TimeSpan.FromMilliseconds( 150d );
            this.MeasurementDelay = TimeSpan.FromMilliseconds( 150d );
            this.MaximumTrialsCount = 5;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region "  INIT, READ, FETCH "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the fetch command. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The fetch command. </value>
        [Obsolete( "Not supported" )]
        protected override string FetchCommand { get; set; } = string.Empty;

        /// <summary> Gets the read command. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The read command. </value>
        [Obsolete( "Not supported" )]
        protected override string ReadCommand { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " FUNCTION MODE "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        [Obsolete( "Not supported" )]
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        [Obsolete( "Not supported" )]
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #endregion

        #region " WRITE "

        /// <summary> The supported commands. </summary>
        private readonly string[] _SupportedCommands;

        /// <summary> Gets the supported commands. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A String. </returns>
        public string[] SupportedCommands()
        {
            return this._SupportedCommands;
        }

        #endregion

        #region " FETCH "

        /// <summary> Fetches the data. </summary>
        /// <remarks> the Tegam 1700 meter does not require issuing a read command. </remarks>
        /// <returns> A Double? </returns>
        public override double? Fetch()
        {
            return this.Read();
        }

        /// <summary> Fetches the data. </summary>
        /// <remarks> the Tegam 1700 meter does not require issuing a read command. </remarks>
        /// <returns> A Double? </returns>
        public override double? Read()
        {
            string value = this.Session.ReadLineTrimEnd();
            if ( !string.IsNullOrWhiteSpace( this.LastReading ) )
            {
                // the emulator will set the last reading. 
                this.PrimaryReading.Value = this.ParsePrimaryReading( value );
            }

            return this.PrimaryReadingValue;
        }

        #endregion

        #region " RANDOM READING "

        /// <summary> Gets or sets the random resistance. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The random reading. </value>
        private Random RandomResistance { get; set; }

        /// <summary> Creates a new reading. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="meanValue"> The mean value. </param>
        /// <param name="range">     The range. </param>
        /// <returns> null if it fails, else a list of. </returns>
        public string NewReading( double meanValue, double range )
        {
            double value = meanValue * (1d + range * (this.RandomResistance.NextDouble() - 0.5d));
            return string.Format( System.Globalization.CultureInfo.InvariantCulture, "{0} Ohm", value );
        }

        #endregion

        #region " PARSE READING "

        /// <summary> Parse scale. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> A Double. </returns>
        public static double ParseScale( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
                throw new ArgumentNullException( nameof( value ) );
            value = value.Trim();
            switch ( true )
            {
                case object _ when value.StartsWith( "mOhm", StringComparison.Ordinal ):
                    {
                        return 0.001d;
                    }

                case object _ when value.StartsWith( "Ohm", StringComparison.Ordinal ):
                    {
                        return 1d;
                    }

                case object _ when value.StartsWith( "KOhm", StringComparison.Ordinal ):
                    {
                        return 1000d;
                    }

                case object _ when value.StartsWith( "MOhm", StringComparison.Ordinal ):
                    {
                        return 1000000d;
                    }

                default:
                    {
                        return 0d;
                    }
            }
        }

        /// <summary> Parses a new set of reading elements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
        /// <returns> A Tuple(Of Boolean?, Double?) </returns>
        private static Tuple<bool?, double?> ParseReadings( string reading )
        {
            var overRanges = new string[] { "2.9999", "29.999", "299.99", "2999.9", "29999" };
            double? res;
            bool? overRange;
            if ( string.IsNullOrWhiteSpace( reading ) )
            {
                overRange = new bool?();
                res = new double?();
            }
            else
            {
                reading = reading.Trim();
                var readings = reading.Split( ' ' );
                if ( readings.Count() >= 2 )
                {
                    if ( overRanges.Contains( readings[0] ) )
                    {
                        overRange = true;
                        res = new double?();
                    }
                    else
                    {
                        double r = double.Parse( readings[0] );
                        string units = readings[readings.Count() - 1].Trim();
                        double s = ParseScale( units );
                        if ( s > 0d )
                        {
                            overRange = false;
                            res = r * s;
                        }
                        else
                        {
                            overRange = new bool?();
                            res = new double?();
                        }
                    }
                }
                else
                {
                    overRange = new bool?();
                    res = new double?();
                }
            }

            return new Tuple<bool?, double?>( overRange, res );
        }

        /// <summary> Parses the reading into the data elements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
        /// <returns> A Double? </returns>
        public override double? ParsePrimaryReading( string reading )
        {
            var result = ParseReadings( reading );
            this.OverRangeOpenWire = result.Item1;
            Core.ApplianceBase.DoEvents();
            return result.Item2;
        }

        #endregion

        #region " OVER RANGE OPEN WIRE "

        /// <summary> The over range open wire timeout. </summary>
        private TimeSpan _OverRangeOpenWireTimeout;

        /// <summary>
        /// Gets or sets the over range open wire timeout. Measurements is allowed to repeat until the
        /// timeout expires.
        /// </summary>
        /// <value> The over range open wire timeout. </value>
        public TimeSpan OverRangeOpenWireTimeout
        {
            get => this._OverRangeOpenWireTimeout;

            set {
                if ( !value.Equals( this.OverRangeOpenWireTimeout ) )
                {
                    this._OverRangeOpenWireTimeout = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> The over range open wire stopwatch. </summary>
        private Stopwatch _OverRangeOpenWireStopwatch;

        /// <summary> Resets the over range open wire timeout. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ResetOverRangeOpenWireTimeout()
        {
            this._OverRangeOpenWireStopwatch = Stopwatch.StartNew();
        }

        /// <summary>
        /// Checks if time elapsed beyond the
        /// <see cref="OverRangeOpenWireTimeout">over range open wire timeout</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// <c>True</c> if time elapsed beyond the
        /// <see cref="OverRangeOpenWireTimeout">over range open wire timeout</see>; Otherwise,
        /// <c>False</c>.
        /// </returns>
        public bool IsOverRangeOpenWireTimeoutExpired()
        {
            return this._OverRangeOpenWireStopwatch.Elapsed > this.OverRangeOpenWireTimeout;
        }

        /// <summary> Gets or sets the over range open wire. </summary>
        /// <value> The over range open wire. </value>
        private bool? _OverRangeOpenWire;

        /// <summary> Gets or sets the over range open wire. </summary>
        /// <value> The over range open wire. </value>
        public bool? OverRangeOpenWire
        {
            get => this._OverRangeOpenWire;

            set {
                this._OverRangeOpenWire = value;
                this.NotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " RANGE MODE PARSERS "

        /// <summary> Try convert. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="current">   The current. </param>
        /// <param name="range">     The range. </param>
        /// <param name="rangeMode"> [in,out] The range mode. </param>
        /// <returns> <c>True</c> if converted; Otherwise, <c>False</c>. </returns>
        public static bool TryConvert( string current, string range, ref ResistanceRangeMode rangeMode )
        {
            rangeMode = ResistanceRangeMode.R0;
            foreach ( ResistanceRangeMode e in Enum.GetValues( typeof( ResistanceRangeMode ) ) )
            {
                string c = string.Empty;
                string r = string.Empty;
                if ( TryParse( e, ref c, ref r ) )
                {
                    if ( string.Equals( c, current, StringComparison.OrdinalIgnoreCase ) && string.Equals( r, range, StringComparison.OrdinalIgnoreCase ) )
                    {
                        rangeMode = e;
                        break;
                    }
                }
                else
                {
                }
            }

            return ResistanceRangeMode.R0 != rangeMode;
        }

        /// <summary>
        /// Find range match. The matched range is the first range where both current and range are
        /// greater or equal to the specified values. If a current match is not found, a range is
        /// selected based on the range value along.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="current">   The current. </param>
        /// <param name="range">     The range. </param>
        /// <param name="rangeMode"> [in,out] The range mode. </param>
        /// <returns> <c>True</c> if converted; Otherwise, <c>False</c>. </returns>
        public static bool TryMatch( double current, double range, ref ResistanceRangeMode rangeMode )
        {
            rangeMode = ResistanceRangeMode.R0;
            foreach ( ResistanceRangeMode e in Enum.GetValues( typeof( ResistanceRangeMode ) ) )
            {
                var c = default( double );
                var r = default( double );
                if ( TryParse( e, ref c, ref r ) )
                {
                    if ( current >= c && range <= r )
                    {
                        rangeMode = e;
                        break;
                    }
                    else if ( range <= r )
                    {
                        // if has the last matched range so we use the smallest current; set it in case current does not match.
                        rangeMode = e;
                    }
                }
                else
                {
                }
            }

            return ResistanceRangeMode.R0 != rangeMode;
        }

        /// <summary> Try convert. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="current">   The current. </param>
        /// <param name="range">     The range. </param>
        /// <param name="rangeMode"> [in,out] The range mode. </param>
        /// <returns> <c>True</c> if converted; Otherwise, <c>False</c>. </returns>
        public static bool TryConvert( double current, double range, ref ResistanceRangeMode rangeMode )
        {
            rangeMode = ResistanceRangeMode.R0;
            foreach ( ResistanceRangeMode e in Enum.GetValues( typeof( ResistanceRangeMode ) ) )
            {
                var c = default( double );
                var r = default( double );
                if ( TryParse( e, ref c, ref r ) )
                {
                    if ( Math.Abs( c - current ) < 0.0001d * current && Math.Abs( r - range ) < 0.0001d * range )
                    {
                        rangeMode = e;
                        break;
                    }
                }
                else
                {
                }
            }

            return ResistanceRangeMode.R0 != rangeMode;
        }

        /// <summary> Tries parsing the range mode from the range description. </summary>
        /// <remarks>
        /// Parses the description, such as <para>
        /// <c>2m ohm range @ 1 Amp Test Current (R01)</c></para><para>
        /// To current (e.g,, 1 Amp) and resistance (2m ohm) values.</para>
        /// </remarks>
        /// <param name="rangeMode"> The range mode. </param>
        /// <param name="current">   [in,out] The current. </param>
        /// <param name="range">     [in,out] The range. </param>
        /// <returns> <c>True</c> if parsed; Otherwise, <c>False</c>. </returns>
        public static bool TryParse( ResistanceRangeMode rangeMode, ref string current, ref string range )
        {
            var elements = rangeMode.Description().Split( ' ' );
            if ( elements.Count() == 9 && elements.Contains( "range" ) && elements.Contains( "Test" ) )
            {
                var builder = new System.Text.StringBuilder( elements[0] );
                _ = builder.Append( " " );
                _ = builder.Append( elements[1] );
                range = builder.ToString();
                builder = new System.Text.StringBuilder( elements[4] );
                _ = builder.Append( " " );
                _ = builder.Append( elements[5] );
                current = builder.ToString();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary> Tries parsing the range mode from the range description. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="rangeMode"> The range mode. </param>
        /// <param name="current">   [in,out] The current. </param>
        /// <param name="range">     [in,out] The range. </param>
        /// <returns> <c>True</c> if parsed; Otherwise, <c>False</c>. </returns>
        public static bool TryParse( ResistanceRangeMode rangeMode, ref double current, ref double range )
        {
            string c = string.Empty;
            string r = string.Empty;
            if ( TryParse( rangeMode, ref c, ref r ) )
            {
                var cc = c.Split( ' ' );
                if ( cc.Count() == 2 )
                {
                    if ( double.TryParse( cc[0], out current ) )
                    {
                        switch ( cc[1] ?? "" )
                        {
                            case "A":
                                {
                                    current *= 1d;
                                    break;
                                }

                            case "mA":
                                {
                                    current *= 0.001d;
                                    break;
                                }

                            case "nA":
                                {
                                    current *= 0.000000001d;
                                    break;
                                }

                            case "uA":
                                {
                                    current *= 0.000001d;
                                    break;
                                }

                            default:
                                {
                                    return false;
                                }
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

                var rr = r.Split( ' ' );
                if ( rr.Count() == 2 )
                {
                    string value = rr[0];
                    string units = value.Substring( value.Length - 1, 1 );
                    double scale = 0d;
                    switch ( units ?? "" )
                    {
                        case "2":
                            {
                                scale = 1d;
                                break;
                            }

                        case "0":
                            {
                                scale = 1d;
                                break;
                            }

                        case "m":
                            {
                                scale = 0.001d;
                                value = value.Substring( 0, value.Length - 1 );
                                break;
                            }

                        case "K":
                        case "k":
                            {
                                scale = 1000d;
                                value = value.Substring( 0, value.Length - 1 );
                                break;
                            }

                        case "M":
                            {
                                scale = 1000000d;
                                value = value.Substring( 0, value.Length - 1 );
                                break;
                            }
                    }

                    if ( double.TryParse( value, out range ) )
                    {
                        range *= scale;
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }

            }
            else
            {
                return false;
            }
        }

        #endregion

        #region " RANGE MODE "

        /// <summary> Gets or sets a dictionary of resistance range currents. </summary>
        /// <value> A dictionary of resistance range currents. </value>
        public IDictionary<int, ResistanceRangeCurrent> ResistanceRangeCurrentDictionary { get; private set; }

        /// <summary> Gets or sets the resistance range read writes. </summary>
        /// <value> The resistance range read writes. </value>
        public Pith.EnumReadWriteCollection ResistanceRangeReadWrites { get; private set; }

        /// <summary> The range settling time in milli-seconds. </summary>
        public const int RangeSettlingTimeMilliseconds = 250;

        /// <summary> The range mode. </summary>
        private ResistanceRangeMode? _RangeMode;

        /// <summary> Gets or sets the cached source RangeMode. </summary>
        /// <value> The <see cref="RangeMode">Range Mode</see> or none if not set or unknown. </value>
        public ResistanceRangeMode? RangeMode
        {
            get => this._RangeMode;

            protected set {
                if ( !Nullable.Equals( this.RangeMode, value ) )
                {
                    this._RangeMode = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> Writes and reads back the Range Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The  Range Mode. </param>
        /// <returns> The <see cref="RangeMode">Range Mode</see> or none if unknown. </returns>
        public ResistanceRangeMode? ApplyRangeMode( ResistanceRangeMode value )
        {
            _ = this.WriteRangeMode( value );
            return this.QueryRangeMode();
        }

        /// <summary> Queries the Range Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The <see cref="RangeMode">Range Mode</see> or none if unknown. </returns>
        public ResistanceRangeMode? QueryRangeMode()
        {
            string mode = this.RangeMode.ToString();
            this.Session.MakeEmulatedReplyIfEmpty( mode );
            this.RangeMode = this.Session.Query( this.RangeMode.GetValueOrDefault( 0 ), this.ResistanceRangeReadWrites, "U0x" );
            return this.RangeMode;
        }

        /// <summary> Writes the Range Mode without reading back the value from the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The range mode. </param>
        /// <returns> The <see cref="RangeMode">Range Mode</see> or none if unknown. </returns>
        public ResistanceRangeMode? WriteRangeMode( ResistanceRangeMode value )
        {
            _ = this.Session.WriteLine( value.ExtractBetween() + "x" );
            this.RangeMode = value;
            return this.RangeMode;
        }

        #endregion

        #region " TRIGGER DELAY "

        /// <summary> The TriggerDelay. </summary>
        private TimeSpan? _TriggerDelay;

        /// <summary> Gets or sets the cached Trigger Delay. </summary>
        /// <remarks>
        /// The TriggerDelay is used to TriggerDelay operation in the trigger layer. After the programmed
        /// trigger event occurs, the instrument waits until the TriggerDelay period expires before
        /// performing the Device Action.
        /// </remarks>
        /// <value> The Trigger Delay or none if not set or unknown. </value>
        public TimeSpan? TriggerDelay
        {
            get => this._TriggerDelay;

            protected set {
                if ( !Nullable.Equals( this.TriggerDelay, value ) )
                {
                    this._TriggerDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Delay. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The current TriggerDelay. </param>
        /// <returns> The Trigger Delay or none if unknown. </returns>
        public TimeSpan? ApplyTriggerDelay( TimeSpan value )
        {
            _ = this.WriteTriggerDelay( value );
            return this.QueryTriggerDelay();
        }

        /// <summary> Queries the TriggerDelay. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The TriggerDelay or none if unknown. </returns>
        public TimeSpan? QueryTriggerDelay()
        {
            string reading = (( int ) Math.Round( this.TriggerDelay.GetValueOrDefault( TimeSpan.FromMilliseconds( 111d ) ).TotalMilliseconds )).ToString();
            this.Session.MakeEmulatedReplyIfEmpty( reading );
            reading = this.Session.QueryTrimEnd( "U0x" );
            if ( !string.IsNullOrWhiteSpace( reading ) )
            {
                // 012345678901234567890123
                // C0D111F0M63P0R05S0T0B0Y0
                reading = reading.Substring( 2, 3 );
            }

            this.TriggerDelay = int.TryParse( reading, out int value ) ? TimeSpan.FromMilliseconds( value ) : new TimeSpan?();
            return this.TriggerDelay;
        }

        /// <summary> Writes the Trigger Delay without reading back the value from the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The current TriggerDelay. </param>
        /// <returns> The Trigger Delay or none if unknown. </returns>
        public TimeSpan? WriteTriggerDelay( TimeSpan value )
        {
            _ = this.Session.WriteLine( "D{0:000}x", ( object ) value.TotalMilliseconds );
            this.TriggerDelay = value;
            return this.TriggerDelay;
        }

        #endregion

        #region " TRIGGER MODE "

        /// <summary> The trigger mode. </summary>
        private TriggerMode? _TriggerMode;

        /// <summary> Gets or sets the cached source TriggerMode. </summary>
        /// <value> The <see cref="TriggerMode">Trigger Mode</see> or none if not set or unknown. </value>
        public TriggerMode? TriggerMode
        {
            get => this._TriggerMode;

            protected set {
                if ( !Nullable.Equals( this.TriggerMode, value ) )
                {
                    this._TriggerMode = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> Writes and reads back the Trigger Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The  Trigger Mode. </param>
        /// <returns> The <see cref="TriggerMode">Trigger Mode</see> or none if unknown. </returns>
        public TriggerMode? ApplyTriggerMode( TriggerMode value )
        {
            _ = this.WriteTriggerMode( value );
            return this.QueryTriggerMode();
        }

        /// <summary> Queries the Trigger Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The <see cref="TriggerMode">Trigger Mode</see> or none if unknown. </returns>
        public TriggerMode? QueryTriggerMode()
        {
            string mode = this.TriggerMode.ToString();
            this.Session.MakeEmulatedReplyIfEmpty( mode );
            mode = this.Session.QueryTrimEnd( "U0x" );
            if ( string.IsNullOrWhiteSpace( mode ) )
            {
                string message = "Failed fetching Trigger Mode";
                Debug.Assert( !Debugger.IsAttached, message );
                this.TriggerMode = T1700.TriggerMode.T2;
            }
            else
            {
                // 0         1         2
                // 012345678901234567890123
                // C0D111F0M63P0R05S0T0B0Y0
                mode = mode.Substring( 18, 2 );
                this.TriggerMode = ( TriggerMode ) Conversions.ToInteger( Enum.Parse( typeof( TriggerMode ), mode ) );
            }

            return this.TriggerMode;
        }

        /// <summary> Writes the Trigger Mode without reading back the value from the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The trigger mode. </param>
        /// <returns> The <see cref="TriggerMode">Trigger Mode</see> or none if unknown. </returns>
        public TriggerMode? WriteTriggerMode( TriggerMode value )
        {
            _ = this.Session.WriteLine( value.ExtractBetween() + "x" );
            this.TriggerMode = value;
            return this.TriggerMode;
        }

        #endregion

        #region " MEASURE SEQUENCE "

        /// <summary> The initial delay. </summary>
        private TimeSpan _InitialDelay;

        /// <summary> Gets or sets the initial delay. </summary>
        /// <value> The initial delay. </value>
        public TimeSpan InitialDelay
        {
            get => this._InitialDelay;

            set {
                if ( !value.Equals( this.InitialDelay ) )
                {
                    this._InitialDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The measurement delay. </summary>
        private TimeSpan _MeasurementDelay;

        /// <summary> Gets or sets the delay between successive measurements. </summary>
        /// <value> The Measurement delay. </value>
        public TimeSpan MeasurementDelay
        {
            get => this._MeasurementDelay;

            set {
                if ( !value.Equals( this.MeasurementDelay ) )
                {
                    this._MeasurementDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Number of maximum trials. </summary>
        private int _MaximumTrialsCount;

        /// <summary> Gets or sets the maximum number trials. </summary>
        /// <value> The number of Maximum measurements. </value>
        public int MaximumTrialsCount
        {
            get => this._MaximumTrialsCount;

            set {
                if ( !value.Equals( this.MaximumTrialsCount ) )
                {
                    this._MaximumTrialsCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The maximum difference. </summary>
        private double _MaximumDifference;

        /// <summary>
        /// Gets or sets the maximum allowed difference between successive measurements.
        /// </summary>
        /// <value> The maximum difference. </value>
        public double MaximumDifference
        {
            get => this._MaximumDifference;

            set {
                if ( !value.Equals( this._MaximumDifference ) )
                {
                    this._MaximumDifference = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Are measurements done. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="values">     The values. </param>
        /// <param name="finalValue"> [in,out] The final value. </param>
        /// <returns> <c>True</c> if measurement accepted or maximum count is done. </returns>
        private bool AreMeasurementsDone( IList<double?> values, ref double? finalValue )
        {
            if ( values is null )
            {
                return false;
            }
            else if ( values.Count >= 2 && values[values.Count - 1].HasValue && values[values.Count - 2].HasValue )
            {
                if ( Math.Abs( values[values.Count - 1].Value - values[values.Count - 2].Value ) <= this.MaximumDifference )
                {
                    finalValue = values[values.Count - 1];
                    return true;
                }
                else if ( values.Count >= this.MaximumTrialsCount )
                {
                    finalValue = new double?();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else if ( values.Count >= this.MaximumTrialsCount )
            {
                finalValue = new double?();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary> Gets or sets the emulated resistance average. </summary>
        /// <value> The emulated resistance average. </value>
        public double EmulatedResistanceAverage { get; set; } = 1d;

        /// <summary> Gets or sets the emulated resistance range. </summary>
        /// <value> The emulated resistance range. </value>
        public double EmulatedResistanceRange { get; set; } = 0.1d;

        /// <summary> Measures a resistance using the measurement sequence. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void Measure()
        {
            var measurements = new List<double?>();
            var finalValue = new double?();
            var delayTime = this.InitialDelay;
            double? resistance;
            do
            {
                this.Session.EmulatedStatusByte = Pith.ServiceRequests.MeasurementEvent;
                _ = this.Session.ReadStatusRegister(); // was Me.StatusSubsystem.ReadRegisters(), which seems unnecessary
                Core.ApplianceBase.DoEvents();
                if ( !this.StatusSubsystem.ErrorAvailable )
                {
                    _ = Core.ApplianceBase.DoEventsWaitElapsed( TimeSpan.FromMilliseconds( 10d ), delayTime );
                    delayTime = this.MeasurementDelay;
                    this.Session.MakeEmulatedReply( this.NewReading( this.EmulatedResistanceAverage, this.EmulatedResistanceRange ) );
                    this.LastReading = this.Session.ReadLineTrimEnd();
                    Core.ApplianceBase.DoEvents();
                    if ( string.IsNullOrWhiteSpace( this.LastReading ) )
                    {
                        this.OverRangeOpenWire = false;
                        resistance = new double?();
                        Core.ApplianceBase.DoEvents();
                    }
                    else
                    {
                        // the emulator will set the last reading. 
                        resistance = this.ParsePrimaryReading( this.LastReading );
                        Core.ApplianceBase.DoEvents();
                    }

                    measurements.Add( resistance );
                }
            }
            while ( !this.StatusSubsystem.ErrorAvailable && !this.AreMeasurementsDone( measurements, ref finalValue ) );
            this.PrimaryReading.Value = finalValue;
            Core.ApplianceBase.DoEvents();
        }

        #endregion

    }

    /// <summary> Values that represent the resistance ranges with current specifications. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum ResistanceRangeMode
    {

        /// <summary> An enum constant representing the auto range option. </summary>
        [System.ComponentModel.Description( "Auto Range (R0)" )]
        R0 = 0,

        /// <summary> An enum constant representing the r 1 option. </summary>
        [System.ComponentModel.Description( "2 m ohm range @ 1 A Test Current (R1)" )]
        R1,

        /// <summary> An enum constant representing the r 2 option. </summary>
        [System.ComponentModel.Description( "20 m ohm range @ 1 A Test Current (R2)" )]
        R2,

        /// <summary> An enum constant representing the r 3 option. </summary>
        [System.ComponentModel.Description( "20 m ohm range @ 100 mA Test Current (R3)" )]
        R3,

        /// <summary> An enum constant representing the r 4 option. </summary>
        [System.ComponentModel.Description( "200 m ohm range @ 1 A Test Current (R4)" )]
        R4,

        /// <summary> An enum constant representing the r 5 option. </summary>
        [System.ComponentModel.Description( "200 m ohm range @ 100 mA Test Current (R5)" )]
        R5,

        /// <summary> An enum constant representing the r 6 option. </summary>
        [System.ComponentModel.Description( "2  ohm range @ 100 mA Test Current (R6)" )]
        R6,

        /// <summary> An enum constant representing the r 7 option. </summary>
        [System.ComponentModel.Description( "2  ohm range @ 10 mA Test Current (R7)" )]
        R7,

        /// <summary> An enum constant representing the r 8 option. </summary>
        [System.ComponentModel.Description( "20  ohm range @ 10 mA Test Current (R8)" )]
        R8,

        /// <summary> An enum constant representing the r 9 option. </summary>
        [System.ComponentModel.Description( "20  ohm range @ 1 mA Test Current (R9)" )]
        R9,

        /// <summary> An enum constant representing the 10 option. </summary>
        [System.ComponentModel.Description( "200  ohm range @ 10 mA Test Current (R10)" )]
        R10,

        /// <summary> An enum constant representing the 11 option. </summary>
        [System.ComponentModel.Description( "200  ohm range @ 1 mA Test Current (R11)" )]
        R11,

        /// <summary> An enum constant representing the 12 option. </summary>
        [System.ComponentModel.Description( "200  ohm range @ 100 uA Test Current (R12)" )]
        R12,

        /// <summary> An enum constant representing the 13 option. </summary>
        [System.ComponentModel.Description( "2 K ohm range @ 1 mA Test Current (R13)" )]
        R13,

        /// <summary> An enum constant representing the 14 option. </summary>
        [System.ComponentModel.Description( "2 K ohm range @ 100 uA Test Current (R14)" )]
        R14,

        /// <summary> An enum constant representing the 15 option. </summary>
        [System.ComponentModel.Description( "20 K ohm range @ 100 uA Test Current (R15)" )]
        R15,

        /// <summary> An enum constant representing the 16 option. </summary>
        [System.ComponentModel.Description( "20 K ohm range @ 10 uA Test Current (R16)" )]
        R16,

        /// <summary> An enum constant representing the 17 option. </summary>
        [System.ComponentModel.Description( "200 K ohm range @ 10 uA Test Current (R17)" )]
        R17,

        /// <summary> An enum constant representing the 18 option. </summary>
        [System.ComponentModel.Description( "2 M ohm range @ 1 uA Test Current (R18)" )]
        R18,

        /// <summary> An enum constant representing the 19 option. </summary>
        [System.ComponentModel.Description( "20 M ohm range @ 100 nA Test Current (R19)" )]
        R19
    }
}
