﻿
namespace isr.VI.T1700.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.T1700;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI Tegam T1700 Meter Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Tegam T1700 Meter Virtual Instrument Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.T1700.Meter";
    }
}