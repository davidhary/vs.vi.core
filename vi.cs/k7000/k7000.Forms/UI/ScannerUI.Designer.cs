﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K7000.Forms
{
    [DesignerGenerated()]
    public partial class ScannerUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ScannerView = new ScannerView();
            SuspendLayout();
            // 
            // _ScannerView
            // 
            _ScannerView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _ScannerView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _ScannerView.Dock = System.Windows.Forms.DockStyle.Fill;
            _ScannerView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ScannerView.Location = new System.Drawing.Point(1, 1);
            _ScannerView.Name = "_ScannerView";
            _ScannerView.Padding = new System.Windows.Forms.Padding(1);
            _ScannerView.Size = new System.Drawing.Size(430, 342);
            _ScannerView.TabIndex = 0;
            // 
            // ScannerUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_ScannerView);
            Name = "ScannerUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(432, 344);
            ResumeLayout(false);
        }

        private ScannerView _ScannerView;
    }
}