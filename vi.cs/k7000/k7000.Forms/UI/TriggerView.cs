namespace isr.VI.K7000.Forms
{

    /// <summary>
    /// A Keithley 7000 edition of the basic <see cref="Facade.TriggerView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11  <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class TriggerView : Facade.TriggerView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static new TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K7000Device K7000Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K7000Device value )
        {
            base.AssignDevice( value );
            this.K7000Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 1 );
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 2 );
                this.BindSubsystem( ( TriggerSubsystemBase ) default, 1 );
            }
            else
            {
                this.BindSubsystem( this.K7000Device.ArmLayer1Subsystem, 1 );
                this.BindSubsystem( this.K7000Device.ArmLayer2Subsystem, 2 );
                this.BindSubsystem( this.K7000Device.TriggerSubsystem, 1 );
            }
        }

        #endregion

        #region " TRIGGER LAYER 1 VIEW "

        /// <summary> Handle the TriggerLayer1 view property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="view">         The view. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected override void HandlePropertyChanged( Facade.TriggerLayerView view, string propertyName )
        {
            if ( view is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Facade.TriggerLayerView.Count ):
                    {
                        isr.VI.K7000.Forms.Settings.Default.TriggerViewTriggerCount = view.Count;
                        break;
                    }
            }

            base.HandlePropertyChanged( view, propertyName );
        }

        #endregion

    }
}
