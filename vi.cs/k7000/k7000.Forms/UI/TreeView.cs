using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.K7000.Forms
{

    /// <summary> A Keithley 7000 Device User Interface based on the <see cref="Facade.VisaTreeView"/>. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "K7000 User Interface" )]
    [Description( "Keithley 7000 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( TreeView ) )]
    public class TreeView : Facade.VisaTreeView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public TreeView() : base()
        {
            this.Width = 540;
            this.InitializingComponents = true;
            this.ScannerUI = ScannerView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Scanner", "Scanner", this.ScannerUI );
            this.TriggerUI = TriggerUI.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Trigger", "Trigger", this.TriggerUI );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public TreeView( K7000Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary> Gets the scanner user interface. </summary>
        /// <value> The scanner user interface. </value>
        private ScannerView ScannerUI { get; set; }

        /// <summary> Gets the trigger user interface. </summary>
        /// <value> The trigger user interface. </value>
        private TriggerUI TriggerUI { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the K7000 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    this.AssignDeviceThis( null );
                    if ( this.ScannerUI is object )
                    {
                        this.ScannerUI.Dispose();
                        this.ScannerUI = null;
                    }

                    if ( this.TriggerUI is object )
                    {
                        this.TriggerUI.Dispose();
                        this.TriggerUI = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K7000Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( K7000Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = K7000.My.MySettings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( K7000Device value )
        {
            this.AssignDeviceThis( value );
        }

        #endregion

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.ScannerUI.AssignDevice( null );
                this.TriggerUI.AssignDevice( null );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            // MyBase.DisplayView.BindMeasureSubsystemControls(Me.Device.MeasureSubsystem)
            // MyBase.DisplayView.BindSubsystemViewModel(Me.Device.SourceSubsystem)
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.ScannerUI.AssignDevice( this.Device );
            this.TriggerUI.AssignDevice( this.Device );
        }

        #endregion

        #region " TALKER "

        /// <summary> Assigns talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( Core.ITraceMessageTalker talker )
        {
            this.ScannerUI.AssignTalker( talker );
            this.TriggerUI.AssignTalker( talker );
            // assigned last as this identifies all talkers.
            base.AssignTalker( talker );
        }

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
