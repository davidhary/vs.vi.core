using System.ComponentModel;

namespace isr.VI.K7000.Forms
{

    /// <summary>
    /// A Keithley 7000 edition of the basic <see cref="Facade.ScannerView"/> user interface.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public class ScannerView : Facade.ScannerView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="ScannerView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ScannerView"/>. </returns>
        public static new ScannerView Create()
        {
            ScannerView view = null;
            try
            {
                view = new ScannerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private K7000Device K7000Device { get; set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void AssignDevice( K7000Device value )
        {
            base.AssignDevice( value );
            this.K7000Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( RouteSubsystemBase ) default );
                this.BindSubsystem( ( TriggerSubsystemBase ) default );
                this.BindSubsystem1( default );
                this.BindSubsystem2( default );
            }
            else
            {
                this.BindSubsystem( this.K7000Device.RouteSubsystem );
                this.BindSubsystem( this.K7000Device.TriggerSubsystem );
                this.BindSubsystem1( this.K7000Device.ArmLayer1Subsystem );
                this.BindSubsystem2( this.K7000Device.ArmLayer2Subsystem );
            }
        }

        #endregion

    }
}
