﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K7000 VI Tests" )]
[assembly: AssemblyDescription( "K7000 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K7000.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
