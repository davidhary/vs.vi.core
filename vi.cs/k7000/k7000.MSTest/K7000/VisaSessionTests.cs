using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K7000.MSTest
{

    /// <summary> K2002 Visa Session unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k7001" )]
    public partial class VisaSessionTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " OPEN/CLOSE "

        /// <summary> (Unit Test Method) visa session should open and close. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaSessionShouldOpenAndClose()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) visa session base should open and close. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaSessionBaseShouldOpenAndClose()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, session, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionBaseShouldClose( TestInfo, session );
            }
        }

        #endregion

        /// <summary> (Unit Test Method) status bitmask should wait for. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void StatusBitmaskShouldWaitFor()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertStatusBitmaskShouldWaitFor( TestInfo, session.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) message available should wait for. </summary>
        /// <remarks>
        /// Initial Service Request Wait Complete Bitmask is 0x00<para>
        /// Initial Standard Event Enable Bitmask Is 0x00</para><para>
        /// DMM2002 status Byte 0x50 0x10 bitmask was Set</para>
        /// </remarks>
        [TestMethod()]
        public void MessageAvailableShouldWaitFor()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertMessageAvailableShouldWaitFor( TestInfo, session.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) operation completion should wait for. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OperationCompletionShouldWaitFor()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertOperationCompletionShouldWaitFor( TestInfo, session.Session, ":ROUT:OPEN ALL; *OPC" );
                VI.DeviceTests.DeviceManager.AssertServiceRequestOperationCompletionWaitFor( TestInfo, session.Session, ":ROUT:OPEN ALL; *OPC" );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) service request handling should toggle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ServiceRequestHandlingShouldToggle()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertServiceRequestHandlingShouldToggle( TestInfo, session.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }
    }
}
