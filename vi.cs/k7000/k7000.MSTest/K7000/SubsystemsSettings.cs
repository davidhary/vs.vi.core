using System;
using System.Collections.Generic;

namespace isr.VI.K7000.MSTest
{

    /// <summary> The Subsystems Test Settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode( "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0" )]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
    internal class SubsystemsSettings : VI.DeviceTests.SubsystemsSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private SubsystemsSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( $"{typeof( SubsystemsSettings )} Editor", Get() );
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static SubsystemsSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static SubsystemsSettings Get()
        {
            if ( _Instance is null )
            {
                lock ( _SyncLocker )
                    _Instance = ( SubsystemsSettings ) Synchronized( new SubsystemsSettings() );
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( _SyncLocker )
                    return _Instance is object;
            }
        }

        #endregion

        #region " INITIAL VALUES: ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the initial closed channels. </summary>
        /// <value> The initial closed channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "(@)" )]
        public override string InitialClosedChannels
        {
            get {
                return base.InitialClosedChannels;
            }

            set {
                base.InitialClosedChannels = value;
            }
        }

        /// <summary> Gets or sets the Initial scan list settings. </summary>
        /// <value> The initial scan list settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "(@1!1:1!10)" )]
        public override string InitialScanList
        {
            get {
                return base.InitialScanList;
            }

            set {
                base.InitialScanList = value;
            }
        }

        #endregion

        #region " INITIAL VALUES: TRIGGER SUBSYSTEM "

        /// <summary> Gets or sets the initial trigger source. </summary>
        /// <value> The initial trigger source. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "Immediate" )]
        public override TriggerSources InitialTriggerSource
        {
            get {
                return base.InitialTriggerSource;
            }

            set {
                base.InitialTriggerSource = value;
            }
        }

        #endregion

        #region " INITIAL VALUES: SYSTEM SUBSYSTEM "

        /// <summary> Gets or sets the number of scan cards. </summary>
        /// <value> The number of scan cards. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "2" )]
        public override int? ScanCardCount
        {
            get {
                return base.ScanCardCount;
            }

            set {
                base.ScanCardCount = value;
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the names of the candidate resources. </summary>
        /// <value> The names of the candidate resources. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "C7054|C7054" )]
        public IEnumerable<string> SlotCardNames
        {
            get {
                return AppSettingGetter( Array.Empty<string>() );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets scan list from one to ten. </summary>
        /// <value> A List of one ten scans. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "(@1!1:1!10)" )]
        public string OneTenScanList
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the state of the one ten channels. </summary>
        /// <value> The one ten channels state. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1,1,1,1,1,1,1,1,1,1" )]
        public string OneTenChannelsState
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

        #region " BUS TRIGGER TEST "

        /// <summary> Gets or sets the bus trigger test trigger source. </summary>
        /// <value> The bus trigger test trigger source. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "Bus" )]
        public TriggerSources BusTriggerTestTriggerSource
        {
            get {
                return AppSettingEnum<TriggerSources>();
            }

            set {
                AppSettingEnumSetter( value );
            }
        }

        /// <summary> Gets or sets a list of bus trigger test scans. </summary>
        /// <value> A list of bus trigger test scans. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "(@1!1:1!10)" )]
        public string BusTriggerTestScanList
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the number of bus trigger test triggers. </summary>
        /// <value> The number of bus trigger test triggers. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "10" )]
        public int BusTriggerTestTriggerCount
        {
            get {
                return AppSettingGetter( 0 );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the bus trigger test trigger delay. </summary>
        /// <value> The bus trigger test trigger delay. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "00:00:00.5" )]
        public TimeSpan BusTriggerTestTriggerDelay
        {
            get {
                return AppSettingGetter( TimeSpan.Zero );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

    }

    internal static partial class K7000Properties
    {

        /// <summary> Gets information describing the 7000 subsystems. </summary>
        /// <value> Information describing the 7000 subsystems. </value>
        internal static SubsystemsSettings SubsystemsInfo => SubsystemsSettings.Get();
    }
}
