﻿using System;

using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K7000.MSTest
{
    public partial class SubsystemsTests
    {

        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SlotCardInfoShouldMatch()
        {
            using var device = K7000Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceManager.OpenSession( TestInfo, device );
                int slotCardNumber = 0;
                foreach ( string slotCardName in SubsystemsSettings.Get().SlotCardNames )
                {
                    slotCardNumber += 1;
                    TestInfo.TraceMessage( $"Running {nameof( VI.DeviceTests.DeviceManager )}.{nameof( VI.DeviceTests.DeviceManager.AssertSlotCardInfoShouldMatch )} Slot Card: #{slotCardNumber}={slotCardName}" );
                    VI.DeviceTests.DeviceManager.AssertSlotCardInfoShouldMatch( device.RouteSubsystem, slotCardNumber, slotCardName );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                device.TriggerSubsystem.Abort();
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) scans the list should toggle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ScanListShouldToggle()
        {
            using var device = K7000Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceManager.OpenSession( TestInfo, device );
                _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromMilliseconds( 200d ) );
                string propertyName = nameof( RouteSubsystemBase.ClosedChannels ).SplitWords();
                Assert.IsNull( device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should be null" );
                propertyName = nameof( RouteSubsystemBase.ClosedChannelsState ).SplitWords();
                Assert.IsNull( device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should be null" );
                try
                {
                    _ = device.RouteSubsystem.ApplyClosedChannels( SubsystemsSettings.Get().OneTenScanList, TimeSpan.FromMilliseconds( 200d ) );
                    propertyName = nameof( RouteSubsystemBase.ClosedChannels ).SplitWords();
                    Assert.IsNotNull( device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should be closed" );
                    Assert.AreEqual( SubsystemsSettings.Get().OneTenScanList, device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should match" );
                    propertyName = nameof( RouteSubsystemBase.ClosedChannelsState ).SplitWords();
                    Assert.IsNotNull( device.RouteSubsystem.ClosedChannelsState, $"'{propertyName}' should be set" );
                    Assert.AreEqual( SubsystemsSettings.Get().OneTenChannelsState, device.RouteSubsystem.ClosedChannelsState, $"'{propertyName}' should match" );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromMilliseconds( 200d ) );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                device.TriggerSubsystem.Abort();
                DeviceManager.CloseSession( TestInfo, device );
            }
        }
    }
}