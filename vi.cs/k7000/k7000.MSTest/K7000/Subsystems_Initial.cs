using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K7000.MSTest
{

    /// <summary>   The subsystems tests. </summary>
    /// <remarks>   David, 2021-09-07. </remarks>
    public partial class SubsystemsTests
    {

        /// <summary> Opens session check status. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
        /// <param name="resourceInfo">     Information describing the resource. </param>
        /// <param name="subsystemsInfo">   Information describing the subsystems. </param>
        private static void OpenSessionCheckStatus( bool readErrorEnabled, ResourceSettings resourceInfo, SubsystemsSettings subsystemsInfo )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K7000Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertSessionInitialValuesShouldMatch( device.Session, resourceInfo, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceModelShouldMatch( device.StatusSubsystemBase, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldMatch( device.StatusSubsystemBase, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertTerminationValuesShouldMatch( device.Session, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.RouteSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.TriggerSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSessionDeviceErrorsShouldClear( device, subsystemsInfo );
                if ( readErrorEnabled )
                    VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldRead( device, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertOrphanMessagesShouldBeEmpty( device.StatusSubsystemBase );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> A test for Open Session and status. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionCheckStatusTest()
        {
            OpenSessionCheckStatus( false, K7000Properties.K7000ResourceInfo, K7000Properties.SubsystemsInfo );
        }

        /// <summary> (Unit Test Method) tests open session read device errors. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionReadDeviceErrorsTest()
        {
            OpenSessionCheckStatus( true, K7000Properties.K7000ResourceInfo, K7000Properties.SubsystemsInfo );
        }
    }
}
