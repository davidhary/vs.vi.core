﻿using System;

using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K7000.MSTest
{
    public partial class SubsystemsTests
    {

        /// <summary> Assert bus trigger scan should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertBusTriggerScanShouldPass( K7000Device device )
        {
            string propertyName = nameof( RouteSubsystemBase.ScanList ).SplitWords();
            string expectedScanList = SubsystemsSettings.Get().InitialScanList;
            string actualScanList = device.RouteSubsystem.QueryScanList();
            Assert.AreEqual( expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'" );
            device.ClearExecutionState();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.TriggerSubsystem.Abort();
            device.Session.EnableServiceRequestWaitComplete();
            _ = device.RouteSubsystem.WriteOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.Session.ReadStatusRegister();
            _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.RouteSubsystem.QueryClosedChannels();
            propertyName = nameof( RouteSubsystemBase.ScanList ).SplitWords();
            expectedScanList = SubsystemsSettings.Get().BusTriggerTestScanList;
            actualScanList = device.RouteSubsystem.ApplyScanList( expectedScanList );
            Assert.AreEqual( expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'" );
            _ = device.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer1Subsystem.ApplyArmCount( 1 );
            _ = device.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer2Subsystem.ApplyArmCount( 1 );
            _ = device.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
            // device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Bus)
            propertyName = nameof( TriggerSubsystemBase.TriggerSource ).SplitWords();
            var expectedTriggerSource = SubsystemsSettings.Get().BusTriggerTestTriggerSource;
            var actualTriggerSource = device.TriggerSubsystem.ApplyTriggerSource( expectedTriggerSource );
            Assert.IsTrue( actualTriggerSource.HasValue, $"{propertyName} should have a value" );
            Assert.AreEqual( expectedTriggerSource, actualTriggerSource.Value, $"{propertyName} should match" );
            _ = device.TriggerSubsystem.ApplyTriggerCount( 9999 ); // in place of infinite
            _ = device.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
            _ = device.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Source );

            // start the K7000 trigger model
            device.TriggerSubsystem.Initiate();
            // Note: device error -211 if query closed channels: Trigger ignored.
            for ( int i = 1, loopTo = SubsystemsSettings.Get().BusTriggerTestTriggerCount; i <= loopTo; i++ )
            {
                Core.ApplianceBase.DoEventsWait( SubsystemsSettings.Get().BusTriggerTestTriggerDelay );
                device.Session.AssertTrigger();
            }

            device.TriggerSubsystem.Abort();
            _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.RouteSubsystem.QueryClosedChannels();
        }

        /// <summary> (Unit Test Method) bus trigger scan should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void BusTriggerScanShouldPass()
        {
            using var device = K7000Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            DeviceManager.OpenSession( TestInfo, device );
            try
            {
                AssertBusTriggerScanShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                device.TriggerSubsystem.Abort();
                DeviceManager.CloseSession( TestInfo, device );
            }
        }
    }
}