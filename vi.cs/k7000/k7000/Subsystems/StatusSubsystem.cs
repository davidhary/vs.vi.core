using System;

using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.K7000
{

    /// <summary> Defines a SCPI Status Subsystem for a generic Switch. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-10, 3.0.5001. </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        ///                        session</see>. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ) )
        {
            this.VersionInfo = new VersionInfo();
            this.VersionInfoBase = this.VersionInfo;
            InitializeSession( session );
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Define operation event bitmasks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( OperationEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Arming, 1 << 6 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Idle, 1 << 10 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Setting, 1 << 1 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Triggering, 1 << 5 );
        }

        /// <summary> Defines the operation event bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineOperationEventsBitmasks()
        {
            base.DefineOperationEventsBitmasks();
            DefineBitmasks( this.OperationEventsBitmasks );
        }

        #endregion

        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based TSP session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.ClearExecutionStateCommand = Pith.Ieee488.Syntax.ClearExecutionStateCommand;
            session.ResetKnownStateCommand = Pith.Ieee488.Syntax.ResetKnownStateCommand;
            session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
            session.OperationCompletedQueryCommand = Pith.Ieee488.Syntax.OperationCompletedQueryCommand;
            session.StandardServiceEnableCommandFormat = Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat;
            session.StandardServiceEnableCompleteCommandFormat = Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat;
            session.ServiceRequestEnableCommandFormat = Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat;
            session.ServiceRequestEnableQueryCommand = Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand;
            session.StandardEventStatusQueryCommand = Pith.Ieee488.Syntax.StandardEventStatusQueryCommand;
            session.StandardEventEnableQueryCommand = Pith.Ieee488.Syntax.StandardEventEnableQueryCommand;
            session.WaitCommand = Pith.Ieee488.Syntax.WaitCommand;
        }

        #endregion

        #region " PRESET "

        /// <summary> Gets or sets the preset command. </summary>
        /// <remarks>
        /// SCPI: ":STAT:PRES".
        /// <see cref="F:isr.VI.Pith.Scpi.Syntax.ScpiSyntax.StatusPresetCommand"></see>
        /// </remarks>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = Pith.Scpi.Syntax.StatusPresetCommand;

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <remarks> K7001 does not support this commend == *CLS clears the queue. </remarks>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = Pith.Ieee488.Syntax.ClearExecutionStateCommand;

        /// <summary> Gets or sets the error queue query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = Pith.Scpi.Syntax.NextErrorQueryCommand;

        #endregion

        #region " LINE FREQUENCY "

        /// <summary> Gets or sets line frequency query command. </summary>
        /// <remarks> K7001 does not support this command. </remarks>
        /// <value> The line frequency query command. </value>
        protected override string LineFrequencyQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = Pith.Ieee488.Syntax.IdentityQueryCommand;

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.PublishVerbose( "Trying to read identity;. " );
                Core.ApplianceBase.DoEvents();
                // wait for the delay time.
                // Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.PublishVerbose( $"Setting identity to {value};. " );
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

    }
}
