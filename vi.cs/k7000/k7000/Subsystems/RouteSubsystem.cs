using System;

namespace isr.VI.K7000
{

    /// <summary> Defines a SCPI Route Subsystem for Switch SCPI devices. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class RouteSubsystem : RouteSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public RouteSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " CHANNEL "

        /// <summary> Gets or sets the closed Channel query command. </summary>
        /// <value> The closed Channel query command. </value>
        protected override string ClosedChannelQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the closed Channel command format. </summary>
        /// <value> The closed Channel command format. </value>
        protected override string ClosedChannelCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the open Channel command format. </summary>
        /// <value> The open Channel command format. </value>
        protected override string OpenChannelCommandFormat { get; set; } = string.Empty;

        #endregion

        #region " CHANNELS "

        /// <summary> Gets or sets the closed channels state query command. </summary>
        /// <value> The closed channels state query command. </value>
        protected override string ClosedChannelsStateQueryCommand { get; set; } = ":ROUT:CLOS? {0}";

        /// <summary> Gets or sets the closed channels query command. </summary>
        /// <value> The closed channels query command. </value>
        protected override string ClosedChannelsQueryCommand { get; set; } = ":ROUT:CLOS:STATE?";

        /// <summary> Gets or sets the closed channels command format. </summary>
        /// <value> The closed channels command format. </value>
        protected override string ClosedChannelsCommandFormat { get; set; } = ":ROUT:CLOS {0}";

        /// <summary> Gets or sets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        protected override string OpenChannelsCommandFormat { get; set; } = ":ROUT:OPEN {0}";

        /// <summary> Gets or sets the Open channels query command. </summary>
        /// <value> The Open channels query command. </value>
        protected override string OpenChannelsStateQueryCommand { get; set; } = ":ROUT:OPEN? {0}";

        /// <summary> Gets or sets the Open channels query command. </summary>
        /// <value> The Open channels query command. </value>
        protected override string OpenChannelsQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " CHANNEL PATTERN / MEMORY "

        /// <summary> Gets or sets the recall channel pattern command format. </summary>
        /// <value> The recall channel pattern command format. </value>
        protected override string RecallChannelPatternCommandFormat { get; set; } = ":ROUT:MEM:REC M{0}";

        /// <summary> Gets or sets the save channel pattern command format. </summary>
        /// <value> The save channel pattern command format. </value>
        protected override string SaveChannelPatternCommandFormat { get; set; } = ":ROUT:MEM:SAVE M{0}";

        /// <summary> Gets or sets the open channels command. </summary>
        /// <value> The open channels command. </value>
        protected override string OpenChannelsCommand { get; set; } = ":ROUT:OPEN ALL";

        #endregion

        #region " SCAN LIST "

        /// <summary> Gets or sets the scan list persists after toggling power. </summary>
        /// <remarks>
        /// If the scan lists persists after toggling power, the scan list initial values cannot be
        /// checked.
        /// </remarks>
        /// <value> The scan list persists. </value>
        public override bool ScanListPersists { get; set; } = true;

        /// <summary> Gets or sets the scan list command query. </summary>
        /// <value> The scan list query command. </value>
        protected override string ScanListQueryCommand { get; set; } = ":ROUT:SCAN?";

        /// <summary> Gets or sets the scan list command format. </summary>
        /// <value> The scan list command format. </value>
        protected override string ScanListCommandFormat { get; set; } = ":ROUT:SCAN {0}";

        #endregion

        #region " SLOT CARD TYPE "

        /// <summary> Gets or sets the slot card type query command format. </summary>
        /// <value> The slot card type query command format. </value>
        protected override string SlotCardTypeQueryCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:CTYPE?";

        /// <summary> Gets or sets the slot card type command format. </summary>
        /// <value> The slot card type command format. </value>
        protected override string SlotCardTypeCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:CTYPE{1}";

        #endregion

        #region " SLOT CARD SETTLING TIME "

        /// <summary> Gets or sets the slot card settling time command format. </summary>
        /// <value> The slot card settling time command format. </value>
        protected override string SlotCardSettlingTimeCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:STIME {1}";

        /// <summary> Gets or sets the slot card settling time query command format. </summary>
        /// <value> The slot card settling time query command format. </value>
        protected override string SlotCardSettlingTimeQueryCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:STIME?";

        #endregion

        #region " TERMINAL MODE "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the terminals mode query command. </summary>
        /// <value> The terminals mode command. </value>
        [Obsolete( "Not supported for 7000 Series Switches." )]
        protected override string TerminalsModeQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        [Obsolete( "Not supported for 7000 Series Switches." )]
        protected override string TerminalsModeCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

    }
}
