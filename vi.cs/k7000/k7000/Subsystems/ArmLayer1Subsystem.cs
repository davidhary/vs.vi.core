namespace isr.VI.K7000
{

    /// <summary> Defines a SCPI Arm Layer Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class ArmLayer1Subsystem : ArmLayerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ArmLayer1Subsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public ArmLayer1Subsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.SupportedArmSources = ArmSources.Bus | ArmSources.External | ArmSources.Hold | ArmSources.Immediate | ArmSources.Manual | ArmSources.TriggerLink;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " COMMANDS "

        /// <summary> Gets or sets the Immediate command. </summary>
        /// <value> The Immediate command. </value>
        protected override string ImmediateCommand { get; set; } = ":ARM:LAY1:IMM";

        #endregion

        #region " ARM COUNT "

        /// <summary> Gets or sets Arm count query command. </summary>
        /// <value> The Arm count query command. </value>
        protected override string ArmCountQueryCommand { get; set; } = ":ARM:LAY1:COUN?";

        /// <summary> Gets or sets Arm count command format. </summary>
        /// <value> The Arm count command format. </value>
        protected override string ArmCountCommandFormat { get; set; } = ":ARM:LAY1:COUN {0}";

        #endregion

        #region " DIRECTION (BYPASS) "

        /// <summary> Gets or sets the Trigger Direction query command. </summary>
        /// <value> The Trigger Direction query command. </value>
        protected override string ArmLayerBypassModeQueryCommand { get; set; } = ":ARM:LAY1:TCON:DIR?";

        /// <summary> Gets or sets the Trigger Direction command format. </summary>
        /// <value> The Trigger Direction command format. </value>
        protected override string ArmLayerBypassModeCommandFormat { get; set; } = ":ARM:LAY1:TCON:DIR {0}";

        #endregion

        #region " ARM SOURCE "

        /// <summary> Gets or sets the Arm source command format. </summary>
        /// <value> The write Arm source command format. </value>
        protected override string ArmSourceCommandFormat { get; set; } = ":ARM:LAY1:SOUR {0}";

        /// <summary> Gets or sets the Arm source query command. </summary>
        /// <value> The Arm source query command. </value>
        protected override string ArmSourceQueryCommand { get; set; } = ":ARM:LAY1:SOUR?";

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = string.Empty; // ":ARM:LAY1:DEL {0:s\.fff}"

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = string.Empty; // "s\.FFFFFFF"

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = string.Empty; // ":ARM:LAY1:DEL?"

        #endregion

        #region " INPUT LINE NUMBER "

        /// <summary> Gets or sets the Input Line Number command format. </summary>
        /// <value> The Input Line Number command format. </value>
        protected override string InputLineNumberCommandFormat { get; set; } = ":ARM:LAY1:TCON:ASYN:ILIN {0}";

        /// <summary> Gets or sets the Input Line Number query command. </summary>
        /// <value> The Input Line Number query command. </value>
        protected override string InputLineNumberQueryCommand { get; set; } = ":ARM:LAY1:TCON:ASYN:ILIN?";

        #endregion

        #region " OUTPUT LINE NUMBER "

        /// <summary> Gets or sets the Output Line Number command format. </summary>
        /// <value> The Output Line Number command format. </value>
        protected override string OutputLineNumberCommandFormat { get; set; } = ":ARM:LAY1:TCON:ASYN:OLIN {0}";

        /// <summary> Gets or sets the Output Line Number query command. </summary>
        /// <value> The Output Line Number query command. </value>
        protected override string OutputLineNumberQueryCommand { get; set; } = ":ARM:LAY1:TCON:ASYN:OLIN?";

        #endregion

        #region " TIMER TIME SPAN "

        /// <summary> Gets or sets the Timer Interval command format. </summary>
        /// <value> The query command format. </value>
        protected override string TimerIntervalCommandFormat { get; set; } = string.Empty; // ":ARM:LAY1:TIM {0:s\.FFFFFFF}"

        /// <summary>
        /// Gets or sets the Timer Interval format for converting the query to time span.
        /// </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalFormat { get; set; } = string.Empty; // "s\.FFFFFFF"

        /// <summary> Gets or sets the Timer Interval query command. </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalQueryCommand { get; set; } = string.Empty; // ":ARM:LAY1:TIM?"

        #endregion

        #endregion

    }
}
