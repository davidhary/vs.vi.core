#region " TYPES "

using System;

namespace isr.VI.K7000
{

    /// <summary> Enumerates the status byte flags of the operation event register. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags()]
    public enum OperationEvents
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the calibrating option. </summary>
        [System.ComponentModel.Description( "Calibrating" )]
        Calibrating = 1,

        /// <summary> An enum constant representing the settling option. </summary>
        [System.ComponentModel.Description( "Settling" )]
        Settling = 2,

        /// <summary> An enum constant representing the sweeping option. </summary>
        [System.ComponentModel.Description( "Sweeping" )]
        Sweeping = 8,

        /// <summary> An enum constant representing the waiting for trigger option. </summary>
        [System.ComponentModel.Description( "Waiting For Trigger" )]
        WaitingForTrigger = 32,

        /// <summary> An enum constant representing the waiting for arm option. </summary>
        [System.ComponentModel.Description( "Waiting For Arm" )]
        WaitingForArm = 64,

        /// <summary> An enum constant representing the idle option. </summary>
        [System.ComponentModel.Description( "Idle" )]
        Idle = 1024
    }

    /// <summary>
    /// Enumerates the status byte flags of the operation transition event register.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags()]
    public enum OperationTransitionEvents
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the settling option. </summary>
        [System.ComponentModel.Description( "Settling" )]
        Settling = 2,

        /// <summary> An enum constant representing the waiting for trigger option. </summary>
        [System.ComponentModel.Description( "Waiting For Trigger" )]
        WaitingForTrigger = 32,

        /// <summary> An enum constant representing the waiting for arm option. </summary>
        [System.ComponentModel.Description( "Waiting For Arm" )]
        WaitingForArm = 64,

        /// <summary> An enum constant representing the idle option. </summary>
        [System.ComponentModel.Description( "Idle " )]
        Idle = 1024
    }

    /// <summary> Enumerates the status byte flags of the questionable event register. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags()]
    public enum QuestionableEvents
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the calibration event option. </summary>
        [System.ComponentModel.Description( "Calibration Event" )]
        CalibrationEvent = 256,

        /// <summary> An enum constant representing the command warning option. </summary>
        [System.ComponentModel.Description( "Command Warning" )]
        CommandWarning = 16384
        // All = 32767
    }
}

#endregion
