namespace isr.VI.K34980
{

    /// <summary> Defines a Instrument Subsystem for a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class InstrumentSubsystem : InstrumentSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="InstrumentSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public InstrumentSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            _ = this.QueryDmmInstalled();
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " DMM INSTALLED "

        /// <summary> Gets or sets the DMM Installed query command. </summary>
        /// <value> The DMM Installed query command. </value>
        protected override string DmmInstalledQueryCommand { get; set; } = ":INST:DMM:INST?";

        #endregion

        #endregion

    }
}
