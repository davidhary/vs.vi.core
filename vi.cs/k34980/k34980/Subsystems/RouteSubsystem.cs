using System;

namespace isr.VI.K34980
{

    /// <summary> Defines a Route Subsystem for a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class RouteSubsystem : RouteSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public RouteSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "



        #endregion

        #region " COMMAND SYNTAX "

        #region " CHANNEL "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the closed Channel query command. </summary>
        /// <value> The closed Channel query command. </value>
        [Obsolete( "Not supported with 34980A" )]
        protected override string ClosedChannelQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the closed Channel command format. </summary>
        /// <value> The closed Channel command format. </value>
        [Obsolete( "Use Channels commands with 34980A" )]
        protected override string ClosedChannelCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the open Channel command format. </summary>
        /// <value> The open Channel command format. </value>
        [Obsolete( "Use Channels commands with 34980A" )]
        protected override string OpenChannelCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " CHANNELS CLOSED "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the closed channels query command. </summary>
        /// <value> The closed channels query command. </value>
        [Obsolete( "Not supported with 34980A" )]
        protected override string ClosedChannelsQueryCommand { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets or sets the closed channels command format. </summary>
        /// <value> The closed channels command format. </value>
        protected override string ClosedChannelsCommandFormat { get; set; } = ":ROUT:CLOS {0}";

        #endregion

        #region " CHANNELS OPEN "

        /// <summary> Gets or sets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        protected override string OpenChannelsCommandFormat { get; set; } = ":ROUT:OPEN {0}";

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the open channels query command. </summary>
        /// <value> The open channels query command. </value>
        [Obsolete( "Not supported with 34980A" )]
        protected override string OpenChannelsQueryCommand { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets or sets the open channels command. </summary>
        /// <value> The open channels command. </value>
        protected override string OpenChannelsCommand { get; set; } = ":ROUT:OPEN:ALL";

        #endregion

        #region " CHANNELS "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the recall channel pattern command format. </summary>
        /// <value> The recall channel pattern command format. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string RecallChannelPatternCommandFormat { get; set; } = ":ROUT:MEM:REC M{0}";

        /// <summary> Gets the save channel pattern command format. </summary>
        /// <value> The save channel pattern command format. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string SaveChannelPatternCommandFormat { get; set; } = ":ROUT:MEM:SAVE M{0}";
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " SCAN LIST "

        /// <summary> Gets or sets the scan list command query. </summary>
        /// <value> The scan list query command. </value>
        protected override string ScanListQueryCommand { get; set; } = ":ROUT:SCAN?";

        /// <summary> Gets or sets the scan list command format. </summary>
        /// <value> The scan list command format. </value>
        protected override string ScanListCommandFormat { get; set; } = ":ROUT:SCAN {0}";

        #endregion

        #region " SLOT CARD TYPE "

        /// <summary> Gets or sets the slot card type query command format. </summary>
        /// <value> The slot card type query command format. </value>
        protected override string SlotCardTypeQueryCommandFormat { get; set; } = ":SYST:CTYPE?";

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the slot card type command format. </summary>
        /// <value> The slot card type command format. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string SlotCardTypeCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:CTYPER {1}";
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " SLOT CARD SETTLING TIME "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the slot card settling time query command format. </summary>
        /// <value> The slot card settling time query command format. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string SlotCardSettlingTimeQueryCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the slot card settling time command format. </summary>
        /// <value> The slot card settling time command format. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string SlotCardSettlingTimeCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " TERMINAL MODE "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the terminals mode query command. </summary>
        /// <value> The terminals mode command. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string TerminalsModeQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        [Obsolete( "Not supported 34980A" )]
        protected override string TerminalsModeCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #endregion

    }
}
