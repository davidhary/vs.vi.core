using System;
using System.Collections.Generic;

namespace isr.VI.K34980
{

    /// <summary> Information about the version of a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks>
        /// KEYSIGHT, Model 34980, xxxxxxx,m.mm–b.bb–f.ff–d.dd <para>
        /// where</para><para>xxxxxxx is the serial number.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument
        ///                         <see cref="VersionInfoBase.FirmwareRevisionElements">revisions</see>
        ///                         e.g., <c>m.mm–b.bb–f.ff–d.dd</c> where <para>
        ///                         m.mm - Mainframe version number; </para>. </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '-' ) );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Mainframe.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.BootCode.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.FrontPanel.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.InternalMeter.ToString(), revSections.Dequeue().Trim() );
            }
        }
    }
}
