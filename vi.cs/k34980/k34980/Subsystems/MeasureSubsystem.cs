namespace isr.VI.K34980
{

    /// <summary> Defines a Measure Subsystem for a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class MeasureSubsystem : MeasureSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MeasureSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            // reading elements are added (reading is initialized) when the format system is reset.
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " READ, FETCH "

        /// <summary> Gets or sets the fetch command. </summary>
        /// <value> The fetch command. </value>
        protected override string FetchCommand { get; set; } = ":FETCH?";

        /// <summary> Gets or sets the read command. </summary>
        /// <value> The read command. </value>
        protected override string ReadCommand { get; set; } = ":READ?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #endregion

    }
}
