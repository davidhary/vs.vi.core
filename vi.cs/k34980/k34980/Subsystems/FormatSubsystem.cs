using System;

namespace isr.VI.K34980
{

    /// <summary> Defines a Format Subsystem for a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class FormatSubsystem : FormatSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="FormatSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public FormatSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Elements = ReadingElementTypes.Reading;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " ELEMENTS "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the elements query command. </summary>
        /// <value> The elements query command. </value>
        [Obsolete( "Not supported with the 34980A scanner" )]
        protected override string ElementsQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the elements command format. </summary>
        /// <value> The elements command format. </value>
        [Obsolete( "Not supported with the 34980A scanner" )]
        protected override string ElementsCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #endregion

    }
}
