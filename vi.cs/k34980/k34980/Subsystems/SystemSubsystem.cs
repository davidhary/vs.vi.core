namespace isr.VI.K34980
{

    /// <summary> Defines a System Subsystem for a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.BeeperEnabled = true;
        }

        #endregion

        #region " BEEPER ENABLED + IMMEDIATE "

        /// <summary> Gets or sets the Beeper enabled query command. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledQueryCommand { get; set; } = ":SYST:BEEP:STAT?";

        /// <summary> Gets or sets the Beeper enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledCommandFormat { get; set; } = ":SYST:BEEP:STAT {0:'1';'1';'0'}";

        /// <summary> Gets or sets the beeper immediate command format. </summary>
        /// <value> The beeper immediate command format. </value>
        protected override string BeeperImmediateCommandFormat { get; set; } = ":SYST:BEEP:IMM {0}, {1}";

        #endregion

    }
}
