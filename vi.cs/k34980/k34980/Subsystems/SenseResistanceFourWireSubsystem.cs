namespace isr.VI.K34980
{

    /// <summary>
    /// Defines a SCPI Sense Resistance Subsystem for a Keysight 34980 Meter/Scanner.
    /// </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2014-03-01, 3.0.5173. </para>
    /// </remarks>
    public class SenseResistanceFourWireSubsystem : SenseResistanceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseResistanceFourWireSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ohm );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm;
            this.ResistanceRangeCurrents.Clear();
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0m, 0m, "Auto Range" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 100m, 0.001m, $"100 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 1000m, 0.001m, $"1000 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 1 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 10000m, 0.0001m, $"10 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 100 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 100000m, 0.00001m, $"100 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 10 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 1000000m, 0.000005m, $"1 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 5 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 10000000m, 0.000005m, $"10 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 5 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.PowerLineCyclesRange = this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d ) == 60d ? new Core.Primitives.RangeR( 0.02d, 200d ) : new Core.Primitives.RangeR( 0.02d, 200d );
            this.FunctionRange = new Core.Primitives.RangeR( 100d, 100000000.0d );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm;
            base.DefineKnownResetState();
            this.PowerLineCyclesRange = new Core.Primitives.RangeR( 0.02d, 200d );
            this.FunctionRange = new Core.Primitives.RangeR( 100d, 100000000.0d );
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:FRES:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:FRES:RANG:AUTO?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:FRES:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:FRES:NPLC?";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SENS:FRES:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SENS:FRES:RANG?";

        #endregion

        #endregion

    }
}
