namespace isr.VI.K34980
{

    /// <summary> Defines a Trigger Subsystem for a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TriggerSubsystem : TriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TriggerSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.SupportedTriggerSources = TriggerSources.Bus | TriggerSources.External | TriggerSources.Immediate;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " ABORT / INIT COMMANDS "

        /// <summary> Gets or sets the Abort command. </summary>
        /// <value> The Abort command. </value>
        protected override string AbortCommand { get; set; } = ":ABOR";

        /// <summary> Gets or sets the initiate command. </summary>
        /// <value> The initiate command. </value>
        protected override string InitiateCommand { get; set; } = ":INIT";

        #endregion

        #region " AUTO DELAY "

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledCommandFormat { get; set; } = ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledQueryCommand { get; set; } = ":TRIG:DEL:AUTO?";

        #endregion

        #region " TRIGGER COUNT "

        /// <summary> Gets or sets trigger count query command. </summary>
        /// <value> The trigger count query command. </value>
        protected override string TriggerCountQueryCommand { get; set; } = ":TRIG:COUN?";

        /// <summary> Gets or sets trigger count command format. </summary>
        /// <value> The trigger count command format. </value>
        protected override string TriggerCountCommandFormat { get; set; } = ":TRIG:COUN {0}";

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = @":TRIG:DEL {0:s\.FFFFFFF}";

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = ":TRIG:DEL?";

        #endregion

        #region " SOURCE "

        /// <summary> Gets or sets the Trigger Source query command. </summary>
        /// <value> The Trigger Source query command. </value>
        protected override string TriggerSourceQueryCommand { get; set; } = ":TRIG:SOUR?";

        /// <summary> Gets or sets the Trigger Source command format. </summary>
        /// <value> The Trigger Source command format. </value>
        protected override string TriggerSourceCommandFormat { get; set; } = ":TRIG:SOUR {0}";

        #endregion

        #region " TIMER TIME SPAN "

        /// <summary> Gets or sets the Timer Interval command format. </summary>
        /// <value> The query command format. </value>
        protected override string TimerIntervalCommandFormat { get; set; } = @":TRIG:TIM {0:s\.FFFFFFF}";

        /// <summary>
        /// Gets or sets the Timer Interval format for converting the query to time span.
        /// </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the Timer Interval query command. </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalQueryCommand { get; set; } = ":TRIG:TIM?";

        #endregion

        #endregion

    }
}
