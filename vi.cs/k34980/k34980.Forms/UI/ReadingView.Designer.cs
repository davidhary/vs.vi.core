﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K34980.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadingView));
            __ReadingsDataGridView = new System.Windows.Forms.DataGridView();
            __ReadingsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(ReadingsDataGridView_DataError);
            _ReadingToolStrip = new System.Windows.Forms.ToolStrip();
            __ReadButton = new System.Windows.Forms.ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __InitiateButton = new System.Windows.Forms.ToolStripButton();
            __InitiateButton.Click += new EventHandler(InitiateButton_Click);
            __TraceButton = new System.Windows.Forms.ToolStripButton();
            __TraceButton.Click += new EventHandler(TraceButton_Click);
            _ReadingsCountLabel = new System.Windows.Forms.ToolStripLabel();
            __ReadingComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReadingComboBox.SelectedIndexChanged += new EventHandler(ReadingComboBox_SelectedIndexChanged);
            __AbortButton = new System.Windows.Forms.ToolStripButton();
            __AbortButton.Click += new EventHandler(AbortButton_Click);
            __ClearBufferDisplayButton = new System.Windows.Forms.ToolStripButton();
            __ClearBufferDisplayButton.Click += new EventHandler(ClearBufferDisplayButton_Click);
            ((System.ComponentModel.ISupportInitialize)__ReadingsDataGridView).BeginInit();
            _ReadingToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ReadingsDataGridView
            // 
            __ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            __ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            __ReadingsDataGridView.Location = new System.Drawing.Point(0, 25);
            __ReadingsDataGridView.Name = "__ReadingsDataGridView";
            __ReadingsDataGridView.Size = new System.Drawing.Size(383, 301);
            __ReadingsDataGridView.TabIndex = 25;
            // 
            // _ReadingToolStrip
            // 
            _ReadingToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ReadButton, __InitiateButton, __TraceButton, _ReadingsCountLabel, __ReadingComboBox, __AbortButton, __ClearBufferDisplayButton });
            _ReadingToolStrip.Location = new System.Drawing.Point(0, 0);
            _ReadingToolStrip.Name = "_ReadingToolStrip";
            _ReadingToolStrip.Size = new System.Drawing.Size(383, 25);
            _ReadingToolStrip.TabIndex = 24;
            _ReadingToolStrip.Text = "ToolStrip1";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(37, 22);
            __ReadButton.Text = "Read";
            __ReadButton.ToolTipText = "Read single reading";
            // 
            // _InitiateButton
            // 
            __InitiateButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __InitiateButton.Image = (System.Drawing.Image)resources.GetObject("_InitiateButton.Image");
            __InitiateButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __InitiateButton.Name = "__InitiateButton";
            __InitiateButton.Size = new System.Drawing.Size(47, 22);
            __InitiateButton.Text = "Initiate";
            // 
            // _TraceButton
            // 
            __TraceButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __TraceButton.Image = (System.Drawing.Image)resources.GetObject("_TraceButton.Image");
            __TraceButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __TraceButton.Name = "__TraceButton";
            __TraceButton.Size = new System.Drawing.Size(39, 22);
            __TraceButton.Text = "Trace";
            __TraceButton.ToolTipText = "Reads the buffer";
            // 
            // _ReadingsCountLabel
            // 
            _ReadingsCountLabel.Name = "_ReadingsCountLabel";
            _ReadingsCountLabel.Size = new System.Drawing.Size(13, 22);
            _ReadingsCountLabel.Text = "0";
            _ReadingsCountLabel.ToolTipText = "Buffer count";
            // 
            // _ReadingComboBox
            // 
            __ReadingComboBox.Name = "__ReadingComboBox";
            __ReadingComboBox.Size = new System.Drawing.Size(121, 25);
            __ReadingComboBox.ToolTipText = "Select reading type";
            // 
            // _AbortButton
            // 
            __AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            __AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AbortButton.Name = "__AbortButton";
            __AbortButton.Size = new System.Drawing.Size(41, 22);
            __AbortButton.Text = "Abort";
            __AbortButton.ToolTipText = "Aborts active trigger";
            // 
            // _ClearBufferDisplayButton
            // 
            __ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ClearBufferDisplayButton.Font = new System.Drawing.Font("Wingdings", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(2));
            __ClearBufferDisplayButton.Image = (System.Drawing.Image)resources.GetObject("_ClearBufferDisplayButton.Image");
            __ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearBufferDisplayButton.Name = "__ClearBufferDisplayButton";
            __ClearBufferDisplayButton.Size = new System.Drawing.Size(25, 22);
            __ClearBufferDisplayButton.Text = string.Empty;
            __ClearBufferDisplayButton.ToolTipText = "CLear the buffer display";
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(__ReadingsDataGridView);
            Controls.Add(_ReadingToolStrip);
            Name = "ReadingView";
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            ((System.ComponentModel.ISupportInitialize)__ReadingsDataGridView).EndInit();
            _ReadingToolStrip.ResumeLayout(false);
            _ReadingToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.DataGridView __ReadingsDataGridView;

        private System.Windows.Forms.DataGridView _ReadingsDataGridView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingsDataGridView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingsDataGridView != null)
                {
                    __ReadingsDataGridView.DataError -= ReadingsDataGridView_DataError;
                }

                __ReadingsDataGridView = value;
                if (__ReadingsDataGridView != null)
                {
                    __ReadingsDataGridView.DataError += ReadingsDataGridView_DataError;
                }
            }
        }

        private System.Windows.Forms.ToolStrip _ReadingToolStrip;
        private System.Windows.Forms.ToolStripButton __ReadButton;

        private System.Windows.Forms.ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __InitiateButton;

        private System.Windows.Forms.ToolStripButton _InitiateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click -= InitiateButton_Click;
                }

                __InitiateButton = value;
                if (__InitiateButton != null)
                {
                    __InitiateButton.Click += InitiateButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __TraceButton;

        private System.Windows.Forms.ToolStripButton _TraceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TraceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TraceButton != null)
                {
                    __TraceButton.Click -= TraceButton_Click;
                }

                __TraceButton = value;
                if (__TraceButton != null)
                {
                    __TraceButton.Click += TraceButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _ReadingsCountLabel;
        private System.Windows.Forms.ToolStripComboBox __ReadingComboBox;

        private System.Windows.Forms.ToolStripComboBox _ReadingComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged -= ReadingComboBox_SelectedIndexChanged;
                }

                __ReadingComboBox = value;
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged += ReadingComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __AbortButton;

        private System.Windows.Forms.ToolStripButton _AbortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortButton != null)
                {
                    __AbortButton.Click -= AbortButton_Click;
                }

                __AbortButton = value;
                if (__AbortButton != null)
                {
                    __AbortButton.Click += AbortButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __ClearBufferDisplayButton;

        private System.Windows.Forms.ToolStripButton _ClearBufferDisplayButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearBufferDisplayButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearBufferDisplayButton != null)
                {
                    __ClearBufferDisplayButton.Click -= ClearBufferDisplayButton_Click;
                }

                __ClearBufferDisplayButton = value;
                if (__ClearBufferDisplayButton != null)
                {
                    __ClearBufferDisplayButton.Click += ClearBufferDisplayButton_Click;
                }
            }
        }
    }
}