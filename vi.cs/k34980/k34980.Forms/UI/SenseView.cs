using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K34980.Forms
{

    /// <summary> A Sense view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SenseView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public SenseView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ApplyFunctionModeButton.Name = "_ApplyFunctionModeButton";
            this.__ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton";
        }

        /// <summary> Creates a new <see cref="SenseView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="SenseView"/>. </returns>
        public static SenseView Create()
        {
            SenseView view = null;
            try
            {
                view = new SenseView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K34980Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K34980Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindSenseSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K34980Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Gets the Sense subsystem. </summary>
        /// <value> The Sense subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Bind Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseSubsystem( K34980Device device )
        {
            if ( this.SenseSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseSubsystem = device.SenseSubsystem;
                this.BindSubsystem( true, this.SenseSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K34980.SenseSubsystem.SupportedFunctionModes ) );
                this.HandlePropertyChanged( subsystem, nameof( K34980.SenseSubsystem.FunctionMode ) );
                this.HandlePropertyChanged( subsystem, nameof( K34980.SenseSubsystem.FunctionRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K34980.SenseSubsystem.FunctionRangeDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K34980.SenseSubsystem.FunctionUnit ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the supported function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnSupportedFunctionModesChanged( SenseSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.SupportedFunctionModes != SenseFunctionModes.None )
            {
                this._SenseFunctionComboBox.DataSource = null;
                this._SenseFunctionComboBox.Items.Clear();
                this._SenseFunctionComboBox.DataSource = typeof( SenseFunctionModes ).EnumValues().IncludeFilter( ( long ) subsystem.SupportedFunctionModes ).ValueDescriptionPairs().ToList();
                this._SenseFunctionComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
                this._SenseFunctionComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
                if ( this._SenseFunctionComboBox.Items.Count > 0 )
                {
                    this._SenseFunctionComboBox.SelectedItem = SenseFunctionModes.VoltageDC.ValueDescriptionPair();
                }
            }
        }

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        private static void OnFunctionModesChanged( SenseFunctionSubsystemBase value )
        {
            _ = value.QueryRange();
            _ = value.QueryAutoRangeEnabled();
            _ = value.QueryAutoZeroEnabled();
            _ = value.QueryPowerLineCycles();
        }

        /// <summary> Gets the selected sense subsystem. </summary>
        /// <value> The selected sense subsystem. </value>
        private SenseFunctionSubsystemBase SelectedSenseSubsystem => this.SelectSenseSubsystem( this.SelectedFunctionMode );

        /// <summary> Select sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> A SenseFunctionSubsystemBase. </returns>
        private SenseFunctionSubsystemBase SelectSenseSubsystem( SenseFunctionModes value )
        {
            SenseFunctionSubsystemBase result = this.Device.SenseVoltageSubsystem;
            switch ( value )
            {
                case SenseFunctionModes.CurrentDC:
                    {
                        this.BindSenseSubsystem( this.Device.SenseCurrentSubsystem );
                        break;
                    }

                case SenseFunctionModes.VoltageDC:
                    {
                        this.BindSenseSubsystem( this.Device.SenseVoltageSubsystem );
                        break;
                    }

                case SenseFunctionModes.Resistance:
                    {
                        this.BindSenseSubsystem( this.Device.SenseResistanceSubsystem );
                        break;
                    }

                case SenseFunctionModes.ResistanceFourWire:
                    {
                        this.BindSenseSubsystem( this.Device.SenseResistanceFourWireSubsystem );
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

            return result;
        }

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        private void OnFunctionModesChanged( SenseFunctionModes value )
        {
            OnFunctionModesChanged( this.SelectSenseSubsystem( value ) );
        }

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void OnFunctionModesChanged( SenseSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.FunctionMode.HasValue )
            {
                var value = subsystem.FunctionMode.GetValueOrDefault( SenseFunctionModes.None );
                if ( value != SenseFunctionModes.None )
                {
                    _ = this._SenseFunctionComboBox.SafeSelectSenseFunctionModes( value );
                    this.OnFunctionModesChanged( value );
                }
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( K34980.SenseSubsystem.SupportedFunctionModes ):
                    {
                        this.OnSupportedFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( K34980.SenseSubsystem.FunctionMode ):
                    {
                        this.OnFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( K34980.SenseSubsystem.FunctionRange ):
                    {
                        _ = this._SenseRangeNumeric.RangeSetter( subsystem.FunctionRange.Min, subsystem.FunctionRange.Max );
                        break;
                    }

                case nameof( K34980.SenseSubsystem.FunctionRangeDecimalPlaces ):
                    {
                        this._SenseRangeNumeric.DecimalPlaces = subsystem.FunctionRangeDecimalPlaces;
                        break;
                    }

                case nameof( K34980.SenseSubsystem.FunctionUnit ):
                    {
                        this.Device.MeasureSubsystem.ReadingAmounts.PrimaryReading.ApplyUnit( subsystem.FunctionUnit );
                        subsystem.ReadingAmounts.PrimaryReading.ApplyUnit( subsystem.FunctionUnit );
                        this._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:";
                        this._SenseRangeNumericLabel.Left = this._SenseRangeNumeric.Left - this._SenseRangeNumericLabel.Width;
                        break;
                    }
            }
        }

        /// <summary> Sense subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SenseSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE FUNCTION "

        /// <summary> Gets the sense function subsystem. </summary>
        /// <value> The sense function subsystem. </value>
        private SenseFunctionSubsystemBase SenseFunctionSubsystem { get; set; }

        /// <summary> Bind Sense function subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseSubsystem( SenseFunctionSubsystemBase subsystem )
        {
            if ( this.SenseFunctionSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseFunctionSubsystem );
                this.SenseFunctionSubsystem = null;
            }

            if ( subsystem is object )
            {
                this.SenseFunctionSubsystem = subsystem;
                this.BindSubsystem( true, this.SenseFunctionSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseFunctionSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseFunctionSubsystemBasePropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.AutoRangeEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.PowerLineCycles ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.PowerLineCyclesRange ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.Range ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.FunctionRange ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.FunctionRangeDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseFunctionSubsystemBase.FunctionUnit ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseFunctionSubsystemBasePropertyChanged;
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseFunctionSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( SenseFunctionSubsystemBase.AutoRangeEnabled ):
                    {
                        if ( this.Device is object && subsystem.AutoRangeEnabled.HasValue )
                        {
                            this._SenseAutoRangeToggle.Checked = subsystem.AutoRangeEnabled.Value;
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.PowerLineCycles ):
                    {
                        if ( this.Device is object && subsystem.PowerLineCycles.HasValue )
                        {
                            _ = this._PowerLineCyclesNumeric.ValueSetter( subsystem.PowerLineCycles.Value );
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.PowerLineCyclesRange ):
                    {
                        this._PowerLineCyclesNumeric.Maximum = ( decimal ) subsystem.PowerLineCyclesRange.Max;
                        this._PowerLineCyclesNumeric.Minimum = ( decimal ) subsystem.PowerLineCyclesRange.Min;
                        this._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces;
                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.Range ):
                    {
                        if ( this.Device is object && subsystem.Range.HasValue )
                        {
                            _ = this._SenseRangeNumeric.ValueSetter( subsystem.Range.Value );
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.FunctionRange ):
                    {
                        _ = this._SenseRangeNumeric.RangeSetter( subsystem.FunctionRange.Min, subsystem.FunctionRange.Max );
                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.FunctionRangeDecimalPlaces ):
                    {
                        this._SenseRangeNumeric.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces;
                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.FunctionUnit ):
                    {
                        this._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:";
                        this._SenseRangeNumericLabel.Left = this._SenseRangeNumeric.Left - this._SenseRangeNumericLabel.Width;
                        break;
                    }
            }
        }

        /// <summary> Sense voltage subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseFunctionSubsystemBasePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SenseFunctionSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseFunctionSubsystemBasePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseFunctionSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE SETTINGS: FUNCTION MODE "

        /// <summary> Selects a new sense mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        internal void ApplyFunctionMode( SenseFunctionModes value )
        {
            if ( this.Device?.IsDeviceOpen == true )
            {
                _ = this.Device.SenseSubsystem.ApplyFunctionMode( value );
            }
        }

        /// <summary> Gets the selected function mode. </summary>
        /// <value> The selected function mode. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SenseFunctionModes SelectedFunctionMode => ( SenseFunctionModes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._SenseFunctionComboBox.SelectedItem).Key );

        #endregion

        #region " CONTROL EVENT HANDLERS: SENSE "

        /// <summary> Applies the function mode button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        ///                                             <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyFunctionModeButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} applying function mode";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.ApplyFunctionMode( this.SelectedFunctionMode );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Sense range setter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void SenseRangeSetter( double value )
        {
            if ( value <= ( double ) this._SenseRangeNumeric.Maximum && value >= ( double ) this._SenseRangeNumeric.Minimum )
                this._SenseRangeNumeric.Value = ( decimal ) value;
        }

        /// <summary> Applies the selected measurements settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplySenseSettings()
        {
            var subsystem = this.SelectedSenseSubsystem;
            if ( !Equals( subsystem.PowerLineCycles, this._PowerLineCyclesNumeric.Value ) )
            {
                _ = subsystem.ApplyPowerLineCycles( ( double ) this._PowerLineCyclesNumeric.Value );
            }

            if ( !Nullable.Equals( subsystem.AutoRangeEnabled, this._SenseAutoRangeToggle.Checked ) )
            {
                _ = subsystem.ApplyAutoRangeEnabled( this._SenseAutoRangeToggle.Checked );
            }

            if ( subsystem.AutoRangeEnabled == true )
            {
                _ = subsystem.QueryRange();
            }
            else if ( !Equals( subsystem.Range, this._SenseRangeNumeric.Value ) )
            {
                _ = subsystem.ApplyRange( ( double ) this._SenseRangeNumeric.Value );
            }
        }

        /// <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySenseSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} applying sense settings";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.ApplySenseSettings();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
