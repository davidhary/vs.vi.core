﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K34980.Forms
{
    [DesignerGenerated()]
    public partial class SenseView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __ApplyFunctionModeButton = new System.Windows.Forms.Button();
            __ApplyFunctionModeButton.Click += new EventHandler(ApplyFunctionModeButton_Click);
            _TriggerDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _SenseRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _PowerLineCyclesNumeric = new System.Windows.Forms.NumericUpDown();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            _SenseRangeNumericLabel = new System.Windows.Forms.Label();
            _IntegrationPeriodNumericLabel = new System.Windows.Forms.Label();
            _SenseFunctionComboBox = new System.Windows.Forms.ComboBox();
            _SenseFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _SenseAutoRangeToggle = new System.Windows.Forms.CheckBox();
            __ApplySenseSettingsButton = new System.Windows.Forms.Button();
            __ApplySenseSettingsButton.Click += new EventHandler(ApplySenseSettingsButton_Click);
            _Panel = new System.Windows.Forms.Panel();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_PowerLineCyclesNumeric).BeginInit();
            _Panel.SuspendLayout();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _ApplyFunctionModeButton
            // 
            __ApplyFunctionModeButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __ApplyFunctionModeButton.Location = new System.Drawing.Point(310, 10);
            __ApplyFunctionModeButton.Name = "__ApplyFunctionModeButton";
            __ApplyFunctionModeButton.Size = new System.Drawing.Size(38, 25);
            __ApplyFunctionModeButton.TabIndex = 32;
            __ApplyFunctionModeButton.Text = "SET";
            __ApplyFunctionModeButton.UseVisualStyleBackColor = true;
            // 
            // _TriggerDelayNumeric
            // 
            _TriggerDelayNumeric.DecimalPlaces = 3;
            _TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerDelayNumeric.Location = new System.Drawing.Point(118, 111);
            _TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
            _TriggerDelayNumeric.Size = new System.Drawing.Size(76, 25);
            _TriggerDelayNumeric.TabIndex = 30;
            // 
            // _SenseRangeNumeric
            // 
            _SenseRangeNumeric.DecimalPlaces = 3;
            _SenseRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SenseRangeNumeric.Location = new System.Drawing.Point(118, 77);
            _SenseRangeNumeric.Maximum = new decimal(new int[] { 1010, 0, 0, 0 });
            _SenseRangeNumeric.Name = "_SenseRangeNumeric";
            _SenseRangeNumeric.Size = new System.Drawing.Size(76, 25);
            _SenseRangeNumeric.TabIndex = 27;
            _SenseRangeNumeric.Value = new decimal(new int[] { 105, 0, 0, 196608 });
            // 
            // _PowerLineCyclesNumeric
            // 
            _PowerLineCyclesNumeric.DecimalPlaces = 3;
            _PowerLineCyclesNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PowerLineCyclesNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _PowerLineCyclesNumeric.Location = new System.Drawing.Point(118, 43);
            _PowerLineCyclesNumeric.Maximum = new decimal(new int[] { 200, 0, 0, 0 });
            _PowerLineCyclesNumeric.Minimum = new decimal(new int[] { 2, 0, 0, 131072 });
            _PowerLineCyclesNumeric.Name = "_PowerLineCyclesNumeric";
            _PowerLineCyclesNumeric.Size = new System.Drawing.Size(76, 25);
            _PowerLineCyclesNumeric.TabIndex = 25;
            _PowerLineCyclesNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(8, 115);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(107, 17);
            _TriggerDelayNumericLabel.TabIndex = 29;
            _TriggerDelayNumericLabel.Text = "Trigger Delay [s]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseRangeNumericLabel
            // 
            _SenseRangeNumericLabel.AutoSize = true;
            _SenseRangeNumericLabel.Location = new System.Drawing.Point(48, 81);
            _SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel";
            _SenseRangeNumericLabel.Size = new System.Drawing.Size(68, 17);
            _SenseRangeNumericLabel.TabIndex = 26;
            _SenseRangeNumericLabel.Text = "Range [V]:";
            _SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _IntegrationPeriodNumericLabel
            // 
            _IntegrationPeriodNumericLabel.AutoSize = true;
            _IntegrationPeriodNumericLabel.Location = new System.Drawing.Point(18, 47);
            _IntegrationPeriodNumericLabel.Name = "_IntegrationPeriodNumericLabel";
            _IntegrationPeriodNumericLabel.Size = new System.Drawing.Size(98, 17);
            _IntegrationPeriodNumericLabel.TabIndex = 24;
            _IntegrationPeriodNumericLabel.Text = "Aperture [nplc]:";
            _IntegrationPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseFunctionComboBox
            // 
            _SenseFunctionComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _SenseFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _SenseFunctionComboBox.Items.AddRange(new object[] { "I", "V" });
            _SenseFunctionComboBox.Location = new System.Drawing.Point(118, 9);
            _SenseFunctionComboBox.Name = "_SenseFunctionComboBox";
            _SenseFunctionComboBox.Size = new System.Drawing.Size(187, 25);
            _SenseFunctionComboBox.TabIndex = 23;
            // 
            // _SenseFunctionComboBoxLabel
            // 
            _SenseFunctionComboBoxLabel.AutoSize = true;
            _SenseFunctionComboBoxLabel.Location = new System.Drawing.Point(57, 13);
            _SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel";
            _SenseFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SenseFunctionComboBoxLabel.TabIndex = 22;
            _SenseFunctionComboBoxLabel.Text = "Function:";
            _SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseAutoRangeToggle
            // 
            _SenseAutoRangeToggle.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _SenseAutoRangeToggle.Location = new System.Drawing.Point(200, 79);
            _SenseAutoRangeToggle.Name = "_SenseAutoRangeToggle";
            _SenseAutoRangeToggle.Size = new System.Drawing.Size(103, 21);
            _SenseAutoRangeToggle.TabIndex = 28;
            _SenseAutoRangeToggle.Text = "Auto Range";
            _SenseAutoRangeToggle.UseVisualStyleBackColor = true;
            // 
            // _ApplySenseSettingsButton
            // 
            __ApplySenseSettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __ApplySenseSettingsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplySenseSettingsButton.Location = new System.Drawing.Point(294, 105);
            __ApplySenseSettingsButton.Name = "__ApplySenseSettingsButton";
            __ApplySenseSettingsButton.Size = new System.Drawing.Size(58, 30);
            __ApplySenseSettingsButton.TabIndex = 31;
            __ApplySenseSettingsButton.Text = "&Apply";
            __ApplySenseSettingsButton.UseVisualStyleBackColor = true;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__ApplyFunctionModeButton);
            _Panel.Controls.Add(_TriggerDelayNumeric);
            _Panel.Controls.Add(_SenseRangeNumeric);
            _Panel.Controls.Add(_PowerLineCyclesNumeric);
            _Panel.Controls.Add(_TriggerDelayNumericLabel);
            _Panel.Controls.Add(_SenseRangeNumericLabel);
            _Panel.Controls.Add(_IntegrationPeriodNumericLabel);
            _Panel.Controls.Add(_SenseFunctionComboBox);
            _Panel.Controls.Add(_SenseFunctionComboBoxLabel);
            _Panel.Controls.Add(_SenseAutoRangeToggle);
            _Panel.Controls.Add(__ApplySenseSettingsButton);
            _Panel.Location = new System.Drawing.Point(10, 89);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(363, 148);
            _Panel.TabIndex = 0;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(383, 326);
            _Layout.TabIndex = 1;
            // 
            // SenseView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_Layout);
            Name = "SenseView";
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_PowerLineCyclesNumeric).EndInit();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.Button __ApplyFunctionModeButton;

        private System.Windows.Forms.Button _ApplyFunctionModeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyFunctionModeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyFunctionModeButton != null)
                {
                    __ApplyFunctionModeButton.Click -= ApplyFunctionModeButton_Click;
                }

                __ApplyFunctionModeButton = value;
                if (__ApplyFunctionModeButton != null)
                {
                    __ApplyFunctionModeButton.Click += ApplyFunctionModeButton_Click;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _TriggerDelayNumeric;
        private System.Windows.Forms.NumericUpDown _SenseRangeNumeric;
        private System.Windows.Forms.NumericUpDown _PowerLineCyclesNumeric;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
        private System.Windows.Forms.Label _SenseRangeNumericLabel;
        private System.Windows.Forms.Label _IntegrationPeriodNumericLabel;
        private System.Windows.Forms.ComboBox _SenseFunctionComboBox;
        private System.Windows.Forms.Label _SenseFunctionComboBoxLabel;
        private System.Windows.Forms.CheckBox _SenseAutoRangeToggle;
        private System.Windows.Forms.Button __ApplySenseSettingsButton;

        private System.Windows.Forms.Button _ApplySenseSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySenseSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click -= ApplySenseSettingsButton_Click;
                }

                __ApplySenseSettingsButton = value;
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click += ApplySenseSettingsButton_Click;
                }
            }
        }
    }
}