﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "N5700 VI Tests" )]
[assembly: AssemblyDescription( "N5700 Power Supply Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.N5700" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
