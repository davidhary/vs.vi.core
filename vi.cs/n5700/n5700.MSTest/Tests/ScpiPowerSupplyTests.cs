using System;

using Microsoft.VisualBasic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.N5700.MSTest
{

    /// <summary>
    /// This is a test class for PowerSupplyTest and is intended to contain all PowerSupplyTest Unit
    /// Tests.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [TestClass()]
    [TestCategory( "n5700" )]
    public class ScpiPowerSupplyTests
    {

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Select resource name. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="interfaceType"> Type of the interface. </param>
        /// <returns> A String. </returns>
        internal string SelectResourceName( Pith.HardwareInterfaceType interfaceType )
        {
            switch ( interfaceType )
            {
                case Pith.HardwareInterfaceType.Gpib:
                    {
                        return "GPIB0::5::INSTR";
                    }

                case Pith.HardwareInterfaceType.Tcpip:
                    {
                        return "TCPIP0::A-N5767A-K4381";
                    }

                case Pith.HardwareInterfaceType.Usb:
                    {
                        return "USB0::0x0957::0x0807::N5767A-US11K4381H::0::INSTR";
                    }

                default:
                    {
                        return "GPIB0::5::INSTR";
                    }
            }
        }

        /// <summary> (Unit Test Method) opens close session should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenCloseSessionShouldPass()
        {
            using var device = new N5700Device();
            try
            {
                this.AssertOpenSession( device, Pith.HardwareInterfaceType.Usb );
            }
            catch
            {
                throw;
            }
            finally
            {
                if ( device.IsDeviceOpen )
                    device.CloseSession();
            }
        }

        /// <summary> Assert open session. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="device">             The device. </param>
        /// <param name="usingInterfaceType"> Type of the using interface. </param>
        public void AssertOpenSession( N5700Device device, Pith.HardwareInterfaceType usingInterfaceType )
        {
            var (Success, Details) = device.TryOpenSession( this.SelectResourceName( usingInterfaceType ), "Power Supply" );
            Assert.IsTrue( Success, $"session should open; {Details}" );
            Assert.IsTrue( device.IsDeviceOpen, $"session to device {device.ResourceNameCaption} should be open" );
        }

        /// <summary> (Unit Test Method) output off should toggle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OutputOffShouldToggle()
        {
            bool expectedBoolean = true;
            bool actualBoolean;
            using var device = new N5700Device();
            this.AssertOpenSession( device, Pith.HardwareInterfaceType.Gpib );
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState( false ).GetValueOrDefault( true );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output Off;" );
            expectedBoolean = false;
            actualBoolean = device.OutputSubsystem.QueryOutputOnState().GetValueOrDefault( true );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output State" );
            string expectedString = "no error";
            if ( device.StatusSubsystem.Session.IsErrorBitSet() )
            {
                var (success, details) = device.StatusSubsystem.TryQueryExistingDeviceErrors();
                Assert.IsTrue( success, $"Device error query failed {details}" );
            }

            string actualString = device.StatusSubsystem.CompoundErrorMessage;
            Assert.AreEqual( expectedString, actualString, true, System.Globalization.CultureInfo.CurrentCulture, "Device error mismatch" );
        }

        /// <summary> (Unit Test Method) turn and keep on should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void TurnAndKeepOnShouldPass()
        {
            double voltage = 28.0d;
            double currentLimit = 2.0d;
            bool expectedBoolean = true;
            bool actualBoolean;
            using var device = new N5700Device();
            var e = new Core.ActionEventArgs();
            this.AssertOpenSession( device, Pith.HardwareInterfaceType.Gpib );
            device.ResetClearInit();
            actualBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, "Reset;" );
            string expectedString = "Agilent";
            string actualString = device.StatusSubsystem.Identity.Substring( 0, Strings.Len( expectedString ) );
            Assert.AreEqual( expectedString, actualString );
            actualBoolean = device.OutputOn( voltage, currentLimit, 0.1d, voltage + 2d, TimeSpan.FromMilliseconds( 1000d ) );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output On;" );
            expectedBoolean = true;
            actualBoolean = device.OutputSubsystem.QueryOutputOnState().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output State;" );
            double expectedDouble = voltage;
            double actualDouble = device.MeasureVoltageSubsystem.MeasureReadingAmounts().GetValueOrDefault( 0d );
            Assert.AreEqual( expectedDouble, actualDouble, 0.1d );
            expectedDouble = 0.1d;
            actualDouble = device.MeasureCurrentSubsystem.MeasureReadingAmounts().GetValueOrDefault( 0d );
            if ( actualDouble < expectedDouble )
            {
                Assert.AreEqual( expectedDouble, actualDouble, 0.1d, "Actual must be greater" );
            }

            expectedString = "no error";
            if ( device.StatusSubsystem.Session.IsErrorBitSet() )
            {
                var (Success, Details) = device.StatusSubsystemBase.TryQueryExistingDeviceErrors();
                Assert.IsTrue( Success, $"Device error query failed: {Details}" );
            }

            actualString = device.StatusSubsystem.CompoundErrorMessage;
            Assert.AreEqual( expectedString, actualString, true, System.Globalization.CultureInfo.CurrentCulture, "Device error mismatch" );
        }

        /// <summary> (Unit Test Method) output on off should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OutputOnOffShouldPass()
        {
            double voltage = 28.0d;
            double currentLimit = 1.0d;
            bool expectedBoolean = true;
            bool actualBoolean;
            using var device = new N5700Device();
            this.AssertOpenSession( device, Pith.HardwareInterfaceType.Gpib );
            device.ResetClearInit();
            actualBoolean = true;
            Assert.AreEqual( expectedBoolean, actualBoolean, "Reset;" );
            string expectedString = "Agilent";
            string actualString = device.StatusSubsystem.Identity.Substring( 0, Strings.Len( expectedString ) );
            Assert.AreEqual( expectedString, actualString );
            actualBoolean = device.OutputOn( voltage, currentLimit, 0.1d, voltage + 2d, TimeSpan.FromMilliseconds( 1000d ) );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output On;" );
            expectedBoolean = true;
            actualBoolean = device.OutputSubsystem.QueryOutputOnState().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output State;" );
            double expectedDouble = voltage;
            double actualDouble = device.MeasureVoltageSubsystem.MeasureReadingAmounts().GetValueOrDefault( 0d );
            Assert.AreEqual( expectedDouble, actualDouble, 0.1d );
            expectedDouble = 0.1d;
            actualDouble = device.MeasureCurrentSubsystem.MeasureReadingAmounts().GetValueOrDefault( 0d );
            if ( actualDouble < expectedDouble )
            {
                Assert.AreEqual( expectedDouble, actualDouble, 0.1d, "Actual must be greater" );
            }

            actualBoolean = device.OutputSubsystem.ApplyOutputOnState( false ).GetValueOrDefault( true );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output Off;" );
            expectedBoolean = false;
            actualBoolean = device.OutputSubsystem.QueryOutputOnState().GetValueOrDefault( true );
            Assert.AreEqual( expectedBoolean, actualBoolean, "Output State;" );
            expectedString = "no error";
            if ( device.Session.IsErrorBitSet() )
            {
                var (Success, Details) = device.StatusSubsystem.TryQueryExistingDeviceErrors();
                Assert.IsTrue( Success, $"Device error query failed: {Details}" );
            }

            actualString = device.StatusSubsystem.CompoundErrorMessage;
            Assert.AreEqual( expectedString, actualString, true, System.Globalization.CultureInfo.CurrentCulture, "Device error mismatch" );
        }

        /// <summary> (Unit Test Method) measure current should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MeasureCurrentShouldPass()
        {
            double expectedDouble = 0.0d;
            double actualDouble;
            using var device = new N5700Device();
            this.AssertOpenSession( device, Pith.HardwareInterfaceType.Gpib );
            actualDouble = device.MeasureCurrentSubsystem.MeasureReadingAmounts().GetValueOrDefault( 0d );
            Assert.AreEqual( expectedDouble, actualDouble );
        }
    }
}
