namespace isr.VI.N5700
{

    /// <summary> Defines a SCPI Output Subsystem for power supplies. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class OutputSubsystem : OutputSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="OutputSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public OutputSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " TERMINAL MODE "

        /// <summary> Gets or sets the terminals mode query command. </summary>
        /// <value> The terminals mode query command. </value>
        protected override string OutputTerminalsModeQueryCommand { get; set; } = ":OUTP:ROUT:TERM?";

        /// <summary> Gets or sets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        protected override string OutputTerminalsModeCommandFormat { get; set; } = ":OUTP:ROUT:TERM {0}";

        #endregion

        #endregion

    }
}
