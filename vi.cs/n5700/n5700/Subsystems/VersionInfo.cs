using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.VI.N5700
{

    /// <summary> Information about the version of a Keysight 5700 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
            this.FirmwareLevelMajorRevision = string.Empty;
            this.FirmwareLevelMinorRevision = string.Empty;
            this.FirmwareLevelDate = DateTime.MinValue;
            this.SupportsContactCheck = false;
        }

        /// <summary> Returns the instrument firmware level major revision name . </summary>
        /// <value> The firmware level major revision. </value>
        public string FirmwareLevelMajorRevision { get; private set; }

        /// <summary> Gets or sets the firmware level minor revision. </summary>
        /// <value> The firmware level minor revision. </value>
        public string FirmwareLevelMinorRevision { get; private set; }

        /// <summary> Returns the instrument firmware level date. </summary>
        /// <value> The firmware level date. </value>
        public DateTimeOffset FirmwareLevelDate { get; private set; }

        /// <summary> Returns True if the instrument supports contact check. </summary>
        /// <value> The supports contact check. </value>
        public bool SupportsContactCheck { get; private set; }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks>
        /// Identity: <para>
        /// <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02
        /// /D/B/E.</c>.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument firmware revision with the following items:
        ///                         <see cref="FirmwareLevelMajorRevision">firmware level major
        ///                         revision</see>
        ///                         <see cref="FirmwareLevelDate">date</see>
        ///                         <see cref="FirmwareLevelMinorRevision">firmware level minor
        ///                         revision</see>
        ///                         <see cref="VersionInfoBase.FirmwareRevisionElements">board revisions</see>
        ///                         e.g., <c>C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.  
        ///                         The last three letters separated by "/" indicate the board revisions
        ///                         (i.e. /Analog/Digital/Contact Check). Contact Check board revisions
        ///                         have the following features:<p>
        ///                         Revisions A through C have only one resistance range.</p><p>
        ///                         Revisions D and above have selectable resistance ranges.</p> </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '/' ) );

                // Rev: C11; revision: Oct 10 1997 09:51:36
                string firmwareLevel = revSections.Dequeue().Trim();
                this.FirmwareLevelMajorRevision = firmwareLevel.Split( ' ' )[0];

                // date string: Oct 10 1997 09:51:36
                this.FirmwareLevelDate = DateTimeOffset.TryParse( firmwareLevel.Substring( this.FirmwareLevelMajorRevision.Length ).Trim(), System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AssumeLocal, out DateTimeOffset fwDate )
                    ? fwDate
                    : DateTimeOffset.MinValue;


                // Minor revision: A02
                this.FirmwareLevelMinorRevision = revSections.Dequeue().Trim();
                if ( revSections.Any() )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Analog.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Any() )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Digital.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Any() )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.ContactCheck.ToString(), revSections.Dequeue().Trim() );
                this.SupportsContactCheck = this.FirmwareRevisionElements.ContainsKey( FirmwareRevisionElement.ContactCheck.ToString() );
            }
        }
    }
}
