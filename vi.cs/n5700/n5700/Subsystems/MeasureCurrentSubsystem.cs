namespace isr.VI.N5700
{

    /// <summary> Defines a SCPI Measure Current Subsystem for power supplies. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class MeasureCurrentSubsystem : MeasureCurrentSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="MeasureCurrentSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MeasureCurrentSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ampere );
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region "  READ, FETCH "

        /// <summary> Gets or sets the fetch command. </summary>
        /// <value> The fetch command. </value>
        protected override string FetchCommand { get; set; } = ":FETCH?";

        /// <summary> Gets or sets the read command. </summary>
        /// <value> The read command. </value>
        protected override string ReadCommand { get; set; } = ":READ?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #endregion

    }
}
