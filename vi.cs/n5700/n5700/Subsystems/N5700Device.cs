using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.N5700
{

    /// <summary> Implements a Power Supply device. </summary>
    /// <remarks>
    /// An instrument is defined, for the purpose of this library, as a device with a front panel.
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class N5700Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="N5700Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public N5700Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected N5700Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            My.MySettings.Default.PropertyChanged += this.MySettings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static N5700Device Create()
        {
            N5700Device device = null;
            try
            {
                device = new N5700Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static N5700Device Validated( N5700Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( N5700Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindMeasureCurrentSubsystem( null );
                this.BindMeasureVoltageSubsystem( null );
                this.BindOutputSubsystem( null );
                this.BindSourceCurrentSubsystem( null );
                this.BindSourceVoltageSubsystem( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.BindMeasureCurrentSubsystem( new MeasureCurrentSubsystem( this.StatusSubsystem ) );
                this.BindMeasureVoltageSubsystem( new MeasureVoltageSubsystem( this.StatusSubsystem ) );
                this.BindOutputSubsystem( new OutputSubsystem( this.StatusSubsystem ) );
                this.BindSourceCurrentSubsystem( new SourceCurrentSubsystem( this.StatusSubsystem ) );
                this.BindSourceVoltageSubsystem( new SourceVoltageSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Gets or sets the Measure Current Subsystem. </summary>
        /// <value> The Measure Current Subsystem. </value>
        public MeasureCurrentSubsystem MeasureCurrentSubsystem { get; private set; }

        /// <summary> Binds the Measure Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureCurrentSubsystem( MeasureCurrentSubsystem subsystem )
        {
            if ( this.MeasureCurrentSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MeasureCurrentSubsystem );
                this.MeasureCurrentSubsystem = null;
            }

            this.MeasureCurrentSubsystem = subsystem;
            if ( this.MeasureCurrentSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureCurrentSubsystem );
            }
        }

        /// <summary> Gets or sets the Measure Voltage Subsystem. </summary>
        /// <value> The Measure Voltage Subsystem. </value>
        public MeasureVoltageSubsystem MeasureVoltageSubsystem { get; private set; }

        /// <summary> Binds the Measure Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureVoltageSubsystem( MeasureVoltageSubsystem subsystem )
        {
            if ( this.MeasureVoltageSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MeasureVoltageSubsystem );
                this.MeasureVoltageSubsystem = null;
            }

            this.MeasureVoltageSubsystem = subsystem;
            if ( this.MeasureVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureVoltageSubsystem );
            }
        }

        /// <summary> Gets or sets the Output Subsystem. </summary>
        /// <value> The Output Subsystem. </value>
        public OutputSubsystem OutputSubsystem { get; private set; }

        /// <summary> Binds the Output subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindOutputSubsystem( OutputSubsystem subsystem )
        {
            if ( this.OutputSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.OutputSubsystem );
                this.OutputSubsystem = null;
            }

            this.OutputSubsystem = subsystem;
            if ( this.OutputSubsystem is object )
            {
                this.Subsystems.Add( this.OutputSubsystem );
            }
        }

        /// <summary> Gets or sets the Source Current Subsystem. </summary>
        /// <value> The Source Current Subsystem. </value>
        public SourceCurrentSubsystem SourceCurrentSubsystem { get; private set; }

        /// <summary> Binds the Source Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceCurrentSubsystem( SourceCurrentSubsystem subsystem )
        {
            if ( this.SourceCurrentSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SourceCurrentSubsystem );
                this.SourceCurrentSubsystem = null;
            }

            this.SourceCurrentSubsystem = subsystem;
            if ( this.SourceCurrentSubsystem is object )
            {
                this.Subsystems.Add( this.SourceCurrentSubsystem );
            }
        }

        /// <summary> Gets or sets the Source Voltage Subsystem. </summary>
        /// <value> The Source Voltage Subsystem. </value>
        public SourceVoltageSubsystem SourceVoltageSubsystem { get; private set; }

        /// <summary> Binds the Source Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceVoltageSubsystem( SourceVoltageSubsystem subsystem )
        {
            if ( this.SourceVoltageSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SourceVoltageSubsystem );
                this.SourceVoltageSubsystem = null;
            }

            this.SourceVoltageSubsystem = subsystem;
            if ( this.SourceVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.SourceVoltageSubsystem );
            }
        }

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #endregion

        #region " MIXED SUBSYSTEMS: OUTPUT / MEASURE "

        /// <summary>
        /// Sets the specified voltage and current limit and turns on the power supply.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="voltage">          The voltage. </param>
        /// <param name="currentLimit">     The current limit. </param>
        /// <param name="overvoltageLimit"> The over voltage limit. </param>
        public void OutputOn( double voltage, double currentLimit, double overvoltageLimit )
        {

            // Set the voltage
            _ = this.SourceVoltageSubsystem.ApplyLevel( voltage );

            // Set the over voltage level
            _ = this.SourceVoltageSubsystem.ApplyProtectionLevel( overvoltageLimit );

            // Turn on over current protection
            _ = this.SourceCurrentSubsystem.ApplyProtectionEnabled( true );

            // Set the current level
            _ = this.SourceCurrentSubsystem.ApplyLevel( currentLimit );

            // turn output on.
            _ = this.OutputSubsystem.ApplyOutputOnState( true );
            _ = this.Session.QueryOperationCompleted();

            // operation completion is not sufficient to ensure that the Device has hit the correct voltage.

        }

        /// <summary>
        /// Turns the power supply on waiting for the voltage to reach the desired level.
        /// </summary>
        /// <remarks> SCPI Command: OUTP ON. </remarks>
        /// <param name="voltage">      The voltage. </param>
        /// <param name="currentLimit"> The current limit. </param>
        /// <param name="delta">        The delta. </param>
        /// <param name="voltageLimit"> The over voltage limit. </param>
        /// <param name="timeout">      The timeout for awaiting for the voltage to reach the level. </param>
        /// <returns>
        /// <c>True</c> if successful <see cref="OutputModes">Output On</see> and in range;
        ///          <c>False</c> otherwise.
        /// </returns>
        public bool OutputOn( double voltage, double currentLimit, double delta, double voltageLimit, TimeSpan timeout )
        {
            this.OutputOn( voltage, currentLimit, voltageLimit );
            return this.MeasureVoltageSubsystem.AwaitLevel( voltage, delta, timeout );
        }

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "Power Supply Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"Trace log level changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"Trace show level changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double )sender.ReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.StatusReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime;
                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
