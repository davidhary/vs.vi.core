using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.N5700
{

    /// <summary> Defines a SCPI Status Subsystem for power supplies. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        ///                        session</see>. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ) )
        {
            this.VersionInfo = new VersionInfo();
            this.VersionInfoBase = this.VersionInfo;
            InitializeSession( session );
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion


        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based TSP session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.ClearExecutionStateCommand = Pith.Ieee488.Syntax.ClearExecutionStateCommand;
            session.ResetKnownStateCommand = Pith.Ieee488.Syntax.ResetKnownStateCommand;
            session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
            session.OperationCompletedQueryCommand = Pith.Ieee488.Syntax.OperationCompletedQueryCommand;
            session.StandardServiceEnableCommandFormat = Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat;
            session.StandardServiceEnableCompleteCommandFormat = Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat;
            session.ServiceRequestEnableCommandFormat = Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat;
            session.ServiceRequestEnableQueryCommand = Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand;
            session.StandardEventStatusQueryCommand = Pith.Ieee488.Syntax.StandardEventStatusQueryCommand;
            session.StandardEventEnableQueryCommand = Pith.Ieee488.Syntax.StandardEventEnableQueryCommand;
            session.WaitCommand = Pith.Ieee488.Syntax.WaitCommand;
        }

        #endregion

        #region " PRESET "

        /// <summary> Gets or sets the preset command. </summary>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = Pith.Scpi.Syntax.StatusPresetCommand;

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = Pith.Scpi.Syntax.ClearErrorQueueCommand;

        /// <summary> Gets or sets the error queue query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = Pith.Scpi.Syntax.NextErrorQueryCommand;

        #endregion

        #region " MEASUREMENT REGISTER EVENTS "

        /// <summary> Gets or sets the measurement status query command. </summary>
        /// <value> The measurement status query command. </value>
        protected override string MeasurementStatusQueryCommand { get; set; } = Pith.Scpi.Syntax.MeasurementEventQueryCommand;

        /// <summary> Gets or sets the measurement event condition query command. </summary>
        /// <value> The measurement event condition query command. </value>
        protected override string MeasurementEventConditionQueryCommand { get; set; } = Pith.Scpi.Syntax.MeasurementEventConditionQueryCommand;

        #endregion

        #region " OPERATION REGISTER EVENTS "

        /// <summary> Gets or sets the operation event enable Query command. </summary>
        /// <value> The operation event enable Query command. </value>
        protected override string OperationEventEnableQueryCommand { get; set; } = Pith.Scpi.Syntax.OperationEventEnableQueryCommand;

        /// <summary> Gets or sets the operation event enable command format. </summary>
        /// <value> The operation event enable command format. </value>
        protected override string OperationEventEnableCommandFormat { get; set; } = Pith.Scpi.Syntax.OperationEventEnableCommandFormat;

        /// <summary> Gets or sets the operation event status query command. </summary>
        /// <value> The operation event status query command. </value>
        protected override string OperationEventStatusQueryCommand { get; set; } = Pith.Scpi.Syntax.OperationEventQueryCommand;

        #endregion

        #region " QUESTIONABLE REGISTER "

        /// <summary> Gets or sets the questionable status query command. </summary>
        /// <value> The questionable status query command. </value>
        protected override string QuestionableStatusQueryCommand { get; set; } = Pith.Scpi.Syntax.QuestionableEventQueryCommand;

        #endregion

        #region " IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = Pith.Ieee488.Syntax.IdentityQueryCommand;

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, "Trying to read identity;. " );
                Core.ApplianceBase.DoEvents();
                // wait for the delay time.
                // Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.Talker.Publish( TraceEventType.Verbose, My.MyLibrary.TraceEventId, $"Setting identity to {value};. " );
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

    }
}
