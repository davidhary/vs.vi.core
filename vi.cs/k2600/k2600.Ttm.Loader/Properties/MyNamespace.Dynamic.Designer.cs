﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.VI.Tsp.K2600.Ttm.Loader.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public LoaderForm m_LoaderForm;

            public LoaderForm LoaderForm
            {
                [DebuggerHidden]
                get
                {
                    m_LoaderForm = Create__Instance__(m_LoaderForm);
                    return m_LoaderForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_LoaderForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_LoaderForm);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public MySplashScreen m_MySplashScreen;

            public MySplashScreen MySplashScreen
            {
                [DebuggerHidden]
                get
                {
                    m_MySplashScreen = Create__Instance__(m_MySplashScreen);
                    return m_MySplashScreen;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_MySplashScreen))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_MySplashScreen);
                }
            }
        }
    }
}