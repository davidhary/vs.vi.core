ISR.TTM. Loader<sub>&trade;</sub>: ISR Thermal Transient Meter Firmware Loading Program
The TTM software measures thermal transients using the Keithley 26xxA/B
source measure systems.

* [Releases](#Current-releases)
* [Software](#Software-Revision-History)
* [Firmware](#Firmware-Revision-History)
* [Fair End User Use License](#End-User-License)
* [Fair End User Firmware License](#Firmware-License)
* [Open Source License](#Open-Source-License)
* [Open Source License](#Open-Source-License)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Current releases [](#){name=Current-releases}

Software: 4.0.5362; 2014-09-06

Firmware: 2.3.4004 2010-12-23

### Software Revision History [](#){name=Software-Revision-History}

*7.0.7054 2019-04-25*  
Upgrades to 2019 core libraries and open sourced.

*4.0.6318 2017-04-19*  
Uses safe send event notifications. Uses full blown
exception reporting. Validated using TCP/IP. Ignore bogus instrument TSP
Syntax error messages.

*4.0.6276 2017-03-08*  
Uses .NET 4.6.1.

*4.0.5222 2014-04-19*  
Created based on legacy TTM Console.

*4.0.5099 2013-12-17*  
Uses new core frameworks for VISA and the Thermal
Transient Meter. Uses the new Thermal Transient Meter Driver 4.x. See
the API documentation for details. Installation package includes both 32
and 64 bit assemblies.

*3.1.4966 2013-08-06*  
New installation package. Adds driver source code to
the installation. Allows custom installation of Console, Driver, and
source code.

*3.1.4905 2012-11-10*  
Fixes installation.

*3.1.4871 2013-05-03*  
Changes product name to TTM.2013. Packages installer
in NSIS package. Installer alerts if a .NET revision update is required.
Adds alert sound to console application.

*3.1.4697 2012-11-10*  
Supports Shunt Resistance.

*3.0.4697 2012-11-10*  
Supports USB and Ethernet.

*3.0.4693 2012-11-06*  
Supports the 2600B GPIB.

*3.0.4687 2012-10-31*  
Uses Visual Studio 2010.

*2.3.4073 2011-02-25*  
Verified auto sequence.

*2.3.4049 2011-01-01*  
Adds contact check.

*2.3.4007 2010-12-21*  
Fetches, displays and saves the trace.

*2.3.3922 2010-09-27*  
Uses the new Driver and Device libraries.

*2.2.3891 2010-08-27*  
Released with version 3891 of the TTM Framework.

*2.2.3869 2010-08-05*  
Released with version 3868 of the TTM Framework.

*2.2.3839 2010-07-06*  
Renames application to TTM Console. Adds settings for
the default SMU to allow testing with SMUB on a two channel Driver.

*2.2.3509 2009-08-10*  
Updates released versions and messages.

*2.2.3507 2009-08-08*  
Increases thermal transient range to 1.514mA; sends
and reads back instrument measurement settings when applying values;
displays actual settings on the measurement panel; updates settings when
validating the numeric values (requires tabbing away from the control
for the change to register); adds time stamps to measurements; warns
operator when closing the form; adds operator and Lot id; displays times
tamps and serial numbers; allows setting a serial number; saves operator
id, lot id, timestamp and serial number.

*2.1.3342 2009-02-24*  
Version 2.1 released.

### Firmware Revision History [](#){name=Firmware-Revision-History}

*2.3.4009 2010-12-23*  
Outputs a message at the end of the triggered
sequence.

*2.3.3921 2010-09-26*  
Adds support for single trigger. Clears all
measurements when triggered to measure. Sets outcome to nil when
clearing so that it can be used to indicated that the measurement was
not made.

*2.2.3891 2010-08-27*  
Adds limits to menu. Uses the limits to determine
pass/fail outcome. Saves settings in non-volatile memory.

*2.2.3868 2010-08-04*  
Adds support for PLC control.

*2.2.3492 2009-07-23*  
Matches support library with KTS. Adds access control
load testing to those based on serial numbers.

*2.1.3134 2008-07-31*  
Upgraded and tested with 2636.

*2.1.3342 2009-02-24*  
Version 2.1 released.

Installation Information

This program installs the ISR TTM Loader.

This software measures thermal transients using the Keithley 26xxA/B
source measure systems.

**1. Supported Instruments**

2600A, 2600B

**2. Supported Interfaces**

GPIB (IEEE488.2); USB.

**3. Installation Package**

The program installation files can be downloaded from the TTM [FTP site](http://bit.ly/aJgNDP).

**4. First Time Installation**

The first time the program is installed on a computer, you may have to
install the Microsoft .NET framework and the NI VISA drivers. The
installer is capable of detecting the existing .Net version and display
an alert if an update is required.

**5. Installed Files**

The installation program copies files to the destination directory
chosen during installation. The default root folder is under the
*Integrated.Scientific.Resources* folder of the program files folder.
The program configuration files are located in the programs file folder.
When configuration changes are made, these are stored under the program
data folders. The public program data folder In Windows XP is
*C:\\Documents and Settings\\All Users\\Application
Data\\Integrated.Scientific.Resources*. In Windows versions following XP
this folder is located under
*Users\\Public\\AppData\\Local\\Integrated.Scientific.Resources* or
under the *Program Data* folder. The private folders are similarly
located under the specific user name instead of under to *All Users* or
*Public* folders listed above.

**6. On Line Resources**


Click on this [link](http://bit.ly/aJgNDP) to get the most
recent version of the TTM Console.

Click this
[link](http://www.microsoft.com/en-us/download/details.aspx?id=17851)
to get the Microsoft .NET Framework.

Click [.Net Info](http://msdn.microsoft.com/en-us/library/aa139615.aspx)
to get Information on the .NET framework deployment.

Click [NI VISA Runtime](http://ftp.ni.com/support/softlib/visa/VISA%20Run-Time%20Engine)
to download the National Instrument VISA runtime engine. Note that
NI-VISA is included in the NI-488.2 (GPIB) driver package.

**7. How to Uninstall**


Uninstall the program from the Add/Remove Programs applets of the
Control Panel.

**8. Contact Information**
support[at]IntegratedScientificResources[.]com

\(c\) 2005 by Integrated Scientific Resources, Inc. All rights reserved.

### End User License [](#){name=End-User-License}

Licensed under the [Fair End User Use License](http://www.isr.cc/licenses/FairEndUserUseLicense.pdf).

Unless required by applicable law or agreed to in writing, software
distributed under the ISR License is distributed on an \"AS IS\" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

### Firmware License [](#){name=Firmware-License}

Licensed under the [Fair End User Firmware License](http://www.isr.cc/licenses/FairEndUserUseLicense.pdf).

Unless required by applicable law or agreed to in writing, software
distributed under the ISR License is distributed on an \"AS IS\" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.

### Open Source License [](#){name=Open-Source-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  
Firmware for the Thermal Transient Meter instrument was developed and
tested using Eclipse ® from the [Eclipse Foundation](http://www.eclipse.org).

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)  
[table.copy](http://www.loop.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)  
[Working LUA base-64 codec](http://www.it-rfc.de)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
