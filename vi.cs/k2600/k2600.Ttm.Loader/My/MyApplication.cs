﻿
namespace isr.VI.Tsp.K2600.Ttm.Loader.My
{
    internal partial class MyApplication
    {

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 2; // TraceEventIds.IsrVITtmLoader

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "TTM Loader";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "TTM Loader";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "TTM.Loader";
    }
}