using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.VI.Tsp.K2600.Ttm.Loader.My
{
    internal partial class MyApplication
    {

        /// <summary> Destroys objects for this project. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        internal void Destroy()
        {
            MyProject.Forms.MySplashScreen.Close();
            MyProject.Forms.MySplashScreen.Dispose();
            this.SplashScreen = null;
        }

        /// <summary> Gets or sets the name of the computer. </summary>
        /// <value> The name of the computer. </value>
        public static string ComputerName { get; set; }

        /// <summary> Instantiates the application to its known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool TryinitializeKnownState()
        {
            try
            {
                Cursor.Current = Cursors.AppStarting;

                // show status
                if ( InDesignMode )
                {
                    this.SplashTraceEvent( TraceEventType.Verbose, "Application is initializing. Design Mode." );
                }
                else
                {
                    this.SplashTraceEvent( TraceEventType.Verbose, "Application is initializing. Runtime Mode." );
                }

                // Apply command line results.
                if ( CommandLineInfo.DevicesEnabled.HasValue )
                {
                    this.SplashTraceEvent( TraceEventType.Information, "{0} use of devices from command line", Interaction.IIf( CommandLineInfo.DevicesEnabled.Value, "Enabled", "Disabling" ) );
                    MySettings.Default.DevicesEnabled = CommandLineInfo.DevicesEnabled.Value;
                }

                if ( string.IsNullOrWhiteSpace( ComputerName ) )
                {
                    ComputerName = MyProject.Computer.Name;
                }

                return true;
            }
            catch ( Exception ex )
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
                this.SplashTraceEvent( TraceEventType.Error, "Exception occurred initializing application known state;. {0}", ex.ToFullBlownString() );
                try
                {
                    this.Destroy();
                }
                finally
                {
                }

                return false;
            }
            finally
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary>
        /// Processes the startup. Sets the event arguments
        /// <see cref="System.ComponentModel.CancelEventArgs.Cancel">cancel</see>
        /// value if failed.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        ///                  instance containing the event data. </param>
        private void ProcessStartup( Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e )
        {
            if ( e is object && !e.Cancel )
            {
                MySplashScreen.CreateInstance( MyProject.Application.SplashScreen );
                this.SplashTraceEvent( TraceEventType.Verbose, "Using splash panel." );
                this.SplashTraceEvent( TraceEventType.Verbose, "Parsing command line" );
                e.Cancel = !CommandLineInfo.TryParseCommandLine( e.CommandLine );
            }
        }

        /// <summary> Processes the shut down. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ProcessShutDown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            if ( MyProject.Application.SaveMySettingsOnExit )
            {
                Rig.My.MySettings.Default.Save();
            }
        }
    }
}
