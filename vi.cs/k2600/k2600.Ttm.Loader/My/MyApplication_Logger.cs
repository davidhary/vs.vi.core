﻿using System;
using System.Diagnostics;

using isr.Core;

namespace isr.VI.Tsp.K2600.Ttm.Loader.My
{
    internal partial class MyApplication
    {

        #region " LOG "

        /// <summary> The logger. </summary>
        private Logger _Logger;

        /// <summary> Gets the Logger. </summary>
        /// <value> The logger. </value>
        public Logger Logger
        {
            get {
                if ( this._Logger is null )
                    this.CreateLogger();
                return this._Logger;
            }
        }

        /// <summary> Creates the Logger. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void CreateLogger()
        {
            this._Logger = null;
            try
            {
                this._Logger = Logger.NewInstance( MyProject.Application.Info.ProductName );
                Microsoft.VisualBasic.Logging.FileLogTraceListener listener;
                listener = this.Logger.ReplaceDefaultTraceListener( true );
                if ( !this.Logger.LogFileExists )
                    _ = this.Logger.TraceEventOverride( ProductTimeTraceMessage() );

                // set the log for the application
                if ( !string.Equals( MyProject.Application.Log.DefaultFileLogWriter.FullLogFileName, listener.FullLogFileName, StringComparison.OrdinalIgnoreCase ) )
                {
                    MyProject.Application.Log.TraceSource.Listeners.Remove( CustomFileLogTraceListener.DefaultFileLogWriterName );
                    _ = MyProject.Application.Log.TraceSource.Listeners.Add( listener );
                    MyProject.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose;
                }

                // set the trace level.
                this.ApplyTraceLogLevel();
            }
            catch
            {
                if ( this._Logger is object )
                    this._Logger.Dispose();
                this._Logger = null;
                throw;
            }
        }

        /// <summary> Gets the trace level. </summary>
        /// <value> The trace level. </value>
        public TraceEventType TraceLevel { get; set; }

        #endregion

        #region " IDENTITY "

        /// <summary> Identifies this application. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public void Identify( ITraceMessageTalker talker )
        {
            _ = talker.PublishDateMessage( ProductTimeTraceMessage() );
            _ = talker.PublishDateMessage( this.LogTraceMessage() );
            talker.IdentifyTalker( this.IdentityTraceMessage() );
        }

        /// <summary> Gets the identity. </summary>
        /// <value> The identity. </value>
        public string Identity => $"{AssemblyProduct} ID = {TraceEventId:X}";

        /// <summary> Gets a message describing the identity trace. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage IdentityTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, TraceEventId, this.Identity );
        }

        /// <summary> Application log trace message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A TraceMessage. </returns>
        public TraceMessage LogTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, TraceEventId, $"Log at;. {this.Logger.FullLogFileName}" );
        }

        /// <summary> Product time trace message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A TraceMessage. </returns>
        public static TraceMessage ProductTimeTraceMessage()
        {
            return new TraceMessage( TraceEventType.Information, TraceEventId, Core.AssemblyExtensions.AssemblyExtensionMethods.BuildProductTimeCaption( MyProject.Application.Info ) );
        }

        #endregion

        #region " UNPUBLISHED MESSAGES "

        /// <summary> Gets or sets the unpublished identify date. </summary>
        /// <value> The unpublished identify date. </value>
        public DateTimeOffset UnpublishedIdentifyDate { get; set; }

        /// <summary> Gets or sets the unpublished trace messages. </summary>
        /// <value> The unpublished trace messages. </value>
        public TraceMessagesQueue UnpublishedTraceMessages { get; private set; } = new TraceMessagesQueue();

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        /// <returns> A String. </returns>
        public string LogUnpublishedMessage( TraceMessage message )
        {
            if ( message is null )
            {
                return string.Empty;
            }
            else
            {
                if ( DateTimeOffset.Now.Date > this.UnpublishedIdentifyDate )
                {
                    _ = this.Logger.TraceEventOverride( ProductTimeTraceMessage() );
                    _ = this.Logger.TraceEventOverride( this.LogTraceMessage() );
                    _ = this.Logger.TraceEventOverride( this.IdentityTraceMessage() );
                    this.UnpublishedIdentifyDate = DateTimeOffset.Now.Date;
                }

                this.UnpublishedTraceMessages.Enqueue( message );
                _ = this.Logger.TraceEvent( message );
                return message.Details;
            }
        }

        #endregion

    }
}