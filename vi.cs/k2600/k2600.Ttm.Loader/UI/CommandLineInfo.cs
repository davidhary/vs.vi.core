using System;
using System.Diagnostics;

namespace isr.VI.Tsp.K2600.Ttm.Loader
{

    /// <summary>
    /// A sealed class the parses the command line and provides the command line values.
    /// </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2011-02-02, x.x.4050.x. </para>
    /// </remarks>
    public sealed class CommandLineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private CommandLineInfo() : base()
        {
        }

        #endregion

        #region " PARSER "

        /// <summary> Gets the no-operation command line option. </summary>
        /// <value> The no operation option. </value>
        public static string NoOperationOption => "/nop";

        /// <summary> Gets the resource name command line option. </summary>
        /// <value> The resource name option. </value>
        public static string ResourceNameOption => "/r:";

        /// <summary> Gets the source measure unit name command line option. </summary>
        /// <value> The resource name option. </value>
        public static string SourceMeasureUnitNameOption => "/smu:";

        /// <summary> Gets the Device-Enabled option. </summary>
        /// <value> The Device enabled option. </value>
        public static string DevicesEnabledOption => "/d:";

        /// <summary> Validate the command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        ///                                      not handled. </exception>
        /// <param name="commandLineArguments"> The command line arguments. </param>
        public static void ValidateCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArguments )
        {
            if ( commandLineArguments is object )
            {
                foreach ( string argument in commandLineArguments )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( ResourceNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( SourceMeasureUnitNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else
                    {
                        Nop = argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase )
                            ? true
                            : throw new ArgumentException( string.Format( System.Globalization.CultureInfo.CurrentCulture, "Unknown command line argument '{0}' was detected. Should be Ignored.", argument ), nameof( commandLineArguments ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            if ( commandLineArgs is object )
            {
                foreach ( string argument in commandLineArgs )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        string value = argument.Substring( DevicesEnabledOption.Length );
                        DevicesEnabled = !value.StartsWith( "n", StringComparison.OrdinalIgnoreCase );
                    }
                    else if ( argument.StartsWith( ResourceNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        ResourceName = argument.Substring( ResourceNameOption.Length );
                    }
                    else if ( argument.StartsWith( SourceMeasureUnitNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        SourceMeasureUnitName = string.Format( "smu{0}", argument.Substring( ResourceNameOption.Length ) );
                    }
                    else if ( argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        Nop = true;
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        /// <returns> True if success or false if Exception occurred. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static bool TryParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            try
            {
                ParseCommandLine( commandLineArgs );
                return true;
            }
            catch ( ArgumentException ex )
            {
                My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, "Unknown argument ignored" );
                return true;
            }
            catch ( Exception ex )
            {
                if ( commandLineArgs is null )
                {
                    My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, "Failed parsing empty command line" );
                }
                else
                {
                    // #Disable Warning CA1825 ' Avoid zero-length array allocations.
                    var args = Array.Empty<string>();
                    // #Enable Warning CA1825 ' Avoid zero-length array allocations.
                    commandLineArgs.CopyTo( args, 0 );
                    My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, "Failed parsing command line '{0}'", string.Join( " ", args ) );
                }

                return false;
            }
        }

        #endregion

        #region " COMMAND LINE ELEMENTS "

        /// <summary> Gets or sets the source measure unit name. </summary>
        /// <value> The resource name. </value>
        public static string SourceMeasureUnitName { get; set; }

        /// <summary> Gets or sets the resource name. </summary>
        /// <value> The resource name. </value>
        public static string ResourceName { get; set; }

        /// <summary> Gets or sets a value indicating whether actual use of Device is enabled. </summary>
        /// <value>
        /// <c>null</c> if no value, <c>True</c> if Device are enabled; otherwise, <c>False</c>.
        /// </value>
        public static bool? DevicesEnabled { get; set; }

        /// <summary> Gets or sets the no operation option. </summary>
        /// <value> The nop. </value>
        public static bool Nop { get; set; }

        #endregion

    }
}
