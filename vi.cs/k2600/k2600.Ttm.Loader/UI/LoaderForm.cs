using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Loader
{

    /// <summary> Form for viewing the loader. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public class LoaderForm : Core.Forma.ConsoleForm
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public LoaderForm() : base()
        {
        }

        /// <summary> Gets or sets the model view base. </summary>
        /// <value> The model view base. </value>
        private Core.Forma.ModelViewTalkerBase ModelViewBase { get; set; } = null;

        /// <summary> Gets or sets the visa session. </summary>
        /// <value> The visa session. </value>
        private VisaSessionBase VisaSession { get; set; } = null;

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.VisaSession is object )
            {
                this.VisaSession.Dispose();
                this.VisaSession = null;
            }

            if ( this.ModelViewBase is object )
            {
                this.ModelViewBase.Dispose();
                this.ModelViewBase = null;
            }

            base.Dispose( disposing );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        ///                  event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if ( this.VisaSession is object )
                {
                    if ( this.VisaSession.CandidateResourceNameValidated )
                    {
                        My.MySettings.Default.ResourceName = this.VisaSession.ValidatedResourceName;
                    }

                    if ( !string.IsNullOrWhiteSpace( this.VisaSession.OpenResourceTitle ) )
                    {
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding Meter View";
                this.LoaderView = new Rig.Forms.LoaderView();
                this.ModelViewBase = this.LoaderView;
                this.AssignDeviceThis( new Rig.TspDevice() );
                this.AddTalkerControl( "TTM", this.ModelViewBase, false, false );
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener( My.MyProject.Application.Logger );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary> Gets or sets the await the resource name validation task enabled. </summary>
        /// <value> The await the resource name validation task enabled. </value>
        protected bool AwaitResourceNameValidationTaskEnabled { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.ModelViewBase.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
                this.LoaderView.SelectNavigatorTreeViewNode( this.LoaderView.ConnectorNodeName );
                if ( this.VisaSession is object && this.VisaSession.IsValidatingResourceName() )
                {
                    if ( this.AwaitResourceNameValidationTaskEnabled )
                    {
                        activity = $"{this.Name}; awaiting {this.VisaSession.CandidateResourceName} validation";
                        this.VisaSession.AwaitResourceNameValidation( TimeSpan.FromSeconds( 3d ) );
                    }
                    else
                    {
                        activity = $"{this.Name}; validating {this.VisaSession.CandidateResourceName}";
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.ModelViewBase is object )
                    this.ModelViewBase.Cursor = Cursors.Default;
            }
        }

        #region " DEVICE EVENTS "

        /// <summary> Gets or sets reference to the TSP device accessing the TTM Driver. </summary>
        /// <value> The tsp device. </value>
        public Rig.TspDevice TspDevice { get; private set; }

        /// <summary> Gets or sets the loader view. </summary>
        /// <value> The loader view. </value>
        private Rig.Forms.LoaderView LoaderView { get; set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( Rig.TspDevice value )
        {
            if ( this.TspDevice is object )
            {
                this.TspDevice.Opened -= this.DeviceOpened;
                this.AssignTalker( null );
                this.TspDevice = null;
                this.LoaderView?.AssignDevice( this.TspDevice );
            }

            this.TspDevice = value;
            if ( value is object )
            {
                // the talker control publishes the device messages which thus get published to the form message box.
                this.LoaderView.AssignDevice( this.TspDevice );
                this.LoaderView.ScriptManager.AuthorTitle = My.Resources.Resources.AuthorTitle;
                this.LoaderView.ScriptManager.BootFirmwareVersion = My.Resources.Resources.BootFirmwareVersion;
                this.LoaderView.ScriptManager.FirmwareReleasedVersion = My.Resources.Resources.MeterFirmwareVersion;
                this.LoaderView.ScriptManager.FrameworkName = My.Resources.Resources.FrameworkName;
                this.LoaderView.ScriptManager.FrameworkNamespace = My.Resources.Resources.FrameworkNamespace;
                this.LoaderView.ScriptManager.FrameworkTitle = My.Resources.Resources.FrameworkTitle;
                this.LoaderView.ScriptManager.MeterFirmwareVersion = My.Resources.Resources.MeterFirmwareVersion;
                this.LoaderView.ScriptManager.SupportFirmwareVersion = My.Resources.Resources.SupportFirmwareVersion;
                this.LoaderView.ScriptManager.LegacyScriptNames = My.Resources.Resources.LegacyScriptNames;
                this.LoaderView.ScriptManager.ScriptFilesFolderName = My.MySettings.Default.ScriptFilesFolderName;
                this.LoaderView.ScriptManager.FtpAddress = My.MySettings.Default.FtpAddress;
                this.LoaderView.ResourceName = My.MySettings.Default.ResourceName;
                this.VisaSession = this.LoaderView.VisaSessionBase;
                this.VisaSession.CandidateResourceName = My.MySettings.Default.ResourceName;
                this.VisaSession.CandidateResourceTitle = "TTM";
                if ( !string.IsNullOrWhiteSpace( this.VisaSession.CandidateResourceName ) )
                {
                    _ = this.PublishInfo( $"{this.Name}; starting {this.VisaSession.CandidateResourceName} selection task" );
                    _ = this.VisaSession.AsyncValidateResourceName( this.VisaSession.CandidateResourceName );
                }

                this.AssignTalker( this.TspDevice.Talker );
                this.TspDevice.Opened += this.DeviceOpened;
            }
        }

        /// <summary> Device opened. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void DeviceOpened( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "setting access system properties";
                this.TspDevice.AccessSubsystem.ScriptsFolderName = My.MySettings.Default.ScriptFilesFolderName;
                this.TspDevice.AccessSubsystem.ReleasedInstrumentsFileName = My.MySettings.Default.ReleasedInstrumentsFileName;
                this.TspDevice.AccessSubsystem.CertifiedInstrumentsFileName = My.MySettings.Default.CertifiedInstrumentsFileName;
                this.TspDevice.AccessSubsystem.SaltFileName = My.MySettings.Default.SaltFileName;
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyProject.Application.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
