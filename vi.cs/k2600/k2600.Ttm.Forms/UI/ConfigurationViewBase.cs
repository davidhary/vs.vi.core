using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Forma;
using isr.Core.NumericExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Panel for editing the configuration. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-02-25 </para>
    /// </remarks>
    public partial class ConfigurationViewBase : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected ConfigurationViewBase() : base()
        {
            this.InitializeComponent();
            this.ApplyConfigurationButtonCaption = this._ApplyConfigurationButton.Text;
            this.ApplyNewConfigurationButtonCaption = this._ApplyNewConfigurationButton.Text;
            this.__RestoreDefaultsButton.Name = "_RestoreDefaultsButton";
            this.__ApplyNewConfigurationButton.Name = "_ApplyNewConfigurationButton";
            this.__ApplyConfigurationButton.Name = "_ApplyConfigurationButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.ReleaseResources();
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " ON CONTROL EVENTS "

        /// <summary> Releases the resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected virtual void ReleaseResources()
        {
            this.DeviceUnderTest = null;
            this.PartInternal = null;
        }

        #endregion

        #region " PART "

        private DeviceUnderTest _PartInternal;

        private DeviceUnderTest PartInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PartInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PartInternal != null )
                {
                    this._PartInternal.PropertyChanged -= this.Part_PropertyChanged;
                }

                this._PartInternal = value;
                if ( this._PartInternal != null )
                {
                    this._PartInternal.PropertyChanged += this.Part_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the part. </summary>
        /// <value> The part. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DeviceUnderTest Part
        {
            get => this.PartInternal;

            set {
                this.PartInternal = value;
                if ( value is object )
                {
                    this.BindIntialResitanceControls();
                    this.BindThermalTransientControls();
                }
            }
        }

        /// <summary> Executes the property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( DeviceUnderTest sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.DeviceUnderTest.InitialResistance ):
                    {
                        this.OnConfigurationChanged();
                        break;
                    }

                case nameof( Ttm.DeviceUnderTest.FinalResistance ):
                    {
                        this.OnConfigurationChanged();
                        break;
                    }

                case nameof( Ttm.DeviceUnderTest.ShuntResistance ):
                    {
                        break;
                    }

                case nameof( Ttm.DeviceUnderTest.ThermalTransient ):
                    {
                        this.OnConfigurationChanged();
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _InitialResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Part_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling part {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.Part_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as DeviceUnderTest, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Bind controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void BindIntialResitanceControls()
        {

            // set the GUI based on the current defaults.
            var instrumentSettings = Ttm.My.MySettings.Default;
            this._CheckContactsCheckBox.DataBindings.Clear();
            this._CheckContactsCheckBox.DataBindings.Add( new Binding( "Checked", this.Part, "ContactCheckEnabled", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._CheckContactsCheckBox.Checked = Ttm.My.MySettings.Default.ContactCheckEnabled;
            this.Part.ContactCheckEnabled = this._CheckContactsCheckBox.Checked;
            Core.ApplianceBase.DoEvents();
            this._ColdVoltageNumeric.Minimum = instrumentSettings.ColdResistanceVoltageMinimum;
            this._ColdVoltageNumeric.Maximum = instrumentSettings.ColdResistanceVoltageMaximum;
            this._ColdVoltageNumeric.DataBindings.Clear();
            this._ColdVoltageNumeric.DataBindings.Add( new Binding( "Value", this.Part.InitialResistance, "VoltageLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ColdVoltageNumeric.Value = Ttm.My.MySettings.Default.ColdResistanceVoltageLimit.Clip( this._ColdVoltageNumeric.Minimum, this._ColdVoltageNumeric.Maximum );
            this.Part.InitialResistance.VoltageLimit = ( double ) this._ColdVoltageNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ColdCurrentNumeric.Minimum = instrumentSettings.ColdResistanceCurrentMinimum;
            this._ColdCurrentNumeric.Maximum = instrumentSettings.ColdResistanceCurrentMaximum;
            this._ColdCurrentNumeric.DataBindings.Clear();
            this._ColdCurrentNumeric.DataBindings.Add( new Binding( "Value", this.Part.InitialResistance, "CurrentLevel", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ColdCurrentNumeric.Value = Ttm.My.MySettings.Default.ColdResistanceCurrentLevel.Clip( this._ColdCurrentNumeric.Minimum, this._ColdCurrentNumeric.Maximum );
            this.Part.InitialResistance.CurrentLevel = ( double ) this._ColdCurrentNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ColdResistanceHighLimitNumeric.Minimum = instrumentSettings.ColdResistanceMinimum;
            this._ColdResistanceHighLimitNumeric.Maximum = instrumentSettings.ColdResistanceMaximum;
            this._ColdResistanceHighLimitNumeric.DataBindings.Clear();
            this._ColdResistanceHighLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.InitialResistance, "HighLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ColdResistanceHighLimitNumeric.Value = Ttm.My.MySettings.Default.ColdResistanceHighLimit.Clip( this._ColdResistanceHighLimitNumeric.Minimum, this._ColdResistanceHighLimitNumeric.Maximum );
            this.Part.InitialResistance.HighLimit = ( double ) this._ColdResistanceHighLimitNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ColdResistanceLowLimitNumeric.Minimum = instrumentSettings.ColdResistanceMinimum;
            this._ColdResistanceLowLimitNumeric.Maximum = instrumentSettings.ColdResistanceMaximum;
            this._ColdResistanceLowLimitNumeric.DataBindings.Clear();
            this._ColdResistanceLowLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.InitialResistance, "LowLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ColdResistanceLowLimitNumeric.Value = Ttm.My.MySettings.Default.ColdResistanceLowLimit.Clip( this._ColdResistanceLowLimitNumeric.Minimum, this._ColdResistanceLowLimitNumeric.Maximum );
            this.Part.InitialResistance.LowLimit = ( double ) this._ColdResistanceLowLimitNumeric.Value;
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Bind controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void BindThermalTransientControls()
        {

            // set the GUI based on the current defaults.
            Ttm.My.MySettings instrumentSettings = Ttm.My.MySettings.Default;
            this._PostTransientDelayNumeric.Minimum = instrumentSettings.PostTransientDelayMinimum;
            this._PostTransientDelayNumeric.Maximum = instrumentSettings.PostTransientDelayMaximum;
            this._PostTransientDelayNumeric.DataBindings.Clear();
            this._PostTransientDelayNumeric.DataBindings.Add( new Binding( "Value", this.Part.ThermalTransient, "PostTransientDelay", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._PostTransientDelayNumeric.Value = Ttm.My.MySettings.Default.PostTransientDelay.Clip( this._PostTransientDelayNumeric.Minimum, this._PostTransientDelayNumeric.Maximum );
            this.Part.ThermalTransient.PostTransientDelay = ( double ) this._PostTransientDelayNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ThermalVoltageNumeric.Minimum = instrumentSettings.ThermalTransientVoltageMinimum;
            this._ThermalVoltageNumeric.Maximum = instrumentSettings.ThermalTransientVoltageMaximum;
            this._ThermalVoltageNumeric.DataBindings.Clear();
            this._ThermalVoltageNumeric.DataBindings.Add( new Binding( "Value", this.Part.ThermalTransient, "AllowedVoltageChange", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ThermalVoltageNumeric.Value = Ttm.My.MySettings.Default.ThermalTransientVoltageChange.Clip( this._ThermalVoltageNumeric.Minimum, this._ThermalVoltageNumeric.Maximum );
            this.Part.ThermalTransient.AllowedVoltageChange = ( double ) this._ThermalVoltageNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ThermalCurrentNumeric.Minimum = instrumentSettings.ThermalTransientCurrentMinimum;
            this._ThermalCurrentNumeric.Maximum = instrumentSettings.ThermalTransientCurrentMaximum;
            this._ThermalCurrentNumeric.DataBindings.Clear();
            this._ThermalCurrentNumeric.DataBindings.Add( new Binding( "Value", this.Part.ThermalTransient, "CurrentLevel", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ThermalCurrentNumeric.Value = Ttm.My.MySettings.Default.ThermalTransientCurrentLevel.Clip( this._ThermalCurrentNumeric.Minimum, this._ThermalCurrentNumeric.Maximum );
            this.Part.ThermalTransient.CurrentLevel = ( double ) this._ThermalCurrentNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ThermalVoltageLimitNumeric.Minimum = instrumentSettings.ThermalTransientVoltageMinimum;
            this._ThermalVoltageLimitNumeric.Maximum = instrumentSettings.ThermalTransientVoltageMaximum;
            this._ThermalVoltageLimitNumeric.DataBindings.Clear();
            this._ThermalVoltageLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.ThermalTransient, "VoltageLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ThermalVoltageLimitNumeric.Value = Ttm.My.MySettings.Default.ThermalTransientVoltageLimit.Clip( this._ThermalVoltageLimitNumeric.Minimum, this._ThermalVoltageLimitNumeric.Maximum );
            this.Part.ThermalTransient.VoltageLimit = ( double ) this._ThermalVoltageLimitNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ThermalVoltageHighLimitNumeric.Minimum = instrumentSettings.ThermalTransientVoltageChangeMinimum;
            this._ThermalVoltageHighLimitNumeric.Maximum = instrumentSettings.ThermalTransientVoltageChangeMaximum;
            this._ThermalVoltageHighLimitNumeric.DataBindings.Clear();
            this._ThermalVoltageHighLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.ThermalTransient, "HighLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ThermalVoltageHighLimitNumeric.Value = Ttm.My.MySettings.Default.ThermalTransientHighLimit.Clip( this._ThermalVoltageHighLimitNumeric.Minimum, this._ThermalVoltageHighLimitNumeric.Maximum );
            this.Part.ThermalTransient.HighLimit = ( double ) this._ThermalVoltageHighLimitNumeric.Value;
            Core.ApplianceBase.DoEvents();
            this._ThermalVoltageLowLimitNumeric.Minimum = instrumentSettings.ThermalTransientVoltageChangeMinimum;
            this._ThermalVoltageLowLimitNumeric.Maximum = instrumentSettings.ThermalTransientVoltageChangeMaximum;
            this._ThermalVoltageLowLimitNumeric.DataBindings.Clear();
            this._ThermalVoltageLowLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.ThermalTransient, "LowLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ThermalVoltageLowLimitNumeric.Value = Ttm.My.MySettings.Default.ThermalTransientLowLimit.Clip( this._ThermalVoltageLowLimitNumeric.Minimum, this._ThermalVoltageLowLimitNumeric.Maximum );
            this.Part.ThermalTransient.LowLimit = ( double ) this._ThermalVoltageLowLimitNumeric.Value;
        }

        #endregion

        #region " DEVICE UNDER TEST "

        /// <summary> Gets or sets the device under test. </summary>
        /// <value> The device under test. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DeviceUnderTest DeviceUnderTest { get; set; }

        #endregion

        #region " CONFIGURE "

        /// <summary> Restore defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private static void RestoreDefaults()
        {
            Ttm.My.MySettings.Default.ContactCheckEnabled = Ttm.My.MySettings.Default.ContactCheckEnabledDefault;
            Ttm.My.MySettings.Default.ContactCheckThreshold = Ttm.My.MySettings.Default.ContactCheckThresholdDefault;
            Ttm.My.MySettings.Default.ColdResistanceVoltageLimit = Ttm.My.MySettings.Default.ColdResistanceVoltageLimitDefault;
            Ttm.My.MySettings.Default.ColdResistanceCurrentLevel = Ttm.My.MySettings.Default.ColdResistanceCurrentLevelDefault;
            Ttm.My.MySettings.Default.ColdResistanceHighLimit = Ttm.My.MySettings.Default.ColdResistanceHighLimitDefault;
            Ttm.My.MySettings.Default.ColdResistanceLowLimit = Ttm.My.MySettings.Default.ColdResistanceLowLimitDefault;
            Ttm.My.MySettings.Default.PostTransientDelay = Ttm.My.MySettings.Default.PostTransientDelayDefault;
            Ttm.My.MySettings.Default.ThermalTransientVoltageChange = Ttm.My.MySettings.Default.ThermalTransientVoltageChangeDefault;
            Ttm.My.MySettings.Default.ThermalTransientCurrentLevel = Ttm.My.MySettings.Default.ThermalTransientCurrentLevelDefault;
            Ttm.My.MySettings.Default.ThermalTransientHighLimit = Ttm.My.MySettings.Default.ThermalTransientHighLimitDefault;
            Ttm.My.MySettings.Default.ThermalTransientLowLimit = Ttm.My.MySettings.Default.ThermalTransientLowLimitDefault;
            Ttm.My.MySettings.Default.ThermalTransientVoltageLimit = Ttm.My.MySettings.Default.ThermalTransientVoltageLimitDefault;
        }

        /// <summary> Copy part values to the settings store. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void CopySettings()
        {
            if ( this.Part is object )
            {
                Ttm.My.MySettings.Default.SourceMeasureUnit = this.Part.InitialResistance.SourceMeasureUnit;
                Ttm.My.MySettings.Default.ContactCheckEnabled = this.Part.ContactCheckEnabled;
                Ttm.My.MySettings.Default.ContactCheckThreshold = this.Part.ContactCheckThreshold;
                Ttm.My.MySettings.Default.ColdResistanceVoltageLimit = ( decimal ) this.Part.InitialResistance.VoltageLimit;
                Ttm.My.MySettings.Default.ColdResistanceCurrentLevel = ( decimal ) this.Part.InitialResistance.CurrentLevel;
                Ttm.My.MySettings.Default.ColdResistanceHighLimit = ( decimal ) this.Part.InitialResistance.HighLimit;
                Ttm.My.MySettings.Default.ColdResistanceLowLimit = ( decimal ) this.Part.InitialResistance.LowLimit;
                Ttm.My.MySettings.Default.PostTransientDelay = ( decimal ) this.Part.ThermalTransient.PostTransientDelay;
                Ttm.My.MySettings.Default.ThermalTransientVoltageChange = ( decimal ) this.Part.ThermalTransient.AllowedVoltageChange;
                Ttm.My.MySettings.Default.ThermalTransientCurrentLevel = ( decimal ) this.Part.ThermalTransient.CurrentLevel;
                Ttm.My.MySettings.Default.ThermalTransientHighLimit = ( decimal ) this.Part.ThermalTransient.HighLimit;
                Ttm.My.MySettings.Default.ThermalTransientLowLimit = ( decimal ) this.Part.ThermalTransient.LowLimit;
                Ttm.My.MySettings.Default.ThermalTransientVoltageLimit = ( decimal ) this.Part.ThermalTransient.VoltageLimit;
            }
        }

        /// <summary> Gets or sets the apply new configuration button caption. </summary>
        /// <value> The apply new configuration button caption. </value>
        private string ApplyNewConfigurationButtonCaption { get; set; }

        /// <summary> Gets or sets the configuration button caption. </summary>
        /// <value> The apply configuration button caption. </value>
        private string ApplyConfigurationButtonCaption { get; set; }

        /// <summary> Checks if new configuration settings need to be applied. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// <c>True</c> if settings where updated so that meter settings needs to be updated.
        /// </returns>
        private bool IsNewConfigurationSettings()
        {
            return !this.Part.ThermalTransientConfigurationEquals( this.DeviceUnderTest );
        }

        /// <summary> True if new configuration setting is available, false if not. </summary>
        private bool _IsNewConfigurationSettingAvailable;

        /// <summary> Gets or sets the is new configuration setting available. </summary>
        /// <value> The is new configuration setting available. </value>
        public bool IsNewConfigurationSettingAvailable
        {
            get => this._IsNewConfigurationSettingAvailable;

            set {
                if ( !value.Equals( this.IsNewConfigurationSettingAvailable ) )
                {
                    this._IsNewConfigurationSettingAvailable = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Updates the configuration status. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void OnConfigurationChanged()
        {
            string caption = this.ApplyConfigurationButtonCaption;
            string changedCaption = this.ApplyNewConfigurationButtonCaption;
            this.IsNewConfigurationSettingAvailable = this.IsNewConfigurationSettings();
            if ( this.IsNewConfigurationSettingAvailable )
            {
                caption += " !";
                changedCaption += " !";
            }

            if ( !caption.Equals( this._ApplyConfigurationButton.Text ) )
            {
                this._ApplyConfigurationButton.Text = caption;
            }

            if ( !changedCaption.Equals( this._ApplyNewConfigurationButton.Text ) )
            {
                this._ApplyNewConfigurationButton.Text = changedCaption;
            }
        }

        /// <summary> Configures changed values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="part"> The Part. </param>
        protected virtual void ConfigureChanged( DeviceUnderTest part )
        {
        }

        /// <summary>
        /// Event handler. Called by _ApplyNewThermalTransientConfigurationButton for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyNewConfigurationButton_Click( object sender, EventArgs e )
        {
            if ( this.Part is null || this.DeviceUnderTest is null )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "Meter not connected" );
                _ = this.PublishWarning( "Meter not connected;. " );
                return;
            }

            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"Applying configuration";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Part.FinalResistance.CopyConfiguration( this.Part.InitialResistance );
                if ( !this.DeviceUnderTest.InfoConfigurationEquals( this.Part ) )
                {
                    // clear execution state is not required - done before the measurement.
                    // Me.Meter.ClearExecutionState()
                    activity = $"Configuring Thermal Transient";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.ConfigureChanged( this.Part );
                }
            }
            catch ( Core.OperationFailedException ex )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "failed configuring--most one configuration element did not succeed in setting the correct value. See messages for details." );
                _ = this.PublishException( activity, ex );
            }
            catch ( ArgumentException ex )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "failed configuring--most likely due to a validation issue. See messages for details." );
                _ = this.PublishException( activity, ex );
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "failed configuring thermal transient" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.OnConfigurationChanged();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Configures all values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="part"> The Part. </param>
        protected virtual void Configure( DeviceUnderTest part )
        {
        }

        /// <summary> Saves the configuration settings and sends them to the meter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyConfigurationButton_Click( object sender, EventArgs e )
        {
            if ( this.Part is null || this.DeviceUnderTest is null )
            {
                this.InfoProvider.SetError( this._ApplyConfigurationButton, "Meter not connected" );
                _ = this.PublishWarning( "Meter not connected;. " );
                return;
            }

            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"Applying configuration";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Part.FinalResistance.CopyConfiguration( this.Part.InitialResistance );
                // clear execution state is not required - done before the measurement.
                // Me.Meter.ClearExecutionState()
                _ = this.PublishVerbose( "Configuring Thermal Transient;. " );
                this.Configure( this.Part );
            }
            catch ( Core.OperationFailedException ex )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "failed configuring--most one configuration element did not succeed in setting the correct value. See messages for details." );
                _ = this.PublishException( activity, ex );
            }
            catch ( ArgumentException ex )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "failed configuring--most likely due to a validation issue. See messages for details." );
                _ = this.PublishException( activity, ex );
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._ApplyNewConfigurationButton, "failed configuring thermal transient" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.OnConfigurationChanged();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _RestoreDefaultsButton for click events. Restores the defaults
        /// settings.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RestoreDefaultsButton_Click( object sender, EventArgs e )
        {

            // read the instrument default settings.
            RestoreDefaults();

            // reset part to know state based on the current defaults
            this.Part.ResetKnownState();
            Core.ApplianceBase.DoEvents();

            // bind.
            this.BindIntialResitanceControls();
            this.BindThermalTransientControls();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
