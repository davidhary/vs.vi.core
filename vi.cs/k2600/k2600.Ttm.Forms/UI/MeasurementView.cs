using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Panel for editing the measurement. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-17 </para>
    /// </remarks>
    public partial class MeasurementView : MeasurementViewBase
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-09-04. </remarks>
        public MeasurementView()
        {
            this.InitializeComponent();
        }

        /// <summary> Releases the resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ReleaseResources()
        {
            this.Meter = null;
            base.ReleaseResources();
        }

        /// <summary> Gets the is device open. </summary>
        /// <value> The is device open. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public override bool IsDeviceOpen => this.Meter is not null && this.Meter.IsDeviceOpen;

        /// <summary> The meter. </summary>
        private Meter _Meter;

        /// <summary> Gets or sets the meter. </summary>
        /// <value> The meter. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public Meter Meter
        {
            get => this._Meter;

            set {
                this._Meter = value;
                if ( value is object )
                {
                    this.MasterDevice = this.Meter.MasterDevice;
                    this.TriggerSequencer = value.TriggerSequencer;
                    this.MeasureSequencer = value.MeasureSequencer;
                    this.OnStateChanged();
                }
            }
        }

        /// <summary> Abort Trigger Sequence if started. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void AbortTriggerSequenceIf()
        {
            _ = this.PublishVerbose( "Aborting measurements;. " );
            this.Meter.AbortTriggerSequenceIf();
        }

        /// <summary> Clears the part measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ClearPartMeasurements()
        {
            _ = this.PublishVerbose( "Clearing part measurements;. " );
            this.Meter.Part.ClearMeasurements();
        }

        /// <summary> Measure initial resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The measurement <see cref="MeasurementOutcomes">outcome</see>. </returns>
        protected override MeasurementOutcomes MeasureInitialResistance()
        {
            this.Meter.MeasureInitialResistance( this.Meter.Part.InitialResistance );
            return this.Meter.Part.Outcome;
        }

        /// <summary> Measure thermal transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The measurement <see cref="MeasurementOutcomes">outcome</see>. </returns>
        protected override MeasurementOutcomes MeasureThermalTransient()
        {
            this.Meter.MeasureThermalTransient( this.Meter.Part.ThermalTransient );
            return this.Meter.Part.Outcome;
        }

        /// <summary> Measure final resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The measurement <see cref="MeasurementOutcomes">outcome</see>. </returns>
        protected override MeasurementOutcomes MeasureFinalResistance()
        {
            this.Meter.MeasureFinalResistance( this.Meter.Part.FinalResistance );
            return this.Meter.Part.Outcome;
        }

        /// <summary> Displays a thermal transient trace described by chart. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The chart. </param>
        /// <returns> A list of trace values. </returns>
        protected override IList<System.Windows.Point> DisplayThermalTransientTrace( Chart chart )
        {
            this.ChartThermalTransientTrace( chart );
            return this.Meter.ThermalTransient.LastTimeSeries;
        }

        /// <summary> Models the thermal transient trace. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The chart. </param>
        protected override void ModelThermalTransientTrace( Chart chart )
        {
            this.Meter.ThermalTransient.ModelTransientResponse( this.Meter.Part.ThermalTransient );
            this.DisplayModel( chart );
        }

        #region " CHARTING "

        /// <summary> Displays a thermal transient trace. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The <see cref="System.Windows.Forms.DataVisualization.Charting.Chart">Chart</see>. </param>
        public void ChartThermalTransientTrace( Chart chart )
        {
            if ( this.Meter.ThermalTransient.NewTraceReadyToRead )
            {
                _ = this.PublishVerbose( "Reading trace;. " );
                var st = Stopwatch.StartNew();
                this.Meter.ThermalTransient.ReadThermalTransientTrace();
                var t = st.Elapsed;
                _ = this.PublishVerbose( "Read trace in {0} ms;. ", ( object ) t.TotalMilliseconds );
            }
            else
            {
                _ = this.PublishVerbose( "Displaying thermal transient trace;. " );
            }

            ConfigureTraceChart( chart );
            DisplayTrace( chart, this.Meter.ThermalTransient.LastTimeSeries );
        }

        /// <summary> Displays a trace described by timeSeries. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="chart"> The <see cref="System.Windows.Forms.DataVisualization.Charting.Chart">Chart</see>. </param>
        public void DisplayModel( Chart chart )
        {
            if ( chart is null )
                throw new ArgumentNullException( nameof( chart ) );
            if ( chart.Series.Count == 1 )
            {
                _ = chart.Series.Add( "VersionInfoBase.Model" );
                chart.Series[1].ChartType = SeriesChartType.Line;
            }

            var s = chart.Series[1];
            s.Points.SuspendUpdates();
            s.Points.Clear();
            s.Color = System.Drawing.Color.Chocolate;
            for ( int i = 0, loopTo = this.Meter.ThermalTransient.Model.FunctionValues().Count() - 1; i <= loopTo; i++ )
            {
                double x = this.Meter.ThermalTransient.LastTimeSeries[i].X;
                double y = this.Meter.ThermalTransient.Model.FunctionValues().ElementAtOrDefault( i );
                if ( x > 0d && y > 0d )
                {
                    _ = s.Points.AddXY( x, y );
                }
            }

            s.Points.ResumeUpdates();
            s.Points.Invalidate();
        }

        /// <summary> Configure trace chart. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The <see cref="System.Windows.Forms.DataVisualization.Charting.Chart">Chart</see>. </param>
        private static void ConfigureTraceChart( Chart chart )
        {
            if ( chart.ChartAreas.Count == 0 )
            {
                var chartArea = new ChartArea( "Area" );
                chart.ChartAreas.Add( chartArea );
                chartArea.AxisY.Title = "Voltage, mV";
                chartArea.AxisY.Minimum = 0d;
                chartArea.AxisY.TitleFont = new System.Drawing.Font( chart.Parent.Font, System.Drawing.FontStyle.Bold );
                chartArea.AxisX.Title = "Time, ms";
                chartArea.AxisX.Minimum = 0d;
                chartArea.AxisX.TitleFont = new System.Drawing.Font( chart.Parent.Font, System.Drawing.FontStyle.Bold );
                chartArea.AxisX.IsLabelAutoFit = true;
                chartArea.AxisX.LabelStyle.Format = "#.0";
                chartArea.AxisX.LabelStyle.Enabled = true;
                chartArea.AxisX.LabelStyle.IsEndLabelVisible = true;
                chart.Location = new System.Drawing.Point( 82, 16 );
                chart.Series.Add( new Series( "Trace" ) );
                var s = chart.Series["Trace"];
                s.SmartLabelStyle.Enabled = true;
                s.ChartArea = "Area";
                // s.Legend = "Legend"
                s.ChartType = SeriesChartType.Line;
                s.XAxisType = AxisType.Primary;
                s.YAxisType = AxisType.Primary;
                s.XValueType = ChartValueType.Double;
                s.YValueType = ChartValueType.Double;
                chart.Text = "Thermal Transient Trace";
                chart.Titles.Add( "Thermal Transient Trace" ).Font = new System.Drawing.Font( chart.Parent.Font.FontFamily, 11f, System.Drawing.FontStyle.Bold );
            }
        }

        /// <summary> Gets a simulate time series. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="asymptote">    The asymptote. </param>
        /// <param name="timeConstant"> The time constant. </param>
        /// <returns> null if it fails, else a list of. </returns>
        private static IList<System.Windows.Point> Simulate( double asymptote, double timeConstant )
        {
            var l = new List<System.Windows.Point>();
            double vc = 0d;
            double deltaT = 0.1d;
            for ( int i = 0; i <= 99; i++ )
            {
                double deltaV = deltaT * (asymptote - vc) / timeConstant;
                l.Add( new System.Windows.Point( ( float ) (deltaT * (i + 1)), ( float ) vc ) );
                vc += deltaV;
            }

            return l;
        }

        /// <summary> Displays a trace described by timeSeries. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart">      The <see cref="System.Windows.Forms.DataVisualization.Charting.Chart">Chart</see>. </param>
        /// <param name="timeSeries"> The time series in millivolts and milliseconds. </param>
        private static void DisplayTrace( Chart chart, IList<System.Windows.Point> timeSeries )
        {
            var s = chart.Series[0];
            s.Points.SuspendUpdates();
            s.Points.Clear();
            _ = s.Points.AddXY( 0d, 0d );
            double deltaT = 0d;
            if ( timeSeries.Count > 1 )
            {
                deltaT = timeSeries[1].X - timeSeries[0].X;
            }

            foreach ( System.Windows.Point v in timeSeries )
                _ = s.Points.AddXY( v.X, v.Y );
            // Add one more point
            _ = s.Points.AddXY( timeSeries[timeSeries.Count - 1].X + deltaT, 2d * timeSeries[timeSeries.Count - 1].Y - timeSeries[timeSeries.Count - 2].Y );
            s.Points.ResumeUpdates();
            s.Points.Invalidate();
        }

        /// <summary> Displays a thermal transient trace. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The <see cref="System.Windows.Forms.DataVisualization.Charting.Chart">Chart</see>. </param>
        public static void EmulateThermalTransientTrace( Chart chart )
        {
            ConfigureTraceChart( chart );
            DisplayTrace( chart, Simulate( 100d, 5d ) );
        }

        #endregion

    }
}
