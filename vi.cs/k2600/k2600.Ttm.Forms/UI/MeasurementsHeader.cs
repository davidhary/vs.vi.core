using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Measurements header. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-02-25 </para>
    /// </remarks>
    public partial class MeasurementsHeader : ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MeasurementsHeader() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._OutcomeTextBox.Text = string.Empty;
        }

        /// <summary> Releases the resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ReleaseResources()
        {
            this.InitialResistanceInternal = null;
            this.FinalResistanceInternal = null;
            this.ThermalTransientInternal = null;
            this.DeviceUnderTestInternal = null;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2021-09-08. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.ReleaseResources();
                    if ( disposing )
                    {
                        this.components?.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion

        #region " DUT "

        private DeviceUnderTest _DeviceUnderTestInternal;

        private DeviceUnderTest DeviceUnderTestInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceUnderTestInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceUnderTestInternal != null )
                {
                    this._DeviceUnderTestInternal.PropertyChanged -= this.DeviceUnderTest_PropertyChanged;
                }

                this._DeviceUnderTestInternal = value;
                if ( this._DeviceUnderTestInternal != null )
                {
                    this._DeviceUnderTestInternal.PropertyChanged += this.DeviceUnderTest_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the device under test. </summary>
        /// <value> The device under test. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DeviceUnderTest DeviceUnderTest
        {
            get => this.DeviceUnderTestInternal;

            set {
                this.DeviceUnderTestInternal = value;
                if ( value is null )
                {
                    this.InitialResistanceInternal = null;
                    this.FinalResistanceInternal = null;
                    this.ThermalTransientInternal = null;
                }
                else
                {
                    this.InitialResistanceInternal = value.InitialResistance;
                    this.FinalResistanceInternal = value.FinalResistance;
                    this.ThermalTransientInternal = value.ThermalTransient;
                }
            }
        }

        /// <summary> Handles the device under test property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( DeviceUnderTest sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.DeviceUnderTest.Outcome ):
                    {
                        this.OutcomeSetter( sender.Outcome );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _DeviceUnderTest for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeviceUnderTest_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DeviceUnderTest_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as DeviceUnderTest, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        /// <summary> Message describing the measurement. </summary>
        private string _MeasurementMessage;

        /// <summary> Gets or sets a message describing the measurement. </summary>
        /// <value> A message describing the measurement. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string MeasurementMessage
        {
            get => this._MeasurementMessage;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }
                else if ( (value ?? "") != (this.MeasurementMessage ?? "") )
                {
                    this._OutcomeTextBox.Text = value;
                }

                this._MeasurementMessage = value;
            }
        }

        /// <summary> Gets or sets the test <see cref="MeasurementOutcomes">outcome</see>. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void OutcomeSetter( MeasurementOutcomes value )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<MeasurementOutcomes>( this.OutcomeSetter ), new object[] { value } );
                return;
            }

            if ( value == MeasurementOutcomes.None )
            {
                this.MeasurementMessage = string.Empty;
                this._OutcomePictureBox.Visible = false;
            }
            else if ( (value & MeasurementOutcomes.MeasurementFailed) == 0 )
            {
                this.MeasurementMessage = "OKAY";
            }
            else if ( (value & MeasurementOutcomes.FailedContactCheck) != 0 )
            {
                this.MeasurementMessage = "CONTACTS";
            }
            else if ( (value & MeasurementOutcomes.HitCompliance) != 0 )
            {
                this.MeasurementMessage = "COMPLIANCE";
            }
            else if ( (value & MeasurementOutcomes.UnexpectedReadingFormat) != 0 )
            {
                this.MeasurementMessage = "READING ?";
            }
            else if ( (value & MeasurementOutcomes.UnexpectedOutcomeFormat) != 0 )
            {
                this.MeasurementMessage = "OUTCOME ?";
            }
            else if ( (value & MeasurementOutcomes.UnspecifiedStatusException) != 0 )
            {
                this.MeasurementMessage = "DEVICE";
            }
            else if ( (value & MeasurementOutcomes.UnspecifiedProgramFailure) != 0 )
            {
                this.MeasurementMessage = "PROGRAM";
            }
            else if ( (value & MeasurementOutcomes.MeasurementNotMade) != 0 )
            {
                this.MeasurementMessage = "PARTIAL";
            }
            else
            {
                this.MeasurementMessage = "FAILED";
                if ( (value & MeasurementOutcomes.UnknownOutcome) != 0 )
                {
                    Debug.Assert( !Debugger.IsAttached, "Unknown outcome" );
                }
            }

            if ( (value & MeasurementOutcomes.PartPassed) != 0 )
            {
                this._OutcomePictureBox.Visible = true;
                this._OutcomePictureBox.Image = My.Resources.Resources.Good;
            }
            else if ( (value & MeasurementOutcomes.PartFailed) != 0 )
            {
                this._OutcomePictureBox.Visible = true;
                this._OutcomePictureBox.Image = My.Resources.Resources.Bad;
            }
        }

        #endregion

        #region " DISPLAY ALERT "

        /// <summary> Shows the alerts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="show">   true to show, false to hide. </param>
        /// <param name="isGood"> true if this object is good. </param>
        public void ShowAlerts( bool show, bool isGood )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<bool, bool>( this.ShowAlerts ), new object[] { show, isGood } );
            }
            else
            {
                this._AlertsPictureBox.Image = show ? isGood ? My.Resources.Resources.Good : My.Resources.Resources.Bad : null;
            }
        }

        /// <summary> Shows the outcome. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="show">   true to show, false to hide. </param>
        /// <param name="isGood"> true if this object is good. </param>
        public void ShowOutcome( bool show, bool isGood )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<bool, bool>( this.ShowOutcome ), new object[] { show, isGood } );
            }
            else
            {
                this._OutcomePictureBox.Image = show ? isGood ? My.Resources.Resources.Good : My.Resources.Resources.Bad : null;
            }
        }

        #endregion

        #region " DISPLAY VALUE "

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void Clear()
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action( this.Clear ) );
            }
            else
            {
                this.InfoProvider.Clear();
                this.ShowOutcome( false, false );
                this.ShowAlerts( false, false );
                this._InitialResistanceTextBox.Text = string.Empty;
                this._FinalResistanceTextBox.Text = string.Empty;
                this._ThermalTransientVoltageTextBox.Text = string.Empty;
            }
        }

        /// <summary> Displays the thermal transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox">    The text box control. </param>
        /// <param name="resistance"> The resistance. </param>
        private void SetErrorProvider( TextBox textBox, ResistanceMeasureBase resistance )
        {
            if ( (resistance.Outcome & MeasurementOutcomes.PartFailed) != 0 )
            {
                this.InfoProvider.SetIconPadding( textBox, -this.InfoProvider.Icon.Width );
                this.InfoProvider.SetError( textBox, "Value out of range" );
            }
            else if ( (resistance.Outcome & MeasurementOutcomes.MeasurementFailed) != 0 )
            {
                this.InfoProvider.SetIconPadding( textBox, -this.InfoProvider.Icon.Width );
                this.InfoProvider.SetError( textBox, "Measurement failed" );
            }
            else if ( (resistance.Outcome & MeasurementOutcomes.MeasurementNotMade) != 0 )
            {
                this.InfoProvider.SetIconPadding( textBox, -this.InfoProvider.Icon.Width );
                this.InfoProvider.SetError( textBox, "Measurement not made" );
            }
            else
            {
                this.InfoProvider.SetError( textBox, "" );
            }
        }

        /// <summary> Displays the resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox"> The text box control. </param>
        private void ClearResistance( TextBox textBox )
        {
            if ( textBox is object )
            {
                if ( textBox.InvokeRequired )
                {
                    _ = textBox.Invoke( new Action<TextBox>( this.ClearResistance ), textBox );
                }
                else
                {
                    textBox.Text = string.Empty;
                    this.InfoProvider.SetError( textBox, "" );
                }
            }
        }

        /// <summary> Displays the resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox">    The text box control. </param>
        /// <param name="resistance"> The resistance. </param>
        private void ShowResistance( TextBox textBox, ResistanceMeasureBase resistance )
        {
            if ( textBox is object )
            {
                if ( textBox.InvokeRequired )
                {
                    _ = textBox.Invoke( new Action<TextBox, ResistanceMeasureBase>( this.ShowResistance ), new object[] { textBox, resistance } );
                }
                else
                {
                    textBox.Text = resistance.ResistanceCaption;
                    this.SetErrorProvider( textBox, resistance );
                }
            }
        }

        /// <summary> Displays the thermal transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox">    The text box control. </param>
        /// <param name="resistance"> The resistance. </param>
        private void ShowThermalTransient( TextBox textBox, ResistanceMeasureBase resistance )
        {
            if ( textBox is object )
            {
                if ( textBox.InvokeRequired )
                {
                    _ = textBox.Invoke( new Action<TextBox, ResistanceMeasureBase>( this.ShowThermalTransient ), new object[] { textBox, resistance } );
                }
                else
                {
                    textBox.Text = resistance.VoltageCaption;
                    this.SetErrorProvider( textBox, resistance );
                }
            }
        }

        #endregion

        #region " PART: INITIAL RESISTANCE "

        /// <summary> The Part Initial Resistance. </summary>
        private ColdResistance _InitialResistanceInternal;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private ColdResistance InitialResistanceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._InitialResistanceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._InitialResistanceInternal != null )
                {
                    this._InitialResistanceInternal.PropertyChanged -= this.InitialResistance_PropertyChanged;
                }

                this._InitialResistanceInternal = value;
                if ( this._InitialResistanceInternal != null )
                {
                    this._InitialResistanceInternal.PropertyChanged += this.InitialResistance_PropertyChanged;
                }
            }
        }

        /// <summary> Executes the initial resistance property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnInitialResistancePropertyChanged( ColdResistance sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ColdResistance.MeasurementAvailable ):
                    {
                        if ( sender.MeasurementAvailable )
                        {
                            this.ShowResistance( this._InitialResistanceTextBox, sender );
                        }

                        break;
                    }

                case nameof( ColdResistance.LastReading ):
                    {
                        if ( string.IsNullOrWhiteSpace( sender.LastReading ) )
                        {
                            this.ClearResistance( this._InitialResistanceTextBox );
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _InitialResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitialResistance_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.InitialResistance_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnInitialResistancePropertyChanged( sender as ColdResistance, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        #endregion

        #region " PART: FINAL RESISTANCE "

        /// <summary> The Part Final Resistance. </summary>
        private ColdResistance _FinalResistanceInternal;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private ColdResistance FinalResistanceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._FinalResistanceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._FinalResistanceInternal != null )
                {
                    this._FinalResistanceInternal.PropertyChanged -= this.FinalResistance_PropertyChanged;
                }

                this._FinalResistanceInternal = value;
                if ( this._FinalResistanceInternal != null )
                {
                    this._FinalResistanceInternal.PropertyChanged += this.FinalResistance_PropertyChanged;
                }
            }
        }
        // #Enable Warning IDE1006 ' Naming Styles

        /// <summary> Executes the Final resistance property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnFinalResistancePropertyChanged( ColdResistance sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ColdResistance.MeasurementAvailable ):
                    {
                        if ( sender.MeasurementAvailable )
                        {
                            this.ShowResistance( this._FinalResistanceTextBox, sender );
                        }

                        break;
                    }

                case nameof( ColdResistance.LastReading ):
                    {
                        if ( string.IsNullOrWhiteSpace( sender.LastReading ) )
                        {
                            this.ClearResistance( this._FinalResistanceTextBox );
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _FinalResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FinalResistance_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.FinalResistance_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnFinalResistancePropertyChanged( sender as ColdResistance, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }


        #endregion

        #region " PART: THERMAL TRANSIENT "

        /// <summary> The Part Thermal Transient. </summary>
        private ThermalTransient _ThermalTransientInternal;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private ThermalTransient ThermalTransientInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ThermalTransientInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ThermalTransientInternal != null )
                {
                    this._ThermalTransientInternal.PropertyChanged -= this.ThermalTransient_PropertyChanged;
                }

                this._ThermalTransientInternal = value;
                if ( this._ThermalTransientInternal != null )
                {
                    this._ThermalTransientInternal.PropertyChanged += this.ThermalTransient_PropertyChanged;
                }
            }
        }

        /// <summary> Executes the initial resistance property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( ResistanceMeasureBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ResistanceMeasureBase.MeasurementAvailable ):
                    {
                        if ( sender.MeasurementAvailable )
                        {
                            this.ShowThermalTransient( this._ThermalTransientVoltageTextBox, sender );
                        }

                        break;
                    }

                case nameof( ResistanceMeasureBase.LastReading ):
                    {
                        if ( string.IsNullOrWhiteSpace( sender.LastReading ) )
                        {
                            this.ClearResistance( this._ThermalTransientVoltageTextBox );
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _ThermalTransient for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalTransient_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ThermalTransient_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as ResistanceMeasureBase, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        #endregion

    }
}
