using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Part header. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-12 </para>
    /// </remarks>
    public partial class PartHeader : ModelViewBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public PartHeader() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._PartNumberTextBox.Text = string.Empty;
            this._PartSerialNumberTextBox.Text = string.Empty;
        }

        /// <summary> Releases the resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ReleaseResources()
        {
            this.DeviceUnderTestInternal = null;
        }

        /// <summary>   UserControl overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-09-07. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.ReleaseResources();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }


        #endregion

        #region " DUT "

        private DeviceUnderTest _DeviceUnderTestInternal;

        private DeviceUnderTest DeviceUnderTestInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceUnderTestInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceUnderTestInternal != null )
                {
                    this._DeviceUnderTestInternal.PropertyChanged -= this.DeviceUnderTest_PropertyChanged;
                }

                this._DeviceUnderTestInternal = value;
                if ( this._DeviceUnderTestInternal != null )
                {
                    this._DeviceUnderTestInternal.PropertyChanged += this.DeviceUnderTest_PropertyChanged;
                }
            }
        }
        // #Enable Warning IDE1006 ' Naming Styles

        /// <summary> Gets or sets the device under test. </summary>
        /// <value> The device under test. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DeviceUnderTest DeviceUnderTest
        {
            get => this.DeviceUnderTestInternal;

            set => this.DeviceUnderTestInternal = value;
        }

        /// <summary> Executes the property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( DeviceUnderTest sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.DeviceUnderTest.PartNumber ):
                    {
                        if ( string.IsNullOrWhiteSpace( sender.PartNumber ) )
                        {
                            this.Visible = false;
                        }
                        else
                        {
                            this.Visible = true;
                            this._PartNumberTextBox.Text = sender.PartNumber;
                        }

                        break;
                    }

                case nameof( Ttm.DeviceUnderTest.SerialNumber ):
                    {
                        this._PartSerialNumberTextBox.Text = sender.SerialNumber.ToString();
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _DeviceUnderTest for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeviceUnderTest_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DeviceUnderTest_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as DeviceUnderTest, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        #endregion

    }
}
