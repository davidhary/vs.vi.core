using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Panel for editing the parts. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-12 </para>
    /// </remarks>
    public partial class PartsView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public PartsView() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.OnPartOutcomeChanged();
            this.PartsInternal = new DeviceUnderTestCollection();
            this.ConfigurePartsDisplay( this._PartsDataGridView );
            this.OnPartsChanged( this.PartsInternal, EventArgs.Empty );
            this.__AddPartToolStripButton.Name = "_AddPartToolStripButton";
            this.__ClearPartListToolStripButton.Name = "_ClearPartListToolStripButton";
            this.__SavePartsToolStripButton.Name = "_SavePartsToolStripButton";
            this.__PartsDataGridView.Name = "_PartsDataGridView";

#if designMode 
            Debug.Assert( False, "Illegal call; reset design mode" )
#endif
        }

        /// <summary> Release resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ReleaseResources()
        {
            this.PartInternal = null;
            if ( this.Parts() is object )
            {
                this.PartsInternal.Clear();
                this.PartsInternal = null;
            }
        }

#endregion

#region " DEVICE UNDER TEST "

        private DeviceUnderTest _PartInternal;

        private DeviceUnderTest PartInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PartInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PartInternal != null )
                {
                    this._PartInternal.PropertyChanged -= this.Part_PropertyChanged;
                }

                this._PartInternal = value;
                if ( this._PartInternal != null )
                {
                    this._PartInternal.PropertyChanged += this.Part_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the part. </summary>
        /// <value> The part. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DeviceUnderTest Part
        {
            get => this.PartInternal;

            set => this.PartInternal = value;
        }

        /// <summary> Update outcome display and enable adding part. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void OnPartOutcomeChanged()
        {
            if ( this.Part is null )
            {
                this.OutcomeSetter( MeasurementOutcomes.None );
                this._ClearMeasurementsToolStripMenuItem.Enabled = false;
                this._AddPartToolStripButton.Enabled = false;
            }
            else
            {
                this.OutcomeSetter( this.Part.Outcome );
                this._AddPartToolStripButton.Enabled = this.Part.AllMeasurementsAvailable && (this.Part.Outcome & MeasurementOutcomes.FailedContactCheck) == 0 && (this.Part.Outcome & MeasurementOutcomes.MeasurementFailed) == 0 && (this.Part.Outcome & MeasurementOutcomes.MeasurementNotMade) == 0;
                this._ClearMeasurementsToolStripMenuItem.Enabled = this.Part.AnyMeasurementsAvailable;
            }
        }

        /// <summary> Executes the part property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPartPropertyChanged( DeviceUnderTest sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( DeviceUnderTest.AnyMeasurementsAvailable ):
                    {
                        this.OnPartOutcomeChanged();
                        break;
                    }

                case nameof( DeviceUnderTest.AllMeasurementsAvailable ):
                    {
                        this.OnPartOutcomeChanged();
                        break;
                    }

                case nameof( DeviceUnderTest.LotId ):
                    {
                        this._LotToolStripTextBox.Text = this.Part.LotId;
                        break;
                    }

                case nameof( DeviceUnderTest.Outcome ):
                    {
                        this.OnPartOutcomeChanged();
                        break;
                    }

                case nameof( DeviceUnderTest.PartNumber ):
                    {
                        this._PartNumberToolStripTextBox.Text = this.Part.PartNumber.ToString();
                        break;
                    }

                case nameof( DeviceUnderTest.OperatorId ):
                    {
                        this._OperatorToolStripTextBox.Text = this.Part.OperatorId;
                        break;
                    }

                case nameof( DeviceUnderTest.SerialNumber ):
                    {
                        this._SerialNumberToolStripTextBox.Text = this.Part.SerialNumber.ToString();
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _Part for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Part_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling part {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.Part_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPartPropertyChanged( sender as DeviceUnderTest, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Message describing the measurement. </summary>
        private string _MeasurementMessage;

        /// <summary> Gets or sets a message describing the measurement. </summary>
        /// <value> A message describing the measurement. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string MeasurementMessage
        {
            get => this._MeasurementMessage;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = string.Empty;
                }
                else if ( (value ?? "") != (this.MeasurementMessage ?? "") )
                {
                    this._OutcomeToolStripLabel.Text = value;
                }

                this._MeasurementMessage = value;
            }
        }

        /// <summary> Gets or sets the test <see cref="MeasurementOutcomes">outcome</see>. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void OutcomeSetter( MeasurementOutcomes value )
        {
            if ( value == MeasurementOutcomes.None )
            {
                this.MeasurementMessage = string.Empty;
                this._PassFailToolStripButton.Visible = false;
            }
            else if ( (value & MeasurementOutcomes.MeasurementFailed) == 0 )
            {
                this.MeasurementMessage = "OKAY";
            }
            else if ( (value & MeasurementOutcomes.FailedContactCheck) != 0 )
            {
                this.MeasurementMessage = "CONTACTS";
            }
            else if ( (value & MeasurementOutcomes.HitCompliance) != 0 )
            {
                this.MeasurementMessage = "COMPLIANCE";
            }
            else if ( (value & MeasurementOutcomes.UnexpectedReadingFormat) != 0 )
            {
                this.MeasurementMessage = "READING FORMAT";
            }
            else if ( (value & MeasurementOutcomes.UnexpectedOutcomeFormat) != 0 )
            {
                this.MeasurementMessage = "OUTCOME FORMAT";
            }
            else if ( (value & MeasurementOutcomes.UnspecifiedStatusException) != 0 )
            {
                this.MeasurementMessage = "DEVICE";
            }
            else if ( (value & MeasurementOutcomes.UnspecifiedProgramFailure) != 0 )
            {
                this.MeasurementMessage = "PROGRAM";
            }
            else if ( (value & MeasurementOutcomes.MeasurementNotMade) != 0 )
            {
                this.MeasurementMessage = "PARTIAL";
            }
            else
            {
                this.MeasurementMessage = "FAILED";
                if ( (value & MeasurementOutcomes.UnknownOutcome) != 0 )
                {
                    Debug.Assert( !Debugger.IsAttached, "Unknown outcome" );
                }
            }

            if ( (value & MeasurementOutcomes.PartPassed) != 0 )
            {
                this._PassFailToolStripButton.Visible = true;
                this._PassFailToolStripButton.Image = My.Resources.Resources.Good;
            }
            else if ( (value & MeasurementOutcomes.PartFailed) != 0 )
            {
                this._PassFailToolStripButton.Visible = true;
                this._PassFailToolStripButton.Image = My.Resources.Resources.Bad;
            }
        }

#region " SETTINGS AND BINDING "

        /// <summary> Copy panel values to the settings store. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void CopySettings()
        {
            Ttm.My.MySettings.Default.AutomaticallyAddPart = this._AutoAddToolStripMenuItem.Checked;
            string value = this._PartNumberToolStripTextBox.Text.Trim();
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                Ttm.My.MySettings.Default.PartNumber = value;
            }

            value = this._LotToolStripTextBox.Text.Trim();
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                Ttm.My.MySettings.Default.LotNumber = value;
            }

            value = this._OperatorToolStripTextBox.Text.Trim();
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                Ttm.My.MySettings.Default.OperatorId = value;
            }
        }

        /// <summary> Applies settings to the controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ApplySettings()
        {
            this._AutoAddToolStripMenuItem.Checked = Ttm.My.MySettings.Default.AutomaticallyAddPart;
            this.Part.PartNumber = Ttm.My.MySettings.Default.PartNumber;
            this._PartNumberToolStripTextBox.Text = Ttm.My.MySettings.Default.PartNumber;
            this.Part.LotId = Ttm.My.MySettings.Default.LotNumber;
            this._LotToolStripTextBox.Text = Ttm.My.MySettings.Default.LotNumber;
            this.Part.OperatorId = Ttm.My.MySettings.Default.OperatorId;
            this._OperatorToolStripTextBox.Text = Ttm.My.MySettings.Default.OperatorId;
            this._SerialNumberToolStripTextBox.Text = this.Part.SerialNumber.ToString();
        }

#endregion

#endregion

#region " PARTS LIST MANAGEMENT "

        private DeviceUnderTestCollection _PartsInternal;

        private DeviceUnderTestCollection PartsInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._PartsInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._PartsInternal != null )
                {

                    this._PartsInternal.CollectionChanged -= this.OnPartsChanged;
                }

                this._PartsInternal = value;
                if ( this._PartsInternal != null )
                {
                    this._PartsInternal.CollectionChanged += this.OnPartsChanged;
                }
            }
        }

        /// <summary> Gets the parts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A DeviceUnderTestCollection. </returns>
        public DeviceUnderTestCollection Parts()
        {
            return this.PartsInternal;
        }

        /// <summary>   Raises the parts changed event. </summary>
        /// <remarks>   David, 2021-09-04. </remarks>
        /// <param name="sender">   Specifies the object where the call originated. </param>
        /// <param name="e">        Event information to send to registered event handlers. </param>
        protected void OnPartsChanged( object sender, EventArgs e )
        {
            // Me.ConfigurePartsDisplay(Me._PartsDataGridView)
            this._PartsBindingSource.ResetBindings( false );
            this._AddPartToolStripButton.Enabled = this._AddPartToolStripButton.Enabled && this.Parts() is object && this.Parts().Any();
            this._ClearPartListToolStripButton.Enabled = this.Parts() is object && this.Parts().Any();
            this._SavePartsToolStripButton.Enabled = this.Parts() is object && this.Parts().Any();
        }

        /// <summary> Clears the list of measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ClearParts()
        {
            _ = this.PublishVerbose( "Clearing parts;. " );
            if ( this.Parts() is null )
            {
                this.PartsInternal = new DeviceUnderTestCollection();
            }

            this.Parts().Clear();
            _ = this.PublishVerbose( "Part list cleared;. " );
        }

        /// <summary> Gets the automatic add enabled. </summary>
        /// <value> The automatic add enabled. </value>
        public bool AutoAddEnabled => this._AddPartToolStripButton.Enabled && this._AutoAddToolStripMenuItem.Checked;

        /// <summary> Adds the current part to the list of measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void AddPart()
        {
            if ( this.Part.AllMeasurementsMade() )
            {
                if ( string.IsNullOrEmpty( this._PartNumberToolStripTextBox.Text.Trim() ) )
                {
                    _ = this.PublishWarning( "Enter part number;. " );
                }
                else if ( string.IsNullOrEmpty( this._LotToolStripTextBox.Text.Trim() ) )
                {
                    _ = this.PublishWarning( "Enter lot number;. " );
                }
                else if ( string.IsNullOrEmpty( this._OperatorToolStripTextBox.Text.Trim() ) )
                {
                    _ = this.PublishWarning( "Enter operator id;. " );
                }
                else
                {
                    this.CopySettings();
                    this.Part.PartNumber = this._PartNumberToolStripTextBox.Text.Trim();
                    this.Part.LotId = this._LotToolStripTextBox.Text.Trim();
                    this.Part.OperatorId = this._OperatorToolStripTextBox.Text.Trim();
                    bool localTryParse() {
                        _ = this.Part.SerialNumber;
                        var ret = int.TryParse( this._SerialNumberToolStripTextBox.Text.Trim(), out int argresult ); this.Part.SerialNumber = argresult; return ret; }

                    if ( !localTryParse() )
                    {
                        this.Part.SerialNumber += 1;
                    }

                    _ = this.PublishVerbose( "Setting part sample number to '{0}';. ", ( object ) (this.Parts().Count + 1) );
                    this.Part.SampleNumber = this.Parts().Count + 1;
                    _ = this.PublishVerbose( "Adding part '{0}';. ", this.Part.UniqueKey );
                    this.Parts().Add( new DeviceUnderTest( this.Part ) );
                    _ = this.PublishVerbose( "Part '{0}' added;. ", this.Part.UniqueKey );
                }
            }
            else
            {
                _ = this.PublishInfo( "Part has incomplete data;. Outcome: {0)", this.Part.Outcome.ToString() );
            }
        }

        /// <summary> Sets the default file path in case the data folder does not exist. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> System.String. </returns>
        private static string UpdateDefaultFilePath( string filePath )
        {

            // check validity of data folder.
            string dataFolder = System.IO.Path.GetDirectoryName( filePath );
            if ( !System.IO.Directory.Exists( dataFolder ) )
            {
                if ( Debugger.IsAttached )
                {

                    // in design mode use the application folder.
                    dataFolder = My.MyProject.Application.Info.DirectoryPath;
                    dataFolder = System.IO.Path.Combine( dataFolder, @"..\Measurements" );
                    dataFolder = System.IO.Directory.Exists( dataFolder ) ? System.IO.Path.GetDirectoryName( dataFolder ) : System.IO.Directory.CreateDirectory( dataFolder ).FullName;
                }
                else
                {

                    // in run time use the documents folder.
                    dataFolder = My.MyProject.Computer.FileSystem.SpecialDirectories.MyDocuments;
                    dataFolder = System.IO.Path.Combine( dataFolder, "TTM" );
                    if ( !System.IO.Directory.Exists( dataFolder ) )
                    {
                        _ = System.IO.Directory.CreateDirectory( dataFolder );
                    }

                    dataFolder = System.IO.Path.Combine( dataFolder, "Measurements" );
                    if ( !System.IO.Directory.Exists( dataFolder ) )
                    {
                        _ = System.IO.Directory.CreateDirectory( dataFolder );
                    }
                }
            }

            return System.IO.Path.Combine( dataFolder, System.IO.Path.GetFileName( filePath ) );
        }

        /// <summary> Gets a new file for storing the data. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> A file name or empty if error. </returns>
        private static string BrowseForFile( string filePath )
        {

            // make sure the default data file name is valid.
            filePath = UpdateDefaultFilePath( filePath );
            using var dialog = new SaveFileDialog();
            dialog.CheckFileExists = false;
            dialog.CheckPathExists = true;
            dialog.DefaultExt = ".xls";
            dialog.FileName = filePath;
            dialog.Filter = "Comma Separated Values (*.csv)|*.csv|All Files (*.*)|*.*";
            dialog.FilterIndex = 0;
            dialog.InitialDirectory = System.IO.Path.GetDirectoryName( filePath );
            dialog.RestoreDirectory = true;
            dialog.Title = "Select a Comma-Separated File";

            // Open the Open dialog
            if ( dialog.ShowDialog() == DialogResult.OK )
            {

                // if file selected,
                return dialog.FileName;
            }
            else
            {

                // if some error, just ignore
                return string.Empty;
            }
        }

        /// <summary> Gets the save enabled. </summary>
        /// <value> The save enabled. </value>
        public bool SaveEnabled => this._SavePartsToolStripButton.Enabled;

        /// <summary> Saves data for all parts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void SaveParts()
        {
            if ( this.Parts() is object && this.Parts().Any() )
            {
                _ = this.PublishVerbose( "Browsing for file;. " );
                string filePath = PartsView.BrowseForFile( Ttm.My.MySettings.Default.PartsFilePath );
                if ( string.IsNullOrWhiteSpace( filePath ) )
                {
                    return;
                }

                Ttm.My.MySettings.Default.PartsFilePath = filePath;
                string activity = string.Empty;
                try
                {
                    activity = $"Saving...;. data to '{filePath}'";
                    _ = this.PublishVerbose( activity );
                    using ( var writer = new System.IO.StreamWriter( filePath, false ) )
                    {
                        foreach ( DeviceUnderTest part in this.Parts() )
                        {
                            if ( part.SampleNumber == 1 )
                            {
                                writer.WriteLine( "\"TTM measurements\"" );
                                writer.WriteLine( "\"Part number\",\"{0}\"", part.PartNumber );
                                writer.WriteLine( "\"Lot number\",\"{0}\"", part.LotId );
                                writer.WriteLine( "\"Operator Id\",\"{0}\"", part.OperatorId );
                                writer.WriteLine( DeviceUnderTest.DataHeader );
                            }

                            writer.WriteLine( part.DataRecord );
                        }

                        writer.Flush();
                    }

                    _ = this.PublishVerbose( "Done saving part number {0};. ", this.Part.PartNumber );
                    this._SavePartsToolStripButton.Enabled = false;
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                    this.InfoProvider.SetError( this._PartsListToolStrip, $"Exception {activity}" );
                }
                finally
                {
                }
            }
        }

        /// <summary> Event handler. Called by _AddPartToolStripButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        private void AddPartToolStripButton_Click( object sender, EventArgs e )
        {
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            this.InfoProvider.SetError( this._PartsListToolStrip, "" );
            this.AddPart();
        }

        /// <summary> Event handler. Called by _CLearPartListToolStripButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        private void ClearPartListToolStripButton_Click( object sender, EventArgs e )
        {
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            this.InfoProvider.SetError( this._PartsListToolStrip, "" );
            this.ClearParts();
        }

        /// <summary> Event handler. Called by _SavePartsToolStripButton for click events.3. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        private void SavePartsToolStripButton_Click( object sender, EventArgs e )
        {
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            this.InfoProvider.SetError( this._PartsListToolStrip, "" );
            this.SaveParts();
        }

#endregion

#region " PARTS DISPLAY "

        /// <summary> Event handler. Called by _PartsDataGridView for data error events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Data grid view data error event information. </param>
        private void PartsDataGridView_DataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            if ( e is object )
            {
                e.Cancel = false;
            }
        }

        /// <summary> The binder. </summary>
        private BindingSource _PartsBindingSource;

        /// <summary> Configures parts display. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid"> The grid. </param>
        public void ConfigurePartsDisplay( DataGridView grid )
        {
            if ( grid is null )
                throw new ArgumentNullException( nameof( grid ) );
            this._PartsBindingSource = new BindingSource() { DataSource = this.Parts() };
            grid.Enabled = false;
            grid.Columns.Clear();
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSalmon;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            grid.AllowUserToAddRows = false;
            grid.RowHeadersVisible = false;
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            grid.AutoGenerateColumns = false;
            grid.ReadOnly = true;
            grid.DataSource = this._PartsBindingSource;
            grid.Refresh();
            int displayIndex = 0;
            DataGridViewTextBoxColumn column;
            column = new DataGridViewTextBoxColumn() {
                DataPropertyName = "SampleNumber",
                Name = "SampleNumber",
                Visible = true,
                HeaderText = " # ",
                DisplayIndex = displayIndex
            };
            displayIndex += 1;
            column.Width = 30;
            _ = grid.Columns.Add( column );
            column = new DataGridViewTextBoxColumn() {
                DataPropertyName = "SerialNumber",
                Name = "SerialNumber",
                Visible = true,
                HeaderText = " S/N ",
                DisplayIndex = displayIndex
            };
            displayIndex += 1;
            _ = grid.Columns.Add( column );
            column = new DataGridViewTextBoxColumn() {
                DataPropertyName = "Timestamp",
                Name = "Timestamp",
                Visible = true,
                HeaderText = " Time ",
                DisplayIndex = displayIndex
            };
            displayIndex += 1;
            _ = grid.Columns.Add( column );
            column = new DataGridViewTextBoxColumn() {
                DataPropertyName = "InitialResistanceCaption",
                Name = "InitialResistanceCaption",
                Visible = true,
                HeaderText = " Initial ",
                DisplayIndex = displayIndex
            };
            displayIndex += 1;
            _ = grid.Columns.Add( column );
            column = new DataGridViewTextBoxColumn() {
                DataPropertyName = "FinalResistanceCaption",
                Name = "FinalResistanceCaption",
                Visible = true,
                HeaderText = " Final ",
                DisplayIndex = displayIndex
            };
            displayIndex += 1;
            _ = grid.Columns.Add( column );
            column = new DataGridViewTextBoxColumn() {
                DataPropertyName = "ThermalTransientVoltageCaption",
                Name = "ThermalTransientVoltageCaption",
                Visible = true,
                HeaderText = " Transient ",
                DisplayIndex = displayIndex
            };
            _ = grid.Columns.Add( column );
            grid.Enabled = true;
        }

#endregion

#region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

#endregion

    }
}
