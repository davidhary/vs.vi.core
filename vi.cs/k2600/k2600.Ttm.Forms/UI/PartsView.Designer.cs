using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class PartsView
    {

        /// <summary>   UserControl overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-09-07. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    ReleaseResources();
                    if (components is object)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(PartsView));
            _PartsListToolStrip = new System.Windows.Forms.ToolStrip();
            __AddPartToolStripButton = new System.Windows.Forms.ToolStripButton();
            __AddPartToolStripButton.Click += new EventHandler(AddPartToolStripButton_Click);
            __ClearPartListToolStripButton = new System.Windows.Forms.ToolStripButton();
            __ClearPartListToolStripButton.Click += new EventHandler(ClearPartListToolStripButton_Click);
            __SavePartsToolStripButton = new System.Windows.Forms.ToolStripButton();
            __SavePartsToolStripButton.Click += new EventHandler(SavePartsToolStripButton_Click);
            _PartNumberToolStrip = new System.Windows.Forms.ToolStrip();
            _OperatorLabel = new System.Windows.Forms.ToolStripLabel();
            _OperatorToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            _LotToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _LotToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            _PartNumberToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _PartNumberToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            _ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            _PartToolStrip = new System.Windows.Forms.ToolStrip();
            _SerialNumberToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _SerialNumberToolStripTextBox = new System.Windows.Forms.ToolStripTextBox();
            _ToolStripSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            _AutoAddToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ClearMeasurementsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            _OutcomeToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _PassFailToolStripButton = new System.Windows.Forms.ToolStripButton();
            __PartsDataGridView = new System.Windows.Forms.DataGridView();
            __PartsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(PartsDataGridView_DataError);
            _PartsListToolStrip.SuspendLayout();
            _PartNumberToolStrip.SuspendLayout();
            _PartToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__PartsDataGridView).BeginInit();
            SuspendLayout();
            // 
            // _PartsListToolStrip
            // 
            _PartsListToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom;
            _PartsListToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartsListToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __AddPartToolStripButton, __ClearPartListToolStripButton, __SavePartsToolStripButton });
            _PartsListToolStrip.Location = new System.Drawing.Point(0, 404);
            _PartsListToolStrip.Name = "_PartsListToolStrip";
            _PartsListToolStrip.Size = new System.Drawing.Size(584, 25);
            _PartsListToolStrip.TabIndex = 2;
            _PartsListToolStrip.Text = "ToolStrip1";
            // 
            // _AddPartToolStripButton
            // 
            __AddPartToolStripButton.AutoToolTip = false;
            __AddPartToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __AddPartToolStripButton.Image = (System.Drawing.Image)resources.GetObject("_AddPartToolStripButton.Image");
            __AddPartToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AddPartToolStripButton.Name = "__AddPartToolStripButton";
            __AddPartToolStripButton.Size = new System.Drawing.Size(93, 22);
            __AddPartToolStripButton.Text = "ADD PART";
            __AddPartToolStripButton.ToolTipText = "Add part to the list";
            // 
            // _ClearPartListToolStripButton
            // 
            __ClearPartListToolStripButton.AutoToolTip = false;
            __ClearPartListToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ClearPartListToolStripButton.Image = (System.Drawing.Image)resources.GetObject("_ClearPartListToolStripButton.Image");
            __ClearPartListToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearPartListToolStripButton.Name = "__ClearPartListToolStripButton";
            __ClearPartListToolStripButton.Size = new System.Drawing.Size(140, 22);
            __ClearPartListToolStripButton.Text = "CLEAR PARTS LIST";
            __ClearPartListToolStripButton.ToolTipText = "Clear Parts List";
            // 
            // _SavePartsToolStripButton
            // 
            __SavePartsToolStripButton.AutoToolTip = false;
            __SavePartsToolStripButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __SavePartsToolStripButton.Image = (System.Drawing.Image)resources.GetObject("_SavePartsToolStripButton.Image");
            __SavePartsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __SavePartsToolStripButton.Name = "__SavePartsToolStripButton";
            __SavePartsToolStripButton.Size = new System.Drawing.Size(102, 22);
            __SavePartsToolStripButton.Text = "SAVE PARTS";
            __SavePartsToolStripButton.ToolTipText = "Save parts to file";
            // 
            // _PartNumberToolStrip
            // 
            _PartNumberToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartNumberToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _OperatorLabel, _OperatorToolStripTextBox, _LotToolStripLabel, _LotToolStripTextBox, _PartNumberToolStripLabel, _PartNumberToolStripTextBox, _ToolStripSeparator1 });
            _PartNumberToolStrip.Location = new System.Drawing.Point(0, 0);
            _PartNumberToolStrip.Name = "_PartNumberToolStrip";
            _PartNumberToolStrip.Size = new System.Drawing.Size(584, 25);
            _PartNumberToolStrip.TabIndex = 3;
            _PartNumberToolStrip.Text = "ToolStrip1";
            // 
            // _OperatorLabel
            // 
            _OperatorLabel.Name = "_OperatorLabel";
            _OperatorLabel.Size = new System.Drawing.Size(65, 22);
            _OperatorLabel.Text = "Operator:";
            // 
            // _OperatorToolStripTextBox
            // 
            _OperatorToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OperatorToolStripTextBox.Name = "_OperatorToolStripTextBox";
            _OperatorToolStripTextBox.Size = new System.Drawing.Size(132, 25);
            _OperatorToolStripTextBox.ToolTipText = "Operator Id";
            // 
            // _LotToolStripLabel
            // 
            _LotToolStripLabel.Name = "_LotToolStripLabel";
            _LotToolStripLabel.Size = new System.Drawing.Size(29, 22);
            _LotToolStripLabel.Text = "Lot:";
            // 
            // _LotToolStripTextBox
            // 
            _LotToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _LotToolStripTextBox.Name = "_LotToolStripTextBox";
            _LotToolStripTextBox.Size = new System.Drawing.Size(132, 25);
            _LotToolStripTextBox.ToolTipText = "Lot number";
            // 
            // _PartNumberToolStripLabel
            // 
            _PartNumberToolStripLabel.Name = "_PartNumberToolStripLabel";
            _PartNumberToolStripLabel.Size = new System.Drawing.Size(34, 22);
            _PartNumberToolStripLabel.Text = "Part:";
            // 
            // _PartNumberToolStripTextBox
            // 
            _PartNumberToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartNumberToolStripTextBox.Name = "_PartNumberToolStripTextBox";
            _PartNumberToolStripTextBox.Size = new System.Drawing.Size(132, 25);
            _PartNumberToolStripTextBox.ToolTipText = "Part number";
            // 
            // _ToolStripSeparator1
            // 
            _ToolStripSeparator1.Name = "_ToolStripSeparator1";
            _ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // _PartToolStrip
            // 
            _PartToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _SerialNumberToolStripLabel, _SerialNumberToolStripTextBox, _ToolStripSplitButton, _ToolStripSeparator2, _OutcomeToolStripLabel, _PassFailToolStripButton });
            _PartToolStrip.Location = new System.Drawing.Point(0, 25);
            _PartToolStrip.Name = "_PartToolStrip";
            _PartToolStrip.Size = new System.Drawing.Size(584, 25);
            _PartToolStrip.TabIndex = 4;
            _PartToolStrip.Text = "ToolStrip1";
            // 
            // _SerialNumberToolStripLabel
            // 
            _SerialNumberToolStripLabel.Name = "_SerialNumberToolStripLabel";
            _SerialNumberToolStripLabel.Size = new System.Drawing.Size(33, 22);
            _SerialNumberToolStripLabel.Text = "S/N:";
            // 
            // _SerialNumberToolStripTextBox
            // 
            _SerialNumberToolStripTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SerialNumberToolStripTextBox.Name = "_SerialNumberToolStripTextBox";
            _SerialNumberToolStripTextBox.Size = new System.Drawing.Size(132, 25);
            _SerialNumberToolStripTextBox.ToolTipText = "Serial Number";
            // 
            // _ToolStripSplitButton
            // 
            _ToolStripSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _AutoAddToolStripMenuItem, _ClearMeasurementsToolStripMenuItem });
            _ToolStripSplitButton.Image = (System.Drawing.Image)resources.GetObject("_ToolStripSplitButton.Image");
            _ToolStripSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ToolStripSplitButton.Name = "_ToolStripSplitButton";
            _ToolStripSplitButton.Size = new System.Drawing.Size(86, 22);
            _ToolStripSplitButton.Text = "Options";
            // 
            // _AutoAddToolStripMenuItem
            // 
            _AutoAddToolStripMenuItem.CheckOnClick = true;
            _AutoAddToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _AutoAddToolStripMenuItem.Name = "_AutoAddToolStripMenuItem";
            _AutoAddToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            _AutoAddToolStripMenuItem.Text = "AUTO ADD";
            _AutoAddToolStripMenuItem.ToolTipText = "Automatically add parts when measurements complete.";
            // 
            // _ClearMeasurementsToolStripMenuItem
            // 
            _ClearMeasurementsToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ClearMeasurementsToolStripMenuItem.Name = "_ClearMeasurementsToolStripMenuItem";
            _ClearMeasurementsToolStripMenuItem.Size = new System.Drawing.Size(222, 22);
            _ClearMeasurementsToolStripMenuItem.Text = "CLEAR MEASUREMENTS";
            _ClearMeasurementsToolStripMenuItem.ToolTipText = "Clears all measurements on the current part";
            // 
            // _ToolStripSeparator2
            // 
            _ToolStripSeparator2.Name = "_ToolStripSeparator2";
            _ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // _OutcomeToolStripLabel
            // 
            _OutcomeToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OutcomeToolStripLabel.Name = "_OutcomeToolStripLabel";
            _OutcomeToolStripLabel.Size = new System.Drawing.Size(80, 22);
            _OutcomeToolStripLabel.Text = "<outcome>";
            // 
            // _PassFailToolStripButton
            // 
            _PassFailToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            _PassFailToolStripButton.Image = My.Resources.Resources.Good;
            _PassFailToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _PassFailToolStripButton.Name = "_PassFailToolStripButton";
            _PassFailToolStripButton.Size = new System.Drawing.Size(23, 22);
            _PassFailToolStripButton.Visible = false;
            // 
            // _PartsDataGridView
            // 
            __PartsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            __PartsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            __PartsDataGridView.Location = new System.Drawing.Point(0, 50);
            __PartsDataGridView.Name = "__PartsDataGridView";
            __PartsDataGridView.Size = new System.Drawing.Size(584, 354);
            __PartsDataGridView.TabIndex = 5;
            // 
            // PartsPanel
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(8.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(__PartsDataGridView);
            Controls.Add(_PartToolStrip);
            Controls.Add(_PartNumberToolStrip);
            Controls.Add(_PartsListToolStrip);
            Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            Name = "PartsPanel";
            Size = new System.Drawing.Size(584, 429);
            _PartsListToolStrip.ResumeLayout(false);
            _PartsListToolStrip.PerformLayout();
            _PartNumberToolStrip.ResumeLayout(false);
            _PartNumberToolStrip.PerformLayout();
            _PartToolStrip.ResumeLayout(false);
            _PartToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__PartsDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.ToolStrip _PartsListToolStrip;
        private System.Windows.Forms.ToolStripButton __AddPartToolStripButton;

        private System.Windows.Forms.ToolStripButton _AddPartToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AddPartToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AddPartToolStripButton != null)
                {
                    __AddPartToolStripButton.Click -= AddPartToolStripButton_Click;
                }

                __AddPartToolStripButton = value;
                if (__AddPartToolStripButton != null)
                {
                    __AddPartToolStripButton.Click += AddPartToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __ClearPartListToolStripButton;

        private System.Windows.Forms.ToolStripButton _ClearPartListToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearPartListToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearPartListToolStripButton != null)
                {
                    __ClearPartListToolStripButton.Click -= ClearPartListToolStripButton_Click;
                }

                __ClearPartListToolStripButton = value;
                if (__ClearPartListToolStripButton != null)
                {
                    __ClearPartListToolStripButton.Click += ClearPartListToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __SavePartsToolStripButton;

        private System.Windows.Forms.ToolStripButton _SavePartsToolStripButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SavePartsToolStripButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SavePartsToolStripButton != null)
                {
                    __SavePartsToolStripButton.Click -= SavePartsToolStripButton_Click;
                }

                __SavePartsToolStripButton = value;
                if (__SavePartsToolStripButton != null)
                {
                    __SavePartsToolStripButton.Click += SavePartsToolStripButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStrip _PartNumberToolStrip;
        private System.Windows.Forms.ToolStripLabel _OperatorLabel;
        private System.Windows.Forms.ToolStripTextBox _OperatorToolStripTextBox;
        private System.Windows.Forms.ToolStripLabel _LotToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox _LotToolStripTextBox;
        private System.Windows.Forms.ToolStripLabel _PartNumberToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox _PartNumberToolStripTextBox;
        private System.Windows.Forms.ToolStrip _PartToolStrip;
        private System.Windows.Forms.ToolStripLabel _SerialNumberToolStripLabel;
        private System.Windows.Forms.ToolStripTextBox _SerialNumberToolStripTextBox;
        private System.Windows.Forms.ToolStripSplitButton _ToolStripSplitButton;
        private System.Windows.Forms.ToolStripMenuItem _AutoAddToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _ClearMeasurementsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator _ToolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator _ToolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel _OutcomeToolStripLabel;
        private System.Windows.Forms.ToolStripButton _PassFailToolStripButton;
        private System.Windows.Forms.DataGridView __PartsDataGridView;

        private System.Windows.Forms.DataGridView _PartsDataGridView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PartsDataGridView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PartsDataGridView != null)
                {
                    __PartsDataGridView.DataError -= PartsDataGridView_DataError;
                }

                __PartsDataGridView = value;
                if (__PartsDataGridView != null)
                {
                    __PartsDataGridView.DataError += PartsDataGridView_DataError;
                }
            }
        }
    }
}
