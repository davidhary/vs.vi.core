﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class MeterView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MeterView));
            _SplitContainer = new System.Windows.Forms.SplitContainer();
            __NavigatorTreeView = new System.Windows.Forms.TreeView();
            __NavigatorTreeView.BeforeSelect += new System.Windows.Forms.TreeViewCancelEventHandler(NavigatorTreeView_BeforeSelect);
            __NavigatorTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(NavigatorTreeView_AfterSelect);
            __Tabs = new System.Windows.Forms.TabControl();
            __Tabs.DrawItem += new System.Windows.Forms.DrawItemEventHandler(Tabs_DrawItem);
            _ConnectTabPage = new System.Windows.Forms.TabPage();
            _ConnectTabLayout = new System.Windows.Forms.TableLayoutPanel();
            _ConnectGroupBox = new System.Windows.Forms.GroupBox();
            _ResourceSelectorConnector = new Core.Controls.SelectorOpener();
            _ResourceInfoLabel = new System.Windows.Forms.Label();
            _IdentityTextBox = new System.Windows.Forms.TextBox();
            _TtmConfigTabPage = new System.Windows.Forms.TabPage();
            _MainLayout = new System.Windows.Forms.TableLayoutPanel();
            _ConfigurationLayout = new System.Windows.Forms.TableLayoutPanel();
            __TTMConfigurationPanel = new ConfigurationView();
            __TTMConfigurationPanel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(TTMConfigurationPanel_PropertyChanged);
            _TtmTabPage = new System.Windows.Forms.TabPage();
            _TtmLayout = new System.Windows.Forms.TableLayoutPanel();
            _MeasurementPanel = new MeasurementView();
            _ShuntTabPage = new System.Windows.Forms.TabPage();
            _ShuntLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShuntDisplayLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShuntResistanceTextBox = new System.Windows.Forms.TextBox();
            _ShuntResistanceTextBoxLabel = new System.Windows.Forms.Label();
            _ShuntGroupBoxLayout = new System.Windows.Forms.TableLayoutPanel();
            __MeasureShuntResistanceButton = new System.Windows.Forms.Button();
            __MeasureShuntResistanceButton.Click += new EventHandler(MeasureShuntResistanceButton_Click);
            _ShuntConfigureGroupBoxLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShuntResistanceConfigurationGroupBox = new System.Windows.Forms.GroupBox();
            __RestoreShuntResistanceDefaultsButton = new System.Windows.Forms.Button();
            __RestoreShuntResistanceDefaultsButton.Click += new EventHandler(RestoreShuntResistanceDefaultsButton_Click);
            __ApplyNewShuntResistanceConfigurationButton = new System.Windows.Forms.Button();
            __ApplyNewShuntResistanceConfigurationButton.Click += new EventHandler(ApplyNewShuntResistanceConfigurationButton_Click);
            __ApplyShuntResistanceConfigurationButton = new System.Windows.Forms.Button();
            __ApplyShuntResistanceConfigurationButton.Click += new EventHandler(ApplyShuntResistanceConfigurationButton_Click);
            _ShuntResistanceCurrentRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceCurrentRangeNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceLowLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceLowLimitNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceHighLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceHighLimitNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceVoltageLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceVoltageLimitNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceCurrentLevelNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceCurrentLevelNumericLabel = new System.Windows.Forms.Label();
            _PartsTabPage = new System.Windows.Forms.TabPage();
            _PartsLayout = new System.Windows.Forms.TableLayoutPanel();
            __PartsPanel = new PartsView();
            __PartsPanel.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(PartsPanel_PropertyChanged);
            _MessagesTabPage = new System.Windows.Forms.TabPage();
            __TraceMessagesBox = new Core.Forma.TraceMessagesBox();
            __TraceMessagesBox.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(TraceMessagesBox_PropertyChanged);
            _PartHeader = new PartHeader();
            _ThermalTransientHeader = new ThermalTransientHeader();
            _StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _StatusStrip = new System.Windows.Forms.StatusStrip();
            _MeasurementsHeader = new MeasurementsHeader();
            _MeterTimer = new System.Windows.Forms.Timer(components);
            ((System.ComponentModel.ISupportInitialize)_SplitContainer).BeginInit();
            _SplitContainer.Panel1.SuspendLayout();
            _SplitContainer.SuspendLayout();
            __Tabs.SuspendLayout();
            _ConnectTabPage.SuspendLayout();
            _ConnectTabLayout.SuspendLayout();
            _ConnectGroupBox.SuspendLayout();
            _TtmConfigTabPage.SuspendLayout();
            _MainLayout.SuspendLayout();
            _ConfigurationLayout.SuspendLayout();
            _TtmTabPage.SuspendLayout();
            _TtmLayout.SuspendLayout();
            _ShuntTabPage.SuspendLayout();
            _ShuntLayout.SuspendLayout();
            _ShuntDisplayLayout.SuspendLayout();
            _ShuntGroupBoxLayout.SuspendLayout();
            _ShuntConfigureGroupBoxLayout.SuspendLayout();
            _ShuntResistanceConfigurationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceLowLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceHighLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceVoltageLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentLevelNumeric).BeginInit();
            _PartsTabPage.SuspendLayout();
            _PartsLayout.SuspendLayout();
            _MessagesTabPage.SuspendLayout();
            _StatusStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _SplitContainer
            // 
            _SplitContainer.Location = new System.Drawing.Point(12, 413);
            _SplitContainer.Name = "_SplitContainer";

            // 
            // _SplitContainer.Panel1
            // 
            _SplitContainer.Panel1.Controls.Add(__NavigatorTreeView);
            _SplitContainer.Size = new System.Drawing.Size(264, 231);
            _SplitContainer.SplitterDistance = 133;
            _SplitContainer.TabIndex = 12;
            // 
            // _NavigatorTreeView
            // 
            __NavigatorTreeView.Dock = System.Windows.Forms.DockStyle.Fill;
            __NavigatorTreeView.Enabled = false;
            __NavigatorTreeView.Location = new System.Drawing.Point(0, 0);
            __NavigatorTreeView.Name = "__NavigatorTreeView";
            __NavigatorTreeView.Size = new System.Drawing.Size(133, 231);
            __NavigatorTreeView.TabIndex = 3;
            // 
            // _Tabs
            // 
            __Tabs.Controls.Add(_ConnectTabPage);
            __Tabs.Controls.Add(_TtmConfigTabPage);
            __Tabs.Controls.Add(_TtmTabPage);
            __Tabs.Controls.Add(_ShuntTabPage);
            __Tabs.Controls.Add(_PartsTabPage);
            __Tabs.Controls.Add(_MessagesTabPage);
            __Tabs.Location = new System.Drawing.Point(56, 205);
            __Tabs.Multiline = true;
            __Tabs.Name = "__Tabs";
            __Tabs.Padding = new System.Drawing.Point(12, 3);
            __Tabs.SelectedIndex = 0;
            __Tabs.Size = new System.Drawing.Size(676, 537);
            __Tabs.TabIndex = 8;
            // 
            // _ConnectTabPage
            // 
            _ConnectTabPage.Controls.Add(_ConnectTabLayout);
            _ConnectTabPage.Location = new System.Drawing.Point(4, 26);
            _ConnectTabPage.Name = "_ConnectTabPage";
            _ConnectTabPage.Size = new System.Drawing.Size(668, 507);
            _ConnectTabPage.TabIndex = 2;
            _ConnectTabPage.Text = "CONNECT";
            _ConnectTabPage.UseVisualStyleBackColor = true;
            // 
            // _ConnectTabLayout
            // 
            _ConnectTabLayout.ColumnCount = 3;
            _ConnectTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConnectTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ConnectTabLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConnectTabLayout.Controls.Add(_ConnectGroupBox, 1, 1);
            _ConnectTabLayout.Location = new System.Drawing.Point(12, 12);
            _ConnectTabLayout.Name = "_ConnectTabLayout";
            _ConnectTabLayout.RowCount = 3;
            _ConnectTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConnectTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ConnectTabLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConnectTabLayout.Size = new System.Drawing.Size(644, 403);
            _ConnectTabLayout.TabIndex = 0;
            // 
            // _ConnectGroupBox
            // 
            _ConnectGroupBox.Controls.Add(_ResourceSelectorConnector);
            _ConnectGroupBox.Controls.Add(_ResourceInfoLabel);
            _ConnectGroupBox.Controls.Add(_IdentityTextBox);
            _ConnectGroupBox.Location = new System.Drawing.Point(5, 100);
            _ConnectGroupBox.Name = "_ConnectGroupBox";
            _ConnectGroupBox.Size = new System.Drawing.Size(634, 203);
            _ConnectGroupBox.TabIndex = 2;
            _ConnectGroupBox.TabStop = false;
            _ConnectGroupBox.Text = "CONNECT";
            // 
            // _ResourceSelectorConnector
            // 
            _ResourceSelectorConnector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _ResourceSelectorConnector.BackColor = System.Drawing.Color.Transparent;
            _ResourceSelectorConnector.Clearable = true;
            _ResourceSelectorConnector.Openable = true;
            _ResourceSelectorConnector.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ResourceSelectorConnector.Location = new System.Drawing.Point(28, 21);
            _ResourceSelectorConnector.Margin = new System.Windows.Forms.Padding(0);
            _ResourceSelectorConnector.Name = "_ResourceSelectorConnector";
            _ResourceSelectorConnector.PublishBindingSuccessEnabled = false;
            _ResourceSelectorConnector.Searchable = true;
            _ResourceSelectorConnector.Size = new System.Drawing.Size(578, 29);
            _ResourceSelectorConnector.TabIndex = 5;
            ToolTip.SetToolTip(_ResourceSelectorConnector, "Find resources and connect");
            // 
            // _ResourceInfoLabel
            // 
            _ResourceInfoLabel.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _ResourceInfoLabel.Location = new System.Drawing.Point(28, 89);
            _ResourceInfoLabel.Name = "_ResourceInfoLabel";
            _ResourceInfoLabel.Size = new System.Drawing.Size(578, 102);
            _ResourceInfoLabel.TabIndex = 4;
            _ResourceInfoLabel.Text = resources.GetString("_ResourceInfoLabel.Text");
            // 
            // _IdentityTextBox
            // 
            _IdentityTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _IdentityTextBox.Location = new System.Drawing.Point(28, 61);
            _IdentityTextBox.Name = "_IdentityTextBox";
            _IdentityTextBox.ReadOnly = true;
            _IdentityTextBox.Size = new System.Drawing.Size(578, 25);
            _IdentityTextBox.TabIndex = 3;
            ToolTip.SetToolTip(_IdentityTextBox, "Displays the meter identity information");
            // 
            // _TtmConfigTabPage
            // 
            _TtmConfigTabPage.Controls.Add(_MainLayout);
            _TtmConfigTabPage.Location = new System.Drawing.Point(4, 26);
            _TtmConfigTabPage.Name = "_TtmConfigTabPage";
            _TtmConfigTabPage.Size = new System.Drawing.Size(668, 507);
            _TtmConfigTabPage.TabIndex = 0;
            _TtmConfigTabPage.Text = "TTM CONFIG.";
            _TtmConfigTabPage.UseVisualStyleBackColor = true;
            // 
            // _MainLayout
            // 
            _MainLayout.ColumnCount = 3;
            _MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _MainLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MainLayout.Controls.Add(_ConfigurationLayout, 1, 1);
            _MainLayout.Location = new System.Drawing.Point(13, 12);
            _MainLayout.Name = "_MainLayout";
            _MainLayout.RowCount = 3;
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _MainLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _MainLayout.Size = new System.Drawing.Size(655, 453);
            _MainLayout.TabIndex = 0;
            // 
            // _ConfigurationLayout
            // 
            _ConfigurationLayout.ColumnCount = 3;
            _ConfigurationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConfigurationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ConfigurationLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConfigurationLayout.Controls.Add(__TTMConfigurationPanel, 1, 0);
            _ConfigurationLayout.Location = new System.Drawing.Point(-13, 55);
            _ConfigurationLayout.Name = "_ConfigurationLayout";
            _ConfigurationLayout.RowCount = 1;
            _ConfigurationLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _ConfigurationLayout.Size = new System.Drawing.Size(681, 343);
            _ConfigurationLayout.TabIndex = 2;
            // 
            // _TTMConfigurationPanel
            // 
            __TTMConfigurationPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __TTMConfigurationPanel.BackColor = System.Drawing.Color.Transparent;
            __TTMConfigurationPanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TTMConfigurationPanel.IsNewConfigurationSettingAvailable = false;
            __TTMConfigurationPanel.Location = new System.Drawing.Point(68, 4);
            __TTMConfigurationPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __TTMConfigurationPanel.Name = "__TTMConfigurationPanel";
            __TTMConfigurationPanel.PublishBindingSuccessEnabled = false;
            __TTMConfigurationPanel.Size = new System.Drawing.Size(545, 335);
            __TTMConfigurationPanel.TabIndex = 0;
            __TTMConfigurationPanel.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_TTMConfigurationPanel.TraceLogEvent");
            __TTMConfigurationPanel.TraceLogLevel = TraceEventType.Information;
            __TTMConfigurationPanel.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_TTMConfigurationPanel.TraceShowEvent");
            __TTMConfigurationPanel.TraceShowLevel = TraceEventType.Information;
            // 
            // _TtmTabPage
            // 
            _TtmTabPage.Controls.Add(_TtmLayout);
            _TtmTabPage.Location = new System.Drawing.Point(4, 26);
            _TtmTabPage.Name = "_TtmTabPage";
            _TtmTabPage.Size = new System.Drawing.Size(668, 507);
            _TtmTabPage.TabIndex = 4;
            _TtmTabPage.Text = "TTM";
            _TtmTabPage.UseVisualStyleBackColor = true;
            // 
            // _TtmLayout
            // 
            _TtmLayout.ColumnCount = 3;
            _TtmLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _TtmLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TtmLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _TtmLayout.Controls.Add(_MeasurementPanel, 1, 1);
            _TtmLayout.Location = new System.Drawing.Point(16, 16);
            _TtmLayout.Name = "_TtmLayout";
            _TtmLayout.RowCount = 3;
            _TtmLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _TtmLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TtmLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _TtmLayout.Size = new System.Drawing.Size(589, 413);
            _TtmLayout.TabIndex = 0;
            // 
            // _MeasurementPanel
            // 
            _MeasurementPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _MeasurementPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _MeasurementPanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasurementPanel.Location = new System.Drawing.Point(23, 23);
            _MeasurementPanel.Name = "_MeasurementPanel";
            _MeasurementPanel.PublishBindingSuccessEnabled = false;
            _MeasurementPanel.Size = new System.Drawing.Size(543, 367);
            _MeasurementPanel.TabIndex = 0;
            _MeasurementPanel.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_MeasurementPanel.TraceLogEvent");
            _MeasurementPanel.TraceLogLevel = TraceEventType.Information;
            _MeasurementPanel.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_MeasurementPanel.TraceShowEvent");
            _MeasurementPanel.TraceShowLevel = TraceEventType.Information;
            // 
            // _ShuntTabPage
            // 
            _ShuntTabPage.Controls.Add(_ShuntLayout);
            _ShuntTabPage.Location = new System.Drawing.Point(4, 26);
            _ShuntTabPage.Name = "_ShuntTabPage";
            _ShuntTabPage.Size = new System.Drawing.Size(668, 507);
            _ShuntTabPage.TabIndex = 3;
            _ShuntTabPage.Text = "SHUNT";
            _ShuntTabPage.UseVisualStyleBackColor = true;
            // 
            // _ShuntLayout
            // 
            _ShuntLayout.ColumnCount = 3;
            _ShuntLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.Controls.Add(_ShuntDisplayLayout, 1, 1);
            _ShuntLayout.Controls.Add(_ShuntGroupBoxLayout, 1, 3);
            _ShuntLayout.Controls.Add(_ShuntConfigureGroupBoxLayout, 1, 2);
            _ShuntLayout.Location = new System.Drawing.Point(5, 5);
            _ShuntLayout.Name = "_ShuntLayout";
            _ShuntLayout.RowCount = 5;
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.Size = new System.Drawing.Size(466, 492);
            _ShuntLayout.TabIndex = 0;
            // 
            // _ShuntDisplayLayout
            // 
            _ShuntDisplayLayout.BackColor = System.Drawing.Color.Black;
            _ShuntDisplayLayout.ColumnCount = 3;
            _ShuntDisplayLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0f));
            _ShuntDisplayLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntDisplayLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0f));
            _ShuntDisplayLayout.Controls.Add(_ShuntResistanceTextBox, 1, 2);
            _ShuntDisplayLayout.Controls.Add(_ShuntResistanceTextBoxLabel, 1, 1);
            _ShuntDisplayLayout.Dock = System.Windows.Forms.DockStyle.Left;
            _ShuntDisplayLayout.Location = new System.Drawing.Point(64, 11);
            _ShuntDisplayLayout.Name = "_ShuntDisplayLayout";
            _ShuntDisplayLayout.RowCount = 4;
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5.0f));
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5.0f));
            _ShuntDisplayLayout.Size = new System.Drawing.Size(337, 75);
            _ShuntDisplayLayout.TabIndex = 0;
            // 
            // _ShuntResistanceTextBox
            // 
            _ShuntResistanceTextBox.BackColor = System.Drawing.Color.Black;
            _ShuntResistanceTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _ShuntResistanceTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ShuntResistanceTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _ShuntResistanceTextBox.Location = new System.Drawing.Point(68, 25);
            _ShuntResistanceTextBox.Name = "_ShuntResistanceTextBox";
            _ShuntResistanceTextBox.ReadOnly = true;
            _ShuntResistanceTextBox.Size = new System.Drawing.Size(201, 39);
            _ShuntResistanceTextBox.TabIndex = 7;
            _ShuntResistanceTextBox.Text = "0.000";
            _ShuntResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ShuntResistanceTextBox, "Measured Shunt Resistance");
            // 
            // _ShuntResistanceTextBoxLabel
            // 
            _ShuntResistanceTextBoxLabel.AutoSize = true;
            _ShuntResistanceTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _ShuntResistanceTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ShuntResistanceTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _ShuntResistanceTextBoxLabel.Location = new System.Drawing.Point(68, 5);
            _ShuntResistanceTextBoxLabel.Name = "_ShuntResistanceTextBoxLabel";
            _ShuntResistanceTextBoxLabel.Size = new System.Drawing.Size(201, 17);
            _ShuntResistanceTextBoxLabel.TabIndex = 6;
            _ShuntResistanceTextBoxLabel.Text = "SHUNT RESISTANCE [Ω]";
            _ShuntResistanceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _ShuntGroupBoxLayout
            // 
            _ShuntGroupBoxLayout.ColumnCount = 3;
            _ShuntGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntGroupBoxLayout.Controls.Add(__MeasureShuntResistanceButton, 1, 1);
            _ShuntGroupBoxLayout.Location = new System.Drawing.Point(64, 417);
            _ShuntGroupBoxLayout.Name = "_ShuntGroupBoxLayout";
            _ShuntGroupBoxLayout.RowCount = 3;
            _ShuntGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _ShuntGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _ShuntGroupBoxLayout.Size = new System.Drawing.Size(337, 63);
            _ShuntGroupBoxLayout.TabIndex = 2;
            // 
            // _MeasureShuntResistanceButton
            // 
            __MeasureShuntResistanceButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __MeasureShuntResistanceButton.Location = new System.Drawing.Point(53, 13);
            __MeasureShuntResistanceButton.Name = "__MeasureShuntResistanceButton";
            __MeasureShuntResistanceButton.Size = new System.Drawing.Size(230, 36);
            __MeasureShuntResistanceButton.TabIndex = 3;
            __MeasureShuntResistanceButton.Text = "READ SHUNT RESISTANCE";
            __MeasureShuntResistanceButton.UseVisualStyleBackColor = true;
            // 
            // _ShuntConfigureGroupBoxLayout
            // 
            _ShuntConfigureGroupBoxLayout.ColumnCount = 3;
            _ShuntConfigureGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntConfigureGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.Controls.Add(_ShuntResistanceConfigurationGroupBox, 1, 1);
            _ShuntConfigureGroupBoxLayout.Location = new System.Drawing.Point(64, 92);
            _ShuntConfigureGroupBoxLayout.Name = "_ShuntConfigureGroupBoxLayout";
            _ShuntConfigureGroupBoxLayout.RowCount = 3;
            _ShuntConfigureGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntConfigureGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.Size = new System.Drawing.Size(337, 319);
            _ShuntConfigureGroupBoxLayout.TabIndex = 3;
            // 
            // _ShuntResistanceConfigurationGroupBox
            // 
            _ShuntResistanceConfigurationGroupBox.Controls.Add(__RestoreShuntResistanceDefaultsButton);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(__ApplyNewShuntResistanceConfigurationButton);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(__ApplyShuntResistanceConfigurationButton);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentRangeNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentRangeNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceLowLimitNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceLowLimitNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceHighLimitNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceHighLimitNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceVoltageLimitNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceVoltageLimitNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentLevelNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentLevelNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            _ShuntResistanceConfigurationGroupBox.Location = new System.Drawing.Point(39, 12);
            _ShuntResistanceConfigurationGroupBox.Name = "_ShuntResistanceConfigurationGroupBox";
            _ShuntResistanceConfigurationGroupBox.Size = new System.Drawing.Size(258, 294);
            _ShuntResistanceConfigurationGroupBox.TabIndex = 2;
            _ShuntResistanceConfigurationGroupBox.TabStop = false;
            _ShuntResistanceConfigurationGroupBox.Text = "SHUNT RESISTANCE CONFIG.";
            // 
            // _RestoreShuntResistanceDefaultsButton
            // 
            __RestoreShuntResistanceDefaultsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __RestoreShuntResistanceDefaultsButton.Location = new System.Drawing.Point(14, 252);
            __RestoreShuntResistanceDefaultsButton.Name = "__RestoreShuntResistanceDefaultsButton";
            __RestoreShuntResistanceDefaultsButton.Size = new System.Drawing.Size(234, 30);
            __RestoreShuntResistanceDefaultsButton.TabIndex = 12;
            __RestoreShuntResistanceDefaultsButton.Text = "RESTORE DEFAULTS";
            __RestoreShuntResistanceDefaultsButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyNewShuntResistanceConfigurationButton
            // 
            __ApplyNewShuntResistanceConfigurationButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplyNewShuntResistanceConfigurationButton.Location = new System.Drawing.Point(133, 210);
            __ApplyNewShuntResistanceConfigurationButton.Name = "__ApplyNewShuntResistanceConfigurationButton";
            __ApplyNewShuntResistanceConfigurationButton.Size = new System.Drawing.Size(115, 30);
            __ApplyNewShuntResistanceConfigurationButton.TabIndex = 11;
            __ApplyNewShuntResistanceConfigurationButton.Text = "APPLY CHANGES";
            __ApplyNewShuntResistanceConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyShuntResistanceConfigurationButton
            // 
            __ApplyShuntResistanceConfigurationButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplyShuntResistanceConfigurationButton.Location = new System.Drawing.Point(11, 209);
            __ApplyShuntResistanceConfigurationButton.Name = "__ApplyShuntResistanceConfigurationButton";
            __ApplyShuntResistanceConfigurationButton.Size = new System.Drawing.Size(115, 30);
            __ApplyShuntResistanceConfigurationButton.TabIndex = 10;
            __ApplyShuntResistanceConfigurationButton.Text = "APPLY ALL";
            ToolTip.SetToolTip(__ApplyShuntResistanceConfigurationButton, "Clears last measurement and applies shunt resistance configuration");
            __ApplyShuntResistanceConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // _ShuntResistanceCurrentRangeNumeric
            // 
            _ShuntResistanceCurrentRangeNumeric.DecimalPlaces = 3;
            _ShuntResistanceCurrentRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceCurrentRangeNumeric.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            _ShuntResistanceCurrentRangeNumeric.Location = new System.Drawing.Point(161, 62);
            _ShuntResistanceCurrentRangeNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 65536 });
            _ShuntResistanceCurrentRangeNumeric.Name = "_ShuntResistanceCurrentRangeNumeric";
            _ShuntResistanceCurrentRangeNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceCurrentRangeNumeric.TabIndex = 3;
            _ShuntResistanceCurrentRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ShuntResistanceCurrentRangeNumeric, "Current range in Amperes");
            _ShuntResistanceCurrentRangeNumeric.Value = new decimal(new int[] { 1, 0, 0, 131072 });
            // 
            // _ShuntResistanceCurrentRangeNumericLabel
            // 
            _ShuntResistanceCurrentRangeNumericLabel.AutoSize = true;
            _ShuntResistanceCurrentRangeNumericLabel.Location = new System.Drawing.Point(25, 66);
            _ShuntResistanceCurrentRangeNumericLabel.Name = "_ShuntResistanceCurrentRangeNumericLabel";
            _ShuntResistanceCurrentRangeNumericLabel.Size = new System.Drawing.Size(134, 17);
            _ShuntResistanceCurrentRangeNumericLabel.TabIndex = 2;
            _ShuntResistanceCurrentRangeNumericLabel.Text = "CURRENT RANGE [A]:";
            _ShuntResistanceCurrentRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceLowLimitNumeric
            // 
            _ShuntResistanceLowLimitNumeric.DecimalPlaces = 1;
            _ShuntResistanceLowLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceLowLimitNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceLowLimitNumeric.Location = new System.Drawing.Point(161, 172);
            _ShuntResistanceLowLimitNumeric.Maximum = new decimal(new int[] { 2000, 0, 0, 0 });
            _ShuntResistanceLowLimitNumeric.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceLowLimitNumeric.Name = "_ShuntResistanceLowLimitNumeric";
            _ShuntResistanceLowLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ShuntResistanceLowLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceLowLimitNumeric.TabIndex = 9;
            _ShuntResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ShuntResistanceLowLimitNumeric, "Low limit of passed shunt resistance");
            _ShuntResistanceLowLimitNumeric.Value = new decimal(new int[] { 1150, 0, 0, 0 });
            // 
            // _ShuntResistanceLowLimitNumericLabel
            // 
            _ShuntResistanceLowLimitNumericLabel.AutoSize = true;
            _ShuntResistanceLowLimitNumericLabel.Location = new System.Drawing.Point(63, 176);
            _ShuntResistanceLowLimitNumericLabel.Name = "_ShuntResistanceLowLimitNumericLabel";
            _ShuntResistanceLowLimitNumericLabel.Size = new System.Drawing.Size(96, 17);
            _ShuntResistanceLowLimitNumericLabel.TabIndex = 8;
            _ShuntResistanceLowLimitNumericLabel.Text = "LO&W LIMIT [Ω]:";
            _ShuntResistanceLowLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceHighLimitNumeric
            // 
            _ShuntResistanceHighLimitNumeric.DecimalPlaces = 1;
            _ShuntResistanceHighLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceHighLimitNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceHighLimitNumeric.Location = new System.Drawing.Point(161, 135);
            _ShuntResistanceHighLimitNumeric.Maximum = new decimal(new int[] { 3000, 0, 0, 0 });
            _ShuntResistanceHighLimitNumeric.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceHighLimitNumeric.Name = "_ShuntResistanceHighLimitNumeric";
            _ShuntResistanceHighLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ShuntResistanceHighLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceHighLimitNumeric.TabIndex = 7;
            _ShuntResistanceHighLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ShuntResistanceHighLimitNumeric, "High limit of passed shunt resistance.");
            _ShuntResistanceHighLimitNumeric.Value = new decimal(new int[] { 1250, 0, 0, 0 });
            // 
            // _ShuntResistanceHighLimitNumericLabel
            // 
            _ShuntResistanceHighLimitNumericLabel.AutoSize = true;
            _ShuntResistanceHighLimitNumericLabel.Location = new System.Drawing.Point(61, 139);
            _ShuntResistanceHighLimitNumericLabel.Name = "_ShuntResistanceHighLimitNumericLabel";
            _ShuntResistanceHighLimitNumericLabel.Size = new System.Drawing.Size(98, 17);
            _ShuntResistanceHighLimitNumericLabel.TabIndex = 6;
            _ShuntResistanceHighLimitNumericLabel.Text = "&HIGH LIMIT [Ω]:";
            _ShuntResistanceHighLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceVoltageLimitNumeric
            // 
            _ShuntResistanceVoltageLimitNumeric.DecimalPlaces = 2;
            _ShuntResistanceVoltageLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceVoltageLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ShuntResistanceVoltageLimitNumeric.Location = new System.Drawing.Point(161, 98);
            _ShuntResistanceVoltageLimitNumeric.Maximum = new decimal(new int[] { 40, 0, 0, 0 });
            _ShuntResistanceVoltageLimitNumeric.Name = "_ShuntResistanceVoltageLimitNumeric";
            _ShuntResistanceVoltageLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceVoltageLimitNumeric.TabIndex = 5;
            _ShuntResistanceVoltageLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ShuntResistanceVoltageLimitNumeric, "Voltage Limit in Volts");
            _ShuntResistanceVoltageLimitNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _ShuntResistanceVoltageLimitNumericLabel
            // 
            _ShuntResistanceVoltageLimitNumericLabel.AutoSize = true;
            _ShuntResistanceVoltageLimitNumericLabel.Location = new System.Drawing.Point(38, 102);
            _ShuntResistanceVoltageLimitNumericLabel.Name = "_ShuntResistanceVoltageLimitNumericLabel";
            _ShuntResistanceVoltageLimitNumericLabel.Size = new System.Drawing.Size(119, 17);
            _ShuntResistanceVoltageLimitNumericLabel.TabIndex = 4;
            _ShuntResistanceVoltageLimitNumericLabel.Text = "&VOLTAGE LIMIT [V]:";
            _ShuntResistanceVoltageLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceCurrentLevelNumeric
            // 
            _ShuntResistanceCurrentLevelNumeric.DecimalPlaces = 4;
            _ShuntResistanceCurrentLevelNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceCurrentLevelNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _ShuntResistanceCurrentLevelNumeric.Location = new System.Drawing.Point(161, 27);
            _ShuntResistanceCurrentLevelNumeric.Maximum = new decimal(new int[] { 100, 0, 0, 262144 });
            _ShuntResistanceCurrentLevelNumeric.Name = "_ShuntResistanceCurrentLevelNumeric";
            _ShuntResistanceCurrentLevelNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceCurrentLevelNumeric.TabIndex = 1;
            _ShuntResistanceCurrentLevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ShuntResistanceCurrentLevelNumeric, "Current Level in Amperes");
            _ShuntResistanceCurrentLevelNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ShuntResistanceCurrentLevelNumericLabel
            // 
            _ShuntResistanceCurrentLevelNumericLabel.AutoSize = true;
            _ShuntResistanceCurrentLevelNumericLabel.Location = new System.Drawing.Point(33, 31);
            _ShuntResistanceCurrentLevelNumericLabel.Name = "_ShuntResistanceCurrentLevelNumericLabel";
            _ShuntResistanceCurrentLevelNumericLabel.Size = new System.Drawing.Size(126, 17);
            _ShuntResistanceCurrentLevelNumericLabel.TabIndex = 0;
            _ShuntResistanceCurrentLevelNumericLabel.Text = "C&URRENT LEVEL [A]:";
            _ShuntResistanceCurrentLevelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PartsTabPage
            // 
            _PartsTabPage.Controls.Add(_PartsLayout);
            _PartsTabPage.Location = new System.Drawing.Point(4, 26);
            _PartsTabPage.Name = "_PartsTabPage";
            _PartsTabPage.Size = new System.Drawing.Size(668, 507);
            _PartsTabPage.TabIndex = 5;
            _PartsTabPage.Text = "PARTS";
            _PartsTabPage.UseVisualStyleBackColor = true;
            // 
            // _PartsLayout
            // 
            _PartsLayout.ColumnCount = 3;
            _PartsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _PartsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _PartsLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _PartsLayout.Controls.Add(__PartsPanel, 1, 1);
            _PartsLayout.Location = new System.Drawing.Point(14, 7);
            _PartsLayout.Name = "_PartsLayout";
            _PartsLayout.RowCount = 3;
            _PartsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _PartsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _PartsLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _PartsLayout.Size = new System.Drawing.Size(573, 489);
            _PartsLayout.TabIndex = 0;
            // 
            // _PartsPanel
            // 
            __PartsPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __PartsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            __PartsPanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __PartsPanel.Location = new System.Drawing.Point(23, 24);
            __PartsPanel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __PartsPanel.Name = "__PartsPanel";
            __PartsPanel.PublishBindingSuccessEnabled = false;
            __PartsPanel.Size = new System.Drawing.Size(527, 441);
            __PartsPanel.TabIndex = 0;
            __PartsPanel.TraceLogEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_PartsPanel.TraceLogEvent");
            __PartsPanel.TraceLogLevel = TraceEventType.Information;
            __PartsPanel.TraceShowEvent = (KeyValuePair<TraceEventType, string>)resources.GetObject("_PartsPanel.TraceShowEvent");
            __PartsPanel.TraceShowLevel = TraceEventType.Information;
            // 
            // _MessagesTabPage
            // 
            _MessagesTabPage.Controls.Add(__TraceMessagesBox);
            _MessagesTabPage.Location = new System.Drawing.Point(4, 26);
            _MessagesTabPage.Name = "_MessagesTabPage";
            _MessagesTabPage.Size = new System.Drawing.Size(668, 507);
            _MessagesTabPage.TabIndex = 1;
            _MessagesTabPage.Text = "Log";
            _MessagesTabPage.UseVisualStyleBackColor = true;
            // 
            // _TraceMessagesBox
            // 
            __TraceMessagesBox.AlertLevel = TraceEventType.Warning;
            __TraceMessagesBox.BackColor = System.Drawing.SystemColors.Info;
            __TraceMessagesBox.CaptionFormat = "{0} ≡";
            __TraceMessagesBox.CausesValidation = false;
            __TraceMessagesBox.Font = new System.Drawing.Font("Consolas", 8.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TraceMessagesBox.Location = new System.Drawing.Point(0, 0);
            __TraceMessagesBox.Multiline = true;
            __TraceMessagesBox.Name = "__TraceMessagesBox";
            __TraceMessagesBox.PresetCount = 100;
            __TraceMessagesBox.ReadOnly = true;
            __TraceMessagesBox.ResetCount = 200;
            __TraceMessagesBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            __TraceMessagesBox.Size = new System.Drawing.Size(601, 471);
            __TraceMessagesBox.TabIndex = 0;
            __TraceMessagesBox.TraceLevel = TraceEventType.Verbose;
            // 
            // _PartHeader
            // 
            _PartHeader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _PartHeader.BackColor = System.Drawing.SystemColors.Desktop;
            _PartHeader.Dock = System.Windows.Forms.DockStyle.Top;
            _PartHeader.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartHeader.Location = new System.Drawing.Point(0, 119);
            _PartHeader.Margin = new System.Windows.Forms.Padding(0);
            _PartHeader.Name = "_PartHeader";
            _PartHeader.Size = new System.Drawing.Size(804, 26);
            _PartHeader.TabIndex = 11;
            // 
            // _ThermalTransientHeader
            // 
            _ThermalTransientHeader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _ThermalTransientHeader.BackColor = System.Drawing.Color.Black;
            _ThermalTransientHeader.Dock = System.Windows.Forms.DockStyle.Top;
            _ThermalTransientHeader.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ThermalTransientHeader.Location = new System.Drawing.Point(0, 55);
            _ThermalTransientHeader.Name = "_ThermalTransientHeader";
            _ThermalTransientHeader.Size = new System.Drawing.Size(804, 64);
            _ThermalTransientHeader.TabIndex = 10;
            // 
            // _StatusLabel
            // 
            _StatusLabel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _StatusLabel.Size = new System.Drawing.Size(787, 17);
            _StatusLabel.Spring = true;
            _StatusLabel.Text = "Loading...";
            _StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _StatusLabel });
            _StatusStrip.Location = new System.Drawing.Point(0, 790);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Padding = new System.Windows.Forms.Padding(1, 0, 16, 0);
            _StatusStrip.Size = new System.Drawing.Size(804, 22);
            _StatusStrip.TabIndex = 7;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _MeasurementsHeader
            // 
            _MeasurementsHeader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _MeasurementsHeader.BackColor = System.Drawing.Color.Black;
            _MeasurementsHeader.Dock = System.Windows.Forms.DockStyle.Top;
            _MeasurementsHeader.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasurementsHeader.Location = new System.Drawing.Point(0, 0);
            _MeasurementsHeader.Name = "_MeasurementsHeader";
            _MeasurementsHeader.Size = new System.Drawing.Size(804, 55);
            _MeasurementsHeader.TabIndex = 9;
            // 
            // MeterView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_SplitContainer);
            Controls.Add(__Tabs);
            Controls.Add(_PartHeader);
            Controls.Add(_ThermalTransientHeader);
            Controls.Add(_StatusStrip);
            Controls.Add(_MeasurementsHeader);
            Name = "MeterView";
            Size = new System.Drawing.Size(804, 812);
            ToolTip.IsBalloon = true;
            _SplitContainer.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_SplitContainer).EndInit();
            _SplitContainer.ResumeLayout(false);
            __Tabs.ResumeLayout(false);
            _ConnectTabPage.ResumeLayout(false);
            _ConnectTabLayout.ResumeLayout(false);
            _ConnectGroupBox.ResumeLayout(false);
            _ConnectGroupBox.PerformLayout();
            _TtmConfigTabPage.ResumeLayout(false);
            _MainLayout.ResumeLayout(false);
            _ConfigurationLayout.ResumeLayout(false);
            _TtmTabPage.ResumeLayout(false);
            _TtmLayout.ResumeLayout(false);
            _ShuntTabPage.ResumeLayout(false);
            _ShuntLayout.ResumeLayout(false);
            _ShuntDisplayLayout.ResumeLayout(false);
            _ShuntDisplayLayout.PerformLayout();
            _ShuntGroupBoxLayout.ResumeLayout(false);
            _ShuntConfigureGroupBoxLayout.ResumeLayout(false);
            _ShuntResistanceConfigurationGroupBox.ResumeLayout(false);
            _ShuntResistanceConfigurationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceLowLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceHighLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceVoltageLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentLevelNumeric).EndInit();
            _PartsTabPage.ResumeLayout(false);
            _PartsLayout.ResumeLayout(false);
            _MessagesTabPage.ResumeLayout(false);
            _MessagesTabPage.PerformLayout();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.SplitContainer _SplitContainer;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TreeView __NavigatorTreeView;

        private System.Windows.Forms.TreeView _NavigatorTreeView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NavigatorTreeView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NavigatorTreeView != null)
                {
                    __NavigatorTreeView.BeforeSelect -= NavigatorTreeView_BeforeSelect;
                    __NavigatorTreeView.AfterSelect -= NavigatorTreeView_AfterSelect;
                }

                __NavigatorTreeView = value;
                if (__NavigatorTreeView != null)
                {
                    __NavigatorTreeView.BeforeSelect += NavigatorTreeView_BeforeSelect;
                    __NavigatorTreeView.AfterSelect += NavigatorTreeView_AfterSelect;
                }
            }
        }

        private System.Windows.Forms.TabControl __Tabs;

        private System.Windows.Forms.TabControl _Tabs
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Tabs;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Tabs != null)
                {
                    __Tabs.DrawItem -= Tabs_DrawItem;
                }

                __Tabs = value;
                if (__Tabs != null)
                {
                    __Tabs.DrawItem += Tabs_DrawItem;
                }
            }
        }

        private System.Windows.Forms.TabPage _ConnectTabPage;
        private System.Windows.Forms.TableLayoutPanel _ConnectTabLayout;
        private System.Windows.Forms.GroupBox _ConnectGroupBox;
        private Core.Controls.SelectorOpener _ResourceSelectorConnector;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ResourceInfoLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _IdentityTextBox;
        private System.Windows.Forms.TabPage _TtmConfigTabPage;
        private System.Windows.Forms.TableLayoutPanel _MainLayout;
        private System.Windows.Forms.TableLayoutPanel _ConfigurationLayout;
        private ConfigurationView __TTMConfigurationPanel;

        private ConfigurationView _TTMConfigurationPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TTMConfigurationPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TTMConfigurationPanel != null)
                {
                    __TTMConfigurationPanel.PropertyChanged -= TTMConfigurationPanel_PropertyChanged;
                }

                __TTMConfigurationPanel = value;
                if (__TTMConfigurationPanel != null)
                {
                    __TTMConfigurationPanel.PropertyChanged += TTMConfigurationPanel_PropertyChanged;
                }
            }
        }

        private System.Windows.Forms.TabPage _TtmTabPage;
        private System.Windows.Forms.TableLayoutPanel _TtmLayout;
        private MeasurementView _MeasurementPanel;
        private System.Windows.Forms.TabPage _ShuntTabPage;
        private System.Windows.Forms.TableLayoutPanel _ShuntLayout;
        private System.Windows.Forms.TableLayoutPanel _ShuntDisplayLayout;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _ShuntResistanceTextBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ShuntResistanceTextBoxLabel;
        private System.Windows.Forms.TableLayoutPanel _ShuntGroupBoxLayout;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Button __MeasureShuntResistanceButton;

        private System.Windows.Forms.Button _MeasureShuntResistanceButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureShuntResistanceButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureShuntResistanceButton != null)
                {
                    __MeasureShuntResistanceButton.Click -= MeasureShuntResistanceButton_Click;
                }

                __MeasureShuntResistanceButton = value;
                if (__MeasureShuntResistanceButton != null)
                {
                    __MeasureShuntResistanceButton.Click += MeasureShuntResistanceButton_Click;
                }
            }
        }

        private System.Windows.Forms.TableLayoutPanel _ShuntConfigureGroupBoxLayout;
        private System.Windows.Forms.GroupBox _ShuntResistanceConfigurationGroupBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Button __RestoreShuntResistanceDefaultsButton;

        private System.Windows.Forms.Button _RestoreShuntResistanceDefaultsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RestoreShuntResistanceDefaultsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RestoreShuntResistanceDefaultsButton != null)
                {
                    __RestoreShuntResistanceDefaultsButton.Click -= RestoreShuntResistanceDefaultsButton_Click;
                }

                __RestoreShuntResistanceDefaultsButton = value;
                if (__RestoreShuntResistanceDefaultsButton != null)
                {
                    __RestoreShuntResistanceDefaultsButton.Click += RestoreShuntResistanceDefaultsButton_Click;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Button __ApplyNewShuntResistanceConfigurationButton;

        private System.Windows.Forms.Button _ApplyNewShuntResistanceConfigurationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyNewShuntResistanceConfigurationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyNewShuntResistanceConfigurationButton != null)
                {
                    __ApplyNewShuntResistanceConfigurationButton.Click -= ApplyNewShuntResistanceConfigurationButton_Click;
                }

                __ApplyNewShuntResistanceConfigurationButton = value;
                if (__ApplyNewShuntResistanceConfigurationButton != null)
                {
                    __ApplyNewShuntResistanceConfigurationButton.Click += ApplyNewShuntResistanceConfigurationButton_Click;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Button __ApplyShuntResistanceConfigurationButton;

        private System.Windows.Forms.Button _ApplyShuntResistanceConfigurationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyShuntResistanceConfigurationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyShuntResistanceConfigurationButton != null)
                {
                    __ApplyShuntResistanceConfigurationButton.Click -= ApplyShuntResistanceConfigurationButton_Click;
                }

                __ApplyShuntResistanceConfigurationButton = value;
                if (__ApplyShuntResistanceConfigurationButton != null)
                {
                    __ApplyShuntResistanceConfigurationButton.Click += ApplyShuntResistanceConfigurationButton_Click;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.NumericUpDown _ShuntResistanceCurrentRangeNumeric;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ShuntResistanceCurrentRangeNumericLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.NumericUpDown _ShuntResistanceLowLimitNumeric;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ShuntResistanceLowLimitNumericLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.NumericUpDown _ShuntResistanceHighLimitNumeric;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ShuntResistanceHighLimitNumericLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.NumericUpDown _ShuntResistanceVoltageLimitNumeric;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ShuntResistanceVoltageLimitNumericLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.NumericUpDown _ShuntResistanceCurrentLevelNumeric;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ShuntResistanceCurrentLevelNumericLabel;
        private System.Windows.Forms.TabPage _PartsTabPage;
        private System.Windows.Forms.TableLayoutPanel _PartsLayout;
        private PartsView __PartsPanel;

        private PartsView _PartsPanel
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PartsPanel;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PartsPanel != null)
                {
                    __PartsPanel.PropertyChanged -= PartsPanel_PropertyChanged;
                }

                __PartsPanel = value;
                if (__PartsPanel != null)
                {
                    __PartsPanel.PropertyChanged += PartsPanel_PropertyChanged;
                }
            }
        }

        private System.Windows.Forms.TabPage _MessagesTabPage;
        private Core.Forma.TraceMessagesBox __TraceMessagesBox;

        private Core.Forma.TraceMessagesBox _TraceMessagesBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TraceMessagesBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TraceMessagesBox != null)
                {
                    __TraceMessagesBox.PropertyChanged -= TraceMessagesBox_PropertyChanged;
                }

                __TraceMessagesBox = value;
                if (__TraceMessagesBox != null)
                {
                    __TraceMessagesBox.PropertyChanged += TraceMessagesBox_PropertyChanged;
                }
            }
        }

        private PartHeader _PartHeader;
        private ThermalTransientHeader _ThermalTransientHeader;
        private System.Windows.Forms.StatusStrip _StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _StatusLabel;
        private MeasurementsHeader _MeasurementsHeader;
        private System.Windows.Forms.Timer _MeterTimer;
    }
}