using System.Diagnostics;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ConfigurationView
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            SuspendLayout();
            // 
            // TTMConfigurationPanel
            // 
            Margin = new System.Windows.Forms.Padding(3);
            Name = "TTMConfigurationPanel";
            ResumeLayout(false);
        }
    }
}
