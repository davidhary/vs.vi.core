using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Measurement panel base. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-12 </para><para>
    /// David, 2014-03-15 </para>
    /// </remarks>
    public partial class MeasurementViewBase : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected MeasurementViewBase() : base()
        {
            this.InitializeComponent();
            this._TtmMeasureControlsToolStrip.Enabled = false;
            this.__AbortSequenceToolStripMenuItem.Name = "_AbortSequenceToolStripMenuItem";
            this.__MeasureAllToolStripMenuItem.Name = "_MeasureAllToolStripMenuItem";
            this.__FinalResistanceToolStripMenuItem.Name = "_FinalResistanceToolStripMenuItem";
            this.__ThermalTransientToolStripMenuItem.Name = "_ThermalTransientToolStripMenuItem";
            this.__InitialResistanceToolStripMenuItem.Name = "_InitialResistanceToolStripMenuItem";
            this.__ClearToolStripMenuItem.Name = "_ClearToolStripMenuItem";
            this.__WaitForTriggerToolStripMenuItem.Name = "_WaitForTriggerToolStripMenuItem";
            this.__AssertTriggerToolStripMenuItem.Name = "_AssertTriggerToolStripMenuItem";
            this.__AbortToolStripMenuItem.Name = "_AbortToolStripMenuItem";
            this.__ModelTraceToolStripMenuItem.Name = "_ModelTraceToolStripMenuItem";
            this.__SaveTraceToolStripMenuItem.Name = "_SaveTraceToolStripMenuItem";
            this.__ClearTraceToolStripMenuItem.Name = "_ClearTraceToolStripMenuItem";
            this.__ReadTraceToolStripMenuItem.Name = "_ReadTraceToolStripMenuItem";
            this.__LogTraceLevelComboBox.Name = "_LogTraceLevelComboBox";
            this.__DisplayTraceLevelComboBox.Name = "_DisplayTraceLevelComboBox";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.ReleaseResources();
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " ON CONTROL EVENTS "

        /// <summary> Releases the resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected virtual void ReleaseResources()
        {
            this.TriggerSequencerInternal = null;
            this.MeasureSequencerInternal = null;
            this.LastTimeSeries = null;
        }

        /// <summary> Gets or sets the is device open. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The is device open. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public virtual bool IsDeviceOpen { get; private set; }

        /// <summary> True if trace is available, false if not. </summary>
        private bool _IsTraceAvailable;

        /// <summary> Updates the availability of the controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected void OnStateChanged()
        {
            var measurementSequenceState = MeasurementSequenceState.None;
            if ( this.MeasureSequencer is object )
            {
                measurementSequenceState = this.MeasureSequencer.MeasurementSequenceState;
            }

            var triggerSequenceState = TriggerSequenceState.None;
            if ( this.TriggerSequencer is object )
            {
                triggerSequenceState = this.TriggerSequencer.TriggerSequenceState;
            }

            this._TtmMeasureControlsToolStrip.Enabled = this.IsDeviceOpen;
            this._TriggerToolStripDropDownButton.Enabled = this.IsDeviceOpen && MeasurementSequenceState.Idle == measurementSequenceState;
            if ( this._TriggerToolStripDropDownButton.Enabled )
            {
                this._WaitForTriggerToolStripMenuItem.Enabled = this.IsDeviceOpen && (MeasurementSequenceState.Idle == measurementSequenceState || TriggerSequenceState.WaitingForTrigger == triggerSequenceState);
                this._AssertTriggerToolStripMenuItem.Enabled = TriggerSequenceState.WaitingForTrigger == triggerSequenceState;
            }

            this._MeasureToolStripDropDownButton.Enabled = this.IsDeviceOpen && TriggerSequenceState.Idle == triggerSequenceState;
            if ( this._TtmMeasureControlsToolStrip.Enabled )
            {
                this._InitialResistanceToolStripMenuItem.Enabled = MeasurementSequenceState.Idle == measurementSequenceState;
                this._ThermalTransientToolStripMenuItem.Enabled = MeasurementSequenceState.Idle == measurementSequenceState;
                this._FinalResistanceToolStripMenuItem.Enabled = MeasurementSequenceState.Idle == measurementSequenceState;
                this._MeasureAllToolStripMenuItem.Enabled = MeasurementSequenceState.Idle == measurementSequenceState;
                this._AbortSequenceToolStripMenuItem.Enabled = MeasurementSequenceState.Idle != measurementSequenceState;
            }

            this._TraceToolStripDropDownButton.Enabled = this.IsDeviceOpen && TriggerSequenceState.Idle == triggerSequenceState && MeasurementSequenceState.Idle == measurementSequenceState;
            if ( this._TraceToolStripDropDownButton.Enabled )
            {
                this._ReadTraceToolStripMenuItem.Enabled = this._IsTraceAvailable;
                bool enabled = this.LastTimeSeries is object && this.LastTimeSeries.Any();
                this._SaveTraceToolStripMenuItem.Enabled = enabled;
                this._ClearTraceToolStripMenuItem.Enabled = enabled;
                this._ModelTraceToolStripMenuItem.Enabled = enabled;
            }
        }

        #endregion

        #region " TRACE TOOL STRIP "

        #region " TRACE LIST VIEW "

        /// <summary> Displays a part described by grid. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="grid">   The grid. </param>
        /// <param name="values"> The values. </param>
        private static void DisplayTrace( DataGridView grid, IList<System.Windows.Point> values )
        {
            if ( grid is null )
                throw new ArgumentNullException( nameof( grid ) );
            grid.Enabled = false;
            grid.Columns.Clear();
            grid.DataSource = new List<PointF>();
            grid.AlternatingRowsDefaultCellStyle.BackColor = Color.LightSteelBlue;
            grid.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None;
            grid.RowHeadersVisible = false;
            grid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            if ( values is object )
            {
                grid.DataSource = values;
            }

            grid.Refresh();
            if ( grid.Columns is object && grid.Columns.Count > 0 )
            {
                DataGridViewColumn column;
                foreach ( DataGridViewColumn currentColumn in grid.Columns )
                {
                    column = currentColumn;
                    column.Visible = false;
                }

                int displayIndex = 0;
                column = grid.Columns["X"];
                column.Visible = true;
                column.HeaderText = " Time, ms ";
                column.DisplayIndex = displayIndex;
                displayIndex += 1;
                column = grid.Columns["Y"];
                column.Visible = true;
                column.HeaderText = " Voltage, mV ";
                column.DisplayIndex = displayIndex;
                grid.Enabled = true;
            }
        }

        #endregion

        /// <summary> Gets or sets the last time series. </summary>
        /// <value> The last time series. </value>
        private IList<System.Windows.Point> LastTimeSeries { get; set; }

        /// <summary> Displays the thermal transient trace. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The chart. </param>
        /// <returns> The trace values. </returns>
        protected virtual IList<System.Windows.Point> DisplayThermalTransientTrace( Chart chart )
        {
            // #Disable Warning CA1825 ' Avoid zero-length array allocations.
            return new List<System.Windows.Point>( Array.Empty<System.Windows.Point>() );
            // #Enable Warning CA1825 ' Avoid zero-length array allocations.
        }

        /// <summary> Event handler. Called by _ReadTraceToolStripMenuItem for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadTraceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"reading and displaying the trace";
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
                this.InfoProvider.SetIconPadding( this._TtmMeasureControlsToolStrip, -15 );
                // MeterThermalTransient.EmulateThermalTransientTrace(Me._Chart)
                this.LastTimeSeries = this.DisplayThermalTransientTrace( this._Chart );
                // list the trace values
                DisplayTrace( this._TraceDataGridView, this.LastTimeSeries );
                bool enabled = this.LastTimeSeries is object && this.LastTimeSeries.Any();
                this._SaveTraceToolStripMenuItem.Enabled = enabled;
                this._ClearTraceToolStripMenuItem.Enabled = enabled;
                this._ModelTraceToolStripMenuItem.Enabled = enabled;
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed reading and displaying the thermal transient trace." );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the trace list. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        private void ClearTraceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            DisplayTrace( this._TraceDataGridView, null );
        }

        /// <summary> Determines whether the specified folder path is writable. </summary>
        /// <remarks>
        /// Uses a temporary random file name to test if the file can be created. The file is deleted
        /// thereafter.
        /// </remarks>
        /// <param name="path"> The path. </param>
        /// <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static bool IsFolderWritable( string path )
        {
            string filePath = string.Empty;
            bool affirmative = false;
            try
            {
                filePath = System.IO.Path.Combine( path, System.IO.Path.GetRandomFileName() );
                using ( var s = System.IO.File.Open( filePath, System.IO.FileMode.OpenOrCreate ) )
                {
                }

                affirmative = true;
            }
            catch
            {
            }
            finally
            {
                // SS reported an exception from this test possibly indicating that Windows allowed writing the file 
                // by failed report deletion. Or else, Windows raised another exception type.
                try
                {
                    if ( System.IO.File.Exists( filePath ) )
                    {
                        System.IO.File.Delete( filePath );
                    }
                }
                catch
                {
                }
            }

            return affirmative;
        }

        /// <summary> Sets the default file path in case the data folder does not exist. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> System.String. </returns>
        private static string UpdateDefaultFilePath( string filePath )
        {

            // check validity of data folder.
            string dataFolder = System.IO.Path.GetDirectoryName( filePath );
            if ( !System.IO.Directory.Exists( dataFolder ) )
            {
                dataFolder = My.MyProject.Application.Info.DirectoryPath;
                if ( IsFolderWritable( dataFolder ) )
                {
                    dataFolder = System.IO.Path.Combine( dataFolder, @"..\Measurements" );
                    dataFolder = System.IO.Directory.Exists( dataFolder ) ? System.IO.Path.GetDirectoryName( dataFolder ) : System.IO.Directory.CreateDirectory( dataFolder ).FullName;
                }
                else
                {

                    // in run time use the documents folder.
                    dataFolder = My.MyProject.Computer.FileSystem.SpecialDirectories.MyDocuments;
                    dataFolder = System.IO.Path.Combine( dataFolder, "TTM" );
                    if ( !System.IO.Directory.Exists( dataFolder ) )
                    {
                        _ = System.IO.Directory.CreateDirectory( dataFolder );
                    }

                    dataFolder = System.IO.Path.Combine( dataFolder, "Measurements" );
                    if ( !System.IO.Directory.Exists( dataFolder ) )
                    {
                        _ = System.IO.Directory.CreateDirectory( dataFolder );
                    }
                }
            }

            return System.IO.Path.Combine( dataFolder, System.IO.Path.GetFileName( filePath ) );
        }

        /// <summary> Gets a new file for storing the data. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="filePath"> The file path. </param>
        /// <returns> A file name or empty if error. </returns>
        private static string BrowseForFile( string filePath )
        {

            // make sure the default data file name is valid.
            filePath = UpdateDefaultFilePath( filePath );
            using var dialog = new System.Windows.Forms.SaveFileDialog();
            dialog.CheckFileExists = false;
            dialog.CheckPathExists = true;
            dialog.DefaultExt = ".xls";
            dialog.FileName = filePath;
            dialog.Filter = "Comma Separated Values (*.csv)|*.csv|All Files (*.*)|*.*";
            dialog.FilterIndex = 0;
            dialog.InitialDirectory = System.IO.Path.GetDirectoryName( filePath );
            dialog.RestoreDirectory = true;
            dialog.Title = "Select a Comma-Separated File";

            // Open the Open dialog
            if ( dialog.ShowDialog() == DialogResult.OK )
            {

                // if file selected,
                return dialog.FileName;
            }
            else
            {

                // if some error, just ignore
                return string.Empty;
            }
        }

        /// <summary> Saves the trace to file. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveTraceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            if ( this.LastTimeSeries is object && this.LastTimeSeries.Any() )
            {
                _ = this.PublishVerbose( "Browsing for file;. " );
                string filePath = BrowseForFile( Ttm.My.MySettings.Default.TraceFilePath );
                if ( string.IsNullOrWhiteSpace( filePath ) )
                {
                    return;
                }

                Ttm.My.MySettings.Default.TraceFilePath = filePath;
                string activity = string.Empty;
                try
                {
                    activity = $"Saving trace;. to '{filePath}' ";
                    _ = this.PublishVerbose( activity );
                    using ( var writer = new System.IO.StreamWriter( filePath, false ) )
                    {
                        string record = string.Empty;
                        foreach ( System.Windows.Point point in this.LastTimeSeries )
                        {
                            if ( string.IsNullOrWhiteSpace( record ) )
                            {
                                record = "Trace time Series";
                                writer.WriteLine( record );
                                record = "Time,Voltage";
                                writer.WriteLine( record );
                                record = "ms,mV";
                                writer.WriteLine( record );
                            }

                            record = string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0},{1}", point.X, point.Y );
                            writer.WriteLine( record );
                        }

                        writer.Flush();
                    }

                    _ = this.PublishVerbose( "Done saving;. " );
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                    this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Exception occurred saving trace" );
                }
                finally
                {
                }
            }
        }

        /// <summary> Creates a model of the thermal transient trace. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="chart"> The chart. </param>
        protected virtual void ModelThermalTransientTrace( Chart chart )
        {
        }

        /// <summary> Event handler. Called by _ModelTraceToolStripMenuItem for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ModelTraceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"modeling the trace";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
                this.InfoProvider.SetIconPadding( this._TtmMeasureControlsToolStrip, -15 );
                // MeterThermalTransient.EmulateThermalTransientTrace(Me._Chart)
                this.ModelThermalTransientTrace( this._Chart );
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed modeling the thermal transient trace." );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TTM MEASURE TOOL STRIP "

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected virtual void ClearPartMeasurements()
        {
        }

        /// <summary> Event handler. Called by _ClearToolStripMenuItem for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ClearToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
            this.ClearPartMeasurements();
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The MeasurementOutcomes. </returns>
        protected virtual MeasurementOutcomes MeasureInitialResistance()
        {
            return MeasurementOutcomes.None;
        }

        /// <summary> Measures initial resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitialResistanceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Measuring Initial Resistance";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
                this.InfoProvider.SetIconPadding( this._TtmMeasureControlsToolStrip, -15 );
                _ = this.MeasureInitialResistance();
                var outcome = this.MeasureInitialResistance();
                if ( MeasurementOutcomes.None == outcome || outcome == MeasurementOutcomes.PartPassed )
                {
                    _ = this.PublishVerbose( "Initial Resistance measured;. " );
                }
                else if ( (outcome & MeasurementOutcomes.FailedContactCheck) != 0 )
                {
                    this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed contact check." );
                    _ = this.PublishWarning( "Contact check failed;. " );
                }
                else
                {
                    this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed initial resistance." );
                    _ = this.PublishWarning( "Initial Resistance failed with outcome = {0};. ", ( object ) ( int ) outcome );
                }
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed Measuring Initial Resistance" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The MeasurementOutcomes. </returns>
        protected virtual MeasurementOutcomes MeasureThermalTransient()
        {
            return MeasurementOutcomes.None;
        }

        /// <summary> Measures the thermal transient voltage. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalTransientToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Measuring thermal Resistance";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
                this.InfoProvider.SetIconPadding( this._TtmMeasureControlsToolStrip, -15 );
                var outcome = this.MeasureThermalTransient();
                if ( MeasurementOutcomes.None == outcome || outcome == MeasurementOutcomes.PartPassed )
                {
                    this._IsTraceAvailable = true;
                    _ = this.PublishVerbose( "Thermal Transient measured;. " );
                }
                else
                {
                    this._IsTraceAvailable = false;
                    this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Thermal transient failed." );
                    _ = this.PublishWarning( "Thermal transient failed with outcome = {0};. ", ( object ) ( int ) outcome );
                }

                this.OnStateChanged();
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed Measuring Thermal Transient" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The MeasurementOutcomes. </returns>
        protected virtual MeasurementOutcomes MeasureFinalResistance()
        {
            return MeasurementOutcomes.None;
        }

        /// <summary> Measures the final resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FinalResistanceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Measuring final Resistance";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
                this.InfoProvider.SetIconPadding( this._TtmMeasureControlsToolStrip, -15 );
                _ = this.PublishVerbose( "Measuring Final Resistance...;. " );
                var outcome = this.MeasureFinalResistance();
                if ( MeasurementOutcomes.None == outcome || outcome == MeasurementOutcomes.PartPassed )
                {
                    _ = this.PublishVerbose( "Final Resistance measured;. " );
                }
                else
                {
                    this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed final resistance." );
                    _ = this.PublishWarning( "Final Resistance failed with outcome = {0};. ", ( object ) ( int ) outcome );
                }
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "Failed Measuring Final Resistance" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SEQUENCED MEASUREMENTS "

        private MeasureSequencer _MeasureSequencerInternal;

        private MeasureSequencer MeasureSequencerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MeasureSequencerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MeasureSequencerInternal != null )
                {
                    this._MeasureSequencerInternal.PropertyChanged -= this.MeasureSequencer_PropertyChanged;
                }

                this._MeasureSequencerInternal = value;
                if ( this._MeasureSequencerInternal != null )
                {
                    this._MeasureSequencerInternal.PropertyChanged += this.MeasureSequencer_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the sequencer. </summary>
        /// <value> The sequencer. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public MeasureSequencer MeasureSequencer
        {
            get => this.MeasureSequencerInternal;

            set => this.MeasureSequencerInternal = value;
        }

        /// <summary> Handles the measure sequencer property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( MeasureSequencer sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.MeasureSequencer.MeasurementSequenceState ):
                    {
                        this.OnMeasurementSequenceStateChanged( sender.MeasurementSequenceState );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _MeasureSequencer for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSequencer_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handler measure sequencer {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeasureSequencer_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as MeasureSequencer, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Updates the progress bar. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="state"> The state. </param>
        private void UpdateProgressbar( MeasurementSequenceState state )
        {

            // unhide the progress bar.
            this._TtmToolStripProgressBar.Visible = true;
            if ( state == MeasurementSequenceState.Failed || state == MeasurementSequenceState.Starting || state == MeasurementSequenceState.Idle )
            {
                this._TtmToolStripProgressBar.Value = this._TtmToolStripProgressBar.Minimum;
                this._TtmToolStripProgressBar.ToolTipText = "Failed";
            }
            else if ( state == MeasurementSequenceState.Completed )
            {
                this._TtmToolStripProgressBar.Value = this._TtmToolStripProgressBar.Maximum;
                this._TtmToolStripProgressBar.ToolTipText = "Completed";
            }
            else
            {
                this._TtmToolStripProgressBar.Value = this.MeasureSequencer.PercentProgress();
            }
        }

        /// <summary> Handles the change in measurement state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="state"> The state. </param>
        private void OnMeasurementSequenceStateChanged( MeasurementSequenceState state )
        {
            _ = this.PublishInfo( "Processing the {0} state;. ", state.Description() );
            this.UpdateProgressbar( state );
            switch ( state )
            {
                case MeasurementSequenceState.Aborted:
                    {
                        break;
                    }

                case MeasurementSequenceState.Completed:
                    {
                        break;
                    }

                case MeasurementSequenceState.Failed:
                    {
                        break;
                    }

                case MeasurementSequenceState.MeasureInitialResistance:
                    {
                        break;
                    }

                case MeasurementSequenceState.MeasureThermalTransient:
                    {
                        break;
                    }

                case MeasurementSequenceState.Idle:
                    {
                        this.OnStateChanged();
                        break;
                    }

                case var @case when @case == MeasurementSequenceState.None:
                    {
                        break;
                    }

                case MeasurementSequenceState.PostTransientPause:
                    {
                        this._IsTraceAvailable = true;
                        break;
                    }

                case MeasurementSequenceState.MeasureFinalResistance:
                    {
                        break;
                    }

                case MeasurementSequenceState.Starting:
                    {
                        this._IsTraceAvailable = false;
                        this.OnStateChanged();
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + state.ToString() );
                        break;
                    }
            }
        }

        /// <summary> Aborts the measurement sequence. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        private void AbortSequenceToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            _ = this.PublishVerbose( "Abort requested;. " );
            this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Abort );
        }

        /// <summary> Starts measurement sequence. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        private void MeasureAllToolStripMenuItem_Click( object sender, EventArgs e )
        {
            _ = this.PublishVerbose( "User action @{0};. ", System.Reflection.MethodBase.GetCurrentMethod().Name );
            _ = this.PublishVerbose( "Start requested;. " );
            this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
            this.InfoProvider.SetIconPadding( this._TtmMeasureControlsToolStrip, -15 );
            this.MeasureSequencer.StartMeasurementSequence();
        }

        #endregion

        #region " TRIGGERED MEASUREMENTS "

        /// <summary> Abort triggered measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected virtual void AbortTriggerSequenceIf()
        {
        }

        /// <summary> Event handler. Called by _AbortToolStripMenuItem for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AbortToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.InfoProvider.SetError( this._TtmMeasureControlsToolStrip, "" );
            this.AbortTriggerSequenceIf();
        }

        private TriggerSequencer _TriggerSequencerInternal;

        private TriggerSequencer TriggerSequencerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TriggerSequencerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TriggerSequencerInternal != null )
                {
                    this._TriggerSequencerInternal.PropertyChanged -= this.TriggerSequencer_PropertyChanged;
                }

                this._TriggerSequencerInternal = value;
                if ( this._TriggerSequencerInternal != null )
                {
                    this._TriggerSequencerInternal.PropertyChanged += this.TriggerSequencer_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the trigger sequencer. </summary>
        /// <value> The sequencer. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TriggerSequencer TriggerSequencer
        {
            get => this.TriggerSequencerInternal;

            set => this.TriggerSequencerInternal = value;
        }

        /// <summary> Handles the trigger sequencer property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( TriggerSequencer sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.TriggerSequencer.TriggerSequenceState ):
                    {
                        this.OnTriggerSequenceStateChanged( sender.TriggerSequenceState );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _TriggerSequencer for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSequencer_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handler trigger sequencer {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSequencer_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as TriggerSequencer, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> The last status bar message. </summary>
        protected string LastStatusBarMessage { get; set; }

        /// <summary> Handles the change in measurement state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="state"> The state. </param>
        private void OnTriggerSequenceStateChanged( TriggerSequenceState state )
        {
            this._TriggerActionToolStripLabel.Text = this.TriggerSequencer.ProgressMessage( this.LastStatusBarMessage );
            switch ( state )
            {
                case TriggerSequenceState.Aborted:
                    {
                        this.OnStateChanged();
                        break;
                    }

                case TriggerSequenceState.Stopped:
                    {
                        break;
                    }

                case TriggerSequenceState.Failed:
                    {
                        break;
                    }

                case TriggerSequenceState.WaitingForTrigger:
                    {
                        break;
                    }

                case TriggerSequenceState.MeasurementCompleted:
                    {
                        this._IsTraceAvailable = true;
                        break;
                    }

                case TriggerSequenceState.ReadingValues:
                    {
                        break;
                    }

                case TriggerSequenceState.Idle:
                    {
                        this.OnStateChanged();
                        this._WaitForTriggerToolStripMenuItem.Image = My.Resources.Resources.ViewRefresh7;
                        break;
                    }

                case var @case when @case == TriggerSequenceState.None:
                    {
                        break;
                    }

                case TriggerSequenceState.Starting:
                    {
                        this._IsTraceAvailable = false;
                        this.OnStateChanged();
                        this._AssertTriggerToolStripMenuItem.Enabled = true;
                        this._WaitForTriggerToolStripMenuItem.Image = My.Resources.Resources.MediaPlaybackStop2;
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + state.ToString() );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AssertTrigger()
        {
            _ = this.PublishVerbose( "Assert requested;. " );
            this.TriggerSequencer.AssertTrigger();
        }

        /// <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AssertTriggerToolStripMenuItem_Click( object sender, EventArgs e )
        {
            this.AssertTrigger();
        }

        /// <summary>
        /// Event handler. Called by _WaitForTriggerToolStripMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void WaitForTriggerToolStripMenuItem_CheckChanged( object sender, EventArgs e )
        {
            if ( this._WaitForTriggerToolStripMenuItem.Enabled )
            {
                if ( this.TriggerSequencerInternal.TriggerSequenceState == TriggerSequenceState.Idle )
                {
                    _ = this.PublishVerbose( "Starting triggered measurements;. " );
                    this.TriggerSequencerInternal.StartMeasurementSequence();
                }
                else if ( this.TriggerSequencerInternal.TriggerSequenceState == TriggerSequenceState.WaitingForTrigger )
                {
                    _ = this.PublishVerbose( "Stopping triggered measurements;. " );
                    this.TriggerSequencerInternal.Enqueue( TriggerSequenceSignal.Stop );
                }
            }
        }

        #endregion

        #region " TRACE LEVEL "

        /// <summary> The master device. </summary>
        private K2600Device _MasterDevice;

        /// <summary> Gets or sets the master device. </summary>
        /// <value> The master device. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public K2600Device MasterDevice
        {
            get => this._MasterDevice;

            set {
                this._MasterDevice = value;
                if ( value is null )
                {
                    this.DisableTraceLevelControls();
                }
                else
                {
                    this.EnableTraceLevelControls();
                }
            }
        }

        /// <summary> Disables the trace level controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void DisableTraceLevelControls()
        {
            this._LogTraceLevelComboBox.ComboBox.SelectedValueChanged -= this.LogTraceLevelComboBox_SelectedIndexChanged;
            this._DisplayTraceLevelComboBox.ComboBox.SelectedValueChanged -= this.DisplayTraceLevelComboBox_SelectedIndexChanged;
        }

        /// <summary> Enables the trace level controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void EnableTraceLevelControls()
        {
            ListTraceEventLevels( this._LogTraceLevelComboBox.ComboBox );
            this._LogTraceLevelComboBox.ComboBox.SelectedValueChanged += this.LogTraceLevelComboBox_SelectedIndexChanged;
            ListTraceEventLevels( this._DisplayTraceLevelComboBox.ComboBox );
            this._DisplayTraceLevelComboBox.ComboBox.SelectedValueChanged += this.DisplayTraceLevelComboBox_SelectedIndexChanged;
            ModelViewTalkerBase.SelectItem( this._LogTraceLevelComboBox, Ttm.My.MySettings.Default.TraceLogLevel );
            ModelViewTalkerBase.SelectItem( this._DisplayTraceLevelComboBox, Ttm.My.MySettings.Default.TraceShowLevel );
        }

        /// <summary>
        /// Event handler. Called by _LogTraceLevelComboBox for selected value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LogTraceLevelComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"selecting log trace level on this instrument only";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.MasterDevice.ApplyTalkerTraceLevel( ListenerType.Logger, ModelViewTalkerBase.SelectedValue( this._LogTraceLevelComboBox, Ttm.My.MySettings.Default.TraceLogLevel ) );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, ex.Message );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _DisplayTraceLevelComboBox for selected value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DisplayTraceLevelComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"selecting display trace level on this instrument only";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.MasterDevice.ApplyTalkerTraceLevel( ListenerType.Display, ModelViewTalkerBase.SelectedValue( this._DisplayTraceLevelComboBox, Ttm.My.MySettings.Default.TraceShowLevel ) );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, ex.Message );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
