using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> Thermal Transient Model header. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-02-25 </para>
    /// </remarks>
    public partial class ThermalTransientHeader : ModelViewBase
    {
        /// <summary>   Default constructor. </summary>
        /// <remarks>   David, 2021-09-04. </remarks>
        public ThermalTransientHeader()
        {
            this.InitializeComponent();
        }

        #region " DUT "

        private DeviceUnderTest _DeviceUnderTestInternal;

        private DeviceUnderTest DeviceUnderTestInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceUnderTestInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceUnderTestInternal != null )
                {
                    this._DeviceUnderTestInternal.PropertyChanged -= this.DeviceUnderTest_PropertyChanged;
                }

                this._DeviceUnderTestInternal = value;
                if ( this._DeviceUnderTestInternal != null )
                {
                    this._DeviceUnderTestInternal.PropertyChanged += this.DeviceUnderTest_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the device under test. </summary>
        /// <value> The device under test. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public DeviceUnderTest DeviceUnderTest
        {
            get => this.DeviceUnderTestInternal;

            set {
                this.DeviceUnderTestInternal = value;
                this.ThermalTransientInternal = value?.ThermalTransient;
            }
        }

        /// <summary> Releases the resources. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ReleaseResources()
        {
            this.ThermalTransientInternal = null;
            this.DeviceUnderTestInternal = null;
        }

        /// <summary> Executes the device under test property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( DeviceUnderTest sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.DeviceUnderTest.Outcome ):
                    {
                        if ( sender.Outcome == MeasurementOutcomes.None )
                        {
                            this.Clear();
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _DeviceUnderTest for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeviceUnderTest_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.DeviceUnderTest_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as DeviceUnderTest, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        #endregion

        #region " DISPLAY VALUE "

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void Clear()
        {
            this.InfoProvider.Clear();
            this._AsymptoteTextBox.Text = string.Empty;
            this._EstimatedVoltageTextBox.Text = string.Empty;
            this._IterationsCountTextBox.Text = string.Empty;
            this._CorrelationCoefficientTextBox.Text = string.Empty;
            this._StandardErrorTextBox.Text = string.Empty;
            this._TimeConstantTextBox.Text = string.Empty;
            this._OutcomeTextBox.Text = string.Empty;
        }

        #endregion

        #region " PART: THERMAL TRANSIENT "

        /// <summary> The Part Thermal Transient. </summary>
        private ThermalTransient _ThermalTransientInternal;

        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private ThermalTransient ThermalTransientInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ThermalTransientInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ThermalTransientInternal != null )
                {
                    this._ThermalTransientInternal.PropertyChanged -= this.ThermalTransient_PropertyChanged;
                }

                this._ThermalTransientInternal = value;
                if ( this._ThermalTransientInternal != null )
                {
                    this._ThermalTransientInternal.PropertyChanged += this.ThermalTransient_PropertyChanged;
                }
            }
        }

        /// <summary> Executes the device under test property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( ThermalTransient sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ThermalTransient.TimeConstant ):
                    {
                        this._TimeConstantTextBox.Text = sender.TimeConstantCaption;
                        break;
                    }

                case nameof( ThermalTransient.Asymptote ):
                    {
                        this._AsymptoteTextBox.Text = sender.AsymptoteCaption;
                        break;
                    }

                case nameof( ThermalTransient.EstimatedVoltage ):
                    {
                        this._EstimatedVoltageTextBox.Text = sender.EstimatedVoltageCaption;
                        break;
                    }

                case nameof( ThermalTransient.CorrelationCoefficient ):
                    {
                        this._CorrelationCoefficientTextBox.Text = sender.CorrelationCoefficientCaption;
                        break;
                    }

                case nameof( ThermalTransient.StandardError ):
                    {
                        this._StandardErrorTextBox.Text = sender.StandardErrorCaption;
                        break;
                    }

                case nameof( ThermalTransient.Iterations ):
                    {
                        this._IterationsCountTextBox.Text = sender.IterationsCaption;
                        break;
                    }

                case nameof( ThermalTransient.OptimizationOutcome ):
                    {
                        this._OutcomeTextBox.Text = sender.OptimizationOutcomeCaption;
                        this.ToolTip.SetToolTip( this._OutcomeTextBox, sender.OptimizationOutcomeDescription );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _ThermalTransient for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalTransient_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ThermalTransient_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as ThermalTransient, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
        }

        #endregion

    }
}
