using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class PartHeader
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _TableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _PartNumberTextBoxLabel = new System.Windows.Forms.Label();
            _PartNumberTextBox = new System.Windows.Forms.TextBox();
            _PartSerialNumberTextBoxLabel = new System.Windows.Forms.Label();
            _PartSerialNumberTextBox = new System.Windows.Forms.TextBox();
            _TableLayoutPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _TableLayoutPanel
            // 
            _TableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            _TableLayoutPanel.ColumnCount = 5;
            _TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TableLayoutPanel.Controls.Add(_PartNumberTextBoxLabel, 0, 1);
            _TableLayoutPanel.Controls.Add(_PartNumberTextBox, 1, 1);
            _TableLayoutPanel.Controls.Add(_PartSerialNumberTextBoxLabel, 3, 1);
            _TableLayoutPanel.Controls.Add(_PartSerialNumberTextBox, 4, 1);
            _TableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _TableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _TableLayoutPanel.Name = "_TableLayoutPanel";
            _TableLayoutPanel.RowCount = 3;
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TableLayoutPanel.Size = new System.Drawing.Size(670, 21);
            _TableLayoutPanel.TabIndex = 0;
            // 
            // _PartNumberTextBoxLabel
            // 
            _PartNumberTextBoxLabel.AutoSize = true;
            _PartNumberTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _PartNumberTextBoxLabel.Location = new System.Drawing.Point(3, 2);
            _PartNumberTextBoxLabel.Margin = new System.Windows.Forms.Padding(3);
            _PartNumberTextBoxLabel.Name = "_PartNumberTextBoxLabel";
            _PartNumberTextBoxLabel.Size = new System.Drawing.Size(98, 17);
            _PartNumberTextBoxLabel.TabIndex = 1;
            _PartNumberTextBoxLabel.Text = "PART NUMBER:";
            // 
            // _PartNumberTextBox
            // 
            _PartNumberTextBox.BackColor = System.Drawing.SystemColors.Desktop;
            _PartNumberTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _PartNumberTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartNumberTextBox.ForeColor = System.Drawing.Color.LawnGreen;
            _PartNumberTextBox.Location = new System.Drawing.Point(107, 2);
            _PartNumberTextBox.Name = "_PartNumberTextBox";
            _PartNumberTextBox.Size = new System.Drawing.Size(164, 18);
            _PartNumberTextBox.TabIndex = 2;
            _PartNumberTextBox.Text = "10";
            // 
            // _PartSerialNumberTextBoxLabel
            // 
            _PartSerialNumberTextBoxLabel.AutoSize = true;
            _PartSerialNumberTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _PartSerialNumberTextBoxLabel.Location = new System.Drawing.Point(541, 2);
            _PartSerialNumberTextBoxLabel.Margin = new System.Windows.Forms.Padding(3);
            _PartSerialNumberTextBoxLabel.Name = "_PartSerialNumberTextBoxLabel";
            _PartSerialNumberTextBoxLabel.Size = new System.Drawing.Size(33, 17);
            _PartSerialNumberTextBoxLabel.TabIndex = 3;
            _PartSerialNumberTextBoxLabel.Text = "S/N:";
            // 
            // _PartSerialNumberTextBox
            // 
            _PartSerialNumberTextBox.BackColor = System.Drawing.SystemColors.Desktop;
            _PartSerialNumberTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _PartSerialNumberTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PartSerialNumberTextBox.ForeColor = System.Drawing.Color.LawnGreen;
            _PartSerialNumberTextBox.Location = new System.Drawing.Point(580, 2);
            _PartSerialNumberTextBox.Name = "_PartSerialNumberTextBox";
            _PartSerialNumberTextBox.Size = new System.Drawing.Size(87, 18);
            _PartSerialNumberTextBox.TabIndex = 4;
            _PartSerialNumberTextBox.Text = "10";
            // 
            // PartHeader
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BackColor = System.Drawing.Color.Black;
            Controls.Add(_TableLayoutPanel);
            Name = "PartHeader";
            Size = new System.Drawing.Size(670, 21);
            _TableLayoutPanel.ResumeLayout(false);
            _TableLayoutPanel.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _TableLayoutPanel;
        private System.Windows.Forms.Label _PartNumberTextBoxLabel;
        private System.Windows.Forms.TextBox _PartNumberTextBox;
        private System.Windows.Forms.Label _PartSerialNumberTextBoxLabel;
        private System.Windows.Forms.TextBox _PartSerialNumberTextBox;
    }
}
