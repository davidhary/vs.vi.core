using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.Forma;
using isr.Core.NumericExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{

    /// <summary> A meter view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class MeterView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MeterView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();

            // note that the caption is not set if this is run inside the On Load function.
            // set defaults for the messages box.
            this._TraceMessagesBox.ResetCount = 500;
            this._TraceMessagesBox.PresetCount = 250;
            this._TraceMessagesBox.ContainerPanel = this._MessagesTabPage;
            this._TraceMessagesBox.AlertsToggleControl = this._MessagesTabPage;
            this._TraceMessagesBox.CommenceUpdates();
            this._Tabs.DrawMode = TabDrawMode.OwnerDrawFixed;
            base.AddPrivateListener( this._TraceMessagesBox );
            this.ApplyShuntResistanceButtonCaption = this._ApplyShuntResistanceConfigurationButton.Text;
            this.ApplyNewShuntResistanceButtonCaption = this._ApplyNewShuntResistanceConfigurationButton.Text;

            // hide the alerts
            this._MeasurementsHeader.ShowAlerts( false, false );
            this._MeasurementsHeader.ShowOutcome( false, false );
            this.InitializingComponents = false;
            this.__NavigatorTreeView.Name = "_NavigatorTreeView";
            this.__Tabs.Name = "_Tabs";
            this.__TTMConfigurationPanel.Name = "_TTMConfigurationPanel";
            this.__MeasureShuntResistanceButton.Name = "_MeasureShuntResistanceButton";
            this.__RestoreShuntResistanceDefaultsButton.Name = "_RestoreShuntResistanceDefaultsButton";
            this.__ApplyNewShuntResistanceConfigurationButton.Name = "_ApplyNewShuntResistanceConfigurationButton";
            this.__ApplyShuntResistanceConfigurationButton.Name = "_ApplyShuntResistanceConfigurationButton";
            this.__PartsPanel.Name = "_PartsPanel";
            this.__TraceMessagesBox.Name = "_TraceMessagesBox";
        }

        /// <summary> Creates a new <see cref="MeterView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="MeterView"/>. </returns>
        public static MeterView Create()
        {
            MeterView view = null;
            try
            {
                view = new MeterView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    this._TraceMessagesBox.SuspendUpdatesReleaseIndicators();
                    this.BindPart( null );
                    this.AssignMeter( null );
                    if ( this.Meter is object )
                    {
                        this.Meter.Dispose();
                        this.Meter = null;
                    }

                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " EVENT HANDLERS:  FORM "

        /// <summary>
        /// Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
        ///                  data. </param>
        protected override void OnFormClosing( CancelEventArgs e )
        {
            base.OnFormClosing( e );
            if ( e is object && !e.Cancel )
            {
                this.RemovePrivateListener( this._TraceMessagesBox );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"Loading the driver console form";
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                if ( !this.DesignMode )
                {
                    // add listeners before the first talker publish command
                    this.AddPrivateListeners();
                    this.AssignMeter( new Meter() );
                }

                _ = this.PublishVerbose( $"{activity};. " );

                // build the navigator tree.
                this.BuildNavigatorTreeView();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                if ( MyDialogResult.Abort == WindowsForms.ShowDialogAbortIgnore( ex ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                this.Cursor = Cursors.Default;
                Trace.CorrelationManager.StopLogicalOperation();
                ApplianceBase.DoEvents();
                this.SelectNavigatorTreeViewNode( TreeViewNode.ConnectNode );
                _ = this._ResourceSelectorConnector.Focus();
                ApplianceBase.DoEvents();
            }
        }

        #endregion

        #region " DEVICE UNDER TEST "

        /// <summary> Gets the device under test. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The device under test. </value>
        private DeviceUnderTest DeviceUnderTest { get; set; }

        #endregion

        #region " SHUNT "

        #region " CONFIGURE SHUNT"

        /// <summary> Bind controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void BindShuntControls()
        {

            // set the GUI based on the current defaults.
            Ttm.My.MySettings instrumentSettings = Ttm.My.MySettings.Default;
            this._ShuntResistanceCurrentRangeNumeric.Minimum = instrumentSettings.ShuntResistanceCurrentMinimum;
            this._ShuntResistanceCurrentRangeNumeric.Maximum = instrumentSettings.ShuntResistanceCurrentMaximum;
            this._ShuntResistanceCurrentRangeNumeric.DataBindings.Clear();
            this._ShuntResistanceCurrentRangeNumeric.DataBindings.Add( new Binding( "Value", this.Part.ShuntResistance, "CurrentRange", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ShuntResistanceCurrentRangeNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceCurrentRange.Clip( this._ShuntResistanceCurrentRangeNumeric.Minimum, this._ShuntResistanceCurrentRangeNumeric.Maximum );
            this.Part.ShuntResistance.CurrentRange = ( double ) this._ShuntResistanceCurrentRangeNumeric.Value;
            this._ShuntResistanceCurrentLevelNumeric.Minimum = instrumentSettings.ShuntResistanceCurrentMinimum;
            this._ShuntResistanceCurrentLevelNumeric.Maximum = instrumentSettings.ShuntResistanceCurrentMaximum;
            this._ShuntResistanceCurrentLevelNumeric.DataBindings.Clear();
            this._ShuntResistanceCurrentLevelNumeric.DataBindings.Add( new Binding( "Value", this.Part.ShuntResistance, "CurrentLevel", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ShuntResistanceCurrentLevelNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel.Clip( this._ShuntResistanceCurrentLevelNumeric.Minimum, this._ShuntResistanceCurrentLevelNumeric.Maximum );
            this.Part.ShuntResistance.CurrentLevel = ( double ) this._ShuntResistanceCurrentLevelNumeric.Value;
            this._ShuntResistanceHighLimitNumeric.Minimum = instrumentSettings.ShuntResistanceMinimum;
            this._ShuntResistanceHighLimitNumeric.Maximum = instrumentSettings.ShuntResistanceMaximum;
            this._ShuntResistanceHighLimitNumeric.DataBindings.Clear();
            this._ShuntResistanceHighLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.ShuntResistance, "HighLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ShuntResistanceHighLimitNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceHighLimit.Clip( this._ShuntResistanceHighLimitNumeric.Minimum, this._ShuntResistanceHighLimitNumeric.Maximum );
            this.Part.ShuntResistance.HighLimit = ( double ) this._ShuntResistanceHighLimitNumeric.Value;
            this._ShuntResistanceLowLimitNumeric.Minimum = instrumentSettings.ShuntResistanceMinimum;
            this._ShuntResistanceLowLimitNumeric.Maximum = instrumentSettings.ShuntResistanceMaximum;
            this._ShuntResistanceLowLimitNumeric.DataBindings.Clear();
            this._ShuntResistanceLowLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.ShuntResistance, "LowLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ShuntResistanceLowLimitNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceLowLimit.Clip( this._ShuntResistanceLowLimitNumeric.Minimum, this._ShuntResistanceLowLimitNumeric.Maximum );
            this.Part.ShuntResistance.LowLimit = ( double ) this._ShuntResistanceLowLimitNumeric.Value;
            this._ShuntResistanceVoltageLimitNumeric.Minimum = instrumentSettings.ShuntResistanceVoltageMinimum;
            this._ShuntResistanceVoltageLimitNumeric.Maximum = instrumentSettings.ShuntResistanceVoltageMaximum;
            this._ShuntResistanceVoltageLimitNumeric.DataBindings.Clear();
            this._ShuntResistanceVoltageLimitNumeric.DataBindings.Add( new Binding( "Value", this.Part.ShuntResistance, "VoltageLimit", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ShuntResistanceVoltageLimitNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit.Clip( this._ShuntResistanceVoltageLimitNumeric.Minimum, this._ShuntResistanceVoltageLimitNumeric.Maximum );
            this.Part.ShuntResistance.VoltageLimit = ( double ) this._ShuntResistanceVoltageLimitNumeric.Value;
        }

        /// <summary> Restore defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private static void RestoreShuntDefaults()
        {
            Ttm.My.MySettings.Default.ShuntResistanceCurrentRange = Ttm.My.MySettings.Default.ShuntResistanceCurrentRangeDefault;
            Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel = Ttm.My.MySettings.Default.ShuntResistanceCurrentLevelDefault;
            Ttm.My.MySettings.Default.ShuntResistanceHighLimit = Ttm.My.MySettings.Default.ShuntResistanceHighLimitDefault;
            Ttm.My.MySettings.Default.ShuntResistanceLowLimit = Ttm.My.MySettings.Default.ShuntResistanceLowLimitDefault;
            Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit = Ttm.My.MySettings.Default.ShuntResistanceVoltageLimitDefault;
        }

        /// <summary> Copy shunt values to the settings store. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void CopyShuntSettings()
        {
            if ( this.Part is object )
            {
                Ttm.My.MySettings.Default.ShuntResistanceCurrentRange = ( decimal ) this.Part.ShuntResistance.CurrentRange;
                Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel = ( decimal ) this.Part.ShuntResistance.CurrentLevel;
                Ttm.My.MySettings.Default.ShuntResistanceHighLimit = ( decimal ) this.Part.ShuntResistance.HighLimit;
                Ttm.My.MySettings.Default.ShuntResistanceLowLimit = ( decimal ) this.Part.ShuntResistance.LowLimit;
                Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit = ( decimal ) this.Part.ShuntResistance.VoltageLimit;
            }
        }


        /// <summary> Updates the shunt bound values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void UpdateShuntBoundValues()
        {
            if ( this.Meter is object && this.Meter.ShuntResistance is object )
            {
                Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel = ( decimal ) this.Meter.ShuntResistance.CurrentLevel;
                this._ShuntResistanceCurrentLevelNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel.Clip( this._ShuntResistanceCurrentLevelNumeric.Minimum, this._ShuntResistanceCurrentLevelNumeric.Maximum );
                Ttm.My.MySettings.Default.ShuntResistanceCurrentRange = ( decimal ) this.Meter.ShuntResistance.CurrentRange;
                this._ShuntResistanceCurrentRangeNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceCurrentRange.Clip( this._ShuntResistanceCurrentRangeNumeric.Minimum, this._ShuntResistanceCurrentRangeNumeric.Maximum );
                Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit = ( decimal ) this.Meter.ShuntResistance.VoltageLimit;
                this._ShuntResistanceVoltageLimitNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit.Clip( this._ShuntResistanceVoltageLimitNumeric.Minimum, this._ShuntResistanceVoltageLimitNumeric.Maximum );
                Ttm.My.MySettings.Default.ShuntResistanceHighLimit = ( decimal ) this.Meter.ShuntResistance.HighLimit;
                this._ShuntResistanceHighLimitNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceHighLimit.Clip( this._ShuntResistanceHighLimitNumeric.Minimum, this._ShuntResistanceHighLimitNumeric.Maximum );
                Ttm.My.MySettings.Default.ShuntResistanceLowLimit = ( decimal ) this.Meter.ShuntResistance.LowLimit;
                this._ShuntResistanceLowLimitNumeric.Value = Ttm.My.MySettings.Default.ShuntResistanceLowLimit.Clip( this._ShuntResistanceLowLimitNumeric.Minimum, this._ShuntResistanceLowLimitNumeric.Maximum );
            }
        }

        /// <summary> Gets the apply new shunt resistance button caption. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The apply shunt resistance button caption. </value>
        private string ApplyNewShuntResistanceButtonCaption { get; set; }

        /// <summary> Gets the apply shunt resistance button caption. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The apply shunt resistance button caption. </value>
        private string ApplyShuntResistanceButtonCaption { get; set; }

        /// <summary> Is new shunt resistance settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// <c>True</c> if settings where updated so that meter settings needs to be updated.
        /// </returns>
        private bool IsNewShuntResistanceSettings()
        {
            return this.DeviceUnderTest is object && !this.Part.ShuntResistance.ConfigurationEquals( this.DeviceUnderTest.ShuntResistance );
        }

        /// <summary> Updates the shunt configuration button caption. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void UpdateShuntConfigButtonCaption()
        {
            string caption = this.ApplyShuntResistanceButtonCaption;
            string changedCaption = this.ApplyNewShuntResistanceButtonCaption;
            if ( this.IsNewShuntResistanceSettings() )
            {
                caption += " !";
                changedCaption += " !";
            }

            if ( !caption.Equals( this._ApplyShuntResistanceConfigurationButton.Text ) )
            {
                this._ApplyShuntResistanceConfigurationButton.Text = caption;
            }

            if ( !changedCaption.Equals( this._ApplyNewShuntResistanceConfigurationButton.Text ) )
            {
                this._ApplyNewShuntResistanceConfigurationButton.Text = changedCaption;
            }
        }

        /// <summary>
        /// Handles the Click event of the _ApplyShuntResistanceConfigurationButton control. Saves the
        /// configuration settings and sends them to the meter.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyShuntResistanceConfigurationButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} Configuring Shunt Resistance";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishVerbose( "Configuring Shunt Resistance;. " );
                if ( this.Meter.IsDeviceOpen )
                {

                    // not required. 
                    // Me.Meter.ClearExecutionState()
                    _ = this.PublishVerbose( "Sending Shunt resistance configuration settings to the meter;. " );
                    this.Meter.ConfigureShuntResistance( this.Part.ShuntResistance );
                    _ = this.PublishVerbose( "Shunt resistance measurement configured successfully;. " );
                }
                else
                {
                    this.InfoProvider.SetError( this._ApplyShuntResistanceConfigurationButton, "Meter not connected" );
                    _ = this.PublishWarning( "Meter not connected;. " );
                }
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._ApplyShuntResistanceConfigurationButton, "Failed configuring shunt resistance" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.UpdateShuntConfigButtonCaption();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Applies new shunt resistance settings.
        ///           Event handler. Called by _ApplyNewShuntResistanceConfigurationButton for click
        /// events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyNewShuntResistanceConfigurationButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} Configuring Shunt Resistance";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishVerbose( "Configuring Shunt Resistance;. " );
                if ( this.Meter.IsDeviceOpen )
                {

                    // not required. 
                    // Me.Meter.ClearExecutionState()
                    _ = this.PublishVerbose( "Sending Shunt resistance configuration settings to the meter;. " );
                    this.Meter.ConfigureShuntResistanceChanged( this.Part.ShuntResistance );
                    _ = this.PublishVerbose( "Shunt resistance measurement configured successfully;. " );
                }
                else
                {
                    this.InfoProvider.SetError( this._ApplyShuntResistanceConfigurationButton, "Meter not connected" );
                    _ = this.PublishWarning( "Meter not connected;. " );
                }
            }
            catch ( Exception ex )
            {
                this.InfoProvider.SetError( this._ApplyShuntResistanceConfigurationButton, "Failed configuring shunt resistance" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.UpdateShuntConfigButtonCaption();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _RestoreShuntResistanceDefaultsButton for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void RestoreShuntResistanceDefaultsButton_Click( object sender, EventArgs e )
        {

            // read the instrument default settings.
            RestoreShuntDefaults();

            // reset part to know state based on the current defaults
            this.Part.ShuntResistance.ResetKnownState();
            ApplianceBase.DoEvents();

            // bind.
            this.BindShuntControls();
        }


        #endregion

        #region " MEASURE SHUNT "

        /// <summary> Measures Shunt resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureShuntResistanceButton_Click( object sender, EventArgs e )
        {
            lock ( this.Meter )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"{this.Meter.ResourceName} measuring Shunt Resistance";
                    this.Cursor = Cursors.WaitCursor;
                    this.InfoProvider.SetError( this._ShuntResistanceTextBox, "" );
                    this.InfoProvider.SetError( this._MeasureShuntResistanceButton, "" );
                    this.InfoProvider.SetIconPadding( this._MeasureShuntResistanceButton, -15 );
                    _ = this.PublishVerbose( "Measuring Shunt Resistance...;. " );
                    this.Meter.MeasureShuntResistance( this.Part.ShuntResistance );
                    _ = this.PublishVerbose( "Shunt Resistance measured;. " );
                }
                catch ( Exception ex )
                {
                    this._ShuntResistanceTextBox.Text = string.Empty;
                    this.InfoProvider.SetError( this._MeasureShuntResistanceButton, "Failed Measuring Shunt Resistance" );
                    _ = this.PublishException( activity, ex );
                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }
            }
        }

        #endregion

        #endregion

        #region " DISPLAY "

        /// <summary> Displays the thermal transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox">    The text box control. </param>
        /// <param name="resistance"> The resistance. </param>
        private void SetErrorProvider( TextBox textBox, ResistanceMeasureBase resistance )
        {
            if ( (resistance.Outcome & MeasurementOutcomes.PartFailed) != 0 )
            {
                this.InfoProvider.SetError( textBox, "Value out of range" );
            }
            else if ( (resistance.Outcome & MeasurementOutcomes.MeasurementFailed) != 0 )
            {
                this.InfoProvider.SetError( textBox, "Measurement failed" );
            }
            else if ( (resistance.Outcome & MeasurementOutcomes.MeasurementNotMade) != 0 )
            {
                this.InfoProvider.SetError( textBox, "Measurement not made" );
            }
        }

        /// <summary> Displays the resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox">    The text box control. </param>
        /// <param name="resistance"> The resistance. </param>
        private void ShowResistance( TextBox textBox, ResistanceMeasureBase resistance )
        {
            textBox.Text = resistance.ResistanceCaption;
            this.SetErrorProvider( textBox, resistance );
        }


        /// <summary> Displays the thermal transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="textBox">    The text box control. </param>
        /// <param name="resistance"> The resistance. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void ShowThermalTransient( TextBox textBox, ResistanceMeasureBase resistance )
        {
            textBox.Text = resistance.VoltageCaption;
            this.SetErrorProvider( textBox, resistance );
        }

        #endregion

        #region " PART "

        /// <summary> Gets the part. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The part. </value>
        private DeviceUnderTest Part { get; set; }

        /// <summary> Bind part. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void BindPart( DeviceUnderTest value )
        {
            if ( this.Part is object )
            {
                this._ResourceSelectorConnector.Enabled = false;
            }

            this.Part = value;
            if ( value is object )
            {
                this._ResourceSelectorConnector.Openable = true;
                this._ResourceSelectorConnector.Clearable = true;
                this._ResourceSelectorConnector.Searchable = true;
                _ = this.PublishInfo( "Enabling controls;. " );
            }

            this.BindShuntResistance( value );
            this.OnMeasurementStatusChanged();
            if ( value is object )
            {
                _ = this.PublishInfo( "Ready - List resources and select one to connect too;. " );
                this._ResourceSelectorConnector.Enabled = true;
            }
        }

        /// <summary> Query if 'e' is closing ready. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Cancel event information. </param>
        /// <returns> True if closing ready, false if not. </returns>
        public bool IsClosingReady( CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            try
            {
                if ( this._PartsPanel.SaveEnabled )
                {
                    var dialogResult = System.Windows.Forms.MessageBox.Show( "Data not saved. Select Yes to save, no to skip saving or cancel to cancel closing the program.", "SAVE DATA?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
                    if ( dialogResult == DialogResult.Yes )
                    {
                        this._PartsPanel.SaveParts();
                    }
                    else if ( dialogResult == DialogResult.No )
                    {
                    }
                    else if ( dialogResult == DialogResult.Cancel )
                    {
                        if ( e is object )
                        {
                            e.Cancel = true;
                        }
                    }
                    else if ( e is object )
                    {
                        e.Cancel = true;
                    }
                }

                if ( e is null || !e.Cancel )
                {
                    this._PartsPanel.CopySettings();
                    this._TTMConfigurationPanel.CopySettings();
                    this.CopyShuntSettings();
                    Ttm.My.MySettings.Default.ResourceName = this.ResourceName;
                    Ttm.My.MySettings.Default.Save();

                    // flush the log.
                    My.MyProject.Application.Log.TraceSource.Flush();

                    // wait for timer to terminate all is actions
                    ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 400d ) );

                    // allow all events requiring the panel to execute on their thread.
                    // this allows all timer events that where in progress to be consummated before closing the form.
                    // this does not prevent timer exceptions in design mode.
                    for ( int i = 1; i <= 1000; i++ )
                        Application.DoEvents();
                }
            }
            finally
            {
                ApplianceBase.DoEvents();
            }

            return !e.Cancel;
        }

        #endregion

        #region " PART: SHUNT RESISTANCE "

        /// <summary> Gets the shunt resistance. </summary>
        /// <value> The shunt resistance. </value>
        private ShuntResistance ShuntResistance { get; set; }

        /// <summary> Bind shunt resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void BindShuntResistance( DeviceUnderTest value )
        {
            if ( this.ShuntResistance is object )
            {
                this.ShuntResistance.PropertyChanged -= this.ShuntResistancePropertyChanged;
                this.ShuntResistance = null;
            }

            if ( value is object )
            {
                this.ShuntResistance = value.ShuntResistance;
                this.ShuntResistance.PropertyChanged += this.ShuntResistancePropertyChanged;
                this.HandlePropertyChanged( this.ShuntResistance, nameof( Ttm.ShuntResistance.MeasurementAvailable ) );
            }
        }

        /// <summary> Raises the property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ShuntResistance sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            this.UpdateShuntConfigButtonCaption();
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.ShuntResistance.MeasurementAvailable ):
                    {
                        if ( sender.MeasurementAvailable )
                        {
                            this.ShowResistance( this._ShuntResistanceTextBox, sender );
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _ShuntResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ShuntResistancePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling shunt resistance {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ShuntResistancePropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ShuntResistance, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " PARTS "

        /// <summary> Parts panel property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        private void PartsPanel_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object && !string.IsNullOrWhiteSpace( e.PropertyName ) )
            {
            }
        }

        /// <summary> Raises the property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( ConfigurationView sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ConfigurationView.IsNewConfigurationSettingAvailable ):
                    {
                        if ( this._NavigatorTreeView.Nodes is object && this._NavigatorTreeView.Nodes.Count > 0 )
                        {
                            string caption = TreeViewNode.ConfigureNode.Description();
                            if ( sender.IsNewConfigurationSettingAvailable )
                            {
                                caption += " *";
                            }

                            this._NavigatorTreeView.Nodes[TreeViewNode.ConfigureNode.ToString()].Text = caption;
                        }

                        break;
                    }
            }
        }

        /// <summary> Ttm configuration panel property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TTMConfigurationPanel_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling configuration panel {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TTMConfigurationPanel_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as ConfigurationView, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TTM METER "

        /// <summary> Gets reference to the thermal transient meter device. </summary>
        /// <value> The meter. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Meter Meter { get; private set; }

        /// <summary> Assigns a Meter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void AssignMeterThis( Meter value )
        {
            if ( this.Meter is object )
            {
                this.AssignTalker( null );
                this.Meter.PropertyChanged -= this.MeterPropertyChanged;
                this.Meter.Dispose();
                this.Meter = null;
            }

            this.Meter = value;
            if ( this.Meter is object )
            {
                this.AssignTalker( this.Meter.Talker );
                this.Meter.AddListeners( this.Talker );
                this.AddPrivateListeners();
                this.Meter.PropertyChanged += this.MeterPropertyChanged;
                this.Meter.MasterDevice.Enabled = true;
            }

            this.BindPart( new DeviceUnderTest() );
            this.AssignDevice( this.Meter?.MasterDevice );
            ApplianceBase.DoEvents();
        }

        /// <summary> Gets the is meter that owns this item. </summary>
        /// <value> The is meter owner. </value>
        private bool IsMeterOwner { get; set; }

        /// <summary> Assigns a Meter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignMeter( Meter value )
        {
            this.IsMeterOwner = false;
            this.AssignMeterThis( value );
        }

        /// <summary> Releases the Meter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected void ReleaseMeter()
        {
            if ( this.IsMeterOwner )
            {
                this.Meter?.Dispose();
                this.Meter = null;
            }
            else
            {
                this.Meter = null;
            }
        }

        /// <summary> Gets the is Meter assigned. </summary>
        /// <value> The is Meter assigned. </value>
        public bool IsMeterAssigned => this.Meter is object && !this.Meter.IsDisposed;

        /// <summary> Raises the property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HanldePropertyChanged( Meter sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.Meter.MeasurementCompleted ):
                    {
                        if ( sender.MeasurementCompleted )
                        {
                            _ = this.PublishInfo( "{0} measurement completed;. ", sender.ResourceName );
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _meter for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeterPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling meter {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeterPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HanldePropertyChanged( sender as Meter, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets the master device. </summary>
        /// <value> The master device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2600Device MasterDevice => this.Device;

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2600Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2600Device value )
        {
            if ( this.Device is object )
            {
                this.Device.Opening -= this.DeviceOpening;
                this.Device.Opened -= this.DeviceOpened;
                this.Device.Closing -= this.DeviceClosing;
                this.Device.Closed -= this.DeviceClosed;
                this.Device.Initialized -= this.DeviceInitialized;
                this.Device.Initializing -= this.DeviceInitializing;
                this.Device.SessionFactory.PropertyChanged -= this.SessionFactoryPropertyChanged;
                this._ResourceSelectorConnector.AssignSelectorViewModel( null );
                this._ResourceSelectorConnector.AssignOpenerViewModel( null );
                this._ResourceSelectorConnector.AssignTalker( null );
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
                this._ResourceSelectorConnector.AssignSelectorViewModel( this.Device.SessionFactory );
                this._ResourceSelectorConnector.AssignOpenerViewModel( value );
                this._ResourceSelectorConnector.AssignTalker( this.Device.Talker );
                this.Device.Opening += this.DeviceOpening;
                this.Device.Opened += this.DeviceOpened;
                this.Device.Closing += this.DeviceClosing;
                this.Device.Closed += this.DeviceClosed;
                this.Device.Initialized += this.DeviceInitialized;
                this.Device.Initializing += this.DeviceInitializing;
                this.Device.SessionFactory.PropertyChanged += this.SessionFactoryPropertyChanged;
                this.Device.SessionFactory.CandidateResourceName = Ttm.My.MySettings.Default.ResourceName;
                if ( this.Device.IsDeviceOpen )
                {
                    this.DeviceOpened( this.Device, EventArgs.Empty );
                }
                else
                {
                    this.DeviceClosed( this.Device, EventArgs.Empty );
                }
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2600Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #region " SESSION FACTORY "

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        public string ResourceName
        {
            get => this.Device.SessionFactory.CandidateResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceName ) )
                {
                    this.Device.SessionFactory.CandidateResourceName = value;
                }
            }
        }

        /// <summary> Gets or sets the Search Pattern of the resource. </summary>
        /// <value> The Search Pattern of the resource. </value>
        public string ResourceFilter
        {
            get => this.Device.SessionFactory.ResourcesFilter;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceFilter ) )
                {
                    this.Device.SessionFactory.ResourcesFilter = value;
                }
            }
        }

        /// <summary> Executes the session factory property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SessionFactory sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SessionFactory.ValidatedResourceName ):
                    {
                        if ( sender.IsOpen )
                        {
                            this._IdentityTextBox.Text = $"Resource {sender.ValidatedResourceName} connected";
                            _ = this.PublishInfo( $"Resource connected;. {sender.ValidatedResourceName}" );
                        }
                        else
                        {
                            this._IdentityTextBox.Text = $"Resource {sender.ValidatedResourceName} located";
                            _ = this.PublishInfo( $"Resource locate;. {sender.ValidatedResourceName}" );
                        }

                        break;
                    }

                case nameof( SessionFactory.CandidateResourceName ):
                    {
                        if ( !sender.IsOpen )
                        {
                            this._IdentityTextBox.Text = $"Resource {sender.ValidatedResourceName}";
                            _ = this.PublishInfo( $"Candidate resource;. {sender.ValidatedResourceName}" );
                        }

                        break;
                    }
            }
        }

        /// <summary> Session factory property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionFactoryPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling session factory {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SessionFactoryPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SessionFactory, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE EVENTS "

        /// <summary>
        /// Event handler. Called upon device opening so as to instantiated all subsystems.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected void DeviceOpening( object sender, CancelEventArgs e )
        {
        }

        /// <summary> Updates the availability of the controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void OnMeasurementStatusChanged()
        {
            var measurementSequenceState = MeasurementSequenceState.Idle;
            if ( this.MeasureSequencer is object )
            {
                measurementSequenceState = this.MeasureSequencer.MeasurementSequenceState;
            }

            var triggerSequenceState = TriggerSequenceState.Idle;
            if ( this.TriggerSequencer is object )
            {
                triggerSequenceState = this.TriggerSequencer.TriggerSequenceState;
            }

            bool enabled = this.Meter is object && this.Meter.IsDeviceOpen && TriggerSequenceState.Idle == triggerSequenceState && MeasurementSequenceState.Idle == measurementSequenceState;
            this._ConnectGroupBox.Enabled = TriggerSequenceState.Idle == triggerSequenceState && MeasurementSequenceState.Idle == measurementSequenceState;
            this._TTMConfigurationPanel.Enabled = enabled;
            this._MeasureShuntResistanceButton.Enabled = enabled;
            this._ApplyShuntResistanceConfigurationButton.Enabled = enabled;
            this._ApplyNewShuntResistanceConfigurationButton.Enabled = enabled;
        }

        /// <summary>
        /// Event handler. Called after the device opened and all subsystems were defined.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        ///                                             <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void DeviceOpened( object sender, EventArgs e )
        {
            var outcome = TraceEventType.Information;
            if ( this.Device.Session.Enabled & !this.Device.Session.IsSessionOpen )
                outcome = TraceEventType.Warning;
            _ = this.Publish( outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ", this.Device.ResourceTitleCaption, this.Device.Session.Enabled.GetHashCode(), this.Device.Session.IsDeviceOpen.GetHashCode(), this.Device.Session.IsSessionOpen.GetHashCode() );
            string activity = string.Empty;
            try
            {
                activity = "applying device under test configuration";
                // set reference tot he device under test.
                this.DeviceUnderTest = this.Meter.ConfigInfo;
                if ( this.SupportsParts )
                    this._PartHeader.DeviceUnderTest = this.Part;
                this._MeasurementsHeader.DeviceUnderTest = this.Part;
                this._MeasurementsHeader.Clear();
                this._ThermalTransientHeader.DeviceUnderTest = this.Part;
                this._ThermalTransientHeader.Clear();
                this._TTMConfigurationPanel.Meter = this.Meter;
                this._MeasurementPanel.Meter = this.Meter;
                if ( this.SupportsParts )
                    this._PartsPanel.Part = this.Part;
                this.Part.ClearPartInfo();
                this.Part.ClearMeasurements();
                if ( this.SupportsParts )
                    this._PartsPanel.ClearParts();
                if ( this.SupportsParts )
                    this._PartsPanel.ApplySettings();
                this.Meter.Part = this.Part;
                this.MeasureSequencer = this.Meter.MeasureSequencer;
                this.TriggerSequencer = this.Meter.TriggerSequencer;

                // initialize the device system state.
                activity = "Clearing master device active state";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Meter.MasterDevice.ClearActiveState();
                ApplianceBase.DoEvents();
                activity = $"Reading identity from {this.ResourceName}";
                _ = this.PublishVerbose( $"{activity};. " );
                this._IdentityTextBox.Text = this.Meter.MasterDevice.StatusSubsystem.QueryIdentity();
                ApplianceBase.DoEvents();
                activity = $"Resetting and Clearing meter";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Meter.ResetClear();
                ApplianceBase.DoEvents();

                // reset part to know state based on the current defaults
                this.Part.ResetKnownState();
                ApplianceBase.DoEvents();
                this._TTMConfigurationPanel.Part = this.Part;
                ApplianceBase.DoEvents();
                activity = $"binding controls";
                _ = this.PublishVerbose( $"{activity};. " );
                this.BindShuntControls();
                activity = $"enables controls";
                _ = this.PublishVerbose( $"{activity};. " );
                this.OnMeasurementStatusChanged();
                activity = $"Connected to {this.ResourceName}";
                _ = this.PublishVerbose( $"{activity};. " );
                ApplianceBase.DoEvents();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }


        #endregion

        #region " OPENING / OPEN "

        /// <summary>
        /// Attempts to open a session to the device using the specified resource name.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The title. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryOpenDeviceSession( string resourceName, string resourceTitle )
        {
            return this.Device.TryOpenSession( resourceName, resourceTitle );
        }

        #endregion

        #region " INITALIZING / INITIALIZED  "

        /// <summary> Device initializing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        protected virtual void DeviceInitializing( object sender, CancelEventArgs e )
        {
        }

        /// <summary> Device initialized. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected virtual void DeviceInitialized( object sender, EventArgs e )
        {
        }

        #endregion

        #region " CLOSING / CLOSED "

        /// <summary> Event handler. Called when device is closing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        ///                       <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosing( object sender, CancelEventArgs e )
        {
            if ( this.Device is object )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"Disabling meter timer";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this._MeterTimer.Enabled = false;
                    activity = $"Disconnecting from {this.ResourceName}";
                    _ = this.PublishInfo( $"{activity};. " );
                    if ( this.Device.Session is object )
                        this.Device.Session.DisableServiceRequestEventHandler();
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        /// <summary> Event handler. Called when device is closed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this
        ///                       <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosed( object sender, EventArgs e )
        {
            if ( this.Device is object )
            {
                string activity = string.Empty;
                try
                {
                    _ = this.Device.Session.IsSessionOpen
                        ? this.PublishWarning( $"{this.Device.Session.ResourceNameCaption} closed but session still open;. " )
                        : this.Device.Session.IsDeviceOpen
                            ? this.PublishWarning( $"{this.Device.Session.ResourceNameCaption} closed but emulated session still open;. " )
                            : this.PublishVerbose( "Disconnected; Device access closed." );

                    activity = $"disables controls";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.OnMeasurementStatusChanged();
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
            else
            {
                _ = this.PublishInfo( "Disconnected; Device disposed." );
            }
        }

        #endregion

        #endregion

        #region " SEQUENCED MEASUREMENTS "

        private MeasureSequencer _MeasureSequencerInternal;

        private MeasureSequencer MeasureSequencerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MeasureSequencerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MeasureSequencerInternal != null )
                {
                    this._MeasureSequencerInternal.PropertyChanged -= this.MeasureSequencer_PropertyChanged;
                }

                this._MeasureSequencerInternal = value;
                if ( this._MeasureSequencerInternal != null )
                {
                    this._MeasureSequencerInternal.PropertyChanged += this.MeasureSequencer_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the sequencer. </summary>
        /// <value> The sequencer. </value>
        private MeasureSequencer MeasureSequencer
        {
            get => this.MeasureSequencerInternal;

            set => this.MeasureSequencerInternal = value;
        }

        /// <summary> Handles the measure sequencer property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( MeasureSequencer sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.MeasureSequencer.MeasurementSequenceState ):
                    {
                        this.OnMeasurementSequenceStateChanged( sender.MeasurementSequenceState );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _MeasureSequencer for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSequencer_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling measure sequencer {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeasureSequencer_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as MeasureSequencer, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Ends a completed sequence. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void OnMeasurementSequenceCompleted()
        {
            _ = this.PublishInfo( "Measurement completed;. " );
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling measure completed event";
                // add part if auto add is enabled.
                if ( this._PartsPanel.AutoAddEnabled )
                {
                    activity = $"{this.Meter.ResourceName} adding part";
                    this._PartsPanel.AddPart();
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> State of the last measurement sequence. </summary>
        private MeasurementSequenceState _LastMeasurementSequenceState;

        /// <summary> Handles the change in measurement state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="state"> The state. </param>
        private void OnMeasurementSequenceStateChanged( MeasurementSequenceState state )
        {
            if ( this._LastMeasurementSequenceState != state )
            {
                _ = this.PublishInfo( "Processing the {0} state;. ", state.Description() );
                this._LastMeasurementSequenceState = state;
            }

            switch ( state )
            {
                case MeasurementSequenceState.Aborted:
                    {
                        break;
                    }

                case MeasurementSequenceState.Completed:
                    {
                        break;
                    }

                case MeasurementSequenceState.Failed:
                    {
                        break;
                    }

                case MeasurementSequenceState.MeasureInitialResistance:
                    {
                        break;
                    }

                case MeasurementSequenceState.MeasureThermalTransient:
                    {
                        break;
                    }

                case MeasurementSequenceState.Idle:
                    {
                        this.OnMeasurementStatusChanged();
                        break;
                    }

                case var @case when @case == MeasurementSequenceState.None:
                    {
                        break;
                    }

                case MeasurementSequenceState.PostTransientPause:
                    {
                        break;
                    }

                case MeasurementSequenceState.MeasureFinalResistance:
                    {
                        break;
                    }

                case MeasurementSequenceState.Starting:
                    {
                        this.OnMeasurementStatusChanged();
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + state.ToString() );
                        break;
                    }
            }
        }

        #endregion

        #region " TRIGGERED MEASUREMENTS "

        private TriggerSequencer _TriggerSequencerInternal;

        private TriggerSequencer TriggerSequencerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TriggerSequencerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TriggerSequencerInternal != null )
                {
                    this._TriggerSequencerInternal.PropertyChanged -= this.TriggerSequencer_PropertyChanged;
                }

                this._TriggerSequencerInternal = value;
                if ( this._TriggerSequencerInternal != null )
                {
                    this._TriggerSequencerInternal.PropertyChanged += this.TriggerSequencer_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the trigger sequencer. </summary>
        /// <value> The sequencer. </value>
        private TriggerSequencer TriggerSequencer
        {
            get => this.TriggerSequencerInternal;

            set => this.TriggerSequencerInternal = value;
        }

        /// <summary> Handles the trigger sequencer property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( TriggerSequencer sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.TriggerSequencer.TriggerSequenceState ):
                    {
                        this.OnTriggerSequenceStateChanged( sender.TriggerSequenceState );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _TriggerSequencer for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSequencer_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling trigger sequencer {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSequencer_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.OnPropertyChanged( sender as TriggerSequencer, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> State of the last trigger sequence. </summary>
        private TriggerSequenceState _LastTriggerSequenceState;

        /// <summary> Handles the change in measurement state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="state"> The state. </param>
        private void OnTriggerSequenceStateChanged( TriggerSequenceState state )
        {
            if ( this._LastTriggerSequenceState != state )
            {
                _ = this.PublishInfo( "Processing the {0} state;. ", state.Description() );
                this._LastTriggerSequenceState = state;
            }

            switch ( state )
            {
                case TriggerSequenceState.Aborted:
                    {
                        this.OnMeasurementStatusChanged();
                        break;
                    }

                case TriggerSequenceState.Stopped:
                    {
                        break;
                    }

                case TriggerSequenceState.Failed:
                    {
                        break;
                    }

                case TriggerSequenceState.WaitingForTrigger:
                    {
                        break;
                    }

                case TriggerSequenceState.MeasurementCompleted:
                    {
                        break;
                    }

                case TriggerSequenceState.ReadingValues:
                    {
                        break;
                    }

                case TriggerSequenceState.Idle:
                    {
                        this.OnMeasurementStatusChanged();
                        break;
                    }

                case var @case when @case == TriggerSequenceState.None:
                    {
                        break;
                    }

                case TriggerSequenceState.Starting:
                    {
                        this.OnMeasurementStatusChanged();
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + state.ToString() );
                        break;
                    }
            }
        }

        #endregion

        #region " TAB CONTROL EVENTS "

        /// <summary> Tabs draw item. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Draw item event information. </param>
        private void Tabs_DrawItem( object sender, DrawItemEventArgs e )
        {
            var page = this._Tabs.TabPages[e.Index];
            var paddedBounds = e.Bounds;
            var backClr = e.State == DrawItemState.Selected ? System.Drawing.SystemColors.ControlDark : page.BackColor;
            using ( System.Drawing.Brush brush = new System.Drawing.SolidBrush( backClr ) )
            {
                e.Graphics.FillRectangle( brush, paddedBounds );
            }

            int yOffset = e.State == DrawItemState.Selected ? -2 : 1;
            paddedBounds = e.Bounds;
            paddedBounds.Offset( 1, yOffset );
            TextRenderer.DrawText( e.Graphics, page.Text, page.Font, paddedBounds, page.ForeColor );
        }

        #endregion

        #region " NAVIGATION "

        /// <summary> Gets the support parts. </summary>
        /// <value> The support parts. </value>
        public bool SupportsParts { get; set; }

        /// <summary> Gets the supports shunt. </summary>
        /// <value> The supports shunt. </value>
        public bool SupportsShunt { get; set; }

        /// <summary> Enumerates the nodes. Each item is the same as the node name. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private enum TreeViewNode
        {

            /// <summary> An enum constant representing the connect node option. </summary>
            [Description( "CONNECT" )]
            ConnectNode,

            /// <summary> An enum constant representing the configure node option. </summary>
            [Description( "CONFIGURE" )]
            ConfigureNode,

            /// <summary> An enum constant representing the measure node option. </summary>
            [Description( "MEASURE" )]
            MeasureNode,

            /// <summary> An enum constant representing the shunt node option. </summary>
            [Description( "SHUNT" )]
            ShuntNode,

            /// <summary> An enum constant representing the parts node option. </summary>
            [Description( "PARTS" )]
            PartsNode,

            /// <summary> An enum constant representing the messages node option. </summary>
            [Description( "MESSAGES" )]
            MessagesNode
        }

        /// <summary> Builds the navigator tree view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void BuildNavigatorTreeView()
        {
            this._PartHeader.Visible = this.SupportsParts;
            this._SplitContainer.Dock = DockStyle.Fill;
            this._SplitContainer.SplitterDistance = 120;
            this._NavigatorTreeView.Enabled = false;
            this._NavigatorTreeView.Nodes.Clear();
            var nodes = new List<TreeNode>();
            foreach ( TreeViewNode node in Enum.GetValues( typeof( TreeViewNode ) ) )
            {
                if ( node == TreeViewNode.PartsNode && !this.SupportsParts )
                    continue;
                if ( node == TreeViewNode.ShuntNode && !this.SupportsShunt )
                    continue;
                nodes.Add( new TreeNode( node.Description() ) );
                nodes[nodes.Count - 1].Name = node.ToString();
                nodes[nodes.Count - 1].Text = node.Description();
                if ( node == TreeViewNode.MessagesNode )
                {
                    this._TraceMessagesBox.ContainerTreeNode = nodes[nodes.Count - 1];
                    this._TraceMessagesBox.TabCaption = "MESSAGES";
                }
            }

            this._NavigatorTreeView.Nodes.AddRange( nodes.ToArray() );
            this._NavigatorTreeView.Enabled = true;
        }

        /// <summary> The last node selected. </summary>
        private TreeNode _LastNodeSelected;

        /// <summary> Gets the last tree view node selected. </summary>
        /// <value> The last tree view node selected. </value>
        private TreeViewNode LastTreeViewNodeSelected => this._LastNodeSelected is null ? 0 : ( TreeViewNode ) Conversions.ToInteger( Enum.Parse( typeof( TreeViewNode ), this._LastNodeSelected.Name ) );

        /// <summary> The nodes visited. </summary>
        private List<TreeViewNode> _NodesVisited;

        /// <summary> Called after a node is selected. Displays to relevant screen. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="node"> The node. </param>
        private void OnNodeSelected( TreeViewNode node )
        {
            if ( this._NodesVisited is null )
            {
                this._NodesVisited = new List<TreeViewNode>();
            }

            Control activeControl = null;
            Control focusControl = null;
            DataGridView activeDisplay = null;
            switch ( node )
            {
                case TreeViewNode.ConnectNode:
                    {
                        activeControl = this._ConnectTabLayout;
                        focusControl = null;
                        activeDisplay = null;
                        break;
                    }

                case TreeViewNode.ConfigureNode:
                    {
                        activeControl = this._MainLayout;
                        focusControl = null;
                        activeDisplay = null;
                        break;
                    }

                case TreeViewNode.MeasureNode:
                    {
                        activeControl = this._TtmLayout;
                        focusControl = null;
                        activeDisplay = null;
                        break;
                    }

                case TreeViewNode.ShuntNode:
                    {
                        activeControl = this._ShuntLayout;
                        focusControl = null;
                        activeDisplay = null;
                        break;
                    }

                case TreeViewNode.PartsNode:
                    {
                        activeControl = this._PartsLayout;
                        focusControl = null;
                        activeDisplay = null;
                        break;
                    }

                case TreeViewNode.MessagesNode:
                    {
                        activeControl = this._TraceMessagesBox;
                        focusControl = null;
                        activeDisplay = null;
                        break;
                    }
            }

            // turn off the visibility of the current panel, this turns off the visibility of the
            // contained controls, which will now be removed from the panel.
            this._SplitContainer.Panel2.Hide();
            this._SplitContainer.Panel2.Controls.Clear();
            // If Me._SplitContainer.Panel2.HasChildren Then
            // For Each Control As Control In .Controls
            // Me._SplitContainer.Panel2.Controls.Remove(Control)
            // Next
            // End If

            if ( activeControl is object )
            {
                activeControl.Dock = DockStyle.None;
                this._SplitContainer.Panel2.Controls.Add( activeControl );
                activeControl.Dock = DockStyle.Fill;
            }

            // turn on visibility on the panel -- this toggles the visibility of the contained controls,
            // which is required for the messages boxes.
            this._SplitContainer.Panel2.Show();
            if ( !this._NodesVisited.Contains( node ) )
            {
                if ( focusControl is object )
                {
                    _ = focusControl.Focus();
                }

                this._NodesVisited.Add( node );
            }

            if ( activeDisplay is object )
            {
                // DataDirector.UpdateColumnDisplayOrder(activeDisplay, columnOrder)
            }
        }


        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Console"/> is navigating.
        /// </summary>
        /// <remarks>
        /// Used to ignore changes in grids during the navigation. The grids go through selecting their
        /// rows when navigating.
        /// </remarks>
        /// <value> <c>True</c> if navigating; otherwise, <c>False</c>. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private bool Navigating { get; set; }

        /// <summary> Handles the BeforeSelect event of the _NavigatorTreeView control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Windows.Forms.TreeViewCancelEventArgs"/> instance
        ///                       containing the event data. </param>
        private void NavigatorTreeView_BeforeSelect( object sender, TreeViewCancelEventArgs e )
        {
            this.Navigating = true;
        }

        /// <summary> Handles the AfterSelect event of the Me._NavigatorTreeView control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="System.Windows.Forms.TreeViewEventArgs" /> instance
        ///                       containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void NavigatorTreeView_AfterSelect( object sender, TreeViewEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter.ResourceName} handling navigator after select event";
                if ( this._LastNodeSelected is object )
                {
                    this._LastNodeSelected.BackColor = this._NavigatorTreeView.BackColor;
                }

                if ( sender is object && (sender as Control).Enabled && e is object && e.Node is object && e.Node.IsSelected )
                {
                    this._LastNodeSelected = e.Node;
                    e.Node.BackColor = System.Drawing.SystemColors.Highlight;
                    this.OnNodeSelected( this.LastTreeViewNodeSelected );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Navigating = false;
            }
        }

        /// <summary> Selects the navigator tree view node. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="node"> The node. </param>
        private void SelectNavigatorTreeViewNode( TreeViewNode node )
        {
            this._NavigatorTreeView.SelectedNode = this._NavigatorTreeView.Nodes[node.ToString()];
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds the listeners such as the current trace messages box. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected void AddPrivateListeners()
        {
            this.AddPrivateListener( this._TraceMessagesBox );
        }

        /// <summary> Assign talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        private void AssignTalkerThis( ITraceMessageTalker talker )
        {
            this._PartsPanel.AssignTalker( talker );
            this._TTMConfigurationPanel.AssignTalker( talker );
        }

        /// <summary> Assign talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public override void AssignTalker( ITraceMessageTalker talker )
        {
            this.AssignTalkerThis( talker );
            base.AssignTalker( talker );
            this.AddListener( this._TraceMessagesBox );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            if ( listenerType == this._TraceMessagesBox.ListenerType )
                this._TraceMessagesBox.ApplyTraceLevel( value );
            this._PartsPanel.ApplyListenerTraceLevel( listenerType, value );
            this._TTMConfigurationPanel.ApplyListenerTraceLevel( listenerType, value );
            // this should apply only to the listeners associated with this form
            // MyBase.ApplyListenerTraceLevel(listenerType, value)
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        /// <summary> Executes the trace messages box property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceMessagesBox sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            if ( string.Equals( propertyName, nameof( TraceMessagesBox.StatusPrompt ) ) )
            {
                this._StatusLabel.Text = Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact( sender.StatusPrompt, this._StatusLabel );
                this._StatusLabel.ToolTipText = sender.StatusPrompt;
            }
        }

        /// <summary> Trace messages box property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceMessagesBox_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Meter?.ResourceName} handling trace message box {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceMessagesBox_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TraceMessagesBox, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

    }
}
