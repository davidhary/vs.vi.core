﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class MeasurementViewBase
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MeasurementViewBase));
            _TtmToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            _TtmMeasureControlsToolStrip = new System.Windows.Forms.ToolStrip();
            _MeasureToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            __AbortSequenceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AbortSequenceToolStripMenuItem.Click += new EventHandler(AbortSequenceToolStripMenuItem_Click);
            __MeasureAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __MeasureAllToolStripMenuItem.Click += new EventHandler(MeasureAllToolStripMenuItem_Click);
            __FinalResistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __FinalResistanceToolStripMenuItem.Click += new EventHandler(FinalResistanceToolStripMenuItem_Click);
            __ThermalTransientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ThermalTransientToolStripMenuItem.Click += new EventHandler(ThermalTransientToolStripMenuItem_Click);
            __InitialResistanceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __InitialResistanceToolStripMenuItem.Click += new EventHandler(InitialResistanceToolStripMenuItem_Click);
            __ClearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ClearToolStripMenuItem.Click += new EventHandler(ClearToolStripMenuItem_Click);
            _TtmToolStripProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            _ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            _TriggerToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            __WaitForTriggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __WaitForTriggerToolStripMenuItem.CheckedChanged += new EventHandler(WaitForTriggerToolStripMenuItem_CheckChanged);
            __AssertTriggerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AssertTriggerToolStripMenuItem.Click += new EventHandler(AssertTriggerToolStripMenuItem_Click);
            __AbortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AbortToolStripMenuItem.Click += new EventHandler(AbortToolStripMenuItem_Click);
            _TriggerActionToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _ToolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            _TraceToolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            __ModelTraceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ModelTraceToolStripMenuItem.Click += new EventHandler(ModelTraceToolStripMenuItem_Click);
            __SaveTraceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __SaveTraceToolStripMenuItem.Click += new EventHandler(SaveTraceToolStripMenuItem_Click);
            __ClearTraceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ClearTraceToolStripMenuItem.Click += new EventHandler(ClearTraceToolStripMenuItem_Click);
            __ReadTraceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ReadTraceToolStripMenuItem.Click += new EventHandler(ReadTraceToolStripMenuItem_Click);
            __LogTraceLevelComboBox = new Core.Controls.ToolStripComboBox();
            __LogTraceLevelComboBox.SelectedIndexChanged += new EventHandler(LogTraceLevelComboBox_SelectedIndexChanged);
            __DisplayTraceLevelComboBox = new Core.Controls.ToolStripComboBox();
            __DisplayTraceLevelComboBox.SelectedIndexChanged += new EventHandler(DisplayTraceLevelComboBox_SelectedIndexChanged);
            _Chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            _MeterTimer = new System.Windows.Forms.Timer(components);
            _TraceDataGridView = new System.Windows.Forms.DataGridView();
            _SplitContainer = new System.Windows.Forms.SplitContainer();
            _TtmToolStripContainer.BottomToolStripPanel.SuspendLayout();
            _TtmToolStripContainer.ContentPanel.SuspendLayout();
            _TtmToolStripContainer.SuspendLayout();
            _TtmMeasureControlsToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_Chart).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_TraceDataGridView).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SplitContainer).BeginInit();
            _SplitContainer.Panel1.SuspendLayout();
            _SplitContainer.Panel2.SuspendLayout();
            _SplitContainer.SuspendLayout();
            SuspendLayout();
            // 
            // _TtmToolStripContainer
            // 
            // 
            // _TtmToolStripContainer.BottomToolStripPanel
            // 
            _TtmToolStripContainer.BottomToolStripPanel.Controls.Add(_TtmMeasureControlsToolStrip);
            // 
            // _TtmToolStripContainer.ContentPanel
            // 
            _TtmToolStripContainer.ContentPanel.Controls.Add(_SplitContainer);
            _TtmToolStripContainer.ContentPanel.Size = new System.Drawing.Size(523, 427);
            _TtmToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            _TtmToolStripContainer.Location = new System.Drawing.Point(0, 0);
            _TtmToolStripContainer.Name = "_TtmToolStripContainer";
            _TtmToolStripContainer.Size = new System.Drawing.Size(523, 477);
            _TtmToolStripContainer.TabIndex = 7;
            _TtmToolStripContainer.Text = "ToolStripContainer1";
            // 
            // _TtmMeasureControlsToolStrip
            // 
            _TtmMeasureControlsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _TtmMeasureControlsToolStrip.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TtmMeasureControlsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _MeasureToolStripDropDownButton, _TtmToolStripProgressBar, _ToolStripSeparator1, _TriggerToolStripDropDownButton, _TriggerActionToolStripLabel, _ToolStripSeparator2, _TraceToolStripDropDownButton });
            _TtmMeasureControlsToolStrip.Location = new System.Drawing.Point(3, 0);
            _TtmMeasureControlsToolStrip.Name = "_TtmMeasureControlsToolStrip";
            _TtmMeasureControlsToolStrip.Size = new System.Drawing.Size(389, 25);
            _TtmMeasureControlsToolStrip.TabIndex = 0;
            _TtmMeasureControlsToolStrip.Text = "TTM Measure Controls";
            // 
            // _MeasureToolStripDropDownButton
            // 
            _MeasureToolStripDropDownButton.AutoToolTip = false;
            _MeasureToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _MeasureToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __AbortSequenceToolStripMenuItem, __MeasureAllToolStripMenuItem, __FinalResistanceToolStripMenuItem, __ThermalTransientToolStripMenuItem, __InitialResistanceToolStripMenuItem, __ClearToolStripMenuItem, __LogTraceLevelComboBox, __DisplayTraceLevelComboBox });
            _MeasureToolStripDropDownButton.Image = (System.Drawing.Image)resources.GetObject("_MeasureToolStripDropDownButton.Image");
            _MeasureToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _MeasureToolStripDropDownButton.Name = "_MeasureToolStripDropDownButton";
            _MeasureToolStripDropDownButton.Size = new System.Drawing.Size(78, 22);
            _MeasureToolStripDropDownButton.Text = "MEASURE:";
            _MeasureToolStripDropDownButton.ToolTipText = "Measure thermal transient elements";
            // 
            // _AbortSequenceToolStripMenuItem
            // 
            __AbortSequenceToolStripMenuItem.Image = My.Resources.Resources.MediaPlaybackStop2;
            __AbortSequenceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __AbortSequenceToolStripMenuItem.Name = "__AbortSequenceToolStripMenuItem";
            __AbortSequenceToolStripMenuItem.Size = new System.Drawing.Size(223, 38);
            __AbortSequenceToolStripMenuItem.Text = "ABORT";
            __AbortSequenceToolStripMenuItem.ToolTipText = "Abort measurement sequence";
            // 
            // _MeasureAllToolStripMenuItem
            // 
            __MeasureAllToolStripMenuItem.Image = My.Resources.Resources.ArrowRightDouble;
            __MeasureAllToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __MeasureAllToolStripMenuItem.Name = "__MeasureAllToolStripMenuItem";
            __MeasureAllToolStripMenuItem.Size = new System.Drawing.Size(223, 38);
            __MeasureAllToolStripMenuItem.Text = "ALL";
            __MeasureAllToolStripMenuItem.ToolTipText = "Measure initial resistance, thermal transient, and final resistance";
            // 
            // _FinalResistanceToolStripMenuItem
            // 
            __FinalResistanceToolStripMenuItem.Image = My.Resources.Resources.ArrowRight2;
            __FinalResistanceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __FinalResistanceToolStripMenuItem.Name = "__FinalResistanceToolStripMenuItem";
            __FinalResistanceToolStripMenuItem.Size = new System.Drawing.Size(223, 38);
            __FinalResistanceToolStripMenuItem.Text = "FINAL RESISTANCE";
            __FinalResistanceToolStripMenuItem.ToolTipText = "Measure Final Resistance";
            // 
            // _ThermalTransientToolStripMenuItem
            // 
            __ThermalTransientToolStripMenuItem.Image = My.Resources.Resources.ArrowRight2;
            __ThermalTransientToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __ThermalTransientToolStripMenuItem.Name = "__ThermalTransientToolStripMenuItem";
            __ThermalTransientToolStripMenuItem.Size = new System.Drawing.Size(223, 38);
            __ThermalTransientToolStripMenuItem.Text = "THERMAL TRANSIENT";
            __ThermalTransientToolStripMenuItem.ToolTipText = "Measure Thermal Transient";
            // 
            // _InitialResistanceToolStripMenuItem
            // 
            __InitialResistanceToolStripMenuItem.Image = My.Resources.Resources.ArrowRight2;
            __InitialResistanceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __InitialResistanceToolStripMenuItem.Name = "__InitialResistanceToolStripMenuItem";
            __InitialResistanceToolStripMenuItem.Size = new System.Drawing.Size(223, 38);
            __InitialResistanceToolStripMenuItem.Text = "INITIAL RESISTANCE";
            __InitialResistanceToolStripMenuItem.ToolTipText = "Measure Initial Resistance";
            // 
            // _ClearToolStripMenuItem
            // 
            __ClearToolStripMenuItem.Image = My.Resources.Resources.EditClear2;
            __ClearToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __ClearToolStripMenuItem.Name = "__ClearToolStripMenuItem";
            __ClearToolStripMenuItem.Size = new System.Drawing.Size(223, 38);
            __ClearToolStripMenuItem.Text = "CLEAR MEASUREMENTS";
            __ClearToolStripMenuItem.ToolTipText = "Clear measurements on next Initial Resistance measurement";
            // 
            // _TtmToolStripProgressBar
            // 
            _TtmToolStripProgressBar.AutoSize = false;
            _TtmToolStripProgressBar.Name = "_TtmToolStripProgressBar";
            _TtmToolStripProgressBar.Size = new System.Drawing.Size(100, 15);
            _TtmToolStripProgressBar.ToolTipText = "Measurement sequence progress";
            // 
            // _ToolStripSeparator1
            // 
            _ToolStripSeparator1.Name = "_ToolStripSeparator1";
            _ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // _TriggerToolStripDropDownButton
            // 
            _TriggerToolStripDropDownButton.AutoToolTip = false;
            _TriggerToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TriggerToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __WaitForTriggerToolStripMenuItem, __AssertTriggerToolStripMenuItem, __AbortToolStripMenuItem });
            _TriggerToolStripDropDownButton.Image = (System.Drawing.Image)resources.GetObject("_TriggerToolStripDropDownButton.Image");
            _TriggerToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _TriggerToolStripDropDownButton.Name = "_TriggerToolStripDropDownButton";
            _TriggerToolStripDropDownButton.Size = new System.Drawing.Size(71, 22);
            _TriggerToolStripDropDownButton.Text = "TRIGGER";
            _TriggerToolStripDropDownButton.ToolTipText = "Trigger Options";
            // 
            // _WaitForTriggerToolStripMenuItem
            // 
            __WaitForTriggerToolStripMenuItem.CheckOnClick = true;
            __WaitForTriggerToolStripMenuItem.Image = My.Resources.Resources.ViewRefresh7;
            __WaitForTriggerToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __WaitForTriggerToolStripMenuItem.Name = "__WaitForTriggerToolStripMenuItem";
            __WaitForTriggerToolStripMenuItem.Size = new System.Drawing.Size(179, 38);
            __WaitForTriggerToolStripMenuItem.Text = "Wait for Trigger";
            __WaitForTriggerToolStripMenuItem.ToolTipText = "Check to enable waiting for trigger";
            // 
            // _AssertTriggerToolStripMenuItem
            // 
            __AssertTriggerToolStripMenuItem.Enabled = false;
            __AssertTriggerToolStripMenuItem.Image = My.Resources.Resources.EditAdd;
            __AssertTriggerToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __AssertTriggerToolStripMenuItem.Name = "__AssertTriggerToolStripMenuItem";
            __AssertTriggerToolStripMenuItem.Size = new System.Drawing.Size(179, 38);
            __AssertTriggerToolStripMenuItem.Text = "Assert Trigger";
            __AssertTriggerToolStripMenuItem.ToolTipText = "Assert instrument trigger";
            // 
            // _AbortToolStripMenuItem
            // 
            __AbortToolStripMenuItem.Image = My.Resources.Resources.ProcessStop;
            __AbortToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __AbortToolStripMenuItem.Name = "__AbortToolStripMenuItem";
            __AbortToolStripMenuItem.Size = new System.Drawing.Size(179, 38);
            __AbortToolStripMenuItem.Text = "ABORT";
            __AbortToolStripMenuItem.ToolTipText = "Forces abort";
            // 
            // _TriggerActionToolStripLabel
            // 
            _TriggerActionToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerActionToolStripLabel.Name = "_TriggerActionToolStripLabel";
            _TriggerActionToolStripLabel.Size = new System.Drawing.Size(52, 22);
            _TriggerActionToolStripLabel.Text = "Inactive";
            // 
            // _ToolStripSeparator2
            // 
            _ToolStripSeparator2.Name = "_ToolStripSeparator2";
            _ToolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // _TraceToolStripDropDownButton
            // 
            _TraceToolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TraceToolStripDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __ModelTraceToolStripMenuItem, __SaveTraceToolStripMenuItem, __ClearTraceToolStripMenuItem, __ReadTraceToolStripMenuItem });
            _TraceToolStripDropDownButton.Image = (System.Drawing.Image)resources.GetObject("_TraceToolStripDropDownButton.Image");
            _TraceToolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _TraceToolStripDropDownButton.Name = "_TraceToolStripDropDownButton";
            _TraceToolStripDropDownButton.Size = new System.Drawing.Size(62, 22);
            _TraceToolStripDropDownButton.Text = "TRACE: ";
            // 
            // _ModelTraceToolStripMenuItem
            // 
            __ModelTraceToolStripMenuItem.Image = My.Resources.Resources.GamesSolve;
            __ModelTraceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __ModelTraceToolStripMenuItem.Name = "__ModelTraceToolStripMenuItem";
            __ModelTraceToolStripMenuItem.Size = new System.Drawing.Size(148, 38);
            __ModelTraceToolStripMenuItem.Text = "CURVE FIT";
            __ModelTraceToolStripMenuItem.ToolTipText = "Fix a model to the trace";
            // 
            // _SaveTraceToolStripMenuItem
            // 
            __SaveTraceToolStripMenuItem.Image = My.Resources.Resources.DocumentExport;
            __SaveTraceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __SaveTraceToolStripMenuItem.Name = "__SaveTraceToolStripMenuItem";
            __SaveTraceToolStripMenuItem.Size = new System.Drawing.Size(148, 38);
            __SaveTraceToolStripMenuItem.Text = "SAVE";
            __SaveTraceToolStripMenuItem.ToolTipText = "Save trace values to file";
            // 
            // _ClearTraceToolStripMenuItem
            // 
            __ClearTraceToolStripMenuItem.Image = My.Resources.Resources.EditClear2;
            __ClearTraceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __ClearTraceToolStripMenuItem.Name = "__ClearTraceToolStripMenuItem";
            __ClearTraceToolStripMenuItem.Size = new System.Drawing.Size(148, 38);
            __ClearTraceToolStripMenuItem.Text = "CLEAR";
            __ClearTraceToolStripMenuItem.ToolTipText = "Clear trace chart and list";
            // 
            // _ReadTraceToolStripMenuItem
            // 
            __ReadTraceToolStripMenuItem.Image = My.Resources.Resources.ViewObjectHistogramLogarithmic;
            __ReadTraceToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            __ReadTraceToolStripMenuItem.Name = "__ReadTraceToolStripMenuItem";
            __ReadTraceToolStripMenuItem.Size = new System.Drawing.Size(148, 38);
            __ReadTraceToolStripMenuItem.Text = "READ";
            // 
            // _Chart
            // 
            _Chart.BorderlineColor = System.Drawing.Color.Aqua;
            _Chart.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            _Chart.Dock = System.Windows.Forms.DockStyle.Fill;
            _Chart.Location = new System.Drawing.Point(0, 0);
            _Chart.Name = "_Chart";
            _Chart.Size = new System.Drawing.Size(330, 427);
            _Chart.TabIndex = 0;
            _Chart.Text = "Chart1";
            // 
            // _TraceDataGridView
            // 
            _TraceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _TraceDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            _TraceDataGridView.Location = new System.Drawing.Point(0, 0);
            _TraceDataGridView.Name = "_TraceDataGridView";
            _TraceDataGridView.Size = new System.Drawing.Size(189, 427);
            _TraceDataGridView.TabIndex = 1;
            // 
            // _SplitContainer
            // 
            _SplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            _SplitContainer.Location = new System.Drawing.Point(0, 0);
            _SplitContainer.Name = "_SplitContainer";
            // 
            // _SplitContainer.Panel1
            // 
            _SplitContainer.Panel1.Controls.Add(_Chart);
            // 
            // _SplitContainer.Panel2
            // 
            _SplitContainer.Panel2.Controls.Add(_TraceDataGridView);
            _SplitContainer.Size = new System.Drawing.Size(523, 427);
            _SplitContainer.SplitterDistance = 330;
            _SplitContainer.TabIndex = 2;
            // 
            // _LogTraceLevelComboBox
            // 
            __LogTraceLevelComboBox.Name = "__LogTraceLevelComboBox";
            __LogTraceLevelComboBox.Size = new System.Drawing.Size(100, 22);
            __LogTraceLevelComboBox.Text = "Verbose";
            __LogTraceLevelComboBox.ToolTipText = "Log Trace Level";
            // 
            // _DisplayTraceLevelComboBox
            // 
            __DisplayTraceLevelComboBox.Name = "__DisplayTraceLevelComboBox";
            __DisplayTraceLevelComboBox.Size = new System.Drawing.Size(100, 22);
            __DisplayTraceLevelComboBox.Text = "Warning";
            __DisplayTraceLevelComboBox.ToolTipText = "Display trace level";
            // 
            // MeasurementPanelBase
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_TtmToolStripContainer);
            Name = "MeasurementPanelBase";
            Size = new System.Drawing.Size(523, 477);
            _TtmToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            _TtmToolStripContainer.BottomToolStripPanel.PerformLayout();
            _TtmToolStripContainer.ContentPanel.ResumeLayout(false);
            _TtmToolStripContainer.ResumeLayout(false);
            _TtmToolStripContainer.PerformLayout();
            _TtmMeasureControlsToolStrip.ResumeLayout(false);
            _TtmMeasureControlsToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_Chart).EndInit();
            ((System.ComponentModel.ISupportInitialize)_TraceDataGridView).EndInit();
            _SplitContainer.Panel1.ResumeLayout(false);
            _SplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_SplitContainer).EndInit();
            _SplitContainer.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.ToolStripContainer _TtmToolStripContainer;
        private System.Windows.Forms.ToolStrip _TtmMeasureControlsToolStrip;
        private System.Windows.Forms.ToolStripProgressBar _TtmToolStripProgressBar;
        private System.Windows.Forms.DataVisualization.Charting.Chart _Chart;
        private System.Windows.Forms.Timer _MeterTimer;
        private System.Windows.Forms.ToolStripDropDownButton _MeasureToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem __AbortSequenceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AbortSequenceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortSequenceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortSequenceToolStripMenuItem != null)
                {
                    __AbortSequenceToolStripMenuItem.Click -= AbortSequenceToolStripMenuItem_Click;
                }

                __AbortSequenceToolStripMenuItem = value;
                if (__AbortSequenceToolStripMenuItem != null)
                {
                    __AbortSequenceToolStripMenuItem.Click += AbortSequenceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __MeasureAllToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _MeasureAllToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureAllToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureAllToolStripMenuItem != null)
                {
                    __MeasureAllToolStripMenuItem.Click -= MeasureAllToolStripMenuItem_Click;
                }

                __MeasureAllToolStripMenuItem = value;
                if (__MeasureAllToolStripMenuItem != null)
                {
                    __MeasureAllToolStripMenuItem.Click += MeasureAllToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __FinalResistanceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _FinalResistanceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FinalResistanceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FinalResistanceToolStripMenuItem != null)
                {
                    __FinalResistanceToolStripMenuItem.Click -= FinalResistanceToolStripMenuItem_Click;
                }

                __FinalResistanceToolStripMenuItem = value;
                if (__FinalResistanceToolStripMenuItem != null)
                {
                    __FinalResistanceToolStripMenuItem.Click += FinalResistanceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ThermalTransientToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ThermalTransientToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ThermalTransientToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ThermalTransientToolStripMenuItem != null)
                {
                    __ThermalTransientToolStripMenuItem.Click -= ThermalTransientToolStripMenuItem_Click;
                }

                __ThermalTransientToolStripMenuItem = value;
                if (__ThermalTransientToolStripMenuItem != null)
                {
                    __ThermalTransientToolStripMenuItem.Click += ThermalTransientToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __InitialResistanceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _InitialResistanceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitialResistanceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitialResistanceToolStripMenuItem != null)
                {
                    __InitialResistanceToolStripMenuItem.Click -= InitialResistanceToolStripMenuItem_Click;
                }

                __InitialResistanceToolStripMenuItem = value;
                if (__InitialResistanceToolStripMenuItem != null)
                {
                    __InitialResistanceToolStripMenuItem.Click += InitialResistanceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ClearToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ClearToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearToolStripMenuItem != null)
                {
                    __ClearToolStripMenuItem.Click -= ClearToolStripMenuItem_Click;
                }

                __ClearToolStripMenuItem = value;
                if (__ClearToolStripMenuItem != null)
                {
                    __ClearToolStripMenuItem.Click += ClearToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSeparator _ToolStripSeparator1;
        private System.Windows.Forms.ToolStripDropDownButton _TriggerToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem __WaitForTriggerToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _WaitForTriggerToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WaitForTriggerToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WaitForTriggerToolStripMenuItem != null)
                {
                    __WaitForTriggerToolStripMenuItem.CheckedChanged -= WaitForTriggerToolStripMenuItem_CheckChanged;
                }

                __WaitForTriggerToolStripMenuItem = value;
                if (__WaitForTriggerToolStripMenuItem != null)
                {
                    __WaitForTriggerToolStripMenuItem.CheckedChanged += WaitForTriggerToolStripMenuItem_CheckChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __AssertTriggerToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AssertTriggerToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AssertTriggerToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AssertTriggerToolStripMenuItem != null)
                {
                    __AssertTriggerToolStripMenuItem.Click -= AssertTriggerToolStripMenuItem_Click;
                }

                __AssertTriggerToolStripMenuItem = value;
                if (__AssertTriggerToolStripMenuItem != null)
                {
                    __AssertTriggerToolStripMenuItem.Click += AssertTriggerToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _TriggerActionToolStripLabel;
        private System.Windows.Forms.ToolStripSeparator _ToolStripSeparator2;
        private System.Windows.Forms.ToolStripDropDownButton _TraceToolStripDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem __ModelTraceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ModelTraceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ModelTraceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ModelTraceToolStripMenuItem != null)
                {
                    __ModelTraceToolStripMenuItem.Click -= ModelTraceToolStripMenuItem_Click;
                }

                __ModelTraceToolStripMenuItem = value;
                if (__ModelTraceToolStripMenuItem != null)
                {
                    __ModelTraceToolStripMenuItem.Click += ModelTraceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __SaveTraceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _SaveTraceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveTraceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveTraceToolStripMenuItem != null)
                {
                    __SaveTraceToolStripMenuItem.Click -= SaveTraceToolStripMenuItem_Click;
                }

                __SaveTraceToolStripMenuItem = value;
                if (__SaveTraceToolStripMenuItem != null)
                {
                    __SaveTraceToolStripMenuItem.Click += SaveTraceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ClearTraceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ClearTraceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearTraceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearTraceToolStripMenuItem != null)
                {
                    __ClearTraceToolStripMenuItem.Click -= ClearTraceToolStripMenuItem_Click;
                }

                __ClearTraceToolStripMenuItem = value;
                if (__ClearTraceToolStripMenuItem != null)
                {
                    __ClearTraceToolStripMenuItem.Click += ClearTraceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ReadTraceToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ReadTraceToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadTraceToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadTraceToolStripMenuItem != null)
                {
                    __ReadTraceToolStripMenuItem.Click -= ReadTraceToolStripMenuItem_Click;
                }

                __ReadTraceToolStripMenuItem = value;
                if (__ReadTraceToolStripMenuItem != null)
                {
                    __ReadTraceToolStripMenuItem.Click += ReadTraceToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __AbortToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AbortToolStripMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortToolStripMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortToolStripMenuItem != null)
                {
                    __AbortToolStripMenuItem.Click -= AbortToolStripMenuItem_Click;
                }

                __AbortToolStripMenuItem = value;
                if (__AbortToolStripMenuItem != null)
                {
                    __AbortToolStripMenuItem.Click += AbortToolStripMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.DataGridView _TraceDataGridView;
        private System.Windows.Forms.SplitContainer _SplitContainer;
        private Core.Controls.ToolStripComboBox __LogTraceLevelComboBox;

        private Core.Controls.ToolStripComboBox _LogTraceLevelComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LogTraceLevelComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LogTraceLevelComboBox != null)
                {
                    __LogTraceLevelComboBox.SelectedIndexChanged -= LogTraceLevelComboBox_SelectedIndexChanged;
                }

                __LogTraceLevelComboBox = value;
                if (__LogTraceLevelComboBox != null)
                {
                    __LogTraceLevelComboBox.SelectedIndexChanged += LogTraceLevelComboBox_SelectedIndexChanged;
                }
            }
        }

        private Core.Controls.ToolStripComboBox __DisplayTraceLevelComboBox;

        private Core.Controls.ToolStripComboBox _DisplayTraceLevelComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DisplayTraceLevelComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DisplayTraceLevelComboBox != null)
                {
                    __DisplayTraceLevelComboBox.SelectedIndexChanged -= DisplayTraceLevelComboBox_SelectedIndexChanged;
                }

                __DisplayTraceLevelComboBox = value;
                if (__DisplayTraceLevelComboBox != null)
                {
                    __DisplayTraceLevelComboBox.SelectedIndexChanged += DisplayTraceLevelComboBox_SelectedIndexChanged;
                }
            }
        }
    }
}