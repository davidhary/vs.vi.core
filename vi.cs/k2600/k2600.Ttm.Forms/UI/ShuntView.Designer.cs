﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class ShuntView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ShuntLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShuntDisplayLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShuntResistanceTextBox = new System.Windows.Forms.TextBox();
            _ShuntResistanceTextBoxLabel = new System.Windows.Forms.Label();
            _ShuntGroupBoxLayout = new System.Windows.Forms.TableLayoutPanel();
            _MeasureShuntResistanceButton = new System.Windows.Forms.Button();
            _ShuntConfigureGroupBoxLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShuntResistanceConfigurationGroupBox = new System.Windows.Forms.GroupBox();
            _RestoreShuntResistanceDefaultsButton = new System.Windows.Forms.Button();
            _ApplyNewShuntResistanceConfigurationButton = new System.Windows.Forms.Button();
            _ApplyShuntResistanceConfigurationButton = new System.Windows.Forms.Button();
            _ShuntResistanceCurrentRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceCurrentRangeNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceLowLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceLowLimitNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceHighLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceHighLimitNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceVoltageLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceVoltageLimitNumericLabel = new System.Windows.Forms.Label();
            _ShuntResistanceCurrentLevelNumeric = new System.Windows.Forms.NumericUpDown();
            _ShuntResistanceCurrentLevelNumericLabel = new System.Windows.Forms.Label();
            _ShuntLayout.SuspendLayout();
            _ShuntDisplayLayout.SuspendLayout();
            _ShuntGroupBoxLayout.SuspendLayout();
            _ShuntConfigureGroupBoxLayout.SuspendLayout();
            _ShuntResistanceConfigurationGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceLowLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceHighLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceVoltageLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentLevelNumeric).BeginInit();
            SuspendLayout();
            // 
            // _ShuntLayout
            // 
            _ShuntLayout.ColumnCount = 3;
            _ShuntLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.Controls.Add(_ShuntDisplayLayout, 1, 1);
            _ShuntLayout.Controls.Add(_ShuntGroupBoxLayout, 1, 3);
            _ShuntLayout.Controls.Add(_ShuntConfigureGroupBoxLayout, 1, 2);
            _ShuntLayout.Location = new System.Drawing.Point(3, 9);
            _ShuntLayout.Name = "_ShuntLayout";
            _ShuntLayout.RowCount = 5;
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntLayout.Size = new System.Drawing.Size(466, 492);
            _ShuntLayout.TabIndex = 1;
            // 
            // _ShuntDisplayLayout
            // 
            _ShuntDisplayLayout.BackColor = System.Drawing.Color.Black;
            _ShuntDisplayLayout.ColumnCount = 3;
            _ShuntDisplayLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0f));
            _ShuntDisplayLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntDisplayLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0f));
            _ShuntDisplayLayout.Controls.Add(_ShuntResistanceTextBox, 1, 2);
            _ShuntDisplayLayout.Controls.Add(_ShuntResistanceTextBoxLabel, 1, 1);
            _ShuntDisplayLayout.Dock = System.Windows.Forms.DockStyle.Left;
            _ShuntDisplayLayout.Location = new System.Drawing.Point(64, 11);
            _ShuntDisplayLayout.Name = "_ShuntDisplayLayout";
            _ShuntDisplayLayout.RowCount = 4;
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5.0f));
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntDisplayLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5.0f));
            _ShuntDisplayLayout.Size = new System.Drawing.Size(337, 75);
            _ShuntDisplayLayout.TabIndex = 0;
            // 
            // _ShuntResistanceTextBox
            // 
            _ShuntResistanceTextBox.BackColor = System.Drawing.Color.Black;
            _ShuntResistanceTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _ShuntResistanceTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ShuntResistanceTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _ShuntResistanceTextBox.Location = new System.Drawing.Point(68, 25);
            _ShuntResistanceTextBox.Name = "_ShuntResistanceTextBox";
            _ShuntResistanceTextBox.ReadOnly = true;
            _ShuntResistanceTextBox.Size = new System.Drawing.Size(201, 39);
            _ShuntResistanceTextBox.TabIndex = 7;
            _ShuntResistanceTextBox.Text = "0.000";
            _ShuntResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _ShuntResistanceTextBoxLabel
            // 
            _ShuntResistanceTextBoxLabel.AutoSize = true;
            _ShuntResistanceTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _ShuntResistanceTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ShuntResistanceTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _ShuntResistanceTextBoxLabel.Location = new System.Drawing.Point(68, 5);
            _ShuntResistanceTextBoxLabel.Name = "_ShuntResistanceTextBoxLabel";
            _ShuntResistanceTextBoxLabel.Size = new System.Drawing.Size(201, 17);
            _ShuntResistanceTextBoxLabel.TabIndex = 6;
            _ShuntResistanceTextBoxLabel.Text = "SHUNT RESISTANCE [Ω]";
            _ShuntResistanceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _ShuntGroupBoxLayout
            // 
            _ShuntGroupBoxLayout.ColumnCount = 3;
            _ShuntGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntGroupBoxLayout.Controls.Add(_MeasureShuntResistanceButton, 1, 1);
            _ShuntGroupBoxLayout.Location = new System.Drawing.Point(64, 417);
            _ShuntGroupBoxLayout.Name = "_ShuntGroupBoxLayout";
            _ShuntGroupBoxLayout.RowCount = 3;
            _ShuntGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _ShuntGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _ShuntGroupBoxLayout.Size = new System.Drawing.Size(337, 63);
            _ShuntGroupBoxLayout.TabIndex = 2;
            // 
            // _MeasureShuntResistanceButton
            // 
            _MeasureShuntResistanceButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _MeasureShuntResistanceButton.Location = new System.Drawing.Point(53, 13);
            _MeasureShuntResistanceButton.Name = "_MeasureShuntResistanceButton";
            _MeasureShuntResistanceButton.Size = new System.Drawing.Size(230, 36);
            _MeasureShuntResistanceButton.TabIndex = 3;
            _MeasureShuntResistanceButton.Text = "READ SHUNT RESISTANCE";
            _MeasureShuntResistanceButton.UseVisualStyleBackColor = true;
            // 
            // _ShuntConfigureGroupBoxLayout
            // 
            _ShuntConfigureGroupBoxLayout.ColumnCount = 3;
            _ShuntConfigureGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShuntConfigureGroupBoxLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.Controls.Add(_ShuntResistanceConfigurationGroupBox, 1, 1);
            _ShuntConfigureGroupBoxLayout.Location = new System.Drawing.Point(64, 92);
            _ShuntConfigureGroupBoxLayout.Name = "_ShuntConfigureGroupBoxLayout";
            _ShuntConfigureGroupBoxLayout.RowCount = 3;
            _ShuntConfigureGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShuntConfigureGroupBoxLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShuntConfigureGroupBoxLayout.Size = new System.Drawing.Size(337, 319);
            _ShuntConfigureGroupBoxLayout.TabIndex = 3;
            // 
            // _ShuntResistanceConfigurationGroupBox
            // 
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_RestoreShuntResistanceDefaultsButton);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ApplyNewShuntResistanceConfigurationButton);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ApplyShuntResistanceConfigurationButton);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentRangeNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentRangeNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceLowLimitNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceLowLimitNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceHighLimitNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceHighLimitNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceVoltageLimitNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceVoltageLimitNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentLevelNumeric);
            _ShuntResistanceConfigurationGroupBox.Controls.Add(_ShuntResistanceCurrentLevelNumericLabel);
            _ShuntResistanceConfigurationGroupBox.Dock = System.Windows.Forms.DockStyle.Top;
            _ShuntResistanceConfigurationGroupBox.Location = new System.Drawing.Point(39, 12);
            _ShuntResistanceConfigurationGroupBox.Name = "_ShuntResistanceConfigurationGroupBox";
            _ShuntResistanceConfigurationGroupBox.Size = new System.Drawing.Size(258, 294);
            _ShuntResistanceConfigurationGroupBox.TabIndex = 2;
            _ShuntResistanceConfigurationGroupBox.TabStop = false;
            _ShuntResistanceConfigurationGroupBox.Text = "SHUNT RESISTANCE CONFIG.";
            // 
            // _RestoreShuntResistanceDefaultsButton
            // 
            _RestoreShuntResistanceDefaultsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _RestoreShuntResistanceDefaultsButton.Location = new System.Drawing.Point(14, 252);
            _RestoreShuntResistanceDefaultsButton.Name = "_RestoreShuntResistanceDefaultsButton";
            _RestoreShuntResistanceDefaultsButton.Size = new System.Drawing.Size(234, 30);
            _RestoreShuntResistanceDefaultsButton.TabIndex = 12;
            _RestoreShuntResistanceDefaultsButton.Text = "RESTORE DEFAULTS";
            _RestoreShuntResistanceDefaultsButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyNewShuntResistanceConfigurationButton
            // 
            _ApplyNewShuntResistanceConfigurationButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ApplyNewShuntResistanceConfigurationButton.Location = new System.Drawing.Point(133, 210);
            _ApplyNewShuntResistanceConfigurationButton.Name = "_ApplyNewShuntResistanceConfigurationButton";
            _ApplyNewShuntResistanceConfigurationButton.Size = new System.Drawing.Size(115, 30);
            _ApplyNewShuntResistanceConfigurationButton.TabIndex = 11;
            _ApplyNewShuntResistanceConfigurationButton.Text = "APPLY CHANGES";
            _ApplyNewShuntResistanceConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyShuntResistanceConfigurationButton
            // 
            _ApplyShuntResistanceConfigurationButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ApplyShuntResistanceConfigurationButton.Location = new System.Drawing.Point(11, 209);
            _ApplyShuntResistanceConfigurationButton.Name = "_ApplyShuntResistanceConfigurationButton";
            _ApplyShuntResistanceConfigurationButton.Size = new System.Drawing.Size(115, 30);
            _ApplyShuntResistanceConfigurationButton.TabIndex = 10;
            _ApplyShuntResistanceConfigurationButton.Text = "APPLY ALL";
            _ApplyShuntResistanceConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // _ShuntResistanceCurrentRangeNumeric
            // 
            _ShuntResistanceCurrentRangeNumeric.DecimalPlaces = 3;
            _ShuntResistanceCurrentRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceCurrentRangeNumeric.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            _ShuntResistanceCurrentRangeNumeric.Location = new System.Drawing.Point(161, 62);
            _ShuntResistanceCurrentRangeNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 65536 });
            _ShuntResistanceCurrentRangeNumeric.Name = "_ShuntResistanceCurrentRangeNumeric";
            _ShuntResistanceCurrentRangeNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceCurrentRangeNumeric.TabIndex = 3;
            _ShuntResistanceCurrentRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ShuntResistanceCurrentRangeNumeric.Value = new decimal(new int[] { 1, 0, 0, 131072 });
            // 
            // _ShuntResistanceCurrentRangeNumericLabel
            // 
            _ShuntResistanceCurrentRangeNumericLabel.AutoSize = true;
            _ShuntResistanceCurrentRangeNumericLabel.Location = new System.Drawing.Point(25, 66);
            _ShuntResistanceCurrentRangeNumericLabel.Name = "_ShuntResistanceCurrentRangeNumericLabel";
            _ShuntResistanceCurrentRangeNumericLabel.Size = new System.Drawing.Size(134, 17);
            _ShuntResistanceCurrentRangeNumericLabel.TabIndex = 2;
            _ShuntResistanceCurrentRangeNumericLabel.Text = "CURRENT RANGE [A]:";
            _ShuntResistanceCurrentRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceLowLimitNumeric
            // 
            _ShuntResistanceLowLimitNumeric.DecimalPlaces = 1;
            _ShuntResistanceLowLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceLowLimitNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceLowLimitNumeric.Location = new System.Drawing.Point(161, 172);
            _ShuntResistanceLowLimitNumeric.Maximum = new decimal(new int[] { 2000, 0, 0, 0 });
            _ShuntResistanceLowLimitNumeric.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceLowLimitNumeric.Name = "_ShuntResistanceLowLimitNumeric";
            _ShuntResistanceLowLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ShuntResistanceLowLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceLowLimitNumeric.TabIndex = 9;
            _ShuntResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ShuntResistanceLowLimitNumeric.Value = new decimal(new int[] { 1150, 0, 0, 0 });
            // 
            // _ShuntResistanceLowLimitNumericLabel
            // 
            _ShuntResistanceLowLimitNumericLabel.AutoSize = true;
            _ShuntResistanceLowLimitNumericLabel.Location = new System.Drawing.Point(63, 176);
            _ShuntResistanceLowLimitNumericLabel.Name = "_ShuntResistanceLowLimitNumericLabel";
            _ShuntResistanceLowLimitNumericLabel.Size = new System.Drawing.Size(96, 17);
            _ShuntResistanceLowLimitNumericLabel.TabIndex = 8;
            _ShuntResistanceLowLimitNumericLabel.Text = "LO&W LIMIT [Ω]:";
            _ShuntResistanceLowLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceHighLimitNumeric
            // 
            _ShuntResistanceHighLimitNumeric.DecimalPlaces = 1;
            _ShuntResistanceHighLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceHighLimitNumeric.Increment = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceHighLimitNumeric.Location = new System.Drawing.Point(161, 135);
            _ShuntResistanceHighLimitNumeric.Maximum = new decimal(new int[] { 3000, 0, 0, 0 });
            _ShuntResistanceHighLimitNumeric.Minimum = new decimal(new int[] { 10, 0, 0, 0 });
            _ShuntResistanceHighLimitNumeric.Name = "_ShuntResistanceHighLimitNumeric";
            _ShuntResistanceHighLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ShuntResistanceHighLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceHighLimitNumeric.TabIndex = 7;
            _ShuntResistanceHighLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ShuntResistanceHighLimitNumeric.Value = new decimal(new int[] { 1250, 0, 0, 0 });
            // 
            // _ShuntResistanceHighLimitNumericLabel
            // 
            _ShuntResistanceHighLimitNumericLabel.AutoSize = true;
            _ShuntResistanceHighLimitNumericLabel.Location = new System.Drawing.Point(61, 139);
            _ShuntResistanceHighLimitNumericLabel.Name = "_ShuntResistanceHighLimitNumericLabel";
            _ShuntResistanceHighLimitNumericLabel.Size = new System.Drawing.Size(98, 17);
            _ShuntResistanceHighLimitNumericLabel.TabIndex = 6;
            _ShuntResistanceHighLimitNumericLabel.Text = "&HIGH LIMIT [Ω]:";
            _ShuntResistanceHighLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceVoltageLimitNumeric
            // 
            _ShuntResistanceVoltageLimitNumeric.DecimalPlaces = 2;
            _ShuntResistanceVoltageLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceVoltageLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ShuntResistanceVoltageLimitNumeric.Location = new System.Drawing.Point(161, 98);
            _ShuntResistanceVoltageLimitNumeric.Maximum = new decimal(new int[] { 40, 0, 0, 0 });
            _ShuntResistanceVoltageLimitNumeric.Name = "_ShuntResistanceVoltageLimitNumeric";
            _ShuntResistanceVoltageLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceVoltageLimitNumeric.TabIndex = 5;
            _ShuntResistanceVoltageLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ShuntResistanceVoltageLimitNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _ShuntResistanceVoltageLimitNumericLabel
            // 
            _ShuntResistanceVoltageLimitNumericLabel.AutoSize = true;
            _ShuntResistanceVoltageLimitNumericLabel.Location = new System.Drawing.Point(38, 102);
            _ShuntResistanceVoltageLimitNumericLabel.Name = "_ShuntResistanceVoltageLimitNumericLabel";
            _ShuntResistanceVoltageLimitNumericLabel.Size = new System.Drawing.Size(119, 17);
            _ShuntResistanceVoltageLimitNumericLabel.TabIndex = 4;
            _ShuntResistanceVoltageLimitNumericLabel.Text = "&VOLTAGE LIMIT [V]:";
            _ShuntResistanceVoltageLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ShuntResistanceCurrentLevelNumeric
            // 
            _ShuntResistanceCurrentLevelNumeric.DecimalPlaces = 4;
            _ShuntResistanceCurrentLevelNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ShuntResistanceCurrentLevelNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _ShuntResistanceCurrentLevelNumeric.Location = new System.Drawing.Point(161, 27);
            _ShuntResistanceCurrentLevelNumeric.Maximum = new decimal(new int[] { 100, 0, 0, 262144 });
            _ShuntResistanceCurrentLevelNumeric.Name = "_ShuntResistanceCurrentLevelNumeric";
            _ShuntResistanceCurrentLevelNumeric.Size = new System.Drawing.Size(75, 25);
            _ShuntResistanceCurrentLevelNumeric.TabIndex = 1;
            _ShuntResistanceCurrentLevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ShuntResistanceCurrentLevelNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ShuntResistanceCurrentLevelNumericLabel
            // 
            _ShuntResistanceCurrentLevelNumericLabel.AutoSize = true;
            _ShuntResistanceCurrentLevelNumericLabel.Location = new System.Drawing.Point(33, 31);
            _ShuntResistanceCurrentLevelNumericLabel.Name = "_ShuntResistanceCurrentLevelNumericLabel";
            _ShuntResistanceCurrentLevelNumericLabel.Size = new System.Drawing.Size(126, 17);
            _ShuntResistanceCurrentLevelNumericLabel.TabIndex = 0;
            _ShuntResistanceCurrentLevelNumericLabel.Text = "C&URRENT LEVEL [A]:";
            _ShuntResistanceCurrentLevelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ShuntView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_ShuntLayout);
            Name = "ShuntView";
            Size = new System.Drawing.Size(472, 510);
            _ShuntLayout.ResumeLayout(false);
            _ShuntDisplayLayout.ResumeLayout(false);
            _ShuntDisplayLayout.PerformLayout();
            _ShuntGroupBoxLayout.ResumeLayout(false);
            _ShuntConfigureGroupBoxLayout.ResumeLayout(false);
            _ShuntResistanceConfigurationGroupBox.ResumeLayout(false);
            _ShuntResistanceConfigurationGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceLowLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceHighLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceVoltageLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ShuntResistanceCurrentLevelNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _ShuntLayout;
        private System.Windows.Forms.TableLayoutPanel _ShuntDisplayLayout;
        private System.Windows.Forms.TextBox _ShuntResistanceTextBox;
        private System.Windows.Forms.Label _ShuntResistanceTextBoxLabel;
        private System.Windows.Forms.TableLayoutPanel _ShuntGroupBoxLayout;
        private System.Windows.Forms.Button _MeasureShuntResistanceButton;
        private System.Windows.Forms.TableLayoutPanel _ShuntConfigureGroupBoxLayout;
        private System.Windows.Forms.GroupBox _ShuntResistanceConfigurationGroupBox;
        private System.Windows.Forms.Button _RestoreShuntResistanceDefaultsButton;
        private System.Windows.Forms.Button _ApplyNewShuntResistanceConfigurationButton;
        private System.Windows.Forms.Button _ApplyShuntResistanceConfigurationButton;
        private System.Windows.Forms.NumericUpDown _ShuntResistanceCurrentRangeNumeric;
        private System.Windows.Forms.Label _ShuntResistanceCurrentRangeNumericLabel;
        private System.Windows.Forms.NumericUpDown _ShuntResistanceLowLimitNumeric;
        private System.Windows.Forms.Label _ShuntResistanceLowLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ShuntResistanceHighLimitNumeric;
        private System.Windows.Forms.Label _ShuntResistanceHighLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ShuntResistanceVoltageLimitNumeric;
        private System.Windows.Forms.Label _ShuntResistanceVoltageLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ShuntResistanceCurrentLevelNumeric;
        private System.Windows.Forms.Label _ShuntResistanceCurrentLevelNumericLabel;
    }
}