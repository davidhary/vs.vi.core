using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class MeasurementsHeader
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _OutcomeLayout = new System.Windows.Forms.TableLayoutPanel();
            _ThermalTransientVoltageTextBox = new System.Windows.Forms.TextBox();
            _ThermalTransientVoltageTextBoxLabel = new System.Windows.Forms.Label();
            _FinalResistanceTextBox = new System.Windows.Forms.TextBox();
            _InitialResistanceTextBox = new System.Windows.Forms.TextBox();
            _FinalResistanceTextBoxLabel = new System.Windows.Forms.Label();
            _InitialResistanceTextBoxLabel = new System.Windows.Forms.Label();
            _OutcomePictureBox = new System.Windows.Forms.PictureBox();
            _AlertsPictureBox = new System.Windows.Forms.PictureBox();
            _OutcomeTextBox = new System.Windows.Forms.TextBox();
            _OutcomeTextBoxLabel = new System.Windows.Forms.Label();
            _OutcomeLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_OutcomePictureBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_AlertsPictureBox).BeginInit();
            SuspendLayout();
            // 
            // _OutcomeLayout
            // 
            _OutcomeLayout.BackColor = System.Drawing.Color.Transparent;
            _OutcomeLayout.ColumnCount = 6;
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _OutcomeLayout.Controls.Add(_ThermalTransientVoltageTextBox, 2, 2);
            _OutcomeLayout.Controls.Add(_ThermalTransientVoltageTextBoxLabel, 2, 1);
            _OutcomeLayout.Controls.Add(_FinalResistanceTextBox, 3, 2);
            _OutcomeLayout.Controls.Add(_InitialResistanceTextBox, 1, 2);
            _OutcomeLayout.Controls.Add(_FinalResistanceTextBoxLabel, 3, 1);
            _OutcomeLayout.Controls.Add(_InitialResistanceTextBoxLabel, 1, 1);
            _OutcomeLayout.Controls.Add(_OutcomePictureBox, 5, 2);
            _OutcomeLayout.Controls.Add(_AlertsPictureBox, 0, 2);
            _OutcomeLayout.Controls.Add(_OutcomeTextBox, 4, 2);
            _OutcomeLayout.Controls.Add(_OutcomeTextBoxLabel, 4, 1);
            _OutcomeLayout.Dock = System.Windows.Forms.DockStyle.Top;
            _OutcomeLayout.Location = new System.Drawing.Point(0, 0);
            _OutcomeLayout.Name = "_OutcomeLayout";
            _OutcomeLayout.RowCount = 3;
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _OutcomeLayout.Size = new System.Drawing.Size(968, 59);
            _OutcomeLayout.TabIndex = 1;
            // 
            // _ThermalTransientVoltageTextBox
            // 
            _ThermalTransientVoltageTextBox.BackColor = System.Drawing.Color.Black;
            _ThermalTransientVoltageTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _ThermalTransientVoltageTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ThermalTransientVoltageTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ThermalTransientVoltageTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _ThermalTransientVoltageTextBox.Location = new System.Drawing.Point(261, 24);
            _ThermalTransientVoltageTextBox.Name = "_ThermalTransientVoltageTextBox";
            _ThermalTransientVoltageTextBox.ReadOnly = true;
            _ThermalTransientVoltageTextBox.Size = new System.Drawing.Size(219, 32);
            _ThermalTransientVoltageTextBox.TabIndex = 3;
            _ThermalTransientVoltageTextBox.Text = "0.000";
            _ThermalTransientVoltageTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _ThermalTransientVoltageTextBoxLabel
            // 
            _ThermalTransientVoltageTextBoxLabel.AutoSize = true;
            _ThermalTransientVoltageTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _ThermalTransientVoltageTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ThermalTransientVoltageTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _ThermalTransientVoltageTextBoxLabel.Location = new System.Drawing.Point(261, 4);
            _ThermalTransientVoltageTextBoxLabel.Name = "_ThermalTransientVoltageTextBoxLabel";
            _ThermalTransientVoltageTextBoxLabel.Size = new System.Drawing.Size(219, 17);
            _ThermalTransientVoltageTextBoxLabel.TabIndex = 2;
            _ThermalTransientVoltageTextBoxLabel.Text = "THERMAL TRANSIENT [V]";
            _ThermalTransientVoltageTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _FinalResistanceTextBox
            // 
            _FinalResistanceTextBox.BackColor = System.Drawing.Color.Black;
            _FinalResistanceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _FinalResistanceTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _FinalResistanceTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FinalResistanceTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _FinalResistanceTextBox.Location = new System.Drawing.Point(486, 24);
            _FinalResistanceTextBox.Name = "_FinalResistanceTextBox";
            _FinalResistanceTextBox.ReadOnly = true;
            _FinalResistanceTextBox.Size = new System.Drawing.Size(219, 32);
            _FinalResistanceTextBox.TabIndex = 5;
            _FinalResistanceTextBox.Text = "0.000";
            _FinalResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _InitialResistanceTextBox
            // 
            _InitialResistanceTextBox.BackColor = System.Drawing.Color.Black;
            _InitialResistanceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _InitialResistanceTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _InitialResistanceTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _InitialResistanceTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _InitialResistanceTextBox.Location = new System.Drawing.Point(36, 24);
            _InitialResistanceTextBox.Name = "_InitialResistanceTextBox";
            _InitialResistanceTextBox.ReadOnly = true;
            _InitialResistanceTextBox.Size = new System.Drawing.Size(219, 32);
            _InitialResistanceTextBox.TabIndex = 1;
            _InitialResistanceTextBox.Text = "0.000";
            _InitialResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _FinalResistanceTextBoxLabel
            // 
            _FinalResistanceTextBoxLabel.AutoSize = true;
            _FinalResistanceTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _FinalResistanceTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _FinalResistanceTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _FinalResistanceTextBoxLabel.Location = new System.Drawing.Point(486, 4);
            _FinalResistanceTextBoxLabel.Name = "_FinalResistanceTextBoxLabel";
            _FinalResistanceTextBoxLabel.Size = new System.Drawing.Size(219, 17);
            _FinalResistanceTextBoxLabel.TabIndex = 4;
            _FinalResistanceTextBoxLabel.Text = "FINAL RESISTANCE [Ω]";
            _FinalResistanceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _InitialResistanceTextBoxLabel
            // 
            _InitialResistanceTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _InitialResistanceTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _InitialResistanceTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _InitialResistanceTextBoxLabel.Location = new System.Drawing.Point(36, 4);
            _InitialResistanceTextBoxLabel.Name = "_InitialResistanceTextBoxLabel";
            _InitialResistanceTextBoxLabel.Size = new System.Drawing.Size(219, 17);
            _InitialResistanceTextBoxLabel.TabIndex = 0;
            _InitialResistanceTextBoxLabel.Text = "INITIAL RESISTANCE [Ω]";
            _InitialResistanceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _OutcomePictureBox
            // 
            _OutcomePictureBox.BackColor = System.Drawing.Color.Black;
            _OutcomePictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            _OutcomePictureBox.Image = My.Resources.Resources.Good;
            _OutcomePictureBox.Location = new System.Drawing.Point(936, 24);
            _OutcomePictureBox.Name = "_OutcomePictureBox";
            _OutcomePictureBox.Size = new System.Drawing.Size(29, 28);
            _OutcomePictureBox.TabIndex = 6;
            _OutcomePictureBox.TabStop = false;
            // 
            // _AlertsPictureBox
            // 
            _AlertsPictureBox.BackColor = System.Drawing.Color.Black;
            _AlertsPictureBox.Dock = System.Windows.Forms.DockStyle.Top;
            _AlertsPictureBox.Image = My.Resources.Resources.Bad;
            _AlertsPictureBox.Location = new System.Drawing.Point(3, 24);
            _AlertsPictureBox.Name = "_AlertsPictureBox";
            _AlertsPictureBox.Size = new System.Drawing.Size(27, 28);
            _AlertsPictureBox.TabIndex = 7;
            _AlertsPictureBox.TabStop = false;
            // 
            // _OutcomeTextBox
            // 
            _OutcomeTextBox.BackColor = System.Drawing.Color.Black;
            _OutcomeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _OutcomeTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _OutcomeTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OutcomeTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _OutcomeTextBox.Location = new System.Drawing.Point(711, 24);
            _OutcomeTextBox.Name = "_OutcomeTextBox";
            _OutcomeTextBox.ReadOnly = true;
            _OutcomeTextBox.Size = new System.Drawing.Size(219, 32);
            _OutcomeTextBox.TabIndex = 5;
            _OutcomeTextBox.Text = "COMPLIANCE";
            _OutcomeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _OutcomeTextBoxLabel
            // 
            _OutcomeTextBoxLabel.AutoSize = true;
            _OutcomeTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _OutcomeTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _OutcomeTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _OutcomeTextBoxLabel.Location = new System.Drawing.Point(711, 4);
            _OutcomeTextBoxLabel.Name = "_OutcomeTextBoxLabel";
            _OutcomeTextBoxLabel.Size = new System.Drawing.Size(219, 17);
            _OutcomeTextBoxLabel.TabIndex = 4;
            _OutcomeTextBoxLabel.Text = "OUTCOME";
            _OutcomeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // MeasurementsHeader
            // 
            BackColor = System.Drawing.Color.Black;
            Controls.Add(_OutcomeLayout);
            Name = "MeasurementsHeader";
            Size = new System.Drawing.Size(968, 61);
            _OutcomeLayout.ResumeLayout(false);
            _OutcomeLayout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_OutcomePictureBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)_AlertsPictureBox).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _OutcomeLayout;
        private System.Windows.Forms.TextBox _ThermalTransientVoltageTextBox;
        private System.Windows.Forms.Label _ThermalTransientVoltageTextBoxLabel;
        private System.Windows.Forms.TextBox _FinalResistanceTextBox;
        private System.Windows.Forms.TextBox _InitialResistanceTextBox;
        private System.Windows.Forms.Label _FinalResistanceTextBoxLabel;
        private System.Windows.Forms.Label _InitialResistanceTextBoxLabel;
        private System.Windows.Forms.PictureBox _OutcomePictureBox;
        private System.Windows.Forms.PictureBox _AlertsPictureBox;
        private System.Windows.Forms.TextBox _OutcomeTextBox;
        private System.Windows.Forms.Label _OutcomeTextBoxLabel;
    }
}
