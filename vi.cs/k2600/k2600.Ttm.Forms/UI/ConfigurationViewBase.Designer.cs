﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class ConfigurationViewBase
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _GroupBox = new System.Windows.Forms.GroupBox();
            __RestoreDefaultsButton = new System.Windows.Forms.Button();
            __RestoreDefaultsButton.Click += new EventHandler(RestoreDefaultsButton_Click);
            __ApplyNewConfigurationButton = new System.Windows.Forms.Button();
            __ApplyNewConfigurationButton.Click += new EventHandler(ApplyNewConfigurationButton_Click);
            __ApplyConfigurationButton = new System.Windows.Forms.Button();
            __ApplyConfigurationButton.Click += new EventHandler(ApplyConfigurationButton_Click);
            _ThermalTransientGroupBox = new System.Windows.Forms.GroupBox();
            _ThermalVoltageLimitLabel = new System.Windows.Forms.Label();
            _ThermalVoltageLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ThermalVoltageLowLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ThermalVoltageLowLimitNumericLabel = new System.Windows.Forms.Label();
            _ThermalVoltageHighLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ThermalVoltageHighLimitNumericLabel = new System.Windows.Forms.Label();
            _ThermalVoltageNumeric = new System.Windows.Forms.NumericUpDown();
            _ThermalVoltageNumericLabel = new System.Windows.Forms.Label();
            _ThermalCurrentNumeric = new System.Windows.Forms.NumericUpDown();
            _ThermalCurrentNumericLabel = new System.Windows.Forms.Label();
            _ColdResistanceConfigGroupBox = new System.Windows.Forms.GroupBox();
            _CheckContactsCheckBox = new System.Windows.Forms.CheckBox();
            _PostTransientDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _PostTransientDelayNumericLabel = new System.Windows.Forms.Label();
            _ColdResistanceLowLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ResistanceLowLimitNumericLabel = new System.Windows.Forms.Label();
            _ColdResistanceHighLimitNumeric = new System.Windows.Forms.NumericUpDown();
            _ResistanceHighLimitNumericLabel = new System.Windows.Forms.Label();
            _ColdVoltageNumeric = new System.Windows.Forms.NumericUpDown();
            _ColdVoltageNumericLabel = new System.Windows.Forms.Label();
            _ColdCurrentNumeric = new System.Windows.Forms.NumericUpDown();
            _ColdCurrentNumericLabel = new System.Windows.Forms.Label();
            _GroupBox.SuspendLayout();
            _ThermalTransientGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageLowLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageHighLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalCurrentNumeric).BeginInit();
            _ColdResistanceConfigGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_PostTransientDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ColdResistanceLowLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ColdResistanceHighLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ColdVoltageNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_ColdCurrentNumeric).BeginInit();
            SuspendLayout();
            // 
            // _GroupBox
            // 
            _GroupBox.Controls.Add(__RestoreDefaultsButton);
            _GroupBox.Controls.Add(__ApplyNewConfigurationButton);
            _GroupBox.Controls.Add(__ApplyConfigurationButton);
            _GroupBox.Controls.Add(_ThermalTransientGroupBox);
            _GroupBox.Controls.Add(_ColdResistanceConfigGroupBox);
            _GroupBox.Location = new System.Drawing.Point(0, 0);
            _GroupBox.Name = "_GroupBox";
            _GroupBox.Size = new System.Drawing.Size(544, 333);
            _GroupBox.TabIndex = 2;
            _GroupBox.TabStop = false;
            _GroupBox.Text = "TTM CONFIGURATION";
            // 
            // _RestoreDefaultsButton
            // 
            __RestoreDefaultsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __RestoreDefaultsButton.Location = new System.Drawing.Point(364, 294);
            __RestoreDefaultsButton.Name = "__RestoreDefaultsButton";
            __RestoreDefaultsButton.Size = new System.Drawing.Size(153, 30);
            __RestoreDefaultsButton.TabIndex = 4;
            __RestoreDefaultsButton.Text = "RESTORE DEFAULTS";
            __RestoreDefaultsButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyNewConfigurationButton
            // 
            __ApplyNewConfigurationButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplyNewConfigurationButton.Location = new System.Drawing.Point(196, 294);
            __ApplyNewConfigurationButton.Name = "__ApplyNewConfigurationButton";
            __ApplyNewConfigurationButton.Size = new System.Drawing.Size(153, 30);
            __ApplyNewConfigurationButton.TabIndex = 3;
            __ApplyNewConfigurationButton.Text = "APPLY CHANGES";
            __ApplyNewConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyConfigurationButton
            // 
            __ApplyConfigurationButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplyConfigurationButton.Location = new System.Drawing.Point(28, 294);
            __ApplyConfigurationButton.Name = "__ApplyConfigurationButton";
            __ApplyConfigurationButton.Size = new System.Drawing.Size(153, 30);
            __ApplyConfigurationButton.TabIndex = 2;
            __ApplyConfigurationButton.Text = "APPLY ALL";
            __ApplyConfigurationButton.UseVisualStyleBackColor = true;
            // 
            // _ThermalTransientGroupBox
            // 
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageLimitLabel);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageLimitNumeric);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageLowLimitNumeric);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageLowLimitNumericLabel);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageHighLimitNumeric);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageHighLimitNumericLabel);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageNumeric);
            _ThermalTransientGroupBox.Controls.Add(_ThermalVoltageNumericLabel);
            _ThermalTransientGroupBox.Controls.Add(_ThermalCurrentNumeric);
            _ThermalTransientGroupBox.Controls.Add(_ThermalCurrentNumericLabel);
            _ThermalTransientGroupBox.Location = new System.Drawing.Point(277, 23);
            _ThermalTransientGroupBox.Name = "_ThermalTransientGroupBox";
            _ThermalTransientGroupBox.Size = new System.Drawing.Size(258, 259);
            _ThermalTransientGroupBox.TabIndex = 0;
            _ThermalTransientGroupBox.TabStop = false;
            _ThermalTransientGroupBox.Text = "THERMAL TRANSIENT";
            // 
            // _ThermalVoltageLimitLabel
            // 
            _ThermalVoltageLimitLabel.AutoSize = true;
            _ThermalVoltageLimitLabel.Location = new System.Drawing.Point(45, 113);
            _ThermalVoltageLimitLabel.Name = "_ThermalVoltageLimitLabel";
            _ThermalVoltageLimitLabel.Size = new System.Drawing.Size(121, 17);
            _ThermalVoltageLimitLabel.TabIndex = 4;
            _ThermalVoltageLimitLabel.Text = "VOLTAGE LIMIT &[V]:";
            _ThermalVoltageLimitLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ThermalVoltageLimitNumeric
            // 
            _ThermalVoltageLimitNumeric.DecimalPlaces = 3;
            _ThermalVoltageLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ThermalVoltageLimitNumeric.Location = new System.Drawing.Point(168, 109);
            _ThermalVoltageLimitNumeric.Maximum = new decimal(new int[] { 9999, 0, 0, 196608 });
            _ThermalVoltageLimitNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 131072 });
            _ThermalVoltageLimitNumeric.Name = "_ThermalVoltageLimitNumeric";
            _ThermalVoltageLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ThermalVoltageLimitNumeric.TabIndex = 5;
            _ThermalVoltageLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ThermalVoltageLimitNumeric.Value = new decimal(new int[] { 99, 0, 0, 131072 });
            // 
            // _ThermalVoltageLowLimitNumeric
            // 
            _ThermalVoltageLowLimitNumeric.DecimalPlaces = 3;
            _ThermalVoltageLowLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ThermalVoltageLowLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _ThermalVoltageLowLimitNumeric.Location = new System.Drawing.Point(168, 189);
            _ThermalVoltageLowLimitNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            _ThermalVoltageLowLimitNumeric.Name = "_ThermalVoltageLowLimitNumeric";
            _ThermalVoltageLowLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ThermalVoltageLowLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ThermalVoltageLowLimitNumeric.TabIndex = 9;
            _ThermalVoltageLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ThermalVoltageLowLimitNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ThermalVoltageLowLimitNumericLabel
            // 
            _ThermalVoltageLowLimitNumericLabel.AutoSize = true;
            _ThermalVoltageLowLimitNumericLabel.Location = new System.Drawing.Point(72, 193);
            _ThermalVoltageLowLimitNumericLabel.Name = "_ThermalVoltageLowLimitNumericLabel";
            _ThermalVoltageLowLimitNumericLabel.Size = new System.Drawing.Size(94, 17);
            _ThermalVoltageLowLimitNumericLabel.TabIndex = 8;
            _ThermalVoltageLowLimitNumericLabel.Text = "LOW LIMI&T [V]:";
            _ThermalVoltageLowLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ThermalVoltageHighLimitNumeric
            // 
            _ThermalVoltageHighLimitNumeric.DecimalPlaces = 3;
            _ThermalVoltageHighLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ThermalVoltageHighLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _ThermalVoltageHighLimitNumeric.Location = new System.Drawing.Point(168, 149);
            _ThermalVoltageHighLimitNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            _ThermalVoltageHighLimitNumeric.Name = "_ThermalVoltageHighLimitNumeric";
            _ThermalVoltageHighLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ThermalVoltageHighLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ThermalVoltageHighLimitNumeric.TabIndex = 7;
            _ThermalVoltageHighLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ThermalVoltageHighLimitNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ThermalVoltageHighLimitNumericLabel
            // 
            _ThermalVoltageHighLimitNumericLabel.AutoSize = true;
            _ThermalVoltageHighLimitNumericLabel.Location = new System.Drawing.Point(70, 153);
            _ThermalVoltageHighLimitNumericLabel.Name = "_ThermalVoltageHighLimitNumericLabel";
            _ThermalVoltageHighLimitNumericLabel.Size = new System.Drawing.Size(96, 17);
            _ThermalVoltageHighLimitNumericLabel.TabIndex = 6;
            _ThermalVoltageHighLimitNumericLabel.Text = "HI&GH LIMIT [V]:";
            _ThermalVoltageHighLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ThermalVoltageNumeric
            // 
            _ThermalVoltageNumeric.DecimalPlaces = 3;
            _ThermalVoltageNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ThermalVoltageNumeric.Increment = new decimal(new int[] { 1, 0, 0, 131072 });
            _ThermalVoltageNumeric.Location = new System.Drawing.Point(168, 69);
            _ThermalVoltageNumeric.Maximum = new decimal(new int[] { 99, 0, 0, 131072 });
            _ThermalVoltageNumeric.Name = "_ThermalVoltageNumeric";
            _ThermalVoltageNumeric.Size = new System.Drawing.Size(75, 25);
            _ThermalVoltageNumeric.TabIndex = 3;
            _ThermalVoltageNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ThermalVoltageNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ThermalVoltageNumericLabel
            // 
            _ThermalVoltageNumericLabel.AutoSize = true;
            _ThermalVoltageNumericLabel.Location = new System.Drawing.Point(25, 73);
            _ThermalVoltageNumericLabel.Name = "_ThermalVoltageNumericLabel";
            _ThermalVoltageNumericLabel.Size = new System.Drawing.Size(141, 17);
            _ThermalVoltageNumericLabel.TabIndex = 2;
            _ThermalVoltageNumericLabel.Text = "VOLTAGE C&HANGE [V]:";
            _ThermalVoltageNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ThermalCurrentNumeric
            // 
            _ThermalCurrentNumeric.DecimalPlaces = 3;
            _ThermalCurrentNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ThermalCurrentNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _ThermalCurrentNumeric.Location = new System.Drawing.Point(168, 29);
            _ThermalCurrentNumeric.Maximum = new decimal(new int[] { 1514, 0, 0, 196608 });
            _ThermalCurrentNumeric.Name = "_ThermalCurrentNumeric";
            _ThermalCurrentNumeric.Size = new System.Drawing.Size(75, 25);
            _ThermalCurrentNumeric.TabIndex = 1;
            _ThermalCurrentNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ThermalCurrentNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ThermalCurrentNumericLabel
            // 
            _ThermalCurrentNumericLabel.AutoSize = true;
            _ThermalCurrentNumericLabel.Location = new System.Drawing.Point(40, 33);
            _ThermalCurrentNumericLabel.Name = "_ThermalCurrentNumericLabel";
            _ThermalCurrentNumericLabel.Size = new System.Drawing.Size(126, 17);
            _ThermalCurrentNumericLabel.TabIndex = 0;
            _ThermalCurrentNumericLabel.Text = "CURR&ENT LEVEL [A]:";
            _ThermalCurrentNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ColdResistanceConfigGroupBox
            // 
            _ColdResistanceConfigGroupBox.Controls.Add(_CheckContactsCheckBox);
            _ColdResistanceConfigGroupBox.Controls.Add(_PostTransientDelayNumeric);
            _ColdResistanceConfigGroupBox.Controls.Add(_PostTransientDelayNumericLabel);
            _ColdResistanceConfigGroupBox.Controls.Add(_ColdResistanceLowLimitNumeric);
            _ColdResistanceConfigGroupBox.Controls.Add(_ResistanceLowLimitNumericLabel);
            _ColdResistanceConfigGroupBox.Controls.Add(_ColdResistanceHighLimitNumeric);
            _ColdResistanceConfigGroupBox.Controls.Add(_ResistanceHighLimitNumericLabel);
            _ColdResistanceConfigGroupBox.Controls.Add(_ColdVoltageNumeric);
            _ColdResistanceConfigGroupBox.Controls.Add(_ColdVoltageNumericLabel);
            _ColdResistanceConfigGroupBox.Controls.Add(_ColdCurrentNumeric);
            _ColdResistanceConfigGroupBox.Controls.Add(_ColdCurrentNumericLabel);
            _ColdResistanceConfigGroupBox.Location = new System.Drawing.Point(9, 23);
            _ColdResistanceConfigGroupBox.Name = "_ColdResistanceConfigGroupBox";
            _ColdResistanceConfigGroupBox.Size = new System.Drawing.Size(258, 259);
            _ColdResistanceConfigGroupBox.TabIndex = 0;
            _ColdResistanceConfigGroupBox.TabStop = false;
            _ColdResistanceConfigGroupBox.Text = "COLD RESISTANCE";
            // 
            // _CheckContactsCheckBox
            // 
            _CheckContactsCheckBox.AutoSize = true;
            _CheckContactsCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _CheckContactsCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _CheckContactsCheckBox.Location = new System.Drawing.Point(36, 233);
            _CheckContactsCheckBox.Name = "_CheckContactsCheckBox";
            _CheckContactsCheckBox.Size = new System.Drawing.Size(139, 21);
            _CheckContactsCheckBox.TabIndex = 5;
            _CheckContactsCheckBox.Text = "CHECK CONTACTS:";
            _CheckContactsCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            _CheckContactsCheckBox.UseVisualStyleBackColor = true;
            // 
            // _PostTransientDelayNumeric
            // 
            _PostTransientDelayNumeric.DecimalPlaces = 2;
            _PostTransientDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _PostTransientDelayNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _PostTransientDelayNumeric.Location = new System.Drawing.Point(162, 189);
            _PostTransientDelayNumeric.Maximum = new decimal(new int[] { 600, 0, 0, 0 });
            _PostTransientDelayNumeric.Name = "_PostTransientDelayNumeric";
            _PostTransientDelayNumeric.Size = new System.Drawing.Size(75, 25);
            _PostTransientDelayNumeric.TabIndex = 9;
            _PostTransientDelayNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _PostTransientDelayNumeric.Value = new decimal(new int[] { 1, 0, 0, 65536 });
            // 
            // _PostTransientDelayNumericLabel
            // 
            _PostTransientDelayNumericLabel.AutoSize = true;
            _PostTransientDelayNumericLabel.Location = new System.Drawing.Point(28, 193);
            _PostTransientDelayNumericLabel.Name = "_PostTransientDelayNumericLabel";
            _PostTransientDelayNumericLabel.Size = new System.Drawing.Size(132, 17);
            _PostTransientDelayNumericLabel.TabIndex = 8;
            _PostTransientDelayNumericLabel.Text = "POST TTM &DELAY [S]:";
            _PostTransientDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ColdResistanceLowLimitNumeric
            // 
            _ColdResistanceLowLimitNumeric.DecimalPlaces = 3;
            _ColdResistanceLowLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ColdResistanceLowLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ColdResistanceLowLimitNumeric.Location = new System.Drawing.Point(162, 149);
            _ColdResistanceLowLimitNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _ColdResistanceLowLimitNumeric.Name = "_ColdResistanceLowLimitNumeric";
            _ColdResistanceLowLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ColdResistanceLowLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ColdResistanceLowLimitNumeric.TabIndex = 7;
            _ColdResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ColdResistanceLowLimitNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ResistanceLowLimitNumericLabel
            // 
            _ResistanceLowLimitNumericLabel.AutoSize = true;
            _ResistanceLowLimitNumericLabel.Location = new System.Drawing.Point(64, 153);
            _ResistanceLowLimitNumericLabel.Name = "_ResistanceLowLimitNumericLabel";
            _ResistanceLowLimitNumericLabel.Size = new System.Drawing.Size(96, 17);
            _ResistanceLowLimitNumericLabel.TabIndex = 6;
            _ResistanceLowLimitNumericLabel.Text = "LO&W LIMIT [Ω]:";
            _ResistanceLowLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ColdResistanceHighLimitNumeric
            // 
            _ColdResistanceHighLimitNumeric.DecimalPlaces = 3;
            _ColdResistanceHighLimitNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ColdResistanceHighLimitNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ColdResistanceHighLimitNumeric.Location = new System.Drawing.Point(162, 109);
            _ColdResistanceHighLimitNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _ColdResistanceHighLimitNumeric.Name = "_ColdResistanceHighLimitNumeric";
            _ColdResistanceHighLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ColdResistanceHighLimitNumeric.Size = new System.Drawing.Size(75, 25);
            _ColdResistanceHighLimitNumeric.TabIndex = 5;
            _ColdResistanceHighLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ColdResistanceHighLimitNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _ResistanceHighLimitNumericLabel
            // 
            _ResistanceHighLimitNumericLabel.AutoSize = true;
            _ResistanceHighLimitNumericLabel.Location = new System.Drawing.Point(62, 113);
            _ResistanceHighLimitNumericLabel.Name = "_ResistanceHighLimitNumericLabel";
            _ResistanceHighLimitNumericLabel.Size = new System.Drawing.Size(98, 17);
            _ResistanceHighLimitNumericLabel.TabIndex = 4;
            _ResistanceHighLimitNumericLabel.Text = "&HIGH LIMIT [Ω]:";
            _ResistanceHighLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ColdVoltageNumeric
            // 
            _ColdVoltageNumeric.DecimalPlaces = 2;
            _ColdVoltageNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ColdVoltageNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _ColdVoltageNumeric.Location = new System.Drawing.Point(162, 69);
            _ColdVoltageNumeric.Maximum = new decimal(new int[] { 2, 0, 0, 0 });
            _ColdVoltageNumeric.Name = "_ColdVoltageNumeric";
            _ColdVoltageNumeric.Size = new System.Drawing.Size(75, 25);
            _ColdVoltageNumeric.TabIndex = 3;
            _ColdVoltageNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ColdVoltageNumeric.Value = new decimal(new int[] { 1, 0, 0, 131072 });
            // 
            // _ColdVoltageNumericLabel
            // 
            _ColdVoltageNumericLabel.AutoSize = true;
            _ColdVoltageNumericLabel.Location = new System.Drawing.Point(39, 73);
            _ColdVoltageNumericLabel.Name = "_ColdVoltageNumericLabel";
            _ColdVoltageNumericLabel.Size = new System.Drawing.Size(121, 17);
            _ColdVoltageNumericLabel.TabIndex = 2;
            _ColdVoltageNumericLabel.Text = "&VOLTAGE LIMIT [V]:";
            _ColdVoltageNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ColdCurrentNumeric
            // 
            _ColdCurrentNumeric.DecimalPlaces = 4;
            _ColdCurrentNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ColdCurrentNumeric.Increment = new decimal(new int[] { 1, 0, 0, 196608 });
            _ColdCurrentNumeric.Location = new System.Drawing.Point(162, 29);
            _ColdCurrentNumeric.Maximum = new decimal(new int[] { 100, 0, 0, 262144 });
            _ColdCurrentNumeric.Name = "_ColdCurrentNumeric";
            _ColdCurrentNumeric.Size = new System.Drawing.Size(75, 25);
            _ColdCurrentNumeric.TabIndex = 1;
            _ColdCurrentNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _ColdCurrentNumeric.Value = new decimal(new int[] { 1, 0, 0, 262144 });
            // 
            // _ColdCurrentNumericLabel
            // 
            _ColdCurrentNumericLabel.AutoSize = true;
            _ColdCurrentNumericLabel.Location = new System.Drawing.Point(34, 33);
            _ColdCurrentNumericLabel.Name = "_ColdCurrentNumericLabel";
            _ColdCurrentNumericLabel.Size = new System.Drawing.Size(126, 17);
            _ColdCurrentNumericLabel.TabIndex = 0;
            _ColdCurrentNumericLabel.Text = "C&URRENT LEVEL [A]:";
            _ColdCurrentNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ConfigurationPanel
            // 
            BackColor = System.Drawing.Color.Transparent;
            Controls.Add(_GroupBox);
            Name = "ConfigurationPanel";
            Size = new System.Drawing.Size(544, 333);
            _GroupBox.ResumeLayout(false);
            _ThermalTransientGroupBox.ResumeLayout(false);
            _ThermalTransientGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageLowLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageHighLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalVoltageNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ThermalCurrentNumeric).EndInit();
            _ColdResistanceConfigGroupBox.ResumeLayout(false);
            _ColdResistanceConfigGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_PostTransientDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ColdResistanceLowLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ColdResistanceHighLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ColdVoltageNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_ColdCurrentNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.GroupBox _GroupBox;
        private System.Windows.Forms.Button __RestoreDefaultsButton;

        private System.Windows.Forms.Button _RestoreDefaultsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RestoreDefaultsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RestoreDefaultsButton != null)
                {
                    __RestoreDefaultsButton.Click -= RestoreDefaultsButton_Click;
                }

                __RestoreDefaultsButton = value;
                if (__RestoreDefaultsButton != null)
                {
                    __RestoreDefaultsButton.Click += RestoreDefaultsButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ApplyNewConfigurationButton;

        private System.Windows.Forms.Button _ApplyNewConfigurationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyNewConfigurationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyNewConfigurationButton != null)
                {
                    __ApplyNewConfigurationButton.Click -= ApplyNewConfigurationButton_Click;
                }

                __ApplyNewConfigurationButton = value;
                if (__ApplyNewConfigurationButton != null)
                {
                    __ApplyNewConfigurationButton.Click += ApplyNewConfigurationButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ApplyConfigurationButton;

        private System.Windows.Forms.Button _ApplyConfigurationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyConfigurationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyConfigurationButton != null)
                {
                    __ApplyConfigurationButton.Click -= ApplyConfigurationButton_Click;
                }

                __ApplyConfigurationButton = value;
                if (__ApplyConfigurationButton != null)
                {
                    __ApplyConfigurationButton.Click += ApplyConfigurationButton_Click;
                }
            }
        }

        private System.Windows.Forms.GroupBox _ThermalTransientGroupBox;
        private System.Windows.Forms.Label _ThermalVoltageLimitLabel;
        private System.Windows.Forms.NumericUpDown _ThermalVoltageLimitNumeric;
        private System.Windows.Forms.NumericUpDown _ThermalVoltageLowLimitNumeric;
        private System.Windows.Forms.Label _ThermalVoltageLowLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ThermalVoltageHighLimitNumeric;
        private System.Windows.Forms.Label _ThermalVoltageHighLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ThermalVoltageNumeric;
        private System.Windows.Forms.Label _ThermalVoltageNumericLabel;
        private System.Windows.Forms.NumericUpDown _ThermalCurrentNumeric;
        private System.Windows.Forms.Label _ThermalCurrentNumericLabel;
        private System.Windows.Forms.GroupBox _ColdResistanceConfigGroupBox;
        private System.Windows.Forms.CheckBox _CheckContactsCheckBox;
        private System.Windows.Forms.NumericUpDown _PostTransientDelayNumeric;
        private System.Windows.Forms.Label _PostTransientDelayNumericLabel;
        private System.Windows.Forms.NumericUpDown _ColdResistanceLowLimitNumeric;
        private System.Windows.Forms.Label _ResistanceLowLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ColdResistanceHighLimitNumeric;
        private System.Windows.Forms.Label _ResistanceHighLimitNumericLabel;
        private System.Windows.Forms.NumericUpDown _ColdVoltageNumeric;
        private System.Windows.Forms.Label _ColdVoltageNumericLabel;
        private System.Windows.Forms.NumericUpDown _ColdCurrentNumeric;
        private System.Windows.Forms.Label _ColdCurrentNumericLabel;
    }
}