using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Forms
{
    [DesignerGenerated()]
    public partial class ThermalTransientHeader
    {

        /// <summary>   UserControl overrides dispose to clean up the component list. </summary>
        /// <remarks>   David, 2021-09-07. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (disposing)
                {
                    ReleaseResources();
                    if (disposing)
                    {
                        components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _OutcomeLayout = new System.Windows.Forms.TableLayoutPanel();
            _AsymptoteTextBox = new System.Windows.Forms.TextBox();
            _ThermalTransientVoltageTextBoxLabel = new System.Windows.Forms.Label();
            _EstimatedVoltageTextBox = new System.Windows.Forms.TextBox();
            _EstimatedVoltageTextBoxLabel = new System.Windows.Forms.Label();
            _TimeConstantTextBoxLabel = new System.Windows.Forms.Label();
            _TimeConstantTextBox = new System.Windows.Forms.TextBox();
            _CorrelationCoefficientTextBoxLabel = new System.Windows.Forms.Label();
            _CorrelationCoefficientTextBox = new System.Windows.Forms.TextBox();
            _StandardErrorTextBox = new System.Windows.Forms.TextBox();
            _IterationsCountTextBox = new System.Windows.Forms.TextBox();
            _OutcomeTextBox = new System.Windows.Forms.TextBox();
            _StandardErrorTextBoxLabel = new System.Windows.Forms.Label();
            _IterationsCountTextBoxLabel = new System.Windows.Forms.Label();
            _OutcomeTextBoxLabel = new System.Windows.Forms.Label();
            _OutcomeLayout.SuspendLayout();
            SuspendLayout();
            // 
            // _OutcomeLayout
            // 
            _OutcomeLayout.BackColor = System.Drawing.Color.Transparent;
            _OutcomeLayout.ColumnCount = 7;
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667f));
            _OutcomeLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _OutcomeLayout.Controls.Add(_AsymptoteTextBox, 1, 2);
            _OutcomeLayout.Controls.Add(_ThermalTransientVoltageTextBoxLabel, 1, 1);
            _OutcomeLayout.Controls.Add(_EstimatedVoltageTextBox, 2, 2);
            _OutcomeLayout.Controls.Add(_EstimatedVoltageTextBoxLabel, 2, 1);
            _OutcomeLayout.Controls.Add(_TimeConstantTextBoxLabel, 0, 1);
            _OutcomeLayout.Controls.Add(_TimeConstantTextBox, 0, 2);
            _OutcomeLayout.Controls.Add(_CorrelationCoefficientTextBoxLabel, 3, 1);
            _OutcomeLayout.Controls.Add(_CorrelationCoefficientTextBox, 3, 2);
            _OutcomeLayout.Controls.Add(_StandardErrorTextBox, 4, 2);
            _OutcomeLayout.Controls.Add(_IterationsCountTextBox, 5, 2);
            _OutcomeLayout.Controls.Add(_OutcomeTextBox, 6, 2);
            _OutcomeLayout.Controls.Add(_StandardErrorTextBoxLabel, 4, 1);
            _OutcomeLayout.Controls.Add(_IterationsCountTextBoxLabel, 5, 1);
            _OutcomeLayout.Controls.Add(_OutcomeTextBoxLabel, 6, 1);
            _OutcomeLayout.Dock = System.Windows.Forms.DockStyle.Top;
            _OutcomeLayout.ForeColor = System.Drawing.SystemColors.ButtonFace;
            _OutcomeLayout.Location = new System.Drawing.Point(0, 0);
            _OutcomeLayout.Name = "_OutcomeLayout";
            _OutcomeLayout.RowCount = 3;
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _OutcomeLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _OutcomeLayout.Size = new System.Drawing.Size(968, 65);
            _OutcomeLayout.TabIndex = 1;
            // 
            // _AsymptoteTextBox
            // 
            _AsymptoteTextBox.BackColor = System.Drawing.Color.Black;
            _AsymptoteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _AsymptoteTextBox.CausesValidation = false;
            _AsymptoteTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _AsymptoteTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _AsymptoteTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _AsymptoteTextBox.Location = new System.Drawing.Point(137, 23);
            _AsymptoteTextBox.Name = "_AsymptoteTextBox";
            _AsymptoteTextBox.ReadOnly = true;
            _AsymptoteTextBox.Size = new System.Drawing.Size(128, 32);
            _AsymptoteTextBox.TabIndex = 3;
            _AsymptoteTextBox.Text = "0.0";
            _AsymptoteTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_AsymptoteTextBox, "Asymptotic voltage");
            // 
            // _ThermalTransientVoltageTextBoxLabel
            // 
            _ThermalTransientVoltageTextBoxLabel.AutoSize = true;
            _ThermalTransientVoltageTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _ThermalTransientVoltageTextBoxLabel.CausesValidation = false;
            _ThermalTransientVoltageTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ThermalTransientVoltageTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _ThermalTransientVoltageTextBoxLabel.Location = new System.Drawing.Point(137, 3);
            _ThermalTransientVoltageTextBoxLabel.Name = "_ThermalTransientVoltageTextBoxLabel";
            _ThermalTransientVoltageTextBoxLabel.Size = new System.Drawing.Size(128, 17);
            _ThermalTransientVoltageTextBoxLabel.TabIndex = 2;
            _ThermalTransientVoltageTextBoxLabel.Text = "V ∞ [mV]";
            _ThermalTransientVoltageTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _EstimatedVoltageTextBox
            // 
            _EstimatedVoltageTextBox.BackColor = System.Drawing.Color.Black;
            _EstimatedVoltageTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _EstimatedVoltageTextBox.CausesValidation = false;
            _EstimatedVoltageTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _EstimatedVoltageTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _EstimatedVoltageTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _EstimatedVoltageTextBox.Location = new System.Drawing.Point(271, 23);
            _EstimatedVoltageTextBox.Name = "_EstimatedVoltageTextBox";
            _EstimatedVoltageTextBox.ReadOnly = true;
            _EstimatedVoltageTextBox.Size = new System.Drawing.Size(128, 32);
            _EstimatedVoltageTextBox.TabIndex = 5;
            _EstimatedVoltageTextBox.Text = "0.0";
            _EstimatedVoltageTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_EstimatedVoltageTextBox, "Voltage at the end of the pulse");
            // 
            // _EstimatedVoltageTextBoxLabel
            // 
            _EstimatedVoltageTextBoxLabel.AutoSize = true;
            _EstimatedVoltageTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _EstimatedVoltageTextBoxLabel.CausesValidation = false;
            _EstimatedVoltageTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _EstimatedVoltageTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _EstimatedVoltageTextBoxLabel.Location = new System.Drawing.Point(271, 3);
            _EstimatedVoltageTextBoxLabel.Name = "_EstimatedVoltageTextBoxLabel";
            _EstimatedVoltageTextBoxLabel.Size = new System.Drawing.Size(128, 17);
            _EstimatedVoltageTextBoxLabel.TabIndex = 4;
            _EstimatedVoltageTextBoxLabel.Text = "V [mV]";
            _EstimatedVoltageTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _TimeConstantTextBoxLabel
            // 
            _TimeConstantTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _TimeConstantTextBoxLabel.CausesValidation = false;
            _TimeConstantTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _TimeConstantTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _TimeConstantTextBoxLabel.Location = new System.Drawing.Point(3, 3);
            _TimeConstantTextBoxLabel.Name = "_TimeConstantTextBoxLabel";
            _TimeConstantTextBoxLabel.Size = new System.Drawing.Size(128, 17);
            _TimeConstantTextBoxLabel.TabIndex = 0;
            _TimeConstantTextBoxLabel.Text = "TC [ms]";
            _TimeConstantTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _TimeConstantTextBox
            // 
            _TimeConstantTextBox.BackColor = System.Drawing.Color.Black;
            _TimeConstantTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _TimeConstantTextBox.CausesValidation = false;
            _TimeConstantTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _TimeConstantTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TimeConstantTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _TimeConstantTextBox.Location = new System.Drawing.Point(3, 23);
            _TimeConstantTextBox.Name = "_TimeConstantTextBox";
            _TimeConstantTextBox.ReadOnly = true;
            _TimeConstantTextBox.Size = new System.Drawing.Size(128, 32);
            _TimeConstantTextBox.TabIndex = 1;
            _TimeConstantTextBox.Text = "0.00";
            _TimeConstantTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_TimeConstantTextBox, "Time constant");
            // 
            // _CorrelationCoefficientTextBoxLabel
            // 
            _CorrelationCoefficientTextBoxLabel.AutoSize = true;
            _CorrelationCoefficientTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _CorrelationCoefficientTextBoxLabel.CausesValidation = false;
            _CorrelationCoefficientTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _CorrelationCoefficientTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _CorrelationCoefficientTextBoxLabel.Location = new System.Drawing.Point(405, 3);
            _CorrelationCoefficientTextBoxLabel.Name = "_CorrelationCoefficientTextBoxLabel";
            _CorrelationCoefficientTextBoxLabel.Size = new System.Drawing.Size(128, 17);
            _CorrelationCoefficientTextBoxLabel.TabIndex = 4;
            _CorrelationCoefficientTextBoxLabel.Text = "R";
            _CorrelationCoefficientTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _CorrelationCoefficientTextBox
            // 
            _CorrelationCoefficientTextBox.BackColor = System.Drawing.Color.Black;
            _CorrelationCoefficientTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _CorrelationCoefficientTextBox.CausesValidation = false;
            _CorrelationCoefficientTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _CorrelationCoefficientTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _CorrelationCoefficientTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _CorrelationCoefficientTextBox.Location = new System.Drawing.Point(405, 23);
            _CorrelationCoefficientTextBox.Name = "_CorrelationCoefficientTextBox";
            _CorrelationCoefficientTextBox.ReadOnly = true;
            _CorrelationCoefficientTextBox.Size = new System.Drawing.Size(128, 32);
            _CorrelationCoefficientTextBox.TabIndex = 8;
            _CorrelationCoefficientTextBox.Text = "0.0000";
            _CorrelationCoefficientTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_CorrelationCoefficientTextBox, "Correlation coefficient.");
            // 
            // _StandardErrorTextBox
            // 
            _StandardErrorTextBox.BackColor = System.Drawing.Color.Black;
            _StandardErrorTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _StandardErrorTextBox.CausesValidation = false;
            _StandardErrorTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _StandardErrorTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _StandardErrorTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _StandardErrorTextBox.Location = new System.Drawing.Point(539, 23);
            _StandardErrorTextBox.Name = "_StandardErrorTextBox";
            _StandardErrorTextBox.ReadOnly = true;
            _StandardErrorTextBox.Size = new System.Drawing.Size(128, 32);
            _StandardErrorTextBox.TabIndex = 8;
            _StandardErrorTextBox.Text = "0.00";
            _StandardErrorTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_StandardErrorTextBox, "Standard Error");
            // 
            // _IterationsCountTextBox
            // 
            _IterationsCountTextBox.BackColor = System.Drawing.Color.Black;
            _IterationsCountTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _IterationsCountTextBox.CausesValidation = false;
            _IterationsCountTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _IterationsCountTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _IterationsCountTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _IterationsCountTextBox.Location = new System.Drawing.Point(673, 23);
            _IterationsCountTextBox.Name = "_IterationsCountTextBox";
            _IterationsCountTextBox.ReadOnly = true;
            _IterationsCountTextBox.Size = new System.Drawing.Size(128, 32);
            _IterationsCountTextBox.TabIndex = 8;
            _IterationsCountTextBox.Text = "0";
            _IterationsCountTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_IterationsCountTextBox, "Number of iterations required to estimate the best model");
            // 
            // _OutcomeTextBox
            // 
            _OutcomeTextBox.BackColor = System.Drawing.Color.Black;
            _OutcomeTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _OutcomeTextBox.CausesValidation = false;
            _OutcomeTextBox.Dock = System.Windows.Forms.DockStyle.Bottom;
            _OutcomeTextBox.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _OutcomeTextBox.ForeColor = System.Drawing.Color.Aquamarine;
            _OutcomeTextBox.Location = new System.Drawing.Point(807, 23);
            _OutcomeTextBox.Name = "_OutcomeTextBox";
            _OutcomeTextBox.ReadOnly = true;
            _OutcomeTextBox.Size = new System.Drawing.Size(158, 32);
            _OutcomeTextBox.TabIndex = 8;
            _OutcomeTextBox.Text = "OPTIMIZED";
            _OutcomeTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _StandardErrorTextBoxLabel
            // 
            _StandardErrorTextBoxLabel.AutoSize = true;
            _StandardErrorTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _StandardErrorTextBoxLabel.CausesValidation = false;
            _StandardErrorTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _StandardErrorTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _StandardErrorTextBoxLabel.Location = new System.Drawing.Point(539, 3);
            _StandardErrorTextBoxLabel.Name = "_StandardErrorTextBoxLabel";
            _StandardErrorTextBoxLabel.Size = new System.Drawing.Size(128, 17);
            _StandardErrorTextBoxLabel.TabIndex = 4;
            _StandardErrorTextBoxLabel.Text = "σ [mV]";
            _StandardErrorTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            ToolTip.SetToolTip(_StandardErrorTextBoxLabel, "Standard Error");
            // 
            // _IterationsCountTextBoxLabel
            // 
            _IterationsCountTextBoxLabel.AutoSize = true;
            _IterationsCountTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _IterationsCountTextBoxLabel.CausesValidation = false;
            _IterationsCountTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _IterationsCountTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _IterationsCountTextBoxLabel.Location = new System.Drawing.Point(673, 3);
            _IterationsCountTextBoxLabel.Name = "_IterationsCountTextBoxLabel";
            _IterationsCountTextBoxLabel.Size = new System.Drawing.Size(128, 17);
            _IterationsCountTextBoxLabel.TabIndex = 4;
            _IterationsCountTextBoxLabel.Text = "N";
            _IterationsCountTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // _OutcomeTextBoxLabel
            // 
            _OutcomeTextBoxLabel.AutoSize = true;
            _OutcomeTextBoxLabel.BackColor = System.Drawing.Color.Black;
            _OutcomeTextBoxLabel.CausesValidation = false;
            _OutcomeTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _OutcomeTextBoxLabel.ForeColor = System.Drawing.Color.Yellow;
            _OutcomeTextBoxLabel.Location = new System.Drawing.Point(807, 3);
            _OutcomeTextBoxLabel.Name = "_OutcomeTextBoxLabel";
            _OutcomeTextBoxLabel.Size = new System.Drawing.Size(158, 17);
            _OutcomeTextBoxLabel.TabIndex = 4;
            _OutcomeTextBoxLabel.Text = "Outcome";
            _OutcomeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // ThermalTransientHeader
            // 
            BackColor = System.Drawing.Color.Black;
            Controls.Add(_OutcomeLayout);
            Name = "ThermalTransientHeader";
            Size = new System.Drawing.Size(968, 59);
            _OutcomeLayout.ResumeLayout(false);
            _OutcomeLayout.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _OutcomeLayout;
        private System.Windows.Forms.TextBox _AsymptoteTextBox;
        private System.Windows.Forms.Label _ThermalTransientVoltageTextBoxLabel;
        private System.Windows.Forms.TextBox _EstimatedVoltageTextBox;
        private System.Windows.Forms.TextBox _TimeConstantTextBox;
        private System.Windows.Forms.Label _EstimatedVoltageTextBoxLabel;
        private System.Windows.Forms.Label _TimeConstantTextBoxLabel;
        private System.Windows.Forms.Label _CorrelationCoefficientTextBoxLabel;
        private System.Windows.Forms.TextBox _CorrelationCoefficientTextBox;
        private System.Windows.Forms.TextBox _StandardErrorTextBox;
        private System.Windows.Forms.TextBox _IterationsCountTextBox;
        private System.Windows.Forms.TextBox _OutcomeTextBox;
        private System.Windows.Forms.Label _StandardErrorTextBoxLabel;
        private System.Windows.Forms.Label _IterationsCountTextBoxLabel;
        private System.Windows.Forms.Label _OutcomeTextBoxLabel;
    }
}
