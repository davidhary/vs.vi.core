﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{
    [DesignerGenerated()]
    public partial class ScriptsView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _UserScriptsList = new System.Windows.Forms.ListBox();
            __RemoveScriptButton = new System.Windows.Forms.Button();
            __RemoveScriptButton.Enter += new EventHandler(Button_Enter);
            __RemoveScriptButton.Leave += new EventHandler(Button_Leave);
            __RemoveScriptButton.Click += new EventHandler(RemoveScriptButton_Click);
            __TspScriptSelector = new Core.Controls.FileSelector();
            __TspScriptSelector.SelectedChanged += new EventHandler<EventArgs>(TspScriptSelector_SelectedChanged);
            _ScriptNameTextBox = new System.Windows.Forms.TextBox();
            __LoadScriptButton = new System.Windows.Forms.Button();
            __LoadScriptButton.Click += new EventHandler(LoadScriptButton_Click);
            __LoadAndRunButton = new System.Windows.Forms.Button();
            __LoadAndRunButton.Click += new EventHandler(LoadAndRunButton_Click);
            __RunScriptButton = new System.Windows.Forms.Button();
            __RunScriptButton.Click += new EventHandler(RunScriptButton_Click);
            _ScriptsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _ScriptsPanel4 = new System.Windows.Forms.Panel();
            _UserScriptsListLabel = new System.Windows.Forms.Label();
            __RefreshUserScriptsListButton = new System.Windows.Forms.Button();
            __RefreshUserScriptsListButton.Enter += new EventHandler(Button_Enter);
            __RefreshUserScriptsListButton.Leave += new EventHandler(Button_Leave);
            __RefreshUserScriptsListButton.Click += new EventHandler(RefreshUserScriptsListButton_Click);
            _ScriptsPanel3 = new System.Windows.Forms.Panel();
            _TspScriptSelectorLabel = new System.Windows.Forms.Label();
            _ScriptsPanel2 = new System.Windows.Forms.Panel();
            _RetainCodeOutlineToggle = new System.Windows.Forms.CheckBox();
            _ScriptNameTextBoxLabel = new System.Windows.Forms.Label();
            _ScriptsPanel1 = new System.Windows.Forms.Panel();
            _ScriptsTableLayoutPanel.SuspendLayout();
            _ScriptsPanel4.SuspendLayout();
            _ScriptsPanel3.SuspendLayout();
            _ScriptsPanel2.SuspendLayout();
            _ScriptsPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // _UserScriptsList
            // 
            _UserScriptsList.BackColor = System.Drawing.SystemColors.Window;
            _UserScriptsList.Cursor = System.Windows.Forms.Cursors.Default;
            _UserScriptsList.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _UserScriptsList.ForeColor = System.Drawing.SystemColors.WindowText;
            _UserScriptsList.ItemHeight = 17;
            _UserScriptsList.Location = new System.Drawing.Point(4, 28);
            _UserScriptsList.Name = "_UserScriptsList";
            _UserScriptsList.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _UserScriptsList.Size = new System.Drawing.Size(173, 140);
            _UserScriptsList.TabIndex = 2;
            // 
            // _RemoveScriptButton
            // 
            __RemoveScriptButton.BackColor = System.Drawing.SystemColors.Control;
            __RemoveScriptButton.Cursor = System.Windows.Forms.Cursors.Default;
            __RemoveScriptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __RemoveScriptButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __RemoveScriptButton.Location = new System.Drawing.Point(4, 176);
            __RemoveScriptButton.Name = "__RemoveScriptButton";
            __RemoveScriptButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __RemoveScriptButton.Size = new System.Drawing.Size(84, 33);
            __RemoveScriptButton.TabIndex = 0;
            __RemoveScriptButton.Text = "&REMOVE";
            __RemoveScriptButton.UseVisualStyleBackColor = true;
            // 
            // _TspScriptSelector
            // 
            __TspScriptSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __TspScriptSelector.BackColor = System.Drawing.SystemColors.Window;
            __TspScriptSelector.DefaultExtension = ".TSP";
            __TspScriptSelector.DialogFilter = "TSP Files (*.tsp; *.lua)|*.tsp;*.lua|Script Files (*.dbg)|*.dbg|All Files (*.*)|*" + ".*";
            __TspScriptSelector.DialogTitle = "SELECT TSP SCRIPT FILE";
            __TspScriptSelector.Dock = System.Windows.Forms.DockStyle.Top;
            __TspScriptSelector.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TspScriptSelector.Location = new System.Drawing.Point(3, 22);
            __TspScriptSelector.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __TspScriptSelector.Name = "__TspScriptSelector";
            __TspScriptSelector.Padding = new System.Windows.Forms.Padding(3);
            __TspScriptSelector.Size = new System.Drawing.Size(594, 27);
            __TspScriptSelector.TabIndex = 0;
            // 
            // _ScriptNameTextBox
            // 
            _ScriptNameTextBox.AcceptsReturn = true;
            _ScriptNameTextBox.BackColor = System.Drawing.SystemColors.Window;
            _ScriptNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _ScriptNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ScriptNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _ScriptNameTextBox.Location = new System.Drawing.Point(4, 27);
            _ScriptNameTextBox.MaxLength = 0;
            _ScriptNameTextBox.Name = "_ScriptNameTextBox";
            _ScriptNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ScriptNameTextBox.Size = new System.Drawing.Size(269, 25);
            _ScriptNameTextBox.TabIndex = 1;
            _ScriptNameTextBox.Text = "product.tsp";
            // 
            // _LoadScriptButton
            // 
            __LoadScriptButton.BackColor = System.Drawing.SystemColors.Control;
            __LoadScriptButton.Cursor = System.Windows.Forms.Cursors.Default;
            __LoadScriptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __LoadScriptButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __LoadScriptButton.Location = new System.Drawing.Point(15, 14);
            __LoadScriptButton.Name = "__LoadScriptButton";
            __LoadScriptButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __LoadScriptButton.Size = new System.Drawing.Size(145, 33);
            __LoadScriptButton.TabIndex = 0;
            __LoadScriptButton.Text = "&LOAD";
            __LoadScriptButton.UseVisualStyleBackColor = true;
            // 
            // _LoadAndRunButton
            // 
            __LoadAndRunButton.BackColor = System.Drawing.SystemColors.Control;
            __LoadAndRunButton.Cursor = System.Windows.Forms.Cursors.Default;
            __LoadAndRunButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __LoadAndRunButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __LoadAndRunButton.Location = new System.Drawing.Point(15, 86);
            __LoadAndRunButton.Name = "__LoadAndRunButton";
            __LoadAndRunButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __LoadAndRunButton.Size = new System.Drawing.Size(145, 33);
            __LoadAndRunButton.TabIndex = 2;
            __LoadAndRunButton.Text = "L&OAD  AND  RUN";
            __LoadAndRunButton.UseVisualStyleBackColor = true;
            // 
            // _RunScriptButton
            // 
            __RunScriptButton.BackColor = System.Drawing.SystemColors.Control;
            __RunScriptButton.Cursor = System.Windows.Forms.Cursors.Default;
            __RunScriptButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __RunScriptButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __RunScriptButton.Location = new System.Drawing.Point(15, 50);
            __RunScriptButton.Name = "__RunScriptButton";
            __RunScriptButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __RunScriptButton.Size = new System.Drawing.Size(145, 33);
            __RunScriptButton.TabIndex = 1;
            __RunScriptButton.Text = "&RUN";
            __RunScriptButton.UseVisualStyleBackColor = true;
            // 
            // _ScriptsTableLayoutPanel
            // 
            _ScriptsTableLayoutPanel.ColumnCount = 1;
            _ScriptsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _ScriptsTableLayoutPanel.Controls.Add(_ScriptsPanel4, 0, 5);
            _ScriptsTableLayoutPanel.Controls.Add(_ScriptsPanel3, 0, 3);
            _ScriptsTableLayoutPanel.Controls.Add(_ScriptsPanel2, 0, 1);
            _ScriptsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ScriptsTableLayoutPanel.Location = new System.Drawing.Point(1, 1);
            _ScriptsTableLayoutPanel.Name = "_ScriptsTableLayoutPanel";
            _ScriptsTableLayoutPanel.RowCount = 7;
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ScriptsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _ScriptsTableLayoutPanel.Size = new System.Drawing.Size(606, 466);
            _ScriptsTableLayoutPanel.TabIndex = 43;
            // 
            // _ScriptsPanel4
            // 
            _ScriptsPanel4.Controls.Add(_UserScriptsListLabel);
            _ScriptsPanel4.Controls.Add(_UserScriptsList);
            _ScriptsPanel4.Controls.Add(__RefreshUserScriptsListButton);
            _ScriptsPanel4.Controls.Add(__RemoveScriptButton);
            _ScriptsPanel4.Location = new System.Drawing.Point(3, 191);
            _ScriptsPanel4.Name = "_ScriptsPanel4";
            _ScriptsPanel4.Size = new System.Drawing.Size(191, 220);
            _ScriptsPanel4.TabIndex = 40;
            // 
            // _UserScriptsListLabel
            // 
            _UserScriptsListLabel.BackColor = System.Drawing.Color.Transparent;
            _UserScriptsListLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _UserScriptsListLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _UserScriptsListLabel.Location = new System.Drawing.Point(4, 8);
            _UserScriptsListLabel.Name = "_UserScriptsListLabel";
            _UserScriptsListLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _UserScriptsListLabel.Size = new System.Drawing.Size(173, 16);
            _UserScriptsListLabel.TabIndex = 18;
            _UserScriptsListLabel.Text = "User Scripts: ";
            // 
            // _RefreshUserScriptsListButton
            // 
            __RefreshUserScriptsListButton.BackColor = System.Drawing.SystemColors.Control;
            __RefreshUserScriptsListButton.Cursor = System.Windows.Forms.Cursors.Default;
            __RefreshUserScriptsListButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __RefreshUserScriptsListButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __RefreshUserScriptsListButton.Location = new System.Drawing.Point(93, 176);
            __RefreshUserScriptsListButton.Name = "__RefreshUserScriptsListButton";
            __RefreshUserScriptsListButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __RefreshUserScriptsListButton.Size = new System.Drawing.Size(84, 33);
            __RefreshUserScriptsListButton.TabIndex = 0;
            __RefreshUserScriptsListButton.Text = "REFRES&H";
            __RefreshUserScriptsListButton.UseVisualStyleBackColor = true;
            // 
            // _ScriptsPanel3
            // 
            _ScriptsPanel3.Controls.Add(__TspScriptSelector);
            _ScriptsPanel3.Controls.Add(_TspScriptSelectorLabel);
            _ScriptsPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            _ScriptsPanel3.Location = new System.Drawing.Point(3, 108);
            _ScriptsPanel3.Name = "_ScriptsPanel3";
            _ScriptsPanel3.Padding = new System.Windows.Forms.Padding(3, 6, 3, 6);
            _ScriptsPanel3.Size = new System.Drawing.Size(600, 57);
            _ScriptsPanel3.TabIndex = 39;
            // 
            // _TspScriptSelectorLabel
            // 
            _TspScriptSelectorLabel.BackColor = System.Drawing.Color.Transparent;
            _TspScriptSelectorLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _TspScriptSelectorLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _TspScriptSelectorLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _TspScriptSelectorLabel.Location = new System.Drawing.Point(3, 6);
            _TspScriptSelectorLabel.Name = "_TspScriptSelectorLabel";
            _TspScriptSelectorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _TspScriptSelectorLabel.Size = new System.Drawing.Size(594, 16);
            _TspScriptSelectorLabel.TabIndex = 17;
            _TspScriptSelectorLabel.Text = "Script File: ";
            // 
            // _ScriptsPanel2
            // 
            _ScriptsPanel2.Controls.Add(_RetainCodeOutlineToggle);
            _ScriptsPanel2.Controls.Add(_ScriptNameTextBoxLabel);
            _ScriptsPanel2.Controls.Add(_ScriptNameTextBox);
            _ScriptsPanel2.Location = new System.Drawing.Point(3, 23);
            _ScriptsPanel2.Name = "_ScriptsPanel2";
            _ScriptsPanel2.Size = new System.Drawing.Size(577, 59);
            _ScriptsPanel2.TabIndex = 38;
            // 
            // _RetainCodeOutlineToggle
            // 
            _RetainCodeOutlineToggle.AutoSize = true;
            _RetainCodeOutlineToggle.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _RetainCodeOutlineToggle.Location = new System.Drawing.Point(320, 21);
            _RetainCodeOutlineToggle.Name = "_RetainCodeOutlineToggle";
            _RetainCodeOutlineToggle.Size = new System.Drawing.Size(151, 21);
            _RetainCodeOutlineToggle.TabIndex = 2;
            _RetainCodeOutlineToggle.Text = "Retain Code Outline";
            _RetainCodeOutlineToggle.UseVisualStyleBackColor = true;
            // 
            // _ScriptNameTextBoxLabel
            // 
            _ScriptNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _ScriptNameTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ScriptNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ScriptNameTextBoxLabel.Location = new System.Drawing.Point(4, 7);
            _ScriptNameTextBoxLabel.Name = "_ScriptNameTextBoxLabel";
            _ScriptNameTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ScriptNameTextBoxLabel.Size = new System.Drawing.Size(80, 16);
            _ScriptNameTextBoxLabel.TabIndex = 0;
            _ScriptNameTextBoxLabel.Text = "Script Name: ";
            // 
            // _ScriptsPanel1
            // 
            _ScriptsPanel1.Controls.Add(__LoadScriptButton);
            _ScriptsPanel1.Controls.Add(__LoadAndRunButton);
            _ScriptsPanel1.Controls.Add(__RunScriptButton);
            _ScriptsPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            _ScriptsPanel1.Location = new System.Drawing.Point(607, 1);
            _ScriptsPanel1.Name = "_ScriptsPanel1";
            _ScriptsPanel1.Size = new System.Drawing.Size(174, 466);
            _ScriptsPanel1.TabIndex = 42;
            // 
            // ScriptsView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_ScriptsTableLayoutPanel);
            Controls.Add(_ScriptsPanel1);
            Name = "ScriptsView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(782, 468);
            _ScriptsTableLayoutPanel.ResumeLayout(false);
            _ScriptsPanel4.ResumeLayout(false);
            _ScriptsPanel3.ResumeLayout(false);
            _ScriptsPanel2.ResumeLayout(false);
            _ScriptsPanel2.PerformLayout();
            _ScriptsPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _ScriptsTableLayoutPanel;
        private System.Windows.Forms.Panel _ScriptsPanel4;
        private System.Windows.Forms.Label _UserScriptsListLabel;
        private System.Windows.Forms.ListBox _UserScriptsList;
        private System.Windows.Forms.Button __RefreshUserScriptsListButton;

        private System.Windows.Forms.Button _RefreshUserScriptsListButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RefreshUserScriptsListButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RefreshUserScriptsListButton != null)
                {
                    __RefreshUserScriptsListButton.Enter -= Button_Enter;
                    __RefreshUserScriptsListButton.Leave -= Button_Leave;
                    __RefreshUserScriptsListButton.Click -= RefreshUserScriptsListButton_Click;
                }

                __RefreshUserScriptsListButton = value;
                if (__RefreshUserScriptsListButton != null)
                {
                    __RefreshUserScriptsListButton.Enter += Button_Enter;
                    __RefreshUserScriptsListButton.Leave += Button_Leave;
                    __RefreshUserScriptsListButton.Click += RefreshUserScriptsListButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __RemoveScriptButton;

        private System.Windows.Forms.Button _RemoveScriptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RemoveScriptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RemoveScriptButton != null)
                {
                    __RemoveScriptButton.Enter -= Button_Enter;
                    __RemoveScriptButton.Leave -= Button_Leave;
                    __RemoveScriptButton.Click -= RemoveScriptButton_Click;
                }

                __RemoveScriptButton = value;
                if (__RemoveScriptButton != null)
                {
                    __RemoveScriptButton.Enter += Button_Enter;
                    __RemoveScriptButton.Leave += Button_Leave;
                    __RemoveScriptButton.Click += RemoveScriptButton_Click;
                }
            }
        }

        private System.Windows.Forms.Panel _ScriptsPanel3;
        private Core.Controls.FileSelector __TspScriptSelector;

        private Core.Controls.FileSelector _TspScriptSelector
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TspScriptSelector;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TspScriptSelector != null)
                {
                    __TspScriptSelector.SelectedChanged -= TspScriptSelector_SelectedChanged;
                }

                __TspScriptSelector = value;
                if (__TspScriptSelector != null)
                {
                    __TspScriptSelector.SelectedChanged += TspScriptSelector_SelectedChanged;
                }
            }
        }

        private System.Windows.Forms.Label _TspScriptSelectorLabel;
        private System.Windows.Forms.Panel _ScriptsPanel2;
        private System.Windows.Forms.CheckBox _RetainCodeOutlineToggle;
        private System.Windows.Forms.Label _ScriptNameTextBoxLabel;
        private System.Windows.Forms.TextBox _ScriptNameTextBox;
        private System.Windows.Forms.Panel _ScriptsPanel1;
        private System.Windows.Forms.Button __LoadScriptButton;

        private System.Windows.Forms.Button _LoadScriptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadScriptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadScriptButton != null)
                {
                    __LoadScriptButton.Click -= LoadScriptButton_Click;
                }

                __LoadScriptButton = value;
                if (__LoadScriptButton != null)
                {
                    __LoadScriptButton.Click += LoadScriptButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __LoadAndRunButton;

        private System.Windows.Forms.Button _LoadAndRunButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadAndRunButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadAndRunButton != null)
                {
                    __LoadAndRunButton.Click -= LoadAndRunButton_Click;
                }

                __LoadAndRunButton = value;
                if (__LoadAndRunButton != null)
                {
                    __LoadAndRunButton.Click += LoadAndRunButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __RunScriptButton;

        private System.Windows.Forms.Button _RunScriptButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RunScriptButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RunScriptButton != null)
                {
                    __RunScriptButton.Click -= RunScriptButton_Click;
                }

                __RunScriptButton = value;
                if (__RunScriptButton != null)
                {
                    __RunScriptButton.Click += RunScriptButton_Click;
                }
            }
        }
    }
}