using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> A function view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class FunctionsView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public FunctionsView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__CallFunctionButton.Name = "_CallFunctionButton";
            this.__LoadFunctionButton.Name = "_LoadFunctionButton";
        }

        /// <summary> Creates a new <see cref="FunctionsView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="FunctionsView"/>. </returns>
        public static FunctionsView Create()
        {
            FunctionsView view = null;
            try
            {
                view = new FunctionsView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TspDevice Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Event handler. Called by _callFunctionButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CallFunctionButton_Click( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {

                // Turn on the form hourglass
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                activity = "calling function";
                string functionName = this._FunctionNameTextBox.Text;
                string functionArgs = this._FunctionArgsTextBox.Text;
                activity = $"calling function '{functionName}({functionArgs})'";
                if ( !string.IsNullOrWhiteSpace( functionName ) )
                {
                    Syntax.Lua.CallFunction( this.Device.Session, functionName, functionArgs );
                    this.Device.StatusSubsystem.TraceVisaOperation( $"{activity};. " );
                }
            }
            catch ( Pith.NativeException ex )
            {
                this.Device.StatusSubsystem.TraceVisaOperation( $"{activity};. {ex} " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 300d ) );
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by _loadFunctionButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadFunctionButton_Click( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {
                activity = "loading function code";

                // Turn on the form hourglass
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                string functionCode;
                functionCode = this._FunctionCodeTextBox.Text.Replace( Constants.vbCrLf, Strings.Space( 1 ) );
                if ( this.Device.IsSessionOpen && !string.IsNullOrWhiteSpace( functionCode ) )
                {
                    _ = this.PublishInfo( $"{activity};. " );
                    _ = this.Device.Session.WriteLine( functionCode );
                    this.Device.StatusSubsystem.TraceVisaOperation( $"{activity};. " );
                }
            }
            catch ( Pith.NativeException ex )
            {
                this.Device.StatusSubsystem.TraceVisaOperation( ex, $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {

                // Turn off the form hourglass
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 300d ) );
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
