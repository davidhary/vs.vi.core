﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{
    [DesignerGenerated()]
    public partial class InstrumentView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            __GroupTriggerButton = new System.Windows.Forms.Button();
            __GroupTriggerButton.Enter += new EventHandler(Button_Enter);
            __GroupTriggerButton.Leave += new EventHandler(Button_Leave);
            __GroupTriggerButton.Click += new EventHandler(GroupTriggerButton_Click);
            __GroupTriggerButton.Click += new EventHandler(GroupTriggerButton_Click);
            __GroupTriggerButton.Click += new EventHandler(GroupTriggerButton_Click);
            __GroupTriggerButton.Click += new EventHandler(GroupTriggerButton_Click);
            __DeviceClearButton = new System.Windows.Forms.Button();
            __DeviceClearButton.Enter += new EventHandler(Button_Enter);
            __DeviceClearButton.Leave += new EventHandler(Button_Leave);
            __DeviceClearButton.Click += new EventHandler(DeviceClearButton_Click);
            __ShowPromptsCheckBox = new System.Windows.Forms.CheckBox();
            __ShowPromptsCheckBox.CheckStateChanged += new EventHandler(ShowPromptsCheckBox_CheckStateChanged);
            __ShowErrorsCheckBox = new System.Windows.Forms.CheckBox();
            __ShowErrorsCheckBox.CheckStateChanged += new EventHandler(ShowErrorsCheckBox_CheckStateChanged);
            __AbortButton = new System.Windows.Forms.Button();
            __AbortButton.Click += new EventHandler(AbortButton_Click);
            __AbortButton.Enter += new EventHandler(Button_Enter);
            __AbortButton.Leave += new EventHandler(Button_Leave);
            __ResetLocalNodeButton = new System.Windows.Forms.Button();
            __ResetLocalNodeButton.Enter += new EventHandler(Button_Enter);
            __ResetLocalNodeButton.Enter += new EventHandler(Button_Enter);
            __ResetLocalNodeButton.Enter += new EventHandler(Button_Enter);
            __ResetLocalNodeButton.Enter += new EventHandler(Button_Enter);
            __ResetLocalNodeButton.Leave += new EventHandler(Button_Leave);
            __ResetLocalNodeButton.Leave += new EventHandler(Button_Leave);
            __ResetLocalNodeButton.Leave += new EventHandler(Button_Leave);
            __ResetLocalNodeButton.Leave += new EventHandler(Button_Leave);
            __ResetLocalNodeButton.Click += new EventHandler(ResetLocalNodeButton_Click);
            _InstrumentTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _InstrumentPanel3 = new System.Windows.Forms.Panel();
            _InstrumentPanel2 = new System.Windows.Forms.Panel();
            _InstrumentPanel1 = new System.Windows.Forms.Panel();
            _InstrumentTableLayoutPanel.SuspendLayout();
            _InstrumentPanel3.SuspendLayout();
            _InstrumentPanel2.SuspendLayout();
            _InstrumentPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // _GroupTriggerButton
            // 
            __GroupTriggerButton.BackColor = System.Drawing.SystemColors.Control;
            __GroupTriggerButton.Cursor = System.Windows.Forms.Cursors.Default;
            __GroupTriggerButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __GroupTriggerButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __GroupTriggerButton.Location = new System.Drawing.Point(9, 64);
            __GroupTriggerButton.Name = "__GroupTriggerButton";
            __GroupTriggerButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __GroupTriggerButton.Size = new System.Drawing.Size(145, 33);
            __GroupTriggerButton.TabIndex = 1;
            __GroupTriggerButton.Text = "ASSERT &TRIGGER";
            __GroupTriggerButton.UseVisualStyleBackColor = true;
            // 
            // _DeviceClearButton
            // 
            __DeviceClearButton.BackColor = System.Drawing.SystemColors.Control;
            __DeviceClearButton.Cursor = System.Windows.Forms.Cursors.Default;
            __DeviceClearButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __DeviceClearButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __DeviceClearButton.Location = new System.Drawing.Point(9, 16);
            __DeviceClearButton.Name = "__DeviceClearButton";
            __DeviceClearButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __DeviceClearButton.Size = new System.Drawing.Size(145, 33);
            __DeviceClearButton.TabIndex = 0;
            __DeviceClearButton.Text = "CLEAR &DEVICE";
            __DeviceClearButton.UseVisualStyleBackColor = true;
            // 
            // _ShowPromptsCheckBox
            // 
            __ShowPromptsCheckBox.BackColor = System.Drawing.Color.Transparent;
            __ShowPromptsCheckBox.Cursor = System.Windows.Forms.Cursors.Default;
            __ShowPromptsCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ShowPromptsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            __ShowPromptsCheckBox.Location = new System.Drawing.Point(7, 22);
            __ShowPromptsCheckBox.Name = "__ShowPromptsCheckBox";
            __ShowPromptsCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __ShowPromptsCheckBox.Size = new System.Drawing.Size(185, 21);
            __ShowPromptsCheckBox.TabIndex = 0;
            __ShowPromptsCheckBox.Text = "ENABLE &PROMPTS";
            __ShowPromptsCheckBox.ThreeState = true;
            __ShowPromptsCheckBox.UseVisualStyleBackColor = false;
            // 
            // _ShowErrorsCheckBox
            // 
            __ShowErrorsCheckBox.BackColor = System.Drawing.Color.Transparent;
            __ShowErrorsCheckBox.Cursor = System.Windows.Forms.Cursors.Default;
            __ShowErrorsCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ShowErrorsCheckBox.ForeColor = System.Drawing.SystemColors.ControlText;
            __ShowErrorsCheckBox.Location = new System.Drawing.Point(7, 70);
            __ShowErrorsCheckBox.Name = "__ShowErrorsCheckBox";
            __ShowErrorsCheckBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __ShowErrorsCheckBox.Size = new System.Drawing.Size(185, 21);
            __ShowErrorsCheckBox.TabIndex = 1;
            __ShowErrorsCheckBox.Text = "ENABLE &ERROR OUTPUT";
            __ShowErrorsCheckBox.ThreeState = true;
            __ShowErrorsCheckBox.UseVisualStyleBackColor = true;
            // 
            // _AbortButton
            // 
            __AbortButton.BackColor = System.Drawing.SystemColors.Control;
            __AbortButton.Cursor = System.Windows.Forms.Cursors.Default;
            __AbortButton.Enabled = false;
            __AbortButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __AbortButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __AbortButton.Location = new System.Drawing.Point(8, 16);
            __AbortButton.Name = "__AbortButton";
            __AbortButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __AbortButton.Size = new System.Drawing.Size(173, 33);
            __AbortButton.TabIndex = 0;
            __AbortButton.Text = "&ABORT ACTIVE SCRIPT";
            __AbortButton.UseVisualStyleBackColor = true;
            // 
            // _ResetLocalNodeButton
            // 
            __ResetLocalNodeButton.BackColor = System.Drawing.SystemColors.Control;
            __ResetLocalNodeButton.Cursor = System.Windows.Forms.Cursors.Default;
            __ResetLocalNodeButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ResetLocalNodeButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __ResetLocalNodeButton.Location = new System.Drawing.Point(8, 64);
            __ResetLocalNodeButton.Name = "__ResetLocalNodeButton";
            __ResetLocalNodeButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __ResetLocalNodeButton.Size = new System.Drawing.Size(173, 33);
            __ResetLocalNodeButton.TabIndex = 1;
            __ResetLocalNodeButton.Text = "&RESET LOCAL NODE";
            __ResetLocalNodeButton.UseVisualStyleBackColor = true;
            // 
            // _InstrumentTableLayoutPanel
            // 
            _InstrumentTableLayoutPanel.ColumnCount = 7;
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _InstrumentTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _InstrumentTableLayoutPanel.Controls.Add(_InstrumentPanel3, 1, 0);
            _InstrumentTableLayoutPanel.Controls.Add(_InstrumentPanel2, 5, 0);
            _InstrumentTableLayoutPanel.Controls.Add(_InstrumentPanel1, 3, 0);
            _InstrumentTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _InstrumentTableLayoutPanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _InstrumentTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _InstrumentTableLayoutPanel.Name = "_InstrumentTableLayoutPanel";
            _InstrumentTableLayoutPanel.RowCount = 1;
            _InstrumentTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _InstrumentTableLayoutPanel.Size = new System.Drawing.Size(696, 123);
            _InstrumentTableLayoutPanel.TabIndex = 54;
            // 
            // _InstrumentPanel3
            // 
            _InstrumentPanel3.Controls.Add(__GroupTriggerButton);
            _InstrumentPanel3.Controls.Add(__DeviceClearButton);
            _InstrumentPanel3.Location = new System.Drawing.Point(37, 3);
            _InstrumentPanel3.Name = "_InstrumentPanel3";
            _InstrumentPanel3.Size = new System.Drawing.Size(160, 113);
            _InstrumentPanel3.TabIndex = 54;
            // 
            // _InstrumentPanel2
            // 
            _InstrumentPanel2.Controls.Add(__ShowPromptsCheckBox);
            _InstrumentPanel2.Controls.Add(__ShowErrorsCheckBox);
            _InstrumentPanel2.Location = new System.Drawing.Point(465, 3);
            _InstrumentPanel2.Name = "_InstrumentPanel2";
            _InstrumentPanel2.Size = new System.Drawing.Size(194, 113);
            _InstrumentPanel2.TabIndex = 52;
            // 
            // _InstrumentPanel1
            // 
            _InstrumentPanel1.Controls.Add(__AbortButton);
            _InstrumentPanel1.Controls.Add(__ResetLocalNodeButton);
            _InstrumentPanel1.Location = new System.Drawing.Point(237, 3);
            _InstrumentPanel1.Name = "_InstrumentPanel1";
            _InstrumentPanel1.Size = new System.Drawing.Size(188, 113);
            _InstrumentPanel1.TabIndex = 51;
            // 
            // InstrumentView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_InstrumentTableLayoutPanel);
            Name = "InstrumentView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(696, 455);
            _InstrumentTableLayoutPanel.ResumeLayout(false);
            _InstrumentPanel3.ResumeLayout(false);
            _InstrumentPanel2.ResumeLayout(false);
            _InstrumentPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _InstrumentTableLayoutPanel;
        private System.Windows.Forms.Panel _InstrumentPanel3;
        private System.Windows.Forms.Button __GroupTriggerButton;

        private System.Windows.Forms.Button _GroupTriggerButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __GroupTriggerButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__GroupTriggerButton != null)
                {
                    __GroupTriggerButton.Enter -= Button_Enter;
                    __GroupTriggerButton.Leave -= Button_Leave;
                    __GroupTriggerButton.Click -= GroupTriggerButton_Click;
                    __GroupTriggerButton.Click -= GroupTriggerButton_Click;
                }

                __GroupTriggerButton = value;
                if (__GroupTriggerButton != null)
                {
                    __GroupTriggerButton.Enter += Button_Enter;
                    __GroupTriggerButton.Leave += Button_Leave;
                    __GroupTriggerButton.Click += GroupTriggerButton_Click;
                    __GroupTriggerButton.Click += GroupTriggerButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __DeviceClearButton;

        private System.Windows.Forms.Button _DeviceClearButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DeviceClearButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DeviceClearButton != null)
                {
                    __DeviceClearButton.Enter -= Button_Enter;
                    __DeviceClearButton.Leave -= Button_Leave;
                    __DeviceClearButton.Click -= DeviceClearButton_Click;
                }

                __DeviceClearButton = value;
                if (__DeviceClearButton != null)
                {
                    __DeviceClearButton.Enter += Button_Enter;
                    __DeviceClearButton.Leave += Button_Leave;
                    __DeviceClearButton.Click += DeviceClearButton_Click;
                }
            }
        }

        private System.Windows.Forms.Panel _InstrumentPanel2;
        private System.Windows.Forms.CheckBox __ShowPromptsCheckBox;

        private System.Windows.Forms.CheckBox _ShowPromptsCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ShowPromptsCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ShowPromptsCheckBox != null)
                {
                    __ShowPromptsCheckBox.CheckStateChanged -= ShowPromptsCheckBox_CheckStateChanged;
                }

                __ShowPromptsCheckBox = value;
                if (__ShowPromptsCheckBox != null)
                {
                    __ShowPromptsCheckBox.CheckStateChanged += ShowPromptsCheckBox_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox __ShowErrorsCheckBox;

        private System.Windows.Forms.CheckBox _ShowErrorsCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ShowErrorsCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ShowErrorsCheckBox != null)
                {
                    __ShowErrorsCheckBox.CheckStateChanged -= ShowErrorsCheckBox_CheckStateChanged;
                }

                __ShowErrorsCheckBox = value;
                if (__ShowErrorsCheckBox != null)
                {
                    __ShowErrorsCheckBox.CheckStateChanged += ShowErrorsCheckBox_CheckStateChanged;
                }
            }
        }

        private System.Windows.Forms.Panel _InstrumentPanel1;
        private System.Windows.Forms.Button __AbortButton;

        private System.Windows.Forms.Button _AbortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortButton != null)
                {
                    __AbortButton.Click -= AbortButton_Click;
                    __AbortButton.Enter -= Button_Enter;
                    __AbortButton.Leave -= Button_Leave;
                }

                __AbortButton = value;
                if (__AbortButton != null)
                {
                    __AbortButton.Click += AbortButton_Click;
                    __AbortButton.Enter += Button_Enter;
                    __AbortButton.Leave += Button_Leave;
                }
            }
        }

        private System.Windows.Forms.Button __ResetLocalNodeButton;

        private System.Windows.Forms.Button _ResetLocalNodeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResetLocalNodeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResetLocalNodeButton != null)
                {
                    __ResetLocalNodeButton.Enter -= Button_Enter;
                    __ResetLocalNodeButton.Enter -= Button_Enter;
                    __ResetLocalNodeButton.Leave -= Button_Leave;
                    __ResetLocalNodeButton.Leave -= Button_Leave;
                    __ResetLocalNodeButton.Click -= ResetLocalNodeButton_Click;
                }

                __ResetLocalNodeButton = value;
                if (__ResetLocalNodeButton != null)
                {
                    __ResetLocalNodeButton.Enter += Button_Enter;
                    __ResetLocalNodeButton.Enter += Button_Enter;
                    __ResetLocalNodeButton.Leave += Button_Leave;
                    __ResetLocalNodeButton.Leave += Button_Leave;
                    __ResetLocalNodeButton.Click += ResetLocalNodeButton_Click;
                }
            }
        }
    }
}