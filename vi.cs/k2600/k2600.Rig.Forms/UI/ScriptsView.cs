using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> A script view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ScriptsView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ScriptsView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__RemoveScriptButton.Name = "_RemoveScriptButton";
            this.__TspScriptSelector.Name = "_TspScriptSelector";
            this.__LoadScriptButton.Name = "_LoadScriptButton";
            this.__LoadAndRunButton.Name = "_LoadAndRunButton";
            this.__RunScriptButton.Name = "_RunScriptButton";
            this.__RefreshUserScriptsListButton.Name = "_RefreshUserScriptsListButton";
        }

        /// <summary> Creates a new <see cref="ScriptsView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ScriptsView"/>. </returns>
        public static ScriptsView Create()
        {
            ScriptsView view = null;
            try
            {
                view = new ScriptsView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TspDevice Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.AssignScriptManager( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SCRIPT MANAGER "

        /// <summary> Manager for script. </summary>

        /// <summary> Gets the ScriptManager. </summary>
        /// <value> The ScriptManager. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ScriptManager ScriptManager { get; private set; }

        /// <summary> Assigns the ScriptManager and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void AssignScriptManager( TspDevice value )
        {
            if ( this.ScriptManager is object )
            {
                this.ScriptManager = null;
            }

            if ( value is object )
            {
                this.ScriptManager = new ( this.Device );
                // set the default file path for scripts.
                this.ScriptManager.FilePath = System.IO.Path.GetFullPath( System.IO.Path.Combine( My.MyProject.Application.Info.DirectoryPath, "Scripts" ) );
                this.ListUserScripts();
            }
        }

        #endregion

        #region " METHODS "

        /// <summary> List the user scripts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ListUserScripts()
        {
            string activity = string.Empty;
            try
            {
                activity = "listing scripts";
                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;
                _ = this.PublishInfo( "Flushing the read buffer...;. " );
                this.Device.Session.DiscardUnreadData();
                _ = this.PublishInfo( "Fetching script names...;. " );
                int scriptCount = this.ScriptManager.FetchUserScriptNames();
                if ( scriptCount > 0 )
                {
                    _ = this.PublishInfo( "Listing script names...;. " );
                    this._UserScriptsList.DataSource = null;
                    this._UserScriptsList.Items.Clear();
                    this._UserScriptsList.DataSource = this.ScriptManager.UserScriptNames().ToList();
                    if ( this._UserScriptsList.Items.Count > 0 )
                    {
                        this._UserScriptsList.SelectedIndex = 0;
                    }
                }
                else
                {
                    _ = this.PublishInfo( "No Scripts;. " );
                    this._UserScriptsList.DataSource = null;
                    this._UserScriptsList.Items.Clear();
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Toggle the font on the control to highlight selection of this control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void Button_Enter( object eventSender, EventArgs eventArgs )
        {
            Control thisButton = ( Control ) eventSender;
            thisButton.Font = new System.Drawing.Font( thisButton.Font, System.Drawing.FontStyle.Bold );
        }

        /// <summary> Toggle the font on the control to highlight leaving this control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void Button_Leave( object eventSender, EventArgs eventArgs )
        {
            Control thisButton = ( Control ) eventSender;
            thisButton.Font = new System.Drawing.Font( thisButton.Font, System.Drawing.FontStyle.Regular );
        }

        /// <summary> Loads the script. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="scriptName"> Name of the script. </param>
        /// <param name="filePath">   The file path. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool LoadScript( string scriptName, string filePath )
        {
            string activity = string.Empty;
            bool result = false;
            try
            {
                activity = "loading or running script";
                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;
                if ( this.Device.IsDeviceOpen )
                {
                    activity = $"loading script {scriptName} from {filePath}";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.ScriptManager.ScriptNameSetter( scriptName );
                    this.ScriptManager.FilePath = filePath;
                    this.ScriptManager.LoadScriptFile( true, true, this._RetainCodeOutlineToggle.Checked );
                    activity = $"done loading script {scriptName} from {filePath}";
                    _ = this.PublishInfo( $"{activity};. " );
                    activity = $"listing user scripts";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.ListUserScripts();
                    result = true;
                }
            }
            catch ( Exception ex )
            {
                result = false;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }

            return result;
        }

        /// <summary> Executes the script operation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="scriptName"> Name of the script. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool RunScript( string scriptName )
        {
            string activity = string.Empty;
            bool result = false;
            try
            {
                if ( this.Device.IsDeviceOpen )
                {
                    activity = $"running script {scriptName}";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.ScriptManager.RunScript( TimeSpan.FromMilliseconds( 3000d ) );
                    activity = $"script {scriptName} run okay";
                    _ = this.PublishInfo( $"{activity};. " );
                    result = true;
                }
            }
            catch ( Exception ex )
            {
                result = false;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }

            return result;
        }

        /// <summary> Removes the script described by scriptName. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="scriptName"> Name of the script. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool RemoveScript( string scriptName )
        {
            string activity = string.Empty;
            bool result = false;
            try
            {
                if ( this.Device.IsDeviceOpen )
                {
                    activity = $"removing script {scriptName}";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.ScriptManager.RemoveScript( scriptName );
                    activity = $"script {scriptName} removed";
                    _ = this.PublishInfo( $"{activity};. " );
                    activity = $"listing user scripts";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.ListUserScripts();
                    result = true;
                }
            }
            catch ( Exception ex )
            {
                result = false;
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }

            return result;
        }

        /// <summary> Loads the and run script. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="scriptName"> Name of the script. </param>
        /// <param name="filePath">   The file path. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool LoadAndRunScript( string scriptName, string filePath )
        {
            return this.LoadScript( scriptName, filePath ) && this.RunScript( scriptName );
        }

        /// <summary> Event handler. Called by _loadAndRunButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void LoadAndRunButton_Click( object eventSender, EventArgs eventArgs )
        {
            if ( !Script.ScriptEntityBase.IsValidScriptFileName( this._ScriptNameTextBox.Text ) )
                return;
            _ = this.LoadAndRunScript( this._ScriptNameTextBox.Text, this._TspScriptSelector.FilePath );
        }

        /// <summary> Event handler. Called by _loadScriptButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void LoadScriptButton_Click( object eventSender, EventArgs eventArgs )
        {
            if ( !Script.ScriptEntityBase.IsValidScriptFileName( this._ScriptNameTextBox.Text ) )
                return;
            _ = this.LoadScript( this._ScriptNameTextBox.Text, this._TspScriptSelector.FilePath );
        }

        /// <summary>
        /// Event handler. Called by _refreshUserScriptsListButton for click events. Updates the list of
        /// user scripts.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Form"/> </param>
        /// <param name="e">      Event information. </param>
        private void RefreshUserScriptsListButton_Click( object sender, EventArgs e )
        {
            this.ListUserScripts();
        }

        /// <summary> Event handler. Called by _removeScriptButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void RemoveScriptButton_Click( object eventSender, EventArgs eventArgs )
        {
            if ( !Script.ScriptEntityBase.IsValidScriptFileName( this._ScriptNameTextBox.Text ) )
                return;
            _ = this.RemoveScript( this._ScriptNameTextBox.Text );
        }

        /// <summary> Event handler. Called by _runScriptButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void RunScriptButton_Click( object eventSender, EventArgs eventArgs )
        {
            if ( !Script.ScriptEntityBase.IsValidScriptFileName( this._ScriptNameTextBox.Text ) )
                return;
            _ = this.RunScript( this._ScriptNameTextBox.Text );
        }

        /// <summary> Event handler. Called by _tspScriptSelector for selected changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TspScriptSelector_SelectedChanged( object sender, EventArgs e )
        {
            if ( !string.IsNullOrWhiteSpace( this._TspScriptSelector.FileTitle ) )
            {
                this._ScriptNameTextBox.Text = this._TspScriptSelector.FileTitle.Replace( ".", "_" );
                _ = this.PublishInfo( "Selected Script;. '{0}'.", this._ScriptNameTextBox.Text );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
