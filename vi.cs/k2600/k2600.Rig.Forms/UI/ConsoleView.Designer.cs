﻿using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ConsoleView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ExecutionTimeTextBox = new System.Windows.Forms.TextBox();
            _OutputTextBox = new System.Windows.Forms.TextBox();
            _ConsoleTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _InputPanel = new System.Windows.Forms.Panel();
            __InputTextBox = new System.Windows.Forms.TextBox();
            __InputTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(InputTextBox_KeyPress);
            _TimingTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _QueryCommandTextBoxLabel = new System.Windows.Forms.Label();
            _ExecutionTimeTextBoxLabel = new System.Windows.Forms.Label();
            _OutputPanel = new System.Windows.Forms.Panel();
            _ReplyTextBoxLabel = new System.Windows.Forms.Label();
            _ConsoleTableLayoutPanel.SuspendLayout();
            _InputPanel.SuspendLayout();
            _TimingTableLayoutPanel.SuspendLayout();
            _OutputPanel.SuspendLayout();
            SuspendLayout();
            // 
            // _ExecutionTimeTextBox
            // 
            _ExecutionTimeTextBox.AcceptsReturn = true;
            _ExecutionTimeTextBox.BackColor = System.Drawing.SystemColors.Window;
            _ExecutionTimeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _ExecutionTimeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _ExecutionTimeTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _ExecutionTimeTextBox.Location = new System.Drawing.Point(307, 3);
            _ExecutionTimeTextBox.MaxLength = 0;
            _ExecutionTimeTextBox.Name = "_ExecutionTimeTextBox";
            _ExecutionTimeTextBox.ReadOnly = true;
            _ExecutionTimeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ExecutionTimeTextBox.Size = new System.Drawing.Size(74, 25);
            _ExecutionTimeTextBox.TabIndex = 37;
            _ExecutionTimeTextBox.Text = "0.000";
            // 
            // _OutputTextBox
            // 
            _OutputTextBox.AcceptsReturn = true;
            _OutputTextBox.BackColor = System.Drawing.SystemColors.ControlLightLight;
            _OutputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _OutputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _OutputTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _OutputTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _OutputTextBox.Location = new System.Drawing.Point(0, 32);
            _OutputTextBox.MaxLength = 0;
            _OutputTextBox.Multiline = true;
            _OutputTextBox.Name = "_OutputTextBox";
            _OutputTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _OutputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _OutputTextBox.Size = new System.Drawing.Size(384, 428);
            _OutputTextBox.TabIndex = 1;
            _OutputTextBox.WordWrap = false;
            // 
            // _ConsoleTableLayoutPanel
            // 
            _ConsoleTableLayoutPanel.ColumnCount = 2;
            _ConsoleTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConsoleTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ConsoleTableLayoutPanel.Controls.Add(_InputPanel, 0, 0);
            _ConsoleTableLayoutPanel.Controls.Add(_OutputPanel, 1, 0);
            _ConsoleTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ConsoleTableLayoutPanel.Location = new System.Drawing.Point(1, 1);
            _ConsoleTableLayoutPanel.Name = "_ConsoleTableLayoutPanel";
            _ConsoleTableLayoutPanel.RowCount = 1;
            _ConsoleTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _ConsoleTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 470.0f));
            _ConsoleTableLayoutPanel.Size = new System.Drawing.Size(780, 466);
            _ConsoleTableLayoutPanel.TabIndex = 9;
            // 
            // _InputPanel
            // 
            _InputPanel.Controls.Add(__InputTextBox);
            _InputPanel.Controls.Add(_TimingTableLayoutPanel);
            _InputPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _InputPanel.Location = new System.Drawing.Point(3, 3);
            _InputPanel.Name = "_InputPanel";
            _InputPanel.Size = new System.Drawing.Size(384, 460);
            _InputPanel.TabIndex = 41;
            // 
            // _InputTextBox
            // 
            __InputTextBox.AcceptsReturn = true;
            __InputTextBox.BackColor = System.Drawing.SystemColors.Window;
            __InputTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            __InputTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            __InputTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __InputTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            __InputTextBox.Location = new System.Drawing.Point(0, 33);
            __InputTextBox.MaxLength = 0;
            __InputTextBox.Multiline = true;
            __InputTextBox.Name = "__InputTextBox";
            __InputTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __InputTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            __InputTextBox.Size = new System.Drawing.Size(384, 427);
            __InputTextBox.TabIndex = 0;
            __InputTextBox.Text = "print(_VERSION)" + '\r' + '\n';
            __InputTextBox.WordWrap = false;
            // 
            // _TimingTableLayoutPanel
            // 
            _TimingTableLayoutPanel.ColumnCount = 3;
            _TimingTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TimingTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TimingTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TimingTableLayoutPanel.Controls.Add(_ExecutionTimeTextBox, 2, 0);
            _TimingTableLayoutPanel.Controls.Add(_QueryCommandTextBoxLabel, 0, 0);
            _TimingTableLayoutPanel.Controls.Add(_ExecutionTimeTextBoxLabel, 1, 0);
            _TimingTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _TimingTableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            _TimingTableLayoutPanel.Name = "_TimingTableLayoutPanel";
            _TimingTableLayoutPanel.RowCount = 1;
            _TimingTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TimingTableLayoutPanel.Size = new System.Drawing.Size(384, 33);
            _TimingTableLayoutPanel.TabIndex = 42;
            // 
            // _QueryCommandTextBoxLabel
            // 
            _QueryCommandTextBoxLabel.AutoSize = true;
            _QueryCommandTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _QueryCommandTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _QueryCommandTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _QueryCommandTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _QueryCommandTextBoxLabel.Location = new System.Drawing.Point(3, 0);
            _QueryCommandTextBoxLabel.Name = "_QueryCommandTextBoxLabel";
            _QueryCommandTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _QueryCommandTextBoxLabel.Size = new System.Drawing.Size(146, 33);
            _QueryCommandTextBoxLabel.TabIndex = 0;
            _QueryCommandTextBoxLabel.Text = "Instrument Input: ";
            _QueryCommandTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _ExecutionTimeTextBoxLabel
            // 
            _ExecutionTimeTextBoxLabel.AutoSize = true;
            _ExecutionTimeTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _ExecutionTimeTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ExecutionTimeTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ExecutionTimeTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ExecutionTimeTextBoxLabel.Location = new System.Drawing.Point(155, 0);
            _ExecutionTimeTextBoxLabel.Name = "_ExecutionTimeTextBoxLabel";
            _ExecutionTimeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ExecutionTimeTextBoxLabel.Size = new System.Drawing.Size(146, 33);
            _ExecutionTimeTextBoxLabel.TabIndex = 38;
            _ExecutionTimeTextBoxLabel.Text = "Execution Time [ms]:";
            _ExecutionTimeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _OutputPanel
            // 
            _OutputPanel.Controls.Add(_OutputTextBox);
            _OutputPanel.Controls.Add(_ReplyTextBoxLabel);
            _OutputPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _OutputPanel.Location = new System.Drawing.Point(393, 3);
            _OutputPanel.Name = "_OutputPanel";
            _OutputPanel.Size = new System.Drawing.Size(384, 460);
            _OutputPanel.TabIndex = 40;
            // 
            // _ReplyTextBoxLabel
            // 
            _ReplyTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _ReplyTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ReplyTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Top;
            _ReplyTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _ReplyTextBoxLabel.Location = new System.Drawing.Point(0, 0);
            _ReplyTextBoxLabel.Name = "_ReplyTextBoxLabel";
            _ReplyTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ReplyTextBoxLabel.Size = new System.Drawing.Size(384, 32);
            _ReplyTextBoxLabel.TabIndex = 0;
            _ReplyTextBoxLabel.Text = "Instrument Output: ";
            _ReplyTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // ConsoleView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_ConsoleTableLayoutPanel);
            Name = "ConsoleView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(782, 468);
            _ConsoleTableLayoutPanel.ResumeLayout(false);
            _InputPanel.ResumeLayout(false);
            _InputPanel.PerformLayout();
            _TimingTableLayoutPanel.ResumeLayout(false);
            _TimingTableLayoutPanel.PerformLayout();
            _OutputPanel.ResumeLayout(false);
            _OutputPanel.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _ConsoleTableLayoutPanel;
        private System.Windows.Forms.Panel _InputPanel;
        private System.Windows.Forms.TextBox __InputTextBox;

        private System.Windows.Forms.TextBox _InputTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InputTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InputTextBox != null)
                {
                    __InputTextBox.KeyPress -= InputTextBox_KeyPress;
                }

                __InputTextBox = value;
                if (__InputTextBox != null)
                {
                    __InputTextBox.KeyPress += InputTextBox_KeyPress;
                }
            }
        }

        private System.Windows.Forms.TableLayoutPanel _TimingTableLayoutPanel;
        private System.Windows.Forms.TextBox _ExecutionTimeTextBox;
        private System.Windows.Forms.Label _QueryCommandTextBoxLabel;
        private System.Windows.Forms.Label _ExecutionTimeTextBoxLabel;
        private System.Windows.Forms.Panel _OutputPanel;
        private System.Windows.Forms.TextBox _OutputTextBox;
        private System.Windows.Forms.Label _ReplyTextBoxLabel;
    }
}