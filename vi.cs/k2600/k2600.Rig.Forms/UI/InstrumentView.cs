using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> A instrument view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class InstrumentView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public InstrumentView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__GroupTriggerButton.Name = "_GroupTriggerButton";
            this.__DeviceClearButton.Name = "_DeviceClearButton";
            this.__ShowPromptsCheckBox.Name = "_ShowPromptsCheckBox";
            this.__ShowErrorsCheckBox.Name = "_ShowErrorsCheckBox";
            this.__AbortButton.Name = "_AbortButton";
            this.__ResetLocalNodeButton.Name = "_ResetLocalNodeButton";
        }

        /// <summary> Creates a new <see cref="InstrumentView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="InstrumentView"/>. </returns>
        public static InstrumentView Create()
        {
            InstrumentView view = null;
            try
            {
                view = new InstrumentView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TspDevice Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindLocalNodeSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " LOCAL NODE "

        /// <summary> Gets or sets the local node subsystem. </summary>
        /// <value> The local node subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public LocalNodeSubsystemBase LocalNodeSubsystem { get; private set; }

        /// <summary> Bind local node subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindLocalNodeSubsystem( TspDevice device )
        {
            if ( this.LocalNodeSubsystem is object )
            {
                this.BindSubsystem( false, this.LocalNodeSubsystem );
                this.LocalNodeSubsystem = null;
            }

            if ( device is object )
            {
                this.LocalNodeSubsystem = device.InteractiveSubsystem;
                this.BindSubsystem( true, this.LocalNodeSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, LocalNodeSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.LocalNodeSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( LocalNodeSubsystemBase.ExecutionStateCaption ) );
            }
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            // If Not Me.LocalNodeSubsystem.ProcessExecutionStateEnabled Then
            // read execution state explicitly, because session events are disabled.
            // Me.LocalNodeSubsystem.ReadExecutionState()
            // End If
            else
            {
                subsystem.PropertyChanged -= this.LocalNodeSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the Tsp.LocalNode subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( LocalNodeSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( LocalNodeSubsystemBase.ExecutionStateCaption ):
                    {
                        break;
                    }
                // Me._TspStatusLabel.Text = subsystem.ExecutionStateCaption
                case nameof( LocalNodeSubsystemBase.ExecutionState ):
                    {
                        break;
                    }

                case nameof( LocalNodeSubsystemBase.ShowErrors ):
                    {
                        this._ShowErrorsCheckBox.CheckState = subsystem.ShowErrors.ToCheckState();
                        break;
                    }

                case nameof( LocalNodeSubsystemBase.ShowPrompts ):
                    {
                        this._ShowPromptsCheckBox.CheckState = subsystem.ShowPrompts.ToCheckState();
                        break;
                    }
            }
        }

        /// <summary> Tsp.LocalNode subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LocalNodeSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    activity = $"invoking {nameof( LocalNodeSubsystemBase )}.{e.PropertyName} change";
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.LocalNodeSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"handling {nameof( LocalNodeSubsystemBase )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as LocalNodeSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Terminates script execution. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortButton_Click( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {
                activity = "aborting";

                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;

                // Terminates script execution when called from a script that is being
                // executed. This command will not wait for overlapped commands to complete
                // before terminating script execution. If overlapped commands are required
                // to finish, use the wait complete function prior to calling exit.
                // to_do: add a send method that will update the output display.  Send("exit() ", True)
                _ = this.Device.Session.WriteLine( "exit() " );
                if ( !this.Device.InteractiveSubsystem.ProcessExecutionStateEnabled )
                {
                    // read execution state explicitly, because session events are disabled.
                    // update the instrument status.
                    _ = this.Device.InteractiveSubsystem.ReadExecutionState();
                }

                _ = this.PublishInfo( "Aborted;. " );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {

                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Toggle the font on the control to highlight selection of this control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void Button_Enter( object eventSender, EventArgs eventArgs )
        {
            Control thisButton = ( Control ) eventSender;
            thisButton.Font = new System.Drawing.Font( thisButton.Font, System.Drawing.FontStyle.Bold );
        }

        /// <summary> Toggle the font on the control to highlight leaving this control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        private void Button_Leave( object eventSender, EventArgs eventArgs )
        {
            Control thisButton = ( Control ) eventSender;
            thisButton.Font = new System.Drawing.Font( thisButton.Font, System.Drawing.FontStyle.Regular );
        }

        /// <summary> Event handler. Called by _deviceClearButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeviceClearButton_Click( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {

                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;
                activity = "clearing device";
                if ( this.Device.IsDeviceOpen )
                {
                    _ = this.PublishInfo( "Clearing device..;. " );
                    this.Device.Session.ClearActiveState();
                    _ = this.PublishInfo( "Device cleared.;. " );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {

                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by _groupTriggerButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void GroupTriggerButton_Click( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {

                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;
                activity = "asserting trigger";
                if ( this.Device.IsDeviceOpen )
                {
                    _ = this.PublishInfo( "Asserting trigger..;. " );
                    this.Device.Session.AssertTrigger();
                    _ = this.PublishInfo( "Trigger asserted.;. " );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {

                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by _resetLocalNodeButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ResetLocalNodeButton_Click( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {

                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;
                activity = "resetting local node";
                if ( this.Device.IsDeviceOpen )
                {
                    _ = this.PublishInfo( "Resetting local node..;. " );
                    _ = this.Device.Session.WriteLine( "localnode.reset() " );
                    _ = this.PublishInfo( "Local node was reset;. " );
                    if ( !this.Device.InteractiveSubsystem.ProcessExecutionStateEnabled )
                    {
                        // read execution state explicitly, because session events are disabled.
                        _ = this.PublishInfo( "Updating instrument state..;. " );
                        _ = this.Device.InteractiveSubsystem.ReadExecutionState();
                    }

                    _ = this.PublishInfo( "Clearing error queue..;. " );
                    _ = this.Device.Session.WriteLine( "localnode.errorqueue.clear() " );
                    _ = this.PublishInfo( "Error queue cleared;. " );

                    // update the instrument state.
                    if ( !this.Device.InteractiveSubsystem.ProcessExecutionStateEnabled )
                    {
                        // read execution state explicitly, because session events are disabled.
                        _ = this.PublishInfo( "Updating instrument state..;. " );
                        _ = this.Device.InteractiveSubsystem.ReadExecutionState();
                        _ = this.PublishInfo( "Instrument state updated;. " );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _showErrorsCheckBox for check state changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The event sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ShowErrorsCheckBox_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = "showing errors";
                // Turn on the form hourglass
                this.Cursor = Cursors.WaitCursor;
                if ( this._ShowErrorsCheckBox.Enabled && this.Device.IsDeviceOpen )
                {
                    if ( this.Device.InteractiveSubsystem.QueryShowErrors() != this._ShowErrorsCheckBox.Checked == true )
                    {
                        _ = this.PublishInfo( "Toggling showing errors..;. " );
                        _ = this.Device.InteractiveSubsystem.WriteShowErrors( this._ShowErrorsCheckBox.Checked );
                        if ( !this.Device.InteractiveSubsystem.ShowErrors.HasValue )
                        {
                            _ = this.PublishWarning( "Failed toggling showing errors--value not set;. " );
                        }
                        else if ( this.Device.InteractiveSubsystem.ShowErrors.Value != this._ShowErrorsCheckBox.Checked )
                        {
                            _ = this.PublishWarning( "Failed toggling showing errors--incorrect value;. " );
                        }

                        _ = this.PublishInfo( "Showing errors: {0};. ", Interaction.IIf( this.Device.InteractiveSubsystem.ShowErrors.Value, "ON", "OFF" ) );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _showPromptsCheckBox for check state changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The event sender. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ShowPromptsCheckBox_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = "showing prompts";
                this.Cursor = Cursors.WaitCursor;
                if ( this._ShowPromptsCheckBox.Enabled && this.Device.IsDeviceOpen )
                {
                    _ = this.PublishInfo( "Toggling showing prompts..;. " );
                    if ( this.Device.InteractiveSubsystem.QueryShowPrompts() != this._ShowPromptsCheckBox.Checked == true )
                    {
                        _ = this.Device.InteractiveSubsystem.WriteShowPrompts( this._ShowPromptsCheckBox.Checked );
                        if ( !this.Device.InteractiveSubsystem.ShowPrompts.HasValue )
                        {
                            _ = this.PublishWarning( "Failed toggling showing Prompts--value not set;. " );
                        }
                        else if ( this.Device.InteractiveSubsystem.ShowPrompts.Value != this._ShowPromptsCheckBox.Checked )
                        {
                            _ = this.PublishWarning( "Failed toggling showing Prompts--incorrect value;. " );
                        }

                        _ = this.PublishInfo( "Showing Prompts: {0};. ", Interaction.IIf( this.Device.InteractiveSubsystem.ShowPrompts.Value, "ON", "OFF" ) );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                // Turn off the form hourglass
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
