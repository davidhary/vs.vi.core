﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class FunctionsView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            __CallFunctionButton = new System.Windows.Forms.Button();
            __CallFunctionButton.Click += new EventHandler(CallFunctionButton_Click);
            __LoadFunctionButton = new System.Windows.Forms.Button();
            __LoadFunctionButton.Click += new EventHandler(LoadFunctionButton_Click);
            _FunctionsTableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            _FunctionCodeTextBox = new System.Windows.Forms.TextBox();
            _FunctionsPanel1 = new System.Windows.Forms.Panel();
            _FunctionArgsTextBox = new System.Windows.Forms.TextBox();
            _FunctionNameTextBox = new System.Windows.Forms.TextBox();
            _FunctionNameTextBoxLabel = new System.Windows.Forms.Label();
            _FunctionArgsTextBoxLabel = new System.Windows.Forms.Label();
            _FunctionsTableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            _FunctionCodeTextBoxLabel = new System.Windows.Forms.Label();
            _CallInstructionsLabel = new System.Windows.Forms.Label();
            _FunctionsTableLayoutPanel.SuspendLayout();
            _FunctionsPanel1.SuspendLayout();
            _FunctionsTableLayoutPanel5.SuspendLayout();
            SuspendLayout();
            // 
            // _CallFunctionButton
            // 
            __CallFunctionButton.BackColor = System.Drawing.SystemColors.Control;
            __CallFunctionButton.Cursor = System.Windows.Forms.Cursors.Default;
            __CallFunctionButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __CallFunctionButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __CallFunctionButton.Location = new System.Drawing.Point(415, 17);
            __CallFunctionButton.Name = "__CallFunctionButton";
            __CallFunctionButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __CallFunctionButton.Size = new System.Drawing.Size(93, 33);
            __CallFunctionButton.TabIndex = 4;
            __CallFunctionButton.Text = "&CALL";
            __CallFunctionButton.UseVisualStyleBackColor = true;
            // 
            // _LoadFunctionButton
            // 
            __LoadFunctionButton.BackColor = System.Drawing.SystemColors.Control;
            __LoadFunctionButton.Cursor = System.Windows.Forms.Cursors.Default;
            __LoadFunctionButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __LoadFunctionButton.ForeColor = System.Drawing.SystemColors.ControlText;
            __LoadFunctionButton.Location = new System.Drawing.Point(630, 3);
            __LoadFunctionButton.Name = "__LoadFunctionButton";
            __LoadFunctionButton.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __LoadFunctionButton.Size = new System.Drawing.Size(129, 27);
            __LoadFunctionButton.TabIndex = 1;
            __LoadFunctionButton.Text = "&LOAD FUNCTION";
            __LoadFunctionButton.UseVisualStyleBackColor = true;
            // 
            // _FunctionsTableLayoutPanel
            // 
            _FunctionsTableLayoutPanel.ColumnCount = 3;
            _FunctionsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 7.0f));
            _FunctionsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _FunctionsTableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5.0f));
            _FunctionsTableLayoutPanel.Controls.Add(_FunctionCodeTextBox, 1, 5);
            _FunctionsTableLayoutPanel.Controls.Add(_FunctionsPanel1, 1, 1);
            _FunctionsTableLayoutPanel.Controls.Add(_FunctionsTableLayoutPanel5, 1, 3);
            _FunctionsTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _FunctionsTableLayoutPanel.Location = new System.Drawing.Point(1, 1);
            _FunctionsTableLayoutPanel.Name = "_FunctionsTableLayoutPanel";
            _FunctionsTableLayoutPanel.RowCount = 7;
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _FunctionsTableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0f));
            _FunctionsTableLayoutPanel.Size = new System.Drawing.Size(780, 466);
            _FunctionsTableLayoutPanel.TabIndex = 1;
            // 
            // _FunctionCodeTextBox
            // 
            _FunctionCodeTextBox.AcceptsReturn = true;
            _FunctionCodeTextBox.BackColor = System.Drawing.SystemColors.Window;
            _FunctionCodeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _FunctionCodeTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _FunctionCodeTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _FunctionCodeTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _FunctionCodeTextBox.Location = new System.Drawing.Point(10, 117);
            _FunctionCodeTextBox.MaxLength = 0;
            _FunctionCodeTextBox.Multiline = true;
            _FunctionCodeTextBox.Name = "_FunctionCodeTextBox";
            _FunctionCodeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _FunctionCodeTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            _FunctionCodeTextBox.Size = new System.Drawing.Size(762, 337);
            _FunctionCodeTextBox.TabIndex = 1;
            _FunctionCodeTextBox.Text = "function sum ( ... )" + '\r' + '\n' + "  local result = 0" + '\r' + '\n' + "  for index, value in ipairs( arg ) do " + "" + '\r' + '\n' + "    result = result + value" + '\r' + '\n' + "  end" + '\r' + '\n' + "  print ( table.concat( arg , \" + \" ) .. \"" + " = String.Empty .. result )" + '\r' + '\n' + "end" + '\r' + '\n';
            // 
            // _FunctionsPanel1
            // 
            _FunctionsPanel1.Controls.Add(_FunctionArgsTextBox);
            _FunctionsPanel1.Controls.Add(_FunctionNameTextBox);
            _FunctionsPanel1.Controls.Add(__CallFunctionButton);
            _FunctionsPanel1.Controls.Add(_FunctionNameTextBoxLabel);
            _FunctionsPanel1.Controls.Add(_FunctionArgsTextBoxLabel);
            _FunctionsPanel1.Location = new System.Drawing.Point(10, 13);
            _FunctionsPanel1.Name = "_FunctionsPanel1";
            _FunctionsPanel1.Size = new System.Drawing.Size(519, 69);
            _FunctionsPanel1.TabIndex = 0;
            // 
            // _FunctionArgsTextBox
            // 
            _FunctionArgsTextBox.AcceptsReturn = true;
            _FunctionArgsTextBox.BackColor = System.Drawing.SystemColors.Window;
            _FunctionArgsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _FunctionArgsTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _FunctionArgsTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _FunctionArgsTextBox.Location = new System.Drawing.Point(100, 36);
            _FunctionArgsTextBox.MaxLength = 0;
            _FunctionArgsTextBox.Name = "_FunctionArgsTextBox";
            _FunctionArgsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _FunctionArgsTextBox.Size = new System.Drawing.Size(305, 25);
            _FunctionArgsTextBox.TabIndex = 3;
            _FunctionArgsTextBox.Text = "1,2";
            // 
            // _FunctionNameTextBox
            // 
            _FunctionNameTextBox.AcceptsReturn = true;
            _FunctionNameTextBox.BackColor = System.Drawing.SystemColors.Window;
            _FunctionNameTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _FunctionNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _FunctionNameTextBox.ForeColor = System.Drawing.SystemColors.WindowText;
            _FunctionNameTextBox.Location = new System.Drawing.Point(100, 8);
            _FunctionNameTextBox.MaxLength = 0;
            _FunctionNameTextBox.Name = "_FunctionNameTextBox";
            _FunctionNameTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _FunctionNameTextBox.Size = new System.Drawing.Size(305, 25);
            _FunctionNameTextBox.TabIndex = 1;
            _FunctionNameTextBox.Text = "sum";
            // 
            // _FunctionNameTextBoxLabel
            // 
            _FunctionNameTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _FunctionNameTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _FunctionNameTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _FunctionNameTextBoxLabel.Location = new System.Drawing.Point(3, 8);
            _FunctionNameTextBoxLabel.Name = "_FunctionNameTextBoxLabel";
            _FunctionNameTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _FunctionNameTextBoxLabel.Size = new System.Drawing.Size(105, 22);
            _FunctionNameTextBoxLabel.TabIndex = 0;
            _FunctionNameTextBoxLabel.Text = "Function Name: ";
            _FunctionNameTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FunctionArgsTextBoxLabel
            // 
            _FunctionArgsTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _FunctionArgsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _FunctionArgsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _FunctionArgsTextBoxLabel.Location = new System.Drawing.Point(3, 36);
            _FunctionArgsTextBoxLabel.Name = "_FunctionArgsTextBoxLabel";
            _FunctionArgsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _FunctionArgsTextBoxLabel.Size = new System.Drawing.Size(105, 22);
            _FunctionArgsTextBoxLabel.TabIndex = 2;
            _FunctionArgsTextBoxLabel.Text = "Arguments: ";
            _FunctionArgsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FunctionsTableLayoutPanel5
            // 
            _FunctionsTableLayoutPanel5.ColumnCount = 3;
            _FunctionsTableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _FunctionsTableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _FunctionsTableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _FunctionsTableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _FunctionsTableLayoutPanel5.Controls.Add(__LoadFunctionButton, 2, 0);
            _FunctionsTableLayoutPanel5.Controls.Add(_FunctionCodeTextBoxLabel, 0, 0);
            _FunctionsTableLayoutPanel5.Controls.Add(_CallInstructionsLabel, 1, 0);
            _FunctionsTableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            _FunctionsTableLayoutPanel5.Location = new System.Drawing.Point(10, 83);
            _FunctionsTableLayoutPanel5.Name = "_FunctionsTableLayoutPanel5";
            _FunctionsTableLayoutPanel5.RowCount = 1;
            _FunctionsTableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _FunctionsTableLayoutPanel5.Size = new System.Drawing.Size(762, 33);
            _FunctionsTableLayoutPanel5.TabIndex = 25;
            // 
            // _FunctionCodeTextBoxLabel
            // 
            _FunctionCodeTextBoxLabel.BackColor = System.Drawing.Color.Transparent;
            _FunctionCodeTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _FunctionCodeTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _FunctionCodeTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _FunctionCodeTextBoxLabel.Location = new System.Drawing.Point(3, 0);
            _FunctionCodeTextBoxLabel.Name = "_FunctionCodeTextBoxLabel";
            _FunctionCodeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _FunctionCodeTextBoxLabel.Size = new System.Drawing.Size(100, 33);
            _FunctionCodeTextBoxLabel.TabIndex = 0;
            _FunctionCodeTextBoxLabel.Text = "Function Code: ";
            _FunctionCodeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // _CallInstructionsLabel
            // 
            _CallInstructionsLabel.BackColor = System.Drawing.Color.Transparent;
            _CallInstructionsLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _CallInstructionsLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _CallInstructionsLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            _CallInstructionsLabel.Location = new System.Drawing.Point(109, 0);
            _CallInstructionsLabel.Name = "_CallInstructionsLabel";
            _CallInstructionsLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _CallInstructionsLabel.Size = new System.Drawing.Size(515, 33);
            _CallInstructionsLabel.TabIndex = 30;
            _CallInstructionsLabel.Text = "Values returned by the instrument show under the Console tab. ";
            _CallInstructionsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FunctionsView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_FunctionsTableLayoutPanel);
            Name = "FunctionsView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(782, 468);
            _FunctionsTableLayoutPanel.ResumeLayout(false);
            _FunctionsTableLayoutPanel.PerformLayout();
            _FunctionsPanel1.ResumeLayout(false);
            _FunctionsPanel1.PerformLayout();
            _FunctionsTableLayoutPanel5.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _FunctionsTableLayoutPanel;
        private System.Windows.Forms.TextBox _FunctionCodeTextBox;
        private System.Windows.Forms.Panel _FunctionsPanel1;
        private System.Windows.Forms.TextBox _FunctionArgsTextBox;
        private System.Windows.Forms.TextBox _FunctionNameTextBox;
        private System.Windows.Forms.Button __CallFunctionButton;

        private System.Windows.Forms.Button _CallFunctionButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CallFunctionButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CallFunctionButton != null)
                {
                    __CallFunctionButton.Click -= CallFunctionButton_Click;
                }

                __CallFunctionButton = value;
                if (__CallFunctionButton != null)
                {
                    __CallFunctionButton.Click += CallFunctionButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _FunctionNameTextBoxLabel;
        private System.Windows.Forms.Label _FunctionArgsTextBoxLabel;
        private System.Windows.Forms.TableLayoutPanel _FunctionsTableLayoutPanel5;
        private System.Windows.Forms.Button __LoadFunctionButton;

        private System.Windows.Forms.Button _LoadFunctionButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadFunctionButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadFunctionButton != null)
                {
                    __LoadFunctionButton.Click -= LoadFunctionButton_Click;
                }

                __LoadFunctionButton = value;
                if (__LoadFunctionButton != null)
                {
                    __LoadFunctionButton.Click += LoadFunctionButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _FunctionCodeTextBoxLabel;
        private System.Windows.Forms.Label _CallInstructionsLabel;
    }
}