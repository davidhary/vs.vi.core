using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> Keithley Tsp Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "Tsp User Interface" )]
    [Description( "Keithley Tsp Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( RigTreeView ) )]
    public class RigTreeView : Facade.VisaTreeView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public RigTreeView() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.ConsoleView = ConsoleView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Console", "Console", this.ConsoleView );
            this.ScriptsView = ScriptsView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Scripts", "Scripts", this.ScriptsView );
            this.FunctionsView = FunctionsView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Functions", "Functions", this.FunctionsView );
            this.InstrumentView = InstrumentView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Instrument", "Instrument", this.InstrumentView );
            this.InitializingComponents = false;
        }

        /// <summary> Initializes the component. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // RigView
            // 
            this.Name = "RigView";
            this.Size = new System.Drawing.Size( 899, 550 );
            this.ResumeLayout( false );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public RigTreeView( TspDevice device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary> Gets the console view. </summary>
        /// <value> The console view. </value>
        private ConsoleView ConsoleView { get; set; }

        /// <summary> Gets the scripts view. </summary>
        /// <value> The scripts view. </value>
        private ScriptsView ScriptsView { get; set; }

        /// <summary> Gets the functions view. </summary>
        /// <value> The functions view. </value>
        private FunctionsView FunctionsView { get; set; }

        /// <summary> Gets the instrument view. </summary>
        /// <value> The instrument view. </value>
        private InstrumentView InstrumentView { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the K2700 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // If Me.SourceView IsNot Nothing Then Me._SourceView.Dispose() : Me._SourceView = Nothing
                    this.AssignDeviceThis( null );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TspDevice Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = Rig.My.MySettings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.ConsoleView.AssignDevice( null );
                this.ScriptsView.AssignDevice( null );
                this.FunctionsView.AssignDevice( null );
                this.InstrumentView.AssignDevice( null );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            // MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
            // MyBase.DisplayView.BindSubsystemToolstrip(Me.Device.SourceSubsystem)
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.ConsoleView.AssignDevice( this.Device );
            this.ScriptsView.AssignDevice( this.Device );
            this.FunctionsView.AssignDevice( this.Device );
            this.InstrumentView.AssignDevice( this.Device );
            // TO_DO: Assign execution state caption to the display.
            // to_do: Assign prompt and errors display toggle to the display.
            // MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
            // MyBase.DisplayView.BindSubsystemToolstrip(Me.Device.SourceSubsystem)
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
