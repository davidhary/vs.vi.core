using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> A console view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ConsoleView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ConsoleView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__InputTextBox.Name = "_InputTextBox";
        }

        /// <summary> Creates a new <see cref="ConsoleView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ConsoleView"/>. </returns>
        public static ConsoleView Create()
        {
            ConsoleView view = null;
            try
            {
                view = new ConsoleView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    this.DisposeRefreshTimer();
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TspDevice Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.StopRefreshTimer();
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
                this.StartRefreshTimer( TimeSpan.FromMilliseconds( 500d ) );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " IO "

        /// <summary> The receive lock. </summary>
        private readonly object _ReceiveLock = new ();

        /// <summary> Receive a message from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="updateConsole"> True to update the instrument output text box. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Receive( bool updateConsole )
        {
            lock ( this._ReceiveLock )
            {
                string activity = string.Empty;
                try
                {
                    activity = "receiving";

                    // read a message if we have messages.
                    string receiveBuffer = string.Empty;
                    if ( updateConsole && this.Device.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 50d ), 3 ) )
                    {

                        // Turn on the form hourglass
                        this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                        activity = "reading";
                        _ = this.PublishInfo( $"{activity};. " );
                        if ( this.Device?.IsDeviceOpen == true )
                        {
                            this.Device.Session.StartElapsedStopwatch();
                            receiveBuffer = this.Device.Session.ReadLine();
                            _ = this.Device.Session.ReadElapsedTime( true );
                        }

                        activity = "done reading";
                        _ = this.PublishInfo( $"{activity};. " );
                        this._OutputTextBox.SelectionStart = this._OutputTextBox.Text.Length;
                        this._OutputTextBox.SelectionLength = 0;
                        this._OutputTextBox.SelectedText = receiveBuffer + Constants.vbCrLf;
                        this._OutputTextBox.SelectionStart = this._OutputTextBox.Text.Length;
                    }
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
                finally
                {

                    // Turn off the form hourglass
                    this.Cursor = System.Windows.Forms.Cursors.Default;
                }
            }
        }

        /// <summary> Sends a message to the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sendBuffer">         Specifies the message to send. </param>
        /// <param name="updateInputConsole"> True to update the instrument input text box. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Send( string sendBuffer, bool updateInputConsole )
        {

            // Turn on the form hourglass
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            string activity = string.Empty;
            try
            {
                if ( !string.IsNullOrWhiteSpace( sendBuffer ) )
                {
                    this.RefreshTimer.Enabled = false;
                    activity = "Sending query/command";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.Device.Session.StartElapsedStopwatch();
                    _ = this.Device.Session.WriteLine( sendBuffer );
                    this.Device.StatusSubsystem.TraceVisaOperation( "sending query/command;. '{0}'", sendBuffer );
                    _ = this.Device.Session.ReadElapsedTime( true );
                    if ( updateInputConsole )
                    {
                        this._InputTextBox.SelectionStart = this._InputTextBox.Text.Length;
                        this._InputTextBox.SelectionLength = 0;
                        this._InputTextBox.SelectedText = sendBuffer + Constants.vbCrLf;
                        this._InputTextBox.SelectionStart = this._InputTextBox.Text.Length;
                    }
                }
            }
            catch ( Pith.NativeException ex )
            {
                this.Device.StatusSubsystem.TraceVisaOperation( ex, "sending query/command;. '{0}'", sendBuffer );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.RefreshTimer.Enabled = true;

                // Turn off the form hourglass
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region " REFRESH TIMER "

        /// <summary> Dispose refresh timer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void DisposeRefreshTimer()
        {
            // dispose of the timer.
            if ( this.RefreshTimer is object )
            {
                this.RefreshTimer.Close();
                this.RefreshTimer.Dispose();
                this.RefreshTimer = null;
            }
        }

        /// <summary> Starts refresh timer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="interval"> The interval. </param>
        private void StartRefreshTimer( TimeSpan interval )
        {
            this.RefreshTimer = new Core.Controls.StateAwareTimer() { SynchronizingObject = this };
            this.RestartRefreshTimer( interval );
        }

        /// <summary> Restarts refresh timer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="interval"> The interval. </param>
        private void RestartRefreshTimer( TimeSpan interval )
        {
            if ( this.RefreshTimer is object )
            {
                this.RefreshTimer.Interval = interval.TotalMilliseconds;
                this.RefreshTimer.AutoReset = true;
                _ = this.RefreshTimer.Start();
            }
        }

        /// <summary> Stops the refresh timer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void StopRefreshTimer()
        {
            if ( this.RefreshTimer is object )
            {
                _ = this.RefreshTimer.Stop();
                this.RefreshTimer.AutoReset = false;
                _ = this.PublishInfo( "Timer stopped;. " );
            }
        }

        private Core.Controls.StateAwareTimer _RefreshTimer;

        private Core.Controls.StateAwareTimer RefreshTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._RefreshTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._RefreshTimer != null )
                {
                    this._RefreshTimer.Tick -= this.RefreshTimer_Tick;
                }

                this._RefreshTimer = value;
                if ( this._RefreshTimer != null )
                {
                    this._RefreshTimer.Tick += this.RefreshTimer_Tick;
                }
            }
        }

        /// <summary> Serial polls and receives. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RefreshTimer_Tick( object eventSender, EventArgs eventArgs )
        {
            string activity = string.Empty;
            try
            {
                activity = "refreshing";
                if ( this.Device.IsSessionOpen )
                {
                    this.Receive( true );
                    _ = this.Device.Session.ReadStatusRegister();
                    _ = this.Device.InteractiveSubsystem.ReadExecutionState();
                }
            }
            catch ( Exception ex )
            {
                // stop the time on error
                this.StopRefreshTimer();
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }


        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Gets or sets the 'query' command. </summary>
        /// <value> The 'query' command. </value>
        private string QueryCommand { get; set; }

        /// <summary>
        /// Event handler. Called by _inputTextBox for key press events. Handles key at the command
        /// console.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventSender"> The event sender. </param>
        /// <param name="eventArgs">   Key press event information. </param>
        private void InputTextBox_KeyPress( object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs )
        {
            int keyAscii = Strings.Asc( eventArgs.KeyChar );
            switch ( keyAscii )
            {
                case ( int ) System.Windows.Forms.Keys.Return: // enter key
                    {

                        // User pressed enter key.  Send the string to the instrument.

                        // Disable the command console while waiting for command to process.
                        this._InputTextBox.Enabled = false;


                        // dh 6.0.02 use command line at cursor
                        if ( this._InputTextBox.Lines is object && this._InputTextBox.Lines.Length > 0 )
                        {
                            int line = this._InputTextBox.GetLineFromCharIndex( this._InputTextBox.SelectionStart );
                            this.QueryCommand = this._InputTextBox.Lines[line];
                        }
                        else
                        {
                            this.QueryCommand = string.Empty;
                        }
                        // queryCommand = getLine(Me._inputTextBox)

                        // Send the string to the instrument. Receive is invoked by the timer.
                        this.Send( this.QueryCommand, false );

                        // TO_DO: this is a perfect place for setting a service request before read and
                        // letting the service request invoke the next read.

                        // clear the command
                        this.QueryCommand = string.Empty;

                        // Turn command console back on and display response.
                        this._InputTextBox.Enabled = true;
                        _ = this._InputTextBox.Focus();
                        break;
                    }

                case ( int ) System.Windows.Forms.Keys.Back: // backspace key
                    {
                        this.QueryCommand = this.QueryCommand.Length < 2 ? string.Empty : this.QueryCommand.Substring( 0, this.QueryCommand.Length - 1 ); // normal key
                        break;
                    }

                default:
                    {
                        // Stash key in buffer.
                        this.QueryCommand += Conversions.ToString( Strings.Chr( keyAscii ) );
                        break;
                    }
            }

            eventArgs.KeyChar = Strings.Chr( keyAscii );
            if ( keyAscii == 0 )
            {
                eventArgs.Handled = true;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
