﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{
    [DesignerGenerated()]
    public partial class FirmwareLoaderView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            __SaveFirmwareButton = new System.Windows.Forms.Button();
            __SaveFirmwareButton.Click += new EventHandler(SaveFirmwareButton_Click);
            __DeleteFirmwareButton = new System.Windows.Forms.Button();
            __DeleteFirmwareButton.Click += new EventHandler(DeleteFirmwareButton_Click);
            __LoadFirmwareButton = new System.Windows.Forms.Button();
            __LoadFirmwareButton.Click += new EventHandler(LoadFirmwareButton_Click);
            _FirmwareStatusTextBox = new System.Windows.Forms.TextBox();
            _FirmwareStatusTextBoxLabel = new System.Windows.Forms.Label();
            _ReleasedFirmwareVersionTextBox = new System.Windows.Forms.TextBox();
            _InstalledFirmwareVersionTextBox = new System.Windows.Forms.TextBox();
            _ReleasedFirmwareVersionTextBoxLabel = new System.Windows.Forms.Label();
            _InstalledFirmwareVersionTextBoxLabel = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // _SaveFirmwareButton
            // 
            __SaveFirmwareButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __SaveFirmwareButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __SaveFirmwareButton.Location = new System.Drawing.Point(325, 251);
            __SaveFirmwareButton.Name = "__SaveFirmwareButton";
            __SaveFirmwareButton.Size = new System.Drawing.Size(147, 28);
            __SaveFirmwareButton.TabIndex = 8;
            __SaveFirmwareButton.Text = "SAVE FIRMWARE";
            ToolTip.SetToolTip(__SaveFirmwareButton, "Saves firmware to non-volatile memory");
            __SaveFirmwareButton.UseVisualStyleBackColor = true;
            // 
            // _DeleteFirmwareButton
            // 
            __DeleteFirmwareButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            __DeleteFirmwareButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __DeleteFirmwareButton.Location = new System.Drawing.Point(6, 250);
            __DeleteFirmwareButton.Name = "__DeleteFirmwareButton";
            __DeleteFirmwareButton.Size = new System.Drawing.Size(147, 28);
            __DeleteFirmwareButton.TabIndex = 6;
            __DeleteFirmwareButton.Text = "UNLOAD FIRMWARE";
            ToolTip.SetToolTip(__DeleteFirmwareButton, "Deletes the current TTM version");
            __DeleteFirmwareButton.UseVisualStyleBackColor = true;
            // 
            // _LoadFirmwareButton
            // 
            __LoadFirmwareButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            __LoadFirmwareButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __LoadFirmwareButton.Location = new System.Drawing.Point(167, 252);
            __LoadFirmwareButton.Name = "__LoadFirmwareButton";
            __LoadFirmwareButton.Size = new System.Drawing.Size(147, 28);
            __LoadFirmwareButton.TabIndex = 7;
            __LoadFirmwareButton.Text = "L&OAD FIRMWARE";
            ToolTip.SetToolTip(__LoadFirmwareButton, "Loads the current firmware to the Driver.");
            __LoadFirmwareButton.UseVisualStyleBackColor = true;
            // 
            // _FirmwareStatusTextBox
            // 
            _FirmwareStatusTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _FirmwareStatusTextBox.BackColor = System.Drawing.SystemColors.Info;
            _FirmwareStatusTextBox.Location = new System.Drawing.Point(2, 50);
            _FirmwareStatusTextBox.Multiline = true;
            _FirmwareStatusTextBox.Name = "_FirmwareStatusTextBox";
            _FirmwareStatusTextBox.ReadOnly = true;
            _FirmwareStatusTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _FirmwareStatusTextBox.Size = new System.Drawing.Size(474, 194);
            _FirmwareStatusTextBox.TabIndex = 5;
            // 
            // _FirmwareStatusTextBoxLabel
            // 
            _FirmwareStatusTextBoxLabel.AutoSize = true;
            _FirmwareStatusTextBoxLabel.Location = new System.Drawing.Point(-2, 32);
            _FirmwareStatusTextBoxLabel.Name = "_FirmwareStatusTextBoxLabel";
            _FirmwareStatusTextBoxLabel.Size = new System.Drawing.Size(144, 17);
            _FirmwareStatusTextBoxLabel.TabIndex = 4;
            _FirmwareStatusTextBoxLabel.Text = "STATUS AND ACTIONS:";
            // 
            // _ReleasedFirmwareVersionTextBox
            // 
            _ReleasedFirmwareVersionTextBox.BackColor = System.Drawing.SystemColors.Info;
            _ReleasedFirmwareVersionTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ReleasedFirmwareVersionTextBox.Location = new System.Drawing.Point(377, 1);
            _ReleasedFirmwareVersionTextBox.Name = "_ReleasedFirmwareVersionTextBox";
            _ReleasedFirmwareVersionTextBox.ReadOnly = true;
            _ReleasedFirmwareVersionTextBox.Size = new System.Drawing.Size(80, 25);
            _ReleasedFirmwareVersionTextBox.TabIndex = 3;
            _ReleasedFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ReleasedFirmwareVersionTextBox, "Last released firmware revision. The installed version must be the same as the re" + "leased version.");
            // 
            // _InstalledFirmwareVersionTextBox
            // 
            _InstalledFirmwareVersionTextBox.BackColor = System.Drawing.SystemColors.Info;
            _InstalledFirmwareVersionTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _InstalledFirmwareVersionTextBox.Location = new System.Drawing.Point(159, 1);
            _InstalledFirmwareVersionTextBox.Name = "_InstalledFirmwareVersionTextBox";
            _InstalledFirmwareVersionTextBox.ReadOnly = true;
            _InstalledFirmwareVersionTextBox.Size = new System.Drawing.Size(80, 25);
            _InstalledFirmwareVersionTextBox.TabIndex = 1;
            _InstalledFirmwareVersionTextBox.Text = "2.1.3320";
            _InstalledFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_InstalledFirmwareVersionTextBox, "Firmware revision now installed in the instrument (if any)");
            // 
            // _ReleasedFirmwareVersionTextBoxLabel
            // 
            _ReleasedFirmwareVersionTextBoxLabel.AutoSize = true;
            _ReleasedFirmwareVersionTextBoxLabel.Location = new System.Drawing.Point(305, 5);
            _ReleasedFirmwareVersionTextBoxLabel.Name = "_ReleasedFirmwareVersionTextBoxLabel";
            _ReleasedFirmwareVersionTextBoxLabel.Size = new System.Drawing.Size(70, 17);
            _ReleasedFirmwareVersionTextBoxLabel.TabIndex = 2;
            _ReleasedFirmwareVersionTextBoxLabel.Text = "RELEASED:";
            // 
            // _InstalledFirmwareVersionTextBoxLabel
            // 
            _InstalledFirmwareVersionTextBoxLabel.AutoSize = true;
            _InstalledFirmwareVersionTextBoxLabel.Location = new System.Drawing.Point(8, 5);
            _InstalledFirmwareVersionTextBoxLabel.Name = "_InstalledFirmwareVersionTextBoxLabel";
            _InstalledFirmwareVersionTextBoxLabel.Size = new System.Drawing.Size(149, 17);
            _InstalledFirmwareVersionTextBoxLabel.TabIndex = 0;
            _InstalledFirmwareVersionTextBoxLabel.Text = "VERSIONS.   INSTALLED:";
            // 
            // FirmwareLoaderControlBase
            // 
            Controls.Add(__SaveFirmwareButton);
            Controls.Add(__DeleteFirmwareButton);
            Controls.Add(__LoadFirmwareButton);
            Controls.Add(_InstalledFirmwareVersionTextBoxLabel);
            Controls.Add(_FirmwareStatusTextBox);
            Controls.Add(_ReleasedFirmwareVersionTextBoxLabel);
            Controls.Add(_FirmwareStatusTextBoxLabel);
            Controls.Add(_InstalledFirmwareVersionTextBox);
            Controls.Add(_ReleasedFirmwareVersionTextBox);
            Name = "FirmwareLoaderControlBase";
            Size = new System.Drawing.Size(479, 281);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.Button __SaveFirmwareButton;

        private System.Windows.Forms.Button _SaveFirmwareButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SaveFirmwareButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SaveFirmwareButton != null)
                {
                    __SaveFirmwareButton.Click -= SaveFirmwareButton_Click;
                }

                __SaveFirmwareButton = value;
                if (__SaveFirmwareButton != null)
                {
                    __SaveFirmwareButton.Click += SaveFirmwareButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __DeleteFirmwareButton;

        private System.Windows.Forms.Button _DeleteFirmwareButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DeleteFirmwareButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DeleteFirmwareButton != null)
                {
                    __DeleteFirmwareButton.Click -= DeleteFirmwareButton_Click;
                }

                __DeleteFirmwareButton = value;
                if (__DeleteFirmwareButton != null)
                {
                    __DeleteFirmwareButton.Click += DeleteFirmwareButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __LoadFirmwareButton;

        private System.Windows.Forms.Button _LoadFirmwareButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadFirmwareButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadFirmwareButton != null)
                {
                    __LoadFirmwareButton.Click -= LoadFirmwareButton_Click;
                }

                __LoadFirmwareButton = value;
                if (__LoadFirmwareButton != null)
                {
                    __LoadFirmwareButton.Click += LoadFirmwareButton_Click;
                }
            }
        }

        private System.Windows.Forms.TextBox _FirmwareStatusTextBox;
        private System.Windows.Forms.Label _FirmwareStatusTextBoxLabel;
        private System.Windows.Forms.TextBox _ReleasedFirmwareVersionTextBox;
        private System.Windows.Forms.TextBox _InstalledFirmwareVersionTextBox;
        private System.Windows.Forms.Label _ReleasedFirmwareVersionTextBoxLabel;
        private System.Windows.Forms.Label _InstalledFirmwareVersionTextBoxLabel;
    }
}