using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> Firmware loader control. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-04-12 </para>
    /// </remarks>
    public partial class FirmwareLoaderView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// A private constructor for this class making it not publicly creatable. This ensure using the
        /// class as a singleton.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public FirmwareLoaderView() : base()
        {
            this.InitializingComponents = true;
            this.InitializeComponent();
            // indicates the versions are current.
            this.VersionCompareStatus = VersionCompare.Current;
            this.InitializingComponents = false;
            this.__SaveFirmwareButton.Name = "_SaveFirmwareButton";
            this.__DeleteFirmwareButton.Name = "_DeleteFirmwareButton";
            this.__LoadFirmwareButton.Name = "_LoadFirmwareButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.TspDevice = null;
                    if ( disposing && this.components is object )
                    {
                        this.components.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " TSP RIG DEVICE "

        /// <summary> Gets or sets reference to the TSP device accessing the TTM Driver. </summary>
        /// <value> The tsp device. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TspDevice TspDevice { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.VisaSessionBase is object )
            {
                this.TspDevice.PropertyChanged -= this.TspDevice_PropertyChanged;
                this.AssignScriptManager( null );
                this.AssignTalker( null );
                this.VisaSessionBase = null;
            }

            this.TspDevice = value;
            if ( value is object )
            {
                this.AssignScriptManager( new TtmScriptManager( value ) );
                this.AssignTalker( this.TspDevice.Talker );
                this.TspDevice.PropertyChanged += this.TspDevice_PropertyChanged;
            }

            this.BindVisaSessionBase( value as VisaSessionBase );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Executes the session factory property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( TspDevice sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object && !string.IsNullOrWhiteSpace( e.PropertyName ) )
            {
                switch ( e.PropertyName ?? "" )
#pragma warning disable CS1522 // Empty switch block
                {
#pragma warning restore CS1522 // Empty switch block
                }
            }
        }

        /// <summary> Event handler. Called by _MasterDevice for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TspDevice_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling {typeof( TspDevice )}.{e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TspDevice_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TspDevice, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " visa session base (device base) "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void BinVisaSessionBaseThis( VisaSessionBase value )
        {
            if ( this.VisaSessionBase is object )
            {
                this.VisaSessionBase.Opening -= this.DeviceOpening;
                this.VisaSessionBase.Opened -= this.DeviceOpened;
                this.VisaSessionBase.Closing -= this.DeviceClosing;
                this.VisaSessionBase.Closed -= this.DeviceClosed;
                this.VisaSessionBase.Initialized -= this.DeviceInitialized;
                this.VisaSessionBase.Initializing -= this.DeviceInitializing;
                this.VisaSessionBase.SessionFactory.PropertyChanged -= this.SessionFactoryPropertyChanged;
                this.AssignTalker( null );
                this.VisaSessionBase = null;
            }

            this.VisaSessionBase = value;
            if ( value is object )
            {
                this.AssignTalker( this.VisaSessionBase.Talker );
                this.VisaSessionBase.Opening += this.DeviceOpening;
                this.VisaSessionBase.Opened += this.DeviceOpened;
                this.VisaSessionBase.Closing += this.DeviceClosing;
                this.VisaSessionBase.Closed += this.DeviceClosed;
                this.VisaSessionBase.Initialized += this.DeviceInitialized;
                this.VisaSessionBase.Initializing += this.DeviceInitializing;
                this.VisaSessionBase.SessionFactory.PropertyChanged += this.SessionFactoryPropertyChanged;
                // Me.VisaSessionBase.SessionFactory.CandidateResourceName = isr.VI.Tsp.K2600.Ttm.My.Settings.ResourceName
                if ( this.VisaSessionBase.IsDeviceOpen )
                {
                    this.DeviceOpened( this.VisaSessionBase, EventArgs.Empty );
                }
                else
                {
                    this.DeviceClosed( this.VisaSessionBase, EventArgs.Empty );
                }
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void BindVisaSessionBase( VisaSessionBase value )
        {
            this.BinVisaSessionBaseThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.VisaSessionBase.ResourceNameCaption} reading service request";
            try
            {
                _ = this.VisaSessionBase.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #region " SESSION FACTORY "

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        public string ResourceName
        {
            get => this.VisaSessionBase.SessionFactory.CandidateResourceName;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceName ) )
                {
                    this.VisaSessionBase.SessionFactory.CandidateResourceName = value;
                }
            }
        }

        /// <summary> Gets or sets the Search Pattern of the resource. </summary>
        /// <value> The Search Pattern of the resource. </value>
        public string ResourceFilter
        {
            get => this.VisaSessionBase.SessionFactory.ResourcesFilter;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResourceFilter ) )
                {
                    this.VisaSessionBase.SessionFactory.ResourcesFilter = value;
                }
            }
        }

        /// <summary> Executes the session factory property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SessionFactory sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SessionFactory.ValidatedResourceName ):
                    {
                        _ = sender.IsOpen
                            ? this.PublishInfo( $"Resource connected;. {sender.ValidatedResourceName}" )
                            : this.PublishInfo( $"Resource locate;. {sender.ValidatedResourceName}" );

                        break;
                    }

                case nameof( SessionFactory.CandidateResourceName ):
                    {
                        if ( !sender.IsOpen )
                        {
                            _ = this.PublishInfo( $"Candidate resource;. {sender.ValidatedResourceName}" );
                        }

                        break;
                    }
            }
        }

        /// <summary> Session factory property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionFactoryPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling {typeof( SessionFactory )}.{e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SessionFactoryPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SessionFactory, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE EVENTS "

        /// <summary>
        /// Event handler. Called upon device opening so as to instantiated all subsystems.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected void DeviceOpening( object sender, CancelEventArgs e )
        {
        }

        /// <summary>
        /// Event handler. Called after the device opened and all subsystems were defined.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of this device. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void DeviceOpened( object sender, EventArgs e )
        {
            var outcome = TraceEventType.Information;
            if ( this.VisaSessionBase.Session.Enabled & !this.VisaSessionBase.Session.IsSessionOpen )
                outcome = TraceEventType.Warning;
            _ = this.Publish( outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ", this.VisaSessionBase.ResourceTitleCaption, this.VisaSessionBase.Session.Enabled.GetHashCode(), this.VisaSessionBase.Session.IsDeviceOpen.GetHashCode(), this.VisaSessionBase.Session.IsSessionOpen.GetHashCode() );
            string activity = string.Empty;
            try
            {
                activity = "finding firmware";

                // check if firmware is embedded in the Driver.
                if ( this.ScriptManager.FindFirmware() )
                {
                    // display the title.
                    activity = "instrument displaying title";
                    this.TspDevice.DisplaySubsystem.DisplayTitle( this.ScriptManager.FrameworkTitle, this.ScriptManager.AuthorTitle );
                }
                else
                {
                    activity = "instrument restoring display";
                    this.TspDevice.DisplaySubsystem.RestoreDisplay();
                }

                activity = "validating script catalog";
                this.ValidateSavedScriptsCatalog( true, true );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " OPENING / OPEN "

        /// <summary>
        /// Attempts to open a session to the device using the specified resource name.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resourceName">  The name of the resource. </param>
        /// <param name="resourceTitle"> The title. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public (bool Success, string Details) TryOpenDeviceSession( string resourceName, string resourceTitle )
        {
            return this.VisaSessionBase.TryOpenSession( resourceName, resourceTitle );
        }

        #endregion

        #region " INITALIZING / INITIALIZED  "

        /// <summary> Device initializing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Cancel event information. </param>
        protected virtual void DeviceInitializing( object sender, CancelEventArgs e )
        {
        }

        /// <summary> Device initialized. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected virtual void DeviceInitialized( object sender, EventArgs e )
        {
        }

        #endregion

        #region " CLOSING / CLOSED "

        /// <summary> Event handler. Called when device is closing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of the device. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosing( object sender, CancelEventArgs e )
        {
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                    activity = $"Disabling meter timer";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.DisableFirmwareControls();
                    activity = $"Disconnecting from {this.ResourceName}";
                    _ = this.PublishInfo( $"{activity};. " );
                    if ( this.VisaSessionBase.Session is object )
                        this.VisaSessionBase.Session.DisableServiceRequestEventHandler();
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        /// <summary> Event handler. Called when device is closed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/> instance of the device. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void DeviceClosed( object sender, EventArgs e )
        {
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                    _ = this.VisaSessionBase.Session.IsSessionOpen
                        ? this.PublishWarning( $"{this.VisaSessionBase.Session.ResourceNameCaption} closed but session still open;. " )
                        : this.VisaSessionBase.Session.IsDeviceOpen
                            ? this.PublishWarning( $"{this.VisaSessionBase.Session.ResourceNameCaption} closed but emulated session still open;. " )
                            : this.PublishVerbose( "Disconnected; Device access closed." );

                    activity = $"disables controls";
                    _ = this.PublishVerbose( $"{activity};. " );
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
            else
            {
                _ = this.PublishInfo( "Disconnected; Device disposed." );
            }
        }

        #endregion

        #endregion

        #region " SCRIPT MANAGER "

        /// <summary> Gets or sets reference to the TSP Script Manager. </summary>
        /// <value> The tsp Script Manager. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TtmScriptManager ScriptManager { get; private set; }

        /// <summary> Assigns the Script Manager and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignScriptManagerThis( TtmScriptManager value )
        {
            if ( this.ScriptManager is object )
            {
                this.ScriptManager.PropertyChanged -= this.ScriptManager_PropertyChanged;
                this.AssignTalker( null );
                this.ScriptManager = null;
            }

            this.ScriptManager = value;
            if ( value is object )
            {
                this.AssignTalker( this.ScriptManager.Talker );
                this.ScriptManager.PropertyChanged += this.ScriptManager_PropertyChanged;
            }
        }

        /// <summary> Assigns a ScriptManager. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignScriptManager( TtmScriptManager value )
        {
            this.AssignScriptManagerThis( value );
        }

        /// <summary> Executes the session factory property changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( ScriptManager sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object && !string.IsNullOrWhiteSpace( e.PropertyName ) )
            {
                switch ( e.PropertyName ?? "" )
#pragma warning disable CS1522 // Empty switch block
                {
#pragma warning restore CS1522 // Empty switch block
                }
            }
        }

        /// <summary> Event handler. Called by _ScriptManager for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ScriptManager_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling {typeof( ScriptManager )}.{e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ScriptManager_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ScriptManager, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHECK FIRMWARE "

        /// <summary> Gets or sets the installed firmware version. </summary>
        /// <value> The installed firmware version. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string FirmwareInstalledVersion
        {
            get => this._InstalledFirmwareVersionTextBox.Text;

            set {
                if ( !string.Equals( value, this.FirmwareInstalledVersion ) )
                {
                    this._InstalledFirmwareVersionTextBox.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Released firmware version. </summary>
        /// <value> The Released firmware version. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string FirmwareReleasedVersion
        {
            get => this._ReleasedFirmwareVersionTextBox.Text;

            set {
                if ( !string.Equals( value, this.FirmwareReleasedVersion ) )
                {
                    this._ReleasedFirmwareVersionTextBox.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the firmware installed status. </summary>
        /// <value> The firmware installed status. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string FirmwareInstalledStatus
        {
            get => this._FirmwareStatusTextBox.Text;

            set {
                if ( !string.Equals( value, this.FirmwareInstalledStatus ) )
                {
                    this._FirmwareStatusTextBox.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Validates the catalog of saved scripts. Turns on <see cref="UpdateRequired">update
        /// sentinel</see> if the catalog needs to be updated.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="refreshScriptCatalog"> Specifies the condition for updating the catalog of saved
        ///                                     scripts before checking the status of these scripts. 
        /// </param>
        /// <param name="updateDisplay">        true to update the instrument display. </param>
        public void ValidateSavedScriptsCatalog( bool refreshScriptCatalog, bool updateDisplay )
        {
            var firmwareStatusBuilder = new System.Text.StringBuilder();
            string installedVersion = "<not found>";
            string releasedVersion = "<unknown>";
            if ( this.TspDevice.IsDeviceOpen )
            {
                string panelMessage = string.Empty;
                if ( Debugger.IsAttached && this.ScriptManager.Scripts().RequiresReadParseWrite() )
                {
                    this.LoadEnabled = true;
                    this._LoadFirmwareButton.Text = "PARSE SCRIPTS";
                    this.UpdateRequired = false;
                    _ = firmwareStatusBuilder.AppendLine( "Scripts are available for parsing. Click PARSE SCRIPTS." );

                    // set version status to indicate that scripts need to be parsed
                    this.VersionCompareStatus = VersionCompare.ParseScripts;
                }
                else if ( this.ScriptManager.AnyLegacyScriptExists() )
                {
                    this.DeleteEnabled = this.TspDevice.IsDeviceOpen;
                    this.LoadEnabled = false;
                    this.SaveEnabled = false;
                    this.UpdateRequired = false;

                    // set version status to indicate that legacy scripts must be deleted.
                    this.VersionCompareStatus = VersionCompare.DeleteLegacyScripts;
                    _ = firmwareStatusBuilder.AppendLine( "Instrument has legacy firmware loaded. These must be deleted first." );
                    panelMessage = "Delete Legacy F/W";
                }
                else if ( string.IsNullOrWhiteSpace( this.TspDevice.StatusSubsystem.SerialNumberReading ) )
                {
                    _ = firmwareStatusBuilder.AppendLine( "Instrument serial number is empty. Contact developer--Error at Firmware Loader Control Base line 196." );
                    panelMessage = "Instrument serial number is empty";
                }

                // Check if instrument serial number is listed in the instrument resource.
                else if ( this.TspDevice.AccessSubsystem.CertifiedInstruments.IndexOf( Core.HashExtensions.HashExtensionMethods.ToBase64Hash( this.TspDevice.StatusSubsystem.SerialNumberReading ), StringComparison.Ordinal ) < 0 || string.IsNullOrWhiteSpace( this.TspDevice.AccessSubsystem.ReleaseValue( this.TspDevice.StatusSubsystem.SerialNumberReading, this.TspDevice.AccessSubsystem.Salt ) ) )
                {
                    _ = this.PublishWarning( this.TspDevice.StatusSubsystem.NewProgramRequired );
                    _ = firmwareStatusBuilder.AppendLine( this.TspDevice.StatusSubsystem.NewProgramRequired );
                    panelMessage = "GET NEW TTM PROGRAM";
                    this.UpdateRequired = true;
                }
                else
                {

                    // allow loading if any firmware does not exist.
                    this.LoadEnabled = !this.ScriptManager.AllScriptsExist();
                    this.DeleteEnabled = false;
                    this.SaveEnabled = false;
                    if ( this.ScriptManager.AnyScriptExists() )
                    {

                        // allow deleting if any firmware exists.
                        this.DeleteEnabled = true;

                        // allow saving if script loaded but not saved.
                        this.SaveEnabled = !this.ScriptManager.AllScriptsSaved( refreshScriptCatalog );
                    }

                    this.UpdateRequired = false;

                    // check if firmware is embedded in the instrument
                    if ( this.ScriptManager.FindSupportFirmware() )
                    {
                        if ( this.ScriptManager.FindFirmware() )
                        {
                            if ( updateDisplay )
                            {
                                // display the firmware title.
                                this.TspDevice.DisplaySubsystem.DisplayTitle( this.ScriptManager.FrameworkTitle, this.ScriptManager.AuthorTitle );
                            }

                            // read the firmware versions
                            _ = this.ScriptManager.ReadFirmwareVersions();

                            // update the released version
                            releasedVersion = this.ScriptManager.FirmwareReleasedVersionGetter();

                            // set loaded firmware
                            installedVersion = this.ScriptManager.FirmwareVersionGetter();

                            // check if we have the most current firmware
                            int compareStatus = string.Compare( this.ScriptManager.FirmwareVersionGetter(), new Version( this.ScriptManager.FirmwareReleasedVersionGetter() ).ToString( 3 ), System.Globalization.CultureInfo.CurrentCulture, System.Globalization.CompareOptions.OrdinalIgnoreCase );
                            if ( compareStatus == 0 )
                            {
                                this.VersionCompareStatus = VersionCompare.Current;
                                _ = firmwareStatusBuilder.AppendLine( "Thermal Transient Meter firmware is up to date. No actions required." );
                            }
                            else if ( compareStatus < 0 )
                            {
                                this.VersionCompareStatus = VersionCompare.UpdateRequired;
                                _ = firmwareStatusBuilder.AppendLine( "Thermal Transient Meter firmware needs updating. Load new TTM Firmware." );
                            }
                            else if ( compareStatus > 0 )
                            {
                                this.VersionCompareStatus = VersionCompare.NewVersionAvailable;
                                _ = firmwareStatusBuilder.AppendFormat( $"Thermal Transient Meter firmware is old; A new release is available at {this.ScriptManager.FtpAddress}" );
                            }
                        }
                        else
                        {

                            // set version status to indicate that the program needs to be updated.
                            this.VersionCompareStatus = VersionCompare.UpdateRequired;
                            _ = firmwareStatusBuilder.AppendLine( "Firmware not found in the Thermal Transient Meter. Use this application to load new Thermal Transient Meter firmware." );
                            panelMessage = "Load TTM Firmware";
                        }
                    }
                    else
                    {

                        // set version status to indicate that the program needs to be loaded
                        this.VersionCompareStatus = VersionCompare.LoadFirmware;
                        _ = firmwareStatusBuilder.AppendLine( "Firmware not found in the Thermal Transient Meter. Use this program to load Thermal Transient Meter firmware." );
                        panelMessage = "Load TTM Firmware";
                    }

                    if ( !string.IsNullOrWhiteSpace( this.ScriptManager.LastFetchedSavedScripts ) )
                    {
                        _ = firmwareStatusBuilder.Append( "Thermal Transient Meter Firmware saved in non-volatile memory: " );
                        foreach ( string s in this.ScriptManager.LastFetchedAuthorScripts() )
                            _ = firmwareStatusBuilder.AppendLine( s );
                    }
                }

                if ( updateDisplay && !string.IsNullOrWhiteSpace( panelMessage ) )
                {
                    this.TspDevice.DisplaySubsystem.ClearDisplay();
                    this.TspDevice.DisplaySubsystem.DisplayLine( 1, panelMessage );
                }
            }
            else
            {
                this.UpdateRequired = false;
                this.DisableFirmwareControls();
                releasedVersion = this.ScriptManager.MeterFirmwareVersion;
                installedVersion = "<unknown>";
                _ = firmwareStatusBuilder.AppendLine( "<not connected>" );
            }

            this.FirmwareInstalledStatus = firmwareStatusBuilder.ToString();
            this.FirmwareInstalledVersion = installedVersion;
            this.FirmwareReleasedVersion = releasedVersion;
        }

        #endregion

        #region " FIRMWARE MANAGEMENT "

        /// <summary> True if update required. </summary>
        private bool _UpdateRequired;

        /// <summary> Gets or sets the update required. </summary>
        /// <value> The update required. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool UpdateRequired
        {
            get => this._UpdateRequired;

            set {
                if ( !value.Equals( this.UpdateRequired ) )
                {
                    this._UpdateRequired = value;
                    this.NotifyPropertyChanged();
                    if ( value )
                    {
                        this.DisableFirmwareControls();
                    }
                }
            }
        }

        /// <summary> The version compare status. </summary>
        private VersionCompare _VersionCompareStatus;

        /// <summary> Gets or sets the version comparison status. </summary>
        /// <value> The version compare status. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public VersionCompare VersionCompareStatus
        {
            get => this._VersionCompareStatus;

            set {
                if ( !value.Equals( this.VersionCompareStatus ) )
                {
                    this._VersionCompareStatus = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " CONTROLS "

        /// <summary> Disables the firmware controls. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void DisableFirmwareControls()
        {
            this.DeleteEnabled = false;
            this.LoadEnabled = false;
            this.SaveEnabled = false;
        }

        /// <summary> Enable or disable Delete. </summary>
        /// <value> The Delete enabled sentinel. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool DeleteEnabled
        {
            get => this._DeleteFirmwareButton.Enabled;

            set => this._DeleteFirmwareButton.Enabled = value;
        }

        /// <summary> Enable or disable load. </summary>
        /// <value> The load enabled sentinel. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool LoadEnabled
        {
            get => this._LoadFirmwareButton.Enabled;

            set => this._LoadFirmwareButton.Enabled = value;
        }

        /// <summary> Enable or disable Save. </summary>
        /// <value> The Save enabled sentinel. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool SaveEnabled
        {
            get => this._SaveFirmwareButton.Enabled;

            set => this._SaveFirmwareButton.Enabled = value;
        }

        /// <summary>
        /// Event handler. Called by _deleteFirmwareButton for click events. Deletes existing firmware.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeleteFirmwareButton_Click( object sender, EventArgs e )
        {
            string activity = $"User action @{System.Reflection.MethodBase.GetCurrentMethod().Name}";
            _ = this.PublishVerbose( $"{activity};. " );
            this.InfoProvider.Clear();
            activity = "Unloading TTM Firmware";
            _ = this.PublishInfo( $"{activity};. " );

            // delete existing scripts if any
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.UpdateRequired = true;
                this.DisableFirmwareControls();

                // delete script ignoring versions.
                this.ScriptManager.DeleteUserScripts( this.TspDevice.StatusSubsystem, this.TspDevice.DisplaySubsystem );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._DeleteFirmwareButton, Core.Forma.InfoProviderLevel.Error, ex.Message );
                try
                {
                    // flush the buffer so a time out error that might leave stuff will clear the buffers.
                    if ( this.VisaSessionBase.IsSessionOpen )
                        this.VisaSessionBase.Session.DiscardUnreadData();
                }
                catch
                {
                }
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                this.ValidateSavedScriptsCatalog( false, true );
            }
        }

        /// <summary> Loads new firmware or updates existing firmware. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadFirmwareButton_Click( object sender, EventArgs e )
        {
            string activity = $"User action @{System.Reflection.MethodBase.GetCurrentMethod().Name}";
            _ = this.PublishVerbose( $"{activity};. " );
            this.InfoProvider.Clear();
            activity = "Loading TTM Firmware";
            _ = this.PublishInfo( $"{activity};. " );
            string firmwareStatusMessage = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.UpdateRequired = true;
                this.DisableFirmwareControls();
                if ( this.ScriptManager.Scripts().RequiresReadParseWrite() )
                {
                    this.LoadEnabled = false;
                    if ( this.ScriptManager.TryReadParseUserScripts( this.TspDevice.StatusSubsystem, this.TspDevice.DisplaySubsystem ) )
                    {
                        activity = "Adding parsed scripts to resources";
                        _ = this.PublishInfo( $"{activity};. " );
                        firmwareStatusMessage = "TTM Firmware parsed and saved to disk. Add parsed firmware to the application resource.";
                    }
                    else
                    {
                        activity = "Failed reading or parsing TTM Firmware";
                        _ = this.PublishWarning( $"{activity};. " );
                        _ = this.InfoProvider.Annunciate( this._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Failed reading or parsing TTM Firmware" );
                    }
                }
                else if ( this.ScriptManager.UploadUserScripts( this.TspDevice.StatusSubsystem, this.TspDevice.DisplaySubsystem, this.TspDevice.AccessSubsystem ) )
                {

                    // check if code loaded.
                    if ( this.TspDevice.AccessSubsystem.Loaded() )
                    {
                        activity = "Ready for measurements";
                        _ = this.PublishInfo( $"{activity};. " );
                    }
                    else if ( this.TspDevice.AccessSubsystem.Certify( this.TspDevice.AccessSubsystem.ReleaseValue( this.TspDevice.StatusSubsystem.SerialNumberReading, this.TspDevice.AccessSubsystem.Salt ) ) == true )
                    {
                        activity = "Ready for measurements";
                        _ = this.PublishInfo( $"{activity};. " );
                    }
                    else
                    {
                        activity = $"Loading TTM Firmware failed;. Most likely, instrument '{this.TspDevice.StatusSubsystem.Identity}' requires a firmware update.";
                        _ = this.PublishWarning( $"{activity};. " );
                        _ = this.InfoProvider.Annunciate( this._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Loading TTM Firmware failed" );
                    }
                }
                else
                {
                    activity = $"Failed uploading TTM Firmware";
                    _ = this.PublishWarning( $"{activity};. " );
                    _ = this.InfoProvider.Annunciate( this._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Failed uploading TTM Firmware" );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._LoadFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Exception occurred loading TTM Firmware" );
                try
                {
                    // flush the buffer so a time out error that might leave stuff will clear the buffers.
                    if ( this.VisaSessionBase.IsSessionOpen )
                        this.VisaSessionBase.Session.DiscardUnreadData();
                }
                catch
                {
                }
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                this.ValidateSavedScriptsCatalog( true, true );
                if ( !string.IsNullOrWhiteSpace( firmwareStatusMessage ) )
                {
                    this._FirmwareStatusTextBox.Text = firmwareStatusMessage;
                }
            }
        }

        /// <summary> Saves any loaded firmware. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SaveFirmwareButton_Click( object sender, EventArgs e )
        {
            string activity = $"User action @{System.Reflection.MethodBase.GetCurrentMethod().Name}";
            _ = this.PublishVerbose( $"{activity};. " );
            this.InfoProvider.Clear();
            activity = $"Saving TTM Firmware";
            _ = this.PublishInfo( $"{activity};. " );
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;

                // toggles measurements off.
                this.UpdateRequired = true;
                this.DisableFirmwareControls();
                if ( this.ScriptManager.SaveUserScripts( this.VisaSessionBase.StatusSubsystemBase.InitializeTimeout, this.TspDevice.StatusSubsystem, this.TspDevice.DisplaySubsystem, this.TspDevice.AccessSubsystem ) )
                {

                    // check if code loaded.
                    if ( this.TspDevice.AccessSubsystem.Loaded() )
                    {
                        // save the autoexec script
                        if ( this.TspDevice.AccessSubsystem.Loaded() )
                        {
                            activity = $"Ready for measurements";
                            _ = this.PublishInfo( $"{activity};. " );
                        }
                        else if ( this.TspDevice.AccessSubsystem.Certify( this.TspDevice.AccessSubsystem.ReleaseValue( this.TspDevice.StatusSubsystem.SerialNumberReading, this.TspDevice.AccessSubsystem.Salt ) ) == true )
                        {
                            activity = $"Ready for measurements";
                            _ = this.PublishInfo( $"{activity};. " );
                        }
                        else
                        {
                            activity = $"Failed saving startup firmware";
                            _ = this.PublishWarning( $"{activity};. " );
                            _ = this.InfoProvider.Annunciate( this._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Alert, "Failed saving startup firmware" );
                        }
                    }
                    else
                    {
                        activity = $"Saving TTM Firmware failed;. Most likely, instrument '{this.TspDevice.StatusSubsystem.Identity}' requires a firmware update.";
                        _ = this.PublishWarning( $"{activity};. " );
                        _ = this.InfoProvider.Annunciate( this._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Alert, "Saving TTM Firmware failed" );
                    }
                }
                else
                {
                    activity = $"Failed saving TTM Firmware";
                    _ = this.PublishWarning( $"{activity};. " );
                    _ = this.InfoProvider.Annunciate( this._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Alert, "Failed saving TTM Firmware" );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this._SaveFirmwareButton, Core.Forma.InfoProviderLevel.Error, "Exception occurred saving TTM Firmware" );
                try
                {
                    // flush the buffer so a time out error that might leave stuff will clear the buffers.
                    if ( this.VisaSessionBase.IsSessionOpen )
                        this.VisaSessionBase.Session.DiscardUnreadData();
                }
                catch
                {
                }
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
                this.ValidateSavedScriptsCatalog( true, true );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            Rig.My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }

    /// <summary> Values that represent VersionCompare. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum VersionCompare
    {

        /// <summary> An enum constant representing the current option. </summary>
        [Description( "Thermal Transient Meter firmware is up to date" )]
        Current = 0,

        /// <summary> An enum constant representing the parse scripts option. </summary>
        [Description( "Scripts are available for parsing" )]
        ParseScripts = -3,

        /// <summary> An enum constant representing the delete legacy scripts option. </summary>
        [Description( "Legacy scripts must be deleted" )]
        DeleteLegacyScripts = -2,

        /// <summary> An enum constant representing the update required option. </summary>
        [Description( "Thermal Transient Meter firmware needs updating" )]
        UpdateRequired = -1,

        /// <summary> An enum constant representing the new version available option. </summary>
        [Description( "Thermal Transient Meter firmware is available to download" )]
        NewVersionAvailable = 1,

        /// <summary> An enum constant representing the load firmware option. </summary>
        [Description( "Thermal Transient Meter firmware loading required" )]
        LoadFirmware = 2
    }
}
