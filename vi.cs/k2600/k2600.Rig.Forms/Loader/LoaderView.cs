using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> A loader view. </summary>
    /// <remarks>
    /// (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-04-24 </para>
    /// </remarks>
    public class LoaderView : Facade.SplitVisaView
    {

        #region " CONSTRUCTION AND CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public LoaderView() : base()
        {
            this.ProgramInfo = new Core.ProgramInfo();
        }

        /// <summary>
        /// Releases the unmanaged resources used by the K2600 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    this.AssignDeviceThis( null );
                    if ( this.ConnectorView is object )
                    {
                        this.ConnectorView.Dispose();
                        this.ConnectorView = null;
                    }

                    if ( this.TopHeader is object )
                    {
                        this.TopHeader.Dispose();
                        this.TopHeader = null;
                    }

                    if ( this.FirmwareLoaderView is object )
                    {
                        this.FirmwareLoaderView.Dispose();
                        this.FirmwareLoaderView = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " TSP RIG DEVICE "

        /// <summary> Gets or sets reference to the TSP device accessing the TTM Driver. </summary>
        /// <value> The tsp device. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TspDevice TspDevice { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( TspDevice value )
        {
            if ( this.TspDevice is object )
            {
                this.TspDevice.PropertyChanged -= this.TspDevice_PropertyChanged;
                this.AssignTalker( null );
                this.TspDevice = null;
                this.AssignTopHeader( null );
                this.AddFirmwareLoaderNode( null );
                this.AddConnectorNode( null );
            }

            this.TspDevice = value;
            if ( value is object )
            {
                this.AssignTalker( this.TspDevice.Talker );
                this.TspDevice.PropertyChanged += this.TspDevice_PropertyChanged;
                this.AssignTopHeader( new LoaderTopHeader() );
                this.AddProgramInfoNode();
                this.AddFirmwareLoaderNode( new FirmwareLoaderView() );
                this.AddConnectorNode( new Facade.ConnectorView() );
                this.TopHeader.BindVisaSessionBase( value as VisaSessionBase );
                this.ConnectorView.BindVisaSessionBase( value as VisaSessionBase );
                this.FirmwareLoaderView.AssignDevice( value );
                this.AssignScriptManager( this.FirmwareLoaderView.ScriptManager );
                this.HandlePropertyChanged( value, nameof( Rig.TspDevice.Enabled ) );
            }

            this.BindVisaSessionBase( value as VisaSessionBase );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( TspDevice value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Raises the system. component model. property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected void HandlePropertyChanged( TspDevice sender, string propertyName )
        {
            if ( sender is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( Rig.TspDevice.Enabled ):
                        {
                            this.StatusLabel.Text = sender.Enabled ? $"Ready to Connect to Instrument at {sender.CandidateResourceName}" : "Emulation Mode: Ready to 'Measure'";
                            // the smu unit name seems unnecessary for loading the script.
                            this.TopHeader.SourceMeasureUnitName = "smua";
                            // not clear if this was set in the legacy code.
                            this.TopHeader.LastLoadDate = "2019-04-01";
                            break;
                        }
                }
            }
        }

        /// <summary> Event handler. Called by _MasterDevice for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TspDevice_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling {typeof( TspDevice )}.{e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TspDevice_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TspDevice, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE EVENTS "

        /// <summary>
        /// Event handler. Called upon device opening so as to instantiated all subsystems.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected override void DeviceOpening( object sender, CancelEventArgs e )
        {
            base.DeviceOpening( sender, e );
            _ = this.PublishVerbose( $"Opening access to {this.ResourceName};. " );
        }

        /// <summary>
        /// Event handler. Called after the device opened and all subsystems were defined.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" /> instance of this
        ///                       <see cref="T:System.Windows.Forms.Control" /> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void DeviceOpened( object sender, EventArgs e )
        {
            base.DeviceOpened( sender, e );
            string activity = string.Empty;
            try
            {
                activity = "displaying top header resource name";
                _ = this.PublishVerbose( $"{activity};. " );
                // display the resource name
                this.TopHeader.ResourceName = this.ResourceName;
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " INITALIZING / INITIALIZED  "

        /// <summary> Device initializing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        protected override void DeviceInitializing( object sender, CancelEventArgs e )
        {
            base.DeviceInitializing( sender, e );
        }

        /// <summary> Device initialized. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        protected override void DeviceInitialized( object sender, EventArgs e )
        {
            base.DeviceInitialized( sender, e );
        }

        #endregion

        #region " CLOSING / CLOSED "

        /// <summary> Event handler. Called when device is closing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" /> instance of this
        ///                                             <see cref="T:System.Windows.Forms.Control" /> 
        /// </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void DeviceClosing( object sender, CancelEventArgs e )
        {
            base.DeviceClosing( sender, e );
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        /// <summary> Event handler. Called when device is closed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" /> instance of this
        ///                                          <see cref="T:System.Windows.Forms.Control" /> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void DeviceClosed( object sender, EventArgs e )
        {
            base.DeviceClosed( sender, e );
            if ( this.VisaSessionBase is object )
            {
                string activity = string.Empty;
                try
                {
                }
                catch ( Exception ex )
                {
                    _ = this.PublishException( activity, ex );
                }
            }
        }

        #endregion

        #region " SCRIPT MANAGER "

        /// <summary> Gets or sets reference to the TSP Script Manager. </summary>
        /// <value> The tsp Script Manager. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public TtmScriptManager ScriptManager { get; private set; }

        /// <summary> Assigns the Script Manager and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignScriptManagerThis( TtmScriptManager value )
        {
            if ( this.ScriptManager is object )
            {
                this.ScriptManager.PropertyChanged -= this.ScriptManager_PropertyChanged;
                this.AssignTalker( null );
                this.ScriptManager = null;
            }

            this.ScriptManager = value;
            if ( value is object )
            {
                this.AssignTalker( this.ScriptManager.Talker );
                this.ScriptManager.PropertyChanged += this.ScriptManager_PropertyChanged;
            }
        }

        /// <summary> Assigns a ScriptManager. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignScriptManager( TtmScriptManager value )
        {
            this.AssignScriptManagerThis( value );
        }

        /// <summary> Raises the system. component model. property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( ScriptManager sender, PropertyChangedEventArgs e )
        {
            if ( sender is object && e is object && !string.IsNullOrWhiteSpace( e.PropertyName ) )
            {
                switch ( e.PropertyName ?? "" )
#pragma warning disable CS1522 // Empty switch block
                {
#pragma warning restore CS1522 // Empty switch block
                }
            }
        }

        /// <summary> Event handler. Called by _ScriptManager for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ScriptManager_PropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling {typeof( ScriptManager )}.{e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ScriptManager_PropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ScriptManager, e );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TOP HEADER "

        /// <summary> Gets or sets the top header. </summary>
        /// <value> The top header. </value>
        private LoaderTopHeader TopHeader { get; set; }

        /// <summary> Assign top header. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="topHeader"> The top header. </param>
        private void AssignTopHeader( LoaderTopHeader topHeader )
        {
            if ( this.TopHeader is object )
            {
                _ = this.RemoveBinding( this.TopHeader, nameof( LoaderTopHeader.IsCheckAlerts ), this.TraceMessagesBox, nameof( Core.Forma.TraceMessagesBox.AlertsAnnounced ) );
                _ = this.RemoveBinding( this.TopHeader, nameof( LoaderTopHeader.ResourceName ), this, nameof( this.ResourceName ) );
            }

            this.TopHeader = topHeader;
            if ( topHeader is object )
            {
                var binding = this.AddBinding( this.TopHeader, nameof( LoaderTopHeader.IsCheckAlerts ), this.TraceMessagesBox, nameof( Core.Forma.TraceMessagesBox.AlertsAnnounced ) );
                binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
                _ = this.AddBinding( this.TopHeader, nameof( LoaderTopHeader.ResourceName ), this, nameof( this.ResourceName ) );
                this.AddHeader( topHeader );
            }
        }

        #endregion

        #region " CONNECTOR "

        /// <summary> Gets or sets the connector view. </summary>
        /// <value> The connector view. </value>
        private Facade.ConnectorView ConnectorView { get; set; }

        /// <summary> Name of the connector node. </summary>
        private string _ConnectorNodeName = "Connect";

        /// <summary> Gets or sets the name of the connector node. </summary>
        /// <value> The name of the connector node. </value>
        public string ConnectorNodeName
        {
            get => this._ConnectorNodeName;

            set {
                if ( !string.Equals( this.ConnectorNodeName, value ) )
                {
                    this._ConnectorNodeName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Adds a connector node. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="connector"> The connector. </param>
        private void AddConnectorNode( Facade.ConnectorView connector )
        {
            if ( this.ConnectorView is object )
            {
                this.RemoveNode( this.ConnectorNodeName );
            }

            this.ConnectorView = connector;
            if ( this.ConnectorView is object )
            {
                _ = this.AddNode( this.ConnectorNodeName, this.ConnectorNodeName, this.ConnectorView );
                this.SelectNavigatorTreeViewNode( this.ConnectorNodeName );
            }
        }

        #endregion

        #region " FIRMWARE LOADER "

        /// <summary> Gets or sets the firmware loader view. </summary>
        /// <value> The firmware loader view. </value>
        private FirmwareLoaderView FirmwareLoaderView { get; set; }

        /// <summary> Name of the firmware loader node. </summary>
        private string _FirmwareLoaderNodeName = "Firmware";

        /// <summary> Gets or sets the name of the FirmwareLoader node. </summary>
        /// <value> The name of the FirmwareLoader node. </value>
        public string FirmwareLoaderNodeName
        {
            get => this._FirmwareLoaderNodeName;

            set {
                if ( !string.Equals( this.FirmwareLoaderNodeName, value ) )
                {
                    this._FirmwareLoaderNodeName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Adds a firmware loader node. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="loaderView"> The loader view. </param>
        private void AddFirmwareLoaderNode( FirmwareLoaderView loaderView )
        {
            if ( this.FirmwareLoaderView is object )
            {
                this.FirmwareLoaderView.PropertyChanged -= this.FirmwareLoaderViewPropertyChanged;
                this.RemoveNode( this.FirmwareLoaderNodeName );
            }

            this.FirmwareLoaderView = loaderView;
            if ( this.FirmwareLoaderView is object )
            {
                this.FirmwareLoaderView.PropertyChanged += this.FirmwareLoaderViewPropertyChanged;
                _ = this.AddNode( this.FirmwareLoaderNodeName, this.FirmwareLoaderNodeName, this.FirmwareLoaderView );
                this.SelectNavigatorTreeViewNode( this.FirmwareLoaderNodeName );
                this.HandlePropertyChanged( loaderView, nameof( this.FirmwareLoaderView.UpdateRequired ) );
                this.HandlePropertyChanged( loaderView, nameof( this.FirmwareLoaderView.VersionCompareStatus ) );
                this.HandlePropertyChanged( loaderView, nameof( this.FirmwareLoaderView.FirmwareInstalledVersion ) );
                this.HandlePropertyChanged( loaderView, nameof( this.FirmwareLoaderView.FirmwareReleasedVersion ) );
            }
        }

        /// <summary> Raises the system. component model. property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( FirmwareLoaderView sender, string propertyName )
        {
            if ( sender is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( Forms.FirmwareLoaderView.UpdateRequired ):
                        {
                            if ( sender.UpdateRequired )
                                _ = this.PublishInfo( "Updated required;. " );
                            break;
                        }

                    case nameof( Forms.FirmwareLoaderView.VersionCompareStatus ):
                        {
                            this.StatusLabel.Text = sender.VersionCompareStatus.Description();
                            break;
                        }

                    case nameof( Forms.FirmwareLoaderView.FirmwareInstalledVersion ):
                        {
                            this.TopHeader.InstalledFirmwareVersion = sender.FirmwareInstalledVersion;
                            break;
                        }

                    case nameof( Forms.FirmwareLoaderView.FirmwareReleasedVersion ):
                        {
                            this.TopHeader.ReleasedFirmwareVersion = sender.FirmwareReleasedVersion;
                            break;
                        }
                }
            }
        }

        /// <summary> Event handler. Called by _FirmwareLoader for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FirmwareLoaderViewPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Handling {nameof( Forms.FirmwareLoaderView )}.{e?.PropertyName} change";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.FirmwareLoaderViewPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as FirmwareLoaderView, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " PROGRAM INFO "

        /// <summary> Adds program information node. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AddProgramInfoNode()
        {
            this.AboutProgramRichTextBox = new RichTextBox();
            _ = this.AddNode( this.ProgramInfoNodeName, this.ProgramInfoNodeName, this.AboutProgramRichTextBox );
            this.DisplayProgramInfo( this.Font );
            this.SelectNavigatorTreeViewNode( this.ProgramInfoNodeName );
        }

        /// <summary> Name of the program information node. </summary>
        private string _ProgramInfoNodeName = "About";

        /// <summary> Gets or sets the name of the program information node. </summary>
        /// <value> The name of the program information node. </value>
        public string ProgramInfoNodeName
        {
            get => this._ProgramInfoNodeName;

            set {
                if ( !string.Equals( this.ProgramInfoNodeName, value ) )
                {
                    this._ProgramInfoNodeName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the about program rich text box. </summary>
        /// <value> The about program rich text box. </value>
        private RichTextBox AboutProgramRichTextBox { get; set; }

        private Core.ProgramInfo _ProgramInfo;

        private Core.ProgramInfo ProgramInfo
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ProgramInfo;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ProgramInfo != null )
                {
                    this._ProgramInfo.ProgramInfoRequested -= this.ProgramInfo_ProgramInfoRequested;
                    this._ProgramInfo.RefreshRequested -= this.ProgramInfo_RefreshRequested;
                }

                this._ProgramInfo = value;
                if ( this._ProgramInfo != null )
                {
                    this._ProgramInfo.ProgramInfoRequested += this.ProgramInfo_ProgramInfoRequested;
                    this._ProgramInfo.RefreshRequested += this.ProgramInfo_RefreshRequested;
                }
            }
        }

        /// <summary> Displays a program information described by contentsFont. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="contentsFont"> The contents font. </param>
        private void DisplayProgramInfo( Font contentsFont )
        {
            var headerFont = new Font( contentsFont, FontStyle.Bold );
            this.ProgramInfo.BuildContents( headerFont, contentsFont );
            this.ProgramInfo.NotifyRefreshRequested();
        }

        /// <summary> Refresh program information display. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="box"> The box control. </param>
        private void RefreshProgramInfoDisplay( RichTextBox box )
        {
            box.Rtf = string.Empty;
            foreach ( Core.ProgramInfoLine line in this.ProgramInfo.Lines )
            {
                box.SelectionStart = box.TextLength;
                box.SelectionFont = line.Font;
                box.AppendText( line.Text );
                box.AppendText( Environment.NewLine );
            }
        }

        /// <summary> Appends a program information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="contentsFont"> The contents font. </param>
        private void AppendProgramInfo( Font contentsFont )
        {
            var headerFont = new Font( contentsFont, FontStyle.Bold );
            this.ProgramInfo.AppendLine( "Product Application-Data Folder:", headerFont );
            var fi = new System.IO.FileInfo( new Core.MyAssemblyInfo( My.MyProject.Application.Info ).BuildApplicationConfigFilePath( false, 2 ) );
            this.ProgramInfo.AppendLine( fi.DirectoryName, contentsFont );
            this.ProgramInfo.AppendLine( "", contentsFont );
            string logFile = My.MyLibrary.Logger.DefaultFileLogWriter.FullLogFileName;
            this.ProgramInfo.AppendLine( "Application log file:", headerFont );
            this.ProgramInfo.AppendLine( logFile, contentsFont );
            string traceSourceFile = My.MyLibrary.Logger.DefaultFileLogWriter.FullLogFileName;
            if ( !string.Equals( logFile, traceSourceFile ) )
            {
                this.ProgramInfo.AppendLine( "Application log trace source file:", headerFont );
                this.ProgramInfo.AppendLine( traceSourceFile, contentsFont );
            }

            this.ProgramInfo.AppendLine( string.Empty, contentsFont );
        }

        /// <summary> Program information program information requested. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ProgramInfo_ProgramInfoRequested( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Handling {typeof( Core.ProgramInfo )}.{nameof( this.ProgramInfo.ProgramInfoRequested )} event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, EventArgs>( this.ProgramInfo_ProgramInfoRequested ), new object[] { sender, e } );
                }
                else
                {
                    this.AppendProgramInfo( this.Font );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Program information refresh requested. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ProgramInfo_RefreshRequested( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"Handling {typeof( Core.ProgramInfo )}.{nameof( this.ProgramInfo.RefreshRequested )} event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, EventArgs>( this.ProgramInfo_RefreshRequested ), new object[] { sender, e } );
                }
                else
                {
                    this.RefreshProgramInfoDisplay( this.AboutProgramRichTextBox );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " NAVIGATION "

        /// <summary> After node selected. </summary>
        /// <remarks> David, 2020-09-03. </remarks>
        /// <param name="e"> Tree view event information. </param>
        protected override void AfterNodeSelected( TreeViewEventArgs e )
        {
            base.AfterNodeSelected( e );
            switch ( e.Node.Name ?? "" )
            {
                case var @case when @case == (this.ProgramInfoNodeName ?? ""):
                    {
                        this.ProgramInfo.NotifyRefreshRequested();
                        break;
                    }
            }
        }

        #endregion

        #region " SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "TSP Rig UI Settings Editor", My.MySettings.Default );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
