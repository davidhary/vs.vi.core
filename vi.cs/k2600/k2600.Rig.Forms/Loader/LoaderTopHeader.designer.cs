using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig.Forms
{
    [DesignerGenerated()]
    [ToolboxBitmap(typeof(LoaderTopHeader), "TopHeader.ToolboxBitmap")]
    public partial class LoaderTopHeader
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _SourceMeasureUnitTextBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.PictureBox __FailedPicture;

        private System.Windows.Forms.PictureBox _FailedPicture
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FailedPicture;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FailedPicture != null)
                {
                    __FailedPicture.DoubleClick -= FailedPicture_DoubleClick;
                }

                __FailedPicture = value;
                if (__FailedPicture != null)
                {
                    __FailedPicture.DoubleClick += FailedPicture_DoubleClick;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.PictureBox __PassedPicture;

        private System.Windows.Forms.PictureBox _PassedPicture
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PassedPicture;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PassedPicture != null)
                {
                    __PassedPicture.DoubleClick -= PassedPicture_DoubleClick;
                }

                __PassedPicture = value;
                if (__PassedPicture != null)
                {
                    __PassedPicture.DoubleClick += PassedPicture_DoubleClick;
                }
            }
        }

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _LastLoadDateTextBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _InstalledFirmwareVersionTextBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _ReleasedFirmwareVersionTextBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _SourceMeasureUnitTextBoxLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _TitleLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _InstalledFirmwareVersionTextBoxLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _LastLoadDateTextBoxLabel;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ReleasedFirmwareVersionTextBoxLabel;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(LoaderTopHeader));
            _SourceMeasureUnitTextBox = new System.Windows.Forms.TextBox();
            _LastLoadDateTextBox = new System.Windows.Forms.TextBox();
            _InstalledFirmwareVersionTextBox = new System.Windows.Forms.TextBox();
            _ReleasedFirmwareVersionTextBox = new System.Windows.Forms.TextBox();
            _SourceMeasureUnitTextBoxLabel = new System.Windows.Forms.Label();
            _TitleLabel = new System.Windows.Forms.Label();
            _InstalledFirmwareVersionTextBoxLabel = new System.Windows.Forms.Label();
            _LastLoadDateTextBoxLabel = new System.Windows.Forms.Label();
            _ReleasedFirmwareVersionTextBoxLabel = new System.Windows.Forms.Label();
            _DataLayout = new System.Windows.Forms.TableLayoutPanel();
            _TopLayout = new System.Windows.Forms.TableLayoutPanel();
            _ResourceTextBoxLabel = new System.Windows.Forms.Label();
            _ResourceTextBox = new System.Windows.Forms.TextBox();
            _LeftPanel = new System.Windows.Forms.Panel();
            _SettingsToolStrip = new System.Windows.Forms.ToolStrip();
            __FailedPicture = new System.Windows.Forms.PictureBox();
            __FailedPicture.DoubleClick += new EventHandler(FailedPicture_DoubleClick);
            __PassedPicture = new System.Windows.Forms.PictureBox();
            __PassedPicture.DoubleClick += new EventHandler(PassedPicture_DoubleClick);
            __OpenAboutBoxButton = new System.Windows.Forms.ToolStripButton();
            __OpenAboutBoxButton.Click += new EventHandler(OpenAboutBoxButton_Click);
            __OpenDeviceSettingsButton = new System.Windows.Forms.ToolStripButton();
            __OpenDeviceSettingsButton.Click += new EventHandler(OpenDeviceSettingsButton_Click);
            __OpenUserInterfaceSettingsButton = new System.Windows.Forms.ToolStripButton();
            __OpenUserInterfaceSettingsButton.Click += new EventHandler(OpenUserInterfaceSettingsButton_Click);
            _DataLayout.SuspendLayout();
            _TopLayout.SuspendLayout();
            _LeftPanel.SuspendLayout();
            _SettingsToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__FailedPicture).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__PassedPicture).BeginInit();
            SuspendLayout();
            // 
            // _SourceMeasureUnitTextBox
            // 
            _SourceMeasureUnitTextBox.AcceptsReturn = true;
            _SourceMeasureUnitTextBox.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            _SourceMeasureUnitTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _SourceMeasureUnitTextBox.CausesValidation = false;
            _SourceMeasureUnitTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _SourceMeasureUnitTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _SourceMeasureUnitTextBox.Font = new Font("Segoe UI", 18.0f);
            _SourceMeasureUnitTextBox.ForeColor = Color.White;
            _SourceMeasureUnitTextBox.Location = new Point(0, 20);
            _SourceMeasureUnitTextBox.Margin = new System.Windows.Forms.Padding(0);
            _SourceMeasureUnitTextBox.MaxLength = 0;
            _SourceMeasureUnitTextBox.Name = "_SourceMeasureUnitTextBox";
            _SourceMeasureUnitTextBox.ReadOnly = true;
            _SourceMeasureUnitTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _SourceMeasureUnitTextBox.Size = new Size(169, 32);
            _SourceMeasureUnitTextBox.TabIndex = 5;
            _SourceMeasureUnitTextBox.TabStop = false;
            _SourceMeasureUnitTextBox.Text = "1";
            _SourceMeasureUnitTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_SourceMeasureUnitTextBox, "Source measure unit");
            // 
            // _LastLoadDateTextBox
            // 
            _LastLoadDateTextBox.AcceptsReturn = true;
            _LastLoadDateTextBox.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            _LastLoadDateTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _LastLoadDateTextBox.CausesValidation = false;
            _LastLoadDateTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _LastLoadDateTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _LastLoadDateTextBox.Font = new Font("Segoe UI", 18.0f);
            _LastLoadDateTextBox.ForeColor = Color.White;
            _LastLoadDateTextBox.Location = new Point(507, 20);
            _LastLoadDateTextBox.Margin = new System.Windows.Forms.Padding(0);
            _LastLoadDateTextBox.MaxLength = 0;
            _LastLoadDateTextBox.Name = "_LastLoadDateTextBox";
            _LastLoadDateTextBox.ReadOnly = true;
            _LastLoadDateTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _LastLoadDateTextBox.Size = new Size(170, 32);
            _LastLoadDateTextBox.TabIndex = 11;
            _LastLoadDateTextBox.TabStop = false;
            _LastLoadDateTextBox.Text = "0000-00-00";
            _LastLoadDateTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_LastLoadDateTextBox, "Last load date");
            // 
            // _InstalledFirmwareVersionTextBox
            // 
            _InstalledFirmwareVersionTextBox.AcceptsReturn = true;
            _InstalledFirmwareVersionTextBox.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            _InstalledFirmwareVersionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _InstalledFirmwareVersionTextBox.CausesValidation = false;
            _InstalledFirmwareVersionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _InstalledFirmwareVersionTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _InstalledFirmwareVersionTextBox.Font = new Font("Segoe UI", 18.0f);
            _InstalledFirmwareVersionTextBox.ForeColor = Color.White;
            _InstalledFirmwareVersionTextBox.Location = new Point(338, 20);
            _InstalledFirmwareVersionTextBox.Margin = new System.Windows.Forms.Padding(0);
            _InstalledFirmwareVersionTextBox.MaxLength = 0;
            _InstalledFirmwareVersionTextBox.Name = "_InstalledFirmwareVersionTextBox";
            _InstalledFirmwareVersionTextBox.ReadOnly = true;
            _InstalledFirmwareVersionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _InstalledFirmwareVersionTextBox.Size = new Size(169, 32);
            _InstalledFirmwareVersionTextBox.TabIndex = 9;
            _InstalledFirmwareVersionTextBox.TabStop = false;
            _InstalledFirmwareVersionTextBox.Text = "0.0.0000";
            _InstalledFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_InstalledFirmwareVersionTextBox, "Installed version");
            // 
            // _ReleasedFirmwareVersionTextBox
            // 
            _ReleasedFirmwareVersionTextBox.AcceptsReturn = true;
            _ReleasedFirmwareVersionTextBox.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            _ReleasedFirmwareVersionTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _ReleasedFirmwareVersionTextBox.CausesValidation = false;
            _ReleasedFirmwareVersionTextBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            _ReleasedFirmwareVersionTextBox.Dock = System.Windows.Forms.DockStyle.Top;
            _ReleasedFirmwareVersionTextBox.Font = new Font("Segoe UI", 18.0f);
            _ReleasedFirmwareVersionTextBox.ForeColor = Color.White;
            _ReleasedFirmwareVersionTextBox.Location = new Point(169, 20);
            _ReleasedFirmwareVersionTextBox.Margin = new System.Windows.Forms.Padding(0);
            _ReleasedFirmwareVersionTextBox.MaxLength = 0;
            _ReleasedFirmwareVersionTextBox.Name = "_ReleasedFirmwareVersionTextBox";
            _ReleasedFirmwareVersionTextBox.ReadOnly = true;
            _ReleasedFirmwareVersionTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ReleasedFirmwareVersionTextBox.Size = new Size(169, 32);
            _ReleasedFirmwareVersionTextBox.TabIndex = 7;
            _ReleasedFirmwareVersionTextBox.TabStop = false;
            _ReleasedFirmwareVersionTextBox.Text = "0.0.4000";
            _ReleasedFirmwareVersionTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            ToolTip.SetToolTip(_ReleasedFirmwareVersionTextBox, "Released Firmware Version");
            // 
            // _SourceMeasureUnitTextBoxLabel
            // 
            _SourceMeasureUnitTextBoxLabel.BackColor = Color.Transparent;
            _SourceMeasureUnitTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _SourceMeasureUnitTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _SourceMeasureUnitTextBoxLabel.ForeColor = Color.White;
            _SourceMeasureUnitTextBoxLabel.Location = new Point(0, 5);
            _SourceMeasureUnitTextBoxLabel.Margin = new System.Windows.Forms.Padding(0);
            _SourceMeasureUnitTextBoxLabel.Name = "_SourceMeasureUnitTextBoxLabel";
            _SourceMeasureUnitTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _SourceMeasureUnitTextBoxLabel.Size = new Size(169, 15);
            _SourceMeasureUnitTextBoxLabel.TabIndex = 4;
            _SourceMeasureUnitTextBoxLabel.Text = "SMU";
            _SourceMeasureUnitTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _TitleLabel
            // 
            _TitleLabel.AutoSize = true;
            _TitleLabel.BackColor = Color.Transparent;
            _TitleLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _TitleLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _TitleLabel.Font = new Font("Segoe UI", 18.0f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _TitleLabel.ForeColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(249)), Conversions.ToInteger(Conversions.ToByte(172)), Conversions.ToInteger(Conversions.ToByte(60)));
            _TitleLabel.Location = new Point(0, 0);
            _TitleLabel.Margin = new System.Windows.Forms.Padding(0);
            _TitleLabel.Name = "_TitleLabel";
            _TitleLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _TitleLabel.Size = new Size(611, 35);
            _TitleLabel.TabIndex = 1;
            _TitleLabel.Text = "Thermal Transient Meter - Firmware Loader";
            _TitleLabel.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // _InstalledFirmwareVersionTextBoxLabel
            // 
            _InstalledFirmwareVersionTextBoxLabel.BackColor = Color.Transparent;
            _InstalledFirmwareVersionTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _InstalledFirmwareVersionTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _InstalledFirmwareVersionTextBoxLabel.ForeColor = Color.White;
            _InstalledFirmwareVersionTextBoxLabel.Location = new Point(338, 5);
            _InstalledFirmwareVersionTextBoxLabel.Margin = new System.Windows.Forms.Padding(0);
            _InstalledFirmwareVersionTextBoxLabel.Name = "_InstalledFirmwareVersionTextBoxLabel";
            _InstalledFirmwareVersionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _InstalledFirmwareVersionTextBoxLabel.Size = new Size(169, 15);
            _InstalledFirmwareVersionTextBoxLabel.TabIndex = 8;
            _InstalledFirmwareVersionTextBoxLabel.Text = "FIRMWARE";
            _InstalledFirmwareVersionTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _LastLoadDateTextBoxLabel
            // 
            _LastLoadDateTextBoxLabel.BackColor = Color.Transparent;
            _LastLoadDateTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _LastLoadDateTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _LastLoadDateTextBoxLabel.ForeColor = Color.White;
            _LastLoadDateTextBoxLabel.Location = new Point(507, 5);
            _LastLoadDateTextBoxLabel.Margin = new System.Windows.Forms.Padding(0);
            _LastLoadDateTextBoxLabel.Name = "_LastLoadDateTextBoxLabel";
            _LastLoadDateTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _LastLoadDateTextBoxLabel.Size = new Size(170, 15);
            _LastLoadDateTextBoxLabel.TabIndex = 10;
            _LastLoadDateTextBoxLabel.Text = "LAST LOADED";
            _LastLoadDateTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _ReleasedFirmwareVersionTextBoxLabel
            // 
            _ReleasedFirmwareVersionTextBoxLabel.BackColor = Color.Transparent;
            _ReleasedFirmwareVersionTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default;
            _ReleasedFirmwareVersionTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom;
            _ReleasedFirmwareVersionTextBoxLabel.ForeColor = Color.White;
            _ReleasedFirmwareVersionTextBoxLabel.Location = new Point(169, 5);
            _ReleasedFirmwareVersionTextBoxLabel.Margin = new System.Windows.Forms.Padding(0);
            _ReleasedFirmwareVersionTextBoxLabel.Name = "_ReleasedFirmwareVersionTextBoxLabel";
            _ReleasedFirmwareVersionTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _ReleasedFirmwareVersionTextBoxLabel.Size = new Size(169, 15);
            _ReleasedFirmwareVersionTextBoxLabel.TabIndex = 6;
            _ReleasedFirmwareVersionTextBoxLabel.Text = "S/W Version";
            _ReleasedFirmwareVersionTextBoxLabel.TextAlign = ContentAlignment.BottomCenter;
            // 
            // _DataLayout
            // 
            _DataLayout.BackColor = Color.Transparent;
            _DataLayout.ColumnCount = 4;
            _DataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _DataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _DataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _DataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _DataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _DataLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _DataLayout.Controls.Add(_SourceMeasureUnitTextBox, 0, 1);
            _DataLayout.Controls.Add(_SourceMeasureUnitTextBoxLabel, 0, 0);
            _DataLayout.Controls.Add(_ReleasedFirmwareVersionTextBoxLabel, 1, 0);
            _DataLayout.Controls.Add(_ReleasedFirmwareVersionTextBox, 1, 1);
            _DataLayout.Controls.Add(_InstalledFirmwareVersionTextBoxLabel, 2, 0);
            _DataLayout.Controls.Add(_InstalledFirmwareVersionTextBox, 2, 1);
            _DataLayout.Controls.Add(_LastLoadDateTextBox, 3, 1);
            _DataLayout.Controls.Add(_LastLoadDateTextBoxLabel, 3, 0);
            _DataLayout.Dock = System.Windows.Forms.DockStyle.Bottom;
            _DataLayout.Location = new Point(31, 62);
            _DataLayout.Name = "_DataLayout";
            _DataLayout.RowCount = 2;
            _DataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _DataLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _DataLayout.Size = new Size(677, 52);
            _DataLayout.TabIndex = 18;
            // 
            // _TopLayout
            // 
            _TopLayout.ColumnCount = 5;
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TopLayout.Controls.Add(_TitleLabel, 0, 0);
            _TopLayout.Controls.Add(__FailedPicture, 4, 0);
            _TopLayout.Controls.Add(__PassedPicture, 3, 0);
            _TopLayout.Dock = System.Windows.Forms.DockStyle.Top;
            _TopLayout.Location = new Point(31, 0);
            _TopLayout.Name = "_TopLayout";
            _TopLayout.RowCount = 1;
            _TopLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TopLayout.Size = new Size(677, 35);
            _TopLayout.TabIndex = 19;
            // 
            // _ResourceTextBoxLabel
            // 
            _ResourceTextBoxLabel.AutoSize = true;
            _ResourceTextBoxLabel.ForeColor = Color.White;
            _ResourceTextBoxLabel.Location = new Point(97, 40);
            _ResourceTextBoxLabel.Name = "_ResourceTextBoxLabel";
            _ResourceTextBoxLabel.Size = new Size(108, 17);
            _ResourceTextBoxLabel.TabIndex = 20;
            _ResourceTextBoxLabel.Text = "Resource Name: ";
            _ResourceTextBoxLabel.TextAlign = ContentAlignment.TopRight;
            // 
            // _ResourceTextBox
            // 
            _ResourceTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _ResourceTextBox.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            _ResourceTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            _ResourceTextBox.Font = new Font("Segoe UI", 9.75f, FontStyle.Bold, GraphicsUnit.Point, Conversions.ToByte(0));
            _ResourceTextBox.ForeColor = Color.White;
            _ResourceTextBox.Location = new Point(202, 39);
            _ResourceTextBox.Name = "_ResourceTextBox";
            _ResourceTextBox.Size = new Size(503, 18);
            _ResourceTextBox.TabIndex = 21;
            // 
            // _LeftPanel
            // 
            _LeftPanel.Controls.Add(_SettingsToolStrip);
            _LeftPanel.Dock = System.Windows.Forms.DockStyle.Left;
            _LeftPanel.Location = new Point(0, 0);
            _LeftPanel.Name = "_LeftPanel";
            _LeftPanel.Size = new Size(31, 114);
            _LeftPanel.TabIndex = 22;
            // 
            // _SettingsToolStrip
            // 
            _SettingsToolStrip.BackColor = Color.Transparent;
            _SettingsToolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            _SettingsToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _SettingsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _SettingsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __OpenAboutBoxButton, __OpenDeviceSettingsButton, __OpenUserInterfaceSettingsButton });
            _SettingsToolStrip.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            _SettingsToolStrip.Location = new Point(0, 0);
            _SettingsToolStrip.Name = "_SettingsToolStrip";
            _SettingsToolStrip.Size = new Size(32, 114);
            _SettingsToolStrip.TabIndex = 2;
            _SettingsToolStrip.Text = "ToolStrip1";
            // 
            // _FailedPicture
            // 
            __FailedPicture.BackColor = Color.Transparent;
            __FailedPicture.Cursor = System.Windows.Forms.Cursors.Default;
            __FailedPicture.ForeColor = SystemColors.WindowText;
            __FailedPicture.Image = (Image)resources.GetObject("_FailedPicture.Image");
            __FailedPicture.Location = new Point(647, 5);
            __FailedPicture.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            __FailedPicture.Name = "__FailedPicture";
            __FailedPicture.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __FailedPicture.Size = new Size(27, 26);
            __FailedPicture.TabIndex = 3;
            __FailedPicture.TabStop = false;
            ToolTip.SetToolTip(__FailedPicture, "FAILED");
            // 
            // _PassedPicture
            // 
            __PassedPicture.BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            __PassedPicture.Cursor = System.Windows.Forms.Cursors.Default;
            __PassedPicture.ForeColor = SystemColors.WindowText;
            __PassedPicture.Image = (Image)resources.GetObject("_PassedPicture.Image");
            __PassedPicture.Location = new Point(614, 5);
            __PassedPicture.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            __PassedPicture.Name = "__PassedPicture";
            __PassedPicture.RightToLeft = System.Windows.Forms.RightToLeft.No;
            __PassedPicture.Size = new Size(27, 25);
            __PassedPicture.TabIndex = 2;
            __PassedPicture.TabStop = false;
            ToolTip.SetToolTip(__PassedPicture, "PASSED");
            // 
            // _OpenAboutBoxButton
            // 
            __OpenAboutBoxButton.AutoSize = false;
            __OpenAboutBoxButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __OpenAboutBoxButton.Image = My.Resources.Resources.logo48;
            __OpenAboutBoxButton.ImageTransparentColor = Color.Magenta;
            __OpenAboutBoxButton.Name = "__OpenAboutBoxButton";
            __OpenAboutBoxButton.Size = new Size(29, 29);
            __OpenAboutBoxButton.ToolTipText = "Open program about dialog";
            // 
            // _OpenDeviceSettingsButton
            // 
            __OpenDeviceSettingsButton.AutoSize = false;
            __OpenDeviceSettingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __OpenDeviceSettingsButton.Image = (Image)resources.GetObject("_OpenDeviceSettingsButton.Image");
            __OpenDeviceSettingsButton.ImageTransparentColor = Color.Magenta;
            __OpenDeviceSettingsButton.Name = "__OpenDeviceSettingsButton";
            __OpenDeviceSettingsButton.Size = new Size(29, 29);
            __OpenDeviceSettingsButton.ToolTipText = "Open device settings";
            // 
            // _OpenUserInterfaceSettingsButton
            // 
            __OpenUserInterfaceSettingsButton.AutoSize = false;
            __OpenUserInterfaceSettingsButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            __OpenUserInterfaceSettingsButton.Image = (Image)resources.GetObject("_OpenUserInterfaceSettingsButton.Image");
            __OpenUserInterfaceSettingsButton.ImageTransparentColor = Color.Magenta;
            __OpenUserInterfaceSettingsButton.Name = "__OpenUserInterfaceSettingsButton";
            __OpenUserInterfaceSettingsButton.Size = new Size(29, 29);
            __OpenUserInterfaceSettingsButton.ToolTipText = "Open user interface settings";
            // 
            // LoaderTopHeader
            // 
            BackColor = Color.FromArgb(Conversions.ToInteger(Conversions.ToByte(0)), Conversions.ToInteger(Conversions.ToByte(173)), Conversions.ToInteger(Conversions.ToByte(239)));
            Controls.Add(_ResourceTextBox);
            Controls.Add(_ResourceTextBoxLabel);
            Controls.Add(_TopLayout);
            Controls.Add(_DataLayout);
            Controls.Add(_LeftPanel);
            Name = "LoaderTopHeader";
            Size = new Size(708, 114);
            _DataLayout.ResumeLayout(false);
            _DataLayout.PerformLayout();
            _TopLayout.ResumeLayout(false);
            _TopLayout.PerformLayout();
            _LeftPanel.ResumeLayout(false);
            _LeftPanel.PerformLayout();
            _SettingsToolStrip.ResumeLayout(false);
            _SettingsToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__FailedPicture).EndInit();
            ((System.ComponentModel.ISupportInitialize)__PassedPicture).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TableLayoutPanel _DataLayout;
        private System.Windows.Forms.TableLayoutPanel _TopLayout;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.TextBox _ResourceTextBox;

        /// <summary> The with events control. </summary>
        private System.Windows.Forms.Label _ResourceTextBoxLabel;
        private System.Windows.Forms.Panel _LeftPanel;
        private System.Windows.Forms.ToolStrip _SettingsToolStrip;
        private System.Windows.Forms.ToolStripButton __OpenAboutBoxButton;

        private System.Windows.Forms.ToolStripButton _OpenAboutBoxButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenAboutBoxButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenAboutBoxButton != null)
                {
                    __OpenAboutBoxButton.Click -= OpenAboutBoxButton_Click;
                }

                __OpenAboutBoxButton = value;
                if (__OpenAboutBoxButton != null)
                {
                    __OpenAboutBoxButton.Click += OpenAboutBoxButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __OpenDeviceSettingsButton;

        private System.Windows.Forms.ToolStripButton _OpenDeviceSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDeviceSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDeviceSettingsButton != null)
                {
                    __OpenDeviceSettingsButton.Click -= OpenDeviceSettingsButton_Click;
                }

                __OpenDeviceSettingsButton = value;
                if (__OpenDeviceSettingsButton != null)
                {
                    __OpenDeviceSettingsButton.Click += OpenDeviceSettingsButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __OpenUserInterfaceSettingsButton;

        private System.Windows.Forms.ToolStripButton _OpenUserInterfaceSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenUserInterfaceSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenUserInterfaceSettingsButton != null)
                {
                    __OpenUserInterfaceSettingsButton.Click -= OpenUserInterfaceSettingsButton_Click;
                }

                __OpenUserInterfaceSettingsButton = value;
                if (__OpenUserInterfaceSettingsButton != null)
                {
                    __OpenUserInterfaceSettingsButton.Click += OpenUserInterfaceSettingsButton_Click;
                }
            }
        }
    }
}
