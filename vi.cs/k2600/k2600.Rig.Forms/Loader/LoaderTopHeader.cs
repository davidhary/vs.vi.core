using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Rig.Forms
{

    /// <summary> Displays current test results. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2006-06-13, 1.15.2355.x. </para>
    /// </remarks>
    public partial class LoaderTopHeader : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public LoaderTopHeader() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._PassedPicture.Visible = false;
            this._PassedPicture.Invalidate();

            // Add any initialization after the InitializeComponent() call
            // onInstantiate()
            this.ClearDisplayThis();
            this.InitializingComponents = false;
            this.__FailedPicture.Name = "_FailedPicture";
            this.__PassedPicture.Name = "_PassedPicture";
            this.__OpenAboutBoxButton.Name = "_OpenAboutBoxButton";
            this.__OpenDeviceSettingsButton.Name = "_OpenDeviceSettingsButton";
            this.__OpenUserInterfaceSettingsButton.Name = "_OpenUserInterfaceSettingsButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2021-09-08. </remarks>
        /// <param name="disposing">    true to release both managed and unmanaged resources; false to
        ///                             release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " I RESETTABLE IMPLEMENTATION "

        /// <summary> Clears the display. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ClearDisplay()
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action( this.ClearDisplayThis ) );
            }
            else
            {
                this.ClearDisplayThis();
            }
        }

        /// <summary> Set internal value to match the display state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ClearDisplayThis()
        {
            this.InstalledFirmwareVersion = string.Empty;
            this.IsCheckAlerts = false;
            this.ResourceName = string.Empty;
            this.ReleasedFirmwareVersion = string.Empty;
            this.SourceMeasureUnitName = string.Empty;
            this.LastLoadDate = string.Empty;
        }

        #endregion

        #region " visa session base (device base) "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Bind visa session base. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void BindVisaSessionBaseThis( VisaSessionBase value )
        {
            if ( this.VisaSessionBase is object )
            {
                this.VisaSessionBase.SessionFactory.PropertyChanged -= this.SessionFactoryPropertyChanged;
                this.AssignTalker( null );
                this.VisaSessionBase = null;
            }

            this.VisaSessionBase = value;
            if ( value is object )
            {
                this.AssignTalker( this.VisaSessionBase.Talker );
                this.VisaSessionBase.SessionFactory.PropertyChanged += this.SessionFactoryPropertyChanged;
                // Me.VisaSessionBase.SessionFactory.CandidateResourceName = isr.VI.Tsp.K2600.Ttm.My.Settings.ResourceName
                this.HandlePropertyChanged( this.VisaSessionBase.SessionFactory, nameof( this.VisaSessionBase.SessionFactory.IsOpen ) );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public virtual void BindVisaSessionBase( VisaSessionBase value )
        {
            this.BindVisaSessionBaseThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.VisaSessionBase.ResourceNameCaption} reading service request";
            try
            {
                _ = this.VisaSessionBase.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #region " SESSION FACTORY "

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Specifies the object where the call originated. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SessionFactory sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SessionFactory.ValidatedResourceName ):
                    {
                        this.ResourceName = sender.IsOpen ? sender.ValidatedResourceName : string.Empty;
                        break;
                    }

                case nameof( SessionFactory.IsOpen ):
                    {
                        this.ResourceName = sender.IsOpen ? sender.ValidatedResourceName : string.Empty;
                        break;
                    }
            }
        }

        /// <summary> Session factory property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SessionFactoryPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"handling session factory {e?.PropertyName} property changed event";
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SessionFactoryPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SessionFactory, e?.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #endregion

        #region " DISPLAY PROPERTIES "

        /// <summary> Displays the resource name. </summary>
        /// <value> The Last Load Date. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string ResourceName
        {
            get => this._ResourceTextBox.Text;

            set {
                if ( !string.Equals( this.ResourceName, value ) )
                {
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._ResourceTextBox.Text = value; } );
                    }
                    else
                    {
                        this._ResourceTextBox.Text = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Displays the Last Load Date. </summary>
        /// <value> The Last Load Date. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string LastLoadDate
        {
            get => this._LastLoadDateTextBox.Text;

            set {
                if ( !string.Equals( this.LastLoadDate, value ) )
                {
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._LastLoadDateTextBox.Text = value; } );
                    }
                    else
                    {
                        this._LastLoadDateTextBox.Text = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Displays the Installed Firmware Version. </summary>
        /// <value> The initial resistance caption. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string InstalledFirmwareVersion
        {
            get => this._InstalledFirmwareVersionTextBox.Text;

            set {
                if ( !string.Equals( this.InstalledFirmwareVersion, value ) )
                {
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._InstalledFirmwareVersionTextBox.Text = value; } );
                    }
                    else
                    {
                        this._InstalledFirmwareVersionTextBox.Text = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Turns on the alerts message. </summary>
        /// <value> The is check alerts. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public bool IsCheckAlerts
        {
            get => this._FailedPicture.Visible;

            set {
                if ( this.IsCheckAlerts != value )
                {
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._FailedPicture.Visible = value; } );
                    }
                    else
                    {
                        this._FailedPicture.Visible = value;
                        this._FailedPicture.Invalidate();
                    }

                    if ( value && My.MySettings.Default.PlayAlertSound )
                    {
                        My.MyProject.Computer.Audio.PlaySystemSound( System.Media.SystemSounds.Exclamation );
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The released firmware version. </summary>
        private string _ReleasedFirmwareVersion;

        /// <summary> Gets or sets the Released Firmware Version. </summary>
        /// <value> The Released Firmware Version. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string ReleasedFirmwareVersion
        {
            get => this._ReleasedFirmwareVersion;

            set {
                if ( !string.Equals( this.ReleasedFirmwareVersion, value ) )
                {
                    this._ReleasedFirmwareVersion = value;
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._ReleasedFirmwareVersionTextBox.Text = value; } );
                    }
                    else
                    {
                        this._ReleasedFirmwareVersionTextBox.Text = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the source measure unit. </summary>
        private string _SourceMeasureUnitName = string.Empty;

        /// <summary> Gets or sets the Source Measure Unit Name. </summary>
        /// <value> The Source Measure Unit Name. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string SourceMeasureUnitName
        {
            get => this._SourceMeasureUnitName;

            set {
                if ( !string.Equals( this.SourceMeasureUnitName, value ) )
                {
                    this._SourceMeasureUnitName = value;
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._SourceMeasureUnitTextBox.Text = value; } );
                    }
                    else
                    {
                        this._SourceMeasureUnitTextBox.Text = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the header title. </summary>
        /// <value> The main title. </value>
        [Browsable( false )]
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        public string MainTitle
        {
            get => this._TitleLabel.Text;

            set {
                if ( !string.Equals( value, this.MainTitle ) )
                {
                    if ( this.InvokeRequired )
                    {
                        _ = this.BeginInvoke( ( MethodInvoker ) delegate { this._TitleLabel.Text = value; } );
                    }
                    else
                    {
                        this._TitleLabel.Text = value;
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " EVENT HANDLERS "

        /// <summary> Event handler. Called by failedPicture for double click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FailedPicture_DoubleClick( object sender, EventArgs e )
        {
            this._FailedPicture.Visible = false;
        }

        /// <summary> Event handler. Called by passedPicture for double click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PassedPicture_DoubleClick( object sender, EventArgs e )
        {
            this._PassedPicture.Visible = false;
        }

        /// <summary> Displays the version information panel. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenAboutBoxButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            using var aboutScreen = new Core.About();
            aboutScreen.TopMost = true;
            Form parentForm = this.Parent as Form;
            if ( parentForm is null && this.Parent.Parent is object )
            {
                parentForm = this.Parent.Parent as Form;
            }

            if ( parentForm is object )
            {
                aboutScreen.Icon = parentForm.Icon;
            }

            _ = aboutScreen.ShowDialog( System.Reflection.Assembly.GetExecutingAssembly(), "Integrated Scientific Resources, Inc.", "", "TTM", "" );
        }

        /// <summary> Opens device settings button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDeviceSettingsButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            TspDevice.OpenSettingsEditor();
        }

        /// <summary> Opens user interface settings button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenUserInterfaceSettingsButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            LoaderView.OpenSettingsEditor();
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
