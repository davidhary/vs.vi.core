using System;
using System.Diagnostics;

namespace isr.VI.Tsp.K2600.Ttm.Register
{
    internal static class Startup
    {

        /// <summary> Application entry procedure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [STAThread()]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static void Main()
        {
            string lastAction = string.Empty;
            bool success = true;
            try
            {
                lastAction = "initializing";
                My.MyProject.Application.InitializeKnownState();
                lastAction = "parsing command line";
                _ = My.MyApplication.ConsoleWriteLine( lastAction );
                if ( CommandLineInfo.TryParseCommandLine( My.MyProject.Application.CommandLineCollection ) )
                {
                    if ( CommandLineInfo.DisplayHelp.GetValueOrDefault( false ) )
                    {
                        CommandLineInfo.DisplayCommandLine();
                        _ = Console.ReadKey();
                    }

                    if ( CommandLineInfo.DisplayCertification.GetValueOrDefault( false ) )
                    {
                        My.MyProject.Application.DisplayCertification( CommandLineInfo.Password, CommandLineInfo.SerialNumber, CommandLineInfo.Salt );
                    }

                    if ( (CommandLineInfo.Attended.GetValueOrDefault(false) || CommandLineInfo.InteractiveEnabled.GetValueOrDefault(false)) == true )
                    {
                        Console.Write( "Enter any key to exit: " );
                        _ = Console.ReadKey();
                    }
                }
                else
                {
                    success = false;
                    My.MyApplication.ConsoleErrorWriteLine( "Failed {0}.", lastAction );
                    Console.Write( "Enter any key to exit: " );
                    _ = Console.ReadKey();
                }
            }
            catch ( Exception ex )
            {
                success = false;

                // log the exception
                My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "Failed " + lastAction );
                My.MyApplication.ConsoleErrorWriteLine( "Exception occurred {0} with error '{1}'.", lastAction, ex.Message );
            }
            finally
            {

                // flush the log.
                My.MyProject.Application.Log.DefaultFileLogWriter.Flush();

                // For some reason the event handling set in the Settings class dos not really work.
                My.MySettings.Default.Save();
                Console.Write( "Enter any key to exit: " );
                _ = Console.ReadKey();
                if ( success )
                {
                    // exit with success code
                    Environment.Exit( 0 );
                }
                else
                {
                    // exit with an error code
                    Environment.Exit( -1 );
                }
            }
        }
    }
}
