using System;
using System.Diagnostics;

namespace isr.VI.Tsp.K2600.Ttm.Register
{

    /// <summary>
    /// A sealed class the parses the command line and provides the command line values.
    /// </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-02-02, x.x.5093.x. </para>
    /// </remarks>
    public sealed class CommandLineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private CommandLineInfo() : base()
        {
        }

        #endregion

        #region " OPTIONS "

        /// <summary> Displays a command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        internal static void DisplayCommandLine()
        {
            Console.WriteLine( "{9} [{0}|{1}] [{2}] [{3}] [{4}] [{5}<address>] [{6}<password>]  [{7}<serial number>] [{8}]", AttendedOption, InteractiveEnabledOption, CertifyRequestedOption, DisplayCertificationOption, QuiteModeEnabledOption, InstrumentResourceNameOption, PasswordOption, SerialNumberOption, DisplayHelpOption, My.MyProject.Application.Info.AssemblyName );
            Console.WriteLine( "Option Switches:" );
            Console.WriteLine( AttendedOption );
            Console.WriteLine( "Attended mode.  Messages are output to the console. The program exists after the operator enters a key at the prompt." );
            Console.WriteLine( InteractiveEnabledOption );
            Console.WriteLine( "Interactive mode.  User gets to approve or cancel each task." );
            Console.WriteLine( CertifyRequestedOption );
            Console.WriteLine( "Certify the instrument connected at the specified address." );
            Console.WriteLine( DisplayCertificationOption );
            Console.WriteLine( "Displays the certification. Will request password and serial number." );
            Console.WriteLine( QuiteModeEnabledOption );
            Console.WriteLine( "Quite mode.  Only errors are output." );
            Console.WriteLine( "{0}<address>", InstrumentResourceNameOption );
            Console.WriteLine( "Specifies the instrument resource name." );
            Console.WriteLine( "{0}<password>", PasswordOption );
            Console.WriteLine( "Specifies the password." );
            Console.WriteLine( "{0}<serial number>", SerialNumberOption );
            Console.WriteLine( "Specifies the instrument serial number." );
            Console.WriteLine( DisplayHelpOption );
            Console.WriteLine( "Displays help" );
        }

        /// <summary> Gets the 'Attended' command line option. </summary>
        /// <value> The Attended' option. </value>
        public static string AttendedOption => "-a";

        /// <summary> Gets the Certify-Requested option. </summary>
        /// <value> The Certify Requested option. </value>
        public static string CertifyRequestedOption => "-n";

        /// <summary> Gets the 'Display Certification' command line option. </summary>
        /// <value> The Display Certification' option. </value>
        public static string DisplayCertificationOption => "-d";

        /// <summary> Gets the Interactive-Enabled option. </summary>
        /// <value> The Interactive enabled option. </value>
        public static string InteractiveEnabledOption => "-n";

        /// <summary> Gets the Instrument Resource Name command line option. </summary>
        /// <value> The Instrument Resource Name option. </value>
        public static string InstrumentResourceNameOption => "-i";

        /// <summary> Gets the Quite Mode Enabled option. </summary>
        /// <value> <c>True</c> if quite mode is enabled. </value>
        public static string QuiteModeEnabledOption => "-q";

        /// <summary> Gets the Password command line option. </summary>
        /// <value> The Password option. </value>
        public static string PasswordOption => "-p";

        /// <summary> Gets the salt option. </summary>
        /// <value> The salt option. </value>
        public static string SaltOption => "-S";

        /// <summary> Gets the SerialNumber command line option. </summary>
        /// <value> The SerialNumber option. </value>
        public static string SerialNumberOption => "-s";

        /// <summary> Gets the Display Help option. </summary>
        /// <value> The display help option. </value>
        public static string DisplayHelpOption => "-?";

        #endregion

        #region " PARSER "

        /// <summary> Validates the command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        ///                                      not handled. </exception>
        /// <param name="commandLineArguments"> The command line arguments. </param>
        public static void ValidateCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArguments )
        {
            if ( commandLineArguments is object )
            {
                QuiteModeEnabled = true;
                DisplayHelp = true;
                foreach ( string argument in commandLineArguments )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( AttendedOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( CertifyRequestedOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( DisplayCertificationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( InteractiveEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( InstrumentResourceNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( QuiteModeEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( PasswordOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( SerialNumberOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( DisplayHelpOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else
                    {
                        throw new ArgumentException( string.Format( System.Globalization.CultureInfo.CurrentCulture, "Unknown command line argument '{0}' was detected. Should be Ignored.", argument ), nameof( commandLineArguments ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            if ( commandLineArgs is object )
            {
                foreach ( string argument in commandLineArgs )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( AttendedOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        Attended = true;
                    }
                    else if ( argument.StartsWith( CertifyRequestedOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        CertifyRequested = true;
                    }
                    else if ( argument.StartsWith( DisplayCertificationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        DisplayCertification = true;
                    }
                    else if ( argument.StartsWith( InteractiveEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        // Dim value As String = argument.Substring(CommandLineInfo.InteractiveEnabledOption.Length)
                        // CommandLineInfo.InteractiveEnabled = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                        InteractiveEnabled = true;
                    }
                    else if ( argument.StartsWith( InstrumentResourceNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        InstrumentResourceName = argument.Substring( InstrumentResourceNameOption.Length );
                    }
                    else if ( argument.StartsWith( QuiteModeEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        QuiteModeEnabled = true;
                    }
                    else if ( argument.StartsWith( PasswordOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        string pw = argument.Substring( PasswordOption.Length );
                        if ( string.Equals( pw, "file", StringComparison.OrdinalIgnoreCase ) && My.MyProject.Computer.FileSystem.FileExists( My.MySettings.Default.OkayFileName ) )
                        {
                            pw = My.MyProject.Computer.FileSystem.FileExists( My.MySettings.Default.OkayFileName )
                                ? My.MyProject.Computer.FileSystem.ReadAllText( My.MySettings.Default.OkayFileName )
                                : throw new System.IO.FileNotFoundException( $"{nameof( My.MySettings.Default.OkayFileName )} file not found", My.MySettings.Default.OkayFileName );
                        }

                        Password = pw;
                    }
                    else if ( argument.StartsWith( SaltOption, StringComparison.Ordinal ) )
                    {
                        string salt = argument.Substring( SaltOption.Length );
                        if ( string.Equals( salt, "file", StringComparison.OrdinalIgnoreCase ) && My.MyProject.Computer.FileSystem.FileExists( My.MySettings.Default.SaltFileName ) )
                        {
                            salt = My.MyProject.Computer.FileSystem.FileExists( My.MySettings.Default.SaltFileName )
                                ? My.MyProject.Computer.FileSystem.ReadAllText( My.MySettings.Default.SaltFileName )
                                : throw new System.IO.FileNotFoundException( $"{nameof( My.MySettings.Default.SaltFileName )} file not found", My.MySettings.Default.SaltFileName );
                        }

                        Salt = salt.Trim();
                    }
                    else if ( argument.StartsWith( SerialNumberOption, StringComparison.Ordinal ) )
                    {
                        SerialNumber = argument.Substring( SerialNumberOption.Length );
                    }
                    else if ( argument.StartsWith( DisplayHelpOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        DisplayHelp = true;
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        /// <returns> True if success or false if Exception occurred. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static bool TryParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            try
            {
                ParseCommandLine( commandLineArgs );
                return true;
            }
            catch ( ArgumentException ex )
            {
                My.MyProject.Application.Log.WriteException( ex, TraceEventType.Information, "Unknown argument ignored" );
                return true;
            }
            catch ( Exception ex )
            {
                if ( commandLineArgs is null )
                {
                    My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, "Failed parsing empty command line" );
                }
                else
                {
                    // #Disable Warning CA1825 ' Avoid zero-length array allocations.
                    var args = Array.Empty<string>();
                    // #Enable Warning CA1825 ' Avoid zero-length array allocations.
                    commandLineArgs.CopyTo( args, 0 );
                    My.MyProject.Application.Log.WriteException( ex, TraceEventType.Error, string.Format( System.Globalization.CultureInfo.CurrentCulture, "Failed parsing command line '{0}'", string.Join( " ", args ) ) );
                }

                return false;
            }
        }

        #endregion

        #region " COMMAND LINE ELEMENTS "

        /// <summary> Gets or sets the 'attended ' option. </summary>
        /// <remarks>
        /// Messages are output to the console. The program exists after the operator enters a key at the
        /// prompt.
        /// </remarks>
        /// <value>
        /// <c>True</c> if user attended are enabled; otherwise, <c>False</c> or <c>null</c>.
        /// </value>
        public static bool? Attended { get; set; }

        /// <summary> Gets or sets a value indicating whether certification is requested. </summary>
        /// <remarks> Certify the instrument connected at the specified address. </remarks>
        /// <value>
        /// <c>True</c> if certification is requested; otherwise, <c>False</c> or <c>null</c>.
        /// </value>
        public static bool? CertifyRequested { get; set; }

        /// <summary> Gets or sets the 'DisplayCertification ' option. </summary>
        /// <remarks> Displays the certification. Will request password and serial number. </remarks>
        /// <value>
        /// <c>True</c> if user DisplayCertification are enabled; otherwise, <c>False</c> or <c>null</c>.
        /// </value>
        public static bool? DisplayCertification { get; set; }

        /// <summary> Gets or sets a value indicating whether user interactions are enabled. </summary>
        /// <remarks> Interactive mode.  User gets to approve or cancel each task. </remarks>
        /// <value>
        /// <c>True</c> if user interactions are enabled; otherwise, <c>False</c> or <c>null</c>.
        /// </value>
        public static bool? InteractiveEnabled { get; set; }

        /// <summary> Gets or sets the instrument resource name. </summary>
        /// <value> The instrument resource name. </value>
        public static string InstrumentResourceName { get; set; }

        /// <summary> Gets or sets the quite mode sentinel. </summary>
        /// <remarks> Quite mode.  Only errors are output. </remarks>
        /// <value> <c>True</c> if quite mode; otherwise, <c>False</c> or <c>null</c>. </value>
        public static bool? QuiteModeEnabled { get; set; }

        /// <summary> Gets or sets the Password. </summary>
        /// <value> The Password. </value>
        public static string Password { get; set; }

        /// <summary> Gets or sets the salt. </summary>
        /// <value> The salt. </value>
        public static string Salt { get; set; }

        /// <summary> Gets or sets the SerialNumber. </summary>
        /// <value> The SerialNumber. </value>
        public static string SerialNumber { get; set; }

        /// <summary> Gets or sets a value indicating whether to display help. </summary>
        /// <value> <c>True</c> to display help; otherwise, <c>False</c> or <c>null</c>. </value>
        public static bool? DisplayHelp { get; set; }

        #endregion

        #region " PASSWORD "

        /// <summary> The password hash. </summary>
        private const string _PasswordHash = "EtXykU6sTnHsX3XksXVrZFNwfyA=";

        /// <summary> Returns true if the password equals the password hash. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="password"> The password. </param>
        /// <param name="salt">     The salt. </param>
        /// <returns> <c>True</c> if passed; otherwise, <c>False</c>. </returns>
        internal static bool IsPass( string password, string salt )
        {
            return _PasswordHash == (Core.HashExtensions.HashExtensionMethods.ToBase64Hash( $"{password}{salt}" ) ?? "");
        }

        #endregion

    }
}
