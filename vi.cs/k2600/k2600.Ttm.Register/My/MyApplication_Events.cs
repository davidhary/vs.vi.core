using System;
using System.Diagnostics;
using System.Linq;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Ttm.Register.My
{
    internal partial class MyApplication
    {

        #region " APPLICATION EXTENSIONS "

        #endregion

        #region " APPLICATION EVENTS "

        /// <summary>
        /// Gets command line arguments so that custom arguments could be used in design mode or
        /// interactive mode.
        /// </summary>
        /// <value> A Collection of command lines. </value>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> CommandLineCollection { get; private set; }

        /// <summary>
        /// Replaces the default trace listener with the modified listener. Updates the minimum splash
        /// screen display time.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        internal void InitializeKnownState()
        {
            this.CreateLogger();
            string commandLine;
            if ( Debugger.IsAttached && MyProject.Application.CommandLineArgs.Count == 0 )
            {
                Console.Write( "Enter command line arguments (e.g., -? -n -a26 -pPassword): " );
                commandLine = Console.ReadLine();
                // make sure the command line presume interactive mode.
                if ( commandLine.IndexOf( "-n", 0, StringComparison.OrdinalIgnoreCase ) < 0 )
                {
                    commandLine = "-n " + commandLine;
                }

                this.CommandLineCollection = string.IsNullOrWhiteSpace( commandLine ) ? MyProject.Application.CommandLineArgs : new System.Collections.ObjectModel.ReadOnlyCollection<string>( commandLine.Split( ' ' ) );
            }
            else
            {
                this.CommandLineCollection = MyProject.Application.CommandLineArgs;
                commandLine = string.Join( " ", this.CommandLineCollection.ToArray() );
            }

            _ = ConsoleWriteLine( "Using the following command line:" );
            _ = ConsoleWriteLine( commandLine );
        }

        /// <summary> Executes shut down operations. </summary>
        /// <remarks> Saves user settings for all related libraries. </remarks>
        internal void Shutdown()
        {

            // flush the log.
            MyProject.Application.Log.DefaultFileLogWriter.Flush();

            // For some reason the event handling set in the Settings class dos not really work.
            My.MySettings.Default.Save();

            // do some garbage collection
            GC.Collect();
        }

        #endregion

        #region " CONSOLE MANAGEMENT "

        /// <summary>
        /// Writes a line to the console Error output. Output line to the application error log.
        /// </summary>
        /// <remarks> Use this method for all error reporting. </remarks>
        /// <param name="format"> Specifies the message format. </param>
        /// <param name="args">   Specified the message arguments. </param>
        internal static void ConsoleErrorWriteLine( string format, params object[] args )
        {
            ConsoleErrorWriteLine( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary>
        /// Writes a line to the console Error output. Output line to the application error log.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        internal static void ConsoleErrorWriteLine( string message )
        {
            Console.Error.WriteLine( message );
            MyProject.Application.Log.WriteEntry( message, TraceEventType.Error );
        }

        /// <summary>
        /// Writes a line to the console output if not quite mode. Outputs a line to the application log.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        internal static void ConsoleOutWriteLine( string format, params object[] args )
        {
            ConsoleOutWriteLine( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary>
        /// Writes a line to the console output if not quite mode. Outputs a line to the application log.
        /// </summary>
        /// <remarks> This method is called when output to the console. </remarks>
        /// <param name="message"> The message. </param>
        /// <param name="isQuiet"> true if this object is quiet. </param>
        internal void ConsoleOutWriteLine( string message, bool isQuiet )
        {
            if ( !isQuiet )
            {
                Console.Out.WriteLine( message );
            }

            MyProject.Application.Log.WriteEntry( message, TraceEventType.Verbose );
        }

        /// <summary>
        /// Queries the user from the console. In attended mode, this just displays the message. In
        /// interactive mode, this adds the query 'Continue?' and waits of a key.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> Specifies the message format. </param>
        /// <param name="args">   Specified the message arguments. </param>
        /// <returns> <c>True</c> if Y; otherwise, <c>False</c>. </returns>
        internal static bool ConsoleQueryLine( string format, params object[] args )
        {
            return ConsoleQueryLine( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ), false, false );
        }

        /// <summary>
        /// Queries the user from the console. In attended mode, this just displays the message. In
        /// interactive mode, this adds the query 'Continue?' and waits for a key.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message">       The message. </param>
        /// <param name="isInteractive"> true if this object is interactive. </param>
        /// <param name="attended">      true if attended. </param>
        /// <returns> <c>True</c> if Y; otherwise, <c>False</c>. </returns>
        internal static bool ConsoleQueryLine( string message, bool isInteractive, bool attended )
        {
            if ( isInteractive )
            {
                Console.WriteLine( message );
                Console.Write( "Continue (y/n)? " );
                var reply = Console.ReadKey();
                Console.WriteLine();
                return Equals( reply.KeyChar, 'y' ) || Equals( reply.KeyChar, 'Y' );
            }
            else if ( attended )
            {
                Console.WriteLine( message );
                return true;
            }
            else
            {
                return true;
            }
        }

        /// <summary> Writes a line to the console in interactive or attended modes. </summary>
        /// <remarks> Use this method for outputting to the console and standard output. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> The written text. </returns>
        internal string ConsoleWriteLine( string format, params object[] args )
        {
            return ConsoleWriteLine( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary> Writes a line to the console. </summary>
        /// <remarks> Use this method for outputting to the console and standard output. </remarks>
        /// <param name="message"> The message. </param>
        /// <returns> The written text. </returns>
        internal static string ConsoleWriteLine( string message )
        {
            Console.WriteLine( message );
            return message;
        }

        /// <summary> Writes a line to the console in interactive or attended modes. </summary>
        /// <remarks> Use this method for outputting to the console and standard output. </remarks>
        /// <param name="message">       The message. </param>
        /// <param name="isInteractive"> true if this object is interactive. </param>
        /// <param name="attended">      true if attended. </param>
        /// <returns> The written text. </returns>
        internal string ConsoleWriteLine( string message, bool isInteractive, bool attended )
        {
            if ( isInteractive || attended )
            {
                Console.WriteLine( message );
            }
            else
            {
                ConsoleOutWriteLine( message );
            }

            return message;
        }

        /// <summary> Read a password from the console into a string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="prompt"> . </param>
        /// <returns> The password text. </returns>
        public static string ReadPassword( string prompt )
        {
            string password = string.Empty;
            Console.Write( prompt );

            // get the first character of the password
            var nextKey = Console.ReadKey( true );
            while ( nextKey.Key != ConsoleKey.Enter )
            {
                if ( nextKey.Key == ConsoleKey.Backspace )
                {
                    if ( password.Length > 0 )
                    {

                        // erase the last * as well
                        Console.Write( nextKey.KeyChar );
                        Console.Write( " " );
                        Console.Write( nextKey.KeyChar );
                    }

                    if ( password.Length <= 0 )
                    {
                    }
                    else if ( password.Length == 1 )
                    {
                        password = string.Empty;
                    }
                    else if ( password.Length >= 2 )
                    {
                        password = password.Substring( 0, password.Length - 1 );
                    }
                }
                else
                {
                    password += Conversions.ToString( nextKey.KeyChar );
                    Console.Write( "*" );
                }

                nextKey = Console.ReadKey( true );
            }

            Console.WriteLine();
            return password.Trim();
        }

        /// <summary> Enters a password from the console. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The password text. </returns>
        internal static string EnterPassword()
        {
            string password = ReadPassword( "Enter Password: " );
            string retryPassword = ReadPassword( "Confirm Password: " );
            return string.Equals( password, retryPassword ) ? password : EnterPassword();
        }

        #endregion

        #region " CERTIFICATION MANAGEMENT "

        /// <summary> Creates default local or remote jobs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="password">     The password. </param>
        /// <param name="serialNumber"> The serial number. </param>
        /// <param name="salt">         The salt. </param>
        internal void DisplayCertification( string password, string serialNumber, string salt )
        {

            // select Windows Principal policy
            AppDomain.CurrentDomain.SetPrincipalPolicy( System.Security.Principal.PrincipalPolicy.WindowsPrincipal );
            _ = System.Threading.Thread.CurrentPrincipal.Identity.Name;
            if ( string.IsNullOrWhiteSpace( password ) )
            {
                password = EnterPassword();
            }

            if ( CommandLineInfo.IsPass( password, salt ) )
            {
                if ( string.IsNullOrWhiteSpace( serialNumber ) )
                {
                    Console.WriteLine( "Enter serial number" );
                    serialNumber = Console.ReadLine();
                }

                Console.WriteLine( Core.HashExtensions.HashExtensionMethods.ToBase64Hash( $"{serialNumber}{salt}" ) );
                Console.WriteLine( "Enter a key:" );
                _ = Console.ReadKey();
            }
        }

        #endregion

    }
}
