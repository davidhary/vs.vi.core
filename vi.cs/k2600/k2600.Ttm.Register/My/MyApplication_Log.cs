﻿using System;
using System.Diagnostics;

using isr.Core;
using isr.VI.Tsp.K2600.Ttm.Register.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm.Register.My
{
    internal partial class MyApplication
    {

        /// <summary> Logs unpublished exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity">  The activity. </param>
        /// <param name="exception"> The exception. </param>
        public void LogUnpublishedException( string activity, Exception exception )
        {
            _ = this.LogUnpublishedMessage( new TraceMessage( TraceEventType.Error, TraceEventId, $"Exception {activity};. {exception.ToFullBlownString()}" ) );
        }

        /// <summary> Applies the given value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void Apply( Logger value )
        {
            this._Logger = value;
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void ApplyTraceLogLevel( TraceEventType value )
        {
            this.TraceLevel = value;
            this.Logger.ApplyTraceLevel( value );
        }

        /// <summary> Applies the trace level described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ApplyTraceLogLevel()
        {
            this.ApplyTraceLogLevel( MySettings.Default.TraceLevel );
        }
    }
}