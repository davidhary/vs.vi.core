﻿
namespace isr.VI.Tsp.K2600.Ttm.Register.My
{

    /// <summary> my application. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    internal partial class MyApplication
    {

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = 4; // TraceEventIds.IsrVITtmRegister

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "TTM Register";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "TTM Register";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "TTM.Register";
    }
}