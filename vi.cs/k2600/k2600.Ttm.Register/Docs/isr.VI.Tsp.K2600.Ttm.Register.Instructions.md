## ISR TTM TM Register Instructions: Registration information.

**Tuning on the instrument and verifying its connection to the Virtual
Machine:**
-   Turn on the instrument;
-   Connect the instrument to the computer from the USB socket at the
    back of the instrument;
-   Click *Player* from the Virtual Machine top window (move the mouse
    to the top of the screen and the top window with drop down). Then
    click *Removable Devices*. The instrument will be listed as:\
    Keithley Instruments SYSTEM Source Meter

**Registering instrument for Test Script Programs**
-   Click *TTM TSB* from the *TTM* Toolbar folder (bottom right corner
    of the Desktop Window). This opens the TTM TSP workspace from
    *C:\\My\\LIBRARIES\\TSP.1\\TTM.2.x*;
-   Turn on the instrument as described above;
-   Click the *Open Instrument* button from the *TSP - Keithley Test
    Script Builder* *Instrument Console* window.
-   Click the down arrow of the *Select or enter new instrument* drop
    down and select the USB instrument listed with the serial number of
    the instrument, such as:\
    USB0::0x05E6::0x2601::4089222::INSTR
-   Click *OK*. The instrument will connect and display the *TSP\>*
    prompt in the Instrument Console window.
-   Click the down arrow next to the Run button (below and in between
    the Project and Run top menu items) and select 3. isr\_Certify. A
    progress bar shows briefly at the bottom right corner of the
    Instrument Console as the script loads and runs. A new *TSP\>*
    prompt is displayed in the Console;
-   Right-click on *AccessControlTester.tsp* from the *Trash* folder of
    the *Navigator* panel and click *Open*;
-   Click Run, Run As, 1. TSP File from the top menu. The
    *AccessControlTester.tsp* script loads and runs. A new *TSP\>*
    prompt is displayed in the Console;
-   Find the following text in the script window. Copy it and paste it
    at the last TSP\> prompt of the Instrument Console:\
    print( \_G.isr.access.get1( localnode.serialno ) )\
    like so:

TSP\>print( \_G.isr.access.get1( localnode.serialno ) )
-   Click and Enter key. The access key will be displayed, like so:

EFNbB350XE97YYACe194

TSP\>

> Enter the following command at the *TSP\>* Prompt and click enter. The
> instrument will display true like so:\
> TSP\>print( isr.access.allow( \'EFNbB350XE97YYACe194\'))
>
> true\
> TSP\>
-   This serves to verify that the access key was registered.
-   Open the *certified.txt* file from the *RESOURCES* folder of the
    *isr.TTM.Device.Library* project under the *APPS* solution project.

```{=html}
<!-- -->
```
-   Paste the access key at the end of the list, like so:\
    \...\
    EFlmT3h-bQeAdHEDgG50\
    EFNbB350XE97YYACe194

```{=html}
<!-- -->
```
-   Click the *Close Instrument* button from *TSP - Keithley Test Script
    Builder* *Instrument Console* window. This ends the TSB session.
-   Turn off the instrument.

**Registering instrument for Windows Programs**
-   Click *VS 2010* from the *Apps* Toolbar folder (bottom right corner
    of the Desktop Window) to open Visual Studio;
-   Click *Start Page* from the *View* menu;
-   Click *TTM.4.x* to open the TTM solution. The solution project will
    display in the *Solution Explorer* at the right side of the Visual
    Studio development environment screen;
-   Right-Click on the *isr.VI.Tsp.K2600.Ttm.Register* project from the *APPS* folder
    of the *Solution Explorer* and click *Set as StartUp Project*;
-   Right-Click on the *isr.VI.Tsp.K2600.Ttm.Register* project from the *APPS* folder
    of the *Solution Explorer* and click *Rebuild*. This compiles the
    project;
-   Right-Click on the *isr.VI.Tsp.K2600.Ttm.Register* project from the *APPS* folder
    of the *Solution Explorer* and click on *Properties*;
-   Select the *Debug* tab;
-   Enter the instrument serial number at the *-s* switch of the
    *Command line arguments* edit box as follows:\
    -n -pokay.ISR.tsp -s4089222 -d
-   Click the Run button (green right arrow) or press *F5* to run the
    program;
-   The program will display the hash key for the instrument as
    follows:\
    Using the following command line:\
    -n -pokay.ISR.tsp -s4081414 -d\
    parsing command line\
    **ZcOTvmNT9cUjh+YGPyo0UNN1DAE=**\
    Enter a key:
-   Click the application window icon (top left corner of the
    application window) and click *Edit* and then *Mark*;
-   Highlight the hash key (bolded above) and click the *Enter* key on
    the keyboard. This copies the hash key to the clipboard;
-   Open the *certified.txt* file from the *RESOURCES* folder of the
    *isr.TTM.Device.Library* project under the *APPS* solution project.

```{=html}
<!-- -->
```
-   Paste the hash key at the end of the list, like so:\
    \...\
    X+Wi5lAA95cZBA6u88ziuGF/hd8=\
    ZcOTvmNT9cUjh+YGPyo0UNN1DAE=

```{=html}
<!-- -->
```
-   Select the application Window and click any key to close the
    application.

\(c\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

Licensed under the [ISR Fair End User Use License Version
*1.0](http://www.isr.cc/licenses/FairEndUserUseLicense.pdf).
Unless required by applicable law or agreed to in writing, this software
is provided \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.
