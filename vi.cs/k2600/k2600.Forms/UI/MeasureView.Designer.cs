﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Forms
{
    [DesignerGenerated()]
    public partial class MeasureView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            __OpenDetectorCheckBox = new System.Windows.Forms.CheckBox();
            __OpenDetectorCheckBox.CheckedChanged += new EventHandler(OpenDetectorCheckBox_CheckedChanged);
            _AutoDelayCheckBox = new System.Windows.Forms.CheckBox();
            _AutoZeroCheckBox = new System.Windows.Forms.CheckBox();
            _FilterGroupBox = new System.Windows.Forms.GroupBox();
            _FilterWindowNumericLabel = new System.Windows.Forms.Label();
            _FilterWindowNumeric = new System.Windows.Forms.NumericUpDown();
            _RepeatingAverageRadioButton = new System.Windows.Forms.RadioButton();
            _MovingAverageRadioButton = new System.Windows.Forms.RadioButton();
            _FilterCountNumeric = new System.Windows.Forms.NumericUpDown();
            _FilterCountNumericLabel = new System.Windows.Forms.Label();
            __FilterEnabledCheckBox = new System.Windows.Forms.CheckBox();
            __FilterEnabledCheckBox.CheckedChanged += new EventHandler(FilterEnabledCheckBox_CheckedChanged);
            _SenseRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _PowerLineCyclesNumeric = new System.Windows.Forms.NumericUpDown();
            _SenseRangeNumericLabel = new System.Windows.Forms.Label();
            _PowerLineCyclesNumericLabel = new System.Windows.Forms.Label();
            __SenseFunctionComboBox = new System.Windows.Forms.ComboBox();
            __SenseFunctionComboBox.SelectedIndexChanged += new EventHandler(SenseFunctionComboBox_SelectedIndexChanged);
            _SenseFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _AutoRangeCheckBox = new System.Windows.Forms.CheckBox();
            __ApplySenseSettingsButton = new System.Windows.Forms.Button();
            __ApplySenseSettingsButton.Click += new EventHandler(ApplySenseSettingsButton_Click);
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            _FilterGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_FilterWindowNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_FilterCountNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_PowerLineCyclesNumeric).BeginInit();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(365, 341);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__OpenDetectorCheckBox);
            _Panel.Controls.Add(_AutoDelayCheckBox);
            _Panel.Controls.Add(_AutoZeroCheckBox);
            _Panel.Controls.Add(_FilterGroupBox);
            _Panel.Controls.Add(__FilterEnabledCheckBox);
            _Panel.Controls.Add(_SenseRangeNumeric);
            _Panel.Controls.Add(_PowerLineCyclesNumeric);
            _Panel.Controls.Add(_SenseRangeNumericLabel);
            _Panel.Controls.Add(_PowerLineCyclesNumericLabel);
            _Panel.Controls.Add(__SenseFunctionComboBox);
            _Panel.Controls.Add(_SenseFunctionComboBoxLabel);
            _Panel.Controls.Add(_AutoRangeCheckBox);
            _Panel.Controls.Add(__ApplySenseSettingsButton);
            _Panel.Location = new System.Drawing.Point(10, 46);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(345, 249);
            _Panel.TabIndex = 0;
            // 
            // _OpenDetectorCheckBox
            // 
            __OpenDetectorCheckBox.AutoSize = true;
            __OpenDetectorCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __OpenDetectorCheckBox.Location = new System.Drawing.Point(190, 145);
            __OpenDetectorCheckBox.Name = "__OpenDetectorCheckBox";
            __OpenDetectorCheckBox.Size = new System.Drawing.Size(149, 21);
            __OpenDetectorCheckBox.TabIndex = 27;
            __OpenDetectorCheckBox.Text = "Open Detector (off)";
            __OpenDetectorCheckBox.UseVisualStyleBackColor = true;
            // 
            // _AutoDelayCheckBox
            // 
            _AutoDelayCheckBox.AutoSize = true;
            _AutoDelayCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _AutoDelayCheckBox.Location = new System.Drawing.Point(190, 119);
            _AutoDelayCheckBox.Name = "_AutoDelayCheckBox";
            _AutoDelayCheckBox.Size = new System.Drawing.Size(128, 21);
            _AutoDelayCheckBox.TabIndex = 26;
            _AutoDelayCheckBox.Text = "Auto Delay (off)";
            _AutoDelayCheckBox.ThreeState = true;
            _AutoDelayCheckBox.UseVisualStyleBackColor = true;
            // 
            // _AutoZeroCheckBox
            // 
            _AutoZeroCheckBox.AutoSize = true;
            _AutoZeroCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _AutoZeroCheckBox.Location = new System.Drawing.Point(190, 43);
            _AutoZeroCheckBox.Name = "_AutoZeroCheckBox";
            _AutoZeroCheckBox.Size = new System.Drawing.Size(89, 21);
            _AutoZeroCheckBox.TabIndex = 19;
            _AutoZeroCheckBox.Text = "Auto Zero";
            _AutoZeroCheckBox.UseVisualStyleBackColor = true;
            // 
            // _FilterGroupBox
            // 
            _FilterGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left;
            _FilterGroupBox.Controls.Add(_FilterWindowNumericLabel);
            _FilterGroupBox.Controls.Add(_FilterWindowNumeric);
            _FilterGroupBox.Controls.Add(_RepeatingAverageRadioButton);
            _FilterGroupBox.Controls.Add(_MovingAverageRadioButton);
            _FilterGroupBox.Controls.Add(_FilterCountNumeric);
            _FilterGroupBox.Controls.Add(_FilterCountNumericLabel);
            _FilterGroupBox.Location = new System.Drawing.Point(6, 102);
            _FilterGroupBox.Name = "_FilterGroupBox";
            _FilterGroupBox.Size = new System.Drawing.Size(155, 135);
            _FilterGroupBox.TabIndex = 24;
            _FilterGroupBox.TabStop = false;
            _FilterGroupBox.Text = "Filter";
            // 
            // _FilterWindowNumericLabel
            // 
            _FilterWindowNumericLabel.AutoSize = true;
            _FilterWindowNumericLabel.Location = new System.Drawing.Point(3, 48);
            _FilterWindowNumericLabel.Name = "_FilterWindowNumericLabel";
            _FilterWindowNumericLabel.Size = new System.Drawing.Size(81, 17);
            _FilterWindowNumericLabel.TabIndex = 3;
            _FilterWindowNumericLabel.Text = "Window [%]:";
            // 
            // _FilterWindowNumeric
            // 
            _FilterWindowNumeric.DecimalPlaces = 2;
            _FilterWindowNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FilterWindowNumeric.Location = new System.Drawing.Point(87, 44);
            _FilterWindowNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _FilterWindowNumeric.Name = "_FilterWindowNumeric";
            _FilterWindowNumeric.Size = new System.Drawing.Size(54, 25);
            _FilterWindowNumeric.TabIndex = 4;
            _FilterWindowNumeric.Value = new decimal(new int[] { 10, 0, 0, 0 });
            // 
            // _RepeatingAverageRadioButton
            // 
            _RepeatingAverageRadioButton.AutoSize = true;
            _RepeatingAverageRadioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _RepeatingAverageRadioButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _RepeatingAverageRadioButton.Location = new System.Drawing.Point(6, 107);
            _RepeatingAverageRadioButton.Name = "_RepeatingAverageRadioButton";
            _RepeatingAverageRadioButton.Size = new System.Drawing.Size(88, 21);
            _RepeatingAverageRadioButton.TabIndex = 5;
            _RepeatingAverageRadioButton.Text = "Repeating";
            _RepeatingAverageRadioButton.UseVisualStyleBackColor = true;
            // 
            // _MovingAverageRadioButton
            // 
            _MovingAverageRadioButton.AutoSize = true;
            _MovingAverageRadioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _MovingAverageRadioButton.Checked = true;
            _MovingAverageRadioButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MovingAverageRadioButton.Location = new System.Drawing.Point(21, 78);
            _MovingAverageRadioButton.Name = "_MovingAverageRadioButton";
            _MovingAverageRadioButton.Size = new System.Drawing.Size(73, 21);
            _MovingAverageRadioButton.TabIndex = 2;
            _MovingAverageRadioButton.TabStop = true;
            _MovingAverageRadioButton.Text = "Moving";
            _MovingAverageRadioButton.UseVisualStyleBackColor = true;
            // 
            // _FilterCountNumeric
            // 
            _FilterCountNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _FilterCountNumeric.Location = new System.Drawing.Point(87, 15);
            _FilterCountNumeric.Name = "_FilterCountNumeric";
            _FilterCountNumeric.Size = new System.Drawing.Size(53, 25);
            _FilterCountNumeric.TabIndex = 1;
            _FilterCountNumeric.Value = new decimal(new int[] { 100, 0, 0, 0 });
            // 
            // _FilterCountNumericLabel
            // 
            _FilterCountNumericLabel.AutoSize = true;
            _FilterCountNumericLabel.Location = new System.Drawing.Point(39, 18);
            _FilterCountNumericLabel.Name = "_FilterCountNumericLabel";
            _FilterCountNumericLabel.Size = new System.Drawing.Size(45, 17);
            _FilterCountNumericLabel.TabIndex = 0;
            _FilterCountNumericLabel.Text = "Count:";
            _FilterCountNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _FilterEnabledCheckBox
            // 
            __FilterEnabledCheckBox.AutoSize = true;
            __FilterEnabledCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __FilterEnabledCheckBox.Location = new System.Drawing.Point(190, 93);
            __FilterEnabledCheckBox.Name = "__FilterEnabledCheckBox";
            __FilterEnabledCheckBox.Size = new System.Drawing.Size(112, 21);
            __FilterEnabledCheckBox.TabIndex = 23;
            __FilterEnabledCheckBox.Text = "Filter Enabled";
            __FilterEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _SenseRangeNumeric
            // 
            _SenseRangeNumeric.DecimalPlaces = 3;
            _SenseRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SenseRangeNumeric.Location = new System.Drawing.Point(105, 71);
            _SenseRangeNumeric.Maximum = new decimal(new int[] { 1010, 0, 0, 0 });
            _SenseRangeNumeric.Name = "_SenseRangeNumeric";
            _SenseRangeNumeric.Size = new System.Drawing.Size(79, 25);
            _SenseRangeNumeric.TabIndex = 21;
            _SenseRangeNumeric.Value = new decimal(new int[] { 105, 0, 0, 196608 });
            // 
            // _PowerLineCyclesNumeric
            // 
            _PowerLineCyclesNumeric.DecimalPlaces = 3;
            _PowerLineCyclesNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _PowerLineCyclesNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _PowerLineCyclesNumeric.Location = new System.Drawing.Point(105, 41);
            _PowerLineCyclesNumeric.Maximum = new decimal(new int[] { 25, 0, 0, 0 });
            _PowerLineCyclesNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 196608 });
            _PowerLineCyclesNumeric.Name = "_PowerLineCyclesNumeric";
            _PowerLineCyclesNumeric.Size = new System.Drawing.Size(79, 25);
            _PowerLineCyclesNumeric.TabIndex = 18;
            _PowerLineCyclesNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _SenseRangeNumericLabel
            // 
            _SenseRangeNumericLabel.AutoSize = true;
            _SenseRangeNumericLabel.Location = new System.Drawing.Point(35, 75);
            _SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel";
            _SenseRangeNumericLabel.Size = new System.Drawing.Size(68, 17);
            _SenseRangeNumericLabel.TabIndex = 20;
            _SenseRangeNumericLabel.Text = "Range [V]:";
            _SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _PowerLineCyclesNumericLabel
            // 
            _PowerLineCyclesNumericLabel.AutoSize = true;
            _PowerLineCyclesNumericLabel.Location = new System.Drawing.Point(5, 45);
            _PowerLineCyclesNumericLabel.Name = "_PowerLineCyclesNumericLabel";
            _PowerLineCyclesNumericLabel.Size = new System.Drawing.Size(98, 17);
            _PowerLineCyclesNumericLabel.TabIndex = 17;
            _PowerLineCyclesNumericLabel.Text = "Aperture [nplc]:";
            _PowerLineCyclesNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseFunctionComboBox
            // 
            __SenseFunctionComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            __SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __SenseFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __SenseFunctionComboBox.Items.AddRange(new object[] { "I", "V" });
            __SenseFunctionComboBox.Location = new System.Drawing.Point(105, 11);
            __SenseFunctionComboBox.Name = "__SenseFunctionComboBox";
            __SenseFunctionComboBox.Size = new System.Drawing.Size(231, 25);
            __SenseFunctionComboBox.TabIndex = 15;
            // 
            // _SenseFunctionComboBoxLabel
            // 
            _SenseFunctionComboBoxLabel.AutoSize = true;
            _SenseFunctionComboBoxLabel.Location = new System.Drawing.Point(37, 15);
            _SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel";
            _SenseFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SenseFunctionComboBoxLabel.TabIndex = 14;
            _SenseFunctionComboBoxLabel.Text = "Function:";
            _SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _AutoRangeCheckBox
            // 
            _AutoRangeCheckBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _AutoRangeCheckBox.Location = new System.Drawing.Point(190, 67);
            _AutoRangeCheckBox.Name = "_AutoRangeCheckBox";
            _AutoRangeCheckBox.Size = new System.Drawing.Size(103, 21);
            _AutoRangeCheckBox.TabIndex = 22;
            _AutoRangeCheckBox.Text = "Auto Range";
            _AutoRangeCheckBox.UseVisualStyleBackColor = true;
            // 
            // _ApplySenseSettingsButton
            // 
            __ApplySenseSettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __ApplySenseSettingsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplySenseSettingsButton.Location = new System.Drawing.Point(278, 207);
            __ApplySenseSettingsButton.Name = "__ApplySenseSettingsButton";
            __ApplySenseSettingsButton.Size = new System.Drawing.Size(58, 30);
            __ApplySenseSettingsButton.TabIndex = 25;
            __ApplySenseSettingsButton.Text = "&Apply";
            __ApplySenseSettingsButton.UseVisualStyleBackColor = true;
            // 
            // MeasureView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "MeasureView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(367, 343);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _FilterGroupBox.ResumeLayout(false);
            _FilterGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_FilterWindowNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_FilterCountNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_PowerLineCyclesNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.CheckBox __OpenDetectorCheckBox;

        private System.Windows.Forms.CheckBox _OpenDetectorCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenDetectorCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenDetectorCheckBox != null)
                {
                    __OpenDetectorCheckBox.CheckedChanged -= OpenDetectorCheckBox_CheckedChanged;
                }

                __OpenDetectorCheckBox = value;
                if (__OpenDetectorCheckBox != null)
                {
                    __OpenDetectorCheckBox.CheckedChanged += OpenDetectorCheckBox_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.CheckBox _AutoDelayCheckBox;
        private System.Windows.Forms.CheckBox _AutoZeroCheckBox;
        private System.Windows.Forms.GroupBox _FilterGroupBox;
        private System.Windows.Forms.Label _FilterWindowNumericLabel;
        private System.Windows.Forms.NumericUpDown _FilterWindowNumeric;
        private System.Windows.Forms.RadioButton _RepeatingAverageRadioButton;
        private System.Windows.Forms.RadioButton _MovingAverageRadioButton;
        private System.Windows.Forms.NumericUpDown _FilterCountNumeric;
        private System.Windows.Forms.Label _FilterCountNumericLabel;
        private System.Windows.Forms.CheckBox __FilterEnabledCheckBox;

        private System.Windows.Forms.CheckBox _FilterEnabledCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FilterEnabledCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FilterEnabledCheckBox != null)
                {
                    __FilterEnabledCheckBox.CheckedChanged -= FilterEnabledCheckBox_CheckedChanged;
                }

                __FilterEnabledCheckBox = value;
                if (__FilterEnabledCheckBox != null)
                {
                    __FilterEnabledCheckBox.CheckedChanged += FilterEnabledCheckBox_CheckedChanged;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _SenseRangeNumeric;
        private System.Windows.Forms.NumericUpDown _PowerLineCyclesNumeric;
        private System.Windows.Forms.Label _SenseRangeNumericLabel;
        private System.Windows.Forms.Label _PowerLineCyclesNumericLabel;
        private System.Windows.Forms.ComboBox __SenseFunctionComboBox;

        private System.Windows.Forms.ComboBox _SenseFunctionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SenseFunctionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged -= SenseFunctionComboBox_SelectedIndexChanged;
                }

                __SenseFunctionComboBox = value;
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged += SenseFunctionComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.Label _SenseFunctionComboBoxLabel;
        private System.Windows.Forms.CheckBox _AutoRangeCheckBox;
        private System.Windows.Forms.Button __ApplySenseSettingsButton;

        private System.Windows.Forms.Button _ApplySenseSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySenseSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click -= ApplySenseSettingsButton_Click;
                }

                __ApplySenseSettingsButton = value;
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click += ApplySenseSettingsButton_Click;
                }
            }
        }
    }
}