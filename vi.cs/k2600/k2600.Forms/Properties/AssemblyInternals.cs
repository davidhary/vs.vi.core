﻿
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Tsp.K2600.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Tsp.Forms.K2600.Forms.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.K2600.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.Cinco.K2600.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.Morphe.K2600.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Verrzzano.K2600.MSTest,PublicKey=" + isr.VI.My.SolutionInfo.StrainPublicKey )]
