﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( "K2600 Tsp VI Forms Tests" )]
[assembly: AssemblyDescription( "K2600 Tsp Virtual Instrument Forms Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Tsp.K2600.Forms.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
