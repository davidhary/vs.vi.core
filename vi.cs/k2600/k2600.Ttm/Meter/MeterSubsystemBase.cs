using System;
using System.Diagnostics;

using isr.Core.NumericExtensions;
using isr.Core.StackTraceExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary>
    /// Defines the contract that must be implemented by Thermal Transient Meter Subsystems.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-12-12, 3.0.5093. </para>
    /// </remarks>
    public abstract class MeterSubsystemBase : SubsystemPlusStatusBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SubsystemPlusStatusBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        protected MeterSubsystemBase( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known clear state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineClearExecutionState()
        {
            _ = this.PublishVerbose( $"Clearing {this.EntityName};. " );
            base.DefineClearExecutionState();
            this.LastReading = string.Empty;
            this.LastOutcome = string.Empty;
            this.LastMeasurementStatus = string.Empty;
            this.MeasurementEventCondition = 0;
            this.MeasurementAvailable = false;
            // this is not required -- is done internally 
            // Me.Session.WriteLine("{0}:clear()", Me.EntityName)
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Sets the known reset (default) state. </summary>
        /// <remarks>
        /// Reads instrument defaults, applies the values to the instrument and reads them back as
        /// configuration values. This ensures that the instrument state is mirrored in code.
        /// </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            _ = this.PublishVerbose( $"Resetting {this.EntityName};. " );
            Core.ApplianceBase.DoEvents();
            this.ReadInstrumentDefaults();
            this.ApplyInstrumentDefaults();
            // this causes a problem requiring the make an initial resistance measurement before 
            // the thermal transient.
            // Me.Session.WriteLine("{0}:init()", Me.EntityName)
            this.QueryConfiguration();
            Core.ApplianceBase.DoEvents();
        }

        #endregion

        #region " DEVICE UNDER TEST RESISTANCE "

        /// <summary>
        /// Gets or sets the <see cref="ResistanceMeasureBase">part resistance element</see>.
        /// </summary>
        /// <value> The cold resistance. </value>
        public abstract ResistanceMeasureBase Resistance { get; }

        #endregion

        #region " ENTITY "

        /// <summary> Name of the global. </summary>
        public const string GlobalName = "_G";

        /// <summary> Name of the thermal transient base entity. </summary>
        public const string ThermalTransientBaseEntityName = "_G.ttm";

        /// <summary> Name of the final resistance entity. </summary>
        public const string FinalResistanceEntityName = "_G.ttm.fr";

        /// <summary> Name of the initial resistance entity. </summary>
        public const string InitialResistanceEntityName = "_G.ttm.ir";

        /// <summary> Name of the thermal transient entity. </summary>
        public const string ThermalTransientEntityName = "_G.ttm.tr";

        /// <summary> Name of the thermal transient estimator entity. </summary>
        public const string ThermalTransientEstimatorEntityName = "_G.ttm.est";

        /// <summary> The meter entity. </summary>
        private ThermalTransientMeterEntity _MeterEntity;

        /// <summary> Gets or sets the meter entity. </summary>
        /// <value> The meter entity. </value>
        public ThermalTransientMeterEntity MeterEntity
        {
            get => this._MeterEntity;

            set {
                if ( !value.Equals( this.MeterEntity ) )
                {
                    this._MeterEntity = value;
                    this.NotifyPropertyChanged();
                    switch ( value )
                    {
                        case ThermalTransientMeterEntity.FinalResistance:
                            {
                                this.EntityName = FinalResistanceEntityName;
                                this.BaseEntityName = ThermalTransientBaseEntityName;
                                this.DefaultsName = "_G.ttm.coldResistance.Defaults";
                                break;
                            }

                        case ThermalTransientMeterEntity.InitialResistance:
                            {
                                this.EntityName = InitialResistanceEntityName;
                                this.BaseEntityName = ThermalTransientBaseEntityName;
                                this.DefaultsName = "_G.ttm.coldResistance.Defaults";
                                break;
                            }

                        case ThermalTransientMeterEntity.Transient:
                            {
                                this.EntityName = ThermalTransientEntityName;
                                this.BaseEntityName = ThermalTransientBaseEntityName;
                                this.DefaultsName = "_G.ttm.trace.Defaults";
                                break;
                            }

                        case ThermalTransientMeterEntity.Estimator:
                            {
                                this.EntityName = ThermalTransientEstimatorEntityName;
                                this.BaseEntityName = ThermalTransientBaseEntityName;
                                this.DefaultsName = "_G.ttm.estimator.Defaults";
                                break;
                            }

                        case ThermalTransientMeterEntity.Shunt:
                            {
                                this.EntityName = GlobalName;
                                this.BaseEntityName = GlobalName;
                                this.DefaultsName = string.Empty;
                                break;
                            }

                        case ThermalTransientMeterEntity.None:
                            {
                                this.EntityName = ThermalTransientBaseEntityName;
                                this.BaseEntityName = GlobalName;
                                this.DefaultsName = string.Empty;
                                break;
                            }

                        default:
                            {
                                Debug.Assert( !Debugger.IsAttached, "Unhandled case" );
                                break;
                            }
                    }
                }
            }
        }

        /// <summary> Name of the defaults. </summary>
        private string _DefaultsName;

        /// <summary> Gets or sets the Defaults name. </summary>
        /// <value> The Source Measure Unit. </value>
        public string DefaultsName
        {
            get => this._DefaultsName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.DefaultsName ) )
                {
                    this._DefaultsName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the entity. </summary>
        private string _EntityName;

        /// <summary> Gets or sets the entity name. </summary>
        /// <value> The Source Measure Unit. </value>
        public string EntityName
        {
            get => this._EntityName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.EntityName ) )
                {
                    this._EntityName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the base entity. </summary>
        private string _BaseEntityName;

        /// <summary> Gets or sets the Base Entity name. </summary>
        /// <value> The Meter entity. </value>
        public string BaseEntityName
        {
            get => this._BaseEntityName;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.BaseEntityName ) )
                {
                    this._BaseEntityName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " SOURCE MEASURE UNIT "

        /// <summary> Queries if a given source measure unit exists. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sourceMeasureUnitName"> Name of the source measure unit, e.g., 'smua' or 'smub'. </param>
        /// <returns> <c>True</c> if the source measure unit exists; otherwise <c>False</c> </returns>
        public bool SourceMeasureUnitExists( string sourceMeasureUnitName )
        {
            this.Session.MakeTrueFalseReplyIfEmpty( string.Equals( sourceMeasureUnitName, "smua", StringComparison.OrdinalIgnoreCase ) );
            return !this.Session.IsNil( string.Format( "_G.localnode.{0}", sourceMeasureUnitName ) );
        }

        /// <summary> Requires source measure unit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// <c>True</c> if this entity requires the assignment of the source measure unit.
        /// </returns>
        public bool RequiresSourceMeasureUnit()
        {
            return this.MeterEntity == ThermalTransientMeterEntity.FinalResistance || this.MeterEntity == ThermalTransientMeterEntity.InitialResistance || this.MeterEntity == ThermalTransientMeterEntity.Transient;
        }

        /// <summary> Source measure unit. </summary>
        private string _SourceMeasureUnit;

        /// <summary> Gets or sets the cached Source Measure Unit. </summary>
        /// <value> The Source Measure Unit. </value>
        public virtual string SourceMeasureUnit
        {
            get => this._SourceMeasureUnit;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.SourceMeasureUnit ) )
                {
                    this._SourceMeasureUnit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Queries the Source Measure Unit. Also sets the <see cref="SourceMeasureUnit">Source Measure
        /// Unit</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <returns> The Source Measure Unit. </returns>
        public string QuerySourceMeasureUnit()
        {
            if ( this.RequiresSourceMeasureUnit() )
            {
                this.Session.MakeEmulatedReplyIfEmpty( true );
                if ( this.Session.IsStatementTrue( "{0}.smuI==_G.smua", this.EntityName ) )
                {
                    this.SourceMeasureUnit = "smua";
                }
                else if ( this.Session.IsStatementTrue( "{0}.smuI==_G.smub", this.EntityName ) )
                {
                    this.SourceMeasureUnit = "smub";
                }
                else
                {
                    this.SourceMeasureUnit = "unknown";
                    throw new Core.OperationFailedException( $"Failed reading Source Measure Unit;. {this.EntityName}.smuI is neither smua or smub." );
                }
            }

            return this.SourceMeasureUnit;
        }

        /// <summary> Programs the Source Measure Unit. Does not read back from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        ///                                      illegal values. </exception>
        /// <param name="value"> the Source Measure Unit, e.g., 'smua' or 'smub'. </param>
        /// <returns> The Source Measure Unit, e.g., 'smua' or 'smub'. </returns>
        public string WriteSourceMeasureUnit( string value )
        {
            if ( this.RequiresSourceMeasureUnit() )
            {
                _ = this.SourceMeasureUnitExists( value )
                    ? this.Session.WriteLine( "{0}:currentSourceChannelSetter('{1}')", this.EntityName, value )
                    : throw new ArgumentException( "This source measure unit name is invalid.", nameof( value ) );
            }

            this.SourceMeasureUnit = value;
            return this.SourceMeasureUnit;
        }

        /// <summary> Writes and reads back the Source Measure Unit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> the Source Measure Unit, e.g., 'smua' or 'smub'. </param>
        /// <returns> The Source Measure Unit, e.g., 'smua' or 'smub'. </returns>
        public string ApplySourceMeasureUnit( string value )
        {
            _ = this.WriteSourceMeasureUnit( value );
            return this.QuerySourceMeasureUnit();
        }

        #endregion

        #region " APERTURE "

        /// <summary> The Aperture. </summary>
        private double? _Aperture;

        /// <summary> Gets or sets the cached Source Aperture. </summary>
        /// <value> The Source Aperture. </value>
        public virtual double? Aperture
        {
            get => this._Aperture;

            protected set {
                this.Resistance.Aperture = value ?? 0d;
                if ( this.Aperture.Differs( value, 0.000001d ) )
                {
                    this._Aperture = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Aperture. </summary>
        /// <remarks>
        /// This command set the immediate output Aperture. The value is in Amperes. The immediate
        /// Aperture is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Source Aperture. </returns>
        public double? ApplyAperture( double value )
        {
            _ = this.WriteAperture( value );
            return this.QueryAperture();
        }

        /// <summary> Queries the Aperture. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Aperture or none if unknown. </returns>
        public double? QueryAperture()
        {
            const decimal printFormat = 7.4m;
            this.Aperture = this.Session.QueryPrint( this.Aperture.GetValueOrDefault( 1d / 60d ), printFormat, "{0}.aperture", this.EntityName );
            return this.Aperture;
        }

        /// <summary> Writes the source Aperture without reading back the value from the device. </summary>
        /// <remarks>
        /// This command sets the immediate output Aperture. The value is in Amperes. The immediate
        /// Aperture is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Source Aperture. </returns>
        public virtual double? WriteAperture( double value )
        {
            string details = string.Empty;
            if ( this.MeterEntity == ThermalTransientMeterEntity.Transient )
            {
                if ( !ThermalTransient.ValidateAperture( value, ref details ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( value ), details );
                }
            }
            else if ( !ColdResistance.ValidateAperture( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:apertureSetter({1})", this.EntityName, value );
            this.Aperture = value;
            return this.Aperture;
        }

        #endregion

        #region " CURRENT LEVEL "

        /// <summary> The Current Level. </summary>
        private double? _CurrentLevel;

        /// <summary> Gets or sets the cached Source Current Level. </summary>
        /// <value> The Source Current Level. </value>
        public virtual double? CurrentLevel
        {
            get => this._CurrentLevel;

            protected set {
                this.Resistance.CurrentLevel = value ?? 0d;
                if ( this.CurrentLevel.Differs( value, 0.000000001d ) )
                {
                    this._CurrentLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Current Level. </summary>
        /// <remarks>
        /// This command set the immediate output Current Level. The value is in Amperes. The immediate
        /// Current Level is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <param name="value"> The Current Level. </param>
        /// <returns> The Source Current Level. </returns>
        public double? ApplyCurrentLevel( double value )
        {
            _ = this.WriteCurrentLevel( value );
            return this.QueryCurrentLevel();
        }

        /// <summary> Queries the Current Level. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Current Level or none if unknown. </returns>
        public double? QueryCurrentLevel()
        {
            const decimal printFormat = 9.6m;
            this.CurrentLevel = this.Session.QueryPrint( this.CurrentLevel.GetValueOrDefault( 0.27d ), printFormat, "{0}.level", this.EntityName );
            return this.CurrentLevel;
        }

        /// <summary>
        /// Writes the source Current Level without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output Current Level. The value is in Amperes. The immediate
        /// CurrentLevel is the output current setting. At *RST, the current values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Current Level. </param>
        /// <returns> The Source Current Level. </returns>
        public double? WriteCurrentLevel( double value )
        {
            string details = string.Empty;
            if ( this.MeterEntity == ThermalTransientMeterEntity.Transient )
            {
                if ( !ThermalTransient.ValidateCurrentLevel( value, ref details ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( value ), details );
                }
            }
            else if ( !ColdResistance.ValidateCurrentLevel( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:levelSetter({1})", this.EntityName, value );
            this.CurrentLevel = value;
            return this.CurrentLevel;
        }

        #endregion

        #region " VOLTAGE LIMIT "

        /// <summary> The Voltage Limit. </summary>
        private double? _VoltageLimit;

        /// <summary> Gets or sets the cached Source Voltage Limit. </summary>
        /// <value> The Source Voltage Limit. </value>
        public virtual double? VoltageLimit
        {
            get => this._VoltageLimit;

            protected set {
                this.Resistance.VoltageLimit = value ?? 0d;
                if ( this.VoltageLimit.Differs( value, 0.000001d ) )
                {
                    this._VoltageLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Voltage Limit. </summary>
        /// <remarks>
        /// This command set the immediate output Voltage Limit. The value is in Volts. The immediate
        /// Voltage Limit is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Voltage Limit. </param>
        /// <returns> The Source Voltage Limit. </returns>
        public double? ApplyVoltageLimit( double value )
        {
            _ = this.WriteVoltageLimit( value );
            return this.QueryVoltageLimit();
        }

        /// <summary> Queries the Voltage Limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Voltage Limit or none if unknown. </returns>
        public double? QueryVoltageLimit()
        {
            const decimal printFormat = 9.6m;
            this.VoltageLimit = this.Session.QueryPrint( this.VoltageLimit.GetValueOrDefault( 0.1d ), printFormat, "{0}.limit", this.EntityName );
            return this.VoltageLimit;
        }

        /// <summary>
        /// Writes the source Voltage Limit without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output Voltage Limit. The value is in Volts. The immediate
        /// VoltageLimit is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Voltage Limit. </param>
        /// <returns> The Source Voltage Limit. </returns>
        public double? WriteVoltageLimit( double value )
        {
            string details = string.Empty;
            if ( this.MeterEntity == ThermalTransientMeterEntity.Transient )
            {
                if ( !ThermalTransient.ValidateVoltageLimit( value, ref details ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( value ), details );
                }
            }
            else if ( !ColdResistance.ValidateVoltageLimit( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:limitSetter({1})", this.EntityName, value );
            this.VoltageLimit = value;
            return this.VoltageLimit;
        }

        #endregion

        #region " HIGH LIMIT "

        /// <summary> The High Limit. </summary>
        private double? _HighLimit;

        /// <summary> Gets or sets the cached High Limit. </summary>
        /// <value> The High Limit. </value>
        public virtual double? HighLimit
        {
            get => this._HighLimit;

            protected set {
                this.Resistance.HighLimit = value ?? 0d;
                if ( this.HighLimit.Differs( value, 0.000001d ) )
                {
                    this._HighLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the High Limit. </summary>
        /// <remarks>
        /// This command set the immediate output High Limit. The value is in Ohms. The immediate High
        /// Limit is the output High setting. At *RST, the High values = 0.
        /// </remarks>
        /// <param name="value"> The High Limit. </param>
        /// <returns> The High Limit. </returns>
        public double? ApplyHighLimit( double value )
        {
            _ = this.WriteHighLimit( value );
            return this.QueryHighLimit();
        }

        /// <summary> Queries the High Limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The High Limit or none if unknown. </returns>
        public double? QueryHighLimit()
        {
            const decimal printFormat = 9.6m;
            this.HighLimit = this.Session.QueryPrint( this.HighLimit.GetValueOrDefault( 0.1d ), printFormat, "{0}.highLimit", this.EntityName );
            return this.HighLimit;
        }

        /// <summary> Writes the High Limit without reading back the value from the device. </summary>
        /// <remarks>
        /// This command sets the immediate output High Limit. The value is in Ohms. The immediate
        /// HighLimit is the output High setting. At *RST, the High values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The High Limit. </param>
        /// <returns> The High Limit. </returns>
        public double? WriteHighLimit( double value )
        {
            string details = string.Empty;
            if ( this.MeterEntity == ThermalTransientMeterEntity.Transient )
            {
                if ( !ThermalTransient.ValidateLimit( value, ref details ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( value ), details );
                }
            }
            else if ( !ColdResistance.ValidateLimit( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:highLimitSetter({1})", this.EntityName, value );
            this.HighLimit = value;
            return this.HighLimit;
        }

        #endregion

        #region " LOW LIMIT "

        /// <summary> The Low Limit. </summary>
        private double? _LowLimit;

        /// <summary> Gets or sets the cached Low Limit. </summary>
        /// <value> The Low Limit. </value>
        public virtual double? LowLimit
        {
            get => this._LowLimit;

            protected set {
                this.Resistance.LowLimit = value ?? 0d;
                if ( this.LowLimit.Differs( value, 0.000001d ) )
                {
                    this._LowLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Low Limit. </summary>
        /// <remarks>
        /// This command set the immediate output Low Limit. The value is in Ohms. The immediate Low
        /// Limit is the output Low setting. At *RST, the Low values = 0.
        /// </remarks>
        /// <param name="value"> The Low Limit. </param>
        /// <returns> The Low Limit. </returns>
        public double? ApplyLowLimit( double value )
        {
            _ = this.WriteLowLimit( value );
            return this.QueryLowLimit();
        }

        /// <summary> Queries the Low Limit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Low Limit or none if unknown. </returns>
        public double? QueryLowLimit()
        {
            const decimal printFormat = 9.6m;
            this.LowLimit = this.Session.QueryPrint( this.LowLimit.GetValueOrDefault( 0.01d ), printFormat, "{0}.lowLimit", this.EntityName );
            return this.LowLimit;
        }

        /// <summary> Writes the Low Limit without reading back the value from the device. </summary>
        /// <remarks>
        /// This command sets the immediate output Low Limit. The value is in Ohms. The immediate
        /// LowLimit is the output Low setting. At *RST, the Low values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Low Limit. </param>
        /// <returns> The Low Limit. </returns>
        public double? WriteLowLimit( double value )
        {
            string details = string.Empty;
            if ( this.MeterEntity == ThermalTransientMeterEntity.Transient )
            {
                if ( !ThermalTransient.ValidateLimit( value, ref details ) )
                {
                    throw new ArgumentOutOfRangeException( nameof( value ), details );
                }
            }
            else if ( !ColdResistance.ValidateLimit( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:lowLimitSetter({1})", this.EntityName, value );
            this.LowLimit = value;
            return this.LowLimit;
        }

        #endregion

        #region " CONFIGURE "

        /// <summary> Applies the instrument defaults. </summary>
        /// <remarks> This is required until the reset command gets implemented. </remarks>
        public virtual void ApplyInstrumentDefaults()
        {
            _ = this.PublishVerbose( $"Applying {this.EntityName} defaults;. " );
            Core.ApplianceBase.DoEvents();
            _ = this.StatusSubsystem.WriteLine( $"{this.EntityName}:currentSourceChannelSetter({this.DefaultsName}.smuI)" );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( $"{this.EntityName}:apertureSetter({this.DefaultsName}.aperture)" );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( $"{this.EntityName}:levelSetter({this.DefaultsName}.level)" );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( $"{this.EntityName}:limitSetter({this.DefaultsName}.limit)" );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( $"{this.EntityName}:lowLimitSetter({this.DefaultsName}.lowLimit)" );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( $"{this.EntityName}:highLimitSetter({this.DefaultsName}.highLimit)" );
            _ = this.Session.QueryOperationCompleted();
        }

        /// <summary> Reads instrument defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void ReadInstrumentDefaults()
        {
            _ = this.PublishVerbose( $"Reading {this.EntityName} defaults;. " );
            Core.ApplianceBase.DoEvents();
            if ( this.RequiresSourceMeasureUnit() )
            {
                My.MySettings.Default.SourceMeasureUnitDefault = this.Session.QueryTrimEnd( $"_G.print({this.DefaultsName}.smuI)" );
                if ( string.IsNullOrWhiteSpace( My.MySettings.Default.SourceMeasureUnitDefault ) )
                {
                    _ = this.PublishWarning( "failed reading default source measure unit name;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                }
            }

            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Applies changed meter configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The resistance. </param>
        public virtual void ConfigureChanged( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            if ( !resistance.ConfigurationEquals( this.Resistance ) )
            {
                _ = this.PublishVerbose( "Configuring {0} resistance measurement;. ", this.EntityName );
                Core.ApplianceBase.DoEvents();
                if ( !string.Equals( this.Resistance.SourceMeasureUnit, resistance.SourceMeasureUnit ) )
                {
                    _ = this.PublishVerbose( "Setting {0} source measure unit to {1};. ", this.EntityName, resistance.SourceMeasureUnit );
                    _ = this.ApplySourceMeasureUnit( resistance.SourceMeasureUnit );
                }

                if ( !this.Resistance.Aperture.Equals( resistance.Aperture ) )
                {
                    _ = this.PublishVerbose( "Setting {0} aperture to {1};. ", this.EntityName, resistance.Aperture );
                    _ = this.ApplyAperture( resistance.Aperture );
                }

                if ( !this.Resistance.CurrentLevel.Equals( resistance.CurrentLevel ) )
                {
                    _ = this.PublishVerbose( "Setting {0} Current Level to {1};. ", this.EntityName, resistance.CurrentLevel );
                    _ = this.ApplyCurrentLevel( resistance.CurrentLevel );
                }

                if ( !this.Resistance.HighLimit.Equals( resistance.HighLimit ) )
                {
                    _ = this.PublishVerbose( "Setting {0} high limit to {1};. ", this.EntityName, resistance.HighLimit );
                    _ = this.ApplyHighLimit( resistance.HighLimit );
                }

                if ( !this.Resistance.LowLimit.Equals( resistance.LowLimit ) )
                {
                    _ = this.PublishVerbose( "Setting {0} Low limit to {1};. ", this.EntityName, resistance.LowLimit );
                    _ = this.ApplyLowLimit( resistance.LowLimit );
                }

                if ( !this.Resistance.VoltageLimit.Equals( resistance.VoltageLimit ) )
                {
                    _ = this.PublishVerbose( "Setting {0} Voltage Limit to {1};. ", this.EntityName, resistance.VoltageLimit );
                    _ = this.ApplyVoltageLimit( resistance.VoltageLimit );
                }

                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Configures the meter for making the resistance measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The resistance. </param>
        public virtual void Configure( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            _ = this.PublishVerbose( "Configuring {0} resistance measurement;. ", this.EntityName );
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Setting {0} source measure unit to {1};. ", this.EntityName, resistance.SourceMeasureUnit );
            _ = this.ApplySourceMeasureUnit( resistance.SourceMeasureUnit );
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Setting {0} aperture to {1};. ", this.EntityName, resistance.Aperture );
            _ = this.ApplyAperture( resistance.Aperture );
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Setting {0} Current Level to {1};. ", this.EntityName, resistance.CurrentLevel );
            _ = this.ApplyCurrentLevel( resistance.CurrentLevel );
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Setting {0} high limit to {1};. ", this.EntityName, resistance.HighLimit );
            _ = this.ApplyHighLimit( resistance.HighLimit );
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Setting {0} Low limit to {1};. ", this.EntityName, resistance.LowLimit );
            _ = this.ApplyLowLimit( resistance.LowLimit );
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Setting {0} Voltage Limit to {1};. ", this.EntityName, resistance.VoltageLimit );
            _ = this.ApplyVoltageLimit( resistance.VoltageLimit );
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Queries the configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void QueryConfiguration()
        {
            _ = this.PublishVerbose( "Reading {0} meter configuration;. ", this.EntityName );
            Core.ApplianceBase.DoEvents();
            _ = this.QuerySourceMeasureUnit();
            _ = this.QueryAperture();
            _ = this.QueryCurrentLevel();
            _ = this.QueryHighLimit();
            _ = this.QueryLowLimit();
            _ = this.QueryVoltageLimit();
        }

        #endregion

        #region " MEASURE "

        /// <summary> Measures the Final resistance and returns. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void Measure()
        {
            this.DefineClearExecutionState();
            this.Session.EnableServiceRequestWaitComplete();
            switch ( this.MeterEntity )
            {
                case ThermalTransientMeterEntity.FinalResistance:
                    {
                        _ = this.Session.WriteLine( "_G.ttm.measureFinalResistance() waitcomplete()" );
                        break;
                    }

                case ThermalTransientMeterEntity.InitialResistance:
                    {
                        _ = this.Session.WriteLine( "_G.ttm.measureInitialResistance() waitcomplete()" );
                        break;
                    }

                case ThermalTransientMeterEntity.Transient:
                    {
                        _ = this.Session.WriteLine( "_G.ttm.measureThermalTransient() waitcomplete()" );
                        break;
                    }

                default:
                    {
                        break;
                    }
            }

            switch ( this.MeterEntity )
            {
                case ThermalTransientMeterEntity.FinalResistance:
                case ThermalTransientMeterEntity.InitialResistance:
                case ThermalTransientMeterEntity.Transient:
                    {
                        _ = this.Session.QueryOperationCompleted();
                        this.StatusSubsystem.CheckThrowDeviceException( false, "Measuring Final Resistance;. " );
                        break;
                    }

                default:
                    {
                        break;
                    }
            }
        }

        /// <summary> Measures the Thermal Transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The Thermal Transient element. </param>
        public void Measure( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            resistance.DefineClearExecutionState();
            this.Measure();
            this.StatusSubsystem.CheckThrowDeviceException( false, "Measuring Thermal Transient;. last command: '{0}'", this.Session.LastMessageSent );
        }

        #endregion

        #region " OKAY "

        /// <summary> Queries the meter okay sentinel. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if entity okay status is True; otherwise, <c>False</c>. </returns>
        public bool QueryOkay()
        {
            this.Session.MakeEmulatedReplyIfEmpty( true );
            return this.Session.IsStatementTrue( "{0}:isOkay()", this.EntityName );
        }

        #endregion

        #region " OUTCOME "

        /// <summary> Queries the meter outcome status. If nil, measurement was not made. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if entity outcome is nil; otherwise, <c>False</c>. </returns>
        public bool QueryOutcomeNil()
        {
            this.Session.MakeEmulatedReplyIfEmpty( true );
            return this.Session.IsStatementTrue( "{0}.outcome==nil", this.EntityName );
        }

        #endregion

        #region " READ "

        /// <summary> The last reading. </summary>
        private string _LastReading;

        /// <summary> Gets or sets (protected) the last reading. </summary>
        /// <value> The last reading. </value>
        public string LastReading
        {
            get => this._LastReading;

            protected set {
                this._LastReading = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The last outcome. </summary>
        private string _LastOutcome;

        /// <summary> Gets or sets (protected) the last outcome. </summary>
        /// <value> The last outcome. </value>
        public string LastOutcome
        {
            get => this._LastOutcome;

            protected set {
                this._LastOutcome = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The last measurement status. </summary>
        private string _LastMeasurementStatus;

        /// <summary> Gets or sets (protected) the last measurement status. </summary>
        /// <value> The last measurement status. </value>
        public string LastMeasurementStatus
        {
            get => this._LastMeasurementStatus;

            protected set {
                this._LastMeasurementStatus = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> True if measurement available. </summary>
        private bool _MeasurementAvailable;

        /// <summary> Gets or sets (protected) the measurement available. </summary>
        /// <value> The measurement available. </value>
        public bool MeasurementAvailable
        {
            get => this._MeasurementAvailable;

            set {
                this._MeasurementAvailable = value;
                this.SyncNotifyPropertyChanged();
            }
        }

        /// <summary> The measurement event Condition. </summary>
        private int _MeasurementEventCondition;

        /// <summary> Gets or sets the cached Condition of the measurement register events. </summary>
        /// <value> <c>null</c> if value is not known;. </value>
        public int MeasurementEventCondition
        {
            get => this._MeasurementEventCondition;

            protected set {
                if ( !this.MeasurementEventCondition.Equals( value ) )
                {
                    this._MeasurementEventCondition = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Reads the condition of the measurement register event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> System.Nullable{System.Int32}. </returns>
        public bool TryQueryMeasurementEventCondition()
        {
            int argvalue = this.MeasurementEventCondition;
            return this.Session.TryQueryPrint( 1, ref argvalue, "_G.status.measurement.condition" );
            // this.MeasurementEventCondition = argvalue;
        }

        /// <summary> Parses the outcome. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> The parsed outcome. </returns>
        public MeasurementOutcomes ParseOutcome( ref string details )
        {
            var outcome = MeasurementOutcomes.None;
            var detailsBuilder = new System.Text.StringBuilder();
            var measurementCondition = default( int );
            if ( string.IsNullOrWhiteSpace( this.LastOutcome ) )
            {
                outcome |= MeasurementOutcomes.MeasurementNotMade;
                _ = detailsBuilder.AppendFormat( "Failed parsing outcome -- outcome is nothing indicating measurement not made;. " );
            }
            else if ( int.TryParse( this.LastOutcome, System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out int numericValue ) )
            {
                if ( numericValue < 0 )
                {
                    if ( detailsBuilder.Length > 0 )
                        _ = detailsBuilder.AppendLine();
                    _ = detailsBuilder.AppendFormat( "Failed parsing outcome '{0}';. ", this.LastOutcome );
                    outcome = MeasurementOutcomes.MeasurementFailed;
                    outcome |= MeasurementOutcomes.UnexpectedOutcomeFormat;
                }
                // interpret the outcome.
                else if ( numericValue != 0 )
                {
                    outcome = MeasurementOutcomes.MeasurementFailed;
                    if ( (numericValue & 1) == 1 )
                    {
                        // this is a bad status - could be compliance or other things.
                        if ( !string.IsNullOrWhiteSpace( this.LastMeasurementStatus ) )
                        {
                            if ( detailsBuilder.Length > 0 )
                                _ = detailsBuilder.AppendLine();
                            _ = detailsBuilder.AppendFormat( "Outcome status={0};. ", this.LastMeasurementStatus );
                        }

                        if ( detailsBuilder.Length > 0 )
                            _ = detailsBuilder.AppendLine();
                        if ( this.TryQueryMeasurementEventCondition() )
                        {
                            measurementCondition = this.MeasurementEventCondition;
                            _ = detailsBuilder.AppendFormat( "Measurement condition=0x{0:X4};. ", measurementCondition );
                        }
                        else
                        {
                            measurementCondition = 0xFFFF;
                            _ = detailsBuilder.AppendFormat( "Failed getting measurement condition. Set to 0x{0:X4};. ", measurementCondition );
                        }

                        outcome |= MeasurementOutcomes.HitCompliance;
                    }

                    if ( (numericValue & 2) != 0 )
                    {
                        if ( detailsBuilder.Length > 0 )
                            _ = detailsBuilder.AppendLine();
                        _ = detailsBuilder.AppendFormat( "Sampling returned bad time stamps;. Device reported outcome=0x{0:X4} and status measurement condition=0x{1:X4}", numericValue, measurementCondition );
                        outcome |= MeasurementOutcomes.UnspecifiedStatusException;
                    }

                    if ( (numericValue & 4) != 0 )
                    {
                        if ( detailsBuilder.Length > 0 )
                            _ = detailsBuilder.AppendLine();
                        _ = detailsBuilder.AppendFormat( "Configuration failed;. Device reported outcome=0x{0:X4} and measurement condition=0x{1:X4}", numericValue, measurementCondition );
                        outcome |= MeasurementOutcomes.UnspecifiedStatusException;
                    }

                    if ( (numericValue & 8) != 0 )
                    {
                        if ( detailsBuilder.Length > 0 )
                            _ = detailsBuilder.AppendLine();
                        _ = detailsBuilder.AppendFormat( "Initiation failed;. Device reported outcome=0x{0:X4} and status measurement condition=0x{1:X4}", numericValue, measurementCondition );
                        outcome |= MeasurementOutcomes.UnspecifiedStatusException;
                    }

                    if ( (numericValue & 16) != 0 )
                    {
                        if ( detailsBuilder.Length > 0 )
                            _ = detailsBuilder.AppendLine();
                        _ = detailsBuilder.AppendFormat( "Load failed;. Device reported outcome=0x{0:X4} and measurement condition=0x{1:X4}", numericValue, measurementCondition );
                        outcome |= MeasurementOutcomes.UnspecifiedStatusException;
                    }

                    if ( (numericValue & ~31) != 0 )
                    {
                        if ( detailsBuilder.Length > 0 )
                            _ = detailsBuilder.AppendLine();
                        _ = detailsBuilder.AppendFormat( "Unknown failure;. Device reported outcome=0x{0:X4} and measurement condition=0x{1:X4}", numericValue, measurementCondition );
                        outcome |= MeasurementOutcomes.UnspecifiedStatusException;
                    }
                }
            }
            else
            {
                if ( detailsBuilder.Length > 0 )
                    _ = detailsBuilder.AppendLine();
                _ = detailsBuilder.AppendFormat( "Failed parsing outcome '{0}';. {1}{2}.", this.LastOutcome, Environment.NewLine, new StackFrame( true ).UserCallStack() );
                outcome = MeasurementOutcomes.MeasurementFailed | MeasurementOutcomes.UnexpectedReadingFormat;
            }

            details = detailsBuilder.ToString();
            return outcome;
        }

        #endregion

    }
}
