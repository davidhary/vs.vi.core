using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core.Models;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary>
    /// Defines the thermal transient meter including measurement and configuration. The Thermal
    /// Transient meter includes the elements that define a complete set of thermal transient
    /// measurements and settings.
    /// </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2010-09-17, 2.2.3912.x. </para>
    /// </remarks>
    public partial class Meter : ViewModelTalkerBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public Meter() : base()
        {
            this.MasterDeviceInternal = new K2600Device();
            My.MySettings.Default.PropertyChanged += this.MySettings_PropertyChanged;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static Meter Create()
        {
            Meter device;
            try
            {
                device = new Meter();
            }
            catch
            {
                throw;
            }

            return device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets the disposed status. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    try
                    {
                        this.OnClosing();
                        if ( this.MasterDeviceInternal is null )
                        {
                            this.MasterDeviceInternal.Dispose();
                            this.MasterDeviceInternal = null;
                        }
                    }
                    catch ( Exception ex )
                    {
                        Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary> Finalizes this object. </summary>
        /// <remarks>
        /// David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
        /// unmanaged resources.
        /// </remarks>
        ~Meter()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( false );
        }

        #endregion


        #endregion

        #region " MASTER DEVICE "

        private K2600Device _MasterDeviceInternal;

        private K2600Device MasterDeviceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MasterDeviceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MasterDeviceInternal != null )
                {
                    this._MasterDeviceInternal.Closing -= this.MasterDevice_Closing;
                    this._MasterDeviceInternal.Closed -= this.MasterDevice_Closed;
                    this._MasterDeviceInternal.Opening -= this.MasterDevice_Opening;
                    this._MasterDeviceInternal.Opened -= this.MasterDevice_Opened;
                    this._MasterDeviceInternal.Initializing -= this.MasterDevice_Initializing;
                    this._MasterDeviceInternal.Initialized -= this.MasterDevice_Initialized;
                    this._MasterDeviceInternal.PropertyChanged -= this.MasterDevice_PropertyChanged;
                }

                this._MasterDeviceInternal = value;
                if ( this._MasterDeviceInternal != null )
                {
                    this._MasterDeviceInternal.Closing += this.MasterDevice_Closing;
                    this._MasterDeviceInternal.Closed += this.MasterDevice_Closed;
                    this._MasterDeviceInternal.Opening += this.MasterDevice_Opening;
                    this._MasterDeviceInternal.Opened += this.MasterDevice_Opened;
                    this._MasterDeviceInternal.Initializing += this.MasterDevice_Initializing;
                    this._MasterDeviceInternal.Initialized += this.MasterDevice_Initialized;
                    this._MasterDeviceInternal.PropertyChanged += this.MasterDevice_PropertyChanged;
                }
            }
        }

        /// <summary> Gets the master device. </summary>
        /// <value> The master device. </value>
        public K2600Device MasterDevice => this.MasterDeviceInternal;

        /// <summary> Gets a value indicating whether the subsystem has an open Device open. </summary>
        /// <value> <c>True</c> if the device has an open Device; otherwise, <c>False</c>. </value>
        public bool IsDeviceOpen => this.MasterDeviceInternal is object && this.MasterDevice.IsDeviceOpen;

        /// <summary> Gets a value indicating whether the subsystem has an open session open. </summary>
        /// <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
        public bool IsSessionOpen => this.MasterDeviceInternal is object && this.MasterDevice.Session.IsSessionOpen;

        /// <summary> Gets the name of the resource. </summary>
        /// <value> The name of the resource or &lt;closed&gt; if not open. </value>
        public string ResourceName => this.MasterDevice is null
                    ? "<closed>"
                    : this.MasterDevice.IsDeviceOpen ? this.MasterDevice.OpenResourceName : this.MasterDevice.CandidateResourceName;

        /// <summary> Disposes of the local elements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnClosing()
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} handling closing meter";
                try
                {
                    this.AbortTriggerSequenceIf();
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }

                try
                {
                    if ( this.MeasureSequencerInternal is object )
                    {
                        this.MeasureSequencerInternal.Dispose();
                        this.MeasureSequencerInternal = null;
                    }
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }

                try
                {
                    if ( this.TriggerSequencerInternal is object )
                    {
                        this.TriggerSequencerInternal.Dispose();
                        this.TriggerSequencerInternal = null;
                    }
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }

                try
                {
                    this.BindShuntResistance( null );
                    this.BindInitialResistance( null );
                    this.BindFinalResistance( null );
                    this.BindThermalTransient( null );
                    this.BindThermalTransientEstimator( null );
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }

                try
                {
                    this.ConfigInfo = null;
                }
                catch ( Exception ex )
                {
                    Debug.Assert( !Debugger.IsAttached, ex.ToString() );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Called by the master device when stating the closing sequence before disposing of the
        /// subsystems.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MasterDevice_Closing( object sender, EventArgs e )
        {
            if ( sender is object && this.IsDeviceOpen )
                this.OnClosing();
        }

        /// <summary>
        /// Event handler. Called by the Master Device when closed after all subsystems were disposed.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MasterDevice_Closed( object sender, EventArgs e )
        {
        }

        /// <summary>
        /// Event handler. Called by the Master Device before initializing the subsystems.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MasterDevice_Opening( object sender, EventArgs e )
        {
            this.MeasureSequencer = new MeasureSequencer();
            this.TriggerSequencer = new TriggerSequencer();
        }

        /// <summary>
        /// Event handler. Called by MasterDevice when opened after initializing the subsystems.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MasterDevice_Opened( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} handling master device opening event";
                this.ConfigInfo = new DeviceUnderTest();
                // initial resistance must be first.
                this.BindInitialResistance( new MeterColdResistance( this.MasterDevice.StatusSubsystem, this.ConfigInfo.InitialResistance, ThermalTransientMeterEntity.InitialResistance ) );
                this.BindFinalResistance( new MeterColdResistance( this.MasterDevice.StatusSubsystem, this.ConfigInfo.FinalResistance, ThermalTransientMeterEntity.FinalResistance ) );
                this.BindShuntResistance( new ShuntResistance() );
                this.BindThermalTransientEstimator( new ThermalTransientEstimator( this.MasterDevice.StatusSubsystem ) );
                this.BindThermalTransient( new MeterThermalTransient( this.MasterDevice.StatusSubsystem, this.ConfigInfo.ThermalTransient ) );
                this.MasterDevice.AddSubsystem( this.ThermalTransient );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Master device initializing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MasterDevice_Initializing( object sender, System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} initializing master device";
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Master device initialized. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MasterDevice_Initialized( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} initialized master device";
                this.ApplySettings();
                this.ShuntResistance.AddListeners( this.Talker );
            }
            // this is already done.  Me.MasterDevice.AddListeners(Me.Talker)
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Raises the system. component model. property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( K2600Device sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2600Device.IsDeviceOpen ):
                    {
                        _ = sender.IsDeviceOpen
                            ? this.PublishInfo( "{0} open;. ", sender.ResourceNameCaption )
                            : this.PublishInfo( "{0} close;. ", sender.ResourceNameCaption );

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _MasterDevice for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MasterDevice_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} handling master device {e?.PropertyName} property changed event";
                this.OnPropertyChanged( sender as K2600Device, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets values to their known clear execution state. </summary>
        /// <remarks> This erases the last reading. </remarks>
        public virtual void DefineClearExecutionState()
        {
            _ = this.PublishVerbose( "Clearing device under test execution state;. " );
            Core.ApplianceBase.DoEvents();
            this.ConfigInfo.DefineClearExecutionState();
            _ = this.PublishVerbose( "Clearing shunt execution state;. " );
            Core.ApplianceBase.DoEvents();
            this.ShuntResistance.DefineClearExecutionState();
            _ = this.PublishVerbose( "Displaying title;. " );
            this.MasterDevice.DisplaySubsystem.DisplayTitle( "TTM 2016", "Integrated Scientific Resources" );
            Core.ApplianceBase.DoEvents();
        }

        /// <summary>
        /// Clears the queues and resets all registers to zero. Sets system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks> *CLS. </remarks>
        public void ClearExecutionState()
        {
            _ = this.PublishVerbose( "Clearing master device execution state;. " );
            Core.ApplianceBase.DoEvents();
            this.MasterDevice.ClearExecutionState();
            this.DefineClearExecutionState();
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public virtual void InitKnownState()
        {
            _ = this.PublishVerbose( "Initializing part configuration  to known state;. " );
            Core.ApplianceBase.DoEvents();
            this.ConfigInfo.InitKnownState();
            _ = this.PublishVerbose( "Initializing shunt to known state;. " );
            Core.ApplianceBase.DoEvents();
            this.ShuntResistance.InitKnownState();
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Initializing master device to known state;. " );
            this.MasterDevice.InitKnownState();
        }

        /// <summary> Sets the known preset state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void PresetKnownState()
        {
            this.ConfigInfo.PresetKnownState();
        }

        /// <summary> Preforms a full reset, initialize and clear. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ResetClear()
        {
            this.ResetKnownState();
            this.InitKnownState();
            this.ClearExecutionState();
        }

        /// <summary> Sets the known reset (default) state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void ResetKnownState()
        {
            _ = this.PublishVerbose( "Resetting part configuration to known state;. " );
            Core.ApplianceBase.DoEvents();
            this.ConfigInfo.ResetKnownState();
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Resetting shunt to known state;. " );
            Core.ApplianceBase.DoEvents();
            this.ShuntResistance.ResetKnownState();
            Core.ApplianceBase.DoEvents();
            _ = this.PublishVerbose( "Resetting master device to known state;. " );
            this.MasterDevice.ResetKnownState();
            Core.ApplianceBase.DoEvents();
            this._IsStarting = true;
        }

        #endregion

        #region " SUBSYSTEMS: MEASUREMENT ELEMENTS "

        /// <summary>
        /// Gets or sets reference to the <see cref="MeterColdResistance">meter initial cold
        /// resistance</see>
        /// </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <value> The initial resistance. </value>
        public MeterColdResistance InitialResistance { get; private set; }

        /// <summary> Binds the initial resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindInitialResistance( MeterColdResistance subsystem )
        {
            if ( this.InitialResistance is object )
            {
                this.MasterDevice.RemoveSubsystem( this.InitialResistance );
                this.InitialResistance = null;
            }

            this.InitialResistance = subsystem;
            if ( this.InitialResistance is object )
            {
                this.MasterDevice.AddSubsystem( this.InitialResistance );
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="MeterColdResistance">meter Final cold
        /// resistance</see>
        /// </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <value> The Final resistance. </value>
        public MeterColdResistance FinalResistance { get; private set; }

        /// <summary> Binds the Final resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindFinalResistance( MeterColdResistance subsystem )
        {
            if ( this.FinalResistance is object )
            {
                this.MasterDevice.RemoveSubsystem( this.FinalResistance );
                this.FinalResistance = null;
            }

            this.FinalResistance = subsystem;
            if ( this.FinalResistance is object )
            {
                this.MasterDevice.AddSubsystem( this.FinalResistance );
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="ShuntResistance">Shunt resistance</see>
        /// </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <value> The shunt resistance. </value>
        public ShuntResistance ShuntResistance { get; private set; }

        /// <summary> Binds the Shunt resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindShuntResistance( ShuntResistance subsystem )
        {
            if ( this.ShuntResistance is object )
            {
                this.ShuntResistance = null;
            }

            this.ShuntResistance = subsystem;
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="MeterThermalTransient">meter thermal transient</see>
        /// </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <value> The thermal transient. </value>
        public MeterThermalTransient ThermalTransient { get; private set; }

        /// <summary> Binds the Final resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindThermalTransient( MeterThermalTransient subsystem )
        {
            if ( this.ThermalTransient is object )
            {
                this.MasterDevice.RemoveSubsystem( this.ThermalTransient );
                this.ThermalTransient = null;
            }

            this.ThermalTransient = subsystem;
            if ( this.ThermalTransient is object )
            {
                this.MasterDevice.AddSubsystem( this.ThermalTransient );
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="ThermalTransientEstimator">thermal transient
        /// estiamtor</see>
        /// </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <value> The thermal transient estimator. </value>
        public ThermalTransientEstimator ThermalTransientEstimator { get; private set; }

        /// <summary> Binds the Final resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindThermalTransientEstimator( ThermalTransientEstimator subsystem )
        {
            if ( this.ThermalTransientEstimator is object )
            {
                this.MasterDevice.RemoveSubsystem( this.ThermalTransientEstimator );
                this.ThermalTransientEstimator = null;
            }

            this.ThermalTransientEstimator = subsystem;
            if ( this.ThermalTransientEstimator is object )
            {
                this.MasterDevice.AddSubsystem( this.ThermalTransientEstimator );
            }
        }

        #endregion

        #region " CONFIGURE"

        /// <summary> Gets or sets the <see cref="ConfigInfo">Device under test</see>. </summary>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <value> Information describing the configuration. </value>
        public DeviceUnderTest ConfigInfo { get; set; }

        /// <summary> Configure part information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="configurationInformation"> The <see cref="ConfigInfo">Device under test</see>. </param>
        public void ConfigurePartInfo( DeviceUnderTest configurationInformation )
        {
            if ( configurationInformation is null )
                throw new ArgumentNullException( nameof( configurationInformation ) );
            this.ConfigInfo.PartNumber = configurationInformation.PartNumber;
            this.ConfigInfo.OperatorId = configurationInformation.OperatorId;
            this.ConfigInfo.LotId = configurationInformation.LotId;
            this.ConfigInfo.ContactCheckEnabled = configurationInformation.ContactCheckEnabled;
            this.ConfigInfo.ContactCheckThreshold = configurationInformation.ContactCheckThreshold;
        }

        /// <summary> Configures the given device under test. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="configurationInformation"> The <see cref="ConfigInfo">Device under test</see>. </param>
        public void Configure( DeviceUnderTest configurationInformation )
        {
            if ( configurationInformation is null )
            {
                throw new ArgumentNullException( nameof( configurationInformation ) );
            }
            else if ( configurationInformation.InitialResistance is null )
            {
                throw new InvalidOperationException( "Initial Resistance null detected in device under test." );
            }
            else if ( configurationInformation.ThermalTransient is null )
            {
                throw new InvalidOperationException( "Thermal Transient null detected in device under test." );
            }

            if ( !this.ConfigInfo.SourceMeasureUnitEquals( configurationInformation ) )
            {
                if ( !this.InitialResistance.SourceMeasureUnitExists( this.ConfigInfo.InitialResistance.SourceMeasureUnit ) )
                {
                    throw new InvalidOperationException( string.Format( "Source measure unit name {0} is invalid.", this.ConfigInfo.InitialResistance.SourceMeasureUnit ) );
                }
            }

            string details = string.Empty;
            if ( !Ttm.ThermalTransient.ValidateVoltageLimit( configurationInformation.ThermalTransient.VoltageLimit, configurationInformation.InitialResistance.HighLimit, configurationInformation.ThermalTransient.CurrentLevel, configurationInformation.ThermalTransient.AllowedVoltageChange, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( configurationInformation ), details );
            }

            this.ConfigurePartInfo( configurationInformation );
            this.InitialResistance.Configure( configurationInformation.InitialResistance );
            this.FinalResistance.Configure( configurationInformation.FinalResistance );
            this.ThermalTransient.Configure( configurationInformation.ThermalTransient );
        }

        /// <summary> Configures the given device under test for changed values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        ///                                                are null. </exception>
        /// <exception cref="InvalidOperationException">   Thrown when the requested operation is
        ///                                                invalid. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="configurationInformation"> The <see cref="ConfigInfo">Device under test</see>. </param>
        public void ConfigureChanged( DeviceUnderTest configurationInformation )
        {
            if ( configurationInformation is null )
            {
                throw new ArgumentNullException( nameof( configurationInformation ) );
            }
            else if ( configurationInformation.InitialResistance is null )
            {
                throw new InvalidOperationException( "Initial Resistance null detected in device under test." );
            }
            else if ( configurationInformation.ThermalTransient is null )
            {
                throw new InvalidOperationException( "Thermal Transient null detected in device under test." );
            }

            if ( !this.ConfigInfo.SourceMeasureUnitEquals( configurationInformation ) )
            {
                if ( !this.InitialResistance.SourceMeasureUnitExists( this.ConfigInfo.InitialResistance.SourceMeasureUnit ) )
                {
                    throw new InvalidOperationException( string.Format( "Source measure unit name {0} is invalid.", this.ConfigInfo.InitialResistance.SourceMeasureUnit ) );
                }
            }

            string details = string.Empty;
            if ( !Ttm.ThermalTransient.ValidateVoltageLimit( configurationInformation.ThermalTransient.VoltageLimit, configurationInformation.InitialResistance.HighLimit, configurationInformation.ThermalTransient.CurrentLevel, configurationInformation.ThermalTransient.AllowedVoltageChange, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( configurationInformation ), details );
            }

            this.ConfigurePartInfo( configurationInformation );
            this.InitialResistance.ConfigureChanged( configurationInformation.InitialResistance );
            this.FinalResistance.ConfigureChanged( configurationInformation.FinalResistance );
            this.ThermalTransient.ConfigureChanged( configurationInformation.ThermalTransient );
        }

        #endregion

        #region " TTM FRAMEWORK: SOURCE CHANNEL "

        /// <summary> Source measure unit. </summary>
        private string _SourceMeasureUnit;

        /// <summary> Gets or sets (protected) the source measure unit "smua" or "smub". </summary>
        /// <value> The source measure unit. </value>
        public string SourceMeasureUnit
        {
            get => this._SourceMeasureUnit;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.SourceMeasureUnit ) )
                {
                    this._SourceMeasureUnit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Programs the Source Measure Unit. Does not read back from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="value"> the Source Measure Unit, e.g., 'smua' or 'smub'. </param>
        /// <returns> The Source Measure Unit, e.g., 'smua' or 'smub'. </returns>
        public string WriteSourceMeasureUnit( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                throw new ArgumentNullException( value );
            }

            string unitNumber = value.Substring( value.Length - 1 );
            this.MasterDevice.SourceMeasureUnit.UnitNumber = unitNumber;
            _ = this.InitialResistance.WriteSourceMeasureUnit( value );
            _ = this.FinalResistance.WriteSourceMeasureUnit( value );
            _ = this.ThermalTransient.WriteSourceMeasureUnit( value );
            this.SourceMeasureUnit = value;
            return this.SourceMeasureUnit;
        }

        #endregion

        #region " TTM FRAMEWORK: METERS FIRMWARE "

        /// <summary> Checks if the firmware version command exists. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// <c>True</c> if the firmware version command exists; otherwise, <c>False</c>.
        /// </returns>
        public bool FirmwareVersionQueryCommandExists()
        {
            return this.IsSessionOpen && !this.MasterDevice.Session.IsNil( TtmSyntax.Meters.ThermalTransient.VersionQueryCommand.TrimEnd( "()".ToCharArray() ) );
        }

        /// <summary> The firmware released version. </summary>
        private string _FirmwareReleasedVersion;

        /// <summary> Gets or sets the version of the current firmware release. </summary>
        /// <value> The firmware released version. </value>
        public string FirmwareReleasedVersion
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._FirmwareReleasedVersion ) )
                {
                    _ = this.QueryFirmwareVersion();
                }

                return this._FirmwareReleasedVersion;
            }

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.FirmwareReleasedVersion ) )
                {
                    this._FirmwareReleasedVersion = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Queries the embedded firmware version from a remote node and saves it to
        /// <see cref="FirmwareReleasedVersion">the firmware version cache.</see>
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The firmware version. </returns>
        public string QueryFirmwareVersion()
        {
            this.FirmwareReleasedVersion = this.FirmwareVersionQueryCommandExists() ? this.MasterDevice.Session.QueryTrimEnd( TtmSyntax.Meters.ThermalTransient.VersionQueryCommand ) : Syntax.Lua.NilValue;
            return this.FirmwareReleasedVersion;
        }

        #endregion

        #region " TTM FRAMEWORK: SHUNT "

        /// <summary> Configures the meter for making Shunt Resistance measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The shunt resistance. </param>
        public void ConfigureShuntResistance( ShuntResistanceBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            _ = this.PublishVerbose( "Configuring shunt resistance measurement;. " );
            _ = this.PublishVerbose( "Setting {0} to DC current source;. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.SourceSubsystem.ApplySourceFunction( SourceFunctionMode.CurrentDC );
            _ = this.PublishVerbose( "Setting {0} to DC current range {1};. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.CurrentSourceSubsystem.ApplyRange( resistance.CurrentRange );
            this.ShuntResistance.CurrentRange = this.MasterDevice.CurrentSourceSubsystem.Range.GetValueOrDefault( 0d );
            _ = this.PublishVerbose( "Setting {0} to DC current Level {1};. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.CurrentSourceSubsystem.ApplyLevel( resistance.CurrentLevel );
            this.ShuntResistance.CurrentLevel = this.MasterDevice.CurrentSourceSubsystem.Level.GetValueOrDefault( 0d );
            _ = this.PublishVerbose( "Setting {0} to DC current Voltage Limit {1};. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.CurrentSourceSubsystem.ApplyVoltageLimit( resistance.VoltageLimit );
            this.ShuntResistance.VoltageLimit = this.MasterDevice.CurrentSourceSubsystem.VoltageLimit.GetValueOrDefault( 0d );
            _ = this.PublishVerbose( "Setting {0} to four wire sense;. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.SenseSubsystem.ApplySenseMode( SenseActionMode.Remote );
            _ = this.PublishVerbose( "Setting {0} to auto voltage range;. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.MeasureVoltageSubsystem.ApplyAutoRangeVoltageEnabled( true );
            this.MasterDevice.DisplaySubsystem.ClearDisplay();
            this.MasterDevice.DisplaySubsystem.DisplayLine( 2, string.Format( System.Globalization.CultureInfo.CurrentCulture, "SrcI:{0}A LimV:{1}V", this.ShuntResistance.CurrentLevel, this.ShuntResistance.VoltageLimit ) );
            this.MasterDevice.StatusSubsystem.CheckThrowDeviceException( false, "configuring shunt resistance measurement;. " );
            this.ShuntResistance.CheckThrowUnequalConfiguration( resistance );
        }

        /// <summary> Apply changed Shunt Resistance configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The shunt resistance. </param>
        public void ConfigureShuntResistanceChanged( ShuntResistanceBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            _ = this.PublishVerbose( "Setting {0} to DC current source;. ", this.MasterDevice.SourceMeasureUnit );
            _ = this.MasterDevice.SourceSubsystem.ApplySourceFunction( SourceFunctionMode.CurrentDC );
            if ( !this.ShuntResistance.ConfigurationEquals( resistance ) )
            {
                _ = this.PublishVerbose( "Configuring shunt resistance measurement;. " );
                if ( !this.ShuntResistance.CurrentRange.Equals( resistance.CurrentRange ) )
                {
                    _ = this.PublishVerbose( "Setting {0} to DC current range {1};. ", this.MasterDevice.SourceMeasureUnit );
                    _ = this.MasterDevice.CurrentSourceSubsystem.ApplyRange( resistance.CurrentRange );
                    this.ShuntResistance.CurrentRange = this.MasterDevice.CurrentSourceSubsystem.Range.GetValueOrDefault( 0d );
                }

                if ( !this.ShuntResistance.CurrentLevel.Equals( resistance.CurrentLevel ) )
                {
                    _ = this.PublishVerbose( "Setting {0} to DC current Level {1};. ", this.MasterDevice.SourceMeasureUnit );
                    _ = this.MasterDevice.CurrentSourceSubsystem.ApplyLevel( resistance.CurrentLevel );
                    this.ShuntResistance.CurrentLevel = this.MasterDevice.CurrentSourceSubsystem.Level.GetValueOrDefault( 0d );
                }

                if ( !this.ShuntResistance.VoltageLimit.Equals( resistance.VoltageLimit ) )
                {
                    _ = this.PublishVerbose( "Setting {0} to DC current Voltage Limit {1};. ", this.MasterDevice.SourceMeasureUnit );
                    _ = this.MasterDevice.CurrentSourceSubsystem.ApplyVoltageLimit( resistance.VoltageLimit );
                    this.ShuntResistance.VoltageLimit = this.MasterDevice.CurrentSourceSubsystem.VoltageLimit.GetValueOrDefault( 0d );
                }

                _ = this.PublishVerbose( "Setting {0} to four wire sense;. ", this.MasterDevice.SourceMeasureUnit );
                _ = this.MasterDevice.SenseSubsystem.ApplySenseMode( SenseActionMode.Remote );
                _ = this.PublishVerbose( "Setting {0} to auto voltage range;. ", this.MasterDevice.SourceMeasureUnit );
                _ = this.MasterDevice.MeasureVoltageSubsystem.ApplyAutoRangeVoltageEnabled( true );
            }

            this.MasterDevice.DisplaySubsystem.ClearDisplay();
            this.MasterDevice.DisplaySubsystem.DisplayLine( 2, string.Format( System.Globalization.CultureInfo.CurrentCulture, "SrcI:{0}A LimV:{1}V", this.ShuntResistance.CurrentLevel, this.ShuntResistance.VoltageLimit ) );
            this.MasterDevice.StatusSubsystem.CheckThrowDeviceException( false, "configuring shunt resistance measurement;. " );
            this.ShuntResistance.CheckThrowUnequalConfiguration( resistance );
        }

        /// <summary> Measures the Shunt resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The shunt resistance. </param>
        public void MeasureShuntResistance( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            this.ShuntResistance.HighLimit = resistance.HighLimit;
            this.ShuntResistance.LowLimit = resistance.LowLimit;
            this.MasterDevice.MeasureResistanceSubsystem.Measure();
            string reading = this.MasterDevice.MeasureResistanceSubsystem.Reading;
            _ = this.PublishVerbose( "Parsing shunt resistance reading {0};. ", reading );
            this.MasterDevice.StatusSubsystem.CheckThrowDeviceException( false, "measuring shunt resistance;. " );
            this.ShuntResistance.ParseReading( reading, MeasurementOutcomes.None );
            resistance.ParseReading( reading, MeasurementOutcomes.None );
            this.MasterDevice.DisplaySubsystem.DisplayLine( 1, reading.Trim() );
            this.MasterDevice.DisplaySubsystem.DisplayCharacter( 1, reading.Length, 18 );
            this.ShuntResistance.MeasurementAvailable = true;
        }

        #endregion

        #region " TTM FRAMEWORK: MEASURE "

        /// <summary> Makes all measurements. Reading is done after all measurements are done. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void Measure()
        {
            if ( this.IsSessionOpen )
            {
                _ = this.MasterDevice.Session.WriteLine( "_G.ttm.measure()  waitcomplete() " );
            }
        }

        #endregion

        #region " TTM FRAMEWORK: TRIGGER "

        /// <summary> True if the meter was enabled to respond to trigger events. </summary>
        private bool _IsAwaitingTrigger;

        /// <summary> Abort triggered measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void AbortTriggerSequenceIf()
        {
            if ( this.IsSessionOpen && this._IsAwaitingTrigger && this.TriggerSequencer is object && this.MasterDevice is object )
            {
                this.TriggerSequencer.RestartSignal = TriggerSequenceSignal.Stop;
                this.MasterDevice.Session.AssertTrigger();
                // allow time for the measurement to terminate.
                System.Threading.Thread.Sleep( ( int ) Math.Round( 1000d * this.ThermalTransient.ThermalTransient.PostTransientDelay ) + 400 );
                this._IsAwaitingTrigger = false;
                this.MasterDevice.Session.DiscardUnreadData();
            }
        }

        /// <summary> true if this object is starting. </summary>
        private bool _IsStarting;

        /// <summary>
        /// Primes the instrument to wait for a trigger. This clears the digital outputs and loops until
        /// trigger or external *TRG command.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="isStarting"> When true, displays the title and locks the local key. </param>
        private void PrepareForTriggerThis( bool isStarting )
        {
            this.MasterDevice.Session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            this.MasterDevice.Session.EnableServiceRequestWaitComplete();
            _ = this.MasterDevice.Session.WriteLine( "prepareForTrigger({0},'{1}') waitcomplete() ", Interaction.IIf( isStarting, "true", "false" ), "OPC" );
        }

        /// <summary>
        /// Primes the instrument to wait for a trigger. This clears the digital outputs and loops until
        /// trigger or external TRG command.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void PrepareForTrigger()
        {
            this._IsStarting = false;
            this._IsAwaitingTrigger = true;
            this.PrepareForTriggerThis( this._IsStarting );
            this._IsStarting = false;
        }

        /// <summary> True if measurement completed. </summary>
        private bool _MeasurementCompleted;

        /// <summary> Gets or sets the measurement completed. </summary>
        /// <value> The measurement completed. </value>
        public bool MeasurementCompleted
        {
            get => this._MeasurementCompleted;

            set {
                if ( !value.Equals( this.MeasurementCompleted ) )
                {
                    this._MeasurementCompleted = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Monitors the bus to detect completion of measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> True if measurement completed, false if not. </returns>
        public bool IsMeasurementCompleted()
        {
            _ = this.MasterDevice.Session.ReadStatusRegister();
            this.MeasurementCompleted = this.MasterDevice.Session.MessageAvailable; // Me.MasterDevice.Session.IsMessageAvailable(TimeSpan.FromMilliseconds(50), 1)
            return this.MeasurementCompleted;
        }

        /// <summary> Reads the measurements from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="initialResistance"> The initial resistance. </param>
        /// <param name="finalResistance">   The final resistance. </param>
        /// <param name="thermalTransient">  The thermal transient. </param>
        public void ReadMeasurements( ResistanceMeasureBase initialResistance, ResistanceMeasureBase finalResistance, ResistanceMeasureBase thermalTransient )
        {
            _ = this.PublishVerbose( "'{0}' reading measurements;. ", this.ResourceName );
            this._IsAwaitingTrigger = false;
            this.MasterDevice.Session.DiscardUnreadData();
            this.InitialResistance.ReadResistance( initialResistance );
            this.FinalResistance.ReadResistance( finalResistance );
            this.ThermalTransient.ReadThermalTransient( thermalTransient );
        }

        #endregion

        #region " CHECK CONTACTS "

        /// <summary> Checks contact resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="threshold"> The threshold. </param>
        public void CheckContacts( int threshold )
        {
            _ = this.MasterDevice.ContactSubsystem.CheckContacts( threshold );
            if ( !this.MasterDevice.ContactSubsystem.ContactCheckOkay.HasValue )
            {
                throw new Core.OperationFailedException( "Failed Measuring contacts;. " );
            }
            else if ( !this.MasterDevice.ContactSubsystem.ContactCheckOkay.Value )
            {
                throw new Core.OperationFailedException( "High contact resistances;. Values: '{0}'", this.MasterDevice.ContactSubsystem.ContactResistances );
            }
        }

        /// <summary> Checks contact resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="threshold"> The threshold. </param>
        /// <param name="details">   [in,out] The details. </param>
        /// <returns> <c>True</c> if contacts checked okay. </returns>
        public bool TryCheckContacts( int threshold, ref string details )
        {
            _ = this.MasterDevice.ContactSubsystem.CheckContacts( threshold );
            if ( !this.MasterDevice.ContactSubsystem.ContactCheckOkay.HasValue )
            {
                details = "Failed Measuring contacts;. ";
                return false;
            }
            else if ( this.MasterDevice.ContactSubsystem.ContactCheckOkay.Value )
            {
                return true;
            }
            else
            {
                details = string.Format( "High contact resistances;. Values: '{0}'", this.MasterDevice.ContactSubsystem.ContactResistances );
                return false;
            }
        }

        #endregion

        #region " PART MEASUREMENTS "

        /// <summary> Gets or sets the part. </summary>
        /// <remarks>
        /// The part represents the actual device under test. These are the values that gets displayed on
        /// the header and saved.
        /// </remarks>
        /// <value> The part. </value>
        public DeviceUnderTest Part { get; set; }

        /// <summary> Measures and fetches the initial resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The cold resistance entity where the results are saved. </param>
        public void MeasureInitialResistance( ColdResistance value )
        {
            // clears display if not in measurement state
            _ = this.PublishVerbose( "Clearing meter display;. " );
            this.MasterDevice.DisplaySubsystem.ClearDisplayMeasurement();
            bool contactsOkay;
            string details = string.Empty;
            if ( this.Part.ContactCheckEnabled )
            {
                _ = this.PublishVerbose( "Checking contacts;. " );
                contactsOkay = this.TryCheckContacts( this.Part.ContactCheckThreshold, ref details );
            }
            else
            {
                contactsOkay = true;
            }

            if ( contactsOkay )
            {
                if ( this.MasterDevice.Enabled )
                {
                    _ = this.PublishVerbose( "Measuring Initial Resistance...;. " );
                    this.InitialResistance.Measure( value );
                }
                else
                {
                    _ = this.PublishVerbose( "Emulating Initial Resistance...;. " );
                    this.InitialResistance.EmulateResistance( value );
                }

                _ = this.PublishVerbose( "Got Initial Resistance;. " );
            }
            else
            {
                this.Part.InitialResistance.ApplyContactCheckOutcome( MeasurementOutcomes.FailedContactCheck );
                _ = this.PublishWarning( details );
            }
        }

        /// <summary> Measures and fetches the Final resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The cold resistance entity where the results are saved. </param>
        public void MeasureFinalResistance( ColdResistance value )
        {
            if ( this.MasterDevice.Enabled )
            {
                // clears display if not in measurement state
                _ = this.PublishVerbose( "Clearing meter display;. " );
                this.MasterDevice.DisplaySubsystem.ClearDisplayMeasurement();
                _ = this.PublishVerbose( "Measuring Final Resistance...;. " );
                this.FinalResistance.Measure( value );
            }
            else
            {
                _ = this.PublishVerbose( "Emulating Final Resistance...;. " );
                this.FinalResistance.EmulateResistance( value );
            }

            _ = this.PublishVerbose( "Got Final Resistance;. " );
        }

        /// <summary> Measures and fetches the thermal transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The cold resistance entity where the results are saved. </param>
        public void MeasureThermalTransient( ThermalTransient value )
        {
            // clears display if not in measurement state
            _ = this.PublishVerbose( "Clearing meter display;. " );
            this.MasterDevice.DisplaySubsystem.ClearDisplayMeasurement();
            if ( this.MasterDevice.Enabled )
            {
                _ = this.PublishVerbose( "Measuring Thermal Transient...;. " );
                this.ThermalTransient.Measure( value );
            }
            else
            {
                _ = this.PublishVerbose( "Emulating Thermal Transient...;. " );
                this.ThermalTransient.EmulateThermalTransient( value );
            }

            if ( this.MeasureSequencer is object )
            {
                this.MeasureSequencer.StartFinalResistanceTime( TimeSpan.FromMilliseconds( 1000d * this.Part.ThermalTransient.PostTransientDelay ) );
            }

            _ = this.PublishVerbose( "Got Thermal Transient;. " );
        }

        #endregion

        #region " SEQUENCED MEASUREMENTS "

        private MeasureSequencer _MeasureSequencerInternal;

        private MeasureSequencer MeasureSequencerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._MeasureSequencerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._MeasureSequencerInternal != null )
                {
                    this._MeasureSequencerInternal.PropertyChanged -= this.MeasureSequencer_PropertyChanged;
                }

                this._MeasureSequencerInternal = value;
                if ( this._MeasureSequencerInternal != null )
                {
                    this._MeasureSequencerInternal.PropertyChanged += this.MeasureSequencer_PropertyChanged;
                }
            }
        }

        /// <summary>   Gets or sets the sequencer. </summary>
        /// <value> The sequencer. </value>
        public MeasureSequencer MeasureSequencer
        {
            get => this.MeasureSequencerInternal;

            protected set => this.MeasureSequencerInternal = value;
        }

        /// <summary> Handles the measure sequencer property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( MeasureSequencer sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.MeasureSequencer.MeasurementSequenceState ):
                    {
                        this.OnMeasurementSequenceStateChanged( sender.MeasurementSequenceState );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _MeasureSequencer for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSequencer_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} handling measure sequencer {e?.PropertyName} property changed event";
                this.OnPropertyChanged( sender as MeasureSequencer, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="currentState"> The current state. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnMeasurementSequenceStateChanged( MeasurementSequenceState currentState )
        {
            switch ( currentState )
            {
                case MeasurementSequenceState.Idle:
                    {

                        // Waiting for the step signal to start.
                        _ = this.PublishVerbose( "Ready to start a measurement sequence;. " );
                        break;
                    }

                case MeasurementSequenceState.Aborted:
                    {
                        _ = this.PublishInfo( "Measurement sequence aborted;. " );
                        // step to the idle state.
                        this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                        break;
                    }

                case MeasurementSequenceState.Failed:
                    {
                        _ = this.PublishInfo( "Measurement sequence failed;. " );
                        if ( this.IsSessionOpen )
                        {
                            try
                            {
                                // flush the buffer so a time out error that might leave stuff will clear the buffers.
                                this.MasterDevice.Session.DiscardUnreadData();
                            }
                            catch
                            {
                            }
                        }
                        // step to the aborted state.
                        this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                        break;
                    }

                case MeasurementSequenceState.Completed:
                    {
                        _ = this.PublishVerbose( "Measurement sequence completed;. " );

                        // step to the idle state.
                        this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                        break;
                    }

                case MeasurementSequenceState.Starting:
                    {
                        _ = this.PublishVerbose( "Measurement sequence starting;. " );
                        this.Part.ClearMeasurements();

                        // step to the Measure Initial Resistance state.
                        this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                        break;
                    }

                case MeasurementSequenceState.MeasureInitialResistance:
                    {
                        string activity = string.Empty;
                        try
                        {
                            activity = $"{this.ResourceName} Measuring initial resistance";
                            _ = this.PublishVerbose( $"{activity};. " );
                            this.MeasureInitialResistance( this.Part.InitialResistance );
                            if ( this.Part.InitialResistance.Outcome == MeasurementOutcomes.PartPassed )
                            {
                                // step to the Measure Thermal Transient.
                                this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                            }
                            else
                            {
                                // if failure, do not continue.
                                this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Failure );
                            }
                        }
                        catch ( Exception ex )
                        {
                            _ = this.PublishException( activity, ex );
                            this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Failure );
                        }

                        break;
                    }

                case MeasurementSequenceState.MeasureThermalTransient:
                    {
                        string activity = string.Empty;
                        try
                        {
                            activity = $"{this.ResourceName} measuring thermal resistance";
                            _ = this.PublishVerbose( $"{activity};. " );
                            this.MeasureThermalTransient( this.Part.ThermalTransient );
                            // step to the post transient delay
                            this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                        }
                        catch ( Exception ex )
                        {
                            _ = this.PublishException( activity, ex );
                            this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Failure );
                        }

                        break;
                    }

                case MeasurementSequenceState.PostTransientPause:
                    {
                        _ = this.PublishVerbose( "Delaying final resistance measurement;. " );
                        break;
                    }

                case MeasurementSequenceState.MeasureFinalResistance:
                    {
                        string activity = string.Empty;
                        try
                        {
                            activity = $"{this.ResourceName} measuring final resistance";
                            _ = this.PublishVerbose( $"{activity};. " );
                            this.MeasureFinalResistance( this.Part.FinalResistance );
                            // step to the completed state
                            this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Step );
                        }
                        catch ( Exception ex )
                        {
                            _ = this.PublishException( activity, ex );
                            this.MeasureSequencer.Enqueue( MeasurementSequenceSignal.Failure );
                        }

                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + currentState.ToString() );
                        break;
                    }
            }
        }

        #endregion

        #region " TRIGGER SEQUENCE "

        private TriggerSequencer _TriggerSequencerInternal;

        private TriggerSequencer TriggerSequencerInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._TriggerSequencerInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._TriggerSequencerInternal != null )
                {
                    this._TriggerSequencerInternal.PropertyChanged -= this.TriggerSequencer_PropertyChanged;
                }

                this._TriggerSequencerInternal = value;
                if ( this._TriggerSequencerInternal != null )
                {
                    this._TriggerSequencerInternal.PropertyChanged += this.TriggerSequencer_PropertyChanged;
                }
            }
        }

        /// <summary> Gets or sets the trigger sequencer. </summary>
        /// <value> The sequencer. </value>
        public TriggerSequencer TriggerSequencer
        {
            get => this.TriggerSequencerInternal;

            set => this.TriggerSequencerInternal = value;
        }

        /// <summary> Handles the trigger sequencer property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( TriggerSequencer sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ttm.TriggerSequencer.AssertRequested ):
                    {
                        if ( sender.AssertRequested )
                        {
                            this.MasterDevice.Session.AssertTrigger();
                        }

                        break;
                    }

                case nameof( Ttm.TriggerSequencer.TriggerSequenceState ):
                    {
                        this.OnTriggerSequenceStateChanged( sender.TriggerSequenceState );
                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _TriggerSequencer for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSequencer_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceName} handling trigger sequence {e?.PropertyName} property changed event";
                this.OnPropertyChanged( sender as TriggerSequencer, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Event handler. Called by  for  events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="currentState"> The current state. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OnTriggerSequenceStateChanged( TriggerSequenceState currentState )
        {
            switch ( currentState )
            {
                case TriggerSequenceState.Idle:
                    {
                        if ( this.IsSessionOpen )
                        {
                            try
                            {
                                // flush the buffer so a time out error that might leave stuff will clear the buffers.
                                this.MasterDevice.Session.DiscardUnreadData();
                            }
                            catch
                            {
                            }
                        }

                        // Waiting for the step signal to start.
                        _ = this.PublishVerbose( "Ready to start a trigger sequence;. " );
                        break;
                    }

                case TriggerSequenceState.Aborted:
                    {
                        _ = this.PublishInfo( "Measurement sequence aborted;. " );

                        // step to the idle state.
                        this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        break;
                    }

                case TriggerSequenceState.Failed:
                    {
                        _ = this.PublishWarning( "Measurement sequence failed;. " );
                        // step to the aborted state.
                        this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        break;
                    }

                case TriggerSequenceState.Stopped:
                    {
                        _ = this.PublishInfo( "Measurement sequence stopped;. " );

                        // step to the idle state.
                        this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        break;
                    }

                case TriggerSequenceState.Starting:
                    {
                        _ = this.PublishVerbose( "Measurement sequence starting;. " );
                        try
                        {
                            this.PrepareForTrigger();
                            _ = this.PublishVerbose( "Monitoring instrument for measurements;. " );
                            // step to the Waiting state.
                            this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        }
                        catch ( Exception ex )
                        {
                            _ = this.PublishWarning( "Exception occurred preparing instrument for waiting for trigger;. {0}", ex.ToFullBlownString() );
                            // step to the failed state.
                            this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Failure );
                        }

                        break;
                    }

                case TriggerSequenceState.WaitingForTrigger:
                    {
                        if ( this.IsMeasurementCompleted() )
                        {
                            // step to the reading state.
                            this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        }
                        else if ( this.TriggerSequencer.AssertRequested )
                        {
                            this.TriggerSequencer.AssertRequested = false;
                            this.MasterDevice.Session.AssertTrigger();
                        }

                        break;
                    }

                case TriggerSequenceState.MeasurementCompleted:
                    {

                        // clear part measurements.
                        this.Part.ClearMeasurements();

                        // step to the reading state.
                        this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        break;
                    }

                case TriggerSequenceState.ReadingValues:
                    {
                        try
                        {
                            this.ReadMeasurements( this.Part.InitialResistance, this.Part.FinalResistance, this.Part.ThermalTransient );
                            // step to the starting state.
                            this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Step );
                        }
                        catch ( Exception ex )
                        {
                            _ = this.PublishWarning( "Exception occurred reading measurements;. {0}", ex.ToFullBlownString() );
                            // step to the failed state.
                            this.TriggerSequencer.Enqueue( TriggerSequenceSignal.Failure );
                        }

                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + currentState.ToString() );
                        break;
                    }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "TTM Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

    }
}
