﻿
namespace isr.VI.Tsp.K2600.Ttm.TtmSyntax
{
    namespace Support
    {

        /// <summary> A syntax of the Support LUA subsystem. </summary>
        /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para> </remarks>
        public static class SupportSyntax
        {
            /// <summary> The version query command. </summary>
            public const string VersionQueryCommand = "_G.print(_G.isr.version())";
        }
    }

    namespace Meters
    {


        /// <summary> Syntax of the thermal transient subsystem. </summary>
        /// <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para> </remarks>
        public static class ThermalTransient
        {
            /// <summary> The version query command. </summary>
            public const string VersionQueryCommand = "_G.print(_G.isr.meters.thermalTransient.version())";
        }
    }
}