using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

using isr.Core.Models;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Triggered Measurement sequencer. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-14 </para>
    /// </remarks>
    public class TriggerSequencer : ViewModelBase, IDisposable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public TriggerSequencer() : base()
        {
            this._SignalQueueSyncLocker = new object();
            this.LockedSignalQueue = new Queue<TriggerSequenceSignal>();
        }

        #region " I Disposable Support "

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary> Gets or sets the disposed status. </summary>
        /// <value> The is disposed. </value>
        public bool IsDisposed { get; private set; }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected virtual void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    // Free managed resources when explicitly called
                    if ( this.SequencerTimer is object )
                    {
                        this.SequencerTimer.Enabled = false;
                        this.SequencerTimer.Dispose();
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, ex.ToFullBlownString() );
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary> Finalizes this object. </summary>
        /// <remarks>
        /// David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
        /// unmanaged resources.
        /// </remarks>
        ~TriggerSequencer()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( false );
        }

        #endregion

        #endregion

        #region " SEQUENCED MEASUREMENTS "

        /// <summary> Gets or sets the assert requested. </summary>
        /// <value> The assert requested. </value>
        public bool AssertRequested { get; set; }

        /// <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void AssertTrigger()
        {
            this.AssertRequested = true;
            this.SyncNotifyPropertyChanged();
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Next status bar. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lastStatusBar"> The last status bar. </param>
        /// <returns> The next status bar. </returns>
        public static string NextStatusBar( string lastStatusBar )
        {
            lastStatusBar = string.IsNullOrEmpty( lastStatusBar )
                ? "|" : lastStatusBar == "|" ? "/" : lastStatusBar == "/" ? "-" : lastStatusBar == "-" ? @"\" : lastStatusBar == @"\" ? "|" : "|";

            return lastStatusBar;
        }

        /// <summary> The trigger stopwatch. </summary>
        private Stopwatch _TriggerStopwatch;

        /// <summary> Returns the percent progress. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="lastStatusBar"> The last status bar. </param>
        /// <returns> The status message. </returns>
        public string ProgressMessage( string lastStatusBar )
        {
            string message = NextStatusBar( lastStatusBar );
            switch ( this.TriggerSequenceState )
            {
                case TriggerSequenceState.Aborted:
                    {
                        message = "ABORTED";
                        break;
                    }

                case TriggerSequenceState.Failed:
                    {
                        message = "FAILED";
                        break;
                    }

                case TriggerSequenceState.Idle:
                    {
                        message = "Inactive";
                        break;
                    }

                case TriggerSequenceState.MeasurementCompleted:
                    {
                        message = "DATA AVAILABLE";
                        break;
                    }

                case var @case when @case == TriggerSequenceState.None:
                    {
                        break;
                    }

                case TriggerSequenceState.ReadingValues:
                    {
                        message = "READING...";
                        break;
                    }

                case TriggerSequenceState.Starting:
                    {
                        message = "PREPARING";
                        break;
                    }

                case TriggerSequenceState.Stopped:
                    {
                        message = "STOPPED";
                        break;
                    }

                case TriggerSequenceState.WaitingForTrigger:
                    {
                        message = string.Format( @"Waiting for trigger {0:s\.f} s", this._TriggerStopwatch.Elapsed );
                        break;
                    }
            }

            return message;
        }

        /// <summary> State of the trigger sequence. </summary>
        private TriggerSequenceState _TriggerSequenceState;

        /// <summary> Gets or sets the state of the measurement. </summary>
        /// <value> The measurement state. </value>
        public TriggerSequenceState TriggerSequenceState
        {
            get => this._TriggerSequenceState;

            protected set {
                if ( TriggerSequenceState.WaitingForTrigger == value || !value.Equals( this.TriggerSequenceState ) )
                {
                    this._TriggerSequenceState = value;
                    this.SyncNotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> The signal queue synchronize locker. </summary>
        private readonly object _SignalQueueSyncLocker;

        /// <summary> Gets or sets a queue of signals. </summary>
        /// <value> A Queue of signals. </value>
        private Queue<TriggerSequenceSignal> LockedSignalQueue { get; set; }

        /// <summary> Clears the signal queue. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ClearSignalQueue()
        {
            lock ( this._SignalQueueSyncLocker )
                this.LockedSignalQueue.Clear();
        }

        /// <summary> Dequeues a  signal. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="signal"> [in,out] The signal. </param>
        /// <returns> <c>True</c> if a signal was dequeued. </returns>
        public bool Dequeue( ref TriggerSequenceSignal signal )
        {
            lock ( this._SignalQueueSyncLocker )
            {
                if ( this.LockedSignalQueue.Any() )
                {
                    signal = this.LockedSignalQueue.Dequeue();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary> Enqueues a  signal. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="signal"> The signal. </param>
        public void Enqueue( TriggerSequenceSignal signal )
        {
            lock ( this._SignalQueueSyncLocker )
                this.LockedSignalQueue.Enqueue( signal );
        }

        /// <summary> Starts a measurement sequence. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void StartMeasurementSequence()
        {
            this.RestartSignal = TriggerSequenceSignal.None;
            if ( this.SequencerTimer is null )
            {
                this.SequencerTimer = new System.Timers.Timer();
            }

            this.SequencerTimer.Enabled = false;
            this.SequencerTimer.Interval = 100d;
            this.ClearSignalQueue();
            this.Enqueue( TriggerSequenceSignal.Step );
            this.SequencerTimer.Enabled = true;
        }

        /// <summary> Gets or sets the timer. </summary>
        private System.Timers.Timer _SequencerTimer;

        private System.Timers.Timer SequencerTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._SequencerTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._SequencerTimer != null )
                {
                    this._SequencerTimer.Elapsed -= this.SequencerTimer_Elapsed;
                }

                this._SequencerTimer = value;
                if ( this._SequencerTimer != null )
                {
                    this._SequencerTimer.Elapsed += this.SequencerTimer_Elapsed;
                }
            }
        }

        /// <summary> Executes the state sequence. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Specifies the object where the call originated. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SequencerTimer_Elapsed( object sender, EventArgs e )
        {
            try
            {
                this.SequencerTimer.Enabled = false;
                Core.ApplianceBase.DoEvents();
                this.TriggerSequenceState = this.ExecuteMeasurementSequence( this.TriggerSequenceState );
            }
            catch ( Exception )
            {
                this.Enqueue( TriggerSequenceSignal.Failure );
            }
            finally
            {
                Core.ApplianceBase.DoEvents();
                this.SequencerTimer.Enabled = this.TriggerSequenceState != TriggerSequenceState.Idle && this.TriggerSequenceState != TriggerSequenceState.None;
            }
        }

        /// <summary> Gets or sets the restart signal. </summary>
        /// <value> The restart signal. </value>
        public TriggerSequenceSignal RestartSignal { get; set; }

        /// <summary> Executes the measurement sequence returning the next state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="currentState"> The current measurement sequence state. </param>
        /// <returns> The next state. </returns>
        private TriggerSequenceState ExecuteMeasurementSequence( TriggerSequenceState currentState )
        {
            var signal = default( TriggerSequenceSignal );
            switch ( currentState )
            {
                case TriggerSequenceState.Idle:
                    {

                        // Waiting for the step signal to start.
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        currentState = TriggerSequenceState.Starting;
                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.Aborted:
                    {

                        // if failed, no action. The sequencer should stop here
                        // wait for the signal to move to the idle state
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Failure:
                                    {
                                        currentState = TriggerSequenceState.Failed;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        // clear the queue to start a fresh cycle.
                                        this.ClearSignalQueue();
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        // clear the queue to start a fresh cycle.
                                        this.ClearSignalQueue();
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.Failed:
                    {
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.Stopped:
                    {
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.Failure:
                                    {
                                        currentState = TriggerSequenceState.Failed;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.Starting:
                    {
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.Failure:
                                    {
                                        currentState = TriggerSequenceState.Failed;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        this._TriggerStopwatch = Stopwatch.StartNew();
                                        currentState = TriggerSequenceState.WaitingForTrigger;
                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.WaitingForTrigger:
                    {
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                case TriggerSequenceSignal.Failure:
                                case TriggerSequenceSignal.Stop:
                                    {
                                        this.RestartSignal = signal;
                                        // request a trigger. 
                                        this.AssertRequested = true;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        currentState = TriggerSequenceState.MeasurementCompleted;
                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.MeasurementCompleted:
                    {
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.Failure:
                                    {
                                        currentState = TriggerSequenceState.Failed;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        if ( this.RestartSignal == TriggerSequenceSignal.None || this.RestartSignal == TriggerSequenceSignal.Step )
                                        {
                                            currentState = TriggerSequenceState.ReadingValues;
                                        }
                                        else
                                        {
                                            switch ( this.RestartSignal )
                                            {
                                                case TriggerSequenceSignal.Abort:
                                                    {
                                                        currentState = TriggerSequenceState.Aborted;
                                                        break;
                                                    }

                                                case TriggerSequenceSignal.Failure:
                                                    {
                                                        currentState = TriggerSequenceState.Failed;
                                                        break;
                                                    }

                                                case TriggerSequenceSignal.None:
                                                    {
                                                        break;
                                                    }

                                                case TriggerSequenceSignal.Stop:
                                                    {
                                                        currentState = TriggerSequenceState.Idle;
                                                        break;
                                                    }
                                            }

                                            this.RestartSignal = TriggerSequenceSignal.None;
                                        }

                                        break;
                                    }
                            }
                        }

                        break;
                    }

                case TriggerSequenceState.ReadingValues:
                    {
                        if ( this.Dequeue( ref signal ) )
                        {
                            switch ( signal )
                            {
                                case TriggerSequenceSignal.Abort:
                                    {
                                        currentState = TriggerSequenceState.Aborted;
                                        break;
                                    }

                                case TriggerSequenceSignal.Failure:
                                    {
                                        currentState = TriggerSequenceState.Failed;
                                        break;
                                    }

                                case TriggerSequenceSignal.None:
                                    {
                                        break;
                                    }

                                case TriggerSequenceSignal.Stop:
                                    {
                                        currentState = TriggerSequenceState.Idle;
                                        break;
                                    }

                                case TriggerSequenceSignal.Step:
                                    {
                                        if ( this.RestartSignal == TriggerSequenceSignal.None || this.RestartSignal == TriggerSequenceSignal.Step )
                                        {
                                            currentState = TriggerSequenceState.Starting;
                                        }
                                        else
                                        {
                                            switch ( this.RestartSignal )
                                            {
                                                case TriggerSequenceSignal.Abort:
                                                    {
                                                        currentState = TriggerSequenceState.Aborted;
                                                        break;
                                                    }

                                                case TriggerSequenceSignal.Failure:
                                                    {
                                                        currentState = TriggerSequenceState.Failed;
                                                        break;
                                                    }

                                                case TriggerSequenceSignal.None:
                                                    {
                                                        break;
                                                    }

                                                case TriggerSequenceSignal.Stop:
                                                    {
                                                        currentState = TriggerSequenceState.Idle;
                                                        break;
                                                    }
                                            }

                                            this.RestartSignal = TriggerSequenceSignal.None;
                                        }

                                        break;
                                    }
                            }
                        }

                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "Unhandled state: " + currentState.ToString() );
                        this.Enqueue( TriggerSequenceSignal.Abort );
                        break;
                    }
            }

            return currentState;
        }

        #endregion

    }


    // #Disable Warning CA1027 ' Mark enums with FlagsAttribute

    /// <summary> Enumerates the measurement sequence. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1027:Mark enums with FlagsAttribute", Justification = "<Pending>" )]
    public enum TriggerSequenceState
    {
        // #Enable Warning CA1027 ' Mark enums with FlagsAttribute

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not Defined" )]
        None = 0,

        /// <summary> An enum constant representing the idle option. </summary>
        [Description( "Idle" )]
        Idle = 0,

        /// <summary> An enum constant representing the aborted option. </summary>
        [Description( "Triggered Measurement Sequence Aborted" )]
        Aborted,

        /// <summary> An enum constant representing the failed option. </summary>
        [Description( "Triggered Measurement Sequence Failed" )]
        Failed,

        /// <summary> An enum constant representing the starting option. </summary>
        [Description( "Triggered Measurement Sequence Starting" )]
        Starting,

        /// <summary> An enum constant representing the waiting for trigger option. </summary>
        [Description( "Waiting for Trigger" )]
        WaitingForTrigger,

        /// <summary> An enum constant representing the measurement completed option. </summary>
        [Description( "Measurement Completed" )]
        MeasurementCompleted,

        /// <summary> An enum constant representing the reading values option. </summary>
        [Description( "Reading Values" )]
        ReadingValues,

        /// <summary> An enum constant representing the stopped option. </summary>
        [Description( "Triggered Measurement Sequence Stopped" )]
        Stopped
    }

    /// <summary> Enumerates the measurement signals. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum TriggerSequenceSignal
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not Defined" )]
        None = 0,

        /// <summary> An enum constant representing the step] option. </summary>
        [Description( "Step Measurement" )]
        Step,

        /// <summary> An enum constant representing the abort] option. </summary>
        [Description( "Abort Measurement" )]
        Abort,

        /// <summary> An enum constant representing the stop] option. </summary>
        [Description( "Stop Measurement" )]
        Stop,

        /// <summary> An enum constant representing the failure] option. </summary>
        [Description( "Report Failure" )]
        Failure
    }
}
