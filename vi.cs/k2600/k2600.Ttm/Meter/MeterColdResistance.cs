using System;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> The Meter Cold Resistance. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-23 </para>
    /// </remarks>
    public class MeterColdResistance : MeterSubsystemBase
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        /// <param name="resistance">      The cold resistance. </param>
        /// <param name="meterEntity">     The meter entity type. </param>
        public MeterColdResistance( StatusSubsystemBase statusSubsystem, ColdResistance resistance, ThermalTransientMeterEntity meterEntity ) : base( statusSubsystem )
        {
            this.MeterEntity = meterEntity;
            this.ColdResistance = resistance;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Defines the parameters for the Clears known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineClearExecutionState()
        {
            _ = this.PublishVerbose( "Clearing {0} resistance execution state;. ", this.EntityName );
            base.DefineClearExecutionState();
            this.ColdResistance.DefineClearExecutionState();
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {

            // Me.Session.WriteLine("{0}.level = nil", Me.EntityName)
            // Dim b As Boolean = Me.Session.IsStatementTrue("{0}.level==nil", Me.EntityName)
            // 'Me.Session.WriteLine("{0}.level = 0.09", Me.EntityName)
            // 'B = Me.Session.IsStatementTrue("{0}.level==nil", Me.EntityName)
            // Dim val As String = Me.Session.QueryPrintStringFormat("%9.6f", "_G.ttm.coldResistance.Defaults.level")

            _ = this.PublishVerbose( "Resetting {0} resistance known state;. ", this.EntityName );
            Core.ApplianceBase.DoEvents();
            base.DefineKnownResetState();
            Core.ApplianceBase.DoEvents();
            this.ColdResistance.ResetKnownState();
            Core.ApplianceBase.DoEvents();
        }

        #endregion

        #region " DEVICE UNDER TEST: COLD RESISTANCE "

        /// <summary> Gets the <see cref="ColdResistance">cold resistance</see>. </summary>
        /// <value> The cold resistance. </value>
        public ColdResistance ColdResistance { get; set; }

        /// <summary> Gets the <see cref="ResistanceMeasureBase">part resistance element</see>. </summary>
        /// <value> The cold resistance. </value>
        public override ResistanceMeasureBase Resistance => this.ColdResistance;

        #endregion

        #region " CONFIGURE "

        /// <summary> Reads instrument defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void ReadInstrumentDefaults()
        {
            base.ReadInstrumentDefaults();
            bool localTryQueryPrint() { decimal argvalue = My.MySettings.Default.ColdResistanceApertureDefault; var ret = this.Session.TryQueryPrint( 7.4m, ref argvalue, "{0}.aperture", this.DefaultsName ); My.MySettings.Default.ColdResistanceApertureDefault = argvalue; return ret; }

            if ( !localTryQueryPrint() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance aperture;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint1() { decimal argvalue = My.MySettings.Default.ColdResistanceCurrentLevelDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.level", this.DefaultsName ); My.MySettings.Default.ColdResistanceCurrentLevelDefault = argvalue; return ret; }

            if ( !localTryQueryPrint1() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance current level;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint2() { decimal argvalue = My.MySettings.Default.ColdResistanceCurrentMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minCurrent", this.DefaultsName ); My.MySettings.Default.ColdResistanceCurrentMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint2() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance minimum current;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint3() { decimal argvalue = My.MySettings.Default.ColdResistanceCurrentMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxCurrent", this.DefaultsName ); My.MySettings.Default.ColdResistanceCurrentMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint3() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance maximum current;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint4() { decimal argvalue = My.MySettings.Default.ColdResistanceVoltageLimitDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.limit", this.DefaultsName ); My.MySettings.Default.ColdResistanceVoltageLimitDefault = argvalue; return ret; }

            if ( !localTryQueryPrint4() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance voltage limit;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint5() { decimal argvalue = My.MySettings.Default.ColdResistanceVoltageMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minVoltage", this.DefaultsName ); My.MySettings.Default.ColdResistanceVoltageMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint5() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance minimum voltage;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint6() { decimal argvalue = My.MySettings.Default.ColdResistanceVoltageMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxVoltage", this.DefaultsName ); My.MySettings.Default.ColdResistanceVoltageMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint6() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance maximum voltage;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint7() { decimal argvalue = My.MySettings.Default.ColdResistanceLowLimitDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.lowLimit", this.DefaultsName ); My.MySettings.Default.ColdResistanceLowLimitDefault = argvalue; return ret; }

            if ( !localTryQueryPrint7() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance low limit;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint8() { decimal argvalue = My.MySettings.Default.ColdResistanceHighLimitDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.highLimit", this.DefaultsName ); My.MySettings.Default.ColdResistanceHighLimitDefault = argvalue; return ret; }

            if ( !localTryQueryPrint8() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance high limit;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint9() { decimal argvalue = My.MySettings.Default.ColdResistanceMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minResistance", this.DefaultsName ); My.MySettings.Default.ColdResistanceMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint9() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance minimum resistance;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
            bool localTryQueryPrint10() { decimal argvalue = My.MySettings.Default.ColdResistanceMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxResistance", this.DefaultsName ); My.MySettings.Default.ColdResistanceMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint10() )
            {
                _ = this.PublishWarning( "failed reading default cold resistance maximum resistance;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Configures the meter for making the cold resistance measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The cold resistance. </param>
        public override void Configure( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            base.Configure( resistance );
            this.CheckThrowDeviceException( false, "configuring {0} measurement;. ", this.EntityName );
            this.ColdResistance.CheckThrowUnequalConfiguration( resistance );
        }

        /// <summary> Applies changed meter configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The cold resistance. </param>
        public override void ConfigureChanged( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            base.ConfigureChanged( resistance );
            this.StatusSubsystem.CheckThrowDeviceException( false, "configuring {0} measurement;. ", this.EntityName );
            this.ColdResistance.CheckThrowUnequalConfiguration( resistance );
        }

        /// <summary> Queries the configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void QueryConfiguration()
        {
            base.QueryConfiguration();
            this.StatusSubsystem.CheckThrowDeviceException( false, "Reading {0} configuration;. ", this.EntityName );
        }

        #endregion

        #region " MEASURE "

        /// <summary> Measures the Final resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The resistance. </param>
        public new void Measure( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            this.Session.MakeEmulatedReply( resistance.GenerateRandomReading() );
            base.Measure( resistance );
            this.ReadResistance( resistance );
            // Me.EmulateResistance(resistance)
        }

        #endregion

        #region " READ "

        /// <summary>
        /// Reads the resistance. Sets the last <see cref="MeterSubsystemBase.LastReading">reading</see>,
        /// <see cref="MeterSubsystemBase.LastOutcome">outcome</see> <see cref="MeterSubsystemBase.LastMeasurementStatus">status</see> and
        /// <see cref="MeterSubsystemBase.MeasurementAvailable">Measurement available sentinel</see>.
        /// The outcome is left empty if measurements were not made.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void ReadResistance()
        {
            if ( this.QueryOutcomeNil() )
            {
                this.LastReading = string.Empty;
                this.LastOutcome = string.Empty;
                this.LastMeasurementStatus = string.Empty;
                throw new InvalidOperationException( "Measurement not made." );
            }
            else if ( this.QueryOkay() )
            {
                this.LastReading = this.Session.QueryPrintStringFormatTrimEnd( 8.5m, "{0}.resistance", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading resistance;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastOutcome = "0";
                this.LastMeasurementStatus = string.Empty;
            }
            else
            {
                // if outcome failed, read and parse the outcome and status.
                this.LastOutcome = this.Session.QueryPrintStringFormatTrimEnd( 1, "{0}.outcome", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading outcome;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastMeasurementStatus = this.Session.QueryPrintStringFormatTrimEnd( 1, "{0}.status", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading status;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastReading = string.Empty;
            }

            this.ColdResistance.LastOutcome = this.LastOutcome;
            this.MeasurementAvailable = true;
        }

        /// <summary> Reads cold resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The cold resistance. </param>
        public void ReadResistance( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            this.ReadResistance();
            this.StatusSubsystem.CheckThrowDeviceException( false, "Reading Resistance;. last command: '{0}'", this.Session.LastMessageSent );
            var measurementOutcome = MeasurementOutcomes.None;
            if ( this.LastOutcome != "0" )
            {
                string details = string.Empty;
                measurementOutcome = this.ParseOutcome( ref details );
                _ = this.PublishWarning( "Measurement failed;. Details: {0}", details );
            }

            this.ColdResistance.ParseReading( this.LastReading, measurementOutcome );
            resistance.ParseReading( this.LastReading, measurementOutcome );
        }

        /// <summary> Emulates cold resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistance"> The cold resistance. </param>
        public void EmulateResistance( ResistanceMeasureBase resistance )
        {
            if ( resistance is null )
                throw new ArgumentNullException( nameof( resistance ) );
            this.ColdResistance.EmulateReading();
            this.LastReading = this.ColdResistance.LastReading;
            this.LastOutcome = this.ColdResistance.LastOutcome;
            this.LastMeasurementStatus = string.Empty;
            var measurementOutcome = MeasurementOutcomes.None;
            this.ColdResistance.ParseReading( this.LastReading, measurementOutcome );
            resistance.ParseReading( this.LastReading, measurementOutcome );
        }

        #endregion

    }
}
