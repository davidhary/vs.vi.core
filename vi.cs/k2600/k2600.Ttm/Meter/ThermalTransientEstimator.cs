using System;

using isr.Core.NumericExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Thermal transient. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-23 </para>
    /// </remarks>
    public class ThermalTransientEstimator : MeterSubsystemBase
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public ThermalTransientEstimator( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.MeterEntity = ThermalTransientMeterEntity.Estimator;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.ReadInstrumentDefaults();
            this.ApplyInstrumentDefaults();
            base.DefineKnownResetState();
            this.QueryConfiguration();
        }

        #endregion

        #region " PART "

        /// <summary> Gets the <see cref="ResistanceMeasureBase">part resistance element</see>. </summary>
        /// <value> The cold resistance. </value>
        public override ResistanceMeasureBase Resistance => null;

        #endregion

        #region " THERMAL COEFFICIENT "

        /// <summary> The Thermal Coefficient. </summary>
        private double? _ThermalCoefficient;

        /// <summary> Gets or sets the cached Thermal Coefficient. </summary>
        /// <value> The Thermal Coefficient. </value>
        public double? ThermalCoefficient
        {
            get => this._ThermalCoefficient;

            protected set {
                if ( this.ThermalCoefficient.Differs( value, 0.000000001d ) )
                {
                    this._ThermalCoefficient = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Thermal Coefficient. </summary>
        /// <remarks>
        /// This command set the immediate output Thermal Coefficient. The value is in Volts. The
        /// immediate ThermalCoefficient is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Thermal Coefficient. </param>
        /// <returns> The Thermal Coefficient. </returns>
        public double? ApplyThermalCoefficient( double value )
        {
            _ = this.WriteThermalCoefficient( value );
            return this.QueryThermalCoefficient();
        }

        /// <summary> Queries the Thermal Coefficient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The ThermalCoefficient or none if unknown. </returns>
        public double? QueryThermalCoefficient()
        {
            const decimal printFormat = 9.6m;
            this.ThermalCoefficient = this.Session.QueryPrint( this.ThermalCoefficient.GetValueOrDefault( 0d ), printFormat, "{0}.thermalCoefficient", this.EntityName );
            return this.ThermalCoefficient;
        }

        /// <summary>
        /// Writes the ThermalCoefficient without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output Thermal Coefficient. The value is in Volts. The
        /// immediate ThermalCoefficient is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Thermal Coefficient. </param>
        /// <returns> The Thermal Coefficient. </returns>
        public double? WriteThermalCoefficient( double value )
        {
            string details = string.Empty;
            if ( !ThermalTransient.ValidateThermalCoefficient( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:thermalCoefficientSetter({1})", this.EntityName, value );
            this.ThermalCoefficient = value;
            return this.ThermalCoefficient;
        }

        #endregion

        #region " CONFIGURE "

        /// <summary> Applies the instrument defaults. </summary>
        /// <remarks> This is required until the reset command gets implemented. </remarks>
        public override void ApplyInstrumentDefaults()
        {
            _ = this.PublishVerbose( "Applying {0} defaults.", this.EntityName );
            _ = this.StatusSubsystem.WriteLine( "{0}:thermalCoefficientSetter({1}.thermalCoefficient)", this.EntityName, this.DefaultsName );
            _ = this.Session.QueryOperationCompleted();
        }

        /// <summary> Reads instrument defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void ReadInstrumentDefaults()
        {
            _ = this.PublishVerbose( "Reading {0} defaults.", this.EntityName );
            bool localTryQueryPrint() { decimal argvalue = My.MySettings.Default.ThermalCoefficientDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.thermalCoefficient", this.DefaultsName ); My.MySettings.Default.ThermalCoefficientDefault = argvalue; return ret; }

            if ( !localTryQueryPrint() )
            {
                _ = this.PublishWarning( $"failed reading default thermal transient estimator thermal coefficient;. Sent:'{this.Session.LastMessageSent}; Received:'{this.Session.LastMessageReceived}'." );
            }
        }

        /// <summary> Configures the meter for estimation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resistance"> The resistance. </param>
        public override void Configure( ResistanceMeasureBase resistance )
        {
        }

        /// <summary> Queries the configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void QueryConfiguration()
        {
            _ = this.PublishVerbose( "Reading {0} configuration.", this.EntityName );
            _ = this.QueryThermalCoefficient();
            this.StatusSubsystem.CheckThrowDeviceException( false, "Reading {0} configuration;. ", this.EntityName );
        }

        #endregion

        #region " READ "

        /// <summary>
        /// Reads the thermal transient. Sets the last <see cref="MeterSubsystemBase.LastReading">reading</see>,
        /// <see cref="MeterSubsystemBase.LastOutcome">outcome</see> <see cref="MeterSubsystemBase.LastMeasurementStatus">status</see> and
        /// <see cref="MeterSubsystemBase.MeasurementAvailable">Measurement available sentinel</see>.
        /// The outcome is left empty if measurements were not made.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void ReadThermalTransient()
        {
            if ( this.QueryOutcomeNil() )
            {
                this.LastReading = string.Empty;
                this.LastOutcome = string.Empty;
                this.LastMeasurementStatus = string.Empty;
                throw new InvalidOperationException( "Measurement not made." );
            }
            else if ( this.QueryOkay() )
            {
                this.LastReading = this.Session.QueryPrintStringFormatTrimEnd( 9.6m, "{0}.voltageChange", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading voltage change;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastOutcome = "0";
                this.LastMeasurementStatus = string.Empty;
            }
            else
            {
                // if outcome failed, read and parse the outcome and status.
                this.LastOutcome = this.Session.QueryPrintStringFormatTrimEnd( 1, "{0}.outcome", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading outcome;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastMeasurementStatus = this.Session.QueryPrintStringFormatTrimEnd( 1, "{0}.status", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading status;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastReading = string.Empty;
            }

            this.MeasurementAvailable = true;
        }

        #endregion

    }
}
