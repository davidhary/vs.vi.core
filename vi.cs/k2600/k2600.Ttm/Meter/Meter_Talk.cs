﻿using System;
using System.Diagnostics;

using isr.Core;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{
    public partial class Meter
    {

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

        #region " CUSTOM FUNCTIONS "

        /// <summary>
        /// Removes the private listeners. Removes all listeners if the talker was not assigned.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void RemoveListeners()
        {
            base.RemoveListeners();
            this.RemovePrivateListeners();
            this.MasterDevice?.RemoveListeners();
            this.ShuntResistance?.RemoveListeners();
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            // this should apply only to the listeners associated with this form
            // Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
            this.ShuntResistance.ApplyListenerTraceLevel( listenerType, value );
            this.MasterDevice.ApplyListenerTraceLevel( listenerType, value );
            base.ApplyListenerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            base.ApplyTalkerTraceLevel( listenerType, value );
            if ( listenerType == ListenerType.Logger )
            {
                this.NotifyPropertyChanged( nameof( this.TraceLogLevel ) );
            }
            else
            {
                this.NotifyPropertyChanged( nameof( this.TraceShowLevel ) );
            }

            this.ShuntResistance.ApplyTalkerTraceLevel( listenerType, value );
            this.MasterDevice.ApplyTalkerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public override void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            base.ApplyTalkerTraceLevels( talker );
            this.MasterDevice.ApplyTalkerTraceLevels( talker );
            this.ShuntResistance?.ApplyTalkerTraceLevels( talker );
        }

        /// <summary> Applies the talker listeners trace levels described by talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public override void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            base.ApplyListenerTraceLevels( talker );
            this.MasterDevice.ApplyListenerTraceLevels( talker );
            this.ShuntResistance?.ApplyListenerTraceLevels( talker );
        }

        /// <summary> Adds subsystem listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void AddSubsystemListeners()
        {
            this.ShuntResistance.AddListeners( this.Talker );
            this.MasterDevice.AddListeners( this.Talker );
        }

        /// <summary> Clears the subsystem listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void RemoveSubsystemListeners()
        {
            this.MasterDevice.RemoveListeners();
        }

        #endregion

    }
}