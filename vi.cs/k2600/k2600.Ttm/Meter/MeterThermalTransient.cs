using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.NumericExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Thermal transient. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-23 </para>
    /// </remarks>
    public class MeterThermalTransient : MeterSubsystemBase
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem">  The status subsystem. </param>
        /// <param name="thermalTransient"> The thermal transient element. </param>
        public MeterThermalTransient( StatusSubsystemBase statusSubsystem, ThermalTransient thermalTransient ) : base( statusSubsystem )
        {
            this.MeterEntity = ThermalTransientMeterEntity.Transient;
            this.ThermalTransient = thermalTransient;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Clears known state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineClearExecutionState()
        {
            _ = this.PublishVerbose( "Clearing {0} thermal transient execution state;. ", this.EntityName );
            base.DefineClearExecutionState();
            this.ThermalTransient.DefineClearExecutionState();
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            _ = this.PublishVerbose( "Resetting {0} thermal transient to known state;. ", this.EntityName );
            Core.ApplianceBase.DoEvents();
            base.DefineKnownResetState();
            Core.ApplianceBase.DoEvents();
            this.ThermalTransient.ResetKnownState();
            Core.ApplianceBase.DoEvents();
        }

        #endregion


        #region " DEVICE UNDER TEST: THERMAL TRANSIENT "

        /// <summary> Gets the <see cref="ThermalTransient">part Thermal Transient</see>. </summary>
        /// <value> The cold resistance. </value>
        public ThermalTransient ThermalTransient { get; set; }

        /// <summary> Gets the <see cref="ResistanceMeasureBase">part resistance element</see>. </summary>
        /// <value> The cold resistance. </value>
        public override ResistanceMeasureBase Resistance => this.ThermalTransient;

        #endregion

        #region " PULSE DURATION "

        /// <summary> Gets the duration of the pulse. </summary>
        /// <value> The pulse duration. </value>
        public TimeSpan PulseDuration => TimeSpan.FromTicks( ( long ) Math.Round( TimeSpan.TicksPerSecond * this.TracePoints.GetValueOrDefault( 0 ) * this.SamplingInterval.GetValueOrDefault( 0d ) ) );
        #endregion

        #region " APERTURE "

        /// <summary> Writes the Aperture without reading back the value from the device. </summary>
        /// <remarks>
        /// This command sets the immediate output Aperture. The value is in Volts. The immediate
        /// Aperture is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Aperture. </param>
        /// <returns> The Aperture. </returns>
        public override double? WriteAperture( double value )
        {
            _ = base.WriteAperture( value );
            // update the maximum sampling rage.
            _ = this.Session.WriteLine( "{0}:maxRateSetter()", this.EntityName );
            // and read the current period.
            _ = this.QuerySamplingInterval();
            return this.Aperture;
        }

        #endregion

        #region " MEDIAN FILTER SIZE "

        /// <summary> The Median Filter Size. </summary>
        private int? _MedianFilterSize;

        /// <summary> Gets or sets the cached Median Filter Size. </summary>
        /// <value> The Median Filter Size. </value>
        public int? MedianFilterSize
        {
            get => this._MedianFilterSize;

            protected set {
                this.ThermalTransient.MedianFilterSize = value ?? 0;
                if ( !Nullable.Equals( this.MedianFilterSize, value ) )
                {
                    this._MedianFilterSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Median Filter Size. </summary>
        /// <remarks>
        /// This command set the immediate output Median Filter Size. The value is in Volts. The
        /// immediate Median Filter Size is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Median Filter Size. </param>
        /// <returns> The Median Filter Size. </returns>
        public int? ApplyMedianFilterSize( int value )
        {
            _ = this.WriteMedianFilterSize( value );
            return this.QueryMedianFilterSize();
        }

        /// <summary> Queries the Median Filter Size. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Median Filter Size or none if unknown. </returns>
        public int? QueryMedianFilterSize()
        {
            const int printFormat = 1;
            this.MedianFilterSize = this.Session.QueryPrint( this.MedianFilterSize.GetValueOrDefault( 5 ), printFormat, "{0}.medianFilterSize", this.EntityName );
            return this.MedianFilterSize;
        }

        /// <summary>
        /// Writes the Median Filter Size without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output Median Filter Size. The value is in Volts. The
        /// immediate MedianFilterSize is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Median Filter Size. </param>
        /// <returns> The Median Filter Size. </returns>
        public int? WriteMedianFilterSize( int value )
        {
            string details = string.Empty;
            if ( !ThermalTransient.ValidateMedianFilterSize( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:medianFilterSizeSetter({1})", this.EntityName, value );
            this.MedianFilterSize = value;
            return this.MedianFilterSize;
        }

        #endregion

        #region " POST TRANSIENT DELAY "

        /// <summary> The Post Transient Delay. </summary>
        private double? _PostTransientDelay;

        /// <summary> Gets or sets the cached Source Post Transient Delay. </summary>
        /// <value>
        /// The Source Post Transient Delay. Actual Voltage depends on the power supply mode.
        /// </value>
        public double? PostTransientDelay
        {
            get => this._PostTransientDelay;

            protected set {
                this.ThermalTransient.PostTransientDelay = value ?? 0d;
                if ( this.PostTransientDelay.Differs( value, 0.001d ) )
                {
                    this._PostTransientDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the source Post Transient Delay. </summary>
        /// <remarks>
        /// This command set the immediate output Post Transient Delay. The value is in Amperes. The
        /// immediate Post Transient Delay is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Post Transient Delay. </param>
        /// <returns> The Source Post Transient Delay. </returns>
        public double? ApplyPostTransientDelay( double value )
        {
            _ = this.WritePostTransientDelay( value );
            return this.QueryPostTransientDelay();
        }

        /// <summary> Queries the Post Transient Delay. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Post Transient Delay or none if unknown. </returns>
        public double? QueryPostTransientDelay()
        {
            const decimal printFormat = 9.3m;
            this.PostTransientDelay = this.Session.QueryPrint( this.PostTransientDelay.GetValueOrDefault( 0.5d ), printFormat, "{0}.postTransientDelayGetter()", this.BaseEntityName );
            return this.PostTransientDelay;
        }

        /// <summary>
        /// Writes the source Post Transient Delay without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output Post Transient Delay. The value is in Amperes. The
        /// immediate PostTransientDelay is the output Voltage setting. At *RST, the Voltage values =
        /// 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Post Transient Delay. </param>
        /// <returns> The Source Post Transient Delay. </returns>
        public double? WritePostTransientDelay( double value )
        {
            string details = string.Empty;
            if ( !ThermalTransient.ValidatePostTransientDelay( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}.postTransientDelaySetter({1})", this.BaseEntityName, value );
            this.PostTransientDelay = value;
            return this.PostTransientDelay;
        }

        #endregion

        #region " SAMPLING INTERVAL "

        /// <summary> The Sampling Interval. </summary>
        private double? _SamplingInterval;

        /// <summary> Gets or sets the cached Sampling Interval. </summary>
        /// <value> The Sampling Interval. </value>
        public double? SamplingInterval
        {
            get => this._SamplingInterval;

            protected set {
                this.ThermalTransient.SamplingInterval = value ?? 0d;
                if ( this.SamplingInterval.Differs( value, 0.000001d ) )
                {
                    this._SamplingInterval = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Sampling Interval. </summary>
        /// <remarks>
        /// This command set the immediate output Sampling Interval. The value is in Ohms. The immediate
        /// Sampling Interval is the output Low setting. At *RST, the Low values = 0.
        /// </remarks>
        /// <param name="value"> The Sampling Interval. </param>
        /// <returns> The Sampling Interval. </returns>
        public double? ApplySamplingInterval( double value )
        {
            _ = this.WriteSamplingInterval( value );
            return this.QuerySamplingInterval();
        }

        /// <summary> Queries the Sampling Interval. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Sampling Interval or none if unknown. </returns>
        public double? QuerySamplingInterval()
        {
            const decimal printFormat = 9.6m;
            this.SamplingInterval = this.Session.QueryPrint( this.SamplingInterval.GetValueOrDefault( 0.0001d ), printFormat, "{0}.period", this.EntityName );
            return this.SamplingInterval;
        }

        /// <summary>
        /// Writes the Sampling Interval without reading back the value from the device.
        /// </summary>
        /// <remarks>
        /// This command sets the immediate output Sampling Interval. The value is in Ohms. The immediate
        /// SamplingInterval is the output Low setting. At *RST, the Low values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Sampling Interval. </param>
        /// <returns> The Sampling Interval. </returns>
        public double? WriteSamplingInterval( double value )
        {
            string details = string.Empty;
            if ( !ThermalTransient.ValidateLimit( value, ref details ) || this.TracePoints.HasValue && !ThermalTransient.ValidatePulseWidth( value, this.TracePoints.Value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:periodSetter({1})", this.EntityName, value );
            this.SamplingInterval = value;
            return this.SamplingInterval;
        }

        #endregion

        #region " TRACE POINTS "

        /// <summary> The Trace Points. </summary>
        private int? _TracePoints;

        /// <summary> Gets or sets the cached Trace Points. </summary>
        /// <value> The Trace Points. </value>
        public int? TracePoints
        {
            get => this._TracePoints;

            protected set {
                if ( !Nullable.Equals( this.TracePoints, value ) )
                {
                    this._TracePoints = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Trace Points. </summary>
        /// <remarks>
        /// This command set the immediate output Trace Points. The value is in Volts. The immediate
        /// Trace Points is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Trace Points. </param>
        /// <returns> The Trace Points. </returns>
        public int? ApplyTracePoints( int value )
        {
            _ = this.WriteTracePoints( value );
            return this.QueryTracePoints();
        }

        /// <summary> Queries the Trace Points. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Trace Points or none if unknown. </returns>
        public int? QueryTracePoints()
        {
            const int printFormat = 1;
            this.TracePoints = this.Session.QueryPrint( this.TracePoints.GetValueOrDefault( 100 ), printFormat, "{0}.points", this.EntityName );
            return this.TracePoints;
        }

        /// <summary> Writes the Trace Points without reading back the value from the device. </summary>
        /// <remarks>
        /// This command sets the immediate output Trace Points. The value is in Volts. The immediate
        /// TracePoints is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Trace Points. </param>
        /// <returns> The Trace Points. </returns>
        public int? WriteTracePoints( int value )
        {
            string details = string.Empty;
            if ( !ThermalTransient.ValidateTracePoints( value, ref details ) || this.SamplingInterval.HasValue && !ThermalTransient.ValidatePulseWidth( this.SamplingInterval.Value, value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:pointsSetter({1})", this.EntityName, value );
            this.TracePoints = value;
            return this.TracePoints;
        }

        #endregion

        #region " TIME SERIES "

        /// <summary> The last trace. </summary>
        private string _LastTrace;

        /// <summary> Gets or sets (protected) the last trace. </summary>
        /// <value> The last trace. </value>
        public string LastTrace
        {
            get => this._LastTrace;

            protected set {
                this._LastTrace = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Gets or sets the thermal transient time series. </summary>
        /// <value> The last time series. </value>
        public IList<System.Windows.Point> LastTimeSeries { get; private set; }

        #endregion

        #region " VOLTAGE CHANGE "

        /// <summary> The Voltage Change. </summary>
        private double? _VoltageChange;

        /// <summary> Gets or sets the cached Voltage Change. </summary>
        /// <value> The Voltage Change. </value>
        public double? VoltageChange
        {
            get => this._VoltageChange;

            protected set {
                this.ThermalTransient.AllowedVoltageChange = value ?? 0d;
                if ( this.VoltageChange.Differs( value, 0.000001d ) )
                {
                    this._VoltageChange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Voltage Change. </summary>
        /// <remarks>
        /// This command set the immediate output Voltage Change. The value is in Volts. The immediate
        /// Voltage Change is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <param name="value"> The Voltage Change. </param>
        /// <returns> The Voltage Change. </returns>
        public double? ApplyVoltageChange( double value )
        {
            _ = this.WriteVoltageChange( value );
            return this.QueryVoltageChange();
        }

        /// <summary> Queries the Voltage Change. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Voltage Change or none if unknown. </returns>
        public double? QueryVoltageChange()
        {
            const decimal printFormat = 9.6m;
            this.VoltageChange = this.Session.QueryPrint( this.VoltageChange.GetValueOrDefault( 0.099d ), printFormat, "{0}.maxVoltageChange", this.EntityName );
            return this.VoltageChange;
        }

        /// <summary> Writes the Voltage Change without reading back the value from the device. </summary>
        /// <remarks>
        /// This command sets the immediate output Voltage Change. The value is in Volts. The immediate
        /// VoltageChange is the output Voltage setting. At *RST, the Voltage values = 0.
        /// </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        ///                                                the required range. </exception>
        /// <param name="value"> The Voltage Change. </param>
        /// <returns> The Voltage Change. </returns>
        public double? WriteVoltageChange( double value )
        {
            string details = string.Empty;
            if ( !ThermalTransient.ValidateVoltageChange( value, ref details ) )
            {
                throw new ArgumentOutOfRangeException( nameof( value ), details );
            }

            _ = this.Session.WriteLine( "{0}:maxVoltageChangeSetter({1})", this.EntityName, value );
            this.VoltageChange = value;
            return this.VoltageChange;
        }

        #endregion

        #region " CONFIGURE "

        /// <summary> Applies the instrument defaults. </summary>
        /// <remarks> This is required until the reset command gets implemented. </remarks>
        public override void ApplyInstrumentDefaults()
        {
            base.ApplyInstrumentDefaults();
            _ = this.StatusSubsystem.WriteLine( "{0}:maxRateSetter()", this.EntityName );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( "{0}:maxVoltageChangeSetter({1}.level)", this.EntityName, this.DefaultsName );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( "{0}:latencySetter({1}.latency)", this.EntityName, this.DefaultsName );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( "{0}:medianFilterSizeSetter({1}.medianFilterSize)", this.EntityName, this.DefaultsName );
            _ = this.Session.QueryOperationCompleted();
            _ = this.StatusSubsystem.WriteLine( "{0}:pointsSetter({1}.points)", this.EntityName, this.DefaultsName );
            _ = this.Session.QueryOperationCompleted();
        }

        /// <summary> Reads instrument defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void ReadInstrumentDefaults()
        {
            base.ReadInstrumentDefaults();
            bool localTryQueryPrint() { decimal argvalue = My.MySettings.Default.ThermalTransientApertureDefault; var ret = this.Session.TryQueryPrint( 7.4m, ref argvalue, "{0}.aperture", this.DefaultsName ); My.MySettings.Default.ThermalTransientApertureDefault = argvalue; return ret; }

            if ( !localTryQueryPrint() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient aperture;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint1() { decimal argvalue = My.MySettings.Default.ThermalTransientApertureMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minAperture", this.DefaultsName ); My.MySettings.Default.ThermalTransientApertureMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint1() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient minimum aperture;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint2() { decimal argvalue = My.MySettings.Default.ThermalTransientApertureMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxAperture", this.DefaultsName ); My.MySettings.Default.ThermalTransientApertureMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint2() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient maximum aperture;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint3() { decimal argvalue = My.MySettings.Default.ThermalTransientCurrentLevelDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.level", this.DefaultsName ); My.MySettings.Default.ThermalTransientCurrentLevelDefault = argvalue; return ret; }

            if ( !localTryQueryPrint3() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient current level;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint4() { decimal argvalue = My.MySettings.Default.ThermalTransientCurrentMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minCurrent", this.DefaultsName ); My.MySettings.Default.ThermalTransientCurrentMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint4() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient minimum current level;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint5() { decimal argvalue = My.MySettings.Default.ThermalTransientCurrentMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxCurrent", this.DefaultsName ); My.MySettings.Default.ThermalTransientCurrentMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint5() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient maximum current level;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint6() { decimal argvalue = My.MySettings.Default.ThermalTransientVoltageLimitDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.limit", this.DefaultsName ); My.MySettings.Default.ThermalTransientVoltageLimitDefault = argvalue; return ret; }

            if ( !localTryQueryPrint6() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient voltage limit;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint7() { decimal argvalue = My.MySettings.Default.ThermalTransientVoltageMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minVoltage", this.DefaultsName ); My.MySettings.Default.ThermalTransientVoltageMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint7() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient minimum voltage;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint8() { decimal argvalue = My.MySettings.Default.ThermalTransientVoltageMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxVoltage", this.DefaultsName ); My.MySettings.Default.ThermalTransientVoltageMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint8() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient maximum voltage;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint9() { decimal argvalue = My.MySettings.Default.ThermalTransientHighLimitDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.highLimit", this.DefaultsName ); My.MySettings.Default.ThermalTransientHighLimitDefault = argvalue; return ret; }

            if ( !localTryQueryPrint9() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient high limit;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint10() { decimal argvalue = My.MySettings.Default.ThermalTransientLowLimitDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.lowLimit", this.DefaultsName ); My.MySettings.Default.ThermalTransientLowLimitDefault = argvalue; return ret; }

            if ( !localTryQueryPrint10() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient low limit;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint11() { decimal argvalue = My.MySettings.Default.ThermalTransientVoltageChangeDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxVoltageChange", this.DefaultsName ); My.MySettings.Default.ThermalTransientVoltageChangeDefault = argvalue; return ret; }

            if ( !localTryQueryPrint11() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient maximum voltage change;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint12() { int argvalue = My.MySettings.Default.ThermalTransientMedianFilterLengthDefault; var ret = this.Session.TryQueryPrint( 1, ref argvalue, "{0}.medianFilterSize", this.DefaultsName ); My.MySettings.Default.ThermalTransientMedianFilterLengthDefault = argvalue; return ret; }

            if ( !localTryQueryPrint12() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient median filter size;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint13() { decimal argvalue = My.MySettings.Default.ThermalTransientSamplingIntervalDefault; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.period", this.DefaultsName ); My.MySettings.Default.ThermalTransientSamplingIntervalDefault = argvalue; return ret; }

            if ( !localTryQueryPrint13() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient period;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint14() { decimal argvalue = My.MySettings.Default.ThermalTransientSamplingIntervalMinimum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.minPeriod", this.DefaultsName ); My.MySettings.Default.ThermalTransientSamplingIntervalMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint14() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient minimum period;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint15() { decimal argvalue = My.MySettings.Default.ThermalTransientSamplingIntervalMaximum; var ret = this.Session.TryQueryPrint( 9.6m, ref argvalue, "{0}.maxPeriod", this.DefaultsName ); My.MySettings.Default.ThermalTransientSamplingIntervalMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint15() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient maximum period;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint16() { int argvalue = My.MySettings.Default.ThermalTransientTracePointsDefault; var ret = this.Session.TryQueryPrint( 1, ref argvalue, "{0}.points", this.DefaultsName ); My.MySettings.Default.ThermalTransientTracePointsDefault = argvalue; return ret; }

            if ( !localTryQueryPrint16() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient trace points;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint17() { int argvalue = My.MySettings.Default.ThermalTransientTracePointsMinimum; var ret = this.Session.TryQueryPrint( 1, ref argvalue, "{0}.minPoints", this.DefaultsName ); My.MySettings.Default.ThermalTransientTracePointsMinimum = argvalue; return ret; }

            if ( !localTryQueryPrint17() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient minimum trace points;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }

            bool localTryQueryPrint18() { decimal argvalue = My.MySettings.Default.ThermalTransientTracePointsMaximum; var ret = this.Session.TryQueryPrint( 1m, ref argvalue, "{0}.maxPoints", this.DefaultsName ); My.MySettings.Default.ThermalTransientTracePointsMaximum = argvalue; return ret; }

            if ( !localTryQueryPrint18() )
            {
                _ = this.PublishWarning( "failed reading default thermal transient maximum trace points;. Sent:'{0}; Received:'{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
            }
        }

        /// <summary> Configures the meter for making the thermal transient measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="thermalTransient"> The Thermal Transient element. </param>
        public void Configure( ThermalTransientBase thermalTransient )
        {
            if ( thermalTransient is null )
                throw new ArgumentNullException( nameof( thermalTransient ) );
            base.Configure( thermalTransient );
            _ = this.PublishVerbose( "Setting {0} post transient delay to {1};. ", this.BaseEntityName, thermalTransient.PostTransientDelay );
            _ = this.ApplyPostTransientDelay( thermalTransient.PostTransientDelay );
            _ = this.PublishVerbose( "{0} post transient delay set to {1};. ", this.BaseEntityName, thermalTransient.PostTransientDelay );
            _ = this.PublishVerbose( "Setting {0} Allowed Voltage Change to {1};. ", this.EntityName, thermalTransient.AllowedVoltageChange );
            _ = this.ApplyVoltageChange( thermalTransient.AllowedVoltageChange );
            _ = this.PublishVerbose( "{0} Allowed Voltage Change set to {1};. ", this.EntityName, thermalTransient.AllowedVoltageChange );
            this.StatusSubsystem.CheckThrowDeviceException( false, "configuring Thermal Transient measurement;. " );
            this.ThermalTransient.CheckThrowUnequalConfiguration( thermalTransient );
        }

        /// <summary> Applies changed meter configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="thermalTransient"> The Thermal Transient element. </param>
        public void ConfigureChanged( ThermalTransientBase thermalTransient )
        {
            if ( thermalTransient is null )
                throw new ArgumentNullException( nameof( thermalTransient ) );
            base.ConfigureChanged( thermalTransient );
            if ( !this.ThermalTransient.ConfigurationEquals( thermalTransient ) )
            {
                if ( !this.ThermalTransient.PostTransientDelay.Equals( thermalTransient.PostTransientDelay ) )
                {
                    _ = this.PublishVerbose( "Setting {0} post transient delay to {1};. ", this.BaseEntityName, thermalTransient.PostTransientDelay );
                    _ = this.ApplyPostTransientDelay( thermalTransient.PostTransientDelay );
                    _ = this.PublishVerbose( "{0} post transient delay set to {1};. ", this.BaseEntityName, thermalTransient.PostTransientDelay );
                }

                if ( !this.ThermalTransient.AllowedVoltageChange.Equals( thermalTransient.AllowedVoltageChange ) )
                {
                    _ = this.PublishVerbose( "Setting {0} Allowed Voltage Change to {1};. ", this.EntityName, thermalTransient.AllowedVoltageChange );
                    _ = this.ApplyVoltageChange( thermalTransient.AllowedVoltageChange );
                    _ = this.PublishVerbose( "{0} Allowed Voltage Change set to {1};. ", this.EntityName, thermalTransient.AllowedVoltageChange );
                }
            }

            this.StatusSubsystem.CheckThrowDeviceException( false, "configuring Thermal Transient measurement;. " );
            this.ThermalTransient.CheckThrowUnequalConfiguration( thermalTransient );
        }

        /// <summary> Queries the configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void QueryConfiguration()
        {
            base.QueryConfiguration();
            _ = this.QueryMedianFilterSize();
            _ = this.QueryPostTransientDelay();
            _ = this.QuerySamplingInterval();
            _ = this.QueryTracePoints();
            _ = this.QueryVoltageChange();
            this.StatusSubsystem.CheckThrowDeviceException( false, "Reading {0} configuration;. ", this.EntityName );
        }

        #endregion

        #region " MEASURE "

        /// <summary> Measures the Thermal Transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="thermalTransient"> The Thermal Transient element. </param>
        public new void Measure( ResistanceMeasureBase thermalTransient )
        {
            if ( thermalTransient is null )
                throw new ArgumentNullException( nameof( thermalTransient ) );
            if ( this.Session.IsSessionOpen )
            {
                base.Measure( thermalTransient );
                this.ReadThermalTransient( thermalTransient );
            }
            else
            {
                this.EmulateThermalTransient( thermalTransient );
            }
        }

        #endregion

        #region " READ "

        /// <summary> Gets or sets the trace ready to read sentinel. </summary>
        /// <value> The trace ready to read. </value>
        public bool NewTraceReadyToRead { get; set; }

        /// <summary>
        /// Reads the thermal transient. Sets the last <see cref="MeterSubsystemBase.LastReading">reading</see>,
        /// <see cref="MeterSubsystemBase.LastOutcome">outcome</see> <see cref="MeterSubsystemBase.LastMeasurementStatus">status</see> and
        /// <see cref="MeterSubsystemBase.MeasurementAvailable">Measurement available sentinel</see>.
        /// The outcome is left empty if measurements were not made.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void ReadThermalTransient()
        {
            this.NewTraceReadyToRead = false;
            if ( this.QueryOutcomeNil() )
            {
                this.LastReading = string.Empty;
                this.LastOutcome = string.Empty;
                this.LastMeasurementStatus = string.Empty;
                throw new InvalidOperationException( "Measurement Not made." );
            }
            else if ( this.QueryOkay() )
            {
                this.LastReading = this.Session.QueryPrintStringFormatTrimEnd( 9.6m, "{0}.voltageChange", ThermalTransientEstimatorEntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading voltage change;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastOutcome = "0";
                this.LastMeasurementStatus = string.Empty;
                this.NewTraceReadyToRead = true;
            }
            else
            {
                // if outcome failed, read and parse the outcome and status.
                this.LastOutcome = this.Session.QueryPrintStringFormatTrimEnd( 1, "{0}.outcome", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading outcome;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastMeasurementStatus = this.Session.QueryPrintStringFormatTrimEnd( 1, "{0}.status", this.EntityName );
                this.StatusSubsystem.CheckThrowDeviceException( false, "reading status;. Sent: '{0}'; Received: '{1}'.", this.Session.LastMessageSent, this.Session.LastMessageReceived );
                this.LastReading = string.Empty;
            }

            this.MeasurementAvailable = true;
        }

        /// <summary> Reads the Thermal Transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="thermalTransient"> The Thermal Transient element. </param>
        public void ReadThermalTransient( ResistanceMeasureBase thermalTransient )
        {
            if ( thermalTransient is null )
                throw new ArgumentNullException( nameof( thermalTransient ) );
            this.ReadThermalTransient();
            this.StatusSubsystem.CheckThrowDeviceException( false, "Reading Thermal Transient;. last command: '{0}'", this.Session.LastMessageSent );
            var measurementOutcome = MeasurementOutcomes.None;
            if ( this.LastOutcome != "0" )
            {
                string details = string.Empty;
                measurementOutcome = this.ParseOutcome( ref details );
                _ = this.PublishWarning( "Measurement failed;. Details: {0}", details );
            }

            this.ThermalTransient.ParseReading( this.LastReading, thermalTransient.CurrentLevel, measurementOutcome );
            thermalTransient.ParseReading( this.LastReading, thermalTransient.CurrentLevel, measurementOutcome );
        }

        /// <summary> Emulates a Thermal Transient. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="thermalTransient"> The Thermal Transient element. </param>
        public void EmulateThermalTransient( ResistanceMeasureBase thermalTransient )
        {
            if ( thermalTransient is null )
                throw new ArgumentNullException( nameof( thermalTransient ) );
            this.ThermalTransient.EmulateReading();
            this.LastReading = this.ThermalTransient.LastReading;
            this.LastOutcome = this.ThermalTransient.LastOutcome;
            this.LastMeasurementStatus = string.Empty;
            var measurementOutcome = MeasurementOutcomes.None;
            this.ThermalTransient.ParseReading( this.LastReading, thermalTransient.CurrentLevel, measurementOutcome );
            thermalTransient.ParseReading( this.LastReading, thermalTransient.CurrentLevel, measurementOutcome );
        }

        /// <summary>
        /// Reads the thermal transient trace from the instrument into a
        /// <see cref="LastTrace">comma-separated array of times in milliseconds and values in
        /// milli volts.</see> and
        /// <see cref="LastTimeSeries">collection of times in milliseconds and values in millivolts.</see>
        /// Also updates the outcome and status.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public void ReadThermalTransientTrace()
        {

            // clear the new trace sentinel
            this.NewTraceReadyToRead = false;
            string delimiter = ",";
            this.LastTimeSeries = new List<System.Windows.Point>();
            if ( this.LastOutcome != "0" )
            {
                this.LastTrace = string.Empty;
                throw new InvalidOperationException( "Measurement failed." );
            }
            else
            {
                var builder = new System.Text.StringBuilder();
                _ = this.Session.WriteLine( "{0}:printTimeSeries(4)", this.EntityName );
                this.StatusSubsystem.TraceVisaOperation( "sent query '{0}';. ", this.Session.LastMessageSent );
                while ( this.Session.QueryMessageAvailableStatus( TimeSpan.FromMilliseconds( 1d ), 3 ) )
                {
                    string reading = this.Session.ReadLine();
                    if ( builder.Length > 0 )
                    {
                        _ = builder.Append( delimiter );
                    }

                    _ = builder.Append( reading );
                    var xy = reading.Split( ',' );
                    if ( !float.TryParse( xy[0].Trim(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out float x ) )
                    {
                        x = -1;
                    }

                    if ( !float.TryParse( xy[1].Trim(), System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.CurrentCulture, out float y ) )
                    {
                        y = -1;
                    }

                    var point = new System.Windows.Point( x, y );
                    this.LastTimeSeries.Add( point );
                }

                this.LastTrace = builder.ToString();
            }

            this.NotifyPropertyChanged();
        }

        #endregion

        #region " ESTIMATE "

        /// <summary> Gets or sets the model. </summary>
        /// <value> The model. </value>
        public Numerical.Optima.PulseResponseFunction Model { get; private set; }

        /// <summary> The simplex. </summary>
        private Numerical.Optima.Simplex _Simplex;

        /// <summary> Model transient response. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        ///                                              null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="thermalTransient"> The thermal transient element. </param>
        public void ModelTransientResponse( ThermalTransient thermalTransient )
        {
            if ( thermalTransient is null )
            {
                throw new ArgumentNullException( nameof( thermalTransient ) );
            }
            else if ( this.LastTimeSeries is null )
            {
                throw new InvalidOperationException( "Last time series not read" );
            }
            else if ( this.LastTimeSeries.Count == 0 )
            {
                throw new InvalidOperationException( "Time series has no values" );
            }
            else if ( this.LastTimeSeries.Count <= 10 )
            {
                throw new InvalidOperationException( "Time series has less than 10 values" );
            }

            double voltageScale = 1000d; // scale to millivolts
            double timeScale = 1000d; // scale to milliseconds
            double flatnessPrecisionFactor = 0.0001d;
            double pulseDuration = timeScale * this.PulseDuration.TotalSeconds;
            double estimatedTau = timeScale * 0.5d * this.PulseDuration.TotalSeconds;
            double estimatedAsymptote = voltageScale * this.ThermalTransient.HighLimit;
            var voltageRange = new double[] { 0.5d * estimatedAsymptote, 2d * estimatedAsymptote };
            double voltagePresision = flatnessPrecisionFactor * estimatedAsymptote;
            var negativeInverseTauRange = new double[] { -1 / (0.5d * estimatedTau), -1 / (2d * estimatedTau) };
            double inverseTauPrecision = flatnessPrecisionFactor / estimatedTau;
            int dimension = 2;
            int maximumIterations = 100;
            double expectedDeviation = 0.05d * estimatedAsymptote;
            double expectedMaximumSSQ = this.ThermalTransient.TracePoints * expectedDeviation * expectedDeviation;
            double objectivePrecisionFactor = 0.0001d;
            double objectivePrecision = objectivePrecisionFactor * expectedMaximumSSQ;
            _ = this.PublishInfo( "Instantiating Simplex" );
            this._Simplex = new Numerical.Optima.Simplex( "Exponent", dimension, new double[] { voltageRange[0], negativeInverseTauRange[0] }, new double[] { voltageRange[1], negativeInverseTauRange[1] }, maximumIterations, new double[] { voltagePresision, inverseTauPrecision }, objectivePrecision );
            _ = this.PublishInfo( "Instantiating Thermal Transient model" );
            this.Model = new Numerical.Optima.PulseResponseFunction( this.LastTimeSeries );
            _ = this.PublishInfo( "Initializing simplex" );
            this._Simplex.Initialize( this.Model );
            _ = this.PublishVerbose( "Initial simplex is {0}", this._Simplex.ToString() );
            _ = this._Simplex.Solve();
            _ = this.PublishVerbose( "Finished in {0} Iterations", ( object ) this._Simplex.IterationNumber );
            _ = this.PublishVerbose( "Final simplex is {0}", this._Simplex.ToString() );
            this.Model.EvaluateFunctionValues( this._Simplex.BestSolution.Values() );
            _ = this.PublishVerbose( "Estimated Asymptote = {0:G4} mV", ( object ) this._Simplex.BestSolution.Values().ElementAtOrDefault( 0 ) );
            thermalTransient.Asymptote = 0.001d * this._Simplex.BestSolution.Values().ElementAtOrDefault( 0 );
            _ = this.PublishVerbose( "Estimated Time Constant = {0:G4} ms", ( object ) (-1 / this._Simplex.BestSolution.Values().ElementAtOrDefault( 1 )) );
            thermalTransient.TimeConstant = -0.001d / this._Simplex.BestSolution.Values().ElementAtOrDefault( 1 );
            _ = this.PublishVerbose( "Estimated Thermal Transient Voltage = {0:G4} mV", ( object ) this.Model.FunctionValue( this._Simplex.BestSolution.Values(), new double[] { pulseDuration, 0d } ) );
            thermalTransient.EstimatedVoltage = 0.001d * this.Model.FunctionValue( this._Simplex.BestSolution.Values(), new double[] { pulseDuration, 0d } );
            _ = this.PublishVerbose( "Correlation Coefficient = {0:G4}", ( object ) this.Model.EvaluateCorrelationCoefficient() );
            thermalTransient.CorrelationCoefficient = this.Model.EvaluateCorrelationCoefficient();
            _ = this.PublishVerbose( "Standard Error = {0:G4} mV", ( object ) this.Model.EvaluateStandardError( this._Simplex.BestSolution.Objective ) );
            thermalTransient.StandardError = 0.001d * this.Model.EvaluateStandardError( this._Simplex.BestSolution.Objective );
            thermalTransient.Iterations = this._Simplex.IterationNumber;
            thermalTransient.OptimizationOutcome = this._Simplex.Converged()
                ? ( OptimizationOutcome? ) OptimizationOutcome.Converged
                : this._Simplex.Optimized()
                    ? ( OptimizationOutcome? ) OptimizationOutcome.Optimized
                    : this._Simplex.IterationNumber >= maximumIterations ? ( OptimizationOutcome? ) OptimizationOutcome.Exhausted : ( OptimizationOutcome? ) OptimizationOutcome.None;

            _ = this.PublishVerbose( "Converged = {0}", ( object ) this._Simplex.Converged() );
            _ = this.PublishVerbose( "Optimized = {0}", ( object ) this._Simplex.Optimized() );
        }

        #endregion


    }
}
