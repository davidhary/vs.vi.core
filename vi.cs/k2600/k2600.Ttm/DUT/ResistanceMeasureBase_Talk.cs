﻿using System;
using System.Diagnostics;

using isr.Core;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{
    public partial class ResistanceMeasureBase
    {

        #region " I TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            base.ApplyTalkerTraceLevel( listenerType, value );
            if ( listenerType == ListenerType.Logger )
            {
                this.NotifyPropertyChanged( nameof( this.TraceLogLevel ) );
            }
            else
            {
                this.NotifyPropertyChanged( nameof( this.TraceShowLevel ) );
            }
        }

        #endregion

        #region " TALKER PUBLISH "

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}