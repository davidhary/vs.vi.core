﻿
namespace isr.VI.Tsp.K2600.Ttm
{
    public partial class ShuntResistance
    {

        /// <summary> Validates the current level or range described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateCurrent( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ShuntResistanceCurrentMinimum && value <= ( double ) My.MySettings.Default.ShuntResistanceCurrentMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ShuntResistanceCurrentMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Shunt Resistance Current value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ShuntResistanceCurrentMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Shunt Resistance Current value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ShuntResistanceCurrentMaximum );

            return affirmative;
        }

        /// <summary> Validates the limit described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateLimit( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ShuntResistanceMinimum && value <= ( double ) My.MySettings.Default.ShuntResistanceMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ShuntResistanceMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Shunt Resistance Limit value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ShuntResistanceMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Shunt Resistance Limit value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ShuntResistanceMaximum );

            return affirmative;
        }

        /// <summary> Validates the voltage limit described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateVoltageLimit( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ShuntResistanceVoltageMinimum && value <= ( double ) My.MySettings.Default.ShuntResistanceVoltageMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ShuntResistanceVoltageMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Shunt Resistance Voltage Limit value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ShuntResistanceVoltageMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Shunt Resistance Voltage Limit value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ShuntResistanceVoltageMaximum );

            return affirmative;
        }
    }
}