
namespace isr.VI.Tsp.K2600.Ttm
{
    public partial class ThermalTransient
    {

        /// <summary> Validates the allowed voltage change described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateVoltageChange( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalTransientVoltageMinimum && value <= ( double ) My.MySettings.Default.ThermalTransientVoltageMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalTransientVoltageMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Voltage Change value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientVoltageMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Voltage Change value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientVoltageMaximum );

            return affirmative;
        }

        /// <summary> Validates the aperture described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateAperture( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalTransientApertureMinimum && value <= ( double ) My.MySettings.Default.ThermalTransientApertureMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalTransientApertureMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient aperture value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientApertureMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient aperture value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientApertureMaximum );

            return affirmative;
        }

        /// <summary> Validates the current level described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateCurrentLevel( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalTransientCurrentMinimum && value <= ( double ) My.MySettings.Default.ThermalTransientCurrentMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalTransientCurrentMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient CurrentLevel value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientCurrentMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Current Level value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientCurrentMaximum );

            return affirmative;
        }

        /// <summary> Validates the limit described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateLimit( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalTransientVoltageChangeMinimum && value <= ( double ) My.MySettings.Default.ThermalTransientVoltageChangeMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalTransientVoltageMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Limit value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientVoltageMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Limit value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientVoltageMaximum );

            return affirmative;
        }

        /// <summary> Validates the median filter size described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateMedianFilterSize( int value, ref string details )
        {
            bool affirmative = value >= My.MySettings.Default.ThermalTransientMedianFilterLengthMinimum && value <= My.MySettings.Default.ThermalTransientMedianFilterLengthMaximum;
            details = affirmative
                ? string.Empty
                : value < My.MySettings.Default.ThermalTransientTracePointsMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Median Filter Length of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientMedianFilterLengthMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Median Filter Length value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientMedianFilterLengthMaximum );

            return affirmative;
        }

        /// <summary> Validates the Post Transient Delay described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidatePostTransientDelay( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.PostTransientDelayMinimum && value <= ( double ) My.MySettings.Default.PostTransientDelayMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.PostTransientDelayMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Post Transient Delay value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.PostTransientDelayMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Post Transient Delay value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.PostTransientDelayMaximum );

            return affirmative;
        }

        /// <summary> Validates the Duration described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="samplingInterval"> The sampling interval. </param>
        /// <param name="tracePoints">      The trace points. </param>
        /// <param name="details">          [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidatePulseWidth( double samplingInterval, int tracePoints, ref string details )
        {
            double pulseWidth = samplingInterval * tracePoints;
            bool affirmative = ValidateSamplingInterval( samplingInterval, ref details ) && ValidateTracePoints( tracePoints, ref details ) && pulseWidth >= ( double ) My.MySettings.Default.ThermalTransientDurationMinimum && pulseWidth <= ( double ) My.MySettings.Default.ThermalTransientDurationMaximum;
            if ( affirmative )
            {
                details = string.Empty;
            }
            else if ( !string.IsNullOrWhiteSpace( details ) )
            {
            }
            // we have details. nothing to do.
            else
            {
                details = pulseWidth < ( double ) My.MySettings.Default.ThermalTransientDurationMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Pulse Width of {0}s is lower than the minimum of {1}s.", pulseWidth, My.MySettings.Default.ThermalTransientDurationMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Pulse Width of {0}s is higher than the maximum of {1}s.", pulseWidth, My.MySettings.Default.ThermalTransientDurationMaximum );
            }

            return affirmative;
        }

        /// <summary> Validates the sampling interval described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateSamplingInterval( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalTransientSamplingIntervalMinimum && value <= ( double ) My.MySettings.Default.ThermalTransientSamplingIntervalMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalTransientSamplingIntervalMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Sample Interval value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientSamplingIntervalMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Sample Interval value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientSamplingIntervalMaximum );

            return affirmative;
        }

        /// <summary> Validates the trace points described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateTracePoints( int value, ref string details )
        {
            bool affirmative = value >= My.MySettings.Default.ThermalTransientTracePointsMinimum && value <= My.MySettings.Default.ThermalTransientTracePointsMaximum;
            details = affirmative
                ? string.Empty
                : value < My.MySettings.Default.ThermalTransientTracePointsMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Trace Points value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientTracePointsMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Trace Points value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientTracePointsMaximum );

            return affirmative;
        }

        /// <summary> Validates the voltage limit described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateVoltageLimit( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalTransientVoltageMinimum && value <= ( double ) My.MySettings.Default.ThermalTransientVoltageMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalTransientVoltageMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Voltage Limit value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalTransientVoltageMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Transient Voltage Limit value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ThermalTransientVoltageMaximum );

            return affirmative;
        }

        /// <summary> Validates the transient voltage limit described by details. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">                 The value. </param>
        /// <param name="maximumColdResistance"> The maximum cold resistance. </param>
        /// <param name="currentLevel">          The current level. </param>
        /// <param name="allowedVoltageChange">  The allowed voltage change. </param>
        /// <param name="details">               [in,out] The details. </param>
        /// <returns>
        /// <c>True</c> if the voltage limit is in rage; <c>False</c> is the limit is too low.
        /// </returns>
        public static bool ValidateVoltageLimit( double value, double maximumColdResistance, double currentLevel, double allowedVoltageChange, ref string details )
        {
            double expectedMaximumVoltage = maximumColdResistance * currentLevel + allowedVoltageChange;
            details = string.Empty;
            bool affirmative = ValidateVoltageChange( allowedVoltageChange, ref details )
                                    && ValidateCurrentLevel( currentLevel, ref details )
                                    && ValidateVoltageLimit( value, ref details )
                                    && value >= expectedMaximumVoltage;
            if ( !affirmative && string.IsNullOrWhiteSpace( details ) ) 
            {
                details = $"A Thermal transient voltage limit of {value} volts is too low;. Based on the cold resistance high limit and thermal transient current level and voltage change, the voltage might reach {expectedMaximumVoltage} volts.";
            }
            // we have details. nothing to do.
#if false
            else if ( expectedMaximumVoltage < expectedMaximumVoltage )
            {
                details = $"A Thermal transient voltage limit of {value} volts is too low;. Based on the cold resistance high limit and thermal transient current level and voltage change, the voltage might reach {expectedMaximumVoltage} volts.";
            }
#endif
            return affirmative;
        }

#region " ESTIMATOR "

        /// <summary> Validates the ThermalCoefficient described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateThermalCoefficient( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ThermalCoefficientMinimum && value <= ( double ) My.MySettings.Default.ThermalCoefficientMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ThermalCoefficientMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Coefficient value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ThermalCoefficientMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Thermal Coefficient value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ThermalCoefficientMaximum );

            return affirmative;
        }

#endregion

    }
}
