using System;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Defines a measured cold resistance element. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2009-02-02, 2.1.3320.x. </para>
    /// </remarks>
    public abstract class ColdResistanceBase : ResistanceMeasureBase, IEquatable<ColdResistanceBase>
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected ColdResistanceBase() : base()
        {
        }

        /// <summary> Clones an existing measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        protected ColdResistanceBase( ColdResistanceBase value ) : base( value )
        {
        }

        #endregion

        #region " PRESET "

        /// <summary> Restores defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void ResetKnownState()
        {
            base.ResetKnownState();
            this.Aperture = ( double ) My.MySettings.Default.ColdResistanceApertureDefault;
            this.CurrentLevel = ( double ) My.MySettings.Default.ColdResistanceCurrentLevelDefault;
            this.LowLimit = ( double ) My.MySettings.Default.ColdResistanceLowLimitDefault;
            this.HighLimit = ( double ) My.MySettings.Default.ColdResistanceHighLimitDefault;
            this.VoltageLimit = ( double ) My.MySettings.Default.ColdResistanceVoltageLimitDefault;
            Core.ApplianceBase.DoEvents();
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Indicates whether the current <see cref="T:ColdResistanceBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="obj"> An object. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as ColdResistanceBase );
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ColdResistanceBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The cold resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:ColdResistanceBase"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( ColdResistanceBase other )
        {
            return other is object && this.Reading.Equals( other.Reading ) && this.ConfigurationEquals( other ) && true;
        }

        /// <summary> Returns a hash code for this instance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A hash code for this object. </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ColdResistanceBase left, ColdResistanceBase right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ColdResistanceBase left, ColdResistanceBase right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        #endregion

    }
}
