using System;
using System.Diagnostics;

using isr.Core.Models;
using isr.Core.NumericExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Defines a measured cold resistance element. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2009-02-02, 2.1.3320.x. </para>
    /// </remarks>
    public abstract partial class ResistanceMeasureBase : ViewModelTalkerBase
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected ResistanceMeasureBase() : base()
        {
            this._RandomNumberGenerator = new Random( DateTimeOffset.Now.Second );
        }

        /// <summary> Clones an existing measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        protected ResistanceMeasureBase( ResistanceMeasureBase value ) : this()
        {
            if ( value is object )
            {
                // measurement
                this._ResistanceDisplayFormat = value.ResistanceDisplayFormat;
                this._Outcome = value.Outcome;
                this._Resistance = value.Resistance;
                this._ResistanceCaption = value.ResistanceCaption;
                this._Reading = value.Reading;
                this._Voltage = value.Voltage;
                this._VoltageCaption = value.VoltageCaption;
                this._Timestamp = value.Timestamp;
                // Configuration
                this._SourceMeasureUnit = value.SourceMeasureUnit;
                this._Aperture = value.Aperture;
                this._CurrentLevel = value.CurrentLevel;
                this._HighLimit = value.HighLimit;
                this._LowLimit = value.LowLimit;
                this._VoltageLimit = value.VoltageLimit;
            }
        }

        /// <summary> Copies the configuration described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public virtual void CopyConfiguration( ResistanceMeasureBase value )
        {
            if ( value is object )
            {
                this._SourceMeasureUnit = value.SourceMeasureUnit;
                this._Aperture = value.Aperture;
                this._CurrentLevel = value.CurrentLevel;
                this._HighLimit = value.HighLimit;
                this._LowLimit = value.LowLimit;
                this._VoltageLimit = value.VoltageLimit;
            }
        }

        /// <summary> Copies the measurement described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public virtual void CopyMeasurement( ResistanceMeasureBase value )
        {
            if ( value is object )
            {
                this._ResistanceDisplayFormat = value.ResistanceDisplayFormat;
                this._Outcome = value.Outcome;
                this._Resistance = value.Resistance;
                this._ResistanceCaption = value.ResistanceCaption;
                this._Reading = value.Reading;
                this._Voltage = value.Voltage;
                this._VoltageCaption = value.VoltageCaption;
                this._Timestamp = value.Timestamp;
            }
        }

        #endregion

        #region " PRESET "

        /// <summary> Sets values to their known clear execution state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void DefineClearExecutionState()
        {
            this.LastReading = string.Empty;
            this.LastOutcome = string.Empty;
            this.Outcome = MeasurementOutcomes.None;
            this.Timestamp = DateTimeOffset.MinValue;
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public virtual void InitKnownState()
        {
        }

        /// <summary> Sets the known preset state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void PresetKnownState()
        {
        }

        /// <summary> Restores defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void ResetKnownState()
        {
            this.SourceMeasureUnit = My.MySettings.Default.SourceMeasureUnitDefault;
            this.ResistanceDisplayFormat = "0.000";
            this.VoltageDisplayFormat = "0.0000";
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Indicates whether the current <see cref="T:ColdResistanceBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The cold resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:ColdResistanceBase"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( ResistanceMeasureBase other )
        {
            return other is object && this.Reading.Equals( other.Reading ) && this.ConfigurationEquals( other ) && true;
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ResistanceMeasureBase"></see> configuration values
        /// are equal to a specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:ResistanceMeasureBase"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public virtual bool ConfigurationEquals( ResistanceMeasureBase other )
        {
            return other is object && string.Equals( this.SourceMeasureUnit, other.SourceMeasureUnit ) && this.Aperture.Approximates( other.Aperture, 0.00001d ) && this.CurrentLevel.Approximates( other.CurrentLevel, 0.000001d ) && this.HighLimit.Approximates( other.HighLimit, 0.0001d ) && this.LowLimit.Approximates( other.LowLimit, 0.0001d ) && this.VoltageLimit.Approximates( other.VoltageLimit, 0.0001d ) && true;
        }

        /// <summary> Check throw if unequal configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="other"> The resistance to compare to this object. </param>
        public virtual void CheckThrowUnequalConfiguration( ResistanceMeasureBase other )
        {
            if ( other is object )
            {
                if ( !this.ConfigurationEquals( other ) )
                {
                    string format = "Unequal configuring--instrument {0} value of {1} is not {2}";
                    if ( !string.Equals( this.SourceMeasureUnit, other.SourceMeasureUnit ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Source Measure Unit", this.SourceMeasureUnit, other.SourceMeasureUnit ) );
                    }
                    else if ( !this.Aperture.Approximates( other.Aperture, 0.00001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Aperture", this.Aperture, other.Aperture ) );
                    }
                    else if ( !this.CurrentLevel.Approximates( other.CurrentLevel, 0.00001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Current Level", this.CurrentLevel, other.CurrentLevel ) );
                    }
                    else if ( !this.HighLimit.Approximates( other.HighLimit, 0.00001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "High Limit", this.HighLimit, other.HighLimit ) );
                    }
                    else if ( !this.LowLimit.Approximates( other.LowLimit, 0.00001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Low Limit", this.LowLimit, other.LowLimit ) );
                    }
                    else if ( !this.VoltageLimit.Approximates( other.VoltageLimit, 0.0001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Voltage Limit", this.VoltageLimit, other.VoltageLimit ) );
                    }
                    else
                    {
                        Debug.Assert( !Debugger.IsAttached, "Failed logic" );
                    }
                }
            }
        }

        #endregion

        #region " DISPLAY PROPERTIES "

        /// <summary> The resistance display format. </summary>
        private string _ResistanceDisplayFormat;

        /// <summary> Gets or sets the display format for resistance. </summary>
        /// <value> The display format. </value>
        public string ResistanceDisplayFormat
        {
            get => this._ResistanceDisplayFormat;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResistanceDisplayFormat, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ResistanceDisplayFormat = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The voltage display format. </summary>
        private string _VoltageDisplayFormat;

        /// <summary> Gets or sets the display format for Voltage. </summary>
        /// <value> The display format. </value>
        public string VoltageDisplayFormat
        {
            get => this._VoltageDisplayFormat;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.VoltageDisplayFormat, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._VoltageDisplayFormat = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " PARSER "

        /// <summary> The last reading. </summary>
        private string _LastReading;

        /// <summary> Gets or sets the last reading. </summary>
        /// <value> The last reading. </value>
        public string LastReading
        {
            get => this._LastReading;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.LastReading, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LastReading = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last outcome. </summary>
        private string _LastOutcome;

        /// <summary> Gets or sets the last outcome. </summary>
        /// <value> The last outcome. </value>
        public string LastOutcome
        {
            get => this._LastOutcome;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.LastOutcome, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LastOutcome = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The last status. </summary>
        private string _LastStatus;

        /// <summary> Gets or sets the last status. </summary>
        /// <value> The last status. </value>
        public string LastStatus
        {
            get => this._LastStatus;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.LastStatus, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LastStatus = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #region " RESISTANCE "

        /// <summary> Sets the reading and outcome. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resistanceReading"> Specifies the reading as received from the instrument. </param>
        /// <param name="outcome">           . </param>
        public void ParseReading( string resistanceReading, MeasurementOutcomes outcome )
        {
            double value = 0d;
            if ( string.IsNullOrWhiteSpace( resistanceReading ) )
            {
                this.Reading = string.Empty;
                this.Outcome = outcome != MeasurementOutcomes.None ? MeasurementOutcomes.MeasurementFailed | outcome : outcome;
            }
            else
            {
                var numberFormat = System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent;
                this.Reading = resistanceReading;
                this.Outcome = double.TryParse( this.Reading, numberFormat, System.Globalization.CultureInfo.InvariantCulture, out value ) ? value >= this.LowLimit && value <= this.HighLimit ? MeasurementOutcomes.PartPassed : outcome | MeasurementOutcomes.PartFailed : MeasurementOutcomes.MeasurementFailed | MeasurementOutcomes.UnexpectedReadingFormat;
            }

            this.Resistance = value;
            this.Voltage = this.CurrentLevel != 0d ? value + this.CurrentLevel : 0d;
            this.Timestamp = DateTimeOffset.Now;
            this.MeasurementAvailable = true;
        }

        #endregion

        #region " VOLTAGE READING "

        /// <summary> Applies the contact check outcome described by outcome. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="outcome"> . </param>
        public void ApplyContactCheckOutcome( MeasurementOutcomes outcome )
        {
            this.Outcome |= outcome;
        }

        /// <summary> Sets the readings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="voltageReading"> Specifies the voltage reading. </param>
        /// <param name="current">        Specifies the current level. </param>
        /// <param name="outcome">        Specifies the outcome. </param>
        public void ParseReading( string voltageReading, double current, MeasurementOutcomes outcome )
        {
            if ( string.IsNullOrWhiteSpace( voltageReading ) )
            {
                this.Voltage = 0d;
                this.Resistance = 0d;
                this.Outcome = outcome != MeasurementOutcomes.None ? MeasurementOutcomes.MeasurementFailed | outcome : outcome;
            }
            else
            {
                var numberFormat = System.Globalization.NumberStyles.Number | System.Globalization.NumberStyles.AllowExponent;
                this.Reading = voltageReading;
                if ( double.TryParse( this.Reading, numberFormat, System.Globalization.CultureInfo.InvariantCulture, out _ ) )
                {
                    this.Resistance = current != 0d ? 0d / current : 0d;
                    this.Outcome = 0d >= this.LowLimit && 0d <= this.HighLimit ? this.Outcome | MeasurementOutcomes.PartPassed : this.Outcome | MeasurementOutcomes.PartFailed;
                    this.Voltage = 0d;
                }
                else
                {
                    this.Voltage = 0d;
                    this.Resistance = 0d;
                    this.Outcome = MeasurementOutcomes.MeasurementFailed | MeasurementOutcomes.UnexpectedReadingFormat;
                }
            }

            this.Timestamp = DateTimeOffset.Now;
            this.MeasurementAvailable = true;
        }

        #endregion

        #endregion

        #region " MEASUREMENT PROPERTIES "

        /// <summary> True if measurement available. </summary>
        private bool _MeasurementAvailable;

        /// <summary> Gets or sets (protected) the measurement available. </summary>
        /// <value> The measurement available. </value>
        public bool MeasurementAvailable
        {
            get => this._MeasurementAvailable;

            set {
                this._MeasurementAvailable = value;
                this.SyncNotifyPropertyChanged();
            }
        }

        /// <summary> The outcome. </summary>
        private MeasurementOutcomes _Outcome;

        /// <summary> Gets or sets the measurement outcome. </summary>
        /// <value> The outcome. </value>
        public MeasurementOutcomes Outcome
        {
            get => this._Outcome;

            set {
                if ( !value.Equals( this.Outcome ) )
                {
                    this._Outcome = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The reading. </summary>
        private string _Reading;

        /// <summary>
        /// Gets or sets  or sets (protected) the reading.  When set, the value is converted to
        /// resistance.
        /// </summary>
        /// <value> The reading. </value>
        public string Reading
        {
            get => this._Reading;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.Reading, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._Reading = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The resistance. </summary>
        private double _Resistance;

        /// <summary> Gets or sets (protected) the measured resistance. </summary>
        /// <value> The resistance. </value>
        public double Resistance
        {
            get => this._Resistance;

            set {
                if ( !value.Equals( this.Resistance ) )
                {
                    this._Resistance = value;
                    this.NotifyPropertyChanged();
                    this.ResistanceCaption = this.Outcome == MeasurementOutcomes.None || (this.Outcome & MeasurementOutcomes.MeasurementNotMade) != 0
                        ? string.Empty
                        : string.IsNullOrWhiteSpace( this.Reading ) || (this.Outcome & MeasurementOutcomes.MeasurementFailed) != 0
                            ? "#null#"
                            : this._Resistance.ToString( this.ResistanceDisplayFormat, System.Globalization.CultureInfo.CurrentCulture );
                }
            }
        }

        /// <summary> The resistance caption. </summary>
        private string _ResistanceCaption;

        /// <summary> Gets or sets (protected) the measured resistance display caption. </summary>
        /// <value> The resistance caption. </value>
        public string ResistanceCaption
        {
            get => this._ResistanceCaption;

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.ResistanceCaption, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._ResistanceCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Date/Time of the timestamp. </summary>
        private DateTimeOffset _Timestamp;

        /// <summary>
        /// Gets or sets (protected) the measurement time stamp. Gets set when setting the reading or
        /// resistance value.
        /// </summary>
        /// <value> The timestamp. </value>
        public DateTimeOffset Timestamp
        {
            get => this._Timestamp;

            protected set {
                if ( !value.Equals( this.Timestamp ) )
                {
                    this._Timestamp = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The random number generator. </summary>
        private readonly Random _RandomNumberGenerator;

        /// <summary> Generates a random reading. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The random reading. </returns>
        public double GenerateRandomReading()
        {
            double k = 1000d;
            return this._RandomNumberGenerator.Next( ( int ) Math.Round( k * this.LowLimit ), ( int ) Math.Round( k * this.HighLimit ) ) / k;
        }

        /// <summary> Emulates a reading. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void EmulateReading()
        {
            this.LastReading = this.GenerateRandomReading().ToString();
            this.LastOutcome = "0";
        }

        /// <summary> The voltage. </summary>
        private double _Voltage;

        /// <summary> Gets or sets the measured Voltage. </summary>
        /// <value> The Voltage. </value>
        public double Voltage
        {
            get => this._Voltage;

            set {
                if ( !value.Equals( this.Voltage ) )
                {
                    this._Voltage = value;
                    this.NotifyPropertyChanged();
                    this.VoltageCaption = (this.Outcome & MeasurementOutcomes.MeasurementNotMade) != 0
                        ? string.Empty
                        : string.IsNullOrWhiteSpace( this.Reading ) || (this.Outcome & MeasurementOutcomes.MeasurementFailed) != 0
                            ? "#null#"
                            : this._Voltage.ToString( this.VoltageDisplayFormat, System.Globalization.CultureInfo.CurrentCulture );
                }
            }
        }

        /// <summary> The voltage caption. </summary>
        private string _VoltageCaption;

        /// <summary> Gets or sets the measured Voltage display caption. </summary>
        /// <value> The Voltage caption. </value>
        public string VoltageCaption
        {
            get => this._VoltageCaption;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.VoltageCaption, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._VoltageCaption = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " CONFIGURATION PROPERTIES "

        /// <summary> Source measure unit. </summary>
        private string _SourceMeasureUnit;

        /// <summary> Gets or sets the cached Source Measure Unit. </summary>
        /// <value> The Source Measure Unit, e.g., 'smua' or 'smub'. </value>
        public virtual string SourceMeasureUnit
        {
            get => this._SourceMeasureUnit;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !value.Equals( this.SourceMeasureUnit ) )
                {
                    this._SourceMeasureUnit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The aperture. </summary>
        private double _Aperture;

        /// <summary>
        /// Gets or sets the integration period in number of power line cycles for measuring the cold
        /// resistance.
        /// </summary>
        /// <value> The aperture. </value>
        public double Aperture
        {
            get => this._Aperture;

            set {
                if ( !value.Equals( this.Aperture ) )
                {
                    this._Aperture = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The current level. </summary>
        private double _CurrentLevel;

        /// <summary> Gets or sets the current level. </summary>
        /// <value> The current level. </value>
        public double CurrentLevel
        {
            get => this._CurrentLevel;

            set {
                if ( !value.Equals( this.CurrentLevel ) )
                {
                    this._CurrentLevel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The high limit. </summary>
        private double _HighLimit;

        /// <summary>
        /// Gets or sets the high limit for determining measurement pass fail condition.
        /// </summary>
        /// <value> The high limit. </value>
        public double HighLimit
        {
            get => this._HighLimit;

            set {
                if ( !value.Equals( this.HighLimit ) )
                {
                    this._HighLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The low limit. </summary>
        private double _LowLimit;

        /// <summary>
        /// Gets or sets the low limit for determining measurement pass fail condition.
        /// </summary>
        /// <value> The low limit. </value>
        public double LowLimit
        {
            get => this._LowLimit;

            set {
                if ( !value.Equals( this.LowLimit ) )
                {
                    this._LowLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The voltage limit. </summary>
        private double _VoltageLimit;

        /// <summary> Gets or sets the voltage limit. </summary>
        /// <value> The voltage limit. </value>
        public double VoltageLimit
        {
            get => this._VoltageLimit;

            set {
                if ( !value.Equals( this.VoltageLimit ) )
                {
                    this._VoltageLimit = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}
