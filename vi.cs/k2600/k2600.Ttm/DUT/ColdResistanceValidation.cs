﻿
namespace isr.VI.Tsp.K2600.Ttm
{
    public partial class ColdResistance
    {

        /// <summary> Validates the aperture described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateAperture( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ColdResistanceApertureMinimum && value <= ( double ) My.MySettings.Default.ColdResistanceApertureMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ColdResistanceApertureMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance aperture value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ColdResistanceApertureMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance aperture value of {0} is higher than the maximum of {1}.", value, My.MySettings.Default.ColdResistanceApertureMaximum );

            return affirmative;
        }

        /// <summary> Validates the current level described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateCurrentLevel( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ColdResistanceCurrentMinimum && value <= ( double ) My.MySettings.Default.ColdResistanceCurrentMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ColdResistanceCurrentMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance Current value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ColdResistanceCurrentMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance Current value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ColdResistanceCurrentMaximum );

            return affirmative;
        }

        /// <summary> Validates the limit described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateLimit( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ColdResistanceMinimum && value <= ( double ) My.MySettings.Default.ColdResistanceMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ColdResistanceMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance Limit value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ColdResistanceMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance Limit value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ColdResistanceMaximum );

            return affirmative;
        }

        /// <summary> Validates the voltage limit described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">   The value. </param>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
        public static bool ValidateVoltageLimit( double value, ref string details )
        {
            bool affirmative = value >= ( double ) My.MySettings.Default.ColdResistanceVoltageMinimum && value <= ( double ) My.MySettings.Default.ColdResistanceVoltageMaximum;
            details = affirmative
                ? string.Empty
                : value < ( double ) My.MySettings.Default.ColdResistanceVoltageMinimum
                    ? string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance Voltage Limit value of {0} is lower than the minimum of {1}.", value, My.MySettings.Default.ColdResistanceVoltageMinimum )
                    : string.Format( System.Globalization.CultureInfo.CurrentCulture, "Cold Resistance Voltage Limit value of {0} is high than the maximum of {1}.", value, My.MySettings.Default.ColdResistanceVoltageMaximum );

            return affirmative;
        }
    }
}