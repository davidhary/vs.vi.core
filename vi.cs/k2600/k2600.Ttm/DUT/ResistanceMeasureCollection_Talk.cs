using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;

using isr.Core;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{
    public partial class ResistanceMeasureCollection : ITalker
    {

        #region " TALKER ASSIGNMENTS "

        /// <summary> The talker. </summary>
        private ITraceMessageTalker _Talker;

        /// <summary> Gets or sets the trace message talker. </summary>
        /// <value> The trace message talker. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ITraceMessageTalker Talker
        {
            get => this._Talker;

            private set {
                if ( this._Talker is object )
                {
                    this.RemoveListeners();
                }

                this._Talker = value;
            }
        }

        /// <summary> True if is assigned talker, false if not. </summary>
        private bool _IsAssignedTalker;

        /// <summary> Assigns a talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public virtual void AssignTalker( ITraceMessageTalker talker )
        {
            this._IsAssignedTalker = talker is object;
            this.Talker = this._IsAssignedTalker ? talker : new TraceMessageTalker();
            this.IdentifyTalkers();
        }

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void IdentifyTalkers()
        {
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        #endregion

        #region " LISTENERS "

        /// <summary>
        /// Removes the private listeners. Removes all listeners if the talker was not assigned.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void RemoveListeners()
        {
            this.RemovePrivateListeners();
            if ( !this._IsAssignedTalker )
                this.Talker.RemoveListeners();
        }

        /// <summary> The private listeners. </summary>
        private List<IMessageListener> _PrivateListeners;

        /// <summary> Gets the private listeners. </summary>
        /// <value> The private listeners. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public virtual IList<IMessageListener> PrivateListeners
        {
            get {
                if ( this._PrivateListeners is null )
                    this._PrivateListeners = new List<IMessageListener>();
                return this._PrivateListeners;
            }
        }

        /// <summary> Adds a private listener. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void AddPrivateListener( IMessageListener listener )
        {
            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.Add( listener );
            }

            this.AddListener( listener );
        }

        /// <summary> Adds private listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void AddPrivateListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            if ( this.PrivateListeners is object )
            {
                this._PrivateListeners.AddRange( listeners );
            }

            this.AddListeners( listeners );
        }

        /// <summary> Adds private listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public virtual void AddPrivateListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
                throw new ArgumentNullException( nameof( talker ) );
            this.AddPrivateListeners( talker.Listeners );
        }

        /// <summary> Removes the private listener described by listener. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void RemovePrivateListener( IMessageListener listener )
        {
            this.RemoveListener( listener );
            if ( this.PrivateListeners is object )
            {
                _ = this._PrivateListeners.Remove( listener );
            }
        }

        /// <summary> Removes the private listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void RemovePrivateListeners()
        {
            foreach ( IMessageListener listener in this.PrivateListeners )
                this.RemoveListener( listener );
            this._PrivateListeners.Clear();
        }

        /// <summary> Removes the listener described by listener. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void RemoveListener( IMessageListener listener )
        {
            this.Talker.RemoveListener( listener );
        }

        /// <summary> Removes the specified listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void RemoveListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            foreach ( IMessageListener listener in listeners )
                this.RemoveListener( listener );
        }

        /// <summary> Adds the listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="listeners"> The listeners. </param>
        public virtual void AddListeners( IList<IMessageListener> listeners )
        {
            if ( listeners is null )
                throw new ArgumentNullException( nameof( listeners ) );
            foreach ( IMessageListener listener in listeners )
                this.AddListener( listener );
        }

        /// <summary> Adds the listeners. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="talker"> The talker. </param>
        public virtual void AddListeners( ITraceMessageTalker talker )
        {
            if ( talker is null )
                throw new ArgumentNullException( nameof( talker ) );
            this.AddListeners( talker.Listeners );
        }

        #endregion

        #region " TALKER PUBLISH "

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="message"> The message. </param>
        /// <returns> A String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected string Publish( TraceMessage message )
        {
            if ( message is null )
                return string.Empty;
            string activity = string.Empty;
            string result = message.Details;
            try
            {
                if ( this.Talker is null )
                {
                    activity = $"logging unpublished message: {message}";
                    _ = My.MyLibrary.Appliance.LogUnpublishedMessage( message );
                }
                else
                {
                    activity = $"publishing: {message}";
                    _ = this.Talker.Publish( message );
                }
            }
            catch ( Exception ex )
            {
                if ( Debugger.IsAttached )
                {
                    Debug.Assert( !Debugger.IsAttached, $"Exception {activity};. {ex.ToFullBlownString()}" );
                }
                else
                {
                    _ = WindowsForms.ShowDialog( ex );
                }
            }

            return result;
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected virtual string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected virtual string PublishException( string activity, Exception ex )
        {
            return ex is null ? string.Empty : this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Action event information. </param>
        /// <returns> A String. </returns>
        protected string Publish( ActionEventArgs e )
        {
            return e is null ? string.Empty : this.Publish( e.OutcomeEvent, e.Details );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="format">    Describes the format to use. </param>
        /// <param name="args">      A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string Publish( TraceEventType eventType, string format, params object[] args )
        {
            return this.Publish( eventType, string.Format( format, args ) );
        }

        /// <summary> Publish warning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishWarning( string activity )
        {
            return this.Publish( TraceEventType.Warning, activity );
        }

        /// <summary> Publish warning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string PublishWarning( string format, params object[] args )
        {
            return this.Publish( TraceEventType.Warning, format, args );
        }

        /// <summary> Publish verbose. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishVerbose( string activity )
        {
            return this.Publish( TraceEventType.Verbose, activity );
        }

        /// <summary> Publish verbose. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string PublishVerbose( string format, params object[] args )
        {
            return this.Publish( TraceEventType.Verbose, format, args );
        }

        /// <summary> Publish information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <returns> A String. </returns>
        protected string PublishInfo( string activity )
        {
            return this.Publish( TraceEventType.Information, activity );
        }

        /// <summary> Publish information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        /// <returns> A String. </returns>
        protected string PublishInfo( string format, params object[] args )
        {
            return this.Publish( TraceEventType.Information, format, args );
        }

        #endregion

        #region " CUSTOM FUNCTIONS "

        /// <summary> Adds a listener. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listener"> The listener. </param>
        public virtual void AddListener( IMessageListener listener )
        {
            this.Talker.AddListener( listener );
            foreach ( ITalker element in this.Items )
                element.AddListener( listener );
            this.IdentifyTalkers();
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public virtual void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            // this should apply only to the listeners associated with this form
            // Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
            foreach ( ITalker element in this.Items )
                element.ApplyListenerTraceLevel( listenerType, value );
            this.IdentifyTalkers();
        }

        /// <summary> Applies the trace level type to all talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the trace level. </param>
        /// <param name="value">        The value. </param>
        public virtual void ApplyTalkerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this.Talker.ApplyTalkerTraceLevel( listenerType, value );
            foreach ( ITalker element in this.Items )
                element.ApplyTalkerTraceLevel( listenerType, value );
        }

        /// <summary> Applies the talker trace levels described by talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public virtual void ApplyTalkerTraceLevels( ITraceMessageTalker talker )
        {
            foreach ( ITalker element in this.Items )
                element.ApplyTalkerTraceLevels( talker );
            this.Talker.ApplyTalkerTraceLevels( talker );
        }

        /// <summary> Applies the talker listeners trace levels described by talker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="talker"> The talker. </param>
        public virtual void ApplyListenerTraceLevels( ITraceMessageTalker talker )
        {
            this.Talker.ApplyListenerTraceLevels( talker );
            foreach ( ITalker element in this.Items )
                element.ApplyListenerTraceLevels( talker );
        }

        #endregion

    }
}
