using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Runtime.CompilerServices;

using isr.Core.Models;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary>
    /// Defines the device under test element including measurement and configuration.
    /// </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2009-02-02, 2.1.3320.x. </para>
    /// </remarks>
    public class DeviceUnderTest : ViewModelBase, ICloneable
    {

        #region " CONSTRUCTION "

        /// <summary> Primary Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public DeviceUnderTest() : base()
        {
            // initial resistance must be first.
            this.Elements = new ResistanceMeasureCollection();
            // TO_DO: Bind elements and display initial property values.
            this.InitialResistance = new ColdResistance();
            this.Elements.Add( this.InitialResistance );
            this.FinalResistance = new ColdResistance();
            this.Elements.Add( this.FinalResistance );
            this.ShuntResistance = new ShuntResistance();
            this.Elements.Add( this.ShuntResistance );
            this.ThermalTransient = new ThermalTransient();
            this.Elements.Add( this.ThermalTransient );
        }

        /// <summary> Clones an existing part. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public DeviceUnderTest( DeviceUnderTest value ) : base()
        {
            if ( value is object )
            {
                this.PartNumber = value.PartNumber;
                this.OperatorId = value.OperatorId;
                this.LotId = value.LotId;
                this.SampleNumber = value.SampleNumber;
                this.SerialNumber = value.SerialNumber;
                this.ContactCheckEnabled = value.ContactCheckEnabled;
                this.ContactCheckThreshold = value.ContactCheckThreshold;
                // Measurements
                this.Elements = new ResistanceMeasureCollection();
                // initial resistance must be first.
                this.InitialResistance = new ColdResistance( value.InitialResistance );
                this.Elements.Add( this.InitialResistance );
                this.FinalResistance = new ColdResistance( value.FinalResistance );
                this.Elements.Add( this.FinalResistance );
                this.ShuntResistance = new ShuntResistance( value.ShuntResistance );
                this.Elements.Add( this.ShuntResistance );
                this.ThermalTransient = new ThermalTransient( value.ThermalTransient );
                this.Elements.Add( this.ThermalTransient );
                this._Outcome = value.Outcome;
            }
        }

        /// <summary> Clones the part. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A copy of this object. </returns>
        public object Clone()
        {
            return new DeviceUnderTest( this );
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static DeviceUnderTest Create()
        {
            DeviceUnderTest device;
            try
            {
                device = new DeviceUnderTest();
            }
            catch
            {
                throw;
            }

            return device;
        }

        /// <summary> Copies the device under test information except the serial number. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The other device under test. </param>
        public virtual void CopyInfo( DeviceUnderTest value )
        {
            if ( value is object )
            {
                this.OperatorId = value.OperatorId;
                this.LotId = value.LotId;
                this.PartNumber = value.PartNumber;
            }
        }

        /// <summary> Copies the configuration described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The other device under test. </param>
        public virtual void CopyConfiguration( DeviceUnderTest value )
        {
            if ( value is object )
            {
                this._ContactCheckEnabled = value.ContactCheckEnabled;
                this._ContactCheckThreshold = value.ContactCheckThreshold;
                this.InitialResistance.CopyConfiguration( value.InitialResistance );
                this.FinalResistance.CopyConfiguration( value.FinalResistance );
                this.ThermalTransient.CopyConfiguration( value.FinalResistance );
            }
        }

        /// <summary> Copies the measurement described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The other device under test. </param>
        public virtual void CopyMeasurement( DeviceUnderTest value )
        {
            if ( value is object )
            {
                this.InitialResistance.CopyMeasurement( value.InitialResistance );
                this.FinalResistance.CopyMeasurement( value.FinalResistance );
                this.ThermalTransient.CopyMeasurement( value.FinalResistance );
            }
        }

        #endregion

        #region " I PRESETABLE "

        /// <summary> Initializes known state. </summary>
        /// <remarks> This erases the last reading. </remarks>
        public void ClearMeasurements()
        {
            this.Elements.DefineClearExecutionState();
            this.Outcome = MeasurementOutcomes.None;
        }

        /// <summary> Initializes known state. </summary>
        /// <remarks> This erases the last reading. </remarks>
        public void ClearPartInfo()
        {
            this.LotId = string.Empty;
            this.OperatorId = string.Empty;
            this.SampleNumber = 0;
            this.SerialNumber = 0;
            this.PartNumber = string.Empty;
        }

        /// <summary> Sets values to their known clear execution state. </summary>
        /// <remarks> This erases the last reading. </remarks>
        public virtual void DefineClearExecutionState()
        {
            this.ClearPartInfo();
            this.ClearMeasurements();
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public virtual void InitKnownState()
        {
        }

        /// <summary> Sets the known preset state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void PresetKnownState()
        {
        }

        /// <summary> Sets the known reset (default) state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void ResetKnownState()
        {
            this.ContactCheckEnabled = My.MySettings.Default.ContactCheckEnabledDefault;
            this.ContactCheckThreshold = ( int ) Math.Round( My.MySettings.Default.ContactCheckThresholdDefault );
            this.Elements.ResetKnownState();
        }

        #endregion

        #region " COLLECTIBLE "

        /// <summary> Returns a unique key based on the part number and sample number. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="partNumber">   The part number. </param>
        /// <param name="sampleNumber"> The sample number. </param>
        /// <returns> A String. </returns>
        public static string BuildUniqueKey( string partNumber, int sampleNumber )
        {
            return string.Format( System.Globalization.CultureInfo.CurrentCulture, "{0}.{1}", partNumber, sampleNumber );
        }

        /// <summary> The sample number. </summary>
        private int _SampleNumber;

        /// <summary> Gets or sets the sample number. </summary>
        /// <value> The sample number. </value>
        public int SampleNumber
        {
            get => this._SampleNumber;

            set {
                if ( !value.Equals( this.SampleNumber ) )
                {
                    this._SampleNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The unique key. </summary>
        private string _UniqueKey;

        /// <summary> Gets or sets (protected) the unique key. </summary>
        /// <value> The unique key. </value>
        public string UniqueKey
        {
            get {
                string value = BuildUniqueKey( this.PartNumber, this.SampleNumber );
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this._UniqueKey, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._UniqueKey = value;
                    this.NotifyPropertyChanged( nameof( this.UniqueKey ) );
                }

                return this._UniqueKey;
            }

            protected set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.UniqueKey, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._UniqueKey = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " RECORD "

        /// <summary> Gets the data header. </summary>
        /// <value> The data header. </value>
        public static string DataHeader => "\"Sample Number\",\"Time Stamp\",\"Serial Number\",\"Initial Resistance\",\"Final Resistance\",\"Thermal Transient Voltage\"";

        /// <summary> The data record format. </summary>
        private string _DataRecordFormat;

        /// <summary> Gets the data record to save to a comma separated file. </summary>
        /// <value> The data record. </value>
        public string DataRecord
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._DataRecordFormat ) )
                {
                    this._DataRecordFormat = "{0},#{1}#,{2},{3:G5},{4:G5},{5:G5}";
                }

                string timestamp = this.ThermalTransient.Timestamp.ToString( "G" );
                return string.Format( System.Globalization.CultureInfo.CurrentCulture, this._DataRecordFormat, this.SampleNumber, timestamp, this.SerialNumber, this.InitialResistance.Resistance, this.FinalResistance.Resistance, this.ThermalTransient.Voltage );
            }
        }

        #endregion

        #region " DEVICE UNDER TEST INFO "

        /// <summary> Information equals. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The other. </param>
        /// <returns> <c>True</c> if basic info is equal excluding serial number. </returns>
        public bool InfoEquals( DeviceUnderTest other )
        {
            return other is object && string.Equals( this.PartNumber, other.PartNumber ) && string.Equals( this.LotId, other.LotId ) && string.Equals( this.OperatorId, other.OperatorId ) && this.ContactCheckEnabled.Equals( other.ContactCheckEnabled ) && this.ContactCheckThreshold.Equals( other.ContactCheckThreshold ) && true;
        }

        /// <summary> The part number. </summary>
        private string _PartNumber;

        /// <summary> Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public string PartNumber
        {
            get => this._PartNumber;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.PartNumber, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._PartNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Identifier for the lot. </summary>
        private string _LotId;

        /// <summary> Gets or sets or Sets the Lot ID. </summary>
        /// <value> The identifier of the lot. </value>
        public string LotId
        {
            get => this._LotId;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.LotId, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._LotId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Identifier for the operator. </summary>
        private string _OperatorId;

        /// <summary> Gets or sets or Sets the Operator ID. </summary>
        /// <value> The identifier of the operator. </value>
        public string OperatorId
        {
            get => this._OperatorId;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.OperatorId, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._OperatorId = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The serial number. </summary>
        private int _SerialNumber;

        /// <summary> Gets or sets the part serial number. </summary>
        /// <value> The serial number. </value>
        public int SerialNumber
        {
            get => this._SerialNumber;

            set {
                if ( !value.Equals( this.SerialNumber ) )
                {
                    this._SerialNumber = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " CONTACT CHECK INFO "

        /// <summary> True to enable, false to disable the contact check. </summary>
        private bool _ContactCheckEnabled;

        /// <summary> Gets or sets the contact check enabled. </summary>
        /// <value> The contact check enabled. </value>
        public bool ContactCheckEnabled
        {
            get => this._ContactCheckEnabled;

            set {
                if ( !value.Equals( this.ContactCheckEnabled ) )
                {
                    this._ContactCheckEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The contact check threshold. </summary>
        private int _ContactCheckThreshold;

        /// <summary> Gets or sets the contact check threshold. </summary>
        /// <value> The serial number. </value>
        public int ContactCheckThreshold
        {
            get => this._ContactCheckThreshold;

            set {
                if ( !value.Equals( this.ContactCheckThreshold ) )
                {
                    this._ContactCheckThreshold = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " MEASUREMENT ELEMENTS "

        /// <summary> Gets or sets the elements. </summary>
        /// <value> The elements. </value>
        private ResistanceMeasureCollection Elements { get; set; }

        /// <summary> Handles the resistance measure property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void OnPropertyChanged( ResistanceMeasureBase sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ResistanceMeasureBase.MeasurementAvailable ):
                    {
                        if ( sender.MeasurementAvailable )
                        {
                            this.MergeOutcomes();
                        }

                        break;
                    }
            }
        }

        /// <summary> Event handler. Called by _InitialResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitialResistance_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            try
            {
                this.OnPropertyChanged( sender as ResistanceMeasureBase, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
            finally
            {
                this.NotifyPropertyChanged( nameof( this.InitialResistance ) );
            }
        }

        private ColdResistance _InitialResistanceInternal;

        private ColdResistance InitialResistanceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._InitialResistanceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._InitialResistanceInternal != null )
                {
                    this._InitialResistanceInternal.PropertyChanged -= this.InitialResistance_PropertyChanged;
                }

                this._InitialResistanceInternal = value;
                if ( this._InitialResistanceInternal != null )
                {
                    this._InitialResistanceInternal.PropertyChanged += this.InitialResistance_PropertyChanged;
                }
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="ColdResistance">initial cold resistance</see>
        /// </summary>
        /// <value> The initial resistance. </value>
        public ColdResistance InitialResistance
        {
            get => this.InitialResistanceInternal;

            set => this.InitialResistanceInternal = value;
        }

        /// <summary> Event handler. Called by _FinalResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FinalResistance_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            try
            {
                this.OnPropertyChanged( sender as ResistanceMeasureBase, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
            finally
            {
                this.NotifyPropertyChanged( nameof( this.FinalResistance ) );
            }
        }

        private ColdResistance _FinalResistanceInternal;

        private ColdResistance FinalResistanceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._FinalResistanceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._FinalResistanceInternal != null )
                {
                    this._FinalResistanceInternal.PropertyChanged -= this.FinalResistance_PropertyChanged;
                }

                this._FinalResistanceInternal = value;
                if ( this._FinalResistanceInternal != null )
                {
                    this._FinalResistanceInternal.PropertyChanged += this.FinalResistance_PropertyChanged;
                }
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="ColdResistance">Final cold resistance</see>
        /// </summary>
        /// <value> The Final resistance. </value>
        public ColdResistance FinalResistance
        {
            get => this.FinalResistanceInternal;

            set => this.FinalResistanceInternal = value;
        }

        /// <summary> Event handler. Called by _ShuntResistance for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        private void ShuntResistance_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            this.NotifyPropertyChanged( nameof( this.ShuntResistance ) );
        }

        private ShuntResistance _ShuntResistanceInternal;

        private ShuntResistance ShuntResistanceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ShuntResistanceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ShuntResistanceInternal != null )
                {
                    this._ShuntResistanceInternal.PropertyChanged -= this.ShuntResistance_PropertyChanged;
                }

                this._ShuntResistanceInternal = value;
                if ( this._ShuntResistanceInternal != null )
                {
                    this._ShuntResistanceInternal.PropertyChanged += this.ShuntResistance_PropertyChanged;
                }
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="ShuntResistance">Shunt resistance</see>
        /// </summary>
        /// <value> The shunt resistance. </value>
        public ShuntResistance ShuntResistance
        {
            get => this.ShuntResistanceInternal;

            set => this.ShuntResistanceInternal = value;
        }

        /// <summary> Event handler. Called by _ThermalTransient for property changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalTransient_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            try
            {
                this.OnPropertyChanged( sender as ResistanceMeasureBase, e?.PropertyName );
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString() );
            }
            finally
            {
                this.NotifyPropertyChanged( nameof( this.ThermalTransient ) );
            }
        }

        private ThermalTransient _ThermalTransientInternal;

        private ThermalTransient ThermalTransientInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ThermalTransientInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ThermalTransientInternal != null )
                {
                    this._ThermalTransientInternal.PropertyChanged -= this.ThermalTransient_PropertyChanged;
                }

                this._ThermalTransientInternal = value;
                if ( this._ThermalTransientInternal != null )
                {
                    this._ThermalTransientInternal.PropertyChanged += this.ThermalTransient_PropertyChanged;
                }
            }
        }

        /// <summary>
        /// Gets or sets reference to the <see cref="ThermalTransient">thermal transient</see>
        /// </summary>
        /// <value> The thermal transient. </value>
        public ThermalTransient ThermalTransient
        {
            get => this.ThermalTransientInternal;

            set => this.ThermalTransientInternal = value;
        }

        /// <summary> Validates the transient voltage limit described by details. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="details"> [in,out] The details. </param>
        /// <returns>
        /// <c>True</c> if the voltage limit is in rage; <c>False</c> is the limit is too low.
        /// </returns>
        public bool ValidateTransientVoltageLimit( ref string details )
        {
            if ( this.InitialResistance is null )
            {
                throw new InvalidOperationException( "Initial Resistance object is null." );
            }

            if ( this.ThermalTransient is null )
            {
                throw new InvalidOperationException( "Thermal Transient object is null." );
            }

            double value = this.InitialResistance.HighLimit * this.ThermalTransient.CurrentLevel + this.ThermalTransient.AllowedVoltageChange;
            if ( value > this.ThermalTransientInternal.VoltageLimit )
            {
                details = string.Format( System.Globalization.CultureInfo.CurrentCulture, "A Thermal transient voltage limit of {0} volts is too low;. It must be at least {1} volts, which is determined by the cold resistance high limit and thermal transient current level.", this.ThermalTransient.VoltageLimit, value );
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Checks if the configuration is equal to the other <see cref="DeviceUnderTest">DUT</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="other"> The other. </param>
        /// <returns> <c>True</c> if configurations are the same as the other device. </returns>
        public bool ConfigurationEquals( DeviceUnderTest other )
        {
            return other is null
                ? throw new ArgumentNullException( nameof( other ) )
                : other is object && this.ContactCheckEnabled.Equals( other.ContactCheckEnabled ) && this.ContactCheckThreshold.Equals( other.ContactCheckThreshold ) && this.InitialResistance.ConfigurationEquals( other.InitialResistance ) && this.FinalResistance.ConfigurationEquals( other.FinalResistance ) && this.ThermalTransient.ConfigurationEquals( other.ThermalTransient );
        }

        /// <summary>
        /// Checks if the thermal transient configuration is equal to the other
        /// <see cref="DeviceUnderTest">DUT</see> changed.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="other"> The other. </param>
        /// <returns> <c>True</c> if configurations are the same as the other device. </returns>
        public bool ThermalTransientConfigurationEquals( DeviceUnderTest other )
        {
            return other is null
                ? throw new ArgumentNullException( nameof( other ) )
                : other is object && this.InitialResistance.ConfigurationEquals( other.InitialResistance ) && this.FinalResistance.ConfigurationEquals( other.FinalResistance ) && this.ThermalTransient.ConfigurationEquals( other.ThermalTransient );
        }

        /// <summary>
        /// Checks if DUT Information and configuration are equal between the two devices.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="other"> The other. </param>
        /// <returns> <c>True</c> if info and configurations are the same as the other device. </returns>
        public bool InfoConfigurationEquals( DeviceUnderTest other )
        {
            return other is null
                ? throw new ArgumentNullException( nameof( other ) )
                : this.InfoEquals( other ) && this.ConfigurationEquals( other );
        }

        /// <summary>
        /// Checks if the configuration is equal to the other <see cref="DeviceUnderTest">DUT</see>
        /// changed.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
        ///                                              null. </exception>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="other"> The other. </param>
        /// <returns> <c>True</c> if configurations are the same as the other device. </returns>
        public bool SourceMeasureUnitEquals( DeviceUnderTest other )
        {
            if ( other is null )
            {
                throw new ArgumentNullException( nameof( other ) );
            }
            else if ( other.InitialResistance is null || other.FinalResistance is null || other.ThermalTransient is null )
            {
                throw new InvalidOperationException( "DUT elements not instantiated." );
            }
            else if ( other.InitialResistance.SourceMeasureUnit is null || other.FinalResistance.SourceMeasureUnit is null || other.ThermalTransient.SourceMeasureUnit is null )
            {
                throw new InvalidOperationException( "DUT Source Measure Elements are null." );
            }
            else if ( string.IsNullOrWhiteSpace( other.InitialResistance.SourceMeasureUnit ) || string.IsNullOrWhiteSpace( other.FinalResistance.SourceMeasureUnit ) || string.IsNullOrWhiteSpace( other.ThermalTransient.SourceMeasureUnit ) )
            {
                throw new InvalidOperationException( string.Format( "A DUT Source Measure Element is empty: Initial='{0}'; Final='{1}'; Transient='{2}'.", other.InitialResistance.SourceMeasureUnit, other.FinalResistance.SourceMeasureUnit, other.ThermalTransient.SourceMeasureUnit ) );
            }

            return other is object && string.Equals( this.InitialResistance.SourceMeasureUnit, other.InitialResistance.SourceMeasureUnit ) && string.Equals( this.FinalResistance.SourceMeasureUnit, other.FinalResistance.SourceMeasureUnit ) && string.Equals( this.ThermalTransient.SourceMeasureUnit, other.ThermalTransient.SourceMeasureUnit );
        }

        /// <summary> Gets the timestamp. </summary>
        /// <value> The timestamp. </value>
        public DateTimeOffset Timestamp => this.ThermalTransient.Timestamp;

        /// <summary> Gets the initial resistance caption. </summary>
        /// <value> The initial resistance caption. </value>
        public string InitialResistanceCaption => this.InitialResistanceInternal.ResistanceCaption;

        /// <summary> Gets the final resistance caption. </summary>
        /// <value> The final resistance caption. </value>
        public string FinalResistanceCaption => this.FinalResistanceInternal.ResistanceCaption;

        /// <summary> Gets the thermal transient voltage caption. </summary>
        /// <value> The thermal transient voltage caption. </value>
        public string ThermalTransientVoltageCaption => this.ThermalTransientInternal.VoltageCaption;

        #endregion

        #region " OUTCOME "

        /// <summary>
        /// Checks if any part measurement is in. This is determined by examining all outcomes as not
        /// equal to
        /// <see cref="MeasurementOutcomes.None">None</see>
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if one or more measurement outcomes were set. </returns>
        public bool AnyMeasurementMade()
        {
            return this.InitialResistance.Outcome != MeasurementOutcomes.None || this.ThermalTransient.Outcome != MeasurementOutcomes.None || this.FinalResistance.Outcome != MeasurementOutcomes.None;
        }

        /// <summary> True if any measurement available. </summary>
        private bool _AnyMeasurementAvailable;

        /// <summary> Gets or sets the sentinel indicating if Any measurements is available. </summary>
        /// <value> The measurement available. </value>
        public bool AnyMeasurementsAvailable
        {
            get => this._AnyMeasurementAvailable;

            set {
                this._AnyMeasurementAvailable = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary>
        /// Checks if all part measurements are in. This is determined by examining all outcomes as not
        /// equal to
        /// <see cref="MeasurementOutcomes.None">None</see>
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if all measurement outcomes were set. </returns>
        public bool AllMeasurementsMade()
        {
            return this.InitialResistance.Outcome != MeasurementOutcomes.None && this.ThermalTransient.Outcome != MeasurementOutcomes.None && this.FinalResistance.Outcome != MeasurementOutcomes.None;
        }

        /// <summary> True if all measurement available. </summary>
        private bool _AllMeasurementAvailable;

        /// <summary> Gets or sets the sentinel indicating if all measurements are available. </summary>
        /// <value> The measurement available. </value>
        public bool AllMeasurementsAvailable
        {
            get => this._AllMeasurementAvailable;

            set {
                this._AllMeasurementAvailable = value;
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Merges the measurements outcomes to a Double outcome. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void MergeOutcomes()
        {
            var outcome = MeasurementOutcomes.None;
            outcome = MergeOutcomes( outcome, this.InitialResistance.Outcome );
            outcome = MergeOutcomes( outcome, this.FinalResistance.Outcome );
            outcome = MergeOutcomes( outcome, this.ThermalTransient.Outcome );
            this.Outcome = outcome;
        }

        /// <summary>
        /// Combine the existing outcome with new outcome. If failed, leave failed. If hit compliance set
        /// to hit compliance.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="existingOutcome"> The existing outcome. </param>
        /// <param name="newOutcome">      The new outcome. </param>
        /// <returns> The MeasurementOutcomes. </returns>
        private static MeasurementOutcomes MergeOutcomes( MeasurementOutcomes existingOutcome, MeasurementOutcomes newOutcome )
        {
            if ( (newOutcome & MeasurementOutcomes.PartPassed) != 0 )
            {
                return existingOutcome == MeasurementOutcomes.None ? MeasurementOutcomes.PartPassed : existingOutcome;
            }
            else
            {
                existingOutcome &= ~MeasurementOutcomes.PartPassed;
                return existingOutcome | newOutcome;
            }
        }

        /// <summary> The outcome. </summary>
        private MeasurementOutcomes _Outcome;

        /// <summary> Gets or sets the measurement outcome for the part. </summary>
        /// <value> The outcome. </value>
        public MeasurementOutcomes Outcome
        {
            get => this._Outcome;

            protected set {
                // None is used to flag the measurements were cleared.
                if ( MeasurementOutcomes.None == value || !value.Equals( this.Outcome ) )
                {
                    this._Outcome = value;
                    this.NotifyPropertyChanged();
                }

                this.AllMeasurementsAvailable = this.AllMeasurementsMade();
                this.AnyMeasurementsAvailable = this.AnyMeasurementMade();
            }
        }

        #endregion

    }

    /// <summary> Collection of device under tests. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-06 </para>
    /// </remarks>
    public class DeviceUnderTestCollection : System.Collections.ObjectModel.ObservableCollection<DeviceUnderTest>
    {

        /// <summary>
        /// Initializes a new instance of the
        /// <see cref="T:System.Collections.ObjectModel.ObservableCollection`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public DeviceUnderTestCollection() : base()
        {
            this.Devices = new DeviceCollection();
        }

        /// <summary> Collection of devices. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private class DeviceCollection : System.Collections.ObjectModel.KeyedCollection<string, DeviceUnderTest>
        {

            /// <summary>
            /// When implemented in a derived class, extracts the key from the specified element.
            /// </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="item"> The element from which to extract the key. </param>
            /// <returns> The key for the specified element. </returns>
            protected override string GetKeyForItem( DeviceUnderTest item )
            {
                return item?.UniqueKey;
            }
        }

        /// <summary> Gets or sets the devices. </summary>
        /// <value> The devices. </value>
        private DeviceCollection Devices { get; set; }

        /// <summary>
        /// Raises the
        /// <see cref="E:System.Collections.ObjectModel.ObservableCollection`1.CollectionChanged" />
        /// event with the provided arguments.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Arguments of the event being raised. </param>
        protected override void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            this.Devices.Clear();
            foreach ( DeviceUnderTest d in this )
                this.Devices.Add( d );
            base.OnCollectionChanged( e );
        }
    }
}
