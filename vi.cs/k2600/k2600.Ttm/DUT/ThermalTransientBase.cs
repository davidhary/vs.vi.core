﻿using System;
using System.Diagnostics;

using isr.Core.NumericExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Defines a measured thermal transient resistance and voltage. </summary>
    /// <remarks>
    /// David, 2013-08-05             <para>
    /// David, 2009-02-02, 2.1.3320 </para><para>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public abstract class ThermalTransientBase : ResistanceMeasureBase, IEquatable<ThermalTransientBase>
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected ThermalTransientBase() : base()
        {
        }

        /// <summary> Clones an existing measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        protected ThermalTransientBase( ThermalTransientBase value ) : base( value )
        {
            if ( value is object )
            {
                // Configuration
                this._AllowedVoltageChange = value.AllowedVoltageChange;
                this._MedianFilterSize = value.MedianFilterSize;
                this._PostTransientDelay = value.PostTransientDelay;
                this._SamplingInterval = value.SamplingInterval;
                this._TracePoints = value.TracePoints;
            }
        }

        /// <summary> Copies the configuration described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void CopyConfiguration( ThermalTransientBase value )
        {
            base.CopyConfiguration( value );
            if ( value is object )
            {
                this._AllowedVoltageChange = value.AllowedVoltageChange;
                this._MedianFilterSize = value.MedianFilterSize;
                this._PostTransientDelay = value.PostTransientDelay;
                this._SamplingInterval = value.SamplingInterval;
                this._TracePoints = value.TracePoints;
            }
        }

        #endregion

        #region " PRESET "

        /// <summary> Sets the known reset (default) state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void ResetKnownState()
        {
            base.ResetKnownState();
            this.Aperture = ( double ) My.MySettings.Default.ThermalTransientApertureDefault;
            this.CurrentLevel = ( double ) My.MySettings.Default.ThermalTransientCurrentLevelDefault;
            this.LowLimit = ( double ) My.MySettings.Default.ThermalTransientLowLimitDefault;
            this.HighLimit = ( double ) My.MySettings.Default.ThermalTransientHighLimitDefault;
            this.VoltageLimit = ( double ) My.MySettings.Default.ThermalTransientVoltageLimitDefault;
            this.AllowedVoltageChange = ( double ) My.MySettings.Default.ThermalTransientVoltageChangeDefault;
            this.MedianFilterSize = My.MySettings.Default.ThermalTransientMedianFilterLengthDefault;
            this.PostTransientDelay = ( double ) My.MySettings.Default.PostTransientDelayDefault;
            this.SamplingInterval = ( double ) My.MySettings.Default.ThermalTransientSamplingIntervalDefault;
            this.TracePoints = My.MySettings.Default.ThermalTransientTracePointsDefault;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Indicates whether the current <see cref="T:ThermalTransientBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="obj"> An object. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as ThermalTransientBase );
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ThermalTransientBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The cold resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the <paramref name="other" /> parameter and this instance are the same type
        /// and represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( ThermalTransientBase other )
        {
            return other is object && this.Reading.Equals( other.Reading ) && this.ConfigurationEquals( other ) && true;
        }

        /// <summary> Check throw unequal configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="other"> The thermal transient configuration to compare to this object. </param>
        public void CheckThrowUnequalConfiguration( ThermalTransientBase other )
        {
            if ( other is object )
            {
                base.CheckThrowUnequalConfiguration( other );
                if ( !this.ConfigurationEquals( other ) )
                {
                    string format = "Unequal configuring--instrument {0}={1}.NE.{2}";
                    if ( !this.AllowedVoltageChange.Approximates( other.AllowedVoltageChange, 0.0001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Allowed Voltage Change", this.AllowedVoltageChange, other.AllowedVoltageChange ) );
                    }
                    else if ( !this.MedianFilterSize.Equals( other.MedianFilterSize ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Median Filter Size", this.MedianFilterSize, other.MedianFilterSize ) );
                    }
                    else if ( !this.PostTransientDelay.Approximates( other.PostTransientDelay, 0.001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Post Transient Delay", this.PostTransientDelay, other.PostTransientDelay ) );
                    }
                    else if ( !this.SamplingInterval.Approximates( other.SamplingInterval, 0.000001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Sampling Interval", this.SamplingInterval, other.SamplingInterval ) );
                    }
                    else if ( !this.TracePoints.Equals( other.TracePoints ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Trace Points", this.TracePoints, other.TracePoints ) );
                    }
                    else
                    {
                        Debug.Assert( !Debugger.IsAttached, "Failed logic" );
                    }
                }
            }
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ThermalTransientBase"></see> configuration values
        /// are equal to a specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The cold resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:ThermalTransientBase"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool ConfigurationEquals( ThermalTransientBase other )
        {
            return other is object && base.ConfigurationEquals( other ) && this.AllowedVoltageChange.Approximates( other.AllowedVoltageChange, 0.001d ) && this.MedianFilterSize.Equals( other.MedianFilterSize ) && this.PostTransientDelay.Approximates( other.PostTransientDelay, 0.001d ) && this.SamplingInterval.Approximates( other.SamplingInterval, 0.000001d ) && this.TracePoints.Equals( other.TracePoints ) && true;
        }

        /// <summary> Returns a hash code for this instance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A hash code for this object. </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ThermalTransientBase left, ThermalTransientBase right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ThermalTransientBase left, ThermalTransientBase right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        #endregion

        #region " CONFIGURATION PROPERTIES "

        /// <summary> The allowed voltage change. </summary>
        private double _AllowedVoltageChange;

        /// <summary> Gets or sets the maximum expected transient voltage. </summary>
        /// <value> The allowed voltage change. </value>
        public double AllowedVoltageChange
        {
            get => this._AllowedVoltageChange;

            set {
                if ( !value.Equals( this.AllowedVoltageChange ) )
                {
                    this._AllowedVoltageChange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The Median Filter Size. </summary>
        private int _MedianFilterSize;

        /// <summary> Gets or sets the cached Median Filter Size. </summary>
        /// <value> The Median Filter Size. </value>
        public int MedianFilterSize
        {
            get => this._MedianFilterSize;

            set {
                if ( !value.Equals( this.MedianFilterSize ) )
                {
                    this._MedianFilterSize = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The post transient delay. </summary>
        private double _PostTransientDelay;

        /// <summary>
        /// Gets or sets the delay time in seconds between the end of the thermal transient and the start
        /// of the final cold resistance measurement.
        /// </summary>
        /// <value> The post transient delay. </value>
        public double PostTransientDelay
        {
            get => this._PostTransientDelay;

            set {
                if ( !value.Equals( this.PostTransientDelay ) )
                {
                    this._PostTransientDelay = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The sampling interval. </summary>
        private double _SamplingInterval;

        /// <summary> Gets or sets the sampling interval. </summary>
        /// <value> The sampling interval. </value>
        public double SamplingInterval
        {
            get => this._SamplingInterval;

            set {
                if ( !value.Equals( this.SamplingInterval ) )
                {
                    this._SamplingInterval = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The trace points. </summary>
        private int _TracePoints;

        /// <summary> Gets or sets the number of trace points to measure. </summary>
        /// <value> The trace points. </value>
        public int TracePoints
        {
            get => this._TracePoints;

            set {
                if ( !value.Equals( this.TracePoints ) )
                {
                    this._TracePoints = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}