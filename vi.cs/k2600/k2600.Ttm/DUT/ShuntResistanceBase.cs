﻿using System;
using System.Diagnostics;

using isr.Core.NumericExtensions;

namespace isr.VI.Tsp.K2600.Ttm
{

    /// <summary> Defines a measured shunt resistance element. </summary>
    /// <remarks>
    /// David, 2012-11-10, 3.1.4697 <para>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public abstract class ShuntResistanceBase : ResistanceMeasureBase, IEquatable<ShuntResistanceBase>
    {

        #region " CONSTRUCTION and CLONING "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected ShuntResistanceBase() : base()
        {
        }

        /// <summary> Clones an existing measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        protected ShuntResistanceBase( ShuntResistanceBase value ) : base( value )
        {
            if ( value is object )
            {
                this._CurrentRange = value.CurrentRange;
            }
        }

        #endregion

        #region " PRESET "

        /// <summary> Restores defaults. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void ResetKnownState()
        {
            base.ResetKnownState();
            this.Aperture = ( double ) My.MySettings.Default.ShuntResistanceApertureDefault;
            this.CurrentRange = ( double ) My.MySettings.Default.ShuntResistanceCurrentRangeDefault;
            this.CurrentLevel = ( double ) My.MySettings.Default.ShuntResistanceCurrentLevelDefault;
            this.LowLimit = ( double ) My.MySettings.Default.ShuntResistanceLowLimitDefault;
            this.HighLimit = ( double ) My.MySettings.Default.ShuntResistanceHighLimitDefault;
            this.VoltageLimit = ( double ) My.MySettings.Default.ShuntResistanceVoltageLimitDefault;
        }

        #endregion

        #region " EQUALS "

        /// <summary> Check throw if unequal configuration. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="other"> The resistance to compare to this object. </param>
        public void CheckThrowUnequalConfiguration( ShuntResistanceBase other )
        {
            if ( other is object )
            {
                base.CheckThrowUnequalConfiguration( other );
                if ( !this.ConfigurationEquals( other ) )
                {
                    string format = "Unequal configuring--instrument {0}={1}.NE.{2}";
                    if ( !this.CurrentRange.Approximates( other.CurrentRange, 0.000001d ) )
                    {
                        throw new Core.OperationFailedException( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, "Current Range", this.CurrentRange, other.CurrentRange ) );
                    }
                    else
                    {
                        Debug.Assert( !Debugger.IsAttached, "Failed logic" );
                    }
                }
            }
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ShuntResistanceBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="obj"> An object. </param>
        /// <returns>
        /// <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
        /// same value; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as ShuntResistanceBase );
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ShuntResistanceBase"></see> value is equal to a
        /// specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The cold resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:ShuntResistanceBase"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool Equals( ShuntResistanceBase other )
        {
            return other is object && base.Equals( other ) && this.CurrentRange.Approximates( other.CurrentRange, 0.000001d ) && true;
        }

        /// <summary>
        /// Indicates whether the current <see cref="T:ShuntResistanceBase"></see> configuration values
        /// are equal to a specified object.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="other"> The cold resistance to compare to this object. </param>
        /// <returns>
        /// <c>True</c> if the other parameter is equal to the current
        /// <see cref="T:ShuntResistanceBase"></see> value;
        /// otherwise, <c>False</c>.
        /// </returns>
        public bool ConfigurationEquals( ShuntResistanceBase other )
        {
            return other is object && base.ConfigurationEquals( other ) && this.CurrentRange.Approximates( other.CurrentRange, 0.000001d ) && true;
        }

        /// <summary> Returns a hash code for this instance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A hash code for this object. </returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( ShuntResistanceBase left, ShuntResistanceBase right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="left">  The left. </param>
        /// <param name="right"> The right. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( ShuntResistanceBase left, ShuntResistanceBase right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        #endregion

        #region " CONFIGURATION PROPERTIES "

        /// <summary> The current range. </summary>
        private double _CurrentRange;

        /// <summary> Gets or sets the current Range. </summary>
        /// <value> The current Range. </value>
        public double CurrentRange
        {
            get => this._CurrentRange;

            set {
                if ( !value.Equals( this.CurrentRange ) )
                {
                    this._CurrentRange = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

    }
}