using System;
using System.Diagnostics;

using isr.Core;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K2600.Ttm.MSTest
{

    /// <summary>
    /// This is a test class for Measure and is intended to contain all Measure Unit Tests.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [TestClass()]
    [TestCategory( "k2600" )]
    public class TtmCommandsTests
    {

        /// <summary>
        /// Gets or sets the test context which provides information about and functionality for the
        /// current test run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        #region "Additional test attributes"

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
            OnInitialize();
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void MyClassCleanup()
        {
            Logger = null;
        }
        #endregion

        /// <summary> Gets or sets the logger. </summary>
        /// <value> The logger. </value>
        public static Logger Logger { get; private set; } = Logger.NewInstance( My.MyProject.Application.Info.ProductName );

        /// <summary> Executes the 'initialize' action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected static void OnInitialize()
        {
            Logger = Logger.NewInstance( My.MyProject.Application.Info.ProductName );
            Microsoft.VisualBasic.Logging.FileLogTraceListener listener;
            listener = Logger.ReplaceDefaultTraceListener( true );

            // set the log for the application
            My.MyProject.Application.Log.TraceSource.Listeners.Remove( CustomFileLogTraceListener.DefaultFileLogWriterName );
            _ = My.MyProject.Application.Log.TraceSource.Listeners.Add( listener );
            My.MyProject.Application.Log.TraceSource.Switch.Level = SourceLevels.Verbose;
            Logger.ApplyTraceLevel( TraceEventType.Verbose );
        }

        /// <summary> A test for Open session. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        [TestMethod()]
        public void OpenSessionTest()
        {
            string resourceName = "TCPIP0::192.168.1.134::inst0::INSTR";
            var target = SessionFactory.Get.Factory.Session();
            target.OpenSession( resourceName );
            Assert.AreEqual( resourceName, target.OpenResourceName );
            Assert.AreEqual( true, target.IsDeviceOpen );
        }

        /// <summary> Opens a session. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="resourceName"> Name of the resource. </param>
        /// <returns> A VI.Pith.SessionBase. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public Pith.SessionBase OpenSession( string resourceName )
        {
            var target = SessionFactory.Get.Factory.Session();
            target.OpenSession( resourceName );
            Assert.AreEqual( resourceName, target.OpenResourceName );
            Assert.AreEqual( true, target.IsDeviceOpen );
            return target;
        }

        /// <summary> A test for measure current. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="session"> The session. </param>
        /// <returns> A String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public string MeasureCurrent( Pith.SessionBase session )
        {
            if ( session is null )
                throw new ArgumentNullException( nameof( session ) );
            _ = session.WriteLine( "_G.status.reset()" );
            _ = session.ReadStatusByte();
            _ = session.WriteLine( "print(smua.measure.i())" );
            string value = session.ReadLineTrimEnd();
            return value;
        }

        /// <summary> A test for measure current. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MeasureCurrentTest()
        {
            string resourceName = "TCPIP0::192.168.1.134::inst0::INSTR";
            using var target = this.OpenSession( resourceName );
            string value = this.MeasureCurrent( target );
            Assert.AreEqual( false, string.IsNullOrWhiteSpace( value ) );
            My.MyProject.Application.Log.WriteEntry( value );
        }

        /// <summary> (Unit Test Method) tests measure current many. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MeasureCurrentManyTest()
        {
            string resourceName = "TCPIP0::192.168.1.134::inst0::INSTR";
            using var target = this.OpenSession( resourceName );
            for ( int i = 1; i <= 100; i++ )
            {
                string value = this.MeasureCurrent( target );
                My.MyProject.Application.Log.WriteEntry( $"{i} {value}" );
            }
        }

        /// <summary> Logs an iterator. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Performance", "CA1822:Mark members as static", Justification = "<Pending>" )]
        public void LogIT( string value )
        {
            My.MyProject.Application.Log.WriteEntry( value );
        }

        /// <summary> (Unit Test Method) tests start commands. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void StartCommandsTest()
        {
            string resourceName = "TCPIP0::192.168.1.134::inst0::INSTR";
            using var session = this.OpenSession( resourceName );
            string command = string.Empty;
            string value = string.Empty;
            int i = 0;
            command = "_G.status.request_enable=0";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.reset()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(localnode.linefreq)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.coldResistance.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.coldResistance.Defaults.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.level))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minCurrent))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxCurrent))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.limit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minVoltage))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxVoltage))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.lowLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.highLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minResistance))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxResistance))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.ir:currentSourceChannelSetter(_G.ttm.coldResistance.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.ir:apertureSetter(_G.ttm.coldResistance.Defaults.aperture)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.ir:levelSetter(_G.ttm.coldResistance.Defaults.level)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.ir:limitSetter(_G.ttm.coldResistance.Defaults.limit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.ir:lowLimitSetter(_G.ttm.coldResistance.Defaults.lowLimit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.ir:highLimitSetter(_G.ttm.coldResistance.Defaults.highLimit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.ir.smuI==_G.smua)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.ir.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.level))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.highLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.lowLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.ir.limit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.coldResistance.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.coldResistance.Defaults.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.level))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minCurrent))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxCurrent))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.limit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minVoltage))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxVoltage))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.lowLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.highLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.minResistance))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.coldResistance.Defaults.maxResistance))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.fr:currentSourceChannelSetter(_G.ttm.coldResistance.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.fr:apertureSetter(_G.ttm.coldResistance.Defaults.aperture)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.fr:levelSetter(_G.ttm.coldResistance.Defaults.level)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.fr:limitSetter(_G.ttm.coldResistance.Defaults.limit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.fr:lowLimitSetter(_G.ttm.coldResistance.Defaults.lowLimit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.fr:highLimitSetter(_G.ttm.coldResistance.Defaults.highLimit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.fr.smuI==_G.smua)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.fr.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.level))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.highLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.lowLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.fr.limit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.estimator.Defaults.thermalCoefficient))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.est:thermalCoefficientSetter(_G.ttm.estimator.Defaults.thermalCoefficient)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.estimator.Defaults.thermalCoefficient))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.est:thermalCoefficientSetter(_G.ttm.estimator.Defaults.thermalCoefficient)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.est.thermalCoefficient))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.est.thermalCoefficient))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.trace.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.trace.Defaults.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minAperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxAperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.level))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minCurrent))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxCurrent))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.limit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minVoltage))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxVoltage))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.highLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.lowLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxVoltageChange))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%d',_G.ttm.trace.Defaults.medianFilterSize))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.period))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.minPeriod))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.trace.Defaults.maxPeriod))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%d',_G.ttm.trace.Defaults.points))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%d',_G.ttm.trace.Defaults.minPoints))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%1f',_G.ttm.trace.Defaults.maxPoints))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:currentSourceChannelSetter(_G.ttm.trace.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:apertureSetter(_G.ttm.trace.Defaults.aperture)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:levelSetter(_G.ttm.trace.Defaults.level)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:limitSetter(_G.ttm.trace.Defaults.limit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:lowLimitSetter(_G.ttm.trace.Defaults.lowLimit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:highLimitSetter(_G.ttm.trace.Defaults.highLimit)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:maxRateSetter()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:maxVoltageChangeSetter(_G.ttm.trace.Defaults.level)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:latencySetter(_G.ttm.trace.Defaults.latency)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:medianFilterSizeSetter(_G.ttm.trace.Defaults.medianFilterSize)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.ttm.tr:pointsSetter(_G.ttm.trace.Defaults.points)";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.tr.smuI==_G.smua)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.tr.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.level))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.highLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.lowLimit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.limit))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%d',_G.ttm.tr.medianFilterSize))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.3f',_G.ttm.postTransientDelayGetter()))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.period))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%d',_G.ttm.tr.points))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%9.6f',_G.ttm.tr.maxVoltageChange))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.status.reset()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.errorqueue.clear() waitcomplete()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "_G.status.reset() _G.status.standard.enable=253 _G.status.request_enable=32 _G.opc()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*IDN? *WAI";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.errorqueue.clear() waitcomplete()";
            i = ( int ) session.ReadStatusByte();
            this.LogIT( $"STB: {i}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "*IDN? *WAI";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.reset()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(_G.ttm.coldResistance.Defaults.smuI)";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.print(string.format('%7.4f',_G.ttm.coldResistance.Defaults.aperture))";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            i = ( int ) session.ReadStatusByte();
            this.LogIT( $"STB: {i}" );
            command = "_G.errorqueue.clear() waitcomplete()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            i = ( int ) session.ReadStatusByte();
            this.LogIT( $"STB: {i}" );
            command = "_G.status.reset()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*IDN? *WAI";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            session.CommunicationTimeout = TimeSpan.FromMilliseconds( 30000d );
            command = "_G.errorqueue.clear() waitcomplete()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            session.CommunicationTimeout = TimeSpan.FromMilliseconds( 2000d );
            i = ( int ) session.ReadStatusByte();
            this.LogIT( $"STB: {i}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            command = "_G.status.reset()";
            _ = session.WriteLine( command );
            value = string.Empty;
            this.LogIT( $"{command} {value}" );
            command = "*OPC?";
            _ = session.WriteLine( command );
            value = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
            i = ( int ) session.ReadStatusByte();
            this.LogIT( $"STB: {i}" );
            command = "_G.print(display)";
            _ = session.WriteLine( command );
            _ = session.ReadLineTrimEnd();
            this.LogIT( $"{command} {value}" );
        }
    }
}
