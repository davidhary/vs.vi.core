﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( "TTM  VI Tests" )]
[assembly: AssemblyDescription( "Thermal Transient Meter Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Tsp.K2600.Ttm.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
