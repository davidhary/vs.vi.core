﻿using isr.VI.Tsp.Script;

namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary> Script Manager. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class ScriptManager : ScriptManagerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ScriptManager" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public ScriptManager( TspDevice device ) : base( device.StatusSubsystem )
        {
            this.DisplaySubsystem = device.DisplaySubsystem;
            this.LinkSubsystem = device.LinkSubsystem;
            this.InteractiveSubsystem = device.InteractiveSubsystem;
        }

        #endregion


    }
}