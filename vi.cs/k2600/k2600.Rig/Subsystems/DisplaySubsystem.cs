namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary> Display subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class DisplaySubsystem : DisplaySubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="DisplaySubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.Tsp.StatusSubsystemBase">status
        ///                                Subsystem</see>. </param>
        public DisplaySubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " ENABLED "

        /// <summary> Gets the display enable command format. </summary>
        /// <value> The display enable command format. </value>
        protected override string DisplayEnableCommandFormat
        {
            // ":DISP:ENAB {0:'ON';'ON';'OFF'}"
            get => string.Empty;

            set {
            }
        }

        /// <summary> Gets the display enabled query command. </summary>
        /// <value> The display enabled query command. </value>
        protected override string DisplayEnabledQueryCommand
        {
            get =>
                // ":DISP:ENAB?"
                string.Empty;

            set {
            }
        }

        #endregion

        #endregion


    }
}
