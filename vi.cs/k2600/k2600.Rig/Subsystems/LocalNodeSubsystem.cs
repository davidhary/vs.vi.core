namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary> Local Node subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-04 </para>
    /// </remarks>
    public class LocalNodeSubsystem : LocalNodeSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="LocalNodeSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="P:isr.VI.SubsystemPlusStatusBase.StatusSubsystem">TSP
        ///                                status Subsystem</see>. </param>
        public LocalNodeSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region "  "
        #endregion

        #endregion


    }
}
