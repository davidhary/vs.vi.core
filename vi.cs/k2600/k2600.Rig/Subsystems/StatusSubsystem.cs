namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary> Status subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> The session. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( session )
        {
            this.VersionInfoBase = new VersionInfo();
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }


        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            // establish the current node as the controller node. 
            // moved to tsp subsystem base with setting the conditional to true when creating the subsystem: 
            // Me.StatusSubsystem.InitiateControllerNode()
        }

        #endregion

        #region " TSP ACCESS  "

        /// <summary> The new program required. </summary>
        private string _NewProgramRequired;

        /// <summary>
        /// Gets the message indicating that a new program is required for the instrument because this
        /// instrument is not included in the instrument list.
        /// </summary>
        /// <value> The new program required. </value>
        public string NewProgramRequired
        {
            get {
                if ( string.IsNullOrWhiteSpace( this._NewProgramRequired ) )
                {
                    this._NewProgramRequired = $"A new version of the program is required;. for instrument {this.Identity}";
                }

                return this._NewProgramRequired;
            }
        }

        #endregion

    }
}
