using System;

namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary> Defines a System Subsystem for a TSP System. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-12-12, 3.0.5093. </para>
    /// </remarks>
    public class AccessSubsystem : AccessSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="AccessSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="VI.Tsp.StatusSubsystemBase">TSP
        ///                                status Subsystem</see>. </param>
        public AccessSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            string filename = System.IO.Path.Combine( this.ScriptsFolderName, this.ReleasedInstrumentsFileName );
            if ( My.MyProject.Computer.FileSystem.FileExists( filename ) )
            {
                this.ReleasedInstruments = My.MyProject.Computer.FileSystem.ReadAllText( filename );
            }

            filename = System.IO.Path.Combine( this.ScriptsFolderName, this.ReleasedInstrumentsFileName );
            if ( My.MyProject.Computer.FileSystem.FileExists( filename ) )
            {
                this.ReleasedInstruments = My.MyProject.Computer.FileSystem.ReadAllText( filename );
            }

            filename = System.IO.Path.Combine( this.ScriptsFolderName, this.SaltFileName );
            if ( My.MyProject.Computer.FileSystem.FileExists( filename ) )
            {
                this.Salt = My.MyProject.Computer.FileSystem.ReadAllText( filename ).Trim();
            }
        }

        #endregion

        #region " SESSION / STATUS SUBSYSTEM"

        /// <summary> Gets the session. </summary>
        /// <value> The tsp session. </value>
        private Pith.SessionBase TspSession => this.StatusSubsystem.Session;

        #endregion

        #region " CERTIFY "

        /// <summary> Certifies. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns>
        /// <c>null</c> if not known; <c>True</c> if certified; otherwise, <c>False</c>.
        /// </returns>
        public override bool? Certify( string value )
        {
            if ( this.StatusSubsystem?.IsDeviceOpen == true )
            {
                this.Session.StoreCommunicationTimeout( this.CertifyTimeout );
                try
                {
                    this.Certified = string.IsNullOrWhiteSpace( value ) ? new bool?() : this.TspSession.IsStatementTrue( "_G.isr.access.allow('{0}')", value.Trim() );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    this.Session.RestoreCommunicationTimeout();
                }
            }

            return this.Certified;
        }

        #endregion

        #region " ACCESS MANAGEMENT "

        /// <summary> Pathname of the scripts folder. </summary>
        private string _ScriptsFolderName;

        /// <summary> Gets or sets the pathname of the scripts folder. </summary>
        /// <value> The pathname of the scripts folder. </value>
        public string ScriptsFolderName
        {
            get => this._ScriptsFolderName;

            set {
                if ( !string.Equals( value, this.ScriptsFolderName ) )
                {
                    this._ScriptsFolderName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Filename of the certified instruments file. </summary>
        private string _CertifiedInstrumentsFileName;

        /// <summary> Gets or sets the filename of the certified instruments file. </summary>
        /// <value> The filename of the certified instruments file. </value>
        public string CertifiedInstrumentsFileName
        {
            get => this._CertifiedInstrumentsFileName;

            set {
                if ( !string.Equals( value, this.CertifiedInstrumentsFileName ) )
                {
                    this._CertifiedInstrumentsFileName = value;
                    string filename = System.IO.Path.Combine( this.ScriptsFolderName, this.CertifiedInstrumentsFileName );
                    if ( My.MyProject.Computer.FileSystem.FileExists( filename ) )
                    {
                        this.CertifiedInstruments = My.MyProject.Computer.FileSystem.ReadAllText( filename );
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Filename of the salt file. </summary>
        private string _SaltFileName;

        /// <summary> Gets or sets the filename of the released instruments file. </summary>
        /// <value> The filename of the released instruments file. </value>
        public string SaltFileName
        {
            get => this._SaltFileName;

            set {
                if ( !string.Equals( value, this.SaltFileName ) )
                {
                    this._SaltFileName = value;
                    string filename = System.IO.Path.Combine( this.ScriptsFolderName, this.SaltFileName );
                    if ( My.MyProject.Computer.FileSystem.FileExists( filename ) )
                    {
                        this.Salt = My.MyProject.Computer.FileSystem.ReadAllText( filename ).Trim();
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Filename of the released instruments file. </summary>
        private string _ReleasedInstrumentsFileName;

        /// <summary> Gets or sets the filename of the released instruments file. </summary>
        /// <value> The filename of the released instruments file. </value>
        public string ReleasedInstrumentsFileName
        {
            get => this._ReleasedInstrumentsFileName;

            set {
                if ( !string.Equals( value, this.ReleasedInstrumentsFileName ) )
                {
                    this._ReleasedInstrumentsFileName = value;
                    string filename = System.IO.Path.Combine( this.ScriptsFolderName, this.ReleasedInstrumentsFileName );
                    if ( My.MyProject.Computer.FileSystem.FileExists( filename ) )
                    {
                        this.ReleasedInstruments = My.MyProject.Computer.FileSystem.ReadAllText( filename );
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Checks if the custom scripts loaded successfully. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>True</c> if loaded; otherwise, <c>False</c>. </returns>
        public override bool Loaded()
        {
            return this.TspSession.IsStatementTrue( "_G.isr.access.loaded()" );
        }

        /// <summary> Gets the release code for the controller instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="serialNumber"> The serial number. </param>
        /// <param name="salt">         The released instruments. </param>
        /// <returns> The release value of the controller instrument. </returns>
        public override string ReleaseValue( string serialNumber, string salt )
        {
            string result = string.Empty;
            var certifiedInstruments = this.CertifiedInstruments.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            var releasedInstruments = this.ReleasedInstruments.Split( Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries );
            for ( int i = 0, loopTo = certifiedInstruments.Length - 1; i <= loopTo; i++ )
            {
                if ( string.Equals( certifiedInstruments[i].Trim(), Core.HashExtensions.HashExtensionMethods.ToBase64Hash( $"{serialNumber}{salt}" ) ) )
                {
                    if ( i < releasedInstruments.Length )
                    {
                        result = releasedInstruments[i];
                    }

                    break;
                }
            }

            return result;
        }

        #endregion

    }
}
