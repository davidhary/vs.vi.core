using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary>
    /// Implements a TSP based device. Defines the I/O driver for accessing the master node of a TSP
    /// Linked system.
    /// </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-11-06. Based on legacy TSP library.  </para><para>
    /// David, 2009-02-21, 3.0.3339 </para>
    /// </remarks>
    public class TspDevice : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TspDevice" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public TspDevice() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        public TspDevice( StatusSubsystem statusSubsystem ) : base( VI.StatusSubsystemBase.Validated( statusSubsystem ) )
        {
            My.MySettings.Default.PropertyChanged += this.MySettings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static TspDevice Create()
        {
            TspDevice device = null;
            try
            {
                device = new TspDevice();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static TspDevice Validated( TspDevice device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( TspDevice )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            string activity = string.Empty;
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                if ( this.InteractiveSubsystem is object )
                {
                    try
                    {
                        activity = "turning off prompts";
                        // turn off prompts
                        _ = this.InteractiveSubsystem.WriteShowPrompts( false );
                        _ = this.InteractiveSubsystem.WriteShowErrors( false );
                    }
                    catch ( Exception ex )
                    {
                        _ = this.PublishException( activity, ex );
                    }

                    try
                    {
                        // set the state to closed
                        this.InteractiveSubsystem.ExecutionState = TspExecutionState.Closed;
                    }
                    catch ( Exception ex )
                    {
                        Debug.Assert( !Debugger.IsAttached, "Exception disposing", "{0}", ex.ToFullBlownString() );
                    }
                }

                this.DisplaySubsystem?.RestoreDisplay( this.StatusSubsystem.InitializeTimeout );
                this.BindSystemSubsystem( null );
                this.BindDisplaySubsystem( null );
                this.BindLinkSubsystem( null );
                this.BindInteractiveSubsystem( null );
                this.BindAccessSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                // allow connection time to materialize
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 100d ) );
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.BindLinkSubsystem( new LinkSubsystem( LinkStatusSubsystem.Create( this.StatusSubsystem.Session ) ) );
                this.BindInteractiveSubsystem( new LocalNodeSubsystem( this.StatusSubsystem ) );
                this.BindDisplaySubsystem( new DisplaySubsystem( this.StatusSubsystem ) );
                this.BindAccessSubsystem( new AccessSubsystem( this.StatusSubsystem ) );
                this.AccessSubsystem.ScriptsFolderName = My.MySettings.Default.ScriptFilesFolderName;
                this.AccessSubsystem.CertifiedInstrumentsFileName = My.MySettings.Default.CertifiedInstrumentsFileName;
                this.AccessSubsystem.ReleasedInstrumentsFileName = My.MySettings.Default.ReleasedInstrumentsFileName;
                this.AccessSubsystem.SaltFileName = My.MySettings.Default.SaltFileName;
            }
        }

        /// <summary>
        /// Allows the derived device to take actions after opening. Adds subsystems and event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnOpened( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = "initiating controller node";
                // instantiate the Link Subsystem
                this.LinkSubsystem.IsControllerNode = true;

                // instantiate the interactive subsystem
                this.InteractiveSubsystem.ProcessExecutionStateEnabled = true;
                base.OnOpened( e );
                activity = $"setting service request enable register to {Pith.ServiceRequests.None}";
                this.Session.ApplyServiceRequestEnableBitmask( Pith.ServiceRequests.None );
            }
            catch ( Exception ex )
            {
                activity = $"{activity}----closing this session";
                _ = this.PublishException( activity, ex );
                this.CloseSession();
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Gets or sets the Access Subsystem. </summary>
        /// <value> The Access Subsystem. </value>
        public AccessSubsystem AccessSubsystem { get; private set; }

        /// <summary> Binds the Access subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindAccessSubsystem( AccessSubsystem subsystem )
        {
            if ( this.AccessSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.AccessSubsystem );
                this.AccessSubsystem = null;
            }

            this.AccessSubsystem = subsystem;
            if ( this.AccessSubsystem is object )
            {
                this.Subsystems.Add( this.AccessSubsystem );
            }
        }

        /// <summary> Gets or sets the Display Subsystem. </summary>
        /// <value> The Display Subsystem. </value>
        public DisplaySubsystem DisplaySubsystem { get; private set; }

        /// <summary> Binds the Display subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindDisplaySubsystem( DisplaySubsystem subsystem )
        {
            if ( this.DisplaySubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DisplaySubsystem );
                this.DisplaySubsystem = null;
            }

            this.DisplaySubsystem = subsystem;
            if ( this.DisplaySubsystem is object )
            {
                this.Subsystems.Add( this.DisplaySubsystem );
            }
        }

        /// <summary> Gets or sets the Link Subsystem. </summary>
        /// <value> The Link Subsystem. </value>
        public LinkSubsystemBase LinkSubsystem { get; private set; }

        /// <summary> Binds the Link subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindLinkSubsystem( LinkSubsystemBase subsystem )
        {
            if ( this.LinkSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.LinkSubsystem );
                this.LinkSubsystem = null;
            }

            this.LinkSubsystem = subsystem;
            if ( this.LinkSubsystem is object )
            {
                this.Subsystems.Add( this.LinkSubsystem );
            }
        }

        /// <summary> Gets or sets the interactive (Local Node) subsystem. </summary>
        /// <value> The interactive (Local Node) subsystem. </value>
        public LocalNodeSubsystemBase InteractiveSubsystem { get; private set; }

        /// <summary> Binds the interactive (Local Node) subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindInteractiveSubsystem( LocalNodeSubsystemBase subsystem )
        {
            if ( this.InteractiveSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.InteractiveSubsystem );
                this.InteractiveSubsystem = null;
            }

            this.InteractiveSubsystem = subsystem;
            if ( this.InteractiveSubsystem is object )
            {
                this.Subsystems.Add( this.InteractiveSubsystem );
            }
        }

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "TSP Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.ScriptFilesFolderName ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.CertifiedInstrumentsFileName ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.ReleasedInstrumentsFileName ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.Default.SaltFileName ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"Trace log level changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"Trace show level changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.ReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.StatusReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime;
                        break;
                    }

                case nameof( My.MySettings.ScriptFilesFolderName ):
                    {
                        if ( this.IsDeviceOpen )
                        {
                            this.AccessSubsystem.ScriptsFolderName = sender.ScriptFilesFolderName;
                            _ = this.PublishInfo( $"{propertyName} changed to {sender.ScriptFilesFolderName}" );
                        }

                        break;
                    }

                case nameof( My.MySettings.CertifiedInstrumentsFileName ):
                    {
                        if ( this.IsDeviceOpen )
                        {
                            this.AccessSubsystem.CertifiedInstrumentsFileName = sender.CertifiedInstrumentsFileName;
                            _ = this.PublishInfo( $"{propertyName} changed to {sender.CertifiedInstrumentsFileName}" );
                        }

                        break;
                    }

                case nameof( My.MySettings.ReleasedInstrumentsFileName ):
                    {
                        if ( this.IsDeviceOpen )
                        {
                            this.AccessSubsystem.ReleasedInstrumentsFileName = sender.ReleasedInstrumentsFileName;
                            _ = this.PublishInfo( $"{propertyName} changed to {sender.ReleasedInstrumentsFileName}" );
                        }

                        break;
                    }

                case nameof( My.MySettings.SaltFileName ):
                    {
                        if ( this.IsDeviceOpen )
                        {
                            this.AccessSubsystem.SaltFileName = sender.SaltFileName;
                            _ = this.PublishInfo( $"{propertyName} changed to {sender.SaltFileName}" );
                        }

                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
