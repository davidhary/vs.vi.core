using System;
using System.Diagnostics;

using isr.VI.Tsp.Script;

namespace isr.VI.Tsp.K2600.Rig
{

    /// <summary> TTM Script Manager. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class TtmScriptManager : ScriptManagerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="ScriptManager" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public TtmScriptManager( TspDevice device ) : base( device.StatusSubsystem )
        {
            this.DisplaySubsystem = device.DisplaySubsystem;
            this.LinkSubsystem = device.LinkSubsystem;
            this.InteractiveSubsystem = device.InteractiveSubsystem;
        }

        #endregion

        #region " RESOURCES "

        /// <summary> The FTP address. </summary>
        private string _FtpAddress;

        /// <summary> Gets or sets the Author Title. </summary>
        /// <remarks> Get from My.Settings.FtpAddress. </remarks>
        /// <value> The Author Title. </value>
        public string FtpAddress
        {
            get => this._FtpAddress;

            set {
                if ( !string.Equals( value, this.FtpAddress ) )
                {
                    this._FtpAddress = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The author title. </summary>
        private string _AuthorTitle;

        /// <summary> Gets or sets the Author Title. </summary>
        /// <remarks> Get from My.Resources.AuthorTitle. </remarks>
        /// <value> The Author Title. </value>
        public string AuthorTitle
        {
            get => this._AuthorTitle;

            set {
                if ( !string.Equals( value, this.AuthorTitle ) )
                {
                    this._AuthorTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The firmware released version. </summary>
        private string _FirmwareReleasedVersion;

        /// <summary> Gets or sets the firmware released version. </summary>
        /// <remarks> Get from My.Resources.MeterFirmwareVersion. </remarks>
        /// <value> The firmware released version. </value>
        public string FirmwareReleasedVersion
        {
            get => this._FirmwareReleasedVersion;

            set {
                if ( !string.Equals( value, this.FirmwareReleasedVersion ) )
                {
                    this._FirmwareReleasedVersion = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The framework namespace. </summary>
        private string _FrameworkNamespace;

        /// <summary> Gets or sets the framework namespace. </summary>
        /// <remarks> Get from My.Resources.FrameworkNamespace. </remarks>
        /// <value> The framework namespace. </value>
        public string FrameworkNamespace
        {
            get => this._FrameworkNamespace;

            set {
                if ( !string.Equals( value, this.FrameworkNamespace ) )
                {
                    this._FrameworkNamespace = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Name of the framework. </summary>
        private string _FrameworkName;

        /// <summary> Gets or sets the framework Name. </summary>
        /// <remarks> Get from My.Resources.FrameworkName. </remarks>
        /// <value> The framework Name. </value>
        public string FrameworkName
        {
            get => this._FrameworkName;

            set {
                if ( !string.Equals( value, this.FrameworkName ) )
                {
                    this._FrameworkName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The framework title. </summary>
        private string _FrameworkTitle;

        /// <summary> Gets or sets the framework Title. </summary>
        /// <remarks> Get from My.Resources.FrameworkTitle. </remarks>
        /// <value> The framework Title. </value>
        public string FrameworkTitle
        {
            get => this._FrameworkTitle;

            set {
                if ( !string.Equals( value, this.FrameworkTitle ) )
                {
                    this._FrameworkTitle = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The boot firmware version. </summary>
        private string _BootFirmwareVersion;

        /// <summary> Gets or sets the Boot firmware version. </summary>
        /// <remarks> Get from My.Resources.BootFirmwareVersion. </remarks>
        /// <value> The Boot firmware version. </value>
        public string BootFirmwareVersion
        {
            get => this._BootFirmwareVersion;

            set {
                if ( !string.Equals( value, this.BootFirmwareVersion ) )
                {
                    this._BootFirmwareVersion = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The meter firmware version. </summary>
        private string _MeterFirmwareVersion;

        /// <summary> Gets or sets the meter firmware version. </summary>
        /// <remarks> Get from My.Resources.MeterFirmwareVersion. </remarks>
        /// <value> The meter firmware version. </value>
        public string MeterFirmwareVersion
        {
            get => this._MeterFirmwareVersion;

            set {
                if ( !string.Equals( value, this.MeterFirmwareVersion ) )
                {
                    this._MeterFirmwareVersion = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The support firmware version. </summary>
        private string _SupportFirmwareVersion;

        /// <summary> Gets or sets the Support firmware version. </summary>
        /// <remarks> Get from My.Resources.SupportFirmwareVersion. </remarks>
        /// <value> The Support firmware version. </value>
        public string SupportFirmwareVersion
        {
            get => this._SupportFirmwareVersion;

            set {
                if ( !string.Equals( value, this.SupportFirmwareVersion ) )
                {
                    this._SupportFirmwareVersion = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> List of names of the legacy scripts. </summary>
        private string _LegacyScriptNames;

        /// <summary> Gets or sets a list of names of the legacy scripts. </summary>
        /// <remarks> Get from My.Resources.LegacyScripts. </remarks>
        /// <value> A list of names of the legacy scripts. </value>
        public string LegacyScriptNames
        {
            get => this._LegacyScriptNames;

            set {
                if ( !string.Equals( value, this.LegacyScriptNames ) )
                {
                    this._LegacyScriptNames = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Pathname of the script files folder. </summary>
        private string _ScriptFilesFolderName = @"C:\My\Private\ttm";

        /// <summary> Gets or sets the pathname of the script files folder. </summary>
        /// <remarks> C:\My\Private\ttm. </remarks>
        /// <value> The pathname of the script files folder. </value>
        public string ScriptFilesFolderName
        {
            get => this._ScriptFilesFolderName;

            set {
                if ( !string.Equals( value, this.ScriptFilesFolderName ) )
                {
                    this._ScriptFilesFolderName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " FRAMEWORK SCRIPTS "

        /// <summary> Define scripts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineScripts()
        {
            ScriptEntityBase script;
            this.NewLegacyScripts();
            if ( Debugger.IsAttached )
            {
                // enumerate the legacy scripts.
                if ( !string.IsNullOrWhiteSpace( this.LegacyScriptNames ) )
                {
                    var lagacyScrpitNames = this.LegacyScriptNames.Split( ',' );
                    foreach ( string scriptName in lagacyScrpitNames )
                    {
                        script = this.AddLegacyScript( scriptName );
                        script.RequiresDeletion = true;
                    }
                }
            }

            this.NewScripts();
            this.AddSupportScript( "isr_support", InstrumentModelFamily.K2600 );
            this.AddSupportScript( "isr_support", InstrumentModelFamily.K2600A );
            this.AddMeterScript( "isr_ttm", InstrumentModelFamily.K2600 );
            this.AddMeterScript( "isr_ttm", InstrumentModelFamily.K2600A );

            // the boot script must be last.
            this.AddBootScript();
        }

        /// <summary> Adds a meter script to 'instrumentModelFamily'. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="scriptName">            Name of the script. </param>
        /// <param name="instrumentModelFamily"> The instrument model family. </param>
        private void AddMeterScript( string scriptName, InstrumentModelFamily instrumentModelFamily )
        {
            ScriptEntityBase script;
            script = this.AddScript( scriptName, NodeEntityBase.ModelFamilyMask( instrumentModelFamily ) );
            script.FileName = "isr_ttm.tsp";
            script.ResourceFileName = $"{script.FileName}{NodeEntityBase.ModelFamilyResourceFileSuffix( instrumentModelFamily )}";
            script.NamespaceList = "isr,isr.meters,isr.meters.thermalTransient";
            script.FirmwareVersionGetter = "_G.isr.meters.thermalTransient.version()";
            script.ReleasedFirmwareVersion = this.MeterFirmwareVersion;
            script.IsPrimaryScript = true;
            script.FileFormat = ScriptFileFormats.Binary | ScriptFileFormats.Compressed;
            // script.Source = EmbeddedResourceManager.ReadEmbeddedTextResource(script.ResourceFileName)
            script.Source = My.MyProject.Computer.FileSystem.ReadAllText( System.IO.Path.Combine( this.ScriptFilesFolderName, script.ResourceFileName ) );
        }

        /// <summary> Adds a support script to 'instrumentModelFamily'. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="scriptName">            Name of the script. </param>
        /// <param name="instrumentModelFamily"> The instrument model family. </param>
        private void AddSupportScript( string scriptName, InstrumentModelFamily instrumentModelFamily )
        {
            ScriptEntityBase script;
            script = this.AddScript( scriptName, NodeEntityBase.ModelFamilyMask( instrumentModelFamily ) );
            script.FileName = "isr_support.tsp";
            script.ResourceFileName = $"{script.FileName}{NodeEntityBase.ModelFamilyResourceFileSuffix( instrumentModelFamily )}";
            script.NamespaceList = "isr";
            script.FirmwareVersionGetter = "_G.isr.version()";
            script.ReleasedFirmwareVersion = this.SupportFirmwareVersion;
            script.IsSupportScript = true;
            script.FileFormat = ScriptFileFormats.Binary | ScriptFileFormats.Compressed;
            // script.Source = EmbeddedResourceManager.ReadEmbeddedTextResource(script.ResourceFileName)
            script.Source = My.MyProject.Computer.FileSystem.ReadAllText( System.IO.Path.Combine( this.ScriptFilesFolderName, script.ResourceFileName ) );
        }

        /// <summary> Adds boot script. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AddBootScript()
        {
            ScriptEntityBase script;
            script = this.AddScript( "isr_ttm_boot", "" );
            script.FileName = string.Empty; // not available from file
            script.NamespaceList = "isr,isr.ttm";
            script.FirmwareVersionGetter = "_G.isr.ttm.version()";
            script.ReleasedFirmwareVersion = this.BootFirmwareVersion;
            script.IsBootScript = true;

            // create auto script custom commands.
            var customCommands = new System.Text.StringBuilder( 2048 );
            string bootNamespace = string.Empty;
            foreach ( var currentBootNamespace in script.Namespaces() )
            {
                bootNamespace = currentBootNamespace;
                _ = customCommands.AppendFormat( "{0} = {0} or {{}}", bootNamespace );
                _ = customCommands.AppendLine();
            }

            if ( string.IsNullOrWhiteSpace( bootNamespace ) )
            {
                bootNamespace = this.FrameworkNamespace;
            }

            _ = customCommands.AppendFormat( "function {0} return '{1}' end", script.FirmwareVersionGetter, script.ReleasedFirmwareVersion );
            _ = customCommands.AppendLine();
            _ = customCommands.AppendFormat( "{0}.displayTitle = function()", bootNamespace );
            _ = customCommands.AppendLine();
            _ = customCommands.AppendLine( "if _G.display ~= nil then" );
            _ = customCommands.AppendLine( "_G.display.clear()" );
            _ = customCommands.AppendLine( "_G.display.setcursor(1, 1)" );
            _ = customCommands.AppendFormat( "_G.display.settext('{0}')", this.FrameworkTitle );
            _ = customCommands.AppendLine();
            _ = customCommands.AppendLine( "_G.display.setcursor(2, 1)" );
            _ = customCommands.AppendFormat( "_G.display.settext('{0}')", this.AuthorTitle );
            _ = customCommands.AppendLine();
            _ = customCommands.AppendLine( "end" );
            _ = customCommands.AppendLine( "end" );
            _ = customCommands.AppendFormat( "{0}.displayTitle()", bootNamespace );
            _ = customCommands.AppendLine();
            script.Source = BuildAutoRunScript( customCommands, this.Scripts() );
        }

        /// <summary> Deletes the user scripts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="statusSubsystem">  A reference to a
        ///                                 <see cref="VI.Tsp.StatusSubsystemBase">status
        ///                                 subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        ///                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
        ///                                 subsystem</see>. </param>
        public override void DeleteUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem )
        {
            if ( statusSubsystem is null )
                throw new ArgumentNullException( nameof( statusSubsystem ) );
            if ( displaySubsystem is null )
                throw new ArgumentNullException( nameof( displaySubsystem ) );
            bool hasLegacyScripts = this.LegacyScripts().FindAnyScript( this.LinkSubsystem.ControllerNode, this.Session );
            bool hasScripts = this.Scripts().FindAnyScript( this.LinkSubsystem.ControllerNode, this.Session );
            if ( hasLegacyScripts || hasScripts )
            {

                // reset all status values so they are forced to be read.
                this.DefineKnownResetState();
                if ( hasLegacyScripts )
                {
                    displaySubsystem.DisplayLine( 1, "Deleting {0}", this.FrameworkName );
                    displaySubsystem.DisplayLine( 2, "Deleting legacy scripts" );
                    _ = this.DeleteUserScripts( this.LegacyScripts(), this.LinkSubsystem.NodeEntities(), true, true );
                }

                if ( hasScripts )
                {
                    if ( !this.IsDeleteUserScriptRequired( this.Scripts(), this.LinkSubsystem.NodeEntities(), true ) )
                    {
                        if ( Core.MyMessageBox.ShowDialogCancelOkay( "Scripts are up to date -- deletion is not required. Select OK to proceed and remove the scripts.", "Scripts up to date, Are you sure?" ) == Core.MyDialogResult.Ok )
                        {
                            displaySubsystem.DisplayLine( 1, "Deleting {0}", this.FrameworkName );
                            displaySubsystem.DisplayLine( 2, "Deleting scripts" );
                            _ = this.DeleteUserScripts( this.Scripts(), this.LinkSubsystem.NodeEntities(), true, false );
                        }
                    }
                }

                // clear state
                this.DefineKnownResetState();
            }
        }

        /// <summary> Saves the user scripts applying the release value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="timeout">          The timeout. </param>
        /// <param name="statusSubsystem">  A reference to a
        ///                                 <see cref="VI.Tsp.StatusSubsystemBase">status
        ///                                 subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        ///                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
        ///                                 subsystem</see>. </param>
        /// <param name="accessSubsystem">  The access subsystem. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        public override bool SaveUserScripts( TimeSpan timeout, StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem, AccessSubsystemBase accessSubsystem )
        {
            if ( statusSubsystem is null )
            {
                throw new ArgumentNullException( nameof( statusSubsystem ) );
            }

            if ( displaySubsystem is null )
            {
                throw new ArgumentNullException( nameof( displaySubsystem ) );
            }

            if ( accessSubsystem is null )
            {
                throw new ArgumentNullException( nameof( accessSubsystem ) );
            }

            string value = accessSubsystem.ReleaseValue( statusSubsystem.SerialNumberReading, accessSubsystem.Salt );

            // ensure we can finalize TTM.
            _ = this.PublishVerbose( $"'{this.Session.ResourceNameCaption}' confirming {this.FirmwareNameGetter()} loaded;. " );
            if ( !this.FindFirmware() )
            {
                _ = this.PublishWarning( $"'{this.Session.ResourceNameCaption}' failed saving custom firmware {this.FirmwareNameGetter()};. because the firmware was not loaded." );
                return false;
            }

            if ( accessSubsystem.CertifiedInstruments.IndexOf( Core.HashExtensions.HashExtensionMethods.ToBase64Hash( statusSubsystem.SerialNumberReading ), StringComparison.Ordinal ) < 0 )
            {
                _ = accessSubsystem.Loaded()
                    ? this.PublishWarning( this.NewProgramRequired )
                    : this.PublishWarning( $"Saving custom firmware failed;. Instrument '{statusSubsystem.Identity}' may require upgrading the custom scripts loaded by this program to the instrument or registration with this program vendor" );

                return false;
            }

            if ( !accessSubsystem.Certify( value ) == true )
            {
                _ = this.PublishWarning( $"Saving custom firmware failed;. Instrument '{statusSubsystem.Identity}' console program requires update of its custom scripts or registration with the program vendor" );
                return false;
            }

            displaySubsystem.DisplayLine( 1, $"Saving {this.FrameworkName}" );
            displaySubsystem.DisplayLine( 2, "Saving scripts" );
            if ( this.SaveUserScripts( this.Scripts(), this.LinkSubsystem.NodeEntities() ) )
            {

                // write script files of new scripts were saved.
                displaySubsystem.DisplayLine( 2, $"Current Version {this.FirmwareReleasedVersionGetter()}" );
                if ( Debugger.IsAttached )
                {
                    displaySubsystem.DisplayLine( 1, $"Writing {this.FrameworkName}" );
                    if ( !this.WriteScriptFiles( this.FilePath, this.Scripts(), this.LinkSubsystem.NodeEntities() ) )
                    {
                        _ = this.PublishInfo( $"Instrument '{this.Session.ResourceNameCaption}' failed writing one or more user scripts;. Error ignored" );
                    }
                }

                // do a garbage collection
                displaySubsystem.DisplayLine( 1, $"Cleaning up {this.FrameworkName}" );
                displaySubsystem.DisplayLine( 2, "Collecting garbage" );
                _ = this.PublishVerbose( $"'{this.Session.ResourceNameCaption}' collecting garbage" );
                if ( !statusSubsystem.CollectGarbageWaitComplete( timeout, "collecting garbage;. " ) )
                {
                    _ = this.PublishInfo( $"'{this.Session.ResourceNameCaption}' failed collecting garbage;. Problem ignored." );
                }

                displaySubsystem.DisplayLine( 1, $"Resetting {this.FrameworkName}" );
                displaySubsystem.DisplayLine( 2, "Resetting node" );
                _ = this.PublishVerbose( "Instrument '{0}' resetting node;. ", this.Session.ResourceNameCaption );
                _ = this.LinkSubsystem.ResetNode();
                displaySubsystem.DisplayTitle( this.FrameworkTitle, this.AuthorTitle );
            }
            else
            {
                _ = this.PublishInfo( $"'{this.Session.ResourceNameCaption}' failed saving one or more user scripts;. " );
                return false;
            }

            return true;
        }

        /// <summary> Reads and parses user scripts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="statusSubsystem">  A reference to a
        ///                                 <see cref="VI.Tsp.StatusSubsystemBase">status
        ///                                 subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        ///                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
        ///                                 subsystem</see>. </param>
        public void ReadParseUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem )
        {
            if ( statusSubsystem is null )
                throw new ArgumentNullException( nameof( statusSubsystem ) );
            if ( displaySubsystem is null )
                throw new ArgumentNullException( nameof( displaySubsystem ) );
            displaySubsystem.DisplayLine( 1, $"Parsing {this.FrameworkName}" );
            ReadParseWriteScripts( this.LinkSubsystem.ControllerNode.ModelNumber, this.FilePath, this.Scripts(), true );
            _ = this.PublishInfo( $"'{this.Session.ResourceNameCaption}' saved parsed scripts to disk;. Application cannot proceed until scripts are added to the program resource." );
        }

        /// <summary> Try read parse user scripts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem">  A reference to a
        ///                                 <see cref="VI.Tsp.StatusSubsystemBase">status
        ///                                 subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        ///                                 <see cref="VI.Tsp.DisplaySubsystemBase">display
        ///                                 subsystem</see>. </param>
        /// <returns> <c>True</c> if okay; otherwise, <c>False</c>. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override bool TryReadParseUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem )
        {
            if ( statusSubsystem is null )
                throw new ArgumentNullException( nameof( statusSubsystem ) );
            if ( displaySubsystem is null )
                throw new ArgumentNullException( nameof( displaySubsystem ) );
            string activity = string.Empty;
            bool result = false;
            try
            {
                activity = "reading and parsing user scripts";
                this.ReadParseUserScripts( statusSubsystem, displaySubsystem );
                result = true;
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }

            return result;
        }

        /// <summary> Uploads the user scripts to the controller instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem">  A reference to a
        ///                                 <see cref="T:isr.VI.Tsp.StatusSubsystemBase">status
        ///                                 subsystem</see>. </param>
        /// <param name="displaySubsystem"> A reference to a
        ///                                 <see cref="T:isr.VI.Tsp.DisplaySubsystemBase">display
        ///                                 subsystem</see>. </param>
        /// <param name="accessSubsystem">  The access subsystem. </param>
        /// <returns> <c>True</c> if uploaded; otherwise, <c>False</c>. </returns>
        public override bool UploadUserScripts( StatusSubsystemBase statusSubsystem, DisplaySubsystemBase displaySubsystem, AccessSubsystemBase accessSubsystem )
        {
            if ( statusSubsystem is null )
                throw new ArgumentNullException( nameof( statusSubsystem ) );
            if ( displaySubsystem is null )
                throw new ArgumentNullException( nameof( displaySubsystem ) );
            if ( accessSubsystem is null )
                throw new ArgumentNullException( nameof( accessSubsystem ) );

            // reset all status values so as to force a read.
            this.DefineKnownResetState();

            // read the serial number.
            _ = statusSubsystem.QuerySerialNumber();

            // read the instrument identity.
            _ = statusSubsystem.QueryIdentity();

            // check if new script source is available for storage. 
            if ( this.Scripts().RequiresReadParseWrite() )
            {
                this.ReadParseUserScripts( statusSubsystem, displaySubsystem );
                return false;
            }

            // Check if instrument serial number is listed in the instrument resource.
            if ( accessSubsystem.CertifiedInstruments.IndexOf( Core.HashExtensions.HashExtensionMethods.ToBase64Hash( statusSubsystem.SerialNumberReading ), StringComparison.Ordinal ) < 0 )
            {
                _ = this.PublishWarning( this.NewProgramRequired );
                return false;
            }

            // delete legacy scripts if any and ignore any errors that might have occurred.
            // do not ignore version on upload.
            this.DeleteUserScripts( statusSubsystem, displaySubsystem );
            displaySubsystem.DisplayLine( 1, $"Uploading {this.FrameworkName}" );
            displaySubsystem.DisplayLine( 2, "Selecting scripts for upload" );
            bool failed = !this.UpdateUserScripts( this.Scripts(), this.LinkSubsystem.NodeEntities() );

            // display the title if not failed.
            if ( !failed )
                displaySubsystem.DisplayTitle( this.FrameworkTitle, this._FrameworkName );
            return !failed;
        }

        #endregion

    }
}
