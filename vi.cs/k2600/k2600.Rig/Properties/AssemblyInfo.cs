﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.VI.Tsp.K2600.Rig.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.VI.Tsp.K2600.Rig.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.VI.Tsp.K2600.Rig.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
