using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K2600
{

    /// <summary> Implements a Keithley 2600 source meter. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-12 </para>
    /// </remarks>
    public class K2600Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="K2600Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K2600Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected K2600Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            My.MySettings.Default.PropertyChanged += this.Settings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static K2600Device Create()
        {
            K2600Device device = null;
            try
            {
                device = new K2600Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static K2600Device Validated( K2600Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( K2600Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindMeasureResistanceSubsystem( null );
                this.BindMeasureVoltageSubsystem( null );
                this.BindSourceSubsystem( null );
                this.BindSenseSubsystem( null );
                this.BindCurrentSourceSubsystem( null );
                this.BindDisplaySubsystem( null );
                this.BindContactSubsystem( null );
                this.BindLocalNodeSubsystem( null );
                this.BindSourceSubsystem( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.BindSourceMeasureUnit( new SourceMeasureUnit( this.StatusSubsystem ) );
                this.BindLocalNodeSubsystem( new LocalNodeSubsystem( this.StatusSubsystem ) );
                this.BindDisplaySubsystem( new DisplaySubsystem( this.StatusSubsystem ) );
                this.BindContactSubsystem( new ContactSubsystem( this.StatusSubsystem ) );
                this.BindCurrentSourceSubsystem( new CurrentSourceSubsystem( this.StatusSubsystem ) );
                this.BindSenseSubsystem( new SenseSubsystem( this.StatusSubsystem ) );
                this.BindSourceSubsystem( new SourceSubsystem( this.StatusSubsystem ) );
                this.BindMeasureVoltageSubsystem( new MeasureVoltageSubsystem( this.StatusSubsystem ) );
                this.BindMeasureResistanceSubsystem( new MeasureResistanceSubsystem( this.StatusSubsystem ) );
            }
        }

        /// <summary>
        /// Allows the derived device to take actions after opening. Adds subsystems and event handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnOpened( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceTitleCaption} handling on opened event";
                _ = this.PublishVerbose( $"{activity};. " );
                base.OnOpened( e );
                this.Session.ApplyServiceRequestEnableBitmask( Pith.ServiceRequests.None );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( $"{activity}; closing...", ex );
                this.CloseSession();
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        #region " COLLECTION "

        /// <summary> Adds a subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        public void AddSubsystem( SubsystemBase subsystem )
        {
            this.Subsystems.Add( subsystem );
        }

        /// <summary> Removes the subsystem described by subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        public void RemoveSubsystem( SubsystemBase subsystem )
        {
            _ = this.Subsystems.Remove( subsystem );
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #region " CURRENT SOURCE "

        /// <summary> Gets or sets the Current Source Subsystem. </summary>
        /// <value> The Current Source Subsystem. </value>
        public CurrentSourceSubsystem CurrentSourceSubsystem { get; private set; }

        /// <summary> Binds the Current Source subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindCurrentSourceSubsystem( CurrentSourceSubsystem subsystem )
        {
            if ( this.CurrentSourceSubsystem is object )
            {
                this.SourceMeasureUnit.Remove( this.CurrentSourceSubsystem );
                _ = this.Subsystems.Remove( this.CurrentSourceSubsystem );
                this.CurrentSourceSubsystem = null;
            }

            this.CurrentSourceSubsystem = subsystem;
            if ( this.CurrentSourceSubsystem is object )
            {
                this.Subsystems.Add( this.CurrentSourceSubsystem );
                this.SourceMeasureUnit.Add( this.CurrentSourceSubsystem );
            }
        }

        #endregion

        #region " CONTACT "

        /// <summary> Gets or sets the contact Subsystem. </summary>
        /// <value> The contact Subsystem. </value>
        public ContactSubsystem ContactSubsystem { get; private set; }

        /// <summary> Binds the contact subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindContactSubsystem( ContactSubsystem subsystem )
        {
            if ( this.ContactSubsystem is object )
            {
                this.SourceMeasureUnit.Remove( this.ContactSubsystem );
                _ = this.Subsystems.Remove( this.ContactSubsystem );
                this.ContactSubsystem = null;
            }

            this.ContactSubsystem = subsystem;
            if ( this.ContactSubsystem is object )
            {
                this.Subsystems.Add( this.ContactSubsystem );
                this.SourceMeasureUnit.Add( this.ContactSubsystem );
            }
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Gets or sets the Display Subsystem. </summary>
        /// <value> The Display Subsystem. </value>
        public DisplaySubsystem DisplaySubsystem { get; private set; }

        /// <summary> Binds the Display subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDisplaySubsystem( DisplaySubsystem subsystem )
        {
            if ( this.DisplaySubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DisplaySubsystem );
                this.DisplaySubsystem = null;
            }

            this.DisplaySubsystem = subsystem;
            if ( this.DisplaySubsystem is object )
            {
                this.Subsystems.Add( this.DisplaySubsystem );
            }
        }

        #endregion

        #region " SOURCE MEASURE UNIT "

        /// <summary> Gets or sets the source measure unit Subsystem. </summary>
        /// <value> The source measure unit Subsystem. </value>
        public SourceMeasureUnit SourceMeasureUnit { get; private set; }

        /// <summary> Binds the source measure unit subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceMeasureUnit( SourceMeasureUnit subsystem )
        {
            if ( this.SourceMeasureUnit is object )
            {
                this.SourceMeasureUnit.Remove( this.SourceMeasureUnit );
                _ = this.Subsystems.Remove( this.SourceMeasureUnit );
                this.SourceMeasureUnit = null;
            }

            this.SourceMeasureUnit = subsystem;
            if ( this.SourceMeasureUnit is object )
            {
                this.Subsystems.Add( this.SourceMeasureUnit );
                this.SourceMeasureUnit.Add( this.SourceMeasureUnit );
            }
        }

        #endregion

        #region " LOCAL NODE "

        /// <summary> Gets or sets the Local Node Subsystem. </summary>
        /// <value> The Local Node Subsystem. </value>
        public LocalNodeSubsystem LocalNodeSubsystem { get; private set; }

        /// <summary> Binds the Local Node subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindLocalNodeSubsystem( LocalNodeSubsystem subsystem )
        {
            if ( this.LocalNodeSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.LocalNodeSubsystem );
                this.LocalNodeSubsystem = null;
            }

            this.LocalNodeSubsystem = subsystem;
            if ( this.LocalNodeSubsystem is object )
            {
                this.Subsystems.Add( this.LocalNodeSubsystem );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Gets or sets the Sense Subsystem. </summary>
        /// <value> The Sense Subsystem. </value>
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Binds the Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseSubsystem( SenseSubsystem subsystem )
        {
            if ( this.SenseSubsystem is object )
            {
                this.SourceMeasureUnit.Remove( this.SenseSubsystem );
                _ = this.Subsystems.Remove( this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            this.SenseSubsystem = subsystem;
            if ( this.SenseSubsystem is object )
            {
                this.Subsystems.Add( this.SenseSubsystem );
                this.SourceMeasureUnit.Add( this.SenseSubsystem );
            }
        }

        #endregion

        #region " SOURCE "

        /// <summary> Gets or sets the Source Subsystem. </summary>
        /// <value> The Source Subsystem. </value>
        public SourceSubsystem SourceSubsystem { get; private set; }

        /// <summary> Binds the Source subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceSubsystem( SourceSubsystem subsystem )
        {
            if ( this.SourceSubsystem is object )
            {
                this.SourceMeasureUnit.Remove( this.SourceSubsystem );
                _ = this.Subsystems.Remove( this.SourceSubsystem );
                this.SourceSubsystem = null;
            }

            this.SourceSubsystem = subsystem;
            if ( this.SourceSubsystem is object )
            {
                this.Subsystems.Add( this.SourceSubsystem );
                this.SourceMeasureUnit.Add( this.SourceSubsystem );
            }
        }

        #endregion

        #region " MEASURE RESISTANCE "

        /// <summary> Gets or sets the Measure Resistance Subsystem. </summary>
        /// <value> The Measure Resistance Subsystem. </value>
        public MeasureResistanceSubsystem MeasureResistanceSubsystem { get; private set; }

        /// <summary> Binds the Measure Resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureResistanceSubsystem( MeasureResistanceSubsystem subsystem )
        {
            if ( this.MeasureResistanceSubsystem is object )
            {
                this.SourceMeasureUnit.Remove( this.MeasureResistanceSubsystem );
                _ = this.Subsystems.Remove( this.MeasureResistanceSubsystem );
                this.MeasureResistanceSubsystem = null;
            }

            this.MeasureResistanceSubsystem = subsystem;
            if ( this.MeasureResistanceSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureResistanceSubsystem );
                this.SourceMeasureUnit.Add( this.MeasureResistanceSubsystem );
            }
        }

        #endregion

        #region " MEASURE VOLTAGE "

        /// <summary> Gets or sets the Measure Voltage Subsystem. </summary>
        /// <value> The Measure Voltage Subsystem. </value>
        public MeasureVoltageSubsystem MeasureVoltageSubsystem { get; private set; }

        /// <summary> Binds the Measure Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureVoltageSubsystem( MeasureVoltageSubsystem subsystem )
        {
            if ( this.MeasureVoltageSubsystem is object )
            {
                this.SourceMeasureUnit.Remove( this.MeasureVoltageSubsystem );
                _ = this.Subsystems.Remove( this.MeasureVoltageSubsystem );
                this.MeasureVoltageSubsystem = null;
            }

            this.MeasureVoltageSubsystem = subsystem;
            if ( this.MeasureVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureVoltageSubsystem );
                this.SourceMeasureUnit.Add( this.MeasureVoltageSubsystem );
            }
        }

        #endregion

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "K2600 Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.ReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.StatusReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime;
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Settings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
