using System;

namespace isr.VI.Tsp.K2600
{

    /// <summary> Status subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> The session. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( session )
        {
            this.VersionInfoBase = new VersionInfo();
            InitializeSession( session );
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based TSP session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.ClearExecutionStateCommand = Syntax.Status.ClearExecutionStateCommand;
            session.DeviceClearDelayPeriod = TimeSpan.FromMilliseconds( 10d );
            session.OperationCompletedQueryCommand = Syntax.Lua.OperationCompletedQueryCommand;
            session.ResetKnownStateCommand = Syntax.Lua.ResetKnownStateCommand;
            session.ServiceRequestEnableCommandFormat = Syntax.Status.ServiceRequestEnableCommandFormat;
            session.ServiceRequestEnableQueryCommand = Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand;
            session.StandardEventStatusQueryCommand = Syntax.Status.StandardEventStatusQueryCommand;
            session.StandardEventEnableQueryCommand = Syntax.Status.StandardEventEnableQueryCommand;
            session.StandardServiceEnableCommandFormat = Syntax.Status.StandardServiceEnableCommandFormat;
            session.StandardServiceEnableCompleteCommandFormat = Syntax.Status.StandardServiceEnableCompleteCommandFormat;

            // session.WaitCommand = Tsp.Syntax.Lua.WaitCommand
            session.WaitCommand = Pith.Ieee488.Syntax.WaitCommand;
            session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
        }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = Syntax.ErrorQueue.ClearErrorQueueCommand;

        /// <summary> Gets or sets the error queue query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = Syntax.ErrorQueue.ErrorQueueQueryCommand;

        /// <summary> Gets or sets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected override string DeviceErrorQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the 'Next Error' query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string DequeueErrorQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " MEASUREMENT REGISTER EVENTS "

        /// <summary> Gets or sets the measurement status query command. </summary>
        /// <value> The measurement status query command. </value>
        protected override string MeasurementStatusQueryCommand { get; set; } = Syntax.Status.MeasurementEventQueryCommand;

        /// <summary> Gets or sets the measurement event condition query command. </summary>
        /// <value> The measurement event condition query command. </value>
        protected override string MeasurementEventConditionQueryCommand { get; set; } = Syntax.Status.MeasurementEventConditionQueryCommand;

        #endregion

        #region " OPERATION REGISTER EVENTS "

        /// <summary> Gets or sets the operation event enable Query command. </summary>
        /// <value> The operation event enable Query command. </value>
        protected override string OperationEventEnableQueryCommand { get; set; } = Syntax.Status.OperationEventEnableQueryCommand;

        /// <summary> Gets or sets the operation event enable command format. </summary>
        /// <value> The operation event enable command format. </value>
        protected override string OperationEventEnableCommandFormat { get; set; } = Syntax.Status.OperationEventEnableCommandFormat;

        /// <summary> Gets or sets the operation event status query command. </summary>
        /// <value> The operation event status query command. </value>
        protected override string OperationEventStatusQueryCommand { get; set; } = Syntax.Status.OperationEventQueryCommand;

        #endregion

        #region " QUESTIONABLE REGISTER "

        /// <summary> Gets or sets the questionable status query command. </summary>
        /// <value> The questionable status query command. </value>
        protected override string QuestionableStatusQueryCommand { get; set; } = Syntax.Status.QuestionableEventQueryCommand;

        #endregion

        #region " IDENTITIY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = Pith.Ieee488.Syntax.IdentityQueryCommand + " " + Pith.Ieee488.Syntax.WaitCommand;

        #endregion

    }
}
