﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( "K2600 VI Tests" )]
[assembly: AssemblyDescription( "K2600 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Tsp.K2600.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
