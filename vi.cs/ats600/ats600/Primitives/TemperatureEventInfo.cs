using System;
using System.ComponentModel;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600
{

    /// <summary> Information about the Temperature Event. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-01-09 </para>
    /// </remarks>
    public class TemperatureEventInfo
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public TemperatureEventInfo() : base()
        {
            this.ClearKnownStateThis();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="temperatureEvent"> The temperature event. </param>
        public TemperatureEventInfo( TemperatureEvents temperatureEvent ) : this()
        {
            this.TemperatureEvent = temperatureEvent;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        public TemperatureEventInfo( TemperatureEventInfo value ) : this()
        {
            if ( value is object )
            {
                this.TemperatureEvent = value.TemperatureEvent;
            }
        }

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ClearKnownStateThis()
        {
            this.TemperatureEvent = TemperatureEvents.None;
        }

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ClearKnownState()
        {
            this.ClearKnownStateThis();
        }

        #endregion

        #region " VALUE "

        /// <summary> The temperature event. </summary>

        /// <summary> Gets the temperature event. </summary>
        /// <value> The temperature event. </value>
        public TemperatureEvents TemperatureEvent { get; private set; }

        #endregion

        #region " BIT VALUES "

        /// <summary>
        /// Query if the Bits of the <paramref name="bits">Temperature Events</paramref> are on.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="bits">  The bits. </param>
        /// <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
        public static bool IsBit( TemperatureEvents value, TemperatureEvents bits )
        {
            return (value & bits) != 0;
        }

        /// <summary>
        /// Query if the Bit of the <paramref name="bit">Temperature Event</paramref> is on.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="bit"> The bit. </param>
        /// <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
        public bool IsBit( TemperatureEventBit bit )
        {
            return this.IsBit( ( TemperatureEvents ) Conversions.ToInteger( ( int ) Math.Round( Math.Pow( 2d, ( int ) bit ) ) ) );
        }

        /// <summary>
        /// Query if the Bits of the <paramref name="bits">Temperature Events</paramref> are on.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="bits"> The bits. </param>
        /// <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
        public bool IsBit( TemperatureEvents bits )
        {
            return (this.TemperatureEvent & bits) != 0;
        }

        /// <summary> Query if this object is end of cycles. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>true</c> if end of cycles; otherwise <c>false</c> </returns>
        public bool IsEndOfCycles()
        {
            return this.IsBit( TemperatureEvents.EndOfCycles );
        }

        /// <summary> Query if this object is at temperature. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>true</c> if at temperature; otherwise <c>false</c> </returns>
        public bool IsAtTemperature()
        {
            return this.IsBit( TemperatureEvents.AtTemperature );
        }

        /// <summary> Query if this object is not at temperature. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>true</c> if not at temperature; otherwise <c>false</c> </returns>
        public bool IsNotAtTemperature()
        {
            return this.IsBit( TemperatureEvents.NotAtTemperature );
        }

        /// <summary> Query if this object is end of cycle. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>true</c> if end of cycle; otherwise <c>false</c> </returns>
        public bool IsEndOfCycle()
        {
            return this.IsBit( TemperatureEvents.EndOfCycle );
        }

        /// <summary> Query if this object is cycling stopped. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>true</c> if cycling stopped; otherwise <c>false</c> </returns>
        public bool IsCyclingStopped()
        {
            return this.IsBit( TemperatureEvents.CyclingStopped );
        }

        /// <summary> Query if this object is test time elapsed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>true</c> if test time elapsed; otherwise <c>false</c> </returns>
        public bool IsTestTimeElapsed()
        {
            return this.IsBit( TemperatureEvents.TestTimeElapsed );
        }

        /// <summary> Gets the description. </summary>
        /// <value> The description. </value>
        public string Description => this.TemperatureEvent.Description();

        /// <summary> Query if any <see cref="TemperatureEvent">bits</see> are. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="included"> The included. </param>
        /// <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
        public bool IsIncluded( TemperatureEvents included )
        {
            return IsIncluded( this.TemperatureEvent, included );
        }

        #endregion

        #region " HELPERS "

        /// <summary> Builds Temperature Events. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="excluded"> The excluded. </param>
        /// <param name="included"> The included. </param>
        /// <returns> The TemperatureEvents. </returns>
        public static TemperatureEvents BuildTemperatureEvents( TemperatureEvents value, TemperatureEvents excluded, TemperatureEvents included )
        {
            return (value | included) & ~excluded;
        }

        /// <summary> Query if 'value' is included. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="included"> The included. </param>
        /// <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
        public static bool IsIncluded( TemperatureEvents value, TemperatureEvents included )
        {
            return (value & included) != TemperatureEvents.None;
        }

        #endregion

    }

    /// <summary> Values that represent the Temperature Event bit. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public enum TemperatureEventBit
    {
        /// <summary> An enum constant representing at temperature option. </summary>

        [Description( "At Temperature" )]
        AtTemperature = 0,
        /// <summary> An enum constant representing the not at temperature option. </summary>

        [Description( "Not At Temperature" )]
        NotAtTemperature = 1,
        /// <summary> An enum constant representing the test time elapsed option. </summary>

        [Description( "Test Time Elapsed" )]
        TestTimeElapsed = 2,
        /// <summary> An enum constant representing the end of cycle option. </summary>

        [Description( "End Of Cycle" )]
        EndOfCycle = 3,
        /// <summary> An enum constant representing the end of cycles option. </summary>

        [Description( "End Of Cycles" )]
        EndOfCycles = 4,
        /// <summary> An enum constant representing the cycling stopped option. </summary>

        [Description( "Cycling Stopped" )]
        CyclingStopped = 5
    }

    /// <summary>
    /// A bit field of flags for specifying combination of Temperature Event values.
    /// </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    [Flags()]
    public enum TemperatureEvents
    {
        /// <summary> An enum constant representing the none option. </summary>
        [Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing at temperature option. </summary>
        [Description( "At Temperature" )]
        AtTemperature = 1,

        /// <summary> An enum constant representing the not at temperature option. </summary>
        [Description( "Not At Temperature" )]
        NotAtTemperature = 2,

        /// <summary> An enum constant representing the test time elapsed option. </summary>
        [Description( "Test Time Elapsed" )]
        TestTimeElapsed = 4,

        /// <summary> An enum constant representing the end of cycle option. </summary>
        [Description( "End Of Cycle" )]
        EndOfCycle = 8,

        /// <summary> An enum constant representing the end of cycles option. </summary>
        [Description( "End Of Cycles" )]
        EndOfCycles = 16,

        /// <summary> An enum constant representing the cycling stopped option. </summary>
        [Description( "Cycling Stopped" )]
        CyclingStopped = 32
    }
}
