using System;
using System.ComponentModel;

using isr.Core.EnumExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600
{

    /// <summary> Information about the Auxiliary Status. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-01-09 </para>
    /// </remarks>
    public class AuxiliaryStatusInfo
    {

        #region " CONSTRUCTION "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public AuxiliaryStatusInfo() : base()
        {
            this.ClearKnownStateThis();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="auxiliaryStatus"> The auxiliary status. </param>
        public AuxiliaryStatusInfo( AuxiliaryStatuses auxiliaryStatus ) : this()
        {
            this.AuxiliaryStatus = auxiliaryStatus;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        public AuxiliaryStatusInfo( AuxiliaryStatusInfo value ) : this()
        {
            if ( value is object )
            {
                this.AuxiliaryStatus = value.AuxiliaryStatus;
            }
        }

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ClearKnownStateThis()
        {
            this.AuxiliaryStatus = AuxiliaryStatuses.None;
        }

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ClearKnownState()
        {
            this.ClearKnownStateThis();
        }

        #endregion

        #region " VALUE "

        /// <summary> The auxiliary status. </summary>

        /// <summary> Gets the auxiliary Status. </summary>
        /// <value> The auxiliary Status. </value>
        public AuxiliaryStatuses AuxiliaryStatus { get; private set; }

        #endregion

        #region " BIT VALUES "

        /// <summary>
        /// Query if the Bits of the <paramref name="bits">Auxiliary Statuses</paramref> are on.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <param name="bits">  The bits. </param>
        /// <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
        public static bool IsBit( AuxiliaryStatuses value, AuxiliaryStatuses bits )
        {
            return (value & bits) != 0;
        }

        /// <summary>
        /// Query if the Bit of the <paramref name="bit">Auxiliary Status</paramref> is on.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="bit"> The bit. </param>
        /// <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
        public bool IsBit( AuxiliaryStatusBit bit )
        {
            return this.IsBit( ( AuxiliaryStatuses ) Conversions.ToInteger( ( int ) Math.Round( Math.Pow( 2d, ( int ) bit ) ) ) );
        }

        /// <summary>
        /// Query if the Bits of the <paramref name="bits">Auxiliary Statuses</paramref> are on.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="bits"> The bits. </param>
        /// <returns> <c>true</c> if Bin; otherwise <c>false</c> </returns>
        public bool IsBit( AuxiliaryStatuses bits )
        {
            return (this.AuxiliaryStatus & bits) != 0;
        }

        /// <summary> Query if this object is dut control. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if dut control, false if not. </returns>
        public bool IsDutControl()
        {
            return this.IsBit( AuxiliaryStatuses.DutControlMode );
        }

        /// <summary> Query if this object is heat only mode. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if heat only mode, false if not. </returns>
        public bool IsHeatOnlyMode()
        {
            return this.IsBit( AuxiliaryStatuses.HeatOnlyMode );
        }

        /// <summary> Query if this object is ready. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if ready, false if not. </returns>
        public bool IsReady()
        {
            return this.IsBit( AuxiliaryStatuses.ReadyStartup );
        }

        /// <summary> Query if this object is manual mode. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if manual mode, false if not. </returns>
        public bool IsManualMode()
        {
            return this.IsBit( AuxiliaryStatuses.ManualProgram );
        }

        /// <summary> Query if this object is ramp mode. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if ramp mode, false if not. </returns>
        public bool IsRampMode()
        {
            return this.IsBit( AuxiliaryStatuses.RampMode );
        }

        /// <summary> Query if this object is flow on. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if flow on, false if not. </returns>
        public bool IsFlowOn()
        {
            return this.IsBit( AuxiliaryStatuses.FlowOnOff );
        }

        /// <summary> Query if this object is head up. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> True if head up, false if not. </returns>
        public bool IsHeadUp()
        {
            return this.IsBit( AuxiliaryStatuses.HeadUpDown );
        }

        /// <summary> Gets the description. </summary>
        /// <value> The description. </value>
        public string Description => this.AuxiliaryStatus.Description();

        /// <summary> Query if any <see cref="AuxiliaryStatus">bits</see> are. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="included"> The included. </param>
        /// <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
        public bool IsIncluded( AuxiliaryStatuses included )
        {
            return IsIncluded( this.AuxiliaryStatus, included );
        }

        #endregion

        #region " HELPERS "

        /// <summary> Builds Auxiliary Statuses. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="excluded"> The excluded. </param>
        /// <param name="included"> The included. </param>
        /// <returns> The AuxiliaryStatuses. </returns>
        public static AuxiliaryStatuses BuildAuxiliaryStatuses( AuxiliaryStatuses value, AuxiliaryStatuses excluded, AuxiliaryStatuses included )
        {
            return (value | included) & ~excluded;
        }

        /// <summary> Query if 'value' is included. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value">    The value. </param>
        /// <param name="included"> The included. </param>
        /// <returns> <c>true</c> if included; otherwise <c>false</c> </returns>
        public static bool IsIncluded( AuxiliaryStatuses value, AuxiliaryStatuses included )
        {
            return (value & included) != AuxiliaryStatuses.None;
        }

        #endregion

    }

    /// <summary> Values that represent the auxiliary Status bit. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public enum AuxiliaryStatusBit
    {
        /// <summary> An enum constant representing the none option. </summary>

        [Description( "Not defined" )]
        None = 0,
        /// <summary> An enum constant representing the head up down option. </summary>

        [Description( "Head Up (1) Down (0)" )]
        HeadUpDown = 2,
        /// <summary> An enum constant representing the heat only mode option. </summary>

        [Description( "Heat Only (1) Compressor on (0)" )]
        HeatOnlyMode = 3,
        /// <summary> An enum constant representing the dut control mode option. </summary>

        [Description( "Dut-control (1) or air-control (0) Mode" )]
        DutControlMode = 4,
        /// <summary> An enum constant representing the flow on off option. </summary>

        [Description( "Flow on (1) or off (0)" )]
        FlowOnOff = 5,
        /// <summary> An enum constant representing the ready startup option. </summary>

        [Description( "Ready (1) Startup (0)" )]
        ReadyStartup = 6,
        /// <summary> An enum constant representing the manual program option. </summary>

        [Description( "Short Failure" )]
        ManualProgram = 8,
        /// <summary> An enum constant representing the ramp mode option. </summary>

        [Description( "Ramp Mode" )]
        RampMode = 9
    }

    /// <summary> A bit field of flags for specifying combination of Auxiliary Statuses. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    [Flags()]
    public enum AuxiliaryStatuses
    {
        /// <summary> An enum constant representing the none option. </summary>

        [Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the head up down option. </summary>
        [Description( "Head Up (1) Down (0)" )]
        HeadUpDown = 4, // 2d ^ AuxiliaryStatusBit.HeadUpDown

        /// <summary> An enum constant representing the heat only mode option. </summary>
        [Description( "Heat Only (1) Compressor on (0)" )]
        HeatOnlyMode = 8,

        /// <summary> An enum constant representing the dut control mode option. </summary>
        [Description( "Dut-control (1) or air-control (0) Mode" )]
        DutControlMode = 16,

        /// <summary> An enum constant representing the flow on off option. </summary>
        [Description( "Flow on (1) or off (0)" )]
        FlowOnOff = 32,

        /// <summary> An enum constant representing the ready startup option. </summary>
        [Description( "Ready (1) Startup (0)" )]
        ReadyStartup = 64,

        /// <summary> An enum constant representing the manual program option. </summary>
        [Description( "Manual (1) Program (0)" )]
        ManualProgram = 256,

        /// <summary> An enum constant representing the ramp mode option. </summary>
        [Description( "Ramp Mode" )]
        RampMode = 512
    }
}
