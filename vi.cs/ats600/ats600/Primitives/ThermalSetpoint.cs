using System;

namespace isr.VI.Ats600
{

    /// <summary> Thermal setpoint. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-26 </para>
    /// </remarks>
    public class ThermalSetpoint
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public ThermalSetpoint() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="ordinalNumber"> The ordinal number. </param>
        public ThermalSetpoint( int ordinalNumber ) : this()
        {
            this.OrdinalNumber = ordinalNumber;
        }

        /// <summary> Gets or sets the profile element. </summary>
        /// <value> The setpoint number. </value>
        public int OrdinalNumber { get; set; }

        /// <summary> Gets or sets the temperature. </summary>
        /// <value> The temperature. </value>
        public double Temperature { get; set; }

        /// <summary> Gets or sets the ramp rate in degrees per second. </summary>
        /// <value> The ramp rate. </value>
        public double RampRate { get; set; }

        /// <summary> Gets or sets the soak seconds. </summary>
        /// <value> The soak seconds. </value>
        public int SoakSeconds { get; set; }

        /// <summary> Gets or sets the window. </summary>
        /// <value> The window. </value>
        public double Window { get; set; }
    }

    /// <summary> Thermal profile collection. </summary>
    /// <remarks>
    /// (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2015-03-26 </para>
    /// </remarks>
    public class ThermalProfileCollection : System.Collections.ObjectModel.KeyedCollection<int, ThermalSetpoint>
    {

        /// <summary> Gets key for item. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="item"> The item. </param>
        /// <returns> The key for item. </returns>
        protected override int GetKeyForItem( ThermalSetpoint item )
        {
            return item is null ? throw new ArgumentNullException( nameof( item ) ) : item.OrdinalNumber;
        }

        /// <summary> Adds a new setpoint. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="ordinalNumber"> The ordinal number. </param>
        /// <returns> A ThermalSetpoint. </returns>
        public ThermalSetpoint AddNewSetpoint( int ordinalNumber )
        {
            var setPoint = new ThermalSetpoint( ordinalNumber );
            this.Add( setPoint );
            return setPoint;
        }
    }
}
