## ISR VI Thermo Stream<sub>&trade;</sub>: Thermo Stream VI Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.0.6248 2017-02-08*  
Issues the ramp setting command in addition to the
compound command.

*4.0.6243 2017-02-03*  
Sets read back value if ramp rate is empty (not yet
written).

*4.0.6242 2017-02-02*  
Skips reading back ramp rates below 100 degrees C per
minute because Ats600 returns 0.1 the actual value.

*4.0.6241 2017-02-01*  
Fixes the Ramp Rate range report.

*4.0.6240 2017-01-31*  
Adds low and high ramp range to make sure the correct
command is sent out. Adds ramp rate units.

*4.0.5907 2016-03-04*  
Disables controls when opening the device panel.

*4.0.5803 2015-11-21 New structures and name spaces.

*3.0.5577 2015-04-09 Update detecting at temperature.

*3.0.5553 2015-03-16 Created with code from the multi meter library.

\(C\) 2015 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
