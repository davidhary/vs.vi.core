﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.VI.Ats600.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.VI.Ats600.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.VI.Ats600.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
