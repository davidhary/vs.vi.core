namespace isr.VI.Ats600
{

    /// <summary> Information about the version. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-21 </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void Clear()
        {
            base.Clear();
            this.ParseFirmwareRevision( "" );
        }

        /// <summary> Parses the instrument identity string. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> Specifies the instrument identity string, which includes at a minimum the
        /// following information: e.g., <para> <c>???</c>.</para></param>
        public override void Parse( string value )
        {

            // clear
            base.Parse( "" );

            // save the identity.
            this.Identity = value;
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                base.Parse( value );

                // parse thee firmware revision
                this.ParseFirmwareRevision( "" );
            }
        }
    }
}
