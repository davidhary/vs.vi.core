using System;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600
{

    /// <summary>
    /// Defines a Thermal Stream Subsystem for the InTest Thermo Stream instrument.
    /// </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-03-01, 3.0.5173. </para>
    /// </remarks>
    public class ThermalStreamSubsystem : ThermalStreamSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="ThermalStreamSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public ThermalStreamSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this._TemperatureEventInfo = new TemperatureEventInfo( TemperatureEvents.None );
            this._AuxiliaryStatusInfo = new AuxiliaryStatusInfo( AuxiliaryStatuses.None );
            this._ThermalProfile = new ThermalProfileCollection();
            this.AmbientTemperature = 25;
        }
        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.CycleCountRange = new Core.Primitives.RangeI( 1, 9999 );
            this.DeviceThermalConstantRange = new Core.Primitives.RangeI( 20, 500 );
            this.LowRampRateRange = new Core.Primitives.RangeR( 0d, 99.99d );
            this.HighRampRateRange = new Core.Primitives.RangeR( 100d, 9999d );
            this.SetpointRange = new Core.Primitives.RangeR( -99.9d, 225d );
            this.SetpointNumberRange = new Core.Primitives.RangeI( 0, 11 );
            this.SetpointWindowRange = new Core.Primitives.RangeR( 0.1d, 9.9d );
            this.SoakTimeRange = new Core.Primitives.RangeI( 0, 9999 );
            this.MaximumTestTimeRange = new Core.Primitives.RangeI( 0, 9999 );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            _ = this.QuerySystemScreen();
            if ( !this.OperatorScreen.GetValueOrDefault( false ) )
            {
                this.ApplyOperatorScreen();
            }
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " READ TEMPERATURE "

        /// <summary> Reads current set point values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ReadCurrentSetpointValues()
        {
            _ = this.QuerySetpointNumber();
            _ = this.QuerySetpointWindow();
            _ = this.QuerySoakTime();
            _ = this.QuerySetpoint();
            _ = this.QueryRampRate();
        }

        /// <summary> Searches for the zero-based last setpoint number. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> The found setpoint number. </returns>
        public int FindLastSetpointNumber()
        {
            int currentSetpoint = this.SetpointNumber.GetValueOrDefault( -1 );
            int setPointNumber = -1;
            for ( int i = this.SetpointNumberRange.Min, loopTo = this.SetpointNumberRange.Max; i <= loopTo; i++ )
            {
                _ = this.ApplySetpointNumber( i );
                if ( this.Session.IsSessionOpen )
                {
                    Core.ApplianceBase.DoEventsWait( this.NextSetpointRefractoryTimeSpan );
                }

                _ = this.QueryRampRate();
                if ( this.RampRate.HasValue && Math.Abs( this.RampRate.Value ) > 0.01d )
                {
                    setPointNumber = i;
                }
                else
                {
                    break;
                }
            }

            if ( currentSetpoint >= 0 )
                _ = this.ApplySetpointNumber( currentSetpoint );
            return setPointNumber;
        }

        #endregion

        #region " DEVICE ERROR "

        /// <summary> Gets or sets the DeviceError query command. </summary>
        /// <value> The DeviceError query command. </value>
        protected override string DeviceErrorQueryCommand { get; set; } = "EROR?";

        #endregion

        #region " CYCLE COUNT "

        /// <summary> Gets or sets the Cycle Count command format. </summary>
        /// <value> the Cycle Count command format. </value>
        protected override string CycleCountCommandFormat { get; set; } = "CYCC {0}";

        /// <summary> Gets or sets the Cycle Count query command. </summary>
        /// <value> the Cycle Count query command. </value>
        protected override string CycleCountQueryCommand { get; set; } = "CYCC?";

        #endregion

        #region " CYCLE NUMBER "

        /// <summary> Gets or sets the Cycle Number query command. </summary>
        /// <value> the Cycle Number query command. </value>
        protected override string CycleNumberQueryCommand { get; set; } = "CYCL?";

        #endregion

        #region " CYCLING: START / STOP "

        /// <summary> Gets or sets the command execution refractory time span. </summary>
        /// <value> The command execution refractory time span. </value>
        public override TimeSpan CommandRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 5d );

        /// <summary> Gets or sets the start cycling command. </summary>
        /// <value> The start cycling command. </value>
        protected override string StartCyclingCommand { get; set; } = "CYCL 1";

        /// <summary> Gets or sets the stop cycling command. </summary>
        /// <value> The stop cycling command. </value>
        protected override string StopCyclingCommand { get; set; } = "CYCL 0";

        /// <summary> true if use cycles completed. </summary>
        public const bool UseCyclesCompleted = false;

        /// <summary> Start cycling and wait till confirmed. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="countdown"> The countdown. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) StartCycling( int countdown )
        {
            (bool Success, string Details) result = (true, string.Empty);
            // send the start command
            this.StartCycling();
            bool cycleCompleted = true;
            int setpointNumber = -1;
            int cycleNumber;
            do
            {
                countdown -= 1;
                Core.ApplianceBase.DoEventsWait( this.CommandRefractoryTimeSpan );
                _ = this.QueryCycleNumber();
                cycleNumber = this.CycleNumber.GetValueOrDefault( -1 );
                if ( cycleNumber > 0 )
                {
                    _ = this.QueryTemperatureEventStatus();
                    cycleCompleted = this.IsCycleCompleted || UseCyclesCompleted && this.IsCyclesCompleted;
                    _ = this.QuerySetpointNumber();
                    setpointNumber = this.SetpointNumber.GetValueOrDefault( -1 );
                }
            }
            while ( countdown != 0 && (cycleNumber <= 0 || cycleCompleted || setpointNumber != 0) );
            if ( this.IsCyclingStopped )
            {
                result = (false, $"Thermo-Stream cycle stopped on error. Testing will stop.");
            }
            else if ( !cycleCompleted )
            {
                result = (false, $"Thermo-Stream failed starting - cycle is tagged as completed after sending start. Testing will stop.");
            }
            else if ( cycleNumber <= 0 )
            {
                result = (false, $"Thermo-Stream failed starting - cycle number = {cycleNumber}. Testing will stop.");
            }
            else if ( setpointNumber > 0 )
            {
                result = (false, $"Thermo-Stream failed starting - set point = {setpointNumber}. Testing will stop.");
            }
            else
            {
                // got a start
            }

            return result;
        }

        /// <summary> Stop cycling and wait till confirmed. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="countdown"> The countdown. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) StopCycling( int countdown )
        {
            (bool Success, string Details) result = (true, string.Empty);
            // send the start command
            this.StopCycling();
            bool headUp = false;
            int cycleNumber;
            do
            {
                countdown -= 1;
                Core.ApplianceBase.DoEventsWait( this.CommandRefractoryTimeSpan );
                _ = this.QueryCycleNumber();
                cycleNumber = this.CycleNumber.GetValueOrDefault( -1 );
                if ( cycleNumber > 0 )
                {
                    _ = this.QueryAuxiliaryEventStatus();
                    headUp = this.IsHeadUp;
                }
            }
            while ( countdown != 0 && (cycleNumber != 0 || !headUp) );
            if ( !headUp )
            {
                result = (false, $"Thermo-Stream failed stopping--head is not up. Testing will stop.");
            }
            else if ( cycleNumber > 0 )
            {
                result = (false, $"Thermo-Stream failed starting--cycle number = {cycleNumber}. Testing will stop.");
            }
            else
            {
                // got a stop
            }

            return result;
        }

        #endregion

        #region " DEVICE CONTROL "

        /// <summary> Gets or sets the Device Control command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string DeviceControlCommandFormat { get; set; } = "DUTM {0:        '1';'1';'0'}";

        /// <summary> Gets or sets the Device Control query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string DeviceControlQueryCommand { get; set; } = "DUTM?";

        #endregion

        #region " DEVICE SENSOR TYPE "

        /// <summary> Gets or sets the Device Sensor Type command format. </summary>
        /// <value> the Device Sensor Type command format. </value>
        protected override string DeviceSensorTypeCommandFormat { get; set; } = "DSNS {0}";

        /// <summary> Gets or sets the Device Sensor Type query command. </summary>
        /// <value> the Device Sensor Type query command. </value>
        protected override string DeviceSensorTypeQueryCommand { get; set; } = "DSNS?";

        #endregion

        #region " DEVICE THERMAL CONSTANT "

        /// <summary> Gets or sets the Device Thermal Constant command format. </summary>
        /// <value> the Device Thermal Constant command format. </value>
        protected override string DeviceThermalConstantCommandFormat { get; set; } = "DUTC {0}";

        /// <summary> Gets or sets the Device Thermal Constant query command. </summary>
        /// <value> the Device Thermal Constant query command. </value>
        protected override string DeviceThermalConstantQueryCommand { get; set; } = "DUTC?";

        #endregion

        #region " HEAD STATUS "

        /// <summary> Gets or sets the head down command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string HeadDownCommandFormat { get; set; } = "HEAD {0:'1';'1';'0'}";

        /// <summary> Gets or sets the head down query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string HeadDownQueryCommand { get; set; } = "HEAD?";

        #endregion

        #region " RESET OPERATOR / CYCLE MODE "

        /// <summary>
        /// Gets or sets the refractory time span for resetting to Cycle (manual) mode.
        /// </summary>
        /// <value> The command execution refractory time span. </value>
        public override TimeSpan ResetCycleScreenRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 4000d );

        /// <summary> Gets or sets the Reset Cycle Screen command. </summary>
        /// <value> The Reset Cycle Screen command. </value>
        protected override string ResetCycleScreenCommand { get; set; } = "*RST";

        /// <summary> Cycle Screen. </summary>
        private bool? _CycleScreen;

        /// <summary> Gets or sets the cached Cycle Screen sentinel. </summary>
        /// <value>
        /// <c>null</c> if Cycle Screen is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? CycleScreen
        {
            get => this._CycleScreen;

            protected set {
                if ( !Equals( this.CycleScreen, value ) )
                {
                    this._CycleScreen = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the Cycle screen value. </summary>
        /// <value> The Cycle screen value. </value>
        public int CycleScreenValue { get; set; } = 10;

        /// <summary>
        /// Queries the Operator Screen sentinel. Also sets the
        /// <see cref="OperatorScreen">Operator Screen</see> sentinel.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public override int? QuerySystemScreen()
        {
            _ = base.QuerySystemScreen();
            if ( this.SystemScreen.HasValue )
            {
                this.OperatorScreen = this.SystemScreen.Value == this.OperatorScreenValue;
                this.CycleScreen = this.SystemScreen.Value == this.CycleScreenValue;
            }
            else
            {
                this.OperatorScreen = new bool?();
                this.CycleScreen = new bool?();
            }

            return default;
        }

        /// <summary>
        /// Queries the Cycle Screen sentinel. Also sets the
        /// <see cref="CycleScreen">Cycle Screen</see> sentinel.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryCycleScreen()
        {
            _ = this.QuerySystemScreen();
            return this.CycleScreen;
        }

        /// <summary> Applies the operator screen. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ApplyCyclesScreen()
        {
            this.ResetCycleScreen();
            _ = this.PublishVerbose( "Awaiting reset to Cycles (manual mode) screen;. " );
            Core.ApplianceBase.DoEventsWait( this.ResetCycleScreenRefractoryTimeSpan );
            _ = this.PublishVerbose( "Querying system screen;. " );
            _ = this.QuerySystemScreen();
        }

        /// <summary> Gets or sets the refractory time span for resetting to Operator Mode. </summary>
        /// <value> The command execution refractory time span. </value>
        public override TimeSpan ResetOperatorScreenRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 1000d );

        /// <summary> Gets or sets the Reset Operator Screen command. </summary>
        /// <value> The Reset Operator Screen command. </value>
        protected override string ResetOperatorScreenCommand { get; set; } = "RSTO";

        /// <summary> Operator Screen. </summary>
        private bool? _OperatorScreen;

        /// <summary> Gets or sets the cached Operator Screen sentinel. </summary>
        /// <value>
        /// <c>null</c> if Operator Screen is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public bool? OperatorScreen
        {
            get => this._OperatorScreen;

            protected set {
                if ( !Equals( this.OperatorScreen, value ) )
                {
                    this._OperatorScreen = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the operator screen value. </summary>
        /// <value> The operator screen value. </value>
        public int OperatorScreenValue { get; set; } = 10;

        /// <summary>
        /// Queries the Operator Screen sentinel. Also sets the
        /// <see cref="OperatorScreen">Operator Screen</see> sentinel.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> <c>True</c> if enabled; otherwise <c>False</c>. </returns>
        public bool? QueryOperatorScreen()
        {
            _ = this.QuerySystemScreen();
            return this.OperatorScreen;
        }

        /// <summary> Applies the operator screen. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ApplyOperatorScreen()
        {
            this.ResetOperatorScreen();
            _ = this.PublishVerbose( "Awaiting reset to Operator screen;. " );
            Core.ApplianceBase.DoEventsWait( this.ResetOperatorScreenRefractoryTimeSpan );
            _ = this.PublishVerbose( "Querying system screen;. " );
            _ = this.QuerySystemScreen();
        }

        #endregion

        #region " RAMP RATE "

        /// <summary> Gets Low the Ramp Rate command format. </summary>
        /// <value> the Low Ramp Rate command format. </value>
        protected override string LowRampRateCommandFormat { get; set; } = "RAMP {0:0.0}";

        /// <summary> Gets High the Ramp Rate command format. </summary>
        /// <value> the High Ramp Rate command format. </value>
        protected override string HighRampRateCommandFormat { get; set; } = "RAMP {0:0}";

        /// <summary> Gets the Ramp Rate query command. </summary>
        /// <value> the Ramp Rate query command. </value>
        protected override string RampRateQueryCommand { get; set; } = "RAMP?";

        #endregion

        #region " SET POINT "

        /// <summary> Gets The Set Point command format. </summary>
        /// <value> The Set Point command format. </value>
        protected override string SetpointCommandFormat { get; set; } = "SETP {0}";

        /// <summary> Gets The Set Point query command. </summary>
        /// <value> The Set Point query command. </value>
        protected override string SetpointQueryCommand { get; set; } = "SETP?";

        #endregion

        #region " SET POINT: NEXT "

        /// <summary> Gets the next setpoint refractory time span. </summary>
        /// <value> The next setpoint refractory time span. </value>
        public override TimeSpan NextSetpointRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 100d );

        /// <summary> Gets the Next Set Point command. </summary>
        /// <value> The Next Set Point command. </value>
        protected override string NextSetpointCommand { get; set; } = "NEXT";

        /// <summary> Gets a value indicating whether the cycle completed cache. </summary>
        /// <value> <c>true</c> if cycle completed cache; otherwise <c>false</c> </value>
        public bool CycleCompletedCache { get; private set; }

        /// <summary> Query if this object is cycle ended. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> True if cycle completed; otherwise, false. </returns>
        public bool QueryCycleCompleted()
        {
            _ = this.QueryTemperatureEventStatus();
            bool affirmative = this.IsCycleCompleted || UseCyclesCompleted && this.IsCyclesCompleted;
            if ( !affirmative )
            {
                _ = this.QueryAuxiliaryEventStatus();
                affirmative = this.IsHeadUp;
                if ( !affirmative )
                {
                    _ = this.QueryRampRate();
                    affirmative = this.RampRate.GetValueOrDefault( 0d ) < 0.1d;
                }
            }

            return affirmative;
        }

        /// <summary> Move next setpoint. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="countdown"> The countdown. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) MoveNextSetpoint( int countdown )
        {
            (bool Success, string Details) result = (true, string.Empty);
            // otherwise, save the current setpoint number
            _ = this.QuerySetpointNumber();
            if ( this.SetpointNumber.HasValue )
            {
                int setpointNumber = this.SetpointNumber.Value;
                // move next.
                this.NextSetpoint();
                this.CycleCompletedCache = false;
                do
                {
                    countdown -= 1;
                    Core.ApplianceBase.DoEventsWait( this.NextSetpointRefractoryTimeSpan );
                    this.CycleCompletedCache = this.QueryCycleCompleted();
                    if ( !this.CycleCompletedCache )
                        _ = this.QuerySetpointNumber();
                }
                while ( countdown != 0 && !this.CycleCompletedCache && this.SetpointNumber.GetValueOrDefault( setpointNumber ) == setpointNumber );
                if ( this.IsCyclingStopped )
                {
                    result = (false, "Thermo-Stream cycle stopped on error. Testing will stop.");
                }
                else if ( this.CycleCompletedCache )
                {
                }
                // done with this cycle, no need to check the new setpoint number
                else if ( this.SetpointNumber.GetValueOrDefault( setpointNumber ) == setpointNumber )
                {
                    result = (false, $"Thermo-Stream failed advancing the setpoint number from {setpointNumber}. Testing will stop.");
                }
                else
                {
                    // got a new setpoint number
                }
            }
            else
            {
                result = (false, "Thermo-Stream failed reading the setpoint number--value is empty. Testing will stop.");
                Core.ApplianceBase.DoEvents();
            }

            return result;
        }

        /// <summary>
        /// Queries if cycles stopped or terminated; Otherwise, steps to the next temperature.
        /// </summary>
        /// <remarks> This is the same as indicating that the start of test signal was received. </remarks>
        /// <param name="countdown">      The countdown. </param>
        /// <param name="additionalInfo"> Information describing the additional. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ConditionalCycleMoveNext( int countdown, string additionalInfo )
        {
            if ( string.IsNullOrEmpty( additionalInfo ) )
                additionalInfo = "waiting for At-Temp";
            _ = this.QueryTemperatureEventStatus();
            if ( this.IsCycleCompleted )
            {
                return (false, $"Thermo-Stream cycle completed while {additionalInfo}");
            }
            else if ( UseCyclesCompleted && this.IsCyclesCompleted )
            {
                return (false, $"Thermo-Stream completed all cycles while  {additionalInfo}");
            }
            else if ( this.IsCyclingStopped )
            {
                return (false, $"Thermo-Stream cycle was stopped while  {additionalInfo}");
            }
            else
            {
                var (_, Success, Details) = this.ValidateHandlerHeadDown();
                return Success ? this.MoveNextSetpoint( countdown ) : (false, $"Unable to proceed {additionalInfo};. Details: {Details}");
            }
        }

        #endregion

        #region " SET POINT NUMBER "

        /// <summary> Gets the Set Point Number command format. </summary>
        /// <value> the Set Point Number command format. </value>
        protected override string SetpointNumberCommandFormat { get; set; } = "SETN {0}";

        /// <summary> Gets the Set Point Number query command. </summary>
        /// <value> the Set Point Number query command. </value>
        protected override string SetpointNumberQueryCommand { get; set; } = "SETN?";

        #endregion

        #region " SET POINT WINDOW "

        /// <summary> Gets the Set Point Window command format. </summary>
        /// <value> the Set Point Window command format. </value>
        protected override string SetpointWindowCommandFormat { get; set; } = "WNDW {0}";

        /// <summary> Gets the Set Point Window query command. </summary>
        /// <value> the Set Point Window query command. </value>
        protected override string SetpointWindowQueryCommand { get; set; } = "WNDW?";

        #endregion

        #region " SOAK TIME "

        /// <summary> Gets the Soak Time command format. </summary>
        /// <value> the Soak Time command format. </value>
        protected override string SoakTimeCommandFormat { get; set; } = "SOAK {0}";

        /// <summary> Gets the Soak Time query command. </summary>
        /// <value> the Soak Time query command. </value>
        protected override string SoakTimeQueryCommand { get; set; } = "SOAK?";

        #endregion

        #region " TEMPERATURE "

        /// <summary> Gets the Temperature query command. </summary>
        /// <value> The Temperature query command. </value>
        protected override string TemperatureQueryCommand { get; set; } = "TEMP?";

        #endregion

        #region " MAXIMUM TEST TIME "

        /// <summary> Gets the Maximum Test Time command format. </summary>
        /// <value> the Maximum Test Time command format. </value>
        protected override string MaximumTestTimeCommandFormat { get; set; } = "TTIM {0}";

        /// <summary> Gets the Maximum Test Time query command. </summary>
        /// <value> the Maximum Test Time query command. </value>
        protected override string MaximumTestTimeQueryCommand { get; set; } = "TTIM?";

        #endregion

        #region " SYSTEM SCREEN "

        /// <summary> Gets the System Screen query command. </summary>
        /// <value> The System Screen query command. </value>
        protected override string SystemScreenQueryCommand { get; set; } = "WHAT?";

        #endregion

        #endregion

        #region " PROFILE "

        /// <summary> The thermal profile. </summary>
        private ThermalProfileCollection _ThermalProfile;

        /// <summary> Thermal profile. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A ThermalProfileCollection. </returns>
        public ThermalProfileCollection ThermalProfile()
        {
            return this._ThermalProfile;
        }

        /// <summary> Creates new profile. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void CreateNewProfile()
        {
            this._ThermalProfile = new ThermalProfileCollection();
        }

        /// <summary> Gets the active thermal profile setpoint. </summary>
        /// <value> The active thermal setpoint. </value>
        public ThermalSetpoint ActiveThermalSetpoint { get; private set; }

        /// <summary> Adds a new setpoint. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="number"> Number of. </param>
        /// <returns> A ThermalSetpoint. </returns>
        public ThermalSetpoint AddNewSetpoint( int number )
        {
            return this._ThermalProfile.AddNewSetpoint( number );
        }

        /// <summary> Gets or sets the ambient temperature. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The ambient temperature. </value>
        public int AmbientTemperature { get; set; }

        /// <summary> Gets or sets the hot setpoint number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The hot setpoint number. </value>
        public int HotSetpointNumber { get; set; } = 0;

        /// <summary> Gets or sets the ambient setpoint number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The ambient setpoint number. </value>
        public int AmbientSetpointNumber { get; set; } = 1;

        /// <summary> Gets or sets the cold setpoint number. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <value> The cold setpoint number. </value>
        public int ColdSetpointNumber { get; set; } = 2;

        /// <summary> Parse setpoint number. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="temperature"> The temperature. </param>
        /// <returns> An Integer. </returns>
        public int ParseSetpointNumber( double temperature )
        {
            return temperature < this.AmbientTemperature
                ? this.ColdSetpointNumber
                : temperature > this.AmbientTemperature ? this.HotSetpointNumber : this.AmbientSetpointNumber;
        }

        /// <summary> Builds a command. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="setpoint"> The setpoint. </param>
        /// <returns> A String. </returns>
        public string BuildCommand( ThermalSetpoint setpoint )
        {
            if ( setpoint is null )
                throw new ArgumentNullException( nameof( setpoint ) );
            var builder = new System.Text.StringBuilder();
            string delimiter = "; ";
            _ = builder.AppendFormat( this.SetpointNumberCommandFormat, this.ParseSetpointNumber( setpoint.Temperature ) );
            _ = builder.Append( delimiter );
            _ = builder.AppendFormat( this.SetpointCommandFormat, setpoint.Temperature );
            _ = builder.Append( delimiter );
            // moded out -- must be issued separately.
            // Dim value As Double = 60 * setpoint.RampRate
            // If Me.LowRampRateRange.Contains(value) Then
            // builder.AppendFormat(Me.LowRampRateCommandFormat, value)
            // ElseIf Me.HighRampRateRange.Contains(value) Then
            // builder.AppendFormat(Me.HighRampRateCommandFormat, value)
            // Else
            // Throw New InvalidOperationException($"Ramp range {value} is outside both the low {Me.LowRampRateRange.ToString} and high {Me.HighRampRateRange.ToString} ranges")
            // End If
            // builder.Append(delimiter)
            _ = builder.AppendFormat( this.SoakTimeCommandFormat, Math.Max( this.SoakTimeRange.Min, Math.Min( this.SoakTimeRange.Max, setpoint.SoakSeconds ) ) );
            _ = builder.Append( delimiter );
            _ = builder.AppendFormat( this.SetpointWindowCommandFormat, Math.Max( this.SetpointWindowRange.Min, Math.Min( this.SetpointWindowRange.Max, setpoint.Window ) ) );
            return builder.ToString();
        }

        /// <summary> Applies the active thermal setpoint described by setpoint. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ApplyActiveThermalSetpoint()
        {
            this.Session.Execute( this.BuildCommand( this.ActiveThermalSetpoint ) );
            // must issue this separately.
            _ = this.ApplyRampRate( 60d * this.ActiveThermalSetpoint.RampRate );
            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Select active thermal setpoint. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) SelectActiveThermalSetpoint( int thermalProfileSetpointNumber )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( this.ThermalProfile() is null )
            {
                result = (false, "Thermo-Stream thermal profile not set");
            }
            else if ( this.ThermalProfile().Count == 0 )
            {
                result = (false, "Thermo-Stream thermal profile is empty");
            }
            else if ( thermalProfileSetpointNumber < 1 )
            {
                result = (false, $"Thermo-Stream thermal profile setpoint number {thermalProfileSetpointNumber} must be positive");
            }
            else if ( thermalProfileSetpointNumber > this.ThermalProfile().Count )
            {
                result = (false, $"Thermo-Stream thermal profile setpoint number {thermalProfileSetpointNumber} is out of range [1,{this.ThermalProfile().Count}]");
            }
            else
            {
                this.ActiveThermalSetpoint = this.ThermalProfile()[thermalProfileSetpointNumber];
            }

            return result;
        }

        /// <summary> Validates the thermal setpoint. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="setpoint"> The setpoint. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ValidateThermalSetpoint( ThermalSetpoint setpoint )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( setpoint is null )
                throw new ArgumentNullException( nameof( setpoint ) );
            _ = this.QuerySetpointNumber();
            if ( this.SetpointNumber.HasValue )
            {
                _ = this.QuerySetpoint();
                if ( this.Setpoint.HasValue )
                {
                    _ = this.QueryRampRate();
                    if ( this.RampRate.HasValue )
                    {
                        _ = this.QuerySoakTime();
                        if ( this.SoakTime.HasValue )
                        {
                            _ = this.QuerySetpointWindow();
                            if ( this.SetpointWindow.HasValue )
                            {
                                if ( this.SetpointNumber.Value == this.ParseSetpointNumber( setpoint.Temperature ) )
                                {
                                    if ( Math.Abs( this.Setpoint.Value - setpoint.Temperature ) < 0.1d )
                                    {
                                        if ( Math.Abs( this.RampRate.Value - 60d * setpoint.RampRate ) < 1.1d )
                                        {
                                            if ( Math.Abs( this.SoakTime.Value - setpoint.SoakSeconds ) < 1.1d )
                                            {
                                                if ( Math.Abs( this.SetpointWindow.Value - setpoint.Window ) < 0.1d )
                                                {
                                                }
                                                else
                                                {
                                                    result = (false, $"Thermo-Stream failed setting the setpoint window {this.SetpointWindow.Value} to {setpoint.Window} {Arebis.StandardUnits.TemperatureUnits.DegreeCelsius.Symbol}.");
                                                }
                                            }
                                            else
                                            {
                                                result = (false, $"Thermo-Stream failed setting the soak time {this.SoakTime.Value} to {setpoint.SoakSeconds} s.");
                                            }
                                        }
                                        else
                                        {
                                            result = (false, $"Thermo-Stream failed setting the ramp rate {this.RampRate.Value} to {60d * setpoint.RampRate} {this.RampRateUnit.Symbol}.");
                                        }
                                    }
                                    else
                                    {
                                        result = (false, $"Thermo-Stream failed setting the setpoint {this.Setpoint.Value} to {setpoint.Temperature} {Arebis.StandardUnits.TemperatureUnits.DegreeCelsius.Symbol}.");
                                    }
                                }
                                else
                                {
                                    result = (false, $"Thermo-Stream failed setting the setpoint number {this.SetpointNumber.Value} to {this.ParseSetpointNumber( setpoint.Temperature )}.");
                                }
                            }
                            else
                            {
                                result = (false, $"Thermo-Stream failed reading the setpoint window--value is empty.");
                            }
                        }
                        else
                        {
                            result = (false, $"Thermo-Stream failed reading the soak time--value is empty.");
                        }
                    }
                    else
                    {
                        result = (false, $"Thermo-Stream failed reading the ramp rate--value is empty.");
                    }
                }
                else
                {
                    result = (false, $"Thermo-Stream failed reading the setpoint--value is empty.");
                }
            }
            else
            {
                result = (false, $"Thermo-Stream failed reading the setpoint number--value is empty.");
            }

            Core.ApplianceBase.DoEvents();
            return result;
        }

        /// <summary> Conditional apply setpoint. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
        /// <param name="countdown">                    The countdown. </param>
        /// <param name="additionalInfo">               Information describing the additional. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ConditionalApplySetpoint( int thermalProfileSetpointNumber, int countdown, string additionalInfo )
        {
            if ( string.IsNullOrWhiteSpace( additionalInfo ) )
                additionalInfo = $"applying setpoint {thermalProfileSetpointNumber};";
            var result = this.SelectActiveThermalSetpoint( thermalProfileSetpointNumber );
            if ( result.Success )
            {
                this.ApplyActiveThermalSetpoint();
                do
                {
                    Core.ApplianceBase.DoEventsWait( this.CommandRefractoryTimeSpan );
                    result = this.ValidateThermalSetpoint( this.ThermalProfile()[thermalProfileSetpointNumber] );
                    countdown -= 1;
                }
                while ( !result.Success && countdown != 0 );
            }

            if ( !result.Success )
                result = (false, $"Thermo-Stream failed {additionalInfo};. Details: {result.Details}");
            return result;
        }

        /// <summary> Conditional move first. This assumes the head is still up. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
        /// <param name="countdown">                    The countdown. </param>
        /// <param name="additionalInfo">               Information describing the additional. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ConditionalMoveFirst1( int thermalProfileSetpointNumber, int countdown, string additionalInfo )
        {
            if ( string.IsNullOrWhiteSpace( additionalInfo ) )
                additionalInfo = "selecting first profile @ head up; clear to start";
            var (_, Success, Details) = this.ValidateHandlerHeadUp();
            return Success ? this.ConditionalApplySetpoint( thermalProfileSetpointNumber, countdown, additionalInfo ) : (false, $"Thermo-Stream is unable to start while {additionalInfo};. Details: {Details}");
        }

        /// <summary> Conditional move next. Expects the head to be down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="thermalProfileSetpointNumber"> The thermal profile setpoint number. </param>
        /// <param name="countdown">                    The countdown. </param>
        /// <param name="additionalInfo">               Information describing the additional. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ConditionalMoveNext1( int thermalProfileSetpointNumber, int countdown, string additionalInfo )
        {
            if ( string.IsNullOrWhiteSpace( additionalInfo ) )
                additionalInfo = "moving to next profile";
            var (_, Success, Details) = this.ValidateHandlerHeadDown();
            return Success ? this.ConditionalApplySetpoint( thermalProfileSetpointNumber, countdown, additionalInfo ) : (false, $"Thermo-Stream is unable to start while {additionalInfo};. Details: {Details}");
        }

        /// <summary> Query if 'setpointNumber' exceeds the profile count. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="setpointNumber"> The setpoint number. </param>
        /// <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
        public (bool Affirmative, bool Success, string Details) IsProfileCompleted( int setpointNumber )
        {
            return this.ThermalProfile() is null
                ? (false, false, $"Thermo-Stream thermal profile not set")
                : this.ThermalProfile().Count == 0
                    ? (false, false, $"Thermo-Stream thermal profile is empty")
                    : setpointNumber < 1
                                    ? (false, false, $"Thermo-Stream thermal profile setpoint number {setpointNumber} must be positive")
                                    : setpointNumber > this.ThermalProfile().Count ? (true, true, string.Empty) : (false, false, $"Unknown case");
        }
        #endregion

        #region " REGISTERS "

        #region " TEMPERATURE EVENT "

        /// <summary> Gets or sets the Temperature event condition query command. </summary>
        /// <value> The Temperature event condition query command. </value>
        protected override string TemperatureEventConditionQueryCommand { get; set; } = "TECR?";

        /// <summary> Gets or sets the temperature event enable mask command format. </summary>
        /// <value> The temperature event enable mask command format. </value>
        protected override string TemperatureEventEnableMaskCommandFormat { get; set; } = "TESE {0:D}";

        /// <summary> Gets or sets the temperature event enable mask query command. </summary>
        /// <value> The temperature event enable mask query command. </value>
        protected override string TemperatureEventEnableMaskQueryCommand { get; set; } = "TESE?";

        /// <summary> Gets or sets the Temperature Event status query command. </summary>
        /// <value> The Temperature status query command. </value>
        protected override string TemperatureEventStatusQueryCommand { get; set; } = "TESR?";

        /// <summary> Gets or sets the cached Temperature Event sentinel. </summary>
        /// <value>
        /// <c>null</c> if Temperature Event is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public override int? TemperatureEventStatus
        {
            get => base.TemperatureEventStatus;

            protected set {
                if ( !Equals( this.TemperatureEventStatus, value ) )
                {
                    base.TemperatureEventStatus = value;
                    if ( value.GetValueOrDefault( 0 ) > 0 )
                    {
                        // update the event only if having a new value -- this way the event info gets latched.
                        this.TemperatureEventInfo = new TemperatureEventInfo( ( TemperatureEvents ) Conversions.ToInteger( value.Value ) );
                    }
                }
            }
        }

        /// <summary> Information describing the temperature event. </summary>
        private TemperatureEventInfo _TemperatureEventInfo;

        /// <summary> Gets or sets information describing the temperature event. </summary>
        /// <value> Information describing the temperature event. </value>
        public TemperatureEventInfo TemperatureEventInfo
        {
            get => this._TemperatureEventInfo;

            protected set {
                if ( value is object && this.TemperatureEventInfo.TemperatureEvent != value.TemperatureEvent )
                {
                    this._TemperatureEventInfo = value;
                    this.IsAtTemperature = value.IsAtTemperature();
                    this.IsNotAtTemperature = value.IsNotAtTemperature();
                    this.IsCycleCompleted = value.IsEndOfCycle();
                    this.IsCyclesCompleted = value.IsEndOfCycles();
                    this.IsCyclingStopped = value.IsCyclingStopped();
                    this.IsTestTimeElapsed = value.IsTestTimeElapsed();
                }
            }
        }

        /// <summary> True if is cycle completed, false if not. </summary>
        private bool _IsCycleCompleted;

        /// <summary> Gets or sets a value indicating whether one cycle completed. </summary>
        /// <value> <c>true</c> if one cycle completed; otherwise <c>false</c> </value>
        public bool IsCycleCompleted
        {
            get => this._IsCycleCompleted;

            set {
                if ( value != this.IsCycleCompleted )
                {
                    this._IsCycleCompleted = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is cycles completed, false if not. </summary>
        private bool _IsCyclesCompleted;

        /// <summary> Gets or sets a value indicating whether all Cycles completed. </summary>
        /// <value> <c>true</c> if all Cycles completed; otherwise <c>false</c> </value>
        public bool IsCyclesCompleted
        {
            get => this._IsCyclesCompleted;

            set {
                if ( value != this.IsCyclesCompleted )
                {
                    this._IsCyclesCompleted = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is test time elapsed, false if not. </summary>
        private bool _IsTestTimeElapsed;

        /// <summary> Gets or sets a value indicating whether test time elapsed. </summary>
        /// <value> <c>true</c> if this cycling Stopped; otherwise <c>false</c> </value>
        public bool IsTestTimeElapsed
        {
            get => this._IsTestTimeElapsed;

            set {
                if ( value != this.IsTestTimeElapsed )
                {
                    this._IsTestTimeElapsed = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is cycling stopped, false if not. </summary>
        private bool _IsCyclingStopped;

        /// <summary> Gets or sets a value indicating whether cycling stopped. </summary>
        /// <value> <c>true</c> if this cycling Stopped; otherwise <c>false</c> </value>
        public bool IsCyclingStopped
        {
            get => this._IsCyclingStopped;

            set {
                if ( value != this.IsCyclingStopped )
                {
                    this._IsCyclingStopped = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is at temperature, false if not. </summary>
        private bool _IsAtTemperature;

        /// <summary> Gets or sets a value indicating whether this object is at temperature. </summary>
        /// <remarks>
        /// Use <see cref="IsNotAtTemperature">not at temp</see> as this is on when the instrument turns
        /// on.
        /// </remarks>
        /// <value> <c>true</c> if this object is at temperature; otherwise <c>false</c> </value>
        public bool IsAtTemperature
        {
            get => this._IsAtTemperature;

            set {
                if ( value != this.IsAtTemperature )
                {
                    this._IsAtTemperature = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True if is not at temperature, false if not. </summary>
        private bool _IsNotAtTemperature;

        /// <summary> Gets or sets a value indicating whether this object is not at temperature. </summary>
        /// <value> <c>true</c> if this object is not at temperature; otherwise <c>false</c> </value>
        public bool IsNotAtTemperature
        {
            get => this._IsNotAtTemperature;

            set {
                if ( value != this.IsNotAtTemperature )
                {
                    this._IsNotAtTemperature = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Query if cycles stopped or terminated; Otherwise, queries if reached setpoint.
        /// </summary>
        /// <remarks> This is the same as indicating that the start of test signal was received. </remarks>
        /// <returns> The cycling at temperature. </returns>
        public (bool Affirmative, bool Success, string Details) QueryCyclingAtTemperature()
        {
            (bool Affirmative, bool Success, string Details) result;
            _ = this.QueryTemperatureEventStatus();
            if ( this.IsCycleCompleted )
            {
                result = (false, false, "Thermo-Stream cycle completed while waiting for At-Temp");
            }
            else if ( UseCyclesCompleted && this.IsCyclesCompleted )
            {
                result = (false, false, "Thermo-Stream completed all cycles while waiting for At-Temp");
            }
            else if ( this.IsCyclingStopped )
            {
                result = (false, false, "Thermo-Stream cycle was stopped while waiting for At-Temp");
            }
            else
            {
                result = this.ValidateHandlerHeadDown();
                if ( !result.Success )
                {
                    result = (false, false, $"Thermo-Stream is unable to wait for At-Temp;. Details: {result.Details}");
                }
                else
                {
                    // problem is: at temp may falsely show at temp.
                    result = (this.IsAtTemperature && !this.IsNotAtTemperature, true, string.Empty);
                }
            }

            return result;
        }

        /// <summary>
        /// Query if cycles stopped or terminated; Otherwise, queries if reached setpoint.
        /// </summary>
        /// <remarks> This is the same as indicating that the start of test signal was received. </remarks>
        /// <returns> at temperature. </returns>
        public (bool Affirmative, bool Success, string Details) QueryAtTemperature()
        {
            var result = this.ValidateHandlerHeadDown();
            if ( !result.Success )
            {
                result = (false, false, $"Thermo-Stream is unable to wait for At-Temp;. Details: {result.Details}");
            }
            else
            {
                _ = this.QueryTemperatureEventStatus();
                // problem is: at temp may falsely show at temp.
                result = (this.IsAtTemperature && !this.IsNotAtTemperature, true, string.Empty);
            }

            return result;
        }

        /// <summary> Queries head down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> The cycling head down. </returns>
        public (bool Affirmative, bool Success, string Details) QueryCyclingHeadDown()
        {
            _ = this.QueryTemperatureEventStatus();
            return this.IsCycleCompleted
                ? (false, false, "Thermo-Stream cycle completed while waiting for head down")
                : UseCyclesCompleted && this.IsCyclesCompleted
                    ? (false, false, "Thermo-Stream completed all cycles while waiting for head down")
                    : this.IsCyclingStopped ? (false, false, "Thermo-Stream cycle was stopped while waiting for head down") : this.QueryHeadDown();
        }

        /// <summary> Queries head down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> The head down. </returns>
        public new(bool Affirmative, bool Success, string Details) QueryHeadDown()
        {
            _ = this.QueryAuxiliaryEventStatus();
            return !this.IsReady ? (false, false, $"Thermo-Stream is no longer ready while waiting for head down;. Ready={( int ) AuxiliaryStatuses.ReadyStartup}.and.{( int ) this.AuxiliaryStatusInfo.AuxiliaryStatus}") : (!this.IsHeadUp, true, string.Empty);
        }

        /// <summary> Queries head down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> The head up. </returns>
        public (bool Affirmative, bool Success, string Details) QueryHeadUp()
        {
            _ = this.QueryAuxiliaryEventStatus();
            return !this.IsReady ? (false, false, $"Thermo-Stream is no longer ready while waiting for head up;. Ready={( int ) AuxiliaryStatuses.ReadyStartup}.and.{( int ) this.AuxiliaryStatusInfo.AuxiliaryStatus}") : (this.IsHeadUp, true, string.Empty);
        }

        /// <summary> Check if the handler is ready and cycle not completed and head is down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
        public (bool Affirmative, bool Success, string Details) ValidateCyclingHandlerReady()
        {
            // check if handler is ready and head is up
            var result = this.ValidateHandlerHeadUp();
            if ( result.Success )
            {
                _ = this.QueryTemperatureEventStatus();
                if ( !this.IsCycleCompleted )
                {
                    result = (false, false, "Thermo-Stream seems busy--cycle not completed--stop and try again");
                }
                else if ( UseCyclesCompleted && !this.IsCyclesCompleted )
                {
                    result = (false, false, "Thermo-Stream seems busy--cycles not all completed--stop and try again");
                }
            }

            return result;
        }

        /// <summary> Check if the handler is ready and cycle not completed and head is down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
        public (bool Affirmative, bool Success, string Details) ValidateHandlerHeadUp()
        {
            (bool Affirmative, bool Success, string Details) result = (true, true, string.Empty);
            // check if handler is ready
            _ = this.QueryAuxiliaryEventStatus();
            if ( !this.IsReady )
            {
                result = (false, false, $"Thermo-Stream is not ready--wait for startup operation to complete and try again;. Ready={( int ) AuxiliaryStatuses.ReadyStartup}.and.{( int ) this.AuxiliaryStatusInfo.AuxiliaryStatus}");
            }
            // check if head is up.
            else if ( !this.IsHeadUp )
            {
                result = (false, false, $"Thermo-Stream head is down;. Up={( int ) AuxiliaryStatuses.HeadUpDown}.and.{( int ) this.AuxiliaryStatusInfo.AuxiliaryStatus}");
            }

            return result;
        }

        /// <summary> Check if the handler is ready and cycle not completed and head is down. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> A (Affirmative As Boolean, Success As Boolean, Details As String) </returns>
        public (bool Affirmative, bool Success, string Details) ValidateHandlerHeadDown()
        {
            (bool Affirmative, bool Success, string Details) result = (true, true, string.Empty);
            // check if handler is ready
            _ = this.QueryAuxiliaryEventStatus();
            if ( !this.IsReady )
            {
                result = (false, false, $"Thermo-Stream is not ready--wait for startup operation to complete and try again;. Ready={( int ) AuxiliaryStatuses.ReadyStartup}.and.{( int ) this.AuxiliaryStatusInfo.AuxiliaryStatus}");
            }
            else if ( this.IsHeadUp )
            {
                result = (false, false, $"Thermo-Stream head is up;. Up={( int ) AuxiliaryStatuses.HeadUpDown}.and.{( int ) this.AuxiliaryStatusInfo.AuxiliaryStatus}");
            }

            return result;
        }

        /// <summary> Conditional reset operator mode. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ConditionalResetOperatorMode( int repeatCount )
        {
            (bool Success, string Details) result = (true, string.Empty);
            _ = this.QuerySystemScreen();
            if ( !this.OperatorScreen.GetValueOrDefault( false ) )
            {
                result = this.ResetOperatorMode( repeatCount );
            }

            return result;
        }

        /// <summary>
        /// Check if the handler is ready and head is up and switch to operator screen mode then wait
        /// tile mode established.
        /// </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="repeatCount"> Number of repeats. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public (bool Success, string Details) ResetOperatorMode( int repeatCount )
        {
            var (_, Success, Details) = this.ValidateHandlerHeadUp();
            if ( Success )
            {
                this.ResetOperatorScreen();
                Core.ApplianceBase.DoEventsWait( this.CommandRefractoryTimeSpan );
                bool opMode;
                do
                {
                    _ = this.QuerySystemScreen();
                    opMode = this.OperatorScreen.GetValueOrDefault( false );
                    if ( !opMode )
                    {
                        Core.ApplianceBase.DoEventsWait( this.CommandRefractoryTimeSpan );
                        repeatCount -= 1;
                    }
                }
                while ( !opMode && repeatCount != 0 );
                return opMode ? (true, string.Empty) : (false, $"Thermo-Stream seems failed switching to operator mode--stop and try again");
            }
            else
            {
                return (false, $"Unable to reset operator mode;. Details: {Details}");
            }
        }

        #endregion

        #region " AUXILIARY EVENT "

        /// <summary> Gets or sets the Auxiliary Event status query command. </summary>
        /// <value> The Auxiliary status query command. </value>
        protected override string AuxiliaryEventStatusQueryCommand { get; set; } = "AUXC?";

        /// <summary> Gets or sets the cached Auxiliary Event sentinel. </summary>
        /// <value>
        /// <c>null</c> if Auxiliary Event is not known; <c>True</c> if output is on; otherwise,
        /// <c>False</c>.
        /// </value>
        public override int? AuxiliaryEventStatus
        {
            get => base.AuxiliaryEventStatus;

            protected set {
                if ( !Equals( this.AuxiliaryEventStatus, value ) )
                {
                    base.AuxiliaryEventStatus = value;
                    this.AuxiliaryStatusInfo = new AuxiliaryStatusInfo( ( AuxiliaryStatuses ) Conversions.ToInteger( value.Value ) );
                    if ( value.GetValueOrDefault( 0 ) > 0 )
                    {
                        // update the event only if having a new value -- this way the event info gets latched.
                    }
                }
            }
        }

        /// <summary> Information describing the auxiliary status. </summary>
        private AuxiliaryStatusInfo _AuxiliaryStatusInfo;

        /// <summary> Gets or sets information describing the Auxiliary event. </summary>
        /// <value> Information describing the Auxiliary event. </value>
        public AuxiliaryStatusInfo AuxiliaryStatusInfo
        {
            get => this._AuxiliaryStatusInfo;

            protected set {
                if ( value is object && this.AuxiliaryStatusInfo.AuxiliaryStatus != value.AuxiliaryStatus )
                {
                    this._AuxiliaryStatusInfo = value;
                    this.IsDutControl = value.IsDutControl();
                    this.IsFlowOn = value.IsFlowOn();
                    this.IsHeadUp = value.IsHeadUp();
                    this.IsCyclingStopped = value.IsHeatOnlyMode();
                    this.IsManualMode = value.IsManualMode();
                    this.IsRampMode = value.IsRampMode();
                    this.IsReady = value.IsReady();
                }
            }
        }

        /// <summary> True if is ramp mode, false if not. </summary>
        private bool _IsRampMode;

        /// <summary> Gets or sets a value indicating whether the instrument is in ramp mode. </summary>
        /// <value> <c>true</c> if in ramp mode; otherwise <c>false</c> </value>
        public bool IsRampMode
        {
            get => this._IsRampMode;

            set {
                if ( value != this.IsRampMode )
                {
                    this._IsRampMode = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is ready, false if not. </summary>
        private bool _IsReady;

        /// <summary>
        /// Gets or sets a value indicating whether the instrument is ready for operation or in startup.
        /// </summary>
        /// <value>
        /// <c>true</c> if instrument is ready for operation or in startup; otherwise <c>false</c>
        /// </value>
        public bool IsReady
        {
            get => this._IsReady;

            set {
                if ( value != this.IsReady )
                {
                    this._IsReady = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is head up, false if not. </summary>
        private bool _IsHeadUp;

        /// <summary> Gets or sets a value indicating whether head is up. </summary>
        /// <value> <c>true</c> if head is up; otherwise <c>false</c> </value>
        public bool IsHeadUp
        {
            get => this._IsHeadUp;

            set {
                if ( value != this.IsHeadUp )
                {
                    this._IsHeadUp = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is manual mode, false if not. </summary>
        private bool _IsManualMode;

        /// <summary> Gets or sets a value indicating whether manual mode is on. </summary>
        /// <value> <c>true</c> if manual mode is on; otherwise <c>false</c> </value>
        public bool IsManualMode
        {
            get => this._IsManualMode;

            set {
                if ( value != this.IsManualMode )
                {
                    this._IsManualMode = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is heat only mode, false if not. </summary>
        private bool _IsHeatOnlyMode;

        /// <summary> Gets or sets a value indicating whether heat-only mode. </summary>
        /// <value> <c>true</c> if this heat-only mode; otherwise <c>false</c> </value>
        public bool IsHeatOnlyMode
        {
            get => this._IsHeatOnlyMode;

            set {
                if ( value != this.IsHeatOnlyMode )
                {
                    this._IsHeatOnlyMode = value;
                    this.NotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        /// <summary> True if is dut control, false if not. </summary>
        private bool _IsDutControl;

        /// <summary> Gets or sets a value indicating whether dut control is used. </summary>
        /// <value> <c>true</c> if dut control is used; otherwise <c>false</c> </value>
        public bool IsDutControl
        {
            get => this._IsDutControl;

            set {
                if ( value != this.IsDutControl )
                {
                    this._IsDutControl = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> True if is flow on, false if not. </summary>
        private bool _IsFlowOn;

        /// <summary> Gets or sets a value indicating whether flow is on. </summary>
        /// <value> <c>true</c> if flow is on; otherwise <c>false</c> </value>
        public bool IsFlowOn
        {
            get => this._IsFlowOn;

            set {
                if ( value != this.IsFlowOn )
                {
                    this._IsFlowOn = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

    }
}
