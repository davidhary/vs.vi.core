﻿using System;
using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.Ats600
{

    /// <summary> Defines a Status Subsystem for a InTest Thermo Stream instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( session )
        {
            this.VersionInfo = new VersionInfo();
            InitializeSession( session );
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion


        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.ClearExecutionStateCommand = Pith.Ieee488.Syntax.ClearExecutionStateCommand;
            session.ResetKnownStateCommand = string.Empty;
            session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            session.OperationCompletedQueryCommand = string.Empty;
            session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
            session.StandardEventStatusQueryCommand = Pith.Ieee488.Syntax.StandardEventStatusQueryCommand;
            session.StandardEventEnableQueryCommand = Pith.Ieee488.Syntax.StandardEventEnableQueryCommand;
            session.StandardServiceEnableCommandFormat = Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat;
            // complete not supported (?)
            session.StandardServiceEnableCompleteCommandFormat = Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat;
            session.ServiceRequestEnableCommandFormat = Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat;
            session.ServiceRequestEnableQueryCommand = Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand;
            session.WaitCommand = Pith.Ieee488.Syntax.WaitCommand;
            session.OperationCompleted = true;
        }

        #endregion

        #region " MEASUREMENT REGISTER EVENTS "

        /// <summary> Gets the measurement status query command. </summary>
        /// <value> The measurement status query command. </value>
        protected override string MeasurementStatusQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the measurement event condition query command. </summary>
        /// <value> The measurement event condition query command. </value>
        protected override string MeasurementEventConditionQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " OPERATION REGISTER EVENTS "

        /// <summary> Gets the operation event enable Query command. </summary>
        /// <value> The operation event enable Query command. </value>
        protected override string OperationEventEnableQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the operation event enable command format. </summary>
        /// <value> The operation event enable command format. </value>
        protected override string OperationEventEnableCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets the operation event status query command. </summary>
        /// <value> The operation event status query command. </value>
        protected override string OperationEventStatusQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " QUESTIONABLE REGISTER "

        /// <summary> Gets the questionable status query command. </summary>
        /// <value> The questionable status query command. </value>
        protected override string QuestionableStatusQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " IDENTITY "

        /// <summary> Gets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = Pith.Ieee488.Syntax.IdentityQueryCommand;

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.PublishVerbose( "Trying to read identity;. " );
                Core.ApplianceBase.DoEvents();
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.PublishVerbose( $"Setting identity to {value};. " );
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets the clear error queue command. </summary>
        /// <remarks>
        /// This supposed to use 'CLER' and requires to wait 4 seconds after the error is cleared!
        /// </remarks>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = string.Empty;

        /// <summary> Gets the error queue query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets the 'Next Error' query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string DequeueErrorQueryCommand { get; set; } = string.Empty;

        /// <summary> Clears the error queue and waits. </summary>
        /// <remarks> Uses 'CLER' and wait 4 seconds after the error is cleared. </remarks>
        public override void ClearErrorQueue()
        {
            base.ClearErrorQueue();
            this.Session.Execute( "CLER" );
            if ( this.Session.IsSessionOpen )
            {
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 4000d ) );
            }
        }

        /// <summary> Gets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected override string DeviceErrorQueryCommand
        {
            get => Debugger.IsAttached ? Pith.Scpi.Syntax.LastSystemErrorQueryCommand : string.Empty;

            set {
            }
        }

        #endregion

        #region " LINE FREQUENCY "

        /// <summary> Gets or sets line frequency query command. </summary>
        /// <value> The line frequency query command. </value>
        protected override string LineFrequencyQueryCommand { get; set; } = string.Empty;

        #endregion

    }
}