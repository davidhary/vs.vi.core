using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600
{

    /// <summary> Defines a System Subsystem for a InTest Thermo Stream instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.CoolingEnabled = new bool?();
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the initialize memory command. </summary>
        /// <value> The initialize memory command. </value>
        protected override string InitializeMemoryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the language revision query command. </summary>
        /// <value> The language revision query command. </value>
        protected override string LanguageRevisionQueryCommand { get; set; } = string.Empty;


        #endregion

        #region " SCPI VERSION "

        /// <summary> The automatic tuning enabled. </summary>
        private bool? _AutoTuningEnabled;

        /// <summary>
        /// Gets or sets the cached version level of the SCPI standard implemented by the device.
        /// </summary>
        /// <value> The Auto Tuning Enabled. </value>
        public bool? AutoTuningEnabled
        {
            get => this._AutoTuningEnabled;

            protected set {
                if ( !Equals( this.AutoTuningEnabled, value ) )
                {
                    this._AutoTuningEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the AutoTuning enabled state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> If set to <c>True</c> enable. </param>
        /// <returns>
        /// <c>True</c> if AutoTuning is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? ApplyAutoTuningEnabled( bool value )
        {
            _ = this.WriteAutoTuningEnabled( value );
            return this.QueryAutoTuningEnabled();
        }

        /// <summary> Queries the AutoTuning enabled state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns>
        /// <c>True</c> if AutoTuning is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? QueryAutoTuningEnabled()
        {
            this.AutoTuningEnabled = this.Session.Query( this.AutoTuningEnabled.GetValueOrDefault( true ), "LRNM?" );
            return this.AutoTuningEnabled;
        }

        /// <summary> Writes the AutoTuning enabled state without reading back the actual value. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> if set to <c>True</c> [value]. </param>
        /// <returns>
        /// <c>True</c> if AutoTuning is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? WriteAutoTuningEnabled( bool value )
        {
            _ = this.Session.WriteLine( "LRNM {0:'1';'1';'0'}", ( object ) Conversions.ToInteger( value ) );
            this.AutoTuningEnabled = value;
            return this.AutoTuningEnabled;
        }

        #endregion

        #region " COOLING ENABLED "

        /// <summary> The cooling enabled. </summary>
        private bool? _CoolingEnabled;

        /// <summary> Gets or sets a cached value indicating whether Cooling is enabled. </summary>
        /// <value>
        /// <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
        /// </value>
        public bool? CoolingEnabled
        {
            get => this._CoolingEnabled;

            protected set {
                if ( !Equals( this.CoolingEnabled, value ) )
                {
                    this._CoolingEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Cooling enabled state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> If set to <c>True</c> enable. </param>
        /// <returns>
        /// <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? ApplyCoolingEnabled( bool value )
        {
            _ = this.WriteCoolingEnabled( value );
            return this.QueryCoolingEnabled();
        }

        /// <summary> Queries the Cooling enabled state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns>
        /// <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? QueryCoolingEnabled()
        {
            this.CoolingEnabled = this.Session.Query( this.CoolingEnabled.GetValueOrDefault( true ), "COOL?" );
            return this.CoolingEnabled;
        }

        /// <summary> Writes the Cooling enabled state without reading back the actual value. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> if set to <c>True</c> [value]. </param>
        /// <returns>
        /// <c>True</c> if Cooling is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? WriteCoolingEnabled( bool value )
        {
            _ = this.Session.WriteLine( "COOL {0:'1';'1';'0'}", ( object ) Conversions.ToInteger( value ) );
            this.CoolingEnabled = value;
            return this.CoolingEnabled;
        }

        #endregion

        #region " DEVICE CONTROL ENABLED "

        /// <summary> The device control enabled. </summary>
        private bool? _DeviceControlEnabled;

        /// <summary> Gets or sets a value indicating whether Device Control is enabled. </summary>
        /// <value> <c>True</c> if Device Control mode is enabled; otherwise, <c>False</c>. </value>
        public bool? DeviceControlEnabled
        {
            get => this._DeviceControlEnabled;

            protected set {
                if ( !Equals( this.DeviceControlEnabled, value ) )
                {
                    this._DeviceControlEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Device Control enabled state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> If set to <c>True</c> enable. </param>
        /// <returns>
        /// <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? ApplyDeviceControlEnabled( bool value )
        {
            _ = this.WriteDeviceControlEnabled( value );
            return this.QueryDeviceControlEnabled();
        }

        /// <summary> Queries the Device Control enabled state. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns>
        /// <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? QueryDeviceControlEnabled()
        {
            this.DeviceControlEnabled = this.Session.Query( this.DeviceControlEnabled.GetValueOrDefault( true ), "DUTM?" );
            return this.DeviceControlEnabled;
        }

        /// <summary>
        /// Writes the Device Control enabled state without reading back the actual value.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> if set to <c>True</c> [value]. </param>
        /// <returns>
        /// <c>True</c> if Device Control is enabled; <c>False</c> if not or none if not set or unknown.
        /// </returns>
        public bool? WriteDeviceControlEnabled( bool value )
        {
            this.DeviceControlEnabled = new bool?();
            _ = this.Session.WriteLine( "DUTM {0:'1';'1';'0'}", ( object ) Conversions.ToInteger( value ) );
            this.DeviceControlEnabled = value;
            return this.DeviceControlEnabled;
        }

        #endregion

    }
}
