using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600
{

    /// <summary> Implements a InTest Thermo Stream instrument device. </summary>
    /// <remarks>
    /// An instrument is defined, for the purpose of this library, as a device with a front panel.
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class Ats600Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Ats600Device" /> class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public Ats600Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected Ats600Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            Properties.Settings.Default.PropertyChanged += this.MySettings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A Device. </returns>
        public static Ats600Device Create()
        {
            Ats600Device device = null;
            try
            {
                device = new Ats600Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static Ats600Device Validated( Ats600Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( Ats600Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( null );
                this.BindThermalStreamSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            // this device uses new line termination.
            this.Session.UseNewLineTermination();
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.BindThermalStreamSubsystem( new ThermalStreamSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Gets or sets the Ats600 Thermal Stream Subsystem. </summary>
        /// <value> The Ats600 Thermal Stream Subsystem. </value>
        public ThermalStreamSubsystem ThermalStreamSubsystem { get; private set; }

        /// <summary> Binds the Ats600 Thermal Stream subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem"> The Thermal Stream subsystem. </param>
        private void BindThermalStreamSubsystem( ThermalStreamSubsystem subsystem )
        {
            if ( this.ThermalStreamSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.ThermalStreamSubsystem );
                this.ThermalStreamSubsystem = null;
            }

            this.ThermalStreamSubsystem = subsystem;
            if ( this.ThermalStreamSubsystem is object )
            {
                this.Subsystems.Add( this.ThermalStreamSubsystem );
            }
        }

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "Ats600 Settings Editor", Properties.Settings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        protected override void ApplySettings()
        {
            var settings = Properties.Settings.Default;
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( Properties.Settings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Properties.Settings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( Properties.Settings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) Properties.Settings.Default.ReadDelay );
                        break;
                    }

                case nameof( Properties.Settings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) Properties.Settings.Default.StatusReadDelay );
                        break;
                    }

                case nameof( Properties.Settings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = Properties.Settings.Default.StatusReadTurnaroundTime;
                        break;
                    }

                case nameof( Properties.Settings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case var @case when @case == nameof( Properties.Settings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case var case1 when case1 == nameof( Properties.Settings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) Properties.Settings.Default.ReadDelay );
                        break;
                    }

                case var case2 when case2 == nameof( Properties.Settings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) Properties.Settings.Default.StatusReadDelay );
                        break;
                    }

                case var case3 when case3 == nameof( Properties.Settings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = Properties.Settings.Default.StatusReadTurnaroundTime;
                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( Properties.Settings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as Properties.Settings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
