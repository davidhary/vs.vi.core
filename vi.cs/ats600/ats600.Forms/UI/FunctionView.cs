using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic;

namespace isr.VI.Ats600.Forms
{

    /// <summary> A Function view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class FunctionView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public FunctionView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__QueryStatusButton.Name = "_QueryStatusButton";
            this.__ResetCycleModeButton.Name = "_ResetCycleModeButton";
            this.__StartCycleButton.Name = "_StartCycleButton";
            this.__NextSetpointButton.Name = "_NextSetpointButton";
            this.__StopCycleButton.Name = "_StopCycleButton";
            this.__FindLastSetpointButton1.Name = "_FindLastSetpointButton1";
            this.__ResetOperatorModeButton.Name = "_ResetOperatorModeButton";
            this.__SetpointNumberNumeric.Name = "_SetpointNumberNumeric";
            this.__CycleCountNumeric.Name = "_CycleCountNumeric";
            this.__MaxTestTimeNumeric.Name = "_MaxTestTimeNumeric";
            this.__ReadSetpointButton.Name = "_ReadSetpointButton";
        }

        /// <summary> Creates a new <see cref="FunctionView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="FunctionView"/>. </returns>
        public static FunctionView Create()
        {
            FunctionView view = null;
            try
            {
                view = new FunctionView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( default );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Ats600Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( Ats600Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindThermalStreamSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( Ats600Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " THERMO STREAM "

        /// <summary> Gets or sets the Thermal Stream subsystem. </summary>
        /// <value> The Thermal Stream subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ThermalStreamSubsystem ThermalStreamSubsystem { get; private set; }

        /// <summary> Bind Thermal Stream subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindThermalStreamSubsystem( isr.VI.Ats600.Ats600Device device )
        {
            if ( this.ThermalStreamSubsystem is object )
            {
                this.BindSubsystem( false, this.ThermalStreamSubsystem );
                this.ThermalStreamSubsystem = null;
            }

            if ( device is object )
            {
                this.ThermalStreamSubsystem = device.ThermalStreamSubsystem;
                this.BindSubsystem( true, this.ThermalStreamSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ThermalStreamSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ThermalStreamSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsHeadUp ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsHeadUp ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsReady ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.CycleCount ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.MaximumTestTime ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.RampRate ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.SetpointNumber ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.Setpoint ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.SetpointWindow ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.SoakTime ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsAtTemperature ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsCycleCompleted ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsCyclesCompleted ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsCyclingStopped ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsTestTimeElapsed ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.IsNotAtTemperature ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ThermalStreamSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ThermalStreamSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ats600.ThermalStreamSubsystem.IsHeadUp ):
                    {
                        this._HeadDownCheckBox.Checked = !subsystem.IsHeadUp;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsReady ):
                    {
                        this._ReadyCheckBox.Checked = subsystem.IsReady;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.CycleCount ):
                    {
                        this._CycleCountNumeric.ValueSetter( subsystem.CycleCount );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.MaximumTestTime ):
                    {
                        this._MaxTestTimeNumeric.ValueSetter( ( decimal? ) subsystem.MaximumTestTime );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.RampRate ):
                    {
                        this._RampRateNumeric.ValueSetter( ( decimal? ) subsystem.RampRate );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.SetpointNumber ):
                    {
                        this._SetpointNumberNumeric.ValueSetter( subsystem.SetpointNumber );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.Setpoint ):
                    {
                        this._SetpointNumeric.ValueSetter( ( decimal? ) subsystem.Setpoint );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.SetpointWindow ):
                    {
                        this._SetpointWindowNumeric.ValueSetter( ( decimal? ) subsystem.SetpointWindow );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.SoakTime ):
                    {
                        this._SoakTimeNumeric.ValueSetter( ( decimal? ) subsystem.SoakTime );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsAtTemperature ):
                    {
                        this._AtTempCheckBox.Checked = subsystem.IsAtTemperature;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsCycleCompleted ):
                    {
                        this._CycleCompletedCheckBox1.CheckBoxControl.Checked = subsystem.IsCycleCompleted;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsCyclesCompleted ):
                    {
                        this._CyclesCompletedCheckBox1.CheckBoxControl.Checked = subsystem.IsCyclesCompleted;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsCyclingStopped ):
                    {
                        this._CyclingStoppedCheckBox.CheckBoxControl.Checked = subsystem.IsCyclingStopped;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsTestTimeElapsed ):
                    {
                        this._TestTimeElapsedCheckBox.Checked = subsystem.IsTestTimeElapsed;
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.IsNotAtTemperature ):
                    {
                        this._NotAtTempCheckBox.Checked = subsystem.IsNotAtTemperature;
                        break;
                    }
            }
        }

        /// <summary> Thermal Stream subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalStreamSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ThermalStreamSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ThermalStreamSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ThermalStreamSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: CYCLE PANEL "

        /// <summary> Searches for the last setpoint button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FindLastSetpointButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} finding last setpoint";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    this._FindLastSetpointButton1.Text = $"Find Last Setpoint: {this.Device.ThermalStreamSubsystem.FindLastSetpointNumber()}";
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Queries temporary events. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void QueryTempEvents( object sender )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading temperature events";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    _ = this.Device.ThermalStreamSubsystem.QueryTemperatureEventStatus();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Queries auxiliary status. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void QueryAuxiliaryStatus( object sender )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading auxiliary event status";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    _ = this.Device.ThermalStreamSubsystem.QueryAuxiliaryEventStatus();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by QueryTempEventButton for click events. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void QueryTempEventButton_Click( object sender, EventArgs e )
        {
            this.QueryTempEvents( sender );
        }

        /// <summary> Queries auxiliary status button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void QueryAuxiliaryStatusButton_Click( object sender, EventArgs e )
        {
            this.QueryAuxiliaryStatus( sender );
            this.QueryTempEvents( sender );
        }

        /// <summary> Setpoint number numeric value selected. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SetpointNumberNumeric_ValueSelected( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} selecting setpoint";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    if ( this._SetpointNumberNumeric.SelectedValue.HasValue )
                    {
                        _ = this.Device.ThermalStreamSubsystem.WriteSetpointNumber( ( int ) Math.Round( this._SetpointNumberNumeric.Value ) );
                        Application.DoEvents();
                    }

                    _ = this.Device.ThermalStreamSubsystem.QuerySetpointNumber();
                    Application.DoEvents();
                    this.Device.ThermalStreamSubsystem.ReadCurrentSetpointValues();
                    Application.DoEvents();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Queries head status. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void QueryHeadStatus( object sender )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading head status";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    _ = this.Device.ThermalStreamSubsystem.QueryHeadDown();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads set point button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadSetpointButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading the setpoint values";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    this.Device.ThermalStreamSubsystem.ReadCurrentSetpointValues();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Next button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void NextButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} moving to the next setpoint";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    this.Device.ThermalStreamSubsystem.NextSetpoint();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Maximum test time numeric value selected. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MaxTestTimeNumeric_ValueSelected( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} setting and reading the maximum test time";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    if ( this._MaxTestTimeNumeric.SelectedValue.HasValue )
                    {
                        _ = this.Device.ThermalStreamSubsystem.WriteMaximumTestTime( ( double ) this._MaxTestTimeNumeric.SelectedValue.Value );
                    }

                    _ = this.Device.ThermalStreamSubsystem.QueryMaximumTestTime();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Cycle count numeric value selected. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CycleCountNumeric_ValueSelected( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} setting and reading the cycle count";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) && this._CycleCountNumeric.SelectedValue.HasValue )
                {
                    _ = this.Device.ThermalStreamSubsystem.ApplyCycleCount( ( int ) Math.Round( this._CycleCountNumeric.SelectedValue.Value ) );
                    _ = this.Device.ThermalStreamSubsystem.QueryCycleCount();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Starts button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StartButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} starting cycling";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    this.Device.ThermalStreamSubsystem.StartCycling();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Stops cycle button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StopCycleButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} stopping cycling";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    this.Device.ThermalStreamSubsystem.StopCycling();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Resets the system mode. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="resetOperator"> True to reset operator. </param>
        /// <param name="sender">        Source of the event. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ResetSystemMode( bool resetOperator, object sender )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} resetting system mode";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    var refrectoryPeriod = TimeSpan.Zero;
                    if ( resetOperator )
                    {
                        refrectoryPeriod = this.Device.ThermalStreamSubsystem.ResetOperatorScreenRefractoryTimeSpan;
                        this.Device.ThermalStreamSubsystem.ResetOperatorScreen();
                    }
                    else
                    {
                        refrectoryPeriod = this.Device.ThermalStreamSubsystem.ResetCycleScreenRefractoryTimeSpan;
                        this.Device.ThermalStreamSubsystem.ResetCycleScreen();
                    }

                    _ = this.PublishInfo( "Awaiting reset;. " );
                    if ( this.Device.IsSessionOpen )
                    {
                        Core.ApplianceBase.DoEventsWait( refrectoryPeriod );
                    }

                    _ = this.Device.ThermalStreamSubsystem.QuerySystemScreen();
                    this._ResetOperatorModeButton.Text = this.Device.ThermalStreamSubsystem.OperatorScreen.HasValue ? string.Format( "Operator Mode: {0}", Interaction.IIf( this.Device.ThermalStreamSubsystem.OperatorScreen.Value, "ON", "OFF" ) ) : string.Format( "Operator Mode: {0}", "N/A" );
                    this._ResetCycleModeButton.Text = this.Device.ThermalStreamSubsystem.CycleScreen.HasValue ? string.Format( "Cycle Mode: {0}", Interaction.IIf( this.Device.ThermalStreamSubsystem.CycleScreen.Value, "ON", "OFF" ) ) : string.Format( "Cycle Mode: {0}", "N/A" );
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Resets the operator mode button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResetOperatorModeButton_Click( object sender, EventArgs e )
        {
            this.ResetSystemMode( true, sender );
        }

        /// <summary> Resets the cycle mode button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ResetCycleModeButton_Click( object sender, EventArgs e )
        {
            this.ResetSystemMode( false, sender );
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
