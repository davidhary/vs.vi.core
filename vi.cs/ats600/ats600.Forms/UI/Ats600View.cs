using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Ats600.Forms
{

    /// <summary> Temptonic Ats600 Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "Ats600 User Interface" )]
    [Description( "Temptonic Ats600 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( Ats600View ) )]
    public class Ats600View : Facade.VisaView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public Ats600View() : base()
        {
            this.InitializingComponents = true;
            int index = 0;
            index += 1;
            this.ReadingView = ReadingView.Create();
            this.AddView( new Facade.VisaViewControl( this.ReadingView, index, "_ReadingTabPage", "Read" ) );
            index += 1;
            this.FunctionView = FunctionView.Create();
            this.AddView( new Facade.VisaViewControl( this.FunctionView, index, "_FunctionTabPage", "Function" ) );
            index += 1;
            this.SensorView = SensorView.Create();
            this.AddView( new Facade.VisaViewControl( this.SensorView, index, "_SensorTabPage", "Sensor" ) );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        public Ats600View( Ats600Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the Ats600 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.FunctionView is object )
                    {
                        this.FunctionView.Dispose();
                        this.FunctionView = null;
                    }

                    if ( this.SensorView is object )
                    {
                        this.SensorView.Dispose();
                        this.SensorView = null;
                    }

                    if ( this.ReadingView is object )
                    {
                        this.ReadingView.Dispose();
                        this.ReadingView = null;
                    }

                    this.AssignDeviceThis( default );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Ats600Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( Ats600Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = Ats600.Properties.Settings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( Ats600Device value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                // !@# Me.SourceView.AssignDevice(Nothing)
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            // TO_DO: Use a Dual Display interface to bind the display on the display View.
            // MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            // TO_DO: Me.SourceView.AssignDevice(Me.Device)
            // MyBase.DisplayView.BindMeasureToolstrip(Me.Device.MeasureSubsystem)
        }

        #endregion

        #endregion

        #region " VIEWS  "

        /// <summary> Gets or sets the function view. </summary>
        /// <value> The function view. </value>
        private FunctionView FunctionView { get; set; }

        /// <summary> Gets or sets the reading view. </summary>
        /// <value> The reading view. </value>
        private ReadingView ReadingView { get; set; }

        /// <summary> Gets or sets the sensor view. </summary>
        /// <value> The sensor view. </value>
        private SensorView SensorView { get; set; }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
