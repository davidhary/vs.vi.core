using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Ats600.Forms
{

    /// <summary> A Reading view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ReceiveButton.Name = "_ReceiveButton";
            this.__SendButton.Name = "_SendButton";
            this.__ReadStatusByteButton.Name = "_ReadStatusByteButton";
            this.__ClearErrorQueueButton.Name = "_ClearErrorQueueButton";
            this.__ReadLastErrorButton.Name = "_ReadLastErrorButton";
            this.__ReadButton.Name = "_ReadButton";
            this.__InitializeButton.Name = "_InitializeButton";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( default );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Ats600Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( Ats600Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindThermalStreamSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( Ats600Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " THERMO STREAM "

        /// <summary> Gets or sets the Thermal Stream subsystem. </summary>
        /// <value> The Thermal Stream subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ThermalStreamSubsystem ThermalStreamSubsystem { get; private set; }

        /// <summary> Bind Thermal Stream subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindThermalStreamSubsystem( isr.VI.Ats600.Ats600Device device )
        {
            if ( this.ThermalStreamSubsystem is object )
            {
                this.BindSubsystem( false, this.ThermalStreamSubsystem );
                this.ThermalStreamSubsystem = null;
            }

            if ( device is object )
            {
                this.ThermalStreamSubsystem = device.ThermalStreamSubsystem;
                this.BindSubsystem( true, this.ThermalStreamSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ThermalStreamSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ThermalStreamSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.Temperature ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ThermalStreamSubsystemPropertyChanged;
            }
        }

        /// <summary> Executes the measurement available action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void OnMeasurementAvailable( double? value )
        {
            if ( value.HasValue )
            {
                this.OnMeasurementAvailable( value.Value.ToString() );
            }
            else
            {
                this.OnMeasurementAvailable( "" );
            }
        }

        /// <summary> Executes the measurement available action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void OnMeasurementAvailable( string value )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
            }
            // Me._ReadingToolStripStatusLabel.Text = Ats600Control.DegreesCaption("-.-")
            // Me._FailureToolStripStatusLabel.Text = clear
            else
            {
                // Me._ReadingToolStripStatusLabel.SafeTextSetter(Ats600Control.DegreesCaption(value))
                // Me._FailureToolStripStatusLabel.Text = "  "
                _ = this.PublishVerbose( "Instruments parsed reading elements." );
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ThermalStreamSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ats600.ThermalStreamSubsystem.Temperature ):
                    {
                        this.OnMeasurementAvailable( subsystem.Temperature );
                        break;
                    }
            }
        }

        /// <summary> Thermal Stream subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalStreamSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ThermalStreamSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ThermalStreamSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ThermalStreamSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: MAIN PANEL "

        /// <summary> Reads last error button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadLastErrorButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading last error";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {

                    // Me.Device.SystemSubsystem.WriteLastSystemErrorQuery()
                    // Dim srq As VI.Pith.ServiceRequests = Me.readServiceRequest
                    // If (srq And VI.Pith.ServiceRequests.MessageAvailable) = 0 Then
                    // Me.InfoProvider.Annunciate(sender,  Core.Forma.InfoProviderLevel.Info, "Nothing to read")
                    // Else
                    // Me.Device.SystemSubsystem.ClearErrorCache()
                    // Me.Device.SystemSubsystem.ReadLastSystemError()
                    // End If

                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the error queue button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearErrorQueueButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing error queue";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    this.Device.StatusSubsystem.ClearErrorQueue();
                    _ = this.ReadServiceRequest();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by InitButton for click events. Initiates a reading for retrieval by
        /// way of the service request event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitializeButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} initializing";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.Device.IsDeviceOpen )
                {

                    // clear execution state before enabling events
                    this.Device.ClearExecutionState();

                    // set the service request
                    _ = this.Device.ThermalStreamSubsystem.WriteTemperatureEventEnableBitmask( 255 );
                    this.Device.Session.ApplyServiceRequestEnableBitmask( this.Device.Session.DefaultOperationServiceRequestEnableBitmask );
                    this.Device.ClearExecutionState();
                    _ = this.ReadServiceRequest();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _ReadButton for click events. Query the Device for a reading.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading temperature";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    // update display if changed.
                    _ = this.Device.ThermalStreamSubsystem.QueryTemperature();
                    _ = this.ReadServiceRequest();
                    Application.DoEvents();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads service request. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> The service request. </returns>
        private Pith.ServiceRequests ReadServiceRequest()
        {
            var srq = Pith.ServiceRequests.None;
            if ( (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
            {
                // it takes a few ms for the event to register. 
                srq = this.Device.Session.ReadStatusRegister( TimeSpan.FromMilliseconds( ( double ) this._SrqRefratoryTimeNumeric.Value ) );
                Application.DoEvents();
            }

            return srq;
        }

        /// <summary> Reads status byte button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ReadStatusByteButton_Click( object sender, EventArgs e )
        {
            if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
            {
                _ = this.ReadServiceRequest();
            }
        }

        /// <summary> Sends a button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SendButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} sending";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    if ( !string.IsNullOrWhiteSpace( this._SendComboBox.Text ) )
                    {
                        _ = this.Device.Session.WriteLine( this._SendComboBox.Text );
                        _ = this.ReadServiceRequest();
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Receive button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReceiveButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} receiving";
                if ( sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    Pith.ServiceRequests srq = this.Device.Session.ReadStatusRegister();
                    if ( (srq & Pith.ServiceRequests.MessageAvailable) == 0 )
                    {
                        _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Info, "Nothing to read" );
                    }
                    else
                    {
                        this._ReceiveTextBox.Text = this.Device.Session.ReadLineTrimEnd();
                        _ = this.ReadServiceRequest();
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
