﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600.Forms
{
    [DesignerGenerated()]
    public partial class FunctionView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(FunctionView));
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            __QueryStatusButton = new System.Windows.Forms.Button();
            __QueryStatusButton.Click += new EventHandler(QueryAuxiliaryStatusButton_Click);
            _OperatorCycleTabs = new System.Windows.Forms.TabControl();
            _CycleTabPage1 = new System.Windows.Forms.TabPage();
            _CycleToolStrip = new System.Windows.Forms.ToolStrip();
            __ResetCycleModeButton = new System.Windows.Forms.ToolStripButton();
            __ResetCycleModeButton.Click += new EventHandler(ResetCycleModeButton_Click);
            __StartCycleButton = new System.Windows.Forms.ToolStripButton();
            __StartCycleButton.Click += new EventHandler(StartButton_Click);
            __NextSetpointButton = new System.Windows.Forms.ToolStripButton();
            __NextSetpointButton.Click += new EventHandler(NextButton_Click);
            __StopCycleButton = new System.Windows.Forms.ToolStripButton();
            __StopCycleButton.Click += new EventHandler(StopCycleButton_Click);
            _CyclesCompletedCheckBox1 = new Core.Controls.ToolStripCheckBox();
            _CycleCompletedCheckBox1 = new Core.Controls.ToolStripCheckBox();
            _CyclingStoppedCheckBox = new Core.Controls.ToolStripCheckBox();
            __FindLastSetpointButton1 = new System.Windows.Forms.ToolStripButton();
            __FindLastSetpointButton1.Click += new EventHandler(FindLastSetpointButton_Click);
            _OperatorTabPage = new System.Windows.Forms.TabPage();
            _ResetDelayNumericLabel = new System.Windows.Forms.Label();
            _ResetQueryDelayNumeric = new Core.Controls.NumericUpDown();
            __ResetOperatorModeButton = new System.Windows.Forms.Button();
            __ResetOperatorModeButton.Click += new EventHandler(ResetOperatorModeButton_Click);
            __SetpointNumberNumeric = new Core.Controls.SelectorNumeric();
            __SetpointNumberNumeric.ValueSelected += new EventHandler<EventArgs>(SetpointNumberNumeric_ValueSelected);
            _TestTimeElapsedCheckBox = new Core.Controls.CheckBox();
            _CycleCountNumericLabel = new System.Windows.Forms.Label();
            __CycleCountNumeric = new Core.Controls.SelectorNumeric();
            __CycleCountNumeric.ValueSelected += new EventHandler<EventArgs>(CycleCountNumeric_ValueSelected);
            __MaxTestTimeNumeric = new Core.Controls.SelectorNumeric();
            __MaxTestTimeNumeric.ValueSelected += new EventHandler<EventArgs>(MaxTestTimeNumeric_ValueSelected);
            _MaxTestTimeNumericLabel = new System.Windows.Forms.Label();
            __ReadSetpointButton = new System.Windows.Forms.Button();
            __ReadSetpointButton.Click += new EventHandler(ReadSetpointButton_Click);
            _ReadyCheckBox = new Core.Controls.CheckBox();
            _NotAtTempCheckBox = new Core.Controls.CheckBox();
            _AtTempCheckBox = new Core.Controls.CheckBox();
            _HeadDownCheckBox = new Core.Controls.CheckBox();
            _SetpointWindowNumericLabel = new System.Windows.Forms.Label();
            _SetpointWindowNumeric = new Core.Controls.SelectorNumeric();
            _SetpointNumberNumericLabel = new System.Windows.Forms.Label();
            _RampRateNumeric = new Core.Controls.SelectorNumeric();
            _SoakTimeNumeric = new Core.Controls.SelectorNumeric();
            _SetpointNumeric = new Core.Controls.SelectorNumeric();
            _RampRateNumericLabel = new System.Windows.Forms.Label();
            _SoakTimeNumericLabel = new System.Windows.Forms.Label();
            _SetpointNumericLabel = new System.Windows.Forms.Label();
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            _OperatorCycleTabs.SuspendLayout();
            _CycleTabPage1.SuspendLayout();
            _CycleToolStrip.SuspendLayout();
            _OperatorTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ResetQueryDelayNumeric).BeginInit();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__QueryStatusButton);
            _Panel.Controls.Add(_OperatorCycleTabs);
            _Panel.Controls.Add(__SetpointNumberNumeric);
            _Panel.Controls.Add(_TestTimeElapsedCheckBox);
            _Panel.Controls.Add(_CycleCountNumericLabel);
            _Panel.Controls.Add(__CycleCountNumeric);
            _Panel.Controls.Add(__MaxTestTimeNumeric);
            _Panel.Controls.Add(_MaxTestTimeNumericLabel);
            _Panel.Controls.Add(__ReadSetpointButton);
            _Panel.Controls.Add(_ReadyCheckBox);
            _Panel.Controls.Add(_NotAtTempCheckBox);
            _Panel.Controls.Add(_AtTempCheckBox);
            _Panel.Controls.Add(_HeadDownCheckBox);
            _Panel.Controls.Add(_SetpointWindowNumericLabel);
            _Panel.Controls.Add(_SetpointWindowNumeric);
            _Panel.Controls.Add(_SetpointNumberNumericLabel);
            _Panel.Controls.Add(_RampRateNumeric);
            _Panel.Controls.Add(_SoakTimeNumeric);
            _Panel.Controls.Add(_SetpointNumeric);
            _Panel.Controls.Add(_RampRateNumericLabel);
            _Panel.Controls.Add(_SoakTimeNumericLabel);
            _Panel.Controls.Add(_SetpointNumericLabel);
            _Panel.Location = new System.Drawing.Point(13, 23);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(352, 276);
            _Panel.TabIndex = 0;
            // 
            // _QueryStatusButton
            // 
            __QueryStatusButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __QueryStatusButton.Location = new System.Drawing.Point(237, 5);
            __QueryStatusButton.Name = "__QueryStatusButton";
            __QueryStatusButton.Size = new System.Drawing.Size(107, 28);
            __QueryStatusButton.TabIndex = 24;
            __QueryStatusButton.Text = "Refresh Status";
            __QueryStatusButton.UseVisualStyleBackColor = true;
            // 
            // _OperatorCycleTabs
            // 
            _OperatorCycleTabs.Controls.Add(_CycleTabPage1);
            _OperatorCycleTabs.Controls.Add(_OperatorTabPage);
            _OperatorCycleTabs.Location = new System.Drawing.Point(207, 60);
            _OperatorCycleTabs.Name = "_OperatorCycleTabs";
            _OperatorCycleTabs.SelectedIndex = 0;
            _OperatorCycleTabs.Size = new System.Drawing.Size(138, 208);
            _OperatorCycleTabs.TabIndex = 44;
            // 
            // _CycleTabPage1
            // 
            _CycleTabPage1.Controls.Add(_CycleToolStrip);
            _CycleTabPage1.Location = new System.Drawing.Point(4, 26);
            _CycleTabPage1.Name = "_CycleTabPage1";
            _CycleTabPage1.Padding = new System.Windows.Forms.Padding(3);
            _CycleTabPage1.Size = new System.Drawing.Size(130, 178);
            _CycleTabPage1.TabIndex = 0;
            _CycleTabPage1.Text = "Cycle";
            _CycleTabPage1.UseVisualStyleBackColor = true;
            // 
            // _CycleToolStrip
            // 
            _CycleToolStrip.Dock = System.Windows.Forms.DockStyle.Left;
            _CycleToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _CycleToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ResetCycleModeButton, __StartCycleButton, __NextSetpointButton, __StopCycleButton, _CyclesCompletedCheckBox1, _CycleCompletedCheckBox1, _CyclingStoppedCheckBox, __FindLastSetpointButton1 });
            _CycleToolStrip.Location = new System.Drawing.Point(3, 3);
            _CycleToolStrip.Name = "_CycleToolStrip";
            _CycleToolStrip.Size = new System.Drawing.Size(123, 172);
            _CycleToolStrip.TabIndex = 22;
            _CycleToolStrip.Text = "ToolStrip1";
            // 
            // _ResetCycleModeButton
            // 
            __ResetCycleModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ResetCycleModeButton.Image = (System.Drawing.Image)resources.GetObject("_ResetCycleModeButton.Image");
            __ResetCycleModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ResetCycleModeButton.Margin = new System.Windows.Forms.Padding(0);
            __ResetCycleModeButton.Name = "__ResetCycleModeButton";
            __ResetCycleModeButton.Size = new System.Drawing.Size(120, 19);
            __ResetCycleModeButton.Text = "Cycle Mode: On/Off";
            // 
            // _StartCycleButton
            // 
            __StartCycleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __StartCycleButton.Image = (System.Drawing.Image)resources.GetObject("_StartCycleButton.Image");
            __StartCycleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __StartCycleButton.Margin = new System.Windows.Forms.Padding(0);
            __StartCycleButton.Name = "__StartCycleButton";
            __StartCycleButton.Size = new System.Drawing.Size(120, 19);
            __StartCycleButton.Text = "Start";
            // 
            // _NextSetpointButton
            // 
            __NextSetpointButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __NextSetpointButton.Image = (System.Drawing.Image)resources.GetObject("_NextSetpointButton.Image");
            __NextSetpointButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __NextSetpointButton.Margin = new System.Windows.Forms.Padding(0);
            __NextSetpointButton.Name = "__NextSetpointButton";
            __NextSetpointButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            __NextSetpointButton.Size = new System.Drawing.Size(120, 19);
            __NextSetpointButton.Text = "Next";
            // 
            // _StopCycleButton
            // 
            __StopCycleButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __StopCycleButton.Image = (System.Drawing.Image)resources.GetObject("_StopCycleButton.Image");
            __StopCycleButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __StopCycleButton.Margin = new System.Windows.Forms.Padding(0);
            __StopCycleButton.Name = "__StopCycleButton";
            __StopCycleButton.Size = new System.Drawing.Size(120, 19);
            __StopCycleButton.Text = "Stop";
            __StopCycleButton.ToolTipText = "Stop cycle";
            // 
            // _CyclesCompletedCheckBox1
            // 
            _CyclesCompletedCheckBox1.Checked = false;
            _CyclesCompletedCheckBox1.Margin = new System.Windows.Forms.Padding(0);
            _CyclesCompletedCheckBox1.Name = "_CyclesCompletedCheckBox1";
            _CyclesCompletedCheckBox1.Size = new System.Drawing.Size(120, 19);
            _CyclesCompletedCheckBox1.Text = "Cycles Completed";
            // 
            // _CycleCompletedCheckBox1
            // 
            _CycleCompletedCheckBox1.Checked = false;
            _CycleCompletedCheckBox1.Margin = new System.Windows.Forms.Padding(0);
            _CycleCompletedCheckBox1.Name = "_CycleCompletedCheckBox1";
            _CycleCompletedCheckBox1.Size = new System.Drawing.Size(120, 19);
            _CycleCompletedCheckBox1.Text = "Cycle Complete";
            // 
            // _CyclingStoppedCheckBox
            // 
            _CyclingStoppedCheckBox.Checked = false;
            _CyclingStoppedCheckBox.Margin = new System.Windows.Forms.Padding(0);
            _CyclingStoppedCheckBox.Name = "_CyclingStoppedCheckBox";
            _CyclingStoppedCheckBox.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            _CyclingStoppedCheckBox.Size = new System.Drawing.Size(120, 19);
            _CyclingStoppedCheckBox.Text = "Cycling Stopped";
            // 
            // _FindLastSetpointButton1
            // 
            __FindLastSetpointButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __FindLastSetpointButton1.Image = (System.Drawing.Image)resources.GetObject("_FindLastSetpointButton1.Image");
            __FindLastSetpointButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            __FindLastSetpointButton1.Margin = new System.Windows.Forms.Padding(0);
            __FindLastSetpointButton1.Name = "__FindLastSetpointButton1";
            __FindLastSetpointButton1.Size = new System.Drawing.Size(120, 19);
            __FindLastSetpointButton1.Text = "Find Last Setpoint: -1";
            // 
            // _OperatorTabPage
            // 
            _OperatorTabPage.Controls.Add(_ResetDelayNumericLabel);
            _OperatorTabPage.Controls.Add(_ResetQueryDelayNumeric);
            _OperatorTabPage.Controls.Add(__ResetOperatorModeButton);
            _OperatorTabPage.Location = new System.Drawing.Point(4, 26);
            _OperatorTabPage.Name = "_OperatorTabPage";
            _OperatorTabPage.Padding = new System.Windows.Forms.Padding(3);
            _OperatorTabPage.Size = new System.Drawing.Size(130, 178);
            _OperatorTabPage.TabIndex = 1;
            _OperatorTabPage.Text = "Operator";
            _OperatorTabPage.UseVisualStyleBackColor = true;
            // 
            // _ResetDelayNumericLabel
            // 
            _ResetDelayNumericLabel.AutoSize = true;
            _ResetDelayNumericLabel.Location = new System.Drawing.Point(5, 67);
            _ResetDelayNumericLabel.Name = "_ResetDelayNumericLabel";
            _ResetDelayNumericLabel.Size = new System.Drawing.Size(72, 17);
            _ResetDelayNumericLabel.TabIndex = 14;
            _ResetDelayNumericLabel.Text = "Delay [ms]:";
            // 
            // _ResetQueryDelayNumeric
            // 
            _ResetQueryDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ResetQueryDelayNumeric.Location = new System.Drawing.Point(80, 63);
            _ResetQueryDelayNumeric.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            _ResetQueryDelayNumeric.Name = "_ResetQueryDelayNumeric";
            _ResetQueryDelayNumeric.NullValue = new decimal(new int[] { 5, 0, 0, 0 });
            _ResetQueryDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            _ResetQueryDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            _ResetQueryDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            _ResetQueryDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            _ResetQueryDelayNumeric.Size = new System.Drawing.Size(45, 25);
            _ResetQueryDelayNumeric.TabIndex = 13;
            _ResetQueryDelayNumeric.Value = new decimal(new int[] { 5, 0, 0, 0 });
            // 
            // _ResetOperatorModeButton
            // 
            __ResetOperatorModeButton.Location = new System.Drawing.Point(8, 6);
            __ResetOperatorModeButton.Name = "__ResetOperatorModeButton";
            __ResetOperatorModeButton.Size = new System.Drawing.Size(114, 49);
            __ResetOperatorModeButton.TabIndex = 0;
            __ResetOperatorModeButton.Text = "Operator Mode: On/Off";
            __ResetOperatorModeButton.UseVisualStyleBackColor = true;
            // 
            // _SetpointNumberNumeric
            // 
            __SetpointNumberNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __SetpointNumberNumeric.CanSelectEmptyValue = true;
            __SetpointNumberNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            __SetpointNumberNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            __SetpointNumberNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __SetpointNumberNumeric.Location = new System.Drawing.Point(117, 88);
            __SetpointNumberNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __SetpointNumberNumeric.Minimum = new decimal(new int[] { 0, 0, 0, 0 });
            __SetpointNumberNumeric.Name = "__SetpointNumberNumeric";
            __SetpointNumberNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_SetpointNumberNumeric.SelectorIcon");
            __SetpointNumberNumeric.Size = new System.Drawing.Size(84, 25);
            __SetpointNumberNumeric.TabIndex = 30;
            __SetpointNumberNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            __SetpointNumberNumeric.Watermark = null;
            // 
            // _TestTimeElapsedCheckBox
            // 
            _TestTimeElapsedCheckBox.AutoSize = true;
            _TestTimeElapsedCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _TestTimeElapsedCheckBox.Location = new System.Drawing.Point(25, 48);
            _TestTimeElapsedCheckBox.Name = "_TestTimeElapsedCheckBox";
            _TestTimeElapsedCheckBox.ReadOnly = true;
            _TestTimeElapsedCheckBox.Size = new System.Drawing.Size(104, 21);
            _TestTimeElapsedCheckBox.TabIndex = 28;
            _TestTimeElapsedCheckBox.Text = "Test Timeout:";
            _TestTimeElapsedCheckBox.ThreeState = true;
            _TestTimeElapsedCheckBox.UseVisualStyleBackColor = true;
            // 
            // _CycleCountNumericLabel
            // 
            _CycleCountNumericLabel.AutoSize = true;
            _CycleCountNumericLabel.Location = new System.Drawing.Point(35, 248);
            _CycleCountNumericLabel.Name = "_CycleCountNumericLabel";
            _CycleCountNumericLabel.Size = new System.Drawing.Size(79, 17);
            _CycleCountNumericLabel.TabIndex = 43;
            _CycleCountNumericLabel.Text = "Cycle Count:";
            // 
            // _CycleCountNumeric
            // 
            __CycleCountNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __CycleCountNumeric.CanSelectEmptyValue = true;
            __CycleCountNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            __CycleCountNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            __CycleCountNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __CycleCountNumeric.Location = new System.Drawing.Point(117, 244);
            __CycleCountNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __CycleCountNumeric.Maximum = new decimal(new int[] { 9999, 0, 0, 0 });
            __CycleCountNumeric.Minimum = new decimal(new int[] { 0, 0, 0, 0 });
            __CycleCountNumeric.Name = "__CycleCountNumeric";
            __CycleCountNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_CycleCountNumeric.SelectorIcon");
            __CycleCountNumeric.Size = new System.Drawing.Size(84, 25);
            __CycleCountNumeric.TabIndex = 42;
            __CycleCountNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            __CycleCountNumeric.Watermark = null;
            // 
            // _MaxTestTimeNumeric
            // 
            __MaxTestTimeNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __MaxTestTimeNumeric.CanSelectEmptyValue = true;
            __MaxTestTimeNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            __MaxTestTimeNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            __MaxTestTimeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __MaxTestTimeNumeric.Location = new System.Drawing.Point(117, 218);
            __MaxTestTimeNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __MaxTestTimeNumeric.Maximum = new decimal(new int[] { 9999, 0, 0, 0 });
            __MaxTestTimeNumeric.Minimum = new decimal(new int[] { 0, 0, 0, 0 });
            __MaxTestTimeNumeric.Name = "__MaxTestTimeNumeric";
            __MaxTestTimeNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_MaxTestTimeNumeric.SelectorIcon");
            __MaxTestTimeNumeric.Size = new System.Drawing.Size(84, 25);
            __MaxTestTimeNumeric.TabIndex = 41;
            __MaxTestTimeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            __MaxTestTimeNumeric.Watermark = null;
            // 
            // _MaxTestTimeNumericLabel
            // 
            _MaxTestTimeNumericLabel.AutoSize = true;
            _MaxTestTimeNumericLabel.Location = new System.Drawing.Point(29, 223);
            _MaxTestTimeNumericLabel.Name = "_MaxTestTimeNumericLabel";
            _MaxTestTimeNumericLabel.Size = new System.Drawing.Size(84, 17);
            _MaxTestTimeNumericLabel.TabIndex = 40;
            _MaxTestTimeNumericLabel.Text = "Test Time [s]:";
            // 
            // _ReadSetpointButton
            // 
            __ReadSetpointButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ReadSetpointButton.Location = new System.Drawing.Point(237, 33);
            __ReadSetpointButton.Name = "__ReadSetpointButton";
            __ReadSetpointButton.Size = new System.Drawing.Size(109, 25);
            __ReadSetpointButton.TabIndex = 39;
            __ReadSetpointButton.Text = "Read Setpoint";
            __ReadSetpointButton.UseVisualStyleBackColor = true;
            // 
            // _ReadyCheckBox
            // 
            _ReadyCheckBox.AutoSize = true;
            _ReadyCheckBox.Location = new System.Drawing.Point(143, 10);
            _ReadyCheckBox.Name = "_ReadyCheckBox";
            _ReadyCheckBox.ReadOnly = true;
            _ReadyCheckBox.Size = new System.Drawing.Size(66, 21);
            _ReadyCheckBox.TabIndex = 26;
            _ReadyCheckBox.Text = ":Ready";
            _ReadyCheckBox.ThreeState = true;
            _ReadyCheckBox.UseVisualStyleBackColor = true;
            // 
            // _NotAtTempCheckBox
            // 
            _NotAtTempCheckBox.AutoSize = true;
            _NotAtTempCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _NotAtTempCheckBox.Location = new System.Drawing.Point(25, 29);
            _NotAtTempCheckBox.Name = "_NotAtTempCheckBox";
            _NotAtTempCheckBox.ReadOnly = true;
            _NotAtTempCheckBox.Size = new System.Drawing.Size(104, 21);
            _NotAtTempCheckBox.TabIndex = 27;
            _NotAtTempCheckBox.Text = "Not At Temp:";
            _NotAtTempCheckBox.ThreeState = true;
            _NotAtTempCheckBox.UseVisualStyleBackColor = true;
            // 
            // _AtTempCheckBox
            // 
            _AtTempCheckBox.AutoSize = true;
            _AtTempCheckBox.Location = new System.Drawing.Point(143, 29);
            _AtTempCheckBox.Name = "_AtTempCheckBox";
            _AtTempCheckBox.ReadOnly = true;
            _AtTempCheckBox.Size = new System.Drawing.Size(78, 21);
            _AtTempCheckBox.TabIndex = 25;
            _AtTempCheckBox.Text = ":At Temp";
            _AtTempCheckBox.ThreeState = true;
            _AtTempCheckBox.UseVisualStyleBackColor = true;
            // 
            // _HeadDownCheckBox
            // 
            _HeadDownCheckBox.AutoSize = true;
            _HeadDownCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _HeadDownCheckBox.Location = new System.Drawing.Point(32, 10);
            _HeadDownCheckBox.Name = "_HeadDownCheckBox";
            _HeadDownCheckBox.ReadOnly = true;
            _HeadDownCheckBox.Size = new System.Drawing.Size(98, 21);
            _HeadDownCheckBox.TabIndex = 23;
            _HeadDownCheckBox.Text = "Head Down:";
            _HeadDownCheckBox.ThreeState = true;
            _HeadDownCheckBox.UseVisualStyleBackColor = true;
            // 
            // _SetpointWindowNumericLabel
            // 
            _SetpointWindowNumericLabel.AutoSize = true;
            _SetpointWindowNumericLabel.Location = new System.Drawing.Point(31, 144);
            _SetpointWindowNumericLabel.Name = "_SetpointWindowNumericLabel";
            _SetpointWindowNumericLabel.Size = new System.Drawing.Size(83, 17);
            _SetpointWindowNumericLabel.TabIndex = 33;
            _SetpointWindowNumericLabel.Text = "Window [°C]:";
            // 
            // _SetpointWindowNumeric
            // 
            _SetpointWindowNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _SetpointWindowNumeric.DecimalPlaces = 1;
            _SetpointWindowNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            _SetpointWindowNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            _SetpointWindowNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SetpointWindowNumeric.Location = new System.Drawing.Point(117, 140);
            _SetpointWindowNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _SetpointWindowNumeric.Maximum = new decimal(new int[] { 99, 0, 0, 65536 });
            _SetpointWindowNumeric.Minimum = new decimal(new int[] { 0, 0, 0, 0 });
            _SetpointWindowNumeric.Name = "_SetpointWindowNumeric";
            _SetpointWindowNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_SetpointWindowNumeric.SelectorIcon");
            _SetpointWindowNumeric.Size = new System.Drawing.Size(84, 25);
            _SetpointWindowNumeric.TabIndex = 34;
            _SetpointWindowNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            _SetpointWindowNumeric.Watermark = null;
            // 
            // _SetpointNumberNumericLabel
            // 
            _SetpointNumberNumericLabel.AutoSize = true;
            _SetpointNumberNumericLabel.Location = new System.Drawing.Point(6, 90);
            _SetpointNumberNumericLabel.Name = "_SetpointNumberNumericLabel";
            _SetpointNumberNumericLabel.Size = new System.Drawing.Size(108, 17);
            _SetpointNumberNumericLabel.TabIndex = 29;
            _SetpointNumberNumericLabel.Text = "Setpoint number:";
            // 
            // _RampRateNumeric
            // 
            _RampRateNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _RampRateNumeric.CanSelectEmptyValue = true;
            _RampRateNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            _RampRateNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            _RampRateNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _RampRateNumeric.Location = new System.Drawing.Point(117, 192);
            _RampRateNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _RampRateNumeric.Maximum = new decimal(new int[] { 9999, 0, 0, 0 });
            _RampRateNumeric.Minimum = new decimal(new int[] { 0, 0, 0, 0 });
            _RampRateNumeric.Name = "_RampRateNumeric";
            _RampRateNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_RampRateNumeric.SelectorIcon");
            _RampRateNumeric.Size = new System.Drawing.Size(84, 25);
            _RampRateNumeric.TabIndex = 37;
            _RampRateNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            _RampRateNumeric.Watermark = null;
            // 
            // _SoakTimeNumeric
            // 
            _SoakTimeNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _SoakTimeNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            _SoakTimeNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            _SoakTimeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SoakTimeNumeric.Location = new System.Drawing.Point(117, 166);
            _SoakTimeNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _SoakTimeNumeric.Maximum = new decimal(new int[] { 9999, 0, 0, 0 });
            _SoakTimeNumeric.Minimum = new decimal(new int[] { 0, 0, 0, 0 });
            _SoakTimeNumeric.Name = "_SoakTimeNumeric";
            _SoakTimeNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_SoakTimeNumeric.SelectorIcon");
            _SoakTimeNumeric.Size = new System.Drawing.Size(84, 25);
            _SoakTimeNumeric.TabIndex = 38;
            _SoakTimeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            _SoakTimeNumeric.Watermark = null;
            // 
            // _SetpointNumeric
            // 
            _SetpointNumeric.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _SetpointNumeric.DecimalPlaces = 1;
            _SetpointNumeric.DirtyBackColor = System.Drawing.Color.Orange;
            _SetpointNumeric.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            _SetpointNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SetpointNumeric.Location = new System.Drawing.Point(117, 114);
            _SetpointNumeric.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _SetpointNumeric.Maximum = new decimal(new int[] { 200, 0, 0, 0 });
            _SetpointNumeric.Minimum = new decimal(new int[] { 100, 0, 0, (int)-2147483648L });
            _SetpointNumeric.Name = "_SetpointNumeric";
            _SetpointNumeric.SelectorIcon = (System.Drawing.Image)resources.GetObject("_SetpointNumeric.SelectorIcon");
            _SetpointNumeric.Size = new System.Drawing.Size(84, 25);
            _SetpointNumeric.TabIndex = 32;
            _SetpointNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            _SetpointNumeric.Watermark = null;
            // 
            // _RampRateNumericLabel
            // 
            _RampRateNumericLabel.AutoSize = true;
            _RampRateNumericLabel.Location = new System.Drawing.Point(26, 196);
            _RampRateNumericLabel.Name = "_RampRateNumericLabel";
            _RampRateNumericLabel.Size = new System.Drawing.Size(88, 17);
            _RampRateNumericLabel.TabIndex = 35;
            _RampRateNumericLabel.Text = "Ramp [°/min]:";
            _RampRateNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SoakTimeNumericLabel
            // 
            _SoakTimeNumericLabel.AutoSize = true;
            _SoakTimeNumericLabel.Location = new System.Drawing.Point(25, 170);
            _SoakTimeNumericLabel.Name = "_SoakTimeNumericLabel";
            _SoakTimeNumericLabel.Size = new System.Drawing.Size(89, 17);
            _SoakTimeNumericLabel.TabIndex = 36;
            _SoakTimeNumericLabel.Text = "Soak Time [s]:";
            _SoakTimeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SetpointNumericLabel
            // 
            _SetpointNumericLabel.AutoSize = true;
            _SetpointNumericLabel.Location = new System.Drawing.Point(30, 118);
            _SetpointNumericLabel.Name = "_SetpointNumericLabel";
            _SetpointNumericLabel.Size = new System.Drawing.Size(84, 17);
            _SetpointNumericLabel.TabIndex = 31;
            _SetpointNumericLabel.Text = "Setpoint [°C]:";
            _SetpointNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // FunctionView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "FunctionView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _OperatorCycleTabs.ResumeLayout(false);
            _CycleTabPage1.ResumeLayout(false);
            _CycleTabPage1.PerformLayout();
            _CycleToolStrip.ResumeLayout(false);
            _CycleToolStrip.PerformLayout();
            _OperatorTabPage.ResumeLayout(false);
            _OperatorTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ResetQueryDelayNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Button __QueryStatusButton;

        private System.Windows.Forms.Button _QueryStatusButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __QueryStatusButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__QueryStatusButton != null)
                {
                    __QueryStatusButton.Click -= QueryAuxiliaryStatusButton_Click;
                }

                __QueryStatusButton = value;
                if (__QueryStatusButton != null)
                {
                    __QueryStatusButton.Click += QueryAuxiliaryStatusButton_Click;
                }
            }
        }

        private System.Windows.Forms.TabControl _OperatorCycleTabs;
        private System.Windows.Forms.TabPage _CycleTabPage1;
        private System.Windows.Forms.ToolStrip _CycleToolStrip;
        private System.Windows.Forms.ToolStripButton __ResetCycleModeButton;

        private System.Windows.Forms.ToolStripButton _ResetCycleModeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResetCycleModeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResetCycleModeButton != null)
                {
                    __ResetCycleModeButton.Click -= ResetCycleModeButton_Click;
                }

                __ResetCycleModeButton = value;
                if (__ResetCycleModeButton != null)
                {
                    __ResetCycleModeButton.Click += ResetCycleModeButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __StartCycleButton;

        private System.Windows.Forms.ToolStripButton _StartCycleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartCycleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartCycleButton != null)
                {
                    __StartCycleButton.Click -= StartButton_Click;
                }

                __StartCycleButton = value;
                if (__StartCycleButton != null)
                {
                    __StartCycleButton.Click += StartButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __NextSetpointButton;

        private System.Windows.Forms.ToolStripButton _NextSetpointButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NextSetpointButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NextSetpointButton != null)
                {
                    __NextSetpointButton.Click -= NextButton_Click;
                }

                __NextSetpointButton = value;
                if (__NextSetpointButton != null)
                {
                    __NextSetpointButton.Click += NextButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __StopCycleButton;

        private System.Windows.Forms.ToolStripButton _StopCycleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StopCycleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StopCycleButton != null)
                {
                    __StopCycleButton.Click -= StopCycleButton_Click;
                }

                __StopCycleButton = value;
                if (__StopCycleButton != null)
                {
                    __StopCycleButton.Click += StopCycleButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripCheckBox _CyclesCompletedCheckBox1;
        private Core.Controls.ToolStripCheckBox _CycleCompletedCheckBox1;
        private Core.Controls.ToolStripCheckBox _CyclingStoppedCheckBox;
        private System.Windows.Forms.ToolStripButton __FindLastSetpointButton1;

        private System.Windows.Forms.ToolStripButton _FindLastSetpointButton1
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FindLastSetpointButton1;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FindLastSetpointButton1 != null)
                {
                    __FindLastSetpointButton1.Click -= FindLastSetpointButton_Click;
                }

                __FindLastSetpointButton1 = value;
                if (__FindLastSetpointButton1 != null)
                {
                    __FindLastSetpointButton1.Click += FindLastSetpointButton_Click;
                }
            }
        }

        private System.Windows.Forms.TabPage _OperatorTabPage;
        private System.Windows.Forms.Label _ResetDelayNumericLabel;
        private Core.Controls.NumericUpDown _ResetQueryDelayNumeric;
        private System.Windows.Forms.Button __ResetOperatorModeButton;

        private System.Windows.Forms.Button _ResetOperatorModeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ResetOperatorModeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ResetOperatorModeButton != null)
                {
                    __ResetOperatorModeButton.Click -= ResetOperatorModeButton_Click;
                }

                __ResetOperatorModeButton = value;
                if (__ResetOperatorModeButton != null)
                {
                    __ResetOperatorModeButton.Click += ResetOperatorModeButton_Click;
                }
            }
        }

        private Core.Controls.SelectorNumeric __SetpointNumberNumeric;

        private Core.Controls.SelectorNumeric _SetpointNumberNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SetpointNumberNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SetpointNumberNumeric != null)
                {
                    __SetpointNumberNumeric.ValueSelected -= SetpointNumberNumeric_ValueSelected;
                }

                __SetpointNumberNumeric = value;
                if (__SetpointNumberNumeric != null)
                {
                    __SetpointNumberNumeric.ValueSelected += SetpointNumberNumeric_ValueSelected;
                }
            }
        }

        private Core.Controls.CheckBox _TestTimeElapsedCheckBox;
        private System.Windows.Forms.Label _CycleCountNumericLabel;
        private Core.Controls.SelectorNumeric __CycleCountNumeric;

        private Core.Controls.SelectorNumeric _CycleCountNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CycleCountNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CycleCountNumeric != null)
                {
                    __CycleCountNumeric.ValueSelected -= CycleCountNumeric_ValueSelected;
                }

                __CycleCountNumeric = value;
                if (__CycleCountNumeric != null)
                {
                    __CycleCountNumeric.ValueSelected += CycleCountNumeric_ValueSelected;
                }
            }
        }

        private Core.Controls.SelectorNumeric __MaxTestTimeNumeric;

        private Core.Controls.SelectorNumeric _MaxTestTimeNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MaxTestTimeNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MaxTestTimeNumeric != null)
                {
                    __MaxTestTimeNumeric.ValueSelected -= MaxTestTimeNumeric_ValueSelected;
                }

                __MaxTestTimeNumeric = value;
                if (__MaxTestTimeNumeric != null)
                {
                    __MaxTestTimeNumeric.ValueSelected += MaxTestTimeNumeric_ValueSelected;
                }
            }
        }

        private System.Windows.Forms.Label _MaxTestTimeNumericLabel;
        private System.Windows.Forms.Button __ReadSetpointButton;

        private System.Windows.Forms.Button _ReadSetpointButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadSetpointButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadSetpointButton != null)
                {
                    __ReadSetpointButton.Click -= ReadSetpointButton_Click;
                }

                __ReadSetpointButton = value;
                if (__ReadSetpointButton != null)
                {
                    __ReadSetpointButton.Click += ReadSetpointButton_Click;
                }
            }
        }

        private Core.Controls.CheckBox _ReadyCheckBox;
        private Core.Controls.CheckBox _NotAtTempCheckBox;
        private Core.Controls.CheckBox _AtTempCheckBox;
        private Core.Controls.CheckBox _HeadDownCheckBox;
        private System.Windows.Forms.Label _SetpointWindowNumericLabel;
        private Core.Controls.SelectorNumeric _SetpointWindowNumeric;
        private System.Windows.Forms.Label _SetpointNumberNumericLabel;
        private Core.Controls.SelectorNumeric _RampRateNumeric;
        private Core.Controls.SelectorNumeric _SoakTimeNumeric;
        private Core.Controls.SelectorNumeric _SetpointNumeric;
        private System.Windows.Forms.Label _RampRateNumericLabel;
        private System.Windows.Forms.Label _SoakTimeNumericLabel;
        private System.Windows.Forms.Label _SetpointNumericLabel;
    }
}