using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Ats600.Forms
{

    /// <summary> A Sensor view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SensorView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public SensorView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__DeviceSensorTypeSelector.Name = "_DeviceSensorTypeSelector";
        }

        /// <summary> Creates a new <see cref="SensorView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="SensorView"/>. </returns>
        public static SensorView Create()
        {
            SensorView view = null;
            try
            {
                view = new SensorView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( default );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Ats600Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( Ats600Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
                this.DisplaySensorTypes();
            }

            this.BindThermalStreamSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( Ats600Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " THERMO STREAM "

        /// <summary> Gets or sets the Thermal Stream subsystem. </summary>
        /// <value> The Thermal Stream subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ThermalStreamSubsystem ThermalStreamSubsystem { get; private set; }

        /// <summary> Bind Thermal Stream subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindThermalStreamSubsystem( isr.VI.Ats600.Ats600Device device )
        {
            if ( this.ThermalStreamSubsystem is object )
            {
                this.BindSubsystem( false, this.ThermalStreamSubsystem );
                this.ThermalStreamSubsystem = null;
            }

            if ( device is object )
            {
                this.ThermalStreamSubsystem = device.ThermalStreamSubsystem;
                this.BindSubsystem( true, this.ThermalStreamSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ThermalStreamSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ThermalStreamSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.DeviceSensorType ) );
                this.HandlePropertyChanged( subsystem, nameof( Ats600.ThermalStreamSubsystem.DeviceThermalConstant ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ThermalStreamSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ThermalStreamSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Ats600.ThermalStreamSubsystem.DeviceSensorType ):
                    {
                        _ = this._DeviceSensorTypeSelector.ComboBox.SelectValue( subsystem.DeviceSensorType.GetValueOrDefault( 0 ) );
                        break;
                    }

                case nameof( Ats600.ThermalStreamSubsystem.DeviceThermalConstant ):
                    {
                        this._DeviceThermalConstantSelector.ValueSetter( subsystem.DeviceThermalConstant );
                        break;
                    }
            }
        }

        /// <summary> Thermal Stream subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ThermalStreamSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ThermalStreamSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ThermalStreamSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ThermalStreamSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSOR "

        /// <summary> Type of the device sensor. </summary>
        private Core.EnumExtender<DeviceSensorType> _DeviceSensorType;

        /// <summary> Displays a sensor types. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void DisplaySensorTypes()
        {
            this._DeviceSensorType = new Core.EnumExtender<DeviceSensorType>();
            _ = this._DeviceSensorTypeSelector.ComboBox.ListValues( this._DeviceSensorType.ValueDescriptionPairs );
        }

        /// <summary> Device sensor type selector value selected. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DeviceSensorTypeSelector_ValueSelected( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} setting and reading the device sensor type";
                if ( this._DeviceSensorType is object )
                {
                }

                if ( this._DeviceSensorType is object && sender is object && (this.Device?.IsDeviceOpen).GetValueOrDefault( false ) )
                {
                    var v = this._DeviceSensorTypeSelector.ComboBox.SelectedValue( this._DeviceSensorType.ValueEnumValuePairs );
                    _ = this.Device.ThermalStreamSubsystem.ApplyDeviceSensorType( v );
                    _ = this.Device.ThermalStreamSubsystem.QueryDeviceSensorType();
                    _ = this.Device.ThermalStreamSubsystem.DeviceSensorType.GetValueOrDefault( DeviceSensorType.None ) == DeviceSensorType.None
                        ? this.Device.ThermalStreamSubsystem.ApplyDeviceControl( false )
                        : this.Device.ThermalStreamSubsystem.ApplyDeviceControl( true );

                    _ = this.Device.ThermalStreamSubsystem.QueryDeviceControl();
                    _ = this.Device.ThermalStreamSubsystem.QueryDeviceThermalConstant();
                    this.ReadStatusRegister();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
