﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600.Forms
{
    [DesignerGenerated()]
    public partial class SensorView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(SensorView));
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            _DeviceSensorTypeSelectorLabel = new System.Windows.Forms.Label();
            _DeviceThermalConstantSelectorLabel = new System.Windows.Forms.Label();
            __DeviceSensorTypeSelector = new Core.Controls.SelectorComboBox();
            __DeviceSensorTypeSelector.ValueSelected += new EventHandler<EventArgs>(DeviceSensorTypeSelector_ValueSelected);
            _DeviceThermalConstantSelector = new Core.Controls.SelectorNumeric();
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_DeviceSensorTypeSelectorLabel);
            _Panel.Controls.Add(_DeviceThermalConstantSelectorLabel);
            _Panel.Controls.Add(__DeviceSensorTypeSelector);
            _Panel.Controls.Add(_DeviceThermalConstantSelector);
            _Panel.Location = new System.Drawing.Point(24, 114);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(330, 93);
            _Panel.TabIndex = 0;
            // 
            // _DeviceSensorTypeSelectorLabel
            // 
            _DeviceSensorTypeSelectorLabel.AutoSize = true;
            _DeviceSensorTypeSelectorLabel.Location = new System.Drawing.Point(41, 48);
            _DeviceSensorTypeSelectorLabel.Name = "_DeviceSensorTypeSelectorLabel";
            _DeviceSensorTypeSelectorLabel.Size = new System.Drawing.Size(82, 17);
            _DeviceSensorTypeSelectorLabel.TabIndex = 7;
            _DeviceSensorTypeSelectorLabel.Text = "Sensor Type:";
            // 
            // _DeviceThermalConstantSelectorLabel
            // 
            _DeviceThermalConstantSelectorLabel.AutoSize = true;
            _DeviceThermalConstantSelectorLabel.Location = new System.Drawing.Point(11, 15);
            _DeviceThermalConstantSelectorLabel.Name = "_DeviceThermalConstantSelectorLabel";
            _DeviceThermalConstantSelectorLabel.Size = new System.Drawing.Size(113, 17);
            _DeviceThermalConstantSelectorLabel.TabIndex = 6;
            _DeviceThermalConstantSelectorLabel.Text = "Thermal Constant:";
            // 
            // _DeviceSensorTypeSelector
            // 
            __DeviceSensorTypeSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __DeviceSensorTypeSelector.DirtyBackColor = System.Drawing.Color.Orange;
            __DeviceSensorTypeSelector.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            __DeviceSensorTypeSelector.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __DeviceSensorTypeSelector.Location = new System.Drawing.Point(126, 44);
            __DeviceSensorTypeSelector.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __DeviceSensorTypeSelector.Name = "__DeviceSensorTypeSelector";
            __DeviceSensorTypeSelector.SelectorIcon = (System.Drawing.Image)resources.GetObject("_DeviceSensorTypeSelector.SelectorIcon");
            __DeviceSensorTypeSelector.Size = new System.Drawing.Size(182, 25);
            __DeviceSensorTypeSelector.TabIndex = 5;
            __DeviceSensorTypeSelector.Watermark = null;
            // 
            // _DeviceThermalConstantSelector
            // 
            _DeviceThermalConstantSelector.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _DeviceThermalConstantSelector.DirtyBackColor = System.Drawing.Color.Orange;
            _DeviceThermalConstantSelector.DirtyForeColor = System.Drawing.SystemColors.ActiveCaption;
            _DeviceThermalConstantSelector.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _DeviceThermalConstantSelector.Location = new System.Drawing.Point(126, 12);
            _DeviceThermalConstantSelector.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _DeviceThermalConstantSelector.Maximum = new decimal(new int[] { 500, 0, 0, 0 });
            _DeviceThermalConstantSelector.Minimum = new decimal(new int[] { 20, 0, 0, 0 });
            _DeviceThermalConstantSelector.Name = "_DeviceThermalConstantSelector";
            _DeviceThermalConstantSelector.SelectorIcon = (System.Drawing.Image)resources.GetObject("_DeviceThermalConstantSelector.SelectorIcon");
            _DeviceThermalConstantSelector.Size = new System.Drawing.Size(74, 25);
            _DeviceThermalConstantSelector.TabIndex = 4;
            _DeviceThermalConstantSelector.Value = new decimal(new int[] { 20, 0, 0, 0 });
            _DeviceThermalConstantSelector.Watermark = null;
            // 
            // SensorView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "SensorView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Label _DeviceSensorTypeSelectorLabel;
        private System.Windows.Forms.Label _DeviceThermalConstantSelectorLabel;
        private Core.Controls.SelectorComboBox __DeviceSensorTypeSelector;

        private Core.Controls.SelectorComboBox _DeviceSensorTypeSelector
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DeviceSensorTypeSelector;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DeviceSensorTypeSelector != null)
                {
                    __DeviceSensorTypeSelector.ValueSelected -= DeviceSensorTypeSelector_ValueSelected;
                }

                __DeviceSensorTypeSelector = value;
                if (__DeviceSensorTypeSelector != null)
                {
                    __DeviceSensorTypeSelector.ValueSelected += DeviceSensorTypeSelector_ValueSelected;
                }
            }
        }

        private Core.Controls.SelectorNumeric _DeviceThermalConstantSelector;
    }
}