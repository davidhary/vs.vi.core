﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Ats600.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Panel = new System.Windows.Forms.Panel();
            _ServiceRequestValuesTextBox = new System.Windows.Forms.TextBox();
            _SrqRefratoryTimeNumericLabel = new System.Windows.Forms.Label();
            _SrqRefratoryTimeNumeric = new Core.Controls.NumericUpDown();
            _SendComboBox = new System.Windows.Forms.ComboBox();
            _ReceiveTextBox = new System.Windows.Forms.TextBox();
            __ReceiveButton = new System.Windows.Forms.Button();
            __ReceiveButton.Click += new EventHandler(ReceiveButton_Click);
            __SendButton = new System.Windows.Forms.Button();
            __SendButton.Click += new EventHandler(SendButton_Click);
            _StatusByteLabel = new System.Windows.Forms.Label();
            __ReadStatusByteButton = new System.Windows.Forms.Button();
            __ReadStatusByteButton.Click += new EventHandler(ReadStatusByteButton_Click);
            __ClearErrorQueueButton = new System.Windows.Forms.Button();
            __ClearErrorQueueButton.Click += new EventHandler(ClearErrorQueueButton_Click);
            __ReadLastErrorButton = new System.Windows.Forms.Button();
            __ReadLastErrorButton.Click += new EventHandler(ReadLastErrorButton_Click);
            __ReadButton = new System.Windows.Forms.Button();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __InitializeButton = new System.Windows.Forms.Button();
            __InitializeButton.Click += new EventHandler(InitializeButton_Click);
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_SrqRefratoryTimeNumeric).BeginInit();
            _Layout.SuspendLayout();
            SuspendLayout();
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_ServiceRequestValuesTextBox);
            _Panel.Controls.Add(_SrqRefratoryTimeNumericLabel);
            _Panel.Controls.Add(_SrqRefratoryTimeNumeric);
            _Panel.Controls.Add(_SendComboBox);
            _Panel.Controls.Add(_ReceiveTextBox);
            _Panel.Controls.Add(__ReceiveButton);
            _Panel.Controls.Add(__SendButton);
            _Panel.Controls.Add(_StatusByteLabel);
            _Panel.Controls.Add(__ReadStatusByteButton);
            _Panel.Controls.Add(__ClearErrorQueueButton);
            _Panel.Controls.Add(__ReadLastErrorButton);
            _Panel.Controls.Add(__ReadButton);
            _Panel.Controls.Add(__InitializeButton);
            _Panel.Location = new System.Drawing.Point(13, 41);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(352, 240);
            _Panel.TabIndex = 0;
            // 
            // _ServiceRequestValuesTextBox
            // 
            _ServiceRequestValuesTextBox.Location = new System.Drawing.Point(127, 8);
            _ServiceRequestValuesTextBox.Multiline = true;
            _ServiceRequestValuesTextBox.Name = "_ServiceRequestValuesTextBox";
            _ServiceRequestValuesTextBox.ReadOnly = true;
            _ServiceRequestValuesTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            _ServiceRequestValuesTextBox.Size = new System.Drawing.Size(216, 62);
            _ServiceRequestValuesTextBox.TabIndex = 26;
            _ServiceRequestValuesTextBox.Text = "bit  Value" + '\r' + '\n' + "  2  Device Specific Error" + '\r' + '\n' + "  3  Temperature Event" + '\r' + '\n' + "  4  Message Avai" + "lable" + '\r' + '\n' + "  5  Standard Event Status" + '\r' + '\n' + "  6  Master Summery Status" + '\r' + '\n' + "  7  Ready";
            // 
            // _SrqRefratoryTimeNumericLabel
            // 
            _SrqRefratoryTimeNumericLabel.AutoSize = true;
            _SrqRefratoryTimeNumericLabel.Location = new System.Drawing.Point(223, 84);
            _SrqRefratoryTimeNumericLabel.Name = "_SrqRefratoryTimeNumericLabel";
            _SrqRefratoryTimeNumericLabel.Size = new System.Drawing.Size(72, 17);
            _SrqRefratoryTimeNumericLabel.TabIndex = 25;
            _SrqRefratoryTimeNumericLabel.Text = "Delay [ms]:";
            // 
            // _SrqRefratoryTimeNumeric
            // 
            _SrqRefratoryTimeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SrqRefratoryTimeNumeric.Location = new System.Drawing.Point(298, 80);
            _SrqRefratoryTimeNumeric.Name = "_SrqRefratoryTimeNumeric";
            _SrqRefratoryTimeNumeric.NullValue = new decimal(new int[] { 5, 0, 0, 0 });
            _SrqRefratoryTimeNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            _SrqRefratoryTimeNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            _SrqRefratoryTimeNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            _SrqRefratoryTimeNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            _SrqRefratoryTimeNumeric.Size = new System.Drawing.Size(45, 25);
            _SrqRefratoryTimeNumeric.TabIndex = 24;
            _SrqRefratoryTimeNumeric.Value = new decimal(new int[] { 5, 0, 0, 0 });
            // 
            // _SendComboBox
            // 
            _SendComboBox.FormattingEnabled = true;
            _SendComboBox.Items.AddRange(new object[] { "*CLS ", "*ESE 255", "*ESE?", "*ESR?", "*IDN? ", "*RST ", "*SRE 255", "*SRE?", "EROR?", "FLWR?", "FLRL?", "HEAD?", "LLIM?", "NEXT", "RAMP?", "SETD?", "TEMP?" });
            _SendComboBox.Location = new System.Drawing.Point(70, 149);
            _SendComboBox.Name = "_SendComboBox";
            _SendComboBox.Size = new System.Drawing.Size(273, 25);
            _SendComboBox.TabIndex = 23;
            _SendComboBox.Text = "*IDN? ";
            // 
            // _ReceiveTextBox
            // 
            _ReceiveTextBox.Location = new System.Drawing.Point(70, 182);
            _ReceiveTextBox.Multiline = true;
            _ReceiveTextBox.Name = "_ReceiveTextBox";
            _ReceiveTextBox.ReadOnly = true;
            _ReceiveTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _ReceiveTextBox.Size = new System.Drawing.Size(273, 50);
            _ReceiveTextBox.TabIndex = 22;
            // 
            // _ReceiveButton
            // 
            __ReceiveButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ReceiveButton.Location = new System.Drawing.Point(8, 181);
            __ReceiveButton.Name = "__ReceiveButton";
            __ReceiveButton.Size = new System.Drawing.Size(56, 30);
            __ReceiveButton.TabIndex = 21;
            __ReceiveButton.Text = "&Read:";
            __ReceiveButton.UseVisualStyleBackColor = true;
            // 
            // _SendButton
            // 
            __SendButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __SendButton.Location = new System.Drawing.Point(8, 146);
            __SendButton.Name = "__SendButton";
            __SendButton.Size = new System.Drawing.Size(56, 30);
            __SendButton.TabIndex = 20;
            __SendButton.Text = "&Send:";
            __SendButton.UseVisualStyleBackColor = true;
            // 
            // _StatusByteLabel
            // 
            _StatusByteLabel.AutoSize = true;
            _StatusByteLabel.Font = new System.Drawing.Font("Consolas", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _StatusByteLabel.Location = new System.Drawing.Point(146, 85);
            _StatusByteLabel.Name = "_StatusByteLabel";
            _StatusByteLabel.Size = new System.Drawing.Size(63, 15);
            _StatusByteLabel.TabIndex = 19;
            _StatusByteLabel.Text = "00000000";
            // 
            // _ReadStatusByteButton
            // 
            __ReadStatusByteButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ReadStatusByteButton.Location = new System.Drawing.Point(8, 77);
            __ReadStatusByteButton.Name = "__ReadStatusByteButton";
            __ReadStatusByteButton.Size = new System.Drawing.Size(135, 30);
            __ReadStatusByteButton.TabIndex = 18;
            __ReadStatusByteButton.Text = "Read Status Byte:";
            __ReadStatusByteButton.UseVisualStyleBackColor = true;
            // 
            // _ClearErrorQueueButton
            // 
            __ClearErrorQueueButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ClearErrorQueueButton.Location = new System.Drawing.Point(176, 112);
            __ClearErrorQueueButton.Name = "__ClearErrorQueueButton";
            __ClearErrorQueueButton.Size = new System.Drawing.Size(147, 30);
            __ClearErrorQueueButton.TabIndex = 16;
            __ClearErrorQueueButton.Text = "Clear Error Queue";
            __ClearErrorQueueButton.UseVisualStyleBackColor = true;
            // 
            // _ReadLastErrorButton
            // 
            __ReadLastErrorButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ReadLastErrorButton.Location = new System.Drawing.Point(8, 112);
            __ReadLastErrorButton.Name = "__ReadLastErrorButton";
            __ReadLastErrorButton.Size = new System.Drawing.Size(162, 30);
            __ReadLastErrorButton.TabIndex = 17;
            __ReadLastErrorButton.Text = "Read Last Device Error";
            __ReadLastErrorButton.UseVisualStyleBackColor = true;
            // 
            // _ReadButton
            // 
            __ReadButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ReadButton.Location = new System.Drawing.Point(8, 8);
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(104, 30);
            __ReadButton.TabIndex = 14;
            __ReadButton.Text = "&Read Temp";
            __ReadButton.UseVisualStyleBackColor = true;
            // 
            // _InitializeButton
            // 
            __InitializeButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __InitializeButton.Location = new System.Drawing.Point(8, 42);
            __InitializeButton.Name = "__InitializeButton";
            __InitializeButton.Size = new System.Drawing.Size(104, 30);
            __InitializeButton.TabIndex = 15;
            __InitializeButton.Text = "&Initialize";
            __InitializeButton.UseVisualStyleBackColor = true;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 1;
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "ReadingView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_SrqRefratoryTimeNumeric).EndInit();
            _Layout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TextBox _ServiceRequestValuesTextBox;
        private System.Windows.Forms.Label _SrqRefratoryTimeNumericLabel;
        private Core.Controls.NumericUpDown _SrqRefratoryTimeNumeric;
        private System.Windows.Forms.ComboBox _SendComboBox;
        private System.Windows.Forms.TextBox _ReceiveTextBox;
        private System.Windows.Forms.Button __ReceiveButton;

        private System.Windows.Forms.Button _ReceiveButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReceiveButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReceiveButton != null)
                {
                    __ReceiveButton.Click -= ReceiveButton_Click;
                }

                __ReceiveButton = value;
                if (__ReceiveButton != null)
                {
                    __ReceiveButton.Click += ReceiveButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __SendButton;

        private System.Windows.Forms.Button _SendButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SendButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SendButton != null)
                {
                    __SendButton.Click -= SendButton_Click;
                }

                __SendButton = value;
                if (__SendButton != null)
                {
                    __SendButton.Click += SendButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _StatusByteLabel;
        private System.Windows.Forms.Button __ReadStatusByteButton;

        private System.Windows.Forms.Button _ReadStatusByteButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadStatusByteButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadStatusByteButton != null)
                {
                    __ReadStatusByteButton.Click -= ReadStatusByteButton_Click;
                }

                __ReadStatusByteButton = value;
                if (__ReadStatusByteButton != null)
                {
                    __ReadStatusByteButton.Click += ReadStatusByteButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ClearErrorQueueButton;

        private System.Windows.Forms.Button _ClearErrorQueueButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearErrorQueueButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearErrorQueueButton != null)
                {
                    __ClearErrorQueueButton.Click -= ClearErrorQueueButton_Click;
                }

                __ClearErrorQueueButton = value;
                if (__ClearErrorQueueButton != null)
                {
                    __ClearErrorQueueButton.Click += ClearErrorQueueButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ReadLastErrorButton;

        private System.Windows.Forms.Button _ReadLastErrorButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadLastErrorButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadLastErrorButton != null)
                {
                    __ReadLastErrorButton.Click -= ReadLastErrorButton_Click;
                }

                __ReadLastErrorButton = value;
                if (__ReadLastErrorButton != null)
                {
                    __ReadLastErrorButton.Click += ReadLastErrorButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ReadButton;

        private System.Windows.Forms.Button _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __InitializeButton;

        private System.Windows.Forms.Button _InitializeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitializeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitializeButton != null)
                {
                    __InitializeButton.Click -= InitializeButton_Click;
                }

                __InitializeButton = value;
                if (__InitializeButton != null)
                {
                    __InitializeButton.Click += InitializeButton_Click;
                }
            }
        }
    }
}