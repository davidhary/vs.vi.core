﻿
namespace isr.VI.Ats600.Forms.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.Ats600;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI Ats600 Forms Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Ats600 Virtual Instrument Forms Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.Ats600.Forms";
    }
}