﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2700.Forms
{
    [DesignerGenerated()]
    public partial class SenseView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _TriggerDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _SenseRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _IntegrationPeriodNumeric = new System.Windows.Forms.NumericUpDown();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            _SenseRangeNumericLabel = new System.Windows.Forms.Label();
            _IntegrationPeriodNumericLabel = new System.Windows.Forms.Label();
            __SenseFunctionComboBox = new System.Windows.Forms.ComboBox();
            __SenseFunctionComboBox.SelectedIndexChanged += new EventHandler(SenseFunctionComboBox_SelectedIndexChanged);
            _SenseFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _SenseAutoRangeToggle = new System.Windows.Forms.CheckBox();
            __ApplySenseSettingsButton = new System.Windows.Forms.Button();
            __ApplySenseSettingsButton.Click += new EventHandler(ApplySenseSettingsButton_Click);
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_IntegrationPeriodNumeric).BeginInit();
            SuspendLayout();
            // 
            // _TriggerDelayNumeric
            // 
            _TriggerDelayNumeric.DecimalPlaces = 3;
            _TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerDelayNumeric.Location = new System.Drawing.Point(126, 111);
            _TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
            _TriggerDelayNumeric.Size = new System.Drawing.Size(76, 25);
            _TriggerDelayNumeric.TabIndex = 18;
            // 
            // _SenseRangeNumeric
            // 
            _SenseRangeNumeric.DecimalPlaces = 3;
            _SenseRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SenseRangeNumeric.Location = new System.Drawing.Point(126, 77);
            _SenseRangeNumeric.Maximum = new decimal(new int[] { 1010, 0, 0, 0 });
            _SenseRangeNumeric.Name = "_SenseRangeNumeric";
            _SenseRangeNumeric.Size = new System.Drawing.Size(76, 25);
            _SenseRangeNumeric.TabIndex = 15;
            _SenseRangeNumeric.Value = new decimal(new int[] { 105, 0, 0, 196608 });
            // 
            // _IntegrationPeriodNumeric
            // 
            _IntegrationPeriodNumeric.DecimalPlaces = 3;
            _IntegrationPeriodNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _IntegrationPeriodNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _IntegrationPeriodNumeric.Location = new System.Drawing.Point(126, 43);
            _IntegrationPeriodNumeric.Maximum = new decimal(new int[] { 16667, 0, 0, 131072 });
            _IntegrationPeriodNumeric.Minimum = new decimal(new int[] { 16, 0, 0, 196608 });
            _IntegrationPeriodNumeric.Name = "_IntegrationPeriodNumeric";
            _IntegrationPeriodNumeric.Size = new System.Drawing.Size(76, 25);
            _IntegrationPeriodNumeric.TabIndex = 13;
            _IntegrationPeriodNumeric.Value = new decimal(new int[] { 16667, 0, 0, 196608 });
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(16, 115);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(107, 17);
            _TriggerDelayNumericLabel.TabIndex = 17;
            _TriggerDelayNumericLabel.Text = "Trigger Delay [s]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseRangeNumericLabel
            // 
            _SenseRangeNumericLabel.AutoSize = true;
            _SenseRangeNumericLabel.Location = new System.Drawing.Point(56, 81);
            _SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel";
            _SenseRangeNumericLabel.Size = new System.Drawing.Size(68, 17);
            _SenseRangeNumericLabel.TabIndex = 14;
            _SenseRangeNumericLabel.Text = "Range [V]:";
            _SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _IntegrationPeriodNumericLabel
            // 
            _IntegrationPeriodNumericLabel.AutoSize = true;
            _IntegrationPeriodNumericLabel.Location = new System.Drawing.Point(33, 47);
            _IntegrationPeriodNumericLabel.Name = "_IntegrationPeriodNumericLabel";
            _IntegrationPeriodNumericLabel.Size = new System.Drawing.Size(91, 17);
            _IntegrationPeriodNumericLabel.TabIndex = 12;
            _IntegrationPeriodNumericLabel.Text = "Aperture [ms]:";
            _IntegrationPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseFunctionComboBox
            // 
            __SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __SenseFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __SenseFunctionComboBox.Items.AddRange(new object[] { "I", "V" });
            __SenseFunctionComboBox.Location = new System.Drawing.Point(126, 9);
            __SenseFunctionComboBox.Name = "__SenseFunctionComboBox";
            __SenseFunctionComboBox.Size = new System.Drawing.Size(243, 25);
            __SenseFunctionComboBox.TabIndex = 11;
            // 
            // _SenseFunctionComboBoxLabel
            // 
            _SenseFunctionComboBoxLabel.AutoSize = true;
            _SenseFunctionComboBoxLabel.Location = new System.Drawing.Point(65, 13);
            _SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel";
            _SenseFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SenseFunctionComboBoxLabel.TabIndex = 10;
            _SenseFunctionComboBoxLabel.Text = "Function:";
            _SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseAutoRangeToggle
            // 
            _SenseAutoRangeToggle.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SenseAutoRangeToggle.Location = new System.Drawing.Point(208, 79);
            _SenseAutoRangeToggle.Name = "_SenseAutoRangeToggle";
            _SenseAutoRangeToggle.Size = new System.Drawing.Size(103, 21);
            _SenseAutoRangeToggle.TabIndex = 16;
            _SenseAutoRangeToggle.Text = "Auto Range";
            _SenseAutoRangeToggle.UseVisualStyleBackColor = true;
            // 
            // _ApplySenseSettingsButton
            // 
            __ApplySenseSettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __ApplySenseSettingsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ApplySenseSettingsButton.Location = new System.Drawing.Point(311, 40);
            __ApplySenseSettingsButton.Name = "__ApplySenseSettingsButton";
            __ApplySenseSettingsButton.Size = new System.Drawing.Size(58, 30);
            __ApplySenseSettingsButton.TabIndex = 19;
            __ApplySenseSettingsButton.Text = "&Apply";
            __ApplySenseSettingsButton.UseVisualStyleBackColor = true;
            // 
            // SenseView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_TriggerDelayNumeric);
            Controls.Add(_SenseRangeNumeric);
            Controls.Add(_IntegrationPeriodNumeric);
            Controls.Add(_TriggerDelayNumericLabel);
            Controls.Add(_SenseRangeNumericLabel);
            Controls.Add(_IntegrationPeriodNumericLabel);
            Controls.Add(__SenseFunctionComboBox);
            Controls.Add(_SenseFunctionComboBoxLabel);
            Controls.Add(_SenseAutoRangeToggle);
            Controls.Add(__ApplySenseSettingsButton);
            Name = "SenseView";
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_IntegrationPeriodNumeric).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.NumericUpDown _TriggerDelayNumeric;
        private System.Windows.Forms.NumericUpDown _SenseRangeNumeric;
        private System.Windows.Forms.NumericUpDown _IntegrationPeriodNumeric;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
        private System.Windows.Forms.Label _SenseRangeNumericLabel;
        private System.Windows.Forms.Label _IntegrationPeriodNumericLabel;
        private System.Windows.Forms.ComboBox __SenseFunctionComboBox;

        private System.Windows.Forms.ComboBox _SenseFunctionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SenseFunctionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged -= SenseFunctionComboBox_SelectedIndexChanged;
                }

                __SenseFunctionComboBox = value;
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged += SenseFunctionComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.Label _SenseFunctionComboBoxLabel;
        private System.Windows.Forms.CheckBox _SenseAutoRangeToggle;
        private System.Windows.Forms.Button __ApplySenseSettingsButton;

        private System.Windows.Forms.Button _ApplySenseSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySenseSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click -= ApplySenseSettingsButton_Click;
                }

                __ApplySenseSettingsButton = value;
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click += ApplySenseSettingsButton_Click;
                }
            }
        }
    }
}