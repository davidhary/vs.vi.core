﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2700.Forms
{
    [DesignerGenerated()]
    public partial class ChannelView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _ChannelListComboBox = new System.Windows.Forms.ComboBox();
            _ClosedChannelsTextBoxLabel = new System.Windows.Forms.Label();
            _ClosedChannelsTextBox = new System.Windows.Forms.TextBox();
            __OpenAllButton = new System.Windows.Forms.Button();
            __OpenAllButton.Click += new EventHandler(OpenAllButton_Click);
            __OpenChannelsButton = new System.Windows.Forms.Button();
            __OpenChannelsButton.Click += new EventHandler(OpenChannelsButton_Click);
            __CloseOnlyButton = new System.Windows.Forms.Button();
            __CloseOnlyButton.Click += new EventHandler(CloseOnlyButton_Click);
            __CloseChannelsButton = new System.Windows.Forms.Button();
            __CloseChannelsButton.Click += new EventHandler(CloseChannelsButton_Click);
            _ChannelListComboBoxLabel = new System.Windows.Forms.Label();
            SuspendLayout();
            // 
            // _ChannelListComboBox
            // 
            _ChannelListComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _ChannelListComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ChannelListComboBox.Location = new System.Drawing.Point(9, 40);
            _ChannelListComboBox.Name = "_ChannelListComboBox";
            _ChannelListComboBox.Size = new System.Drawing.Size(365, 25);
            _ChannelListComboBox.TabIndex = 11;
            _ChannelListComboBox.Text = "(@ 201,203:205)";
            ToolTip.SetToolTip(_ChannelListComboBox, "Enter channel list, e.g., (@ 201,203:205)");
            // 
            // _ClosedChannelsTextBoxLabel
            // 
            _ClosedChannelsTextBoxLabel.Location = new System.Drawing.Point(9, 108);
            _ClosedChannelsTextBoxLabel.Name = "_ClosedChannelsTextBoxLabel";
            _ClosedChannelsTextBoxLabel.Size = new System.Drawing.Size(113, 21);
            _ClosedChannelsTextBoxLabel.TabIndex = 14;
            _ClosedChannelsTextBoxLabel.Text = "Closed Channels:";
            // 
            // _ClosedChannelsTextBox
            // 
            _ClosedChannelsTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _ClosedChannelsTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ClosedChannelsTextBox.Location = new System.Drawing.Point(9, 130);
            _ClosedChannelsTextBox.Multiline = true;
            _ClosedChannelsTextBox.Name = "_ClosedChannelsTextBox";
            _ClosedChannelsTextBox.ReadOnly = true;
            _ClosedChannelsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _ClosedChannelsTextBox.Size = new System.Drawing.Size(360, 178);
            _ClosedChannelsTextBox.TabIndex = 15;
            // 
            // _OpenAllButton
            // 
            __OpenAllButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __OpenAllButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __OpenAllButton.Location = new System.Drawing.Point(282, 75);
            __OpenAllButton.Name = "__OpenAllButton";
            __OpenAllButton.Size = new System.Drawing.Size(90, 30);
            __OpenAllButton.TabIndex = 13;
            __OpenAllButton.Text = "Open &All";
            __OpenAllButton.UseVisualStyleBackColor = true;
            // 
            // _OpenChannelsButton
            // 
            __OpenChannelsButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __OpenChannelsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __OpenChannelsButton.Location = new System.Drawing.Point(316, 8);
            __OpenChannelsButton.Name = "__OpenChannelsButton";
            __OpenChannelsButton.Size = new System.Drawing.Size(56, 30);
            __OpenChannelsButton.TabIndex = 10;
            __OpenChannelsButton.Text = "&Open";
            __OpenChannelsButton.UseVisualStyleBackColor = true;
            // 
            // _CloseOnlyButton
            // 
            __CloseOnlyButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __CloseOnlyButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __CloseOnlyButton.Location = new System.Drawing.Point(133, 75);
            __CloseOnlyButton.Name = "__CloseOnlyButton";
            __CloseOnlyButton.Size = new System.Drawing.Size(142, 30);
            __CloseOnlyButton.TabIndex = 12;
            __CloseOnlyButton.Text = "Open All and &Close";
            __CloseOnlyButton.UseVisualStyleBackColor = true;
            // 
            // _CloseChannelsButton
            // 
            __CloseChannelsButton.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right;
            __CloseChannelsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __CloseChannelsButton.Location = new System.Drawing.Point(254, 8);
            __CloseChannelsButton.Name = "__CloseChannelsButton";
            __CloseChannelsButton.Size = new System.Drawing.Size(57, 30);
            __CloseChannelsButton.TabIndex = 9;
            __CloseChannelsButton.Text = "&Close";
            __CloseChannelsButton.UseVisualStyleBackColor = true;
            // 
            // _ChannelListComboBoxLabel
            // 
            _ChannelListComboBoxLabel.Location = new System.Drawing.Point(9, 18);
            _ChannelListComboBoxLabel.Name = "_ChannelListComboBoxLabel";
            _ChannelListComboBoxLabel.Size = new System.Drawing.Size(84, 21);
            _ChannelListComboBoxLabel.TabIndex = 8;
            _ChannelListComboBoxLabel.Text = "Channel List:";
            // 
            // ChannelView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_ClosedChannelsTextBoxLabel);
            Controls.Add(_ClosedChannelsTextBox);
            Controls.Add(__OpenAllButton);
            Controls.Add(__OpenChannelsButton);
            Controls.Add(__CloseOnlyButton);
            Controls.Add(__CloseChannelsButton);
            Controls.Add(_ChannelListComboBox);
            Controls.Add(_ChannelListComboBoxLabel);
            Name = "ChannelView";
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.Label _ClosedChannelsTextBoxLabel;
        private System.Windows.Forms.TextBox _ClosedChannelsTextBox;
        private System.Windows.Forms.Button __OpenAllButton;

        private System.Windows.Forms.Button _OpenAllButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenAllButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenAllButton != null)
                {
                    __OpenAllButton.Click -= OpenAllButton_Click;
                }

                __OpenAllButton = value;
                if (__OpenAllButton != null)
                {
                    __OpenAllButton.Click += OpenAllButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __OpenChannelsButton;

        private System.Windows.Forms.Button _OpenChannelsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenChannelsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenChannelsButton != null)
                {
                    __OpenChannelsButton.Click -= OpenChannelsButton_Click;
                }

                __OpenChannelsButton = value;
                if (__OpenChannelsButton != null)
                {
                    __OpenChannelsButton.Click += OpenChannelsButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __CloseOnlyButton;

        private System.Windows.Forms.Button _CloseOnlyButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseOnlyButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseOnlyButton != null)
                {
                    __CloseOnlyButton.Click -= CloseOnlyButton_Click;
                }

                __CloseOnlyButton = value;
                if (__CloseOnlyButton != null)
                {
                    __CloseOnlyButton.Click += CloseOnlyButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __CloseChannelsButton;

        private System.Windows.Forms.Button _CloseChannelsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseChannelsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseChannelsButton != null)
                {
                    __CloseChannelsButton.Click -= CloseChannelsButton_Click;
                }

                __CloseChannelsButton = value;
                if (__CloseChannelsButton != null)
                {
                    __CloseChannelsButton.Click += CloseChannelsButton_Click;
                }
            }
        }

        private System.Windows.Forms.ComboBox _ChannelListComboBox;
        private System.Windows.Forms.Label _ChannelListComboBoxLabel;
    }
}