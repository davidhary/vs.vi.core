using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;
using isr.VI.Facade.DataGridViewExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2700.Forms
{

    /// <summary> A Reading view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ReadingsDataGridView.Name = "_ReadingsDataGridView";
            this.__ReadButton.Name = "_ReadButton";
            this.__InitiateButton.Name = "_InitiateButton";
            this.__TraceButton.Name = "_TraceButton";
            this.__ReadingComboBox.Name = "_ReadingComboBox";
            this.__AbortButton.Name = "_AbortButton";
            this.__ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2700Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2700Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindFormatSubsystem( value );
            this.BindTraceSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2700Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " FORMAT "

        /// <summary> Gets the Format subsystem. </summary>
        /// <value> The Format subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private FormatSubsystem FormatSubsystem { get; set; }

        /// <summary> Bind format subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindFormatSubsystem( K2700Device device )
        {
            if ( this.FormatSubsystem is object )
            {
                this.BindSubsystem( false, this.FormatSubsystem );
                this.FormatSubsystem = null;
            }

            if ( device is object )
            {
                this.FormatSubsystem = device.FormatSubsystem;
                this.BindSubsystem( true, this.FormatSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, FormatSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.FormatSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K2700.FormatSubsystem.Elements ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.FormatSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the format subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( FormatSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2700.FormatSubsystem.Elements ):
                    {
                        this._ReadingComboBox.ComboBox.ListReadingElementTypes( subsystem.Elements, ReadingElementTypes.Units );
                        break;
                    }
                    // Me._ReadingComboBox.ComboBox.ListEnumDescriptions(Of ReadingElementTypes)(subsystem.Elements, ReadingElementTypes.Units)
            }
        }

        /// <summary> Format subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FormatSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.FormatSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.FormatSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as FormatSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRACE "

        /// <summary> Gets the trace subsystem. </summary>
        /// <value> The trace subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TraceSubsystem TraceSubsystem { get; private set; }

        /// <summary> Bind Trace subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTraceSubsystem( K2700Device device )
        {
            if ( this.TraceSubsystem is object )
            {
                this.BindSubsystem( false, this.TraceSubsystem );
                this.TraceSubsystem = null;
            }

            if ( device is object )
            {
                this.TraceSubsystem = device.TraceSubsystem;
                this.BindSubsystem( true, this.TraceSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TraceSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TraceSubsystemPropertyChanged;
            }
            else
            {
                subsystem.PropertyChanged -= this.TraceSubsystemPropertyChanged;
            }
            // Me.AddRemoveBinding(Me._ReadingsCountLabel, add, NameOf(Control.Text), subsystem, NameOf(VI.K2700.TraceSubsystem.PointsCount))
        }

        /// <summary> Handle the Trace subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2700.TraceSubsystem.PointsCount ):
                    {
                        break;
                    }

                case nameof( K2700.TraceSubsystem.ActualPointCount ):
                    {
                        break;
                    }

                case nameof( K2700.TraceSubsystem.FirstPointNumber ):
                    {
                        break;
                    }

                case nameof( K2700.TraceSubsystem.LastPointNumber ):
                    {
                        break;
                    }
            }

            Application.DoEvents();
        }

        /// <summary> Trace subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TraceSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TraceSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TraceSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: READING "

        /// <summary> Selects a new reading to display. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
        /// <returns> The VI.ReadingElements. </returns>
        internal ReadingElementTypes SelectReading( ReadingElementTypes value )
        {
            if ( this.Device.IsDeviceOpen && value != ReadingElementTypes.None && value != this.SelectedReadingType )
            {
                _ = this._ReadingComboBox.ComboBox.SelectItem( value.ValueDescriptionPair() );
            }

            return this.SelectedReadingType;
        }

        /// <summary> Gets the type of the selected reading. </summary>
        /// <value> The type of the selected reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private ReadingElementTypes SelectedReadingType => ( ReadingElementTypes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ReadingComboBox.SelectedItem).Key );

        /// <summary> Abort button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" />
        ///                                                                   instance of this
        ///                                             <see cref="T:System.Windows.Forms.Control" /> 
        /// </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} aborting measurements(s)";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.Device.IsDeviceOpen )
                {
                    _ = this.PublishInfo( $"{activity};. " );
                    this.Device.TriggerSubsystem.Abort();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, ex.ToFullBlownString() );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by InitButton for click events. Initiates a reading for retrieval by
        /// way of the service request event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitiateButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} initiating measurements(s)";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                // clear execution state before enabling events
                this.Device.ClearExecutionState();

                // set the service request
                _ = this.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask( ( int ) MeasurementEvents.All );
                this.Device.Session.ApplyServiceRequestEnableBitmask( this.Device.Session.DefaultOperationServiceRequestEnableBitmask );

                // trigger the initiation of the measurement letting the service request do the rest.
                this.Device.ClearExecutionState();
                this.Device.TriggerSubsystem.Initiate();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, ex.ToFullBlownString() );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reading combo box selected value changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadingComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} selecting a reading to display";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.MeasureSubsystem.SelectActiveReading( this.SelectedReadingType );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, ex.ToFullBlownString() );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _ReadButton for click events. Query the Device for a reading.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} reading";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                _ = this.Device.SystemSubsystem.QueryFrontTerminalsSelected();
                _ = this.Device.MeasureSubsystem.Read();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, ex.ToFullBlownString() );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Handles the DataError event of the _dataGridView control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
        ///                       event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadingsDataGridView_DataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            try
            {
                // prevent error reporting when adding a new row or editing a cell
                if ( sender is DataGridView grid )
                {
                    if ( grid.CurrentRow is object && grid.CurrentRow.IsNewRow )
                        return;
                    if ( grid.IsCurrentCellInEditMode )
                        return;
                    if ( grid.IsCurrentRowDirty )
                        return;
                    _ = this.Talker.Publish( TraceEventType.Error, My.MyLibrary.TraceEventId, $"{e.Exception.Message} occurred editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString()}" );
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, "Exception occurred editing table" );
                }
            }
            catch
            {
            }
        }

        /// <summary> Reads buffer button click. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                                                   instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} reading readings";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                _ = this._ReadingsDataGridView.Display( new List<Readings>(), false );
                IEnumerable<ReadingAmounts> values = this.Device.TraceSubsystem.QueryReadings( this.Device.MeasureSubsystem.ReadingAmounts );
                this._ReadingsCountLabel.Text = values?.Count().ToString();
                _ = this._ReadingsDataGridView.Display( values, false );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, ex.ToFullBlownString() );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the buffer display button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearBufferDisplayButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} clearing buffer display";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                _ = this._ReadingsDataGridView.Display( new List<Readings>(), false );
                this._ReadingsCountLabel.Text = "0";
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, ex.ToFullBlownString() );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
