using System;
using System.Collections.Generic;

namespace isr.VI.K2700
{

    /// <summary> Information about the version of a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks>
        /// KEITHLEY INSTRUMENTS INC., Model 2700, xxxxxxx, yyyyy/zzz<para>
        /// where</para><para>xxxxxxx is the serial number.</para><para>
        /// yyyyy/zzz Is the firmware revision levels of the digital board ROM And display board
        /// ROM.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument <see cref="VersionInfoBase.FirmwareRevisionElements">board
        ///                         revisions</see>
        ///                         e.g., <c>yyyyy/zzz</c> for the digital and display boards. </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '/' ) );

                // Rev: yyyyy/ZZZ
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Digital.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Display.ToString(), revSections.Dequeue().Trim() );
            }
        }
    }
}
