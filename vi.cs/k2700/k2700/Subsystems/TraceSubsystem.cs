namespace isr.VI.K2700
{

    /// <summary> Defines a Trace Subsystem for a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TraceSubsystem : TraceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TraceSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.PointsCount = 10;
            this.FeedSource = FeedSources.None;
            this.FeedControl = FeedControls.Never;
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the Clear Buffer command. </summary>
        /// <value> The ClearBuffer command. </value>
        protected override string ClearBufferCommand { get; set; } = ":TRAC:CLE";

        /// <summary> Gets or sets the points count query command. </summary>
        /// <value> The points count query command. </value>
        protected override string PointsCountQueryCommand { get; set; } = ":TRAC:POIN:COUN?";

        /// <summary> Gets or sets the points count command format. </summary>
        /// <value> The points count command format. </value>
        protected override string PointsCountCommandFormat { get; set; } = ":TRAC:POIN:COUN {0}";

        /// <summary> Gets or sets the feed Control query command. </summary>
        /// <value> The write feed Control query command. </value>
        protected override string FeedControlQueryCommand { get; set; } = ":TRAC:FEED:CONTROL?";

        /// <summary> Gets or sets the feed Control command format. </summary>
        /// <value> The write feed Control command format. </value>
        protected override string FeedControlCommandFormat { get; set; } = ":TRAC:FEED:CONTROL {0}";

        /// <summary> Gets or sets the feed source query command. </summary>
        /// <value> The write feed source query command. </value>
        protected override string FeedSourceQueryCommand { get; set; } = ":TRAC:FEED?";

        /// <summary> Gets or sets the feed source command format. </summary>
        /// <value> The write feed source command format. </value>
        protected override string FeedSourceCommandFormat { get; set; } = ":TRAC:FEED {0}";

        #endregion

    }
}
