using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2700
{

    /// <summary> Implements a Keithley 2700 instrument device. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class K2700Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="K2700Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K2700Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected K2700Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            My.MySettings.Default.PropertyChanged += this.Settings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static K2700Device Create()
        {
            K2700Device device = null;
            try
            {
                device = new K2700Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static K2700Device Validated( K2700Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( K2700Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( null );
                this.BindMeasureSubsystem( null );
                this.BindFormatSubsystem( null );
                this.BindRouteSubsystem( null );
                this.BindSenseSubsystem( null );
                this.BindSenseVoltageSubsystem( null );
                this.BindSenseCurrentSubsystem( null );
                this.BindSenseResistanceFourWireSubsystem( null );
                this.BindTraceSubsystem( null );
                this.BindTriggerSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                // must be added before the format system
                this.BindMeasureSubsystem( new MeasureSubsystem( this.StatusSubsystem ) );
                // the measure subsystem reading are initialized when the format system is reset
                this.BindFormatSubsystem( new FormatSubsystem( this.StatusSubsystem ) );
                this.BindRouteSubsystem( new RouteSubsystem( this.StatusSubsystem ) );
                this.BindSenseSubsystem( new SenseSubsystem( this.StatusSubsystem ) );
                this.BindSenseVoltageSubsystem( new SenseVoltageSubsystem( this.StatusSubsystem ) );
                this.BindSenseCurrentSubsystem( new SenseCurrentSubsystem( this.StatusSubsystem ) );
                this.BindSenseResistanceFourWireSubsystem( new SenseResistanceFourWireSubsystem( this.StatusSubsystem ) );
                this.BindTraceSubsystem( new TraceSubsystem( this.StatusSubsystem ) );
                this.BindTriggerSubsystem( new TriggerSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        #region " FORMAT "

        /// <summary> Gets or sets the Format Subsystem. </summary>
        /// <value> The Format Subsystem. </value>
        public FormatSubsystem FormatSubsystem { get; private set; }

        /// <summary> Binds the Format subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindFormatSubsystem( FormatSubsystem subsystem )
        {
            if ( this.FormatSubsystem is object )
            {
                this.FormatSubsystem.PropertyChanged -= this.FormatSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.FormatSubsystem );
                this.FormatSubsystem = null;
            }

            this.FormatSubsystem = subsystem;
            if ( this.FormatSubsystem is object )
            {
                this.FormatSubsystem.PropertyChanged += this.FormatSubsystemPropertyChanged;
                this.Subsystems.Add( this.FormatSubsystem );
                this.HandlePropertyChanged( this.FormatSubsystem, nameof( FormatSubsystemBase.Elements ) );
            }
        }

        /// <summary> Handle the Format subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( FormatSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( FormatSubsystemBase.Elements ):
                    {
                        this.MeasureSubsystem.ReadingAmounts.Initialize( subsystem.Elements );
                        break;
                    }
            }
        }

        /// <summary> Format subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FormatSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.FormatSubsystem )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as FormatSubsystem, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        /// <summary> Gets or sets the Measure Subsystem. </summary>
        /// <value> The Measure Subsystem. </value>
        public MeasureSubsystem MeasureSubsystem { get; private set; }

        /// <summary> Binds the Measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureSubsystem( MeasureSubsystem subsystem )
        {
            if ( this.MeasureSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            this.MeasureSubsystem = subsystem;
            if ( this.MeasureSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureSubsystem );
            }
        }

        /// <summary> Gets or sets the Route Subsystem. </summary>
        /// <value> The Route Subsystem. </value>
        public RouteSubsystem RouteSubsystem { get; private set; }

        /// <summary> Binds the Route subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindRouteSubsystem( RouteSubsystem subsystem )
        {
            if ( this.RouteSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.RouteSubsystem );
                this.RouteSubsystem = null;
            }

            this.RouteSubsystem = subsystem;
            if ( this.RouteSubsystem is object )
            {
                this.Subsystems.Add( this.RouteSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Subsystem. </summary>
        /// <value> The Sense Subsystem. </value>
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Binds the Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseSubsystem( SenseSubsystem subsystem )
        {
            if ( this.SenseSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            this.SenseSubsystem = subsystem;
            if ( this.SenseSubsystem is object )
            {
                this.Subsystems.Add( this.SenseSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Current Subsystem. </summary>
        /// <value> The Sense Current Subsystem. </value>
        public SenseCurrentSubsystem SenseCurrentSubsystem { get; private set; }

        /// <summary> Binds the Sense Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseCurrentSubsystem( SenseCurrentSubsystem subsystem )
        {
            if ( this.SenseCurrentSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseCurrentSubsystem );
                this.SenseCurrentSubsystem = null;
            }

            this.SenseCurrentSubsystem = subsystem;
            if ( this.SenseCurrentSubsystem is object )
            {
                this.Subsystems.Add( this.SenseCurrentSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Voltage Subsystem. </summary>
        /// <value> The Sense Voltage Subsystem. </value>
        public SenseVoltageSubsystem SenseVoltageSubsystem { get; private set; }

        /// <summary> Binds the Sense Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseVoltageSubsystem( SenseVoltageSubsystem subsystem )
        {
            if ( this.SenseVoltageSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseVoltageSubsystem );
                this.SenseVoltageSubsystem = null;
            }

            this.SenseVoltageSubsystem = subsystem;
            if ( this.SenseVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.SenseVoltageSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Four Wire Resistance Subsystem. </summary>
        /// <value> The Sense Four Wire Resistance Subsystem. </value>
        public SenseResistanceFourWireSubsystem SenseResistanceFourWireSubsystem { get; private set; }

        /// <summary> Binds the Sense Four Wire Resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseResistanceFourWireSubsystem( SenseResistanceFourWireSubsystem subsystem )
        {
            if ( this.SenseResistanceFourWireSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseResistanceFourWireSubsystem );
                this.SenseResistanceFourWireSubsystem = null;
            }

            this.SenseResistanceFourWireSubsystem = subsystem;
            if ( this.SenseResistanceFourWireSubsystem is object )
            {
                this.Subsystems.Add( this.SenseResistanceFourWireSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Resistance Subsystem. </summary>
        /// <value> The Sense Resistance Subsystem. </value>
        public SenseResistanceFourWireSubsystem SenseResistanceSubsystem { get; private set; }

        /// <summary> Binds the Sense Resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseResistanceSubsystem( SenseResistanceFourWireSubsystem subsystem )
        {
            if ( this.SenseResistanceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseResistanceSubsystem );
                this.SenseResistanceSubsystem = null;
            }

            this.SenseResistanceSubsystem = subsystem;
            if ( this.SenseResistanceSubsystem is object )
            {
                this.Subsystems.Add( this.SenseResistanceSubsystem );
            }
        }

        /// <summary> Gets or sets the Trace Subsystem. </summary>
        /// <value> The Trace Subsystem. </value>
        public TraceSubsystem TraceSubsystem { get; private set; }

        /// <summary> Binds the Trace subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTraceSubsystem( TraceSubsystem subsystem )
        {
            if ( this.TraceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.TraceSubsystem );
                this.TraceSubsystem = null;
            }

            this.TraceSubsystem = subsystem;
            if ( this.TraceSubsystem is object )
            {
                this.Subsystems.Add( this.TraceSubsystem );
            }
        }

        /// <summary> Gets or sets the Trigger Subsystem. </summary>
        /// <value> The Trigger Subsystem. </value>
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Binds the Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTriggerSubsystem( TriggerSubsystem subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( this.TriggerSubsystem is object )
            {
                this.Subsystems.Add( this.TriggerSubsystem );
            }
        }


        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "K2700 Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.ReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.StatusReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime;
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Settings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
