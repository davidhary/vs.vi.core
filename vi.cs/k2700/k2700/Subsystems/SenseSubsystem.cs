namespace isr.VI.K2700
{

    /// <summary> Defines a SCPI Sense Subsystem for a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class SenseSubsystem : SenseSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.SupportedFunctionModes = SenseFunctionModes.CurrentDC | SenseFunctionModes.VoltageDC | SenseFunctionModes.ResistanceFourWire;
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.FunctionModeRanges[( int ) SenseFunctionModes.Current] = new Core.Primitives.RangeR( 0d, 10d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.CurrentAC] = new Core.Primitives.RangeR( 0d, 10d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.CurrentDC] = new Core.Primitives.RangeR( 0d, 10d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.Voltage] = new Core.Primitives.RangeR( 0d, 1000d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.VoltageAC] = new Core.Primitives.RangeR( 0d, 1000d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.VoltageDC] = new Core.Primitives.RangeR( 0d, 1000d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.ResistanceFourWire] = new Core.Primitives.RangeR( 0d, 2000000d );
            this.FunctionModeRanges[( int ) SenseFunctionModes.Resistance] = new Core.Primitives.RangeR( 0d, 1000000000d );
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.Current] = 3;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.CurrentAC] = 3;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.CurrentDC] = 3;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.Voltage] = 3;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.VoltageAC] = 3;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.VoltageDC] = 3;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.ResistanceFourWire] = 0;
            this.FunctionModeDecimalPlaces[( int ) SenseFunctionModes.Resistance] = 0;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.FunctionMode = SenseFunctionModes.VoltageDC;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:CURR:RANG:AUTO?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:CURR:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:CURR:NPLC?";

        #endregion

        #region " PROTECTION LEVEL "

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected override string ProtectionLevelCommandFormat { get; set; } = ":SENS:CURR:PROT {0}";

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected override string ProtectionLevelQueryCommand { get; set; } = ":SENS:CURR:PROT?";

        #endregion

        #region " LATEST DATA "

        /// <summary> Gets or sets the latest data query command. </summary>
        /// <value> The latest data query command. </value>
        protected override string LatestDataQueryCommand { get; set; } = ":SENSE:DATA:LAT?";

        #endregion

        #endregion

    }
}
