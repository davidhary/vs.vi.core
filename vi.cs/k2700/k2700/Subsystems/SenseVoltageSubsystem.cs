namespace isr.VI.K2700
{

    /// <summary> Defines a SCPI Sense Voltage Subsystem for a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2014-03-01, 3.0.5173. </para>
    /// </remarks>
    public class SenseVoltageSubsystem : SenseFunctionSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseVoltageSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:VOLT:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:VOLT:RANG:AUTO?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:VOLT:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:VOLT:NPLC?";

        #endregion

        #region " PROTECTION "

        /// <summary> Gets or sets the Protection enabled command Format. </summary>
        /// <value> The Protection enabled query command. </value>
        protected override string ProtectionEnabledCommandFormat { get; set; } = ":SENS:VOLT:PROT:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Protection enabled query command. </summary>
        /// <value> The Protection enabled query command. </value>
        protected override string ProtectionEnabledQueryCommand { get; set; } = ":SENS:VOLT:PROT:STAT?";

        #endregion

        #region " PROTECTION LEVEL "

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected override string ProtectionLevelCommandFormat { get; set; } = ":SENS:VOLT:PROT {0}";

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected override string ProtectionLevelQueryCommand { get; set; } = ":SENS:VOLT:PROT?";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SENS:VOLT:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SENS:VOLT:RANG?";

        #endregion

        #endregion

    }
}
