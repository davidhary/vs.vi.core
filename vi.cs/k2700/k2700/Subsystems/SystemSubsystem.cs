namespace isr.VI.K2700
{

    /// <summary> Defines a System Subsystem for a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.AutoZeroEnabled = true;
            this.BeeperEnabled = true;
            this.FourWireSenseEnabled = false;
            _ = this.QueryFrontTerminalsSelected();
        }

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Gets or sets the automatic zero enabled query command. </summary>
        /// <value> The automatic zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = ":SYST:AZER:STAT?";

        /// <summary> Gets or sets the automatic zero enabled command format. </summary>
        /// <value> The automatic zero enabled command format. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = ":SYST:AZER:STAT {0:'ON';'ON';'OFF'}";

        #endregion

        #region " BEEPER ENABLED + IMMEDIATE "

        /// <summary> Gets or sets the Beeper enabled query command. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledQueryCommand { get; set; } = ":SYST:BEEP:STAT?";

        /// <summary> Gets or sets the Beeper enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledCommandFormat { get; set; } = ":SYST:BEEP:STAT {0:'1';'1';'0'}";

        /// <summary> Gets or sets the beeper immediate command format. </summary>
        /// <value> The beeper immediate command format. </value>
        protected override string BeeperImmediateCommandFormat { get; set; } = ":SYST:BEEP:IMM {0}, {1}";

        #endregion

        #region " FOUR WIRE SENSE ENABLED "

        /// <summary> Gets or sets the Four Wire Sense enabled query command. </summary>
        /// <value> The Four Wire Sense enabled query command. </value>
        protected override string FourWireSenseEnabledQueryCommand { get; set; } = ":SYST:RSEN?";

        /// <summary> Gets or sets the Four Wire Sense enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:RSEN {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Four Wire Sense enabled query command. </value>
        protected override string FourWireSenseEnabledCommandFormat { get; set; } = ":SYST:RSEN {0:'ON';'ON';'OFF'}";

        #endregion

        #region " FRONT SWITCHED "

        /// <summary> Gets or sets the Front Terminals Selected query command. </summary>
        /// <value> The Front Terminals Selected query command. </value>
        protected override string FrontTerminalsSelectedQueryCommand { get; set; } = ":SYST:FRSW?";

        /// <summary> Gets or sets the Front Terminals Selected command Format. </summary>
        /// <remarks> SCPI: ":SYST:FRSW {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Front Terminals Selected query command. </value>
        protected override string FrontTerminalsSelectedCommandFormat { get; set; } = string.Empty;

        #endregion

    }
}
