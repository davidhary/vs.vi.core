namespace isr.VI.K2700
{

    /// <summary> Defines a Route Subsystem for a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class RouteSubsystem : RouteSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public RouteSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " CHANNEL "

        /// <summary> Gets or sets the closed Channel query command. </summary>
        /// <value> The closed Channel query command. </value>
        protected override string ClosedChannelQueryCommand { get; set; } = ":ROUT:CLOS?";

        /// <summary> Gets or sets the closed Channel command format. </summary>
        /// <value> The closed Channel command format. </value>
        protected override string ClosedChannelCommandFormat { get; set; } = ":ROUT:CLOS {0}";

        /// <summary> Gets or sets the open Channel command format. </summary>
        /// <value> The open Channel command format. </value>
        protected override string OpenChannelCommandFormat { get; set; } = ":ROUT:OPEN {0}";

        #endregion

        #region " CHANNELS "

        /// <summary> Gets or sets the closed channels query command. </summary>
        /// <value> The closed channels query command. </value>
        protected override string ClosedChannelsQueryCommand { get; set; } = ":ROUT:MULT:CLOS?";

        /// <summary> Gets or sets the closed channels command format. </summary>
        /// <value> The closed channels command format. </value>
        protected override string ClosedChannelsCommandFormat { get; set; } = ":ROUT:MULT:CLOS {0}";

        /// <summary> Gets or sets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        protected override string OpenChannelsCommandFormat { get; set; } = ":ROUT:MULT:OPEN {0}";

        #endregion

        #region " CHANNELS "

        /// <summary> Gets or sets the recall channel pattern command format. </summary>
        /// <value> The recall channel pattern command format. </value>
        protected override string RecallChannelPatternCommandFormat { get; set; } = ":ROUT:MEM:REC M{0}";

        /// <summary> Gets or sets the save channel pattern command format. </summary>
        /// <value> The save channel pattern command format. </value>
        protected override string SaveChannelPatternCommandFormat { get; set; } = ":ROUT:MEM:SAVE M{0}";

        /// <summary> Gets or sets the open channels command. </summary>
        /// <value> The open channels command. </value>
        protected override string OpenChannelsCommand { get; set; } = ":ROUT:OPEN:ALL";

        #endregion

        #region " SCAN LIST "

        /// <summary> Gets or sets the scan list command query. </summary>
        /// <value> The scan list query command. </value>
        protected override string ScanListQueryCommand { get; set; } = ":ROUT:SCAN?";

        /// <summary> Gets or sets the scan list command format. </summary>
        /// <value> The scan list command format. </value>
        protected override string ScanListCommandFormat { get; set; } = ":ROUT:SCAN {0}";

        #endregion

        #region " SLOT CARD TYPE "

        /// <summary> Gets or sets the slot card type query command format. </summary>
        /// <value> The slot card type query command format. </value>
        protected override string SlotCardTypeQueryCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:CTYPE?";

        /// <summary> Gets or sets the slot card type command format. </summary>
        /// <value> The slot card type command format. </value>
        protected override string SlotCardTypeCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:CTYPER {1}";

        #endregion

        #region " SLOT CARD SETTLING TIME "

        /// <summary> Gets or sets the slot card settling time query command format. </summary>
        /// <value> The slot card settling time query command format. </value>
        protected override string SlotCardSettlingTimeQueryCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:STIME?";

        /// <summary> Gets or sets the slot card settling time command format. </summary>
        /// <value> The slot card settling time command format. </value>
        protected override string SlotCardSettlingTimeCommandFormat { get; set; } = ":ROUT:CONF:SLOT{0}:STIME {1}";

        #endregion

        #region " TERMINAL MODE "

        /// <summary> Gets or sets the terminals mode query command. </summary>
        /// <value> The terminals mode command. </value>
        protected override string TerminalsModeQueryCommand { get; set; } = ":ROUT:TERM?";

        /// <summary> Gets or sets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        protected override string TerminalsModeCommandFormat { get; set; } = ":ROUT:TERM {0}";

        #endregion

    }
}
