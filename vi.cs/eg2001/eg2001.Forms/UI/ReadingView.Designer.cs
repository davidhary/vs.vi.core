﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.EG2001.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _SentGroupBox = new System.Windows.Forms.GroupBox();
            _SendMessageLabel = new System.Windows.Forms.Label();
            _UnhandledSendLabel = new System.Windows.Forms.Label();
            _ReceivedGroupBox = new System.Windows.Forms.GroupBox();
            _ReceivedMessageLabel = new System.Windows.Forms.Label();
            _UnhandledMessageLabel = new System.Windows.Forms.Label();
            _ReceivedMessagesGroupBox = new System.Windows.Forms.GroupBox();
            _WaferStartReceivedLabel = new System.Windows.Forms.Label();
            _TestStartAttributeLabel = new System.Windows.Forms.Label();
            _TestStartedLabel = new System.Windows.Forms.Label();
            _PatternCompleteLabel = new System.Windows.Forms.Label();
            _TestCompleteLabel = new System.Windows.Forms.Label();
            _EmulatedReplyComboBox = new System.Windows.Forms.ComboBox();
            _CommandComboBox = new System.Windows.Forms.ComboBox();
            _LastMessageTextBox = new System.Windows.Forms.TextBox();
            _LastMessageTextBoxLabel = new System.Windows.Forms.Label();
            __EmulateButton = new System.Windows.Forms.Button();
            __EmulateButton.Click += new EventHandler(EmulateButton_Click);
            __WriteButton = new System.Windows.Forms.Button();
            __WriteButton.Click += new EventHandler(WriteButton_Click);
            _SentGroupBox.SuspendLayout();
            _ReceivedGroupBox.SuspendLayout();
            _ReceivedMessagesGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _SentGroupBox
            // 
            _SentGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _SentGroupBox.Controls.Add(_SendMessageLabel);
            _SentGroupBox.Controls.Add(_UnhandledSendLabel);
            _SentGroupBox.Location = new System.Drawing.Point(153, 143);
            _SentGroupBox.Name = "_SentGroupBox";
            _SentGroupBox.Size = new System.Drawing.Size(229, 72);
            _SentGroupBox.TabIndex = 24;
            _SentGroupBox.TabStop = false;
            _SentGroupBox.Text = "Message Sent";
            // 
            // _SendMessageLabel
            // 
            _SendMessageLabel.AutoSize = true;
            _SendMessageLabel.Location = new System.Drawing.Point(6, 46);
            _SendMessageLabel.Name = "_SendMessageLabel";
            _SendMessageLabel.Size = new System.Drawing.Size(46, 17);
            _SendMessageLabel.TabIndex = 1;
            _SendMessageLabel.Text = "Sent ...";
            // 
            // _UnhandledSendLabel
            // 
            _UnhandledSendLabel.AutoSize = true;
            _UnhandledSendLabel.Location = new System.Drawing.Point(6, 22);
            _UnhandledSendLabel.Name = "_UnhandledSendLabel";
            _UnhandledSendLabel.Size = new System.Drawing.Size(56, 17);
            _UnhandledSendLabel.TabIndex = 0;
            _UnhandledSendLabel.Text = "? Sent: ..";
            // 
            // _ReceivedGroupBox
            // 
            _ReceivedGroupBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _ReceivedGroupBox.Controls.Add(_ReceivedMessageLabel);
            _ReceivedGroupBox.Controls.Add(_UnhandledMessageLabel);
            _ReceivedGroupBox.Location = new System.Drawing.Point(153, 70);
            _ReceivedGroupBox.Name = "_ReceivedGroupBox";
            _ReceivedGroupBox.Size = new System.Drawing.Size(229, 67);
            _ReceivedGroupBox.TabIndex = 23;
            _ReceivedGroupBox.TabStop = false;
            _ReceivedGroupBox.Text = "Received Messages";
            // 
            // _ReceivedMessageLabel
            // 
            _ReceivedMessageLabel.AutoSize = true;
            _ReceivedMessageLabel.Location = new System.Drawing.Point(6, 41);
            _ReceivedMessageLabel.Name = "_ReceivedMessageLabel";
            _ReceivedMessageLabel.Size = new System.Drawing.Size(73, 17);
            _ReceivedMessageLabel.TabIndex = 1;
            _ReceivedMessageLabel.Text = "Received: ..";
            // 
            // _UnhandledMessageLabel
            // 
            _UnhandledMessageLabel.AutoSize = true;
            _UnhandledMessageLabel.BackColor = System.Drawing.Color.LightGreen;
            _UnhandledMessageLabel.Location = new System.Drawing.Point(6, 19);
            _UnhandledMessageLabel.Name = "_UnhandledMessageLabel";
            _UnhandledMessageLabel.Size = new System.Drawing.Size(83, 17);
            _UnhandledMessageLabel.TabIndex = 0;
            _UnhandledMessageLabel.Text = "? Received: ..";
            // 
            // _ReceivedMessagesGroupBox
            // 
            _ReceivedMessagesGroupBox.Controls.Add(_WaferStartReceivedLabel);
            _ReceivedMessagesGroupBox.Controls.Add(_TestStartAttributeLabel);
            _ReceivedMessagesGroupBox.Controls.Add(_TestStartedLabel);
            _ReceivedMessagesGroupBox.Controls.Add(_PatternCompleteLabel);
            _ReceivedMessagesGroupBox.Controls.Add(_TestCompleteLabel);
            _ReceivedMessagesGroupBox.Location = new System.Drawing.Point(5, 69);
            _ReceivedMessagesGroupBox.Name = "_ReceivedMessagesGroupBox";
            _ReceivedMessagesGroupBox.Size = new System.Drawing.Size(131, 133);
            _ReceivedMessagesGroupBox.TabIndex = 22;
            _ReceivedMessagesGroupBox.TabStop = false;
            _ReceivedMessagesGroupBox.Text = "Prober Cycle";
            // 
            // _WaferStartReceivedLabel
            // 
            _WaferStartReceivedLabel.AutoSize = true;
            _WaferStartReceivedLabel.Location = new System.Drawing.Point(6, 19);
            _WaferStartReceivedLabel.Name = "_WaferStartReceivedLabel";
            _WaferStartReceivedLabel.Size = new System.Drawing.Size(88, 17);
            _WaferStartReceivedLabel.TabIndex = 0;
            _WaferStartReceivedLabel.Text = "Wafer Started";
            // 
            // _TestStartAttributeLabel
            // 
            _TestStartAttributeLabel.AutoSize = true;
            _TestStartAttributeLabel.Location = new System.Drawing.Point(6, 41);
            _TestStartAttributeLabel.Name = "_TestStartAttributeLabel";
            _TestStartAttributeLabel.Size = new System.Drawing.Size(98, 17);
            _TestStartAttributeLabel.TabIndex = 1;
            _TestStartAttributeLabel.Text = "1st Test Started";
            // 
            // _TestStartedLabel
            // 
            _TestStartedLabel.AutoSize = true;
            _TestStartedLabel.Location = new System.Drawing.Point(6, 63);
            _TestStartedLabel.Name = "_TestStartedLabel";
            _TestStartedLabel.Size = new System.Drawing.Size(77, 17);
            _TestStartedLabel.TabIndex = 2;
            _TestStartedLabel.Text = "Test Started";
            // 
            // _PatternCompleteLabel
            // 
            _PatternCompleteLabel.AutoSize = true;
            _PatternCompleteLabel.Location = new System.Drawing.Point(6, 107);
            _PatternCompleteLabel.Name = "_PatternCompleteLabel";
            _PatternCompleteLabel.Size = new System.Drawing.Size(109, 17);
            _PatternCompleteLabel.TabIndex = 4;
            _PatternCompleteLabel.Text = "Pattern Complete";
            // 
            // _TestCompleteLabel
            // 
            _TestCompleteLabel.AutoSize = true;
            _TestCompleteLabel.Location = new System.Drawing.Point(6, 85);
            _TestCompleteLabel.Name = "_TestCompleteLabel";
            _TestCompleteLabel.Size = new System.Drawing.Size(91, 17);
            _TestCompleteLabel.TabIndex = 3;
            _TestCompleteLabel.Text = "Test Complete";
            // 
            // _EmulatedReplyComboBox
            // 
            _EmulatedReplyComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _EmulatedReplyComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _EmulatedReplyComboBox.FormattingEnabled = true;
            _EmulatedReplyComboBox.Location = new System.Drawing.Point(86, 40);
            _EmulatedReplyComboBox.Name = "_EmulatedReplyComboBox";
            _EmulatedReplyComboBox.Size = new System.Drawing.Size(296, 25);
            _EmulatedReplyComboBox.TabIndex = 21;
            // 
            // _CommandComboBox
            // 
            _CommandComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;
            _CommandComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _CommandComboBox.FormattingEnabled = true;
            _CommandComboBox.Location = new System.Drawing.Point(86, 9);
            _CommandComboBox.Name = "_CommandComboBox";
            _CommandComboBox.Size = new System.Drawing.Size(296, 25);
            _CommandComboBox.TabIndex = 19;
            _CommandComboBox.Text = "SM15M0000000000000";
            // 
            // _LastMessageTextBox
            // 
            _LastMessageTextBox.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _LastMessageTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _LastMessageTextBox.Location = new System.Drawing.Point(5, 226);
            _LastMessageTextBox.Multiline = true;
            _LastMessageTextBox.Name = "_LastMessageTextBox";
            _LastMessageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _LastMessageTextBox.Size = new System.Drawing.Size(377, 169);
            _LastMessageTextBox.TabIndex = 26;
            // 
            // _LastMessageTextBoxLabel
            // 
            _LastMessageTextBoxLabel.AutoSize = true;
            _LastMessageTextBoxLabel.Location = new System.Drawing.Point(2, 207);
            _LastMessageTextBoxLabel.Name = "_LastMessageTextBoxLabel";
            _LastMessageTextBoxLabel.Size = new System.Drawing.Size(95, 17);
            _LastMessageTextBoxLabel.TabIndex = 25;
            _LastMessageTextBoxLabel.Text = "Last Message: ";
            // 
            // _EmulateButton
            // 
            __EmulateButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __EmulateButton.Location = new System.Drawing.Point(5, 36);
            __EmulateButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __EmulateButton.Name = "__EmulateButton";
            __EmulateButton.Size = new System.Drawing.Size(74, 30);
            __EmulateButton.TabIndex = 20;
            __EmulateButton.Text = "&Emulate";
            __EmulateButton.UseVisualStyleBackColor = true;
            // 
            // _WriteButton
            // 
            __WriteButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __WriteButton.Location = new System.Drawing.Point(21, 5);
            __WriteButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __WriteButton.Name = "__WriteButton";
            __WriteButton.Size = new System.Drawing.Size(58, 30);
            __WriteButton.TabIndex = 18;
            __WriteButton.Text = "&Write";
            __WriteButton.UseVisualStyleBackColor = true;
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_SentGroupBox);
            Controls.Add(_ReceivedGroupBox);
            Controls.Add(_ReceivedMessagesGroupBox);
            Controls.Add(_EmulatedReplyComboBox);
            Controls.Add(_CommandComboBox);
            Controls.Add(_LastMessageTextBox);
            Controls.Add(_LastMessageTextBoxLabel);
            Controls.Add(__EmulateButton);
            Controls.Add(__WriteButton);
            Name = "ReadingView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(390, 408);
            _SentGroupBox.ResumeLayout(false);
            _SentGroupBox.PerformLayout();
            _ReceivedGroupBox.ResumeLayout(false);
            _ReceivedGroupBox.PerformLayout();
            _ReceivedMessagesGroupBox.ResumeLayout(false);
            _ReceivedMessagesGroupBox.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.GroupBox _SentGroupBox;
        private System.Windows.Forms.Label _SendMessageLabel;
        private System.Windows.Forms.Label _UnhandledSendLabel;
        private System.Windows.Forms.GroupBox _ReceivedGroupBox;
        private System.Windows.Forms.Label _ReceivedMessageLabel;
        private System.Windows.Forms.Label _UnhandledMessageLabel;
        private System.Windows.Forms.GroupBox _ReceivedMessagesGroupBox;
        private System.Windows.Forms.Label _WaferStartReceivedLabel;
        private System.Windows.Forms.Label _TestStartAttributeLabel;
        private System.Windows.Forms.Label _TestStartedLabel;
        private System.Windows.Forms.Label _PatternCompleteLabel;
        private System.Windows.Forms.Label _TestCompleteLabel;
        private System.Windows.Forms.ComboBox _EmulatedReplyComboBox;
        private System.Windows.Forms.ComboBox _CommandComboBox;
        private System.Windows.Forms.TextBox _LastMessageTextBox;
        private System.Windows.Forms.Label _LastMessageTextBoxLabel;
        private System.Windows.Forms.Button __EmulateButton;

        private System.Windows.Forms.Button _EmulateButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __EmulateButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__EmulateButton != null)
                {
                    __EmulateButton.Click -= EmulateButton_Click;
                }

                __EmulateButton = value;
                if (__EmulateButton != null)
                {
                    __EmulateButton.Click += EmulateButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __WriteButton;

        private System.Windows.Forms.Button _WriteButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __WriteButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__WriteButton != null)
                {
                    __WriteButton.Click -= WriteButton_Click;
                }

                __WriteButton = value;
                if (__WriteButton != null)
                {
                    __WriteButton.Click += WriteButton_Click;
                }
            }
        }
    }
}