using System;
using System.ComponentModel;
using System.Diagnostics;
using isr.VI.ExceptionExtensions;

namespace isr.VI.EG2001.Forms
{

    /// <summary> A reading view. </summary>
/// <remarks>
/// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
/// Licensed under The MIT License.</para><para>
/// David, 2018-12-31 </para>
/// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__EmulateButton.Name = "_EmulateButton";
            this.__WriteButton.Name = "_WriteButton";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
    /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    /// and its child controls and optionally releases the managed resources.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    ///                                                                            <c>False</c> to
    ///                                                                            release only
    ///                                                                            unmanaged resources
    ///                                                                            when called from
    ///                                                                            the runtime
    ///                                                                            finalize. </param>
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (!this.IsDisposed && disposing)
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis(default);
                    if ( this.components is object)
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose(disposing);
            }
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
            }
            finally
            {
                base.OnLoad(e);
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public EG2001Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis(EG2001Device value)
        {
            if ( this.Device is object)
            {
                // the device base already clears all or only the private listeners. 
                // Me._Device.RemovePrivateListeners()
                this.AssignTalker(null);
                this.Device = null;
            }

            this.Device = value;
            if (value is object)
            {
                this.AssignTalker( this.Device.Talker);
            }
        }

        /// <summary> Assigns a device. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice(EG2001Device value)
        {
            this.AssignDeviceThis(value);
        }

        /// <summary> Reads the status register. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch (Exception ex)
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENTS: READ and WRITE "

        /// <summary> Event handler. Called by _EmulateButton for click events. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void EmulateButton_Click(object sender, EventArgs e)
        {
            string activity = string.Empty;
            try
            {
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Getting emulation message";
                string message = this._EmulatedReplyComboBox.Text.Trim();
                if (!string.IsNullOrWhiteSpace(message))
                {
                    activity = $"Setting last reading to {message}";
                    this.Device.ProberSubsystem.LastReading = message;
                    activity = $"Parsing emulated message {message}";
                    this.Device.ProberSubsystem.ParseReading(message);
                }
            }
            catch (Exception ex)
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Event handler. Called by _WriteButton for click events. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="sender"> Source of the event. </param>
    /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification = "Exception is published")]
        private void WriteButton_Click(object sender, EventArgs e)
        {
            if ( sender is System.Windows.Forms.Control c && !string.IsNullOrWhiteSpace( this._CommandComboBox.Text ) )
            {
                string message = this._CommandComboBox.Text.Trim();
                string activity = string.Empty;
                try
                {
                    activity = $"{this.Device.ResourceNameCaption} sending session message";
                    this.InfoProvider.Clear();
                    this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                    this.Device.ProberSubsystem.CommandTrialCount = EG2001.Properties.Settings.Default.CommandRetrylCount;
                    this.Device.ProberSubsystem.CommandPollInterval = EG2001.Properties.Settings.Default.CommandPollInterval < EG2001.Properties.Settings.Default.ReadDelay ? EG2001.Properties.Settings.Default.ReadDelay : EG2001.Properties.Settings.Default.CommandPollInterval;
                    this.Device.ProberSubsystem.CommandTimeoutInterval = EG2001.Properties.Settings.Default.CommandTimeoutInterval;
                    if ( this._CommandComboBox.Text.StartsWith( this.Device.ProberSubsystem.TestCompleteCommand, StringComparison.OrdinalIgnoreCase ) )
                    {
                        this.Device.ProberSubsystem.CommandTimeoutInterval = EG2001.Properties.Settings.Default.TestCompletedTimeoutInterval;
                    }

                    if ( this.Device.ProberSubsystem.TrySendAsync( message ) )
                    {
                        _ = this.PublishInfo( $"Message sent;. Sent: '{this.Device.ProberSubsystem.LastMessageSent}'; Received: '{this.Device.ProberSubsystem.LastReading}'." );
                    }
                    else if ( this.Device.ProberSubsystem.UnhandledMessageSent.GetValueOrDefault( false ) )
                    {
                        _ = this.PublishWarning( "Failed sending message--unknown message sent;. Sent: {0}", message );
                        this.InfoProvider.SetError( c, "Failed sending message--unknown message sent." );
                    }
                    else if ( this.Device.ProberSubsystem.UnhandledMessageReceived.GetValueOrDefault( false ) )
                    {
                        _ = this.PublishWarning( "Failed sending message--unknown message received;. Sent: {0}", message );
                        this.InfoProvider.SetError( c, "Failed sending message--unknown message received." );
                    }
                    else
                    {
                        _ = this.PublishWarning( "Failed sending message;. Sent: {0}", message );
                        this.InfoProvider.SetError( c, "Failed sending message." );
                    }
                }
                catch ( Exception ex )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                    _ = this.PublishException( activity, ex );
                }
                finally
                {
                    // TO_DO: added with new view; needs validation.
                    this.ReadStatusRegister();
                    this.Cursor = System.Windows.Forms.Cursors.Default;
                }
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
    /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    /// </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="eventType"> Type of the event. </param>
    /// <param name="activity">  The activity. </param>
    /// <returns> A String. </returns>
        protected override string Publish(TraceEventType eventType, string activity)
        {
            return this.Publish(new Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity));
        }

        /// <summary> Publish exception. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    /// <param name="activity"> The activity. </param>
    /// <param name="ex">       The ex. </param>
    /// <returns> A String. </returns>
        protected override string PublishException(string activity, Exception ex)
        {
            return this.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}");
        }

        #endregion

    }
}
