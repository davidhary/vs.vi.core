﻿
namespace isr.VI.EG2001.Forms.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = (int)Pith.My.ProjectTraceEventId.EG2000Prober;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI EG2001 Prober Forms Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "EG2001 Prober Forms Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.EG2001.Forms";
    }
}