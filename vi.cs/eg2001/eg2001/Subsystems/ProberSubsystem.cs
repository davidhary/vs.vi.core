using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.EG2001
{

    /// <summary> Defines a Prober Subsystem for a EG2001 Prober. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-01, 3.0.5022. </para>
    /// </remarks>
    public partial class ProberSubsystem
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new this. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void NewThis()
        {
            this.ErrorReplyPattern = "E";
            this.MessageFailedPattern = "MF";
            this.MessageCompletedPattern = "MC";
            this.FirstTestStartPattern = "TF";
            this.PatternCompleteReplyPattern = "PC";
            this.RetestStartPattern = "TR";
            this.SetModeCommandPrefix = "SM";
            this.TestStartPattern = "TS";
            this.TestAgainStartPattern = "TA";
            this.WaferStartPattern = "WB";
            this.TestCompleteCommand = "TC";
            this.TestCompleteCommand = Properties.Settings.Default.UsingScpi ? string.Empty : "TC";
            this.IdentityReplyPattern = Properties.Settings.Default.UsingScpi ? "KEITHLEY " : "2001X.";
            this.ErrorQueryCommand = Properties.Settings.Default.UsingScpi ? ":SYS:ERR?" : "?E";
            this.IdentityQueryCommand = Properties.Settings.Default.UsingScpi ? "*IDN?" : "ID";
            this.SetModeCommand = Properties.Settings.Default.UsingScpi ? string.Empty : "SM15M101110110001000001";
            this.SetModeQueryCommand = Properties.Settings.Default.UsingScpi ? string.Empty : "?SM15";
        }

        #endregion

        #region" I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. Sets the response mode. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.Session.ReadDelay = Properties.Settings.Default.ReadDelay;
            this.Session.StatusReadDelay = Properties.Settings.Default.StatusDelay;
            // enable MF/MC handshake
            // SM15M101110110001000001
            var mode = ResponseModes.None | ResponseModes.HandshakePosition | ResponseModes.HandshakeDeviceCommands | ResponseModes.HandshakeCommands | ResponseModes.TestStartSent | ResponseModes.PatternCompleteResponse | ResponseModes.PauseContinueResponse | ResponseModes.EnhancedTestStart | ResponseModes.WaferBegin | ResponseModes.None;
            // sends the message to the prober and read the reply.
            if ( !Properties.Settings.Default.UsingScpi )
                _ = this.WriteResponseModeReadReply( mode );
            // update the response mode. 
            this.ResponseMode = mode;

            // set the emulation.
            this.SupportedCommandPrefixes = new string[] { this.SetModeCommandPrefix, "ID", this.TestCompleteCommand, "?E" };
            this.SupportedCommands = new string[] { this.SetModeCommand, this.IdentityQueryCommand, this.TestCompleteCommand, this.ErrorQueryCommand };
            this.SupportedReplyPatterns = new string[] { $"{this.ErrorReplyPattern}0", $"{this.ErrorReplyPattern}0", $"{this.IdentityReplyPattern}CD.249799-011", this.PatternCompleteReplyPattern, this.TestAgainStartPattern, this.FirstTestStartPattern, this.RetestStartPattern, this.TestStartPattern, this.MessageCompletedPattern, this.MessageFailedPattern, $"{this.WaferStartPattern}I/-299" };
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.ResponseMode = ResponseModes.EnhancedTestStart | ResponseModes.EnhancedPatternComplete | ResponseModes.HandshakePosition | ResponseModes.HandshakeDeviceCommands | ResponseModes.PatternCompleteResponse | ResponseModes.TestStartSent | ResponseModes.WaferComplete;
        }
        #endregion

        #region" COMMAND SYNTAX "

        #endregion

        #region" RESPONSE MODE "

        /// <summary> The Response Mode. </summary>
        private ResponseModes? _ResponseMode;

        /// <summary> Gets or sets the cached Response Mode. </summary>
        /// <value> The Response Mode or null if unknown. </value>
        public ResponseModes? ResponseMode
        {
            get => this._ResponseMode;

            protected set {
                if ( !Nullable.Equals( this.ResponseMode, value ) )
                {
                    this._ResponseMode = value;
                    this.SyncNotifyPropertyChanged();
                }
            }
        }

        /// <summary> Queries trim end. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="dataToWrite"> The data to write. </param>
        /// <returns> The trim end. </returns>
        public string QueryTrimEnd( string dataToWrite )
        {
            return this.QueryTrimEnd( this.Session.StatusReadDelay, this.Session.ReadDelay, dataToWrite );
        }

        /// <summary>
        /// Queries the Response Mode. Also sets the <see cref="ResponseMode"></see> cached value.
        /// </summary>
        /// <remarks> The 2001x does not returns the expected replay. It returns: 'SZDW0C1'. </remarks>
        /// <returns> The Response Mode or null if unknown. </returns>
        public ResponseModes? QueryResponseMode()
        {
            string mode = this.ResponseMode.ToString();
            this.Session.MakeEmulatedReplyIfEmpty( mode );
            mode = this.QueryTrimEnd( "?SM15" );
            if ( string.IsNullOrWhiteSpace( mode ) )
            {
                this.ResponseMode = ResponseModes.None;
            }
            else
            {
                // reverse the order so as to match the enumeration.
                mode = mode.ToCharArray().Reverse().ToString();
                this.ResponseMode = ( ResponseModes ) Conversions.ToInteger( Convert.ToInt32( mode, 2 ) );
            }

            return this.ResponseMode;
        }

        /// <summary> Writes a response mode read reply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="mode"> The mode. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool WriteResponseModeReadReply( ResponseModes mode )
        {
            _ = this.PublishInfo( "Setting response mode to 0x{0: X4};. ", ( object ) ( int ) mode );
            bool affirmative;
            if ( this.WriteResponseMode( mode ).HasValue )
            {
                // Dim bit As VI.Pith.ServiceRequests = Me.StatusSubsystem.MessageAvailableBits
                // Me.PublishInfo("Awaiting SRQ equal to 0x{0:X2};. ", CInt(bit))
                _ = this.PublishInfo( $"Reading with delay {this.Session.ReadDelay:hh\\:mm\\:ss\\.fff};. " );
                this.Session.MakeEmulatedReplyIfEmpty( this.MessageCompletedPattern );
                this.FetchAndParse( this.Session.StatusReadDelay, this.Session.ReadDelay );
                if ( this.MessageCompleted )
                {
                    affirmative = true;
                    _ = this.PublishInfo( "Response mode set to 0x{0:X4};. ", ( object ) ( int ) this.ResponseMode );
                }
                else if ( this.MessageFailed )
                {
                    throw new Core.OperationFailedException( "Message Failed initializing response mode to 0x{0:X4};.", ( object ) ( int ) mode );
                }
                else
                {
                    throw new Core.OperationFailedException( "Unexpected reply '{0}' initializing response mode;. ", this.LastReading );
                }
            }
            else
            {
                throw new Core.OperationFailedException( "Failed initializing response mode." );
            }

            return affirmative;
        }

        /// <summary> Response mode command. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The Response Mode. </param>
        /// <returns> A String. </returns>
        public static string ResponseModeCommand( ResponseModes value )
        {
            var chars = Convert.ToString( ( int ) value, 2 ).ToCharArray();
            Array.Reverse( chars );
            return $"SM15M{new string( chars )}";
        }

        /// <summary> Writes the Response Mode. Does not read back from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The Response Mode. </param>
        /// <returns> The Response Mode or null if unknown. </returns>
        public ResponseModes? WriteResponseMode( ResponseModes value )
        {
            // Dim chars As Char() = Convert.ToString(value, 2).ToCharArray
            // Array.Reverse(chars)
            // Me.Session.WriteLine("SM15M{0}", New String(chars))
            _ = this.Session.WriteLine( ResponseModeCommand( value ) );
            this.ResponseMode = value;
            return this.ResponseMode;
        }

        /// <summary> Writes the Response Mode. Does not read back from the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The Response Mode. </param>
        /// <returns> The Response Mode or null if unknown. </returns>
        public ResponseModes? WriteAsyncResponseMode( ResponseModes value )
        {
            var chars = Convert.ToString( ( int ) value, 2 ).ToCharArray();
            Array.Reverse( chars );
            string responseMode = new ( chars );
            string msg = string.Format( System.Globalization.CultureInfo.InvariantCulture, "SM15M{0}", responseMode );
            this.ResponseMode = this.TrySendAsync( msg, 3, TimeSpan.FromMilliseconds( 100d ), TimeSpan.FromMilliseconds( 1000d ) ) ? value : new ResponseModes?();
            return this.ResponseMode;
        }

        /// <summary> Writes and reads back the Response Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="ResponseModes">Response Mode</see>. </param>
        /// <returns> The Response Mode or null if unknown. </returns>
        public ResponseModes? ApplyAsyncResponseMode( ResponseModes value )
        {
            return this.WriteAsyncResponseMode( value ).HasValue ? this.QueryResponseMode() : new ResponseModes?();
        }

        #endregion

        #region" TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }

    /// <summary> Enumerates the responses. </summary>
    /// <remarks> See Electroglass Manual page 4-9 (23) for detailed descriptions. </remarks>
    [Flags()]
    public enum ResponseModes
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not set" )]
        None,

        /// <summary> An enum constant representing the handshake position option. </summary>
        [Description( "1: MC/MF on X/Y" )]
        HandshakePosition = 1,

        /// <summary> An enum constant representing the handshake chuck option. </summary>
        [Description( "2: MC/MF on Z" )]
        HandshakeChuck = 2,

        /// <summary> An enum constant representing the handshake device commands option. </summary>
        [Description( "3: MC/MF on device commands" )]
        HandshakeDeviceCommands = 4,

        /// <summary> An enum constant representing the handshake commands option. </summary>
        [Description( "4: MC/MF on balance parameter commands" )]
        HandshakeCommands = 8,

        /// <summary> An enum constant representing the test start sent option. </summary>
        [Description( "5: TS sent" )]
        TestStartSent = 16,

        /// <summary> An enum constant representing the test complete response option. </summary>
        [Description( "6: TC response" )]
        TestCompleteResponse = 32,

        /// <summary> An enum constant representing the pattern complete response option. </summary>
        [Description( "7: PC response" )]
        PatternCompleteResponse = 64,

        /// <summary> An enum constant representing the pause continue response option. </summary>
        [Description( "8: PA/CO response" )]
        PauseContinueResponse = 128,

        /// <summary> An enum constant representing the alarm response option. </summary>
        [Description( "9: Alarm response" )]
        AlarmResponse = 256,

        /// <summary> An enum constant representing the wafer complete option. </summary>
        [Description( "10: WC (Wafer complete)" )]
        WaferComplete = 512,

        /// <summary> An enum constant representing the enhanced pattern complete option. </summary>
        [Description( "11: Enhanced PC" )]
        EnhancedPatternComplete = 1024,

        /// <summary> An enum constant representing the enhanced test start option. </summary>
        [Description( "12: Enhanced TS" )]
        EnhancedTestStart = 2048,

        /// <summary> An enum constant representing the ugly die report option. </summary>
        [Description( "13: Ugly Die Report" )]
        UglyDieReport = 4096,

        /// <summary> An enum constant representing the map transfer retries option. </summary>
        [Description( "14: Map Transfer Retries" )]
        MapTransferRetries = 8192,

        /// <summary> An enum constant representing the send coordinates with test start option. </summary>
        [Description( "15: Send Coordinates With Test Start" )]
        SendCoordinatesWithTestStart = 16384,

        /// <summary> An enum constant representing the send cassette messages option. </summary>
        [Description( "16: Send EC/BC (End/Begin Cassette) Messages" )]
        SendCassetteMessages = 32768,

        /// <summary> An enum constant representing the pause pending option. </summary>
        [Description( "17: Pause Pending" )]
        PausePending = 65536,

        /// <summary> An enum constant representing the wafer begin option. </summary>
        [Description( "18: Wafer Begin" )]
        WaferBegin = 131072
    }
}
