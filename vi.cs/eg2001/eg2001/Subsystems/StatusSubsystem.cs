using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.EG2001
{

    /// <summary> Defines a Status Subsystem for a EG2001 Prober. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-01, 3.0.5022. </para>
    /// </remarks>
    public partial class StatusSubsystem : StatusSubsystemBase
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        ///                        session</see>. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ) )
        {
            this.VersionInfo = new VersionInfo();
            this.VersionInfoBase = this.VersionInfo;
            InitializeSession( session );
            if ( Properties.Settings.Default.UsingScpi )
            {
                this.IdentityQueryCommand = Pith.Ieee488.Syntax.IdentityQueryCommand;
                this.DeviceErrorQueryCommand = Pith.Scpi.Syntax.LastSystemErrorQueryCommand;
                this.NoErrorCompoundMessage = Pith.Scpi.Syntax.NoErrorCompoundMessage;
            }
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region" SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        ///                        session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.ClearExecutionStateCommand = string.Empty;
            session.ResetKnownStateCommand = string.Empty;
            session.ErrorAvailableBit = Pith.ServiceRequests.ErrorAvailable;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.MessageAvailable;
            session.OperationCompletedQueryCommand = string.Empty;
            session.StandardEventBit = Pith.ServiceRequests.StandardEvent;
            session.StandardEventStatusQueryCommand = string.Empty;
            session.StandardEventEnableQueryCommand = string.Empty;
            session.StandardServiceEnableCommandFormat = string.Empty;
            session.StandardServiceEnableCompleteCommandFormat = string.Empty;
            session.ServiceRequestEnableQueryCommand = string.Empty;
            session.ServiceRequestEnableCommandFormat = string.Empty;
            session.ServiceRequestEnableQueryCommand = string.Empty;
            session.WaitCommand = Pith.Ieee488.Syntax.WaitCommand;

            // operation completion not support -- set to Completed.
            session.OperationCompleted = true;
        }

        #endregion

        #region" I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            // operation completion not supported, set to true
            this.Session.OperationCompleted = true;
        }

        #endregion

        #region" IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = "ID";

        /// <summary> Queries the Identity. </summary>
        /// <remarks>
        /// Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. Detection of service
        /// request is unreliable with IVI VISA 18.
        /// </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.PublishVerbose( "Trying to read identity;. " );
                Core.ApplianceBase.DoEventsWait( this.Session.StatusReadDelay );
                // read Status byte
                _ = this.Session.ReadStatusRegister();
                // wait for the read delay
                Core.ApplianceBase.DoEventsWait( this.Session.ReadDelay );
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.PublishVerbose( $"Setting identity to {value};. " );
                // Me.VersionInfo.Parse("2001X.CD.999999-011")
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

        #region" DEVICE ERRORS "

        /// <summary> Queues device error. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="compoundErrorMessage"> Message describing the compound error. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected override VI.DeviceError EnqueueDeviceError( string compoundErrorMessage )
        {
            var de = new DeviceError();
            de.Parse( compoundErrorMessage );
            if ( de.IsError )
                this.DeviceErrorQueue.Enqueue( de );
            return de;
        }

        #endregion

        #region" TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        #endregion

    }
}
