using System;

namespace isr.VI.EG2001
{

    /// <summary> A device error. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-12 </para>
    /// </remarks>
    public class DeviceError : VI.DeviceError
    {

        #region" CONSTRUCTION "

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceError" /> class specifying no error.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public DeviceError() : base( "0,No Errors" )
        {
        }

        #endregion

        #region" PARSE "

        /// <summary> Parses the error message. </summary>
        /// <remarks>
        /// TSP2 error: -285,TSP Syntax error at line 1: unexpected symbol near `*',level=1 TSP error: -
        /// 285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20 SCPI Error: -113,
        /// "Undefined header;1;2018/05/26 14:00:14.871".
        /// </remarks>
        /// <param name="compoundError"> The compound error. </param>
        public override void Parse( string compoundError )
        {
            base.Parse( compoundError );
            if ( !string.IsNullOrWhiteSpace( compoundError ) )
            {
                // parse EG Prober Errors.
                if ( compoundError.StartsWith( "E", StringComparison.OrdinalIgnoreCase ) )
                {
                    bool localTryParse() {
                        _ = this.ErrorNumber;
                        var ret = int.TryParse( compoundError.Substring( 1 ), out int argresult ); this.ErrorNumber = argresult; return ret; }

                    if ( localTryParse() )
                    {
                    }

                    this.ErrorMessage = compoundError;
                }
            }
        }

        #endregion

    }
}
