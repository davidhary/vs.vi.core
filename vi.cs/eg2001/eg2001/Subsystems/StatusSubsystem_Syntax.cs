﻿
namespace isr.VI.EG2001
{
    public partial class StatusSubsystem
    {

        #region" PRESET "

        /// <summary> Gets or sets the preset command. </summary>
        /// <remarks>
        /// SCPI: ":STAT:PRES".
        /// <see cref="F:isr.VI.Pith.Scpi.Syntax.ScpiSyntax.StatusPresetCommand"></see>
        /// </remarks>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = string.Empty;

        #endregion

        #region" DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the 'Next Error' query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string DequeueErrorQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected override string DeviceErrorQueryCommand { get; set; } = "?E";

        /// <summary> Gets or sets the error queue query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = string.Empty;

        #endregion

        #region" LINE FREQUENCY "

        /// <summary> Gets or sets line frequency query command. </summary>
        /// <value> The line frequency query command. </value>
        protected override string LineFrequencyQueryCommand { get; set; } = Pith.Scpi.Syntax.ReadLineFrequencyCommand;

        #endregion

    }
}