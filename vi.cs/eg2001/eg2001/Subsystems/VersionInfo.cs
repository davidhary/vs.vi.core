using System.Collections.Generic;
using System.Linq;

namespace isr.VI.EG2001
{

    /// <summary> Information about the version. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-09-21 </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
            this.ParseFirmwareRevision( "" );
        }

        /// <summary> Parses the instrument identity string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> Specifies the instrument identity string, which includes at a minimum the
        ///                      following information: e.g., <para> <c>2001X.CD.249799-
        ///                      011</c>.</para><para>
        ///                      CD is revision letters,</para><para>
        ///                      249799 is software part number</para><para>
        ///                      001 is EG standard PROM-base software</para><para>
        ///                      </para>
        ///                      <see cref="VersionInfoBase.ManufacturerName">manufacturer</see>,
        ///                      <see cref="VersionInfoBase.Model">model</see>,
        ///                      <see cref="VersionInfoBase.SerialNumber">serial number</see>,. </param>
        public override void Parse( string value )
        {

            // clear
            base.Parse( "" );

            // save the identity.
            this.Identity = value;
            if ( !string.IsNullOrWhiteSpace( value ) )
            {
                this.ManufacturerName = "Electroglass";

                // Parse the id to get the revision number
                var idItems = new Queue<string>( value.Split( '.' ) );
                if ( idItems.Any() )
                {
                    // model: 2001X
                    this.Model = idItems.Dequeue();
                }

                if ( idItems.Any() )
                {
                    // firmware: CD
                    this.FirmwareRevision = idItems.Dequeue();
                }

                if ( idItems.Any() )
                {
                    // Serial Number: 249799-011
                    this.SerialNumber = idItems.Dequeue();
                }

                // parse thee firmware revision
                this.ParseFirmwareRevision( "" );
            }
        }
    }
}
