using System;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.EG2001
{

    /// <summary> Implements an Electroglass 2000 Prober device. </summary>
    /// <remarks>
    /// An instrument is defined, for the purpose of this library, as a device with a front panel.
    /// <para>
    /// David, 2018-12-31, Updated. </para><para>
    /// David, 2013-10-01, 3.0.5022. </para><para>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License. </para>
    /// </remarks>
    public class EG2001Device : VisaSessionBase
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="EG2001Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public EG2001Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected EG2001Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            if ( statusSubsystem is object )
            {
                Properties.Settings.Default.PropertyChanged += this.HandleSettingsPropertyChanged;
            }

            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static EG2001Device Create()
        {
            EG2001Device device = null;
            try
            {
                device = new EG2001Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static EG2001Device Validated( EG2001Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            // release unmanaged-only resources.
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( EG2001Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region" SESSION INITIALIZATION PROPERTIES "

        /// <summary>
        /// Gets or sets the bits that would be set for detecting if an error is available.
        /// </summary>
        /// <value> The error available bits. </value>
        protected override Pith.ServiceRequests ErrorAvailableBits { get; set; } = Pith.ServiceRequests.ErrorAvailable;

        /// <summary> Gets or sets the keep alive interval. </summary>
        /// <remarks> Required only with Non-Standard NI VISA. </remarks>
        /// <value> The keep alive interval. </value>
        protected override TimeSpan KeepAliveInterval { get; set; } = TimeSpan.Zero;

        /// <summary> Gets or sets the is alive command. </summary>
        /// <remarks> Required only with Non-Standard NI VISA. </remarks>
        /// <value> The is alive command. </value>
        protected override string IsAliveCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the is alive query command. </summary>
        /// <remarks> Required only with Non-Standard NI VISA. </remarks>
        /// <value> The is alive query command. </value>
        protected override string IsAliveQueryCommand { get; set; } = string.Empty;

        #endregion

        #region" SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindProberSubsystem( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.BindProberSubsystem( new ProberSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region" SUBSYSTEMS "

        #region" PROBER "

        /// <summary> Gets or sets the Prober Subsystem. </summary>
        /// <value> The Prober Subsystem. </value>
        public ProberSubsystem ProberSubsystem { get; private set; }

        /// <summary> Binds the Prober subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindProberSubsystem( ProberSubsystem subsystem )
        {
            if ( this.ProberSubsystem is object )
            {
                this.ProberSubsystem.PropertyChanged -= this.ProberSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.ProberSubsystem );
                this.ProberSubsystem.Dispose();
                this.ProberSubsystem = null;
            }

            this.ProberSubsystem = subsystem;
            if ( this.ProberSubsystem is object )
            {
                this.ProberSubsystem.PropertyChanged += this.ProberSubsystemPropertyChanged;
                this.Subsystems.Add( this.ProberSubsystem );
                this.HandlePropertyChanged( this.ProberSubsystem, nameof( EG2001.ProberSubsystem.IdentityRead ) );
                this.HandlePropertyChanged( this.ProberSubsystem, nameof( EG2001.ProberSubsystem.ErrorRead ) );
            }
        }

        /// <summary> Handle the Prober subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ProberSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            string lastReading = subsystem.LastReading;
            switch ( propertyName ?? "" )
            {
                case nameof( EG2001.ProberSubsystem.IdentityRead ):
                    {
                        if ( subsystem.IdentityRead && !string.IsNullOrWhiteSpace( lastReading ) )
                        {
                            this.StatusSubsystem.Identity = lastReading;
                        }

                        break;
                    }

                case nameof( EG2001.ProberSubsystem.ErrorRead ):
                    {
                        if ( subsystem.ErrorRead )
                        {
                            _ = this.StatusSubsystem.EnqueueLastError( subsystem?.LastReading );
                        }
                        else
                        {
                        }

                        break;
                    }
            }
        }

        /// <summary> Prober subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ProberSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ProberSubsystem )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as ProberSubsystem, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region" STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region" SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #endregion

        #region" SERVICE REQUEST "

        /// <summary> Reads the event registers after receiving a service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query unterminated error: Me.ReadEventRegisters()
            if ( this.StatusSubsystem.MessageAvailable )
            {
                string firstReading = string.Empty;
                string reading = this.Session.ReadFiniteLine().TrimEnd( this.Session.TerminationCharacters() );
                if ( !this.ProberSubsystem.IsValidReading( reading ) )
                {
                    firstReading = reading;
                    _ = this.PublishInfo( $"Invalid reading '{reading}'; trying again;. " );
                    try
                    {
                        this.Session.StoreCommunicationTimeout( Properties.Settings.Default.PostSrqRetryTimeout );
                        reading = this.Session.ReadFiniteLine().TrimEnd( this.Session.TerminationCharacters() );
                    }
                    catch
                    {
                        throw;
                    }
                    finally
                    {
                        this.Session.RestoreCommunicationTimeout();
                    }
                }

                this.ProberSubsystem.LastReading = reading;
                if ( this.ProberSubsystem.IsValidReading( reading ) )
                {
                    _ = this.PublishVerbose( $"Parsing;. {this.ProberSubsystem.LastReading};. " );
                    this.ProberSubsystem.ParseReading( this.ProberSubsystem.LastReading );
                }
                else
                {
                    _ = this.PublishWarning( $"Failed reading valid reply '{reading}' from the Prober after reading '{firstReading}';. " );
                }
            }
        }

        #endregion

        #region" MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "EG2001 Settings Editor", Properties.Settings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = Properties.Settings.Default;
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.Default.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.Default.StatusDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( Properties.Settings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Properties.Settings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( Properties.Settings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.SessionMessageNotificationLevel ):
                    {
                        this.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {this.Session.MessageNotificationLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.Default.ReadDelay ):
                    {
                        this.Session.ReadDelay = sender.ReadDelay;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ReadDelay}" );
                        break;
                    }

                case nameof( Properties.Settings.Default.StatusDelay ):
                    {
                        this.Session.StatusReadDelay = sender.StatusDelay;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.StatusDelay}" );
                        break;
                    }
            }
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleSettingsPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( Properties.Settings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as Properties.Settings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region" TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
