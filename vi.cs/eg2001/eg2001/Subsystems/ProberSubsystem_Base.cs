using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;

namespace isr.VI.EG2001
{
    public partial class ProberSubsystem : ProberSubsystemBase, IDisposable
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public ProberSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.Worker = new System.ComponentModel.BackgroundWorker() { WorkerSupportsCancellation = true };
            this.CommandTrialCount = 3;
            this.CommandPollInterval = TimeSpan.FromMilliseconds( 30d );
            this.CommandTimeoutInterval = TimeSpan.FromMilliseconds( 200d );
            this.ProberTimer = new System.Timers.Timer( 1.5d * this.CommandTrialCount * this.CommandTimeoutInterval.TotalMilliseconds );
            this.ProberTimer.Stop();
            this._SupportedReplyPatterns = new List<string>();
            this._SupportedCommandPrefixes = new List<string>();
            this._SupportedCommands = new List<string>();
            this.NewThis();
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets or sets the dispose status sentinel of the base class.  This applies to the derived
        /// class provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
        /// If True, the method has been called directly or indirectly by a user's code--managed and
        /// unmanaged resources can be disposed.</para><para>
        /// If False, the method has been called by the runtime from inside the finalizer and you should
        /// not reference other objects--only unmanaged resources can be disposed.</para>
        /// </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
        ///                                                                            resources;
        ///                                                                            False if this
        ///                                                                            method releases
        ///                                                                            only unmanaged
        ///                                                                            resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.ProberTimer is object )
                    {
                        this.ProberTimer.Stop();
                        Core.ApplianceBase.DoEvents();
                        this.ProberTimer.Dispose();
                        this.ProberTimer = null;
                    }
                    // Free managed resources when explicitly called
                    if ( this.Worker is object )
                    {
                        this.Worker.CancelAsync();
                        Core.ApplianceBase.DoEvents();
                        if ( !(this.Worker.IsBusy || this.Worker.CancellationPending) )
                        {
                            this.Worker.Dispose();
                        }
                    }
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region" IDENTITY COMPLETE "

        /// <summary> A pattern specifying the Identity. </summary>
        /// <value> The identity reply pattern. </value>
        protected string IdentityReplyPattern { get; set; }

        #endregion

        #region" ERROR PATTERN "

        /// <summary> A pattern specifying the Error Message coming back from the Prober. </summary>
        /// <value> The error reply pattern. </value>
        protected string ErrorReplyPattern { get; set; }

        #endregion

        #region" MESSAGE FAILED "

        /// <summary> A pattern specifying the message failed. </summary>
        private string _MessageFailedPattern;

        /// <summary> Gets or sets the message failed pattern. </summary>
        /// <value> The message failed pattern. </value>
        public string MessageFailedPattern
        {
            get => this._MessageFailedPattern;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.MessageFailedPattern, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._MessageFailedPattern = value;
                    this.SyncNotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        #endregion

        #region" MESSAGE COMPLETED "

        /// <summary> A pattern specifying the message completed. </summary>
        private string _MessageCompletedPattern;

        /// <summary> Gets or sets the message Completed pattern. </summary>
        /// <value> The message Completed pattern. </value>
        public string MessageCompletedPattern
        {
            get => this._MessageCompletedPattern;

            set {
                if ( string.IsNullOrWhiteSpace( value ) )
                    value = string.Empty;
                if ( !string.Equals( value, this.MessageCompletedPattern, StringComparison.OrdinalIgnoreCase ) )
                {
                    this._MessageCompletedPattern = value;
                    this.SyncNotifyPropertyChanged();
                    Core.ApplianceBase.DoEvents();
                }
            }
        }

        #endregion

        #region" PATTERN COMPLETE "

        /// <summary> A pattern specifying the pattern complete. </summary>
        /// <value> The pattern complete reply pattern. </value>
        public string PatternCompleteReplyPattern { get; set; }

        #endregion

        #region" SET MODE "

        /// <summary> A pattern specifying a mode setup command prefix. </summary>
        /// <value> The set mode command prefix. </value>
        protected string SetModeCommandPrefix { get; set; }

        /// <summary> Gets or sets the set mode command. </summary>
        /// <value> The set mode command. </value>
        public string SetModeCommand { get; set; }

        /// <summary> Gets or sets the set mode query command. </summary>
        /// <value> The set mode query command. </value>
        public string SetModeQueryCommand { get; set; }

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        public string IdentityQueryCommand { get; set; }

        /// <summary> Gets or sets the error query command. </summary>
        /// <value> The error query command. </value>
        public string ErrorQueryCommand { get; set; }

        #endregion

        #region" TEST COMPLETE "

        /// <summary> Gets or sets the test complete command. </summary>
        /// <value> The test complete command. </value>
        public string TestCompleteCommand { get; set; }

        #endregion

        #region" TEST START "

        /// <summary> Gets or sets the retest start pattern. </summary>
        /// <value> The test start pattern. </value>
        public string RetestStartPattern { get; set; }

        /// <summary> Gets or sets the test again start pattern. </summary>
        /// <value> The test start pattern. </value>
        public string TestAgainStartPattern { get; set; }

        /// <summary> Gets or sets the test start pattern. </summary>
        /// <value> The test start pattern. </value>
        public string TestStartPattern { get; set; }

        /// <summary> Gets or sets the first test start pattern. </summary>
        /// <value> The first test start pattern. </value>
        public string FirstTestStartPattern { get; set; }

        #endregion

        #region" WAFER START "

        /// <summary> Gets or sets the wafer start pattern. </summary>
        /// <value> The wafer start pattern. </value>
        public string WaferStartPattern { get; set; }

        #endregion

        #region" WRITE "

        /// <summary> The supported command prefixes. </summary>
        private List<string> _SupportedCommandPrefixes;

        /// <summary> Gets or sets the supported command prefixes. </summary>
        /// <value> The supported command prefixes. </value>
        public IList<string> SupportedCommandPrefixes
        {
            get => this._SupportedCommandPrefixes;

            set {
                this._SupportedCommandPrefixes = new List<string>( value );
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The supported commands. </summary>
        private List<string> _SupportedCommands;

        /// <summary> Gets or sets the supported commands. </summary>
        /// <value> The supported commands. </value>
        public IList<string> SupportedCommands
        {
            get => this._SupportedCommands;

            set {
                this._SupportedCommands = new List<string>( value );
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> The last message sent. </summary>
        private string _LastMessageSent;

        /// <summary> Gets or sets (protected) the last Message Sent. </summary>
        /// <value> The last MessageSent. </value>
        public string LastMessageSent
        {
            get => this._LastMessageSent;

            protected set {
                this._LastMessageSent = value;
                this.SyncNotifyPropertyChanged();
                Core.ApplianceBase.DoEvents();
            }
        }

        /// <summary> Sends a message to the instrument. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="format"> Describes the format to use. </param>
        /// <param name="args">   A variable-length parameters list containing arguments. </param>
        public void Send( string format, params object[] args )
        {
            this.Send( string.Format( System.Globalization.CultureInfo.CurrentCulture, format, args ) );
        }

        /// <summary>
        /// Synchronously writes and ASCII-encoded string data. Terminates the data with the
        /// <see cref="VI.Pith.SessionBase.TerminationCharacters">termination character</see>.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="dataToWrite"> The data to write. </param>
        public void Send( string dataToWrite )
        {
            this.ProberTimer.Stop();
            this.ClearSendSentinels();
            this.ClearFetchSentinels();
            if ( !string.IsNullOrWhiteSpace( dataToWrite ) )
            {
                this.LastMessageSent = dataToWrite;
                _ = this.Session.WriteLine( dataToWrite );
                if ( dataToWrite.StartsWith( this.TestCompleteCommand, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.TestCompleteSent = true;
                }
                else if ( dataToWrite.StartsWith( this.SetModeCommandPrefix, StringComparison.OrdinalIgnoreCase ) )
                {
                    this.SetModeSent = true;
                }
                else if ( this._SupportedCommandPrefixes.Contains( dataToWrite.Substring( 0, 2 ), StringComparer.OrdinalIgnoreCase ) )
                {
                }
                else if ( dataToWrite.StartsWith( "*SRE", StringComparison.OrdinalIgnoreCase ) )
                {
                    this.LastReading = this.MessageCompletedPattern;
                    this.ParseReading( this.LastReading );
                }
                else if ( dataToWrite.StartsWith( "*IDN?", StringComparison.OrdinalIgnoreCase ) )
                {
                }
                else
                {
                    this.UnhandledMessageSent = true;
                }
            }
        }

        #endregion

        #region" BACKGROUND WORKER "

        /// <summary> Worker payload. </summary>
        /// <remarks> David, 2013-10-29. </remarks>
        private class WorkerPayLoad
        {

            /// <summary> Gets or sets the message. </summary>
            /// <value> The message. </value>
            public string Message { get; set; }

            /// <summary> Gets or sets the trial number. </summary>
            /// <value> The trial number. </value>
            public int TrialNumber { get; set; }

            /// <summary> Gets or sets the resend on retry. </summary>
            /// <value> The resend on retry. </value>
            public bool ResendOnRetry { get; set; }

            /// <summary> Gets or sets the number of trials. </summary>
            /// <value> The number of trials. </value>
            public int TrialCount { get; set; }

            /// <summary> Gets or sets the poll interval. </summary>
            /// <value> The poll interval. </value>
            public TimeSpan PollInterval { get; set; }

            /// <summary> Gets or sets the timeout interval. </summary>
            /// <value> The timeout interval. </value>
            public TimeSpan TimeoutInterval { get; set; }
        }

        private System.ComponentModel.BackgroundWorker _Worker;

        private System.ComponentModel.BackgroundWorker Worker
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._Worker;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._Worker != null )
                {

                    this._Worker.DoWork -= this.Worker_DoWork;

                    this._Worker.RunWorkerCompleted -= this.Worker_RunWorkerCompleted;
                }

                this._Worker = value;
                if ( this._Worker != null )
                {
                    this._Worker.DoWork += this.Worker_DoWork;
                    this._Worker.RunWorkerCompleted += this.Worker_RunWorkerCompleted;
                }
            }
        }

        /// <summary> Worker do work. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Do work event information. </param>
        private void Worker_DoWork( object sender, System.ComponentModel.DoWorkEventArgs e )
        {
            if ( !(this.IsDisposed || e is null || e.Cancel) )
            {
                WorkerPayLoad payload = e.Argument as WorkerPayLoad;
                payload.TrialNumber = 0;
                var sw = Stopwatch.StartNew();
                do
                {
                    sw.Restart();
                    payload.TrialNumber += 1;
                    if ( payload.ResendOnRetry || payload.TrialNumber == 1 )
                        this.Send( payload.Message );
                    _ = Core.ApplianceBase.DoEventsWaitUntil( payload.PollInterval, payload.TimeoutInterval, () => this.MessageCompleted || this.MessageFailed );
                }
                while ( !this.MessageCompleted && !this.MessageFailed && payload.TrialNumber < payload.TrialCount );
            }
        }

        /// <summary> Worker run worker completed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Run worker completed event information. </param>
        private void Worker_RunWorkerCompleted( object sender, System.ComponentModel.RunWorkerCompletedEventArgs e )
        {
            if ( !(this.IsDisposed || e is null || e.Cancelled || e.Error is object) )
            {
            }
        }

        /// <summary> Gets or sets the number of command trials. </summary>
        /// <value> The number of command trials. </value>
        public int CommandTrialCount { get; set; }

        /// <summary> Gets or sets the command poll interval. </summary>
        /// <value> The command poll interval. </value>
        public TimeSpan CommandPollInterval { get; set; }

        /// <summary> Gets or sets the command timeout interval. </summary>
        /// <value> The command timeout interval. </value>
        public TimeSpan CommandTimeoutInterval { get; set; }

        /// <summary>
        /// Try sending a message using a background worker waiting for an asynchronous reply.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>True</c> if message was completed before the trial count expired. </returns>
        public bool TrySendAsync( string value )
        {
            return this.TrySendAsync( value, this.CommandTrialCount, this.CommandPollInterval, this.CommandTimeoutInterval );
        }

        /// <summary> Try sending a message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">        The value. </param>
        /// <param name="trialCount">   Number of trials. </param>
        /// <param name="pollInterval"> The poll interval. </param>
        /// <param name="timeout">      The timeout. </param>
        /// <returns> <c>True</c> if message was completed before the trial count expired. </returns>
        public bool TrySendAsync( string value, int trialCount, TimeSpan pollInterval, TimeSpan timeout )
        {
            // wait for previous operation to complete.
            var sw = Stopwatch.StartNew();
            while ( !this.IsDisposed && this.Worker.IsBusy && sw.Elapsed <= timeout )
                Core.ApplianceBase.DoEvents();
            if ( this.Worker.IsBusy )
            {
                this.Worker.CancelAsync();
            }

            sw.Restart();
            while ( !this.IsDisposed && this.Worker.IsBusy && sw.Elapsed <= timeout )
                Core.ApplianceBase.DoEvents();
            if ( this.Worker.IsBusy )
            {
                return false;
            }

            var payload = new WorkerPayLoad() {
                Message = value,
                TrialCount = trialCount,
                PollInterval = pollInterval,
                TimeoutInterval = timeout,
                ResendOnRetry = false
            };
            if ( !(this.IsDisposed || this.Worker.IsBusy) )
            {
                sw.Restart();
                this.Worker.RunWorkerAsync( payload );
                // wait for worker to get busy.
                while ( !(this.IsDisposed || this.Worker.IsBusy) )
                    Core.ApplianceBase.DoEvents();
                // wait till worker is done
                while ( !this.IsDisposed && this.Worker.IsBusy && sw.Elapsed <= payload.TimeoutInterval )
                    Core.ApplianceBase.DoEvents();
                while ( !this.IsDisposed && this.Worker.IsBusy )
                    Core.ApplianceBase.DoEvents();
            }

            return this.MessageCompleted;
        }

        /// <summary> Clears the send sentinels. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ClearSendSentinels()
        {
            this.TestCompleteSent = false;
            this.UnhandledMessageSent = false;
        }

        #endregion

        #region" EMULATE "

        /// <summary> Emulates sending a Prober message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="command">   The command. </param>
        /// <param name="timeDelay"> The time delay. </param>
        public void Emulate( string command, TimeSpan timeDelay )
        {
            this.ProberCommand = string.Empty;
            if ( this.ProberTimer is object )
            {
                this.ProberTimer.Stop();
                this.ProberCommand = command;
                this.ProberTimer.Interval = timeDelay.TotalMilliseconds;
                this.ProberTimer.Start();
            }
        }

        /// <summary> Gets or sets the Prober command. </summary>
        /// <value> The Prober command. </value>
        private string ProberCommand { get; set; }

        private System.Timers.Timer _ProberTimer;

        private System.Timers.Timer ProberTimer
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._ProberTimer;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._ProberTimer != null )
                {

                    this._ProberTimer.Elapsed -= this.ProberTimer_Elapsed;
                }

                this._ProberTimer = value;
                if ( this._ProberTimer != null )
                {
                    this._ProberTimer.Elapsed += this.ProberTimer_Elapsed;
                }
            }
        }


        /// <summary> Event handler. Called by the Prober Timer for elapsed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Elapsed event information. </param>
        private void ProberTimer_Elapsed( object sender, System.Timers.ElapsedEventArgs e )
        {
            if ( this.ProberTimer is object )
            {
                this.ProberTimer.Stop();
            }

            if ( !string.IsNullOrWhiteSpace( this.ProberCommand ) )
            {
                string value = this.ProberCommand;
                this.ProberCommand = string.Empty;
                this.LastReading = value;
                this.ParseReading( value );
            }
        }

        #endregion

        #region" FETCH "

        /// <summary> The supported reply patterns. </summary>
        private List<string> _SupportedReplyPatterns;

        /// <summary> Gets or sets the supported reply patterns. </summary>
        /// <value> The supported reply patterns. </value>
        public IList<string> SupportedReplyPatterns
        {
            get => this._SupportedReplyPatterns;

            set {
                this._SupportedReplyPatterns = new List<string>( value );
                this.NotifyPropertyChanged();
            }
        }

        /// <summary> Clears the fetch sentinels. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ClearFetchSentinels()
        {
            this.ErrorRead = false;
            this.IdentityRead = false;
            this.MessageCompleted = false;
            this.MessageFailed = false;
            this.UnhandledMessageReceived = false;
        }

        /// <summary> Parses the message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="reading"> The reading. </param>
        /// <returns> True if valid reading, false if not. </returns>
        public bool IsValidReading( string reading )
        {
            bool result = false;
            if ( !string.IsNullOrWhiteSpace( reading ) )
            {
                result = reading.StartsWith( this.ErrorReplyPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.IdentityReplyPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.PatternCompleteReplyPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.FirstTestStartPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.TestStartPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.RetestStartPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.TestAgainStartPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.WaferStartPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.MessageCompletedPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( this.MessageFailedPattern, StringComparison.OrdinalIgnoreCase ) || reading.StartsWith( "Keithley", StringComparison.OrdinalIgnoreCase );
            }

            return result;
        }

        /// <summary> Parses the message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="reading"> The reading. </param>
        public override void ParseReading( string reading )
        {
            this.ClearFetchSentinels();
            if ( string.IsNullOrWhiteSpace( reading ) )
            {
            }
            else if ( reading.StartsWith( this.ErrorReplyPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                // message is completed if the reply is in response to an error query.
                this.MessageCompleted = true;
                this.ErrorRead = false;
                this.ErrorRead = true;
            }
            else if ( reading.StartsWith( this.IdentityReplyPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                this.IdentityRead = false;
                this.IdentityRead = true;
                this.MessageCompleted = true;
            }
            else if ( reading.StartsWith( this.PatternCompleteReplyPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                // tag message completed as it is assumed that this is the reply to the test complete command.
                this.MessageCompleted = true;
                this.PatternCompleteReceived = false;
                this.PatternCompleteReceived = true;
            }
            else if ( reading.StartsWith( this.FirstTestStartPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                // tag message completed as it is assumed that this is the reply to the test complete command.
                this.MessageCompleted = true;
                this.RetestRequested = false;
                this.TestAgainRequested = false;
                this.IsFirstTestStart = true;
                this.TestStartReceived = true;
            }
            else if ( reading.StartsWith( this.TestStartPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                // tag message completed as it is assumed that this is the reply to the test complete command.
                this.MessageCompleted = true;
                this.RetestRequested = false;
                this.TestAgainRequested = false;
                this.IsFirstTestStart = false;
                this.TestStartReceived = true;
            }
            else if ( reading.StartsWith( this.RetestStartPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                // tag message completed as it is assumed that this is the reply to the test complete command.
                this.MessageCompleted = true;
                this.RetestRequested = true;
                this.TestAgainRequested = false;
                this.IsFirstTestStart = false;
                this.TestStartReceived = true;
            }
            else if ( reading.StartsWith( this.TestAgainStartPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                // tag message completed as it is assumed that this is the reply to the test complete command.
                this.MessageCompleted = true;
                this.RetestRequested = false;
                this.TestAgainRequested = true;
                this.IsFirstTestStart = false;
                this.TestStartReceived = true;
            }
            else if ( reading.StartsWith( this.WaferStartPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                this.WaferStartReceived = false;
                this.WaferStartReceived = true;
            }
            else if ( reading.StartsWith( this.MessageCompletedPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                this.MessageCompleted = false;
                this.MessageCompleted = true;
            }
            else if ( reading.StartsWith( this.MessageFailedPattern, StringComparison.OrdinalIgnoreCase ) )
            {
                this.MessageFailed = true;
            }
            else if ( reading.StartsWith( "Keithley", StringComparison.OrdinalIgnoreCase ) )
            {
                this.MessageCompleted = true;
            }
            else
            {
                this.UnhandledMessageReceived = true;
            }
        }

        #endregion

    }
}
