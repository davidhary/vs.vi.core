namespace isr.VI.EG2001
{

    /// <summary> Defines a System Subsystem for a EG2001 Prober. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-01, 3.0.5022. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region" COMMAND SYNTAX "

        /// <summary> Gets or sets the preset command. </summary>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = Pith.Scpi.Syntax.SystemPresetCommand;

        #endregion

    }
}
