using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> K3706 Device with K3730 Matrix Resistance Measurements unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3730" )]
    public class K3730Tests
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// 	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// 	''' <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( K3730Settings.Get().Exists, $"{typeof( K3730Settings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// 	''' <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// 	''' Gets the test context which provides information about and functionality for the current test
        /// 	''' run.
        /// 	''' </summary>
        /// 	''' <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// 	''' <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " CHANNEL SUBSYSTEM TEST "

        /// <summary> Assert channel subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertChannelSubsystemInfoShouldPass( K3706Device device )
        {
            string expectedChannelList = string.Empty;
            string actualChannelList = device.ChannelSubsystem.QueryClosedChannels();
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Initial {typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
            expectedChannelList = K3730Settings.Get().ResistorChannelList;
            actualChannelList = device.ChannelSubsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"{typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
            expectedChannelList = string.Empty;
            actualChannelList = device.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Open All {typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
        }

        /// <summary> (Unit Test Method) channel subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ChannelSubsystemInfoShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertChannelSubsystemInfoShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region  MEASURE RESISTANCE 

        /// <summary> Measure resistance should prepare. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void MeasureResistanceShouldPrepare( K3706Device device )
        {
            double expectedPowerLineCycles = K3730Settings.Get().PowerLineCycles;
            double actualPowerLineCycles = device.MultimeterSubsystem.ApplyPowerLineCycles( expectedPowerLineCycles ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, 60d / TimeSpan.TicksPerSecond, $"{typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )} Is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}" );
            bool expectedBoolean = K3730Settings.Get().AutoRangeEnabled;
            bool actualBoolean = device.MultimeterSubsystem.ApplyAutoRangeEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoRangeEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            expectedBoolean = K3730Settings.Get().AutoZeroEnabled;
            actualBoolean = device.MultimeterSubsystem.ApplyAutoZeroEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            var expectedFunction = K3730Settings.Get().SenseFunction;
            var actualFunction = device.MultimeterSubsystem.ApplyFunctionMode( expectedFunction ).GetValueOrDefault( MultimeterFunctionModes.ResistanceFourWire );
            Assert.AreEqual( expectedFunction, actualFunction, $"{typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FunctionMode )} is {actualFunction} ; expected {expectedFunction}" );
        }

        /// <summary> Measure resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trialNumber">   The trial number. </param>
        /// <param name="device">        The device. </param>
        /// <param name="expectedValue"> The expected value. </param>
        /// <param name="epsilon">       The epsilon. </param>
        /// <param name="channelList">   List of channels. </param>
        private static void AssertMeasureResistanceShouldPass( int trialNumber, K3706Device device, double expectedValue, double epsilon, string channelList )
        {
            string expectedChannelList = string.Empty;
            string actualChannelList = device.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Open All {trialNumber} {typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
            expectedChannelList = channelList;
            actualChannelList = device.ChannelSubsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"{typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )}='{actualChannelList}'; expected {expectedChannelList}" );
            double expectedResistance = expectedValue;
            double resistance = device.MultimeterSubsystem.MeasureReadingAmounts().GetValueOrDefault( -1 );
            Assert.AreEqual( expectedValue, resistance, epsilon, $"{typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PrimaryReadingValue )} {channelList} is {resistance}; expected {expectedResistance} within {epsilon}" );
        }

        /// <summary> (Unit Test Method) measure resistance should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MeasureResistanceShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                MeasureResistanceShouldPrepare( device );
                int trialNumber = 0;
                trialNumber += 1;
                AssertMeasureResistanceShouldPass( trialNumber, device, 0d, K3730Settings.Get().ExpectedShort32ResistanceEpsilon, K3730Settings.Get().ShortChannelList );
                trialNumber += 1;
                AssertMeasureResistanceShouldPass( trialNumber, device, K3730Settings.Get().ExpectedResistance, K3730Settings.Get().ExpectedResistanceEpsilon, K3730Settings.Get().ResistorChannelList );
                trialNumber += 1;
                AssertMeasureResistanceShouldPass( trialNumber, device, K3730Settings.Get().ExpectedOpen, 1d, K3730Settings.Get().OpenChannelList );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
