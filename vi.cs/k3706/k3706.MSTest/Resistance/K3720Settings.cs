namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> A K3720 Test Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class K3720Settings : VI.DeviceTests.TestSettingsBase
    {

        #region  SINGLETON 

        /// <summary> Specialized default constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        private K3720Settings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(K3720Settings)} Editor", Get());
        }

        /// <summary>
    /// Gets the locking object to enforce thread safety when creating the singleton instance.
    /// </summary>
    /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
    /// <value> The instance. </value>
        private static K3720Settings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
        public static K3720Settings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (K3720Settings)Synchronized(new K3720Settings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
    /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region  MEASURE SUBSYSTEM INFORMATION 

        /// <summary> Gets or sets the auto zero Enabled settings. </summary>
    /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoZeroEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the automatic range enabled. </summary>
    /// <value> The automatic range enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoRangeEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the Sense Function settings. </summary>
    /// <value> The Sense Function settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("ResistanceFourWire")]
        public MultimeterFunctionModes SenseFunction
        {
            get
            {
                return AppSettingEnum<MultimeterFunctionModes>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets the power line cycles settings. </summary>
    /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public double PowerLineCycles
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of short channels. </summary>
    /// <value> A List of short channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1030;1060;1912;1921")]
        public string ShortChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of resistor channels. </summary>
    /// <value> A List of resistor channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1028;1058;1912;1921")]
        public string ResistorChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of open channels. </summary>
    /// <value> A List of open channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1029;1059;1912;1921")]
        public string OpenChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the expected resistance. </summary>
    /// <value> The expected resistance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("10")]
        public double ExpectedResistance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the expected open. </summary>
    /// <value> The expected open. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("9.9E+37")]
        public double ExpectedOpen
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the expected resistance epsilon. </summary>
    /// <value> The expected resistance epsilon. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.4")]
        public double ExpectedResistanceEpsilon
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the expected zero resistance epsilon. </summary>
    /// <value> The expected zero resistance epsilon. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.001")]
        public double ExpectedZeroResistanceEpsilon
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the expected short 32 resistance epsilon. </summary>
    /// <value> The expected short 32 resistance epsilon. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.05")]
        public double ExpectedShort32ResistanceEpsilon
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}
