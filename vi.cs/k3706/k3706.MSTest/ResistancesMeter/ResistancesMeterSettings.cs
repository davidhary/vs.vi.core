namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> A resistances meter Test Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode( "Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0" )]
    [System.ComponentModel.EditorBrowsable( System.ComponentModel.EditorBrowsableState.Advanced )]
    internal class ResistancesMeterSettings : VI.DeviceTests.SubsystemsSettingsBase
    {

        #region  SINGLETON 

        /// <summary> Specialized default constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private ResistancesMeterSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( $"{typeof( ResistancesMeterSettings )} Editor", Get() );
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static ResistancesMeterSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static ResistancesMeterSettings Get()
        {
            if ( _Instance is null )
            {
                lock ( _SyncLocker )
                    _Instance = ( ResistancesMeterSettings ) Synchronized( new ResistancesMeterSettings() );
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get {
                lock ( _SyncLocker )
                    return _Instance is object;
            }
        }

        #endregion

        #region  MEASURE SUBSYSTEM INFORMATION 

        /// <summary> Gets or sets the auto zero Enabled settings. </summary>
        /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool AutoZeroEnabled
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the automatic range enabled. </summary>
        /// <value> The automatic range enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "True" )]
        public bool AutoRangeEnabled
        {
            get {
                return AppSettingGetter( false );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the Sense Function settings. </summary>
        /// <value> The Sense Function settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "ResistanceFourWire" )]
        public MultimeterFunctionModes SenseFunctionMode
        {
            get {
                return AppSettingEnum<MultimeterFunctionModes>();
            }

            set {
                AppSettingEnumSetter( value );
            }
        }

        /// <summary> Gets or sets the power line cycles settings. </summary>
        /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1" )]
        public double PowerLineCycles
        {
            get {
                return AppSettingGetter( 0.0d );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

        #region  RESISTANCES METER INFORMATION 

        /// <summary> Gets or sets a list of 1 channels. </summary>
        /// <value> A List of 1 channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1001;1031;1912;1921" )]
        public string R1ChannelList
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets a list of 2 channels. </summary>
        /// <value> A List of 2 channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1002;1032;1912;1921" )]
        public string R2ChannelList
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets a list of 3 channels. </summary>
        /// <value> A List of 3 channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1003;1033;1912;1921" )]
        public string R3ChannelList
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets a list of 4 channels. </summary>
        /// <value> A List of 4 channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "1004;1034;1912;1921" )]
        public string R4ChannelList
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets or sets the 1 values. </summary>
        /// <value> The r 1 values. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "500|464" )]
        public string R1Values
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets the r 1. </summary>
        /// <value> The r 1. </value>
        public double R1
        {
            get {
                return double.Parse( R1Values.Split( '|' )[ResourceSettings.Get().HostInfoIndex] );
            }
        }

        /// <summary> Gets or sets the 2 values. </summary>
        /// <value> The r 2 values. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "500|462" )]
        public string R2Values
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets the r 2. </summary>
        /// <value> The r 2. </value>
        public double R2
        {
            get {
                return double.Parse( R2Values.Split( '|' )[ResourceSettings.Get().HostInfoIndex] );
            }
        }

        /// <summary> Gets or sets the 3 values. </summary>
        /// <value> The r 3 values. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "500|464" )]
        public string R3Values
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets the r 3. </summary>
        /// <value> The r 3. </value>
        public double R3
        {
            get {
                return double.Parse( R3Values.Split( '|' )[ResourceSettings.Get().HostInfoIndex] );
            }
        }

        /// <summary> Gets or sets the 4 values. </summary>
        /// <value> The r 4 values. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "500|465" )]
        public string R4Values
        {
            get {
                return AppSettingGetter( string.Empty );
            }

            set {
                AppSettingSetter( value );
            }
        }

        /// <summary> Gets the r 4. </summary>
        /// <value> The r 4. </value>
        public double R4
        {
            get {
                return double.Parse( R4Values.Split( '|' )[ResourceSettings.Get().HostInfoIndex] );
            }
        }

        /// <summary> Gets or sets the resistance epsilon. </summary>
        /// <remarks> Is rather high to allow temperature effects. </remarks>
        /// <value> The resistance epsilon. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue( "20" )]
        public double ResistanceEpsilon
        {
            get {
                return AppSettingGetter( 0.0d );
            }

            set {
                AppSettingSetter( value );
            }
        }

        #endregion

    }

    /// <summary> Collection of resistors. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-18 </para>
    /// </remarks>
    public class ResistorCollection : ChannelResistorCollection
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ResistorCollection() : base()
        {
            this.AddResistor( "R1", ResistancesMeterSettings.Get().R1ChannelList );
            this.AddResistor( "R2", ResistancesMeterSettings.Get().R2ChannelList );
            this.AddResistor( "R3", ResistancesMeterSettings.Get().R3ChannelList );
            this.AddResistor( "R4", ResistancesMeterSettings.Get().R4ChannelList );
        }
    }
}
