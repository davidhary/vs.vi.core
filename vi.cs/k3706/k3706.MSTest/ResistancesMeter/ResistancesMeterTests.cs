using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> Resistances meter unit tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-15 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3720" )]
    public class ResistancesMeterTests
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
                _ResistancesMeterDevice = ResistancesMeterDevice.Create();
                _ResistancesMeterDevice.AddListener( TestInfo.TraceMessagesQueueListener );
                _ResistancesMeterDevice.Populate( new ResistorCollection() );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
            if ( _ResistancesMeterDevice is object )
            {
                _ResistancesMeterDevice.Dispose();
                _ResistancesMeterDevice = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( ResistancesMeterSettings.Get().Exists, $"{typeof( ResistancesMeterSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region  SHARED DEVICE 

        /// <summary> The resistances meter device. </summary>
        private static ResistancesMeterDevice _ResistancesMeterDevice;

        /// <summary> Queries if a given assert session should open. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trialNumber"> The trial number. </param>
        /// <param name="device">      The resistance meter device. </param>
        internal static void AssertSessionShouldOpen( int trialNumber, ResistancesMeterDevice device )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            var (Success, Details) = device.TryOpenSession( ResourceSettings.Get().ResourceName, ResourceSettings.Get().ResourceTitle );
            Assert.IsTrue( Success, $"{trialNumber} session should open; {Details}" );
            Assert.IsTrue( device.IsDeviceOpen, $"{trialNumber} session to device {device.ResourceNameCaption} should be open" );

            // check the MODEL
            Assert.AreEqual( ResourceSettings.Get().ResourceModel, device.StatusSubsystem.VersionInfo.Model, $"Version Info Model {device.ResourceNameCaption}", System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Closes a session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trialNumber"> The trial number. </param>
        /// <param name="device">      The resistance meter device. </param>
        internal static void AssertSessionShouldClose( int trialNumber, ResistancesMeterDevice device )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            bool actualBoolean;
            _ = device.TryCloseSession();
            actualBoolean = device.IsDeviceOpen;
            bool expectedBoolean = false;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} Disconnect still connected {device.ResourceNameCaption}" );
            actualBoolean = device.IsDeviceOpen;
            expectedBoolean = false;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} Close still open {device.ResourceNameCaption}" );
        }

        #endregion

        #region " DEVICE OPEN TEST "

        /// <summary> (Unit Test Method) resistances meter should open close. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ResistancesMeterShouldOpenClose()
        {
            AssertSessionShouldOpen( 1, _ResistancesMeterDevice );
            AssertSessionShouldClose( 1, _ResistancesMeterDevice );
        }

        #endregion

        #region  CONFIGURE TEST 

        /// <summary> (Unit Test Method) resistances meter should configure. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        [TestMethod()]
        public void ResistancesMeterShouldConfigure()
        {
            AssertSessionShouldOpen( 1, _ResistancesMeterDevice );
            var (success, details) = _ResistancesMeterDevice.TryConfigureMeter( ResistancesMeterSettings.Get().PowerLineCycles );
            TestInfo.AssertMessageQueue();
            Assert.IsTrue( success, $"Configuring Resistances meter failed; {details}" );
            AssertSessionShouldClose( 1, _ResistancesMeterDevice );
        }

        #endregion

        #region  MEASURE RESISTOR 

        /// <summary> Resistances meter measure resistor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The resistance meter device. </param>
        private static void AssertResistancesMeterShouldMeasureResistor( ResistancesMeterDevice device )
        {
            var r = device.TryConfigureMeter( ResistancesMeterSettings.Get().PowerLineCycles );
            TestInfo.AssertMessageQueue();
            Assert.IsTrue( r.Success, $"Configuring Resistances meter failed; {r.Details}" );
            var resistor = device.Resistors[0];
            r = device.TryMeasureResistance( resistor );
            TestInfo.AssertMessageQueue();
            Assert.IsTrue( r.Success, $"Measuring resistor {resistor.Title} failed; {r.Details}" );
            Assert.AreEqual( ResistancesMeterSettings.Get().R1, resistor.Resistance, ResistancesMeterSettings.Get().ResistanceEpsilon, $"Measuring resistor resistance expected {ResistancesMeterSettings.Get().R1:G5} actual {resistor.Resistance:G5}" );
        }

        /// <summary> (Unit Test Method) resistances meter should measure resistor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ResistancesMeterShouldMeasureResistor()
        {
            AssertSessionShouldOpen( 1, _ResistancesMeterDevice );
            AssertResistancesMeterShouldMeasureResistor( _ResistancesMeterDevice );
            AssertSessionShouldClose( 1, _ResistancesMeterDevice );
        }

        #endregion

        #region  MEASURE RESISTANCES 

        /// <summary> Assert resistances meter should measure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The resistance meter device. </param>
        private static void AssertResistancesMeterShouldMeasure( ResistancesMeterDevice device )
        {
            var r = device.TryConfigureMeter( ResistancesMeterSettings.Get().PowerLineCycles );
            TestInfo.AssertMessageQueue();
            Assert.IsTrue( r.Success, $"Configuring resistances meter failed; {r.Details}" );
            r = device.TryMeasureResistors();
            TestInfo.AssertMessageQueue();
            Assert.IsTrue( r.Success, $"Measuring resistances failed; {r.Details}" );
            var resistor = device.Resistors[0];
            double expectedResistance = ResistancesMeterSettings.Get().R1;
            Assert.AreEqual( expectedResistance, resistor.Resistance, ResistancesMeterSettings.Get().ResistanceEpsilon, $"Measuring resistor {resistor.Title} resistance expected {expectedResistance:G5} actual {resistor.Resistance:G5}" );
            resistor = device.Resistors[1];
            expectedResistance = ResistancesMeterSettings.Get().R2;
            Assert.AreEqual( expectedResistance, resistor.Resistance, ResistancesMeterSettings.Get().ResistanceEpsilon, $"Measuring resistor {resistor.Title} resistance expected {expectedResistance:G5} actual {resistor.Resistance:G5}" );
            resistor = device.Resistors[2];
            expectedResistance = ResistancesMeterSettings.Get().R3;
            Assert.AreEqual( expectedResistance, resistor.Resistance, ResistancesMeterSettings.Get().ResistanceEpsilon, $"Measuring resistor {resistor.Title} resistance expected {expectedResistance:G5} actual {resistor.Resistance:G5}" );
            resistor = device.Resistors[3];
            expectedResistance = ResistancesMeterSettings.Get().R4;
            Assert.AreEqual( expectedResistance, resistor.Resistance, ResistancesMeterSettings.Get().ResistanceEpsilon, $"Measuring resistor {resistor.Title} resistance expected {expectedResistance:G5} actual {resistor.Resistance:G5}" );
        }

        /// <summary> (Unit Test Method) resistances meter should measure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ResistancesMeterShouldMeasure()
        {
            AssertSessionShouldOpen( 1, _ResistancesMeterDevice );
            AssertResistancesMeterShouldMeasure( _ResistancesMeterDevice );
            AssertSessionShouldClose( 1, _ResistancesMeterDevice );
        }

        #endregion

    }
}
