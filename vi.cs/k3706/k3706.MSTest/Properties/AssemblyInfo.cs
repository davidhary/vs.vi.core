﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K3706 VI Tests" )]
[assembly: AssemblyDescription( "K3706 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K3706.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
