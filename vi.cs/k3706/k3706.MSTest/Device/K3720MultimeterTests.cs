using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> K3706 Device with K3720 Multiplexer Resistance Measurements unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3720" )]
    public class K3720MultimeterTests
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( K3720Settings.Get().Exists, $"{typeof( K3720Settings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " MULTIMETER SUBSYSTEM: RESISTANCE RANGE TEST "

        /// <summary> (Unit Test Method) multimeter subsystem resistance range should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MultimeterSubsystemResistanceRangeShouldPass()
        {
            using var device = K3706Device.Create();
            try
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                double initialRange = 0.1d * SubsystemsSettings.Get().ExpectedResistance;
                double retryFactor = 2d;
                int retryCount = 10;
                AssertCloseChannelsShouldMatch( device.ChannelSubsystem, SubsystemsSettings.Get().ResistorChannelList );
                AssertFunctionModeShouldApply( device.MultimeterSubsystem, MultimeterFunctionModes.ResistanceFourWire );
                var status = device.MultimeterSubsystem.EstablishRange( initialRange, retryFactor, retryCount );
                double expectedRange = SubsystemsSettings.Get().ExpectedResistance;
                Assert.IsTrue( device.MultimeterSubsystem.Range.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )}" );
                Assert.IsTrue( status.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( MeasuredAmount )}.{nameof( MetaStatus )}" );
                Assert.IsTrue( device.MultimeterSubsystem.Range.Value - expectedRange < 0.05d * expectedRange, $"actual {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )} {device.MultimeterSubsystem.Range.Value} too low < {expectedRange}" );
                expectedRange *= 10d;
                Assert.IsTrue( expectedRange - device.MultimeterSubsystem.Range.Value > 0.05d * expectedRange, $"actual {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )} {device.MultimeterSubsystem.Range.Value} too high > {expectedRange}" );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " MULTIMETER SUBSYSTEM: TEST VOLTAGE MEASUREMENT "

        /// <summary> Assert close channels should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">   The subsystem. </param>
        /// <param name="channelList"> List of channels. </param>
        private static void AssertCloseChannelsShouldMatch( ChannelSubsystemBase subsystem, string channelList )
        {
            string expectedChannelList = string.Empty;
            string actualChannelList = subsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Open All failed {typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )}" );
            expectedChannelList = channelList;
            actualChannelList = subsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Close channels failed {typeof( ChannelSubsystemBase )}.{nameof( ChannelSubsystemBase.ClosedChannels )}" );
        }

        /// <summary> Assert function mode should apply. </summary>
        /// <remarks> David, 2021-07-05. </remarks>
        /// <param name="subsystem">            The subsystem. </param>
        /// <param name="expectedFunctionMode"> The expected function mode. </param>
        private static void AssertFunctionModeShouldApply( MultimeterSubsystemBase subsystem, MultimeterFunctionModes expectedFunctionMode )
        {
            var actualFunctionMode = subsystem.ApplyFunctionMode( expectedFunctionMode );
            Assert.IsTrue( actualFunctionMode.HasValue, $"Failed reading {typeof( MultimeterSubsystem )}.{nameof( MultimeterSubsystemBase.FunctionMode )}" );
            Assert.AreEqual( expectedFunctionMode, ( object ) actualFunctionMode, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FunctionMode )}" );
        }

        /// <summary> Assert automatic zero once should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private static void AssertAutoZeroOnceShouldApply( MultimeterSubsystemBase subsystem )
        {
            subsystem.AutoZeroOnce();
            bool expectedBoolean = false;
            var actualBoolean = subsystem.QueryAutoZeroEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroOnce )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroOnce )} {nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )} still enabled" );
        }

        /// <summary> Assert automatic zero enabled should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertAutoZeroEnabledShouldApply( MultimeterSubsystemBase subsystem, bool value )
        {
            bool expectedBoolean = value;
            var actualBoolean = subsystem.ApplyAutoZeroEnabled( value );
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )}" );
        }

        /// <summary> Assert filter enabled should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertFilterEnabledShouldApply( MultimeterSubsystemBase subsystem, bool value )
        {
            bool expectedBoolean = value;
            var actualBoolean = subsystem.ApplyFilterEnabled( value );
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterEnabled )}" );
        }

        /// <summary> Assert moving average filter enabled should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertMovingAverageFilterEnabledShouldApply( MultimeterSubsystemBase subsystem, bool value )
        {
            bool expectedBoolean = value;
            var actualBoolean = subsystem.ApplyMovingAverageFilterEnabled( value );
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.MovingAverageFilterEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.MovingAverageFilterEnabled )}" );
        }

        /// <summary> Aseert filter count should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AseertFilterCountShouldApply( MultimeterSubsystemBase subsystem, int value )
        {
            int expectedInteger = value;
            var actualInteger = subsystem.ApplyFilterCount( value );
            Assert.IsTrue( actualInteger.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterCount )}" );
            Assert.AreEqual( expectedInteger, actualInteger.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterCount )}" );
        }

        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertFilterWindowShouldApply( MultimeterSubsystemBase subsystem, double value )
        {
            double expectedDouble = value;
            var actualDouble = subsystem.ApplyFilterWindow( value );
            Assert.IsTrue( actualDouble.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterWindow )}" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterWindow )}" );
        }

        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertPowerLineCyclesShouldApply( MultimeterSubsystemBase subsystem, double value )
        {
            double expectedDouble = value;
            var actualDouble = subsystem.ApplyPowerLineCycles( value );
            Assert.IsTrue( actualDouble.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )}" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )}" );
        }

        /// <summary> Assert range should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertRangeShouldApply( MultimeterSubsystemBase subsystem, double value )
        {
            double expectedDouble = value;
            var actualDouble = subsystem.ApplyRange( value );
            Assert.IsTrue( actualDouble.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )}" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )}" );
        }

        /// <summary> Assert measure should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private static void AssertMeasureShouldPass( MultimeterSubsystemBase subsystem )
        {
            var expectedTimeSpan = subsystem.EstimateMeasurementTime();
            int timeout = Math.Max( 10000, 4 * ( int ) Math.Round( expectedTimeSpan.TotalMilliseconds ) );
            subsystem.Session.StoreCommunicationTimeout( TimeSpan.FromMilliseconds( timeout ) );
            var sw = Stopwatch.StartNew();
            var value = subsystem.MeasurePrimaryReading();
            var actualTimespan = sw.Elapsed;
            Assert.IsTrue( value.HasValue, $"Failed reading {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.PrimaryReadingValue )}" );
            Assert.IsTrue( value.Value > -0.001d, $"Failed reading positive {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PrimaryReadingValue )}" );
            Assert.IsTrue( actualTimespan >= expectedTimeSpan, $"Reading too short; expected {expectedTimeSpan} actual {actualTimespan}" );
            var twiceInterval = expectedTimeSpan.Add( expectedTimeSpan );
            Assert.IsTrue( actualTimespan < twiceInterval, $"Reading too long; expected {twiceInterval} actual {actualTimespan}" );
            subsystem.Session.RestoreCommunicationTimeout();
        }

        /// <summary>
        /// (Unit Test Method) multimeter subsystem voltage measurement timing should pass.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MultimeterSubsystemVoltageMeasurementTimingShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                double powerLineCycles = 5d;
                int FilterCount = 20;
                double window = 0.095d;
                double range = 0.1d;
                AssertCloseChannelsShouldMatch( device.ChannelSubsystem, SubsystemsSettings.Get().ShortChannelList );
                AssertPowerLineCyclesShouldApply( device.MultimeterSubsystem, powerLineCycles );
                AssertRangeShouldApply( device.MultimeterSubsystem, range );
                AssertAutoZeroEnabledShouldApply( device.MultimeterSubsystem, false );
                AssertAutoZeroOnceShouldApply( device.MultimeterSubsystem );
                AssertMovingAverageFilterEnabledShouldApply( device.MultimeterSubsystem, false ); // use repeat filter.
                AseertFilterCountShouldApply( device.MultimeterSubsystem, FilterCount );
                AssertFilterWindowShouldApply( device.MultimeterSubsystem, window );
                AssertFilterEnabledShouldApply( device.MultimeterSubsystem, true );
                AssertMeasureShouldPass( device.MultimeterSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>
        /// (Unit Test Method) multimeter subsystem resistance measurement timing should pass.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MultimeterSubsystemResistanceMeasurementTimingShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                double powerLineCycles = 5d;
                int FilterCount = 20;
                double window = 0.095d;
                double range = 1d;
                AssertCloseChannelsShouldMatch( device.ChannelSubsystem, SubsystemsSettings.Get().ShortChannelList );
                AssertFunctionModeShouldApply( device.MultimeterSubsystem, MultimeterFunctionModes.ResistanceFourWire );
                AssertPowerLineCyclesShouldApply( device.MultimeterSubsystem, powerLineCycles );
                AssertRangeShouldApply( device.MultimeterSubsystem, range );
                AssertAutoZeroEnabledShouldApply( device.MultimeterSubsystem, false );
                AssertAutoZeroOnceShouldApply( device.MultimeterSubsystem );
                AssertMovingAverageFilterEnabledShouldApply( device.MultimeterSubsystem, false ); // use repeat filter.
                AseertFilterCountShouldApply( device.MultimeterSubsystem, FilterCount );
                AssertFilterWindowShouldApply( device.MultimeterSubsystem, window );
                AssertFilterEnabledShouldApply( device.MultimeterSubsystem, true );
                AssertMeasureShouldPass( device.MultimeterSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion


    }
}
