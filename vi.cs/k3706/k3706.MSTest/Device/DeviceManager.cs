namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> Static class for managing the common functions. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    internal sealed partial class DeviceManager
    {

        #region " CONSTRUCTION " 

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private DeviceManager() : base()
        {
        }

        #endregion

        #region " DEVICE OPEN, CLOSE "

        /// <summary> Opens a session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void OpenSession( VI.DeviceTests.TestSite testInfo, VisaSessionBase device )
        {
            VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( testInfo, device, ResourceSettings.Get() );
        }

        /// <summary> Closes a session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void CloseSession( VI.DeviceTests.TestSite testInfo, VisaSessionBase device )
        {
            VI.DeviceTests.DeviceManager.AssertDeviceShopuldCloseWithoutErrors( testInfo, device );
        }

        #endregion


    }
}
