using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> K3706 Device Gage Board measurements unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "GageBoard" )]
    public class GageBoardTests
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( GageBoardSettings.Get().Exists, $"{typeof( GageBoardSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " CHANNEL SUBSYSTEM TEST "

        /// <summary> Reads channel subsystem information. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertChannelSubsystemInfoShouldPass( K3706Device device )
        {
            string expectedChannelList = string.Empty;
            string actualChannelList = device.ChannelSubsystem.QueryClosedChannels();
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Initial {nameof( ChannelSubsystem )}.{nameof( ChannelSubsystem.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
            expectedChannelList = GageBoardSettings.Get().FirstRtdChannelList;
            actualChannelList = device.ChannelSubsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"{nameof( ChannelSubsystem )}.{nameof( ChannelSubsystem.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
            expectedChannelList = string.Empty;
            actualChannelList = device.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Open All {nameof( ChannelSubsystem )}.{nameof( ChannelSubsystem.ClosedChannels )} is {actualChannelList}; expected {expectedChannelList}" );
        }

        /// <summary> (Unit Test Method) tests read channel subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ChannelSubsystemInfoShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertChannelSubsystemInfoShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region  GAGE BOARD SUBSYSTEM TEST 

        /// <summary> Assert multimeter subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertMultimeterSubsystemInfoShouldPass( K3706Device device )
        {
            double expectedPowerLineCycles = SubsystemsSettings.Get().InitialPowerLineCycles;
            double actualPowerLineCycles = device.MultimeterSubsystem.QueryPowerLineCycles().GetValueOrDefault( 0d );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, SubsystemsSettings.Get().LineFrequency / TimeSpan.TicksPerSecond, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.PowerLineCycles )} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}" );
            bool expectedBoolean = SubsystemsSettings.Get().InitialAutoRangeEnabled;
            bool actualBoolean = device.MultimeterSubsystem.QueryAutoRangeEnabled().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.AutoRangeEnabled )} Is {actualBoolean}; expected {expectedBoolean}" );
            expectedBoolean = SubsystemsSettings.Get().InitialAutoZeroEnabled;
            actualBoolean = device.MultimeterSubsystem.QueryAutoZeroEnabled().GetValueOrDefault( false );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.AutoZeroEnabled )} Is {actualBoolean}; expected {expectedBoolean}" );
            var expectedFunction = SubsystemsSettings.Get().InitialMultimeterFunction;
            var senseFn = device.MultimeterSubsystem.QueryFunctionMode().GetValueOrDefault( MultimeterFunctionModes.ResistanceTwoWire );
            Assert.AreEqual( expectedFunction, senseFn, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.FunctionMode )} Is {senseFn} ; expected {expectedFunction}" );
        }

        /// <summary> (Unit Test Method) multimeter subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MultimeterSubsystemInfoShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertMultimeterSubsystemInfoShouldPass( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region  GAGE BOARD RESISTANCE 

        /// <summary> Assert gage board resistance should prepare. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertGageBoardResistanceShouldPrepare( K3706Device device )
        {
            double expectedPowerLineCycles = GageBoardSettings.Get().PowerLineCycles;
            double actualPowerLineCycles = device.MultimeterSubsystem.ApplyPowerLineCycles( expectedPowerLineCycles ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, 60d / TimeSpan.TicksPerSecond, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.PowerLineCycles )} Is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}" );
            bool expectedBoolean = GageBoardSettings.Get().AutoRangeEnabled;
            bool actualBoolean = device.MultimeterSubsystem.ApplyAutoRangeEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.AutoRangeEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            expectedBoolean = GageBoardSettings.Get().AutoZeroEnabled;
            actualBoolean = device.MultimeterSubsystem.ApplyAutoZeroEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.IsTrue( actualBoolean, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.AutoZeroEnabled )} is {actualBoolean}; expected {expectedBoolean}" );
            var expectedFunction = GageBoardSettings.Get().SenseFunctionMode;
            var actualFunction = device.MultimeterSubsystem.ApplyFunctionMode( expectedFunction ).GetValueOrDefault( MultimeterFunctionModes.ResistanceFourWire );
            Assert.AreEqual( expectedFunction, actualFunction, $"{nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.FunctionMode )} is {actualFunction} ; expected {expectedFunction}" );
            bool expectedDetectorEnabled = false;
            _ = device.MultimeterSubsystem.ApplyOpenDetectorEnabled( expectedDetectorEnabled );
            Assert.IsTrue( device.MultimeterSubsystem.OpenDetectorEnabled.HasValue, $"Detector enabled has value: {device.MultimeterSubsystem.OpenDetectorEnabled.HasValue}" );
            bool detectorEnabled = device.MultimeterSubsystem.OpenDetectorEnabled.Value;
            Assert.AreEqual( expectedDetectorEnabled, detectorEnabled, $"Detector enabled {device.MultimeterSubsystem.OpenDetectorEnabled.Value}" );
        }

        /// <summary> Assert gage board resistance should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trialNumber">   The trial number. </param>
        /// <param name="device">        The device. </param>
        /// <param name="expectedValue"> The expected value. </param>
        /// <param name="epsilon">       The epsilon. </param>
        /// <param name="channelList">   List of channels. </param>
        private static void AssertGageBoardResistanceShouldMatch( int trialNumber, K3706Device device, double expectedValue, double epsilon, string channelList )
        {
            string expectedChannelList = string.Empty;
            string actualChannelList = device.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"Open All {trialNumber} {nameof( ChannelSubsystem )}.{nameof( ChannelSubsystem.ClosedChannels )}='{actualChannelList}'; expected {expectedChannelList}" );
            expectedChannelList = channelList;
            actualChannelList = device.ChannelSubsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
            Assert.AreEqual( expectedChannelList, actualChannelList, $"{trialNumber} {nameof( ChannelSubsystem )}.{nameof( ChannelSubsystem.ClosedChannels )}='{actualChannelList}'; expected {expectedChannelList}" );
            double expectedResistance = expectedValue;
            double resistance = device.MultimeterSubsystem.MeasureReadingAmounts().GetValueOrDefault( -1 );
            Assert.AreEqual( expectedValue, resistance, epsilon, $"{trialNumber} {nameof( ChannelSubsystem )}.{nameof( ChannelSubsystem.ClosedChannels )}='{actualChannelList}' {nameof( MultimeterSubsystem )}.{nameof( MultimeterSubsystem.PrimaryReadingValue )} {channelList} is {resistance}; expected {expectedResistance} within {epsilon}" );
        }

        /// <summary> (Unit Test Method) gage board resistance should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void GageBoardResistanceShouldMatch()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertGageBoardResistanceShouldPrepare( device );
                int trialNumber = 0;
                double expectedValue = GageBoardSettings.Get().RtdAmbientResistance;
                double epsilon = GageBoardSettings.Get().RtdResistanceEpsilon;
                string channelList = GageBoardSettings.Get().FirstRtdChannelList;
                trialNumber += 1;
                AssertGageBoardResistanceShouldMatch( trialNumber, device, expectedValue, epsilon, channelList );
                expectedValue = GageBoardSettings.Get().GaugeAmbientResistance;
                epsilon = GageBoardSettings.Get().GaugeResistanceEpsilon;
                channelList = GageBoardSettings.Get().FirstGaugeChannelList;
                trialNumber += 1;
                AssertGageBoardResistanceShouldMatch( trialNumber, device, expectedValue, epsilon, channelList );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) two wire circuit should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void TwoWireCircuitShouldPass()
        {
            using var device = K3706Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertGageBoardResistanceShouldPrepare( device );
                int trialNumber = 0;
                double expectedValue = GageBoardSettings.Get().RtdAmbientResistance;
                double epsilon = GageBoardSettings.Get().RtdResistanceEpsilon;
                string channelList = GageBoardSettings.Get().TwoWireRtdSenseChannelList;
                trialNumber += 1;
                AssertGageBoardResistanceShouldMatch( trialNumber, device, expectedValue, epsilon, channelList );
                expectedValue = GageBoardSettings.Get().RtdAmbientResistance + GageBoardSettings.Get().GaugeAmbientResistance;
                epsilon = GageBoardSettings.Get().RtdResistanceEpsilon + GageBoardSettings.Get().GaugeResistanceEpsilon;
                channelList = GageBoardSettings.Get().TwoWireRtdSourceChannelList;
                trialNumber += 1;
                AssertGageBoardResistanceShouldMatch( trialNumber, device, expectedValue, epsilon, channelList );
                expectedValue = GageBoardSettings.Get().GaugeAmbientResistance;
                epsilon = GageBoardSettings.Get().GaugeResistanceEpsilon;
                channelList = GageBoardSettings.Get().TwoWireGageSenseChannelList;
                trialNumber += 1;
                AssertGageBoardResistanceShouldMatch( trialNumber, device, expectedValue, epsilon, channelList );
                expectedValue = 3d * GageBoardSettings.Get().GaugeAmbientResistance;
                epsilon = 3d * GageBoardSettings.Get().GaugeResistanceEpsilon;
                channelList = GageBoardSettings.Get().TwoWireGageSourceChannelList;
                trialNumber += 1;
                AssertGageBoardResistanceShouldMatch( trialNumber, device, expectedValue, epsilon, channelList );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
