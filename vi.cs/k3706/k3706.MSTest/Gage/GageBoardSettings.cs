namespace isr.VI.Tsp.K3706.MSTest
{

    /// <summary> A Gauge Board Tests Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class GageBoardSettings : VI.DeviceTests.TestSettingsBase
    {

        #region  SINGLETON 

        /// <summary> Specialized default constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        private GageBoardSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(GageBoardSettings)} Editor", Get());
        }

        /// <summary>
    /// Gets the locking object to enforce thread safety when creating the singleton instance.
    /// </summary>
    /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
    /// <value> The instance. </value>
        private static GageBoardSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
        public static GageBoardSettings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (GageBoardSettings)Synchronized(new GageBoardSettings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
    /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region  MEASURE SUBSYSTEM INFORMATION 

        /// <summary> Gets or sets the auto zero Enabled settings. </summary>
    /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoZeroEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the automatic range enabled. </summary>
    /// <value> The automatic range enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoRangeEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the Sense Function settings. </summary>
    /// <value> The Sense Function settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("CurrentDC")]
        public MultimeterFunctionModes SenseFunctionMode
        {
            get
            {
                return AppSettingEnum<MultimeterFunctionModes>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets the power line cycles settings. </summary>
    /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public double PowerLineCycles
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of first RTD channels. </summary>
    /// <value> A list of first RTD channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3001,3031,3912,3921")]
        public string FirstRtdChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the rtd ambient resistance. </summary>
    /// <value> The rtd ambient resistance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("100")]
        public double RtdAmbientResistance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the rtd resistance epsilon. </summary>
    /// <value> The rtd resistance epsilon. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("5")]
        public double RtdResistanceEpsilon
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of first gauge channels. </summary>
    /// <value> A list of first gauge channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3002,3032,3912,3921")]
        public string FirstGaugeChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the gauge ambient resistance. </summary>
    /// <value> The gauge ambient resistance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1050")]
        public double GaugeAmbientResistance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the gauge resistance epsilon. </summary>
    /// <value> The gauge resistance epsilon. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("50")]
        public double GaugeResistanceEpsilon
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of two wire rtd sense channels. </summary>
    /// <value> A list of two wire rtd sense channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3001,3911,3912")]
        public string TwoWireRtdSenseChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of two wire rtd source channels. </summary>
    /// <value> A list of two wire rtd source channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3031,3921,3922")]
        public string TwoWireRtdSourceChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of two wire gage sense channels. </summary>
    /// <value> A list of two wire gage sense channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3003,3911,3912")]
        public string TwoWireGageSenseChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets a list of two wire gage source channels. </summary>
    /// <value> A list of two wire gage source channels. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3033,3921,3922")]
        public string TwoWireGageSourceChannelList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }


        #endregion

    }
}
