﻿using System;
using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K3706.Forms
{
    [DesignerGenerated()]
    public partial class MovingWindowView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;
        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _InstrumentLayout = new System.Windows.Forms.TableLayoutPanel();
            _InstrumentPanel = new K3706View();
            _MovingWindowMeter = new MovingWindowMeter();
            _InstrumentLayout.SuspendLayout();
            SuspendLayout();
            // 
            // _InstrumentLayout
            // 
            _InstrumentLayout.ColumnCount = 3;
            _InstrumentLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _InstrumentLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _InstrumentLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _InstrumentLayout.Controls.Add(_InstrumentPanel, 1, 2);
            _InstrumentLayout.Controls.Add(_MovingWindowMeter, 1, 1);
            _InstrumentLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _InstrumentLayout.Location = new System.Drawing.Point(0, 0);
            _InstrumentLayout.Name = "_InstrumentLayout";
            _InstrumentLayout.RowCount = 4;
            _InstrumentLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _InstrumentLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _InstrumentLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _InstrumentLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _InstrumentLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _InstrumentLayout.Size = new System.Drawing.Size(412, 627);
            _InstrumentLayout.TabIndex = 1;
            // 
            // _InstrumentPanel
            // 
            _InstrumentPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _InstrumentPanel.BackColor = System.Drawing.Color.Transparent;
            _InstrumentPanel.Cursor = System.Windows.Forms.Cursors.Default;
            _InstrumentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            _InstrumentPanel.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _InstrumentPanel.Location = new System.Drawing.Point(6, 135);
            _InstrumentPanel.Name = "_InstrumentPanel";
            _InstrumentPanel.PublishBindingSuccessEnabled = false;
            _InstrumentPanel.Size = new System.Drawing.Size(400, 486);
            _InstrumentPanel.TabIndex = 0;
            _InstrumentPanel.WithResistancesMetersView = false;
            // 
            // _MovingWindowMeter
            // 
            _MovingWindowMeter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _MovingWindowMeter.Dock = System.Windows.Forms.DockStyle.Top;
            _MovingWindowMeter.ExpectedStopTimeout = TimeSpan.Parse("00:00:00.1000000");
            _MovingWindowMeter.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MovingWindowMeter.Length = 40;
            _MovingWindowMeter.Location = new System.Drawing.Point(6, 6);
            _MovingWindowMeter.Name = "_MovingWindowMeter";
            _MovingWindowMeter.PublishBindingSuccessEnabled = false;
            _MovingWindowMeter.Size = new System.Drawing.Size(400, 123);
            _MovingWindowMeter.TabIndex = 1;
            // 
            // MovingWindowView
            // 
            BackColor = System.Drawing.SystemColors.Control;
            Controls.Add(_InstrumentLayout);
            Location = new System.Drawing.Point(297, 150);
            Name = "MovingWindowView";
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Padding = new System.Windows.Forms.Padding(1);
            RightToLeft = System.Windows.Forms.RightToLeft.No;
            Size = new System.Drawing.Size(412, 627);
            ToolTip.IsBalloon = true;
            _InstrumentLayout.ResumeLayout(false);
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _InstrumentLayout;
        private K3706View _InstrumentPanel;
        private MovingWindowMeter _MovingWindowMeter;
    }
}