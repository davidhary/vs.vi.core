using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.VI.Tsp.K3706.Forms
{
    public partial class MovingWindowMeter
    {

        /// <summary> Initializes the worker. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void InitializeWorker()
        {
            this.Worker = new BackgroundWorker() {
                WorkerSupportsCancellation = true,
                WorkerReportsProgress = true
            };
        }

        /// <summary> Gets the measurement rate. </summary>
        /// <value> The measurement rate. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double MeasurementRate { get; set; } = 25d;

        /// <summary> Worker payload. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private class WorkerPayLoad
        {

            /// <summary> Gets the device. </summary>
            /// <value> The device. </value>
            public K3706Device Device { get; set; }

            /// <summary> Gets the moving window. </summary>
            /// <value> The moving window. </value>
            public Core.MovingFilters.MovingWindow MovingWindow { get; set; }

            /// <summary> Gets the estimated count-out. </summary>
            /// <value> The estimated count-out. </value>
            public int EstimatedCountout { get; set; }

            /// <summary> Gets the number of do events. </summary>
            /// <value> The number of do events. </value>
            public int DoEventCount { get; set; } = 10;

            /// <summary> Clears the known state. </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            public void ClearKnownState()
            {
                this.MovingWindow.ClearKnownState();
            }

            /// <summary> Initializes the known state. </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="measurementRate"> The measurement rate. </param>
            public void InitializeKnownState( double measurementRate )
            {
                this.EstimatedCountout = ( int ) Math.Round( measurementRate * this.MovingWindow.TimeoutInterval.TotalSeconds );
            }
        }

        /// <summary> A user state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private class UserState
        {

            /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            public UserState() : base()
            {
                this.Result = new TaskResult();
            }

            /// <summary> Gets the result. </summary>
            /// <value> The result. </value>
            public TaskResult Result { get; set; }

            /// <summary> Gets the moving average. </summary>
            /// <value> The moving average. </value>
            public Core.MovingFilters.MovingWindow MovingAverage { get; set; }

            /// <summary> Gets the estimated count-out. </summary>
            /// <value> The estimated count-out. </value>
            public int EstimatedCountout { get; set; }

            /// <summary> Gets the percent progress. </summary>
            /// <value> The percent progress. </value>
            public int PercentProgress
            {
                get {
                    int baseCount = 0;
                    int count = (this.MovingAverage?.TotalReadingsCount).GetValueOrDefault( 0 );
                    if ( count < 2 * this.MovingAverage.Length )
                    {
                        baseCount = 2 * this.MovingAverage.Length;
                    }
                    else if ( this.EstimatedCountout > 0 )
                    {
                        baseCount = this.EstimatedCountout;
                    }

                    return baseCount > 0
                        ? ( int ) Math.Round( 100 * count / ( double ) baseCount )
                        : this.MovingAverage.TimeoutInterval > TimeSpan.Zero
                            ? ( int ) Math.Round( 100d * this.MovingAverage.ElapsedMilliseconds / this.MovingAverage.TimeoutInterval.TotalMilliseconds )
                            : this.LogPercentProgress;
                }
            }

            /// <summary> Gets the log percent progress. </summary>
            /// <value> The log percent progress. </value>
            public int LogPercentProgress
            {
                get {
                    int count = (this.MovingAverage?.TotalReadingsCount).GetValueOrDefault( 0 );
                    return this.EstimatedCountout > 0
                        ? ( int ) Math.Round( 100d * Math.Log( count ) / Math.Log( this.EstimatedCountout ) )
                        : this.MovingAverage.TimeoutInterval > TimeSpan.Zero
                            ? ( int ) Math.Round( 100d * Math.Log( this.MovingAverage.ElapsedMilliseconds ) / Math.Log( this.MovingAverage.TimeoutInterval.TotalMilliseconds ) )
                            : ( int ) Math.Round( 100 * count / ( double ) this.MovingAverage.Length );
                }
            }
        }

        private BackgroundWorker _Worker;

        private BackgroundWorker Worker
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._Worker;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._Worker != null )
                {

                    this._Worker.DoWork -= this.Worker_DoWork;

                    this._Worker.RunWorkerCompleted -= this.Worker_RunWorkerCompleted;

                    this._Worker.ProgressChanged -= this.Worker_ProgressChanged;
                }

                this._Worker = value;
                if ( this._Worker != null )
                {
                    this._Worker.DoWork += this.Worker_DoWork;
                    this._Worker.RunWorkerCompleted += this.Worker_RunWorkerCompleted;
                    this._Worker.ProgressChanged += this.Worker_ProgressChanged;
                }
            }
        }

        /// <summary> Worker do work. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Do work event information. </param>
        private void Worker_DoWork( object sender, DoWorkEventArgs e )
        {
            // TO_DO: Add to view model. Me.ApplyCapturedSyncContext()
            if ( sender is not BackgroundWorker w || this.IsDisposed || e is null || e.Cancel )
                return;
            var userState = new UserState();
            if ( e.Argument is not WorkerPayLoad payload )
            {
                userState.Result.RegisterFailure( "Payload Not assigned to worker" );
                e.Result = userState.Result;
                e.Cancel = true;
                return;
            }

            payload.MovingWindow.ClearKnownState();
            userState.MovingAverage = this.MovingWindow;
            userState.EstimatedCountout = payload.EstimatedCountout;
            do
            {
                if ( payload.MovingWindow.ReadValue( () => payload.Device.MultimeterSubsystem.MeasureReadingAmounts() ) )
                {
                    w.ReportProgress( userState.PercentProgress, userState );
                }
                else
                {
                    userState.Result.RegisterFailure( "device returned a null value" );
                }

                e.Result = userState.Result;
                e.Cancel = userState.Result.Failed;
                if ( e is object && !e.Cancel )
                {
                    int eventCount = payload.DoEventCount;
                    while ( eventCount > 0 )
                    {
                        Core.ApplianceBase.DoEvents();
                        eventCount -= 1;
                    }
                }
            }
            while ( !w.CancellationPending && !e.Cancel && !payload.MovingWindow.IsStopStatus() );
            while ( !e.Cancel && !payload.MovingWindow.IsStopStatus() )
            {
                int eventCount = payload.DoEventCount;
                while ( eventCount > 0 )
                {
                    Core.ApplianceBase.DoEvents();
                    eventCount -= 1;
                }
            }
        }

        /// <summary> Handles run worker completed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnWorkerRunWorkerCompleted( RunWorkerCompletedEventArgs e )
        {
            // TO_DO: Add to ViewModel. Me.ApplyCapturedSyncContext()
            if ( e is null )
                return;
            if ( e.Result is not TaskResult result )
                return;
            if ( e.Cancelled && !result.Failed )
            {
                result.RegisterFailure( "Worker canceled" );
            }

            if ( e.Error is object && result.Exception is null )
            {
                result.RegisterFailure( e.Error, "Worker exception" );
            }

            this.ProcessCompletion( result );
        }

        /// <summary> Worker run worker completed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Run worker completed event information. </param>
        private void Worker_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            this.OnWorkerRunWorkerCompleted( e );
        }

        /// <summary> Worker progress changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Progress changed event information. </param>
        private void Worker_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            // TO_DO: Add to view model Me.ApplyCapturedSyncContext()

            UserState us = e.UserState as UserState;
            var ma = new Core.MovingFilters.MovingWindow( us.MovingAverage as Core.MovingFilters.MovingWindow );
            this.ReportProgressChanged( ma, us.PercentProgress, us.Result );
        }

        /// <summary> Stops measure asynchronous if. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool StopMeasureAsyncIf( TimeSpan timeout )
        {
            bool stopped = this.Worker is null || !this.Worker.IsBusy;
            if ( !stopped )
            {
                // wait for previous operation to complete.
                var sw = Stopwatch.StartNew();
                _ = this.PublishInfo( "Waiting for worker to complete previous task" );
                while ( !this.IsDisposed && this.Worker.IsBusy && sw.Elapsed <= timeout )
                    Core.ApplianceBase.DoEvents();
                if ( this.Worker.IsBusy )
                {
                    this.Worker.CancelAsync();
                    _ = this.PublishInfo( "Waiting for worker to cancel previous task" );
                    sw.Restart();
                    while ( !this.IsDisposed && this.Worker.IsBusy && sw.Elapsed <= timeout )
                        Core.ApplianceBase.DoEvents();
                }

                stopped = !this.Worker.IsBusy;
                if ( !stopped )
                {
                    _ = this.PublishWarning( "Failed when waiting for worker to complete previous task" );
                    return false;
                }
            }

            return stopped;
        }

        /// <summary> Starts measure asynchronous. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool StartMeasureWork()
        {
            this.MovingWindowTaskResult = new TaskResult();
            bool stopped = this.StopMeasureAsyncIf( TimeSpan.FromSeconds( 1d ) );
            if ( !stopped )
                return false;
            this.InitializeWorker();
            var payload = new WorkerPayLoad() {
                Device = Device,
                MovingWindow = MovingWindow
            };
            payload.ClearKnownState();
            payload.InitializeKnownState( this.MeasurementRate );
            if ( !(this.IsDisposed || this.Worker.IsBusy) )
            {
                this.Worker.RunWorkerAsync( payload );
                this.NotifyTaskStart();
            }
            // Me.MeasurementStarted = True
            else
            {
                this.ClearTaskStart();
                // Me.MeasurementStarted = False
            }

            return this.TaskStart != NotificationSemaphores.None; // Me.MeasurementStarted
        }

        /// <summary> Starts a measure. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        public void StartMeasureWorkSync()
        {
            if ( this.StartMeasureWork() )
            {
                // wait for worker to get busy.
                while ( !(this.IsDisposed || this.Worker.IsBusy) )
                    Core.ApplianceBase.DoEvents();
                // wait till worker is done
                while ( !this.IsDisposed && this.Worker.IsBusy )
                    Core.ApplianceBase.DoEvents();
            }
        }
    }
}
