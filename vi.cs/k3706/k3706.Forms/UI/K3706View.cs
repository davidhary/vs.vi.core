using System.ComponentModel;
using System.Diagnostics;

namespace isr.VI.Tsp.K3706.Forms
{

    /// <summary> Keithley 3700 Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "K3706 User Interface" )]
    [Description( "Keithley 3700 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( K3706View ) )]
    public class K3706View : Facade.VisaView
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K3706View() : base()
        {
            this.InitializingComponents = true;
            int index = 0;
            index += 1;
            this.ReadingView = ReadingView.Create();
            this.AddView( new Facade.VisaViewControl( this.ReadingView, index, "_ReadTabPage", "Read" ) );
            index += 1;
            this.MultimeterView = MultimeterView.Create();
            this.AddView( new Facade.VisaViewControl( this.MultimeterView, index, "_MultimeterTabPage", "Meter" ) );
            index += 1;
            this.ChannelView = ChannelView.Create();
            this.AddView( new Facade.VisaViewControl( this.ChannelView, index, "_ChannelTabPage", "Channel" ) );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public K3706View( K3706Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the K3706 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    this.AssignDeviceThis( null );
                    if ( this.ChannelView is object )
                    {
                        this.ChannelView.Dispose();
                        this.ChannelView = null;
                    }

                    if ( this.MultimeterView is object )
                    {
                        this.MultimeterView.Dispose();
                        this.MultimeterView = null;
                    }

                    if ( this.ReadingView is object )
                    {
                        this.ReadingView.Dispose();
                        this.ReadingView = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE " 

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3706Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( K3706Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = K3706.My.MySettings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( K3706Device value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.ReadingView.AssignDevice( null );
                this.MultimeterView.AssignDevice( null );
                this.ChannelView.AssignDevice( null );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            this.DisplayView.BindMeasureToolStrip( this.Device.MultimeterSubsystem );
            this.DisplayView.BindSubsystemToolStrip( this.Device.ChannelSubsystem );
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.ReadingView.AssignDevice( this.Device );
            this.MultimeterView.AssignDevice( this.Device );
            this.ChannelView.AssignDevice( this.Device );
            this.DisplayView.BindMeasureToolStrip( this.Device.MultimeterSubsystem );
            this.DisplayView.BindSubsystemToolStrip( this.Device.ChannelSubsystem );
        }

        #endregion

        #endregion

        #region  VIEWS  

        /// <summary> Gets or sets the channel view. </summary>
        /// <value> The channel view. </value>
        private ChannelView ChannelView { get; set; }

        /// <summary> Gets or sets the reading view. </summary>
        /// <value> The reading view. </value>
        private ReadingView ReadingView { get; set; }

        /// <summary> Gets or sets the multimeter view. </summary>
        /// <value> The multimeter view. </value>
        private MultimeterView MultimeterView { get; set; }

        /// <summary> Gets or sets the resistances meter view. </summary>
        /// <value> The resistances meter view. </value>
        private ResistancesMeterView ResistancesMeterView { get; set; }

        /// <summary> Gets or sets the with resistances meters view. </summary>
        /// <value> The with resistances meters view. </value>
        public bool WithResistancesMetersView
        {
            get => this.ResistancesMeterView is object;

            set {
                if ( value != this.WithResistancesMetersView )
                {
                    // TO_DO: Resistance meter device must not inherit the device but rather use it.
                    this.ResistancesMeterView = new ResistancesMeterView();
                    this.AddView( new Facade.VisaViewControl( this.ResistancesMeterView, this.ViewsCount - 1, "_ResistancesTabPage", "Rs" ) );
                }
            }
        }

        #endregion

    }
}
