﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K3706.Forms
{
    [DesignerGenerated()]
    public partial class MovingWindowMeter
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(MovingWindowMeter));
            _TopLayout = new System.Windows.Forms.TableLayoutPanel();
            _RightLayout = new System.Windows.Forms.TableLayoutPanel();
            _MaximumLabel = new System.Windows.Forms.Label();
            _MinimumLabel = new System.Windows.Forms.Label();
            _ReadingLabel = new System.Windows.Forms.Label();
            _StatusLabel = new System.Windows.Forms.Label();
            _MiddleLayout = new System.Windows.Forms.TableLayoutPanel();
            _TaskProgressBar = new System.Windows.Forms.ProgressBar();
            _AverageLabel = new System.Windows.Forms.Label();
            _LeftLayout = new System.Windows.Forms.TableLayoutPanel();
            _ElapsedTimeLabel = new System.Windows.Forms.Label();
            _CountLabel = new System.Windows.Forms.Label();
            _ReadingsCountLabel = new System.Windows.Forms.Label();
            _ReadingTimeSpanLabel = new System.Windows.Forms.Label();
            _SettingsToolStrip = new System.Windows.Forms.ToolStrip();
            _LengthTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _LengthTextBox = new System.Windows.Forms.ToolStripTextBox();
            _WindowTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _WindowTextBox = new System.Windows.Forms.ToolStripTextBox();
            _TimeoutTextBoxLabel = new System.Windows.Forms.ToolStripLabel();
            _TimeoutTextBox = new System.Windows.Forms.ToolStripTextBox();
            __StartMovingWindowButton = new System.Windows.Forms.ToolStripButton();
            __StartMovingWindowButton.CheckStateChanged += new EventHandler(StartMovingWindowButton_CheckStateChanged);
            _TopLayout.SuspendLayout();
            _RightLayout.SuspendLayout();
            _MiddleLayout.SuspendLayout();
            _LeftLayout.SuspendLayout();
            _SettingsToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _TopLayout
            // 
            _TopLayout.BackColor = System.Drawing.Color.Transparent;
            _TopLayout.ColumnCount = 3;
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84.0f));
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TopLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84.0f));
            _TopLayout.Controls.Add(_RightLayout, 2, 0);
            _TopLayout.Controls.Add(_MiddleLayout, 1, 0);
            _TopLayout.Controls.Add(_LeftLayout, 0, 0);
            _TopLayout.Dock = System.Windows.Forms.DockStyle.Top;
            _TopLayout.Location = new System.Drawing.Point(1, 1);
            _TopLayout.Name = "_TopLayout";
            _TopLayout.RowCount = 1;
            _TopLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _TopLayout.Size = new System.Drawing.Size(368, 100);
            _TopLayout.TabIndex = 0;
            // 
            // _RightLayout
            // 
            _RightLayout.ColumnCount = 1;
            _RightLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _RightLayout.Controls.Add(_MaximumLabel, 0, 0);
            _RightLayout.Controls.Add(_MinimumLabel, 0, 2);
            _RightLayout.Controls.Add(_ReadingLabel, 0, 1);
            _RightLayout.Controls.Add(_StatusLabel, 0, 3);
            _RightLayout.Location = new System.Drawing.Point(284, 0);
            _RightLayout.Margin = new System.Windows.Forms.Padding(0);
            _RightLayout.Name = "_RightLayout";
            _RightLayout.RowCount = 4;
            _RightLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _RightLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _RightLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _RightLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _RightLayout.Size = new System.Drawing.Size(84, 98);
            _RightLayout.TabIndex = 2;
            // 
            // _MaximumLabel
            // 
            _MaximumLabel.AutoSize = true;
            _MaximumLabel.BackColor = System.Drawing.Color.Black;
            _MaximumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _MaximumLabel.ForeColor = System.Drawing.Color.LightGreen;
            _MaximumLabel.Location = new System.Drawing.Point(3, 0);
            _MaximumLabel.Name = "_MaximumLabel";
            _MaximumLabel.Size = new System.Drawing.Size(78, 24);
            _MaximumLabel.TabIndex = 0;
            _MaximumLabel.Text = "maximum";
            _MaximumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _MinimumLabel
            // 
            _MinimumLabel.AutoSize = true;
            _MinimumLabel.BackColor = System.Drawing.Color.Black;
            _MinimumLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _MinimumLabel.ForeColor = System.Drawing.Color.LightGreen;
            _MinimumLabel.Location = new System.Drawing.Point(3, 48);
            _MinimumLabel.Name = "_MinimumLabel";
            _MinimumLabel.Size = new System.Drawing.Size(78, 24);
            _MinimumLabel.TabIndex = 0;
            _MinimumLabel.Text = "minimum";
            _MinimumLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _ReadingLabel
            // 
            _ReadingLabel.AutoSize = true;
            _ReadingLabel.BackColor = System.Drawing.Color.Black;
            _ReadingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReadingLabel.ForeColor = System.Drawing.Color.LightGreen;
            _ReadingLabel.Location = new System.Drawing.Point(3, 24);
            _ReadingLabel.Name = "_ReadingLabel";
            _ReadingLabel.Size = new System.Drawing.Size(78, 24);
            _ReadingLabel.TabIndex = 0;
            _ReadingLabel.Text = "reading";
            _ReadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _StatusLabel
            // 
            _StatusLabel.AutoSize = true;
            _StatusLabel.BackColor = System.Drawing.Color.Black;
            _StatusLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _StatusLabel.ForeColor = System.Drawing.Color.LightGreen;
            _StatusLabel.Location = new System.Drawing.Point(3, 72);
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Size = new System.Drawing.Size(78, 26);
            _StatusLabel.TabIndex = 0;
            _StatusLabel.Text = "status";
            _StatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _MiddleLayout
            // 
            _MiddleLayout.ColumnCount = 1;
            _MiddleLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _MiddleLayout.Controls.Add(_TaskProgressBar, 0, 1);
            _MiddleLayout.Controls.Add(_AverageLabel, 0, 0);
            _MiddleLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _MiddleLayout.Location = new System.Drawing.Point(84, 0);
            _MiddleLayout.Margin = new System.Windows.Forms.Padding(0);
            _MiddleLayout.Name = "_MiddleLayout";
            _MiddleLayout.RowCount = 2;
            _MiddleLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _MiddleLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _MiddleLayout.Size = new System.Drawing.Size(200, 100);
            _MiddleLayout.TabIndex = 3;
            // 
            // _TaskProgressBar
            // 
            _TaskProgressBar.Dock = System.Windows.Forms.DockStyle.Bottom;
            _TaskProgressBar.Location = new System.Drawing.Point(3, 74);
            _TaskProgressBar.Name = "_TaskProgressBar";
            _TaskProgressBar.Size = new System.Drawing.Size(194, 23);
            _TaskProgressBar.Step = 1;
            _TaskProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            _TaskProgressBar.TabIndex = 0;
            // 
            // _AverageLabel
            // 
            _AverageLabel.AutoSize = true;
            _AverageLabel.BackColor = System.Drawing.Color.Black;
            _AverageLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _AverageLabel.Font = new System.Drawing.Font("Segoe UI", 18.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _AverageLabel.ForeColor = System.Drawing.Color.Aquamarine;
            _AverageLabel.Location = new System.Drawing.Point(3, 0);
            _AverageLabel.Name = "_AverageLabel";
            _AverageLabel.Size = new System.Drawing.Size(194, 71);
            _AverageLabel.TabIndex = 1;
            _AverageLabel.Text = "average";
            _AverageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // _LeftLayout
            // 
            _LeftLayout.ColumnCount = 1;
            _LeftLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 84.0f));
            _LeftLayout.Controls.Add(_ElapsedTimeLabel, 0, 0);
            _LeftLayout.Controls.Add(_CountLabel, 0, 1);
            _LeftLayout.Controls.Add(_ReadingsCountLabel, 0, 2);
            _LeftLayout.Controls.Add(_ReadingTimeSpanLabel, 0, 3);
            _LeftLayout.Location = new System.Drawing.Point(0, 0);
            _LeftLayout.Margin = new System.Windows.Forms.Padding(0);
            _LeftLayout.Name = "_LeftLayout";
            _LeftLayout.RowCount = 4;
            _LeftLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _LeftLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _LeftLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _LeftLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.0f));
            _LeftLayout.Size = new System.Drawing.Size(84, 98);
            _LeftLayout.TabIndex = 1;
            // 
            // _ElapsedTimeLabel
            // 
            _ElapsedTimeLabel.AutoSize = true;
            _ElapsedTimeLabel.BackColor = System.Drawing.Color.Black;
            _ElapsedTimeLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ElapsedTimeLabel.ForeColor = System.Drawing.Color.LightGreen;
            _ElapsedTimeLabel.Location = new System.Drawing.Point(3, 0);
            _ElapsedTimeLabel.Name = "_ElapsedTimeLabel";
            _ElapsedTimeLabel.Size = new System.Drawing.Size(78, 24);
            _ElapsedTimeLabel.TabIndex = 0;
            _ElapsedTimeLabel.Text = "elapsed";
            _ElapsedTimeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _CountLabel
            // 
            _CountLabel.AutoSize = true;
            _CountLabel.BackColor = System.Drawing.Color.Black;
            _CountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _CountLabel.ForeColor = System.Drawing.Color.LightGreen;
            _CountLabel.Location = new System.Drawing.Point(3, 24);
            _CountLabel.Name = "_CountLabel";
            _CountLabel.Size = new System.Drawing.Size(78, 24);
            _CountLabel.TabIndex = 0;
            _CountLabel.Text = "count";
            _CountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _ReadingsCountLabel
            // 
            _ReadingsCountLabel.AutoSize = true;
            _ReadingsCountLabel.BackColor = System.Drawing.Color.Black;
            _ReadingsCountLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReadingsCountLabel.ForeColor = System.Drawing.Color.LightGreen;
            _ReadingsCountLabel.Location = new System.Drawing.Point(3, 48);
            _ReadingsCountLabel.Name = "_ReadingsCountLabel";
            _ReadingsCountLabel.Size = new System.Drawing.Size(78, 24);
            _ReadingsCountLabel.TabIndex = 0;
            _ReadingsCountLabel.Text = "readings";
            _ReadingsCountLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _ReadingTimeSpanLabel
            // 
            _ReadingTimeSpanLabel.AutoSize = true;
            _ReadingTimeSpanLabel.BackColor = System.Drawing.Color.Black;
            _ReadingTimeSpanLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReadingTimeSpanLabel.ForeColor = System.Drawing.Color.LightGreen;
            _ReadingTimeSpanLabel.Location = new System.Drawing.Point(3, 72);
            _ReadingTimeSpanLabel.Name = "_ReadingTimeSpanLabel";
            _ReadingTimeSpanLabel.Size = new System.Drawing.Size(78, 26);
            _ReadingTimeSpanLabel.TabIndex = 0;
            _ReadingTimeSpanLabel.Text = "read time";
            _ReadingTimeSpanLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // _SettingsToolStrip
            // 
            _SettingsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _LengthTextBoxLabel, _LengthTextBox, _WindowTextBoxLabel, _WindowTextBox, _TimeoutTextBoxLabel, _TimeoutTextBox, __StartMovingWindowButton });
            _SettingsToolStrip.Location = new System.Drawing.Point(1, 101);
            _SettingsToolStrip.Name = "_SettingsToolStrip";
            _SettingsToolStrip.Size = new System.Drawing.Size(368, 25);
            _SettingsToolStrip.TabIndex = 1;
            _SettingsToolStrip.Text = "ToolStrip1";
            // 
            // _LengthTextBoxLabel
            // 
            _LengthTextBoxLabel.Name = "_LengthTextBoxLabel";
            _LengthTextBoxLabel.Size = new System.Drawing.Size(47, 22);
            _LengthTextBoxLabel.Text = "Length:";
            // 
            // _LengthTextBox
            // 
            _LengthTextBox.Margin = new System.Windows.Forms.Padding(1, 0, 4, 0);
            _LengthTextBox.Name = "_LengthTextBox";
            _LengthTextBox.Size = new System.Drawing.Size(25, 25);
            _LengthTextBox.Text = "40";
            _LengthTextBox.ToolTipText = "Moving average filter length";
            // 
            // _WindowTextBoxLabel
            // 
            _WindowTextBoxLabel.Name = "_WindowTextBoxLabel";
            _WindowTextBoxLabel.Size = new System.Drawing.Size(75, 22);
            _WindowTextBoxLabel.Text = "Window [%]:";
            // 
            // _WindowTextBox
            // 
            _WindowTextBox.Margin = new System.Windows.Forms.Padding(1, 0, 4, 0);
            _WindowTextBox.Name = "_WindowTextBox";
            _WindowTextBox.Size = new System.Drawing.Size(34, 25);
            _WindowTextBox.Text = "0.05";
            _WindowTextBox.ToolTipText = "Moving average window in %";
            // 
            // _TimeoutTextBoxLabel
            // 
            _TimeoutTextBoxLabel.Name = "_TimeoutTextBoxLabel";
            _TimeoutTextBoxLabel.Size = new System.Drawing.Size(70, 22);
            _TimeoutTextBoxLabel.Text = "Timeout [s]:";
            // 
            // _TimeoutTextBox
            // 
            _TimeoutTextBox.Margin = new System.Windows.Forms.Padding(1, 0, 4, 0);
            _TimeoutTextBox.Name = "_TimeoutTextBox";
            _TimeoutTextBox.Size = new System.Drawing.Size(34, 25);
            _TimeoutTextBox.Text = "120";
            _TimeoutTextBox.ToolTipText = "Timeout in seconds";
            // 
            // _StartMovingWindowButton
            // 
            __StartMovingWindowButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            __StartMovingWindowButton.CheckOnClick = true;
            __StartMovingWindowButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __StartMovingWindowButton.Image = (System.Drawing.Image)resources.GetObject("_StartMovingWindowButton.Image");
            __StartMovingWindowButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __StartMovingWindowButton.Name = "__StartMovingWindowButton";
            __StartMovingWindowButton.Size = new System.Drawing.Size(35, 22);
            __StartMovingWindowButton.Text = "Start";
            __StartMovingWindowButton.ToolTipText = "Start the moving average";
            // 
            // MovingWindowMeter
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_SettingsToolStrip);
            Controls.Add(_TopLayout);
            Name = "MovingWindowMeter";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(370, 127);
            _TopLayout.ResumeLayout(false);
            _RightLayout.ResumeLayout(false);
            _RightLayout.PerformLayout();
            _MiddleLayout.ResumeLayout(false);
            _MiddleLayout.PerformLayout();
            _LeftLayout.ResumeLayout(false);
            _LeftLayout.PerformLayout();
            _SettingsToolStrip.ResumeLayout(false);
            _SettingsToolStrip.PerformLayout();
            Load += new EventHandler(MovingWindowMeter_Load);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TableLayoutPanel _TopLayout;
        private System.Windows.Forms.TableLayoutPanel _MiddleLayout;
        private System.Windows.Forms.ProgressBar _TaskProgressBar;
        private System.Windows.Forms.Label _AverageLabel;
        private System.Windows.Forms.TableLayoutPanel _LeftLayout;
        private System.Windows.Forms.Label _ElapsedTimeLabel;
        private System.Windows.Forms.Label _CountLabel;
        private System.Windows.Forms.Label _ReadingsCountLabel;
        private System.Windows.Forms.Label _ReadingTimeSpanLabel;
        private System.Windows.Forms.TableLayoutPanel _RightLayout;
        private System.Windows.Forms.Label _MaximumLabel;
        private System.Windows.Forms.Label _MinimumLabel;
        private System.Windows.Forms.Label _ReadingLabel;
        private System.Windows.Forms.Label _StatusLabel;
        private System.Windows.Forms.ToolStrip _SettingsToolStrip;
        private System.Windows.Forms.ToolStripLabel _LengthTextBoxLabel;
        private System.Windows.Forms.ToolStripTextBox _LengthTextBox;
        private System.Windows.Forms.ToolStripLabel _WindowTextBoxLabel;
        private System.Windows.Forms.ToolStripTextBox _WindowTextBox;
        private System.Windows.Forms.ToolStripLabel _TimeoutTextBoxLabel;
        private System.Windows.Forms.ToolStripTextBox _TimeoutTextBox;
        private System.Windows.Forms.ToolStripButton __StartMovingWindowButton;

        private System.Windows.Forms.ToolStripButton _StartMovingWindowButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StartMovingWindowButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StartMovingWindowButton != null)
                {
                    __StartMovingWindowButton.CheckStateChanged -= StartMovingWindowButton_CheckStateChanged;
                }

                __StartMovingWindowButton = value;
                if (__StartMovingWindowButton != null)
                {
                    __StartMovingWindowButton.CheckStateChanged += StartMovingWindowButton_CheckStateChanged;
                }
            }
        }
    }
}