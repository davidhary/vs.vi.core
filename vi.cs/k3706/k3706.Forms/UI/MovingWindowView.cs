﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core;
using isr.Core.Forma;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K3706.Forms
{

    /// <summary> The 3700 and moving meter controls. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2005-02-07, 2.0.2597.x. </para>
    /// </remarks>
    public partial class MovingWindowView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MovingWindowView() : base()
        {
            this.InitializeComponent();
            this.AddPrivateListeners();
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    // removes the private text box listener
                    this._InstrumentPanel.RemovePrivateListeners();
                    this._MovingWindowMeter.RemovePrivateListeners();
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region  FORM EVENT HANDLERS 

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"loading the control";
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );

                // set the form caption
                this.Text = $"{My.MyProject.Application.Info.ProductName} release {My.MyProject.Application.Info.Version}";
                if ( !this.DesignMode )
                {
                    activity = $"assigning the device";
                    this._MovingWindowMeter.Device = this._InstrumentPanel.Device;
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
                if ( MyDialogResult.Abort == WindowsForms.ShowDialogAbortIgnore( ex ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                base.OnLoad( e );
                Trace.CorrelationManager.StopLogicalOperation();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary> Adds the listeners such as the current trace messages box. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected void AddPrivateListeners()
        {
            this._InstrumentPanel.AddPrivateListeners( this.Talker );
            this._MovingWindowMeter.AddPrivateListeners( this.Talker );
        }

        /// <summary> Applies the trace level to all listeners to the specified type. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="listenerType"> Type of the listener. </param>
        /// <param name="value">        The value. </param>
        public override void ApplyListenerTraceLevel( ListenerType listenerType, TraceEventType value )
        {
            this._InstrumentPanel.ApplyListenerTraceLevel( listenerType, value );
            this._MovingWindowMeter.ApplyListenerTraceLevel( listenerType, value );
            // this should apply only to the listeners associated with this form
            // MyBase.ApplyListenerTraceLevel(listenerType, value)
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}