using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

using isr.Core.Forma;
using isr.Core.WinForms.ControlExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K3706.Forms
{

    /// <summary> A moving window averaging meter. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-01-30 </para>
    /// </remarks>
    public partial class MovingWindowMeter : ModelViewTalkerBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MovingWindowMeter() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.MeasurementFormatString = "G8";
            this.MovingWindow = new Core.MovingFilters.MovingWindow( _DefaultMovingWindowLength );
            this.ExpectedStopTimeout = TimeSpan.FromMilliseconds( 100d );

            // set very long in case operator starts browsing the application
            this.TaskCompleteNotificationTimeout = TimeSpan.FromSeconds( 10d );

            // set very long in case operator starts browsing the application
            this.TaskStartNotificationTimeout = TimeSpan.FromSeconds( 10d );
            this.TaskStopTimeout = TimeSpan.FromSeconds( 1d );
            this.__StartMovingWindowButton.Name = "_StartMovingWindowButton";
        }

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> The default moving window length. </summary>
        private const int _DefaultMovingWindowLength = 40;

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        /// <param name="disposing">    <c>True</c> to release both managed and unmanaged resources;
        ///                             <c>False</c> to release only unmanaged
        ///                             resources when called from the runtime
        ///                             finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                if ( disposing )
                {
                    this.Worker?.Dispose();
                    this.Worker = null;

                    this.MovingWindow?.ClearKnownState();
                    this.MovingWindow = null;

                    this.Task?.Dispose();
                    this.Task = null;

                    // release the device.
                    this.DeviceInternal = null;

                    this.components?.Dispose();
                    this.components = null;
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region  FORM EVENTS  

        /// <summary> Moving window meter load. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MovingWindowMeter_Load( object sender, EventArgs e )
        {
            this._StartMovingWindowButton.Enabled = false;
        }

        #endregion

        #region " MOVING WINDOW AVERAGE "

        /// <summary> Device open changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Device_OpenChanged( object sender, EventArgs e )
        {
            this._StartMovingWindowButton.Enabled = this.DeviceInternal.IsDeviceOpen;
        }

        private K3706Device _DeviceInternal;

        private K3706Device DeviceInternal
        {
            [MethodImpl( MethodImplOptions.Synchronized )]
            get => this._DeviceInternal;

            [MethodImpl( MethodImplOptions.Synchronized )]
            set {
                if ( this._DeviceInternal != null )
                {
                    this._DeviceInternal.Opened -= this.Device_OpenChanged;
                    this._DeviceInternal.Closed -= this.Device_OpenChanged;
                }

                this._DeviceInternal = value;
                if ( this._DeviceInternal != null )
                {
                    this._DeviceInternal.Opened += this.Device_OpenChanged;
                    this._DeviceInternal.Closed += this.Device_OpenChanged;
                }
            }
        }

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3706Device Device
        {
            get => this.DeviceInternal;

            set {
                this.DeviceInternal = value;
                if ( value is object )
                {
                    this._StartMovingWindowButton.Enabled = value.IsDeviceOpen;
                }
            }
        }

        /// <summary> Gets or sets the moving window. </summary>
        /// <value> The moving window. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Core.MovingFilters.MovingWindow MovingWindow { get; private set; }

        /// <summary> Gets or sets the length. </summary>
        /// <value> The length. </value>
        public int Length
        {
            get => this.MovingWindow.Length;

            set {
                if ( this.Length != value )
                {
                    this.MovingWindow.Length = value;
                    this.MovingWindow.ClearKnownState();
                }

                this._LengthTextBox.Text = value.ToString();
            }
        }

        /// <summary> Gets or sets the timeout interval. </summary>
        /// <value> The timeout interval. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan TimeoutInterval
        {
            get => this.MovingWindow.TimeoutInterval;

            set {
                this.MovingWindow.TimeoutInterval = value;
                this._TimeoutTextBox.Text = value.TotalSeconds.ToString();
            }
        }

        /// <summary> Gets or sets the update rule. </summary>
        /// <value> The update rule. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Core.MovingFilters.MovingWindowUpdateRule UpdateRule
        {
            get => this.MovingWindow.UpdateRule;

            set => this.MovingWindow.UpdateRule = value;
        }

        /// <summary> Gets or sets the window. </summary>
        /// <value> The window. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Core.MovingFilters.FilterWindow<double> Window
        {
            get => this.MovingWindow.RelativeWindow;

            set {
                if ( value != this.Window )
                {
                    this.MovingWindow.RelativeWindow = new Core.MovingFilters.FilterWindow<double>( value );
                    this._WindowTextBox.Text = $"{100d * (value.Max - value.Min):0.####}";
                }
            }
        }

        /// <summary> The reading timespan. </summary>
        private TimeSpan _ReadingTimespan;

        /// <summary> Gets or sets the reading timespan. </summary>
        /// <value> The reading timespan. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan ReadingTimespan
        {
            get => this._ReadingTimespan;

            set {
                if ( value != this.ReadingTimespan )
                {
                    this._ReadingTimespan = value;
                    this.UpdateReadingTimespanCaption( value );
                }
            }
        }

        /// <summary> Updates the reading timespan caption described by value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void UpdateReadingTimespanCaption( TimeSpan value )
        {
            string caption = $"{value.TotalMilliseconds:0}";
            if ( !string.Equals( caption, this._ReadingTimeSpanLabel.Text ) )
                this._ReadingTimeSpanLabel.Text = caption;
        }

        /// <summary> Gets the has moving window task result. </summary>
        /// <value> The has moving window task result. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool HasMovingWindowTaskResult => this.MovingWindowTaskResult is object;

        /// <summary> Gets or sets the measurement Format String. </summary>
        /// <value> The measurement FormatString. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string MeasurementFormatString { get; protected set; }

        /// <summary> Gets the moving window measurement failed. </summary>
        /// <value> The moving window measurement failed. </value>
        public bool MovingWindowMeasurementFailed => this.MovingWindow.Status != Core.MovingFilters.MovingWindowStatus.Within;

        #endregion

        #region  MOVING WINDOW TASK 

        #region  TASK FAILURE INFO 

        /// <summary> Gets the sentinel indicating if the moving window average task failed. </summary>
        /// <value> The sentinel indicating if the moving window task failed. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool MovingWindowTaskFailed => this.HasMovingWindowTaskResult && this.MovingWindowTaskResult.Failed;

        /// <summary> Gets the task failure details. </summary>
        /// <value> The failure details. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public string MovingWindowTaskFailureDetails => this.MovingWindowTaskFailed ? this.MovingWindowTaskResult.Exception is object ? this.MovingWindowTaskResult.Exception.ToFullBlownString() : this.MovingWindowTaskResult.Details : string.Empty;

        /// <summary> Gets the failure exception. </summary>
        /// <value> The failure exception. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Exception MovingWindowTaskFailureException => this.MovingWindowTaskResult.Exception;

        #endregion

        #region  TASK COMPLETE 

        /// <summary> Gets or sets the task complete notification timeout. </summary>
        /// <remarks>
        /// Defaults to 10 seconds to allow for user interface browsing while collecting data.
        /// </remarks>
        /// <value> The task complete notification timeout. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan TaskCompleteNotificationTimeout { get; set; }

        /// <summary> Clears the task complete semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void ClearTaskComplete()
        {
            this.TaskComplete = NotificationSemaphores.None;
            // TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3706.MovingWindowMeter.TaskComplete))
        }

        /// <summary> Set the task complete semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void NotifyTaskComplete()
        {
            this.TaskComplete = NotificationSemaphores.Sent;
            // TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3706.MovingWindowMeter.TaskComplete))
        }

        /// <summary> Set the task complete semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool NotifyTaskComplete( TimeSpan timeout )
        {
            var sw = Stopwatch.StartNew();
            this.NotifyTaskComplete();
            do
                Core.ApplianceBase.DoEvents();
            while ( (this.TaskComplete & NotificationSemaphores.Acknowledged) == 0 && sw.Elapsed <= timeout );
            return (this.TaskComplete & NotificationSemaphores.Acknowledged) != 0;
        }

        /// <summary> Acknowledge task complete semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void AcknowledgeTaskComplete()
        {
            this.TaskComplete |= NotificationSemaphores.Acknowledged;
        }

        /// <summary> Gets or sets the task complete semaphore. </summary>
        /// <value> The measurement Completed. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public NotificationSemaphores TaskComplete { get; private set; }

        #endregion

        #region  TASK START 

        /// <summary> Gets or sets the task start notification timeout. </summary>
        /// <remarks>
        /// Defaults to 10 seconds to allow for user interface browsing while collecting data.
        /// </remarks>
        /// <value> The task start notification timeout. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan TaskStartNotificationTimeout { get; set; }

        /// <summary> Clears the task Start semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ClearTaskStart()
        {
            this.TaskStart = NotificationSemaphores.None;
            // TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3706.MovingWindowMeter.TaskStart))
        }

        /// <summary> Set the task Start semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void NotifyTaskStart()
        {
            this.TaskStart = NotificationSemaphores.Sent;
            // TO_DO: Move to Moving WIndow Meter Model     Me.NotifyPropertyChanged(NameOf(K3706.MovingWindowMeter.TaskStart))
        }

        /// <summary> Set the task Start semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        private bool NotifyTaskStart( TimeSpan timeout )
        {
            var sw = Stopwatch.StartNew();
            this.NotifyTaskStart();
            do
                Core.ApplianceBase.DoEvents();
            while ( (this.TaskStart & NotificationSemaphores.Acknowledged) == 0 && sw.Elapsed <= timeout );
            return (this.TaskStart & NotificationSemaphores.Acknowledged) != 0;
        }

        /// <summary> Acknowledge task Start semaphore. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void AcknowledgeTaskStart()
        {
            this.TaskStart |= NotificationSemaphores.Acknowledged;
        }

        /// <summary> Gets or sets the task Start semaphore. </summary>
        /// <value> The measurement Started. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public NotificationSemaphores TaskStart { get; private set; }

        #endregion

        #endregion

        #region  PROGRESS REPORTING 

        /// <summary> Number of. </summary>
        private int _Count;

        /// <summary> Gets or sets the number of. </summary>
        /// <value> The count. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int Count
        {
            get => this._Count;

            protected set {
                if ( this.Count != value )
                {
                    this._Count = value;
                    this._CountLabel.Text = value.ToString();
                }
            }
        }

        /// <summary> Number of readings. </summary>
        private int _ReadingsCount;

        /// <summary> Gets or sets the number of readings. </summary>
        /// <value> The number of readings. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int ReadingsCount
        {
            get => this._ReadingsCount;

            protected set {
                if ( this.ReadingsCount != value )
                {
                    this._ReadingsCount = value;
                    this._ReadingsCountLabel.Text = value.ToString();
                }
            }
        }

        /// <summary> The percent progress. </summary>
        private int _PercentProgress;

        /// <summary> Gets or sets the percent progress. </summary>
        /// <value> The percent progress. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public int PercentProgress
        {
            get => this._PercentProgress;

            protected set {
                if ( this.PercentProgress != value )
                {
                    this._PercentProgress = value;
                    _ = this._TaskProgressBar.SafelyInvoke( () => this._TaskProgressBar.Value = value );
                }
            }
        }

        /// <summary> The elapsed time. </summary>
        private TimeSpan _ElapsedTime;

        /// <summary> Gets or sets the elapsed time. </summary>
        /// <value> The elapsed time. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan ElapsedTime
        {
            get => this._ElapsedTime;

            protected set {
                if ( this.ElapsedTime != value )
                {
                    this._ElapsedTime = value;
                    this._ElapsedTimeLabel.Text = value.ToString( @"mm\:ss\.ff", System.Globalization.CultureInfo.CurrentCulture );
                }
            }
        }

        /// <summary> The maximum reading. </summary>
        private double _MaximumReading;

        /// <summary> Gets or sets the maximum reading. </summary>
        /// <value> The maximum reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double MaximumReading
        {
            get => this._MaximumReading;

            protected set {
                if ( this.MaximumReading != value )
                {
                    this._MaximumReading = value;
                    this._MaximumLabel.Text = value.ToString( this.MeasurementFormatString );
                }
            }
        }

        /// <summary> The minimum reading. </summary>
        private double _MinimumReading;

        /// <summary> Gets or sets the minimum reading. </summary>
        /// <value> The minimum reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double MinimumReading
        {
            get => this._MinimumReading;

            protected set {
                if ( this.MinimumReading != value )
                {
                    this._MinimumReading = value;
                    this._MinimumLabel.Text = value.ToString( this.MeasurementFormatString );
                }
            }
        }

        /// <summary> The reading. </summary>
        private double _Reading;

        /// <summary> Gets or sets the reading. </summary>
        /// <value> The reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double Reading
        {
            get => this._Reading;

            protected set {
                if ( value != this.Reading )
                {
                    this._Reading = value;
                    this._AverageLabel.Text = value.ToString( this.MeasurementFormatString );
                }
            }
        }

        /// <summary> The last reading. </summary>
        private double? _LastReading;

        /// <summary> Gets or sets the last reading. </summary>
        /// <value> The last reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public double? LastReading
        {
            get => this._LastReading;

            protected set {
                if ( !Nullable.Equals( value, this.LastReading ) )
                {
                    this._LastReading = value;
                    if ( value.HasValue )
                        this._ReadingLabel.Text = value.Value.ToString( this.MeasurementFormatString );
                }
            }
        }

        /// <summary> The reading status. </summary>
        private Core.MovingFilters.MovingWindowStatus _ReadingStatus;

        /// <summary> Gets or sets the reading status. </summary>
        /// <value> The reading status. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public Core.MovingFilters.MovingWindowStatus ReadingStatus
        {
            get => this._ReadingStatus;

            protected set {
                if ( this.ReadingStatus != value )
                {
                    this._ReadingStatus = value;
                    string v = Core.MovingFilters.MovingWindow.StatusAnnunciationCaption( value );
                    this._StatusLabel.Text = Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact( v, this._StatusLabel );
                    this.ToolTip.SetToolTip( this._StatusLabel, v );
                }
            }
        }

        /// <summary> Reports progress changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="movingWindow">    The moving window. </param>
        /// <param name="percentProgress"> The percent progress. </param>
        /// <param name="result">          The result. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReportProgressChanged( Core.MovingFilters.MovingWindow movingWindow, int percentProgress, TaskResult result )
        {
            if ( this.InvokeRequired )
            {
                _ = this.Invoke( new Action<Core.MovingFilters.MovingWindow, int, TaskResult>( this.ReportProgressChanged ), new object[] { movingWindow, percentProgress, result } );
            }
            else
            {
                var ma = movingWindow;
                if ( ma is object && ma.TotalReadingsCount > 0 )
                {
                    try
                    {
                        double value = 0d;
                        bool hasReading = Core.MovingFilters.MovingWindow.HasReading( ma.Status );
                        if ( hasReading )
                            value = ma.Mean;
                        this.Window = ma.RelativeWindow;
                        this.ReadingTimespan = ma.ReadingTimespan;
                        this.PercentProgress = percentProgress;
                        this.ElapsedTime = ma.ElapsedTime;
                        this.Count = ma.Count;
                        this.ReadingsCount = ma.TotalReadingsCount;
                        this.MaximumReading = ma.Maximum;
                        this.MinimumReading = ma.Minimum;
                        this.LastReading = ma.LastAmenableReading.Value;
                        this.ReadingStatus = ma.Status;
                        if ( hasReading )
                            this.Reading = value;
                        // this helps flash out exceptions:
                        Core.ApplianceBase.DoEvents();
                    }
                    catch ( Exception ex )
                    {
                        result.RegisterFailure( ex, "exception reporting progress" );
                    }
                }
            }
        }

        /// <summary> Reports progress changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="movingWindow"> The moving window. </param>
        private void ReportProgressChanged( Core.MovingFilters.MovingWindow movingWindow )
        {
            this.ReportProgressChanged( movingWindow, movingWindow.PercentProgress, this.MovingWindowTaskResult );
        }

        /// <summary> Process the completion described by result. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="result"> The result. </param>
        private void ProcessCompletion( TaskResult result )
        {
            if ( result is null )
            {
                this.MovingWindowTaskResult = new TaskResult();
                this.MovingWindowTaskResult.RegisterFailure( "Unexpected null task result when completing the task;. Contact the developer" );
            }
            else
            {
                this.MovingWindowTaskResult = result;
            }

            if ( !this.NotifyTaskComplete( this.TaskCompleteNotificationTimeout ) )
            {
                if ( this.MovingWindowTaskResult.Failed )
                {
                    this.MovingWindowTaskResult.RegisterFailure( $"{this.MovingWindowTaskResult.Details}; also, timeout receiving completion acknowledgment" );
                }
                else
                {
                    this.MovingWindowTaskResult.RegisterFailure( "Timeout receiving completion acknowledgment" );
                }
            }
        }

        #endregion

        #region  MEASURE 

        /// <summary> Gets the moving window task result. </summary>
        /// <value> The moving window task result. </value>
        private TaskResult MovingWindowTaskResult { get; set; } = new TaskResult();

        /// <summary> Measure moving window average. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="progress"> The progress reporter. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureMovingWindowAverage( IProgress<Core.MovingFilters.MovingWindow> progress )
        {
            this.MovingWindowTaskResult = new TaskResult();
            try
            {
                // TO_DO: Move to Moving WIndow Meter Model Me.ApplyCapturedSyncContext()
                this.MovingWindow.ClearKnownState();
                if ( this.NotifyTaskStart( this.TaskStartNotificationTimeout ) )
                {
                    do
                    {
                        // measure and time
                        if ( this.MovingWindow.ReadValue( () => this.Device.MultimeterSubsystem.MeasureReadingAmounts() ) )
                        {
                            progress.Report( new Core.MovingFilters.MovingWindow( this.MovingWindow ) );
                        }
                        else
                        {
                            this.MovingWindowTaskResult.RegisterFailure( "device returned a null value" );
                        }
                    }
                    while ( !this.IsCancellationRequested && !this.MovingWindowTaskResult.Failed && !this.MovingWindow.IsStopStatus() );
                }
                else
                {
                    this.MovingWindowTaskResult.RegisterFailure( "Timeout receiving start acknowledgment" );
                }
            }
            catch ( Exception ex )
            {
                this.MovingWindowTaskResult.RegisterFailure( ex );
            }
            finally
            {
                this.ProcessCompletion( this.MovingWindowTaskResult );
            }
        }

        #endregion

        #region  ASYNC TASK 

        /// <summary> The cancellation token source. </summary>
        /// <value> The cancellation token source. </value>
        private CancellationTokenSource CancellationTokenSource { get; set; }

        /// <summary> The cancellation token. </summary>
        /// <value> The cancellation token. </value>
        private CancellationToken CancellationToken { get; set; }

        /// <summary> Requests the state machine to immediately stop operation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public virtual void RequestCancellation()
        {
            if ( !this.IsCancellationRequested )
            {
                // stops the wait.
                this.CancellationTokenSource.Cancel();
                // TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3706.MovingWindowMeter.IsCancellationRequested))
            }
        }

        /// <summary> Query if cancellation requested. </summary>
        /// <value> <c>true</c> if cancellation requested; otherwise <c>false</c> </value>
        public bool IsCancellationRequested => this.CancellationToken.IsCancellationRequested;

        /// <summary> Gets the sentinel indicating if the meter is running. </summary>
        /// <value> The sentinel indicating if the meter is running. </value>
        public bool IsRunning => this.Task is object && this.Task.Status == TaskStatus.Running;

        /// <summary> Gets the is stopped. </summary>
        /// <value> The is stopped. </value>
        public bool IsStopped => this.Task is null || this.Task.Status != TaskStatus.Running;

        /// <summary> Gets the expected stop timeout. </summary>
        /// <value> The expected stop timeout. </value>
        public TimeSpan ExpectedStopTimeout { get; set; }

        /// <summary> Stops measure asynchronous if. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool StopAsyncTask()
        {
            return this.StopAsyncTask( this.ExpectedStopTimeout );
        }

        /// <summary> Stops measure asynchronous if. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool StopAsyncTask( TimeSpan timeout )
        {
            if ( !this.IsStopped && !this.IsCancellationRequested )
            {
                // wait for previous operation to complete.
                var sw = Stopwatch.StartNew();
                _ = this.PublishInfo( "Waiting for previous task to complete" );
                while ( !this.IsStopped && sw.Elapsed <= timeout )
                    Core.ApplianceBase.DoEvents();
                if ( !this.IsStopped && !this.IsCancellationRequested )
                {
                    _ = this.PublishInfo( "Requesting cancellation of previous tasks" );
                    this.RequestCancellation();
                    _ = this.PublishInfo( "Waiting for previous task to stop" );
                    sw.Restart();
                    while ( !this.IsStopped && sw.Elapsed <= timeout )
                        Core.ApplianceBase.DoEvents();
                }
            }

            if ( !this.IsStopped )
                _ = this.PublishWarning( "Attempt to stop previous task failed" );
            return this.IsStopped;
        }

        /// <summary> Gets the task. </summary>
        /// <value> The task. </value>
        protected Task Task { get; private set; }

        /// <summary> Gets or sets the task stopping timeout. </summary>
        /// <value> The task stopping timeout. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan TaskStopTimeout { get; set; }

        /// <summary> Activates the machine asynchronous task. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="runSynchronously"> True to run synchronously. </param>
        public void StartMeasureTask( bool runSynchronously )
        {
            // TO_DO: Move to Moving WIndow Meter Model Me.CaptureSyncContext(syncContext)
            _ = this.StopAsyncTask( this.TaskStopTimeout );
            // proceed even if not stopped
            this.TaskStart = NotificationSemaphores.None;
            this.CancellationTokenSource = new CancellationTokenSource();
            this.CancellationToken = this.CancellationTokenSource.Token;
            var progress = new Progress<Core.MovingFilters.MovingWindow>( this.ReportProgressChanged );
            this.Task = new Task( () => this.MeasureMovingWindowAverage( progress ) );
            if ( runSynchronously )
            {
                this.Task.RunSynchronously( TaskScheduler.FromCurrentSynchronizationContext() );
            }
            else
            {
                this.Task.Start();
            }
        }

        /// <summary> Starts measure asynchronous. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        public void StartMeasureAsync()
        {
            this.StartMeasureTask( false );
        }

        /// <summary> Activates the machine asynchronous task. </summary>
        /// <remarks>
        /// This was suspected to cause issues. The synchronization context is captured as part of the
        /// property change and other event handlers and is no longer needed here.
        /// </remarks>
        /// <param name="runSynchronously"> True to run synchronously. </param>
        public void StartMeasureTaskStopRequired( bool runSynchronously )
        {
            if ( this.StopAsyncTask( this.TaskStopTimeout ) )
            {
                this.TaskStart = NotificationSemaphores.None;
                this.CancellationTokenSource = new CancellationTokenSource();
                this.CancellationToken = this.CancellationTokenSource.Token;
                var progress = new Progress<Core.MovingFilters.MovingWindow>( this.ReportProgressChanged );
                this.Task = new Task( () => this.MeasureMovingWindowAverage( progress ) );
                if ( runSynchronously )
                {
                    this.Task.RunSynchronously( TaskScheduler.FromCurrentSynchronizationContext() );
                }
                else
                {
                    this.Task.Start();
                }
            }
            else
            {
                this.TaskStart = NotificationSemaphores.None;
                // Me.MeasurementStarted = False
            }
        }

        /// <summary> Starts measure task stop required asynchronous. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        public void StartMeasureTaskStopRequiredAsync()
        {
            this.StartMeasureTaskStopRequired( false );
        }

        /// <summary> Asynchronous task. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> An asynchronous result. </returns>
        public async Task AsyncTask()
        {
            var progress = new Progress<Core.MovingFilters.MovingWindow>( this.ReportProgressChanged );
            await Task.Run( () => this.MeasureMovingWindowAverage( progress ) );
        }

        #endregion

        #region  START STOP 

        /// <summary> Starts moving window button check state changed. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StartMovingWindowButton_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( sender is not ToolStripButton button ) return;
            string activity = string.Empty;
            try
            {
                if ( button.Checked )
                {
                    this.MovingWindow.Length = Conversions.ToInteger( this._LengthTextBox.Text );
                    double span = 0.01d * Conversions.ToDouble( this._WindowTextBox.Text );
                    this.MovingWindow.RelativeWindow = new Core.MovingFilters.FilterWindow<double>( -0.5d * span, 0.5d * span );
                    this.MovingWindow.UpdateRule = Core.MovingFilters.MovingWindowUpdateRule.StopOnWithinWindow;
                    this.MovingWindow.TimeoutInterval = TimeSpan.FromSeconds( Conversions.ToDouble( this._TimeoutTextBox.Text ) );
                    activity = $"{this.Device.ResourceNameCaption} starting the moving window task";
                    this.StartMeasureAsync();
                    if ( !this.IsRunning )
                    {
                        _ = this.PublishWarning( $"Failed {activity}" );
                    }
                }
                else
                {
                    activity = $"{this.Device.ResourceNameCaption} stopping the moving window task";
                    _ = this.StopAsyncTask( this.TaskStopTimeout );
                    if ( !this.IsStopped )
                    {
                        _ = this.PublishWarning( $"Failed {activity}" );
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                button.Text = $"{button.Checked.GetHashCode():'Stop';'Stop';'Start'}";
            }
        }

        #endregion

        #region  TASK RESULT 

        /// <summary> Encapsulates the result of a task. </summary>
        /// <remarks>
        /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2016-09-24 </para>
        /// </remarks>
        private class TaskResult
        {

            /// <summary> Registers the failure described by exception. </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="details"> The details. </param>
            public void RegisterFailure( string details )
            {
                this.Failed = true;
                this.Details = details;
            }

            /// <summary> Registers the failure described by exception. </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="exception"> The exception. </param>
            public void RegisterFailure( Exception exception )
            {
                this.RegisterFailure( exception, "Exception occurred" );
            }

            /// <summary> Registers the failure described by exception. </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="exception"> The exception. </param>
            /// <param name="details">   The details. </param>
            public void RegisterFailure( Exception exception, string details )
            {
                this.Failed = true;
                this.Details = details;
                this.Exception = exception;
            }

            /// <summary> Gets or sets the failed sentinel. </summary>
            /// <value> The failed sentinel. </value>
            public bool Failed { get; private set; }

            /// <summary> Gets or sets the failure details. </summary>
            /// <value> The details. </value>
            public string Details { get; private set; }

            /// <summary> Gets or sets the exception. </summary>
            /// <value> The exception. </value>
            public Exception Exception { get; private set; }
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }

    /// <summary> Values that represent notification semaphores. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags]
    public enum NotificationSemaphores
    {

        /// <summary> An enum constant representing the none option. </summary>
        [Description( "Not set" )]
        None = 0,

        /// <summary> An enum constant representing the sent option. </summary>
        [Description( "Notification Sent" )]
        Sent = 1,

        /// <summary> An enum constant representing the acknowledged option. </summary>
        [Description( "Notification Acknowledged" )]
        Acknowledged = 2
    }
}
