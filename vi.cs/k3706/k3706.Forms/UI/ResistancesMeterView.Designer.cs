﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K3706.Forms
{
    [DesignerGenerated()]
    public partial class ResistancesMeterView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ResistancesMeterView));
            _Layout = new TableLayoutPanel();
            _Panel = new Panel();
            _DataGrid = new DataGridView();
            _BottomToolStrip = new ToolStrip();
            _GageTitleLabel = new ToolStripLabel();
            _ReadingLabel = new ToolStripLabel();
            _ChannelLabel = new ToolStripLabel();
            _ComplianceLabel = new ToolStripLabel();
            __MeasureButton = new ToolStripButton();
            __MeasureButton.Click += new EventHandler(MeasureButton_Click);
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_DataGrid).BeginInit();
            _BottomToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 3.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 3.0f));
            _Layout.Size = new System.Drawing.Size(341, 360);
            _Layout.TabIndex = 1;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_DataGrid);
            _Panel.Controls.Add(_BottomToolStrip);
            _Panel.Dock = DockStyle.Fill;
            _Panel.Location = new System.Drawing.Point(6, 6);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(329, 348);
            _Panel.TabIndex = 2;
            // 
            // _DataGrid
            // 
            _DataGrid.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _DataGrid.Dock = DockStyle.Fill;
            _DataGrid.Location = new System.Drawing.Point(0, 0);
            _DataGrid.Name = "_DataGrid";
            _DataGrid.Size = new System.Drawing.Size(329, 323);
            _DataGrid.TabIndex = 8;
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = DockStyle.Bottom;
            _BottomToolStrip.GripMargin = new Padding(0);
            _BottomToolStrip.GripStyle = ToolStripGripStyle.Hidden;
            _BottomToolStrip.Items.AddRange(new ToolStripItem[] { _GageTitleLabel, _ReadingLabel, _ChannelLabel, _ComplianceLabel, __MeasureButton });
            _BottomToolStrip.Location = new System.Drawing.Point(0, 323);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(329, 25);
            _BottomToolStrip.TabIndex = 2;
            _BottomToolStrip.Text = "Tool Strip";
            // 
            // _GageTitleLabel
            // 
            _GageTitleLabel.Name = "_GageTitleLabel";
            _GageTitleLabel.Size = new System.Drawing.Size(20, 22);
            _GageTitleLabel.Text = "R1";
            _GageTitleLabel.ToolTipText = "Gage";
            // 
            // _ReadingLabel
            // 
            _ReadingLabel.Name = "_ReadingLabel";
            _ReadingLabel.Size = new System.Drawing.Size(70, 22);
            _ReadingLabel.Text = "0.0000 Ohm";
            _ReadingLabel.ToolTipText = "Measured value";
            // 
            // _ChannelLabel
            // 
            _ChannelLabel.Name = "_ChannelLabel";
            _ChannelLabel.Size = new System.Drawing.Size(112, 22);
            _ChannelLabel.Text = "1001;1031;1912;1921";
            _ChannelLabel.ToolTipText = "Channel List";
            // 
            // _ComplianceLabel
            // 
            _ComplianceLabel.Name = "_ComplianceLabel";
            _ComplianceLabel.Size = new System.Drawing.Size(21, 22);
            _ComplianceLabel.Text = ".C.";
            _ComplianceLabel.ToolTipText = "Measurement Compliance";
            // 
            // _MeasureButton
            // 
            __MeasureButton.Alignment = ToolStripItemAlignment.Right;
            __MeasureButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __MeasureButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __MeasureButton.Image = (System.Drawing.Image)resources.GetObject("_MeasureButton.Image");
            __MeasureButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __MeasureButton.Name = "__MeasureButton";
            __MeasureButton.Size = new System.Drawing.Size(64, 22);
            __MeasureButton.Text = "Measure";
            __MeasureButton.ToolTipText = "Measure resistances";
            // 
            // ResistancesMeterView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_Layout);
            Name = "ResistancesMeterView";
            Padding = new Padding(1);
            Size = new System.Drawing.Size(343, 362);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_DataGrid).EndInit();
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private TableLayoutPanel _Layout;
        private ToolStrip _BottomToolStrip;
        private ToolStripLabel _ReadingLabel;
        private DataGridView _DataGrid;
        private ToolStripLabel _GageTitleLabel;
        private ToolStripLabel _ChannelLabel;
        private ToolStripLabel _ComplianceLabel;
        private Panel _Panel;
        private ToolStripButton __MeasureButton;

        private ToolStripButton _MeasureButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click -= MeasureButton_Click;
                }

                __MeasureButton = value;
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click += MeasureButton_Click;
                }
            }
        }
    }
}