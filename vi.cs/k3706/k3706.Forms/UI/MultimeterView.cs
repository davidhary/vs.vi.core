﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K3706.Forms
{

    /// <summary> A multimeter view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class MultimeterView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MultimeterView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__OpenDetectorCheckBox.Name = "_OpenDetectorCheckBox";
            this.__AutoDelayCheckBox.Name = "_AutoDelayCheckBox";
            this.__ApplyFunctionModeButton.Name = "_ApplyFunctionModeButton";
            this.__FilterEnabledCheckBox.Name = "_FilterEnabledCheckBox";
            this.__SenseFunctionComboBox.Name = "_SenseFunctionComboBox";
            this.__ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton";
        }

        /// <summary> Creates a new <see cref="MultimeterView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static MultimeterView Create()
        {
            MultimeterView view = null;
            try
            {
                view = new MultimeterView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE " 

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3706Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K3706Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindMultimeterSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K3706Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region  MUTLIMETER 

        /// <summary> Gets or sets the Multimeter subsystem. </summary>
        /// <value> The Multimeter subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public MultimeterSubsystem MultimeterSubsystem { get; private set; }

        /// <summary> Bind Multimeter subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindMultimeterSubsystem( K3706Device device )
        {
            if ( this.MultimeterSubsystem is object )
            {
                this.BindSubsystem( false, this.MultimeterSubsystem );
                this.MultimeterSubsystem = null;
            }

            if ( device is object )
            {
                this.MultimeterSubsystem = device.MultimeterSubsystem;
                this.BindSubsystem( true, this.MultimeterSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MultimeterSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.MultimeterSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.SupportedAutoDelayModes ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.AutoDelayMode ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.AutoRangeEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.AutoZeroEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.AutoDelayEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FilterCount ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FilterCountRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FilterEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FilterWindow ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FilterWindowRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.MovingAverageFilterEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FunctionRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FunctionRangeDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.FunctionUnit ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.OpenDetectorEnabled ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.PowerLineCycles ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.PowerLineCyclesDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.PowerLineCyclesRange ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.PowerLineCyclesDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.MultimeterSubsystem.Range ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.MultimeterSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the Multimeter subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MultimeterSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K3706.MultimeterSubsystem.AutoDelayMode ):
                    {
                        if ( subsystem.AutoDelayMode.HasValue )
                            this.AutoDelayMode = subsystem.AutoDelayMode.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.AutoRangeEnabled ):
                    {
                        if ( subsystem.AutoRangeEnabled.HasValue )
                            this._AutoRangeCheckBox.Checked = subsystem.AutoRangeEnabled.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.AutoZeroEnabled ):
                    {
                        if ( subsystem.AutoZeroEnabled.HasValue )
                            this._AutoZeroCheckBox.Checked = subsystem.AutoZeroEnabled.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FilterCount ):
                    {
                        if ( subsystem.FilterCount.HasValue )
                            this._FilterCountNumeric.Value = subsystem.FilterCount.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FilterCountRange ):
                    {
                        _ = this._FilterCountNumeric.RangeSetter( subsystem.FilterCountRange.Min, ( decimal ) subsystem.FilterCountRange.Max );
                        this._FilterCountNumeric.DecimalPlaces = 0;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FilterEnabled ):
                    {
                        if ( subsystem.FilterEnabled.HasValue )
                            this._FilterEnabledCheckBox.Checked = subsystem.FilterEnabled.Value;
                        if ( this._FilterEnabledCheckBox.Checked != this._FilterGroupBox.Enabled )
                            this._FilterGroupBox.Enabled = this._FilterEnabledCheckBox.Checked;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FilterWindow ):
                    {
                        if ( subsystem.FilterWindow.HasValue )
                            this._FilterWindowNumeric.Value = ( decimal ) (100d * subsystem.FilterWindow.Value);
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FilterWindowRange ):
                    {
                        var range = subsystem.FilterWindowRange.TransposedRange( 0d, 100d );
                        _ = this._FilterWindowNumeric.RangeSetter( range.Min, range.Max );
                        this._FilterWindowNumeric.DecimalPlaces = 0;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.MovingAverageFilterEnabled ):
                    {
                        if ( subsystem.MovingAverageFilterEnabled.HasValue )
                            this._MovingAverageRadioButton.Checked = subsystem.MovingAverageFilterEnabled.Value;
                        if ( subsystem.MovingAverageFilterEnabled.HasValue )
                            this._RepeatingAverageRadioButton.Checked = !subsystem.MovingAverageFilterEnabled.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FunctionMode ):
                    {
                        var value = subsystem.FunctionMode.GetValueOrDefault( MultimeterFunctionModes.VoltageDC );
                        _ = this._SenseFunctionComboBox.SelectedEnumValue( value );
                        // changing the function mode changes range, auto delay mode and open detector enabled. 
                        _ = subsystem.QueryRange();
                        _ = subsystem.QueryAutoDelayMode();
                        if ( subsystem.FunctionOpenDetectorCapable )
                            _ = subsystem.QueryOpenDetectorEnabled();
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FunctionRange ):
                    {
                        _ = this._SenseRangeNumeric.RangeSetter( subsystem.FunctionRange.Min, subsystem.FunctionRange.Max );
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FunctionRangeDecimalPlaces ):
                    {
                        this._SenseRangeNumeric.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.FunctionUnit ):
                    {
                        this._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:";
                        this._SenseRangeNumericLabel.Left = this._SenseRangeNumeric.Left - this._SenseRangeNumericLabel.Width;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.OpenDetectorEnabled ):
                    {
                        if ( subsystem.OpenDetectorEnabled.HasValue )
                            this._OpenDetectorCheckBox.Checked = subsystem.OpenDetectorEnabled.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.PowerLineCycles ):
                    {
                        if ( subsystem.PowerLineCycles.HasValue )
                            this._PowerLineCyclesNumeric.Value = ( decimal ) subsystem.PowerLineCycles.Value;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.PowerLineCyclesRange ):
                    {
                        _ = this._PowerLineCyclesNumeric.RangeSetter( subsystem.PowerLineCyclesRange.Min, subsystem.PowerLineCyclesRange.Max );
                        this._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.PowerLineCyclesDecimalPlaces ):
                    {
                        this._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces;
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.Range ):
                    {
                        if ( subsystem.Range.HasValue )
                            this.SenseRangeSetter( subsystem.Range.Value );
                        break;
                    }

                case nameof( K3706.MultimeterSubsystem.LastReading ):
                    {
                        break;
                    }
                // Dim value As String = subsystem.LastReading
                // Me._LastReadingTextBox.Text = If(String.IsNullOrWhiteSpace(value), "<last reading>", value)
                // subsystem.LastActionElapsedTime = Me.ReadElapsedTime(True)
                case nameof( K3706.MultimeterSubsystem.LastActionElapsedTime ):
                    {
                        break;
                    }
                // Dim value As String = subsystem.LastReading
                // Me._LastReadingTextBox.Text = $"{If(String.IsNullOrWhiteSpace(value), "<last reading>", value)} @{subsystem.LastActionElapsedTime.ToExactMilliseconds:0.0}ms"
                case nameof( K3706.MultimeterSubsystem.FailureCode ):
                    {
                        break;
                    }
                // Me._FailureToolStripStatusLabel.Text = subsystem.FailureCode
                case nameof( K3706.MultimeterSubsystem.FailureLongDescription ):
                    {
                        break;
                    }
                // Me._FailureToolStripStatusLabel.ToolTipText = subsystem.FailureLongDescription
                case nameof( K3706.MultimeterSubsystem.ReadingCaption ):
                    {
                        break;
                    }
                // Me._ReadingToolStripStatusLabel.Text = subsystem.ReadingCaption

                case nameof( K3706.MultimeterSubsystem.SupportedAutoDelayModes ):
                    {
                        bool init = this.InitializingComponents;
                        this.InitializingComponents = true;
                        this._SenseFunctionComboBox.ListEnumDescriptions( subsystem.SupportedFunctionModes, ~subsystem.SupportedFunctionModes );
                        this.InitializingComponents = init;
                        break;
                    }
            }
        }

        /// <summary> Multimeter subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MultimeterSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    activity = $"invoking {nameof( this.MultimeterSubsystem )}.{e.PropertyName} change";
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MultimeterSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"handling {nameof( this.MultimeterSubsystem )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as MultimeterSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE SETTINGS: FUNCTION MODE "

        /// <summary> Gets or sets the selected function mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The VI.MultimeterFunctionModes. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]

        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private MultimeterFunctionModes SelectedFunctionMode()
        {
            string activity = $"Selecting function mode {this._SenseFunctionComboBox.SelectedItem}";
            var result = MultimeterFunctionModes.CurrentAC;
            try
            {
                result = this._SenseFunctionComboBox.SelectedEnumValue( result );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( this._SenseFunctionComboBox, Core.Forma.InfoProviderLevel.Error, ex.ToString() );
                _ = this.PublishException( activity, ex );
            }

            return result;
        }

        #endregion

        #region  CONTROL EVENT HANDLERS: SENSE 

        /// <summary> Sense range setter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        private void SenseRangeSetter( double value )
        {
            if ( value <= ( double ) this._SenseRangeNumeric.Maximum && value >= ( double ) this._SenseRangeNumeric.Minimum )
                this._SenseRangeNumeric.Value = ( decimal ) value;
        }

        /// <summary>
        /// Event handler. Called by _SenseFunctionComboBox for selected index changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseFunctionComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} applying function mode";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.Device.MultimeterSubsystem.ApplyFunctionMode( this.SelectedFunctionMode() );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the function mode button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyFunctionModeButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} applying function mode";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.Device.MultimeterSubsystem.ApplyFunctionMode( this.SelectedFunctionMode() );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the selected measurements settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplySenseSettings()
        {
            if ( !Equals( this.Device.MultimeterSubsystem.PowerLineCycles, this._PowerLineCyclesNumeric.Value ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyPowerLineCycles( ( double ) this._PowerLineCyclesNumeric.Value );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.AutoDelayMode, this._AutoDelayMode ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyAutoDelayMode( this._AutoDelayMode );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.AutoRangeEnabled, this._AutoRangeCheckBox.Checked ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyAutoRangeEnabled( this._AutoRangeCheckBox.Checked );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.AutoZeroEnabled, this._AutoZeroCheckBox.Checked ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyAutoZeroEnabled( this._AutoZeroCheckBox.Checked );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.FilterEnabled, this._FilterEnabledCheckBox.Checked ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyFilterEnabled( this._FilterEnabledCheckBox.Checked );
            }

            if ( !Equals( this.Device.MultimeterSubsystem.FilterCount, this._FilterCountNumeric.Value ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyFilterCount( ( int ) Math.Round( this._FilterCountNumeric.Value ) );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.MovingAverageFilterEnabled, this._MovingAverageRadioButton.Checked ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyMovingAverageFilterEnabled( this._MovingAverageRadioButton.Checked );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.OpenDetectorEnabled, this._OpenDetectorCheckBox.Checked ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyOpenDetectorEnabled( this._OpenDetectorCheckBox.Checked );
            }

            if ( this.Device.MultimeterSubsystem.AutoRangeEnabled == true )
            {
                _ = this.Device.MultimeterSubsystem.QueryRange();
            }
            else if ( !Equals( this.Device.MultimeterSubsystem.Range, this._SenseRangeNumeric.Value ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyRange( ( int ) Math.Round( this._SenseRangeNumeric.Value ) );
            }

            if ( !Nullable.Equals( this.Device.MultimeterSubsystem.FilterWindow, 0.01d * ( double ) this._FilterWindowNumeric.Value ) )
            {
                _ = this.Device.MultimeterSubsystem.ApplyFilterWindow( 0.01d * ( double ) this._FilterWindowNumeric.Value );
            }
        }

        /// <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySenseSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} applying sense functions";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.SelectedFunctionMode() != this.Device.MultimeterSubsystem.FunctionMode.Value )
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Info, "Set function first" );
                }
                else
                {
                    this.ApplySenseSettings();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Filter enabled check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FilterEnabledCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._FilterGroupBox.Enabled = this._FilterEnabledCheckBox.Checked;
        }

        /// <summary> Opens detector check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenDetectorCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            this._OpenDetectorCheckBox.Text = $"Open Detector {this._OpenDetectorCheckBox.Checked.GetHashCode():'ON';'ON';'OFF'}";
        }

        /// <summary> The automatic delay mode. </summary>
        private MultimeterAutoDelayModes _AutoDelayMode;

        /// <summary> Gets or sets the automatic delay mode. </summary>
        /// <value> The automatic delay mode. </value>
        private MultimeterAutoDelayModes AutoDelayMode
        {
            get {
                if ( this._AutoDelayCheckBox.CheckState == CheckState.Checked )
                {
                    this._AutoDelayMode = MultimeterAutoDelayModes.On;
                }
                else if ( this._AutoDelayCheckBox.CheckState == CheckState.Indeterminate )
                {
                    this._AutoDelayMode = MultimeterAutoDelayModes.Once;
                }
                else
                {
                    this._AutoDelayMode = MultimeterAutoDelayModes.Off;
                }

                return this._AutoDelayMode;
            }

            set {
                this._AutoDelayMode = value;
                switch ( value )
                {
                    case MultimeterAutoDelayModes.Off:
                        {
                            this._AutoDelayCheckBox.CheckState = CheckState.Unchecked;
                            break;
                        }

                    case MultimeterAutoDelayModes.On:
                        {
                            this._AutoDelayCheckBox.CheckState = CheckState.Checked;
                            break;
                        }

                    case MultimeterAutoDelayModes.Once:
                        {
                            this._AutoDelayCheckBox.CheckState = CheckState.Indeterminate;
                            break;
                        }
                }
            }
        }

        /// <summary> Automatic delay check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AutoDelayCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this._AutoDelayCheckBox.CheckState == CheckState.Checked )
            {
                this.AutoDelayMode = MultimeterAutoDelayModes.On;
                this._AutoDelayCheckBox.Text = "Auto Delay ON";
            }
            else if ( this._AutoDelayCheckBox.CheckState == CheckState.Indeterminate )
            {
                this.AutoDelayMode = MultimeterAutoDelayModes.Once;
                this._AutoDelayCheckBox.Text = "Auto Delay ONCE";
            }
            else
            {
                this.AutoDelayMode = MultimeterAutoDelayModes.Off;
                this._AutoDelayCheckBox.Text = "Auto Delay OFF";
            }
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}