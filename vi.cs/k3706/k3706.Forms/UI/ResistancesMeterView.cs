﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K3706.Forms
{

    /// <summary> A meter control. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-09 </para>
    /// </remarks>
    public partial class ResistancesMeterView : Core.Forma.ModelViewTalkerBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device">        The device. </param>
        /// <param name="isDeviceOwner"> True if is device owner, false if not. </param>
        public ResistancesMeterView( ResistancesMeterDevice device, bool isDeviceOwner ) : base()
        {
            this.NewThis( device, isDeviceOwner );
            this.__MeasureButton.Name = "_MeasureButton";
        }

        #region " CONSTRUCTION " 

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ResistancesMeterView() : this( ResistancesMeterDevice.Create(), true )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device">        The device. </param>
        /// <param name="isDeviceOwner"> True if is device owner, false if not. </param>
        private void NewThis( ResistancesMeterDevice device, bool isDeviceOwner )
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._BottomToolStrip.Renderer = new CustomProfessionalRenderer();
            this.AssignDeviceThis( device, isDeviceOwner );
        }

        /// <summary>
        /// Releases the unmanaged resources used by the isr.VI.Instrument.ResourcePanelBase and
        /// optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null, this.IsDeviceOwner );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region  FORM EVENTS 

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                this._BottomToolStrip.Renderer = new CustomProfessionalRenderer();
                this._BottomToolStrip.Invalidate();
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " DEVICE " 

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ResistancesMeterDevice Device { get; private set; }

        /// <summary> Gets or sets the is device that owns this item. </summary>
        /// <value> The is device owner. </value>
        private bool IsDeviceOwner { get; set; }

        /// <summary> Assign device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">         The value. </param>
        /// <param name="isDeviceOwner"> True if is device owner, false if not. </param>
        private void AssignDeviceThis( ResistancesMeterDevice value, bool isDeviceOwner )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                if ( this.IsDeviceOwner )
                    this.Device?.Dispose();
                this.Device = null;
            }

            this.Device = value;
            this.IsDeviceOwner = isDeviceOwner;
            if ( value is object )
            {
                // If Me.Device.ChannelSubsystem IsNot Nothing Then AddHandler Me.Device.ChannelSubsystem.PropertyChanged, AddressOf Me.ChannelSubsystemPropertyChanged
                this.AssignTalker( this.Device.Talker );

                // TO_DO: Bind resistors to data grid
                // Me.Device.Resistors?.DisplayValues(Me._DataGrid)

                // TO_D: Bind measurement enabled to 
                // Me._MeasureGroupBox.Enabled = sender.MeasurementEnabled
                // Me._MeasureGroupBox.Enabled = Me.Device.IsDeviceOpen AndAlso Me.Device.MultimeterSubsystem.FunctionMode.HasValue AndAlso Me.Device.MultimeterSubsystem.FunctionMode.Value = MultimeterFunctionMode.ResistanceFourWire

                // TO_DO: Add to the 3700 an option to add the resistance view
                // Use the 3700 Multimeter tab to set the filtering.
                // Include in the K3706 device as an

                // bind resistance title, resistance value, closed channels. 

            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value">         True to show or False to hide the control. </param>
        /// <param name="isDeviceOwner"> True if is device owner, false if not. </param>
        public void AssignDevice( ResistancesMeterDevice value, bool isDeviceOwner )
        {
            this.AssignDeviceThis( value, isDeviceOwner );
        }

        #endregion

        #region  CONTROL EVENTS 

        /// <summary> Measure button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        private void MeasureButton_Click( object sender, EventArgs e )
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;
                var args = new Core.ActionEventArgs();
                if ( this.Device.IsMeasurementEnabled() )
                {
                    string activity = $"{this.Device.ResourceNameCaption} measuring resistances";
                    _ = this.Device.TryMeasureResistors();
                }
                else
                {
                    _ = this.PublishInfo( "measurement not enabled; configure first" );
                }
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region  TOOL STRIP RENDERER 

        /// <summary> A custom professional renderer. </summary>
        /// <remarks>
        /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
        /// Licensed under The MIT License.</para><para>
        /// David, 2017-10-30 </para>
        /// </remarks>
        private class CustomProfessionalRenderer : ToolStripProfessionalRenderer
        {

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
            /// event.
            /// </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
            ///                  contains the event data. </param>
            protected override void OnRenderLabelBackground( ToolStripItemRenderEventArgs e )
            {
                if ( e is object && e.Item.BackColor != System.Drawing.SystemColors.ControlDark )
                {
                    using ( var brush = new System.Drawing.SolidBrush( e.Item.BackColor ) )
                    {
                        e.Graphics.FillRectangle( brush, e.Item.ContentRectangle );
                    }
                }
            }
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}