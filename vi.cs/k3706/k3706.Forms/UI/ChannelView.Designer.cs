﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.VI.Tsp.K3706.Forms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class ChannelView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            _ClosedChannelsTextBoxLabel = new System.Windows.Forms.Label();
            _ClosedChannelsTextBox = new System.Windows.Forms.TextBox();
            __OpenAllButton = new System.Windows.Forms.Button();
            __OpenAllButton.Click += new EventHandler(OpenAllButton_Click);
            __OpenChannelsButton = new System.Windows.Forms.Button();
            __OpenChannelsButton.Click += new EventHandler(OpenChannelsButton_Click);
            __CloseOnlyButton = new System.Windows.Forms.Button();
            __CloseOnlyButton.Click += new EventHandler(CloseOnlyButton_Click);
            __CloseChannelsButton = new System.Windows.Forms.Button();
            __CloseChannelsButton.Click += new EventHandler(CloseChannelsButton_Click);
            _ChannelListComboBox = new System.Windows.Forms.ComboBox();
            _ChannelListComboBoxLabel = new System.Windows.Forms.Label();
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_ClosedChannelsTextBoxLabel);
            _Panel.Controls.Add(_ClosedChannelsTextBox);
            _Panel.Controls.Add(__OpenAllButton);
            _Panel.Controls.Add(__OpenChannelsButton);
            _Panel.Controls.Add(__CloseOnlyButton);
            _Panel.Controls.Add(__CloseChannelsButton);
            _Panel.Controls.Add(_ChannelListComboBox);
            _Panel.Controls.Add(_ChannelListComboBoxLabel);
            _Panel.Location = new System.Drawing.Point(13, 17);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(352, 287);
            _Panel.TabIndex = 0;
            // 
            // _ClosedChannelsTextBoxLabel
            // 
            _ClosedChannelsTextBoxLabel.AutoSize = true;
            _ClosedChannelsTextBoxLabel.Location = new System.Drawing.Point(10, 106);
            _ClosedChannelsTextBoxLabel.Name = "_ClosedChannelsTextBoxLabel";
            _ClosedChannelsTextBoxLabel.Size = new System.Drawing.Size(51, 17);
            _ClosedChannelsTextBoxLabel.TabIndex = 14;
            _ClosedChannelsTextBoxLabel.Text = "Closed:";
            // 
            // _ClosedChannelsTextBox
            // 
            _ClosedChannelsTextBox.Location = new System.Drawing.Point(9, 128);
            _ClosedChannelsTextBox.Multiline = true;
            _ClosedChannelsTextBox.Name = "_ClosedChannelsTextBox";
            _ClosedChannelsTextBox.ReadOnly = true;
            _ClosedChannelsTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _ClosedChannelsTextBox.Size = new System.Drawing.Size(332, 151);
            _ClosedChannelsTextBox.TabIndex = 15;
            // 
            // _OpenAllButton
            // 
            __OpenAllButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __OpenAllButton.Location = new System.Drawing.Point(251, 73);
            __OpenAllButton.Name = "__OpenAllButton";
            __OpenAllButton.Size = new System.Drawing.Size(90, 30);
            __OpenAllButton.TabIndex = 13;
            __OpenAllButton.Text = "Open &All";
            __OpenAllButton.UseVisualStyleBackColor = true;
            // 
            // _OpenChannelsButton
            // 
            __OpenChannelsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __OpenChannelsButton.Location = new System.Drawing.Point(285, 6);
            __OpenChannelsButton.Name = "__OpenChannelsButton";
            __OpenChannelsButton.Size = new System.Drawing.Size(56, 30);
            __OpenChannelsButton.TabIndex = 9;
            __OpenChannelsButton.Text = "&Open";
            __OpenChannelsButton.UseVisualStyleBackColor = true;
            // 
            // _CloseOnlyButton
            // 
            __CloseOnlyButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __CloseOnlyButton.Location = new System.Drawing.Point(102, 73);
            __CloseOnlyButton.Name = "__CloseOnlyButton";
            __CloseOnlyButton.Size = new System.Drawing.Size(142, 30);
            __CloseOnlyButton.TabIndex = 12;
            __CloseOnlyButton.Text = "Open All and &Close";
            __CloseOnlyButton.UseVisualStyleBackColor = true;
            // 
            // _CloseChannelsButton
            // 
            __CloseChannelsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __CloseChannelsButton.Location = new System.Drawing.Point(220, 6);
            __CloseChannelsButton.Name = "__CloseChannelsButton";
            __CloseChannelsButton.Size = new System.Drawing.Size(57, 30);
            __CloseChannelsButton.TabIndex = 8;
            __CloseChannelsButton.Text = "&Close";
            __CloseChannelsButton.UseVisualStyleBackColor = true;
            // 
            // _ChannelListComboBox
            // 
            _ChannelListComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _ChannelListComboBox.Location = new System.Drawing.Point(9, 38);
            _ChannelListComboBox.Name = "_ChannelListComboBox";
            _ChannelListComboBox.Size = new System.Drawing.Size(332, 25);
            _ChannelListComboBox.TabIndex = 11;
            _ChannelListComboBox.Text = "1921,1912,1001,1031";
            // 
            // _ChannelListComboBoxLabel
            // 
            _ChannelListComboBoxLabel.AutoSize = true;
            _ChannelListComboBoxLabel.Location = new System.Drawing.Point(8, 18);
            _ChannelListComboBoxLabel.Name = "_ChannelListComboBoxLabel";
            _ChannelListComboBoxLabel.Size = new System.Drawing.Size(80, 17);
            _ChannelListComboBoxLabel.TabIndex = 10;
            _ChannelListComboBoxLabel.Text = "Channel List:";
            // 
            // ChannelView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "ChannelView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.Label _ClosedChannelsTextBoxLabel;
        private System.Windows.Forms.TextBox _ClosedChannelsTextBox;
        private System.Windows.Forms.Button __OpenAllButton;

        private System.Windows.Forms.Button _OpenAllButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenAllButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenAllButton != null)
                {
                    __OpenAllButton.Click -= OpenAllButton_Click;
                }

                __OpenAllButton = value;
                if (__OpenAllButton != null)
                {
                    __OpenAllButton.Click += OpenAllButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __OpenChannelsButton;

        private System.Windows.Forms.Button _OpenChannelsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenChannelsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenChannelsButton != null)
                {
                    __OpenChannelsButton.Click -= OpenChannelsButton_Click;
                }

                __OpenChannelsButton = value;
                if (__OpenChannelsButton != null)
                {
                    __OpenChannelsButton.Click += OpenChannelsButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __CloseOnlyButton;

        private System.Windows.Forms.Button _CloseOnlyButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseOnlyButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseOnlyButton != null)
                {
                    __CloseOnlyButton.Click -= CloseOnlyButton_Click;
                }

                __CloseOnlyButton = value;
                if (__CloseOnlyButton != null)
                {
                    __CloseOnlyButton.Click += CloseOnlyButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __CloseChannelsButton;

        private System.Windows.Forms.Button _CloseChannelsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __CloseChannelsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__CloseChannelsButton != null)
                {
                    __CloseChannelsButton.Click -= CloseChannelsButton_Click;
                }

                __CloseChannelsButton = value;
                if (__CloseChannelsButton != null)
                {
                    __CloseChannelsButton.Click += CloseChannelsButton_Click;
                }
            }
        }

        private System.Windows.Forms.ComboBox _ChannelListComboBox;
        private System.Windows.Forms.Label _ChannelListComboBoxLabel;
        private System.Windows.Forms.TableLayoutPanel _Layout;
    }
}