﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K3706.Forms
{

    /// <summary> A channel view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ChannelView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ChannelView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__OpenAllButton.Name = "_OpenAllButton";
            this.__OpenChannelsButton.Name = "_OpenChannelsButton";
            this.__CloseOnlyButton.Name = "_CloseOnlyButton";
            this.__CloseChannelsButton.Name = "_CloseChannelsButton";
        }

        /// <summary> Creates a new <see cref="ChannelView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ChannelView"/>. </returns>
        public static ChannelView Create()
        {
            ChannelView view = null;
            try
            {
                view = new ChannelView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE " 

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3706Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K3706Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindChannelSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K3706Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL " 

        /// <summary> Gets or sets the Channel subsystem. </summary>
        /// <value> The Channel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ChannelSubsystem ChannelSubsystem { get; private set; }

        /// <summary> Bind Channel subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindChannelSubsystem( K3706Device device )
        {
            if ( this.ChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.ChannelSubsystem );
                this.ChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.ChannelSubsystem = device.ChannelSubsystem;
                this.BindSubsystem( true, this.ChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K3706.ChannelSubsystem.ClosedChannels ) );
                this.HandlePropertyChanged( subsystem, nameof( K3706.ChannelSubsystem.LastActionElapsedTime ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the Channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( ChannelSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K3706.ChannelSubsystem.ClosedChannels ):
                    {
                        subsystem.LastActionElapsedTime = this.Device.Session.ReadElapsedTime( true );
                        this.ClosedChannels = subsystem.ClosedChannels;
                        break;
                    }

                case nameof( K3706.ChannelSubsystem.LastActionElapsedTime ):
                    {
                        break;
                    }
                    // BIND TO DISPLAY SOURCE LABEL
                    // Me._ChannelListTextBox.Text = $"{subsystem.ClosedChannels} @{subsystem.LastActionElapsedTime.ToExactMilliseconds:0.0}ms"
            }
        }

        /// <summary> Channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    activity = $"invoking {nameof( this.ChannelSubsystem )}.{e.PropertyName} change";
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"handling {nameof( this.ChannelSubsystem )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as ChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNELS "

        /// <summary> Gets or sets the scan list of closed channels. </summary>
        /// <value> The closed channels. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]

        public string ClosedChannels
        {
            get => this._ClosedChannelsTextBox.Text;

            set {
                if ( string.IsNullOrEmpty( value ) )
                {
                    value = "nil";
                }
                else if ( string.IsNullOrWhiteSpace( value ) )
                {
                    value = "all open";
                }

                this._ClosedChannelsTextBox.Text = value;
            }
        }

        /// <summary> Adds new items to the combo box. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        internal void UpdateChannelListComboBox()
        {
            if ( this.Visible )
            {
                // check if we are asking for a new channel list
                if ( this._ChannelListComboBox is object && !string.IsNullOrWhiteSpace( this._ChannelListComboBox.Text ) && this._ChannelListComboBox.FindString( this._ChannelListComboBox.Text ) < 0 )
                {
                    // if we have a new string, add it to the channel list
                    _ = this._ChannelListComboBox.Items.Add( this._ChannelListComboBox.Text );
                }
            }
        }

        /// <summary> Event handler. Called by _closeChannelsButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CloseChannelsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} updating closed channel list";
                this.UpdateChannelListComboBox();
                // this also turns off status bit setting that enable notifications of wait completion.
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                this.Device.ClearExecutionState();
                // must be reabled after clearing execution state.
                activity = $"{this.Device.ResourceNameCaption} enabling wait complete";
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} closing {this._ChannelListComboBox.Text}";
                this.Device.ChannelSubsystem.StartElapsedStopwatch( 2 );
                _ = this.Device.ChannelSubsystem.ApplyClosedChannels( this._ChannelListComboBox.Text, TimeSpan.FromSeconds( 2d ) );
                this.Device.ChannelSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by channel_OpenButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenChannelsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} updating closed channel list";
                this.UpdateChannelListComboBox();
                // this also turns off status bit setting that enable notifications of wait completion.
                activity = $"{this.Device.ResourceNameCaption} clearing execution state";
                this.Device.ClearExecutionState();
                // must be reabled after clearing execution state.
                activity = $"{this.Device.ResourceNameCaption} enabling wait complete";
                this.Device.Session.EnableServiceRequestWaitComplete();
                activity = $"{this.Device.ResourceNameCaption} opening {this._ChannelListComboBox.Text}";
                this.Device.ChannelSubsystem.StartElapsedStopwatch( 2 );
                _ = this.Device.ChannelSubsystem.ApplyOpenChannels( this._ChannelListComboBox.Text, TimeSpan.FromSeconds( 2d ) );
                this.Device.ChannelSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by CloseOnlyButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CloseOnlyButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} closing only channels";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.UpdateChannelListComboBox();
                // this also turns off status bit setting that enable notifications of wait completion.
                this.Device.ClearExecutionState();
                // must be reabled after clearing execution state.
                this.Device.Session.EnableServiceRequestWaitComplete();
                this.Device.ChannelSubsystem.StartElapsedStopwatch( 3 );
                _ = this.Device.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
                _ = this.Device.ChannelSubsystem.ApplyClosedChannels( this._ChannelListComboBox.Text, TimeSpan.FromSeconds( 2d ) );
            }
            // this works only if a single channel:
            // VI.ChannelSubsystem.CloseChannels(Me.Device, Me._channelListComboBox.Text)
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by OpenAllButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenAllButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} opening all channels";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                // this also turns off status bit setting that enable notifications of wait completion.
                this.Device.ClearExecutionState();
                // must be reabled after clearing execution state.
                this.Device.Session.EnableServiceRequestWaitComplete();
                this.Device.ChannelSubsystem.StartElapsedStopwatch( 2 );
                _ = this.Device.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}