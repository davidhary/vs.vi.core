using System;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K3706.Forms.MSTest
{

    /// <summary> K3706 Visa View unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3706" )]
    public class K3706VisaViewTests
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region  VISA VIEW: DEVICE OPEN TEST 

        /// <summary> (Unit Test Method) tests selected resource name. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SelectedResourceNameTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaView( device );
                FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected( TestInfo, view, ResourceSettings.Get() );
            }

            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaTreeView( device );
                FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected( TestInfo, view, ResourceSettings.Get() );
            }
        }

        /// <summary> (Unit Test Method) tests the Visa View talker trace message. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void TalkerTraceMessageTest()
        {
            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaView( device );
                FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit( view, TestInfo.TraceMessagesQueueListener );
            }

            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaTreeView( device );
                FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit( view, TestInfo.TraceMessagesQueueListener );
            }
        }

        /// <summary> A test for Open connect and disconnect. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaView( device );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen( TestInfo, 0, view, ResourceSettings.Get() );
                    FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch( view.VisaSessionBase.Session, ResourceSettings.Get() );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 0, view );
                }
            }

            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaTreeView( device );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen( TestInfo, 0, view, ResourceSettings.Get() );
                    FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch( view.VisaSessionBase.Session, ResourceSettings.Get() );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 0, view );
                }
            }
        }

        /// <summary> A test for dual Open. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionTwiceTest()
        {
            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using ( Facade.IVisaView view = new Facade.VisaView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
                }

                using ( Facade.IVisaView view = new Facade.VisaView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 2, view, ResourceSettings.Get() );
                }
            }

            using ( var device = K3706Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using ( var view = new Facade.VisaTreeView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
                }

                using ( var view = new Facade.VisaTreeView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 2, view, ResourceSettings.Get() );
                }

                using ( var view = new Facade.VisaTreeView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 3, view, ResourceSettings.Get() );
                    Task.Delay( TimeSpan.FromMilliseconds( 100d ) ).Wait();
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 4, view, ResourceSettings.Get() );
                }
            }
        }

        #endregion

        #region  VISA VIEW: ASSIGNED DEVICE TESTS 

        /// <summary> (Unit Test Method) tests assign device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignDeviceTest()
        {
            using ( var view = new K3706View() )
            {
                using var device = K3706Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                view.AssignDevice( device );
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
            }

            using ( var view = new K3706TreeView() )
            {
                using var device = K3706Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                view.AssignDevice( device );
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
            }
        }

        /// <summary> (Unit Test Method) tests assign open device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignOpenDeviceTest()
        {
            using ( var view = new K3706View() )
            {
                using var device = K3706Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, 1, device, ResourceSettings.Get() );
                    view.AssignDevice( device );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 1, view );
                }
            }

            using ( var view = new K3706TreeView() )
            {
                using var device = K3706Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, 1, device, ResourceSettings.Get() );
                    view.AssignDevice( device );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 1, view );
                }
            }
        }

        #endregion

    }
}
