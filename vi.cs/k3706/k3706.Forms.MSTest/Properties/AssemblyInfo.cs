﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K3706 VI Forms Tests" )]
[assembly: AssemblyDescription( "K3706 Virtual Instrument Forms Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K3706.Forms.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
