namespace isr.VI.Tsp.K3706.Forms.MSTest
{

    /// <summary> A resistances meter Test Info. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class ResistancesMeterSettings : DeviceTests.TestSettingsBase
    {

        #region  SINGLETON 

        /// <summary> Specialized default constructor for use only by derived class. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        private ResistancesMeterSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(ResistancesMeterSettings)} Editor", Get());
        }

        /// <summary>
    /// Gets the locking object to enforce thread safety when creating the singleton instance.
    /// </summary>
    /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
    /// <value> The instance. </value>
        private static ResistancesMeterSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
    /// <remarks> Use this property to instantiate a single instance of this class. This class uses
    /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    /// <returns> A new or existing instance of the class. </returns>
        public static ResistancesMeterSettings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (ResistancesMeterSettings)Synchronized(new ResistancesMeterSettings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
    /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region  MEASURE SUBSYSTEM INFORMATION 

        /// <summary> Gets or sets the auto zero Enabled settings. </summary>
    /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoZeroEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the automatic range enabled. </summary>
    /// <value> The automatic range enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoRangeEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the Sense Function settings. </summary>
    /// <value> The Sense Function settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("ResistanceFourWire")]
        public MultimeterFunctionModes SenseFunctionMode
        {
            get
            {
                return AppSettingEnum<MultimeterFunctionModes>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets the power line cycles settings. </summary>
    /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public double PowerLineCycles
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        #endregion

    }
}
