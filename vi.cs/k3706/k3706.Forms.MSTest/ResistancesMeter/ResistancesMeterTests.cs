using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp.K3706.Forms.MSTest
{

    /// <summary> Resistances meter unit tests. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-15 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3706" )]
    public class ResistancesMeterTests
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
                _ResistancesMeterDevice = ResistancesMeterDevice.Create();
                _ResistancesMeter = new ResistancesMeterView();
                _ResistancesMeterDevice.AddListener( TestInfo.TraceMessagesQueueListener );
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
            if ( _ResistancesMeter is object )
            {
                _ResistancesMeter.Dispose();
                _ResistancesMeter = null;
            }

            if ( _ResistancesMeterDevice is object )
            {
                _ResistancesMeterDevice.Dispose();
                _ResistancesMeterDevice = null;
            }
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( ResistancesMeterSettings.Get().Exists, $"{typeof( ResistancesMeterSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region  SHARED DEVICE 

        /// <summary> The resistances meter. </summary>
        private static ResistancesMeterView _ResistancesMeter;

        /// <summary> The resistances meter device. </summary>
        private static ResistancesMeterDevice _ResistancesMeterDevice;

        /// <summary> Opens a session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trialNumber"> The trial number. </param>
        /// <param name="device">      The resistance meter device. </param>
        internal static void OpenSession( int trialNumber, ResistancesMeterDevice device )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            var (success, details) = device.TryOpenSession( ResourceSettings.Get().ResourceName, ResourceSettings.Get().ResourceTitle );
            Assert.IsTrue( success, $"{trialNumber} session should open; {details}" );
            Assert.IsTrue( device.IsDeviceOpen, $"{trialNumber} session to device {device.ResourceNameCaption} should be open" );

            // check the MODEL
            Assert.AreEqual( ResourceSettings.Get().ResourceModel, device.StatusSubsystem.VersionInfo.Model, $"Version Info Model {device.ResourceNameCaption}", System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary> Closes a session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="trialNumber"> The trial number. </param>
        /// <param name="device">      The resistance meter device. </param>
        internal static void CloseSession( int trialNumber, ResistancesMeterDevice device )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            bool actualBoolean;
            _ = device.TryCloseSession();
            actualBoolean = device.IsDeviceOpen;
            bool expectedBoolean = false;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} Disconnect still connected {device.ResourceNameCaption}" );
            actualBoolean = device.IsDeviceOpen;
            expectedBoolean = false;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"{trialNumber} Close still open {device.ResourceNameCaption}" );
        }

        #endregion

        #region  ASSIGNED DEVICE TESTS 

        /// <summary> (Unit Test Method) tests assigned device measure resistor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignedDeviceMeasureResistorTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            try
            {
                _ResistancesMeter.AssignDevice( _ResistancesMeterDevice, false );
                OpenSession( 1, _ResistancesMeter.Device );
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseSession( 1, _ResistancesMeter.Device );
                _ResistancesMeter.AssignDevice( null, true );
            }
        }

        /// <summary> (Unit Test Method) tests assigned open device measure resistor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignedOpenDeviceMeasureResistorTest()
        {
            try
            {
                OpenSession( 0, _ResistancesMeterDevice );
                _ResistancesMeter.AssignDevice( _ResistancesMeterDevice, false );
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseSession( 1, _ResistancesMeter.Device );
                _ResistancesMeter.AssignDevice( null, true );
            }
        }

        #endregion

    }
}
