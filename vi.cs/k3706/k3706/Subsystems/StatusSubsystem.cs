using System;

namespace isr.VI.Tsp.K3706
{

    /// <summary> Status subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> The session. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( session )
        {
            this.VersionInfoBase = new VersionInfo();
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region  I PRESETTABLE 

        /// <summary> Define measurement events bitmasks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( MeasurementEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            int failuresSummaryBitmask = 0;
            int bitmask;
            bitmask = 1 << 0;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit1, bitmask );
            bitmask = 1 << 1;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit1, bitmask );
            bitmask = 1 << 2;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit2, bitmask );
            bitmask = 1 << 3;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit2, bitmask );
            bitmask = 1 << 7;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.ReadingOverflow, bitmask );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.BufferAvailable, 1 << 8 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.Questionable, 1 << 31, true );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.FailuresSummary, failuresSummaryBitmask );
        }

        /// <summary> Define measurement events bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineMeasurementEventsBitmasks()
        {
            base.DefineMeasurementEventsBitmasks();
            DefineBitmasks( this.MeasurementEventsBitmasks );
        }

        #endregion

    }
}
