using System;
using System.Diagnostics;

namespace isr.VI.Tsp.K3706
{

    /// <summary> Defines a Multimeter Subsystem for a Keithley 3700 instrument. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-01-15 </para>
    /// </remarks>
    public class MultimeterSubsystem : MultimeterSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MultimeterSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.OpenDetectorCapabilities[( int ) MultimeterFunctionModes.ResistanceFourWire] = true;
            this.FunctionModeDecimalPlaces[( int ) MultimeterFunctionModes.ResistanceCommonSide] = 0;
            this.FunctionModeDecimalPlaces[( int ) MultimeterFunctionModes.ResistanceFourWire] = 0;
            this.FunctionModeDecimalPlaces[( int ) MultimeterFunctionModes.ResistanceTwoWire] = 0;
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.CurrentAC].SetRange( 0d, 3.1d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.CurrentDC].SetRange( 0d, 3.1d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.VoltageDC].SetRange( 0d, 303d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.VoltageAC].SetRange( 0d, 303d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.ResistanceCommonSide].SetRange( 0d, 120000000.0d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.ResistanceTwoWire].SetRange( 0d, 120000000.0d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.ResistanceFourWire].SetRange( 0d, 120000000.0d );
        }

        #endregion

        #region  I PRESETTABLE 

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.ApertureRange = new Core.Primitives.RangeR( 0.00000833d, 0.25d );
            this.FilterCountRange = new Core.Primitives.RangeI( 1, 100 );
            this.FilterWindowRange = new Core.Primitives.RangeR( 0d, 0.1d );
            this.PowerLineCyclesRange = new Core.Primitives.RangeR( 0.0005d, 15d );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.PowerLineCycles = 1;
            this.AutoDelayMode = MultimeterAutoDelayModes.Once;
            this.AutoRangeEnabled = true;
            this.AutoZeroEnabled = true;
            this.FilterCount = 10;
            this.FilterEnabled = false;
            this.MovingAverageFilterEnabled = false;
            this.OpenDetectorEnabled = false;
            this.FilterWindow = 0.001d;
            this.FunctionMode = MultimeterFunctionModes.VoltageDC;
            this.Range = 303; // defaults volts range
        }

        #endregion

        #region  COMMAND SYNTAX 

        #region  APERTURE 

        /// <summary> Gets or sets the Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string ApertureQueryCommand { get; set; } = "_G.print(_G.dmm.aperture)";

        /// <summary> Gets or sets the Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected override string ApertureCommandFormat { get; set; } = "_G.dmm.aperture={0}";

        #endregion

        #region  AUTO DELAY MODE 

        /// <summary> Queries the Multimeter Auto Delay Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// The <see cref="MultimeterAutoDelayModes">Multimeter Auto Delay Mode</see> or none if unknown.
        /// </returns>
        public override MultimeterAutoDelayModes? QueryAutoDelayMode()
        {
            string mode = this.AutoDelayMode.ToString();
            this.Session.MakeEmulatedReplyIfEmpty( mode );
            mode = this.Session.QueryTrimEnd( Syntax.Lua.PrintCommandStringIntegerFormat, "_G.dmm.autodelay" );
            if ( string.IsNullOrWhiteSpace( mode ) )
            {
                string message = "Failed fetching Multimeter Auto Delay Mode";
                Debug.Assert( !Debugger.IsAttached, message );
                this.AutoDelayMode = new MultimeterAutoDelayModes?();
            }
            else
            {
                if ( Enum.TryParse( mode, out MultimeterAutoDelayModes value ) )
                {
                    this.AutoDelayMode = value;
                }
                else
                {
                    string message = $"Failed parsing Multimeter Auto Delay Mode value of '{mode}'";
                    Debug.Assert( !Debugger.IsAttached, message );
                    this.AutoDelayMode = new MultimeterAutoDelayModes?();
                }
            }

            return this.AutoDelayMode;
        }

        /// <summary>
        /// Writes the Multimeter Auto Delay Mode without reading back the value from the device.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The Auto Delay Mode. </param>
        /// <returns>
        /// The <see cref="MultimeterAutoDelayModes">Multimeter Auto Delay Mode</see> or none if unknown.
        /// </returns>
        public override MultimeterAutoDelayModes? WriteAutoDelayMode( MultimeterAutoDelayModes value )
        {
            _ = this.Session.WriteLine( "_G.dmm.autodelay={0}", ( object ) ( int ) value );
            this.AutoDelayMode = value;
            return this.AutoDelayMode;
        }

        #endregion

        #region  AUTO RANGE ENABLED 

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = "_G.dmm.autorange={0:'1';'1';'0'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.autorange==1)";

        #endregion

        #region  AUTO ZERO ENABLED 

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = "_G.dmm.autozero={0:'1';'1';'0'}";

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.autozero==1)";

        #endregion

        #region  CONNECT/DISCONNECT 

        /// <summary> Gets or sets the 'connect' command. </summary>
        /// <value> The 'connect' command. </value>
        protected override string ConnectCommand { get; set; } = "if nil ~= dmm then dmm.connect = 7 end";

        /// <summary> Gets or sets the 'disconnect' command. </summary>
        /// <value> The 'disconnect' command. </value>
        protected override string DisconnectCommand { get; set; } = "if nil ~= dmm then dmm.connect = 0 end";

        #endregion

        #region  FILTER 

        #region  FILTER COUNT 

        /// <summary> Gets or sets the Filter Count query command. </summary>
        /// <value> The FilterCount query command. </value>
        protected override string FilterCountQueryCommand { get; set; } = "_G.print(_G.dmm.filter.count)";

        /// <summary> Gets or sets the Filter Count command format. </summary>
        /// <value> The FilterCount command format. </value>
        protected override string FilterCountCommandFormat { get; set; } = "_G.dmm.filter.count={0}";

        #endregion

        #region  FILTER ENABLED 

        /// <summary> Gets or sets the Filter enabled command Format. </summary>
        /// <value> The Filter enabled query command. </value>
        protected override string FilterEnabledCommandFormat { get; set; } = "_G.dmm.filter.enable={0:'1';'1';'0'}";

        /// <summary> Gets or sets the Filter enabled query command. </summary>
        /// <value> The Filter enabled query command. </value>
        protected override string FilterEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.filter.enable==1)";

        #endregion

        #region  MOVING AVERAGE ENABLED 

        /// <summary> Gets or sets the moving average filter enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string MovingAverageFilterEnabledCommandFormat { get; set; } = "_G.dmm.filter.type={0:'0';'0';'1'}";

        /// <summary> Gets or sets the moving average filter enabled query command. </summary>
        /// <value> The moving average filter enabled query command. </value>
        protected override string MovingAverageFilterEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.filter.type==0)";

        #endregion

        #region  FILTER Window 

        /// <summary> Gets or sets the Filter Window query command. </summary>
        /// <value> The FilterWindow query command. </value>
        protected override string FilterWindowQueryCommand { get; set; } = "_G.print(_G.dmm.filter.window)";

        /// <summary> Gets or sets the Filter Window command format. </summary>
        /// <value> The FilterWindow command format. </value>
        protected override string FilterWindowCommandFormat { get; set; } = "_G.dmm.filter.window={0}";

        #endregion

        #endregion

        #region  FUNCTION MODE 

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = "_G.dmm.func='{0}'";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = "_G.print(_G.dmm.func)";

        #endregion

        #region  MEASURE 

        /// <summary> Gets or sets the Measure query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string MeasureQueryCommand { get; set; } = "_G.print(_G.dmm.measure())";

        #endregion

        #region  OPEN DETECTOR ENABLED 

        /// <summary> Gets or sets the open detector enabled command Format. </summary>
        /// <value> The open detector enabled query command. </value>
        protected override string OpenDetectorEnabledCommandFormat { get; set; } = "_G.dmm.opendetector={0:'1';'1';'0'}";

        /// <summary> Gets or sets the open detector enabled query command. </summary>
        /// <value> The open detector enabled query command. </value>
        protected override string OpenDetectorEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.opendetector==1)";

        #endregion

        #region  POWER LINE CYCLES 

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = "_G.dmm.nplc={0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = "_G.print(_G.dmm.nplc)";

        #endregion

        #region  RANGE 

        /// <summary> Gets or sets the Range query command. </summary>
        /// <value> The Range query command. </value>
        protected override string RangeQueryCommand { get; set; } = "_G.print(_G.dmm.range)";

        /// <summary> Gets or sets the Range command format. </summary>
        /// <value> The Range command format. </value>
        protected override string RangeCommandFormat { get; set; } = "_G.dmm.range={0}";

        #endregion

        #endregion

        #region  MEASURE 

        /// <summary> Reads a value in to the primary reading and converts it to Double. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The measured value or none if unknown. </returns>
        public override double? MeasurePrimaryReading()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return base.MeasurePrimaryReading();
        }

        /// <summary> Queries the reading and parse the reading amounts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The reading or none if unknown. </returns>
        public override double? MeasureReadingAmounts()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return base.MeasureReadingAmounts();
        }

        #endregion

    }
}
