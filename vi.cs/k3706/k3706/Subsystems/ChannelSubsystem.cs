namespace isr.VI.Tsp.K3706
{

    /// <summary> Defines a Channel Subsystem for a Keithley 3700 instrument. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-01-13 </para>
    /// </remarks>
    public class ChannelSubsystem : ChannelSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                Subsystem</see>. </param>
        public ChannelSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }
        #endregion

        #region  COMMAND SYNTAX 

        #region " CHANNELS "

        /// <summary> Gets or sets the closed channels query command. </summary>
        /// <value> The closed channels query command. </value>
        protected override string ClosedChannelsQueryCommand { get; set; } = "_G.print(_G.channel.getclose('allslots'))";

        /// <summary> Gets or sets the closed channels command format. </summary>
        /// <value> The closed channels command format. </value>
        protected override string ClosedChannelsCommandFormat { get; set; } = "_G.channel.close('{0}')";

        /// <summary> Gets or sets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        protected override string OpenChannelsCommandFormat { get; set; } = "_G.channel.open('{0}')";

        /// <summary> Gets or sets the open channels command. </summary>
        /// <value> The open channels command. </value>
        protected override string OpenChannelsCommand { get; set; } = "_G.channel.open('allslots')";

        #endregion

        #endregion

    }
}
