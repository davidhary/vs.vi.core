using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp.K3706
{

    /// <summary> A 3700 device. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public class K3706Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Initializes a new instance of the <see cref="K3706Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K3706Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected K3706Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            if ( statusSubsystem is object )
            {
                statusSubsystem.ExpectedLanguage = Pith.Ieee488.Syntax.LanguageTsp;
                My.MySettings.Default.PropertyChanged += this.HandleSettingsPropertyChanged;
            }

            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static K3706Device Create()
        {
            K3706Device device = null;
            try
            {
                device = new K3706Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static K3706Device Validated( K3706Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support " 

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            // release unmanaged-only resources.
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( K3706Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION " 

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindMultimeterSubsystem( null );
                this.ChannelSubsystem = null;
                this.BindSlotsSubsystem( null );
                this.BindDisplaySubsystem( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.ChannelSubsystem = new ChannelSubsystem( this.StatusSubsystem );
                this.BindDisplaySubsystem( new DisplaySubsystem( this.StatusSubsystem ) );
                this.BindSlotsSubsystem( new SlotsSubsystem( 6, this.StatusSubsystem ) );
                this.BindMultimeterSubsystem( new MultimeterSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " SUBSYSTEMS " 

        #region " STATUS " 

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #region " CHANNEL " 

        /// <summary> The channel subsystem. </summary>
        private ChannelSubsystem _ChannelSubsystem;

        /// <summary> Gets or sets the Channel Subsystem. </summary>
        /// <value> The Channel Subsystem. </value>
        public ChannelSubsystem ChannelSubsystem
        {
            get => this._ChannelSubsystem;

            set {
                if ( this._ChannelSubsystem is object )
                {
                    _ = this.Subsystems.Remove( this.ChannelSubsystem );
                    this._ChannelSubsystem = null;
                }

                this._ChannelSubsystem = value;
                if ( this._ChannelSubsystem is object )
                {
                    this.Subsystems.Add( this.ChannelSubsystem );
                }
            }
        }

        #endregion

        #region " DISPLAY " 

        /// <summary> Gets or sets the Display Subsystem. </summary>
        /// <value> The Display Subsystem. </value>
        public DisplaySubsystem DisplaySubsystem { get; private set; }

        /// <summary> Binds the Display subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDisplaySubsystem( DisplaySubsystem subsystem )
        {
            if ( this.DisplaySubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DisplaySubsystem );
                this.DisplaySubsystem = null;
            }

            this.DisplaySubsystem = subsystem;
            if ( this.DisplaySubsystem is object )
            {
                this.Subsystems.Add( this.DisplaySubsystem );
            }
        }

        #endregion

        #region " MULTIMETER " 

        /// <summary> Gets or sets the Multimeter Subsystem. </summary>
        /// <value> The Multimeter Subsystem. </value>
        public MultimeterSubsystem MultimeterSubsystem { get; private set; }

        /// <summary> Binds the Multimeter subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMultimeterSubsystem( MultimeterSubsystem subsystem )
        {
            if ( this.MultimeterSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MultimeterSubsystem );
                this.MultimeterSubsystem = null;
            }

            this.MultimeterSubsystem = subsystem;
            if ( this.MultimeterSubsystem is object )
            {
                this.Subsystems.Add( this.MultimeterSubsystem );
            }
        }

        /// <summary> Establishes range using the sense circuit. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="countOut">            The count out. </param>
        /// <param name="twoWireChannelList">  List of two wire channels. </param>
        /// <param name="fourWireChannelList"> List of four wire channels. </param>
        /// <param name="openLimit">           The open limit. </param>
        /// <returns> The VI.MetaStatus. </returns>
        public MetaStatus EstablishSenseRange( int countOut, string twoWireChannelList, string fourWireChannelList, double openLimit )
        {
            double rangeRetryScaleFactor = 1.5d;
            double rangeGuardFactor = 1.1d; // check on these values in the original code.
            var relayClosureTimeout = TimeSpan.FromMilliseconds( 50d );
            var result = new MetaStatus();
            countOut -= 1;
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} establishing two wire sense circuit range @'{twoWireChannelList}';. " );
            _ = this.ChannelSubsystem.ApplyClosedChannels( twoWireChannelList, relayClosureTimeout );
            if ( this.MultimeterSubsystem.AutoRangeEnabled.GetValueOrDefault( true ) )
            {
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} disabling auto range;. " );
                _ = this.MultimeterSubsystem.ApplyAutoRangeEnabled( false );
            }

            _ = this.MultimeterSubsystem.ApplyRange( openLimit );
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} range candidate set as {this.MultimeterSubsystem.Range:G6};. " );
            var measuredResistance = this.MultimeterSubsystem.MeasureReadingAmounts();
            if ( !measuredResistance.HasValue )
            {
                if ( countOut <= 0 )
                {
                    // leave result as has no value
                    result.ToggleBit( MetaStatusBit.HasValue, false );
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement failed;. " );
                }
                else
                {
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement failed--trying again;. " );
                    result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                }
            }
            else if ( measuredResistance.Value > openLimit )
            {
                if ( countOut <= 0 )
                {
                    // tag result as high
                    result.ToggleBit( MetaStatusBit.High, true );
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement high;. " );
                }
                else
                {
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement high--trying again;. " );
                    openLimit *= rangeRetryScaleFactor;
                    result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                }
            }
            else
            {
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} establishing 4-wire sense range @'{fourWireChannelList}';. " );
                _ = this.ChannelSubsystem.ApplyClosedChannels( fourWireChannelList, relayClosureTimeout );
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} 2-wire value {measuredResistance.Value:G6};. " );
                _ = this.MultimeterSubsystem.ApplyRange( rangeGuardFactor * measuredResistance.Value );
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} range set as {this.MultimeterSubsystem.Range:G6};. " );
                measuredResistance = this.MultimeterSubsystem.MeasureReadingAmounts();
                if ( !measuredResistance.HasValue )
                {
                    if ( countOut <= 0 )
                    {
                        // leave result as has no value
                        result.ToggleBit( MetaStatusBit.HasValue, false );
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement failed;. " );
                    }
                    else
                    {
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement failed--trying again;. " );
                        result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                    }
                }
                else if ( measuredResistance.Value > openLimit )
                {
                    if ( countOut <= 0 )
                    {
                        result.ToggleBit( MetaStatusBit.High, true );
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement high;. " );
                    }
                    else
                    {
                        openLimit *= rangeRetryScaleFactor;
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement high--trying again;. " );
                        result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                    }
                }
                else
                {
                    result.ToggleBit( MetaStatusBit.Pass, true );
                }
            }

            return result;
        }

        /// <summary> Establish source range. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="countOut">            The count out. </param>
        /// <param name="twoWireChannelList">  List of two wire channels. </param>
        /// <param name="fourWireChannelList"> List of four wire channels. </param>
        /// <param name="openLimit">           The open limit. </param>
        /// <returns> The VI.MetaStatus. </returns>
        public MetaStatus EstablishSourceRange( int countOut, string twoWireChannelList, string fourWireChannelList, double openLimit )
        {
            double rangeRetryScaleFactor = 1.5d;
            double rangeGuardFactor = 1.1d; // check on these values in the original code.
            var relayClosureTimeout = TimeSpan.FromMilliseconds( 50d );
            var result = new MetaStatus();
            countOut -= 1;
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} establishing 2-wire source circuit range @'{twoWireChannelList}';. " );
            _ = this.ChannelSubsystem.ApplyClosedChannels( twoWireChannelList, relayClosureTimeout );
            if ( this.MultimeterSubsystem.AutoRangeEnabled.GetValueOrDefault( true ) )
            {
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} disabling auto range;. " );
                _ = this.MultimeterSubsystem.ApplyAutoRangeEnabled( false );
            }

            _ = this.MultimeterSubsystem.ApplyRange( openLimit );
            _ = this.PublishVerbose( $"{this.ResourceNameCaption} range candidate set as {this.MultimeterSubsystem.Range:G6};. " );
            var measuredResistance = this.MultimeterSubsystem.MeasureReadingAmounts();
            if ( !measuredResistance.HasValue )
            {
                if ( countOut <= 0 )
                {
                    // leave result as has no value
                    result.ToggleBit( MetaStatusBit.HasValue, false );
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement failed;. " );
                }
                else
                {
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement failed--trying again;. " );
                    result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                }
            }
            else if ( measuredResistance.Value > openLimit )
            {
                if ( countOut <= 0 )
                {
                    // tag result as high
                    result.ToggleBit( MetaStatusBit.High, true );
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement high;. " );
                }
                else
                {
                    _ = this.PublishInfo( $"{this.ResourceNameCaption} 2-wire range measurement high--trying again;. " );
                    openLimit *= rangeRetryScaleFactor;
                    result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                }
            }
            else
            {
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} establishing 4-wire source range @'{fourWireChannelList}';. " );
                _ = this.ChannelSubsystem.ApplyClosedChannels( fourWireChannelList, relayClosureTimeout );
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} 2-wire value {measuredResistance.Value:G6};. " );
                _ = this.MultimeterSubsystem.ApplyRange( rangeGuardFactor * measuredResistance.Value );
                _ = this.PublishVerbose( $"{this.ResourceNameCaption} range set as {this.MultimeterSubsystem.Range:G6};. " );
                measuredResistance = this.MultimeterSubsystem.MeasureReadingAmounts();
                if ( !measuredResistance.HasValue )
                {
                    if ( countOut <= 0 )
                    {
                        // leave result as has no value
                        result.ToggleBit( MetaStatusBit.HasValue, false );
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement failed;. " );
                    }
                    else
                    {
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement failed--trying again;. " );
                        result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                    }
                }
                else if ( measuredResistance.Value > openLimit )
                {
                    if ( countOut <= 0 )
                    {
                        // tag result as high
                        result.ToggleBit( MetaStatusBit.High, true );
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement high;. " );
                    }
                    else
                    {
                        _ = this.PublishInfo( $"{this.ResourceNameCaption} 4-wire range measurement high--trying again;. " );
                        openLimit *= rangeRetryScaleFactor;
                        result = this.EstablishSenseRange( countOut, twoWireChannelList, fourWireChannelList, openLimit );
                    }
                }
                else
                {
                    result.ToggleBit( MetaStatusBit.Pass, true );
                }
            }

            return result;
        }

        #endregion

        #region " SLOTS " 

        /// <summary> Gets or sets the Slots Subsystem. </summary>
        /// <value> The Slots Subsystem. </value>
        public SlotsSubsystem SlotsSubsystem { get; private set; }

        /// <summary> Binds the Slots subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSlotsSubsystem( SlotsSubsystem subsystem )
        {
            if ( this.SlotsSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SlotsSubsystem );
                this.SlotsSubsystem = null;
            }

            this.SlotsSubsystem = subsystem;
            if ( this.SlotsSubsystem is object )
            {
                this.Subsystems.Add( this.SlotsSubsystem );
            }
        }

        #endregion

        #endregion

        #region  SERVICE REQUEST 

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region  MY SETTINGS 

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "K3706 Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {this.Session.MessageNotificationLevel}" );
                        break;
                    }
            }
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleSettingsPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
