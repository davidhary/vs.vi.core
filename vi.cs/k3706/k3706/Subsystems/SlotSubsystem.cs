
namespace isr.VI.Tsp.K3706
{

    /// <summary> A slot subsystem. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public class SlotSubsystem : SlotSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="slotNumber">      The slot number. </param>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="VI.Tsp.StatusSubsystemBase">message based
        ///                                session</see>. </param>
        public SlotSubsystem( int slotNumber, StatusSubsystemBase statusSubsystem ) : base( slotNumber, statusSubsystem )
        {
        }

        #endregion

    }
}
