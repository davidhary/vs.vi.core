using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Tsp.K3706
{

    /// <summary> A resistances meter device using the K3706 instrument. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-03-22 </para>
    /// </remarks>
    public class ResistancesMeterDevice : K3706Device
    {

        #region " CONSTRUCTION " 

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ResistancesMeterDevice() : base()
        {
            this.Resistors = new ChannelResistorCollection();
            this._MultimeterSenseChannel = 912;
            this._MultimeterSourceChannel = 921;
            this.SlotCapacity = 30;
            this.ResistorPrefix = "R";
        }

        /// <summary> Validated the given value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A ResistancesMeterDevice. </returns>
        public static ResistancesMeterDevice Validated( ResistancesMeterDevice device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        /// <summary> Creates a new Me. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Me. </returns>
        public new static ResistancesMeterDevice Create()
        {
            ResistancesMeterDevice device = null;
            try
            {
                device = new ResistancesMeterDevice();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        #endregion

        #region " DEVICE " 

        /// <summary>
        /// Allows the derived device to take actions after initialization is completed.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnInitialized( EventArgs e )
        {
            base.OnInitialized( e );
            if ( this.IsDeviceOpen )
            {
                _ = this.TryConfigureMeter( this.MultimeterSubsystem.PowerLineCycles.GetValueOrDefault( 1d ) );
            }

            this.SyncNotifyPropertyChanged( nameof( this.Resistors ) );
        }

        /// <summary> Checks if measurements are enabled. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> True if the measurement is enabled, false if not. </returns>
        public bool IsMeasurementEnabled()
        {
            return this.IsDeviceOpen && MultimeterFunctionModes.ResistanceFourWire == this.MultimeterSubsystem.FunctionMode.GetValueOrDefault( MultimeterFunctionModes.None ) && this.MultimeterSubsystem.PowerLineCycles.HasValue;
        }


        #endregion

        #region " SUBSYSTEMS " 

        #region  MUTLIMETER 

        /// <summary> True to enable, false to disable the measurement. </summary>
        private bool _MeasurementEnabled;

        /// <summary> Gets or sets the measurement enabled. </summary>
        /// <value> The measurement enabled. </value>
        public bool MeasurementEnabled
        {
            get => this._MeasurementEnabled;

            set {
                if ( value != this.MeasurementEnabled )
                {
                    this._MeasurementEnabled = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Number of filters. </summary>
        private int _FilterCount;

        /// <summary> Gets or sets the number of filters. </summary>
        /// <value> The number of filters. </value>
        public virtual int FilterCount
        {
            get => this._FilterCount;

            set {
                if ( value != this.FilterCount )
                {
                    this._FilterCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The filter window. </summary>
        private double _FilterWindow;

        /// <summary> Gets or sets the filter window. </summary>
        /// <value> The filter window. </value>
        public virtual double FilterWindow
        {
            get => this._FilterWindow;

            set {
                if ( value != this.FilterWindow )
                {
                    this._FilterWindow = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The power line cycles. </summary>
        private double _PowerLineCycles;

        /// <summary> Gets or sets the power line cycles. </summary>
        /// <value> The power line cycles. </value>
        public virtual double PowerLineCycles
        {
            get => this._PowerLineCycles;

            set {
                if ( value != this.PowerLineCycles )
                {
                    this._PowerLineCycles = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #endregion

        #region  MEASURE 

        /// <summary> The multimeter sense channel. </summary>
        private int _MultimeterSenseChannel;

        /// <summary> Gets or sets a list of multimeter channels. </summary>
        /// <value> A List of multimeter channels. </value>
        public int MultimeterSenseChannel
        {
            get => this._MultimeterSenseChannel;

            set {
                if ( !Equals( this.MultimeterSenseChannel, value ) )
                {
                    this._MultimeterSenseChannel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The multimeter source channel. </summary>
        private int _MultimeterSourceChannel;

        /// <summary> Gets or sets a list of multimeter channels. </summary>
        /// <value> A List of multimeter channels. </value>
        public int MultimeterSourceChannel
        {
            get => this._MultimeterSourceChannel;

            set {
                if ( !Equals( this.MultimeterSourceChannel, value ) )
                {
                    this._MultimeterSourceChannel = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The slot capacity. </summary>
        private int _SlotCapacity;

        /// <summary> Gets or sets the slot capacity. </summary>
        /// <value> The slot capacity. </value>
        public int SlotCapacity
        {
            get => this._SlotCapacity;

            set {
                if ( !Equals( this.SlotCapacity, value ) )
                {
                    this._SlotCapacity = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The resistor prefix. </summary>
        private string _ResistorPrefix;

        /// <summary> Gets or sets the resistor prefix. </summary>
        /// <value> The resistor prefix. </value>
        public string ResistorPrefix
        {
            get => this._ResistorPrefix;

            set {
                if ( !string.Equals( this.ResistorPrefix, value ) )
                {
                    this._ResistorPrefix = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// Gets the number of sets of <paramref name="setSize"/> size that fits in a slot.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setSize"> Number of elements in a set. </param>
        /// <returns> An Integer. </returns>
        public int SlotSetCapacity( int setSize )
        {
            return ( int ) Math.Round( Math.Floor( this.SlotCapacity / ( double ) setSize ) );
        }

        /// <summary> Slot net capacity. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setSize"> Number of elements in a set. </param>
        /// <returns> An Integer. </returns>
        public int SlotNetCapacity( int setSize )
        {
            return setSize * this.SlotSetCapacity( setSize );
        }

        /// <summary> Linear relay number. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setNumber">     The set number. </param>
        /// <param name="ordinalNumber"> The ordinal number within the set. </param>
        /// <param name="setSize">       Number of elements in a set. </param>
        /// <returns> An Integer. </returns>
        public static int LinearRelayNumber( int setNumber, int ordinalNumber, int setSize )
        {
            return ordinalNumber + (setNumber - 1) * setSize;
        }

        /// <summary>
        /// Get the slot number for the specified resistance set ordinal number and set size.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setNumber">     The set number. </param>
        /// <param name="ordinalNumber"> The ordinal number within the set. </param>
        /// <param name="setSize">       Number of. </param>
        /// <returns> An Integer. </returns>
        public int SlotNumber( int setNumber, int ordinalNumber, int setSize )
        {
            return 1 + ( int ) Math.Round( Math.Floor( LinearRelayNumber( setNumber, ordinalNumber, setSize ) / ( double ) this.SlotNetCapacity( setSize ) ) );
        }

        /// <summary> Relay number. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setNumber">     The set number. </param>
        /// <param name="ordinalNumber"> The ordinal number within the set. </param>
        /// <param name="setSize">       Number of elements in a set. </param>
        /// <returns> An Integer. </returns>
        public int SenseRelayNumber( int setNumber, int ordinalNumber, int setSize )
        {
            return LinearRelayNumber( setNumber, ordinalNumber, setSize ) % this.SlotNetCapacity( setSize );
        }

        /// <summary> Source relay number. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setNumber">     The set number. </param>
        /// <param name="ordinalNumber"> The ordinal number within the set. </param>
        /// <param name="setSize">       Number of elements in a set. </param>
        /// <returns> An Integer. </returns>
        public int SourceRelayNumber( int setNumber, int ordinalNumber, int setSize )
        {
            return this.SlotCapacity + this.SenseRelayNumber( setNumber, ordinalNumber, setSize );
        }

        /// <summary> Builds channel list. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="setNumber">     The set number. </param>
        /// <param name="ordinalNumber"> The ordinal number within the set. </param>
        /// <param name="setSize">       Number of elements in a set. </param>
        /// <returns> A String. </returns>
        public string BuildChannelList( int setNumber, int ordinalNumber, int setSize )
        {
            int slotBaseNumber = 1000 * this.SlotNumber( setNumber, ordinalNumber, setSize );
            var relayNumbers = new List<int>() { slotBaseNumber + this.SenseRelayNumber( setNumber, ordinalNumber, setSize ), slotBaseNumber + this.SourceRelayNumber( setNumber, ordinalNumber, setSize ), slotBaseNumber + this.MultimeterSenseChannel, slotBaseNumber + this.MultimeterSourceChannel };
            relayNumbers.Sort();
            var builder = new System.Text.StringBuilder( relayNumbers[0].ToString() );
            string delimiter = ";";
            for ( int i = 1, loopTo = relayNumbers.Count - 1; i <= loopTo; i++ )
                _ = builder.Append( $"{delimiter}{relayNumbers[i]}" );
            return builder.ToString();
        }

        /// <summary> Populates the given resistors. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="prefix">    The prefix. </param>
        /// <param name="setNumber"> The set number. </param>
        /// <param name="setSize">   Number of elements in a set. </param>
        public void Populate( string prefix, int setNumber, int setSize )
        {
            this.Resistors.Clear();
            for ( int resistorOrdinalNumber = 1, loopTo = setSize; resistorOrdinalNumber <= loopTo; resistorOrdinalNumber++ )
                this.Resistors.Add( new ChannelResistor( $"{prefix}{resistorOrdinalNumber}", this.BuildChannelList( setNumber, resistorOrdinalNumber, setSize ) ) );
        }

        /// <summary> Populates the given resistors. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resistors"> The resistors. </param>
        public void Populate( ChannelResistorCollection resistors )
        {
            this.Resistors = resistors;
        }

        /// <summary> Gets or sets the resistors. </summary>
        /// <value> The resistors. </value>
        public ChannelResistorCollection Resistors { get; private set; }

        /// <summary> Configure automatic zero once. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        public void ConfigureAutoZeroOnce()
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                _ = this.PublishVerbose( $"Configuring auto Zero once" );
                this.MultimeterSubsystem.AutoZeroOnce();
                activity = this.PublishVerbose( $"Check auto Zero enabled (false if once)" );
                var autoZero = this.MultimeterSubsystem.QueryAutoZeroEnabled();
                if ( autoZero.HasValue )
                {
                    if ( autoZero.Value )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {false} <> Actual {autoZero.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Configure automatic zero. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="autoZeroEnabled"> True to enable, false to disable the automatic zero. </param>
        public void ConfigureAutoZero( bool autoZeroEnabled )
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                activity = this.PublishVerbose( $"Configuring auto Zero {autoZeroEnabled}" );
                var autoZero = this.MultimeterSubsystem.ApplyAutoZeroEnabled( autoZeroEnabled );
                if ( autoZero.HasValue )
                {
                    if ( autoZeroEnabled != autoZero.Value )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {autoZeroEnabled} <> Actual {autoZero.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Configure function mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="expectedFunctionMode"> The expected function mode. </param>
        public void ConfigureFunctionMode( MultimeterFunctionModes expectedFunctionMode )
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                activity = this.PublishVerbose( $"Configuring function mode {expectedFunctionMode}" );
                var actualFunctionMode = this.MultimeterSubsystem.ApplyFunctionMode( expectedFunctionMode );
                if ( actualFunctionMode.HasValue )
                {
                    if ( Nullable.Equals( actualFunctionMode, expectedFunctionMode ) )
                    {
                        // changing the function mode changes range, auto delay mode and open detector enabled. 
                        _ = this.MultimeterSubsystem.QueryAperture();
                        _ = this.MultimeterSubsystem.QueryRange();
                        _ = this.MultimeterSubsystem.QueryAutoDelayEnabled();
                        _ = this.MultimeterSubsystem.QueryOpenDetectorEnabled();
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedFunctionMode} <> Actual {actualFunctionMode.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Configure power line cycles. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="powerLineCycles"> The power line cycles. </param>
        public void ConfigurePowerLineCycles( double powerLineCycles )
        {
            if ( powerLineCycles <= 0d )
                throw new ArgumentException( $"Value {powerLineCycles} must be greater than zero", nameof( powerLineCycles ) );
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                activity = this.PublishVerbose( $"Configuring power line cycles {powerLineCycles}" );
                var actualPowerLineCycles = this.MultimeterSubsystem.ApplyPowerLineCycles( powerLineCycles );
                if ( actualPowerLineCycles.HasValue )
                {
                    if ( powerLineCycles != actualPowerLineCycles.Value )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {powerLineCycles} <> Actual {actualPowerLineCycles.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Configure automatic range. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="autoRangeEnabled"> True to enable, false to disable the automatic range. </param>
        public void ConfigureAutoRange( bool autoRangeEnabled )
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                activity = this.PublishVerbose( $"Configuring auto range {autoRangeEnabled}" );
                var autoRange = this.MultimeterSubsystem.ApplyAutoRangeEnabled( autoRangeEnabled );
                if ( autoRange.HasValue )
                {
                    if ( autoRangeEnabled != autoRange.Value )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {autoRangeEnabled} <> Actual {autoRange.Value}" );
                    }
                    // read the range
                    _ = this.MultimeterSubsystem.QueryRange();
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Configure range. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentException">        Thrown when one or more arguments have
        ///                                             unsupported or illegal values. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="range"> The range. </param>
        public void ConfigureRange( double range )
        {
            if ( range <= 0d )
                throw new ArgumentException( $"Value {range} must be greater than zero", nameof( range ) );
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                this.ConfigureAutoRange( false );
                activity = this.PublishVerbose( $"Configuring range {range}" );
                var actualRange = this.MultimeterSubsystem.ApplyRange( range );
                if ( actualRange.HasValue )
                {
                    if ( Math.Abs( actualRange.Value / range - 1d ) > 0.01d )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {range} <> Actual {actualRange.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Configure moving average filter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="filterCount">  Number of filters. </param>
        /// <param name="filterWindow"> The filter window. </param>
        public void ConfigureMovingAverageFilter( int filterCount, double filterWindow )
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                bool expectedFilterEnabled = filterCount > 0;
                bool? actualFilterEnabled;
                if ( expectedFilterEnabled )
                {

                    // using repeat filter returns a measurement after the filter takes the expected number of in-Window values.
                    bool expectedMovingAverageFilterEnabled = false;
                    activity = this.PublishVerbose( $"{(expectedMovingAverageFilterEnabled ? "Enabling" : "Disabling")} moving average filter" );
                    var actualMovingAverageFilterEnabled = this.MultimeterSubsystem.ApplyMovingAverageFilterEnabled( expectedMovingAverageFilterEnabled );
                    if ( actualMovingAverageFilterEnabled.HasValue )
                    {
                        if ( actualMovingAverageFilterEnabled.Value != expectedMovingAverageFilterEnabled )
                        {
                            throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedMovingAverageFilterEnabled} <> Actual {actualMovingAverageFilterEnabled.Value}" );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                    }

                    activity = this.PublishVerbose( $"Setting filter count {filterCount}" );
                    var actualFilterCount = this.MultimeterSubsystem.ApplyFilterCount( filterCount );
                    if ( actualFilterCount.HasValue )
                    {
                        if ( actualFilterCount.Value != filterCount )
                        {
                            throw new Core.OperationFailedException( $"Failed {activity} Expected {filterCount} <> Actual {actualFilterCount.Value}" );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                    }

                    activity = this.PublishVerbose( $"Setting filter window {filterWindow}" );
                    var actualFilterWindow = this.MultimeterSubsystem.ApplyFilterWindow( filterWindow );
                    if ( actualFilterWindow.HasValue )
                    {
                        if ( Math.Abs( actualFilterWindow.Value / filterWindow - 1d ) > 0.01d )
                        {
                            throw new Core.OperationFailedException( $"Failed {activity} Expected {filterWindow} <> Actual {actualFilterWindow.Value}" );
                        }
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                    }

                    activity = this.PublishVerbose( $"Enabling filter" );
                    actualFilterEnabled = this.MultimeterSubsystem.ApplyFilterEnabled( expectedFilterEnabled );
                }
                else
                {
                    activity = this.PublishVerbose( $"Disabling filter" );
                    actualFilterEnabled = this.MultimeterSubsystem.ApplyFilterEnabled( expectedFilterEnabled );
                }

                if ( actualFilterEnabled.HasValue )
                {
                    if ( actualFilterEnabled.Value != expectedFilterEnabled )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedFilterEnabled} <> Actual {actualFilterEnabled.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--value not set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Measure value into the reading amounts. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <returns> A Double. </returns>
        public double MeasureReadingAmounts()
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            double result;
            if ( this.IsDeviceOpen )
            {
                activity = this.PublishVerbose( $"measuring {this.MultimeterSubsystem.FunctionMode}" );
                if ( this.MultimeterSubsystem.MeasureReadingAmounts().HasValue )
                {
                    result = this.MultimeterSubsystem.PrimaryReadingValue.Value;
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}; driver returned nothing" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }

            return result;
        }

        /// <summary> Estimates the lower bound on voltage measurement time. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A TimeSpan. </returns>
        public TimeSpan EstimateVoltageMeasurementTime()
        {
            double aperture = this.PowerLineCycles / this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d );
            double timeSeconds;
            if ( this.FilterCount > 0 && this.FilterWindow > 0d )
            {
                // if auto zero once is included the time maybe too long
                timeSeconds = aperture * this.FilterCount;
            }
            else
            {
                // assumes auto zero
                timeSeconds = aperture * 2d;
            }

            return TimeSpan.FromSeconds( timeSeconds );
        }

        /// <summary> Configure measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>" )]
        public void ConfigureMeasurement()
        {
            string activity = $"Checking {this.ResourceNameCaption} is open";
            if ( this.IsDeviceOpen )
            {
                if ( !Nullable.Equals( MultimeterFunctionModes.ResistanceFourWire, this.MultimeterSubsystem.FunctionMode ) )
                {
                    activity = $"Configuring function {MultimeterFunctionModes.ResistanceFourWire}";
                    this.ConfigureFunctionMode( MultimeterFunctionModes.ResistanceFourWire );
                }

                if ( !Nullable.Equals( this.PowerLineCycles, this.MultimeterSubsystem.PowerLineCycles ) )
                {
                    activity = $"Configuring power line cycles {this.PowerLineCycles}";
                    this.ConfigurePowerLineCycles( this.PowerLineCycles );
                }

                if ( Nullable.Equals( true, this.MultimeterSubsystem.AutoZeroEnabled ) )
                {
                    activity = $"Setting auto zero once";
                    this.ConfigureAutoZeroOnce();
                }

                if ( !Nullable.Equals( false, this.MultimeterSubsystem.AutoRangeEnabled ) )
                {
                    activity = $"Configuring auto range";
                    this.ConfigureAutoRange( true );
                }

                if ( this.FilterCount > 0 )
                {
                    if ( !Nullable.Equals( true, this.MultimeterSubsystem.FilterEnabled ) || !Nullable.Equals( this.FilterCount, this.MultimeterSubsystem.FilterCount ) || !Nullable.Equals( true, this.MultimeterSubsystem.MovingAverageFilterEnabled ) || !Nullable.Equals( this.FilterWindow, this.MultimeterSubsystem.FilterWindow ) )
                    {
                        activity = $"Enabling filter";
                        this.ConfigureMovingAverageFilter( this.FilterCount, this.FilterWindow );
                    }
                }
                else if ( !Nullable.Equals( false, this.MultimeterSubsystem.FilterEnabled ) )
                {
                    activity = $"Disabling filter";
                    this.ConfigureMovingAverageFilter( this.FilterCount, this.FilterWindow );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Attempts to configure resistance measurement from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryConfigureMeasurement()
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            try
            {
                activity = this.PublishVerbose( $"Configuring measurement" );
                this.ConfigureMeasurement();
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }

            return result;
        }

        /// <summary> Configure meter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="powerLineCycles"> The power line cycles. </param>
        public void ConfigureMeter( double powerLineCycles )
        {
            string activity = $"Checking {this.Session.ResourceNameCaption} Is open";
            if ( this.IsDeviceOpen )
            {
                activity = $"Configuring function mode {MultimeterFunctionModes.ResistanceFourWire}";
                var expectedMeasureFunction = MultimeterFunctionModes.ResistanceFourWire;
                var measureFunction = this.MultimeterSubsystem.ApplyFunctionMode( expectedMeasureFunction );
                if ( measureFunction.HasValue )
                {
                    if ( Nullable.Equals( measureFunction, expectedMeasureFunction ) )
                    {
                        // changing the function mode changes range, auto delay mode and open detector enabled. 
                        _ = this.MultimeterSubsystem.QueryAperture();
                        _ = this.MultimeterSubsystem.QueryRange();
                        _ = this.MultimeterSubsystem.QueryAutoDelayEnabled();
                        _ = this.MultimeterSubsystem.QueryOpenDetectorEnabled();
                    }
                    else
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedMeasureFunction} <> Actual {measureFunction.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--no value set" );
                }

                activity = $"Configuring power line cycles {powerLineCycles}";
                var actualPowerLineCycles = this.MultimeterSubsystem.ApplyPowerLineCycles( powerLineCycles );
                if ( actualPowerLineCycles.HasValue )
                {
                    if ( powerLineCycles != actualPowerLineCycles.Value )
                    {
                        throw new Core.OperationFailedException( $"Failed {activity} Expected {powerLineCycles} <> Actual {actualPowerLineCycles.Value}" );
                    }
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}--no value set" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device Is Not open" );
            }
        }

        /// <summary> Attempts to configure meter from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="powerLineCycles"> The power line cycles. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryConfigureMeter( double powerLineCycles )
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = $"Configuring resistances meter {this.Session.ResourceNameCaption}";
            try
            {
                this.ConfigureMeter( powerLineCycles );
            }
            catch ( Exception ex )
            {
                result = (false, this.PublishException( activity, ex ));
            }

            return result;
        }

        /// <summary> Try measure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="resistor"> The resistor. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryMeasure( ChannelResistor resistor )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( resistor is null )
                throw new ArgumentNullException( nameof( resistor ) );
            string activity = string.Empty;
            try
            {
                activity = this.PublishVerbose( $"Configuring measurement" );
                this.ConfigureMeasurement();
                activity = $"{resistor.Title} opening channels";
                string expectedChannelList = string.Empty;
                string actualChannelList = this.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
                if ( (expectedChannelList ?? "") != (actualChannelList ?? "") )
                {
                    throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedChannelList} <> Actual {actualChannelList}" );
                }

                activity = $"{resistor.Title} closing {resistor.ChannelList}";
                expectedChannelList = resistor.ChannelList;
                actualChannelList = this.ChannelSubsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
                if ( (expectedChannelList ?? "") != (actualChannelList ?? "") )
                {
                    throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedChannelList} <> Actual {actualChannelList}" );
                }

                activity = $"measuring {resistor.Title}";
                if ( this.MultimeterSubsystem.MeasureReadingAmounts().HasValue )
                {
                    resistor.Resistance = this.MultimeterSubsystem.PrimaryReading.Value.GetValueOrDefault( -1 );
                    resistor.Status = this.MultimeterSubsystem.PrimaryReading.MetaStatus;
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}; driver returned nothing" );
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }

            return result;
        }

        /// <summary> Try assign measurement. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="resistor"> The resistor. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryAssignMeasurement( ChannelResistor resistor )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( resistor is null )
                throw new ArgumentNullException( nameof( resistor ) );
            string activity = string.Empty;
            try
            {
                activity = this.PublishVerbose( $"assigning resistance" );
                if ( this.MultimeterSubsystem.PrimaryReading.Value.HasValue )
                {
                    resistor.Resistance = this.MultimeterSubsystem.PrimaryReading.Value.GetValueOrDefault( -1 );
                    resistor.Status = this.MultimeterSubsystem.PrimaryReading.MetaStatus;
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}; driver returned nothing" );
                }
            }
            catch ( Exception ex )
            {
                result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
            }

            return result;
        }

        /// <summary> Measure resistance. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        /// <param name="resistor"> The resistor. </param>
        public void MeasureResistance( ChannelResistor resistor )
        {
            if ( resistor is null )
                throw new ArgumentNullException( nameof( resistor ) );
            string activity = $"measuring {resistor.Title}";
            if ( this.IsDeviceOpen )
            {
                activity = $"{resistor.Title} opening channels";
                string expectedChannelList = string.Empty;
                string actualChannelList = this.ChannelSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 2d ) );
                if ( (expectedChannelList ?? "") != (actualChannelList ?? "") )
                {
                    throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedChannelList} <> Actual {actualChannelList}" );
                }

                activity = $"{resistor.Title} closing {resistor.ChannelList}";
                expectedChannelList = resistor.ChannelList;
                actualChannelList = this.ChannelSubsystem.ApplyClosedChannels( expectedChannelList, TimeSpan.FromSeconds( 2d ) );
                if ( (expectedChannelList ?? "") != (actualChannelList ?? "") )
                {
                    throw new Core.OperationFailedException( $"Failed {activity} Expected {expectedChannelList} <> Actual {actualChannelList}" );
                }

                activity = $"measuring {resistor.Title}";
                if ( this.MultimeterSubsystem.MeasureReadingAmounts().HasValue )
                {
                    resistor.Resistance = this.MultimeterSubsystem.PrimaryReading.Value.GetValueOrDefault( -1 );
                    resistor.Status = this.MultimeterSubsystem.PrimaryReading.MetaStatus;
                }
                else
                {
                    throw new Core.OperationFailedException( $"Failed {activity}; driver returned nothing" );
                }
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Attempts to measure resistance from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="resistor"> The resistor. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryMeasureResistance( ChannelResistor resistor )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( resistor is null )
                throw new ArgumentNullException( nameof( resistor ) );
            string activity = $"measuring {resistor.Title}";
            try
            {
                this.MeasureResistance( resistor );
            }
            catch ( Exception ex )
            {
                result = (false, this.PublishException( activity, ex ));
            }

            return result;
        }

        /// <summary> Measure resistors. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
        public void MeasureResistors()
        {
            string activity = $"Measuring resistors at {this.Session.ResourceNameCaption}";
            if ( this.IsDeviceOpen )
            {
                foreach ( ChannelResistor resistor in this.Resistors )
                {
                    activity = $"Measuring {resistor.Title} at {this.Session.ResourceNameCaption}";
                    this.MeasureResistance( resistor );
                }

                this.SyncNotifyPropertyChanged( nameof( this.Resistors ) );
            }
            else
            {
                throw new Core.OperationFailedException( $"Failed {activity}; VISA session to this device is not open" );
            }
        }

        /// <summary> Attempts to measure resistors from the given data. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public (bool Success, string Details) TryMeasureResistors()
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = $"Measuring resistors at {this.Session.ResourceNameCaption}";
            try
            {
                this.MeasureResistors();
            }
            catch ( Exception ex )
            {
                result = (false, this.PublishException( activity, ex ));
            }

            return result;
        }

        #endregion

        #region  TALKER 

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
