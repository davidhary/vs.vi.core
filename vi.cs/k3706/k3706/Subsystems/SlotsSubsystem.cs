
using System.Collections.Generic;

namespace isr.VI.Tsp.K3706
{

    /// <summary> The slots subsystem. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public class SlotsSubsystem : SlotsSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP " 

        /// <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="maxSlotCount">    A reference to a
        ///                                <see cref="VI.Tsp.StatusSubsystemBase">message based
        ///                                session</see>. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SlotsSubsystem( int maxSlotCount, StatusSubsystemBase statusSubsystem ) : base( maxSlotCount, statusSubsystem )
        {
            this.SlotList = new List<SlotSubsystem>();
            for ( int i = 1, loopTo = this.MaximumSlotCount; i <= loopTo; i++ )
            {
                var s = new SlotSubsystem( i, statusSubsystem );
                this.SlotList.Add( s );
            }
        }

        #endregion

        #region  I PRESETTABLE 

        /// <summary> Gets or sets a list of slots. </summary>
        /// <value> A list of slots. </value>
        public IList<SlotSubsystem> SlotList { get; private set; }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            foreach ( SlotSubsystemBase s in this.SlotList )
                this.Slots.Add( s );
            base.InitKnownState();
        }

        #endregion


    }
}
