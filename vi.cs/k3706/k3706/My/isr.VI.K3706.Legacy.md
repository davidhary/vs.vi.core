## ISR VI K3706<sub>&trade;</sub>: Keithley 3700 Meter/Scanner VI Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*6.1.6939 2018-12-31*  
Adds device and device view implementing MVVM.

*4.1.6440 2017-08-19*  
Uses modified trace message and logger.

*4.0.6384 2017-06-24*  
Defaults to UTC time.

*4.0.6163 2016-11-15*  
Moving Window filter: Moves setting the measurement
started property into the task message. Fixes the code for stopping a
running task.

*4.0.6103 2016-09-17*  
Moving Window: Adds support for using task parallel.

*4.0.5907 2016-03-04*  
Disables controls when opening the device panel.

*4.0.5889 2016-02-15*  
Adds slot and slots and support for interlock.

*4.0.5882 2016-02-08*  
Adds multimeter open detector and auto delay
functionality.

*4.0.5873 2016-01-30*  
Adds the moving average control.

*4.0.5838 2015-12-28 Created from the scanner/meter library.

\(C\) 2013 Integrated Scientific Resources, Inc. All rights reserved.\
### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
