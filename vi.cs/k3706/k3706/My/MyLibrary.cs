﻿
namespace isr.VI.Tsp.K3706.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.K3700;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI K3706 Meter Scanner Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "K3706 Meter Scanner Virtual Instrument Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.K3706";
    }
}