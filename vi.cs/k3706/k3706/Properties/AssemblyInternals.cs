﻿
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.VI.Tsp2.K7500Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.K3706Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.Cinco.K3706Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Ohmni.Morphe.K3706Tests,PublicKey=" + isr.VI.My.SolutionInfo.SharedPublicKey )]
[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( "isr.Verrzzano.K3706Tests,PublicKey=" + isr.VI.My.SolutionInfo.StrainPublicKey )]
