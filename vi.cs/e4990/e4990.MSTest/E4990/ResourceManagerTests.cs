using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.E4990.MSTest
{

    /// <summary> E4990 Device resource only unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "e4990" )]
    public class ResourceManagerTests
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( E4990Properties.E4990ResourceInfo.Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region" VISA RESOURCE TESTS "

        /// <summary> (Unit Test Method) visa resource manager should include resource. </summary>
        /// <remarks> Finds the resource using the session factory resources manager. </remarks>
        [TestMethod()]
        public void VisaResourceManagerShouldIncludeResource()
        {
            VI.DeviceTests.DeviceManager.AssertVisaResourceManagerShouldIncludeResource( E4990Properties.E4990ResourceInfo );
        }

        /// <summary> (Unit Test Method) visa session base should find resource. </summary>
        /// <remarks> Finds the resource using the device class. </remarks>
        [TestMethod()]
        public void VisaSessionBaseShouldFindResource()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = E4990Device.Create();
            VI.DeviceTests.DeviceManager.AssertVisaSessionBaseShouldFindResource( device, E4990Properties.E4990ResourceInfo );
        }

        #endregion

        #region" TRACE MESSAGES TESTS "

        /// <summary> (Unit Test Method) device trace message should be queued. </summary>
        /// <remarks> Checks if the device adds a trace message to a listener. </remarks>
        [TestMethod()]
        public void DeviceTraceMessageShouldBeQueued()
        {
            using var device = E4990Device.Create();
            VI.DeviceTests.DeviceManager.AssertDeviceTraceMessageShouldBeQueued( TestInfo, device );
        }

        #endregion

    }
}
