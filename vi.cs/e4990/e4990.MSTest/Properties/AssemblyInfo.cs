﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "E4990 VI Tests" )]
[assembly: AssemblyDescription( "E4990 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.E4990.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
