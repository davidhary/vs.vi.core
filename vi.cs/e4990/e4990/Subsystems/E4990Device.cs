using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990
{

    /// <summary> Implements a E4990 Impedance Meter device. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-10, 3.0.5001. </para>
    /// </remarks>
    public class E4990Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="E4990Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public E4990Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected E4990Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            My.MySettings.Default.PropertyChanged += this.MySettings_PropertyChanged;
            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static E4990Device Create()
        {
            E4990Device device = null;
            try
            {
                device = new E4990Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static E4990Device Validated( E4990Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( E4990Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( null );
                this.BindCalculateChannelSubsystem( null );
                this.BindCompensateOpenSubsystem( null );
                this.BindCompensateShortSubsystem( null );
                this.BindCompensateLoadSubsystem( null );
                this.BindChannelMarkerSubsystem( null );
                this.BindPrimaryChannelTraceSubsystem( null );
                this.BindSecondaryChannelTraceSubsystem( null );
                this.BindChannelTriggerSubsystem( null );
                this.BindDisplaySubsystem( null );
                this.BindSenseChannelSubsystem( null );
                this.BindSourceChannelSubsystem( null );
                this.BindTriggerSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                this.BindCalculateChannelSubsystem( new CalculateChannelSubsystem( 1, this.StatusSubsystem ) );
                this.BindCompensateOpenSubsystem( new CompensateChannelSubsystem( CompensationTypes.OpenCircuit, 1, this.StatusSubsystem ) );
                this.BindCompensateShortSubsystem( new CompensateChannelSubsystem( CompensationTypes.ShortCircuit, 1, this.StatusSubsystem ) );
                this.BindCompensateLoadSubsystem( new CompensateChannelSubsystem( CompensationTypes.Load, 1, this.StatusSubsystem ) );
                this.BindChannelMarkerSubsystem( new ChannelMarkerSubsystem( 1, 1, this.StatusSubsystem ) );
                this.BindPrimaryChannelTraceSubsystem( new ChannelTraceSubsystem( 1, 1, this.StatusSubsystem ) );
                this.BindSecondaryChannelTraceSubsystem( new ChannelTraceSubsystem( 2, 1, this.StatusSubsystem ) );
                this.BindChannelTriggerSubsystem( new ChannelTriggerSubsystem( 1, this.StatusSubsystem ) );
                this.BindDisplaySubsystem( new DisplaySubsystem( this.StatusSubsystem ) );
                this.BindSenseChannelSubsystem( new SenseChannelSubsystem( 1, this.StatusSubsystem ) );
                this.BindSourceChannelSubsystem( new SourceChannelSubsystem( 1, this.StatusSubsystem ) );
                this.BindTriggerSubsystem( new TriggerSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Gets or sets the CalculateChannel Subsystem. </summary>
        /// <value> The CalculateChannel Subsystem. </value>
        public CalculateChannelSubsystem CalculateChannelSubsystem { get; private set; }

        /// <summary> Binds the CalculateChannel subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindCalculateChannelSubsystem( CalculateChannelSubsystem subsystem )
        {
            if ( this.CalculateChannelSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.CalculateChannelSubsystem );
                this.CalculateChannelSubsystem = null;
            }

            this.CalculateChannelSubsystem = subsystem;
            if ( this.CalculateChannelSubsystem is object )
            {
                this.Subsystems.Add( this.CalculateChannelSubsystem );
            }
        }

        /// <summary> Gets or sets the ChannelMarker Subsystem. </summary>
        /// <value> The ChannelMarker Subsystem. </value>
        public ChannelMarkerSubsystem ChannelMarkerSubsystem { get; private set; }

        /// <summary> Binds the ChannelMarker subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindChannelMarkerSubsystem( ChannelMarkerSubsystem subsystem )
        {
            if ( this.ChannelMarkerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.ChannelMarkerSubsystem );
                this.ChannelMarkerSubsystem = null;
            }

            this.ChannelMarkerSubsystem = subsystem;
            if ( this.ChannelMarkerSubsystem is object )
            {
                this.Subsystems.Add( this.ChannelMarkerSubsystem );
            }
        }

        /// <summary> Gets or sets the Primary Channel Trace Subsystem. </summary>
        /// <value> The Primary Channel Trace Subsystem. </value>
        public ChannelTraceSubsystem PrimaryChannelTraceSubsystem { get; private set; }

        /// <summary> Binds the Primary Channel Trace subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindPrimaryChannelTraceSubsystem( ChannelTraceSubsystem subsystem )
        {
            if ( this.PrimaryChannelTraceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.PrimaryChannelTraceSubsystem );
                this.PrimaryChannelTraceSubsystem = null;
            }

            this.PrimaryChannelTraceSubsystem = subsystem;
            if ( this.PrimaryChannelTraceSubsystem is object )
            {
                this.Subsystems.Add( this.PrimaryChannelTraceSubsystem );
            }
        }

        /// <summary> Gets or sets the Secondary Channel Trace Subsystem. </summary>
        /// <value> The Secondary Channel Trace Subsystem. </value>
        public ChannelTraceSubsystem SecondaryChannelTraceSubsystem { get; private set; }

        /// <summary> Binds the Secondary Channel Trace subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSecondaryChannelTraceSubsystem( ChannelTraceSubsystem subsystem )
        {
            if ( this.SecondaryChannelTraceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SecondaryChannelTraceSubsystem );
                this.SecondaryChannelTraceSubsystem = null;
            }

            this.SecondaryChannelTraceSubsystem = subsystem;
            if ( this.SecondaryChannelTraceSubsystem is object )
            {
                this.Subsystems.Add( this.SecondaryChannelTraceSubsystem );
            }
        }

        /// <summary> Gets or sets the ChannelTrigger Subsystem. </summary>
        /// <value> The ChannelTrigger Subsystem. </value>
        public ChannelTriggerSubsystem ChannelTriggerSubsystem { get; private set; }

        /// <summary> Binds the ChannelTrigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindChannelTriggerSubsystem( ChannelTriggerSubsystem subsystem )
        {
            if ( this.ChannelTriggerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.ChannelTriggerSubsystem );
                this.ChannelTriggerSubsystem = null;
            }

            this.ChannelTriggerSubsystem = subsystem;
            if ( this.ChannelTriggerSubsystem is object )
            {
                this.Subsystems.Add( this.ChannelTriggerSubsystem );
            }
        }

        /// <summary> Gets or sets the Compensate Open Subsystem. </summary>
        /// <value> The Compensate Open Subsystem. </value>
        public CompensateChannelSubsystem CompensateOpenSubsystem { get; private set; }

        /// <summary> Binds the Compensate Open subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindCompensateOpenSubsystem( CompensateChannelSubsystem subsystem )
        {
            if ( this.CompensateOpenSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.CompensateOpenSubsystem );
                this.CompensateOpenSubsystem = null;
            }

            this.CompensateOpenSubsystem = subsystem;
            if ( this.CompensateOpenSubsystem is object )
            {
                this.Subsystems.Add( this.CompensateOpenSubsystem );
            }
        }

        /// <summary> Gets or sets the Compensate Short Subsystem. </summary>
        /// <value> The Compensate Short Subsystem. </value>
        public CompensateChannelSubsystem CompensateShortSubsystem { get; private set; }

        /// <summary> Binds the Compensate Short subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindCompensateShortSubsystem( CompensateChannelSubsystem subsystem )
        {
            if ( this.CompensateShortSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.CompensateShortSubsystem );
                this.CompensateShortSubsystem = null;
            }

            this.CompensateShortSubsystem = subsystem;
            if ( this.CompensateShortSubsystem is object )
            {
                this.Subsystems.Add( this.CompensateShortSubsystem );
            }
        }

        /// <summary> Gets or sets the Compensate Load Subsystem. </summary>
        /// <value> The Compensate Load Subsystem. </value>
        public CompensateChannelSubsystem CompensateLoadSubsystem { get; private set; }

        /// <summary> Binds the Compensate Load subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindCompensateLoadSubsystem( CompensateChannelSubsystem subsystem )
        {
            if ( this.CompensateLoadSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.CompensateLoadSubsystem );
                this.CompensateLoadSubsystem = null;
            }

            this.CompensateLoadSubsystem = subsystem;
            if ( this.CompensateLoadSubsystem is object )
            {
                this.Subsystems.Add( this.CompensateLoadSubsystem );
            }
        }

        /// <summary> Gets or sets the Display Subsystem. </summary>
        /// <value> The Display Subsystem. </value>
        public DisplaySubsystem DisplaySubsystem { get; private set; }

        /// <summary> Binds the Display subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDisplaySubsystem( DisplaySubsystem subsystem )
        {
            if ( this.DisplaySubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DisplaySubsystem );
                this.DisplaySubsystem = null;
            }

            this.DisplaySubsystem = subsystem;
            if ( this.DisplaySubsystem is object )
            {
                this.Subsystems.Add( this.DisplaySubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Channel Subsystem. </summary>
        /// <value> The Sense Channel Subsystem. </value>
        public SenseChannelSubsystem SenseChannelSubsystem { get; private set; }

        /// <summary> Binds the Sense Channel subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseChannelSubsystem( SenseChannelSubsystem subsystem )
        {
            if ( this.SenseChannelSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseChannelSubsystem );
                this.SenseChannelSubsystem = null;
            }

            this.SenseChannelSubsystem = subsystem;
            if ( this.SenseChannelSubsystem is object )
            {
                this.Subsystems.Add( this.SenseChannelSubsystem );
            }
        }

        /// <summary> Gets or sets the Source Channel Subsystem. </summary>
        /// <value> The Source Channel Subsystem. </value>
        public SourceChannelSubsystem SourceChannelSubsystem { get; private set; }

        /// <summary> Binds the Source Channel subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSourceChannelSubsystem( SourceChannelSubsystem subsystem )
        {
            if ( this.SourceChannelSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SourceChannelSubsystem );
                this.SourceChannelSubsystem = null;
            }

            this.SourceChannelSubsystem = subsystem;
            if ( this.SourceChannelSubsystem is object )
            {
                this.Subsystems.Add( this.SourceChannelSubsystem );
            }
        }

        /// <summary> Gets or sets the Trigger Subsystem. </summary>
        /// <value> The Trigger Subsystem. </value>
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Binds the Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTriggerSubsystem( TriggerSubsystem subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( this.TriggerSubsystem is object )
            {
                this.Subsystems.Add( this.TriggerSubsystem );
            }
        }

        #region " COMPENSATION "

        /// <summary> Clears the compensations. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void ClearCompensations()
        {

            // clear the calibration parameters
            this.SenseChannelSubsystem.ClearCompensations();
            _ = this.Session.QueryOperationCompleted();
            this.CompensateOpenSubsystem.ClearMeasurements();
            _ = this.Session.QueryOperationCompleted();
            this.CompensateShortSubsystem.ClearMeasurements();
            _ = this.Session.QueryOperationCompleted();
            this.CompensateLoadSubsystem.ClearMeasurements();
            _ = this.Session.QueryOperationCompleted();
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "E4990 Settings Editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ReadDelay ):
                    {
                        this.Session.ReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.ReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = TimeSpan.FromMilliseconds( ( double ) sender.StatusReadDelay );
                        break;
                    }

                case nameof( My.MySettings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime;
                        break;
                    }
            }
        }

        /// <summary> My settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MySettings_PropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
