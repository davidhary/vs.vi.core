using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990
{

    /// <summary> Defines a SCPI Channel Trace Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class ChannelTraceSubsystem : ChannelTraceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="traceNumber">     The Trace number. </param>
        /// <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public ChannelTraceSubsystem( int traceNumber, int channelNumber, StatusSubsystemBase statusSubsystem ) : base( traceNumber, channelNumber, statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.SupportedParameters = ( TraceParameters ) Conversions.ToInteger( -1 + (( int ) TraceParameters.ComplexAdmittance << 1) );
            this.Parameter = this.TraceNumber == 1 ? TraceParameters.AbsoluteImpedance : TraceParameters.ImpedancePhase;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO SCALE "

        /// <summary> Gets the auto scale command. </summary>
        /// <remarks> SCPI: ":DISP:WIND{0}:TRAC{1}:Y:AUTO". </remarks>
        /// <value> The auto scale command. </value>
        protected override string AutoScaleCommand { get; set; } = ":DISP:WIND{0}:TRAC{1}:Y:AUTO";

        #endregion

        #region " PARAMETER "

        /// <summary> Gets the trace parameter command format. </summary>
        /// <value> The trace parameter command format. </value>
        protected override string ParameterCommandFormat { get; set; } = ":CALC{0}:PAR{1}:DEF {{0}}";

        /// <summary> Gets the trace parameter query command. </summary>
        /// <value> The trace parameter query command. </value>
        protected override string ParameterQueryCommand { get; set; } = ":CALC{0}:PAR{1}:DEF?";

        #endregion

        #region " SELECT "

        /// <summary> Gets the Select command. </summary>
        /// <remarks> SCPI: ":CALC{0}:PAR{1}:SEL". </remarks>
        /// <value> The Select command. </value>
        protected override string SelectCommand { get; set; } = ":CALC{0}:PAR{1}:SEL";

        #endregion

        #endregion

    }
}
