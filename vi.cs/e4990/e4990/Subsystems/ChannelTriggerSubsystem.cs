namespace isr.VI.E4990
{

    /// <summary> Defines a SCPI Channel Trigger Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class ChannelTriggerSubsystem : ChannelTriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public ChannelTriggerSubsystem( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( channelNumber, statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.ContinuousEnabled = false;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " INITIATE "

        /// <summary> Gets the initiate command. </summary>
        /// <remarks> SCPI: ":INIT{0}:IMM". </remarks>
        /// <value> The initiate command. </value>
        protected override string InitiateCommand { get; set; } = ":INIT{0}:IMM";

        /// <summary> Gets the continuous initiation enabled query command. </summary>
        /// <value> The continuous initiation enabled query command. </value>
        protected override string ContinuousEnabledQueryCommand { get; set; } = ":INIT{0}:CONT?";

        /// <summary> Gets the continuous initiation enabled command Format. </summary>
        /// <value> The continuous initiation enabled query command. </value>
        protected override string ContinuousEnabledCommandFormat { get; set; } = ":INIT{0}:CONT {1:1;1;0}";

        #endregion

        #endregion

    }
}
