using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.VI.E4990
{

    /// <summary> Information about the version of a Keithley 2700 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks>
        /// Agilent\sTechnologies,E4990A,MY54100800,A.02.12\n<para>
        /// where; MY54100800 Is the serial number</para><para>
        /// A Is the firmware revision top level revision</para><para>
        /// 02.12 Is the firmware revision</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument <see cref="VersionInfoBase.FirmwareRevisionElements">board
        ///                         revisions</see>
        ///                         e.g., <c>xxxxx,/yyyyy/zzzzz</c> for the digital and display boards. 
        /// </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '.' ) );

                // Rev: A.02.12
                if ( revSections.Any() )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Digital.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Any() )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Display.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Any() )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.LedDisplay.ToString(), revSections.Dequeue().Trim() );
            }
        }
    }
}
