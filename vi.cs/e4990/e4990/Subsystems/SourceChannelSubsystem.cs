namespace isr.VI.E4990
{

    /// <summary> Defines a SCPI Source Channel Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class SourceChannelSubsystem : SourceChannelSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="channelNumber">   The channel number. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SourceChannelSubsystem( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( channelNumber, statusSubsystem )
        {
            this.SupportedFunctionModes = SourceFunctionModes.Current | SourceFunctionModes.Voltage;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.FunctionMode = SourceFunctionModes.Voltage;
            this.Level = 0.5d; // volt RMS
            this.SupportedFunctionModes = SourceFunctionModes.Current | SourceFunctionModes.Voltage;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " FUNCTION MODE "

        /// <summary> Gets the function mode query command. </summary>
        /// <value> The function mode query command, e.g., :SOUR:FUNC? </value>
        protected override string FunctionModeQueryCommand
        {
            get => $":SOUR{this.ChannelNumber}:MODE?";

            set {
            }
        }

        /// <summary> Gets the function mode command. </summary>
        /// <value> The function mode command, e.g., :SOUR:FUNC {0}. </value>
        protected override string FunctionModeCommandFormat
        {
            get => $":SOUR{this.ChannelNumber}:MODE {{0}}";

            set {
            }
        }

        #endregion

        #region " LEVEL "

        /// <summary> Gets the Level command format. </summary>
        /// <value> The Level command format. </value>
        protected override string LevelCommandFormat
        {
            get => $":SOUR{this.ChannelNumber}:{this.FunctionCode}:LEV {{0}}";

            set {
            }
        }

        /// <summary> Gets the Level query command. </summary>
        /// <value> The Level query command. </value>
        protected override string LevelQueryCommand
        {
            get => $":SOUR{this.ChannelNumber}:{this.FunctionCode}:LEV?";

            set {
            }
        }

        #endregion

        #endregion

    }
}
