using System;

namespace isr.VI.E4990
{

    /// <summary> Defines a SCPI Channel marker Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class ChannelMarkerSubsystem : ChannelMarkerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="markerNumber">    The marker number. </param>
        /// <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public ChannelMarkerSubsystem( int markerNumber, int channelNumber, StatusSubsystemBase statusSubsystem ) : base( markerNumber, channelNumber, statusSubsystem, new MarkerReadings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Primary | ReadingElementTypes.Secondary );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets subsystem values to their known execution clear state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.MarkerReadings.Reset();
        }

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.MarkerReadings.Initialize( ReadingElementTypes.Primary | ReadingElementTypes.Secondary );
            this.NotifyPropertyChanged( nameof( this.MarkerReadings ) );
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " LATEST DATA "

        /// <summary> Gets the latest data query command. </summary>
        /// <remarks> Not exactly the same as reading the latest data. </remarks>
        /// <value> The latest data query command. </value>
        protected override string LatestDataQueryCommand { get; set; } = ":CALC{0}:MARK{1}:Y?";

        #endregion

        #region " ABSCISSA "

        /// <summary> Gets the Abscissa command format. </summary>
        /// <value> The Abscissa command format. </value>
        protected override string AbscissaCommandFormat { get; set; } = ":CALC{0}:MARK{1}:X {2}";

        /// <summary> Gets the Abscissa query command. </summary>
        /// <value> The Abscissa query command. </value>
        protected override string AbscissaQueryCommand { get; set; } = ":CALC{0}:MARK{1}:X?";

        #endregion

        #region " ENABLED "

        /// <summary> Gets the automatic delay enabled query command. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string EnabledQueryCommand { get; set; } = ":CALC{0}:MARK{1}:STAT?";

        /// <summary> Gets the automatic delay enabled command Format. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string EnabledCommandFormat { get; set; } = ":CALC{0}:MARK{1}:STAT {2:1;1;0}";

        #endregion

        #endregion

        #region " LATEST DATA "

        /// <summary> The marker readings. </summary>
        private MarkerReadings _MarkerReadings;

        /// <summary> Returns the readings. </summary>
        /// <value> The readings. </value>
        public MarkerReadings MarkerReadings
        {
            get => this._MarkerReadings;

            set {
                this._MarkerReadings = value;
                this.AssignReadingAmounts( value );
            }
        }

        /// <summary> Parses a new set of reading elements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
        /// <returns> A Double? </returns>
        public override double? ParsePrimaryReading( string reading )
        {
            return this.ParseReadingAmounts( reading );
        }

        /// <summary> Initializes the marker average. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="triggerSubsystem">          The trigger subsystem. </param>
        /// <param name="calculateChannelSubsystem"> The calculate channel subsystem. </param>
        public void InitializeMarkerAverage( TriggerSubsystemBase triggerSubsystem, CalculateChannelSubsystemBase calculateChannelSubsystem )
        {
            if ( triggerSubsystem is null )
                throw new ArgumentNullException( nameof( triggerSubsystem ) );
            if ( calculateChannelSubsystem is null )
                throw new ArgumentNullException( nameof( calculateChannelSubsystem ) );
            this.MarkerReadings.Reset();
            _ = triggerSubsystem.ApplyTriggerSource( TriggerSources.Bus );
            calculateChannelSubsystem.ClearAverage();
            _ = triggerSubsystem.ApplyAveragingEnabled( true );
            triggerSubsystem.Immediate();
        }

        /// <summary> Reads marker average. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="triggerSubsystem">          The trigger subsystem. </param>
        /// <param name="calculateChannelSubsystem"> The calculate channel subsystem. </param>
        public void ReadMarkerAverage( TriggerSubsystemBase triggerSubsystem, CalculateChannelSubsystemBase calculateChannelSubsystem )
        {
            this.InitializeMarkerAverage( triggerSubsystem, calculateChannelSubsystem );
            _ = this.Session.QueryOperationCompleted().GetValueOrDefault( false );
            _ = this.FetchLatestData();
        }

        #endregion

    }
}
