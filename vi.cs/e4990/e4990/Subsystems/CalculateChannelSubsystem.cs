namespace isr.VI.E4990
{
    /// <summary> Defines a SCPI Calculate Channel Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class CalculateChannelSubsystem : CalculateChannelSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public CalculateChannelSubsystem( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( channelNumber, statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.TraceCount = 2;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AVERAGE "

        /// <summary> Gets the clear command. </summary>
        /// <remarks> SCPI: ":CALC{0}:AVER:CLE". </remarks>
        /// <value> The clear command. </value>
        protected override string AverageClearCommand { get; set; } = ":CALC{0}:AVER:CLE";

        /// <summary> Gets Average Count command format. </summary>
        /// <remarks> SCPI: ":CALC{0}:AVER:COUN {1}". </remarks>
        /// <value> The Average Count command format. </value>
        protected override string AverageCountCommandFormat { get; set; } = ":CALC{0}:AVER:COUN {1}";

        /// <summary> Gets Average Count query command. </summary>
        /// <remarks> SCPI: ":CALC{0}:AVER:COUN?". </remarks>
        /// <value> The Average Count query command. </value>
        protected override string AverageCountQueryCommand { get; set; } = ":CALC{0}:AVER:COUN?";

        #region " AVERAGING ENABLED "

        /// <summary> Gets the averaging enabled query command. </summary>
        /// <value> The averaging enabled query command. </value>
        protected override string AveragingEnabledQueryCommand { get; set; } = ":CALC{0}:AVER?";

        /// <summary> Gets the averaging enabled command Format. </summary>
        /// <value> The averaging enabled query command. </value>
        protected override string AveragingEnabledCommandFormat { get; set; } = ":CALC{0}:AVER {1:1;1;0}";

        #endregion

        #endregion

        #region " PARAMETER "

        /// <summary> Gets or sets the channel Trace Count command format. </summary>
        /// <value> The Trace Count command format. </value>
        protected override string TraceCountCommandFormat { get; set; } = ":CALC{0}:PAR:COUN {1}";

        /// <summary> Gets Trace Count query command. </summary>
        /// <value> The Trace Count query command. </value>
        protected override string TraceCountQueryCommand { get; set; } = ":CALC{0}:PAR:COUN?";

        #endregion

        #endregion

    }
}
