namespace isr.VI.E4990
{

    /// <summary> Defines a Trigger Subsystem for a Keithley 7500 Meter. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TriggerSubsystem : TriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TriggerSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.AveragingEnabled = false;
            this.TriggerSource = TriggerSources.Internal;
            this.SupportedTriggerSources = TriggerSources.Bus | TriggerSources.External | TriggerSources.Immediate | TriggerSources.Internal;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " ABORT / INIT COMMANDS "

        /// <summary> Gets or sets the Abort command. </summary>
        /// <value> The Abort command. </value>
        protected override string AbortCommand { get; set; } = ":ABOR";

        /// <summary> Gets or sets the initiate command. </summary>
        /// <value> The initiate command. </value>
        protected override string InitiateCommand { get; set; } = ":TRIG";

        /// <summary> Gets or sets the Immediate command. </summary>
        /// <remarks>
        /// This command generates a trigger immediately and executes a measurement, regardless of the
        /// setting of the trigger mode. This command Is different from :TRIG as the execution of the
        /// object finishes when the measurement (all of the sweep) initiated with this object Is
        /// complete. In other words, you can wait for the end of the measurement using the *OPC object.
        /// If you execute this Object When the trigger system Is Not In the trigger wait state (trigger
        /// Event detection state), an Error occurs When executed And the Object Is ignored.
        /// </remarks>
        /// <value> The Immediate command. </value>
        protected override string ImmediateCommand { get; set; } = ":TRIG:SING";

        #endregion

        #region " AVERAGING ENABLED "

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AveragingEnabledQueryCommand { get; set; } = ":TRIG:AVER?";

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AveragingEnabledCommandFormat { get; set; } = ":TRIG:AVER {0:1;1;0}";

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = @":TRIG:EXT:DEL {0:s\.FFFFFFF}";

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = ":TRIG:EXT:DEL?";

        #endregion

        #region " SOURCE "

        /// <summary> Gets or sets the Trigger source query command. </summary>
        /// <value> The Trigger source query command. </value>
        protected override string TriggerSourceQueryCommand { get; set; } = ":TRIG:SOUR?";

        /// <summary> Gets or sets the Trigger source command format. </summary>
        /// <remarks> SCPI: "{0}". </remarks>
        /// <value> The write Trigger source command format. </value>
        protected override string TriggerSourceCommandFormat { get; set; } = ":TRIG:SOUR {0}";

        #endregion

        #endregion

    }
}
