namespace isr.VI.E4990
{

    /// <summary> Defines a SCPI Compensation Channel Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class CompensateChannelSubsystem : CompensateChannelSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="compensationType"> Type of the compensation. </param>
        /// <param name="channelNumber">    A reference to a <see cref="StatusSubsystemBase">message
        ///                                 based session</see>. </param>
        /// <param name="statusSubsystem">  The status subsystem. </param>
        public CompensateChannelSubsystem( CompensationTypes compensationType, int channelNumber, StatusSubsystemBase statusSubsystem ) : base( compensationType, channelNumber, statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Enabled = false;
            if ( ( int? ) this.CompensationType == ( int? ) CompensationTypes.Load == true )
            {
                this.ModelResistance = 50;
                this.ModelInductance = 0;
                this.ModelCapacitance = 0;
            }
            else if ( ( int? ) this.CompensationType == ( int? ) CompensationTypes.OpenCircuit == true )
            {
                this.ModelInductance = 0;
                this.ModelConductance = 0;
            }
            else if ( ( int? ) this.CompensationType == ( int? ) CompensationTypes.ShortCircuit == true )
            {
                this.ModelResistance = 0;
                this.ModelCapacitance = 0;
            }
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " COMMANDS "

        /// <summary> Gets the Acquire measurements command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:COLL:ACQ:{1}". </remarks>
        /// <value> The clear measurements command. </value>
        protected override string AcquireMeasurementsCommand { get; set; } = ":SENS{0}:CORR2:COLL:ACQ:{1}";

        /// <summary> Gets the clear measured data command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2{1}:COLL:CLE". </remarks>
        /// <value> The clear measurements command. </value>
        protected override string ClearMeasurementsCommand { get; set; } = ":SENS{0}:CORR2:{1}:COLL:CLE";

        #endregion

        #region " ENABLED "

        /// <summary> Gets the compensation enabled query command. </summary>
        /// <value> The compensation enabled query command. </value>
        protected override string EnabledQueryCommand { get; set; } = ":SENS{0}:CORR2:{1}:STAT?";

        /// <summary> Gets the compensation enabled command Format. </summary>
        /// <value> The compensation enabled query command. </value>
        protected override string EnabledCommandFormat { get; set; } = ":SENS{0}:CORR2:{1}:STAT {2:1;1;0}";

        #endregion

        #region " FREQUENCY STIMULUS POINTS "

        /// <summary> Gets the Frequency Stimulus Points query command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:ZME:{1}:POIN?". </remarks>
        /// <value> The Frequency Stimulus Points query command. </value>
        protected override string FrequencyStimulusPointsQueryCommand { get; set; } = ":SENS{0}:CORR2:ZME:{1}:POIN?";

        #endregion

        #region " FREQUENCY ARRAY "

        /// <summary> Gets the Frequency Array query command. </summary>
        /// <value> The Frequency Array query command. </value>
        protected override string FrequencyArrayQueryCommand { get; set; } = ":SENS{0}:CORR2:ZME:{1}:FREQ?";

        #endregion

        #region " IMPEDANCE ARRAY "

        /// <summary> Gets the Impedance Array query command. </summary>
        /// <value> The Impedance Array query command. </value>
        protected override string ImpedanceArrayQueryCommand { get; set; } = ":SENS{0}:CORR2:ZME:{1}:DATA?";

        /// <summary> Gets the Impedance Array command Format. </summary>
        /// <value> The Impedance Array query command. </value>
        protected override string ImpedanceArrayCommandFormat { get; set; } = ":SENS{0}:CORR2:ZME:{1}:DATA {2}";

        #endregion

        #region " MODEL RESISTANCE "

        /// <summary> Gets the Model Resistance query command. </summary>
        /// <value> The Model Resistance query command. </value>
        protected override string ModelResistanceQueryCommand { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:R?";

        /// <summary> Gets the Model Resistance command Format. </summary>
        /// <value> The Model Resistance query command. </value>
        protected override string ModelResistanceCommandFormat { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:R {2}";

        #endregion

        #region " MODEL CONDUCTANCE "

        /// <summary> Gets the Model Conductance query command. </summary>
        /// <value> The Model Conductance query command. </value>
        protected override string ModelConductanceQueryCommand { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:G?";

        /// <summary> Gets the Model Conductance command Format. </summary>
        /// <value> The Model Conductance query command. </value>
        protected override string ModelConductanceCommandFormat { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:G {2}";

        #endregion

        #region " MODEL CAPACITANCE "

        /// <summary> Gets the Model Capacitance query command. </summary>
        /// <value> The Model Capacitance query command. </value>
        protected override string ModelCapacitanceQueryCommand { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:C?";

        /// <summary> Gets the Model Capacitance command Format. </summary>
        /// <value> The Model Capacitance query command. </value>
        protected override string ModelCapacitanceCommandFormat { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:C {2}";

        #endregion

        #region " MODEL INDUCTANCE "

        /// <summary> Gets the Model Inductance query command. </summary>
        /// <value> The Model Inductance query command. </value>
        protected override string ModelInductanceQueryCommand { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:L?";

        /// <summary> Gets the Model Inductance command Format. </summary>
        /// <value> The Model Inductance query command. </value>
        protected override string ModelInductanceCommandFormat { get; set; } = ":SENS{0}:CORR2:CKIT:{1}:L {{0}}";

        #endregion

        #endregion

    }
}
