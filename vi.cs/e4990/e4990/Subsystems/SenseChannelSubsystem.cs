namespace isr.VI.E4990
{

    /// <summary> Defines a SCPI Sense Channel Subsystem. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2016-07-06, 4.0.6031. </para>
    /// </remarks>
    public class SenseChannelSubsystem : SenseChannelSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseChannelSubsystem( int channelNumber, StatusSubsystemBase statusSubsystem ) : base( channelNumber, statusSubsystem, new MarkerReadings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Primary | ReadingElementTypes.Secondary );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ohm );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.SupportedAdapterTypes = AdapterTypes.E4M1 | AdapterTypes.E4M2 | AdapterTypes.None;
            this.SupportedSweepTypes = SweepTypes.Linear | SweepTypes.Logarithmic;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.ApertureRange = new Core.Primitives.RangeR( 1d, 5d );
            this.AdapterType = AdapterTypes.None;
            this.SweepPoints = 201;
            this.SweepStart = 20;
            this.SweepStop = 10000000.0d;
            this.SweepType = SweepTypes.Linear;
            this.FrequencyPointsType = FrequencyPointsTypes.Fixed;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " COMMANDS "

        /// <summary> Gets the clear compensations command. </summary>
        /// <remarks> SCPI: ":SENS{0}:CORR2:CLE". </remarks>
        /// <value> The clear compensations command. </value>
        protected override string ClearCompensationsCommand
        {
            get => $":SENS{0}:CORR2:CLE";

            set {
            }
        }

        #endregion

        #region " ADAPTER TYPE "

        /// <summary> Gets the Adapter Type query command. </summary>
        /// <value> The Adapter Type query command, e.g., :SENSE:ADAPT? </value>
        protected override string AdapterTypeQueryCommand { get; set; } = ":SENS:ADAP?";

        /// <summary> Gets the Adapter Type command. </summary>
        /// <value> The Adapter Type command, e.g., :SENSE:ADAPT {0}. </value>
        protected override string AdapterTypeCommandFormat { get; set; } = "SENS:ADAP {0}";

        #endregion

        #region " APERTURE "

        /// <summary> Gets The Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected override string ApertureCommandFormat
        {
            get => $":SENS{0}:APER {{0}}";

            set {
            }
        }

        /// <summary> Gets The Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string ApertureQueryCommand
        {
            get => $":SENS{0}:APER?";

            set {
            }
        }

        #endregion

        #region " FREQUENCY POINTS TYPE "

        /// <summary> Gets Frequency Points Type query command. </summary>
        /// <value> The Frequency Points Type query command. </value>
        protected override string FrequencyPointsTypeQueryCommand
        {
            get => $":SENS{0}:CORR:COLL:FPO?";

            set {
            }
        }

        /// <summary> Gets Frequency Points Type command format. </summary>
        /// <value> The Frequency Points Type command format. </value>
        protected override string FrequencyPointsTypeCommandFormat
        {
            get => $":SENS{0}:CORR:COLL:FPO {{0}}";

            set {
            }
        }

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = $":SENS{0}:FUNC {{0}}";

        /// <summary> Gets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = $":SENS{0}:FUNC?";

        #endregion

        #region " SWEEP "

        /// <summary> Gets Sweep Points query command. </summary>
        /// <value> The Sweep Points query command. </value>
        protected override string SweepPointsQueryCommand
        {
            get => $":SENS{0}:SWE:POIN?";

            set {
            }
        }

        /// <summary> Gets Sweep Points command format. </summary>
        /// <value> The Sweep Points command format. </value>
        protected override string SweepPointsCommandFormat
        {
            get => $":SENS{0}:SWE:POIN {{0}}";

            set {
            }
        }

        /// <summary> Gets Sweep Start query command. </summary>
        /// <value> The Sweep Start query command. </value>
        protected override string SweepStartQueryCommand
        {
            get => $":SENS{0}:FREQ:STAR?";

            set {
            }
        }

        /// <summary> Gets Sweep Start command format. </summary>
        /// <value> The Sweep Start command format. </value>
        protected override string SweepStartCommandFormat
        {
            get => $":SENS{0}:FREQ:STAR {{0}}";

            set {
            }
        }

        /// <summary> Gets Sweep Stop query command. </summary>
        /// <value> The Sweep Stop query command. </value>
        protected override string SweepStopQueryCommand
        {
            get => $":SENS{0}:FREQ:STOP?";

            set {
            }
        }

        /// <summary> Gets Sweep Stop command format. </summary>
        /// <value> The Sweep Stop command format. </value>
        protected override string SweepStopCommandFormat
        {
            get => $":SENS{0}:FREQ:STOP {{0}}";

            set {
            }
        }

        /// <summary> Gets Sweep Type query command. </summary>
        /// <value> The Sweep Type query command. </value>
        protected override string SweepTypeQueryCommand
        {
            get => $":SENS{0}:SWE:TYPE?";

            set {
            }
        }

        /// <summary> Gets Sweep Type command format. </summary>
        /// <value> The Sweep Type command format. </value>
        protected override string SweepTypeCommandFormat
        {
            get => $":SENS{0}:SWE:TYPE {{0}}";

            set {
            }
        }

        #endregion

        #endregion

    }
}
