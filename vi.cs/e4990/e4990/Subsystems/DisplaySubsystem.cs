namespace isr.VI.E4990
{

    /// <summary> Display subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class DisplaySubsystem : DisplaySubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="DisplaySubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public DisplaySubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " COMMANDS "

        /// <summary> Gets or sets the clear caution messages command. </summary>
        /// <value> The clear caution messages command. </value>
        protected override string ClearCautionMessagesCommand { get; set; } = ":DISP:CCL";
        #endregion

        #region " ENABLED "

        /// <summary> Gets or sets the display enable command format. </summary>
        /// <value> The display enable command format. </value>
        protected override string DisplayEnableCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the display enabled query command. </summary>
        /// <value> The display enabled query command. </value>
        protected override string DisplayEnabledQueryCommand { get; set; } = string.Empty;

        #endregion

        #endregion

    }
}
