using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.E4990.Forms.MSTest
{

    /// <summary> E4990 Visa View unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "e4990" )]
    public class VisaViewTests
    {

        #region" CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( E4990Properties.E4990ResourceInfo.Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region" SELECTOR OPENER: SELECT, OPEN, CLOSE "

        /// <summary> (Unit Test Method) tests selector opener. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SelectorOpenerTest()
        {
            FacadeTests.DeviceManager.AssertResourceShouldOpenAndClose( TestInfo, E4990Properties.E4990ResourceInfo );
        }

        #endregion

        #region" VISA VIEW: DEVICE OPEN TEST "

        /// <summary> (Unit Test Method) tests selected resource name visa view. </summary>
        /// <remarks>
        /// Visa View Timing:<para>
        /// Ping elapsed 0.123   </para><para>
        /// Create device 0.097   </para><para>
        /// Check selected resource name 2.298   </para><para>
        /// </para>
        /// </remarks>
        [TestMethod()]
        public void SelectedResourceNameVisaViewTest()
        {
            var sw = Stopwatch.StartNew();
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            TestInfo.TraceMessage( $"Ping elapsed {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            using var device = E4990Device.Create();
            TestInfo.TraceMessage( $"Create device {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            using Facade.IVisaView view = new Facade.VisaView( device );
            FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected( TestInfo, view, E4990Properties.E4990ResourceInfo );
            TestInfo.TraceMessage( $"Check selected resource name {sw.Elapsed:s\\.fff}" );
            sw.Restart();
        }

        /// <summary> (Unit Test Method) tests selected resource name visa tree view. </summary>
        /// <remarks>
        /// Visa Tree View Timing:<para>
        /// Ping elapsed 0.119   </para><para>
        /// Create device 0.125   </para><para>
        /// Check selected resource name 2.293   </para><para>
        /// </para>
        /// </remarks>
        [TestMethod()]
        public void SelectedResourceNameVisaTreeViewTest()
        {
            var sw = Stopwatch.StartNew();
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            TestInfo.TraceMessage( $"Ping elapsed {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            using var device = E4990Device.Create();
            TestInfo.TraceMessage( $"Create device {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            using Facade.IVisaView view = new Facade.VisaTreeView( device );
            FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected( TestInfo, view, E4990Properties.E4990ResourceInfo );
            TestInfo.TraceMessage( $"Check selected resource name {sw.Elapsed:s\\.fff}" );
            sw.Restart();
        }

        /// <summary> (Unit Test Method) tests talker trace message visa view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void TalkerTraceMessageVisaViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            int messageCount = TestInfo.TraceMessagesQueueListener.Count;
            using Facade.IVisaView view = new Facade.VisaTreeView( device );
            // when assigning the device to the view, the listener is assigned.
            Assert.IsTrue( TestInfo.TraceMessagesQueueListener.Count > messageCount, "Constructing a device should add trace messages" );
            // view.AddListener(TestInfo.TraceMessagesQueueListener)
            FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit( view, TestInfo.TraceMessagesQueueListener );
        }

        /// <summary> (Unit Test Method) tests talker trace message visa tree view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void TalkerTraceMessageVisaTreeViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            using Facade.IVisaView view = new Facade.VisaTreeView( device );
            FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit( view, TestInfo.TraceMessagesQueueListener );
        }

        /// <summary> (Unit Test Method) tests open session visa view. </summary>
        /// <remarks>
        /// <para>
        /// Ping elapsed 0.126   </para><para>
        /// Session open 5.423   </para><para>
        /// Session checked 0.001   </para><para>
        /// Session closed 0.024   </para><para>
        ///    </para>
        /// </remarks>
        [TestMethod()]
        public void OpenSessionVisaViewTest()
        {
            var sw = Stopwatch.StartNew();
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            TestInfo.TraceMessage( $"Ping elapsed {sw.Elapsed:s\\.fff}" );
            sw.Restart();
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            sw.Restart();
            using Facade.IVisaView view = new Facade.VisaView( device );
            try
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen( TestInfo, 0, view, E4990Properties.E4990ResourceInfo );
                TestInfo.TraceMessage( $"Session open {sw.Elapsed:s\\.fff}" );
                sw.Restart();
                FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch( view.VisaSessionBase.Session, E4990Properties.E4990ResourceInfo );
                TestInfo.TraceMessage( $"Session checked {sw.Elapsed:s\\.fff}" );
                sw.Restart();
            }
            catch
            {
                throw;
            }
            finally
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 0, view );
                TestInfo.TraceMessage( $"Session closed {sw.Elapsed:s\\.fff}" );
                sw.Restart();
            }
        }

        /// <summary> (Unit Test Method) tests open session visa tree view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionVisaTreeViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            using Facade.IVisaView view = new Facade.VisaTreeView( device );
            try
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen( TestInfo, 0, view, E4990Properties.E4990ResourceInfo );
                FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch( view.VisaSessionBase.Session, E4990Properties.E4990ResourceInfo );
            }
            catch
            {
                throw;
            }
            finally
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 0, view );
            }
        }

        /// <summary> (Unit Test Method) tests open session twice visa view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionTwiceVisaViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            using ( Facade.IVisaView view = new Facade.VisaView( device ) )
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, E4990Properties.E4990ResourceInfo );
            }

            using ( Facade.IVisaView view = new Facade.VisaView( device ) )
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 2, view, E4990Properties.E4990ResourceInfo );
            }
        }

        /// <summary> (Unit Test Method) tests open session twice visa tree view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void OpenSessionTwiceVisaTreeViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            using ( Facade.IVisaView view = new Facade.VisaTreeView( device ) )
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, E4990Properties.E4990ResourceInfo );
            }

            using ( Facade.IVisaView view = new Facade.VisaTreeView( device ) )
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 2, view, E4990Properties.E4990ResourceInfo );
            }
        }

        #endregion

        #region" VISA VIEW: ASSIGNED DEVICE TESTS "

        /// <summary> (Unit Test Method) tests assign device view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignDeviceViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var view = new E4990View();
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            view.AssignDevice( device );
            FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, E4990Properties.E4990ResourceInfo );
        }

        /// <summary> (Unit Test Method) tests assign device tree view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignDeviceTreeViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var view = new E4990TreeView();
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            view.AssignDevice( device );
            FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, E4990Properties.E4990ResourceInfo );
        }

        /// <summary> (Unit Test Method) tests assign open device view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignOpenDeviceViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var view = new E4990View();
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, 1, device, E4990Properties.E4990ResourceInfo );
                view.AssignDevice( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 1, view );
            }
        }

        /// <summary> (Unit Test Method) tests assign open device tree view. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void AssignOpenDeviceTreeViewTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var view = new E4990TreeView();
            using var device = E4990Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, 1, device, E4990Properties.E4990ResourceInfo );
                view.AssignDevice( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 1, view );
            }
        }

        #endregion

    }
}
