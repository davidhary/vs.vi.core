﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "E4990 VI Forms Tests" )]
[assembly: AssemblyDescription( "E4990 Virtual Instrument Forms Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.E4990.Forms.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
