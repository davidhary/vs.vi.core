using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Controls;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{

    /// <summary> Wizard for setting the compensation. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-07-08 </para>
    /// </remarks>
    public partial class CompensationWizard : Core.Forma.ListenerFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new instance of the <see cref="CompensationWizard"/> class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public CompensationWizard() : base()
        {
            // required for designer support
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
            // Me.ConstructorSafeSetter(New TraceMessageTalker)
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( "This wizard will guide you through the steps of performing a compensation task." );
            _ = builder.AppendLine( "The following items are required:" );
            _ = builder.AppendLine( "-- An Open fixture for open compensation;" );
            _ = builder.AppendLine( "-- A Short fixture for short compensation;" );
            _ = builder.AppendLine( "-- A Load Resistance fixture for load compensation; and" );
            _ = builder.AppendLine( "-- A Yardstick Resistance fixture for validation." );
            this._WelcomeWizardPage.Description = builder.ToString();
            this._Wizard.HeaderImage = My.Resources.Resources.HeaderIcon;
            this._Wizard.WelcomeImage = My.Resources.Resources.WelcomeImage;
            this._LoadResistanceNumeric.Value = E4990.My.MySettings.Default.LoadResistance;
            this._YardstickResistanceNumeric.Value = E4990.My.MySettings.Default.YardstickResistance;
            this._YardstickAcceptanceToleranceNumeric.Value = 100m * E4990.My.MySettings.Default.YardstickResistanceTolerance;
            this._YardstickInductanceLimitNumeric.Value = ( decimal ) (1000000.0d * ( double ) E4990.My.MySettings.Default.YardstickInductanceLimit);
            this.__AgreeCheckBox.Name = "_AgreeCheckBox";
            this.__Wizard.Name = "_Wizard";
            this.__YardstickInductanceLimitNumeric.Name = "_YardstickInductanceLimitNumeric";
            this.__YardstickAcceptanceToleranceNumeric.Name = "_YardstickAcceptanceToleranceNumeric";
            this.__MeasureYardstickButton.Name = "_MeasureYardstickButton";
            this.__AcquireLoadCompensationButton.Name = "_AcquireLoadCompensationButton";
            this.__AcquireShortCompensationButton.Name = "_AcquireShortCompensationButton";
            this.__AcquireOpenCompensationButton.Name = "_AcquireOpenCompensationButton";
            this.__RestartAveragingButton.Name = "_RestartAveragingButton";
            this.__ApplySettingsButton.Name = "_ApplySettingsButton";
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                                                   release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed ) return;
            try
            {
                {
                    if ( disposing )
                    {
                        this.Device = null;
                        if ( this.components is object )
                            this.components?.Dispose();
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE AND SUBSYSTEMS "

        /// <summary> The device. </summary>
        private E4990Device _Device;

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        public E4990Device Device
        {
            get => this._Device;

            set {
                if ( this._Device is object )
                {
                    this.Device.CalculateChannelSubsystem.PropertyChanged -= this.CalculateChannelSubsystemPropertyChanged;
                    this.Device.CompensateOpenSubsystem.PropertyChanged -= this.CompensateChannelSubsystemPropertyChanged;
                    this.Device.CompensateShortSubsystem.PropertyChanged -= this.CompensateChannelSubsystemPropertyChanged;
                    this.Device.CompensateLoadSubsystem.PropertyChanged -= this.CompensateChannelSubsystemPropertyChanged;
                    this.Device.ChannelMarkerSubsystem.PropertyChanged -= this.ChannelMarkerSubsystemPropertyChanged;
                    this.Device.PrimaryChannelTraceSubsystem.PropertyChanged -= this.ChannelTraceSubsystemPropertyChanged;
                    this.Device.SecondaryChannelTraceSubsystem.PropertyChanged -= this.ChannelTraceSubsystemPropertyChanged;
                    this.Device.ChannelTriggerSubsystem.PropertyChanged -= this.ChannelTriggerSubsystemPropertyChanged;
                    this.Device.TriggerSubsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
                    this.Device.DisplaySubsystem.PropertyChanged -= this.DisplaySubsystemPropertyChanged;
                    this.Device.SenseChannelSubsystem.PropertyChanged -= this.SenseChannelSubsystemPropertyChanged;
                    this.Device.StatusSubsystem.PropertyChanged -= this.StatusSubsystemPropertyChanged;
                    this.Device.SystemSubsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
                }

                this._Device = value;
                if ( this._Device is object )
                {
                    this.Device.StatusSubsystem.PropertyChanged += this.StatusSubsystemPropertyChanged;
                    this.Device.SystemSubsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
                    this.Device.SenseChannelSubsystem.PropertyChanged += this.SenseChannelSubsystemPropertyChanged;
                    this.Device.TriggerSubsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                    this.Device.DisplaySubsystem.PropertyChanged += this.DisplaySubsystemPropertyChanged;
                    this.Device.CalculateChannelSubsystem.PropertyChanged += this.CalculateChannelSubsystemPropertyChanged;
                    this.Device.CompensateOpenSubsystem.PropertyChanged += this.CompensateChannelSubsystemPropertyChanged;
                    this.Device.CompensateShortSubsystem.PropertyChanged += this.CompensateChannelSubsystemPropertyChanged;
                    this.Device.CompensateLoadSubsystem.PropertyChanged += this.CompensateChannelSubsystemPropertyChanged;
                    this.Device.PrimaryChannelTraceSubsystem.PropertyChanged += this.ChannelTraceSubsystemPropertyChanged;
                    this.Device.SecondaryChannelTraceSubsystem.PropertyChanged += this.ChannelTraceSubsystemPropertyChanged;
                    this.Device.ChannelTriggerSubsystem.PropertyChanged += this.ChannelTriggerSubsystemPropertyChanged;
                    this.Device.ChannelMarkerSubsystem.PropertyChanged += this.ChannelMarkerSubsystemPropertyChanged;
                    this._AdapterComboBox.ListSupportedAdapters( this.Device.SenseChannelSubsystem.SupportedAdapterTypes );
                    // TO_DO: Bind subsystem and handle relevant property changes to display current values.
                }
            }
        }

        #region " SUBSYSTEMS "

        #region " CALCULATE "

        /// <summary> Handle the Calculate channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( CalculateChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( CalculateChannelSubsystem.AveragingEnabled ):
                    {
                        this._AveragingEnabledCheckBox.Checked = subsystem.AveragingEnabled.GetValueOrDefault( false );
                        break;
                    }

                case nameof( CalculateChannelSubsystem.AverageCount ):
                    {
                        this._AveragingCountNumeric.Value = subsystem.AverageCount.GetValueOrDefault( 0 );
                        break;
                    }
            }
        }

        /// <summary> Calculate channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CalculateChannelSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( CalculateChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.CalculateChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as CalculateChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " COMPENSATE "

        /// <summary> Handle the Compensate channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( CompensateChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( CompensateChannelSubsystem.HasCompleteCompensationValues ):
                    {
                        string value = string.Empty;
                        if ( subsystem.HasCompleteCompensationValues )
                        {
                            value = CompensateChannelSubsystemBase.Merge( subsystem.FrequencyArrayReading, subsystem.ImpedanceArrayReading );
                        }

                        if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.OpenCircuit == true )
                        {
                            this._OpenCompensationValuesTextBox.Text = value;
                        }
                        else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.ShortCircuit == true )
                        {
                            this._ShortCompensationValuesTextBox.Text = value;
                        }
                        else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.Load == true )
                        {
                            this._LoadCompensationTextBox.Text = value;
                        }

                        break;
                    }
            }

            if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.OpenCircuit == true )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( CompensateChannelSubsystem.HasCompleteCompensationValues ):
                        {
                            break;
                        }
                }
            }
            else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.ShortCircuit == true )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( CompensateChannelSubsystem.FrequencyArrayReading ):
                        {
                            break;
                        }
                }
            }
            else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.Load == true )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( CompensateChannelSubsystem.FrequencyArrayReading ):
                        {
                            break;
                        }

                    case nameof( CompensateChannelSubsystem.ModelResistance ):
                        {
                            if ( subsystem.ModelResistance.HasValue )
                                this._LoadResistanceNumeric.Value = ( decimal ) subsystem.ModelResistance.Value;
                            break;
                        }
                }
            }
        }

        /// <summary> Compensate channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CompensateChannelSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( CompensateChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.CompensateChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as CompensateChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL MARKER "

        /// <summary> Handle the channel marker subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelMarkerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ChannelMarkerSubsystem.LastReading ):
                    {
                        this.UpdateYardstickValues();
                        break;
                    }
            }
        }

        /// <summary> Channel marker subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelMarkerSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( ChannelMarkerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.ChannelMarkerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelMarkerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL TRACE "

        /// <summary> Handle the channel Trace subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelTraceSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
#pragma warning disable CS1522 // Empty switch block
            {
#pragma warning restore CS1522 // Empty switch block
                // Case NameOf(VI.ChannelTraceSubsystemBase.ChannelNumber)
            }
        }

        /// <summary> Channel Trace subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelTraceSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( ChannelTraceSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.ChannelTraceSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelTraceSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL TRIGGER "

        /// <summary> Handle the channel Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelTriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
#pragma warning disable CS1522 // Empty switch block
            {
#pragma warning restore CS1522 // Empty switch block
                // Case NameOf(VI.ChannelTriggerSubsystemBase.ChannelNumber)
            }
        }

        /// <summary> Channel Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelTriggerSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( ChannelTriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.ChannelTriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelTriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Handle the Display subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( DisplaySubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
#pragma warning disable CS1522 // Empty switch block
            {
#pragma warning restore CS1522 // Empty switch block
                // Case NameOf(VI.DisplaySubsystemBase.Enabled)
            }
        }

        /// <summary> Display subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DisplaySubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( DisplaySubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.DisplaySubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as DisplaySubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Selects a new Adapter to display. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.AdapterTypes. </returns>
        internal AdapterTypes SelectAdapter( AdapterTypes value )
        {
            if ( this.InitializingComponents || value == AdapterTypes.None )
                return AdapterTypes.None;
            if ( value != AdapterTypes.None && value != this.SelectedAdapterType )
            {
                _ = this._AdapterComboBox.SelectItem( value.ValueDescriptionPair() );
            }

            return this.SelectedAdapterType;
        }

        /// <summary> Gets the type of the selected Adapter. </summary>
        /// <value> The type of the selected Adapter. </value>
        private AdapterTypes SelectedAdapterType => ( AdapterTypes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._AdapterComboBox.SelectedItem).Key );

        /// <summary> Handle the sense channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SenseChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SenseChannelSubsystemBase.AdapterType ):
                    {
                        if ( subsystem.AdapterType.HasValue )
                            _ = this.SelectAdapter( subsystem.AdapterType.Value );
                        break;
                    }

                case nameof( SenseChannelSubsystemBase.Aperture ):
                    {
                        if ( subsystem.Aperture.HasValue )
                            this._ApertureNumeric.Value = ( decimal ) subsystem.Aperture.Value;
                        break;
                    }
            }
        }

        /// <summary> Sense channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseChannelSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SenseChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.SenseChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( TriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TriggerSubsystemBase.Delay ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " STATUS "

        /// <summary> Reports the last error. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="lastError"> The last error. </param>
        private void OnLastError( DeviceError lastError )
        {
            this._StatusLabel.Text = Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact( lastError.CompoundErrorMessage, this._StatusLabel );
            this._StatusLabel.ToolTipText = lastError.CompoundErrorMessage;
        }

        /// <summary> Handle the Status subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( StatusSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( StatusSubsystemBase.DeviceErrorReport ):
                    {
                        this.OnLastError( subsystem.DeviceErrorQueue.LastError );
                        break;
                    }

                case nameof( StatusSubsystemBase.DeviceErrorQueue ):
                    {
                        this.OnLastError( subsystem.DeviceErrorQueue.LastError );
                        break;
                    }
            }
        }

        /// <summary> Status subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StatusSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( StatusSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.StatusSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as StatusSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadStatusRegister()
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} reading service request";
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SYSTEM "

        /// <summary> Handle the System subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            Application.DoEvents();
        }

        /// <summary> System subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SystemSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, System.ComponentModel.PropertyChangedEventArgs>( this.SystemSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #endregion

        #endregion

        #region " METHODS "

        /// <summary> Starts a sample task simulation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void StartTask()
        {
            // setup wizard page
            this._Wizard.BackEnabled = false;
            this._Wizard.NextEnabled = false;
            this._LongTaskProgressBar.Value = this._LongTaskProgressBar.Minimum;

            // start timer to simulate a long running task
            this._LongTaskTimer.Enabled = true;
        }

        #endregion

        #region " CONTROL EVENTS HANDLERS "

        #region " SETTINGS "

        /// <summary> Restart averaging button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RestartAveragingButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} restarting average";
                this._InfoProvider.Clear();
                this.Device.CalculateChannelSubsystem.ClearAverage();
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Applies the settings button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} apply settings";
                this._InfoProvider.Clear();
                _ = this.Device.SenseChannelSubsystem.ApplyAdapterType( this.SelectedAdapterType );
                _ = this.Device.SenseChannelSubsystem.ApplyAperture( ( double ) this._ApertureNumeric.Value );
                this.Device.CalculateChannelSubsystem.ApplyAverageSettings( this._AveragingEnabledCheckBox.Checked, ( int ) Math.Round( this._AveragingCountNumeric.Value ) );
                this.Device.CalculateChannelSubsystem.ClearAverage();
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        #endregion

        #region " OPEN "

        /// <summary> Acquires the open compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AcquireOpenCompensation()
        {

            // Set user-specified frequencies
            _ = this.Device.SenseChannelSubsystem.ApplyFrequencyPointsType( FrequencyPointsTypes.User );

            // Acquire open fixture compensation
            this.Device.CompensateOpenSubsystem.AcquireMeasurements();

            // Wait for measurement end
            _ = this.Device.Session.QueryOperationCompleted();
        }

        /// <summary> Reads open compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ReadOpenCompensation()
        {

            // read the frequencies
            _ = this.Device.CompensateOpenSubsystem.QueryFrequencyArray();

            // read the data
            _ = this.Device.CompensateOpenSubsystem.QueryImpedanceArray();
        }

        /// <summary> Acquires the open compensation button context menu strip changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireOpenCompensationButton_ContextMenuStripChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring open compensation";
                this._InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.Device.ClearCompensations();
                this.Device.ChannelMarkerSubsystem.MarkerReadings.Reset();
                this.AcquireOpenCompensation();
                this.ReadOpenCompensation();
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        #endregion

        #region " SHORT "

        /// <summary> Acquires the Short compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AcquireShortCompensation()
        {

            // Set user-specified frequencies
            _ = this.Device.SenseChannelSubsystem.ApplyFrequencyPointsType( FrequencyPointsTypes.User );

            // Acquire Short fixture compensation
            this.Device.CompensateShortSubsystem.AcquireMeasurements();

            // Wait for measurement end
            _ = this.Device.Session.QueryOperationCompleted();
        }

        /// <summary> Reads Short compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ReadShortCompensation()
        {

            // read the frequencies
            _ = this.Device.CompensateShortSubsystem.QueryFrequencyArray();

            // read the data
            _ = this.Device.CompensateShortSubsystem.QueryImpedanceArray();
        }

        /// <summary> Acquires the Short compensation button context menu strip changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireShortCompensationButton_ContextMenuStripChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring short compensation";
                this._InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.AcquireShortCompensation();
                this.ReadShortCompensation();
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " LOAD "

        /// <summary> Acquires the Load compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AcquireLoadCompensation()
        {

            // Set user-specified frequencies
            _ = this.Device.SenseChannelSubsystem.ApplyFrequencyPointsType( FrequencyPointsTypes.User );
            _ = this.Device.CompensateLoadSubsystem.ApplyModelResistance( ( double ) this._LoadResistanceNumeric.Value );
            _ = this.Device.CompensateLoadSubsystem.ApplyModelCapacitance( 0d );
            _ = this.Device.CompensateLoadSubsystem.ApplyModelInductance( 0d );
            _ = this.Device.Session.QueryOperationCompleted();

            // Acquire Load fixture compensation
            this.Device.CompensateLoadSubsystem.AcquireMeasurements();

            // Wait for measurement end
            _ = this.Device.Session.QueryOperationCompleted();
        }

        /// <summary> Reads Load compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ReadLoadCompensation()
        {

            // read the frequencies
            _ = this.Device.CompensateLoadSubsystem.QueryFrequencyArray();

            // read the data
            _ = this.Device.CompensateLoadSubsystem.QueryImpedanceArray();
        }

        /// <summary> Acquires the Load compensation button context menu strip changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireLoadCompensationButton_ContextMenuStripChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring load compensation";
                this._InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.AcquireLoadCompensation();
                this.ReadLoadCompensation();
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " YARDSTICK "

        /// <summary> Yardstick acceptance tolerance numeric value changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void YardstickAcceptanceToleranceNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AnnunciateYardstickResult();
        }

        /// <summary> Yardstick inductance limit numeric value changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void YardstickInductanceLimitNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AnnunciateYardstickResult();
        }

        /// <summary> Gets the yardstick resistance validated. </summary>
        /// <value> The yardstick resistance validated. </value>
        private bool? YardstickResistanceValidated
        {
            get {
                if ( this.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.HasValue )
                {
                    double r = this.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.Value;
                    double delta = 100d * (r / ( double ) this._YardstickResistanceNumeric.Value - 1d);
                    return Math.Abs( delta ) <= ( double ) this._YardstickAcceptanceToleranceNumeric.Value;
                }
                else
                {
                    return new bool?();
                }
            }
        }

        /// <summary> Gets the yardstick impedance validated. </summary>
        /// <value> The yardstick impedance validated. </value>
        private bool? YardstickInductanceValidated
        {
            get {
                if ( this.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.HasValue )
                {
                    double l = 1000000.0d * this.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.Value;
                    this._YardstickMeasuredInductanceTextBox.Text = string.Format( "{0:0.###}", l );
                    return Math.Abs( l ) <= ( double ) this._YardstickInductanceLimitNumeric.Value;
                }
                else
                {
                    return new bool?();
                }
            }
        }

        /// <summary> Gets the yardstick validated. </summary>
        /// <value> The yardstick validated. </value>
        private bool YardstickValidated => this.YardstickResistanceValidated.GetValueOrDefault( false ) && this.YardstickInductanceValidated.GetValueOrDefault( false );

        /// <summary> Updates the yardstick values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void UpdateYardstickValues()
        {
            this._InfoProvider.Clear();
            this._YardstickValuesTextBox.Text = this.Device.ChannelMarkerSubsystem.MarkerReadings.RawReading;
            double r = this.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.GetValueOrDefault( 0d );
            this._MeasuredYardstickResistanceTextBox.Text = string.Format( "{0:0.###}", r );
            double delta = 100d * (r / ( double ) this._YardstickResistanceNumeric.Value - 1d);
            this._YardstickResistanceDeviationTextBox.Text = string.Format( "{0:0.##}", delta );
            double l = 1000000.0d * this.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.GetValueOrDefault( 0d );
            this._YardstickMeasuredInductanceTextBox.Text = string.Format( "{0:0.###}", l );
            this.AnnunciateYardstickResult();
        }

        /// <summary> Annunciate yardstick result. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AnnunciateYardstickResult()
        {
            if ( !this.YardstickResistanceValidated == true )
            {
                _ = this._InfoProvider.Annunciate( this._YardstickAcceptanceToleranceNumeric, Core.Forma.InfoProviderLevel.Alert, "Exceeds minimum" );
            }

            if ( !this.YardstickInductanceValidated == true )
            {
                _ = this._InfoProvider.Annunciate( this._YardstickInductanceLimitNumeric, Core.Forma.InfoProviderLevel.Alert, "Exceeds minimum" );
            }
        }

        /// <summary> Measure yardstick button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureYardstickButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring reading meter; yardstick";
                this._InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.Device.ChannelMarkerSubsystem.ReadMarkerAverage( this.Device.TriggerSubsystem, this.Device.CalculateChannelSubsystem );
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region " WIZARD EVENTS HANDLERS "

        /// <summary> Handles the AfterSwitchPages event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Page changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_AfterSwitchPages( object sender, PageChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} after moving page";
                this._InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                switch ( true )
                {
                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._SettingsWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._OpenWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._ShortWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._LoadWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._YardstickWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._FinishWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._LicenseWizardPage ):
                        {
                            // check if license page
                            // sync next button's state with check box
                            this._Wizard.NextEnabled = this._AgreeCheckBox.Checked;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._ProgressWizardPage ):
                        {
                            // check if progress page
                            // start the sample task
                            this.StartTask();
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Handles the BeforeSwitchPages event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Page changing event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_BeforeSwitchPages( object sender, PageChangingEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} before moving page";
                this._InfoProvider.Clear();
                switch ( true )
                {
                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._SettingsWizardPage ) && e.Direction == Direction.Next:
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._OpenWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._ShortWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._LoadWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._YardstickWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._LicenseWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._ProgressWizardPage ):
                        {
                            break;
                        }
                }

                _ = this.Device.Session.QueryOperationCompleted();
                _ = this.Device.Session.ReadStatusRegister();

                // check if we're going forward from options page
                if ( ReferenceEquals( this._Wizard.OldPage, this._OptionsWizardPage ) && e.NewIndex > e.OldIndex )
                {
                    // check if user selected one option
                    if ( this._CheckOptionRadioButton.Checked == false && this._SkipOptionRadioButton.Checked == false )
                    {
                        // display hint & cancel step
                        _ = MessageBox.Show( "Please chose one of the options presented.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
                        e.Cancel = true;
                    }
                    // check if user chose to skip validation
                    else if ( this._SkipOptionRadioButton.Checked )
                    {
                        // skip the validation page
                        e.NewIndex += 1;
                    }
                }
            }
            catch ( Exception ex )
            {
                e.Cancel = true;
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Handles the Cancel event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_Cancel( object sender, System.ComponentModel.CancelEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} canceling a wizard step";
                this._InfoProvider.Clear();
                // check if task is running
                bool isTaskRunning = this._LongTaskTimer.Enabled;
                // stop the task
                this._LongTaskTimer.Enabled = false;

                // ask user to confirm
                if ( MessageBox.Show( $"Are you sure you wand to exit {this.Text}?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly ) != DialogResult.Yes )
                {
                    // cancel closing
                    e.Cancel = true;
                    // restart the task
                    this._LongTaskTimer.Enabled = isTaskRunning;
                }
                else
                {
                    // ensure parent form is closed (even when ShowDialog is not used)
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Handles the Finish event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_Finish( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} closing the wizard";
                this._InfoProvider.Clear();
                if ( this.YardstickValidated )
                {
                    _ = MessageBox.Show( $"The {this.Text} finished successfully.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
                    // ensure parent form is closed (even when ShowDialog is not used)
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
                else if ( MessageBox.Show( $"The {this.Text} yardstick was not validated or is out of range. Are you sure?", $"{this.Text} Save Data; Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Yes )
                {
                    this.DialogResult = DialogResult.OK;
                    this.Close();
                }
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Handles the Help event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_Help( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} showing help info";
                this._InfoProvider.Clear();
                string text = $"This is a really cool wizard control!{Environment.NewLine}:-)";
                switch ( true )
                {
                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._SettingsWizardPage ):
                        {
                            text = My.Resources.Resources.SettingsHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._OpenWizardPage ):
                        {
                            text = My.Resources.Resources.OpenHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._ShortWizardPage ):
                        {
                            text = My.Resources.Resources.ShortHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._LoadWizardPage ):
                        {
                            text = My.Resources.Resources.LoadHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._YardstickWizardPage ):
                        {
                            text = My.Resources.Resources.YardstickHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._FinishWizardPage ):
                        {
                            text = My.Resources.Resources.FinishHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._LicenseWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._ProgressWizardPage ):
                        {
                            break;
                        }
                }

                _ = MessageBox.Show( text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            catch ( Exception ex )
            {
                _ = this._InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        #endregion

        #region " SIMULATION EVENT HANDLERS "

        /// <summary> Handles the CheckedChanged event of checkIAgree. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AgreeCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            // sync next button's state with check box
            this._Wizard.NextEnabled = this._AgreeCheckBox.Checked;
        }

        /// <summary> Handles the Tick event of timerTask. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void LongTaskTimer_Tick( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            // check if task completed
            if ( this._LongTaskProgressBar.Value == this._LongTaskProgressBar.Maximum )
            {
                // stop the timer & switch to next page
                this._LongTaskTimer.Enabled = false;
                this._Wizard.Next();
            }
            else
            {
                // update progress bar
                this._LongTaskProgressBar.PerformStep();
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
