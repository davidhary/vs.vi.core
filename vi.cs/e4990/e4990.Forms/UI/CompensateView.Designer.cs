﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{
    [DesignerGenerated()]
    public partial class CompensateView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _YardstickPageLayout = new System.Windows.Forms.TableLayoutPanel();
            _YardstickPagePanel = new System.Windows.Forms.Panel();
            __YardstickInductanceLimitNumeric = new System.Windows.Forms.NumericUpDown();
            __YardstickInductanceLimitNumeric.ValueChanged += new EventHandler(YardstickInductanceLimitNumeric_ValueChanged);
            _YardstickMeasuredInductanceTextBox = new System.Windows.Forms.TextBox();
            _YardstickMeasuredInductanceTextBoxLabel = new System.Windows.Forms.Label();
            _YardstickDeltaLabel = new System.Windows.Forms.Label();
            _YardstickActualLabel = new System.Windows.Forms.Label();
            _YardstickResistanceDeviationTextBox = new System.Windows.Forms.TextBox();
            _MeasuredYardstickResistanceTextBox = new System.Windows.Forms.TextBox();
            __YardstickAcceptanceToleranceNumeric = new System.Windows.Forms.NumericUpDown();
            __YardstickAcceptanceToleranceNumeric.ValueChanged += new EventHandler(YardstickAcceptanceToleranceNumeric_ValueChanged);
            _YardstickResistanceNumeric = new System.Windows.Forms.NumericUpDown();
            _YardstickAcceptanceToleranceNumericLabel = new System.Windows.Forms.Label();
            _YardstickResistanceNumericLabel = new System.Windows.Forms.Label();
            _YardstickValuesTextBox = new System.Windows.Forms.TextBox();
            __MeasureYardstickButton = new System.Windows.Forms.Button();
            __MeasureYardstickButton.Click += new EventHandler(MeasureYardstickButton_Click);
            _LoadWizardPage = new Core.Controls.WizardPage();
            _LoadPageLayout = new System.Windows.Forms.TableLayoutPanel();
            _LoadPagePanel = new System.Windows.Forms.Panel();
            _LoadResistanceNumeric = new System.Windows.Forms.NumericUpDown();
            _LoadResistanceNumericLabel = new System.Windows.Forms.Label();
            _LoadCompensationTextBox = new System.Windows.Forms.TextBox();
            __AcquireLoadCompensationButton = new System.Windows.Forms.Button();
            __AcquireLoadCompensationButton.ContextMenuStripChanged += new EventHandler(AcquireLoadCompensationButton_ContextMenuStripChanged);
            _ShortWizardPage = new Core.Controls.WizardPage();
            _ShortPageLayout = new System.Windows.Forms.TableLayoutPanel();
            _ShortPagePanel = new System.Windows.Forms.Panel();
            _ShortCompensationValuesTextBox = new System.Windows.Forms.TextBox();
            __AcquireShortCompensationButton = new System.Windows.Forms.Button();
            __AcquireShortCompensationButton.ContextMenuStripChanged += new EventHandler(AcquireShortCompensationButton_ContextMenuStripChanged);
            _OpenWizardPage = new Core.Controls.WizardPage();
            _OpenPageLayout = new System.Windows.Forms.TableLayoutPanel();
            _OpenPagePanel = new System.Windows.Forms.Panel();
            _OpenCompensationValuesTextBox = new System.Windows.Forms.TextBox();
            __AcquireOpenCompensationButton = new System.Windows.Forms.Button();
            __AcquireOpenCompensationButton.ContextMenuStripChanged += new EventHandler(AcquireOpenCompensationButton_ContextMenuStripChanged);
            _LongTaskTimer = new System.Windows.Forms.Timer(components);
            _StatusStrip = new System.Windows.Forms.StatusStrip();
            _StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            _ProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            _YardstickWizardPage = new Core.Controls.WizardPage();
            _ApertureNumeric = new System.Windows.Forms.NumericUpDown();
            _AveragingEnabledCheckBox = new System.Windows.Forms.CheckBox();
            __RestartAveragingButton = new System.Windows.Forms.Button();
            __RestartAveragingButton.Click += new EventHandler(RestartAveragingButton_Click);
            _AveragingCountNumeric = new System.Windows.Forms.NumericUpDown();
            _AdapterComboBox = new System.Windows.Forms.ComboBox();
            _ApertureNumericLabel = new System.Windows.Forms.Label();
            _AveragingGroupBox = new System.Windows.Forms.GroupBox();
            _AveragingCountNumericLabel = new System.Windows.Forms.Label();
            _SettingsPageLayout = new System.Windows.Forms.TableLayoutPanel();
            _CompensationGroupBox = new System.Windows.Forms.GroupBox();
            __ApplySettingsButton = new System.Windows.Forms.Button();
            __ApplySettingsButton.Click += new EventHandler(ApplySettingsButton_Click);
            _AdapterComboBoxLabel = new System.Windows.Forms.Label();
            _SettingsWizardPage = new Core.Controls.WizardPage();
            __Wizard = new Core.Controls.Wizard();
            __Wizard.PageChanged += new EventHandler<Core.Controls.PageChangedEventArgs>(Wizard_AfterSwitchPages);
            __Wizard.PageChanging += new EventHandler<Core.Controls.PageChangingEventArgs>(Wizard_BeforeSwitchPages);
            __Wizard.Cancel += new System.ComponentModel.CancelEventHandler(Wizard_Cancel);
            __Wizard.Finish += new EventHandler(Wizard_Finish);
            __Wizard.Help += new EventHandler(Wizard_Help);
            _WelcomeWizardPage = new Core.Controls.WizardPage();
            _FinishWizardPage = new Core.Controls.WizardPage();
            _YardstickPageLayout.SuspendLayout();
            _YardstickPagePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__YardstickInductanceLimitNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__YardstickAcceptanceToleranceNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_YardstickResistanceNumeric).BeginInit();
            _LoadWizardPage.SuspendLayout();
            _LoadPageLayout.SuspendLayout();
            _LoadPagePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_LoadResistanceNumeric).BeginInit();
            _ShortWizardPage.SuspendLayout();
            _ShortPageLayout.SuspendLayout();
            _ShortPagePanel.SuspendLayout();
            _OpenWizardPage.SuspendLayout();
            _OpenPageLayout.SuspendLayout();
            _OpenPagePanel.SuspendLayout();
            _StatusStrip.SuspendLayout();
            _YardstickWizardPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_AveragingCountNumeric).BeginInit();
            _AveragingGroupBox.SuspendLayout();
            _SettingsPageLayout.SuspendLayout();
            _CompensationGroupBox.SuspendLayout();
            _SettingsWizardPage.SuspendLayout();
            __Wizard.SuspendLayout();
            SuspendLayout();
            // 
            // _YardstickPageLayout
            // 
            _YardstickPageLayout.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _YardstickPageLayout.ColumnCount = 3;
            _YardstickPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _YardstickPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _YardstickPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _YardstickPageLayout.Controls.Add(_YardstickPagePanel, 1, 1);
            _YardstickPageLayout.Location = new System.Drawing.Point(3, 68);
            _YardstickPageLayout.Name = "_YardstickPageLayout";
            _YardstickPageLayout.RowCount = 3;
            _YardstickPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _YardstickPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _YardstickPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _YardstickPageLayout.Size = new System.Drawing.Size(375, 137);
            _YardstickPageLayout.TabIndex = 1;
            // 
            // _YardstickPagePanel
            // 
            _YardstickPagePanel.Controls.Add(__YardstickInductanceLimitNumeric);
            _YardstickPagePanel.Controls.Add(_YardstickMeasuredInductanceTextBox);
            _YardstickPagePanel.Controls.Add(_YardstickMeasuredInductanceTextBoxLabel);
            _YardstickPagePanel.Controls.Add(_YardstickDeltaLabel);
            _YardstickPagePanel.Controls.Add(_YardstickActualLabel);
            _YardstickPagePanel.Controls.Add(_YardstickResistanceDeviationTextBox);
            _YardstickPagePanel.Controls.Add(_MeasuredYardstickResistanceTextBox);
            _YardstickPagePanel.Controls.Add(__YardstickAcceptanceToleranceNumeric);
            _YardstickPagePanel.Controls.Add(_YardstickResistanceNumeric);
            _YardstickPagePanel.Controls.Add(_YardstickAcceptanceToleranceNumericLabel);
            _YardstickPagePanel.Controls.Add(_YardstickResistanceNumericLabel);
            _YardstickPagePanel.Controls.Add(_YardstickValuesTextBox);
            _YardstickPagePanel.Controls.Add(__MeasureYardstickButton);
            _YardstickPagePanel.Location = new System.Drawing.Point(39, -30);
            _YardstickPagePanel.Name = "_YardstickPagePanel";
            _YardstickPagePanel.Size = new System.Drawing.Size(297, 198);
            _YardstickPagePanel.TabIndex = 0;
            // 
            // _YardstickInductanceLimitNumeric
            // 
            __YardstickInductanceLimitNumeric.DecimalPlaces = 1;
            __YardstickInductanceLimitNumeric.Location = new System.Drawing.Point(230, 64);
            __YardstickInductanceLimitNumeric.Name = "__YardstickInductanceLimitNumeric";
            __YardstickInductanceLimitNumeric.Size = new System.Drawing.Size(57, 25);
            __YardstickInductanceLimitNumeric.TabIndex = 12;
            // 
            // _YardstickMeasuredInductanceTextBox
            // 
            _YardstickMeasuredInductanceTextBox.Location = new System.Drawing.Point(104, 64);
            _YardstickMeasuredInductanceTextBox.Name = "_YardstickMeasuredInductanceTextBox";
            _YardstickMeasuredInductanceTextBox.ReadOnly = true;
            _YardstickMeasuredInductanceTextBox.Size = new System.Drawing.Size(56, 25);
            _YardstickMeasuredInductanceTextBox.TabIndex = 11;
            // 
            // _YardstickMeasuredInductanceTextBoxLabel
            // 
            _YardstickMeasuredInductanceTextBoxLabel.AutoSize = true;
            _YardstickMeasuredInductanceTextBoxLabel.Location = new System.Drawing.Point(13, 68);
            _YardstickMeasuredInductanceTextBoxLabel.Name = "_YardstickMeasuredInductanceTextBoxLabel";
            _YardstickMeasuredInductanceTextBoxLabel.Size = new System.Drawing.Size(23, 17);
            _YardstickMeasuredInductanceTextBoxLabel.TabIndex = 10;
            _YardstickMeasuredInductanceTextBoxLabel.Text = "Ls:";
            // 
            // _YardstickDeltaLabel
            // 
            _YardstickDeltaLabel.AutoSize = true;
            _YardstickDeltaLabel.Location = new System.Drawing.Point(169, 13);
            _YardstickDeltaLabel.Name = "_YardstickDeltaLabel";
            _YardstickDeltaLabel.Size = new System.Drawing.Size(53, 17);
            _YardstickDeltaLabel.TabIndex = 9;
            _YardstickDeltaLabel.Text = "Delta %";
            // 
            // _YardstickActualLabel
            // 
            _YardstickActualLabel.AutoSize = true;
            _YardstickActualLabel.Location = new System.Drawing.Point(112, 13);
            _YardstickActualLabel.Name = "_YardstickActualLabel";
            _YardstickActualLabel.Size = new System.Drawing.Size(43, 17);
            _YardstickActualLabel.TabIndex = 8;
            _YardstickActualLabel.Text = "Actual";
            // 
            // _YardstickResistanceDeviationTextBox
            // 
            _YardstickResistanceDeviationTextBox.Location = new System.Drawing.Point(167, 32);
            _YardstickResistanceDeviationTextBox.Name = "_YardstickResistanceDeviationTextBox";
            _YardstickResistanceDeviationTextBox.ReadOnly = true;
            _YardstickResistanceDeviationTextBox.Size = new System.Drawing.Size(56, 25);
            _YardstickResistanceDeviationTextBox.TabIndex = 7;
            _YardstickResistanceDeviationTextBox.Text = "0.000";
            _YardstickResistanceDeviationTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _MeasuredYardstickResistanceTextBox
            // 
            _MeasuredYardstickResistanceTextBox.Location = new System.Drawing.Point(105, 32);
            _MeasuredYardstickResistanceTextBox.Name = "_MeasuredYardstickResistanceTextBox";
            _MeasuredYardstickResistanceTextBox.ReadOnly = true;
            _MeasuredYardstickResistanceTextBox.Size = new System.Drawing.Size(56, 25);
            _MeasuredYardstickResistanceTextBox.TabIndex = 6;
            _MeasuredYardstickResistanceTextBox.Text = "99.999";
            _MeasuredYardstickResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _YardstickAcceptanceToleranceNumeric
            // 
            __YardstickAcceptanceToleranceNumeric.DecimalPlaces = 2;
            __YardstickAcceptanceToleranceNumeric.Location = new System.Drawing.Point(230, 32);
            __YardstickAcceptanceToleranceNumeric.Name = "__YardstickAcceptanceToleranceNumeric";
            __YardstickAcceptanceToleranceNumeric.Size = new System.Drawing.Size(57, 25);
            __YardstickAcceptanceToleranceNumeric.TabIndex = 5;
            // 
            // _YardstickResistanceNumeric
            // 
            _YardstickResistanceNumeric.DecimalPlaces = 2;
            _YardstickResistanceNumeric.Location = new System.Drawing.Point(39, 32);
            _YardstickResistanceNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _YardstickResistanceNumeric.Name = "_YardstickResistanceNumeric";
            _YardstickResistanceNumeric.Size = new System.Drawing.Size(59, 25);
            _YardstickResistanceNumeric.TabIndex = 4;
            _YardstickResistanceNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _YardstickAcceptanceToleranceNumericLabel
            // 
            _YardstickAcceptanceToleranceNumericLabel.AutoSize = true;
            _YardstickAcceptanceToleranceNumericLabel.Location = new System.Drawing.Point(241, 13);
            _YardstickAcceptanceToleranceNumericLabel.Name = "_YardstickAcceptanceToleranceNumericLabel";
            _YardstickAcceptanceToleranceNumericLabel.Size = new System.Drawing.Size(35, 17);
            _YardstickAcceptanceToleranceNumericLabel.TabIndex = 3;
            _YardstickAcceptanceToleranceNumericLabel.Text = "Limit";
            // 
            // _YardstickResistanceNumericLabel
            // 
            _YardstickResistanceNumericLabel.AutoSize = true;
            _YardstickResistanceNumericLabel.Location = new System.Drawing.Point(11, 36);
            _YardstickResistanceNumericLabel.Name = "_YardstickResistanceNumericLabel";
            _YardstickResistanceNumericLabel.Size = new System.Drawing.Size(25, 17);
            _YardstickResistanceNumericLabel.TabIndex = 2;
            _YardstickResistanceNumericLabel.Text = "Rs:";
            // 
            // _YardstickValuesTextBox
            // 
            _YardstickValuesTextBox.Location = new System.Drawing.Point(29, 132);
            _YardstickValuesTextBox.Multiline = true;
            _YardstickValuesTextBox.Name = "_YardstickValuesTextBox";
            _YardstickValuesTextBox.ReadOnly = true;
            _YardstickValuesTextBox.Size = new System.Drawing.Size(239, 52);
            _YardstickValuesTextBox.TabIndex = 1;
            // 
            // _MeasureYardstickButton
            // 
            __MeasureYardstickButton.Location = new System.Drawing.Point(62, 95);
            __MeasureYardstickButton.Name = "__MeasureYardstickButton";
            __MeasureYardstickButton.Size = new System.Drawing.Size(173, 33);
            __MeasureYardstickButton.TabIndex = 0;
            __MeasureYardstickButton.Text = "Measure Yardstick";
            __MeasureYardstickButton.UseVisualStyleBackColor = true;
            // 
            // _LoadWizardPage
            // 
            _LoadWizardPage.Controls.Add(_LoadPageLayout);
            _LoadWizardPage.Description = "Load compensation";
            _LoadWizardPage.Location = new System.Drawing.Point(0, 0);
            _LoadWizardPage.Name = "_LoadWizardPage";
            _LoadWizardPage.Size = new System.Drawing.Size(428, 208);
            _LoadWizardPage.TabIndex = 17;
            _LoadWizardPage.Title = "Load";
            // 
            // _LoadPageLayout
            // 
            _LoadPageLayout.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _LoadPageLayout.ColumnCount = 3;
            _LoadPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _LoadPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _LoadPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _LoadPageLayout.Controls.Add(_LoadPagePanel, 1, 1);
            _LoadPageLayout.Location = new System.Drawing.Point(3, 63);
            _LoadPageLayout.Name = "_LoadPageLayout";
            _LoadPageLayout.RowCount = 3;
            _LoadPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _LoadPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _LoadPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _LoadPageLayout.Size = new System.Drawing.Size(327, 142);
            _LoadPageLayout.TabIndex = 1;
            // 
            // _LoadPagePanel
            // 
            _LoadPagePanel.Controls.Add(_LoadResistanceNumeric);
            _LoadPagePanel.Controls.Add(_LoadResistanceNumericLabel);
            _LoadPagePanel.Controls.Add(_LoadCompensationTextBox);
            _LoadPagePanel.Controls.Add(__AcquireLoadCompensationButton);
            _LoadPagePanel.Location = new System.Drawing.Point(34, -27);
            _LoadPagePanel.Name = "_LoadPagePanel";
            _LoadPagePanel.Size = new System.Drawing.Size(259, 196);
            _LoadPagePanel.TabIndex = 0;
            // 
            // _LoadResistanceNumeric
            // 
            _LoadResistanceNumeric.DecimalPlaces = 2;
            _LoadResistanceNumeric.Location = new System.Drawing.Point(131, 23);
            _LoadResistanceNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _LoadResistanceNumeric.Name = "_LoadResistanceNumeric";
            _LoadResistanceNumeric.Size = new System.Drawing.Size(59, 25);
            _LoadResistanceNumeric.TabIndex = 7;
            _LoadResistanceNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _LoadResistanceNumericLabel
            // 
            _LoadResistanceNumericLabel.AutoSize = true;
            _LoadResistanceNumericLabel.Location = new System.Drawing.Point(69, 27);
            _LoadResistanceNumericLabel.Name = "_LoadResistanceNumericLabel";
            _LoadResistanceNumericLabel.Size = new System.Drawing.Size(59, 17);
            _LoadResistanceNumericLabel.TabIndex = 6;
            _LoadResistanceNumericLabel.Text = "R [Ohm]:";
            // 
            // _LoadCompensationTextBox
            // 
            _LoadCompensationTextBox.Location = new System.Drawing.Point(10, 132);
            _LoadCompensationTextBox.Multiline = true;
            _LoadCompensationTextBox.Name = "_LoadCompensationTextBox";
            _LoadCompensationTextBox.ReadOnly = true;
            _LoadCompensationTextBox.Size = new System.Drawing.Size(239, 52);
            _LoadCompensationTextBox.TabIndex = 1;
            // 
            // _AcquireLoadCompensationButton
            // 
            __AcquireLoadCompensationButton.Location = new System.Drawing.Point(74, 65);
            __AcquireLoadCompensationButton.Name = "__AcquireLoadCompensationButton";
            __AcquireLoadCompensationButton.Size = new System.Drawing.Size(110, 54);
            __AcquireLoadCompensationButton.TabIndex = 0;
            __AcquireLoadCompensationButton.Text = "Acquire Load Compensation";
            __AcquireLoadCompensationButton.UseVisualStyleBackColor = true;
            // 
            // _ShortWizardPage
            // 
            _ShortWizardPage.Controls.Add(_ShortPageLayout);
            _ShortWizardPage.Description = "Short compensation";
            _ShortWizardPage.Location = new System.Drawing.Point(0, 0);
            _ShortWizardPage.Name = "_ShortWizardPage";
            _ShortWizardPage.Size = new System.Drawing.Size(428, 208);
            _ShortWizardPage.TabIndex = 16;
            _ShortWizardPage.Title = "Short";
            // 
            // _ShortPageLayout
            // 
            _ShortPageLayout.ColumnCount = 3;
            _ShortPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShortPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _ShortPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShortPageLayout.Controls.Add(_ShortPagePanel, 1, 1);
            _ShortPageLayout.Location = new System.Drawing.Point(3, 66);
            _ShortPageLayout.Name = "_ShortPageLayout";
            _ShortPageLayout.RowCount = 3;
            _ShortPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShortPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _ShortPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _ShortPageLayout.Size = new System.Drawing.Size(291, 266);
            _ShortPageLayout.TabIndex = 1;
            // 
            // _ShortPagePanel
            // 
            _ShortPagePanel.Controls.Add(_ShortCompensationValuesTextBox);
            _ShortPagePanel.Controls.Add(__AcquireShortCompensationButton);
            _ShortPagePanel.Location = new System.Drawing.Point(16, 62);
            _ShortPagePanel.Name = "_ShortPagePanel";
            _ShortPagePanel.Size = new System.Drawing.Size(259, 142);
            _ShortPagePanel.TabIndex = 0;
            // 
            // _ShortCompensationValuesTextBox
            // 
            _ShortCompensationValuesTextBox.Location = new System.Drawing.Point(10, 81);
            _ShortCompensationValuesTextBox.Multiline = true;
            _ShortCompensationValuesTextBox.Name = "_ShortCompensationValuesTextBox";
            _ShortCompensationValuesTextBox.ReadOnly = true;
            _ShortCompensationValuesTextBox.Size = new System.Drawing.Size(239, 52);
            _ShortCompensationValuesTextBox.TabIndex = 1;
            // 
            // _AcquireShortCompensationButton
            // 
            __AcquireShortCompensationButton.Location = new System.Drawing.Point(74, 14);
            __AcquireShortCompensationButton.Name = "__AcquireShortCompensationButton";
            __AcquireShortCompensationButton.Size = new System.Drawing.Size(110, 54);
            __AcquireShortCompensationButton.TabIndex = 0;
            __AcquireShortCompensationButton.Text = "Acquire Short Compensation";
            __AcquireShortCompensationButton.UseVisualStyleBackColor = true;
            // 
            // _OpenWizardPage
            // 
            _OpenWizardPage.Controls.Add(_OpenPageLayout);
            _OpenWizardPage.Description = "Open compensation";
            _OpenWizardPage.Location = new System.Drawing.Point(0, 0);
            _OpenWizardPage.Name = "_OpenWizardPage";
            _OpenWizardPage.Size = new System.Drawing.Size(428, 208);
            _OpenWizardPage.TabIndex = 15;
            _OpenWizardPage.Title = "Open";
            // 
            // _OpenPageLayout
            // 
            _OpenPageLayout.ColumnCount = 3;
            _OpenPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _OpenPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _OpenPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _OpenPageLayout.Controls.Add(_OpenPagePanel, 1, 1);
            _OpenPageLayout.Location = new System.Drawing.Point(4, 67);
            _OpenPageLayout.Name = "_OpenPageLayout";
            _OpenPageLayout.RowCount = 3;
            _OpenPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _OpenPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _OpenPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _OpenPageLayout.Size = new System.Drawing.Size(342, 243);
            _OpenPageLayout.TabIndex = 1;
            // 
            // _OpenPagePanel
            // 
            _OpenPagePanel.Controls.Add(_OpenCompensationValuesTextBox);
            _OpenPagePanel.Controls.Add(__AcquireOpenCompensationButton);
            _OpenPagePanel.Location = new System.Drawing.Point(41, 50);
            _OpenPagePanel.Name = "_OpenPagePanel";
            _OpenPagePanel.Size = new System.Drawing.Size(259, 142);
            _OpenPagePanel.TabIndex = 0;
            // 
            // _OpenCompensationValuesTextBox
            // 
            _OpenCompensationValuesTextBox.Location = new System.Drawing.Point(10, 81);
            _OpenCompensationValuesTextBox.Multiline = true;
            _OpenCompensationValuesTextBox.Name = "_OpenCompensationValuesTextBox";
            _OpenCompensationValuesTextBox.ReadOnly = true;
            _OpenCompensationValuesTextBox.Size = new System.Drawing.Size(239, 52);
            _OpenCompensationValuesTextBox.TabIndex = 1;
            // 
            // _AcquireOpenCompensationButton
            // 
            __AcquireOpenCompensationButton.Location = new System.Drawing.Point(74, 14);
            __AcquireOpenCompensationButton.Name = "__AcquireOpenCompensationButton";
            __AcquireOpenCompensationButton.Size = new System.Drawing.Size(110, 54);
            __AcquireOpenCompensationButton.TabIndex = 0;
            __AcquireOpenCompensationButton.Text = "Acquire Open Compensation";
            __AcquireOpenCompensationButton.UseVisualStyleBackColor = true;
            // 
            // _StatusStrip
            // 
            _StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _StatusLabel, _ProgressBar });
            _StatusStrip.Location = new System.Drawing.Point(1, 380);
            _StatusStrip.Name = "_StatusStrip";
            _StatusStrip.Size = new System.Drawing.Size(403, 22);
            _StatusStrip.TabIndex = 3;
            _StatusStrip.Text = "StatusStrip1";
            // 
            // _StatusLabel
            // 
            _StatusLabel.Name = "_StatusLabel";
            _StatusLabel.Size = new System.Drawing.Size(290, 17);
            _StatusLabel.Spring = true;
            _StatusLabel.Text = "Status: ";
            // 
            // _ProgressBar
            // 
            _ProgressBar.Name = "_ProgressBar";
            _ProgressBar.Size = new System.Drawing.Size(100, 16);
            // 
            // _YardstickWizardPage
            // 
            _YardstickWizardPage.Controls.Add(_YardstickPageLayout);
            _YardstickWizardPage.Description = "Validation step";
            _YardstickWizardPage.Location = new System.Drawing.Point(0, 0);
            _YardstickWizardPage.Name = "_YardstickWizardPage";
            _YardstickWizardPage.Size = new System.Drawing.Size(428, 208);
            _YardstickWizardPage.TabIndex = 18;
            _YardstickWizardPage.Title = "Yardstick";
            // 
            // _ApertureNumeric
            // 
            _ApertureNumeric.Location = new System.Drawing.Point(73, 30);
            _ApertureNumeric.Maximum = new decimal(new int[] { 5, 0, 0, 0 });
            _ApertureNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _ApertureNumeric.Name = "_ApertureNumeric";
            _ApertureNumeric.Size = new System.Drawing.Size(41, 25);
            _ApertureNumeric.TabIndex = 1;
            _ApertureNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _AveragingEnabledCheckBox
            // 
            _AveragingEnabledCheckBox.AutoSize = true;
            _AveragingEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _AveragingEnabledCheckBox.Location = new System.Drawing.Point(10, 94);
            _AveragingEnabledCheckBox.Name = "_AveragingEnabledCheckBox";
            _AveragingEnabledCheckBox.Size = new System.Drawing.Size(77, 21);
            _AveragingEnabledCheckBox.TabIndex = 4;
            _AveragingEnabledCheckBox.Text = "Enabled:";
            _AveragingEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _RestartAveragingButton
            // 
            __RestartAveragingButton.Location = new System.Drawing.Point(31, 120);
            __RestartAveragingButton.Name = "__RestartAveragingButton";
            __RestartAveragingButton.Size = new System.Drawing.Size(76, 28);
            __RestartAveragingButton.TabIndex = 5;
            __RestartAveragingButton.Text = "Restart";
            __RestartAveragingButton.UseVisualStyleBackColor = true;
            // 
            // _AveragingCountNumeric
            // 
            _AveragingCountNumeric.Location = new System.Drawing.Point(73, 63);
            _AveragingCountNumeric.Maximum = new decimal(new int[] { 999, 0, 0, 0 });
            _AveragingCountNumeric.Name = "_AveragingCountNumeric";
            _AveragingCountNumeric.Size = new System.Drawing.Size(50, 25);
            _AveragingCountNumeric.TabIndex = 3;
            _AveragingCountNumeric.Value = new decimal(new int[] { 999, 0, 0, 0 });
            // 
            // _AdapterComboBox
            // 
            _AdapterComboBox.Location = new System.Drawing.Point(17, 54);
            _AdapterComboBox.Name = "_AdapterComboBox";
            _AdapterComboBox.Size = new System.Drawing.Size(108, 25);
            _AdapterComboBox.TabIndex = 1;
            // 
            // _ApertureNumericLabel
            // 
            _ApertureNumericLabel.AutoSize = true;
            _ApertureNumericLabel.Location = new System.Drawing.Point(9, 34);
            _ApertureNumericLabel.Name = "_ApertureNumericLabel";
            _ApertureNumericLabel.Size = new System.Drawing.Size(62, 17);
            _ApertureNumericLabel.TabIndex = 0;
            _ApertureNumericLabel.Text = "Aperture:";
            // 
            // _AveragingGroupBox
            // 
            _AveragingGroupBox.Controls.Add(_ApertureNumeric);
            _AveragingGroupBox.Controls.Add(_AveragingEnabledCheckBox);
            _AveragingGroupBox.Controls.Add(_ApertureNumericLabel);
            _AveragingGroupBox.Controls.Add(__RestartAveragingButton);
            _AveragingGroupBox.Controls.Add(_AveragingCountNumericLabel);
            _AveragingGroupBox.Controls.Add(_AveragingCountNumeric);
            _AveragingGroupBox.Location = new System.Drawing.Point(35, -30);
            _AveragingGroupBox.Name = "_AveragingGroupBox";
            _AveragingGroupBox.Size = new System.Drawing.Size(139, 161);
            _AveragingGroupBox.TabIndex = 0;
            _AveragingGroupBox.TabStop = false;
            _AveragingGroupBox.Text = "Averaging";
            // 
            // _AveragingCountNumericLabel
            // 
            _AveragingCountNumericLabel.AutoSize = true;
            _AveragingCountNumericLabel.Location = new System.Drawing.Point(26, 67);
            _AveragingCountNumericLabel.Name = "_AveragingCountNumericLabel";
            _AveragingCountNumericLabel.Size = new System.Drawing.Size(45, 17);
            _AveragingCountNumericLabel.TabIndex = 2;
            _AveragingCountNumericLabel.Text = "Count:";
            // 
            // _SettingsPageLayout
            // 
            _SettingsPageLayout.Anchor = System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right;

            _SettingsPageLayout.ColumnCount = 4;
            _SettingsPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SettingsPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _SettingsPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _SettingsPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SettingsPageLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _SettingsPageLayout.Controls.Add(_AveragingGroupBox, 1, 1);
            _SettingsPageLayout.Controls.Add(_CompensationGroupBox, 2, 1);
            _SettingsPageLayout.Location = new System.Drawing.Point(3, 64);
            _SettingsPageLayout.Name = "_SettingsPageLayout";
            _SettingsPageLayout.RowCount = 3;
            _SettingsPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SettingsPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _SettingsPageLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SettingsPageLayout.Size = new System.Drawing.Size(361, 101);
            _SettingsPageLayout.TabIndex = 1;
            // 
            // _CompensationGroupBox
            // 
            _CompensationGroupBox.Controls.Add(__ApplySettingsButton);
            _CompensationGroupBox.Controls.Add(_AdapterComboBox);
            _CompensationGroupBox.Controls.Add(_AdapterComboBoxLabel);
            _CompensationGroupBox.Location = new System.Drawing.Point(180, -30);
            _CompensationGroupBox.Name = "_CompensationGroupBox";
            _CompensationGroupBox.Size = new System.Drawing.Size(145, 161);
            _CompensationGroupBox.TabIndex = 1;
            _CompensationGroupBox.TabStop = false;
            _CompensationGroupBox.Text = "Compensation";
            // 
            // _ApplySettingsButton
            // 
            __ApplySettingsButton.Location = new System.Drawing.Point(31, 120);
            __ApplySettingsButton.Name = "__ApplySettingsButton";
            __ApplySettingsButton.Size = new System.Drawing.Size(76, 28);
            __ApplySettingsButton.TabIndex = 2;
            __ApplySettingsButton.Text = "Apply";
            __ApplySettingsButton.UseVisualStyleBackColor = true;
            // 
            // _AdapterComboBoxLabel
            // 
            _AdapterComboBoxLabel.AutoSize = true;
            _AdapterComboBoxLabel.Location = new System.Drawing.Point(14, 34);
            _AdapterComboBoxLabel.Name = "_AdapterComboBoxLabel";
            _AdapterComboBoxLabel.Size = new System.Drawing.Size(58, 17);
            _AdapterComboBoxLabel.TabIndex = 0;
            _AdapterComboBoxLabel.Text = "Adapter:";
            // 
            // _SettingsWizardPage
            // 
            _SettingsWizardPage.Controls.Add(_SettingsPageLayout);
            _SettingsWizardPage.Description = "Enter settings";
            _SettingsWizardPage.Location = new System.Drawing.Point(0, 0);
            _SettingsWizardPage.Name = "_SettingsWizardPage";
            _SettingsWizardPage.Size = new System.Drawing.Size(428, 208);
            _SettingsWizardPage.TabIndex = 0;
            _SettingsWizardPage.Title = "Settings";
            // 
            // _Wizard
            // 
            __Wizard.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            __Wizard.Controls.Add(_WelcomeWizardPage);
            __Wizard.Controls.Add(_FinishWizardPage);
            __Wizard.Controls.Add(_YardstickWizardPage);
            __Wizard.Controls.Add(_LoadWizardPage);
            __Wizard.Controls.Add(_ShortWizardPage);
            __Wizard.Controls.Add(_OpenWizardPage);
            __Wizard.Controls.Add(_SettingsWizardPage);
            __Wizard.FinishText = null;
            __Wizard.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __Wizard.HelpVisible = true;
            __Wizard.Location = new System.Drawing.Point(1, 1);
            __Wizard.Name = "__Wizard";
            __Wizard.Pages.AddRange(new Core.Controls.WizardPage[] { _WelcomeWizardPage, _SettingsWizardPage, _OpenWizardPage, _ShortWizardPage, _LoadWizardPage, _YardstickWizardPage, _FinishWizardPage });
            __Wizard.Size = new System.Drawing.Size(403, 379);
            __Wizard.TabIndex = 2;
            // 
            // _WelcomeWizardPage
            // 
            _WelcomeWizardPage.Description = "This wizard will guide you through the steps of performing a compensation task.";
            _WelcomeWizardPage.Location = new System.Drawing.Point(0, 0);
            _WelcomeWizardPage.Name = "_WelcomeWizardPage";
            _WelcomeWizardPage.Size = new System.Drawing.Size(403, 331);
            _WelcomeWizardPage.TabIndex = 9;
            _WelcomeWizardPage.Title = "Compensation Wizard";
            _WelcomeWizardPage.WizardPageStyle = Core.Controls.WizardPageStyle.Welcome;
            // 
            // _FinishWizardPage
            // 
            _FinishWizardPage.Description = "Thank you for using the compensation wizard." + '\n' + " Press OK to exit and save or Cancel" + " to exit without saving.";
            _FinishWizardPage.Location = new System.Drawing.Point(0, 0);
            _FinishWizardPage.Name = "_FinishWizardPage";
            _FinishWizardPage.Size = new System.Drawing.Size(407, 335);
            _FinishWizardPage.TabIndex = 12;
            _FinishWizardPage.Title = "Complete Compensation";
            _FinishWizardPage.WizardPageStyle = Core.Controls.WizardPageStyle.Finish;
            // 
            // CompensateView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(__Wizard);
            Controls.Add(_StatusStrip);
            Name = "CompensateView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(405, 403);
            _YardstickPageLayout.ResumeLayout(false);
            _YardstickPagePanel.ResumeLayout(false);
            _YardstickPagePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__YardstickInductanceLimitNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__YardstickAcceptanceToleranceNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_YardstickResistanceNumeric).EndInit();
            _LoadWizardPage.ResumeLayout(false);
            _LoadPageLayout.ResumeLayout(false);
            _LoadPagePanel.ResumeLayout(false);
            _LoadPagePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_LoadResistanceNumeric).EndInit();
            _ShortWizardPage.ResumeLayout(false);
            _ShortPageLayout.ResumeLayout(false);
            _ShortPagePanel.ResumeLayout(false);
            _ShortPagePanel.PerformLayout();
            _OpenWizardPage.ResumeLayout(false);
            _OpenPageLayout.ResumeLayout(false);
            _OpenPagePanel.ResumeLayout(false);
            _OpenPagePanel.PerformLayout();
            _StatusStrip.ResumeLayout(false);
            _StatusStrip.PerformLayout();
            _YardstickWizardPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_AveragingCountNumeric).EndInit();
            _AveragingGroupBox.ResumeLayout(false);
            _AveragingGroupBox.PerformLayout();
            _SettingsPageLayout.ResumeLayout(false);
            _CompensationGroupBox.ResumeLayout(false);
            _CompensationGroupBox.PerformLayout();
            _SettingsWizardPage.ResumeLayout(false);
            __Wizard.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.TableLayoutPanel _YardstickPageLayout;
        private System.Windows.Forms.Panel _YardstickPagePanel;
        private System.Windows.Forms.NumericUpDown __YardstickInductanceLimitNumeric;

        private System.Windows.Forms.NumericUpDown _YardstickInductanceLimitNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __YardstickInductanceLimitNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__YardstickInductanceLimitNumeric != null)
                {
                    __YardstickInductanceLimitNumeric.ValueChanged -= YardstickInductanceLimitNumeric_ValueChanged;
                }

                __YardstickInductanceLimitNumeric = value;
                if (__YardstickInductanceLimitNumeric != null)
                {
                    __YardstickInductanceLimitNumeric.ValueChanged += YardstickInductanceLimitNumeric_ValueChanged;
                }
            }
        }

        private System.Windows.Forms.TextBox _YardstickMeasuredInductanceTextBox;
        private System.Windows.Forms.Label _YardstickMeasuredInductanceTextBoxLabel;
        private System.Windows.Forms.Label _YardstickDeltaLabel;
        private System.Windows.Forms.Label _YardstickActualLabel;
        private System.Windows.Forms.TextBox _YardstickResistanceDeviationTextBox;
        private System.Windows.Forms.TextBox _MeasuredYardstickResistanceTextBox;
        private System.Windows.Forms.NumericUpDown __YardstickAcceptanceToleranceNumeric;

        private System.Windows.Forms.NumericUpDown _YardstickAcceptanceToleranceNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __YardstickAcceptanceToleranceNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__YardstickAcceptanceToleranceNumeric != null)
                {
                    __YardstickAcceptanceToleranceNumeric.ValueChanged -= YardstickAcceptanceToleranceNumeric_ValueChanged;
                }

                __YardstickAcceptanceToleranceNumeric = value;
                if (__YardstickAcceptanceToleranceNumeric != null)
                {
                    __YardstickAcceptanceToleranceNumeric.ValueChanged += YardstickAcceptanceToleranceNumeric_ValueChanged;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _YardstickResistanceNumeric;
        private System.Windows.Forms.Label _YardstickAcceptanceToleranceNumericLabel;
        private System.Windows.Forms.Label _YardstickResistanceNumericLabel;
        private System.Windows.Forms.TextBox _YardstickValuesTextBox;
        private System.Windows.Forms.Button __MeasureYardstickButton;

        private System.Windows.Forms.Button _MeasureYardstickButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureYardstickButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureYardstickButton != null)
                {
                    __MeasureYardstickButton.Click -= MeasureYardstickButton_Click;
                }

                __MeasureYardstickButton = value;
                if (__MeasureYardstickButton != null)
                {
                    __MeasureYardstickButton.Click += MeasureYardstickButton_Click;
                }
            }
        }

        private Core.Controls.Wizard __Wizard;

        private Core.Controls.Wizard _Wizard
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Wizard;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Wizard != null)
                {
                    __Wizard.PageChanged -= Wizard_AfterSwitchPages;
                    __Wizard.PageChanging -= Wizard_BeforeSwitchPages;
                    __Wizard.Cancel -= Wizard_Cancel;
                    __Wizard.Finish -= Wizard_Finish;
                    __Wizard.Help -= Wizard_Help;
                }

                __Wizard = value;
                if (__Wizard != null)
                {
                    __Wizard.PageChanged += Wizard_AfterSwitchPages;
                    __Wizard.PageChanging += Wizard_BeforeSwitchPages;
                    __Wizard.Cancel += Wizard_Cancel;
                    __Wizard.Finish += Wizard_Finish;
                    __Wizard.Help += Wizard_Help;
                }
            }
        }

        private Core.Controls.WizardPage _FinishWizardPage;
        private Core.Controls.WizardPage _YardstickWizardPage;
        private Core.Controls.WizardPage _LoadWizardPage;
        private System.Windows.Forms.TableLayoutPanel _LoadPageLayout;
        private System.Windows.Forms.Panel _LoadPagePanel;
        private System.Windows.Forms.NumericUpDown _LoadResistanceNumeric;
        private System.Windows.Forms.Label _LoadResistanceNumericLabel;
        private System.Windows.Forms.TextBox _LoadCompensationTextBox;
        private System.Windows.Forms.Button __AcquireLoadCompensationButton;

        private System.Windows.Forms.Button _AcquireLoadCompensationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcquireLoadCompensationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcquireLoadCompensationButton != null)
                {
                    __AcquireLoadCompensationButton.ContextMenuStripChanged -= AcquireLoadCompensationButton_ContextMenuStripChanged;
                }

                __AcquireLoadCompensationButton = value;
                if (__AcquireLoadCompensationButton != null)
                {
                    __AcquireLoadCompensationButton.ContextMenuStripChanged += AcquireLoadCompensationButton_ContextMenuStripChanged;
                }
            }
        }

        private Core.Controls.WizardPage _ShortWizardPage;
        private System.Windows.Forms.TableLayoutPanel _ShortPageLayout;
        private System.Windows.Forms.Panel _ShortPagePanel;
        private System.Windows.Forms.TextBox _ShortCompensationValuesTextBox;
        private System.Windows.Forms.Button __AcquireShortCompensationButton;

        private System.Windows.Forms.Button _AcquireShortCompensationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcquireShortCompensationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcquireShortCompensationButton != null)
                {
                    __AcquireShortCompensationButton.ContextMenuStripChanged -= AcquireShortCompensationButton_ContextMenuStripChanged;
                }

                __AcquireShortCompensationButton = value;
                if (__AcquireShortCompensationButton != null)
                {
                    __AcquireShortCompensationButton.ContextMenuStripChanged += AcquireShortCompensationButton_ContextMenuStripChanged;
                }
            }
        }

        private Core.Controls.WizardPage _OpenWizardPage;
        private System.Windows.Forms.TableLayoutPanel _OpenPageLayout;
        private System.Windows.Forms.Panel _OpenPagePanel;
        private System.Windows.Forms.TextBox _OpenCompensationValuesTextBox;
        private System.Windows.Forms.Button __AcquireOpenCompensationButton;

        private System.Windows.Forms.Button _AcquireOpenCompensationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcquireOpenCompensationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcquireOpenCompensationButton != null)
                {
                    __AcquireOpenCompensationButton.ContextMenuStripChanged -= AcquireOpenCompensationButton_ContextMenuStripChanged;
                }

                __AcquireOpenCompensationButton = value;
                if (__AcquireOpenCompensationButton != null)
                {
                    __AcquireOpenCompensationButton.ContextMenuStripChanged += AcquireOpenCompensationButton_ContextMenuStripChanged;
                }
            }
        }

        private Core.Controls.WizardPage _SettingsWizardPage;
        private System.Windows.Forms.TableLayoutPanel _SettingsPageLayout;
        private System.Windows.Forms.GroupBox _AveragingGroupBox;
        private System.Windows.Forms.NumericUpDown _ApertureNumeric;
        private System.Windows.Forms.CheckBox _AveragingEnabledCheckBox;
        private System.Windows.Forms.Label _ApertureNumericLabel;
        private System.Windows.Forms.Button __RestartAveragingButton;

        private System.Windows.Forms.Button _RestartAveragingButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RestartAveragingButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RestartAveragingButton != null)
                {
                    __RestartAveragingButton.Click -= RestartAveragingButton_Click;
                }

                __RestartAveragingButton = value;
                if (__RestartAveragingButton != null)
                {
                    __RestartAveragingButton.Click += RestartAveragingButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _AveragingCountNumericLabel;
        private System.Windows.Forms.NumericUpDown _AveragingCountNumeric;
        private System.Windows.Forms.GroupBox _CompensationGroupBox;
        private System.Windows.Forms.ComboBox _AdapterComboBox;
        private System.Windows.Forms.Label _AdapterComboBoxLabel;
        private Core.Controls.WizardPage _WelcomeWizardPage;
        private System.Windows.Forms.StatusStrip _StatusStrip;
        private System.Windows.Forms.ToolStripStatusLabel _StatusLabel;
        private System.Windows.Forms.ToolStripProgressBar _ProgressBar;
        private System.Windows.Forms.Timer _LongTaskTimer;
        private System.Windows.Forms.Button __ApplySettingsButton;

        private System.Windows.Forms.Button _ApplySettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySettingsButton != null)
                {
                    __ApplySettingsButton.Click -= ApplySettingsButton_Click;
                }

                __ApplySettingsButton = value;
                if (__ApplySettingsButton != null)
                {
                    __ApplySettingsButton.Click += ApplySettingsButton_Click;
                }
            }
        }
    }
}