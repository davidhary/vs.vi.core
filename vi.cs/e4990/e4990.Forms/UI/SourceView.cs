﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

namespace isr.VI.E4990.Forms
{

    /// <summary> A Source view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SourceView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public SourceView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ApplySourceSettingButton.Name = "_ApplySourceSettingButton";
            this.__SourceFunctionComboBox.Name = "_SourceFunctionComboBox";
            this.__ApplySourceFunctionButton.Name = "_ApplySourceFunctionButton";
        }

        /// <summary> Creates a new <see cref="SourceView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="SourceView"/>. </returns>
        public static SourceView Create()
        {
            SourceView view = null;
            try
            {
                view = new SourceView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindSourceChannelSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SOURCE "

        /// <summary> Gets or sets the SourceChannel subsystem. </summary>
        /// <value> The SourceChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SourceChannelSubsystem SourceChannelSubsystem { get; private set; }

        /// <summary> Bind SourceChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSourceChannelSubsystem( E4990Device device )
        {
            if ( this.SourceChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.SourceChannelSubsystem );
                this.SourceChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.SourceChannelSubsystem = device.SourceChannelSubsystem;
                this.BindSubsystem( true, this.SourceChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SourceChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SourceChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.SourceChannelSubsystem.SupportedFunctionModes ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SourceChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Source channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SourceChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SourceChannelSubsystemBase.SupportedFunctionModes ):
                    {
                        this._SourceFunctionComboBox.ListSupportedSourceFunctionModes( subsystem.SupportedFunctionModes );
                        break;
                    }
                // Imports isr.VI.Facade.ComboBoxExtensions
                case nameof( SourceChannelSubsystemBase.FunctionMode ):
                    {
                        this.SelectSourceFunctionMode();
                        break;
                    }

                case nameof( SourceChannelSubsystemBase.Level ):
                    {
                        if ( subsystem.Level.HasValue )
                            this.SourceLevel = subsystem.Level.Value;
                        break;
                    }
            }
        }

        /// <summary> Source channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SourceChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SourceChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SourceChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SourceChannelSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: SOURCE "

        /// <summary> Gets or sets source level. </summary>
        /// <value> The source level. </value>
        private double SourceLevel
        {
            get => this.SelectedSourceFunctionMode == SourceFunctionModes.Voltage ? ( double ) this._LevelNumeric.Value : 0.001d * ( double ) this._LevelNumeric.Value;

            set => this._LevelNumeric.Value = this.SelectedSourceFunctionMode == SourceFunctionModes.Voltage ? ( decimal ) value : ( decimal ) (1000d * value);
        }

        /// <summary> Gets the selected source function mode. </summary>
        /// <value> The selected source function mode. </value>
        public SourceFunctionModes SelectedSourceFunctionMode => this._SourceFunctionComboBox.SelectedSourceFunctionModes();

        /// <summary> Applies the source setting button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySourceSettingButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying source settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                // Set OSC mode
                _ = this.Device.SourceChannelSubsystem.ApplyFunctionMode( this.SelectedSourceFunctionMode );
                _ = this.Device.SourceChannelSubsystem.ApplyLevel( this.SourceLevel );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Select source function mode. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void SelectSourceFunctionMode()
        {
            _ = this._SourceFunctionComboBox.SafeSelectSourceFunctionModes( this.Device.SourceChannelSubsystem.FunctionMode );
            if ( this.Device.SourceChannelSubsystem.FunctionMode.HasValue )
            {
                if ( this.Device.SourceChannelSubsystem.FunctionMode.Value == SourceFunctionModes.Current )
                {
                    this._LevelNumeric.Minimum = ( decimal ) (1000d * this.Device.SourceChannelSubsystem.FunctionRange.Min);
                    this._LevelNumeric.Maximum = ( decimal ) (1000d * this.Device.SourceChannelSubsystem.FunctionRange.Max);
                    this._LevelNumeric.DecimalPlaces = Math.Max( 0, this.Device.SourceChannelSubsystem.FunctionRange.DecimalPlaces - 3 );
                    this._LevelNumeric.Increment = this._LevelNumeric.Minimum > 0m ? this._LevelNumeric.Minimum : 0.1m * (this._LevelNumeric.Maximum - this._LevelNumeric.Minimum);
                }
                else
                {
                    this._LevelNumeric.Minimum = ( decimal ) this.Device.SourceChannelSubsystem.FunctionRange.Min;
                    this._LevelNumeric.Maximum = ( decimal ) this.Device.SourceChannelSubsystem.FunctionRange.Max;
                    this._LevelNumeric.DecimalPlaces = this.Device.SourceChannelSubsystem.FunctionRange.DecimalPlaces;
                    this._LevelNumeric.Increment = this._LevelNumeric.Minimum > 0m ? this._LevelNumeric.Minimum : 0.1m * (this._LevelNumeric.Maximum - this._LevelNumeric.Minimum);
                }
            }
        }

        /// <summary> Source function combo box selected value changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SourceFunctionComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} toggling source settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this._LevelNumericLabel.Text = this.SelectedSourceFunctionMode == SourceFunctionModes.Voltage ? "Level [V]:" : "Level [mA]:";
                this._LevelNumericLabel.Left = this._LevelNumeric.Left - this._LevelNumericLabel.Width;
                this._LevelNumericLabel.Invalidate();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the source function button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySourceFunctionButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying source settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                // Set OSC mode
                _ = this.Device.SourceChannelSubsystem.ApplyFunctionMode( this.SelectedSourceFunctionMode );
                _ = this.Device.SourceChannelSubsystem.QueryLevel();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}