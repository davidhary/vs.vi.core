using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{

    /// <summary> A Calibrate view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class CalibrateView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public CalibrateView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__AcquireCompensationButton.Name = "_AcquireCompensationButton";
            this.__ApplyLoadButton.Name = "_ApplyLoadButton";
            this.__ApplyShortButton.Name = "_ApplyShortButton";
            this.__ApplyOpenButton.Name = "_ApplyOpenButton";
        }

        /// <summary> Creates a new <see cref="CalibrateView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="CalibrateView"/>. </returns>
        public static CalibrateView Create()
        {
            CalibrateView view = null;
            try
            {
                view = new CalibrateView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindSenseChannelSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DATA BINDING "

        /// <summary> Bind controls settings. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void BindControlsSettings()
        {
            var MySettings2 = E4990.My.MySettings.Default;
            this._FrequencyStimulusTextBox.DataBindings.Add( new Binding( "Text", MySettings2, "FrequencyArrayReading", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._FrequencyStimulusTextBox.Text = MySettings2.FrequencyArrayReading;
            this._LoadCompensationTextBox.DataBindings.Add( new Binding( "Text", MySettings2, "LoadCompensationReading", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._LoadCompensationTextBox.Text = MySettings2.LoadCompensationReading;
            this._ShortCompensationTextBox.DataBindings.Add( new Binding( "Text", MySettings2, "ShortCompensationReading", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._ShortCompensationTextBox.Text = MySettings2.ShortCompensationReading;
            this._OpenCompensationTextBox.DataBindings.Add( new Binding( "Text", MySettings2, "OpenCompensationReading", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._OpenCompensationTextBox.Text = MySettings2.OpenCompensationReading;
        }

        #endregion

        #region " SENSE "

        /// <summary> Gets the SenseChannel subsystem. </summary>
        /// <value> The SenseChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseChannelSubsystem SenseChannelSubsystem { get; private set; }

        /// <summary> Bind SenseChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseChannelSubsystem( E4990Device device )
        {
            if ( this.SenseChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseChannelSubsystem );
                this.SenseChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseChannelSubsystem = device.SenseChannelSubsystem;
                this.BindSubsystem( true, this.SenseChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.SenseChannelSubsystem.SupportedAdapterTypes ) );
                this.HandlePropertyChanged( subsystem, nameof( E4990.SenseChannelSubsystem.AdapterType ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the sense channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SenseChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SenseChannelSubsystemBase.SupportedAdapterTypes ):
                    {
                        this._AdapterComboBox.ListSupportedAdapters( subsystem.SupportedAdapterTypes );
                        break;
                    }

                case nameof( SenseChannelSubsystemBase.AdapterType ):
                    {
                        if ( subsystem.AdapterType.HasValue )
                            _ = this.SelectAdapter( subsystem.AdapterType.Value );
                        break;
                    }
            }
        }

        /// <summary> Sense channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SenseChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: SENSE / CAL "

        /// <summary> Selects a new Adapter to display. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        /// <returns> A Scpi.AdapterType. </returns>
        internal AdapterTypes SelectAdapter( string value )
        {
            var v = AdapterTypes.None;
            if ( this.InitializingComponents || string.IsNullOrWhiteSpace( value ) )
                return v;
            if ( Enum.GetNames( typeof( AdapterTypes ) ).Contains( value ) )
            {
                v = ( AdapterTypes ) Conversions.ToInteger( Enum.Parse( typeof( AdapterTypes ), value ) );
            }

            return this.SelectAdapter( v );
        }

        /// <summary> Selects a new Adapter to display. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        /// <returns> A Scpi.AdapterType. </returns>
        internal AdapterTypes SelectAdapter( AdapterTypes value )
        {
            if ( this.InitializingComponents || value == AdapterTypes.None )
                return AdapterTypes.None;
            if ( value != AdapterTypes.None && value != this.SelectedAdapterType )
            {
                _ = this._AdapterComboBox.SelectItem( value.ValueDescriptionPair() );
            }

            return this.SelectedAdapterType;
        }

        /// <summary> Gets the type of the selected Adapter. </summary>
        /// <value> The type of the selected Adapter. </value>
        private AdapterTypes SelectedAdapterType => ( AdapterTypes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._AdapterComboBox.SelectedItem).Key );

        /// <summary> Acquires the compensation button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireCompensationButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring compensation";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                using var w = new CompensationWizard();
                var result = w.ShowDialog( this );
                if ( result == DialogResult.OK )
                {
                    E4990.My.MySettings.Default.AdapterType = this.Device.SenseChannelSubsystem.AdapterType.ToString();
                    E4990.My.MySettings.Default.FrequencyArrayReading = this.Device.CompensateOpenSubsystem.FrequencyArrayReading;
                    E4990.My.MySettings.Default.OpenCompensationReading = this.Device.CompensateOpenSubsystem.ImpedanceArrayReading;
                    E4990.My.MySettings.Default.ShortCompensationReading = this.Device.CompensateShortSubsystem.ImpedanceArrayReading;
                    E4990.My.MySettings.Default.LoadCompensationReading = this.Device.CompensateLoadSubsystem.ImpedanceArrayReading;
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the load button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyLoadButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying load compensation";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.Device.CompensateLoadSubsystem.ApplyImpedanceArray( this._LoadCompensationTextBox.Text );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the short button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyShortButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying short compensation";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.Device.CompensateShortSubsystem.ApplyImpedanceArray( this._ShortCompensationTextBox.Text );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Applies the open button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyOpenButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying open compensation";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.Device.SenseChannelSubsystem.ApplyAdapterType( this.SelectedAdapterType );
                _ = this.Device.CompensateOpenSubsystem.ApplyImpedanceArray( this._OpenCompensationTextBox.Text );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
