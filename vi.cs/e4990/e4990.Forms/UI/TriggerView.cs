﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Forma;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

namespace isr.VI.E4990.Forms
{

    /// <summary> A trigger view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class TriggerView : ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public TriggerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ApplyTriggerModeButton.Name = "_ApplyTriggerModeButton";
        }

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindChannelTriggerSubsystem( value );
            this.BindTriggerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL TRIGGER "

        /// <summary> Gets or sets the ChannelTrigger subsystem. </summary>
        /// <value> The ChannelTrigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ChannelTriggerSubsystem ChannelTriggerSubsystem { get; private set; }

        /// <summary> Bind ChannelTrigger subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindChannelTriggerSubsystem( E4990Device device )
        {
            if ( this.ChannelTriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.ChannelTriggerSubsystem );
                this.ChannelTriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.ChannelTriggerSubsystem = device.ChannelTriggerSubsystem;
                this.BindSubsystem( true, this.ChannelTriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ChannelTriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ChannelTriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.ChannelTriggerSubsystem.ContinuousEnabled ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ChannelTriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the channel Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelTriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( ChannelTriggerSubsystemBase.ContinuousEnabled ):
                    {
                        if ( subsystem.ContinuousEnabled.HasValue )
                            this._ContinuousTriggerCheckBox.Checked = subsystem.ContinuousEnabled.Value;
                        break;
                    }
            }
        }

        /// <summary> Channel Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelTriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ChannelTriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ChannelTriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelTriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( E4990Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.TriggerSubsystem.TriggerSource ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( TriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TriggerSubsystemBase.TriggerSource ):
                    {
                        _ = this._TriggerSourceCombo.ComboBox.SafeSelectTriggerSources( subsystem.TriggerSource );
                        break;
                    }

                case nameof( TriggerSubsystemBase.Delay ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: TRIGGER "

        /// <summary> Applies the trigger options menu item click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" />
        ///                                                                   instance of this
        ///                                             <see cref="T:System.Windows.Forms.Control" /> 
        /// </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyTriggerModeButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying trigger source";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();

                // Turn on Continuous Activation mode for channel 1
                // Me.Device.Session.Write(":INIT1:CONT ON")
                _ = this.Device.ChannelTriggerSubsystem.ApplyContinuousEnabled( this._ContinuousTriggerCheckBox.Checked );

                // Set trigger source, e.g., 
                // Me.Device.Session.Write(":TRIG:SOUR BUS")
                _ = this.Device.TriggerSubsystem.ApplyTriggerSource( this._TriggerSourceCombo.ComboBox.SelectTriggerSources( new TriggerSources?( TriggerSources.Internal ) ) );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}