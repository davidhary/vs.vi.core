﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{

    /// <summary> A Sense view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SenseView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public SenseView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ApplyAveragingButton.Name = "_ApplyAveragingButton";
            this.__RestartAveragingButton.Name = "_RestartAveragingButton";
            this.__ApplySweepSettingsButton.Name = "_ApplySweepSettingsButton";
            this.__ApplyTracesButton.Name = "_ApplyTracesButton";
            this.__ApplyMarkerSettingsButton.Name = "_ApplyMarkerSettingsButton";
        }

        /// <summary> Creates a new <see cref="SenseView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="SenseView"/>. </returns>
        public static SenseView Create()
        {
            SenseView view = null;
            try
            {
                view = new SenseView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindCalculateChannelSubsystem( value );
            this.BindChannelMarkerSubsystem( value );
            this.BindChannelTraceSubsystem( value.PrimaryChannelTraceSubsystem );
            this.BindSenseChannelSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CALCULATE "

        /// <summary> Gets the CalculateChannel subsystem. </summary>
        /// <value> The CalculateChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public CalculateChannelSubsystem CalculateChannelSubsystem { get; private set; }

        /// <summary> Bind CalculateChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindCalculateChannelSubsystem( E4990Device device )
        {
            if ( this.CalculateChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.CalculateChannelSubsystem );
                this.CalculateChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.CalculateChannelSubsystem = device.CalculateChannelSubsystem;
                this.BindSubsystem( true, this.CalculateChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, CalculateChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.CalculateChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.CalculateChannelSubsystem.AveragingEnabled ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.CalculateChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Calculate channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( CalculateChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( CalculateChannelSubsystemBase.AveragingEnabled ):
                    {
                        this._AveragingEnabledCheckBox.Checked = subsystem.AveragingEnabled.GetValueOrDefault( false );
                        break;
                    }

                case nameof( CalculateChannelSubsystemBase.AverageCount ):
                    {
                        this._AveragingCountNumeric.Value = subsystem.AverageCount.GetValueOrDefault( 0 );
                        break;
                    }

                case nameof( CalculateChannelSubsystemBase.TraceCount ):
                    {
                        if ( subsystem.TraceCount.HasValue )
                            this._TraceGroupBox.Text = $"Traces ({subsystem.TraceCount.Value})";
                        break;
                    }
            }
        }

        /// <summary> Calculate channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CalculateChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( E4990.CalculateChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.CalculateChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as CalculateChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL MARKER "

        /// <summary> Gets the ChannelMarker subsystem. </summary>
        /// <value> The ChannelMarker subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ChannelMarkerSubsystem ChannelMarkerSubsystem { get; private set; }

        /// <summary> Bind ChannelMarker subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindChannelMarkerSubsystem( E4990Device device )
        {
            if ( this.ChannelMarkerSubsystem is object )
            {
                this.BindSubsystem( false, this.ChannelMarkerSubsystem );
                this.ChannelMarkerSubsystem = null;
            }

            if ( device is object )
            {
                this.ChannelMarkerSubsystem = device.ChannelMarkerSubsystem;
                this.BindSubsystem( true, this.ChannelMarkerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ChannelMarkerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ChannelmarkerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.ChannelMarkerSubsystem.Abscissa ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ChannelmarkerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the channel marker subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelMarkerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( E4990.ChannelMarkerSubsystem.Abscissa ):
                    {
                        this._MarkerFrequencyComboBox.Text = subsystem.Abscissa.ToString();
                        break;
                    }
            }
        }

        /// <summary> Channel marker subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelmarkerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( E4990.ChannelMarkerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ChannelmarkerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelMarkerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL TRACE "

        /// <summary> Gets the ChannelTrace subsystem. </summary>
        /// <value> The ChannelTrace subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ChannelTraceSubsystem ChannelTraceSubsystem { get; private set; }

        /// <summary> Bind ChannelTrace subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindChannelTraceSubsystem( ChannelTraceSubsystem subsystem )
        {
            if ( this.ChannelTraceSubsystem is object )
            {
                this.BindSubsystem( false, this.ChannelTraceSubsystem );
                this.ChannelTraceSubsystem = null;
            }

            if ( this.Device is object )
            {
                this.ChannelTraceSubsystem = subsystem;
                this.BindSubsystem( true, this.ChannelTraceSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ChannelTraceSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ChannelTraceSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.ChannelTraceSubsystem.ChannelNumber ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ChannelTraceSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the channel Trace subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelTraceSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            var combo = subsystem.TraceNumber == 1 ? this._PrimaryTraceParameterComboBox : this._SecondaryTraceParameterComboBox;
            switch ( propertyName ?? "" )
            {
                case nameof( ChannelTraceSubsystemBase.SupportedParameters ):
                    {
                        combo.ListSupportedTraceParameters( subsystem.SupportedParameters );
                        break;
                    }

                case nameof( ChannelTraceSubsystemBase.Parameter ):
                    {
                        _ = combo.SafeSelectTraceParameters( subsystem.Parameter );
                        break;
                    }

                case nameof( ChannelTraceSubsystemBase.TraceNumber ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Channel Trace subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelTraceSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ChannelTraceSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ChannelTraceSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelTraceSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Gets the SenseChannel subsystem. </summary>
        /// <value> The SenseChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseChannelSubsystem SenseChannelSubsystem { get; private set; }

        /// <summary> Bind SenseChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseChannelSubsystem( E4990Device device )
        {
            if ( this.SenseChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseChannelSubsystem );
                this.SenseChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseChannelSubsystem = device.SenseChannelSubsystem;
                this.BindSubsystem( true, this.SenseChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.SenseChannelSubsystem.Aperture ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the sense channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SenseChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SenseChannelSubsystemBase.Aperture ):
                    {
                        if ( subsystem.Aperture.HasValue )
                            this._ApertureNumeric.Value = ( decimal ) subsystem.Aperture.Value;
                        break;
                    }

                case nameof( SenseChannelSubsystemBase.SweepPoints ):
                    {
                        if ( subsystem.SweepPoints.HasValue )
                            this._SweepGroupBox.Text = $"Sweep points: {subsystem.SweepPoints.Value}";
                        break;
                    }

                case nameof( SenseChannelSubsystemBase.SweepStart ):
                    {
                        if ( subsystem.SweepStart.HasValue )
                            this._LowFrequencyNumeric.Value = ( decimal ) subsystem.SweepStart.Value;
                        break;
                    }

                case var @case when @case == nameof( SenseChannelSubsystemBase.SweepStart ):
                    {
                        if ( subsystem.SweepStart.HasValue )
                            this._HighFrequencyNumeric.Value = ( decimal ) subsystem.SweepStop.Value;
                        break;
                    }
            }
        }

        /// <summary> Sense channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SenseChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: SENSE "

        #region " SENSE / AVERAGING "

        /// <summary> Applies the averaging button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyAveragingButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying average settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.Device.SenseChannelSubsystem.ApplyAperture( ( double ) this._ApertureNumeric.Value );
                this.Device.CalculateChannelSubsystem.ApplyAverageSettings( this._AveragingEnabledCheckBox.Checked, ( int ) Math.Round( this._AveragingCountNumeric.Value ) );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Restart averaging button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RestartAveragingButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} restarting average";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.CalculateChannelSubsystem.ClearAverage();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SENSE / SWEEP "

        /// <summary> Configure sweep. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="lowFrequency">  The low frequency. </param>
        /// <param name="highFrequency"> The high frequency. </param>
        public void ConfigureSweep( double lowFrequency, double highFrequency )
        {

            // Set number of points
            // Me.Device.Session.Write(":SENS1:SWE:POIN 2")
            _ = this.Device.SenseChannelSubsystem.ApplySweepPoints( 2 );

            // Set start frequency
            // Me.Device.Session.WriteLine(":SENS1:FREQ:STAR {0}", lowFrequency)
            _ = this.Device.SenseChannelSubsystem.ApplySweepStart( lowFrequency );

            // Set stop frequency
            // Me.Device.Session.WriteLine(":SENS1:FREQ:STOP {0}", highFrequency)
            _ = this.Device.SenseChannelSubsystem.ApplySweepStop( highFrequency );
        }

        /// <summary> Applies the sweep settings button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySweepSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying sweep settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this._MarkerFrequencyComboBox.Items.Clear();
                _ = this._MarkerFrequencyComboBox.Items.Add( this._LowFrequencyNumeric.Value.ToString() );
                _ = this._MarkerFrequencyComboBox.Items.Add( this._HighFrequencyNumeric.Value.ToString() );
                this._MarkerFrequencyComboBox.SelectedIndex = 1;
                // set a two point sweep.
                this.ConfigureSweep( ( double ) this._LowFrequencyNumeric.Value, ( double ) this._HighFrequencyNumeric.Value );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SENSE / TRACES "

        /// <summary> Configure sweep. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public void ConfigureTrace()
        {

            // Setup Channel 1
            _ = this.Device.CalculateChannelSubsystem.ApplyTraceCount( 2 );

            // Allocate measurement parameter for trace 1: Rs
            // Me.Device.Session.Write(":CALC1:PAR1:DEF RS")
            _ = this.Device.PrimaryChannelTraceSubsystem.ApplyParameter( this._PrimaryTraceParameterComboBox.SelectedTraceParameters() );
            this.Device.PrimaryChannelTraceSubsystem.AutoScale();

            // Allocate measurement parameter for trace 2: Ls
            // Me.Device.Session.Write(":CALC1:PAR2:DEF LS")
            _ = this.Device.SecondaryChannelTraceSubsystem.ApplyParameter( this._SecondaryTraceParameterComboBox.SelectedTraceParameters() );
            this.Device.SecondaryChannelTraceSubsystem.AutoScale();
        }

        /// <summary> Select primary trace parameter. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.TraceParameters. </returns>
        private TraceParameters SelectPrimaryTraceParameter( TraceParameters value )
        {
            if ( this.InitializingComponents || value == TraceParameters.None )
                return TraceParameters.None;
            if ( value != TraceParameters.None && value != this.SelectedPrimaryTraceParameter )
            {
                _ = this._PrimaryTraceParameterComboBox.SafeSelectTraceParameters( value );
            }

            return this.SelectedPrimaryTraceParameter;
        }

        /// <summary> Gets the selected primary trace parameter. </summary>
        /// <value> The selected primary trace parameter. </value>
        private TraceParameters SelectedPrimaryTraceParameter => ( TraceParameters ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._PrimaryTraceParameterComboBox.SelectedItem).Key );

        /// <summary> Select secondary trace parameter. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.TraceParameters. </returns>
        private TraceParameters SelectSecondaryTraceParameter( TraceParameters value )
        {
            if ( this.InitializingComponents || value == TraceParameters.None )
                return TraceParameters.None;
            if ( value != TraceParameters.None && value != this.SelectedSecondaryTraceParameter )
            {
                _ = this._SecondaryTraceParameterComboBox.SafeSelectTraceParameters( value );
            }

            return this.SelectedSecondaryTraceParameter;
        }

        /// <summary> Gets the selected secondary trace parameter. </summary>
        /// <value> The selected secondary trace parameter. </value>
        private TraceParameters SelectedSecondaryTraceParameter => ( TraceParameters ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._SecondaryTraceParameterComboBox.SelectedItem).Key );

        /// <summary> Applies the traces button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyTracesButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying trace settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                // set a two point sweep.
                this.ConfigureTrace();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SENSE / MARKERS "

        /// <summary> Applies the marker settings button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplyMarkerSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying marker settings";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                double f = double.Parse( this._MarkerFrequencyComboBox.Text );
                // to_do: use sense trace function to set the reading units.
                // Turn on marker 1
                // Me.Device.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC{0}:MARK{1} ON", channelNumber, markerNumber))
                _ = this.Device.ChannelMarkerSubsystem.ApplyEnabled( true );
                // set marker position
                // Me.Device.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC{0}:MARK{1}:X {2}", channelNumber, markerNumber, frequency))
                _ = this.Device.ChannelMarkerSubsystem.ApplyAbscissa( f );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}