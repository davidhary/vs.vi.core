using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.Core.WinForms.ErrorProviderExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{

    /// <summary> A Reading view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this._TraceDropDownButton.Visible = false;
            this._ReadingsCountLabel.Visible = false;
            this._ClearBufferDisplayButton.Visible = false;
            this.InitializingComponents = false;
            this.__ReadButton.Name = "_ReadButton";
            this.__AutoScaleEnabledMenuItem.Name = "_AutoScaleEnabledMenuItem";
            this.__InitiateMenuItem.Name = "_InitiateMenuItem";
            this.__AbortMenuItem.Name = "_AbortMenuItem";
            this.__ReadingComboBox.Name = "_ReadingComboBox";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindCalculateChannelSubsystem( value );
            this.BindChannelMarkerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CALCULATE "

        /// <summary> Gets the CalculateChannel subsystem. </summary>
        /// <value> The CalculateChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public CalculateChannelSubsystem CalculateChannelSubsystem { get; private set; }

        /// <summary> Bind CalculateChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindCalculateChannelSubsystem( E4990Device device )
        {
            if ( this.CalculateChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.CalculateChannelSubsystem );
                this.CalculateChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.CalculateChannelSubsystem = device.CalculateChannelSubsystem;
                this.BindSubsystem( true, this.CalculateChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, CalculateChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.CalculateChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.CalculateChannelSubsystem.AveragingEnabled ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.CalculateChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Calculate channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( CalculateChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( CalculateChannelSubsystemBase.AveragingEnabled ):
                    {
                        this._AverageEnabledMenuItem.Checked = subsystem.AveragingEnabled.GetValueOrDefault( false );
                        break;
                    }

                case nameof( CalculateChannelSubsystemBase.AverageCount ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Calculate channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CalculateChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( E4990.CalculateChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.CalculateChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as CalculateChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL MARKER "

        /// <summary> Gets the ChannelMarker subsystem. </summary>
        /// <value> The ChannelMarker subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ChannelMarkerSubsystem ChannelMarkerSubsystem { get; private set; }

        /// <summary> Bind ChannelMarker subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindChannelMarkerSubsystem( E4990Device device )
        {
            if ( this.ChannelMarkerSubsystem is object )
            {
                this.BindSubsystem( false, this.ChannelMarkerSubsystem );
                this.ChannelMarkerSubsystem = null;
            }

            if ( device is object )
            {
                this.ChannelMarkerSubsystem = device.ChannelMarkerSubsystem;
                this.BindSubsystem( true, this.ChannelMarkerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ChannelMarkerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ChannelmarkerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.ChannelMarkerSubsystem.MarkerReadings ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ChannelmarkerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the channel marker subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelMarkerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( E4990.ChannelMarkerSubsystem.MarkerReadings ):
                    {
                        if ( subsystem.MarkerReadings is null )
                        {
                            this._ReadingComboBox.Items.Clear();
                        }
                        else
                        {
                            this._ReadingComboBox.ComboBox.ListEnumDescriptions( subsystem.MarkerReadings.Elements, ReadingElementTypes.Units );
                        }

                        break;
                    }

                case nameof( E4990.ChannelMarkerSubsystem.LastReading ):
                    {
                        // To_DO: Bind the sense subsystem to display the data
                        _ = this.Device.SenseChannelSubsystem.ParsePrimaryReading( subsystem.LastReading );
                        break;
                    }

                case nameof( E4990.ChannelMarkerSubsystem.LastActionElapsedTime ):
                    {
                        break;
                    }
                // Me._LastReadingTextBox.Text = $"{subsystem.LastReading} @{subsystem.LastActionElapsedTime.ToExactMilliseconds:0.0}ms"
                case nameof( E4990.ChannelMarkerSubsystem.FailureCode ):
                    {
                        break;
                    }
                // Me._FailureToolStripStatusLabel.Text = subsystem.FailureCode
                case nameof( E4990.ChannelMarkerSubsystem.FailureLongDescription ):
                    {
                        break;
                    }
                // Me._FailureToolStripStatusLabel.ToolTipText = subsystem.FailureLongDescription
                case nameof( E4990.ChannelMarkerSubsystem.ReadingCaption ):
                    {
                        break;
                    }
                    // Me._ReadingToolStripStatusLabel.Text = subsystem.ReadingCaption
            }
        }

        /// <summary> Channel marker subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelmarkerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( E4990.ChannelMarkerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ChannelmarkerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelMarkerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: READING "

        /// <summary> Selects a new reading to display. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.ReadingElementTypes. </returns>
        internal ReadingElementTypes SelectReading( ReadingElementTypes value )
        {
            if ( this.Device.IsDeviceOpen && value != ReadingElementTypes.None && value != this.SelectedReadingType )
            {
                _ = this._ReadingComboBox.ComboBox.SelectItem( value.ValueDescriptionPair() );
            }

            return this.SelectedReadingType;
        }

        /// <summary> Gets the type of the selected reading. </summary>
        /// <value> The type of the selected reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private ReadingElementTypes SelectedReadingType => ( ReadingElementTypes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ReadingComboBox.SelectedItem).Key );

        /// <summary> Abort button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" />
        ///                                                                   instance of this
        ///                                             <see cref="T:System.Windows.Forms.Control" /> 
        /// </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} aborting measurements(s)";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.Device.IsDeviceOpen )
                {
                    _ = this.PublishInfo( $"{activity};. " );
                    this.Device.TriggerSubsystem.Abort();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by InitButton for click events. Initiates a reading for retrieval by
        /// way of the service request event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitiateMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} aborting measurements(s)";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );

                // clear execution state before enabling events
                this.Device.ClearExecutionState();

                // set the service request
                this.Device.Session.ApplyServiceRequestEnableBitmask( this.Device.Session.DefaultOperationServiceRequestEnableBitmask );

                // trigger the initiation of the measurement letting the service request do the rest.
                this.Device.ClearExecutionState();
            }
            // Me.Device.TriggerSubsystem.Initiate()

            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reading combo box selected value changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadingComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} selecting a reading to display";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ChannelMarkerSubsystem.SelectActiveReading( this.SelectedReadingType );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads a marker. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Action event information. </param>
        public void ReadMarker( ActionEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            if ( this.Device.ChannelMarkerSubsystem.Enabled == true )
            {
                string activity = $"{this.Device.ResourceNameCaption} reading a marker";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.ClearExecutionState();
                // clear the device display from warnings
                // Me.Device.Session.Write(":DISP:CCL")
                this.Device.DisplaySubsystem.ClearCautionMessages();
                if ( this.Device.CalculateChannelSubsystem.AveragingEnabled.GetValueOrDefault( false ) )
                {
                    this.Device.StatusSubsystem.EnableMeasurementAvailable();
                    this.Device.ChannelMarkerSubsystem.InitializeMarkerAverage( this.Device.TriggerSubsystem, this.Device.CalculateChannelSubsystem );
                }
                else
                {
                    // Me.Device.Session.WriteLine(":TRIG")
                    this.Device.TriggerSubsystem.Initiate();
                }

                (bool timedOut, Pith.ServiceRequests status, _) = this.Device.Session.AwaitStatusBitmask( Pith.ServiceRequests.RequestingService, TimeSpan.FromMilliseconds( 100d ), this.Device.Session.StatusReadDelay, this.Device.Session.EffectiveStatusReadTurnaroundTime );
                if ( timedOut )
                {
                    e.RegisterFailure( "timeout" );
                }
                else
                {
                    _ = this.Device.Session.ApplyServiceRequest( status );
                    // auto scale after measurement completes
                    this.Device.PrimaryChannelTraceSubsystem.AutoScale();
                    this.Device.SecondaryChannelTraceSubsystem.AutoScale();
                    this.Device.PrimaryChannelTraceSubsystem.Select();
                    _ = this.Device.ChannelMarkerSubsystem.FetchLatestData();
                }
            }
            else
            {
                e.RegisterFailure( "Marker not defined" );
            }
        }

        /// <summary>
        /// Event handler. Called by _ReadButton for click events. Query the Device for a reading.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} reading a marker";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                var args = new ActionEventArgs();
                this.ReadMarker( args );
                if ( args.Failed )
                {
                    _ = this.InfoProvider.Annunciate( sender, args.Details );
                }

                if ( this.Device.ChannelMarkerSubsystem.Enabled == true )
                {
                    _ = this.PublishInfo( $"{activity};. " );
                    this.Device.Session.StartElapsedStopwatch();
                    this.Device.ClearExecutionState();
                    _ = this.Device.Session.ReadElapsedTime( true );
                    // clear the device display from warnings
                    // Me.Device.Session.Write(":DISP:CCL")
                    this.Device.DisplaySubsystem.ClearCautionMessages();
                    if ( this._AverageEnabledMenuItem.Checked )
                    {
                        this.Device.StatusSubsystem.EnableMeasurementAvailable();
                        this.Device.ChannelMarkerSubsystem.InitializeMarkerAverage( this.Device.TriggerSubsystem, this.Device.CalculateChannelSubsystem );
                    }
                    else
                    {
                        // Me.Device.Session.WriteLine(":TRIG")
                        this.Device.TriggerSubsystem.Initiate();
                    }

                    (bool TimedOut, Pith.ServiceRequests Status, TimeSpan Elpased) = this.Device.Session.AwaitStatusBitmask( Pith.ServiceRequests.RequestingService, TimeSpan.FromMilliseconds( 100d ), this.Device.Session.StatusReadDelay, this.Device.Session.EffectiveStatusReadTurnaroundTime );
                    if ( TimedOut )
                    {
                        _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Alert, "timeout" );
                    }
                    else
                    {
                        _ = this.Device.Session.ApplyServiceRequest( Status );
                        // auto scale after measurement completes
                        this.Device.PrimaryChannelTraceSubsystem.AutoScale();
                        this.Device.SecondaryChannelTraceSubsystem.AutoScale();
                        this.Device.PrimaryChannelTraceSubsystem.Select();
                        _ = this.Device.ChannelMarkerSubsystem.FetchLatestData();
                    }
                }
                else
                {
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Info, "Define a marker first." );
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        #endregion

        #region " CONTROL EVENT HANDLERS: SCALE "

        /// <summary> Automatic scale menu item click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> <see cref="T:System.Object" />
        ///                                                                   instance of this
        ///                                             <see cref="T:System.Windows.Forms.Control" /> 
        /// </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AutoScaleCheckBox_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} applying auto scale";
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.PrimaryChannelTraceSubsystem.AutoScale();
                this.Device.SecondaryChannelTraceSubsystem.AutoScale();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
