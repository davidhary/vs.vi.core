using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.Controls;
using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{

    /// <summary> A Compensate view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class CompensateView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new instance of the <see cref="CompensationWizard"/> class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public CompensateView() : base()
        {
            // required for designer support
            this.InitializingComponents = true;
            this.InitializeComponent();
            this.InitializingComponents = false;
            // Me.ConstructorSafeSetter(New TraceMessageTalker)
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( "This wizard will guide you through the steps of performing a compensation task." );
            _ = builder.AppendLine( "The following items are required:" );
            _ = builder.AppendLine( "-- An Open fixture for open compensation;" );
            _ = builder.AppendLine( "-- A Short fixture for short compensation;" );
            _ = builder.AppendLine( "-- A Load Resistance fixture for load compensation; and" );
            _ = builder.AppendLine( "-- A Yardstick Resistance fixture for validation." );
            this._WelcomeWizardPage.Description = builder.ToString();
            this._Wizard.HeaderImage = My.Resources.Resources.HeaderIcon;
            this._Wizard.WelcomeImage = My.Resources.Resources.WelcomeImage;
            this._LoadResistanceNumeric.Value = E4990.My.MySettings.Default.LoadResistance;
            this._YardstickResistanceNumeric.Value = E4990.My.MySettings.Default.YardstickResistance;
            this._YardstickAcceptanceToleranceNumeric.Value = 100m * E4990.My.MySettings.Default.YardstickResistanceTolerance;
            this._YardstickInductanceLimitNumeric.Value = ( decimal ) (1000000.0d * ( double ) E4990.My.MySettings.Default.YardstickInductanceLimit);
            this.__YardstickInductanceLimitNumeric.Name = "_YardstickInductanceLimitNumeric";
            this.__YardstickAcceptanceToleranceNumeric.Name = "_YardstickAcceptanceToleranceNumeric";
            this.__MeasureYardstickButton.Name = "_MeasureYardstickButton";
            this.__AcquireLoadCompensationButton.Name = "_AcquireLoadCompensationButton";
            this.__AcquireShortCompensationButton.Name = "_AcquireShortCompensationButton";
            this.__AcquireOpenCompensationButton.Name = "_AcquireOpenCompensationButton";
            this.__RestartAveragingButton.Name = "_RestartAveragingButton";
            this.__ApplySettingsButton.Name = "_ApplySettingsButton";
            this.__Wizard.Name = "_Wizard";
        }

        /// <summary> Creates a new <see cref="CompensateView"/> </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <returns> A <see cref="CompensateView"/>. </returns>
        public static CompensateView Create()
        {
            CompensateView view = null;
            try
            {
                view = new CompensateView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindCalculateChannelSubsystem( value );
            this.BindChannelMarkerSubsystem( value );
            this.BindCompensateChannelSubsystem( value.CompensateLoadSubsystem );
            this.BindSenseChannelSubsystem( value );
            this.BindStatusSubsystem( value );
            this.BindTriggerSubsystem( value );
            this.Device = value;
            if ( this.Device is object )
            {
                this._AdapterComboBox.ListSupportedAdapters( this.Device.SenseChannelSubsystem.SupportedAdapterTypes );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        #endregion

        #region " DATA BINDING "

        /// <summary> Bind controls settings. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void BindControlsSettings()
        {
            var MySettings2 = E4990.My.MySettings.Default;
            this._LoadCompensationTextBox.DataBindings.Add( new Binding( "Text", MySettings2, "LoadCompensationReading", true, DataSourceUpdateMode.OnPropertyChanged ) );
            this._LoadCompensationTextBox.Text = MySettings2.LoadCompensationReading;
        }

        #endregion

        #region " CALCULATE "

        /// <summary> Gets the CalculateChannel subsystem. </summary>
        /// <value> The CalculateChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public CalculateChannelSubsystem CalculateChannelSubsystem { get; private set; }

        /// <summary> Bind CalculateChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindCalculateChannelSubsystem( E4990Device device )
        {
            if ( this.CalculateChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.CalculateChannelSubsystem );
                this.CalculateChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.CalculateChannelSubsystem = device.CalculateChannelSubsystem;
                this.BindSubsystem( true, this.CalculateChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, CalculateChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.CalculateChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.CalculateChannelSubsystem.AveragingEnabled ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.CalculateChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Calculate channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( CalculateChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( E4990.CalculateChannelSubsystem.AveragingEnabled ):
                    {
                        this._AveragingEnabledCheckBox.Checked = subsystem.AveragingEnabled.GetValueOrDefault( false );
                        break;
                    }

                case nameof( E4990.CalculateChannelSubsystem.AverageCount ):
                    {
                        this._AveragingCountNumeric.Value = subsystem.AverageCount.GetValueOrDefault( 0 );
                        break;
                    }
            }
        }

        /// <summary> Calculate channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CalculateChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.CalculateChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.CalculateChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as CalculateChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " COMPENSATE "

        /// <summary> Gets the CompensateChannel subsystem. </summary>
        /// <value> The CompensateChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public CompensateChannelSubsystemBase CompensateChannelSubsystem { get; private set; }

        /// <summary> Bind CompensateChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem"> The device. </param>
        private void BindCompensateChannelSubsystem( CompensateChannelSubsystemBase subsystem )
        {
            if ( this.CompensateChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.CompensateChannelSubsystem );
                this.CompensateChannelSubsystem = null;
            }

            if ( subsystem is object )
            {
                this.CompensateChannelSubsystem = subsystem;
                this.BindSubsystem( true, this.CompensateChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, CompensateChannelSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.CompensateChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.CompensateChannelSubsystem.HasCompleteCompensationValues ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.CompensateChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Compensate channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( CompensateChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( E4990.CompensateChannelSubsystem.HasCompleteCompensationValues ):
                    {
                        string value = string.Empty;
                        if ( subsystem.HasCompleteCompensationValues )
                        {
                            value = CompensateChannelSubsystemBase.Merge( subsystem.FrequencyArrayReading, subsystem.ImpedanceArrayReading );
                        }

                        if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.OpenCircuit == true )
                        {
                            this._OpenCompensationValuesTextBox.Text = value;
                        }
                        else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.ShortCircuit == true )
                        {
                            this._ShortCompensationValuesTextBox.Text = value;
                        }
                        else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.Load == true )
                        {
                            this._LoadCompensationTextBox.Text = value;
                        }

                        break;
                    }
            }

            if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.OpenCircuit == true )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( E4990.CompensateChannelSubsystem.HasCompleteCompensationValues ):
                        {
                            break;
                        }
                }
            }
            else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.ShortCircuit == true )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( E4990.CompensateChannelSubsystem.FrequencyArrayReading ):
                        {
                            break;
                        }
                }
            }
            else if ( ( int? ) subsystem.CompensationType == ( int? ) CompensationTypes.Load == true )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( E4990.CompensateChannelSubsystem.FrequencyArrayReading ):
                        {
                            break;
                        }

                    case nameof( E4990.CompensateChannelSubsystem.ModelResistance ):
                        {
                            if ( subsystem.ModelResistance.HasValue )
                                this._LoadResistanceNumeric.Value = ( decimal ) subsystem.ModelResistance.Value;
                            break;
                        }
                }
            }
        }

        /// <summary> Compensate channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void CompensateChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.CompensateChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.CompensateChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as CompensateChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CHANNEL MARKER "

        /// <summary> Gets the ChannelMarker subsystem. </summary>
        /// <value> The ChannelMarker subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public ChannelMarkerSubsystem ChannelMarkerSubsystem { get; private set; }

        /// <summary> Bind ChannelMarker subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindChannelMarkerSubsystem( E4990Device device )
        {
            if ( this.ChannelMarkerSubsystem is object )
            {
                this.BindSubsystem( false, this.ChannelMarkerSubsystem );
                this.ChannelMarkerSubsystem = null;
            }

            if ( device is object )
            {
                this.ChannelMarkerSubsystem = device.ChannelMarkerSubsystem;
                this.BindSubsystem( true, this.ChannelMarkerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, ChannelMarkerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.ChannelMarkerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.ChannelMarkerSubsystem.LastReading ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.ChannelMarkerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the channel marker subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( ChannelMarkerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( E4990.ChannelMarkerSubsystem.LastReading ):
                    {
                        this.UpdateYardstickValues();
                        break;
                    }
            }
        }

        /// <summary> Channel marker subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ChannelMarkerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.ChannelMarkerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.ChannelMarkerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as ChannelMarkerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Selects a new Adapter to display. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.AdapterTypes. </returns>
        internal AdapterTypes SelectAdapter( AdapterTypes value )
        {
            if ( this.InitializingComponents || value == AdapterTypes.None )
                return AdapterTypes.None;
            if ( value != AdapterTypes.None && value != this.SelectedAdapterType )
            {
                _ = this._AdapterComboBox.SelectItem( value.ValueDescriptionPair() );
            }

            return this.SelectedAdapterType;
        }

        /// <summary> Gets the type of the selected Adapter. </summary>
        /// <value> The type of the selected Adapter. </value>
        private AdapterTypes SelectedAdapterType => ( AdapterTypes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._AdapterComboBox.SelectedItem).Key );

        /// <summary> Gets the SenseChannel subsystem. </summary>
        /// <value> The SenseChannel subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseChannelSubsystem SenseChannelSubsystem { get; private set; }

        /// <summary> Bind SenseChannel subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseChannelSubsystem( E4990Device device )
        {
            if ( this.SenseChannelSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseChannelSubsystem );
                this.SenseChannelSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseChannelSubsystem = device.SenseChannelSubsystem;
                this.BindSubsystem( true, this.SenseChannelSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseChannelSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseChannelSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.SenseChannelSubsystem.AdapterType ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseChannelSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the sense channel subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( SenseChannelSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( SenseChannelSubsystemBase.AdapterType ):
                    {
                        if ( subsystem.AdapterType.HasValue )
                            _ = this.SelectAdapter( subsystem.AdapterType.Value );
                        break;
                    }

                case nameof( SenseChannelSubsystemBase.Aperture ):
                    {
                        if ( subsystem.Aperture.HasValue )
                            this._ApertureNumeric.Value = ( decimal ) subsystem.Aperture.Value;
                        break;
                    }
            }
        }

        /// <summary> Sense channel subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseChannelSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SenseChannelSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseChannelSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseChannelSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( E4990Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.TriggerSubsystem.Delay ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( TriggerSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TriggerSubsystemBase.Delay ):
                    {
                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets the Status subsystem. </summary>
        /// <value> The Status subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public StatusSubsystem StatusSubsystem { get; private set; }

        /// <summary> Bind Status subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        private void BindStatusSubsystem( E4990Device device )
        {
            if ( this.StatusSubsystem is object )
            {
                this.BindSubsystem( false, this.StatusSubsystem );
                this.StatusSubsystem = null;
            }

            if ( device is object )
            {
                this.StatusSubsystem = device.StatusSubsystem;
                this.BindSubsystem( true, this.StatusSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, StatusSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.StatusSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( E4990.StatusSubsystem.DeviceErrorReport ) );
                this.HandlePropertyChanged( subsystem, nameof( E4990.StatusSubsystem.CompoundErrorMessage ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.StatusSubsystemPropertyChanged;
            }
        }

        /// <summary> Reports the last error. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="lastError"> The last error. </param>
        private void OnLastError( string lastError )
        {
            this._StatusLabel.Text = Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact( lastError, this._StatusLabel );
            this._StatusLabel.ToolTipText = lastError;
        }

        /// <summary> Handle the Status subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        protected virtual void HandlePropertyChanged( StatusSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( StatusSubsystemBase.CompoundErrorMessage ):
                    {
                        this.OnLastError( subsystem.CompoundErrorMessage );
                        break;
                    }
            }
        }

        /// <summary> Status subsystem property changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StatusSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.StatusSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.StatusSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as StatusSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public void ReadStatusRegister()
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} reading service request";
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENTS HANDLERS "

        #region " SETTINGS "

        /// <summary> Restart averaging button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RestartAveragingButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} restarting average";
                this.InfoProvider.Clear();
                this.Device.CalculateChannelSubsystem.ClearAverage();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Applies the settings button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} apply settings";
                this.InfoProvider.Clear();
                _ = this.Device.SenseChannelSubsystem.ApplyAdapterType( this.SelectedAdapterType );
                _ = this.Device.SenseChannelSubsystem.ApplyAperture( ( double ) this._ApertureNumeric.Value );
                this.Device.CalculateChannelSubsystem.ApplyAverageSettings( this._AveragingEnabledCheckBox.Checked, ( int ) Math.Round( this._AveragingCountNumeric.Value ) );
                this.Device.CalculateChannelSubsystem.ClearAverage();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        #endregion

        #region " OPEN "

        /// <summary> Acquires the open compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AcquireOpenCompensation()
        {

            // Set user-specified frequencies
            _ = this.Device.SenseChannelSubsystem.ApplyFrequencyPointsType( FrequencyPointsTypes.User );

            // Acquire open fixture compensation
            this.Device.CompensateOpenSubsystem.AcquireMeasurements();

            // Wait for measurement end
            _ = this.Device.Session.QueryOperationCompleted();
        }

        /// <summary> Reads open compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ReadOpenCompensation()
        {

            // read the frequencies
            _ = this.Device.CompensateOpenSubsystem.QueryFrequencyArray();

            // read the data
            _ = this.Device.CompensateOpenSubsystem.QueryImpedanceArray();
        }

        /// <summary> Acquires the open compensation button context menu strip changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireOpenCompensationButton_ContextMenuStripChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring open compensation";
                this.InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.Device.ClearCompensations();
                this.Device.ChannelMarkerSubsystem.MarkerReadings.Reset();
                this.AcquireOpenCompensation();
                this.ReadOpenCompensation();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }


        #endregion

        #region " SHORT "

        /// <summary> Acquires the Short compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AcquireShortCompensation()
        {

            // Set user-specified frequencies
            _ = this.Device.SenseChannelSubsystem.ApplyFrequencyPointsType( FrequencyPointsTypes.User );

            // Acquire Short fixture compensation
            this.Device.CompensateShortSubsystem.AcquireMeasurements();

            // Wait for measurement end
            _ = this.Device.Session.QueryOperationCompleted();
        }

        /// <summary> Reads Short compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ReadShortCompensation()
        {

            // read the frequencies
            _ = this.Device.CompensateShortSubsystem.QueryFrequencyArray();

            // read the data
            _ = this.Device.CompensateShortSubsystem.QueryImpedanceArray();
        }

        /// <summary> Acquires the Short compensation button context menu strip changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireShortCompensationButton_ContextMenuStripChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring short compensation";
                this.InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.AcquireShortCompensation();
                this.ReadShortCompensation();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " LOAD "

        /// <summary> Acquires the Load compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AcquireLoadCompensation()
        {

            // Set user-specified frequencies
            _ = this.Device.SenseChannelSubsystem.ApplyFrequencyPointsType( FrequencyPointsTypes.User );
            _ = this.Device.CompensateLoadSubsystem.ApplyModelResistance( ( double ) this._LoadResistanceNumeric.Value );
            _ = this.Device.CompensateLoadSubsystem.ApplyModelCapacitance( 0d );
            _ = this.Device.CompensateLoadSubsystem.ApplyModelInductance( 0d );
            _ = this.Device.Session.QueryOperationCompleted();

            // Acquire Load fixture compensation
            this.Device.CompensateLoadSubsystem.AcquireMeasurements();

            // Wait for measurement end
            _ = this.Device.Session.QueryOperationCompleted();
        }

        /// <summary> Reads Load compensation. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void ReadLoadCompensation()
        {

            // read the frequencies
            _ = this.Device.CompensateLoadSubsystem.QueryFrequencyArray();

            // read the data
            _ = this.Device.CompensateLoadSubsystem.QueryImpedanceArray();
        }

        /// <summary> Acquires the Load compensation button context menu strip changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AcquireLoadCompensationButton_ContextMenuStripChanged( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring load compensation";
                this.InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.AcquireLoadCompensation();
                this.ReadLoadCompensation();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " YARDSTICK "

        /// <summary> Yardstick acceptance tolerance numeric value changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void YardstickAcceptanceToleranceNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AnnunciateYardstickResult();
        }

        /// <summary> Yardstick inductance limit numeric value changed. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void YardstickInductanceLimitNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this.AnnunciateYardstickResult();
        }

        /// <summary> Gets the yardstick resistance validated. </summary>
        /// <value> The yardstick resistance validated. </value>
        private bool? YardstickResistanceValidated
        {
            get {
                if ( this.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.HasValue )
                {
                    double r = this.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.Value;
                    double delta = 100d * (r / ( double ) this._YardstickResistanceNumeric.Value - 1d);
                    return Math.Abs( delta ) <= ( double ) this._YardstickAcceptanceToleranceNumeric.Value;
                }
                else
                {
                    return new bool?();
                }
            }
        }

        /// <summary> Gets the yardstick impedance validated. </summary>
        /// <value> The yardstick impedance validated. </value>
        private bool? YardstickInductanceValidated
        {
            get {
                if ( this.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.HasValue )
                {
                    double l = 1000000.0d * this.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.Value;
                    this._YardstickMeasuredInductanceTextBox.Text = $"{l:0.###}";
                    return Math.Abs( l ) <= ( double ) this._YardstickInductanceLimitNumeric.Value;
                }
                else
                {
                    return new bool?();
                }
            }
        }

        /// <summary> Gets the yardstick validated. </summary>
        /// <value> The yardstick validated. </value>
        private bool YardstickValidated => this.YardstickResistanceValidated.GetValueOrDefault( false ) && this.YardstickInductanceValidated.GetValueOrDefault( false );

        /// <summary> Updates the yardstick values. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void UpdateYardstickValues()
        {
            this.InfoProvider.Clear();
            this._YardstickValuesTextBox.Text = this.Device.ChannelMarkerSubsystem.MarkerReadings.RawReading;
            double r = this.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.GetValueOrDefault( 0d );
            this._MeasuredYardstickResistanceTextBox.Text = $"{r:0.###}";
            double delta = 100d * (r / ( double ) this._YardstickResistanceNumeric.Value - 1d);
            this._YardstickResistanceDeviationTextBox.Text = $"{delta:0.##}";
            double l = 1000000.0d * this.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.GetValueOrDefault( 0d );
            this._YardstickMeasuredInductanceTextBox.Text = $"{l:0.###}";
            this.AnnunciateYardstickResult();
        }

        /// <summary> Annunciate yardstick result. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void AnnunciateYardstickResult()
        {
            if ( !this.YardstickResistanceValidated == true )
            {
                _ = this.InfoProvider.Annunciate( this._YardstickAcceptanceToleranceNumeric, Core.Forma.InfoProviderLevel.Alert, "Exceeds minimum" );
            }

            if ( !this.YardstickInductanceValidated == true )
            {
                _ = this.InfoProvider.Annunciate( this._YardstickInductanceLimitNumeric, Core.Forma.InfoProviderLevel.Alert, "Exceeds minimum" );
            }
        }

        /// <summary> Measure yardstick button click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureYardstickButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} acquiring reading meter; yardstick";
                this.InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                this.Device.ChannelMarkerSubsystem.ReadMarkerAverage( this.Device.TriggerSubsystem, this.Device.CalculateChannelSubsystem );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region " WIZARD EVENTS HANDLERS "

        /// <summary> Handles the AfterSwitchPages event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Page changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_AfterSwitchPages( object sender, PageChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} after moving page";
                this.InfoProvider.Clear();
                this.Cursor = Cursors.WaitCursor;
                switch ( true )
                {
                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._SettingsWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._OpenWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._ShortWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._LoadWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._YardstickWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._FinishWizardPage ):
                        {
                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Handles the BeforeSwitchPages event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Page changing event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_BeforeSwitchPages( object sender, PageChangingEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} before moving page";
                this.InfoProvider.Clear();
                switch ( true )
                {
                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._SettingsWizardPage ) && e.Direction == Direction.Next:
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._OpenWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._ShortWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._LoadWizardPage ):
                        {
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.OldPage, this._YardstickWizardPage ):
                        {
                            break;
                        }
                }

                _ = this.Device.Session.QueryOperationCompleted();
                _ = this.Device.Session.ReadStatusRegister();

                // check if we're going forward from options page
                if ( ReferenceEquals( this._Wizard.OldPage, this._OpenWizardPage ) && e.NewIndex > e.OldIndex )
                {
                    // check if user selected one option
                    bool skipOption = false;
                    bool CheckOption = true;
                    if ( !CheckOption && !skipOption )
                    {
                        // display hint & cancel step
                        _ = MessageBox.Show( "Please chose one of the options presented.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
                        e.Cancel = true;
                    }
                    // check if user chose to skip validation
                    else if ( skipOption )
                    {
                        // skip the validation page
                        e.NewIndex += 1;
                    }
                }
            }
            catch ( Exception ex )
            {
                e.Cancel = true;
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Handles the Cancel event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_Cancel( object sender, CancelEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} canceling a wizard step";
                this.InfoProvider.Clear();
                // check if task is running
                bool isTaskRunning = this._LongTaskTimer.Enabled;
                // stop the task
                this._LongTaskTimer.Enabled = false;

                // ask user to confirm
                if ( MessageBox.Show( $"Are you sure you wand to exit {this.Text}?", this.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly ) != DialogResult.Yes )
                {
                    // cancel closing
                    e.Cancel = true;
                    // restart the task
                    this._LongTaskTimer.Enabled = isTaskRunning;
                }
                else
                {
                    // ensure parent form is closed (even when ShowDialog is not used)
                    // Me.DialogResult = DialogResult.Cancel
                    // Me.Close()
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Handles the Finish event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_Finish( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} closing the wizard";
                this.InfoProvider.Clear();
                if ( this.YardstickValidated )
                {
                    _ = MessageBox.Show( $"The {this.Text} finished successfully.", this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
                }
                // ensure parent form is closed (even when ShowDialog is not used)
                // Me.DialogResult = DialogResult.OK
                // Me.Close()
                else if ( MessageBox.Show( $"The {this.Text} yardstick was not validated or is out of range. Are you sure?", $"{this.Text} Save Data; Are you sure?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2, MessageBoxOptions.DefaultDesktopOnly ) == DialogResult.Yes )
                {
                    // Me.DialogResult = DialogResult.OK
                    // Me.Close()
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Handles the Help event of the wizard form. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void Wizard_Help( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} showing help info";
                this.InfoProvider.Clear();
                string text = $"This is a really cool wizard control!{Environment.NewLine}:-)";
                switch ( true )
                {
                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._SettingsWizardPage ):
                        {
                            text = My.Resources.Resources.SettingsHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._OpenWizardPage ):
                        {
                            text = My.Resources.Resources.OpenHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._ShortWizardPage ):
                        {
                            text = My.Resources.Resources.ShortHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._LoadWizardPage ):
                        {
                            text = My.Resources.Resources.LoadHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._YardstickWizardPage ):
                        {
                            text = My.Resources.Resources.YardstickHelp;
                            break;
                        }

                    case object _ when ReferenceEquals( this._Wizard.NewPage, this._FinishWizardPage ):
                        {
                            text = My.Resources.Resources.FinishHelp;
                            break;
                        }
                }

                _ = MessageBox.Show( text, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
