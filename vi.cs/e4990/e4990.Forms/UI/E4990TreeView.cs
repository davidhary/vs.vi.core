using System;
using System.ComponentModel;
using System.Diagnostics;

using isr.VI.ExceptionExtensions;

namespace isr.VI.E4990.Forms
{

    /// <summary> Keysight 4990 Device User Interface. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-30 </para></remarks>
    [DisplayName( "E4990 User Interface" )]
    [Description( "Keysight 4990 Device User Interface" )]
    [System.Drawing.ToolboxBitmap( typeof( E4990TreeView ) )]
    public class E4990TreeView : Facade.VisaTreeView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public E4990TreeView() : base()
        {
            this.InitializingComponents = true;
            this.ReadingView = ReadingView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Read", "Read", this.ReadingView );
            this.SourceView = SourceView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Source", "Source", this.SourceView );
            this.SenseView = SenseView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Sense", "Sense", this.SenseView );
            this.TriggerView = TriggerView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Trigger", "Trigger", this.TriggerView );
            this.CalibrateView = CalibrateView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Calibrate", "Calibrate", this.CalibrateView );
            this.CompensateView = CompensateView.Create();
            this.InsertViewControl( this.ViewsCount - 1, "Compensate", "Compensate", this.CompensateView );
            this.InitializingComponents = false;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="device"> The device. </param>
        public E4990TreeView( E4990Device device ) : this()
        {
            this.AssignDeviceThis( device );
        }

        /// <summary> Gets source view. </summary>
        /// <value> The source view. </value>
        private SourceView SourceView { get; set; }

        /// <summary> Gets the sense view. </summary>
        /// <value> The sense view. </value>
        private SenseView SenseView { get; set; }

        /// <summary> Gets the reading view. </summary>
        /// <value> The reading view. </value>
        private ReadingView ReadingView { get; set; }

        /// <summary> Gets the calibrate view. </summary>
        /// <value> The calibrate view. </value>
        private CalibrateView CalibrateView { get; set; }

        /// <summary> Gets the compensate view. </summary>
        /// <value> The compensate view. </value>
        private CompensateView CompensateView { get; set; }

        /// <summary> Gets the trigger view. </summary>
        /// <value> The trigger view. </value>
        private TriggerView TriggerView { get; set; }

        /// <summary>
        /// Releases the unmanaged resources used by the E4990 View and optionally releases the managed
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    if ( this.SourceView is object )
                    {
                        this.SourceView.Dispose();
                        this.SourceView = null;
                    }

                    if ( this.SenseView is object )
                    {
                        this.SenseView.Dispose();
                        this.SenseView = null;
                    }

                    if ( this.TriggerView is object )
                    {
                        this.TriggerView.Dispose();
                        this.TriggerView = null;
                    }

                    if ( this.CompensateView is object )
                    {
                        this.CompensateView.Dispose();
                        this.CompensateView = null;
                    }

                    if ( this.CalibrateView is object )
                    {
                        this.CalibrateView.Dispose();
                        this.CalibrateView = null;
                    }

                    if ( this.ReadingView is object )
                    {
                        this.ReadingView.Dispose();
                        this.ReadingView = null;
                    }

                    this.AssignDeviceThis( null );
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public E4990Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( E4990Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.StatusView.DeviceSettings = null;
                this.StatusView.UserInterfaceSettings = null;
                this.Device = null;
            }

            this.Device = value;
            base.BindVisaSessionBase( value );
            if ( value is object )
            {
                this.StatusView.DeviceSettings = E4990.My.MySettings.Default;
                this.StatusView.UserInterfaceSettings = null;
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( E4990Device value )
        {
            this.AssignDeviceThis( value );
        }

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Executes the device closing action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnDeviceClosing( CancelEventArgs e )
        {
            base.OnDeviceClosing( e );
            if ( e is object && !e.Cancel )
            {
                // release the device before subsystems are disposed
                this.ReadingView.AssignDevice( null );
                this.SourceView.AssignDevice( null );
                this.SenseView.AssignDevice( null );
                this.TriggerView.AssignDevice( null );
                this.CalibrateView.AssignDevice( null );
                this.CompensateView.AssignDevice( null );
            }
        }

        /// <summary> Executes the device closed action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        protected override void OnDeviceClosed()
        {
            base.OnDeviceClosed();
            // remove binding after subsystems are disposed
            // because the device closed the subsystems are null and binding will be removed.
            // TO_DO:
            // MyBase.DisplayView.BindSubsystemViewModel(Me.Device.ChannelMarkerSubsystem)
            // MyBase.DisplayView.BindSubsystemViewModel(Me.Device.SourceChannelSubsystem)
            // MyBase.StatusView.ReadTerminalsState = nothing
            // MyBase.DisplayView.BindTerminalsDisplay(Me.Device.SystemSubsystem)
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        protected override void OnDeviceOpened()
        {
            base.OnDeviceOpened();
            // assigning device and subsystems after the subsystems are created
            this.ReadingView.AssignDevice( this.Device );
            this.SourceView.AssignDevice( this.Device );
            this.SenseView.AssignDevice( this.Device );
            this.TriggerView.AssignDevice( this.Device );
            this.CalibrateView.AssignDevice( this.Device );
            this.CompensateView.AssignDevice( this.Device );
            // TO_DO
            // MyBase.DisplayView.BindSubsystemViewModel(Me.Device.ChannelMarkerSubsystem)
            // MyBase.DisplayView.BindSubsystemViewModel(Me.Device.SourceChannelSubsystem)
            // MyBase.StatusView.ReadTerminalsState = AddressOf Me.Device.SystemSubsystem.QueryFrontTerminalsSelected
            // MyBase.DisplayView.BindTerminalsDisplay(Me.Device.SystemSubsystem)
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
