﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadingView));
            _ReadingsDataGridView = new System.Windows.Forms.DataGridView();
            _ReadingToolStrip = new System.Windows.Forms.ToolStrip();
            __ReadButton = new System.Windows.Forms.ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            _ReadingOptionsDropDownButton = new Core.Controls.ToolStripDropDownButton();
            __AutoScaleEnabledMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AutoScaleEnabledMenuItem.Click += new EventHandler(AutoScaleCheckBox_Click);
            _AverageEnabledMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _TraceDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            __InitiateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __InitiateMenuItem.Click += new EventHandler(InitiateMenuItem_Click);
            _TraceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AbortMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AbortMenuItem.Click += new EventHandler(AbortMenuItem_Click);
            _ReadingsCountLabel = new System.Windows.Forms.ToolStripLabel();
            __ReadingComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReadingComboBox.SelectedIndexChanged += new EventHandler(ReadingComboBox_SelectedIndexChanged);
            _ClearBufferDisplayButton = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)_ReadingsDataGridView).BeginInit();
            _ReadingToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ReadingsDataGridView
            // 
            _ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReadingsDataGridView.Location = new System.Drawing.Point(1, 26);
            _ReadingsDataGridView.Name = "_ReadingsDataGridView";
            _ReadingsDataGridView.Size = new System.Drawing.Size(379, 297);
            _ReadingsDataGridView.TabIndex = 23;
            // 
            // _ReadingToolStrip
            // 
            _ReadingToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ReadButton, _ReadingOptionsDropDownButton, _TraceDropDownButton, _ReadingsCountLabel, __ReadingComboBox, _ClearBufferDisplayButton });
            _ReadingToolStrip.Location = new System.Drawing.Point(1, 1);
            _ReadingToolStrip.Name = "_ReadingToolStrip";
            _ReadingToolStrip.Size = new System.Drawing.Size(379, 25);
            _ReadingToolStrip.TabIndex = 22;
            _ReadingToolStrip.Text = "ToolStrip1";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(37, 22);
            __ReadButton.Text = "Read";
            __ReadButton.ToolTipText = "Read single reading";
            // 
            // _ReadingOptionsDropDownButton
            // 
            _ReadingOptionsDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _ReadingOptionsDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __AutoScaleEnabledMenuItem, _AverageEnabledMenuItem });
            _ReadingOptionsDropDownButton.Image = (System.Drawing.Image)resources.GetObject("_ReadingOptionsDropDownButton.Image");
            _ReadingOptionsDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ReadingOptionsDropDownButton.Name = "_ReadingOptionsDropDownButton";
            _ReadingOptionsDropDownButton.Size = new System.Drawing.Size(62, 22);
            _ReadingOptionsDropDownButton.Text = "Options";
            // 
            // _AutoScaleEnabledMenuItem
            // 
            __AutoScaleEnabledMenuItem.CheckOnClick = true;
            __AutoScaleEnabledMenuItem.Name = "__AutoScaleEnabledMenuItem";
            __AutoScaleEnabledMenuItem.Size = new System.Drawing.Size(130, 22);
            __AutoScaleEnabledMenuItem.Text = "Auto Scale";
            __AutoScaleEnabledMenuItem.ToolTipText = "Check to auto scale";
            // 
            // _AverageEnabledMenuItem
            // 
            _AverageEnabledMenuItem.CheckOnClick = true;
            _AverageEnabledMenuItem.Name = "_AverageEnabledMenuItem";
            _AverageEnabledMenuItem.Size = new System.Drawing.Size(130, 22);
            _AverageEnabledMenuItem.Text = "Average";
            _AverageEnabledMenuItem.ToolTipText = "Check to average";
            // 
            // _TraceDropDownButton
            // 
            _TraceDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _TraceDropDownButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __InitiateMenuItem, _TraceMenuItem, __AbortMenuItem });
            _TraceDropDownButton.Image = (System.Drawing.Image)resources.GetObject("_TraceDropDownButton.Image");
            _TraceDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _TraceDropDownButton.Name = "_TraceDropDownButton";
            _TraceDropDownButton.Size = new System.Drawing.Size(67, 22);
            _TraceDropDownButton.Text = "Initiation";
            // 
            // _InitiateMenuItem
            // 
            __InitiateMenuItem.CheckOnClick = true;
            __InitiateMenuItem.Name = "__InitiateMenuItem";
            __InitiateMenuItem.Size = new System.Drawing.Size(110, 22);
            __InitiateMenuItem.Text = "Initiate";
            // 
            // _TraceMenuItem
            // 
            _TraceMenuItem.Name = "_TraceMenuItem";
            _TraceMenuItem.Size = new System.Drawing.Size(110, 22);
            _TraceMenuItem.Text = "Trace";
            // 
            // _AbortMenuItem
            // 
            __AbortMenuItem.Name = "__AbortMenuItem";
            __AbortMenuItem.Size = new System.Drawing.Size(110, 22);
            __AbortMenuItem.Text = "Abort";
            // 
            // _ReadingsCountLabel
            // 
            _ReadingsCountLabel.Name = "_ReadingsCountLabel";
            _ReadingsCountLabel.Size = new System.Drawing.Size(13, 22);
            _ReadingsCountLabel.Text = "0";
            _ReadingsCountLabel.ToolTipText = "Buffer count";
            // 
            // _ReadingComboBox
            // 
            __ReadingComboBox.Name = "__ReadingComboBox";
            __ReadingComboBox.Size = new System.Drawing.Size(121, 25);
            __ReadingComboBox.ToolTipText = "Select reading type";
            // 
            // _ClearBufferDisplayButton
            // 
            _ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _ClearBufferDisplayButton.Font = new System.Drawing.Font("Wingdings", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(2));
            _ClearBufferDisplayButton.Image = (System.Drawing.Image)resources.GetObject("_ClearBufferDisplayButton.Image");
            _ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton";
            _ClearBufferDisplayButton.Size = new System.Drawing.Size(25, 22);
            _ClearBufferDisplayButton.Text = string.Empty;
            _ClearBufferDisplayButton.ToolTipText = "Clears the buffer display";
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_ReadingsDataGridView);
            Controls.Add(_ReadingToolStrip);
            Name = "ReadingView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            ((System.ComponentModel.ISupportInitialize)_ReadingsDataGridView).EndInit();
            _ReadingToolStrip.ResumeLayout(false);
            _ReadingToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.DataGridView _ReadingsDataGridView;
        private System.Windows.Forms.ToolStrip _ReadingToolStrip;
        private System.Windows.Forms.ToolStripButton __ReadButton;

        private System.Windows.Forms.ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _ReadingsCountLabel;
        private System.Windows.Forms.ToolStripComboBox __ReadingComboBox;

        private System.Windows.Forms.ToolStripComboBox _ReadingComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged -= ReadingComboBox_SelectedIndexChanged;
                }

                __ReadingComboBox = value;
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged += ReadingComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton _ClearBufferDisplayButton;
        private System.Windows.Forms.ToolStripDropDownButton _TraceDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem __InitiateMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _InitiateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateMenuItem != null)
                {
                    __InitiateMenuItem.Click -= InitiateMenuItem_Click;
                }

                __InitiateMenuItem = value;
                if (__InitiateMenuItem != null)
                {
                    __InitiateMenuItem.Click += InitiateMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _TraceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem __AbortMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AbortMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortMenuItem != null)
                {
                    __AbortMenuItem.Click -= AbortMenuItem_Click;
                }

                __AbortMenuItem = value;
                if (__AbortMenuItem != null)
                {
                    __AbortMenuItem.Click += AbortMenuItem_Click;
                }
            }
        }

        private Core.Controls.ToolStripDropDownButton _ReadingOptionsDropDownButton;
        private System.Windows.Forms.ToolStripMenuItem __AutoScaleEnabledMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AutoScaleEnabledMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoScaleEnabledMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoScaleEnabledMenuItem != null)
                {
                    __AutoScaleEnabledMenuItem.Click -= AutoScaleCheckBox_Click;
                }

                __AutoScaleEnabledMenuItem = value;
                if (__AutoScaleEnabledMenuItem != null)
                {
                    __AutoScaleEnabledMenuItem.Click += AutoScaleCheckBox_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _AverageEnabledMenuItem;
    }
}