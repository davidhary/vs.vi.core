﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace isr.VI.E4990.Forms
{
    [Microsoft.VisualBasic.CompilerServices.DesignerGenerated()]
    public partial class CalibrateView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var MySettings1 = new E4990.My.MySettings();
            _CalLayout = new System.Windows.Forms.TableLayoutPanel();
            _CompensationGroupBox = new System.Windows.Forms.GroupBox();
            _AdapterComboBox = new System.Windows.Forms.ComboBox();
            _AdapterComboBoxLabel = new System.Windows.Forms.Label();
            __AcquireCompensationButton = new System.Windows.Forms.Button();
            __AcquireCompensationButton.Click += new EventHandler(AcquireCompensationButton_Click);
            _LoadCompensationTextBoxLabel = new System.Windows.Forms.Label();
            _ShortCompensationTextBoxLabel = new System.Windows.Forms.Label();
            _OpenCompensationTextBoxLabel = new System.Windows.Forms.Label();
            _FrequencyStimulusTextBox = new System.Windows.Forms.TextBox();
            _FrequencyStimulusTextBoxLabel = new System.Windows.Forms.Label();
            _LoadCompensationTextBox = new System.Windows.Forms.TextBox();
            _ShortCompensationTextBox = new System.Windows.Forms.TextBox();
            _OpenCompensationTextBox = new System.Windows.Forms.TextBox();
            __ApplyLoadButton = new System.Windows.Forms.Button();
            __ApplyLoadButton.Click += new EventHandler(ApplyLoadButton_Click);
            __ApplyShortButton = new System.Windows.Forms.Button();
            __ApplyShortButton.Click += new EventHandler(ApplyShortButton_Click);
            __ApplyOpenButton = new System.Windows.Forms.Button();
            __ApplyOpenButton.Click += new EventHandler(ApplyOpenButton_Click);
            _CalLayout.SuspendLayout();
            _CompensationGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _CalLayout
            // 
            _CalLayout.ColumnCount = 3;
            _CalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _CalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _CalLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _CalLayout.Controls.Add(_CompensationGroupBox, 1, 1);
            _CalLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _CalLayout.Location = new System.Drawing.Point(1, 1);
            _CalLayout.Name = "_CalLayout";
            _CalLayout.RowCount = 3;
            _CalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _CalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _CalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _CalLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0f));
            _CalLayout.Size = new System.Drawing.Size(379, 322);
            _CalLayout.TabIndex = 4;
            // 
            // _CompensationGroupBox
            // 
            _CompensationGroupBox.Controls.Add(_AdapterComboBox);
            _CompensationGroupBox.Controls.Add(_AdapterComboBoxLabel);
            _CompensationGroupBox.Controls.Add(__AcquireCompensationButton);
            _CompensationGroupBox.Controls.Add(_LoadCompensationTextBoxLabel);
            _CompensationGroupBox.Controls.Add(_ShortCompensationTextBoxLabel);
            _CompensationGroupBox.Controls.Add(_OpenCompensationTextBoxLabel);
            _CompensationGroupBox.Controls.Add(_FrequencyStimulusTextBox);
            _CompensationGroupBox.Controls.Add(_FrequencyStimulusTextBoxLabel);
            _CompensationGroupBox.Controls.Add(_LoadCompensationTextBox);
            _CompensationGroupBox.Controls.Add(_ShortCompensationTextBox);
            _CompensationGroupBox.Controls.Add(_OpenCompensationTextBox);
            _CompensationGroupBox.Controls.Add(__ApplyLoadButton);
            _CompensationGroupBox.Controls.Add(__ApplyShortButton);
            _CompensationGroupBox.Controls.Add(__ApplyOpenButton);
            _CompensationGroupBox.Location = new System.Drawing.Point(48, 42);
            _CompensationGroupBox.Name = "_CompensationGroupBox";
            _CompensationGroupBox.Size = new System.Drawing.Size(283, 237);
            _CompensationGroupBox.TabIndex = 0;
            _CompensationGroupBox.TabStop = false;
            _CompensationGroupBox.Text = "Compensation";
            // 
            // _AdapterComboBox
            // 
            _AdapterComboBox.Location = new System.Drawing.Point(164, 202);
            _AdapterComboBox.Name = "_AdapterComboBox";
            _AdapterComboBox.Size = new System.Drawing.Size(108, 25);
            _AdapterComboBox.TabIndex = 10;
            // 
            // _AdapterComboBoxLabel
            // 
            _AdapterComboBoxLabel.AutoSize = true;
            _AdapterComboBoxLabel.Location = new System.Drawing.Point(103, 206);
            _AdapterComboBoxLabel.Name = "_AdapterComboBoxLabel";
            _AdapterComboBoxLabel.Size = new System.Drawing.Size(58, 17);
            _AdapterComboBoxLabel.TabIndex = 9;
            _AdapterComboBoxLabel.Text = "Adapter:";
            // 
            // _AcquireCompensationButton
            // 
            __AcquireCompensationButton.Location = new System.Drawing.Point(6, 201);
            __AcquireCompensationButton.Name = "__AcquireCompensationButton";
            __AcquireCompensationButton.Size = new System.Drawing.Size(55, 26);
            __AcquireCompensationButton.TabIndex = 8;
            __AcquireCompensationButton.Text = "New";
            __AcquireCompensationButton.UseVisualStyleBackColor = true;
            // 
            // _LoadCompensationTextBoxLabel
            // 
            _LoadCompensationTextBoxLabel.AutoSize = true;
            _LoadCompensationTextBoxLabel.Location = new System.Drawing.Point(20, 149);
            _LoadCompensationTextBoxLabel.Name = "_LoadCompensationTextBoxLabel";
            _LoadCompensationTextBoxLabel.Size = new System.Drawing.Size(40, 17);
            _LoadCompensationTextBoxLabel.TabIndex = 7;
            _LoadCompensationTextBoxLabel.Text = "Load:";
            // 
            // _ShortCompensationTextBoxLabel
            // 
            _ShortCompensationTextBoxLabel.AutoSize = true;
            _ShortCompensationTextBoxLabel.Location = new System.Drawing.Point(18, 100);
            _ShortCompensationTextBoxLabel.Name = "_ShortCompensationTextBoxLabel";
            _ShortCompensationTextBoxLabel.Size = new System.Drawing.Size(42, 17);
            _ShortCompensationTextBoxLabel.TabIndex = 5;
            _ShortCompensationTextBoxLabel.Text = "Short:";
            // 
            // _OpenCompensationTextBoxLabel
            // 
            _OpenCompensationTextBoxLabel.AutoSize = true;
            _OpenCompensationTextBoxLabel.Location = new System.Drawing.Point(17, 52);
            _OpenCompensationTextBoxLabel.Name = "_OpenCompensationTextBoxLabel";
            _OpenCompensationTextBoxLabel.Size = new System.Drawing.Size(43, 17);
            _OpenCompensationTextBoxLabel.TabIndex = 3;
            _OpenCompensationTextBoxLabel.Text = "Open:";
            // 
            // _FrequencyStimulusTextBox
            // 
            MySettings1.AdapterType = string.Empty;
            MySettings1.ClearRefractoryPeriod = TimeSpan.Parse("00:00:00.1000000");
            MySettings1.DeviceClearRefractoryPeriod = TimeSpan.Parse("00:00:01.0500000");
            MySettings1.FrequencyArrayReading = string.Empty;
            MySettings1.InitializeTimeout = TimeSpan.Parse("00:00:05");
            MySettings1.InitRefractoryPeriod = TimeSpan.Parse("00:00:00.1000000");
            MySettings1.InterfaceClearRefractoryPeriod = TimeSpan.Parse("00:00:00.5000000");
            MySettings1.LoadCompensationReading = string.Empty;
            MySettings1.LoadResistance = new decimal(new int[] { 10, 0, 0, 0 });
            MySettings1.OpenCompensationReading = string.Empty;
            MySettings1.ResetRefractoryPeriod = TimeSpan.Parse("00:00:00.2000000");
            MySettings1.SessionMessageNotificationLevel = 0;
            MySettings1.SettingsKey = string.Empty;
            MySettings1.ShortCompensationReading = string.Empty;
            MySettings1.TraceLogLevel = TraceEventType.Warning;
            MySettings1.TraceShowLevel = TraceEventType.Warning;
            MySettings1.YardstickInductanceLimit = new decimal(new int[] { 1, 0, 0, 393216 });
            MySettings1.YardstickResistance = new decimal(new int[] { 5, 0, 0, 0 });
            MySettings1.YardstickResistanceTolerance = new decimal(new int[] { 1, 0, 0, 131072 });
            _FrequencyStimulusTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", MySettings1, "FrequencyArrayReading", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            _FrequencyStimulusTextBox.Location = new System.Drawing.Point(63, 22);
            _FrequencyStimulusTextBox.Name = "_FrequencyStimulusTextBox";
            _FrequencyStimulusTextBox.ReadOnly = true;
            _FrequencyStimulusTextBox.Size = new System.Drawing.Size(211, 25);
            _FrequencyStimulusTextBox.TabIndex = 1;
            _FrequencyStimulusTextBox.Text = MySettings1.FrequencyArrayReading;
            // 
            // _FrequencyStimulusTextBoxLabel
            // 
            _FrequencyStimulusTextBoxLabel.AutoSize = true;
            _FrequencyStimulusTextBoxLabel.Location = new System.Drawing.Point(17, 26);
            _FrequencyStimulusTextBoxLabel.Name = "_FrequencyStimulusTextBoxLabel";
            _FrequencyStimulusTextBoxLabel.Size = new System.Drawing.Size(44, 17);
            _FrequencyStimulusTextBoxLabel.TabIndex = 0;
            _FrequencyStimulusTextBoxLabel.Text = "F [Hz]:";
            // 
            // _LoadCompensationTextBox
            // 
            _LoadCompensationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", MySettings1, "LoadCompensationReading", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            _LoadCompensationTextBox.Location = new System.Drawing.Point(62, 149);
            _LoadCompensationTextBox.Multiline = true;
            _LoadCompensationTextBox.Name = "_LoadCompensationTextBox";
            _LoadCompensationTextBox.ReadOnly = true;
            _LoadCompensationTextBox.Size = new System.Drawing.Size(212, 45);
            _LoadCompensationTextBox.TabIndex = 3;
            _LoadCompensationTextBox.Text = MySettings1.LoadCompensationReading;
            // 
            // _ShortCompensationTextBox
            // 
            _ShortCompensationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", MySettings1, "ShortCompensationReading", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            _ShortCompensationTextBox.Location = new System.Drawing.Point(62, 100);
            _ShortCompensationTextBox.Multiline = true;
            _ShortCompensationTextBox.Name = "_ShortCompensationTextBox";
            _ShortCompensationTextBox.ReadOnly = true;
            _ShortCompensationTextBox.Size = new System.Drawing.Size(212, 45);
            _ShortCompensationTextBox.TabIndex = 3;
            _ShortCompensationTextBox.Text = MySettings1.ShortCompensationReading;
            // 
            // _OpenCompensationTextBox
            // 
            _OpenCompensationTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", MySettings1, "OpenCompensationReading", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            _OpenCompensationTextBox.Location = new System.Drawing.Point(62, 51);
            _OpenCompensationTextBox.Multiline = true;
            _OpenCompensationTextBox.Name = "_OpenCompensationTextBox";
            _OpenCompensationTextBox.ReadOnly = true;
            _OpenCompensationTextBox.Size = new System.Drawing.Size(212, 45);
            _OpenCompensationTextBox.TabIndex = 2;
            _OpenCompensationTextBox.Text = MySettings1.OpenCompensationReading;
            // 
            // _ApplyLoadButton
            // 
            __ApplyLoadButton.Location = new System.Drawing.Point(6, 168);
            __ApplyLoadButton.Name = "__ApplyLoadButton";
            __ApplyLoadButton.Size = new System.Drawing.Size(55, 26);
            __ApplyLoadButton.TabIndex = 6;
            __ApplyLoadButton.Text = "Apply";
            __ApplyLoadButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyShortButton
            // 
            __ApplyShortButton.Location = new System.Drawing.Point(6, 119);
            __ApplyShortButton.Name = "__ApplyShortButton";
            __ApplyShortButton.Size = new System.Drawing.Size(55, 26);
            __ApplyShortButton.TabIndex = 4;
            __ApplyShortButton.Text = "Apply";
            __ApplyShortButton.UseVisualStyleBackColor = true;
            // 
            // _ApplyOpenButton
            // 
            __ApplyOpenButton.Location = new System.Drawing.Point(6, 70);
            __ApplyOpenButton.Name = "__ApplyOpenButton";
            __ApplyOpenButton.Size = new System.Drawing.Size(55, 26);
            __ApplyOpenButton.TabIndex = 2;
            __ApplyOpenButton.Text = "Apply";
            __ApplyOpenButton.UseVisualStyleBackColor = true;
            // 
            // CalibrateView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_CalLayout);
            Name = "CalibrateView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _CalLayout.ResumeLayout(false);
            _CompensationGroupBox.ResumeLayout(false);
            _CompensationGroupBox.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _CalLayout;
        private System.Windows.Forms.GroupBox _CompensationGroupBox;
        private System.Windows.Forms.ComboBox _AdapterComboBox;
        private System.Windows.Forms.Label _AdapterComboBoxLabel;
        private System.Windows.Forms.Button __AcquireCompensationButton;

        private System.Windows.Forms.Button _AcquireCompensationButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AcquireCompensationButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AcquireCompensationButton != null)
                {
                    __AcquireCompensationButton.Click -= AcquireCompensationButton_Click;
                }

                __AcquireCompensationButton = value;
                if (__AcquireCompensationButton != null)
                {
                    __AcquireCompensationButton.Click += AcquireCompensationButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _LoadCompensationTextBoxLabel;
        private System.Windows.Forms.Label _ShortCompensationTextBoxLabel;
        private System.Windows.Forms.Label _OpenCompensationTextBoxLabel;
        private System.Windows.Forms.TextBox _FrequencyStimulusTextBox;
        private System.Windows.Forms.Label _FrequencyStimulusTextBoxLabel;
        private System.Windows.Forms.TextBox _LoadCompensationTextBox;
        private System.Windows.Forms.TextBox _ShortCompensationTextBox;
        private System.Windows.Forms.TextBox _OpenCompensationTextBox;
        private System.Windows.Forms.Button __ApplyLoadButton;

        private System.Windows.Forms.Button _ApplyLoadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyLoadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyLoadButton != null)
                {
                    __ApplyLoadButton.Click -= ApplyLoadButton_Click;
                }

                __ApplyLoadButton = value;
                if (__ApplyLoadButton != null)
                {
                    __ApplyLoadButton.Click += ApplyLoadButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ApplyShortButton;

        private System.Windows.Forms.Button _ApplyShortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyShortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyShortButton != null)
                {
                    __ApplyShortButton.Click -= ApplyShortButton_Click;
                }

                __ApplyShortButton = value;
                if (__ApplyShortButton != null)
                {
                    __ApplyShortButton.Click += ApplyShortButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __ApplyOpenButton;

        private System.Windows.Forms.Button _ApplyOpenButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyOpenButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyOpenButton != null)
                {
                    __ApplyOpenButton.Click -= ApplyOpenButton_Click;
                }

                __ApplyOpenButton = value;
                if (__ApplyOpenButton != null)
                {
                    __ApplyOpenButton.Click += ApplyOpenButton_Click;
                }
            }
        }
    }
}