﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{
    [DesignerGenerated()]
    public partial class SourceView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            __ApplySourceSettingButton = new System.Windows.Forms.Button();
            __ApplySourceSettingButton.Click += new EventHandler(ApplySourceSettingButton_Click);
            _SourceFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _LevelNumericLabel = new System.Windows.Forms.Label();
            __SourceFunctionComboBox = new System.Windows.Forms.ComboBox();
            __SourceFunctionComboBox.SelectedValueChanged += new EventHandler(SourceFunctionComboBox_SelectedValueChanged);
            _LevelNumeric = new System.Windows.Forms.NumericUpDown();
            __ApplySourceFunctionButton = new System.Windows.Forms.Button();
            __ApplySourceFunctionButton.Click += new EventHandler(ApplySourceFunctionButton_Click);
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_LevelNumeric).BeginInit();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(381, 324);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__ApplySourceSettingButton);
            _Panel.Controls.Add(_SourceFunctionComboBoxLabel);
            _Panel.Controls.Add(_LevelNumericLabel);
            _Panel.Controls.Add(__SourceFunctionComboBox);
            _Panel.Controls.Add(_LevelNumeric);
            _Panel.Controls.Add(__ApplySourceFunctionButton);
            _Panel.Location = new System.Drawing.Point(39, 99);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(302, 125);
            _Panel.TabIndex = 0;
            // 
            // _ApplySourceSettingButton
            // 
            __ApplySourceSettingButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __ApplySourceSettingButton.Location = new System.Drawing.Point(227, 83);
            __ApplySourceSettingButton.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __ApplySourceSettingButton.Name = "__ApplySourceSettingButton";
            __ApplySourceSettingButton.Size = new System.Drawing.Size(58, 30);
            __ApplySourceSettingButton.TabIndex = 28;
            __ApplySourceSettingButton.Text = "Apply";
            __ApplySourceSettingButton.UseVisualStyleBackColor = true;
            // 
            // _SourceFunctionComboBoxLabel
            // 
            _SourceFunctionComboBoxLabel.AutoSize = true;
            _SourceFunctionComboBoxLabel.Location = new System.Drawing.Point(17, 19);
            _SourceFunctionComboBoxLabel.Name = "_SourceFunctionComboBoxLabel";
            _SourceFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SourceFunctionComboBoxLabel.TabIndex = 26;
            _SourceFunctionComboBoxLabel.Text = "Function:";
            // 
            // _LevelNumericLabel
            // 
            _LevelNumericLabel.AutoSize = true;
            _LevelNumericLabel.Location = new System.Drawing.Point(125, 53);
            _LevelNumericLabel.Name = "_LevelNumericLabel";
            _LevelNumericLabel.Size = new System.Drawing.Size(71, 17);
            _LevelNumericLabel.TabIndex = 30;
            _LevelNumericLabel.Text = "Level [mA]:";
            // 
            // _SourceFunctionComboBox
            // 
            __SourceFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __SourceFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __SourceFunctionComboBox.Items.AddRange(new object[] { "I", "V" });
            __SourceFunctionComboBox.Location = new System.Drawing.Point(79, 15);
            __SourceFunctionComboBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            __SourceFunctionComboBox.Name = "__SourceFunctionComboBox";
            __SourceFunctionComboBox.Size = new System.Drawing.Size(150, 25);
            __SourceFunctionComboBox.TabIndex = 27;
            // 
            // _LevelNumeric
            // 
            _LevelNumeric.Location = new System.Drawing.Point(199, 49);
            _LevelNumeric.Maximum = new decimal(new int[] { 10, 0, 0, 0 });
            _LevelNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 196608 });
            _LevelNumeric.Name = "_LevelNumeric";
            _LevelNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No;
            _LevelNumeric.Size = new System.Drawing.Size(85, 25);
            _LevelNumeric.TabIndex = 31;
            _LevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _LevelNumeric.Value = new decimal(new int[] { 5, 0, 0, 0 });
            // 
            // _ApplySourceFunctionButton
            // 
            __ApplySourceFunctionButton.Location = new System.Drawing.Point(232, 12);
            __ApplySourceFunctionButton.Name = "__ApplySourceFunctionButton";
            __ApplySourceFunctionButton.Size = new System.Drawing.Size(53, 30);
            __ApplySourceFunctionButton.TabIndex = 29;
            __ApplySourceFunctionButton.Text = "Apply";
            __ApplySourceFunctionButton.UseVisualStyleBackColor = true;
            // 
            // SourceView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_Layout);
            Name = "SourceView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(383, 326);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_LevelNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Button __ApplySourceSettingButton;

        private System.Windows.Forms.Button _ApplySourceSettingButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySourceSettingButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySourceSettingButton != null)
                {
                    __ApplySourceSettingButton.Click -= ApplySourceSettingButton_Click;
                }

                __ApplySourceSettingButton = value;
                if (__ApplySourceSettingButton != null)
                {
                    __ApplySourceSettingButton.Click += ApplySourceSettingButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _SourceFunctionComboBoxLabel;
        private System.Windows.Forms.Label _LevelNumericLabel;
        private System.Windows.Forms.ComboBox __SourceFunctionComboBox;

        private System.Windows.Forms.ComboBox _SourceFunctionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SourceFunctionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SourceFunctionComboBox != null)
                {
                    __SourceFunctionComboBox.SelectedValueChanged -= SourceFunctionComboBox_SelectedValueChanged;
                }

                __SourceFunctionComboBox = value;
                if (__SourceFunctionComboBox != null)
                {
                    __SourceFunctionComboBox.SelectedValueChanged += SourceFunctionComboBox_SelectedValueChanged;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _LevelNumeric;
        private System.Windows.Forms.Button __ApplySourceFunctionButton;

        private System.Windows.Forms.Button _ApplySourceFunctionButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySourceFunctionButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySourceFunctionButton != null)
                {
                    __ApplySourceFunctionButton.Click -= ApplySourceFunctionButton_Click;
                }

                __ApplySourceFunctionButton = value;
                if (__ApplySourceFunctionButton != null)
                {
                    __ApplySourceFunctionButton.Click += ApplySourceFunctionButton_Click;
                }
            }
        }
    }
}