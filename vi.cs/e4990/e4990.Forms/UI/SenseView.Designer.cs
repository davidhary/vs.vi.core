﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{
    [DesignerGenerated()]
    public partial class SenseView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _SenseTabControl = new System.Windows.Forms.TabControl();
            _AverageTabPage = new System.Windows.Forms.TabPage();
            _AveragingLayout = new System.Windows.Forms.TableLayoutPanel();
            _AveragingGroupBox = new System.Windows.Forms.GroupBox();
            _ApertureNumeric = new System.Windows.Forms.NumericUpDown();
            __ApplyAveragingButton = new System.Windows.Forms.Button();
            __ApplyAveragingButton.Click += new EventHandler(ApplyAveragingButton_Click);
            _AveragingEnabledCheckBox = new System.Windows.Forms.CheckBox();
            _ApertureNumericLabel = new System.Windows.Forms.Label();
            __RestartAveragingButton = new System.Windows.Forms.Button();
            __RestartAveragingButton.Click += new EventHandler(RestartAveragingButton_Click);
            _AveragingCountNumericLabel = new System.Windows.Forms.Label();
            _AveragingCountNumeric = new System.Windows.Forms.NumericUpDown();
            _SweepTabPage = new System.Windows.Forms.TabPage();
            _SweepLayout = new System.Windows.Forms.TableLayoutPanel();
            _SweepGroupBox = new System.Windows.Forms.GroupBox();
            _LowFrequencyNumericLabel = new System.Windows.Forms.Label();
            __ApplySweepSettingsButton = new System.Windows.Forms.Button();
            __ApplySweepSettingsButton.Click += new EventHandler(ApplySweepSettingsButton_Click);
            _HighFrequencyNumeric = new System.Windows.Forms.NumericUpDown();
            _LowFrequencyNumeric = new System.Windows.Forms.NumericUpDown();
            _HighFrequencyNumericLabel = new System.Windows.Forms.Label();
            _TraceTabPage = new System.Windows.Forms.TabPage();
            _TraceLayout = new System.Windows.Forms.TableLayoutPanel();
            _TraceGroupBox = new System.Windows.Forms.GroupBox();
            __ApplyTracesButton = new System.Windows.Forms.Button();
            __ApplyTracesButton.Click += new EventHandler(ApplyTracesButton_Click);
            _PrimaryTraceParameterComboBox = new System.Windows.Forms.ComboBox();
            _PrimaryTraceParameterComboBoxLabel = new System.Windows.Forms.Label();
            _SecondaryTraceParameterComboBoxLabel = new System.Windows.Forms.Label();
            _SecondaryTraceParameterComboBox = new System.Windows.Forms.ComboBox();
            _MarkersTabPage = new System.Windows.Forms.TabPage();
            _MarkersLayout = new System.Windows.Forms.TableLayoutPanel();
            _MarkersGroupBox = new System.Windows.Forms.GroupBox();
            _MarkerFrequencyComboBox = new System.Windows.Forms.ComboBox();
            _MarkerFrequencyComboBoxLabel = new System.Windows.Forms.Label();
            __ApplyMarkerSettingsButton = new System.Windows.Forms.Button();
            __ApplyMarkerSettingsButton.Click += new EventHandler(ApplyMarkerSettingsButton_Click);
            _SenseTabControl.SuspendLayout();
            _AverageTabPage.SuspendLayout();
            _AveragingLayout.SuspendLayout();
            _AveragingGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_AveragingCountNumeric).BeginInit();
            _SweepTabPage.SuspendLayout();
            _SweepLayout.SuspendLayout();
            _SweepGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_HighFrequencyNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_LowFrequencyNumeric).BeginInit();
            _TraceTabPage.SuspendLayout();
            _TraceLayout.SuspendLayout();
            _TraceGroupBox.SuspendLayout();
            _MarkersTabPage.SuspendLayout();
            _MarkersLayout.SuspendLayout();
            _MarkersGroupBox.SuspendLayout();
            SuspendLayout();
            // 
            // _SenseTabControl
            // 
            _SenseTabControl.Controls.Add(_AverageTabPage);
            _SenseTabControl.Controls.Add(_SweepTabPage);
            _SenseTabControl.Controls.Add(_TraceTabPage);
            _SenseTabControl.Controls.Add(_MarkersTabPage);
            _SenseTabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            _SenseTabControl.Location = new System.Drawing.Point(1, 1);
            _SenseTabControl.Name = "_SenseTabControl";
            _SenseTabControl.SelectedIndex = 0;
            _SenseTabControl.Size = new System.Drawing.Size(379, 322);
            _SenseTabControl.TabIndex = 18;
            // 
            // _AverageTabPage
            // 
            _AverageTabPage.Controls.Add(_AveragingLayout);
            _AverageTabPage.Location = new System.Drawing.Point(4, 26);
            _AverageTabPage.Name = "_AverageTabPage";
            _AverageTabPage.Padding = new System.Windows.Forms.Padding(3);
            _AverageTabPage.Size = new System.Drawing.Size(371, 292);
            _AverageTabPage.TabIndex = 0;
            _AverageTabPage.Text = "Average";
            _AverageTabPage.UseVisualStyleBackColor = true;
            // 
            // _AveragingLayout
            // 
            _AveragingLayout.ColumnCount = 3;
            _AveragingLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _AveragingLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _AveragingLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _AveragingLayout.Controls.Add(_AveragingGroupBox, 1, 1);
            _AveragingLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _AveragingLayout.Location = new System.Drawing.Point(3, 3);
            _AveragingLayout.Name = "_AveragingLayout";
            _AveragingLayout.RowCount = 3;
            _AveragingLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _AveragingLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _AveragingLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _AveragingLayout.Size = new System.Drawing.Size(365, 286);
            _AveragingLayout.TabIndex = 2;
            // 
            // _AveragingGroupBox
            // 
            _AveragingGroupBox.Controls.Add(_ApertureNumeric);
            _AveragingGroupBox.Controls.Add(__ApplyAveragingButton);
            _AveragingGroupBox.Controls.Add(_AveragingEnabledCheckBox);
            _AveragingGroupBox.Controls.Add(_ApertureNumericLabel);
            _AveragingGroupBox.Controls.Add(__RestartAveragingButton);
            _AveragingGroupBox.Controls.Add(_AveragingCountNumericLabel);
            _AveragingGroupBox.Controls.Add(_AveragingCountNumeric);
            _AveragingGroupBox.Location = new System.Drawing.Point(97, 62);
            _AveragingGroupBox.Name = "_AveragingGroupBox";
            _AveragingGroupBox.Size = new System.Drawing.Size(171, 161);
            _AveragingGroupBox.TabIndex = 1;
            _AveragingGroupBox.TabStop = false;
            _AveragingGroupBox.Text = "Averaging";
            // 
            // _ApertureNumeric
            // 
            _ApertureNumeric.Location = new System.Drawing.Point(73, 30);
            _ApertureNumeric.Maximum = new decimal(new int[] { 5, 0, 0, 0 });
            _ApertureNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _ApertureNumeric.Name = "_ApertureNumeric";
            _ApertureNumeric.Size = new System.Drawing.Size(41, 25);
            _ApertureNumeric.TabIndex = 1;
            _ApertureNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _ApplyAveragingButton
            // 
            __ApplyAveragingButton.Location = new System.Drawing.Point(35, 123);
            __ApplyAveragingButton.Name = "__ApplyAveragingButton";
            __ApplyAveragingButton.Size = new System.Drawing.Size(101, 28);
            __ApplyAveragingButton.TabIndex = 2;
            __ApplyAveragingButton.Text = "Apply";
            __ApplyAveragingButton.UseVisualStyleBackColor = true;
            // 
            // _AveragingEnabledCheckBox
            // 
            _AveragingEnabledCheckBox.AutoSize = true;
            _AveragingEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            _AveragingEnabledCheckBox.Location = new System.Drawing.Point(10, 94);
            _AveragingEnabledCheckBox.Name = "_AveragingEnabledCheckBox";
            _AveragingEnabledCheckBox.Size = new System.Drawing.Size(77, 21);
            _AveragingEnabledCheckBox.TabIndex = 4;
            _AveragingEnabledCheckBox.Text = "Enabled:";
            _AveragingEnabledCheckBox.UseVisualStyleBackColor = true;
            // 
            // _ApertureNumericLabel
            // 
            _ApertureNumericLabel.AutoSize = true;
            _ApertureNumericLabel.Location = new System.Drawing.Point(9, 34);
            _ApertureNumericLabel.Name = "_ApertureNumericLabel";
            _ApertureNumericLabel.Size = new System.Drawing.Size(62, 17);
            _ApertureNumericLabel.TabIndex = 0;
            _ApertureNumericLabel.Text = "Aperture:";
            // 
            // _RestartAveragingButton
            // 
            __RestartAveragingButton.Location = new System.Drawing.Point(94, 89);
            __RestartAveragingButton.Name = "__RestartAveragingButton";
            __RestartAveragingButton.Size = new System.Drawing.Size(63, 28);
            __RestartAveragingButton.TabIndex = 5;
            __RestartAveragingButton.Text = "Restart";
            __RestartAveragingButton.UseVisualStyleBackColor = true;
            // 
            // _AveragingCountNumericLabel
            // 
            _AveragingCountNumericLabel.AutoSize = true;
            _AveragingCountNumericLabel.Location = new System.Drawing.Point(26, 67);
            _AveragingCountNumericLabel.Name = "_AveragingCountNumericLabel";
            _AveragingCountNumericLabel.Size = new System.Drawing.Size(45, 17);
            _AveragingCountNumericLabel.TabIndex = 2;
            _AveragingCountNumericLabel.Text = "Count:";
            // 
            // _AveragingCountNumeric
            // 
            _AveragingCountNumeric.Location = new System.Drawing.Point(73, 63);
            _AveragingCountNumeric.Maximum = new decimal(new int[] { 999, 0, 0, 0 });
            _AveragingCountNumeric.Name = "_AveragingCountNumeric";
            _AveragingCountNumeric.Size = new System.Drawing.Size(50, 25);
            _AveragingCountNumeric.TabIndex = 3;
            _AveragingCountNumeric.Value = new decimal(new int[] { 999, 0, 0, 0 });
            // 
            // _SweepTabPage
            // 
            _SweepTabPage.Controls.Add(_SweepLayout);
            _SweepTabPage.Location = new System.Drawing.Point(4, 26);
            _SweepTabPage.Name = "_SweepTabPage";
            _SweepTabPage.Padding = new System.Windows.Forms.Padding(3);
            _SweepTabPage.Size = new System.Drawing.Size(375, 296);
            _SweepTabPage.TabIndex = 1;
            _SweepTabPage.Text = "Sweep";
            _SweepTabPage.UseVisualStyleBackColor = true;
            // 
            // _SweepLayout
            // 
            _SweepLayout.ColumnCount = 3;
            _SweepLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SweepLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _SweepLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SweepLayout.Controls.Add(_SweepGroupBox, 1, 1);
            _SweepLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _SweepLayout.Location = new System.Drawing.Point(3, 3);
            _SweepLayout.Name = "_SweepLayout";
            _SweepLayout.RowCount = 3;
            _SweepLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SweepLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _SweepLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _SweepLayout.Size = new System.Drawing.Size(369, 290);
            _SweepLayout.TabIndex = 18;
            // 
            // _SweepGroupBox
            // 
            _SweepGroupBox.Controls.Add(_LowFrequencyNumericLabel);
            _SweepGroupBox.Controls.Add(__ApplySweepSettingsButton);
            _SweepGroupBox.Controls.Add(_HighFrequencyNumeric);
            _SweepGroupBox.Controls.Add(_LowFrequencyNumeric);
            _SweepGroupBox.Controls.Add(_HighFrequencyNumericLabel);
            _SweepGroupBox.Location = new System.Drawing.Point(58, 76);
            _SweepGroupBox.Name = "_SweepGroupBox";
            _SweepGroupBox.Size = new System.Drawing.Size(253, 138);
            _SweepGroupBox.TabIndex = 17;
            _SweepGroupBox.TabStop = false;
            _SweepGroupBox.Text = "Sweep";
            // 
            // _LowFrequencyNumericLabel
            // 
            _LowFrequencyNumericLabel.AutoSize = true;
            _LowFrequencyNumericLabel.Location = new System.Drawing.Point(26, 34);
            _LowFrequencyNumericLabel.Name = "_LowFrequencyNumericLabel";
            _LowFrequencyNumericLabel.Size = new System.Drawing.Size(124, 17);
            _LowFrequencyNumericLabel.TabIndex = 12;
            _LowFrequencyNumericLabel.Text = "Low Frequency [Hz]:";
            // 
            // _ApplySweepSettingsButton
            // 
            __ApplySweepSettingsButton.Location = new System.Drawing.Point(163, 93);
            __ApplySweepSettingsButton.Name = "__ApplySweepSettingsButton";
            __ApplySweepSettingsButton.Size = new System.Drawing.Size(75, 28);
            __ApplySweepSettingsButton.TabIndex = 16;
            __ApplySweepSettingsButton.Text = "Apply";
            __ApplySweepSettingsButton.UseVisualStyleBackColor = true;
            // 
            // _HighFrequencyNumeric
            // 
            _HighFrequencyNumeric.Increment = new decimal(new int[] { 100000, 0, 0, 0 });
            _HighFrequencyNumeric.Location = new System.Drawing.Point(153, 61);
            _HighFrequencyNumeric.Maximum = new decimal(new int[] { 10000000, 0, 0, 0 });
            _HighFrequencyNumeric.Minimum = new decimal(new int[] { 20, 0, 0, 0 });
            _HighFrequencyNumeric.Name = "_HighFrequencyNumeric";
            _HighFrequencyNumeric.Size = new System.Drawing.Size(85, 25);
            _HighFrequencyNumeric.TabIndex = 15;
            _HighFrequencyNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _HighFrequencyNumeric.ThousandsSeparator = true;
            _HighFrequencyNumeric.Value = new decimal(new int[] { 10000000, 0, 0, 0 });
            // 
            // _LowFrequencyNumeric
            // 
            _LowFrequencyNumeric.Increment = new decimal(new int[] { 100000, 0, 0, 0 });
            _LowFrequencyNumeric.Location = new System.Drawing.Point(153, 30);
            _LowFrequencyNumeric.Maximum = new decimal(new int[] { 10000000, 0, 0, 0 });
            _LowFrequencyNumeric.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            _LowFrequencyNumeric.Name = "_LowFrequencyNumeric";
            _LowFrequencyNumeric.Size = new System.Drawing.Size(85, 25);
            _LowFrequencyNumeric.TabIndex = 13;
            _LowFrequencyNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _LowFrequencyNumeric.ThousandsSeparator = true;
            _LowFrequencyNumeric.Value = new decimal(new int[] { 1000000, 0, 0, 0 });
            // 
            // _HighFrequencyNumericLabel
            // 
            _HighFrequencyNumericLabel.AutoSize = true;
            _HighFrequencyNumericLabel.Location = new System.Drawing.Point(22, 65);
            _HighFrequencyNumericLabel.Name = "_HighFrequencyNumericLabel";
            _HighFrequencyNumericLabel.Size = new System.Drawing.Size(128, 17);
            _HighFrequencyNumericLabel.TabIndex = 14;
            _HighFrequencyNumericLabel.Text = "High Frequency [Hz]:";
            // 
            // _TraceTabPage
            // 
            _TraceTabPage.Controls.Add(_TraceLayout);
            _TraceTabPage.Location = new System.Drawing.Point(4, 26);
            _TraceTabPage.Name = "_TraceTabPage";
            _TraceTabPage.Size = new System.Drawing.Size(375, 296);
            _TraceTabPage.TabIndex = 3;
            _TraceTabPage.Text = "Trace";
            _TraceTabPage.UseVisualStyleBackColor = true;
            // 
            // _TraceLayout
            // 
            _TraceLayout.ColumnCount = 3;
            _TraceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TraceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _TraceLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TraceLayout.Controls.Add(_TraceGroupBox, 1, 1);
            _TraceLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _TraceLayout.Location = new System.Drawing.Point(0, 0);
            _TraceLayout.Name = "_TraceLayout";
            _TraceLayout.RowCount = 3;
            _TraceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TraceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _TraceLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _TraceLayout.Size = new System.Drawing.Size(375, 296);
            _TraceLayout.TabIndex = 0;
            // 
            // _TraceGroupBox
            // 
            _TraceGroupBox.Controls.Add(__ApplyTracesButton);
            _TraceGroupBox.Controls.Add(_PrimaryTraceParameterComboBox);
            _TraceGroupBox.Controls.Add(_PrimaryTraceParameterComboBoxLabel);
            _TraceGroupBox.Controls.Add(_SecondaryTraceParameterComboBoxLabel);
            _TraceGroupBox.Controls.Add(_SecondaryTraceParameterComboBox);
            _TraceGroupBox.Location = new System.Drawing.Point(39, 76);
            _TraceGroupBox.Name = "_TraceGroupBox";
            _TraceGroupBox.Size = new System.Drawing.Size(297, 143);
            _TraceGroupBox.TabIndex = 1;
            _TraceGroupBox.TabStop = false;
            _TraceGroupBox.Text = "Traces";
            // 
            // _ApplyTracesButton
            // 
            __ApplyTracesButton.Location = new System.Drawing.Point(212, 104);
            __ApplyTracesButton.Name = "__ApplyTracesButton";
            __ApplyTracesButton.Size = new System.Drawing.Size(75, 28);
            __ApplyTracesButton.TabIndex = 6;
            __ApplyTracesButton.Text = "Apply";
            __ApplyTracesButton.UseVisualStyleBackColor = true;
            // 
            // _PrimaryTraceParameterComboBox
            // 
            _PrimaryTraceParameterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _PrimaryTraceParameterComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _PrimaryTraceParameterComboBox.Items.AddRange(new object[] { "I", "V" });
            _PrimaryTraceParameterComboBox.Location = new System.Drawing.Point(81, 31);
            _PrimaryTraceParameterComboBox.Name = "_PrimaryTraceParameterComboBox";
            _PrimaryTraceParameterComboBox.Size = new System.Drawing.Size(206, 25);
            _PrimaryTraceParameterComboBox.TabIndex = 4;
            // 
            // _PrimaryTraceParameterComboBoxLabel
            // 
            _PrimaryTraceParameterComboBoxLabel.AutoSize = true;
            _PrimaryTraceParameterComboBoxLabel.Location = new System.Drawing.Point(23, 35);
            _PrimaryTraceParameterComboBoxLabel.Name = "_PrimaryTraceParameterComboBoxLabel";
            _PrimaryTraceParameterComboBoxLabel.Size = new System.Drawing.Size(55, 17);
            _PrimaryTraceParameterComboBoxLabel.TabIndex = 2;
            _PrimaryTraceParameterComboBoxLabel.Text = "Primary:";
            _PrimaryTraceParameterComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SecondaryTraceParameterComboBoxLabel
            // 
            _SecondaryTraceParameterComboBoxLabel.AutoSize = true;
            _SecondaryTraceParameterComboBoxLabel.Location = new System.Drawing.Point(6, 69);
            _SecondaryTraceParameterComboBoxLabel.Name = "_SecondaryTraceParameterComboBoxLabel";
            _SecondaryTraceParameterComboBoxLabel.Size = new System.Drawing.Size(72, 17);
            _SecondaryTraceParameterComboBoxLabel.TabIndex = 3;
            _SecondaryTraceParameterComboBoxLabel.Text = "Secondary:";
            _SecondaryTraceParameterComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SecondaryTraceParameterComboBox
            // 
            _SecondaryTraceParameterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            _SecondaryTraceParameterComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _SecondaryTraceParameterComboBox.Items.AddRange(new object[] { "I", "V" });
            _SecondaryTraceParameterComboBox.Location = new System.Drawing.Point(81, 65);
            _SecondaryTraceParameterComboBox.Name = "_SecondaryTraceParameterComboBox";
            _SecondaryTraceParameterComboBox.Size = new System.Drawing.Size(206, 25);
            _SecondaryTraceParameterComboBox.TabIndex = 5;
            // 
            // _MarkersTabPage
            // 
            _MarkersTabPage.Controls.Add(_MarkersLayout);
            _MarkersTabPage.Location = new System.Drawing.Point(4, 26);
            _MarkersTabPage.Name = "_MarkersTabPage";
            _MarkersTabPage.Size = new System.Drawing.Size(375, 296);
            _MarkersTabPage.TabIndex = 2;
            _MarkersTabPage.Text = "Markers";
            _MarkersTabPage.UseVisualStyleBackColor = true;
            // 
            // _MarkersLayout
            // 
            _MarkersLayout.ColumnCount = 3;
            _MarkersLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MarkersLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _MarkersLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MarkersLayout.Controls.Add(_MarkersGroupBox, 1, 1);
            _MarkersLayout.Dock = System.Windows.Forms.DockStyle.Fill;
            _MarkersLayout.Location = new System.Drawing.Point(0, 0);
            _MarkersLayout.Name = "_MarkersLayout";
            _MarkersLayout.RowCount = 3;
            _MarkersLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MarkersLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _MarkersLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _MarkersLayout.Size = new System.Drawing.Size(375, 296);
            _MarkersLayout.TabIndex = 3;
            // 
            // _MarkersGroupBox
            // 
            _MarkersGroupBox.Controls.Add(_MarkerFrequencyComboBox);
            _MarkersGroupBox.Controls.Add(_MarkerFrequencyComboBoxLabel);
            _MarkersGroupBox.Controls.Add(__ApplyMarkerSettingsButton);
            _MarkersGroupBox.Location = new System.Drawing.Point(48, 96);
            _MarkersGroupBox.Name = "_MarkersGroupBox";
            _MarkersGroupBox.Size = new System.Drawing.Size(279, 103);
            _MarkersGroupBox.TabIndex = 4;
            _MarkersGroupBox.TabStop = false;
            _MarkersGroupBox.Text = "Markers";
            // 
            // _MarkerFrequencyComboBox
            // 
            _MarkerFrequencyComboBox.FormattingEnabled = true;
            _MarkerFrequencyComboBox.Location = new System.Drawing.Point(83, 23);
            _MarkerFrequencyComboBox.Name = "_MarkerFrequencyComboBox";
            _MarkerFrequencyComboBox.Size = new System.Drawing.Size(186, 25);
            _MarkerFrequencyComboBox.TabIndex = 6;
            // 
            // _MarkerFrequencyComboBoxLabel
            // 
            _MarkerFrequencyComboBoxLabel.AutoSize = true;
            _MarkerFrequencyComboBoxLabel.Location = new System.Drawing.Point(10, 27);
            _MarkerFrequencyComboBoxLabel.Name = "_MarkerFrequencyComboBoxLabel";
            _MarkerFrequencyComboBoxLabel.Size = new System.Drawing.Size(70, 17);
            _MarkerFrequencyComboBoxLabel.TabIndex = 5;
            _MarkerFrequencyComboBoxLabel.Text = "Frequency:";
            // 
            // _ApplyMarkerSettingsButton
            // 
            __ApplyMarkerSettingsButton.Location = new System.Drawing.Point(194, 57);
            __ApplyMarkerSettingsButton.Name = "__ApplyMarkerSettingsButton";
            __ApplyMarkerSettingsButton.Size = new System.Drawing.Size(75, 28);
            __ApplyMarkerSettingsButton.TabIndex = 2;
            __ApplyMarkerSettingsButton.Text = "Apply";
            __ApplyMarkerSettingsButton.UseVisualStyleBackColor = true;
            // 
            // SenseView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_SenseTabControl);
            Name = "SenseView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _SenseTabControl.ResumeLayout(false);
            _AverageTabPage.ResumeLayout(false);
            _AveragingLayout.ResumeLayout(false);
            _AveragingGroupBox.ResumeLayout(false);
            _AveragingGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_ApertureNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_AveragingCountNumeric).EndInit();
            _SweepTabPage.ResumeLayout(false);
            _SweepLayout.ResumeLayout(false);
            _SweepGroupBox.ResumeLayout(false);
            _SweepGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_HighFrequencyNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_LowFrequencyNumeric).EndInit();
            _TraceTabPage.ResumeLayout(false);
            _TraceLayout.ResumeLayout(false);
            _TraceGroupBox.ResumeLayout(false);
            _TraceGroupBox.PerformLayout();
            _MarkersTabPage.ResumeLayout(false);
            _MarkersLayout.ResumeLayout(false);
            _MarkersGroupBox.ResumeLayout(false);
            _MarkersGroupBox.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TabControl _SenseTabControl;
        private System.Windows.Forms.TabPage _AverageTabPage;
        private System.Windows.Forms.TableLayoutPanel _AveragingLayout;
        private System.Windows.Forms.GroupBox _AveragingGroupBox;
        private System.Windows.Forms.NumericUpDown _ApertureNumeric;
        private System.Windows.Forms.Button __ApplyAveragingButton;

        private System.Windows.Forms.Button _ApplyAveragingButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyAveragingButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyAveragingButton != null)
                {
                    __ApplyAveragingButton.Click -= ApplyAveragingButton_Click;
                }

                __ApplyAveragingButton = value;
                if (__ApplyAveragingButton != null)
                {
                    __ApplyAveragingButton.Click += ApplyAveragingButton_Click;
                }
            }
        }

        private System.Windows.Forms.CheckBox _AveragingEnabledCheckBox;
        private System.Windows.Forms.Label _ApertureNumericLabel;
        private System.Windows.Forms.Button __RestartAveragingButton;

        private System.Windows.Forms.Button _RestartAveragingButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RestartAveragingButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RestartAveragingButton != null)
                {
                    __RestartAveragingButton.Click -= RestartAveragingButton_Click;
                }

                __RestartAveragingButton = value;
                if (__RestartAveragingButton != null)
                {
                    __RestartAveragingButton.Click += RestartAveragingButton_Click;
                }
            }
        }

        private System.Windows.Forms.Label _AveragingCountNumericLabel;
        private System.Windows.Forms.NumericUpDown _AveragingCountNumeric;
        private System.Windows.Forms.TabPage _SweepTabPage;
        private System.Windows.Forms.TableLayoutPanel _SweepLayout;
        private System.Windows.Forms.GroupBox _SweepGroupBox;
        private System.Windows.Forms.Label _LowFrequencyNumericLabel;
        private System.Windows.Forms.Button __ApplySweepSettingsButton;

        private System.Windows.Forms.Button _ApplySweepSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySweepSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySweepSettingsButton != null)
                {
                    __ApplySweepSettingsButton.Click -= ApplySweepSettingsButton_Click;
                }

                __ApplySweepSettingsButton = value;
                if (__ApplySweepSettingsButton != null)
                {
                    __ApplySweepSettingsButton.Click += ApplySweepSettingsButton_Click;
                }
            }
        }

        private System.Windows.Forms.NumericUpDown _HighFrequencyNumeric;
        private System.Windows.Forms.NumericUpDown _LowFrequencyNumeric;
        private System.Windows.Forms.Label _HighFrequencyNumericLabel;
        private System.Windows.Forms.TabPage _TraceTabPage;
        private System.Windows.Forms.TableLayoutPanel _TraceLayout;
        private System.Windows.Forms.GroupBox _TraceGroupBox;
        private System.Windows.Forms.Button __ApplyTracesButton;

        private System.Windows.Forms.Button _ApplyTracesButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyTracesButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyTracesButton != null)
                {
                    __ApplyTracesButton.Click -= ApplyTracesButton_Click;
                }

                __ApplyTracesButton = value;
                if (__ApplyTracesButton != null)
                {
                    __ApplyTracesButton.Click += ApplyTracesButton_Click;
                }
            }
        }

        private System.Windows.Forms.ComboBox _PrimaryTraceParameterComboBox;
        private System.Windows.Forms.Label _PrimaryTraceParameterComboBoxLabel;
        private System.Windows.Forms.Label _SecondaryTraceParameterComboBoxLabel;
        private System.Windows.Forms.ComboBox _SecondaryTraceParameterComboBox;
        private System.Windows.Forms.TabPage _MarkersTabPage;
        private System.Windows.Forms.TableLayoutPanel _MarkersLayout;
        private System.Windows.Forms.GroupBox _MarkersGroupBox;
        private System.Windows.Forms.ComboBox _MarkerFrequencyComboBox;
        private System.Windows.Forms.Label _MarkerFrequencyComboBoxLabel;
        private System.Windows.Forms.Button __ApplyMarkerSettingsButton;

        private System.Windows.Forms.Button _ApplyMarkerSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyMarkerSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyMarkerSettingsButton != null)
                {
                    __ApplyMarkerSettingsButton.Click -= ApplyMarkerSettingsButton_Click;
                }

                __ApplyMarkerSettingsButton = value;
                if (__ApplyMarkerSettingsButton != null)
                {
                    __ApplyMarkerSettingsButton.Click += ApplyMarkerSettingsButton_Click;
                }
            }
        }
    }
}