﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.E4990.Forms
{
    [DesignerGenerated()]
    public partial class TriggerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(TriggerView));
            _TriggerToolStrip = new System.Windows.Forms.ToolStrip();
            _TriggerToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _TriggerSourceComboLabel = new System.Windows.Forms.ToolStripLabel();
            _TriggerSourceCombo = new Core.Controls.ToolStripComboBox();
            _ContinuousTriggerCheckBox = new Core.Controls.ToolStripCheckBox();
            __ApplyTriggerModeButton = new System.Windows.Forms.ToolStripButton();
            __ApplyTriggerModeButton.Click += new EventHandler(ApplyTriggerModeButton_Click);
            _TriggerToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _TriggerToolStrip
            // 
            _TriggerToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _TriggerToolStripLabel, _TriggerSourceComboLabel, _TriggerSourceCombo, _ContinuousTriggerCheckBox, __ApplyTriggerModeButton });
            _TriggerToolStrip.Location = new System.Drawing.Point(1, 1);
            _TriggerToolStrip.Name = "_TriggerToolStrip";
            _TriggerToolStrip.Size = new System.Drawing.Size(379, 25);
            _TriggerToolStrip.TabIndex = 0;
            _TriggerToolStrip.Text = "ToolStrip1";
            // 
            // _TriggerToolStripLabel
            // 
            _TriggerToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerToolStripLabel.Name = "_TriggerToolStripLabel";
            _TriggerToolStripLabel.Size = new System.Drawing.Size(50, 22);
            _TriggerToolStripLabel.Text = "Trigger:";
            // 
            // _TriggerSourceComboLabel
            // 
            _TriggerSourceComboLabel.Name = "_TriggerSourceComboLabel";
            _TriggerSourceComboLabel.Size = new System.Drawing.Size(46, 22);
            _TriggerSourceComboLabel.Text = "Source:";
            // 
            // _TriggerSourceCombo
            // 
            _TriggerSourceCombo.Name = "_TriggerSourceCombo";
            _TriggerSourceCombo.Size = new System.Drawing.Size(100, 25);
            _TriggerSourceCombo.ToolTipText = "Trigger source";
            // 
            // _ContinuousTriggerCheckBox
            // 
            _ContinuousTriggerCheckBox.Checked = false;
            _ContinuousTriggerCheckBox.Name = "_ContinuousTriggerCheckBox";
            _ContinuousTriggerCheckBox.Size = new System.Drawing.Size(88, 22);
            _ContinuousTriggerCheckBox.Text = "Continuous";
            _ContinuousTriggerCheckBox.ToolTipText = "Checked if continuous trigger";
            // 
            // _ApplyTriggerModeButton
            // 
            __ApplyTriggerModeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            __ApplyTriggerModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ApplyTriggerModeButton.Image = (System.Drawing.Image)resources.GetObject("_ApplyTriggerModeButton.Image");
            __ApplyTriggerModeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ApplyTriggerModeButton.Name = "__ApplyTriggerModeButton";
            __ApplyTriggerModeButton.Size = new System.Drawing.Size(42, 22);
            __ApplyTriggerModeButton.Text = "Apply";
            __ApplyTriggerModeButton.ToolTipText = "Apply trigger settings";
            // 
            // TriggerView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_TriggerToolStrip);
            Name = "TriggerView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _TriggerToolStrip.ResumeLayout(false);
            _TriggerToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.ToolStrip _TriggerToolStrip;
        private System.Windows.Forms.ToolStripLabel _TriggerToolStripLabel;
        private System.Windows.Forms.ToolStripLabel _TriggerSourceComboLabel;
        private Core.Controls.ToolStripComboBox _TriggerSourceCombo;
        private Core.Controls.ToolStripCheckBox _ContinuousTriggerCheckBox;
        private System.Windows.Forms.ToolStripButton __ApplyTriggerModeButton;

        private System.Windows.Forms.ToolStripButton _ApplyTriggerModeButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplyTriggerModeButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplyTriggerModeButton != null)
                {
                    __ApplyTriggerModeButton.Click -= ApplyTriggerModeButton_Click;
                }

                __ApplyTriggerModeButton = value;
                if (__ApplyTriggerModeButton != null)
                {
                    __ApplyTriggerModeButton.Click += ApplyTriggerModeButton_Click;
                }
            }
        }
    }
}