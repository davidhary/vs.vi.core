using System;

namespace isr.VI.Clt10.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the basic <see cref="Facade.MeterView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class MeterView : Facade.MeterView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="MeterView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="MeterView"/>. </returns>
        public static new MeterView Create()
        {
            MeterView view = null;
            try
            {
                view = new MeterView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>" )]
        private Clt10Device Clt10Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( Clt10Device value )
        {
            base.AssignDevice( value );
            this.Clt10Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( HarmonicsMeasureSubsystem ) default );
            }
            else
            {
                this.BindSubsystem( value.HarmonicsMeasureSubsystem );
            }
        }

        #endregion

        #region " HARMONICS MEASURE MODE HANDLING "

        /// <summary> Handles the measure mode changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected override void HandleHarmonicsMeasureModeChanged( HarmonicsMeasureSubsystemBase subsystem )
        {
            base.HandleHarmonicsMeasureModeChanged( subsystem );
            switch ( subsystem.MeasureMode )
            {
                case HarmonicsMeasureMode.Voltage:
                    {
                        // this.BindSubsystem( this.Clt10Device.SenseCurrentSubsystem, "Current");
                        break;
                    }

                case HarmonicsMeasureMode.Decibel:
                    {
                        // this.BindSubsystem( this.Clt10Device.SenseVoltageSubsystem, "Voltage");
                        break;
                    }

            }
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Fetches and displays buffered readings. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void FetchAndDisplayBufferedReadings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} fetching buffered readings";
                _ = this.PublishVerbose( $"{activity};. " );
                // this.DisplayBufferedReadings( this.Clt10Device.TraceSubsystem.QueryReadings( this.Clt10Device.MeasureSubsystem.ReadingAmounts));
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

    }
}
