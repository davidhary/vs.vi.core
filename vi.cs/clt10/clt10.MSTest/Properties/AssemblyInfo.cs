﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "Clt10 VI Tests" )]
[assembly: AssemblyDescription( "Clt10 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Clt10.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
