using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Clt10.MSTest
{

    /// <summary>   (Unit Test Class) a resource manager tests. </summary>
    /// <remarks>
    /// (c) 2021 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2021-03-29 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "Clt10" )]
    public partial class ResourceManagerTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " RESOURCE "

        /// <summary>   (Unit Test Method) visa resource manager should include resource name. </summary>
        /// <remarks>   Finds the resource using the session factory resources manager. </remarks>
        [TestMethod()]
        public void ResourceNameShouldBeIncluded()
        {
            DeviceTests.DeviceManager.AssertVisaResourceManagerShouldIncludeResource( ResourceSettings.Get() );
        }

        /// <summary>   (Unit Test Method) visa resource manger should find resource name. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        [TestMethod()]
        public void ResourceNameShouldBeFound()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            DeviceTests.DeviceManager.AssertVisaSessionBaseShouldFindResource( device, ResourceSettings.Get() );
        }

        #endregion

    }
}
