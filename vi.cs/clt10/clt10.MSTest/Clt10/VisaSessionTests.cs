using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.Clt10.MSTest
{

    /// <summary> Clt10 Visa Session unit tests. </summary>
    /// <remarks>
    /// (c) 2021 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2021-03-29 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "Clt10" )]
    public partial class VisaSessionTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " DEVICE TRACE MESSAGE SHOULD QUEUE "

        /// <summary>   (Unit Test Method) device trace message should be queued. </summary>
        /// <remarks>   Checks if the device adds a trace message to a listener. </remarks>
        [TestMethod()]
        public void TraceMessageShouldBeQueued()
        {
            using var session = VisaSession.Create();
            DeviceTests.DeviceManager.AssertDeviceTraceMessageShouldBeQueued( TestInfo, session );
        }

        #endregion

        #region " VISA SESSION SHOULD OPEN AND CLOSE "

        /// <summary>   (Unit Test Method) visa session should open and close. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        [TestMethod()]
        public void VisaSessionShouldOpenAndClose()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssetVisaSessionBaseShouldOpen( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionBaseShouldClose( TestInfo, device );
            }
        }

        #endregion

        #region " DEVICE SHOULD OPEN AND CLOSE "

        /// <summary>   (Unit Test Method) device status session only should open and close. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        [TestMethod()]
        public void StatusSessionOnlyShouldOpenAndClose()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) device should open and close without device errors. </summary>
        /// <remarks>   Tests opening and closing a VISA session. </remarks>
        [TestMethod()]
        public void DeviceShouldOpenAndCloseWithoutDeviceErrors()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " SERVICE REQUEST AND POLLING TESTS "

        /// <summary>   (Unit Test Method) queries result should be detected. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        [TestMethod()]
        public void QueryResultShouldBeDetected()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertQueryResultShouldBeDetected( TestInfo, device.Session, "AR?",
                                                                         ( byte ) Clt10.HarmonicsStatusRegisterEvent.QueryResultReady, "AR=0" );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) service request handling should be toggled. </summary>
        /// <remarks>
        /// This test will fail the first time it is run if Windows requests access through the Firewall.
        /// </remarks>
        [TestMethod()]
        public void ServiceRequestHandlingShouldBeToggled()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertServiceRequestHandlingShouldToggle( TestInfo, device.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " DEVICE EVENT TESTS "

        /// <summary>   (Unit Test Method) device should handle service request. </summary>
        /// <remarks>
        /// This test will fail the first time it is run if Windows requests access through the Firewall.
        /// </remarks>
        [TestMethod()]
        public void ServiceRequestShouldBeHandled()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                string queryCommand = "GL?";
                string expectedReply = device.Session.Query( TimeSpan.FromMilliseconds( 10 ), queryCommand ).InsertCommonEscapeSequences();
                DeviceTests.DeviceManager.AssertServiceRequestShouldBeHandled( device, queryCommand, 0xD9, expectedReply );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) device should poll query result. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        [TestMethod()]
        public void QueryResultShouldBePolled()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                string queryCommand = "GL?";
                string expectedReply = device.Session.QueryTrimEnd( TimeSpan.FromMilliseconds( 10 ), queryCommand );
                DeviceTests.DeviceManager.AssertQueryResultShouldBePolled( device, queryCommand, 0xD9, expectedReply );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
