using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;
using isr.Core.EscapeSequencesExtensions;
using isr.Core;
using System.Diagnostics;
using Microsoft.VisualBasic.Logging;
using static System.Net.Mime.MediaTypeNames;
using System.Security.Policy;
using System.ComponentModel;
using isr.VI.Tsp.Syntax;

namespace isr.VI.Clt10.MSTest
{

    /// <summary>   (Unit Test Class) the harmonics measure subsystem tests. </summary>
    /// <remarks>
    /// (c) 2021 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2021-03-29 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "Clt10" )]
    public class HarmonicsMeasureSubsystemTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"Initializing {testContext.FullyQualifiedTestClassName} with Nominal Resistance = {HarmonicsSubsystemSettings.ExpectedNominalResistance}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( SubsystemsSettings.Get().Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " HARMONICS SUBSYSTEM SETTINGS "

        private static class HarmonicsSubsystemSettings
        {

            private const AccessRightsMode _AccessRightsMode = VI.Clt10.AccessRightsMode.Normal;
            private const bool _BandwidthLimitingEnabled = false;
            private const double _GeneratorPowerLevel = 0.125;
            private const int _GeneratorTimerTimeSpanMs = 20;
            private const ImpedanceRangeMode _ImpedanceRangeMode = VI.Clt10.ImpedanceRangeMode.LessThan300;
            private const HarmonicsMeasureMode _MeasureMode = VI.HarmonicsMeasureMode.Decibel;
            // single shot cannot be tested... find where this was documented.
            private const MeasurementStartMode _MeasurementStartMode = VI.Clt10.MeasurementStartMode.MeasurementStop;
#if true
            private const double _NominalResistance = 100;
            private const bool _UsingAutoRange = true;
#elif false
            private const double _NominalResistance = 100;
            private const bool _UsingAutoRange = false;
#else
            private const double _NominalResistance = 1000000;
#endif
#if false
            private const double _ThirdHarmonicsIndexHighLimit = 120;
            private const double _ThirdHarmonicsIndexLowLimit = 119;
#elif true
            private const double _ThirdHarmonicsIndexHighLimit = 160;
            private const double _ThirdHarmonicsIndexLowLimit = 120;
#else
            private const double _ThirdHarmonicsIndexHighLimit = 155;
            private const double _ThirdHarmonicsIndexLowLimit = 145;
#endif
            private const bool _VoltmeterOutputEnabled = true;
            private const VoltmeterRangeMode _VoltmeterRangeMode = VI.Clt10.VoltmeterRangeMode.TenMicroVolt;

            static HarmonicsSubsystemSettings()
            {
                HarmonicsSubsystemSettings.ExpectedAccessRightsMode = HarmonicsSubsystemSettings._AccessRightsMode;
                HarmonicsSubsystemSettings.ExpectedBandwidthLimitingEnabled = HarmonicsSubsystemSettings._BandwidthLimitingEnabled;
                HarmonicsSubsystemSettings.ExpectedGeneratorPowerLevel = HarmonicsSubsystemSettings._GeneratorPowerLevel;
                HarmonicsSubsystemSettings.ExpectedGeneratorTimerTimeSpan = TimeSpan.FromMilliseconds( HarmonicsSubsystemSettings._GeneratorTimerTimeSpanMs );
                HarmonicsSubsystemSettings.ExpectedMeasureMode = HarmonicsSubsystemSettings._MeasureMode;
                HarmonicsSubsystemSettings.ExpectedMeasurementStartMode = HarmonicsSubsystemSettings._MeasurementStartMode;
                HarmonicsSubsystemSettings.ExpectedNominalResistance = HarmonicsSubsystemSettings._NominalResistance;
                HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexHighLimit = HarmonicsSubsystemSettings._ThirdHarmonicsIndexHighLimit;
                HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexLowLimit = HarmonicsSubsystemSettings._ThirdHarmonicsIndexLowLimit;
                HarmonicsSubsystemSettings.ExpectedImpedanceRangeMode = HarmonicsSubsystemSettings._ImpedanceRangeMode;
                HarmonicsSubsystemSettings.ExpectedVoltmeterOutputEnabled = HarmonicsSubsystemSettings._VoltmeterOutputEnabled;
                HarmonicsSubsystemSettings.ExpectedUsingAutoRange = HarmonicsSubsystemSettings._UsingAutoRange;
                HarmonicsSubsystemSettings.ExpectedVoltmeterRangeMode = HarmonicsSubsystemSettings.ExpectedUsingAutoRange
                                                                            ? VoltmeterRangeMode.Auto
                                                                            : HarmonicsSubsystemSettings._VoltmeterRangeMode;
            }

            public static void BuildConfiguration()
            {
                HarmonicsSubsystemSettings.BuildConfiguration( HarmonicsSubsystemSettings._GeneratorPowerLevel, HarmonicsSubsystemSettings._NominalResistance,
                                                               HarmonicsSubsystemSettings._ThirdHarmonicsIndexLowLimit,
                                                               HarmonicsSubsystemSettings._ThirdHarmonicsIndexHighLimit, HarmonicsSubsystemSettings._UsingAutoRange );
            }

            public static void BuildConfiguration( double powerLevel, double nominalResistance, double lowThi, double highThi, bool usingAutoRange )
            {
                HarmonicsSubsystemSettings.ExpectedUsingAutoRange = usingAutoRange;
                HarmonicsSubsystemSettings.ExpectedNominalResistance = nominalResistance;
                HarmonicsSubsystemSettings.ExpectedGeneratorPowerLevel = powerLevel;
                HarmonicsSubsystemSettings.ExpectedGeneratorTimerTimeSpan = HarmonicsMeasureConfiguration.SelectGeneratorTimer( nominalResistance, TimeSpan.FromMilliseconds( 20 ) );
                HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexHighLimit = highThi;
                HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexLowLimit = lowThi;
                HarmonicsSubsystemSettings.ExpectedImpedanceRangeMode = HarmonicsMeasureSubsystem.ToImpedanceRangeMode( nominalResistance );
                HarmonicsSubsystemSettings.ExpectedGeneratorOutputLevel = Math.Sqrt( HarmonicsSubsystemSettings.ExpectedGeneratorPowerLevel * HarmonicsSubsystemSettings.ExpectedNominalResistance );
                HarmonicsSubsystemSettings.ExpectedVoltmeterHighLimit = ExpectedGeneratorOutputLevel * Math.Pow( 10, -ExpectedThirdHarmonicsIndexLowLimit / 20 );
                HarmonicsSubsystemSettings.ExpectedVoltmeterLowLimit = ExpectedGeneratorOutputLevel * Math.Pow( 10, -ExpectedThirdHarmonicsIndexHighLimit / 20 );
                HarmonicsSubsystemSettings.ExpectedVoltmeterRangeMode = usingAutoRange
                    ? VoltmeterRangeMode.Auto
                    : HarmonicsMeasureSubsystem.ToVoltmeterRangeMode( HarmonicsSubsystemSettings.ExpectedVoltmeterHighLimit );

                Configuration = new() {
                    AccessRightsMode = ( int ) HarmonicsSubsystemSettings.ExpectedAccessRightsMode,
                    UsingAutoRange = HarmonicsSubsystemSettings.ExpectedUsingAutoRange,
                    BandwidthLimitingEnabled = HarmonicsSubsystemSettings.ExpectedBandwidthLimitingEnabled,
                    GeneratorPowerLevel = HarmonicsSubsystemSettings.ExpectedGeneratorPowerLevel,
                    GeneratorTimerTimeSpan = HarmonicsSubsystemSettings.ExpectedGeneratorTimerTimeSpan,
                    MeasurementStartMode = ( int ) HarmonicsSubsystemSettings.ExpectedMeasurementStartMode,
                    MeasureMode = HarmonicsSubsystemSettings.ExpectedMeasureMode,
                    VoltmeterOutputEnabled = HarmonicsSubsystemSettings.ExpectedVoltmeterOutputEnabled,
                    VoltmeterRangeMode = ( int ) HarmonicsSubsystemSettings.ExpectedVoltmeterRangeMode,
                    NominalResistance = HarmonicsSubsystemSettings.ExpectedNominalResistance,
                    ThirdHarmonicsIndexHighLimit = HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexHighLimit,
                    ThirdHarmonicsIndexLowLimit = HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexLowLimit
                };
                HarmonicsMeasureSubsystem.EstablishConfigurationRangeModes( HarmonicsSubsystemSettings.Configuration );
            }

            public static HarmonicsMeasureConfiguration Configuration { get; set; }

            public static VI.Clt10.AccessRightsMode ExpectedAccessRightsMode { get; set; } = HarmonicsSubsystemSettings._AccessRightsMode;

            public static bool ExpectedUsingAutoRange { get; set; } = HarmonicsSubsystemSettings._UsingAutoRange;

            public static VI.Clt10.MeasurementStartMode ExpectedMeasurementStartMode { get; set; } = HarmonicsSubsystemSettings._MeasurementStartMode;

            public static bool ExpectedBandwidthLimitingEnabled { get; set; } = _BandwidthLimitingEnabled;

            public static double ExpectedGeneratorPowerLevel { get; set; } = HarmonicsSubsystemSettings._GeneratorPowerLevel;

            public static TimeSpan ExpectedGeneratorTimerTimeSpan { get; set; } = TimeSpan.FromMilliseconds( HarmonicsSubsystemSettings._GeneratorTimerTimeSpanMs );

            public static VI.HarmonicsMeasureMode ExpectedMeasureMode { get; set; } = HarmonicsSubsystemSettings._MeasureMode;

            public static double ExpectedNominalResistance { get; set; } = HarmonicsSubsystemSettings._NominalResistance;

            public static double ExpectedThirdHarmonicsIndexHighLimit { get; set; } = HarmonicsSubsystemSettings._ThirdHarmonicsIndexHighLimit;
            public static double ExpectedThirdHarmonicsIndexLowLimit { get; set; } = HarmonicsSubsystemSettings._ThirdHarmonicsIndexLowLimit;

            public static VI.Clt10.ImpedanceRangeMode ExpectedImpedanceRangeMode { get; set; } = HarmonicsSubsystemSettings._ImpedanceRangeMode;

            public static VI.Clt10.VoltmeterRangeMode ExpectedVoltmeterRangeMode { get; set; } = HarmonicsSubsystemSettings._VoltmeterRangeMode;

            public static double ExpectedGeneratorOutputLevel { get; set; } = Math.Sqrt( HarmonicsSubsystemSettings.ExpectedGeneratorPowerLevel * HarmonicsSubsystemSettings.ExpectedNominalResistance );

            public static bool ExpectedVoltmeterOutputEnabled { get; set; } = HarmonicsSubsystemSettings._VoltmeterOutputEnabled;

            public static double ExpectedVoltmeterHighLimit { get; set; } = ExpectedGeneratorOutputLevel * Math.Pow( 10, -ExpectedThirdHarmonicsIndexLowLimit / 20 );

            public static double ExpectedVoltmeterLowLimit { get; set; } = ExpectedGeneratorOutputLevel * Math.Pow( 10, -ExpectedThirdHarmonicsIndexHighLimit / 20 );

        }

        #endregion

        #region " HARMONICS CONFIGURATION TESTS "

        /// <summary>   Asset configuration should be set. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        private static void AssetConfigurationShouldBeSet()
        {
            HarmonicsSubsystemSettings.BuildConfiguration();
            Assert.AreEqual( ( int ) HarmonicsSubsystemSettings.ExpectedAccessRightsMode, HarmonicsSubsystemSettings.Configuration.AccessRightsMode,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.AccessRightsMode )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedBandwidthLimitingEnabled, HarmonicsSubsystemSettings.Configuration.BandwidthLimitingEnabled,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.BandwidthLimitingEnabled )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedGeneratorTimerTimeSpan, HarmonicsSubsystemSettings.Configuration.GeneratorTimerTimeSpan,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.GeneratorTimerTimeSpan )} should be set" );
            Assert.AreEqual( ( int ) HarmonicsSubsystemSettings.ExpectedImpedanceRangeMode, HarmonicsSubsystemSettings.Configuration.ImpedanceRangeMode,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.ImpedanceRangeMode )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedGeneratorOutputLevel, HarmonicsSubsystemSettings.Configuration.GeneratorOutputLevel, 0.01 * HarmonicsSubsystemSettings.Configuration.GeneratorOutputLevel,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.GeneratorOutputLevel )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedMeasureMode, HarmonicsSubsystemSettings.Configuration.MeasureMode,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.MeasureMode )} should be set" );
            Assert.AreEqual( ( int ) HarmonicsSubsystemSettings.ExpectedVoltmeterRangeMode, HarmonicsSubsystemSettings.Configuration.VoltmeterRangeMode,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.VoltmeterRangeMode )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedNominalResistance, HarmonicsSubsystemSettings.Configuration.NominalResistance,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.NominalResistance )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedGeneratorPowerLevel, HarmonicsSubsystemSettings.Configuration.GeneratorPowerLevel, 0.001 * HarmonicsSubsystemSettings.Configuration.GeneratorPowerLevel,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.GeneratorPowerLevel )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexHighLimit, HarmonicsSubsystemSettings.Configuration.ThirdHarmonicsIndexHighLimit, 0.01 * HarmonicsSubsystemSettings.Configuration.ThirdHarmonicsIndexHighLimit,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.ThirdHarmonicsIndexHighLimit )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedThirdHarmonicsIndexLowLimit, HarmonicsSubsystemSettings.Configuration.ThirdHarmonicsIndexLowLimit, 0.01 * HarmonicsSubsystemSettings.Configuration.ThirdHarmonicsIndexLowLimit,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.ThirdHarmonicsIndexLowLimit )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedVoltmeterOutputEnabled, HarmonicsSubsystemSettings.Configuration.VoltmeterOutputEnabled,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.VoltmeterOutputEnabled )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedVoltmeterHighLimit, HarmonicsSubsystemSettings.Configuration.VoltmeterHighLimit, 0.01 * HarmonicsSubsystemSettings.Configuration.VoltmeterHighLimit,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.VoltmeterHighLimit )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedVoltmeterLowLimit, HarmonicsSubsystemSettings.Configuration.VoltmeterLowLimit, 0.01 * HarmonicsSubsystemSettings.Configuration.VoltmeterHighLimit,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.VoltmeterLowLimit )} should be set" );
            Assert.AreEqual( ( int ) HarmonicsSubsystemSettings.ExpectedMeasurementStartMode, HarmonicsSubsystemSettings.Configuration.MeasurementStartMode,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.MeasurementStartMode )} should be set" );
            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedUsingAutoRange, HarmonicsSubsystemSettings.Configuration.UsingAutoRange,
                      $"{typeof( HarmonicsMeasureConfiguration )}.{nameof( HarmonicsMeasureConfiguration.UsingAutoRange )} should be set" );
        }

        /// <summary>   Assert subsystem current too high. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        public static void AssertConfigurationCurrentTooHigh()
        {
            HarmonicsSubsystemSettings.BuildConfiguration();
            double powerLevel = HarmonicsSubsystemSettings.Configuration.GeneratorPowerLevel;
            double maximumPowerLevel = Math.Pow( HarmonicsSubsystemSettings.Configuration.GeneratorCurrentMaximum, 2 ) * HarmonicsSubsystemSettings.Configuration.NominalResistance;
            HarmonicsSubsystemSettings.Configuration.GeneratorPowerLevel = maximumPowerLevel + 0.1;
            try
            {
                Assert.IsTrue( HarmonicsSubsystemSettings.Configuration.IsGeneratorCurrentExceedsLimit(),
                    $" Current should Exceed limit for generator voltage of {HarmonicsSubsystemSettings.Configuration.GeneratorOutputLevel} and resistance of {HarmonicsSubsystemSettings.Configuration.NominalResistance }" );
            }
            catch
            {
                throw;
            }
            finally
            {
                // restore the power level.
                HarmonicsSubsystemSettings.Configuration.GeneratorPowerLevel = powerLevel;
            }
        }

        /// <summary>   (Unit Test Method) configuration should be set. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        [TestMethod()]
        public void ConfigurationShouldBeSet()
        {
            HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();
        }

        /// <summary>   (Unit Test Method) configuration should report high current. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        [TestMethod()]
        public void ConfigurationShouldReportHighCurrent()
        {
            HarmonicsMeasureSubsystemTests.AssertConfigurationCurrentTooHigh();
        }

        #endregion

        #region " HARMONICS SUBSYSTEM TESTS "

        /// <summary>   Verify subsystem configuration. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        public static void AssertSubsystemShouldBeConfigured( HarmonicsMeasureSubsystem subsystem )
        {
            HarmonicsSubsystemSettings.BuildConfiguration();

            Assert.AreEqual( ( int? ) HarmonicsSubsystemSettings.ExpectedAccessRightsMode, subsystem.AccessRightsMode,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.AccessRightsMode )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.AccessRightsMode )} set to {subsystem.AccessRightsMode}" );

            Assert.AreEqual( ( bool? ) HarmonicsSubsystemSettings.ExpectedBandwidthLimitingEnabled, subsystem.BandwidthLimitingEnabled,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.BandwidthLimitingEnabled )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.BandwidthLimitingEnabled )} set to {subsystem.BandwidthLimitingEnabled}" );

            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedGeneratorOutputLevel, subsystem.GeneratorOutputLevel.GetValueOrDefault( 0 ),
                             0.01 * subsystem.GeneratorOutputLevel.GetValueOrDefault( 0 ),
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.GeneratorOutputLevel )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.GeneratorOutputLevel )} set to {subsystem.GeneratorOutputLevel}" );

            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedGeneratorTimerTimeSpan.TotalMilliseconds, subsystem.GeneratorTimer.GetValueOrDefault( 0 ), 1.0,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.GeneratorTimer )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.GeneratorTimer )} set to {subsystem.GeneratorTimer}ms" );

            Assert.AreEqual( ( int? ) HarmonicsSubsystemSettings.ExpectedImpedanceRangeMode, subsystem.ImpedanceRangeMode,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.ImpedanceRangeMode )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.ImpedanceRangeMode )} set to {subsystem.ImpedanceRangeMode}" );

            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedMeasureMode, subsystem.MeasureMode.GetValueOrDefault( HarmonicsMeasureMode.Voltage ),
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasureMode )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasureMode )} set to {subsystem.MeasureMode}" );

            Assert.AreEqual( ( int? ) HarmonicsSubsystemSettings.ExpectedMeasurementStartMode, subsystem.MeasurementStartMode,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasurementStartMode )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasurementStartMode )} set to {subsystem.MeasurementStartMode}" );

            Assert.AreEqual( ( bool? ) HarmonicsSubsystemSettings.ExpectedVoltmeterOutputEnabled, subsystem.VoltmeterOutputEnabled,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterOutputEnabled )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterOutputEnabled )} set to {subsystem.VoltmeterOutputEnabled}" );

            Assert.AreEqual( ( int? ) HarmonicsSubsystemSettings.ExpectedVoltmeterRangeMode, subsystem.VoltmeterRangeMode,
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} set to {subsystem.VoltmeterRangeMode}" );

            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedVoltmeterHighLimit, subsystem.VoltmeterHighLimit.GetValueOrDefault( 0 ),
                             0.1 * subsystem.VoltmeterHighLimit.GetValueOrDefault( 0 ),
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterHighLimit )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterHighLimit )} set to {subsystem.VoltmeterHighLimit}" );

            Assert.AreEqual( HarmonicsSubsystemSettings.ExpectedVoltmeterLowLimit, subsystem.VoltmeterLowLimit.GetValueOrDefault( 0 ),
                             0.1 * subsystem.VoltmeterLowLimit.GetValueOrDefault( 0 ),
                      $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterLowLimit )} should be set" );
            Console.Out.WriteLine( $"{typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterLowLimit )} set to {subsystem.VoltmeterLowLimit}" );

        }

        /// <summary>   Assert subsystem should be clear state. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        private static void AssertSubsystemShouldBeClearState( HarmonicsMeasureSubsystem subsystem )
        {
            // this is tested when the system has not yet been modified or queried, so the cached values should reflect the initial state.
            Assert.AreEqual( subsystem.AccessRightsMode, subsystem.QueryAccessRightsMode().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.AccessRightsMode )} should be set" );
            Assert.AreEqual( subsystem.BandwidthLimitingEnabled, subsystem.QueryBandwidthLimitingEnabled().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.BandwidthLimitingEnabled )} should be set" );
            Assert.AreEqual( subsystem.GeneratorOutputLevel, subsystem.QueryGeneratorOutputLevel().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.GeneratorOutputLevel )} should be set" );
            Assert.AreEqual( subsystem.GeneratorTimer, subsystem.QueryGeneratorTimer().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.GeneratorTimer )} should be set" );
            Assert.AreEqual( subsystem.ImpedanceRangeMode, subsystem.QueryImpedanceRangeMode().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.ImpedanceRangeMode )} should be set" );
            Assert.AreEqual( subsystem.MeasureMode, subsystem.QueryMeasureMode().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasureMode )} should be set" );
            Assert.AreEqual( subsystem.MeasurementStartMode, subsystem.QueryMeasurementStartMode().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasurementStartMode )} should be set" );
            Assert.AreEqual( subsystem.VoltmeterOutputEnabled, subsystem.QueryVoltmeterOutputEnabled().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterOutputEnabled )} should be set" );
            Assert.AreEqual( subsystem.VoltmeterRangeMode, subsystem.QueryVoltmeterRangeMode().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} should be set" );
            Assert.AreEqual( subsystem.VoltmeterHighLimit, subsystem.QueryVoltmeterHighLimit().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterHighLimit )} should be set" );
            Assert.AreEqual( subsystem.VoltmeterLowLimit, subsystem.QueryVoltmeterLowLimit().ParsedValue,
                      $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.VoltmeterLowLimit )} should be set" );
        }

        /// <summary>   Assert applying measurement start mode. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The measurement start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingMeasurementStartMode( HarmonicsMeasureSubsystem subsystem, Clt10.MeasurementStartMode value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.MeasurementStartMode )} = {value}";
            (int parsedValue, WriteInfo<int> writeInfo, QueryParseInfo<int> queryParseInfo) = subsystem.ApplyMeasurementStartMode( ( int ) value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( ( int ) value, parsedValue, $"{nameof( HarmonicsMeasureSubsystem.MeasurementStartMode )} should be set" );
        }

        /// <summary>   Assert applying access rights mode. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The access rights mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingAccessRightsMode( HarmonicsMeasureSubsystem subsystem, AccessRightsMode value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.AccessRightsMode )} = {value}";
            (int parsedValue, WriteInfo<int> writeInfo, QueryParseInfo<int> queryParseInfo) = subsystem.ApplyAccessRightsMode( ( int ) value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( ( int ) value, parsedValue, $"{nameof( HarmonicsMeasureSubsystem.AccessRightsMode )} should be set" );

        }

        /// <summary>   Assert applying access rights mode using service requests. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The access rights mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingAccessRightsMode( Clt10Device device, AccessRightsMode value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.AccessRightsMode )} = {( int ) value}:{value}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteAccessRightsMode( value );
            _ = device.HarmonicsMeasureSubsystem.WriteAccessRightsModeQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( ( int ) value, device.HarmonicsMeasureSubsystem.AccessRightsMode.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying Harmonics measure mode. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingHarmonicsMeasureMode( HarmonicsMeasureSubsystem subsystem, VI.HarmonicsMeasureMode value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.MeasureMode )} = {value}";
            (VI.HarmonicsMeasureMode parsedValue, WriteInfo<int> writeInfo, QueryParseInfo<int> queryParseInfo) = subsystem.ApplyMeasureMode( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( ( int ) value, ( int ) parsedValue, $"{nameof( HarmonicsMeasureSubsystem.MeasureMode )} should be set" );
        }

        /// <summary>   Assert applying measure mode using service request. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingHarmonicsMeasureMode( Clt10Device device, HarmonicsMeasureMode value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.MeasureMode )} = {( int ) value}:{value}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteMeasureMode( value );
            _ = device.HarmonicsMeasureSubsystem.WriteMeasureModeQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.MeasureMode.GetValueOrDefault( value == HarmonicsMeasureMode.Decibel ? HarmonicsMeasureMode.Voltage : HarmonicsMeasureMode.Decibel ),
                $"Service request {activity} should set the expected value" ); ;
        }

        /// <summary>   Assert applying Bandwidth Limiting enabled. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingBandwidthLimitingEnabled( HarmonicsMeasureSubsystem subsystem, bool value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.BandwidthLimitingEnabled )} = {value}";
            (bool parsedValue, WriteBooleanInfo writeInfo, QueryParseBooleanInfo queryParseInfo) = subsystem.ApplyBandwidthLimitingEnabled( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, $"{nameof( HarmonicsMeasureSubsystem.BandwidthLimitingEnabled )} should be set" );
        }

        /// <summary>   Assert applying Bandwidth Limiting enabled. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingBandwidthLimitingEnabled( Clt10Device device, bool value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.BandwidthLimitingEnabled )} = {value}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteBandwidthLimitingEnabled( value );
            _ = device.HarmonicsMeasureSubsystem.WriteBandwidthLimitingEnabledQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.BandwidthLimitingEnabled.GetValueOrDefault( !value ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying impedance range. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingImpedanceRange( HarmonicsMeasureSubsystem subsystem, double value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.ImpedanceRange )} = {value}";
            (double parsedValue, WriteInfo<double> writeInfo, QueryParseInfo<double> queryParseInfo) = subsystem.ApplyImpedanceRange( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, 1, $"{nameof( HarmonicsMeasureSubsystem.ImpedanceRangeMode )} should be set" );
        }

        /// <summary>   Assert applying impedance range. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingImpedanceRange( Clt10Device device, double value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.ImpedanceRange )} = {value:0.###}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteImpedanceRange( value );
            _ = device.HarmonicsMeasureSubsystem.WriteImpedanceRangeQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.ImpedanceRange.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }


        /// <summary>   Assert applying generator timer. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingGeneratorTimer( HarmonicsMeasureSubsystem subsystem, int value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.GeneratorTimer )} = {value}";
            (double parsedValue, WriteInfo<int> writeInfo, QueryParseInfo<int> queryParseInfo) = subsystem.ApplyGeneratorTimer( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, $"{nameof( HarmonicsMeasureSubsystem.GeneratorTimer )} should be set" );
        }

        /// <summary>   Assert applying generator timer. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingGeneratorTimer( Clt10Device device, int value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.GeneratorTimer )} = {value}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteGeneratorTimer( value );
            _ = device.HarmonicsMeasureSubsystem.WriteGeneratorTimerQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.GeneratorTimer.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying Voltmeter Output enabled. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingVoltmeterOutputEnabled( HarmonicsMeasureSubsystem subsystem, bool value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.VoltmeterOutputEnabled )} = {value}";
            (bool parsedValue, WriteBooleanInfo writeInfo, QueryParseBooleanInfo queryParseInfo) = subsystem.ApplyVoltmeterOutputEnabled( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, $"{nameof( HarmonicsMeasureSubsystem.VoltmeterOutputEnabled )} should be set" );
        }

        /// <summary>   Assert applying Voltmeter Output enabled. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingVoltmeterOutputEnabled( Clt10Device device, bool value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.VoltmeterOutputEnabled )} = {value}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterOutputEnabled( value );
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterOutputEnabledQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.VoltmeterOutputEnabled.GetValueOrDefault( !value ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying Voltmeter Range. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingVoltmeterRange( HarmonicsMeasureSubsystem subsystem, double value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.VoltmeterRange )} = {value}";
            (double parsedValue, WriteInfo<double> writeInfo, QueryParseInfo<double> queryParseInfo) = subsystem.ApplyVoltmeterRange( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, 1e-7, $"{nameof( HarmonicsMeasureSubsystem.VoltmeterRange )} should be set" );
        }

        /// <summary>   Assert applying Voltmeter Range. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingVoltmeterRange( Clt10Device device, double value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.VoltmeterRange )} = {value:E3}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterRange( value );
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterRangeQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.VoltmeterRange.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying voltmeter high limit. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingVoltmeterHighLimit( HarmonicsMeasureSubsystem subsystem, double value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.VoltmeterHighLimit )} = {value}";
            (double parsedValue, WriteInfo<double> writeInfo, QueryParseInfo<double> queryParseInfo) = subsystem.ApplyVoltmeterHighLimit( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, 1e-7, $"{nameof( HarmonicsMeasureSubsystem.VoltmeterHighLimit )} should be set" );
        }

        /// <summary>   Assert applying voltmeter high limit. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingVoltmeterHighLimit( Clt10Device device, double value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.VoltmeterHighLimit )} = {value:E3}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterHighLimit( value );
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterHighLimitQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.VoltmeterHighLimit.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying voltmeter low limit. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingVoltmeterLowLimit( HarmonicsMeasureSubsystem subsystem, double value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.VoltmeterLowLimit )} = {value}";
            (double parsedValue, WriteInfo<double> writeInfo, QueryParseInfo<double> queryParseInfo) = subsystem.ApplyVoltmeterLowLimit( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, 1e-7, $"{nameof( HarmonicsMeasureSubsystem.VoltmeterLowLimit )} should be set" );
        }

        /// <summary>   Assert applying voltmeter low limit. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingVoltmeterLowLimit( Clt10Device device, double value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.VoltmeterLowLimit )} = {value:E3}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterLowLimit( value );
            _ = device.HarmonicsMeasureSubsystem.WriteVoltmeterLowLimitQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.VoltmeterLowLimit.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Assert applying generator output level. </summary>
        /// <remarks>   David, 2021-04-05. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="value">        The start mode. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertApplyingGeneratorOutputLevel( HarmonicsMeasureSubsystem subsystem, double value, bool checkStatus = false )
        {
            string activity = $"Applying {nameof( HarmonicsMeasureSubsystem.GeneratorOutputLevel )} = {value}";
            (double parsedValue, WriteInfo<double> writeInfo, QueryParseInfo<double> queryParseInfo) = subsystem.ApplyGeneratorOutputLevel( value, checkStatus );
            Console.Out.WriteLine( activity );
            Console.Out.WriteLine( writeInfo.BuildInfoReport() );
            Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
            Assert.AreEqual( value, parsedValue, 1e-7, $"{nameof( HarmonicsMeasureSubsystem.GeneratorOutputLevel )} should be set" );
        }

        /// <summary>   Assert applying generator output level. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <param name="device">   The device. </param>
        /// <param name="value">    The start mode. </param>
        /// <param name="timeout">  The timeout. </param>
        private void AssertApplyingGeneratorOutputLevel( Clt10Device device, double value, TimeSpan timeout )
        {
            // clear the last reading for proper monitoring
            device.ServiceRequestReading = string.Empty;
            device.ServiceRequestFailureMessage = string.Empty;

            string activity = $"setting {nameof( HarmonicsMeasureSubsystem.GeneratorOutputLevel )} = {value:G5}";
            Console.Out.WriteLine( activity );
            Stopwatch sw = Stopwatch.StartNew();
            _ = device.HarmonicsMeasureSubsystem.WriteGeneratorOutputLevel( value );
            _ = device.HarmonicsMeasureSubsystem.WriteGeneratorOutputLevelQueryCommand();

            // wait for the reply or timeout.
            device.StartAwaitingServiceRequestReadingTask( timeout ).Wait();

            Console.Out.WriteLine( $"{activity} returned '{device.ServiceRequestReading}' in {sw.ElapsedMilliseconds:0}ms" );

            Assert.IsTrue( string.IsNullOrWhiteSpace( device.ServiceRequestFailureMessage ),
                $"Service request {activity} failed reporting {device.ServiceRequestFailureMessage}" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ),
                $"Service request {activity} timed out" );
            Assert.AreEqual( value, device.HarmonicsMeasureSubsystem.GeneratorOutputLevel.GetValueOrDefault( -1 ),
                $"Service request {activity} should set the expected value" );
        }

        /// <summary>   Asset harmonics measure should trigger and fetch a data value. </summary>
        /// <remarks>   David, 2021-03-31. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssetShouldTriggerFetchDataValue( HarmonicsMeasureSubsystem subsystem, bool checkStatus = false )
        {
            // remember initial value
            // var initialMeasurementStartMode = subsystem.MeasurementStartMode.GetValueOrDefault( ( int ) Clt10.MeasurementStartMode.MeasurementStop );

            try
            {
                string activity = $"Triggering one shot and reading value";
                (double? ParsedValue, ExecuteInfo ExecuteInfo, QueryParseInfo<double> queryParseInfo) = subsystem.TriggerFetch( checkStatus );
                Console.Out.WriteLine( activity );
                Console.Out.WriteLine( ExecuteInfo.BuildInfoReport() );
                Console.Out.WriteLine( queryParseInfo.BuildInfoReport() );
                Assert.IsNotNull( queryParseInfo.HasValue,
                    $"A value should be read sending '{ExecuteInfo.SentMessage.InsertCommonEscapeSequences()}' received : '{queryParseInfo.ReceivedMessage.InsertCommonEscapeSequences()}'" );
            }
            catch ( Exception )
            {
                throw;
            }
            finally
            {
                // toggle start mode to Stop
                this.AssertApplyingMeasurementStartMode( subsystem, MeasurementStartMode.MeasurementStop );

            }
        }

        /// <summary>   Assert should read data value. </summary>
        /// <remarks>   David, 2021-04-06. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertShouldReadDataValue( HarmonicsMeasureSubsystem subsystem, bool checkStatus = false )
        {
            try
            {
                (double? ParsedValue, ExecuteInfo ExecuteInfo, QueryParseInfo<double> queryParseInfo) = subsystem.Read( checkStatus );
                Assert.IsNotNull( ParsedValue, $"A value should be read after sending '{ExecuteInfo.SentMessage}' and receiving '{queryParseInfo.ReceivedMessage}'" );
                Console.Out.WriteLine( $"Read value: {queryParseInfo.ParsedValue}" );
            }
            catch ( Exception )
            {
                throw;
            }
            finally
            {
                // toggle start mode to Stop
                this.AssertApplyingMeasurementStartMode( subsystem, MeasurementStartMode.MeasurementStop );
            }
        }

        /// <summary>   Debug this code. </summary>
        /// <remarks>   David, 2021-04-13. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="device">   The device. </param>
        internal void DebugThisCode( Clt10Device device )
        {
#if false
            var (hasError, Details, StatusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusByte() );
            if ( hasError )
                throw new InvalidOperationException( $"Ready to apply measure mode. Status error 0x{StatusByte:X2}: {Details}" );

            Assert.AreEqual( device.HarmonicsMeasureSubsystem.MeasureMode, device.HarmonicsMeasureSubsystem.QueryMeasureMode().ParsedValue,
                    $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasureMode )} should be set" );
            device.Session.WriteLine( "VD?" );
            (hasError, Details, StatusByte) = device.StatusSubsystem.IsStatusError( (int) device.Session.ReadStatusByte() );
            if ( hasError )
                throw new InvalidOperationException( $"Ready to apply measure mode. Status error 0x{StatusByte:X2}: {Details}" );
            string receivedMessage = device.Session.ReadFreeLine();

            string terminaion = new ( device.Session.TerminationCharacters() );

            // device.Session.WriteLine( "VD,dB" );
            device.HarmonicsMeasureSubsystem.WriteMeasureMode( HarmonicsMeasureMode.Decibel );

            (hasError, Details, StatusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusByte() );
            if ( hasError )
                throw new InvalidOperationException( $"After applying measure mode. Status error 0x{StatusByte:X2}: {Details}" );
               
            device.Session.WriteLine( "VD?" );
            (hasError, Details, StatusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusByte() );
            if ( hasError )
                throw new InvalidOperationException( $"Ready to apply measure mode. Status error 0x{StatusByte:X2}: {Details}" );

            terminaion = new( device.Session.TerminationCharacters() );

            receivedMessage = device.Session.ReadFreeLine();

            (hasError, Details, StatusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusByte() );
            if ( hasError )
                throw new InvalidOperationException( $"After applying measure mode. Status error 0x{StatusByte:X2}: {Details}" );

            Assert.AreEqual( device.HarmonicsMeasureSubsystem.MeasureMode, device.HarmonicsMeasureSubsystem.QueryMeasureMode().ParsedValue,
                    $"Initial {typeof( HarmonicsMeasureSubsystem )}.{nameof( HarmonicsMeasureSubsystem.MeasureMode )} should be set" );

            (hasError, Details, StatusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusByte() );
            if ( hasError )
                throw new InvalidOperationException( $"Reading after apply measure mode. Status error 0x{StatusByte:X2}: {Details}" );
#endif
        }

        /// <summary>   Assert should configure. </summary>
        /// <remarks>   David, 2021-04-14. </remarks>
        /// <param name="device">       The device. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        private void AssertShouldConfigure( Clt10Device device, bool checkStatus = false )
        {

            // single shot cannot be tested here 
            this.AssertApplyingMeasurementStartMode( device.HarmonicsMeasureSubsystem, MeasurementStartMode.MeasurementStop, checkStatus );

            this.AssertApplyingAccessRightsMode( device.HarmonicsMeasureSubsystem, AccessRightsMode.Denial, checkStatus );
            this.AssertApplyingAccessRightsMode( device.HarmonicsMeasureSubsystem, AccessRightsMode.Normal, checkStatus );

            this.AssertApplyingBandwidthLimitingEnabled( device.HarmonicsMeasureSubsystem, true, checkStatus );
            this.AssertApplyingBandwidthLimitingEnabled( device.HarmonicsMeasureSubsystem, false, checkStatus );

            this.AssertApplyingImpedanceRange( device.HarmonicsMeasureSubsystem, HarmonicsMeasureSubsystem.TopImpedanceRangeModeValue, checkStatus );
            this.AssertApplyingImpedanceRange( device.HarmonicsMeasureSubsystem, 30000, checkStatus );
            this.AssertApplyingImpedanceRange( device.HarmonicsMeasureSubsystem, 3000, checkStatus );
            this.AssertApplyingImpedanceRange( device.HarmonicsMeasureSubsystem, 299.5, checkStatus );

            this.AssertApplyingGeneratorTimer( device.HarmonicsMeasureSubsystem, 50, checkStatus );
            this.AssertApplyingGeneratorTimer( device.HarmonicsMeasureSubsystem, 20, checkStatus );

            this.AssertApplyingHarmonicsMeasureMode( device.HarmonicsMeasureSubsystem, VI.HarmonicsMeasureMode.Decibel, checkStatus );
            this.AssertApplyingHarmonicsMeasureMode( device.HarmonicsMeasureSubsystem, VI.HarmonicsMeasureMode.Voltage, checkStatus );

            this.AssertApplyingVoltmeterOutputEnabled( device.HarmonicsMeasureSubsystem, false, checkStatus );
            this.AssertApplyingVoltmeterOutputEnabled( device.HarmonicsMeasureSubsystem, true, checkStatus );

            this.AssertApplyingVoltmeterRange( device.HarmonicsMeasureSubsystem, 1e-6, checkStatus );
            this.AssertApplyingVoltmeterRange( device.HarmonicsMeasureSubsystem, 100E-6, checkStatus );

            this.AssertApplyingVoltmeterHighLimit( device.HarmonicsMeasureSubsystem, 50e-6, checkStatus );
            this.AssertApplyingVoltmeterLowLimit( device.HarmonicsMeasureSubsystem, 0.5e-6, checkStatus );

            this.AssertApplyingGeneratorOutputLevel( device.HarmonicsMeasureSubsystem, 1.58, checkStatus );

        }

        /// <summary>   (Unit Test Method) should configure. </summary>
        /// <remarks>   David, 2021-03-28. <para>
        ///  Settings:  Turn Around: 1ms; Read Delay: 1ms; Status Read Delay: 1ms </para><para>
        /// C:\My\Libraries\VS\IO\VI\VI\Clt10\Tests\bin\_log\testhost.net472.x86-2021-04-14.log </para><para>
        /// Applying MeasurementStartMode = MeasurementStop </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0' </para><para>
        ///   Write Time: 00.003; Read Time: 00.003; Query Time: 00.007; </para><para>
        ///
        /// Applying AccessRightsMode = Denial </para><para>
        ///   Sent Value 2; Command 'AR,{0}'; Sent 'AR,2' </para><para>
        ///   Write Time: 00.004; </para><para>
        ///   Command 'AR?'; Sent 'AR?'; Parsed 2:'AR=2\r\n':'2' </para><para>
        ///   Write Time: 00.002; Read Time: 00.003; Query Time: 00.006; </para><para>
        ///
        /// Applying AccessRightsMode = Normal </para><para>
        ///   Sent Value 0; Command 'AR,{0}'; Sent 'AR,0' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'AR?'; Sent 'AR?'; Parsed 0:'AR=0\r\n':'0' </para><para>
        ///   Write Time: 00.003; Read Time: 00.002; Query Time: 00.006; </para><para>
        ///
        /// Applying BandwidthLimitingEnabled = True </para><para>
        ///   Value True; Command 'BW,ON'; Sent 'BW,ON' </para><para>
        ///   Write Time: 00.004; </para><para>
        ///   Command 'BW?'; Sent 'BW?'; Received True:'BW=ON\r\n':'ON' </para><para>
        ///   Write Time: 00.003; Read Time: 00.003; Query Time: 00.006; </para><para>
        ///
        /// Applying BandwidthLimitingEnabled = False </para><para>
        ///   Value False; Command 'BW,OFF'; Sent 'BW,OFF' </para><para>
        ///   Write Time: 00.007; </para><para>
        ///   Command 'BW?'; Sent 'BW?'; Received False:'BW=OFF\r\n':'OFF' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007; </para><para>
        ///
        /// Applying ImpedanceRange = 1000000000 </para><para>
        ///   Sent Value 1000000000; Command 'ZX,{0}'; Sent 'ZX,4' </para><para>
        ///   Write Time: 00.219; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 1000000000:'ZX=4\r\n':'4' </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.006; </para><para>
        ///
        /// Applying ImpedanceRange = 30000 </para><para>
        ///   Sent Value 30000; Command 'ZX,{0}'; Sent 'ZX,3' </para><para>
        ///   Write Time: 00.006; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 30000:'ZX=3\r\n':'3' </para><para>
        ///   Write Time: 00.003; Read Time: 00.003; Query Time: 00.007; </para><para>
        ///
        /// Applying ImpedanceRange = 3000 </para><para>
        ///   Sent Value 3000; Command 'ZX,{0}'; Sent 'ZX,2' </para><para>
        ///   Write Time: 00.219; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 3000:'ZX=2\r\n':'2' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007; </para><para>
        ///
        /// Applying ImpedanceRange = 299.5 </para><para>
        ///   Sent Value 300; Command 'ZX,{0}'; Sent 'ZX,1' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 300:'ZX=1\r\n':'1' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007; </para><para>
        ///
        /// Applying GeneratorTimer = 50 </para><para>
        ///   Sent Value 50; Command 'GT,{0}'; Sent 'GT,50' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'GT?'; Sent 'GT?'; Parsed 50:'GT=50ms\r\n':'50' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.008; </para><para>
        ///
        /// Applying GeneratorTimer = 20 </para><para>
        ///   Sent Value 20; Command 'GT,{0}'; Sent 'GT,20' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'GT?'; Sent 'GT?'; Parsed 20:'GT=20ms\r\n':'20' </para><para>
        ///   Write Time: 00.004; Read Time: 00.004; Query Time: 00.009; </para><para>
        ///
        /// Applying MeasureMode = Decibel </para><para>
        ///   Sent Value 1; Command 'VD,{0}'; Sent 'VD,dB' </para><para>
        ///   Write Time: 00.004; </para><para>
        ///   Command 'VD?'; Sent 'VD?'; Parsed 1:'VD=dB\r\n':'VD=dB' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.008; </para><para>
        ///
        /// Applying MeasureMode = Voltage </para><para>
        ///   Sent Value 0; Command 'VD,{0}'; Sent 'VD,Volt' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'VD?'; Sent 'VD?'; Parsed 0:'VD=Volt\r\n':'VD=Volt' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.008; </para><para>
        ///
        /// Applying VoltmeterOutputEnabled = False
        ///   Value False; Command 'VM,OFF'; Sent 'VM,OFF'
        ///   Write Time: 00.005;
        ///   Command 'VM?'; Sent 'VM?'; Received False:'VM=OFF\r\n':'OFF'
        ///   Write Time: 00.003; Read Time: 00.004; Query Time: 00.007;
        ///
        /// Applying VoltmeterOutputEnabled = True
        ///   Value True; Command 'VM,ON'; Sent 'VM,ON'
        ///   Write Time: 00.005;
        ///   Command 'VM?'; Sent 'VM?'; Received True:'VM=ON\r\n':'ON'
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007;
        ///   
        /// Applying VoltageMeasureRange = 1E-06 </para><para>
        ///   Sent Value 1E-06; Command 'VR,{0}'; Sent 'VR,1' </para><para>
        ///   Write Time: 00.006; </para><para>
        ///   Command 'VR?'; Sent 'VR?'; Parsed 1E-06:'VR=1uV\r\n':'1' </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007; </para><para>
        ///
        /// Applying VoltageMeasureRange = 0.0001 </para><para>
        ///   Sent Value 0.0001; Command 'VR,{0}'; Sent 'VR,3' </para><para>
        ///   Write Time: 00.005; </para><para>
        ///   Command 'VR?'; Sent 'VR?'; Parsed 0.0001:'VR=100uV\r\n':'100' </para><para>
        ///   Write Time: 00.004; Read Time: 00.004; Query Time: 00.009; </para><para>
        ///
        /// Applying VoltmeterHighLimit = 5E-05 </para><para>
        ///   Sent Value 5E-05; Command 'LH,50uV'; Sent 'LH,50uV' </para><para>
        ///   Write Time: 00.007; </para><para>
        ///   Command 'LH?'; Sent 'LH?'; Parsed 5E-05:'LH=50.00uV\r\n':'50.00' </para><para>
        ///   Write Time: 00.004; Read Time: 00.004; Query Time: 00.008; </para><para>
        ///
        /// Applying VoltmeterLowLimit = 5E-07 </para><para>
        ///   Sent Value 5E-07; Command 'LL,0.5uV'; Sent 'LL,0.5uV' </para><para>
        ///   Write Time: 00.007; </para><para>
        ///   Command 'LL?'; Sent 'LL?'; Parsed 5E-07:'LL=0.500uV\r\n':'0.500' </para><para>
        ///   Write Time: 00.004; Read Time: 00.004; Query Time: 00.009; </para><para>
        ///
        /// Applying GeneratorOutputLevel = 1.58 </para><para>
        ///   Sent Value 1.58; Command 'GL,{0}V'; Sent 'GL,1.58V' </para><para>
        ///   Write Time: 00.008; </para><para>
        ///   Command 'GL?'; Sent 'GL?'; Parsed 1.58:'GL=1.580V\r\n':'1.580' </para><para>
        ///   Write Time: 00.003; Read Time: 00.004; Query Time: 00.008; </para><para>
        /// 
        ///     </para></remarks>
        [TestMethod()]
        public void ShouldConfigure()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeClearState( device.HarmonicsMeasureSubsystem );

                HarmonicsSubsystemSettings.BuildConfiguration();

                this.AssertShouldConfigure( device );

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) should configure status aware. </summary>
        /// <remarks>   David, 2021-04-13. <para>
        ///  Settings:  Turn Around: 1ms; Read Delay: 1ms; Status Read Delay: 1ms </para><para>
        /// C:\My\Libraries\VS\IO\VI\VI\Clt10\Tests\bin\_log\testhost.net472.x86-2021-04-14.log </para><para>
        /// Applying MeasurementStartMode = MeasurementStop </para><para>
        ///   Sent Value 0; Command 'MS,0'; Sent 'MS,0' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Total Write Time: 00.010; </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.014; </para><para>
        ///
        /// Applying AccessRightsMode = Denial </para><para>
        ///   Sent Value 2; Command 'AR,2'; Sent 'AR,2' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Total Write Time: 00.012; </para><para>
        ///   Command 'AR?'; Sent 'AR?'; Parsed 2:'AR=2\r\n':'2' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying AccessRightsMode = Normal </para><para>
        ///   Sent Value 0; Command 'AR,0'; Sent 'AR,0' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Total Write Time: 00.010; </para><para>
        ///   Command 'AR?'; Sent 'AR?'; Parsed 0:'AR=0\r\n':'0' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.004; Query Time: 00.016; </para><para>
        ///
        /// Applying BandwidthLimitingEnabled = True </para><para>
        ///   Value True; Command 'BW,ON'; Sent 'BW,ON' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Total Write Time: 00.010; </para><para>
        ///   Command 'BW?'; Sent 'BW?'; Received True:'BW=ON\r\n':'ON' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying BandwidthLimitingEnabled = False </para><para>
        ///   Value False; Command 'BW,OFF'; Sent 'BW,OFF' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.006; Total Write Time: 00.013; </para><para>
        ///   Command 'BW?'; Sent 'BW?'; Received False:'BW=OFF\r\n':'OFF' </para><para>
        ///   Write Ready Delay: 00.004; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying ImpedanceRange = 1000000000 </para><para>
        ///   Sent Value 1000000000; Command 'ZX,4'; Sent 'ZX,4' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.218; Total Write Time: 00.225; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 1000000000:'ZX=4\r\n':'4' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.014; </para><para>
        ///
        /// Applying ImpedanceRange = 30000 </para><para>
        ///   Sent Value 30000; Command 'ZX,3'; Sent 'ZX,3' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Total Write Time: 00.010; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 30000:'ZX=3\r\n':'3' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.013; </para><para>
        ///
        /// Applying ImpedanceRange = 3000 </para><para>
        ///   Sent Value 3000; Command 'ZX,2'; Sent 'ZX,2' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.219; Total Write Time: 00.224; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 3000:'ZX=2\r\n':'2' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying ImpedanceRange = 299.5 </para><para>
        ///   Sent Value 300; Command 'ZX,1'; Sent 'ZX,1' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Total Write Time: 00.010; </para><para>
        ///   Command 'ZX?'; Sent 'ZX?'; Parsed 300:'ZX=1\r\n':'1' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying GeneratorTimer = 50 </para><para>
        ///   Sent Value 50; Command 'GT,50'; Sent 'GT,50' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.005; Total Write Time: 00.011; </para><para>
        ///   Command 'GT?'; Sent 'GT?'; Parsed 50:'GT=50ms\r\n':'50' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying GeneratorTimer = 20 </para><para>
        ///   Sent Value 20; Command 'GT,20'; Sent 'GT,20' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.005; Total Write Time: 00.011; </para><para>
        ///   Command 'GT?'; Sent 'GT?'; Parsed 20:'GT=20ms\r\n':'20' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.004; Query Time: 00.016; </para><para>
        ///
        /// Applying VoltmeterOutputEnabled = False
        ///   Value False; Command 'VM,OFF'; Sent 'VM,OFF'
        ///   Write Ready Delay: 00.003; Write Time: 00.006; Total Write Time: 00.013;
        ///   Command 'VM?'; Sent 'VM?'; Received False:'VM=OFF\r\n':'OFF'
        ///   Write Ready Delay: 00.002; Write Time: 00.003; Read Delay: 00.002; Read Time: 00.003; Query Time: 00.011;
        ///
        /// Applying VoltmeterOutputEnabled = True
        ///   Value True; Command 'VM,ON'; Sent 'VM,ON'
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Total Write Time: 00.010;
        ///   Command 'VM?'; Sent 'VM?'; Received True:'VM=ON\r\n':'ON'
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003;
        ///
        /// Applying MeasureMode = Decibel </para><para>
        ///   Sent Value 1; Command 'VD,dB'; Sent 'VD,dB' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.005; Total Write Time: 00.011; </para><para>
        ///   Command 'VD?'; Sent 'VD?'; Parsed 1:'VD=dB\r\n':'VD=dB' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.003; Read Delay: 00.002; Read Time: 00.003; Query Time: 00.013; </para><para>
        ///
        /// Applying MeasureMode = Voltage </para><para>
        ///   Sent Value 0; Command 'VD,Volt'; Sent 'VD,Volt' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.006; Total Write Time: 00.012; </para><para>
        ///   Command 'VD?'; Sent 'VD?'; Parsed 0:'VD=Volt\r\n':'VD=Volt' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.013; </para><para>
        ///
        /// Applying VoltageMeasureRange = 1E-06 </para><para>
        ///   Sent Value 1E-06; Command 'VR,1'; Sent 'VR,1' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Total Write Time: 00.011; </para><para>
        ///   Command 'VR?'; Sent 'VR?'; Parsed 1E-06:'VR=1uV\r\n':'1' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.003; Query Time: 00.015; </para><para>
        ///
        /// Applying VoltageMeasureRange = 0.0001 </para><para>
        ///   Sent Value 0.0001; Command 'VR,3'; Sent 'VR,3' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Total Write Time: 00.010; </para><para>
        ///   Command 'VR?'; Sent 'VR?'; Parsed 0.0001:'VR=100uV\r\n':'100' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read </para><para>
        ///
        /// Applying VoltageMeasureRange = 0.0001 </para><para>
        ///   Sent Value 0.0001; Command 'VR,3'; Sent 'VR,3' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Total Write Time: 00.010; </para><para>
        ///   Command 'VR?'; Sent 'VR?'; Parsed 0.0001:'VR=100uV\r\n':'100' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.004; Query Time: 00.015;
        ///
        /// Applying VoltmeterHighLimit = 5E-05 </para><para>
        ///   Sent Value 5E-05; Command 'LH,50uV'; Sent 'LH,50uV' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.006; Total Write Time: 00.013; </para><para>
        ///   Command 'LH?'; Sent 'LH?'; Parsed 5E-05:'LH=50.00uV\r\n':'50.00' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.004; Query Time: 00.016; </para><para>
        ///
        /// Applying VoltmeterLowLimit = 5E-07 </para><para>
        ///   Sent Value 5E-07; Command 'LL,0.5uV'; Sent 'LL,0.5uV' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.007; Total Write Time: 00.012; </para><para>
        ///   Command 'LL?'; Sent 'LL?'; Parsed 5E-07:'LL=0.500uV\r\n':'0.500' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.004; Query Time: 00.014; </para><para>
        ///
        /// Applying GeneratorOutputLevel = 1.58 </para><para>
        ///   Sent Value 1.58; Command 'GL,1.58V'; Sent 'GL,1.58V' </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.008; Total Write Time: 00.014; </para><para>
        ///   Command 'GL?'; Sent 'GL?'; Parsed 1.58:'GL=1.580V\r\n':'1.580' </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.004; Read Delay: 00.003; Read Time: 00.004; Query Time: 00.016; </para><para>
        /// </para></remarks>
        [TestMethod()]
        public void ShouldConfigureStatusAware()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeClearState( device.HarmonicsMeasureSubsystem );

                HarmonicsSubsystemSettings.BuildConfiguration();

                this.AssertShouldConfigure( device, true );

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration, true );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );



            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) should read decibels. </summary>
        /// <remarks>   David, 2021-03-31. <para>
        /// Test Name:	ShouldReadDecibels  </para><para>
        /// Test Outcome:	Passed  </para><para>
        /// Generator timer is set at 20ms  </para><para>
        /// 
        /// Read value: 149.7  </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.005;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;  </para><para>
        ///
        /// Read value: 150.2  </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.004; Query Time: 00.008;  </para><para>
        ///
        /// Read value: 147.7   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.006;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;  </para><para></para><para>
        ///   </para><para>
        /// Read value: 147.8   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.004;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;  </para><para>
        ///   </para><para>
        /// Read value: 149.3   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.005;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.008;  </para><para>
        ///   </para><para>
        /// Read value: 148.9   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.005;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;  </para><para>
        ///   </para><para>
        /// Read value: 148.5   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.005;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;  </para><para>
        ///   </para><para>
        /// Read value: 148.7   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.004;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;  </para><para>
        ///   </para><para>
        /// Read value: 151.2   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.004;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007;  </para><para>
        ///   </para><para>
        /// Read value: 148.2   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop   </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'   </para><para>
        ///   Write Time: 00.004;   </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;  </para><para>
        ///   </para><para>
        ///        Read 10 values in 583ms at 58ms per reading  </para><para>
        ///
        /// 1.46 = System.Math.Pow( 10, -6.415 ) / System.Math.Pow( 10, -6.58 ) </para><para>
        ///  </para></remarks>
        [TestMethod()]
        public void ShouldReadDecibels()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeClearState( device.HarmonicsMeasureSubsystem );

                HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

                //  this is not necessary when the measure mode is set last.
                //  this.AssertMeasureMode( device.HarmonicsMeasureSubsystem, HarmonicsMeasureMode.Decibel );
                // check that we are not in auto mode.
#if false
                // 7873: Auto range is allowed
                Assert.AreNotEqual( ( int ) Clt10.VoltmeterRangeMode.Auto, device.HarmonicsMeasureSubsystem.QueryVoltmeterRangeMode().ParsedValue,
                    $"{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} should not be in auto mode" );
#endif

                // report generator timer:
                double generatorTimer = device.HarmonicsMeasureSubsystem.QueryGeneratorTimer().ParsedValue;
                Console.Out.WriteLine( $"Generator timer is set at {generatorTimer}ms" );

                // repeat taking data multiple times.
                int count = 10;
                Stopwatch sw = Stopwatch.StartNew();
                for ( int i = 0; i < count; i++ )
                {
                    this.AssertShouldReadDataValue( device.HarmonicsMeasureSubsystem );
                }
                sw.Stop();
                Console.Out.WriteLine( $"Read {count} values in {sw.ElapsedMilliseconds}ms at {(sw.ElapsedMilliseconds / count):0}ms per reading" );

            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) should trigger fetch decibels. </summary>
        /// <remarks>   David, 2021-04-14. <para>
        /// Test Name:	ShouldTriggerFetchDecibels </para><para>
        /// Test Outcome:	Passed   </para><para>
        /// Generator timer is set at 20ms   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 148.7:'VM=148.7dB\r\n':'VM=148.7dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.029; Query Time: 00.034;   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 150.3:'VM=150.3dB\r\n':'VM=150.3dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.029; Query Time: 00.034;   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 146.8:'VM=146.8dB\r\n':'VM=146.8dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.033; Query Time: 00.039;   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.005;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 149.1:'VM=149.1dB\r\n':'VM=149.1dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.037; Query Time: 00.042;   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.005;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 148.6:'VM=148.6dB\r\n':'VM=148.6dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.034; Query Time: 00.040;   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.003; Read Time: 00.003; Query Time: 00.007;   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 148.2:'VM=148.2dB\r\n':'VM=148.2dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.035; Query Time: 00.041;   </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;   </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 148.9:'VM=148.9dB\r\n':'VM=148.9dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.033; Query Time: 00.039;   </para><para>
        ///  </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.004; Query Time: 00.009;   </para><para>
        ///  </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 148.9:'VM=148.9dB\r\n':'VM=148.9dB'  </para><para>
        ///   Write Time: 00.004; Read Time: 00.033; Query Time: 00.038;   </para><para>
        ///  </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.005;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        ///  </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 149.6:'VM=149.6dB\r\n':'VM=149.6dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.035; Query Time: 00.041;   </para><para>
        ///  </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        ///  </para><para>
        /// Triggering one shot and reading value  </para><para>
        ///   Message to send MS,2; Sent 'MS,2'  </para><para>
        ///   Write Time: 00.006;  </para><para>
        ///   Command 'VM,1'; Sent 'VM,1'; Parsed 147.7:'VM=147.7dB\r\n':'VM=147.7dB'  </para><para>
        ///   Write Time: 00.005; Read Time: 00.033; Query Time: 00.039;   </para><para>
        ///  </para><para>
        /// Applying MeasurementStartMode = MeasurementStop  </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'  </para><para>
        ///   Write Time: 00.004;  </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'   </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        ///  </para><para>
        /// Triggered Fetched 10 values in 593ms at 59ms per reading   </para><para>
        /// </para></remarks>
        [TestMethod()]
        public void ShouldTriggerFetchDecibels()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeClearState( device.HarmonicsMeasureSubsystem );

                HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

                //  this is not necessary when the measure mode is set last.
                //  this.AssertMeasureMode( device.HarmonicsMeasureSubsystem, HarmonicsMeasureMode.Decibel );
                // check that we are not in auto mode.
#if false
                // 7873: auto range is allowed now
                Assert.AreNotEqual( ( int ) Clt10.VoltmeterRangeMode.Auto, device.HarmonicsMeasureSubsystem.QueryVoltmeterRangeMode().ParsedValue,
                    $"{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} should not be in auto mode" );
#endif

                // report generator timer:
                double generatorTimer = device.HarmonicsMeasureSubsystem.QueryGeneratorTimer().ParsedValue;
                Console.Out.WriteLine( $"Generator timer is set at {generatorTimer}ms" );

                // repeat taking data multiple times.
                int count = 10;
                Stopwatch sw = Stopwatch.StartNew();
                for ( int i = 0; i < count; i++ )
                {
                    this.AssetShouldTriggerFetchDataValue( device.HarmonicsMeasureSubsystem );
                }
                sw.Stop();
                Console.Out.WriteLine( $"Triggered Fetched {count} values in {sw.ElapsedMilliseconds}ms at {(sw.ElapsedMilliseconds / count):0}ms per reading" );

            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) should read decibels status aware. </summary>
        /// <remarks>   David, 2021-04-14. <para>
        /// Test Name:	ShouldReadDecibelsStatsuAware </para><para>
        /// C:\My\Libraries\VS\IO\VI\VI\Clt10\Tests\bin\_log\testhost.net472.x86-2021-04-17.log  </para><para>
        /// Generator timer is set at 20ms                                 </para><para>
        /// Read value: 149                                                </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        ///   Read value: 148.5                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;   </para><para>
        ///   Read value: 148.5                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.004;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;   </para><para>
        ///   Read value: 147.8                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.007;   </para><para>
        ///   Read value: 150.3                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.007;   </para><para>
        ///   Read value: 148.2                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;   </para><para>
        ///   Read value: 147.2                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;   </para><para>
        ///   Read value: 148.7                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.004;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.004; Read Time: 00.004; Query Time: 00.008;   </para><para>
        ///   Read value: 149.6                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.006;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.004; Read Time: 00.005; Query Time: 00.009;   </para><para>
        ///   Read value: 150.3                                            </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                  </para><para>
        ///   Write Time: 00.005;                                          </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'           </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.008;   </para><para>
        ///                                                                </para><para>
        /// Read 10 values in 605ms at 60ms per reading                    </para><para>
        /// Auto Range:
        /// Read 10 values in 634ms at 63ms per reading                    </para><para>
        /// 
        /// </para></remarks>
        [TestMethod()]
        public void ShouldReadDecibelsStatusAware()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeClearState( device.HarmonicsMeasureSubsystem );

                HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

                //  this is not necessary when the measure mode is set last.
                //  this.AssertMeasureMode( device.HarmonicsMeasureSubsystem, HarmonicsMeasureMode.Decibel );
                // check that we are not in auto mode.
#if false
                // 7873: auto range is allowed now
                Assert.AreNotEqual( ( int ) Clt10.VoltmeterRangeMode.Auto, device.HarmonicsMeasureSubsystem.QueryVoltmeterRangeMode().ParsedValue,
                    $"{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} should not be in auto mode" );
#endif

                // report generator timer:
                double generatorTimer = device.HarmonicsMeasureSubsystem.QueryGeneratorTimer().ParsedValue;
                Console.Out.WriteLine( $"Generator timer is set at {generatorTimer}ms" );

                // repeat taking data multiple times.
                int count = 10;
                Stopwatch sw = Stopwatch.StartNew();
                for ( int i = 0; i < count; i++ )
                {
                    this.AssertShouldReadDataValue( device.HarmonicsMeasureSubsystem, true );
                }
                sw.Stop();
                Console.Out.WriteLine( $"Read {count} values in {sw.ElapsedMilliseconds}ms at {(sw.ElapsedMilliseconds / count):0}ms per reading" );


            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) should trigger fetch decibels status aware. </summary>
        /// <remarks>   David, 2021-04-14. <para>
        ///  Test Name:	ShouldTriggerFetchDecibelsStatsuAware </para><para>
        ///Test Name:	ShouldTriggerFetchDecibelsStatsuAware </para><para>
        /// Test Outcome:	Passed   </para><para>
        /// C:\My\Libraries\VS\IO\VI\VI\Clt10\Tests\bin\_log\testhost.net472.x86-2021-04-17.log </para><para>
        /// Generator timer is set at 20ms                                        </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.004; Write Time: 00.005; Query Time: 00.009;  </para><para>
        ///   Command ''; Sent ''; Parsed 148.3:'VM=148.3dB\r\n':'VM=148.3dB'     </para><para>
        ///   Write Time: 00.011; Read Time: 00.025; Read Time: 00.037;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.004;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.003; Read Time: 00.003; Query Time: 00.007;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.003; Write Time: 00.005; Query Time: 00.009;  </para><para>
        ///   Command ''; Sent ''; Parsed 148.2:'VM=148.2dB\r\n':'VM=148.2dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.025; Read Time: 00.040;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.005;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 148.3:'VM=148.3dB\r\n':'VM=148.3dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.023; Read Time: 00.038;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.006;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 144.8:'VM=144.8dB\r\n':'VM=144.8dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.022; Read Time: 00.036;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.005;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.004; Query Time: 00.006;  </para><para>
        ///   Command ''; Sent ''; Parsed 148:'VM=148.0dB\r\n':'VM=148.0dB'       </para><para>
        ///   Write Time: 00.012; Read Time: 00.027; Read Time: 00.039;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.005;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 148.2:'VM=148.2dB\r\n':'VM=148.2dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.022; Read Time: 00.037;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.004;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.004; Read Time: 00.003; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 149.6:'VM=149.6dB\r\n':'VM=149.6dB'     </para><para>
        ///   Write Time: 00.012; Read Time: 00.026; Read Time: 00.038;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.005;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.005; Read Time: 00.003; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 148.6:'VM=148.6dB\r\n':'VM=148.6dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.023; Read Time: 00.038;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.004;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 148.9:'VM=148.9dB\r\n':'VM=148.9dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.026; Read Time: 00.040;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.005;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.005; Read Time: 00.002; Query Time: 00.008;          </para><para>
        /// Triggering one shot and reading value                                 </para><para>
        ///   Message to send MS,2; Sent 'MS,2'                                   </para><para>
        ///   Write Ready Delay: 00.002; Write Time: 00.005; Query Time: 00.008;  </para><para>
        ///   Command ''; Sent ''; Parsed 149.6:'VM=149.6dB\r\n':'VM=149.6dB'     </para><para>
        ///   Write Time: 00.014; Read Time: 00.022; Read Time: 00.037;           </para><para>
        /// Applying MeasurementStartMode = MeasurementStop                       </para><para>
        ///   Sent Value 0; Command 'MS,{0}'; Sent 'MS,0'                         </para><para>
        ///   Write Time: 00.003;                                                 </para><para>
        ///   Command 'MS?'; Sent 'MS?'; Parsed 0:'MS=0\r\n':'0'                  </para><para>
        ///   Write Time: 00.004; Read Time: 00.002; Query Time: 00.007;          </para><para>
        ///                                                                       </para><para>
        /// Triggered and Fetched 10 values in 611ms at 61ms per reading          </para><para>
        /// </para></remarks>
        [TestMethod()]
        public void ShouldTriggerFetchDecibelsStatusAware()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeClearState( device.HarmonicsMeasureSubsystem );

                HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

                //  this is not necessary when the measure mode is set last.
                //  this.AssertMeasureMode( device.HarmonicsMeasureSubsystem, HarmonicsMeasureMode.Decibel );
                // check that we are not in auto mode.
#if false
                // 7873: auto range is allowed now
                Assert.AreNotEqual( ( int ) Clt10.VoltmeterRangeMode.Auto, device.HarmonicsMeasureSubsystem.QueryVoltmeterRangeMode().ParsedValue,
                    $"{nameof( HarmonicsMeasureSubsystem.VoltmeterRangeMode )} should not be in auto mode" );
#endif

                // report generator timer:
                double generatorTimer = device.HarmonicsMeasureSubsystem.QueryGeneratorTimer().ParsedValue;
                Console.Out.WriteLine( $"Generator timer is set at {generatorTimer}ms" );

                // repeat taking data multiple times.
                int count = 10;
                Stopwatch sw = Stopwatch.StartNew();
                for ( int i = 0; i < count; i++ )
                {
                    this.AssetShouldTriggerFetchDataValue( device.HarmonicsMeasureSubsystem, true );
                }
                sw.Stop();
                Console.Out.WriteLine( $"Triggered and Fetched {count} values in {sw.ElapsedMilliseconds}ms at {(sw.ElapsedMilliseconds / count):0}ms per reading" );

            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " SERVICE REQUEST HANDLING "
        /// <summary>   (Unit Test Method) service request should automatic fetch. </summary>
        /// <remarks>   David, 2021-04-06. <para>
        /// Test Name:	ServiceRequestShouldAutoFetch </para><para>
        /// Test Outcome:	Passed 2021-04-17 </para><para>
        /// Monitor status byte 3: 0xD9 </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=147.7dB' of 147.7dB and amount 147.7 dB in 88ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=147.2dB' of 147.2dB and amount 147.2 dB in 99ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=149.4dB' of 149.4dB and amount 149.4 dB in 51ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=146.7dB' of 146.7dB and amount 146.7 dB in 50ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=149.1dB' of 149.1dB and amount 149.1 dB in 55ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=148.0dB' of 148dB and amount 148 dB in 50ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=148.6dB' of 148.6dB and amount 148.6 dB in 53ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=148.1dB' of 148.1dB and amount 148.1 dB in 53ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=147.5dB' of 147.5dB and amount 147.5 dB in 50ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=148.0dB' of 148dB and amount 148 dB in 52ms </para><para>
        /// 
        /// Auto Range </para><para>
        /// 
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=156.9dB' of 156.9dB and amount 156.9 dB in 94ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=159.8dB' of 159.8dB and amount 159.8 dB in 95ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=156.9dB' of 156.9dB and amount 156.9 dB in 104ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=157.7dB' of 157.7dB and amount 157.7 dB in 54ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=156.3dB' of 156.3dB and amount 156.3 dB in 103ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=154.0dB' of 154dB and amount 154 dB in 54ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=152.9dB' of 152.9dB and amount 152.9 dB in 50ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=152.2dB' of 152.2dB and amount 152.2 dB in 52ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=155.2dB' of 155.2dB and amount 155.2 dB in 55ms </para><para>
        /// Service request SRQ 0xD5 </para><para>
        /// Fetched reading 'VM=157.7dB' of 157.7dB and amount 157.7 dB in 48ms </para><para>
        ///
        /// 
        /// </para> </remarks>
        [TestMethod()]
        public void ServiceRequestShouldAutoFetch()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                int monitorNumber = 0;
                monitorNumber += 1;
                int monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration );

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

                // service request handler encountered a query status when and stopped working thereafter.
                // Turning on status aware handling solved this issue.
                // this works: this.AssetShouldTriggerFetchDataValue( device.HarmonicsMeasureSubsystem, true );
                // this works: this.AssertShouldReadDataValue( device.HarmonicsMeasureSubsystem, true );
                // this works: this.AssertShouldReadDataValue( device.HarmonicsMeasureSubsystem );

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                device.StatusSubsystem.DiscardUnreadMessages( TimeSpan.FromMilliseconds( 100 ) );
                if ( !string.IsNullOrEmpty( device.StatusSubsystem.UnreadMessages ) )
                    Console.Out.WriteLine( $"Unread messages:  {Environment.NewLine}{device.StatusSubsystem.UnreadMessages}" );

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                device.StatusSubsystem.DiscardServiceRequests();

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                // Trying to clear the service request status at this point breaks the service request handler
                // Console.Out.WriteLine( $"SRQ = {( int ) device.Session.ReadStatusByte()}" );

                // enable VISA service request handler
                device.Session.EnableServiceRequestEventHandler();
                Assert.IsTrue( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled )} should be enabled" );
                Assert.IsFalse( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead )} should be off" );

                // enable the top level service request auto read sentinel.
                device.ServiceRequestAutoRead = true;
                Assert.IsTrue( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead )} should be on" );
                Assert.IsFalse( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned )} should not be assigned" );

                // add the device-level service request handler 
                device.AddServiceRequestEventHandler();
                Assert.IsTrue( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned )} should be assigned" );

                // turn on the harmonics measure subsystem auto fetch token
                device.HarmonicsMeasureSubsystem.ServiceRequestFetchReadingEnabled = true;

                Stopwatch sw = new();
                int count = 10;
                for ( int i = 0; i < count; i++ )
                {
                    // clear the last reading for proper monitoring
                    device.ServiceRequestStatus = -1;
                    device.HarmonicsMeasureSubsystem.PrimaryReading.Reset();

                    // clearing here in case the SRQ is not handled.
                    device.ServiceRequestReading = string.Empty;
                    device.ServiceRequestFailureMessage = string.Empty;

                    sw.Restart();

                    // trigger a single shot
                    _ = device.HarmonicsMeasureSubsystem.AssertTrigger();

                    // wait for the reply or timeout.
                    device.StartAwaitingServiceRequestReadingTask( TimeSpan.FromMilliseconds( 10000d ) ).Wait();
                    Console.Out.WriteLine( $"Service request SRQ 0x{device.ServiceRequestStatus:X2}" );
                    Assert.IsFalse( string.IsNullOrWhiteSpace( device.ServiceRequestReading ), $"Service request reading '{device.ServiceRequestReading}' should not be empty" );

                    // now report the reading.
                    Assert.IsTrue( device.HarmonicsMeasureSubsystem.PrimaryReading.HasValue, $"{nameof( HarmonicsMeasureSubsystem.PrimaryReading )} should have a value" );
                    Console.Out.WriteLine( $"Fetched reading '{device.HarmonicsMeasureSubsystem.PrimaryReading.ReadingCaption}' of {device.HarmonicsMeasureSubsystem.PrimaryReading.Value}{device.HarmonicsMeasureSubsystem.PrimaryReading.Amount.Unit} and amount {device.HarmonicsMeasureSubsystem.PrimaryReading.Amount} in {sw.ElapsedMilliseconds}ms" );
                }

                // disable service request handling
                device.Session.DisableServiceRequestEventHandler();

                // must refresh the service request here to update the status register flags.
                (bool hasError, string errorDetails, int statusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusRegister() );
                Assert.IsFalse( hasError, $"Status error 0x{statusByte:X2}:'{errorDetails}'" );

                Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled )} should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) service request should stream. </summary>
        /// <remarks>   David, 2021-04-06. <para>
        /// Manual triggers: </para><para>
        /// 01  147.4 dB</para><para>
        /// 02  147.8 dB</para><para>
        /// 03  146.2 dB</para><para>
        /// 04  147.5 dB</para><para>
        /// 05  147.7 dB</para><para>
        /// 06  147.7 dB</para><para>
        /// 07  149 dB</para><para>
        /// 08  148.5 dB</para><para>
        /// 09  148.5 dB</para><para>
        /// 10  146 dB</para><para>
        /// Streamed 10 values in 9202ms at 920ms/reading</para><para>
        /// 
        /// Timed triggers: at 100ms </para><para>
        /// Test Name:	ServiceRequestShouldStream  </para><para>
        /// Test Outcome:	Passed  </para><para>
        /// Result StandardOutput:  </para><para>
        /// C:\My\Libraries\VS\IO\VI\VI\Clt10\Tests\bin\_log\testhost.net472.x86-2021-04-15.log  </para><para>
        /// Monitor status byte 3: 0xD9  </para><para>
        /// Monitoring new readings count property change </para><para>
        /// triggering every 100ms </para><para>
        /// 01  149.9 dB  </para><para>
        /// 02  148.6 dB  </para><para>
        /// 03  149.5 dB  </para><para>
        /// 04  147.5 dB  </para><para>
        /// 05  149.2 dB  </para><para>
        /// 06  149.3 dB  </para><para>
        /// 07  148.9 dB  </para><para>
        /// 08  146.1 dB  </para><para>
        /// 09  146.9 dB  </para><para>
        /// 10  148.8 dB  </para><para>
        /// Streamed 10 values in 1034ms at 103ms/reading  </para><para>
        /// Auto range:  </para><para>
        /// Streamed 10 values in 1037ms at 103ms/reading</para><para>
        /// </para> </remarks>
        [TestMethod()]
        public void ServiceRequestShouldStream()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                int monitorNumber = 0;
                monitorNumber += 1;
                int monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                HarmonicsMeasureSubsystemTests.AssetConfigurationShouldBeSet();

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                // check status is required for turning on service requests making sure the status byte is in sync.
                device.HarmonicsMeasureSubsystem.Configure( HarmonicsSubsystemSettings.Configuration, true );

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                HarmonicsMeasureSubsystemTests.AssertSubsystemShouldBeConfigured( device.HarmonicsMeasureSubsystem );

                device.StatusSubsystem.DiscardUnreadMessages( TimeSpan.FromMilliseconds( 100 ) );
                if ( !string.IsNullOrEmpty( device.StatusSubsystem.UnreadMessages ) )
                    Console.Out.WriteLine( $"Unread messages:  {Environment.NewLine}{device.StatusSubsystem.UnreadMessages}" );

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );

                device.StatusSubsystem.DiscardServiceRequests();

                monitorNumber += 1;
                monitorStatusByte = ( int ) device.Session.ReadStatusByte();
                if ( monitorStatusByte != ( int ) HarmonicsStatusRegisterEvent.IdleState )
                    Console.Out.WriteLine( $"Monitor status byte {monitorNumber}: 0x{monitorStatusByte:X2}" );


                // enable VISA service request handler
                device.Session.EnableServiceRequestEventHandler();
                Assert.IsTrue( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled )} should be enabled" );
                Assert.IsFalse( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead )} should be off" );

                // enable the top level service request auto read sentinel.
                device.ServiceRequestAutoRead = true;
                Assert.IsTrue( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead )} should be on" );
                Assert.IsFalse( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned )} should not be assigned" );

                // add the device-level service request handler 
                device.AddServiceRequestEventHandler();
                Assert.IsTrue( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned )} should be assigned" );

                // turn on the harmonics measure subsystem auto fetch token
                device.HarmonicsMeasureSubsystem.ClearBufferReadingsQueue();
                device.HarmonicsMeasureSubsystem.ServiceRequestStreamReadingsEnabled = true;

                bool monitorPropertyChange = true;
                if ( monitorPropertyChange )
                    device.HarmonicsMeasureSubsystem.PropertyChanged += this.ReportNewReadings;
                Console.Out.WriteLine( monitorPropertyChange ? "Monitoring new readings count property change" : "Polling new readings count" );

                bool usingTimedTriggers = true;
                bool usingAssertTrigger = false;
                TimeSpan streamPeriod = TimeSpan.FromMilliseconds( 100 );
                Console.Out.WriteLine( usingTimedTriggers ? $"triggering every {streamPeriod.TotalMilliseconds}ms" : usingAssertTrigger ? "Using in loop triggers" : "Using manual triggers" );

                int streamCount = 10;
                System.Threading.CancellationTokenSource cancellationTokenSource = new();
                System.Threading.CancellationToken cancellationToken = cancellationTokenSource.Token;
                if ( usingTimedTriggers )
                {
                    System.Threading.Tasks.Task triggerTask = this.StartStreamingTask( device.HarmonicsMeasureSubsystem, streamCount,
                                                                                       streamPeriod, cancellationToken );
                }
                else if ( usingAssertTrigger )
                {
                    _ = device.HarmonicsMeasureSubsystem.AssertTrigger();
                }

                TimeSpan streamTimeout = TimeSpan.FromMilliseconds( 5 * streamCount * streamPeriod.TotalMilliseconds );
                Stopwatch sw = Stopwatch.StartNew();
                int lastCount = 0;
                while ( sw.Elapsed < streamTimeout && device.HarmonicsMeasureSubsystem.BufferReadingsCount < streamCount )
                {
                    if ( usingAssertTrigger && lastCount != device.HarmonicsMeasureSubsystem.BufferReadingsCount )
                    {
                        _ = device.HarmonicsMeasureSubsystem.AssertTrigger();
                        lastCount = device.HarmonicsMeasureSubsystem.BufferReadingsCount;
                    }

                    while ( !monitorPropertyChange && device.HarmonicsMeasureSubsystem.NewReadingsCount > 0 )
                    {
                        _ = device.HarmonicsMeasureSubsystem.NewBufferReadingsQueue.TryDequeue( out MeasuredAmount primaryReading );
                        Console.Out.WriteLine( $"{device.HarmonicsMeasureSubsystem.BufferReadingsCount:00} {primaryReading.ReadingCaption} {primaryReading.Amount}" );
                    }
                }

                cancellationTokenSource.Cancel();
                Assert.IsTrue( device.HarmonicsMeasureSubsystem.BufferReadingsCount > 0, $"Should stream at least 1 reading" );
                Console.Out.WriteLine( $"Streamed {device.HarmonicsMeasureSubsystem.BufferReadingsCount:00} values in {sw.ElapsedMilliseconds}ms at {(sw.ElapsedMilliseconds / device.HarmonicsMeasureSubsystem.BufferReadingsCount):0}ms/reading" );
                Assert.AreEqual( streamCount, device.HarmonicsMeasureSubsystem.BufferReadingsCount, $"Should stream {streamCount} readings" );

                // disable service request handling
                device.Session.DisableServiceRequestEventHandler();

                // must refresh the service request here to update the status register flags.
                _ = device.Session.ReadStatusRegister();
                Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled )} should be disabled" );

            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        private void ReportNewReadings( object sender, PropertyChangedEventArgs e )
        {
            HarmonicsMeasureSubsystemBase subsystem = ( HarmonicsMeasureSubsystemBase ) sender;
            if ( sender is object && String.Equals( e.PropertyName, nameof( HarmonicsMeasureSubsystemBase.NewReadingsCount ) ) )
            {
                while ( subsystem.NewReadingsCount > 0 )
                {
                    _ = subsystem.NewBufferReadingsQueue.TryDequeue( out MeasuredAmount primaryReading );
                    Console.Out.WriteLine( $"{subsystem.BufferReadingsCount:00} {primaryReading.ReadingCaption} {primaryReading.Amount}" );
                }
            }
        }

        /// <summary>   Starts streaming task. </summary>
        /// <remarks>   David, 2021-04-06. </remarks>
        /// <param name="cancellationToken">    A token that allows processing to be canceled. </param>
        /// <param name="subsystem">            The subsystem. </param>
        /// <param name="count">                Number of. </param>
        /// <param name="period">               The period. </param>
        /// <returns>   A System.Threading.Tasks.Task. </returns>
        private async System.Threading.Tasks.Task StartStreamingTask( HarmonicsMeasureSubsystem subsystem, int count, TimeSpan period,
                System.Threading.CancellationToken cancellationToken )
        {
            Stopwatch sw = new();
            await System.Threading.Tasks.Task.Factory.StartNew( () => {
                while ( count > 0 && !cancellationToken.IsCancellationRequested )
                {
                    _ = subsystem.AssertTrigger();
                    sw.Restart();
                    while ( sw.Elapsed < period )
                    {
                        System.Threading.Thread.SpinWait( 1 );
                        ApplianceBase.DoEvents();
                    }
                }
            } ).ConfigureAwait( false );
        }

        /// <summary>   (Unit Test Method) service request should be handled. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        [TestMethod()]
        public void ServiceRequestShouldBeHandled()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                // device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                string queryCommand = "GL?";
                string expectedReply = device.Session.Query( TimeSpan.FromMilliseconds( 10 ), queryCommand ).InsertCommonEscapeSequences();
                DeviceTests.DeviceManager.AssertServiceRequestShouldBeHandled( device, queryCommand, 0xD9, expectedReply );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>   (Unit Test Method) service request should automatic parse. </summary>
        /// <remarks>   David, 2021-04-15. <para>
        ///Test Name:	ServiceRequestShouldAutoParse </para><para>
        /// Test Outcome:	Passed </para><para>
        /// Result StandardOutput: </para><para>
        /// C:\My\Libraries\VS\IO\VI\VI\Clt10\Tests\bin\_log\testhost.net472.x86-2021-04-15.log  </para><para>
        /// setting AccessRightsMode = 2:Denial  </para><para>
        /// setting AccessRightsMode = 2:Denial returned 'AR=2\r\n' in 74ms  </para><para>
        /// setting AccessRightsMode = 0:Normal  </para><para>
        /// setting AccessRightsMode = 0:Normal returned 'AR=0\r\n' in 46ms  </para><para>
        /// setting MeasureMode = 1:Decibel  </para><para>
        /// setting MeasureMode = 1:Decibel returned 'VD=dB\r\n' in 50ms   </para><para>
        /// setting MeasureMode = 0:Voltage  </para><para>
        /// setting MeasureMode = 0:Voltage returned 'VD=Volt\r\n' in 51ms   </para><para>
        /// setting BandwidthLimitingEnabled = True  </para><para>
        /// setting BandwidthLimitingEnabled = True returned 'BW=ON\r\n' in 54ms   </para><para>
        /// setting BandwidthLimitingEnabled = False   </para><para>
        /// setting BandwidthLimitingEnabled = False returned 'BW=OFF\r\n' in 46ms   </para><para>
        /// setting ImpedanceRange = 30000   </para><para>
        /// setting ImpedanceRange = 30000 returned 'ZX=3\r\n' in 282ms  </para><para>
        /// setting ImpedanceRange = 3000  </para><para>
        /// setting ImpedanceRange = 3000 returned 'ZX=2\r\n' in 279ms   </para><para>
        /// setting ImpedanceRange = 299.999   </para><para>
        /// setting ImpedanceRange = 299.999 returned 'ZX=1\r\n' in 53ms   </para><para>
        /// setting GeneratorTimer = 50  </para><para>
        /// setting GeneratorTimer = 50 returned 'GT=50ms\r\n' in 52ms   </para><para>
        /// setting GeneratorTimer = 20  </para><para>
        /// setting GeneratorTimer = 20 returned 'GT=20ms\r\n' in 53ms   </para><para>
        /// setting VoltmeterOutputEnabled = False  </para><para>
        /// setting VoltmeterOutputEnabled = False returned 'VM=OFF\r\n' in 52ms  </para><para>
        /// setting VoltmeterOutputEnabled = True  </para><para>
        /// setting VoltmeterOutputEnabled = True returned 'VM=ON\r\n' in 52ms  </para><para>
        /// setting VoltmeterRange = 1.000E-004  </para><para>
        /// setting VoltmeterRange = 1.000E-004 returned 'VR=100uV\r\n' in 262ms   </para><para>
        /// setting VoltmeterRange = 1.000E-005  </para><para>
        /// setting VoltmeterRange = 1.000E-005 returned 'VR=10uV\r\n' in 261ms  </para><para>
        /// setting VoltmeterHighLimit = 1.000E-003  </para><para>
        /// setting VoltmeterHighLimit = 1.000E-003 returned 'LH=1.000mV\r\n' in 52ms  </para><para>
        /// setting VoltmeterHighLimit = 1.000E-004  </para><para>
        /// setting VoltmeterHighLimit = 1.000E-004 returned 'LH=100.0uV\r\n' in 51ms  </para><para>
        /// setting VoltmeterLowLimit = 1.000E-005   </para><para>
        /// setting VoltmeterLowLimit = 1.000E-005 returned 'LL=10.00uV\r\n' in 53ms   </para><para>
        /// setting VoltmeterLowLimit = 1.000E-006   </para><para>
        /// setting VoltmeterLowLimit = 1.000E-006 returned 'LL=1.000uV\r\n' in 51ms   </para><para>
        /// setting GeneratorOutputLevel = 1.1   </para><para>
        /// setting GeneratorOutputLevel = 1.1 returned 'GL=1.100V\r\n' in 51ms  </para><para>
        /// setting GeneratorOutputLevel = 1.2   </para><para>
        /// setting GeneratorOutputLevel = 1.2 returned 'GL=1.200V\r\n' in 52ms  </para><para>
        ///
        ///
        /// </para> </remarks>
        [TestMethod()]
        public void ServiceRequestShouldAutoParse()
        {
            using var device = Clt10.Clt10Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );

                // enable VISA service request handler
                device.Session.EnableServiceRequestEventHandler();
                Assert.IsTrue( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled )} should be enabled" );
                Assert.IsFalse( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead )} should be off" );

                // enable the top level service request auto read sentinel.
                device.ServiceRequestAutoRead = true;
                Assert.IsTrue( device.ServiceRequestAutoRead, $"{nameof( VisaSessionBase.ServiceRequestAutoRead )} should be on" );
                Assert.IsFalse( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned )} should not be assigned" );

                // add the device-level service request handler 
                device.AddServiceRequestEventHandler();
                Assert.IsTrue( device.ServiceRequestHandlerAssigned, $"{nameof( VisaSessionBase.ServiceRequestHandlerAssigned )} should be assigned" );

                // turn on the harmonics measure subsystem auto fetch token
                device.HarmonicsMeasureSubsystem.ServiceRequestSubsystemParseMessageEnabled = true;

                TimeSpan timeout = TimeSpan.FromMilliseconds( 100 );
                this.AssertApplyingAccessRightsMode( device, AccessRightsMode.Denial, timeout );
                this.AssertApplyingAccessRightsMode( device, AccessRightsMode.Normal, timeout );
                this.AssertApplyingHarmonicsMeasureMode( device, HarmonicsMeasureMode.Decibel, timeout );
                this.AssertApplyingHarmonicsMeasureMode( device, HarmonicsMeasureMode.Voltage, timeout );
                this.AssertApplyingBandwidthLimitingEnabled( device, true, timeout );
                this.AssertApplyingBandwidthLimitingEnabled( device, false, timeout );
                this.AssertApplyingImpedanceRange( device, HarmonicsMeasureSubsystem.ToImpedanceRange( ImpedanceRangeMode.Betweeen3000And30000 ), timeout );
                this.AssertApplyingImpedanceRange( device, HarmonicsMeasureSubsystem.ToImpedanceRange( ImpedanceRangeMode.Between300And3000 ), timeout );
                this.AssertApplyingImpedanceRange( device, HarmonicsMeasureSubsystem.ToImpedanceRange( ImpedanceRangeMode.LessThan300 ), timeout );
                this.AssertApplyingGeneratorTimer( device, 50, timeout );
                this.AssertApplyingGeneratorTimer( device, 20, timeout );
                this.AssertApplyingVoltmeterOutputEnabled( device, false, timeout );
                this.AssertApplyingVoltmeterOutputEnabled( device, true, timeout );
                this.AssertApplyingVoltmeterRange( device, HarmonicsMeasureSubsystem.ToVoltmeterRange( VoltmeterRangeMode.HunderdMicroVolt ), timeout );
                this.AssertApplyingVoltmeterRange( device, HarmonicsMeasureSubsystem.ToVoltmeterRange( VoltmeterRangeMode.TenMicroVolt ), timeout );
                this.AssertApplyingVoltmeterHighLimit( device, 1e-3, timeout );
                this.AssertApplyingVoltmeterHighLimit( device, 1e-4, timeout );
                this.AssertApplyingVoltmeterLowLimit( device, 1e-5, timeout );
                this.AssertApplyingVoltmeterLowLimit( device, 1e-6, timeout );
                this.AssertApplyingGeneratorOutputLevel( device, 1.1, timeout );
                this.AssertApplyingGeneratorOutputLevel( device, 1.2, timeout );

                // disable service request handling
                device.Session.DisableServiceRequestEventHandler();

                // must refresh the service request here to update the status register flags.
                (bool hasError, string errorDetails, int statusByte) = device.StatusSubsystem.IsStatusError( ( int ) device.Session.ReadStatusRegister() );
                Assert.IsFalse( hasError, $"Status error 0x{statusByte:X2}:'{errorDetails}'" );

                Assert.IsFalse( device.Session.ServiceRequestEventEnabled, $"{nameof( Pith.SessionBase.ServiceRequestEventEnabled )} should be disabled" );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }


#endregion


    }
}
