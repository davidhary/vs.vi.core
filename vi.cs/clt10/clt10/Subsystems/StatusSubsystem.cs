using System;

using isr.Core.EnumExtensions;
using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.Clt10
{

    /// <summary> Defines a Status Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ) )
        {
            this.VersionInfo = new VersionInfo();
            this.VersionInfoBase = this.VersionInfo;
            InitializeSession( session );
            base.CollectGarbageWaitCompleteCommand = string.Empty;
            base.ClearErrorQueueCommand = string.Empty;
            base.DequeueErrorQueryCommand = string.Empty;
            base.DeviceErrorQueryCommand = string.Empty;
            base.ErrorQueueCountQueryCommand = string.Empty;
            base.LanguageCommandFormat = string.Empty;
            base.LanguageQueryCommand = string.Empty;
            base.LastSystemErrorQueryCommand = string.Empty;
            base.LineFrequencyQueryCommand = string.Empty;
            base.MeasurementEventConditionQueryCommand = string.Empty;
            base.MeasurementEventEnableCommandFormat = string.Empty;
            base.MeasurementEventEnableQueryCommand = string.Empty;
            base.MeasurementEventNegativeTransitionCommandFormat = string.Empty;
            base.MeasurementEventNegativeTransitionQueryCommand = string.Empty;
            base.MeasurementEventPositiveTransitionCommandFormat = string.Empty;
            base.MeasurementEventPositiveTransitionQueryCommand = string.Empty;
            base.MeasurementStatusQueryCommand = string.Empty;
            base.NextDeviceErrorQueryCommand = string.Empty;
            base.OperationEventConditionQueryCommand = string.Empty;
            base.OperationEventEnableCommandFormat = string.Empty;
            base.OperationEventEnableQueryCommand = string.Empty;
            base.OperationEventMapCommandFormat = string.Empty;
            base.OperationEventMapQueryCommand = string.Empty;
            base.OperationEventStatusQueryCommand = string.Empty;
            base.PresetCommand = string.Empty;
            base.QuestionableEventMapCommandFormat = string.Empty;
            base.QuestionableEventMapQueryCommand = string.Empty;
            base.QuestionableStatusQueryCommand = string.Empty;
            base.SerialNumberQueryCommand = string.Empty;
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Define measurement events bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineMeasurementEventsBitmasks()
        {
            //  Serial poll data not amenable to bit masking
        }

        /// <summary> Defines the operation event bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineOperationEventsBitmasks()
        {
            //  Serial poll data not amenable to bit masking
        }

        /// <summary> Define Questionable event bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineQuestionableEventsBitmasks()
        {
            //  Serial poll data not amenable to bit masking
        }


        /// <summary>   Discards unread messages. </summary>
        /// <remarks>   David, 2021-04-17. </remarks>
        protected override void DiscardUnreadMessages()
        {
            // this call takes longer the first time it is issued no matter the setting of the timeout.
            string msg = this.Session.ReadLine();
            while ( !string.IsNullOrWhiteSpace( msg ) )
            {
                this.LastUnreadMessage = msg;
                msg = this.Session.ReadLine();
            }
        }

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks>   Clears the queues and sets all registers to zero. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public override void DefineClearExecutionState()
        {
            string activity = $"{this.Session.ResourceNameCaption} clearing instrument messages queue";
            try
            {
                _ = this.PublishVerbose( $"{activity};. " );
                // this seems to be done on initialing knwon state.
                // this.DiscardUnreadMessages( TimeSpan.FromMilliseconds( 10 ) );
            }
            catch ( Exception ex )
            {
                ex.Data.Add( $"data{ex.Data.Count}.resource", this.Session.ResourceNameCaption );
                _ = this.PublishException( activity, ex );
            }
        }


        /// <summary>   Discard service requests. </summary>
        /// <remarks>   David, 2021-04-17. </remarks>
        public override void DiscardServiceRequests()
        {
            this.Session.DiscardServiceRequests();
            _ = this.Session.WriteLine( "SS,3" );
            _ = this.Session.WriteLine( "SS,0" );
        }

        #endregion

        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        /// session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.TerminationSequence = Environment.NewLine;
            session.ClearExecutionStateCommand = "RS,10";
            session.ResetKnownStateCommand = string.Empty;
            // Service request are not defined by bits
            session.MeasurementEventBit = 0; // Pith.ServiceRequests.MeasurementEvent;
            session.SystemEventBit = 0; // Pith.ServiceRequests.SystemEvent;
            session.ErrorAvailableBit = 0; // Pith.ServiceRequests.ErrorAvailable;
            session.QuestionableEventBit = 0; // Pith.ServiceRequests.QuestionableEvent;
            session.MessageAvailableBit = 0; // Pith.ServiceRequests.MessageAvailable;
            session.StandardEventBit = 0; // Pith.ServiceRequests.StandardEvent;
            session.RequestingServiceBit = 0; // Pith.ServiceRequests.RequestingService;
            session.OperationEventBit = 0; // Pith.ServiceRequests.OperationEvent;
            session.OperationCompletedQueryCommand = string.Empty;
            session.StandardEventStatusQueryCommand = string.Empty;
            session.StandardEventEnableQueryCommand = string.Empty;
            session.StandardServiceEnableCommandFormat = string.Empty;
            session.StandardServiceEnableCompleteCommandFormat = string.Empty;
            // we are leaving the service request in the default model
            session.ServiceRequestEnableCommandFormat = string.Empty; // "SS,0";
            session.ServiceRequestEnableQueryCommand = string.Empty; // "SS?";
            session.WaitCommand = String.Empty;
        }

        #endregion

        #region " PRESET "

        /// <summary> Gets or sets the preset command. </summary>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = string.Empty;

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = String.Empty;

        /// <summary> Gets or sets the error queue query command. </summary>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = String.Empty;

        #endregion

        #region " MEASUREMENT REGISTER EVENTS "

        /// <summary> Gets or sets the measurement status query command. </summary>
        /// <value> The measurement status query command. </value>
        protected override string MeasurementStatusQueryCommand { get; set; } = String.Empty;

        /// <summary> Gets or sets the measurement event condition query command. </summary>
        /// <value> The measurement event condition query command. </value>
        protected override string MeasurementEventConditionQueryCommand { get; set; } = String.Empty;

        #endregion

        #region " OPERATION REGISTER EVENTS "

        /// <summary> Gets or sets the operation event enable Query command. </summary>
        /// <value> The operation event enable Query command. </value>
        protected override string OperationEventEnableQueryCommand { get; set; } = String.Empty;

        /// <summary> Gets or sets the operation event enable command format. </summary>
        /// <value> The operation event enable command format. </value>
        protected override string OperationEventEnableCommandFormat { get; set; } = String.Empty;

        /// <summary> Gets or sets the operation event status query command. </summary>
        /// <value> The operation event status query command. </value>
        protected override string OperationEventStatusQueryCommand { get; set; } = String.Empty;

        #endregion

        #region " QUESTIONABLE REGISTER "

        /// <summary> Gets or sets the questionable status query command. </summary>
        /// <value> The questionable status query command. </value>
        protected override string QuestionableStatusQueryCommand { get; set; } = String.Empty;

        #endregion

        #region " SERVICE REQUEST STATUS "

        /// <summary>   Query if this instrument is busy. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <returns>   True if busy, false if not. </returns>
        public bool IsStatusBusy()
        {
            return this.IsStatusBusy( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Query if this status byte indicates that the device is busy. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if busy, false if not. </returns>
        public override bool IsStatusBusy( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.Busy == statusByte;
        }

        /// <summary>   Query if this object is idle. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <returns>   True if idle, false if not. </returns>
        public bool IsIdle()
        {
            return ( int ) HarmonicsStatusRegisterEvent.IdleState == ( int ) this.Session.ReadStatusByte();
        }

        /// <summary>   Query if this object is data ready. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <returns>   True if data ready, false if not. </returns>
        public bool IsStatusDataReady()
        {
            return this.IsStatusDataReady( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Query if this object is data ready. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if data ready, false if not. </returns>
        public override bool IsStatusDataReady( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.DataReady == statusByte;
        }

        /// <summary>   Query if this object is query result ready. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <returns>   True if query result ready, false if not. </returns>
        public bool IsStatusQueryResultReady()
        {
            return this.IsStatusQueryResultReady( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Query if this object is query result ready. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if query result ready, false if not. </returns>
        public override bool IsStatusQueryResultReady( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.QueryResultReady == statusByte;
        }

        /// <summary>   Query if this object is query result or data ready. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <returns>   True if query result or data ready, false if not. </returns>
        public bool IsStatusQueryResultOrDataReady()
        {
            return StatusSubsystem.IsStatusQueryResultOrDataReady( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Query if this object is query result or data ready. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if query result or data ready, false if not. </returns>
        public static bool IsStatusQueryResultOrDataReady( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.QueryResultReady == statusByte || ( int ) HarmonicsStatusRegisterEvent.DataReady == statusByte;
        }

        /// <summary>   Gets a value indicating whether [Message available]. </summary>
        /// <value> <c>True</c> if [Message available]; otherwise, <c>False</c>. </value>
        public override bool MessageAvailable => this.IsStatusQueryResultOrDataReady();

        /// <summary>   Queries if the error is available. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <returns>   True if the error is available, false if not. </returns>
        public bool IsStatusError()
        {
            return StatusSubsystem.IsError( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Queries if the error is available. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">  The status byte. </param>
        /// <returns>   True if the error is available, false if not. </returns>
        public static bool IsError( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.SyntaxError == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.IllegalParameterCount == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.ParameterLimitExceededOrUnit == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.UnspecifiedError == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.MissingMeasuringUnit == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.SetupNotdefined == statusByte;
        }

        /// <summary>   Query if 'statusByte' is warning. </summary>
        /// <remarks>   David, 2021-04-17. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if warning, false if not. </returns>
        public static bool IsWarning( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.ParameterTruncated == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.CommandIgnored == statusByte;
        }


        /// <summary>   Queries if the error is available. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   A tuple: ( true if has error, details, status byte). </returns>
        public override (bool HasError, string Details, int StatusByte) IsStatusError( int statusByte )
        {
            return StatusSubsystem.IsError( ( int ) statusByte ) ? (true, (( HarmonicsStatusRegisterEvent ) statusByte).Description(), statusByte) : (false, string.Empty, statusByte);
        }

        /// <summary>   Is status error or warning. </summary>
        /// <remarks>   David, 2021-04-17. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   A Tuple. </returns>
        public override (bool HasError, string Details, int StatusByte) IsStatusErrorOrWarning( int statusByte )
        {
            return (StatusSubsystem.IsError( ( int ) statusByte ) || StatusSubsystem.IsWarning( ( int ) statusByte ))
                ? (true, (( HarmonicsStatusRegisterEvent ) statusByte).Description(), statusByte) : (false, string.Empty, statusByte);
        }

        /// <summary>   Queries status not busy. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <returns>   The status not busy. </returns>
        public override (bool done, int StatusByte) QueryStatusNotBusy()
        {
            var srq = ( HarmonicsStatusRegisterEvent ) Enum.ToObject( typeof( HarmonicsStatusRegisterEvent ), this.Session.ReadStatusByte() );
            return (srq != HarmonicsStatusRegisterEvent.Busy, ( int ) srq);
        }


        /// <summary>   Queries if the warning is available. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <returns>   True if the warning is available, false if not. </returns>
        public bool IsWarningAvailable()
        {
            return StatusSubsystem.IsWarningAvailable( ( int ) this.Session.ReadStatusByte() );
        }

        /// <summary>   Queries if the warning is available. </summary>
        /// <remarks>   David, 2021-04-01. </remarks>
        /// <param name="statusByte">   The status byte. </param>
        /// <returns>   True if the warning is available, false if not. </returns>
        public static bool IsWarningAvailable( int statusByte )
        {
            return ( int ) HarmonicsStatusRegisterEvent.ParameterTruncated == statusByte
                || ( int ) HarmonicsStatusRegisterEvent.CommandIgnored == statusByte;
        }

        /// <summary>   Await status bitmask. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="bitmask">  The bitmask. </param>
        /// <param name="timeout">  The timeout. </param>
        /// <returns>   A Tuple. </returns>
        public virtual (bool TimedOut, int Status, TimeSpan Elapsed) AwaitStatusBitmask( byte bitmask, TimeSpan timeout )
        {
            return this.Session.AwaitStatusBitmask( bitmask, timeout, this.QueryStatusError );
        }

        /// <summary>   Await not busy. </summary>
        /// <remarks>   David, 2021-04-03. </remarks>
        /// <param name="timeout">  The timeout. </param>
        /// <returns>   A Tuple. </returns>
        public virtual (bool TimedOut, int Status, TimeSpan Elapsed) AwaitNotBusy( TimeSpan timeout )
        {
            return this.Session.AwaitStatus( timeout, this.QueryStatusNotBusy );
        }

        #endregion

        #region " IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = "ID?";


        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );

                (string receivedMessage, _) = this.QueryStatusReady( TimeSpan.FromMilliseconds( 50 ), TimeSpan.FromMilliseconds( 5 ), this.IdentityQueryCommand );

                _ = this.ReadIdentity( receivedMessage );
            }
            return this.Identity;
        }

        /// <summary>   Reads an identity. </summary>
        /// <remarks>   David, 2021-04-08. required in case the identify includes multiple lines. </remarks>
        /// <param name="firstLine">    The first line. </param>
        /// <returns>   The identity. </returns>
        public override string ReadIdentity( string firstLine )
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                char delimiter = ',';
                char replacedDelimiter = '=';
                System.Text.StringBuilder builder = new();
                _ = builder.Append( $"{ firstLine.Replace( delimiter, replacedDelimiter ).ReplaceCommonEscapeSequences()}{delimiter}" );

                while ( !this.AwaitQueryResultStatusReady( TimeSpan.FromMilliseconds( 50 ) ).TimedOut )
                {
                    Core.ApplianceBase.DoEvents();
                    _ = builder.Append( $"{this.Session.ReadLine().ReplaceCommonEscapeSequences()}," );
                }
                _ = this.PublishVerbose( $"Setting identity to {builder};. " );
                this.VersionInfo.Parse( builder.ToString() );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }
            return this.Identity;
        }


        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

    }
}
