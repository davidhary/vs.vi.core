namespace isr.VI.Clt10
{

    /// <summary>   Gets or sets the status byte flags of the Harmonics status rregister. </summary>
    /// <remarks>   David, 2020-10-12. </remarks>
    public enum HarmonicsStatusRegisterEvent
    {

        /// <summary>   A binary constant representing the Busy flag. </summary>
        [System.ComponentModel.Description( "Busy" )]
        Busy = 0x0,

        /// <summary>   A binary constant representing the Forced to Local Mode flag. </summary>
        [System.ComponentModel.Description( "Forced to Local Mode" )]
        ForcedToLocalMode = 0x40,

        /// <summary>   A binary constant representing the Forced to Local Released flag. </summary>
        [System.ComponentModel.Description( "Forced to Local Released" )]
        ForcedToLocalReleased = 0x41,

        /// <summary>   A binary constant representing the Idle State flag. </summary>
        [System.ComponentModel.Description( "Idle State" )]
        IdleState = 0x80,

        /// <summary>   A binary constant representing the Syntax error flag. </summary>
        [System.ComponentModel.Description( "Syntax error" )]
        SyntaxError = 0x50,

        /// <summary>   A binary constant representing the Illegal parameter count flag. </summary>
        [System.ComponentModel.Description( "Illegal parameter count" )]
        IllegalParameterCount = 0x51,

        /// <summary>   A binary constant representing the Parameter limit exceeded or unit flag. </summary>
        [System.ComponentModel.Description( "Parameter limit exceeded or unit" )]
        ParameterLimitExceededOrUnit = 0x52,

        /// <summary>   A binary constant representing the unspecified error flag. </summary>
        [System.ComponentModel.Description( "Unspecified Error" )]
        UnspecifiedError = 0x53,

        /// <summary>   A binary constant representing the Missing Measuring Unit flag. </summary>
        [System.ComponentModel.Description( "Missing Measuring Unit" )]
        MissingMeasuringUnit = 0x54,

        /// <summary>   A binary constant representing the Set-up not defined flag. </summary>
        [System.ComponentModel.Description( "Generator level error or set-up not defined" )]
        SetupNotdefined = 0x55,

        /// <summary>   A binary constant representing the parameter truncated flag. </summary>
        [System.ComponentModel.Description( "Parameter truncated" )]
        ParameterTruncated = 0x65,

        /// <summary>   A binary constant representing the command ignored flag. </summary>
        [System.ComponentModel.Description( "Command ignored" )]
        CommandIgnored = 0x6B,

        /// <summary>   A binary constant representing the inspect of setup ready flag. </summary>
        [System.ComponentModel.Description( "Inspect of set-up ready" )]
        InspectOfSetupReady = 0xD4,

        /// <summary>   A binary constant representing the data ready flag. </summary>
        [System.ComponentModel.Description( "Data ready" )]
        DataReady = 0xD5,

        /// <summary>   A binary constant representing the query result ready flag. </summary>
        [System.ComponentModel.Description( "Query result ready" )]
        QueryResultReady = 0xD9

    }
}

