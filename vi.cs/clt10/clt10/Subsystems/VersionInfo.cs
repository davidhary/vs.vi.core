using System;
using System.Collections.Generic;

namespace isr.VI.Clt10
{

    /// <summary> Information about the version of a Danbrdige CLT10 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary>   Parses the instrument identity string. </summary>
        /// <remarks>   The firmware revision can be further interpreted by the child instruments. 
        ///  <remarks>
        /// ID,0\r\n
        /// CLT10 CONTROL UNIT\r\n <para>
        /// SOFTWARE VERSION x.y yymmdd( c) 1992 DANBRIDGE A/S\r\n </para><para>
        /// MU CONNECTED\r\n </para><para>
        /// where</para><para>
        /// x.y is the software version.</para><para>
        /// yymmdd Is the release date. </para><para>
        /// .... is the manufacturer. </para><para>
        /// </para> </remarks>
        /// <param name="value">    Specifies the instrument identity string, which includes at a minimum
        ///                         the following information:
        ///                         <see cref="P:isr.VI.VersionInfoBase.ManufacturerName">manufacturer</see>,
        ///                         <see cref="P:isr.VI.VersionInfoBase.Model">model</see>,
        ///                         <see cref="P:isr.VI.VersionInfoBase.SerialNumber">serial number</see>,
        ///                         e.g., <para><c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10
        ///                         1997
        ///                         09:51:36/A02
        ///                         D/B/E.</c>.</para> </param> </remarks>
        public override void Parse( string value )
        {
            if ( string.IsNullOrEmpty( value ) )
            {
                this.Clear();
            }
            else
            {

                // save the identity.
                this.Identity = value;

                // Parse the id to get the revision number
                var idItems = new Queue<string>( value.Split( ',' ) );

                // ignore the unit number
                _ = idItems.Dequeue();

                this.Model = idItems.Dequeue();
                this.Model = "CLT10";

                this.ManufacturerName = idItems.Dequeue();
                this.ManufacturerName = "DANBRIDGE";

                // Serial Number: 0669977
                // this.SerialNumber = idItems.Dequeue();
                this.SerialNumber = "0";

                this.FirmwareRevision = idItems.Dequeue();
                this.FirmwareRevision = "1.3";

                // parse thee firmware revision
                this.ParseFirmwareRevision( this.FirmwareRevision );
            }
        }


        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks>
        /// CLT10 CONTROL UNIT <para>
        /// SOFTWARE VERSION x.y yymmdd( c) 1992 DANBRIDGE A/S </para><para>
        /// MU CONNECTED </para><para>
        /// where</para><para>
        /// x.y is the software version.</para><para>
        /// yymmdd Is the release date. </para><para>
        /// .... is the manufacturer. </para><para>
        /// </para> </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument <see cref="VersionInfoBase.FirmwareRevisionElements">board
        /// revisions</see>
        /// e.g., <c>yyyyy/zzz</c> for the digital and display boards. </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '/' ) );

                // Rev: yyyyy/ZZZ
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Digital.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Display.ToString(), revSections.Dequeue().Trim() );
            }
        }
    }
}
