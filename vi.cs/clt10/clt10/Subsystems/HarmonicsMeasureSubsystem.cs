using System;
using System.Collections.Generic;
using System.IO;

namespace isr.VI.Clt10
{
    /// <summary> Defines a Harmonics Measure Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public partial class HarmonicsMeasureSubsystem : HarmonicsMeasureSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="HarmonicsMeasureSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public HarmonicsMeasureSubsystem( StatusSubsystem statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            // reading elements are added (reading is initialized) when the format system is reset.
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefineMeasureModeReadWrites( "VD={0}", "{0}" );
            this.GeneratorOutputLevelRange = new Core.Primitives.RangeR( 0, Properties.Settings.Default.GeneratorOutputLevelMaximum );
            this.GeneratorTimerRange = new Core.Primitives.RangeI( Properties.Settings.Default.GeneratorTimerRangeLower, Properties.Settings.Default.GeneratorTimerRangeUpper );
            this.VoltmeterHighLimitRange = new Core.Primitives.RangeR( Properties.Settings.Default.VoltmeterLimitMinimum, Properties.Settings.Default.VoltmeterLimitMaximum );
            this.VoltmeterLowLimitRange = new Core.Primitives.RangeR( Properties.Settings.Default.VoltmeterLimitMinimum, Properties.Settings.Default.VoltmeterLimitMaximum );
            this.VoltmeterRangeRange = new Core.Primitives.RangeR( Properties.Settings.Default.VoltmeterRangeLower, Properties.Settings.Default.VoltmeterRangeUpper );
            this.ImpedanceRangeRange = new Core.Primitives.RangeR( Properties.Settings.Default.NominalResistanceMinimum, HarmonicsMeasureSubsystem.TopImpedanceRangeModeValue );
            this.HarmonicsStatusSubsystem = statusSubsystem;
        }

        /// <summary>
        /// Defines the clear execution state (CLS) by setting system properties to the their Clear
        /// Execution (CLS) default values.
        /// </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        public override void DefineClearExecutionState()
        {
            base.DefineClearExecutionState();
            this.AccessRightsMode = 0;
            this.AutoRangeEnabled = true;
            this.BandwidthLimitingEnabled = false;
            this.VoltmeterOutputEnabled = true;
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultMeasureModeDecimalPlaces;
            this.GeneratorOutputLevel = 0;
            this.GeneratorTimer = 250;
            this.VoltmeterHighLimit = 0.1;
            this.ImpedanceRange = 300;
            this.ImpedanceRangeMode = 1;
            this.VoltmeterLowLimit = 1e-8;
            this.MeasureMode = VI.HarmonicsMeasureMode.Voltage;
            this.VoltmeterRange = 10e-6;
            this.VoltmeterRangeMode = 1;
            this.MeasurementStartMode = 0;
        }

        /// <summary>
        /// Defines the known reset state (RST) by setting system properties to the their Reset (RST)
        /// default values. The instrument does not have a defined reset state. It retains its last
        /// settings. For this reason the reset state is set to null.
        /// </summary>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.AccessRightsMode = new int?();
            this.AutoRangeEnabled = new bool?();
            this.BandwidthLimitingEnabled = new bool?();
            this.VoltmeterOutputEnabled = new bool?();
            this.FunctionUnit = this.DefaultFunctionUnit;
            this.FunctionRange = this.DefaultFunctionRange;
            this.FunctionRangeDecimalPlaces = this.DefaultMeasureModeDecimalPlaces;
            this.GeneratorOutputLevel = new double?();
            this.GeneratorTimer = new int?();
            this.VoltmeterHighLimit = new double?();
            this.ImpedanceRange = new double?();
            this.ImpedanceRangeMode = new int?();
            this.VoltmeterLowLimit = new double?();
            this.VoltmeterRangeMode = new int?();
            this.MeasurementStartMode = new int?();
            this.VoltmeterRange = new double?();
        }

        #endregion

        #region " STATUS SUBSYTEM "

        /// <summary>   Gets the harmonics status subsystem. </summary>
        /// <value> The harmonics status subsystem. </value>
        public StatusSubsystem HarmonicsStatusSubsystem { get; }

        #endregion

        #region " PARSE SUBSYSTEM MESSAGE "

        /// <summary>   Parses a subsystem message assigning value to a subsystem property. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <returns>   True if it message was parsed by the subsystem; otherwise, false. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0045:Convert to conditional expression", Justification = "<Pending>" )]
        public override bool ParseSubsystemMessage( string receivedMessage )
        {
            receivedMessage = receivedMessage.TrimEnd( this.Session.TerminationCharacters() );
            bool outcome = false;
            if ( receivedMessage.StartsWith( "ID," ) )
            {
                _ = this.StatusSubsystem.ReadIdentity( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "AR=" ) )
            {
                _ = this.ParseAccessRightsMode( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "GL=" ) )
            {
                _ = this.ParseGeneratorOutputLevel( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "GT=" ) )
            {
                _ = this.ParseGeneratorTimer( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "BW=" ) )
            {
                _ = this.ParseBandwidthLimitingEnabled( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "LH=" ) )
            {
                _ = this.ParseVoltmeterHighLimit( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "LL=" ) )
            {
                _ = this.ParseVoltmeterLowLimit( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "MS=" ) )
            {
                _ = this.ParseMeasurementStartMode( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "ZX=" ) )
            {
                _ = this.ParseImpedanceRangeMode( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "VD=" ) )
            {
                _ = this.ParseMeasureMode( receivedMessage );
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "VM=" ) )
            {
                if ( receivedMessage.StartsWith( "VM=O" ) )
                {
                    _ = this.ParseVoltmeterOutputEnabled( receivedMessage );
                }
                else
                {
                    _ = this.ParsePrimaryReadingFull( receivedMessage );
                }
                outcome = true;
            }
            else if ( receivedMessage.StartsWith( "VR=" ) )
            {
                _ = this.ParseVoltmeterRange( receivedMessage );
                outcome = true;
            }
            return outcome;
        }

        #endregion

        #region " ACCESS RIGHTS MODE "

        /// <summary>   Gets or sets The Access Rights Mode command format. </summary>
        /// <value> The Access Rights Mode command format. </value>
        protected override string AccessRightsModeCommandFormat { get; set; } = "AR,{0}";

        /// <summary>   Gets or sets The Access Rights Mode query command. </summary>
        /// <value> The Access Rights Mode query command. </value>
        protected override string AccessRightsModeQueryCommand { get; set; } = "AR?";

        /// <summary>
        /// Attempts to parse the access rights mode from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>   A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo). </returns>
        public override (bool HasValue, ParseInfo<int> ParseInfo) TryParseAccessRightsMode( string receivedMessage, out int parsedValue )
        {
            var outcome = this.TryParseInt( receivedMessage, "AR=", string.Empty, out parsedValue );
            if ( outcome.HasValue )
                this.AccessRightsMode = parsedValue;
            return outcome;
        }

        /// <summary>   Writes a Access Rights Mode. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">    The Generator Output Level. </param>
        /// <returns>   A tuple: (int SentValue, WriteInfo{int} WriteInfo) </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteAccessRightsMode( AccessRightsMode value )
        {
            (int sentValue, WriteInfo<int> writeInfo) = base.WriteAccessRightsMode( ( int ) value );
            this.AccessRightsMode = sentValue;
            return (sentValue, writeInfo);
        }

        /// <summary>   Gets the Access Rights Mode. </summary>
        /// <value> The Access Rights Mode. </value>
        public AccessRightsMode CurrentAccessRightsMode => ( AccessRightsMode ) this.AccessRightsMode;

        #endregion

        #region " BANDWIDTH LIMITING ENABLED "

        /// <summary>   Gets or sets the bandwidth limiting enabled command Format. </summary>
        /// <remarks>   "BW,{0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The bandwidth limiting enabled query command. </value>
        protected override string BandwidthLimitingEnabledCommandFormat { get; set; } = "BW,{0:ON;ON;OFF}";

        /// <summary>   Gets or sets the bandwidth limiting enabled query command. </summary>
        /// <value> The bandwidth limiting enabled query command. </value>
        protected override string BandwidthLimitingEnabledQueryCommand { get; set; } = "BW?";

        /// <summary>
        /// Attempts to parse a bandwidth limiting enabled from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, ParseBooleanInfo ParseBooleanInfo) </returns>
        public override (bool HasValue, ParseBooleanInfo ParseBooleanInfo) TryParseBandwidthLimitingEnabled( string receivedMessage, out bool parsedValue )
        {
            return this.TryParseBool( receivedMessage, "BW=", "ON", "OFF", out parsedValue );
        }

        #endregion

        #region " GENERATOR OUTPUT LEVEL "

        /// <summary>   Gets or sets The GeneratorOutputLevel command format. </summary>
        /// <value> The GeneratorOutputLevel command format. </value>
        protected override string GeneratorOutputLevelCommandFormat { get; set; } = "GL,{0}V";

        /// <summary>   Gets or sets The GeneratorOutputLevel query command. </summary>
        /// <value> The GeneratorOutputLevel query command. </value>
        protected override string GeneratorOutputLevelQueryCommand { get; set; } = "GL?";

        /// <summary>
        /// Attempts to parse a generator output level from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>   A Tuple: (bool HasValue, ParseInfo{T} ParseInfo) </returns>
        public override (bool HasValue, ParseInfo<double> ParseInfo) TryParseGeneratorOutputLevel( string receivedMessage, out double parsedValue )
        {
            var outcome =
                this.TryParseDouble( receivedMessage, "GL=", new string[] { "mV", "V" }, new double[] { 0.001, 1 }, out parsedValue );
            if ( outcome.HasValue )
                this.GeneratorOutputLevel = parsedValue;
            return outcome;
        }

        #endregion

        #region " GENERATOR TIMER "

        /// <summary>   Gets or sets The GeneratorTimer command format. </summary>
        /// <value> The GeneratorTimer command format. </value>
        protected override string GeneratorTimerCommandFormat { get; set; } = "GT,{0}";

        /// <summary>   Gets or sets The GeneratorTimer query command. </summary>
        /// <value> The GeneratorTimer query command. </value>
        protected override string GeneratorTimerQueryCommand { get; set; } = "GT?";

        /// <summary>
        /// Attempts to parse a generator timer from the given data, returning a default value rather
        /// than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>
        /// A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo)
        /// </returns>
        public override (bool HasValue, ParseInfo<int> ParseInfo) TryParseGeneratorTimer( string receivedMessage, out int parsedValue )
        {
            return this.TryParseInt( receivedMessage, "GT=", "ms", out parsedValue );
        }

        #endregion

        #region " MEASUREMENT START MODE "

        /// <summary>   Gets or sets The Measurement Start command format. </summary>
        /// <value> The Measurement Start command format. </value>
        protected override string MeasurementStartModeCommandFormat { get; set; } = "MS,{0}";

        /// <summary>   Gets or sets The Measurement Start query command. </summary>
        /// <value> The Measurement Start query command. </value>
        protected override string MeasurementStartModeQueryCommand { get; set; } = "MS?";

        /// <summary>
        /// Attempts to parse a measurement start mode from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A Tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public override (bool HasValue, ParseInfo<int> ParseInfo) TryParseMeasurementStartMode( string receivedMessage, out int parsedValue )
        {
            return this.TryParseInt( receivedMessage, "MS=", string.Empty, out parsedValue );
        }

        /// <summary>   Writes a measurement start. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The Generator Output Level. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (int SentValue, WriteInfo{int} WriteInfo) </returns>
        public (int SentValue, WriteInfo<int> WriteInfo) WriteMeasurementStart( MeasurementStartMode value, bool checkStatus = false )
        {
            (int sentValue, WriteInfo<int> writeInfo) = base.WriteMeasurementStartMode( ( int ) value, checkStatus );
            this.MeasurementStartMode = sentValue;
            return (sentValue, writeInfo);
        }

        /// <summary>   Gets the measurement start mode. </summary>
        /// <value> The measurement start mode. </value>
        public MeasurementStartMode CurrentMeasurementStartMode => ( MeasurementStartMode ) this.MeasurementStartMode;

        #endregion

        #region " IMPEDANCE RANGE MODE "

        /// <summary>   Gets or sets the Impedance Range Mode. </summary>
        /// <value> The Impedance Range Mode. </value>
        public override int? ImpedanceRangeMode
        {
            get => base.ImpedanceRangeMode;

            protected set {
                if ( !Nullable.Equals( this.ImpedanceRangeMode, value ) )
                {
                    base.ImpedanceRangeMode = value;
                    this.NotifyPropertyChanged();
                    this.ImpedanceRange = value.HasValue ? ToImpedanceRange( ( Clt10.ImpedanceRangeMode ) value.Value ) : new double?();
                }
            }
        }

        /// <summary>   Gets or sets The Impedance Range Mode command format. </summary>
        /// <value> The Impedance Range Mode command format. </value>
        protected override string ImpedanceRangeModeCommandFormat { get; set; } = "ZX,{0}";

        /// <summary>   Gets or sets The Impedance Range Mode query command. </summary>
        /// <value> The Impedance Range Mode query command. </value>
        protected override string ImpedanceRangeModeQueryCommand { get; set; } = "ZX?";

        /// <summary>   Queries parse Impedance Range Mode. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>   A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo). </returns>
        public override (bool HasValue, ParseInfo<int> ParseInfo) TryParseImpedanceRangeMode( string receivedMessage, out int parsedValue )
        {
            return this.TryParseInt( receivedMessage, "ZX=", string.Empty, out parsedValue );
        }

        /// <summary>
        /// Attempts to parse an impedance range from the given data, returning a default value rather
        /// than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public override (bool HasValue, ParseInfo<double> ParseInfo) TryParseImpedanceRange( string receivedMessage, out double parsedValue )
        {
            (bool HasValue, ParseInfo<int> ParseInfo) = this.TryParseImpedanceRangeMode( receivedMessage, out int result );
            parsedValue = HasValue ? ToImpedanceRange( ( Clt10.ImpedanceRangeMode ) result ) : default;
            return (HasValue, new ParseInfo<double>( parsedValue, ParseInfo.ParsedMessage, ParseInfo.ReceivedMessage ));
        }

        /// <summary>   Gets the Impedance Range Mode. </summary>
        /// <value> The Impedance Range Mode. </value>
        public ImpedanceRangeMode CurrentImpedanceRangeMode => ( ImpedanceRangeMode ) this.ImpedanceRangeMode;

        /// <summary>   Gets or sets the impedance range change execution refractory time span. </summary>
        /// <value> The impedance range change execution refractory time span. </value>
        public override TimeSpan ImpedanceRangeRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 250 );

        #endregion

        #region " IMPEDANCE RANGE "

        /// <summary>   Converts an impedance to an impedance range mode. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="impedance">    The impedance. </param>
        /// <returns>   Impedance as an ImpedanceRangeMode. </returns>
        public static ImpedanceRangeMode ToImpedanceRangeMode( double impedance )
        {
            return (impedance < 300) ? Clt10.ImpedanceRangeMode.LessThan300
                : (impedance <= 3000) ? Clt10.ImpedanceRangeMode.Between300And3000
                    : (impedance <= 30000) ? Clt10.ImpedanceRangeMode.Betweeen3000And30000
                                    : Clt10.ImpedanceRangeMode.Above30000;

        }

        /// <summary>   Gets the top impedance range mode value. </summary>
        /// <value> The top impedance range mode value. </value>
        public static double TopImpedanceRangeModeValue => Properties.Settings.Default.NominalResistanceMaximum;

        /// <summary>   Converts an impedanceRangeMode to an impedance range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="impedanceRangeMode">   The impedance range mode. </param>
        /// <returns>   ImpedanceRangeMode as a double. </returns>
        public static double ToImpedanceRange( Clt10.ImpedanceRangeMode impedanceRangeMode )
        {

            return (impedanceRangeMode == Clt10.ImpedanceRangeMode.LessThan300) ? (300 - 0.001)
                : (impedanceRangeMode == Clt10.ImpedanceRangeMode.Between300And3000) ? 3000
                    : (impedanceRangeMode == Clt10.ImpedanceRangeMode.Betweeen3000And30000) ? 30000
                                    : HarmonicsMeasureSubsystem.TopImpedanceRangeModeValue;
        }

        /// <summary>   Writes the impedance range query command. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public override ExecuteInfo WriteImpedanceRangeQueryCommand()
        {
            return this.WriteLineElapsed( this.ImpedanceRangeModeQueryCommand );
        }

        /// <summary>   Queries impedance Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo)
        /// </returns>
        public override (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryImpedanceRange( bool checkStatus = false )
        {
            (int impedanceRangeModeParsedValue, QueryParseInfo<int> queryParseInfo) = this.QueryImpedanceRangeMode( checkStatus );
            double parsedValue = ToImpedanceRange( ( ImpedanceRangeMode ) impedanceRangeModeParsedValue );
            return (parsedValue, new QueryParseInfo<double>( parsedValue, queryParseInfo ));
        }

        /// <summary>   Writes a impedance Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The impedance value for setting the range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A tuple: (double SentValue, <see cref="WriteInfo{T}"/> WriteInfo) </returns>
        public override (double SentValue, WriteInfo<double> WriteInfo) WriteImpedanceRange( double value, bool checkStatus = false )
        {
            var (impedanceRangeModeSentValue, WriteInfo) = this.WriteImpedanceRangeMode( ( int ) ToImpedanceRangeMode( value ), checkStatus );
            double sentValue = ToImpedanceRange( ( ImpedanceRangeMode ) impedanceRangeModeSentValue );
            return (sentValue, new WriteInfo<double>( sentValue, WriteInfo.CommandFormat, WriteInfo.SentMessage, WriteInfo.GetElapsedTimes() ));
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary>
        /// Queries the Auto Range Enabled sentinel. Also sets the
        /// <see cref="P:isr.VI.HarmonicsMeasureSubsystemBase.AutoRangeEnabled">Enabled</see> sentinel.
        /// </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <returns>   A Tuple: (bool? ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo). </returns>
        public override (bool? ParsedValue, QueryParseBooleanInfo QueryParseBooleanInfo) QueryAutoRangeEnabled()
        {
            (int measureRangeModeParsedValue, QueryParseInfo<int> QueryParseInfo) = this.QueryVoltmeterRangeMode();
            bool parsedValue = ( int ) Clt10.VoltmeterRangeMode.Auto == measureRangeModeParsedValue;
            return (parsedValue, new QueryParseBooleanInfo( parsedValue, QueryParseInfo ));
        }

        /// <summary>
        /// Writes the Auto Range Enabled sentinel. Does not read back from the instrument.
        /// </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        if set to <c>True</c> is enabled. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   a Tuple: (bool SentValue, WriteBooleanInfo WriteBooleanInfo). </returns>
        public override (bool SentValue, WriteBooleanInfo WriteBooleanInfo) WriteAutoRangeEnabled( bool value, bool checkStatus = false )
        {
            if ( value )
            {
                (int measureRangeModeSentValue, WriteInfo<int> writeInfo) = this.WriteVoltmeterRangeMode( ( int ) Clt10.VoltmeterRangeMode.Auto, checkStatus );
                bool sentValue = ( int ) Clt10.VoltmeterRangeMode.Auto == measureRangeModeSentValue;
                return (sentValue, new WriteBooleanInfo( sentValue, writeInfo ));
            }
            else
            {
                // set the existing value
                bool sentValue = ( int ) Clt10.VoltmeterRangeMode.Auto == this.VoltmeterRangeMode.GetValueOrDefault( ( int ) Clt10.VoltmeterRangeMode.OneVolt );
                return (sentValue, new WriteBooleanInfo( sentValue, WriteInfo<int>.Empty ));
            }
        }

        #endregion

        #region " VOLTMETER HIGH LIMIT "

        /// <summary>   Gets or sets the Voltmeter high limit command format. </summary>
        /// <value> The Voltmeter high limit command format. </value>
        protected override string VoltmeterHighLimitCommandFormat { get; set; } = "LH,{{0}}{0}";

        /// <summary>   Gets or sets the Voltmeter high limit query command. </summary>
        /// <value> The Voltmeter high limit query command. </value>
        protected override string VoltmeterHighLimitQueryCommand { get; set; } = "LH?";

        /// <summary>
        /// Attempts to parse a voltmeter high limit from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public override (bool HasValue, ParseInfo<double> ParseInfo) TryParseVoltmeterHighLimit( string receivedMessage, out double parsedValue )
        {
            return this.TryParseDouble( receivedMessage, "LH=", new string[] { "mV", "uV" }, new double[] { 0.001, 0.000001 }, out parsedValue );
        }

        /// <summary>   Builds limit command. </summary>
        /// <remarks>   David, 2021-06-17. </remarks>
        /// <param name="commandFormat">    The command format. </param>
        /// <param name="value">            The impedance value for setting the range. </param>
        /// <returns>   A string. </returns>
        private static string BuildLimitCommand( string commandFormat, double value )
        {
            double scale = 1000;
            string unit = "mV";
            int decimalPlaces = 3;
            if ( value < 0.001 )
            {
                scale = 1000000;
                unit = "uV";
                decimalPlaces = value > 0.0001 ? 2 : value > 0.00001 ? 3 : 4;
            }
            return string.Format( string.Format( commandFormat, unit ), Math.Round( scale * value, decimalPlaces ) );
        }

        /// <summary>   Writes a scaled Voltmeter high limit. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The voltmeter high limit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns> A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        protected override (double sentValue, WriteInfo<double> writeInfo) WriteScaledVoltmeterHighLimit( double value, bool checkStatus = false )
        {
            string command = HarmonicsMeasureSubsystem.BuildLimitCommand( this.VoltmeterHighLimitCommandFormat, value );
            ExecuteInfo executeInfo = checkStatus
                ? this.WriteStatusReady( this.ReadyToWriteTimeout, command )
                : this.WriteLineElapsed( command );
            return (value, new WriteInfo<double>( value, executeInfo ));
        }

        #endregion

        #region " VOLTMETER LOW LIMIT "

        /// <summary>   Gets or sets The LowLimit command format. </summary>
        /// <value> The LowLimit command format. </value>
        protected override string VoltmeterLowLimitCommandFormat { get; set; } = "LL,{{0}}{0}";

        /// <summary>   Gets or sets The LowLimit query command. </summary>
        /// <value> The LowLimit query command. </value>
        protected override string VoltmeterLowLimitQueryCommand { get; set; } = "LL?";

        /// <summary>
        /// Attempts to parse a voltmeter Low limit from the given data, returning a default value rather
        /// than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>   A tuple: (bool HasValue, <see cref="ParseInfo{T} "/> ParseInfo). </returns>
        public override (bool HasValue, ParseInfo<double> ParseInfo) TryParseVoltmeterLowLimit( string receivedMessage, out double parsedValue )
        {
            return this.TryParseDouble( receivedMessage, "LH=", new string[] { "mV", "uV" }, new double[] { 0.001, 0.000001 }, out parsedValue );
        }

        /// <summary>   Writes a scaled Low limit. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The voltmeter low limit. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns> A tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo)  </returns>
        protected override (double sentValue, WriteInfo<double> writeInfo) WriteScaledVoltmeterLowLimit( double value, bool checkStatus = false )
        {
            string command = HarmonicsMeasureSubsystem.BuildLimitCommand( this.VoltmeterLowLimitCommandFormat, value );
            ExecuteInfo executeInfo = checkStatus
                ? this.WriteStatusReady( this.ReadyToWriteTimeout, command )
                : this.WriteLineElapsed( command );
            return (value, new WriteInfo<double>( value, executeInfo ));
        }


        #endregion

        #region " VOLTMETER OUTPUT ENABLED "

        /// <summary>   Gets or sets the Voltmeter Output enabled command Format. </summary>
        /// <remarks>   "VM,{0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Voltmeter Output enabled query command. </value>
        protected override string VoltmeterOutputEnabledCommandFormat { get; set; } = "VM,{0:ON;ON;OFF}";

        /// <summary>   Gets or sets the Voltmeter Output enabled query command. </summary>
        /// <value> The Voltmeter Output enabled query command. </value>
        protected override string VoltmeterOutputEnabledQueryCommand { get; set; } = "VM?";

        /// <summary>
        /// Attempts to parse a Voltmeter Output enabled from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, ParseBooleanInfo ParseBooleanInfo) </returns>
        public override (bool HasValue, ParseBooleanInfo ParseBooleanInfo) TryParseVoltmeterOutputEnabled( string receivedMessage, out bool parsedValue )
        {
            return this.TryParseBool( receivedMessage, "VM=", "ON", "OFF", out parsedValue );
        }

        #endregion

        #region " VOLTMETER RANGE MODE "

        /// <summary>   Gets or sets the Voltmeter Range Mode. </summary>
        /// <value> The Voltmeter Range Mode. </value>
        public override int? VoltmeterRangeMode
        {
            get => base.VoltmeterRangeMode;

            protected set {
                if ( !Nullable.Equals( this.VoltmeterRangeMode, value ) )
                {
                    base.VoltmeterRangeMode = value;
                    this.NotifyPropertyChanged();
                    this.AutoRangeEnabled = value.HasValue ? (value.Value == ( int ) Clt10.VoltmeterRangeMode.Auto) : new bool?();
                    this.VoltmeterRange = this.AutoRangeEnabled.GetValueOrDefault( false )
                        ? new double?()
                        : value.HasValue ? ToVoltmeterRange( ( Clt10.VoltmeterRangeMode ) value.Value ) : new double?();
                }
            }
        }

        /// <summary>   Gets or sets The Voltmeter Range Mode command format. </summary>
        /// <value> The Voltmeter Range Mode command format. </value>
        protected override string VoltmeterRangeModeCommandFormat { get; set; } = "VR,{0}";

        /// <summary>   Gets or sets The Voltmeter Range Mode query command. </summary>
        /// <value> The Voltmeter Range Mode query command. </value>
        protected override string VoltmeterRangeModeQueryCommand { get; set; } = "VR?";

        /// <summary>   Queries parse Voltmeter Range Mode. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>   A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo). </returns>
        public override (bool HasValue, ParseInfo<int> ParseInfo) TryParseVoltmeterRangeMode( string receivedMessage, out int parsedValue )
        {
            string autoRangeReply = "VR=Autorange";
            if ( string.Equals( autoRangeReply, receivedMessage.TrimEnd( this.Session.TerminationCharacters() ), StringComparison.OrdinalIgnoreCase ))
            {
                this.VoltmeterRange = new double?();
                parsedValue = 0;
                return (true, new ParseInfo<int>( parsedValue, String.Empty, receivedMessage ));
            }
            else
            {
                (bool HasValue, ParseInfo<double> ParseInfo) = this.TryParseDouble( receivedMessage,
                                                            "VR=", new string[] { "uV", "mV" }, new double[] { 0.000001, 0.001 }, out double result );
                this.VoltmeterRange = HasValue ? result : default;
                parsedValue = HasValue ? ( int ) ToVoltmeterRangeMode( result ) : default;
                return (HasValue, new ParseInfo<int>( parsedValue, ParseInfo.ParsedMessage, receivedMessage ));
            }
        }

        /// <summary>   Gets the Voltmeter Range Mode. </summary>
        /// <value> The Voltmeter Range Mode. </value>
        public VoltmeterRangeMode CurrentVoltmeterRangeMode => ( VoltmeterRangeMode ) this.VoltmeterRangeMode;

        #endregion

        #region " VOLTMETER RANGE "

        /// <summary>   Gets or sets the Voltmeter Range. </summary>
        /// <value> The Voltmeter Range. </value>
        public override double? VoltmeterRange
        {
            get => base.VoltmeterRange;

            protected set {
                if ( !Nullable.Equals( this.VoltmeterRange, value ) )
                {
                    base.VoltmeterRange = value;
                    if ( value.HasValue )
                        this.VoltmeterRangeMode = ( int ) ToVoltmeterRangeMode( value.Value );
                    this.VoltmeterRangeMode = value.HasValue ? ( int ) ToVoltmeterRangeMode( value.Value ) : new int?();
                }
            }
        }

        /// <summary>   Converts a voltage to a Voltmeter Range mode. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="voltage">  The voltage. </param>
        /// <returns>   Voltage as a MeasureRangeMode. </returns>
        public static Clt10.VoltmeterRangeMode ToVoltmeterRangeMode( double voltage )
        {
            return (voltage <= 1e-6) ? Clt10.VoltmeterRangeMode.OneMicroVolt
                : (voltage <= 10e-6) ? Clt10.VoltmeterRangeMode.TenMicroVolt
                    : (voltage <= 100e-6) ? Clt10.VoltmeterRangeMode.HunderdMicroVolt
                        : (voltage <= 1e-3) ? Clt10.VoltmeterRangeMode.OneMilliVolt
                            : (voltage <= 10e-3) ? Clt10.VoltmeterRangeMode.OneMicroVolt
                                : (voltage <= 100e-3) ? Clt10.VoltmeterRangeMode.OneMicroVolt
                                    : Clt10.VoltmeterRangeMode.OneVolt;

        }

        /// <summary>   Converts a Voltmeter Range Mode to a Voltmeter range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="rangeMode"> The Voltmeter Range mode. </param>
        /// <returns>   MeasureRangeMode as a double. </returns>
        public static double ToVoltmeterRange( Clt10.VoltmeterRangeMode rangeMode )
        {

            return (rangeMode == Clt10.VoltmeterRangeMode.Auto) ? throw new InvalidOperationException( $"{rangeMode} cannot be converted to a range value" )
                : (rangeMode == Clt10.VoltmeterRangeMode.OneMicroVolt) ? 1e-6
                : (rangeMode == Clt10.VoltmeterRangeMode.TenMicroVolt) ? 10e-6
                    : (rangeMode == Clt10.VoltmeterRangeMode.HunderdMicroVolt) ? 100e-6
                        : (rangeMode == Clt10.VoltmeterRangeMode.OneMilliVolt) ? 1e-3
                            : (rangeMode == Clt10.VoltmeterRangeMode.TenMillivolt) ? 10e-3
                                : (rangeMode == Clt10.VoltmeterRangeMode.HundredMillivolt) ? 100e-3
                                    : 1;
        }

        /// <summary>   Writes the Voltmeter Range query command. </summary>
        /// <remarks>   David, 2021-04-15. </remarks>
        /// <returns>   An ExecuteInfo. </returns>
        public override ExecuteInfo WriteVoltmeterRangeQueryCommand()
        {
            return this.WriteLineElapsed( this.VoltmeterRangeModeQueryCommand );
        }

        /// <summary>
        /// Attempts to parse a Voltmeter Range from the given data, returning a default value
        /// rather than throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  Message describing the received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>  A tuple: (bool hasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        public override (bool hasValue, ParseInfo<double> ParseInfo) TryParseVoltmeterRange( string receivedMessage, out double parsedValue )
        {
            return this.TryParseDouble( receivedMessage, "VR=", new string[] { "uV", "mV" }, new double[] { 0.000001, 0.001 }, out parsedValue );
        }

        /// <summary>   Queries Voltmeter Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>
        /// A Tuple: (double ParsedValue, <see cref="QueryParseInfo{T}"/> QueryParseInfo).
        /// </returns>
        public override (double ParsedValue, QueryParseInfo<double> QueryParseInfo) QueryVoltmeterRange( bool checkStatus = false )
        {
            (int MeasureRangeModeParsedValue, QueryParseInfo<int> QueryParseInfo) = this.QueryVoltmeterRangeMode( checkStatus );
            double parsedValue = ToVoltmeterRange( ( Clt10.VoltmeterRangeMode ) MeasureRangeModeParsedValue );
            return (parsedValue, new QueryParseInfo<double>( parsedValue, QueryParseInfo ));
        }

        /// <summary>   Writes a Voltmeter Range. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="value">        The Voltmeter Range. </param>
        /// <param name="checkStatus">  (Optional) True to check status. </param>
        /// <returns>   A Tuple: (double sentValue, <see cref="WriteInfo{T}"/> writeInfo) </returns>
        public override (double sentValue, WriteInfo<double> writeInfo) WriteVoltmeterRange( double value, bool checkStatus = false )
        {
            var reply = this.WriteVoltmeterRangeMode( ( int ) ToVoltmeterRangeMode( value ), checkStatus );
            Core.ApplianceBase.DoEventsWait( this.VoltmeterRangeRefractoryTimeSpan );
            var sentRange = this.VoltmeterRange.GetValueOrDefault( 1e-6 );
            return (sentRange, new WriteInfo<double>( sentRange, reply.WriteInfo.CommandFormat, reply.WriteInfo.SentMessage, reply.WriteInfo.GetElapsedTimes() ));
        }

        /// <summary>   Gets or sets the range change execution refractory time span. </summary>
        /// <value> The range change execution refractory time span. </value>
        public override TimeSpan VoltmeterRangeRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 200 );

        #endregion

        #region " CONFIGURE "

        /// <summary>   validates the configuration described by configuration. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <param name="configuration">    The configuration. </param>
        public void ValidateConfiguration( HarmonicsMeasureConfiguration configuration )
        {
            //configuration.NominalResistanceMaximum = Properties.Settings.Default.NominalResistanceMaximum;
            //configuration.NominalResistanceMinimum = Properties.Settings.Default.NominalResistanceMinimum;
            configuration.GeneratorCurrentMaximum = Properties.Settings.Default.GeneratorCurrentMaximum;

            if ( configuration.IsGeneratorCurrentExceedsLimit() )
                throw new InvalidOperationException( $"Harmonics system configuration current {(1000 * configuration.GeneratorCurrent):0}mA exceeds the current limit {(1000 * configuration.GeneratorCurrentMaximum):0}mA" );
            if ( !this.GeneratorOutputLevelRange.Contains( configuration.GeneratorOutputLevel ) )
                throw new InvalidOperationException( $"Harmonics system configuration generator output level {configuration.GeneratorOutputLevel}V is outside the acceptable range {this.GeneratorOutputLevelRange}V" );
            if ( !this.VoltmeterHighLimitRange.Contains( configuration.VoltmeterHighLimit ) )
                throw new InvalidOperationException( $"Harmonics system configuration voltmeter high limit {configuration.VoltmeterHighLimit}V is outside the acceptable range {this.VoltmeterHighLimitRange}V" );
            if ( !this.VoltmeterLowLimitRange.Contains( configuration.VoltmeterLowLimit ) )
                throw new InvalidOperationException( $"Harmonics system configuration voltmeter Low limit {configuration.VoltmeterLowLimit}V is outside the acceptable range {this.VoltmeterLowLimitRange}V" );
            if ( !this.GeneratorTimerRange.Contains( ( int ) configuration.GeneratorTimerTimeSpan.TotalMilliseconds ) )
                throw new InvalidOperationException( $"Harmonics system configuration generator timer {configuration.GeneratorTimerTimeSpan.TotalMilliseconds:0}ms is outside the acceptable range {this.GeneratorTimerRange}ms" );

        }

        /// <summary>   Establishes configuration range modes. </summary>
        /// <remarks>   David, 2021-06-19. </remarks>
        /// <param name="configuration">    The configuration. </param>
        public static void EstablishConfigurationRangeModes( HarmonicsMeasureConfiguration configuration )
        {
            configuration.ImpedanceRangeMode = ( int ) ToImpedanceRangeMode( configuration.NominalResistance );
            if ( configuration.UsingAutoRange )
            {
                configuration.VoltmeterRange = 0;
                configuration.VoltmeterRangeMode = ( int ) Clt10.VoltmeterRangeMode.Auto;
            }
            else
            {
                configuration.VoltmeterRange = configuration.VoltmeterHighLimit;
                VoltmeterRangeMode measureRangeMode = ToVoltmeterRangeMode( configuration.VoltmeterHighLimit );
                if ( configuration.ImpedanceRangeMode > ( int ) Clt10.ImpedanceRangeMode.LessThan300 && measureRangeMode == Clt10.VoltmeterRangeMode.OneMicroVolt )
                    measureRangeMode = Clt10.VoltmeterRangeMode.TenMicroVolt;
                if ( configuration.ImpedanceRangeMode < ( int ) Clt10.ImpedanceRangeMode.Betweeen3000And30000 && measureRangeMode == Clt10.VoltmeterRangeMode.OneVolt )
                    measureRangeMode = Clt10.VoltmeterRangeMode.HundredMillivolt;
                configuration.VoltmeterRangeMode = ( int ) measureRangeMode;
            }
        }

        /// <summary>   Configures the given configuration. </summary>
        /// <remarks>   David, 2021-03-30. </remarks>
        /// <param name="configuration">    The configuration. </param>
        /// <param name="checkStatus">      (Optional) True to check status. </param>
        public void Configure( HarmonicsMeasureConfiguration configuration, bool checkStatus = false )
        {
            HarmonicsMeasureSubsystem.EstablishConfigurationRangeModes( configuration );

            this.ValidateConfiguration( configuration );
            // make sure the measurement stops when configuring.
            configuration.MeasurementStartMode = ( int ) Clt10.MeasurementStartMode.MeasurementStop;
            _ = this.ApplyMeasurementStartMode( ( int ) Clt10.MeasurementStartMode.MeasurementStop, checkStatus );

            _ = this.ApplyAccessRightsMode( configuration.AccessRightsMode, checkStatus );

            _ = this.ApplyBandwidthLimitingEnabled( configuration.BandwidthLimitingEnabled, checkStatus );

            _ = this.ApplyVoltmeterOutputEnabled( configuration.VoltmeterOutputEnabled, checkStatus );

            _ = this.ApplyImpedanceRange( configuration.NominalResistance, checkStatus );

            _ = this.ApplyGeneratorTimer( ( int ) configuration.GeneratorTimerTimeSpan.TotalMilliseconds, checkStatus );

            _ = configuration.UsingAutoRange
                ? this.ApplyVoltmeterRangeMode( ( int )  VI.Clt10.VoltmeterRangeMode.Auto, checkStatus )
                : this.ApplyVoltmeterRangeMode( configuration.VoltmeterRangeMode, checkStatus );

            _ = this.ApplyVoltmeterHighLimit( configuration.VoltmeterHighLimit, checkStatus );

            _ = this.ApplyVoltmeterLowLimit( configuration.VoltmeterLowLimit, checkStatus );
            // must be issued after Voltmeter Range mode.

            _ = this.ApplyMeasureMode( configuration.MeasureMode, checkStatus );

            _ = this.ApplyGeneratorOutputLevel( configuration.GeneratorOutputLevel, checkStatus );

            _ = this.ApplyMeasurementStartMode( ( int ) Clt10.MeasurementStartMode.MeasurementStop, checkStatus );
        }

        #endregion

        #region " MEASURE MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string MeasureModeCommandFormat { get; set; } = "VD,{0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string MeasureModeQueryCommand { get; set; } = "VD?";

        /// <summary>   Gets or sets the measure model refractory time span. </summary>
        /// <value> The measure model refractory time span. </value>
        public override TimeSpan MeasureModelRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 100 );

        /// <summary>
        /// Attempts to parse a measure mode from the given data, returning a default value rather than
        /// throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>   A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) . </returns>
        public override (bool HasValue, ParseInfo<int> ParseInfo) TryParseMeasureMode( string receivedMessage, out HarmonicsMeasureMode parsedValue )
        {
            string trimmed = receivedMessage.TrimEnd( this.Session.TerminationCharacters() );
            bool success = Pith.SessionBase.TryParse( this.MeasureModeReadWrites, trimmed, out long v );
            parsedValue = success ? ( HarmonicsMeasureMode ) v : HarmonicsMeasureMode.Voltage;
            return (success, new ParseInfo<int>( ( int ) parsedValue, trimmed, receivedMessage ));
        }

        #endregion

        #region " CLEAR, TRIGGER, FETCH, READ (TRIGGER + FETCH ) "

        /// <summary>   Gets or sets the 'clear known state' command. </summary>
        /// <value> The 'clear known state' command. </value>
        protected override string ClearKnownStateCommand { get; set; } = "RS,10";

        /// <summary> Gets or sets the fetch command. </summary>
        /// <value> The fetch command. </value>
        public override string FetchCommand { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the read command. The instrument reads by issuing the trigger and then fetch
        /// commands implemented as two methods.
        /// </summary>
        /// <remarks>   Send measure start fetch last reading. </remarks>
        /// <value> The read command. </value>
        protected override string ReadCommand { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets The Measure query command. The instrument reads by issuing the trigger and then
        /// fetch commands implemented as two methods.
        /// </summary>
        /// <value> The Measure query command. </value>
        protected override string MeasureQueryCommand { get; set; } = string.Empty;

        /// <summary>   Gets or sets the clear known state refractory time span. </summary>
        /// <value> The clear known state refractory time span. </value>
        public override TimeSpan ClearKnownStateRefractoryTimeSpan { get; set; } = TimeSpan.FromMilliseconds( 100 );

        /// <summary>   Gets or sets the Trigger command. </summary>
        /// <value> The Trigger command. </value>
        protected override string TriggerCommand { get; set; } = $"MS,{( int ) VI.Clt10.MeasurementStartMode.SingleShot}";

        #endregion

    }

    /// <summary>   Values that represent measurement start modes. </summary>
    /// <remarks>   David, 2021-03-27. </remarks>
    public enum MeasurementStartMode
    {
        /// <summary>   An enum constant representing the measurement stop option. </summary>
        MeasurementStop = 0,

        /// <summary>   An enum constant representing the continuous option. </summary>
        Continuous = 1,

        /// <summary>   An enum constant representing the single shot option. </summary>
        SingleShot = 2,

        /// <summary>   An enum constant representing the simulated external trigger option. </summary>
        SimulatedExternalTrigger = 3,

        /// <summary>   An enum constant representing the external trigger option. </summary>
        ExternalTrigger = 4
    }

    /// <summary>   Values that represent access rights modes. </summary>
    /// <remarks>   David, 2021-03-30. </remarks>
    public enum AccessRightsMode
    {
        /// <summary>   An enum constant representing the normal option. </summary>
        Normal = 0,

        /// <summary>   An enum constant representing the wait one option. </summary>
        WaitOne = 1,

        /// <summary>   An enum constant representing the denial option. </summary>
        Denial = 2

    }

    /// <summary>   Values that represent impedance range modes. </summary>
    /// <remarks>   David, 2021-03-27. </remarks>
    public enum ImpedanceRangeMode
    {
        /// <summary>   An enum constant representing the less than 300 option. </summary>
        LessThan300 = 1,

        /// <summary>   An enum constant representing values between 300 and 3000 option. </summary>
        Between300And3000 = 2,

        /// <summary>   An enum constant representing the less than or equal 30000 option. </summary>
        Betweeen3000And30000 = 3,

        /// <summary>   An enum constant representing the above 30000 option. </summary>
        Above30000 = 4
    }

    /// <summary>   Values that represent Voltmeter Range modes. </summary>
    /// <remarks>   David, 2021-03-27. </remarks>
    public enum VoltmeterRangeMode
    {
        /// <summary>   An enum constant representing the Automatic option. </summary>
        Auto = 0,

        /// <summary>   An enum constant representing the one micro volt option. </summary>
        OneMicroVolt = 1,

        /// <summary>   An enum constant representing the ten micro volt option. </summary>
        TenMicroVolt = 2,

        /// <summary>   An enum constant representing the hundred micro volt option. </summary>
        HunderdMicroVolt = 3,

        /// <summary>   An enum constant representing the one milli volt option. </summary>
        OneMilliVolt = 4,

        /// <summary>   An enum constant representing the ten millivolt option. </summary>
        TenMillivolt = 5,

        /// <summary>   An enum constant representing the hundred millivolt option. </summary>
        HundredMillivolt = 6,

        /// <summary>   An enum constant representing the one volt option. </summary>
        OneVolt = 7
    }

}
