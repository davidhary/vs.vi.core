using System;

using isr.Core.EscapeSequencesExtensions;

namespace isr.VI.Clt10
{
    /// <summary> Defines a Harmonics Measure Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public partial class HarmonicsMeasureSubsystem
    {

        /// <summary>
        /// Attempts to parse a double from the given data, returning a default value rather than
        /// throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="suffix">           The suffix. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>
        /// A tuple: (bool Success, double ParsedValue, string ReceivedMessage, string ParsedMessage).
        /// </returns>
        internal (bool Success, string ReceivedMessage, string ParsedMessage) TryParseDouble( string receivedMessage, string prefix, string suffix, out double parsedValue )
        {
            string trimmed = receivedMessage.Substring( prefix.Length ).TrimEnd( this.Session.TerminationCharacters() ).TrimEnd( suffix.ToCharArray() );
            return (Pith.SessionBase.TryParse( trimmed, out parsedValue ))
                        ? (true, receivedMessage.InsertCommonEscapeSequences(), trimmed)
                        : (false, receivedMessage.InsertCommonEscapeSequences(), trimmed);
        }


        /// <summary>   parse reading and units. </summary>
        /// <remarks>   David, 2021-03-28. </remarks>
        /// <param name="rawReading">   The raw reading. </param>
        /// <param name="prefix">       The prefix. </param>
        /// <param name="units">        The units. </param>
        /// <param name="scales">       The scales. </param>
        /// <returns> A tuple: (bool Success, string Reading, string Units, double Scale). </returns>
        internal static (bool Success, string Reading, string Units, double Scale) SelectReadingUnit( string rawReading, string prefix, string[] units, double[] scales )
        {
            return SelectReadingUnit( rawReading.Substring( prefix.Length ), units, scales );
        }

        /// <summary>   parse reading and units. </summary>
        /// <remarks>   David, 2021-04-06. </remarks>
        /// <param name="reading">  The reading. </param>
        /// <param name="units">    The units. </param>
        /// <param name="scales">   The scales. </param>
        /// <returns> A tuple: (bool Success, string Reading, string Units, double Scale). </returns>
        internal static (bool Success, string Reading, string Units, double Scale) SelectReadingUnit( string reading, string[] units, double[] scales )
        {
            int unitIndex = reading.LastIndexOfAny( "0123456789.".ToCharArray() ) + 1;
            string readingValue = reading.Substring( 0, unitIndex );
            string readingUnit = reading.Substring( unitIndex );
#if true
            for ( int i = 0; i < units.Length; i++ )
            {
                if ( string.Equals( readingUnit, units[i], StringComparison.OrdinalIgnoreCase ) )
                    return (true, readingValue, readingUnit, scales[i]);
            }
#else
            for ( int i = 0; i < units.Length; i++ )
            {
                string unit = units[i];
                if ( reading.EndsWith( unit ) )
                    return (true, reading.TrimEnd( units[i].ToCharArray() ), unit, scales[i]);
            }
#endif
            return (false, reading, string.Empty, 0);
        }

        /// <summary>   Parse double. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="units">            The units. </param>
        /// <param name="scales">           The scales. </param>
        /// <returns> A tuple: (double ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo). </returns>
        internal (double ParsedValue, ParseInfo<double> ParseInfo) ParseDouble( string receivedMessage, string prefix, string[] units, double[] scales )
        {
            (bool HasValue, ParseInfo<double> ParseInfo) = this.TryParseDouble( receivedMessage, prefix, units, scales, out double parsedValue );
            return HasValue
                    ? (parsedValue, ParseInfo)
                    : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseInfo.ParsedMessage}'" );
        }

        /// <summary>
        /// Attempts to parse a double from the given data, returning a default value rather than
        /// throwing an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="units">            The units. </param>
        /// <param name="scales">           The scales. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        internal (bool HasValue, ParseInfo<double> ParseInfo) TryParseDouble( string receivedMessage, string prefix, string[] units, double[] scales, out double parsedValue )
        {
            (bool success, string reading, _, double scale) = SelectReadingUnit( receivedMessage.TrimEnd( this.Session.TerminationCharacters
                () ), prefix, units, scales );
            (bool parsed, double result) = success
                                             ? Pith.SessionBase.TryParse( reading, out double value )
                                                ? (true, value)
                                                : (false, default)
                                             : (false, default);
            parsedValue = parsed ? result * scale : result;
            return (parsed, new ParseInfo<double>( parsedValue, reading, receivedMessage ));
        }

        /// <summary>   Parse int. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="suffix">           The suffix. </param>
        /// <returns> A tuple: (int ParsedValue, <see cref="ParseInfo{T}"/> ParseInfo) . </returns>
        internal (int ParsedValue, ParseInfo<int> ParseInfo) ParseInt( string receivedMessage, string prefix, string suffix )
        {
            (bool HasValue, ParseInfo<int> ParseInfo) = this.TryParseInt( receivedMessage, prefix, suffix, out int parsedValue );
            return HasValue
                    ? (parsedValue, ParseInfo)
                    : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseInfo.ParsedMessage}'" );
        }

        /// <summary>
        /// Attempts to parse an int from the given data, returning a default value rather than throwing
        /// an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="suffix">           The suffix. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns> A tuple: (bool HasValue, <see cref="ParseInfo{T}"/> ParseInfo) </returns>
        internal (bool HasValue, ParseInfo<int> ParseInfo) TryParseInt( string receivedMessage, string prefix, string suffix, out int parsedValue )
        {
            string trimmed = receivedMessage.Substring( prefix.Length ).TrimEnd( this.Session.TerminationCharacters() ).TrimEnd( suffix.ToCharArray() );
            return Pith.SessionBase.TryParse( trimmed, out parsedValue )
                        ? (true, new ParseInfo<int>( parsedValue, trimmed, receivedMessage ))
                        : (false, new ParseInfo<int>( parsedValue, trimmed, receivedMessage ));
        }


        /// <summary>   Queries parse integer value. </summary>
        /// <remarks>   David, 2021-03-27. </remarks>
        /// <param name="defaultValue"> The default value. </param>
        /// <param name="queryCommand"> The query command. </param>
        /// <param name="prefix">       The prefix. </param>
        /// <param name="suffixes">     The suffixes. </param>
        /// <returns>   The parse generator output level. </returns>
        internal int? QueryParse( int? defaultValue, string queryCommand, string prefix, string[] suffixes )
        {
            string receivedMessage = this.QueryTrimEnd( this.Session.ReadDelay, queryCommand );
            receivedMessage = receivedMessage.Substring( prefix.Length );

            for ( int i = 0; i < suffixes.Length; i++ )
            {
                if ( receivedMessage.EndsWith( suffixes[i] ) )
                    return !int.TryParse( receivedMessage.TrimEnd( suffixes[i].ToCharArray() ), out int value ) ? defaultValue : value;
            }
            return defaultValue;
        }

        /// <summary>   Parse int. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="suffixes">         The suffixes. </param>
        /// <returns>   An int? </returns>
        internal int? ParseInt( string receivedMessage, string prefix, string[] suffixes )
        {
            receivedMessage = receivedMessage.Substring( prefix.Length );
            for ( int i = 0; i < suffixes.Length; i++ )
            {
                if ( receivedMessage.EndsWith( suffixes[i] ) )
                    return !int.TryParse( receivedMessage.TrimEnd( suffixes[i].ToCharArray() ), out int value ) ? new int?() : value;
            }
            return new int?();
        }

        /// <summary>   Parse bool. </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
        ///                                         type. </exception>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="trueValue">        The true value. </param>
        /// <param name="falseValue">       The false value. </param>
        /// <returns> A tuple: (bool string ReceivedMessage, string ParsedMessage). </returns>
        internal (bool ParsedValue, ParseBooleanInfo ParseBooleanInfo) ParseBool( string receivedMessage, string prefix, string trueValue, string falseValue )
        {
            (bool HasValue, ParseBooleanInfo ParseBooleanInfo) = this.TryParseBool( receivedMessage, prefix, trueValue, falseValue, out bool parsedValue );
            return HasValue
                ? (parsedValue, ParseBooleanInfo)
                : throw new InvalidCastException( $"Failed parsing '{receivedMessage.InsertCommonEscapeSequences()}' from '{ParseBooleanInfo.ParsedMessage}'" );
        }

        /// <summary>
        /// Attempts to parse a bool from the given data, returning a default value rather than throwing
        /// an exception if it fails.
        /// </summary>
        /// <remarks>   David, 2021-04-08. </remarks>
        /// <param name="receivedMessage">  The message received. </param>
        /// <param name="prefix">           The prefix. </param>
        /// <param name="trueValue">        The true value. </param>
        /// <param name="falseValue">       The false value. </param>
        /// <param name="parsedValue">      [out] The parsed value. </param>
        /// <returns>
        /// A tuple: (bool Success, ParseBooleanInfo).
        /// </returns>
        internal (bool HasValue, ParseBooleanInfo) TryParseBool( string receivedMessage, string prefix, string trueValue, string falseValue, out bool parsedValue )
        {
            string trimmed = receivedMessage.Substring( prefix.Length ).TrimEnd( this.Session.TerminationCharacters() );
            (bool hasValue, bool result) = string.Equals( trimmed, trueValue )
                                            ? (true, true)
                                                : string.Equals( trimmed, falseValue )
                                                    ? (true, false)
                                                    : (false, default);
            parsedValue = result;
            return (hasValue, new ParseBooleanInfo( parsedValue, trimmed, receivedMessage ));
        }
    }
}
