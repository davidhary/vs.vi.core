using System;
using System.Diagnostics;

using isr.Core.EscapeSequencesExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Clt10
{

    /// <summary> Implements a Keithley 2002 instrument device. </summary>
    /// <remarks>
    /// An instrument is defined, for the purpose of this library, as a device with a front panel.
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class Clt10Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Clt10Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public Clt10Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected Clt10Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            if ( statusSubsystem is object )
            {
                statusSubsystem.ExpectedLanguage = Pith.Ieee488.Syntax.LanguageTsp;
                Properties.Settings.Default.PropertyChanged += this.HandleSettingsPropertyChanged;
            }

            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static Clt10Device Create()
        {
            Clt10Device device = null;
            try
            {
                device = new Clt10Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static Clt10Device Validated( Clt10Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            // release unmanaged-only resources.
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( Clt10Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindHarmonicsMeasureSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindHarmonicsMeasureSubsystem( new HarmonicsMeasureSubsystem( this.StatusSubsystem ) );
            }
        }


        /// <summary>   Allows the derived device to take actions after opening. </summary>
        /// <remarks>
        /// This override should occur as the last call of the overriding method. The subsystems are
        /// added as part of the
        /// <see cref="M:isr.VI.VisaSessionBase.OnOpening(System.ComponentModel.CancelEventArgs)" />
        /// method. The synchronization context is captured as part of the property change and other
        /// event handlers and is no longer needed here.
        /// </remarks>
        /// <param name="e">    Event information to send to registered event handlers. </param>
        protected override void OnOpened( EventArgs e )
        {
            this.Session.ReadTerminationCharacter = 10;
            this.Session.ReadTerminationCharacterEnabled = true;
            base.OnOpened( e );
        }

        #endregion

        #region " HARMONICS MEASURE SUBSYSTEM "

        /// <summary> Gets or sets the Harmonics Measure Subsystem. </summary>
        /// <value> The Harmonics Measure Subsystem. </value>
        public HarmonicsMeasureSubsystem HarmonicsMeasureSubsystem { get; private set; }

        /// <summary> Binds the Harmonics Measure Subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindHarmonicsMeasureSubsystem( HarmonicsMeasureSubsystem subsystem )
        {
            if ( this.HarmonicsMeasureSubsystem is object )
            {
                this.HarmonicsMeasureSubsystem.PropertyChanged -= this.HarmonicsMeasureSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.HarmonicsMeasureSubsystem );
                this.HarmonicsMeasureSubsystem = null;
            }

            this.HarmonicsMeasureSubsystem = subsystem;
            if ( this.HarmonicsMeasureSubsystem is object )
            {
                this.HarmonicsMeasureSubsystem.PropertyChanged += this.HarmonicsMeasureSubsystemPropertyChanged;
                this.Subsystems.Add( this.HarmonicsMeasureSubsystem );
                this.HandlePropertyChanged( this.HarmonicsMeasureSubsystem, nameof( Clt10.HarmonicsMeasureSubsystem.MeasureMode ) );
            }
        }

        /// <summary> Handle the Harmonics Measure subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( HarmonicsMeasureSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Clt10.HarmonicsMeasureSubsystem.MeasureMode ):
                    {
                        this.SelectHarmonicsMeasureFunctionSubsystem( subsystem.MeasureMode.GetValueOrDefault( HarmonicsMeasureMode.Voltage ) );
                        break;
                    }
            }
        }

        /// <summary> Harmonics Measure subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inHarmonicsMeasureion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HarmonicsMeasureSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( Clt10.HarmonicsMeasureSubsystem )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as HarmonicsMeasureSubsystem, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets or sets the HarmonicsMeasure function subsystem. </summary>
        /// <value> The HarmonicsMeasure function subsystem. </value>
        public HarmonicsMeasureSubsystemBase HarmonicsMeasureFunctionSubsystem { get; private set; }

        /// <summary> Select HarmonicsMeasure function subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="functionMode"> The function mode. </param>
        private void SelectHarmonicsMeasureFunctionSubsystem( HarmonicsMeasureMode functionMode )
        {
            switch ( functionMode )
            {
                case HarmonicsMeasureMode.Voltage:
                    {
                        break;
                    }

                case HarmonicsMeasureMode.Decibel:
                    {
                        break;
                    }

            }
        }

        #endregion

        #region " STATUS SUBSYSTEM "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. <para>
        /// Exceptions are trapped and reported in the device class. </para> </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            // int statusByte = ( int ) this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            int statusByte = ( int ) this.Session.ReadStatusByte(); // this does not update the error bits
            this.ServiceRequestStatus = statusByte;

            this.ServiceRequestReading = string.Empty;
            this.ServiceRequestFailureMessage = string.Empty;

            // check for existing errors.
            (bool hasError, string errorDetails, int errorStatusByte) = this.StatusSubsystem.IsStatusError( statusByte );
            if ( hasError )
                throw new InvalidOperationException( $"Status error 0x{errorStatusByte:X2}:'{errorDetails}' detected" );

            if ( this.ServiceRequestAutoRead )
            {

                string receivedMessage;
                if ( this.StatusSubsystem.IsStatusQueryResultReady( statusByte ) )
                {
                    // if we have a query result, read it.
                    // This maybe too long; 
                    // this.Session.ReadDelay.SpinWait() ;
                    receivedMessage = this.Session.ReadLine();

                    this.ServiceRequestReading = (this.HarmonicsMeasureSubsystem?.ServiceRequestSubsystemParseMessageEnabled).GetValueOrDefault( false )
                        ? this.HarmonicsMeasureSubsystem.ParseSubsystemMessage( receivedMessage )
                            ? receivedMessage.InsertCommonEscapeSequences()
                            : $"Received message {receivedMessage.InsertCommonEscapeSequences()} was unhandled by the subsystem"
                        : receivedMessage.InsertCommonEscapeSequences();

                }
                else if ( this.StatusSubsystem.IsStatusDataReady( statusByte ) )
                {
                    // if we have data, query the last data
                    // the CLT10 does not issue a new SRQ after sending the query command.
                    receivedMessage = this.Session.ReadLine();


                    if ( this.HarmonicsMeasureSubsystem is object )
                    {
                        // has data to fetch
                        if ( this.HarmonicsMeasureSubsystem.ServiceRequestFetchReadingEnabled )
                        {
                            // parse the primary reading.
                            var (Success, _, RawReading) = this.HarmonicsMeasureSubsystem.ParsePrimaryReadingFull( receivedMessage );
                            if ( Success )
                            {
                                // this signals that we have proceed the service request
                                this.ServiceRequestReading = this.HarmonicsMeasureSubsystem.PrimaryReading.ReadingCaption;
                            }
                            else
                            {
                                throw new InvalidCastException( $"Failed parsing reading {receivedMessage.InsertCommonEscapeSequences()} from {RawReading.InsertCommonEscapeSequences()}" );
                            }
                        }
                        else if ( this.HarmonicsMeasureSubsystem.ServiceRequestStreamReadingsEnabled )
                        {
                            // parse a primary reading and adds it to the streaming buffer
                            if ( this.HarmonicsMeasureSubsystem.AddBufferReading( receivedMessage ) )
                            {
                                // this signals that we have proceed the service request
                                this.ServiceRequestReading = this.HarmonicsMeasureSubsystem.PrimaryReading.ReadingCaption;
                            }
                            else
                            {
                                throw new InvalidCastException( $"Failed adding reading {receivedMessage.InsertCommonEscapeSequences()}" );
                            }

                        }
                        else
                        {
                            // otherwise, signal completion of service request.
                            this.ServiceRequestReading = receivedMessage.InsertCommonEscapeSequences();
                        }
                    }
                    else
                    {
                        // otherwise, signal completion of service request.
                        this.ServiceRequestReading = receivedMessage.InsertCommonEscapeSequences();
                    }
                }
                else
                {
                    // otherwise, signal completion of service request with nothing to do.
                    this.ServiceRequestReading = $"Service request handled for status byte 0x{statusByte:X2}; nothing to read";
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "Clt10 Settings Editor", Properties.Settings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = Properties.Settings.Default;
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( Properties.Settings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Properties.Settings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( Properties.Settings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.ReadDelay ):
                    {
                        this.Session.ReadDelay = Properties.Settings.Default.ReadDelay;
                        break;
                    }

                case nameof( Properties.Settings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = Properties.Settings.Default.StatusReadDelay;
                        break;
                    }

                case nameof( Properties.Settings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = Properties.Settings.Default.StatusReadTurnaroundTime;
                        break;
                    }
            }
        }

        /// <summary> Handles settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleSettingsPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( Properties.Settings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as Properties.Settings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
