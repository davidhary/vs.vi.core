﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K2002 VI Forms Tests" )]
[assembly: AssemblyDescription( "K2002 Virtual Instrument Forms Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.vi.K2002.Forms.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
