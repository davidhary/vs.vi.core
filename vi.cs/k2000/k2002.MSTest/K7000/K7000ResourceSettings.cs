namespace isr.VI.K2002Tests
{

    /// <summary> The Keithley 7000 Resource Settings. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class K7000ResourceSettings : DeviceTests.ResourceSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private K7000ResourceSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(ResourceSettings)} Editor", ResourceSettings.Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static K7000ResourceSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static K7000ResourceSettings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (K7000ResourceSettings)Synchronized(new K7000ResourceSettings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " DEVICE RESOURCE INFORMATION "

        /// <summary> Gets or sets the Model of the resource. </summary>
        /// <value> The Model of the resource. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("7001")]
        public override string ResourceModel
        {
            get
            {
                return base.ResourceModel;
            }

            set
            {
                base.ResourceModel = value;
            }
        }

        /// <summary> Gets or sets the names of the candidate resources. </summary>
        /// <value> The names of the candidate resources. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue(@"TCPIP0::192.168.0.254::gpib0,15::INSTR|TCPIP0::192.168.0.254::gpib0,15::INSTR|
TCPIP0::10.1.1.25::gpib0,15::INSTR|TCPIP0::10.1.1.24::gpib0,15::INSTR")]
        public override string ResourceNames
        {
            get
            {
                return base.ResourceNames;
            }

            set
            {
                base.ResourceNames = value;
            }
        }

        /// <summary> Gets or sets the Title of the resource. </summary>
        /// <value> The Title of the resource. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Scanner7001")]
        public override string ResourceTitle
        {
            get
            {
                return base.ResourceTitle;
            }

            set
            {
                base.ResourceTitle = value;
            }
        }

        /// <summary> Gets or sets the language. </summary>
        /// <value> The language. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("SCPI")]
        public override string Language
        {
            get
            {
                return base.Language;
            }

            set
            {
                base.Language = value;
            }
        }

        /// <summary> Gets or sets the firmware revision. </summary>
        /// <value> The firmware revision. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1.6.4c")]
        public override string FirmwareRevision
        {
            get
            {
                return base.FirmwareRevision;
            }

            set
            {
                base.FirmwareRevision = value;
            }
        }

        #endregion

    }

    internal static partial class K7000Properties
    {

        /// <summary> Gets information describing the 7000 resource. </summary>
        /// <value> Information describing the 7000 resource. </value>
        internal static K7000ResourceSettings K7000ResourceInfo => K7000ResourceSettings.Get();
    }
}
