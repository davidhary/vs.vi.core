using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{

    /// <summary>   A resource tests. </summary>
    /// <remarks>   David, 2021-03-25. </remarks>
    public partial class ResourceTests
    {

        /// <summary> (Unit Test Method) tests open status session. </summary>
        [TestMethod()]
        public void OpenStatusSessionTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) tests wait for message available. </summary>
        [TestMethod()]
        public void WaitForMessageAvaiableTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertMessageAvailableShouldWaitFor( TestInfo, device.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) tests wait for operation completion. </summary>
        [TestMethod()]
        public void WaitForOperationCompletionTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertOperationCompletionShouldWaitFor( TestInfo, device.Session, ":SYST:CLE; *OPC" );
                DeviceTests.DeviceManager.AssertServiceRequestOperationCompletionWaitFor( TestInfo, device.Session, ":SYST:CLE; *OPC" );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) tests toggling service request handling. </summary>
        /// <remarks>
        /// This test will fail the first time it is run if Windows requests access through the Firewall.
        /// </remarks>
        [TestMethod()]
        public void ToggleServiceRequestHandlingTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertServiceRequestHandlingShouldToggle( TestInfo, device.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }
    }
}
