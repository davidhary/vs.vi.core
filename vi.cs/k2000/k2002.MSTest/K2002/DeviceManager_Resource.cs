namespace isr.VI.K2002Tests
{
    internal sealed partial class DeviceManager
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        private DeviceManager() : base()
        {
        }

        #endregion

        #region " DEVICE OPEN, CLOSE "

        /// <summary> Opens a session. </summary>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void OpenSession( DeviceTests.TestSite testInfo, VisaSessionBase device )
        {
            DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( testInfo, device, ResourceSettings.Get() );
        }

        /// <summary> Closes a session. </summary>
        /// <param name="testInfo"> Information describing the test. </param>
        /// <param name="device">   The device. </param>
        public static void CloseSession( DeviceTests.TestSite testInfo, VisaSessionBase device )
        {
            DeviceTests.DeviceManager.AssertDeviceShopuldCloseWithoutErrors( testInfo, device );
        }

        #endregion

    }
}
