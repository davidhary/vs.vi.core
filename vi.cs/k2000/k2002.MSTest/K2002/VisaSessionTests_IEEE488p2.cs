using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{
    public partial class VisaSessionTests
    {


        #region " OPEN/CLOSE "

        /// <summary> (Unit Test Method) tests open close visa session. </summary>
        [TestMethod()]
        public void OpenCloseVisaSessionTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) tests open close session base. </summary>
        [TestMethod()]
        public void OpenCloseSessionBaseTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, session, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionBaseShouldClose( TestInfo, session );
            }
        }

        #endregion

        /// <summary> (Unit Test Method) tests wait for status bitmask. </summary>
        [TestMethod()]
        public void WaitForStatusBitmaskTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertStatusBitmaskShouldWaitFor( TestInfo, session.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) tests wait for message available. </summary>
        /// <remarks>
        /// Initial Service Request Wait Complete Bitmask is 0x00<para>
        /// Initial Standard Event Enable Bitmask Is 0x00</para><para>
        /// DMM2002 status Byte 0x50 0x10 bitmask was Set</para>
        /// </remarks>
        [TestMethod()]
        public void WaitForMessageAvailableTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertMessageAvailableShouldWaitFor( TestInfo, session.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) tests wait for operation completion. </summary>
        [TestMethod()]
        public void WaitForOperationCompletionTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertOperationCompletionShouldWaitFor( TestInfo, session.Session, ":SYST:CLE; *OPC" );
                DeviceTests.DeviceManager.AssertServiceRequestOperationCompletionWaitFor( TestInfo, session.Session, ":SYST:CLE; *OPC" );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }

        /// <summary> (Unit Test Method) tests toggling service request handling. </summary>
        [TestMethod()]
        public void ToggleServiceRequestHandlingTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var session = VisaSession.Create();
            session.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldOpen( TestInfo, session, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertServiceRequestHandlingShouldToggle( TestInfo, session.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionShouldClose( TestInfo, session );
            }
        }
    }
}
