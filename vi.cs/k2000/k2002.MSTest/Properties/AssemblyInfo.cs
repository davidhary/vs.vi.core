﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K2002 VI Tests" )]
[assembly: AssemblyDescription( "K2002 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K2002.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
