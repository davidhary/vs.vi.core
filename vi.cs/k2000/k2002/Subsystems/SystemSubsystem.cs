using System;
using System.Collections.Generic;

namespace isr.VI.K2002
{

    /// <summary> Defines a System Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks>
        /// Additional Actions: <para>
        /// Clears Error Queue.
        /// </para>
        /// </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override void InitKnownState()
        {
            base.InitKnownState();
            string activity = string.Empty;
            try
            {
                activity = "Reading options";
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.QueryOptions();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.AutoZeroEnabled = true;
            this.BeeperEnabled = true;
            this.FourWireSenseEnabled = false;
            _ = this.QueryFrontTerminalsSelected();
        }

        #endregion

        #region " SYSTEM COMMANDS "

        /// <summary> Gets or sets the initialize memory command. </summary>
        /// <value> The initialize memory command. </value>
        protected override string InitializeMemoryCommand { get; set; } = Pith.Scpi.Syntax.InitializeMemoryCommand;

        /// <summary> Gets or sets the preset command. </summary>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = Pith.Scpi.Syntax.SystemPresetCommand;

        /// <summary> Gets or sets the language revision query command. </summary>
        /// <value> The language revision query command. </value>
        protected override string LanguageRevisionQueryCommand { get; set; } = Pith.Scpi.Syntax.LanguageRevisionQueryCommand;

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Gets or sets the automatic zero enabled query command. </summary>
        /// <value> The automatic zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = ":SYST:AZER:STAT?";

        /// <summary> Gets or sets the automatic zero enabled command format. </summary>
        /// <value> The automatic zero enabled command format. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = ":SYST:AZER:STAT {0:'ON';'ON';'OFF'}";

        #endregion

        #region " BEEPER ENABLED + IMMEDIATE "

        /// <summary> Gets or sets the Beeper enabled query command. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledQueryCommand { get; set; } = ":SYST:BEEP:STAT?";

        /// <summary> Gets or sets the Beeper enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledCommandFormat { get; set; } = ":SYST:BEEP:STAT {0:'1';'1';'0'}";

        /// <summary> Gets or sets the beeper immediate command format. </summary>
        /// <value> The beeper immediate command format. </value>
        protected override string BeeperImmediateCommandFormat { get; set; } = ":SYST:BEEP:IMM {0}, {1}";

        #endregion

        #region " FOUR WIRE SENSE ENABLED "

        /// <summary> Gets or sets the Four Wire Sense enabled query command. </summary>
        /// <value> The Four Wire Sense enabled query command. </value>
        protected override string FourWireSenseEnabledQueryCommand { get; set; } = ":SYST:RSEN?";

        /// <summary> Gets or sets the Four Wire Sense enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:RSEN {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Four Wire Sense enabled query command. </value>
        protected override string FourWireSenseEnabledCommandFormat { get; set; } = ":SYST:RSEN {0:'ON';'ON';'OFF'}";

        #endregion

        #region " FRONT TERMINALS SELECTED "

        /// <summary> Gets or sets the Front Terminals Selected query command. </summary>
        /// <value> The Front Terminals Selected query command. </value>
        protected override string FrontTerminalsSelectedQueryCommand { get; set; } = ":SYST:FRSW?";

        /// <summary> Gets or sets the Front Terminals Selected command Format. </summary>
        /// <remarks> SCPI: ":SYST:FRSW {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The Front Terminals Selected query command. </value>
        protected override string FrontTerminalsSelectedCommandFormat { get; set; } = string.Empty;

        #endregion

        #region " OPTIONS "

        /// <summary> Gets or sets the option query command. </summary>
        /// <value> The option query command. </value>
        protected override string OptionQueryCommand { get; set; } = "*OPT?";

        #endregion

        #region " SCAN CARD INSTALLED "

        /// <summary>
        /// Gets or sets a list of names of the scan cards. The list is empty of the instrument does not
        /// support scan cards.
        /// </summary>
        /// <value> A list of names of the scan cards. </value>
        protected override IList<string> ScanCardNames { get; set; } = new string[] { "2000-SCAN", "2001-SCAN" };

        #endregion

    }
}
