
namespace isr.VI.K2002
{
    /// <summary> Defines a Measure Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class MeasureSubsystem : MeasureSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MeasureSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            // reading elements are added (reading is initialized) when the format system is reset.
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefineFunctionModeReadWrites( "\"{0}\"", "'{0}'" );
        }

        #endregion

        #region " I PRESETTABLE "

        #endregion

        #region " COMMAND SYNTAX "

        #region " READ, FETCH "

        /// <summary> Gets or sets the fetch command. </summary>
        /// <value> The fetch command. </value>
        protected override string FetchCommand { get; set; } = ":FETCH?";

        /// <summary> Gets or sets the read command. </summary>
        /// <value> The read command. </value>
        protected override string ReadCommand { get; set; } = ":READ?";

        /// <summary> Gets or sets The Measure query command. </summary>
        /// <value> The Measure query command. </value>
        protected override string MeasureQueryCommand { get; set; } = ":READ?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #endregion

        #region " READ  FETCH "

        /// <summary> Fetches the data. </summary>
        /// <remarks>
        /// Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
        /// there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
        /// the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
        /// Buffer. Thus, subsequent executions of FETCh? acquire the same data.
        /// </remarks>
        /// <returns> The reading. </returns>
        public override string FetchReading()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return base.FetchReading();
        }

        /// <summary> Fetches the data. </summary>
        /// <remarks>
        /// Issues the 'FETCH?' query, which reads data stored in the Sample Buffer. If, for example,
        /// there are 20 data arrays stored in the Sample Buffer, then all 20 data arrays will be sent to
        /// the computer when 'FETCh?' is executed. Note that FETCh? does not affect data in the Sample
        /// Buffer. Thus, subsequent executions of FETCh? acquire the same data.
        /// </remarks>
        /// <returns> A Double? </returns>
        public override double? Fetch()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return base.Fetch();
        }

        /// <summary> Initiates an operation and then fetches the data. </summary>
        /// <remarks>
        /// Issues the 'READ?' query, which performs a trigger initiation and then a
        /// <see cref="M:isr.VI.MeasureSubsystemBase.Fetch">FETCh? </see>
        /// The initiate triggers a new measurement cycle which puts new data in the Sample Buffer. Fetch
        /// reads that new data. The
        /// <see cref="M:isr.VI.MeasureCurrentSubsystemBase.Measure">Measure</see> command places the
        /// instrument in a “one-shot” mode and then performs a read.
        /// </remarks>
        /// <returns> A Double? </returns>
        public override double? Read()
        {
            this.Session.MakeEmulatedReplyIfEmpty( this.ReadingAmounts.PrimaryReading.Generator.Value.ToString() );
            return base.Read();
        }

        #endregion

    }
}
