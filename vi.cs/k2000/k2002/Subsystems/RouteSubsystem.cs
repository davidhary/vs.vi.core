using System;
using System.Diagnostics;

namespace isr.VI.K2002
{

    /// <summary> Defines a Route Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class RouteSubsystem : RouteSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        /// <see cref="T:isr.VI.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        public RouteSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " CLOSED CHANNEL "

        /// <summary> Gets or sets the closed Channel query command. </summary>
        /// <remarks> Requires either 2000-SCAN or 2001-SCAN cards. </remarks>
        /// <value> The closed Channel query command. </value>
        protected override string ClosedChannelQueryCommand { get; set; } = ":ROUT:CLOS:STAT?";

        /// <summary> Gets or sets the closed Channel command format. </summary>
        /// <value> The closed Channel command format. </value>
        protected override string ClosedChannelCommandFormat { get; set; } = ":ROUT:CLOS {0}";

        /// <summary> Gets or sets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        protected override string OpenChannelCommandFormat { get; set; } = ":ROUT:OPEN {0}";

        #endregion

        #region " CLOSED CHANNELS "

        /// <summary> Gets the closed channels query command. </summary>
        /// <value> The closed channels query command. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string ClosedChannelsQueryCommand { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets the closed channels command format. </summary>
        /// <value> The closed channels command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string ClosedChannelsCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets the open channels command format. </summary>
        /// <value> The open channels command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string OpenChannelsCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " CHANNELS "

        /// <summary> Gets the recall channel pattern command format. </summary>
        /// <value> The recall channel pattern command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string RecallChannelPatternCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets the save channel pattern command format. </summary>
        /// <value> The save channel pattern command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string SaveChannelPatternCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets or sets the open channels command. </summary>
        /// <value> The open channels command. </value>
        protected override string OpenChannelsCommand { get; set; } = ":ROUT:OPEN:ALL";


        #endregion

        #region " SCAN LIST "

        /// <summary> Gets or sets the scan list command query. </summary>
        /// <value> The scan list query command. </value>
        protected override string ScanListQueryCommand { get; set; } = ":ROUT:SCAN?";

        /// <summary> Gets or sets the scan list command format. </summary>
        /// <value> The scan list command format. </value>
        protected override string ScanListCommandFormat { get; set; } = ":ROUT:SCAN {0}";

        #endregion

        #region " SCAN LIST FUNCTION "

        /// <summary> Gets or sets the Scan List Function command format. </summary>
        /// <value> The Scan List Function command format. </value>
        protected override string ScanListFunctionCommandFormat { get; set; } = ":ROUT:SCAN:FUNC {0},{1}";

        #endregion

        #region " SELECT SCAN LIST "

        /// <summary> Gets or sets the Selected Scan List Type command query. </summary>
        /// <value> The Selected Scan List Type query command. </value>
        protected override string SelectedScanListTypeQueryCommand { get; set; } = ":ROUT:SCAN:LSEL?";

        /// <summary> Gets or sets the Selected Scan List Type command format. </summary>
        /// <value> The Selected Scan List Type command format. </value>
        protected override string SelectedScanListTypeCommandFormat { get; set; } = ":ROUT:SCAN:LSEL {0}";

        #endregion

        #region " SLOT CARD TYPE "

        /// <summary> Gets the slot card type command format. </summary>
        /// <value> The slot card type command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string SlotCardTypeCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets the slot card type query command format. </summary>
        /// <value> The slot card type query command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string SlotCardTypeQueryCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " SLOT CARD SETTLING TIME "

        /// <summary> Gets the slot card settling time command format. </summary>
        /// <value> The slot card settling time command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string SlotCardSettlingTimeCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        /// <summary> Gets the slot card settling time query command format. </summary>
        /// <value> The slot card settling time query command format. </value>
        [Obsolete( "Not supported for the K2002 Multi Meters." )]
#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        protected override string SlotCardSettlingTimeQueryCommandFormat { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " TERMINAL MODE "

        /// <summary> Gets or sets the terminals mode query command. </summary>
        /// <value> The terminals mode command. </value>
        protected override string TerminalsModeQueryCommand { get; set; } = ":ROUT:TERM?";

        /// <summary> Gets or sets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        protected override string TerminalsModeCommandFormat { get; set; } = string.Empty; // read only; ":ROUT:TERM {0}"

        #endregion

        #region " SCAN CARD TEST FUNCTIONS "

        /// <summary> Clears the trace buffer. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="device"> The device. </param>
        public static void ClearBuffer( K2002Device device )
        {
            _ = device.TraceSubsystem.ClearBuffer( FeedControls.Next );
        }

        /// <summary> Clears the trace buffer. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="session"> The session. </param>
        public static void ClearBuffer( Pith.SessionBase session )
        {
            _ = session.WriteLine( "TRAC:CLE" );             // Clear the internal data buffer
            _ = session.WriteLine( "TRAC:FEED:CONT NEXT" );  // Set the buffer control mode
        }

        /// <summary> Builds four wire resistance scan help message. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <returns> A String. </returns>
        public static string BuildFourWireResistanceScanHelpMessage()
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( "Measure channels pairs (1:6, 2:7, 3:8) w/ sense subsystem 4W function." );
            _ = builder.AppendLine( "Saving values to the buffer and reading back when done." );
            _ = builder.AppendLine();
            return builder.ToString();
        }

        /// <summary>
        /// Configures four wire resistance scan using virtual instrument library functions.
        /// </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="device">        The device. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="timerInterval"> The timer interval. </param>
        /// <returns> A String. </returns>
        public static string ConfigureFourWireResistanceScan( K2002Device device, ArmSources triggerSource, int triggerCount, TimeSpan timerInterval )
        {
            return ConfigureFourWireResistanceScan( device, "(@1:3)", 3, triggerSource, triggerCount, timerInterval );
        }

        /// <summary>
        /// Configures four wire resistance scan using virtual instrument library functions.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="device">        The device. </param>
        /// <param name="scanList">      List of scans. </param>
        /// <param name="sampleCount">   Number of samples. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="timerInterval"> The timer interval. </param>
        /// <returns> A String. </returns>
        public static string ConfigureFourWireResistanceScan( K2002Device device, string scanList, int sampleCount, ArmSources triggerSource, int triggerCount, TimeSpan timerInterval )
        {
            string result = string.Empty;
            device.ResetKnownState();
            // this is required for getting the correct function mode when fetching buffers.
            _ = device.SenseSubsystem.ApplyFunctionMode( SenseFunctionModes.ResistanceFourWire );
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            _ = device.RouteSubsystem.ApplySelectedScanListType( ScanListType.None );
            _ = device.RouteSubsystem.ApplyScanListFunction( "(@1:10)", SenseFunctionModes.None, device.SenseSubsystem.FunctionModeReadWrites );
            _ = device.RouteSubsystem.ApplyScanListFunction( scanList, SenseFunctionModes.ResistanceFourWire, device.SenseSubsystem.FunctionModeReadWrites );
            _ = device.TriggerSubsystem.ApplyTriggerCount( sampleCount );
            _ = device.TriggerSubsystem.ApplyTriggerSource( TriggerSources.Immediate );
            _ = device.ArmLayer2Subsystem.ApplyArmCount( triggerCount );
            _ = device.ArmLayer2Subsystem.ApplyArmSource( triggerSource );
            if ( ArmSources.Timer == triggerSource )
            {
                _ = device.ArmLayer2Subsystem.ApplyTimerTimeSpan( timerInterval );
            }

            device.TraceSubsystem.ClearBuffer();
            _ = device.TraceSubsystem.ApplyPointsCount( sampleCount );
            _ = device.TraceSubsystem.ApplyFeedSource( FeedSources.Sense );
            _ = device.TraceSubsystem.ApplyFeedControl( FeedControls.Next );
            _ = device.RouteSubsystem.ApplySelectedScanListType( ScanListType.Internal );
            return result;
        }

        /// <summary> Configures four wire resistance scan using SCPI commands. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="session">       The session. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="timerInterval"> The timer interval. </param>
        /// <returns> A String. </returns>
        public static string ConfigureFourWireResistanceScan( Pith.SessionBase session, string triggerSource, int triggerCount, TimeSpan timerInterval )
        {
            _ = session.WriteLine( "*RST" );
            _ = session.WriteLine( "INIT:CONT OFF" );        // Disable continuous triggering
            _ = session.WriteLine( "ROUT:SCAN:LSEL NONE" );  // Disable scanning
            _ = session.WriteLine( "ROUT:SCAN:FUNC (@1:10), 'NONE'" ); // Turn off measurements on channels 4, 5, 9, and 10;
            _ = session.WriteLine( "ROUT:SCAN:FUNC (@1:3), 'FRES'" ); // Configure scan card paired channels 1-3 for 4W resistance measurement;
                                                                      // session.WriteLine("ROUT:SCAN:FUNC (@4:10), 'NONE'") ' Turn off measurements on channels 4, 5, 9, and 10;

            _ = session.WriteLine( "TRIG:COUN 3" );          // Set to capture 3 samples per trigger event
            _ = session.WriteLine( "TRIG:SOUR IMM" );        // Trigger immediately after passing the arm layer
            _ = session.WriteLine( $"ARM:LAY2:COUN {triggerCount}" );    // Set the number of triggers
            _ = session.WriteLine( $"ARM:LAY2:SOUR {triggerSource}" );   // Set the trigger source
            if ( triggerSource.StartsWith( "TIM", StringComparison.OrdinalIgnoreCase ) )
            {
                _ = session.WriteLine( $"ARM:LAY2:TIM {0.001d * timerInterval.TotalMilliseconds}" );
            }

            _ = session.WriteLine( "TRAC:CLE" );             // Clear the internal data buffer
            _ = session.WriteLine( "TRAC:POIN 3" );          // Size the buffer to capture one reading per sample per trigger
            _ = session.WriteLine( "TRAC:FEED SENS" );       // Set the source of readings
            _ = session.WriteLine( "TRAC:FEED:CONT NEXT" );  // Set the buffer control mode
            _ = session.WriteLine( "ROUT:SCAN:LSEL INT" );   // Enable scanning
            return string.Empty;
        }

        /// <summary>
        /// Initiates four wire resistance scan using virtual instrument library functions.
        /// </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="device">            The device. </param>
        /// <param name="bufferFullBitmask"> The buffer full bitmask. </param>
        public static void InitiateFourWireResistanceScan( K2002Device device, int bufferFullBitmask )
        {
            device.StatusSubsystem.PresetKnownState();
            _ = device.StatusSubsystem.ApplyMeasurementEventEnableBitmask( bufferFullBitmask );
            device.TriggerSubsystem.Initiate();
        }

        /// <summary> Initiates a four wire resistance scan using SCPI Commands. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="session">           The session. </param>
        /// <param name="bufferFullBitmask"> The buffer full bitmask. </param>
        public static void InitiateFourWireResistanceScan( Pith.SessionBase session, int bufferFullBitmask )
        {
            _ = session.WriteLine( "STAT:PRES;*CLS" );
            int bitmask = bufferFullBitmask; // Set measurement event bitmask to buffer available
            _ = session.WriteLine( $"STAT:MEAS:ENAB {bitmask}" );
            _ = session.WriteLine( "*SRE 1" );
            _ = session.WriteLine( "INIT" );                           // Trigger the scanInitiates that trigger plan
        }

        /// <summary>
        /// Fetches four wire resistance scan using virtual instrument library functions.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="device"> The device. </param>
        /// <returns> The (Status As String, Reading As String) </returns>
        public static (string Status, string Value, TimeSpan Elapsed) FetchFourWireResistanceScan( K2002Device device )
        {
            var timeout = TimeSpan.FromSeconds( 10d );
            var sw = Stopwatch.StartNew();
            bool timedOut = sw.Elapsed > timeout;
            var measurementCondition = default( int );
            while ( !timedOut && !device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn( measurementCondition, MeasurementEventBitmaskKey.BufferFull ) )
            {
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 10d ) );
                measurementCondition = device.StatusSubsystem.QueryMeasurementEventStatus().GetValueOrDefault( 0 );
                timedOut = sw.Elapsed > timeout;
            }

            sw.Stop();
            string status = device.StatusSubsystem.QueryMeasurementEventStatus().ToString();
            _ = device.FormatSubsystem.ApplyElements( ReadingElementTypes.Reading | ReadingElementTypes.Units );
            return (status, device.TraceSubsystem.QueryData(), sw.Elapsed);
        }

        /// <summary> Configure four wire resistance scan using SCPI Commands. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="session">           The session. </param>
        /// <param name="bufferFullBitmask"> The buffer full bitmask. </param>
        /// <returns> The (Status As String, Reading As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static (string Status, string Reading, TimeSpan Elapsed) FetchFourWireResistanceScan( Pith.SessionBase session, int bufferFullBitmask )
        {
            // wait for the buffer to fill or some other event that shows up in the status register>>>
            var srq = session.ReadStatusRegister();

            // wait for service request
            var expectedRequest = Pith.ServiceRequests.RequestingService;
            var timeout = TimeSpan.FromSeconds( 10d );
            var sw = Stopwatch.StartNew();
            bool timedOut = sw.Elapsed > timeout;
            while ( expectedRequest != (srq & expectedRequest) && !timedOut )
            {
                srq = session.ReadStatusRegister( TimeSpan.FromMilliseconds( 10d ) );
                timedOut = sw.Elapsed > timeout;
            }

            _ = session.WriteLine( "STAT:MEAS?" );                     // get measurement status
            string status = session.ReadFreeLineTrimEnd();
            _ = session.WriteLine( "TRAC:DATA?" );                     // Extract the data from the buffer
            string reading = session.ReadFreeLineTrimEnd();
            sw.Stop();
            return (status, reading, sw.Elapsed);
        }

        /// <summary> Builds four wire resistance timer scan help message. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <returns> A String. </returns>
        public static string BuildFourWireResistanceTimerScanHelpMessage()
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( "Measures measure 4W resistance on channels 1, 2 and 3." );
            _ = builder.AppendLine( "The meter takes ten sets Of readings, with each set spaced 1 seconds apart." );
            _ = builder.AppendLine( "Each of the three readings in each group taken as fast as possible. The" );
            _ = builder.AppendLine( "The readings are stored in the buffer." );
            _ = builder.AppendLine( "SRQ is asserted SRQ When the buffer Is full." );
            _ = builder.AppendLine( "The program waits For the SRQ, then reads the readings from the buffer. " );
            return builder.ToString();
        }

        #endregion

    }
}
