using System;

namespace isr.VI.K2002
{

    /// <summary> Defines a Trigger Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TriggerSubsystem : TriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TriggerSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.SupportedTriggerSources = TriggerSources.Bus | TriggerSources.External | TriggerSources.Hold | TriggerSources.Immediate | TriggerSources.Manual | TriggerSources.Timer | TriggerSources.TriggerLink;
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-08-06. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.MaximumTriggerCount = 99999;
            this.MaximumDelay = TimeSpan.FromSeconds( 999999.999d );
        }

        #endregion

        #region " ABORT / INIT COMMANDS "

        /// <summary> Gets or sets the Abort command. </summary>
        /// <value> The Abort command. </value>
        protected override string AbortCommand { get; set; } = ":ABOR";

        /// <summary> Gets or sets the initiate command. </summary>
        /// <value> The initiate command. </value>
        protected override string InitiateCommand { get; set; } = ":INIT";

        /// <summary> Gets or sets the clear command. </summary>
        /// <value> The clear command. </value>
        protected override string ClearCommand { get; set; } = string.Empty; // ":TRIG:CLE"

        #endregion

        #region " AUTO DELAY "

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledCommandFormat { get; set; } = string.Empty; // ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}"

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledQueryCommand { get; set; } = string.Empty; // ":TRIG:DEL:AUTO?"

        #endregion

        #region " CONTINUOUS "

        /// <summary> Gets or sets the Continuous enabled command Format. </summary>
        /// <value> The Continuous enabled query command. </value>
        protected override string ContinuousEnabledCommandFormat { get; set; } = ":INIT:CONT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Continuous enabled query command. </summary>
        /// <value> The Continuous enabled query command. </value>
        protected override string ContinuousEnabledQueryCommand { get; set; } = ":INIT:CONT?";

        #endregion

        #region " TRIGGER COUNT "

        /// <summary> Gets or sets trigger count query command. </summary>
        /// <value> The trigger count query command. </value>
        protected override string TriggerCountQueryCommand { get; set; } = ":TRIG:COUN?";

        /// <summary> Gets or sets trigger count command format. </summary>
        /// <value> The trigger count command format. </value>
        protected override string TriggerCountCommandFormat { get; set; } = ":TRIG:COUN {0}";

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = @":TRIG:DEL {0:s\.FFFFFFF}";

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = ":TRIG:DEL?";

        #endregion

        #region " DIRECTION (BYPASS) "

        /// <summary> Gets or sets the Trigger Direction query command. </summary>
        /// <value> The Trigger Direction query command. </value>
        protected override string TriggerLayerBypassModeQueryCommand { get; set; } = ":TRIG:TCON:DIR?";

        /// <summary> Gets or sets the Trigger Direction command format. </summary>
        /// <value> The Trigger Direction command format. </value>
        protected override string TriggerLayerBypassModeCommandFormat { get; set; } = ":TRIG:TCON:DIR {0}";

        #endregion

        #region " INPUT LINE NUMBER--TRIGGER LINK ASYNC "

        /// <summary> Gets or sets the Input Line Number command format. </summary>
        /// <value> The Input Line Number command format. </value>
        protected override string InputLineNumberCommandFormat { get; set; } = ":TRIG:TCON:ASYN:ILIN {0}";

        /// <summary> Gets or sets the Input Line Number query command. </summary>
        /// <value> The Input Line Number query command. </value>
        protected override string InputLineNumberQueryCommand { get; set; } = ":TRIG:TCON:ASYN:ILIN?";

        #endregion

        #region " OUTPUT LINE NUMBER--TRIGGER LINK ASYNC "

        /// <summary> Gets or sets the Output Line Number command format. </summary>
        /// <value> The Output Line Number command format. </value>
        protected override string OutputLineNumberCommandFormat { get; set; } = ":TRIG:TCON:ASYN:OLIN {0}";

        /// <summary> Gets or sets the Output Line Number query command. </summary>
        /// <value> The Output Line Number query command. </value>
        protected override string OutputLineNumberQueryCommand { get; set; } = ":TRIG:TCON:ASYN:OLIN?";

        #endregion

        #region " SOURCE "

        /// <summary> Gets or sets the Trigger Source query command. </summary>
        /// <value> The Trigger Source query command. </value>
        protected override string TriggerSourceQueryCommand { get; set; } = ":TRIG:SOUR?";

        /// <summary> Gets or sets the Trigger Source command format. </summary>
        /// <value> The Trigger Source command format. </value>
        protected override string TriggerSourceCommandFormat { get; set; } = ":TRIG:SOUR {0}";

        #endregion

        #region " TIMER TIME SPAN "

        /// <summary> Gets or sets the Timer Interval command format. </summary>
        /// <value> The query command format. </value>
        protected override string TimerIntervalCommandFormat { get; set; } = @":TRIG:TIM {0:s\.FFFFFFF}";

        /// <summary>
        /// Gets or sets the Timer Interval format for converting the query to time span.
        /// </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the Timer Interval query command. </summary>
        /// <value> The Timer Interval query command. </value>
        protected override string TimerIntervalQueryCommand { get; set; } = ":TRIG:TIM?";

        #endregion

        #region " TRIGGER STATE "

        /// <summary> Queries the Trigger State. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// The <see cref="P:isr.VI.TriggerSubsystemBase.TriggerState">Trigger State</see> or none if
        /// unknown.
        /// </returns>
        public override TriggerState? QueryTriggerState()
        {
            this.StatusSubsystem.OperationEventsBitmasks.Status = this.StatusSubsystem.QueryOperationEventCondition().GetValueOrDefault( 0 );
            switch ( true )
            {
                case object _ when this.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Arming ):
                    {
                        this.TriggerState = VI.TriggerState.Waiting;
                        break;
                    }

                case object _ when this.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle ):
                    {
                        if ( this.IsTriggerStateActive() )
                        {
                            // the first detection go to aborting
                            this.TriggerState = VI.TriggerState.Aborting;
                        }
                        else if ( this.IsTriggerStateAborting() )
                        {
                            this.TriggerState = VI.TriggerState.Aborted;
                        }
                        else if ( ( int? ) this.TriggerState == ( int? ) VI.TriggerState.Aborted == true )
                        {
                            this.TriggerState = VI.TriggerState.Idle;
                        }

                        this.TriggerState = VI.TriggerState.Idle;
                        break;
                    }

                case object _ when this.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Measuring ):
                    {
                        this.TriggerState = VI.TriggerState.Running;
                        break;
                    }

                case object _ when this.StatusSubsystem.OperationEventsBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Triggering ):
                    {
                        this.TriggerState = VI.TriggerState.Running;
                        break;
                    }

                default:
                    {
                        if ( this.IsTriggerStateIdle() )
                        {
                            // if state was idle and no event was detected, state is running or waiting.
                            this.TriggerState = VI.TriggerState.Running;
                        }

                        break;
                    }
            }

            return this.TriggerState;
        }
        #endregion

        #region " TRIGGER SUBSYSTEM TEST FUNCTIONS "

        /// <summary> Builds external scan help message. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="triggerSubsystem"> The trigger subsystem. </param>
        /// <returns> A String. </returns>
        public static string BuildExternalScanHelpMessage( TriggerSubsystemBase triggerSubsystem )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.AppendLine( "Configure external scan taking a reading on each trigger input." );
            _ = builder.AppendLine( $"Trigger: Count = {triggerSubsystem.TriggerCount}; Source: {triggerSubsystem.TriggerSource}" );
            _ = builder.AppendLine( "The meter takes ten sets Of readings, with each set spaced 1 seconds apart." );
            _ = builder.AppendLine( "Each of the three readings in each group taken as fast as possible. The" );
            _ = builder.AppendLine( "The readings are stored in the buffer." );
            _ = builder.AppendLine( "SRQ is asserted SRQ When the buffer Is full." );
            _ = builder.AppendLine( "The program waits For the SRQ, then reads the readings from the buffer. " );
            return builder.ToString();
        }

        /// <summary> Configure external scan. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="device">             The device. </param>
        /// <param name="triggerCount">       Number of triggers. </param>
        /// <param name="triggerLayerSource"> The trigger layer source. </param>
        /// <param name="armLayer1Source">    The arm layer 1 source. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        public static (bool Success, string Details) ConfigureExternalScan( K2002Device device, int triggerCount, TriggerSources triggerLayerSource, ArmSources armLayer1Source )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( device is null )
            {
                result = (false, $"{nameof( device )} is nothing");
            }

            if ( !result.Success )
                return result;
            // string activity = $"{device.ResourceNameCaption} configuring external scan";
            // device.ResetKnownState()
            // device.ClearExecutionState()

            int count = triggerCount;
            // TO_DO: Add service request handler to trace when done. 
            // enable service requests
            // device.Session.EnableServiceRequest()
            // device.StatusSubsystem.EnableServiceRequest(device.Session.DefaultServiceRequestEnableBitmask)
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.TriggerSubsystem.Abort();
            _ = device.SystemSubsystem.ApplyAutoZeroEnabled( true );
            _ = device.FormatSubsystem.ApplyElements( ReadingElementTypes.Reading );
            _ = device.SenseSubsystem.ApplyFunctionMode( SenseFunctionModes.ResistanceFourWire );
            _ = device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled( false );
            if ( count <= 1 )
            {
                // setting the feed source to none causes -109,"Missing parameter" error
                // device.TraceSubsystem.WriteFeedSource(Scpi.FeedSource.None)
                _ = device.TriggerSubsystem.ApplyTriggerSource( triggerLayerSource );
                _ = device.TriggerSubsystem.ApplyTriggerCount( count );
                if ( device.TriggerSubsystem.TriggerSource.Value == TriggerSources.External )
                {
                    device.Session.StatusPrompt = "Ready: Initiate meter and then scanner";
                }
                else if ( device.TriggerSubsystem.TriggerSource.Value == TriggerSources.Immediate )
                {
                    device.Session.StatusPrompt = "Ready: Initiate meter to take a reading";
                }
                else if ( device.TriggerSubsystem.TriggerSource.Value == TriggerSources.Bus )
                {
                    device.Session.StatusPrompt = "Ready: Click the Trigger button to take a reading";
                }
                else
                {
                    result = (false, $"Invalid trigger source {device.TriggerSubsystem.TriggerSource.Value}");
                }
            }
            else
            {
                // use external if the meter waits for the scanner or immediate if the scanner waits for the meter.
                _ = device.ArmLayer1Subsystem.ApplyArmSource( armLayer1Source );
                _ = device.ArmLayer1Subsystem.ApplyArmCount( 1 );
                _ = device.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
                _ = device.ArmLayer2Subsystem.ApplyArmCount( 1 );
                _ = device.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
                _ = device.TriggerSubsystem.ApplyTriggerSource( TriggerSources.External );
                _ = device.TriggerSubsystem.ApplyTriggerCount( count );
                _ = device.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
                _ = device.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Source );
                if ( count > 1 )
                {
                    _ = device.TraceSubsystem.WriteFeedSource( FeedSources.Sense );
                    // device.TraceSubsystem.ApplyPointsCount(count)
                    _ = device.Session.WriteLine( ":TRAC:POIN:AUTO 1" );
                    _ = device.TraceSubsystem.WriteFeedControl( FeedControls.Next );
                }
                else
                {
                    // This does not work. Getting device errors:
                    // -109,"Missing parameter"
                    // -221,"Settings conflict", ""
                    // -222,"Parameter data out of range", ""
                    // -102,"Syntax error", ""
                    // -109,"Missing parameter", ""
                    // device.Session.WriteLine(":TRAC:POIN:AUTO 1")
                    // device.TraceSubsystem.WriteFeedControl(Scpi.FeedControl.Never)
                    // device.TraceSubsystem.WriteFeedSource(Scpi.FeedSource.None)
                }

                if ( device.ArmLayer1Subsystem.ArmSource.Value == ArmSources.External )
                {
                    device.Session.StatusPrompt = "Ready: Initiate meter and then scanner";
                }
                else if ( device.ArmLayer1Subsystem.ArmSource.Value == ArmSources.Immediate )
                {
                    device.Session.StatusPrompt = "Ready: Initiate scanner and then meter";
                }
                else
                {
                    result = (false, $"Invalid arms source {device.ArmLayer1Subsystem.ArmSource.Value}");
                }
            }

            return result;
        }

        /// <summary> Executes the external scan operation. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="session"> The session. </param>
        /// <returns> The (Status As String, Value As String) </returns>
        public static (string Status, string Value) ExecuteExternalScan( Pith.SessionBase session )
        {
            _ = session.WriteLine( "STAT:PRES;*CLS" );
            _ = session.WriteLine( "STAT:MEAS:ENAB 512" );
            _ = session.WriteLine( "*SRE 1" );
            _ = session.WriteLine( "INIT" );                       // Trigger the scan
                                                                   // ' wait for the buffer to fill or some other event that shows up in the status register>>>
            var srq = session.ReadStatusRegister();
            // ' wait for service request
            while ( Pith.ServiceRequests.RequestingService != (srq & Pith.ServiceRequests.RequestingService) )
                srq = session.ReadStatusRegister( TimeSpan.FromMilliseconds( 10d ) );
            _ = session.WriteLine( "STAT:MEAS?" );                 // get measurement status
            string status = session.ReadFreeLineTrimEnd();
            _ = session.WriteLine( "FORM:ELEM READ,UNIT" );        // set reading format.
            _ = session.WriteLine( "TRAC:DATA?" );                 // Extract the data from the buffer
            return (status, session.ReadFreeLine());
        }

        #endregion

    }
}
