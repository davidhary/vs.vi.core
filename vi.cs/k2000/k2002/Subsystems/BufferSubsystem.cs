
namespace isr.VI.K2002
{

    /// <summary> Buffer subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class BufferSubsystem : BufferSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="BufferSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        /// <see cref="T:isr.VI.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        public BufferSubsystem( StatusSubsystemBase statusSubsystem ) : this( DefaultBuffer1Name, statusSubsystem )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="bufferName">      Name of the buffer. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public BufferSubsystem( string bufferName, StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.BufferName = bufferName;
        }

        #endregion

        #region " BUFFER NAME "

        /// <summary> The default buffer 1 name. </summary>
        public const string DefaultBuffer1Name = "defbuffer1";

        /// <summary> The default buffer 2 name. </summary>
        public const string DefaultBuffer2Name = "defbuffer2";

        /// <summary> Name of the buffer. </summary>
        private string _BufferName;

        /// <summary> Gets or sets the name of the buffer. </summary>
        /// <value> The name of the buffer. </value>
        public string BufferName
        {
            get => this._BufferName;

            set {
                if ( !string.Equals( value, this.BufferName ) )
                {
                    this._BufferName = string.IsNullOrWhiteSpace( value ) ? DefaultBuffer1Name : value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " MEMORY OPTION "

        /// <summary> The buffer size standard full. </summary>
        protected const int BufferSizeStandardFull = 404;

        /// <summary> The buffer size standard compact. </summary>
        protected const int BufferSizeStandardCompact = 2027;

        /// <summary> The buffer size memory 1 full. </summary>
        protected const int BufferSizeMemory1Full = 1381;

        /// <summary> The buffer size memory 1 compact. </summary>
        protected const int BufferSizeMemory1Compact = 6909;

        /// <summary> The buffer size memory 2 full. </summary>
        protected const int BufferSizeMemory2Full = 5980;

        /// <summary> The buffer size memory 2 compact. </summary>
        protected const int BufferSizeMemory2Compact = 29908;

        /// <summary> The memory option. </summary>
        private int? _MemoryOption;

        /// <summary> Gets or sets the memory Option. </summary>
        /// <value> The memory Option. </value>
        public int? MemoryOption
        {
            get => this._MemoryOption;

            set {
                if ( !Equals( this.MemoryOption, value ) )
                {
                    this._MemoryOption = value;
                    switch ( value )
                    {
                        case 0:
                            {
                                this.MaximumCapacity = BufferSizeStandardFull;
                                break;
                            }

                        case 1:
                            {
                                this.MaximumCapacity = BufferSizeMemory1Full;
                                break;
                            }

                        case 2:
                            {
                                this.MaximumCapacity = BufferSizeMemory2Full;
                                break;
                            }

                        default:
                            {
                                this.MaximumCapacity = BufferSizeStandardFull;
                                break;
                            }
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the Clear Buffer command. </summary>
        /// <value> The ClearBuffer command. </value>
        protected override string ClearBufferCommand { get; set; } = ":TRAC:CLE";

        /// <summary> Gets or sets the points count query command. </summary>
        /// <value> The points count query command. </value>
        protected override string CapacityQueryCommand { get; set; } = ":TRAC:POIN?";

        /// <summary> Gets or sets the points count command format. </summary>
        /// <remarks> SCPI: ":TRAC:POIN:COUN {0}". </remarks>
        /// <value> The points count query command format. </value>
        protected override string CapacityCommandFormat { get; set; } = ":TRAC:POIN {0}";

        /// <summary> Gets or sets the ActualPoint count query command. </summary>
        /// <value> The ActualPoint count query command. </value>
        protected override string ActualPointCountQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets The First Point Number (1-based) query command. </summary>
        /// <value> The First Point Number query command. </value>
        protected override string FirstPointNumberQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets The Last Point Number (1-based) query command. </summary>
        /// <value> The Last Point Number query command. </value>
        protected override string LastPointNumberQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the Buffer Free query command. </summary>
        /// <value> The buffer free query command. </value>
        protected override string BufferFreePointCountQueryCommand { get; set; } = ":TRAC:FREE?";

        /// <summary> Gets or sets the automatic Delay enabled query command. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected override string FillOnceEnabledQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the automatic Delay enabled command Format. </summary>
        /// <value> The automatic Delay enabled query command. </value>
        protected override string FillOnceEnabledCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the buffer read command format. </summary>
        /// <value> The buffer read command format. </value>
        public override string BufferReadCommandFormat { get; set; } = ":TRACE:DATA?";

        /// <summary> Gets or sets the data query command. </summary>
        /// <value> The points count query command. </value>
        protected override string DataQueryCommand { get; set; } = ":TRACE:DATA?";

        #endregion


    }
}
