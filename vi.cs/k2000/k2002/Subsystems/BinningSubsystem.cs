using System;

namespace isr.VI.K2002
{

    /// <summary> Defines a SCPI Binning Subsystem (CALC2 or CALC3). </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class BinningSubsystem : BinningSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="BinningSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        /// <see cref="T:isr.VI.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        public BinningSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.BinningStrobeDuration = TimeSpan.FromMilliseconds( 100d );
            this.BinningStrobeEnabled = false;
            this.PassSource = 0;
            this.Limit1AutoClear = true;
            this.Limit1Enabled = false;
            this.Limit1LowerLevel = -1;
            this.Limit1LowerSource = 0;
            this.Limit1UpperLevel = 1;
            this.Limit1UpperSource = 0;
            this.Limit2AutoClear = true;
            this.Limit2Enabled = false;
            this.Limit2LowerLevel = -1;
            this.Limit2LowerSource = 0;
            this.Limit2UpperLevel = 1;
            this.Limit2UpperSource = 0;
        }

        #endregion

        #region " COMMANDS "

        /// <summary> Gets or sets the Immediate command. </summary>
        /// <value> The Immediate command. </value>
        protected override string ImmediateCommand { get; set; } = ":CALC3:IMM";

        /// <summary> Gets or sets the limit 1 clear command. </summary>
        /// <value> The limit 1 clear command. </value>
        protected override string Limit1ClearCommand { get; set; } = ":CALC3:LIM1:CLE";

        /// <summary> Gets or sets the limit 2 clear command. </summary>
        /// <value> The limit 2 clear command. </value>
        protected override string Limit2ClearCommand { get; set; } = ":CALC3:LIM2:CLE";

        #endregion

        #region " BINNING ENABLED "

        /// <summary> Gets or sets the Binning Strobe Enabled status command Format. </summary>
        /// <value> The Binning Strobe Enabled status query command. </value>
        protected override string BinningStrobeEnabledCommandFormat { get; set; } = ":CALC3:BSTR:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Binning Strobe Enabled status query command. </summary>
        /// <value> The Binning Strobe Enabled status query command. </value>
        protected override string BinningStrobeEnabledQueryCommand { get; set; } = ":CALC3:BSTR:STAT?";

        #endregion

        #region " PASS SOURCE "

        /// <summary> Gets or sets The Pass Source command format. </summary>
        /// <value> The Pass Source command format. </value>
        protected override string PassSourceCommandFormat { get; set; } = ":CALC3:PASS:SOUR {0}";

        /// <summary> Gets or sets The Pass Source query command. </summary>
        /// <value> The Pass Source query command. </value>
        protected override string PassSourceQueryCommand { get; set; } = ":CALC3:PASS:SOUR?";

        #endregion

        #region " LIMITs FAILED "

        /// <summary> Gets or sets the Limits Failed query command. </summary>
        /// <value> The Limits Failed query command. </value>
        protected override string LimitsFailedQueryCommand { get; set; } = ":CALC3:CLIM:FAIL?";

        #endregion

        #region " LIMIT 1 "

        #region " LIMIT1 AUTO CLEAR "

        /// <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
        /// <value> The Limit1 AutoClear query command. </value>
        protected override string Limit1AutoClearCommandFormat { get; set; } = ":CALC3:LIM1:CLE:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
        /// <value> The Limit1 AutoClear query command. </value>
        protected override string Limit1AutoClearQueryCommand { get; set; } = ":CALC3:LIM1:CLE:AUTO?";

        #endregion

        #region " LIMIT1 ENABLED "

        /// <summary> Gets or sets the Limit1 enabled command Format. </summary>
        /// <value> The Limit1 enabled query command. </value>
        protected override string Limit1EnabledCommandFormat { get; set; } = ":CALC3:LIM1:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit1 enabled query command. </summary>
        /// <value> The Limit1 enabled query command. </value>
        protected override string Limit1EnabledQueryCommand { get; set; } = ":CALC3:LIM1:STAT?";

        #endregion

        #region " LIMIT1 FAILED "

        /// <summary> Gets or sets the Limit1 Failed query command. </summary>
        /// <value> The Limit1 Failed query command. </value>
        protected override string Limit1FailedQueryCommand { get; set; } = ":CALC3:LIM1:FAIL?";

        #endregion

        #region " LIMIT1 LOWER LEVEL "

        /// <summary> Gets or sets the Limit1 Lower Level command format. </summary>
        /// <value> The Limit1LowerLevel command format. </value>
        protected override string Limit1LowerLevelCommandFormat { get; set; } = ":CALC3:LIM1:LOW {0}";

        /// <summary> Gets or sets the Limit1 Lower Level query command. </summary>
        /// <value> The Limit1LowerLevel query command. </value>
        protected override string Limit1LowerLevelQueryCommand { get; set; } = ":CALC3:LIM1:LOW?";

        #endregion

        #region " Limit1 UPPER LEVEL "

        /// <summary> Gets or sets the Limit1 Upper Level command format. </summary>
        /// <value> The Limit1UpperLevel command format. </value>
        protected override string Limit1UpperLevelCommandFormat { get; set; } = ":CALC3:LIM1:UPP {0}";

        /// <summary> Gets or sets the Limit1 Upper Level query command. </summary>
        /// <value> The Limit1UpperLevel query command. </value>
        protected override string Limit1UpperLevelQueryCommand { get; set; } = ":CALC3:LIM1:UPP?";

        #endregion

        #region " LIMIT1 LOWER SOURCE "

        /// <summary> Gets or sets the Limit1 Lower Source command format. </summary>
        /// <value> The Limit1LowerSource command format. </value>
        protected override string Limit1LowerSourceCommandFormat { get; set; } = ":CALC3:LIM1:LOW:SOUR {0}";

        /// <summary> Gets or sets the Limit1 Lower Source query command. </summary>
        /// <value> The Limit1LowerSource query command. </value>
        protected override string Limit1LowerSourceQueryCommand { get; set; } = ":CALC3:LIM1:LOW:SOUR?";

        #endregion

        #region " LIMIT1 UPPER SOURCE "

        /// <summary> Gets or sets the Limit1 Upper Source command format. </summary>
        /// <value> The Limit1UpperSource command format. </value>
        protected override string Limit1UpperSourceCommandFormat { get; set; } = ":CALC3:LIM1:UPP:SOUR {0}";

        /// <summary> Gets or sets the Limit1 Upper Source query command. </summary>
        /// <value> The Limit1UpperSource query command. </value>
        protected override string Limit1UpperSourceQueryCommand { get; set; } = ":CALC3:LIM1:UPP:SOUR?";

        #endregion

        #endregion

        #region " LIMIT 2 "

        #region " LIMIT2 AUTO CLEAR "

        /// <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
        /// <value> The Limit2 AutoClear query command. </value>
        protected override string Limit2AutoClearCommandFormat { get; set; } = ":CALC3:LIM2:CLE:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
        /// <value> The Limit2 AutoClear query command. </value>
        protected override string Limit2AutoClearQueryCommand { get; set; } = ":CALC3:LIM2:CLE:AUTO?";

        #endregion

        #region " LIMIT2 ENABLED "

        /// <summary> Gets or sets the Limit2 enabled command Format. </summary>
        /// <value> The Limit2 enabled query command. </value>
        protected override string Limit2EnabledCommandFormat { get; set; } = ":CALC3:LIM2:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit2 enabled query command. </summary>
        /// <value> The Limit2 enabled query command. </value>
        protected override string Limit2EnabledQueryCommand { get; set; } = ":CALC3:LIM2:STAT?";

        #endregion

        #region " LIMIT2 FAILED "

        /// <summary> Gets or sets the Limit2 Failed query command. </summary>
        /// <value> The Limit2 Failed query command. </value>
        protected override string Limit2FailedQueryCommand { get; set; } = ":CALC3:LIM2:FAIL?";

        #endregion

        #region " LIMIT2 LOWER LEVEL "

        /// <summary> Gets or sets the Limit2 Lower Level command format. </summary>
        /// <value> The Limit2LowerLevel command format. </value>
        protected override string Limit2LowerLevelCommandFormat { get; set; } = ":CALC3:LIM2:LOW {0}";

        /// <summary> Gets or sets the Limit2 Lower Level query command. </summary>
        /// <value> The Limit2LowerLevel query command. </value>
        protected override string Limit2LowerLevelQueryCommand { get; set; } = ":CALC3:LIM2:LOW?";

        #endregion

        #region " LIMIT2 UPPER LEVEL "

        /// <summary> Gets or sets the Limit2 Upper Level command format. </summary>
        /// <value> The Limit2UpperLevel command format. </value>
        protected override string Limit2UpperLevelCommandFormat { get; set; } = ":CALC3:LIM2:UPP {0}";

        /// <summary> Gets or sets the Limit2 Upper Level query command. </summary>
        /// <value> The Limit2UpperLevel query command. </value>
        protected override string Limit2UpperLevelQueryCommand { get; set; } = ":CALC3:LIM2:UPP?";

        #endregion

        #region " LIMIT2 LOWER SOURCE "

        /// <summary> Gets or sets the Limit2 Lower Source command format. </summary>
        /// <value> The Limit2LowerSource command format. </value>
        protected override string Limit2LowerSourceCommandFormat { get; set; } = ":CALC3:LIM2:LOW:SOUR {0}";

        /// <summary> Gets or sets the Limit2 Lower Source query command. </summary>
        /// <value> The Limit2LowerSource query command. </value>
        protected override string Limit2LowerSourceQueryCommand { get; set; } = ":CALC3:LIM2:LOW:SOUR?";

        #endregion

        #region " LIMIT2 UPPER SOURCE "

        /// <summary> Gets or sets the Limit2 Upper Source command format. </summary>
        /// <value> The Limit2UpperSource command format. </value>
        protected override string Limit2UpperSourceCommandFormat { get; set; } = ":CALC3:LIM2:UPP:SOUR {0}";

        /// <summary> Gets or sets the Limit2 Upper Source query command. </summary>
        /// <value> The Limit2UpperSource query command. </value>
        protected override string Limit2UpperSourceQueryCommand { get; set; } = ":CALC3:LIM2:UPP:SOUR?";

        #endregion

        #endregion

    }
}
