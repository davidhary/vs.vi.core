using System;
using System.Diagnostics;

using isr.Core;
using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2002
{

    /// <summary> Implements a Keithley 2002 instrument device. </summary>
    /// <remarks>
    /// An instrument is defined, for the purpose of this library, as a device with a front panel.
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class K2002Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="K2002Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K2002Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected K2002Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            if ( statusSubsystem is object )
            {
                statusSubsystem.ExpectedLanguage = Pith.Ieee488.Syntax.LanguageTsp;
                Properties.Settings.Default.PropertyChanged += this.HandleSettingsPropertyChanged;
            }

            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static K2002Device Create()
        {
            K2002Device device = null;
            try
            {
                device = new K2002Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static K2002Device Validated( K2002Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            // release unmanaged-only resources.
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( K2002Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindMeasureSubsystem( null );
                this.BindFormatSubsystem( null );
                this.BindRouteSubsystem( null );
                this.BindSenseSubsystem( null );
                this.BindSenseVoltageSubsystem( null );
                this.BindSenseCurrentSubsystem( null );
                this.BindSenseResistanceSubsystem( null );
                this.BindSenseResistanceFourWireSubsystem( null );
                this.BindTraceSubsystem( null );
                this.BindTriggerSubsystem( null );
                this.BindBinningSubsystem( null );
                this.BindArmLayer1Subsystem( null );
                this.BindArmLayer2Subsystem( null );
                this.BindDigitalOutputSubsystem( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                // better add before the format subsystem, which reset initializes the readings.
                this.BindMeasureSubsystem( new MeasureSubsystem( this.StatusSubsystem ) );
                // the measure subsystem readings are set when the format system is reset.
                this.BindFormatSubsystem( new FormatSubsystem( this.StatusSubsystem ) );
                this.BindRouteSubsystem( new RouteSubsystem( this.StatusSubsystem ) );
                this.BindSenseVoltageSubsystem( new SenseVoltageSubsystem( this.StatusSubsystem ) );
                this.BindSenseCurrentSubsystem( new SenseCurrentSubsystem( this.StatusSubsystem ) );
                this.BindSenseResistanceSubsystem( new SenseResistanceSubsystem( this.StatusSubsystem ) );
                this.BindSenseResistanceFourWireSubsystem( new SenseResistanceFourWireSubsystem( this.StatusSubsystem ) );
                this.BindSenseSubsystem( new SenseSubsystem( this.StatusSubsystem ) );
                this.BindTraceSubsystem( new TraceSubsystem( this.StatusSubsystem ) );
                this.BindTriggerSubsystem( new TriggerSubsystem( this.StatusSubsystem ) );
                this.BindBinningSubsystem( new BinningSubsystem( this.StatusSubsystem ) );
                this.BindArmLayer1Subsystem( new ArmLayer1Subsystem( this.StatusSubsystem ) );
                this.BindArmLayer2Subsystem( new ArmLayer2Subsystem( this.StatusSubsystem ) );
                this.BindDigitalOutputSubsystem( new DigitalOutputSubsystem( this.StatusSubsystem ) );
            }
        }

        #endregion

        #region " SUBSYSTEMS "

        /// <summary> Gets or sets the arm layer 1 subsystem. </summary>
        /// <value> The arm layer 1 subsystem. </value>
        public ArmLayer1Subsystem ArmLayer1Subsystem { get; private set; }

        /// <summary> Binds the Arm Layer 1 subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindArmLayer1Subsystem( ArmLayer1Subsystem subsystem )
        {
            if ( this.ArmLayer1Subsystem is object )
            {
                _ = this.Subsystems.Remove( this.ArmLayer1Subsystem );
                this.ArmLayer1Subsystem = null;
            }

            this.ArmLayer1Subsystem = subsystem;
            if ( this.ArmLayer1Subsystem is object )
            {
                this.Subsystems.Add( this.ArmLayer1Subsystem );
            }
        }

        /// <summary> Gets or sets the arm layer 2 subsystem. </summary>
        /// <value> The arm layer 2 subsystem. </value>
        public ArmLayer2Subsystem ArmLayer2Subsystem { get; private set; }

        /// <summary> Binds the ArmLayer2 subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindArmLayer2Subsystem( ArmLayer2Subsystem subsystem )
        {
            if ( this.ArmLayer2Subsystem is object )
            {
                _ = this.Subsystems.Remove( this.ArmLayer2Subsystem );
                this.ArmLayer2Subsystem = null;
            }

            this.ArmLayer2Subsystem = subsystem;
            if ( this.ArmLayer2Subsystem is object )
            {
                this.Subsystems.Add( this.ArmLayer2Subsystem );
            }
        }

        /// <summary> Gets or sets the Binning subsystem. </summary>
        /// <value> The Binning subsystem. </value>
        public BinningSubsystem BinningSubsystem { get; private set; }

        /// <summary> Binds the Binning subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindBinningSubsystem( BinningSubsystem subsystem )
        {
            if ( this.BinningSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.BinningSubsystem );
                this.BinningSubsystem = null;
            }

            this.BinningSubsystem = subsystem;
            if ( this.BinningSubsystem is object )
            {
                this.Subsystems.Add( this.BinningSubsystem );
            }
        }

        /// <summary> Gets or sets the Digital output subsystem. </summary>
        /// <value> The Digital Output subsystem. </value>
        public DigitalOutputSubsystem DigitalOutputSubsystem { get; private set; }

        /// <summary> Binds the Digital Output subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDigitalOutputSubsystem( DigitalOutputSubsystem subsystem )
        {
            if ( this.DigitalOutputSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DigitalOutputSubsystem );
                this.DigitalOutputSubsystem.Dispose();
                this.DigitalOutputSubsystem = null;
            }

            this.DigitalOutputSubsystem = subsystem;
            if ( this.DigitalOutputSubsystem is object )
            {
                this.Subsystems.Add( this.DigitalOutputSubsystem );
            }
        }

        /// <summary> Gets or sets the Measure Subsystem. </summary>
        /// <value> The Measure Subsystem. </value>
        public MeasureSubsystem MeasureSubsystem { get; private set; }

        /// <summary> Binds the Measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMeasureSubsystem( MeasureSubsystem subsystem )
        {
            if ( this.MeasureSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            this.MeasureSubsystem = subsystem;
            if ( this.MeasureSubsystem is object )
            {
                this.Subsystems.Add( this.MeasureSubsystem );
            }
        }

        /// <summary> Gets or sets the Route Subsystem. </summary>
        /// <value> The Route Subsystem. </value>
        public RouteSubsystem RouteSubsystem { get; private set; }

        /// <summary> Binds the Route subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindRouteSubsystem( RouteSubsystem subsystem )
        {
            if ( this.RouteSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.RouteSubsystem );
                this.RouteSubsystem = null;
            }

            this.RouteSubsystem = subsystem;
            if ( this.RouteSubsystem is object )
            {
                this.Subsystems.Add( this.RouteSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Current Subsystem. </summary>
        /// <value> The Sense Current Subsystem. </value>
        public SenseCurrentSubsystem SenseCurrentSubsystem { get; private set; }

        /// <summary> Binds the Sense Current subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseCurrentSubsystem( SenseCurrentSubsystem subsystem )
        {
            if ( this.SenseCurrentSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseCurrentSubsystem );
                this.SenseCurrentSubsystem = null;
            }

            this.SenseCurrentSubsystem = subsystem;
            if ( this.SenseCurrentSubsystem is object )
            {
                this.Subsystems.Add( this.SenseCurrentSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Voltage Subsystem. </summary>
        /// <value> The Sense Voltage Subsystem. </value>
        public SenseVoltageSubsystem SenseVoltageSubsystem { get; private set; }

        /// <summary> Binds the Sense Voltage subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseVoltageSubsystem( SenseVoltageSubsystem subsystem )
        {
            if ( this.SenseVoltageSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseVoltageSubsystem );
                this.SenseVoltageSubsystem = null;
            }

            this.SenseVoltageSubsystem = subsystem;
            if ( this.SenseVoltageSubsystem is object )
            {
                this.Subsystems.Add( this.SenseVoltageSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Four Wire Resistance Subsystem. </summary>
        /// <value> The Sense Four Wire Resistance Subsystem. </value>
        public SenseResistanceFourWireSubsystem SenseResistanceFourWireSubsystem { get; private set; }

        /// <summary> Binds the Sense Four Wire Resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseResistanceFourWireSubsystem( SenseResistanceFourWireSubsystem subsystem )
        {
            if ( this.SenseResistanceFourWireSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseResistanceFourWireSubsystem );
                this.SenseResistanceFourWireSubsystem = null;
            }

            this.SenseResistanceFourWireSubsystem = subsystem;
            if ( this.SenseResistanceFourWireSubsystem is object )
            {
                this.Subsystems.Add( this.SenseResistanceFourWireSubsystem );
            }
        }

        /// <summary> Gets or sets the Sense Resistance Subsystem. </summary>
        /// <value> The Sense Resistance Subsystem. </value>
        public SenseResistanceSubsystem SenseResistanceSubsystem { get; private set; }

        /// <summary> Binds the Sense Resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseResistanceSubsystem( SenseResistanceSubsystem subsystem )
        {
            if ( this.SenseResistanceSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SenseResistanceSubsystem );
                this.SenseResistanceSubsystem = null;
            }

            this.SenseResistanceSubsystem = subsystem;
            if ( this.SenseResistanceSubsystem is object )
            {
                this.Subsystems.Add( this.SenseResistanceSubsystem );
            }
        }

        /// <summary> Gets or sets the Trigger Subsystem. </summary>
        /// <value> The Trigger Subsystem. </value>
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Binds the Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTriggerSubsystem( TriggerSubsystem subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.TriggerSubsystem );
                if ( this.TriggerSubsystem.Session.IsSessionOpen )
                    this.TriggerSubsystem.Abort();
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( this.TriggerSubsystem is object )
            {
                this.Subsystems.Add( this.TriggerSubsystem );
            }
        }

        #endregion

        #region " FORMAT SUBSYSTEM "

        /// <summary> Gets or sets the Format Subsystem. </summary>
        /// <value> The Format Subsystem. </value>
        public FormatSubsystem FormatSubsystem { get; private set; }

        /// <summary> Binds the Format subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindFormatSubsystem( FormatSubsystem subsystem )
        {
            if ( this.FormatSubsystem is object )
            {
                this.FormatSubsystem.PropertyChanged -= this.FormatSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.FormatSubsystem );
                this.FormatSubsystem = null;
            }

            this.FormatSubsystem = subsystem;
            if ( this.FormatSubsystem is object )
            {
                this.FormatSubsystem.PropertyChanged += this.FormatSubsystemPropertyChanged;
                this.Subsystems.Add( this.FormatSubsystem );
                this.HandlePropertyChanged( this.FormatSubsystem, nameof( FormatSubsystemBase.Elements ) );
            }
        }

        /// <summary> Handle the Format subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( FormatSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( FormatSubsystemBase.Elements ):
                    {
                        if ( subsystem.SupportsElements )
                        {
                            // supported elements for the 2002 are: Reading channel, reading number, units, timestamp and status.
                            // must define reading at a minimum
                            this.MeasureSubsystem.ReadingAmounts.Initialize( subsystem.Elements | ReadingElementTypes.Reading );
                        }

                        break;
                    }
            }
        }

        /// <summary> Format subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FormatSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( FormatSubsystemBase )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as FormatSubsystemBase, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE SUBSYSTEM "

        /// <summary> Gets or sets the Sense Subsystem. </summary>
        /// <value> The Sense Subsystem. </value>
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Binds the Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseSubsystem( SenseSubsystem subsystem )
        {
            if ( this.SenseSubsystem is object )
            {
                this.SenseSubsystem.PropertyChanged -= this.SenseSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            this.SenseSubsystem = subsystem;
            if ( this.SenseSubsystem is object )
            {
                this.SenseSubsystem.PropertyChanged += this.SenseSubsystemPropertyChanged;
                this.Subsystems.Add( this.SenseSubsystem );
                this.HandlePropertyChanged( this.SenseSubsystem, nameof( K2002.SenseSubsystem.FunctionMode ) );
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2002.SenseSubsystem.FunctionMode ):
                    {
                        this.MeasureSubsystem.FunctionMode = subsystem.FunctionMode;
                        this.SelectSenseFunctionSubsystem( subsystem.FunctionMode.GetValueOrDefault( SenseFunctionModes.Voltage ) );
                        break;
                    }
            }
        }

        /// <summary> Sense subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inSenseion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( K2002.SenseSubsystem )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as SenseSubsystem, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets or sets the sense function subsystem. </summary>
        /// <value> The sense function subsystem. </value>
        public SenseFunctionSubsystemBase SenseFunctionSubsystem { get; private set; }

        /// <summary> Select sense function subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="functionMode"> The function mode. </param>
        private void SelectSenseFunctionSubsystem( SenseFunctionModes functionMode )
        {
            switch ( functionMode )
            {
                case SenseFunctionModes.Current:
                    {
                        this.SenseFunctionSubsystem = this.SenseCurrentSubsystem;
                        break;
                    }

                case SenseFunctionModes.Resistance:
                    {
                        this.SenseFunctionSubsystem = this.SenseResistanceSubsystem;
                        break;
                    }

                case SenseFunctionModes.ResistanceFourWire:
                    {
                        this.SenseFunctionSubsystem = this.SenseResistanceFourWireSubsystem;
                        break;
                    }

                case SenseFunctionModes.Voltage:
                    {
                        this.SenseFunctionSubsystem = this.SenseVoltageSubsystem;
                        break;
                    }
            }
        }

        #endregion

        #region " STATUS SUBSYSTEM "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM SUBSYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                this.SystemSubsystem.PropertyChanged -= this.SystemSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.SystemSubsystem.PropertyChanged += this.SystemSubsystemPropertyChanged;
                this.Subsystems.Add( this.SystemSubsystem );
                if ( this.TraceSubsystem is object )
                {
                    this.TraceSubsystem.MemoryOption = this.SystemSubsystem.MemoryOption;
                }
            }
        }

        /// <summary> Handle the System subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2002.SystemSubsystem.MemoryOption ):
                    {
                        this.TraceSubsystem.MemoryOption = subsystem.MemoryOption;
                        break;
                    }
            }
        }

        /// <summary> System subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inSystemion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SystemSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( K2002.SystemSubsystem )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as SystemSubsystem, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRACE SUBSYSTEM "

        /// <summary> Gets or sets the Trace Subsystem. </summary>
        /// <value> The Trace Subsystem. </value>
        public TraceSubsystem TraceSubsystem { get; private set; }

        /// <summary> Binds the Trace subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTraceSubsystem( TraceSubsystem subsystem )
        {
            if ( this.TraceSubsystem is object )
            {
                this.TraceSubsystem.PropertyChanged -= this.TraceSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.TraceSubsystem );
                this.TraceSubsystem = null;
            }

            this.TraceSubsystem = subsystem;
            if ( this.TraceSubsystem is object )
            {
                this.Subsystems.Add( this.TraceSubsystem );
                this.TraceSubsystem.PropertyChanged += this.TraceSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trace subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TraceSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( TraceSubsystemBase.LastBufferReading ):
                    {
                        _ = this.MeasureSubsystem.ParsePrimaryReading( subsystem.LastBufferReading?.Reading );
                        break;
                    }
            }
        }

        /// <summary> Trace subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inTraceion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TraceSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( TraceSubsystemBase )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as TraceSubsystemBase, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( "K2002 Settings Editor", Properties.Settings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = Properties.Settings.Default;
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( Properties.Settings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( Properties.Settings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( Properties.Settings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( Properties.Settings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( Properties.Settings.SessionMessageNotificationLevel ):
                    {
                        this.StatusSubsystemBase.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} changed to {sender.SessionMessageNotificationLevel}" );
                        break;
                    }

                case nameof( Properties.Settings.ReadDelay ):
                    {
                        this.Session.ReadDelay = Properties.Settings.Default.ReadDelay;
                        break;
                    }

                case nameof( Properties.Settings.StatusReadDelay ):
                    {
                        this.Session.StatusReadDelay = Properties.Settings.Default.StatusReadDelay;
                        break;
                    }

                case nameof( Properties.Settings.StatusReadTurnaroundTime ):
                    {
                        this.Session.StatusReadTurnaroundTime = Properties.Settings.Default.StatusReadTurnaroundTime;
                        break;
                    }
            }
        }

        /// <summary> Handles settings property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleSettingsPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( Properties.Settings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as Properties.Settings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
