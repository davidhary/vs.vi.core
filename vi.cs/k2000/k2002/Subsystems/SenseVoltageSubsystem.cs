namespace isr.VI.K2002
{

    /// <summary> Defines a SCPI Sense Voltage Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-03-01, 3.0.5173. </para>
    /// </remarks>
    public class SenseVoltageSubsystem : SenseFunctionSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseVoltageSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.DefineFunctionModeReadWrites( "\"{0}\"", "'{0}'" );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            base.DefineKnownResetState();
            this.FunctionUnit = this.DefaultFunctionUnit;
        }

        #endregion

        #region " APERTURE "

        /// <summary> Gets or sets The Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string ApertureQueryCommand { get; set; } = ":SENS:VOLT:DC:APER {0}";

        /// <summary> Gets or sets The Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected override string ApertureCommandFormat { get; set; } = ":SENS:VOLT:DC:APER?";

        #endregion

        #region " AVERAGE COUNT "

        /// <summary> Gets or sets The average count command format. </summary>
        /// <value> The average count command format. </value>
        protected override string AverageCountCommandFormat { get; set; } = ":SENS:VOLT:DC:AVER:COUN {0}";

        /// <summary> Gets or sets The average count query command. </summary>
        /// <value> The average count query command. </value>
        protected override string AverageCountQueryCommand { get; set; } = ":SENS:VOLT:DC:AVER:COUN?";

        #endregion

        #region " AVERAGE ENABLED "

        /// <summary> Gets or sets the Average enabled command Format. </summary>
        /// <value> The Average enabled query command. </value>
        protected override string AverageEnabledCommandFormat { get; set; } = ":SENS:VOLT:DC:AVER:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Average enabled query command. </summary>
        /// <value> The Average enabled query command. </value>
        protected override string AverageEnabledQueryCommand { get; set; } = ":SENS:VOLT:DC:AVER:STAT?";

        #endregion

        #region " AVERAGE FILTER TYPE "

        /// <summary> Gets or sets the Average Filter Type command format. </summary>
        /// <value> The write Average Filter Type command format. </value>
        protected override string AverageFilterTypeCommandFormat { get; set; } = ":SENS:VOLT:DC:AVER:TCON {0}";

        /// <summary> Gets or sets the Average Filter Type query command. </summary>
        /// <value> The Average Filter Type query command. </value>
        protected override string AverageFilterTypeQueryCommand { get; set; } = ":SENS:VOLT:DC:AVER:TCON?";

        #endregion

        #region " AVERAGE PERCENT WINDOW "

        /// <summary> Gets or sets The Average Percent Window command format. </summary>
        /// <value> The Average Percent Window command format. </value>
        protected override string AveragePercentWindowCommandFormat { get; set; } = ":SENS:VOLT:DC:AVER:ADV:NTOL {0}";

        /// <summary> Gets or sets The Average Percent Window query command. </summary>
        /// <value> The Average Percent Window query command. </value>
        protected override string AveragePercentWindowQueryCommand { get; set; } = ":SENS:VOLT:DC:AVER:ADV:NTOL?";

        #endregion

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:VOLT:DC:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:VOLT:DC:RANG:AUTO?";

        #endregion

        #region " AUTO ZERO "

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:VOLT:DC:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:VOLT:DC:NPLC?";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SENS:VOLT:DC:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SENS:VOLT:DC:RANG?";

        #endregion

        #region " RESOLUTION DIGITS "

        /// <summary> Gets or sets the Resolution Digits command format. </summary>
        /// <value> The Resolution Digits command format. </value>
        protected override string ResolutionDigitsCommandFormat { get; set; } = ":SENS:VOLT:DC:DIG {0:0}";

        /// <summary> Gets or sets the Resolution Digits query command. </summary>
        /// <value> The Resolution Digits query command. </value>
        protected override string ResolutionDigitsQueryCommand { get; set; } = ":SENS:VOLT:DC:DIG?";

        #endregion

    }
}
