using isr.Core.EnumExtensions;

namespace isr.VI.K2002
{

    /// <summary>
    /// Defines the contract that must be implemented by a SCPI Digital Output Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class DigitalOutputSubsystem : DigitalOutputSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalOutputSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        /// <see cref="T:isr.VI.StatusSubsystemBase">status
        /// subsystem</see>. </param>
        public DigitalOutputSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DigitalActiveLevelReadWrites.Clear();
            this.DigitalActiveLevelReadWrites.Add( ( long ) DigitalActiveLevels.High, "AHIG", "AHIG", DigitalActiveLevels.High.DescriptionUntil() );
            this.DigitalActiveLevelReadWrites.Add( ( long ) DigitalActiveLevels.Low, "ALOW", "ALOW", DigitalActiveLevels.Low.DescriptionUntil() );
            this.SupportedDigitalActiveLevels = DigitalActiveLevels.Low | DigitalActiveLevels.High;
        }

        #endregion

        #region " OUTPUT SUBSYSTEM "

        /// <summary> Gets or sets the Output Polarity query command. </summary>
        /// <remarks> SCPI: OUTP:TTL{0}:LSEN? </remarks>
        /// <value> The Output Polarity query command. </value>
        protected override string DigitalActiveLevelQueryCommand { get; set; } = "OUTP:TTL{0}:LSEN?";

        /// <summary> Gets or sets the Output Polarity command format. </summary>
        /// <remarks> SCPI: OUTP:TTL{0}:LSEN {1} </remarks>
        /// <value> The Output Polarity command format. </value>
        protected override string DigitalActiveLevelCommandFormat { get; set; } = "OUTP:TTL{0}:LSEN {{0}}";

        #endregion

        #region " SOURCE SUBSYSTEM "

        /// <summary> Gets or sets the digital output Level query command. </summary>
        /// <value> The Limit enabled query command. </value>
        protected override string LevelQueryCommand { get; set; } = ":SOUR:TTL:LEV?";

        /// <summary> Gets or sets the digital output Level command format. </summary>
        /// <value> The Level query command format. </value>
        protected override string LevelCommandFormat { get; set; } = ":SOUR:TTL:LEV {0}";

        /// <summary> Gets or sets the digital output Line Level query command format. </summary>
        /// <value> The Line Level query command. </value>
        protected override string LineLevelQueryCommand { get; set; } = ":SOUR:TTL{0}:LEV?";

        /// <summary> Gets or sets the digital output Line Level query command format. </summary>
        /// <value> The Line Level query command format. </value>
        protected override string LineLevelCommandFormat { get; set; } = ":SOUR:TTL{0}:LEV {{0}}";

        #endregion

    }
}
