using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Core.TimeSpanExtensions;

namespace isr.VI.K2002
{

    /// <summary> Defines a Trace Subsystem for a Keithley 2002 instrument. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TraceSubsystem : TraceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TraceSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.PointsCount = 10;
            this.FeedSource = FeedSources.None;
            this.FeedControl = FeedControls.Never;
        }

        #endregion

        #region " MEMORY OPTION "

        /// <summary> The buffer size standard full. </summary>
        protected const int BufferSizeStandardFull = 404;

        /// <summary> The buffer size standard compact. </summary>
        protected const int BufferSizeStandardCompact = 2027;

        /// <summary> The buffer size memory 1 full. </summary>
        protected const int BufferSizeMemory1Full = 1381;

        /// <summary> The buffer size memory 1 compact. </summary>
        protected const int BufferSizeMemory1Compact = 6909;

        /// <summary> The buffer size memory 2 full. </summary>
        protected const int BufferSizeMemory2Full = 5780; // Manual page 3-148 is incorrect at 5980

        /// <summary> The buffer size memory 2 compact. </summary>
        protected const int BufferSizeMemory2Compact = 29908;

        /// <summary> The memory option. </summary>
        private int? _MemoryOption;

        /// <summary> Gets or sets the memory Option. </summary>
        /// <value> The memory Option. </value>
        public int? MemoryOption
        {
            get => this._MemoryOption;

            set {
                if ( !Equals( this.MemoryOption, value ) )
                {
                    this._MemoryOption = value;
                    switch ( value )
                    {
                        case 0:
                            {
                                this.MaximumCapacity = BufferSizeStandardFull;
                                break;
                            }

                        case 1:
                            {
                                this.MaximumCapacity = BufferSizeMemory1Full;
                                break;
                            }

                        case 2:
                            {
                                this.MaximumCapacity = BufferSizeMemory2Full;
                                break;
                            }

                        default:
                            {
                                this.MaximumCapacity = BufferSizeStandardFull;
                                break;
                            }
                    }

                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the Actual Points count query command. </summary>
        /// <value> The Actual Points count query command. </value>
        protected override string ActualPointCountQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the automatic Points enabled command Format. </summary>
        /// <remarks> SCPI: ":TRAC:POIN:AUTO {0:'ON';'ON';'OFF'}". </remarks>
        /// <value> The automatic Points enabled query command. </value>
        protected override string AutoPointsEnabledCommandFormat { get; set; } = ":TRAC:POIN:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Points enabled query command. </summary>
        /// <remarks> SCPI: ":TRAC:POIN:AUTO?". </remarks>
        /// <value> The automatic Points enabled query command. </value>
        protected override string AutoPointsEnabledQueryCommand { get; set; } = ":TRAC:POIN:AUTO?";

        /// <summary> Gets or sets the Buffer Free query command. </summary>
        /// <value> The buffer free query command. </value>
        protected override string BufferFreePointCountQueryCommand { get; set; } = ":TRAC:FREE?";

        /// <summary> Gets or sets the Clear Buffer command. </summary>
        /// <value> The ClearBuffer command. </value>
        protected override string ClearBufferCommand { get; set; } = ":TRAC:CLE";

        /// <summary> Gets or sets the data query command. </summary>
        /// <value> The data query command. </value>
        protected override string DataQueryCommand { get; set; } = ":TRAC:DATA?";

        /// <summary> Gets or sets the feed Control query command. </summary>
        /// <value> The write feed Control query command. </value>
        protected override string FeedControlQueryCommand { get; set; } = ":TRAC:FEED:CONTROL?";

        /// <summary> Gets or sets the feed Control command format. </summary>
        /// <value> The write feed Control command format. </value>
        protected override string FeedControlCommandFormat { get; set; } = ":TRAC:FEED:CONTROL {0}";

        /// <summary> Gets or sets the feed source query command. </summary>
        /// <value> The write feed source query command. </value>
        protected override string FeedSourceQueryCommand { get; set; } = ":TRAC:FEED?";

        /// <summary> Gets or sets the feed source command format. </summary>
        /// <value> The write feed source command format. </value>
        protected override string FeedSourceCommandFormat { get; set; } = ":TRAC:FEED {0}";

        /// <summary> Gets or sets the fetch command. </summary>
        /// <value> The fetch command. </value>
        protected override string FetchCommand { get; set; } = ":FETCH?";

        /// <summary> Gets or sets the points count query command. </summary>
        /// <value> The points count query command. </value>
        protected override string PointsCountQueryCommand { get; set; } = ":TRAC:POIN?";

        /// <summary> Gets or sets the points count command format. </summary>
        /// <value> The points count command format. </value>
        protected override string PointsCountCommandFormat { get; set; } = ":TRAC:POIN {0}";

        #endregion

        #region " CLEAR OVERRIDES "

        /// <summary> Clears the data buffer and restores the feed Control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="feedControl"> The feed control. </param>
        /// <returns> The FeedControls. </returns>
        public FeedControls? ClearBuffer( FeedControls feedControl )
        {
            this.ClearBuffer();
            return this.ApplyFeedControl( feedControl );
        }

        #endregion

        #region " BUFFER STREAMING "

        /// <summary> Queries the current Data. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The Data or empty if none. </returns>
        public override IList<BufferReading> QueryBufferReadings()
        {
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );
            measurementBitmasks.Status = this.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
            return measurementBitmasks.IsAnyBitOn( MeasurementEventBitmaskKey.BufferAvailable ) ? this.EnumerateBufferReadings( this.QueryData() ) : new List<BufferReading>();
        }

        /// <summary> Adds all buffer readings. </summary>
        /// <remarks>
        /// This assumes fetching a trace of multiple readings and, therefore, does not parse timestamps
        /// or status.
        /// </remarks>
        /// <returns> An Integer. </returns>
        public override int AddAllBufferReadings()
        {
            var newReadings = this.QueryBufferReadings();
            if ( newReadings.Any() )
            {
                this.BufferReadingsBindingList.Add( newReadings );
                this.EnqueueRange( newReadings );
                this.LastBufferReading = this.BufferReadingsBindingList.LastReading;
                this.NotifyPropertyChanged( nameof( this.BufferReadingsCount ) );
            }

            return newReadings.Count;
        }

        /// <summary> Adds buffer readings to the reading queue and binding list. </summary>
        /// <remarks>
        /// This assumes the measurement event status indicates that a reading is ready to be fetched and
        /// the status reflect reading exceptions such as out of range or overflow.
        /// </remarks>
        /// <param name="readingUnit">      The reading unit. </param>
        /// <param name="initialTimestamp"> The initial timestamp. </param>
        /// <param name="elapsedTimespan">  The elapsed timespan. </param>
        /// <returns> An Integer. </returns>
        public int AddBufferReadings( Arebis.TypedUnits.Unit readingUnit, DateTimeOffset initialTimestamp, TimeSpan elapsedTimespan )
        {
            return this.AddBufferReadings( this.FetchReading(), readingUnit, initialTimestamp, elapsedTimespan );
        }

        /// <summary> Adds buffer readings to the reading queue and binding list. </summary>
        /// <remarks>
        /// This assumes the reading were already fetched and the status reflect reading exceptions such
        /// as out of range or overflow.
        /// </remarks>
        /// <param name="readingValues">    The reading values. </param>
        /// <param name="readingUnit">      The reading unit. </param>
        /// <param name="initialTimestamp"> The initial timestamp. </param>
        /// <param name="elapsedTimespan">  The elapsed timespan. </param>
        /// <returns> An Integer. </returns>
        public int AddBufferReadings( string readingValues, Arebis.TypedUnits.Unit readingUnit, DateTimeOffset initialTimestamp, TimeSpan elapsedTimespan )
        {
            IList<BufferReading> newReadings = new List<BufferReading>() { new BufferReading() };
            if ( !string.IsNullOrWhiteSpace( readingValues ) )
            {
                newReadings = this.EnumerateBufferReadings( readingValues );
            }

            if ( newReadings.Any() )
            {
                int status = this.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
                foreach ( BufferReading reading in newReadings )
                {
                    // if no reading, the amount will be zero and the buffer has reading will be use to indicate that.
                    reading.BuildAmount( readingUnit );
                    reading.ApplyStatus( status | (reading.HasReading ? 0 : this.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.StatusOnly]) );
                    reading.ParseTimestamp( initialTimestamp, elapsedTimespan );
                }

                this.BufferReadingsBindingList.Add( newReadings );
                this.EnqueueRange( newReadings );
                this.LastBufferReading = this.BufferReadingsBindingList.LastReading;
                // this notifies the automation that buffer readings are 
                this.SyncNotifyPropertyChanged( nameof( this.BufferReadingsCount ) );
                Core.ApplianceBase.DoEvents();
            }

            return newReadings.Count;
        }

        /// <summary> Stream buffer; use with buffer counts exceeding 1 point. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="pollPeriod"> The poll period. </param>
        private void StreamBufferThis( TimeSpan pollPeriod )
        {

            // save the currently set feed control
            var feed = this.FeedControl.Value;

            // clear the buffer
            _ = this.ClearBuffer( feed );

            // clear trace buffer
            this.BufferStreamingEnabled = true;
            this.BufferReadingsBindingList.Clear();
            this.ClearBufferReadingsQueue();
            this.BufferStreamingActive = true;
            _ = this.StatusSubsystem.ApplyMeasurementEventEnableBitmask( this.StatusSubsystem.MeasurementEventsBitmasks.All );
            _ = this.StatusSubsystem.ApplyOperationEventEnableBitmask( this.StatusSubsystem.OperationEventsBitmasks.All );
            var initialTimestamp = DateTimeOffset.Now;
            var sampleTimeStopwatch = Stopwatch.StartNew();
            var bufferCycleDurationStopwatch = Stopwatch.StartNew();
            var measurementEvent = this.StatusSubsystem.MeasurementEventsBitmasks;
            var pollStopwatch = Stopwatch.StartNew();
            while ( this.BufferStreamingEnabled )
            {
                Core.ApplianceBase.DoEvents();
                measurementEvent.Status = this.StatusSubsystem.QueryMeasurementEventStatus().Value;
                if ( measurementEvent.IsAnyBitOn( MeasurementEventBitmaskKey.BufferFull ) )
                {
                    _ = this.AddBufferReadings( this.QueryData(), this.BufferReadingUnit, initialTimestamp, sampleTimeStopwatch.Elapsed );
                    // allow enough time for binning to take place
                    this.BinningDuration.SpinWait();
                    // clear the buffer for the next trigger
                    _ = this.ClearBuffer( feed );
                    this.StreamCycleDuration = bufferCycleDurationStopwatch.Elapsed;
                    bufferCycleDurationStopwatch.Restart();
                }
                else
                {
                    if ( this.BusTriggerRequested )
                    {
                        this.BusTriggerRequested = false;
                        this.Session.AssertTrigger();
                    }

                    if ( this.BinningStrobeRequested )
                    {
                        this.BinningStrobeRequested = false;
                        this.BinningStrobeAction.Invoke();
                    }

                    pollPeriod.Subtract( pollStopwatch.Elapsed ).SpinWait();
                    pollStopwatch.Restart();
                    Core.ApplianceBase.DoEvents();
                }
            }

            this.BufferStreamingActive = false;
        }

        /// <summary> Stream reading this. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="pollPeriod"> The poll period. </param>
        private void StreamReadingThis( TimeSpan pollPeriod )
        {
            var measurementEvent = this.StatusSubsystem.MeasurementEventsBitmasks;
            this.BufferStreamingAlert = false;
            this.BufferStreamingEnabled = true;
            this.BufferReadingsBindingList.Clear();
            this.ClearBufferReadingsQueue();
            this.BufferStreamingActive = true;
            _ = this.StatusSubsystem.ApplyMeasurementEventEnableBitmask( this.StatusSubsystem.MeasurementEventsBitmasks.All );
            _ = this.StatusSubsystem.ApplyOperationEventEnableBitmask( this.StatusSubsystem.OperationEventsBitmasks.All );
            var initialTimestamp = DateTimeOffset.Now;
            var bufferCycleDurationStopwatch = Stopwatch.StartNew();
            var sampleTimeStopwatch = Stopwatch.StartNew();
            var pollStopwatch = Stopwatch.StartNew();
            while ( this.BufferStreamingEnabled )
            {
                Core.ApplianceBase.DoEvents();
                measurementEvent.Status = this.StatusSubsystem.QueryMeasurementEventStatus().Value;
                if ( measurementEvent.IsAnyBitOn( MeasurementEventBitmaskKey.ReadingAvailable ) )
                {
                    _ = this.AddBufferReadings( this.FetchReading(), this.BufferReadingUnit, initialTimestamp, sampleTimeStopwatch.Elapsed );
                    // allow enough time for binning to take place
                    this.BinningDuration.SpinWait();
                    this.StreamCycleDuration = bufferCycleDurationStopwatch.Elapsed;
                    bufferCycleDurationStopwatch.Restart();
                }
                else
                {
                    if ( this.BusTriggerRequested )
                    {
                        this.BusTriggerRequested = false;
                        this.Session.AssertTrigger();
                    }

                    if ( this.BinningStrobeRequested )
                    {
                        this.BinningStrobeRequested = false;
                        this.BinningStrobeAction.Invoke();
                    }

                    pollPeriod.Subtract( pollStopwatch.Elapsed ).SpinWait();
                    pollStopwatch.Restart();
                    Core.ApplianceBase.DoEvents();
                }
            }

            this.BufferStreamingActive = false;
        }

        /// <summary> Stream buffer. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="triggerSubsystem"> The trigger subsystem. </param>
        /// <param name="pollPeriod">       The poll period. </param>
        /// <param name="readingUnit">      The reading unit. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public override void StreamBuffer( TriggerSubsystemBase triggerSubsystem, TimeSpan pollPeriod, Arebis.TypedUnits.Unit readingUnit )
        {
            // the 2002 instrument does not monitor the trigger plan.
            try
            {
                this.BufferReadingUnit = readingUnit;
                var feedControls = this.FeedControl.GetValueOrDefault( FeedControls.None );
                if ( FeedControls.Never == feedControls || FeedControls.None == feedControls )
                {
                    this.StreamReadingThis( pollPeriod );
                }
                else
                {
                    this.StreamBufferThis( pollPeriod );
                }
            }
            catch
            {
                // stop buffer streaming; the exception is handled in the Async Completed event handler
                this.BufferStreamingAlert = true;
                this.BufferStreamingEnabled = false;
                this.BufferStreamingActive = false;
            }
        }

        #endregion

    }
}
