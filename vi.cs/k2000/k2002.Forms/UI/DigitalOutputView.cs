namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the basic <see cref="Facade.DigitalOutputView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-11-13 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class DigitalOutputView : Facade.DigitalOutputView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-11-13. </remarks>
        public DigitalOutputView() : base()
        {
        }

        /// <summary> Creates a new <see cref="DigitalOutputView"/> </summary>
        /// <remarks> David, 2020-11-13. </remarks>
        /// <returns> A <see cref="DigitalOutputView"/>. </returns>
        public static new DigitalOutputView Create()
        {
            DigitalOutputView view = null;
            try
            {
                view = new DigitalOutputView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K2002Device K2002Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-11-13. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2002Device value )
        {
            base.AssignDevice( value );
            this.K2002Device = value;
            if ( value is null )
            {
                this.BindSubsystem( default );
            }
            else
            {
                this.BindSubsystem( this.K2002Device.DigitalOutputSubsystem );
            }
        }

        #endregion

    }
}
