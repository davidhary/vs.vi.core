﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2002.Forms
{
    [DesignerGenerated()]
    public partial class TriggerUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _TriggerView = new TriggerView();
            SuspendLayout();
            // 
            // _TriggerView
            // 
            _TriggerView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _TriggerView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _TriggerView.Dock = System.Windows.Forms.DockStyle.Fill;
            _TriggerView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerView.Location = new System.Drawing.Point(1, 1);
            _TriggerView.Name = "_TriggerView";
            _TriggerView.Padding = new System.Windows.Forms.Padding(1);
            _TriggerView.Size = new System.Drawing.Size(375, 351);
            _TriggerView.TabIndex = 24;
            // 
            // TriggerUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_TriggerView);
            Name = "TriggerUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(377, 353);
            ResumeLayout(false);
        }

        private TriggerView _TriggerView;
    }
}