﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2002.Forms
{
    [DesignerGenerated()]
    public partial class DigitalOutputUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _DigitalOutputView = new DigitalOutputView();
            SuspendLayout();
            // 
            // _DigitalOutputView
            // 
            _DigitalOutputView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _DigitalOutputView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            _DigitalOutputView.Dock = System.Windows.Forms.DockStyle.Fill;
            _DigitalOutputView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _DigitalOutputView.Location = new System.Drawing.Point(1, 1);
            _DigitalOutputView.Name = "_DigitalOutputView";
            _DigitalOutputView.Padding = new System.Windows.Forms.Padding(1);
            _DigitalOutputView.Size = new System.Drawing.Size(375, 351);
            _DigitalOutputView.TabIndex = 24;
            // 
            // DigitalOutputUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_DigitalOutputView);
            Name = "DigitalOutputUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(377, 353);
            ResumeLayout(false);
        }

        private DigitalOutputView _DigitalOutputView;
    }
}