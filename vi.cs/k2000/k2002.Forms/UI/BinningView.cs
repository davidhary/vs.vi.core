using System.ComponentModel;

namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the base <see cref="Facade.BinningView"/> user interface.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public class BinningView : Facade.BinningView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="BinningView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="BinningView"/>. </returns>
        public static new BinningView Create()
        {
            BinningView view = null;
            try
            {
                view = new BinningView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2002Device K2002Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void AssignDevice( K2002Device value )
        {
            base.AssignDevice( value );
            this.K2002Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( BinningSubsystemBase ) default );
                this.BindSubsystem( ( DigitalOutputSubsystemBase ) default );
            }
            else
            {
                this.BindSubsystem( this.K2002Device.BinningSubsystem );
                this.BindSubsystem( this.K2002Device.DigitalOutputSubsystem );
            }
        }

        #endregion

    }
}
