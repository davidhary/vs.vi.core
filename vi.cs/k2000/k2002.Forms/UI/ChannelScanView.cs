using System;
using System.ComponentModel;

using isr.Core.SplitExtensions;

namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the basic <see cref="Facade.ChannelScanView"/> user interface.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public class ChannelScanView : Facade.ChannelScanView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ChannelScanView() : base()
        {
            if ( !this.DesignMode )
            {
                this._FourWireResistanceScanMenu = this.FourWireResistanceScanMakeMenu();
                this._FourWireResistanceTimerScanMenu = this.FourWireResistanceTimerScanMakeMenu( false );
            }
        }

        /// <summary> Creates a new <see cref="ChannelScanView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ChannelScanView"/>. </returns>
        public static new ChannelScanView Create()
        {
            ChannelScanView view = null;
            try
            {
                view = new ChannelScanView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2002Device K2002Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void AssignDevice( K2002Device value )
        {
            base.AssignDevice( value );
            if ( this.K2002Device is object )
            {
                this.K2002Device.SystemSubsystem.PropertyChanged -= this.HandleSystemSubsystemPropertyChange;
                this.BindSubsystem( ( SystemSubsystemBase ) default );
                this.BindSubsystem( ( RouteSubsystemBase ) default );
                this.BindSubsystem( ( SenseSubsystemBase ) default );
            }

            this.K2002Device = value;
            if ( value is object )
            {
                this.BindSubsystem( this.K2002Device.SystemSubsystem );
                this.BindSubsystem( this.K2002Device.RouteSubsystem );
                this.BindSubsystem( this.K2002Device.SenseSubsystem );
                this.K2002Device.SystemSubsystem.PropertyChanged += this.HandleSystemSubsystemPropertyChange;
                this.HandlePropertyChanged( this.K2002Device.SystemSubsystem, nameof( SystemSubsystem.SupportsScanCardOption ) );
            }
        }

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SystemSubsystem subsystem, string propertyName )
        {
            if ( subsystem is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( SystemSubsystemBase.SupportsScanCardOption ):
                        {
                            this._FourWireResistanceScanMenu.Enabled = subsystem.SupportsScanCardOption;
                            this._FourWireResistanceTimerScanMenu.Enabled = subsystem.SupportsScanCardOption;
                            break;
                        }
                }
            }
        }

        /// <summary> Handles the system subsystem property change. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private void HandleSystemSubsystemPropertyChange( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SystemSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.HandleSystemSubsystemPropertyChange ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SystemSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }


        #endregion

        #region " FOUR WIRE RESISTANCE SCAN "

        /// <summary> The four wire resistance scan menu. </summary>
        private readonly System.Windows.Forms.ToolStripMenuItem _FourWireResistanceScanMenu;

        /// <summary> Four wire resistance scan make menu. </summary>
        /// <remarks> David, 2020-04-07. </remarks>
        /// <returns> A Windows.Forms.ToolStripMenuItem. </returns>
        private System.Windows.Forms.ToolStripMenuItem FourWireResistanceScanMakeMenu()
        {
            var infoMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceScanInfoMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Info",
                ToolTipText = "Displays information about the four wire resistor scan"
            };
            infoMenuItem.Click += this.FourWireResistanceScanInfoMenuItem_Click;
            var configureMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceScanConfigureMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Configure",
                ToolTipText = "Configures four wire resistance scan"
            };
            configureMenuItem.Click += this.FourWireResistanceScanConfigureMenuItem_Click;
            var usingLibraryFunctionsMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceLibraryFunctionsMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Using VI Library Function",
                ToolTipText = "Check for using library functions; otherwise, messages are used",
                CheckOnClick = true,
                Checked = false
            };
            usingLibraryFunctionsMenuItem.CheckedChanged += this.UsingLibraryFunctionsMenuItem_CheckedChanged;
            var executeMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceScanExecuteMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Execute",
                ToolTipText = "Executes the four wire resistance scan"
            };
            executeMenuItem.Click += this.FourWireResistanceScanExecuteMenuItem_Click;
            var mainMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceScanMenuItem",
                Size = new System.Drawing.Size( 192, 22 ),
                Text = "Four wire resistance scan...",
                ToolTipText = "Selects four wire resistance scan options",
                Enabled = false
            };
            mainMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] { infoMenuItem, usingLibraryFunctionsMenuItem, configureMenuItem, executeMenuItem } );
            this.ScanView.AddMenuItem( mainMenuItem );
            return mainMenuItem;
        }

        /// <summary> Gets or sets the using library functions. </summary>
        /// <value> The using library functions. </value>
        private bool UsingLibraryFunctions { get; set; }

        /// <summary>
        /// Event handler. Called by UsingLibraryFunctionsMenuItem for checked changed events.
        /// </summary>
        /// <remarks> David, 2020-04-07. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void UsingLibraryFunctionsMenuItem_CheckedChanged( object sender, EventArgs e )
        {
            System.Windows.Forms.ToolStripMenuItem menuItem = sender as System.Windows.Forms.ToolStripMenuItem;
            this.UsingLibraryFunctions = Nullable.Equals( true, menuItem?.Checked );
        }

        /// <summary>
        /// Event handler. Called by FourWireResistanceScanInfoMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-04-07. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FourWireResistanceScanInfoMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} displaying {nameof( RouteSubsystem.BuildFourWireResistanceScanHelpMessage ).SplitWords()}";
                Core.Controls.PopupContainer.PopupInfo( this, RouteSubsystem.BuildFourWireResistanceScanHelpMessage(), this.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by FourWireResistanceScanConfigureMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-04-07. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FourWireResistanceScanConfigureMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.ConfigureFourWireResistanceScan ).SplitWords()}";
                _ = this.UsingLibraryFunctions
                    ? RouteSubsystem.ConfigureFourWireResistanceScan( this.K2002Device, ArmSources.Immediate, 1, TimeSpan.FromMilliseconds( 600d ) )
                    : RouteSubsystem.ConfigureFourWireResistanceScan( this.K2002Device.Session, "IMM", 1, TimeSpan.FromMilliseconds( 600d ) );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by FourWireResistanceScanExecuteMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-04-07. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FourWireResistanceScanExecuteMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                (string Status, string Reading, TimeSpan Elapsed) result;
                int bitmask = this.K2002Device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull];
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.InitiateFourWireResistanceScan ).SplitWords()}";
                if ( this.UsingLibraryFunctions )
                {
                    RouteSubsystem.InitiateFourWireResistanceScan( this.K2002Device, bitmask );
                    activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.FetchFourWireResistanceScan ).SplitWords()}";
                    result = RouteSubsystem.FetchFourWireResistanceScan( this.K2002Device );
                }
                else
                {
                    RouteSubsystem.InitiateFourWireResistanceScan( this.K2002Device.Session, bitmask );
                    activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.FetchFourWireResistanceScan ).SplitWords()}";
                    result = RouteSubsystem.FetchFourWireResistanceScan( this.K2002Device.Session, bitmask );
                }

                Core.Controls.PopupContainer.PopupInfo( this, $@"Status: {result.Status}
Values: {result.Reading}
Elapsed: {result.Elapsed.TotalMilliseconds:0}ms", this.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

        #region " FOUR WIRE RESISTANCE TIMER SCAN "

        /// <summary> The four wire resistance timer scan menu. </summary>
        private readonly System.Windows.Forms.ToolStripMenuItem _FourWireResistanceTimerScanMenu;

        /// <summary> Four wire resistance timer scan make menu. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="visible"> True to show, false to hide. </param>
        /// <returns> A Windows.Forms.ToolStripMenuItem. </returns>
        private System.Windows.Forms.ToolStripMenuItem FourWireResistanceTimerScanMakeMenu( bool visible )
        {
            var infoMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceTimerScanInfoMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Info",
                ToolTipText = "Displays information about the four wire resistor timer scan"
            };
            infoMenuItem.Click += this.FourWireResistanceTimerScanInfoMenuItem_Click;
            var usingLibraryFunctionsMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceLibraryFunctionsMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Using VI Library Function",
                ToolTipText = "Check for using library functions; otherwise, messages are used",
                CheckOnClick = true,
                Checked = false
            };
            usingLibraryFunctionsMenuItem.CheckedChanged += this.UsingLibraryFunctionsMenuItem_CheckedChanged;
            var configureMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceTimerScanConfigureMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Configure",
                ToolTipText = "Configures four wire resistance timer scan"
            };
            configureMenuItem.Click += this.FourWireResistanceTimerScanConfigureMenuItem_Click;
            var executeMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceScanExecuteMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Execute",
                ToolTipText = "Executes the four wire resistance timer scan"
            };
            executeMenuItem.Click += this.FourWireResistanceTimerScanExecuteMenuItem_Click;
            var mainMenuItem = new System.Windows.Forms.ToolStripMenuItem() {
                Name = "FourWireResistanceTimerScanMenuItem",
                Size = new System.Drawing.Size( 192, 22 ),
                Text = "Four wire resistance timer scan...",
                ToolTipText = "Selects four wire resistance time scan options",
                Enabled = false,
                Visible = visible
            };
            mainMenuItem.DropDownItems.AddRange( new System.Windows.Forms.ToolStripItem[] { infoMenuItem, usingLibraryFunctionsMenuItem, configureMenuItem, executeMenuItem } );
            this.ScanView.AddMenuItem( mainMenuItem );
            return mainMenuItem;
        }

        /// <summary>
        /// Event handler. Called by FourWireResistanceTimerScanInfoMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FourWireResistanceTimerScanInfoMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} displaying {nameof( RouteSubsystem.BuildFourWireResistanceTimerScanHelpMessage ).SplitWords()}";
                Core.Controls.PopupContainer.PopupInfo( this, RouteSubsystem.BuildFourWireResistanceTimerScanHelpMessage(), this.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by FourWireResistanceTimerScanConfigureMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FourWireResistanceTimerScanConfigureMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.ConfigureFourWireResistanceScan )}";
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.ConfigureFourWireResistanceScan ).SplitWords()}";
                _ = this.UsingLibraryFunctions
                    ? RouteSubsystem.ConfigureFourWireResistanceScan( this.K2002Device, ArmSources.Timer, 10, TimeSpan.FromMilliseconds( 600d ) )
                    : RouteSubsystem.ConfigureFourWireResistanceScan( this.K2002Device.Session, "TIM", 10, TimeSpan.FromMilliseconds( 600d ) );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by FourWireResistanceTimerScanExecuteMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void FourWireResistanceTimerScanExecuteMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                int triggerCount = 10;
                (string Status, string Reading, TimeSpan Elapsed) result;
                int bitmask = this.K2002Device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull];
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.InitiateFourWireResistanceScan ).SplitWords()}";
                if ( this.UsingLibraryFunctions )
                {
                    RouteSubsystem.InitiateFourWireResistanceScan( this.K2002Device, bitmask );
                }
                else
                {
                    RouteSubsystem.InitiateFourWireResistanceScan( this.K2002Device.Session, bitmask );
                }

                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( RouteSubsystem.FetchFourWireResistanceScan ).SplitWords()}";
                var builder = new System.Text.StringBuilder();
                while ( triggerCount > 0 )
                {
                    result = this.UsingLibraryFunctions ? RouteSubsystem.FetchFourWireResistanceScan( this.K2002Device ) : RouteSubsystem.FetchFourWireResistanceScan( this.K2002Device.Session, bitmask );
                    _ = builder.AppendLine( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                    _ = this.K2002Device.Session.ReadStatusRegister();
                    triggerCount -= 1;
                    RouteSubsystem.ClearBuffer( this.K2002Device.Session );
                }

                Core.Controls.PopupContainer.PopupInfo( this, builder.ToString(), this.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

    }
}
