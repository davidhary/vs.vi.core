using System;

namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the basic <see cref="Facade.MeterView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class MeterView : Facade.MeterView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="MeterView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="MeterView"/>. </returns>
        public static new MeterView Create()
        {
            MeterView view = null;
            try
            {
                view = new MeterView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K2002Device K2002Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2002Device value )
        {
            base.AssignDevice( value );
            this.K2002Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( BufferSubsystemBase ) default );
                this.BindSubsystem( ( FormatSubsystemBase ) default );
                this.BindSubsystem( ( MeasureSubsystemBase ) default );
                this.BindSubsystem( ( MultimeterSubsystemBase ) default );
                this.BindSubsystem( ( SenseSubsystemBase ) default );
                this.BindSubsystem( ( TraceSubsystemBase ) default );
                this.BindSubsystem( ( TriggerSubsystemBase ) default );
                this.BindSubsystem( ( SystemSubsystemBase ) default );
            }
            else
            {
                this.BindSubsystem( value.SystemSubsystem );
                this.BindSubsystem( value.SenseSubsystem );
                // Me.BindSubsystem(value.BufferSubsystem)
                this.BindSubsystem( value.FormatSubsystem );
                this.BindSubsystem( value.MeasureSubsystem );
                // Me.BindSubsystem(value.MultimeterSubsystem)
                this.BindSubsystem( value.TraceSubsystem );
                this.BindSubsystem( value.TriggerSubsystem );
            }
        }

        #endregion

        #region " SENSE FUNCTION HANDLING "

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected override void HandleFunctionModesChanged( SenseSubsystemBase subsystem )
        {
            base.HandleFunctionModesChanged( subsystem );
            switch ( subsystem.FunctionMode )
            {
                case SenseFunctionModes.CurrentDC:
                    {
                        this.BindSubsystem( this.K2002Device.SenseCurrentSubsystem, "Current" );
                        break;
                    }

                case SenseFunctionModes.VoltageDC:
                    {
                        this.BindSubsystem( this.K2002Device.SenseVoltageSubsystem, "Voltage" );
                        break;
                    }

                case SenseFunctionModes.ResistanceFourWire:
                    {
                        this.BindSubsystem( this.K2002Device.SenseResistanceFourWireSubsystem, "Ohm 4W" );
                        break;
                    }

                case SenseFunctionModes.Resistance:
                    {
                        this.BindSubsystem( this.K2002Device.SenseResistanceSubsystem, "Ohm" );
                        break;
                    }
            }
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Fetches and displays buffered readings. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void FetchAndDisplayBufferedReadings()
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} fetching buffered readings";
                _ = this.PublishVerbose( $"{activity};. " );
                this.DisplayBufferedReadings( this.K2002Device.TraceSubsystem.QueryReadings( this.K2002Device.MeasureSubsystem.ReadingAmounts ) );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = System.Windows.Forms.Cursors.Default;
            }
        }

        #endregion

    }
}
