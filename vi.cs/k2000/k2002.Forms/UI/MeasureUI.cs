using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// Measure user interface -- defines the Four Wire Resistance Sense subsystem  settings.
    /// </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-03-04 </para>
    /// </remarks>
    public partial class MeasureUI : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MeasureUI() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this._ReadButton.Text = _ReadLabel;
            this._ReadButton.Enabled = false;
            this._MeasureButton.Enabled = false;
            this.InitializingComponents = false;
            this.__TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
            this.__ReadButton.Name = "_ReadButton";
            this.__MeasureButton.Name = "_MeasureButton";
            this.__MeterRangeComboBox.Name = "_MeterRangeComboBox";
        }

        /// <summary> Creates a new <see cref="MeasureUI"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="MeasureUI"/>. </returns>
        public static MeasureUI Create()
        {
            MeasureUI view = null;
            try
            {
                view = new MeasureUI();
                return view;
            }
            catch
            {
                if ( view is object )
                {
                    view.Dispose();
                }

                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged
        /// resources when called from the runtime
        /// finalize. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K2002Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K2002Device value )
        {
            if ( this.Device is object )
            {
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                _ = this.PublishInfo( $"{value.ResourceNameCaption} assigned to {nameof( MeasureUI ).SplitWords()}" );
            }

            this.BindTriggerSubsystem( value );
            this.BindSenseSubsystem( value );
            this.BindMeasureSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2002Device value )
        {
            this.AssignDeviceThis( value );
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( K2002Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K2002.TriggerSubsystem.Delay ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2002.TriggerSubsystem.Delay ):
                    {
                        if ( subsystem.Delay.HasValue )
                        {
                            this.TriggerDelay = subsystem.Delay.Value;
                        }

                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( K2002.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Gets or sets the Trigger Delay. </summary>
        /// <value> The Trigger Delay. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TimeSpan TriggerDelay
        {
            get => TimeSpan.FromMilliseconds( ( int ) Math.Round( this._TriggerDelayNumeric.Value ) );

            set {
                if ( !TimeSpan.Equals( value, this.TriggerDelay ) )
                {
                    _ = this._TriggerDelayNumeric.ValueSetter( ( decimal ) ( int ) Math.Round( value.TotalMilliseconds ) );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Event handler. Called by _TriggerDelayNumeric for value changed events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void TriggerDelayNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            if ( this.Device.IsDeviceOpen && !Nullable.Equals( this.TriggerSubsystem.Delay, this.TriggerDelay ) )
            {
                _ = this.PublishVerbose( $"Applying trigger delay {this.TriggerDelay};. " );
                _ = this.TriggerSubsystem.ApplyDelay( this.TriggerDelay );
                _ = this.Device.Session.ReadStatusRegister();
            }
        }

        #endregion

        #region " SENSE SUBSYSTEM "

        /// <summary> Gets or sets the Sense subsystem. </summary>
        /// <value> The Sense subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public SenseSubsystem SenseSubsystem { get; private set; }

        /// <summary> Bind Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseSubsystem( K2002Device device )
        {
            if ( this.SenseSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseSubsystem = device.SenseSubsystem;
                this.BindSubsystem( true, this.SenseSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K2002.SenseSubsystem.FunctionMode ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseSubsystemPropertyChanged;
                this.BindSenseResistanceSubsystem( null );
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseSubsystem subsystem, string propertyName )
        {
            if ( this.InitializingComponents || subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2002.SenseSubsystem.FunctionMode ):
                    {
                        if ( subsystem.FunctionMode.HasValue )
                        {
                            if ( subsystem.FunctionMode.Value == SenseFunctionModes.ResistanceFourWire )
                            {
                                this.BindSenseResistanceSubsystem( this.Device.SenseResistanceFourWireSubsystem );
                            }
                            else if ( subsystem.FunctionMode.Value == SenseFunctionModes.Resistance )
                            {
                                this.BindSenseResistanceSubsystem( this.Device.SenseResistanceSubsystem );
                            }
                        }

                        break;
                    }
            }
        }

        /// <summary> Sense subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.SenseSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE RESISTANCE "

        /// <summary> Gets or sets the sense resistance subsystem. </summary>
        /// <value> The sense resistance subsystem. </value>
        public SenseResistanceSubsystemBase SenseResistanceSubsystem { get; private set; }

        /// <summary> Bind Sense resistance subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseResistanceSubsystem( SenseResistanceSubsystemBase subsystem )
        {
            if ( this.SenseResistanceSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseResistanceSubsystem );
                this.SenseResistanceSubsystem = null;
            }

            if ( subsystem is object )
            {
                this.SenseResistanceSubsystem = subsystem;
                this.BindSubsystem( true, this.SenseResistanceSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add; otherwise, remove binding. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseResistanceSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseResistanceSubsystemBaseChanged;
                this.HandlePropertyChanged( subsystem, nameof( SenseResistanceSubsystemBase.Range ) );
                this.HandlePropertyChanged( subsystem, nameof( SenseResistanceSubsystemBase.Current ) );
                //  TO_FIX: Must not apply values when binding! Controls must be set to present values.
                // _ = this.SenseResistanceSubsystem.ApplyPowerLineCycles( 1d );
                // _ = this.SenseResistanceSubsystem.ApplyAutoRangeEnabled( false );
                // _ = this.Device.Session.ReadStatusRegister();
                // populate the range mode selector
                bool @int = this.InitializingComponents;
                this.InitializingComponents = true;
                this._MeterRangeComboBox.ListResistanceRangeCurrents( subsystem.ResistanceRangeCurrents, new int[] { 0 } );
                this.InitializingComponents = @int;
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseResistanceSubsystemBaseChanged;
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseResistanceSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( SenseResistanceSubsystemBase.Range ):
                    {
                        if ( subsystem.Range.HasValue )
                            _ = this._MeterRangeNumeric.SilentValueSetter( subsystem.Range.Value );
                        break;
                    }

                case nameof( SenseResistanceSubsystemBase.Current ):
                    {
                        _ = this._MeterCurrentNumeric.SilentValueSetter( subsystem.Current );
                        break;
                    }
            }
        }

        /// <summary> Sense function subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseResistanceSubsystemBaseChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SenseResistanceSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseResistanceSubsystemBaseChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseResistanceSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the meter current. </summary>
        /// <value> The meter current. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public decimal MeterCurrent
        {
            get => this._MeterCurrentNumeric.Value;

            set {
                if ( !decimal.Equals( value, this.MeterCurrent ) )
                {
                    _ = this._MeterCurrentNumeric.SilentValueSetter( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets or sets the meter range. </summary>
        /// <value> The meter range. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public decimal MeterRange
        {
            get => this._MeterRangeNumeric.Value;

            set {
                if ( !decimal.Equals( value, this.MeterRange ) )
                {
                    _ = this._MeterRangeNumeric.SilentValueSetter( value );
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Gets the selected meter range. </summary>
        /// <value> The selected meter range. </value>
        public ResistanceRangeCurrent SelectedMeterRange => this._MeterRangeComboBox.SelectedResistanceRangeCurrent();

        /// <summary> Selects the meter range based on the range mode description. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="description"> The description. </param>
        /// <returns> A ResistanceRangeCurrent. </returns>
        public ResistanceRangeCurrent SelectMeterRange( string description )
        {
            return this._MeterRangeComboBox.SelectResistanceRangeCurrent( this.SenseResistanceSubsystem.ResistanceRangeCurrents.MatchResistanceRange( description ) );
        }

        /// <summary> Selects the meter range based on the range settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="range"> The range. </param>
        /// <returns> A ResistanceRangeCurrent. </returns>
        public ResistanceRangeCurrent SelectMeterRange( double range )
        {
            return this._MeterRangeComboBox.SelectResistanceRangeCurrent( this.SenseResistanceSubsystem.ResistanceRangeCurrents.MatchResistanceRange( ( decimal ) range ) );
        }

        /// <summary> Selects the meter range based on the range mode description. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="range">   The range. </param>
        /// <param name="current"> The current. </param>
        /// <returns> A ResistanceRangeCurrent. </returns>
        public ResistanceRangeCurrent SelectMeterRange( double range, double current )
        {
            return this._MeterRangeComboBox.SelectResistanceRangeCurrent( this.SenseResistanceSubsystem.ResistanceRangeCurrents.MatchResistanceRange( ( decimal ) range, ( decimal ) current ) );
        }

        /// <summary> Gets or sets the range selection as read only. </summary>
        /// <value> The sentinel indicating if the meter range is read only. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public bool RangeReadOnly
        {
            get => this._MeterRangeComboBox.ReadOnly;

            set {
                if ( this.RangeReadOnly != value )
                {
                    this._MeterRangeComboBox.ReadOnly = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Executes the meter range changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resistanceRangeCurrent"> The resistance range current. </param>
        private void OnMeterRangeChanged( ResistanceRangeCurrent resistanceRangeCurrent )
        {
            if ( this.SenseResistanceSubsystem is null || this.SenseResistanceSubsystem.Range.HasValue && this.SenseResistanceSubsystem.Range.Value != ( double ) resistanceRangeCurrent.ResistanceRange )
            {
                // select a subsystem based on the range.
                if ( resistanceRangeCurrent.ResistanceRange <= this.Device.SenseResistanceFourWireSubsystem.ResistanceRangeCurrents.Last().ResistanceRange )
                {
                    this.BindSenseResistanceSubsystem( this.Device.SenseResistanceFourWireSubsystem );
                }
                else
                {
                    this.BindSenseResistanceSubsystem( this.Device.SenseResistanceSubsystem );
                }

                this.SenseResistanceSubsystem.ResistanceRangeCurrent = resistanceRangeCurrent;
            }
        }

        /// <summary>
        /// Event handler. Called by _MeterRangeComboBox for selected value changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void MeterRangeComboBox_SelectedValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.OnMeterRangeChanged( this._MeterRangeComboBox.SelectedResistanceRangeCurrent() );
        }

        #endregion

        #region " MEASURE "

        /// <summary> Gets or sets the resistance sense subsystem . </summary>
        /// <value> The Resistance Sense subsystem . </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public MeasureSubsystem MeasureSubsystem { get; private set; }

        /// <summary> Bind Measure subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindMeasureSubsystem( K2002Device device )
        {
            if ( this.MeasureSubsystem is object )
            {
                this.BindSubsystem( false, this.MeasureSubsystem );
                this.MeasureSubsystem = null;
            }

            if ( device is null )
            {
                this._ReadButton.Enabled = false;
                this._MeasureButton.Enabled = false;
            }
            else
            {
                this._ReadButton.Enabled = true;
                this._MeasureButton.Enabled = true;
                this.MeasureSubsystem = device.MeasureSubsystem;
                this.BindSubsystem( true, this.MeasureSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MeasureSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.MeasureSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K2002.MeasureSubsystem.PrimaryReadingValue ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.MeasureSubsystemPropertyChanged;
            }
        }

        /// <summary> Measure subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MeasureSubsystem sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K2002.MeasureSubsystem.PrimaryReadingValue ):
                    {
                        this._MeasuredValueTextBox.Text = sender.PrimaryReadingValue?.ToString( "G5" );
                        break;
                    }
            }
        }

        /// <summary> Measure subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeasureSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"Handling {typeof( MeasureSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MeasureSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"Handling {typeof( MeasureSubsystem )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as MeasureSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> The read label. </summary>
        private const string _ReadLabel = "Read";

        /// <summary> Reads button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( sender is Button button )
                {
                    this.Cursor = Cursors.WaitCursor;
                    activity = $"{this.Device.ResourceNameCaption} reading";
                    _ = this.PublishInfo( $"{activity};. {this.Device.ResourceNameCaption}" );
                    _ = this.Device.SystemSubsystem.QueryFrontTerminalsSelected();
                    _ = string.Equals( button.Text, _ReadLabel, StringComparison.OrdinalIgnoreCase )
                        ? this.MeasureSubsystem.Read()
                        : this.MeasureSubsystem.MeasurePrimaryReading();
                }
            }
            catch ( Exception ex )
            {
                this.Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                _ = this.Device.Session.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
