﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2002.Forms
{
    [DesignerGenerated()]
    public partial class MeasureUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _MeterRangeNumeric = new Core.Controls.NumericUpDown();
            _MeterCurrentNumeric = new Core.Controls.NumericUpDown();
            _MeterRangeNumericLabel = new System.Windows.Forms.Label();
            _MeterCurrentNumericLabel = new System.Windows.Forms.Label();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            __TriggerDelayNumeric = new Core.Controls.NumericUpDown();
            __TriggerDelayNumeric.ValueChanged += new EventHandler(TriggerDelayNumeric_ValueChanged);
            __ReadButton = new System.Windows.Forms.Button();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __MeasureButton = new System.Windows.Forms.Button();
            __MeasureButton.Click += new EventHandler(ReadButton_Click);
            _MeasuredValueTextBox = new System.Windows.Forms.TextBox();
            __MeterRangeComboBox = new Core.Controls.ComboBox();
            __MeterRangeComboBox.SelectedValueChanged += new EventHandler(MeterRangeComboBox_SelectedValueChanged);
            _MeterRangeComboBoxLabel = new System.Windows.Forms.Label();
            _Panel = new System.Windows.Forms.Panel();
            _InfoTextBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)_MeterRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_MeterCurrentNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__TriggerDelayNumeric).BeginInit();
            _Panel.SuspendLayout();
            SuspendLayout();
            // 
            // _MeterRangeNumeric
            // 
            _MeterRangeNumeric.BackColor = System.Drawing.SystemColors.Control;
            _MeterRangeNumeric.DecimalPlaces = 3;
            _MeterRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _MeterRangeNumeric.ForeColor = System.Drawing.SystemColors.WindowText;
            _MeterRangeNumeric.Location = new System.Drawing.Point(96, 43);
            _MeterRangeNumeric.Maximum = new decimal(new int[] { 1000000000, 0, 0, 0 });
            _MeterRangeNumeric.Name = "_MeterRangeNumeric";
            _MeterRangeNumeric.NullValue = new decimal(new int[] { 2000000, 0, 0, 0 });
            _MeterRangeNumeric.ReadOnly = true;
            _MeterRangeNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            _MeterRangeNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            _MeterRangeNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            _MeterRangeNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            _MeterRangeNumeric.Size = new System.Drawing.Size(107, 25);
            _MeterRangeNumeric.TabIndex = 3;
            _MeterRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _MeterRangeNumeric.Value = new decimal(new int[] { 2000000, 0, 0, 0 });
            // 
            // _MeterCurrentNumeric
            // 
            _MeterCurrentNumeric.BackColor = System.Drawing.SystemColors.Control;
            _MeterCurrentNumeric.DecimalPlaces = 7;
            _MeterCurrentNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _MeterCurrentNumeric.ForeColor = System.Drawing.SystemColors.WindowText;
            _MeterCurrentNumeric.Location = new System.Drawing.Point(285, 43);
            _MeterCurrentNumeric.Maximum = new decimal(new int[] { 1, 0, 0, 0 });
            _MeterCurrentNumeric.Name = "_MeterCurrentNumeric";
            _MeterCurrentNumeric.NullValue = new decimal(new int[] { 0, 0, 0, 0 });
            _MeterCurrentNumeric.ReadOnly = true;
            _MeterCurrentNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            _MeterCurrentNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            _MeterCurrentNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            _MeterCurrentNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            _MeterCurrentNumeric.Size = new System.Drawing.Size(98, 25);
            _MeterCurrentNumeric.TabIndex = 5;
            _MeterCurrentNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            _MeterCurrentNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _MeterRangeNumericLabel
            // 
            _MeterRangeNumericLabel.AutoSize = true;
            _MeterRangeNumericLabel.Location = new System.Drawing.Point(5, 47);
            _MeterRangeNumericLabel.Name = "_MeterRangeNumericLabel";
            _MeterRangeNumericLabel.Size = new System.Drawing.Size(88, 17);
            _MeterRangeNumericLabel.TabIndex = 2;
            _MeterRangeNumericLabel.Text = "Range [Ohm]:";
            _MeterRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _MeterCurrentNumericLabel
            // 
            _MeterCurrentNumericLabel.AutoSize = true;
            _MeterCurrentNumericLabel.Location = new System.Drawing.Point(209, 47);
            _MeterCurrentNumericLabel.Name = "_MeterCurrentNumericLabel";
            _MeterCurrentNumericLabel.Size = new System.Drawing.Size(74, 17);
            _MeterCurrentNumericLabel.TabIndex = 4;
            _MeterCurrentNumericLabel.Text = "Current [A]:";
            _MeterCurrentNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(21, 85);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(72, 17);
            _TriggerDelayNumericLabel.TabIndex = 6;
            _TriggerDelayNumericLabel.Text = "Delay [ms]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _TriggerDelayNumeric
            // 
            __TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            __TriggerDelayNumeric.Location = new System.Drawing.Point(95, 81);
            __TriggerDelayNumeric.Maximum = new decimal(new int[] { 200, 0, 0, 0 });
            __TriggerDelayNumeric.Name = "__TriggerDelayNumeric";
            __TriggerDelayNumeric.NullValue = new decimal(new int[] { 111, 0, 0, 0 });
            __TriggerDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __TriggerDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __TriggerDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __TriggerDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __TriggerDelayNumeric.Size = new System.Drawing.Size(59, 25);
            __TriggerDelayNumeric.TabIndex = 7;
            __TriggerDelayNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            __TriggerDelayNumeric.Value = new decimal(new int[] { 111, 0, 0, 0 });
            // 
            // _ReadButton
            // 
            __ReadButton.Location = new System.Drawing.Point(160, 79);
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(50, 28);
            __ReadButton.TabIndex = 8;
            __ReadButton.Text = "Read";
            __ReadButton.UseVisualStyleBackColor = true;
            // 
            // _MeasureButton
            // 
            __MeasureButton.Location = new System.Drawing.Point(213, 79);
            __MeasureButton.Name = "__MeasureButton";
            __MeasureButton.Size = new System.Drawing.Size(68, 28);
            __MeasureButton.TabIndex = 9;
            __MeasureButton.Text = "Measure";
            __MeasureButton.UseVisualStyleBackColor = true;
            // 
            // _MeasuredValueTextBox
            // 
            _MeasuredValueTextBox.BackColor = System.Drawing.Color.Black;
            _MeasuredValueTextBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeasuredValueTextBox.ForeColor = System.Drawing.Color.Aqua;
            _MeasuredValueTextBox.Location = new System.Drawing.Point(285, 81);
            _MeasuredValueTextBox.Name = "_MeasuredValueTextBox";
            _MeasuredValueTextBox.ReadOnly = true;
            _MeasuredValueTextBox.Size = new System.Drawing.Size(100, 25);
            _MeasuredValueTextBox.TabIndex = 10;
            _MeasuredValueTextBox.Text = "---.-";
            _MeasuredValueTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // _MeterRangeComboBox
            // 
            __MeterRangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __MeterRangeComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __MeterRangeComboBox.FormattingEnabled = true;
            __MeterRangeComboBox.Location = new System.Drawing.Point(54, 4);
            __MeterRangeComboBox.Name = "__MeterRangeComboBox";
            __MeterRangeComboBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control;
            __MeterRangeComboBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText;
            __MeterRangeComboBox.ReadWriteBackColor = System.Drawing.SystemColors.Window;
            __MeterRangeComboBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText;
            __MeterRangeComboBox.Size = new System.Drawing.Size(329, 25);
            __MeterRangeComboBox.TabIndex = 1;
            // 
            // _MeterRangeComboBoxLabel
            // 
            _MeterRangeComboBoxLabel.AutoSize = true;
            _MeterRangeComboBoxLabel.Location = new System.Drawing.Point(4, 8);
            _MeterRangeComboBoxLabel.Name = "_MeterRangeComboBoxLabel";
            _MeterRangeComboBoxLabel.Size = new System.Drawing.Size(48, 17);
            _MeterRangeComboBoxLabel.TabIndex = 0;
            _MeterRangeComboBoxLabel.Text = "Range:";
            _MeterRangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(__MeterRangeComboBox);
            _Panel.Controls.Add(_MeasuredValueTextBox);
            _Panel.Controls.Add(_TriggerDelayNumericLabel);
            _Panel.Controls.Add(__MeasureButton);
            _Panel.Controls.Add(_MeterCurrentNumericLabel);
            _Panel.Controls.Add(__ReadButton);
            _Panel.Controls.Add(__TriggerDelayNumeric);
            _Panel.Controls.Add(_MeterRangeNumericLabel);
            _Panel.Controls.Add(_MeterRangeComboBoxLabel);
            _Panel.Controls.Add(_MeterCurrentNumeric);
            _Panel.Controls.Add(_MeterRangeNumeric);
            _Panel.Dock = System.Windows.Forms.DockStyle.Top;
            _Panel.Location = new System.Drawing.Point(1, 1);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(428, 112);
            _Panel.TabIndex = 11;
            // 
            // _InfoTextBox
            // 
            _InfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            _InfoTextBox.Location = new System.Drawing.Point(1, 113);
            _InfoTextBox.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            _InfoTextBox.Multiline = true;
            _InfoTextBox.Name = "_InfoTextBox";
            _InfoTextBox.ReadOnly = true;
            _InfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            _InfoTextBox.Size = new System.Drawing.Size(428, 279);
            _InfoTextBox.TabIndex = 26;
            _InfoTextBox.Text = "<info>";
            // 
            // MeasureUI
            // 
            BackColor = System.Drawing.Color.Transparent;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_InfoTextBox);
            Controls.Add(_Panel);
            Name = "MeasureUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(430, 393);
            ((System.ComponentModel.ISupportInitialize)_MeterRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_MeterCurrentNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__TriggerDelayNumeric).EndInit();
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private Core.Controls.NumericUpDown _MeterRangeNumeric;
        private Core.Controls.NumericUpDown _MeterCurrentNumeric;
        private System.Windows.Forms.Label _MeterRangeNumericLabel;
        private System.Windows.Forms.Label _MeterCurrentNumericLabel;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
        private Core.Controls.NumericUpDown __TriggerDelayNumeric;

        private Core.Controls.NumericUpDown _TriggerDelayNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __TriggerDelayNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__TriggerDelayNumeric != null)
                {
                    __TriggerDelayNumeric.ValueChanged -= TriggerDelayNumeric_ValueChanged;
                }

                __TriggerDelayNumeric = value;
                if (__TriggerDelayNumeric != null)
                {
                    __TriggerDelayNumeric.ValueChanged += TriggerDelayNumeric_ValueChanged;
                }
            }
        }

        private Core.Controls.ComboBox __MeterRangeComboBox;

        private Core.Controls.ComboBox _MeterRangeComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeterRangeComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeterRangeComboBox != null)
                {
                    __MeterRangeComboBox.SelectedValueChanged -= MeterRangeComboBox_SelectedValueChanged;
                }

                __MeterRangeComboBox = value;
                if (__MeterRangeComboBox != null)
                {
                    __MeterRangeComboBox.SelectedValueChanged += MeterRangeComboBox_SelectedValueChanged;
                }
            }
        }

        private System.Windows.Forms.Label _MeterRangeComboBoxLabel;
        private System.Windows.Forms.Button __ReadButton;

        private System.Windows.Forms.Button _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.Button __MeasureButton;

        private System.Windows.Forms.Button _MeasureButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeasureButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click -= ReadButton_Click;
                }

                __MeasureButton = value;
                if (__MeasureButton != null)
                {
                    __MeasureButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.TextBox _MeasuredValueTextBox;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.TextBox _InfoTextBox;
    }
}