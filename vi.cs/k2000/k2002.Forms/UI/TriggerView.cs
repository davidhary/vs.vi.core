using System;
using System.Windows.Forms;

using isr.Core.SplitExtensions;

namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the basic <see cref="Facade.TriggerView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class TriggerView : Facade.TriggerView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public TriggerView() : base()
        {
            if ( !this.DesignMode )
            {
                this.ExternalScanMakeMenu();
            }
        }

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static new TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K2002Device K2002Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2002Device value )
        {
            base.AssignDevice( value );
            this.K2002Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 1 );
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 2 );
                this.BindSubsystem( ( TriggerSubsystemBase ) default, 1 );
            }
            else
            {
                this.BindSubsystem( this.K2002Device.ArmLayer1Subsystem, 1 );
                this.BindSubsystem( this.K2002Device.ArmLayer2Subsystem, 2 );
                this.BindSubsystem( this.K2002Device.TriggerSubsystem, 1 );
            }
        }

        #endregion

        #region " INITIATE WAIT FETCH "

        /// <summary> Initiate wait read. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override void InitiateWaitRead()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} clearing execution state";
                this.K2002Device.ClearExecutionState();
                activity = $"{this.K2002Device.ResourceNameCaption} initiating wait read";

                // Me.k2002device.ClearExecutionState()
                // set the service request
                // Me.k2002device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
                // Me.k2002device.StatusSubsystem.EnableServiceRequest(Me.k2002device.Session.DefaultOperationServiceRequestEnableBitmask)
                // Me.k2002device.Session.Write("*SRE 1") ' Set MSB bit of SRE register
                // Me.k2002device.Session.Write("stat:meas:ptr 32767; ntr 0; enab 512") ' Set all PTR bits and clear all NTR bits for measurement events Set Buffer Full bit of Measurement
                // Me.k2002device.Session.Write(":trac:feed calc") ' Select Calculate as reading source
                // Me.k2002device.Session.Write(":trac:poin 10")   ' Set buffer size to 10 points 
                // Me.k2002device.Session.Write(":trac:egr full")  ' Select Full element group

                // trigger the initiation of the measurement letting the triggering or service request do the rest.
                activity = $"{this.K2002Device.ResourceNameCaption} Initiating meter";
                _ = this.PublishVerbose( $"{activity};. " );
                this.K2002Device.TriggerSubsystem.Initiate();
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " EXTERNAL TRIGGER SCAN "

        /// <summary> External scan make menu. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ExternalScanMakeMenu()
        {
            var infoMenuItem = new ToolStripMenuItem() {
                Name = "ExternalScanInfoMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Info",
                ToolTipText = "Displays information about the external scan"
            };
            infoMenuItem.Click += this.ExternalScanInfoMenuItem_Click;
            var configureMenuItem = new ToolStripMenuItem() {
                Name = "ExternalScanConfigureMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Configure",
                ToolTipText = "Configures external scan"
            };
            configureMenuItem.Click += this.ExternalScanConfigureMenuItem_Click;
            var executeMenuItem = new ToolStripMenuItem() {
                Name = "ExternalScanExecuteMenuItem",
                Size = new System.Drawing.Size( 126, 22 ),
                Text = "Execute",
                ToolTipText = "Executes the external scan"
            };
            executeMenuItem.Click += this.ExternalScanExecuteMenuItem_Click;
            var mainMenuItem = new ToolStripMenuItem() {
                Name = "ExternalScanMenuItem",
                Size = new System.Drawing.Size( 192, 22 ),
                Text = "external scan...",
                ToolTipText = "Selects external scan options"
            };
            mainMenuItem.DropDownItems.AddRange( new ToolStripItem[] { infoMenuItem, configureMenuItem, executeMenuItem } );
            this.AddMenuItem( mainMenuItem );
        }

        /// <summary> Event handler. Called by ExternalScanInfoMenuItem for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExternalScanInfoMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} displaying {nameof( TriggerSubsystem.BuildExternalScanHelpMessage ).SplitWords()}";
                Core.Controls.PopupContainer.PopupInfo( this, TriggerSubsystem.BuildExternalScanHelpMessage( this.K2002Device.TriggerSubsystem ), this.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by ExternalScanConfigureMenuItem for click events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExternalScanConfigureMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( TriggerSubsystem.ConfigureExternalScan )}";
                var (Success, Details) = TriggerSubsystem.ConfigureExternalScan( this.K2002Device, this.TriggerCount, this.TriggerLayerSource, this.ArmLayer1Source );
                if ( !Success )
                {
                    activity = this.PublishWarning( $"failed {activity};. {Details}" );
                    _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
                }
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Event handler. Called by ExternalScanExecuteMenuItem for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ExternalScanExecuteMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K2002Device.ResourceNameCaption} {nameof( TriggerSubsystem.ExecuteExternalScan )}";
                var (Status, Value) = TriggerSubsystem.ExecuteExternalScan( this.K2002Device.Session );
                Core.Controls.PopupContainer.PopupInfo( this, $"{Status}{Environment.NewLine}{Value}", this.Location, this.Size );
            }
            catch ( Exception ex )
            {
                this.K2002Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

    }
}
