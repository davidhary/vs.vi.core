﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K2002.Forms
{
    [DesignerGenerated()]
    public partial class ChannelScanUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ChannelScanView = new ChannelScanView();
            SuspendLayout();
            // 
            // _ChannelScanView
            // 
            _ChannelScanView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _ChannelScanView.Dock = System.Windows.Forms.DockStyle.Fill;
            _ChannelScanView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _ChannelScanView.Location = new System.Drawing.Point(1, 1);
            _ChannelScanView.Name = "_ChannelScanView";
            _ChannelScanView.Padding = new System.Windows.Forms.Padding(1);
            _ChannelScanView.Size = new System.Drawing.Size(407, 272);
            _ChannelScanView.TabIndex = 0;
            // 
            // ChannelScanUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_ChannelScanView);
            Name = "ChannelScanUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(409, 274);
            ResumeLayout(false);
        }

        private ChannelScanView _ChannelScanView;
    }
}