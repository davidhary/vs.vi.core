using System;

namespace isr.VI.K2002.Forms
{

    /// <summary>
    /// A Keithley 2002 edition of the basic <see cref="Facade.BufferStreamView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class BufferStreamView : Facade.BufferStreamView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="BufferStreamView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="BufferStreamView"/>. </returns>
        public static new BufferStreamView Create()
        {
            BufferStreamView view = null;
            try
            {
                view = new BufferStreamView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K2002Device K2002Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K2002Device value )
        {
            base.AssignDevice( value );
            this.K2002Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( BinningSubsystemBase ) default );
                this.BindSubsystem( ( SenseFunctionSubsystemBase ) default );
                this.BindSubsystem( ( SenseSubsystemBase ) default );
                this.BindSubsystem( ( SystemSubsystemBase ) default );
                this.BindSubsystem( ( TraceSubsystemBase ) default );
                this.BindSubsystem( ( TriggerSubsystemBase ) default );
                this.BindSubsystem( ( DigitalOutputSubsystemBase ) default );
                this.BindSubsystem( ( RouteSubsystemBase ) default );
                this.BindSubsystem1( default );
                this.BindSubsystem2( default );
                this.BindSubsystem( default, string.Empty );
            }
            else
            {
                this.BindSubsystem( value.BinningSubsystem );
                this.BindSubsystem( value.SenseFunctionSubsystem );
                this.BindSubsystem( value.SenseSubsystem );
                this.BindSubsystem( value.SystemSubsystem );
                this.BindSubsystem( value.TraceSubsystem );
                this.BindSubsystem( value.TriggerSubsystem );
                this.BindSubsystem( value.DigitalOutputSubsystem );
                this.BindSubsystem1( value.ArmLayer1Subsystem );
                this.BindSubsystem2( value.ArmLayer2Subsystem );
                this.BindSubsystem( value.RouteSubsystem );
            }
        }

        #endregion

        #region " SENSE FUNCTION HANDLING "

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected override void HandleFunctionModesChanged( SenseSubsystemBase subsystem )
        {
            base.HandleFunctionModesChanged( subsystem );
            switch ( subsystem.FunctionMode )
            {
                case SenseFunctionModes.CurrentDC:
                    {
                        this.BindSubsystem( this.K2002Device.SenseCurrentSubsystem );
                        break;
                    }

                case SenseFunctionModes.VoltageDC:
                    {
                        this.BindSubsystem( this.K2002Device.SenseVoltageSubsystem );
                        break;
                    }

                case SenseFunctionModes.ResistanceFourWire:
                    {
                        this.BindSubsystem( this.K2002Device.SenseResistanceFourWireSubsystem );
                        break;
                    }

                case SenseFunctionModes.Resistance:
                    {
                        this.BindSubsystem( this.K2002Device.SenseResistanceSubsystem );
                        break;
                    }
            }
        }

        #endregion

        #region " BUFFER STREAM  "

        /// <summary> Configure trigger plan. </summary>
        /// <remarks> David, 2020-07-25. </remarks>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="sampleCount">   Number of samples. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        protected override void ConfigureTriggerPlan( int triggerCount, int sampleCount, TriggerSources triggerSource )
        {
            base.ConfigureTriggerPlan( triggerCount, sampleCount, triggerSource );
            _ = this.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = this.ArmLayer1Subsystem.ApplyArmCount( 1 );
            _ = this.ArmLayer1Subsystem.ApplyArmLayerBypassMode( TriggerLayerBypassModes.Acceptor );
            _ = this.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = this.ArmLayer2Subsystem.ApplyArmCount( sampleCount );
            _ = this.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
            _ = this.ArmLayer2Subsystem.ApplyArmLayerBypassMode( TriggerLayerBypassModes.Acceptor );
            _ = this.TriggerSubsystem.ApplyTriggerSource( triggerSource );
            _ = this.TriggerSubsystem.ApplyTriggerCount( triggerCount );
            _ = this.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
            _ = this.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Acceptor );
        }


        #endregion

    }
}
