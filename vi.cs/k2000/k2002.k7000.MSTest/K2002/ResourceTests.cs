using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{

    /// <summary> K2002 Device resource only unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2002" )]
    public partial class ResourceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " RESOURCE "

        /// <summary> (Unit Test Method) tests visa resource. </summary>
        /// <remarks> Finds the resource using the session factory resources manager. </remarks>
        [TestMethod()]
        public void VisaResourceTest()
        {
            DeviceTests.DeviceManager.AssertVisaResourceManagerIncludesResource( ResourceSettings.Get() );
        }

        /// <summary> (Unit Test Method) tests device resource. </summary>
        [TestMethod()]
        public void DeviceResourceTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            DeviceTests.DeviceManager.AssertResourceFound( device, ResourceSettings.Get() );
        }

        #endregion

        #region " TALKER "

        /// <summary> (Unit Test Method) tests Visa Session talker. </summary>
        /// <remarks> Checks if the device adds a trace message to a listener. </remarks>
        [TestMethod()]
        public void TalkerTest()
        {
            using var session = VisaSession.Create();
            DeviceTests.DeviceManager.AssertDeviceTraceMessageShouldBeQueued( TestInfo, session );
        }

        #endregion

        #region " SESSION TESTS: OPEN/CLOSE, STATUS "

        /// <summary> (Unit Test Method) tests session base. </summary>
        [TestMethod()]
        public void SessionBaseTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssetVisaSessionBaseShouldOpen( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionBaseShouldClose( TestInfo, device );
            }
        }

        /// <summary> Queries operation completion. </summary>
        /// <param name="len">             The length. </param>
        /// <param name="previousCommand"> The previous command. </param>
        /// <param name="sw">              The software. </param>
        /// <param name="session">         The session. </param>
        private static void QueryOperationCompletion( int len, string previousCommand, Stopwatch sw, Pith.SessionBase session )
        {
            string command = "*OPC?";
            string expectedOperationCompleteValue = "1";
            string actualOperationCompleteValue = session.QueryTrimEnd( command );
            Assert.AreEqual( expectedOperationCompleteValue, actualOperationCompleteValue, $"Expected operation completion after {previousCommand}" );
            TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
            sw.Restart();
        }

        /// <summary> Queries measurement enabled. </summary>
        /// <param name="len">     The length. </param>
        /// <param name="sw">      The software. </param>
        /// <param name="session"> The session. </param>
        private static void QueryMeasurementEnabled( int len, Stopwatch sw, Pith.SessionBase session )
        {
            TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} MEAS: Enable={session.QueryTrimEnd( ":stat:meas:ENAB?" )}; PTR={session.QueryTrimEnd( ":stat:meas:PTR?" )}. NTR: {session.QueryTrimEnd( ":stat:meas:NTR?" )}" );
            string Command = ":stat:meas:ENAB?; :stat:meas:PTR?; :stat:meas:NTR?";
            TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {Command}" );
            sw.Restart();
        }

        /// <summary> Queries measurement event. </summary>
        /// <param name="len">     The length. </param>
        /// <param name="sw">      The software. </param>
        /// <param name="session"> The session. </param>
        private static void QueryMeasurementEvent( int len, Stopwatch sw, Pith.SessionBase session )
        {
            string command = ":stat:meas:even?; :stat:meas:cond?;";
            TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} MEAS: Event={session.QueryTrimEnd( ":stat:meas:even?" )}. Cond={session.QueryTrimEnd( ":stat:meas:cond?" )}" );
            TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
            sw.Restart();
        }


        /// <summary> Queries measurement condition. </summary>
        /// <param name="len">     The length. </param>
        /// <param name="sw">      The software. </param>
        /// <param name="session"> The session. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static void QueryMeasurementCondition( int len, Stopwatch sw, Pith.SessionBase session )
        {
            string command = ":stat:meas:cond?";
            TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} MEAS: Condition={session.QueryTrimEnd( command )}." );
            TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
            sw.Restart();
        }

        /// <summary> (Unit Test Method) tests session base measure. </summary>
        /// <remarks>
        /// <list type=" bullet">2020-01-09 w/o OPC <item>
        /// MS: 823: Command: *RST                          </item><item>
        /// MS: 918: Command: :STAT:PRESET                  </item><item>
        /// MS: 919: MEAS: Enable=32767; PTR=32767. NTR: 0  </item><item>
        /// MS: 996: MEAS: Event=32.                        </item><item>
        /// MS: 1083: Command: ABORT                        </item><item>
        /// MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
        /// MEAS: Event=0.                                  </item><item>
        /// MS: 1145: Pre-iNIT                              </item><item>
        /// MS: 1255: Command: INIT                         </item><item>
        /// MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
        /// MS: 1306: Command: FETCH?                       </item><item>
        /// MEAS: Event=32. READING: +0.00400E-03           </item><item>
        /// MEAS: Event=0.                                  </item><item>
        /// MS: 1391: Pre-iNIT                              </item><item>
        /// MS: 1496: Command: INIT                         </item><item>
        /// MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
        /// MS: 1543: Command: FETCH?                       </item><item>
        /// MEAS: Event=32. READING: +0.00393E-03           </item><item>
        /// MEAS: Event=0.                                  </item><item>
        /// MS: 1624: Command: :CALC3:LIM:LOW 1; :CALC3:LIM:STAT ON</item><item>
        /// CALC3: STAT=1; LOW: 1.000000E+00                </item><item>
        /// MS: 1713: Pre-iNIT                              </item><item>
        /// MS: 1817: Command: INIT                         </item><item>
        /// MEAS: Enable=32767; PTR=32767. NTR: 0           </item><item>
        /// MS: 1860: Command: FETCH?                       </item><item>
        /// MEAS: Event=34. READING: +0.00383E-03           </item><item>
        /// MEAS: Event=0.                                  </item><item>
        /// </item></list>
        /// 
        /// <list type=" bullet">2020-01-09 w OPC <item>
        /// MS:   633: Open Session                                   </item><item>
        /// MS:     7: Command: *RST                                  </item><item>
        /// MS:    90: Command: *OPC?                                 </item><item>
        /// MS:     9: Command: :STAT:PRESET                          </item><item>
        /// MS:    19: Command: *OPC?                                 </item><item>
        /// MEAS: Enable=32767; PTR=32767. NTR: 0      </item><item>
        /// MS:    48: Command: :stat:meas:ENAB?; :stat:meas:PTR?; :stat:meas:NTR? </item><item>
        /// MEAS: Event=0. Cond=0                      </item><item>
        /// MS:    29: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// MS:     4: Command: *CLS                                  </item><item>
        /// MS:    13: Command: *OPC?                                 </item><item>
        /// MS:     4: Command: :ABOR                                 </item><item>
        /// MS:    20: Command: *OPC?                                 </item><item>
        /// MEAS: Enable=32767; PTR=32767. NTR: 0      </item><item>
        /// MS:    46: Command: :stat:meas:ENAB?; :stat:meas:PTR?; :stat:meas:NTR? </item><item>
        /// MEAS: Event=0. Cond=0                      </item><item>
        /// MS:    31: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// MS:     4: Command: :INIT                                 </item><item>
        /// MS:   164: MEAS: Event=32                                 </item><item>
        /// MEAS: Event=0. Cond=0. READING: +0.00029E-03            </item><item>
        /// MS:    48: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
        /// MEAS: Event=0. Cond=0                      </item><item>
        /// MS:    29: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// MS:     3: Command: :INIT                                 </item><item>
        /// MS:    99: MEAS: Event=32                                 </item><item>
        /// MEAS: Event=0. Cond=0. READING: +0.00047E-03            </item><item>
        /// MS:    48: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
        /// MEAS: Event=0. Cond=0                      </item><item>
        /// MS:    31: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// MS:    11: Command: :CALC3:LIM:LOW 1; :CALC3:LIM:STAT ON  </item><item>
        /// MS:    24: Command: *OPC?                                 </item><item>
        /// CALC3: STAT=1; LOW: 1.000000E+00           </item><item>
        /// MS:    34: Command: :CALC3:LIM:STAT?; :CALC3:LIM:LOW?     </item><item>
        /// MS:     3: Command: :INIT                                 </item><item>
        /// MS:   101: MEAS: Event=34                                 </item><item>
        /// MEAS: Event=0. Cond=2. READING: +0.00080E-03            </item><item>
        /// MS:    44: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
        /// MEAS: Event=0. Cond=2                      </item><item>
        /// MS:    32: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// MS:     5: Command: :CALC3:LIM:STAT OFF                   </item><item>
        /// MS:    17: Command: *OPC?                                 </item><item>
        /// CALC3: STAT=0                              </item><item>
        /// MS:    14: Command: :CALC3:LIM:STAT?                      </item><item>
        /// MS:     4: Command: :INIT                                 </item><item>
        /// MS:    95: MEAS: Event=32                                 </item><item>
        /// MEAS: Event=0. Cond=0. READING: +0.00088E-03            </item><item>
        /// MS:    47: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?        </item><item>
        /// MEAS: Event=0. Cond=0                      </item><item>
        /// MS:    29: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// MS:     3: Command: :INIT                                 </item><item>
        /// MS:    95: MEAS: Event=32                                 </item><item>
        /// MEAS: Event=0. Cond=0. READING: +0.00096E-03      </item><item>
        /// MS:    46: Command: :stat:meas:even?; :stat:meas:cond?; :FETCH?  </item><item>
        /// MEAS: Event=0. Cond=0                      </item><item>
        /// MS:    28: Command: :stat:meas:even?; :stat:meas:cond?;   </item><item>
        /// </item></list>
        /// </remarks>
        [TestMethod()]
        public void SessionBaseMeasureTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            var sw = Stopwatch.StartNew();
            int len = 5;
            try
            {
                DeviceTests.DeviceManager.AssetVisaSessionBaseShouldOpen( TestInfo, device, ResourceSettings.Get() );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Open Session" );
                sw.Restart();
                // MEAS (POWER ON): Enable=0; PTR=32767. NTR: 0
                // MEAS (PRESET): Enable=32767; PTR=32767. NTR: 0
                string command = "*RST";
                _ = device.Session.WriteLine( command );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                sw.Restart();
                QueryOperationCompletion( len, command, sw, device.Session );
                command = ":STAT:PRESET";
                _ = device.Session.WriteLine( command );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                sw.Restart();
                QueryOperationCompletion( len, command, sw, device.Session );
                QueryMeasurementEnabled( len, sw, device.Session );
                QueryMeasurementEvent( len, sw, device.Session );
                command = "*CLS";
                _ = device.Session.WriteLine( command );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                sw.Restart();
                QueryOperationCompletion( len, command, sw, device.Session );
                command = ":CALC3:LIM:CLE:AUTO ON";
                _ = device.Session.WriteLine( command );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                sw.Restart();
                QueryOperationCompletion( len, command, sw, device.Session );
                command = ":CALC3:LIM:CLE:AUTO?";
                TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} CALC3: AUTO={device.Session.QueryTrimEnd( command )}" );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                sw.Restart();
                command = ":ABOR";
                _ = device.Session.WriteLine( command );
                TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                sw.Restart();
                QueryOperationCompletion( len, command, sw, device.Session );
                QueryMeasurementEnabled( len, sw, device.Session );
                QueryMeasurementEvent( len, sw, device.Session );
                int? measEvent;
                bool timedOut;
                for ( int i = 1; i <= 5; i++ )
                {
                    if ( i == 1 )
                    {
                        command = ":CALC3:LIM:STAT OFF";
                        _ = device.Session.WriteLine( command );
                        TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                        sw.Restart();
                        QueryOperationCompletion( len, command, sw, device.Session );
                        command = ":CALC3:LIM:STAT?";
                        TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} CALC3: STAT={device.Session.QueryTrimEnd( ":CALC3:LIM:STAT?" )}" );
                        TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                        sw.Restart();
                    }

                    command = ":INIT";
                    _ = device.Session.WriteLine( command );
                    TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                    sw.Restart();
                    var task = Pith.SessionBase.StartAwaitingBitmaskTask( 32, TimeSpan.FromMilliseconds( 400d ), () => Convert.ToInt32( device.Session.QueryTrimEnd( ":stat:meas:even?" ) ) );
                    task.Wait();
                    measEvent = task.Result.StatusByte;
                    timedOut = task.Result.TimedOut;
                    TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: MEAS: Event={measEvent}" );
                    sw.Restart();
                    Assert.IsFalse( timedOut, "Measurement timed out" );
                    sw.Restart();
                    command = ":stat:meas:even?; :stat:meas:cond?; :FETCH?";
                    TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} MEAS: Event={device.Session.QueryTrimEnd( ":stat:meas:even?" )}. Cond={device.Session.QueryTrimEnd( ":stat:meas:cond?" )}. READING: {device.Session.QueryTrimEnd( ":FETCH?" )}" );
                    TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                    sw.Restart();
                    QueryMeasurementEvent( len, sw, device.Session );
                    if ( i == 2 )
                    {
                        command = ":CALC3:LIM:LOW 1; :CALC3:LIM:STAT ON";
                        _ = device.Session.WriteLine( command );
                        TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                        sw.Restart();
                        QueryOperationCompletion( len, command, sw, device.Session );
                        command = ":CALC3:LIM:STAT?; :CALC3:LIM:LOW?";
                        TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} CALC3: STAT={device.Session.QueryTrimEnd( ":CALC3:LIM:STAT?" )}; LOW: {device.Session.QueryTrimEnd( ":CALC3:LIM:LOW?" )}" );
                        TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                        sw.Restart();
                    }
                    else if ( i == 3 )
                    {
                        command = ":CALC3:LIM:STAT OFF";
                        _ = device.Session.WriteLine( command );
                        TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                        sw.Restart();
                        QueryOperationCompletion( len, command, sw, device.Session );
                        command = ":CALC3:LIM:STAT?";
                        TestInfo.TraceMessage( $"{" ".PadLeft( 3 * len, ' ' )} CALC3: STAT={device.Session.QueryTrimEnd( ":CALC3:LIM:STAT?" )}" );
                        TestInfo.TraceMessage( $"MS: {sw.ElapsedMilliseconds.ToString().PadLeft( len, ' ' )}: Command: {command}" );
                        sw.Restart();
                    }
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceTests.DeviceManager.AssertVisaSessionBaseShouldClose( TestInfo, device );
            }
        }


        /// <summary> Gets the function. </summary>
        /// <exception cref="NotImplementedException"> Thrown when the requested operation is
        /// unimplemented. </exception>
        /// <returns> A Func(Of T) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private Func<T> Func<T>()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region " DEVICE TESTS: OPEN, CLOSE, CHECK SUSBSYSTEMS "

        /// <summary> (Unit Test Method) tests open session. </summary>
        /// <remarks> Tests opening and closing a VISA session. </remarks>
        [TestMethod()]
        public void OpenSessionTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " DEVICE EVENT TESTS "

        /// <summary> (Unit Test Method) handles the service request test. </summary>
        [TestMethod()]
        public void HandleServiceRequestTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertHandlingServiceRequest( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) tests device handle service request. </summary>
        /// <remarks>
        /// This test will fail the first time it is run if Windows requests access through the Firewall.
        /// </remarks>
        [TestMethod()]
        public void DeviceHandleServiceRequestTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertDeviceHandlingServiceRequest( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) tests device polling. </summary>
        [TestMethod()]
        public void DevicePollingTest()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.AssertDevicePolling( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
