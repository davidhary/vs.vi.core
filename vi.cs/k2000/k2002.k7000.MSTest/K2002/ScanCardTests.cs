using System;
using System.Collections.Generic;
using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{

    /// <summary> K2002 Scan Card unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2001ScanCard" )]
    public class ScanCardTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( K2002Properties.K2002ResourceInfo.Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( K2002Properties.K2002SubsystemsInfo.Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " SCAN CARD TESTS "

        /// <summary> (Unit Test Method) scans the card internal scan function test. </summary>
        [TestMethod()]
        public void ScanCardInternalScanFunctionTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertScanCardInstalledShouldMatch( device.SystemSubsystem, K2002Properties.K2002SubsystemsInfo );
                DeviceTests.DeviceManager.AssertScanCardInternalScanShouldSet( device.RouteSubsystem, device.SystemSubsystem, K2002Properties.K2002SubsystemsInfo );
                _ = device.Session.ReadStatusRegister();
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> Assert scan card four wire resistance scan result. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        /// <param name="result">           The result. </param>
        /// <param name="elapsedTimeRange"> The elapsed time range. </param>
        /// <param name="errorBitmask">     The error bitmask. </param>
        private static void AssertScanCardKelvinScanResult( (string Status, string Reading, TimeSpan Elapsed) result, Core.Primitives.RangeTimeSpan elapsedTimeRange, int errorBitmask )
        {
            int expectedStatus = 0;
            int actualStatus = int.Parse( result.Status );
            Assert.AreEqual( expectedStatus, actualStatus & errorBitmask, $"{nameof( result.Status )} errors bits {errorBitmask:X2} should be off" );
            Assert.IsFalse( string.IsNullOrWhiteSpace( result.Reading ), $"{nameof( result.Reading )} must not be empty" );
            IEnumerable<string> values = result.Reading.Split( ',' );
            int expectedCount = 3;
            Assert.AreEqual( expectedCount, values.Count(), $"{nameof( result.Reading )} must have the expected number of elements" );
            Assert.IsTrue( elapsedTimeRange.Contains( result.Elapsed ), $"Elapsed time {result.Elapsed.TotalMilliseconds:0}ms should be within {elapsedTimeRange.ToString( @"ss\.fff" )}" );
        }

        /// <summary> (Unit Test Method) tests the scan card four wire resistance scan. </summary>
        /// <remarks> David, 2020-04-06. </remarks>
        [TestMethod()]
        public void ScanCardKelvinScanTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                if ( device.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                    Assert.Inconclusive( $"Rear terminal must be selected" );
                _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device.Session, "IMM", 1, TimeSpan.FromMilliseconds( 800d ) );
                K2002.RouteSubsystem.InitiateFourWireResistanceScan( device.Session, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                var result = K2002.RouteSubsystem.FetchFourWireResistanceScan( device.Session, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                TestInfo.TraceMessage( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                int errorBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.FailuresSummary];
                AssertScanCardKelvinScanResult( result, new Core.Primitives.RangeTimeSpan( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 1000d ) ), errorBitmask );
                _ = device.Session.ReadStatusRegister();
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) scans the card four wire resistance timer scan test. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        [TestMethod()]
        public void ScanCardKelvinTimerScanTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                if ( device.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                    Assert.Inconclusive( $"Rear terminal must be selected" );
                int triggerCount = 10;
                _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device.Session, "TIM", triggerCount, TimeSpan.FromMilliseconds( 800d ) );
                K2002.RouteSubsystem.InitiateFourWireResistanceScan( device.Session, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                while ( triggerCount > 0 )
                {
                    var result = K2002.RouteSubsystem.FetchFourWireResistanceScan( device.Session, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                    TestInfo.TraceMessage( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                    int errorBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.FailuresSummary];
                    AssertScanCardKelvinScanResult( result, new Core.Primitives.RangeTimeSpan( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 1000d ) ), errorBitmask );
                    triggerCount -= 1;
                    K2002.RouteSubsystem.ClearBuffer( device.Session );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>
        /// (Unit Test Method) scans the card four wire resistance bus trigger scan test.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        [TestMethod()]
        public void ScanCardKelvinBusTriggerScanTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                if ( device.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                    Assert.Inconclusive( $"Rear terminal must be selected" );
                int triggerCount = 10;
                _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device.Session, "BUS", triggerCount, TimeSpan.FromMilliseconds( 800d ) );
                K2002.RouteSubsystem.InitiateFourWireResistanceScan( device.Session, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                while ( triggerCount > 0 )
                {
                    _ = device.Session.WriteLine( "*TRG" );
                    var result = K2002.RouteSubsystem.FetchFourWireResistanceScan( device.Session, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                    K2002.RouteSubsystem.ClearBuffer( device.Session );
                    TestInfo.TraceMessage( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                    int errorBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.FailuresSummary];
                    AssertScanCardKelvinScanResult( result, new Core.Primitives.RangeTimeSpan( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 1000d ) ), errorBitmask );
                    triggerCount -= 1;
                    Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 200d ) );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) scans the card four wire resistance scan library test. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        [TestMethod()]
        public void ScanCardKelvinScanLibraryTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                if ( device.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                    Assert.Inconclusive( $"Rear terminal must be selected" );
                _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device, ArmSources.Immediate, 1, TimeSpan.FromMilliseconds( 600d ) );
                K2002.RouteSubsystem.InitiateFourWireResistanceScan( device, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                (string Status, string Reading, TimeSpan Elapsed) result = K2002.RouteSubsystem.FetchFourWireResistanceScan( device );
                TestInfo.TraceMessage( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                int errorBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.FailuresSummary];
                AssertScanCardKelvinScanResult( result, new Core.Primitives.RangeTimeSpan( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 1000d ) ), errorBitmask );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>
        /// (Unit Test Method) scans the card four wire resistance timer library scan test.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        [TestMethod()]
        public void ScanCardKelvinTimerLibraryScanTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                if ( device.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                    Assert.Inconclusive( $"Rear terminal must be selected" );
                int triggerCount = 10;
                _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device, ArmSources.Timer, triggerCount, TimeSpan.FromMilliseconds( 600d ) );
                K2002.RouteSubsystem.InitiateFourWireResistanceScan( device, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                while ( triggerCount > 0 )
                {
                    (string Status, string Reading, TimeSpan Elapsed) result = K2002.RouteSubsystem.FetchFourWireResistanceScan( device );
                    TestInfo.TraceMessage( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                    int errorBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.FailuresSummary];
                    AssertScanCardKelvinScanResult( result, new Core.Primitives.RangeTimeSpan( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 1000d ) ), errorBitmask );
                    _ = device.Session.ReadStatusRegister();
                    triggerCount -= 1;
                    K2002.RouteSubsystem.ClearBuffer( device.Session );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary>
        /// (Unit Test Method) scans the card four wire resistance bus trigger library scan test.
        /// </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        [TestMethod()]
        public void ScanCardKelvinBusTriggerLibraryScanTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                if ( device.SystemSubsystem.QueryFrontTerminalsSelected().Value )
                    Assert.Inconclusive( $"Rear terminal must be selected" );
                int triggerCount = 10;
                _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device, ArmSources.Bus, triggerCount, TimeSpan.FromMilliseconds( 600d ) );
                K2002.RouteSubsystem.InitiateFourWireResistanceScan( device, device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferFull] );
                while ( triggerCount > 0 )
                {
                    _ = device.Session.WriteLine( "*TRG" );
                    (string Status, string Reading, TimeSpan Elapsed) result = K2002.RouteSubsystem.FetchFourWireResistanceScan( device );
                    TestInfo.TraceMessage( $"status: {result.Status}; elapsed: {result.Elapsed.TotalMilliseconds:0}ms; reading: {result.Reading}" );
                    int errorBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.FailuresSummary];
                    AssertScanCardKelvinScanResult( result, new Core.Primitives.RangeTimeSpan( TimeSpan.FromMilliseconds( 10d ), TimeSpan.FromMilliseconds( 1000d ) ), errorBitmask );
                    _ = device.Session.ReadStatusRegister();
                    Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 250d ) );
                    triggerCount -= 1;
                    K2002.RouteSubsystem.ClearBuffer( device.Session );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }


        #endregion

    }
}
