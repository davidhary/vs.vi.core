using System;
using System.Collections.Generic;
using System.Diagnostics;

using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{

    /// <summary> K2002 Subsystems unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2002" )]
    public class SubsystemsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( K2002Properties.K2002ResourceInfo.Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( K2002Properties.K2002SubsystemsInfo.Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " STATUS SUSBSYSTEM "

        /// <summary> Opens session check status. </summary>
        /// <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
        /// <param name="resourceInfo">     Information describing the resource. </param>
        /// <param name="subsystemsInfo">   Information describing the subsystems. </param>
        private static void OpenSessionCheckStatus( bool readErrorEnabled, ResourceSettings resourceInfo, SubsystemsSettings subsystemsInfo )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            DeviceTests.DeviceManager.AssertSessionInitialValues( device.Session, resourceInfo, subsystemsInfo );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, resourceInfo );
                DeviceTests.DeviceManager.AssertDeviceModel( device.StatusSubsystemBase, resourceInfo );
                DeviceTests.DeviceManager.AssertDeviceErrors( device.StatusSubsystemBase, subsystemsInfo );
                DeviceTests.DeviceManager.AssertTerminationValues( device.Session, subsystemsInfo );
                DeviceTests.DeviceManager.AssertLineFrequency( device.StatusSubsystem, subsystemsInfo );
                DeviceTests.DeviceManager.AssertIntegrationPeriod( device.StatusSubsystem, subsystemsInfo );
                DeviceTests.DeviceManager.AssertInitialSubsystemValues( device.MeasureSubsystem, subsystemsInfo );
                DeviceTests.DeviceManager.AssertInitialSubsystemValues( device.SenseSubsystem, subsystemsInfo );
                DeviceTests.DeviceManager.AssertClearSessionDeviceErrors( device, subsystemsInfo );
                if ( readErrorEnabled )
                    DeviceTests.DeviceManager.AssertReadingDeviceErrors( device, subsystemsInfo );
                DeviceTests.DeviceManager.AssertOrphanMessages( device.StatusSubsystemBase );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> A test for Open Session and status. </summary>
        [TestMethod()]
        public void OpenSessionCheckStatusTest()
        {
            OpenSessionCheckStatus( false, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        /// <summary> (Unit Test Method) tests open session read device errors. </summary>
        [TestMethod()]
        public void OpenSessionReadDeviceErrorsTest()
        {
            OpenSessionCheckStatus( true, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        #endregion

        #region " SENSE SUBSYSTEM TESTS "

        /// <summary> (Unit Test Method) tests read sense subsystem. </summary>
        [TestMethod()]
        public void ReadSenseSubsystemTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertInitialSubsystemValues( device.SenseSubsystem, K2002Properties.K2002SubsystemsInfo );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) tests toggle sense subsystem function mode. </summary>
        /// <remarks> David, 2020-07-29. </remarks>
        [TestMethod()]
        public void ToggleSenseSubsystemFunctionModeTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                DeviceTests.DeviceManager.ToggleFunctionMode( device.SenseSubsystem, SenseFunctionModes.ResistanceFourWire, SenseFunctionModes.Resistance );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " MEASURE TESTS "

        /// <summary> (Unit Test Method) tests measure subsystem fetch. </summary>
        [TestMethod()]
        public void MeasureSubsystemFetchTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            string propertyName;
            var sw = Stopwatch.StartNew();
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                propertyName = nameof( VisaSessionBase.ResetKnownState ).SplitWords();
                device.Session.ResetKnownState();
                Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed after {propertyName}" );
                propertyName = nameof( StatusSubsystemBase.PresetKnownState ).SplitWords();
                device.StatusSubsystem.PresetKnownState();
                Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed after {propertyName}" );
                propertyName = nameof( StatusSubsystemBase.MeasurementEventEnableBitmask ).SplitWords();
                Assert.AreEqual( 0x7FFF, device.StatusSubsystem.QueryMeasurementEventEnableBitmask().GetValueOrDefault( 0 ), $"{propertyName} bits should all be set" );
                propertyName = nameof( StatusSubsystemBase.MeasurementEventPositiveTransitionBitmask ).SplitWords();
                Assert.AreEqual( 0x7FFF, device.StatusSubsystem.QueryMeasurementEventPositiveTransitionBitmask().GetValueOrDefault( 0 ), $"{propertyName} bits should all be set" );
                propertyName = nameof( StatusSubsystemBase.MeasurementEventNegativeTransitionBitmask ).SplitWords();
                Assert.AreEqual( 0, device.StatusSubsystem.QueryMeasurementEventNegativeTransitionBitmask().GetValueOrDefault( 0 ), $"{propertyName} bits should all be cleared" );
                propertyName = nameof( VisaSessionBase.ClearExecutionState ).SplitWords();
                device.Session.ClearExecutionState();
                Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed after {propertyName}" );
                propertyName = nameof( BinningSubsystemBase.Limit1AutoClear ).SplitWords();
                Assert.IsTrue( device.BinningSubsystem.ApplyLimit1AutoClear( true ).GetValueOrDefault( false ), $"{propertyName} value should match" );
                propertyName = nameof( TriggerSubsystemBase.Abort ).SplitWords();
                device.TriggerSubsystem.Abort();
                Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed after {propertyName}" );
                propertyName = nameof( TriggerSubsystemBase.Initiate ).SplitWords();
                var initialTimestamp = DateTimeOffset.Now;
                sw.Restart();
                device.TriggerSubsystem.Initiate();
                Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 100d ) );
                propertyName = MeasurementEventBitmaskKey.ReadingAvailable.ToString().SplitWords();
                bool timedOut;
                int? actualBitmask;
                int expectedBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.ReadingAvailable];
                var task = Pith.SessionBase.StartAwaitingBitmaskTask( expectedBitmask, TimeSpan.FromMilliseconds( 200d ), () => device.StatusSubsystem.QueryMeasurementEventStatus() );
                timedOut = task.Result.TimedOut;
                actualBitmask = task.Result.StatusByte;
                int stepNumber = 1;
                Assert.IsFalse( timedOut, $"Step#{stepNumber}: {propertyName} should not timeout" );
                propertyName = nameof( StatusSubsystemBase.MeasurementEventStatus ).SplitWords();
                Assert.AreEqual( expectedBitmask, ( object ) actualBitmask, $"Step#{stepNumber}: {propertyName} should match" );

                // the traced (fetched) buffer includes only status values.
                device.TraceSubsystem.OrderedReadingElementTypes = new List<ReadingElementTypes>() { ReadingElementTypes.Reading };
                int actualBufferReadingsCount = device.TraceSubsystem.AddBufferReadings( device.MeasureSubsystem.FunctionUnit, initialTimestamp, sw.Elapsed );
                Assert.AreEqual( 1, actualBufferReadingsCount, $"Step#{stepNumber}: Buffer reading counts should match" );
                int readingNumber = 0;
                IEnumerable<BufferReading> bufferReadings = device.TraceSubsystem.DequeueRange( device.TraceSubsystem.NewReadingsCount );

                // build the buffer readings amounts
                foreach ( BufferReading bufferReading in bufferReadings )
                {
                    readingNumber += 1;
                    // ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Reading}")
                    bufferReading.BuildAmount( device.SenseSubsystem.FunctionUnit );
                    TestInfo.TraceMessage( $"Step#{stepNumber} Reading #{readingNumber} MEAS: Event={device.Session.QueryTrimEnd( ":stat:meas:even?" )}. Cond={device.Session.QueryTrimEnd( ":stat:meas:cond?" )}" );
                    TestInfo.TraceMessage( $"Step#{stepNumber} Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4}" );
                }

                propertyName = nameof( BufferReading ).SplitWords();
                double epsilon = 0.001d;
                expectedBitmask = 0;
                readingNumber = 0;
                foreach ( BufferReading bufferReading in bufferReadings )
                {
                    readingNumber += 1;
                    Assert.AreEqual( 0d, bufferReading.Amount.Value, epsilon, $"Step#{stepNumber} Reading #{readingNumber} {propertyName} value should match" );
                    Assert.AreEqual( expectedBitmask, bufferReading.StatusWord, $"Step#{stepNumber} Reading #{readingNumber} {nameof( BufferReading.StatusWord )} should match" );
                }

                propertyName = nameof( BinningSubsystemBase.Limit1LowerLevel ).SplitWords();
                double expectedLowLevel = 1d;
                double actualLowLevel = device.BinningSubsystem.ApplyLimit1LowerLevel( expectedLowLevel ).GetValueOrDefault( 0d );
                Assert.AreEqual( expectedLowLevel, actualLowLevel, $"{propertyName} value should match" );
                propertyName = nameof( BinningSubsystemBase.Limit1Enabled ).SplitWords();
                Assert.IsTrue( device.BinningSubsystem.ApplyLimit1Enabled( true ).GetValueOrDefault( false ), $"{propertyName} value should match" );
                propertyName = nameof( TriggerSubsystemBase.Abort ).SplitWords();
                device.TriggerSubsystem.Abort();
                Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed after {propertyName}" );
                initialTimestamp = DateTimeOffset.Now;
                sw.Restart();
                propertyName = nameof( TriggerSubsystemBase.Initiate ).SplitWords();
                device.TriggerSubsystem.Initiate();
                expectedBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.ReadingAvailable];
                propertyName = MeasurementEventBitmaskKey.ReadingAvailable.ToString().SplitWords();
                task = Pith.SessionBase.StartAwaitingBitmaskTask( expectedBitmask, TimeSpan.FromMilliseconds( 200d ), () => device.StatusSubsystem.QueryMeasurementEventStatus() );
                timedOut = task.Result.TimedOut;
                actualBitmask = task.Result.StatusByte;
                stepNumber += 1;
                Assert.IsFalse( timedOut, $"Step#{stepNumber}: {propertyName} should not timeout" );
                actualBufferReadingsCount = device.TraceSubsystem.AddBufferReadings( device.MeasureSubsystem.FunctionUnit, initialTimestamp, sw.Elapsed );
                Assert.AreEqual( 1, actualBufferReadingsCount, $"Step#{stepNumber}: Buffer reading counts should match" );
                readingNumber = 0;
                bufferReadings = device.TraceSubsystem.DequeueRange( device.TraceSubsystem.NewReadingsCount );

                // build the buffer readings amounts
                foreach ( BufferReading bufferReading in bufferReadings )
                {
                    readingNumber += 1;
                    // ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Reading}")
                    bufferReading.BuildAmount( device.SenseSubsystem.FunctionUnit );
                    TestInfo.TraceMessage( $"Step#{stepNumber} Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4}" );
                }

                propertyName = nameof( BufferReading ).SplitWords();
                readingNumber = 0;
                expectedBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.LowLimit1];
                foreach ( BufferReading bufferReading in bufferReadings )
                {
                    readingNumber += 1;
                    Assert.AreEqual( 0d, bufferReading.Amount.Value, epsilon, $"Step#{stepNumber} Reading #{readingNumber} {propertyName} value should match" );
                    Assert.AreEqual( expectedBitmask, bufferReading.StatusWord, $"Step#{stepNumber} Reading #{readingNumber} {nameof( BufferReading.StatusWord )} should match" );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }


        #endregion

        #region " DIGITAL OUTPUT SUBSYSTEM TEST "

        /// <summary> (Unit Test Method) tests toggle digital output polarity. </summary>
        [TestMethod()]
        public void ToggleDigitalOutputPolarityTest()
        {
            using var device = K2002.K2002Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDigitalOutputSignalPolarity( device.DigitalOutputSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
