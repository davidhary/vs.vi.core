namespace isr.VI.K2002Tests
{

    /// <summary> The Subsystems Test Information. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class SubsystemsSettings : DeviceTests.SubsystemsSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        private SubsystemsSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(SubsystemsSettings)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static SubsystemsSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static SubsystemsSettings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (SubsystemsSettings)Synchronized(new SubsystemsSettings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " ROUTE SUBSYSTEM "

        /// <summary> Gets or sets the Initial scan list settings. </summary>
        /// <value> The initial scan list settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("(@1,2,3,4,5,6,7,8,9,10)")]
        public override string InitialScanList
        {
            get
            {
                return base.InitialScanList;
            }

            set
            {
                base.InitialScanList = value;
            }
        }

        /// <summary> Gets or sets the scan card installed. </summary>
        /// <value> The scan card installed. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public override bool ScanCardInstalled
        {
            get
            {
                return base.ScanCardInstalled;
            }

            set
            {
                base.ScanCardInstalled = value;
            }
        }


        #endregion

    }

    internal static partial class K2002Properties
    {

        /// <summary> Gets information describing the Keithley 2002 subsystems. </summary>
        /// <value> Information describing the Keithley 2002 subsystems. </value>
        public static SubsystemsSettings K2002SubsystemsInfo => SubsystemsSettings.Get();
    }
}
