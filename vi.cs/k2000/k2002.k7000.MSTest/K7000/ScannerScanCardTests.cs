using System;
using System.Collections.Generic;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{

    /// <summary> K7000 Scanner and Multimeter Scan Card unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2002-1K7001" )]
    public class ScannerScanCardTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            _TestSite?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( K2002Properties.K2002ResourceInfo.Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( K2002Properties.K2002SubsystemsInfo.Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            Assert.IsTrue( K7000Properties.K7000SubsystemInfo.Exists, $"{typeof( K7000ResourceSettings )} settings should exist" );
            Assert.IsTrue( K7000Properties.K7000SubsystemInfo.Exists, $"{typeof( ScannerMeterSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " BUS TRIGGERS BUFFER SCAN CARD "

        /// <summary> Configure four wire resistance scan. </summary>
        /// <param name="device">             The device. </param>
        /// <param name="bufferReadingCount"> Number of buffer readings. </param>
        public static void ConfigureFourWireResistanceScan( K2002.K2002Device device, int bufferReadingCount )
        {
            _ = K2002.RouteSubsystem.ConfigureFourWireResistanceScan( device, ScannerMeterSettings.Get().ScanCardScanList, ScannerMeterSettings.Get().ScanCardSampleCount, ScannerMeterSettings.Get().MeterArmSource, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, TimeSpan.FromMilliseconds( 600d ) );
            // configure the buffer to hold all the readings.
            _ = device.TraceSubsystem.ApplyPointsCount( bufferReadingCount );

            // the fetched buffer includes only reading values.
            device.TraceSubsystem.OrderedReadingElementTypes = new List<ReadingElementTypes>() { ReadingElementTypes.Reading };
        }

        /// <summary> (Unit Test Method) tests bus triggers buffer scan card trace. </summary>
        /// <remarks>
        /// <list type="bullet">SRQ and Operation Event Status 2020/04/09<item>
        /// SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post   </item><item>
        /// #1: SRQ: 0xA0 0xA1 OSB: 0x0040 0x0030 MSB: 0x0000 0x0080 </item><item>
        /// #2: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
        /// #3: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
        /// #4: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
        /// #5: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0180 </item><item>
        /// #6: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #7: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #8: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #9: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #10: SRQ: 0x21 0xA1 OSB: 0x0000 0x0410 MSB: 0x0180 0x0380 </item><item>
        /// OperationEventCondition=0x0460 </item><item>
        /// Reading #1=10.00507 Ω </item><item>
        /// Reading #2=10.005848 Ω </item><item>
        /// Reading #3=20.021992 Ω </item><item>
        /// Reading #4=10.005004 Ω </item><item>
        /// Reading #5=10.005835 Ω </item><item>
        /// Reading #6=20.021957 Ω </item><item>
        /// Reading #7=10.005008 Ω</item><item>
        /// Reading #8=10.0058 Ω </item><item>
        /// Reading #9=20.021935 Ω</item><item>
        /// Reading #10=10.005044 Ω </item><item>
        /// Reading #11=10.005876 Ω </item><item>
        /// Reading #12=20.021976 Ω </item><item>
        /// Reading #13=10.005042 Ω </item><item>
        /// Reading #14=10.005824 Ω </item><item>
        /// Reading #15=20.021971 Ω </item><item>
        /// Reading #16=10.005013 Ω </item><item>
        /// Reading #17=10.005812 Ω </item><item>
        /// Reading #18=20.021969 Ω </item><item>
        /// Reading #19=10.005057 Ω </item><item>
        /// Reading #20=10.005838 Ω </item><item>
        /// Reading #21=20.021971 Ω </item><item>
        /// Reading #22=10.005021 Ω </item><item>
        /// Reading #23=10.00584 Ω </item><item>
        /// Reading #24=20.021938 Ω </item><item>
        /// Reading #25=10.004992 Ω </item><item>
        /// Reading #26=10.005847 Ω </item><item>
        /// Reading #27=20.021933 Ω </item><item>
        /// Reading #28=10.004986 Ω </item><item>
        /// Reading #29=10.005827 Ω </item><item>
        /// Reading #30=20.021963 Ω </item><item>
        /// </item></list>
        /// </remarks>
        public static void AssertScanCardBusTriggersBufferTrace()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
                Assert.IsFalse( K2002Device.SystemSubsystem.QueryFrontTerminalsSelected().Value, $"Rear terminal must be selected" );
                ScannerMeterTests.ConfigureTriggerPlan( K2002Device, int.MaxValue, ScannerMeterSettings.Get().ScanCardSampleCount, ScannerMeterSettings.Get().MeterTriggerSource );
                ScannerMeterTests.ConfigureMeasurement( K2002Device );
                ScannerMeterTests.ConfigureLimitBinning( K2002Device, DigitalActiveLevels.Low );
                ConfigureFourWireResistanceScan( K2002Device, ScannerMeterSettings.Get().ScanCardSampleCount * K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount );
                if ( ArmSources.External == ScannerMeterSettings.Get().MeterArmSource )
                {
                    ScannerMeterTests.ConfigureBusTriggerPlan( k7000Device );
                    ScannerMeterTests.AssertBusTriggersBufferTrace( k7000Device, K2002Device );
                }
                else if ( ArmSources.Bus == ScannerMeterSettings.Get().MeterArmSource )
                {
                    ScannerMeterTests.AssertBusTriggersBufferTrace( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                }

                if ( K2002Device.TraceSubsystem.IsDataTraceEnabled() )
                {
                    _ = ScannerMeterTests.ShowBufferReadings( K2002Device );
                    // ScannerMeterTests.ValidateBufferReadings(ScannerMeterTests.ShowBufferReadigs(K2002Device), K7000SubsystemInfo.BusTriggerTestTriggerCount)
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, k7000Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
            }
        }

        /// <summary>
        /// (Unit Test Method) tests bus triggers buffer scan card trace using meter bus triggers.
        /// </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void MeterScanCardBusTriggersBufferTraceTest()
        {
            ScannerMeterSettings.Get().MeterArmSource = ArmSources.Bus;
            AssertScanCardBusTriggersBufferTrace();
        }

        /// <summary>
        /// (Unit Test Method) tests bus triggers buffer scan card trace using scanner bus triggers.
        /// </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void ScannerScanCardBusTriggersBufferTraceTest()
        {
            ScannerMeterSettings.Get().MeterArmSource = ArmSources.External;
            AssertScanCardBusTriggersBufferTrace();
        }

        /// <summary> (Unit Test Method) tests bus triggers stream scan card. </summary>
        /// <remarks>
        /// <list type="bullet">SRQ and Operation Event Status 2020/04/09<item>
        /// OperationEventCondition=0x0400 </item><item>
        /// Streaming reading #3: 20.022038 Ω 0x0380 </item><item>
        /// Streaming reading #6: 20.021997 Ω 0x0380 </item><item>
        /// Streaming reading #9: 20.022024 Ω 0x0380 </item><item>
        /// Streaming reading #12: 20.022018 Ω 0x0380 </item><item>
        /// Streaming reading #15: 20.022045 Ω 0x0380 </item><item>
        /// Streaming reading #18: 20.022005 Ω 0x0380 </item><item>
        /// Streaming reading #21: 20.021984 Ω 0x0380 </item><item>
        /// Streaming reading #24: 20.02199 Ω 0x0380 </item><item>
        /// Streaming reading #27: 20.021988 Ω 0x0380 </item><item>
        /// Streaming reading #30: 20.021973 Ω 0x0380 </item><item>
        /// OperationEventCondition=0x0400 </item><item>
        /// Reading #1=10.005095 Ω 0x0380 798ms </item><item>
        /// Reading #2=10.005883 Ω 0x0380 798ms </item><item>
        /// Reading #3=20.022038 Ω 0x0380 798ms </item><item>
        /// Reading #4=10.005078 Ω 0x0380 1682ms </item><item>
        /// Reading #5=10.00584 Ω 0x0380 1682ms </item><item>
        /// Reading #6=20.021997 Ω 0x0380 1682ms </item><item>
        /// Reading #7=10.005052 Ω 0x0380 2677ms </item><item>
        /// Reading #8=10.005927 Ω 0x0380 2677ms </item><item>
        /// Reading #9=20.022024 Ω 0x0380 2677ms </item><item>
        /// Reading #10=10.005088 Ω 0x0380 3688ms </item><item>
        /// Reading #11=10.00589 Ω 0x0380 3688ms </item><item>
        /// Reading #12=20.022018 Ω 0x0380 3688ms </item><item>
        /// Reading #13=10.005098 Ω 0x0380 4685ms </item><item>
        /// Reading #14=10.005914 Ω 0x0380 4685ms </item><item>
        /// Reading #15=20.022045 Ω 0x0380 4685ms </item><item>
        /// Reading #16=10.005047 Ω 0x0380 5704ms </item><item>
        /// Reading #17=10.005886 Ω 0x0380 5704ms </item><item>
        /// Reading #18=20.022005 Ω 0x0380 5704ms </item><item>
        /// Reading #19=10.005058 Ω 0x0380 6706ms </item><item>
        /// Reading #20=10.005877 Ω 0x0380 6706ms </item><item>
        /// Reading #21=20.021984 Ω 0x0380 6706ms </item><item>
        /// Reading #22=10.005053 Ω 0x0380 7727ms </item><item>
        /// Reading #23=10.005823 Ω 0x0380 7727ms </item><item>
        /// Reading #24=20.02199 Ω 0x0380 7727ms </item><item>
        /// Reading #25=10.005065 Ω 0x0380 8728ms </item><item>
        /// Reading #26=10.005837 Ω 0x0380 8728ms </item><item>
        /// Reading #27=20.021988 Ω 0x0380 8728ms </item><item>
        /// Reading #28=10.005033 Ω 0x0380 9731ms </item><item>
        /// Reading #29=10.005837 Ω 0x0380 9731ms </item><item>
        /// Reading #30=20.021973 Ω 0x0380 9731ms </item><item>
        /// </item></list>
        /// </remarks>
        public static void AssertScanCardBusTriggersStream()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
                Assert.IsFalse( K2002Device.SystemSubsystem.QueryFrontTerminalsSelected().Value, $"Rear terminal must be selected" );
                ScannerMeterTests.ConfigureTriggerPlan( K2002Device, int.MaxValue, ScannerMeterSettings.Get().ScanCardSampleCount, ScannerMeterSettings.Get().MeterTriggerSource );
                ScannerMeterTests.ConfigureMeasurement( K2002Device );

                // set fixed range with expected nominal value
                // .SenseResistanceFourWireSubsystem
                // K2002Device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled(False)
                _ = K2002Device.SenseFunctionSubsystem.ApplyAverageEnabled( false );
                _ = K2002Device.SenseFunctionSubsystem.ApplyAutoRangeEnabled( false );
                _ = K2002Device.SenseFunctionSubsystem.ApplyRange( 20d );
                ScannerMeterTests.ConfigureLimitBinning( K2002Device, DigitalActiveLevels.Low );
                ConfigureFourWireResistanceScan( K2002Device, ScannerMeterSettings.Get().ScanCardSampleCount );
                if ( ArmSources.Bus == ScannerMeterSettings.Get().MeterArmSource )
                {
                    ScannerMeterTests.AssertBusTriggersStreamReadings( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                }
                else if ( ArmSources.External == ScannerMeterSettings.Get().MeterArmSource )
                {
                    ScannerMeterTests.ConfigureBusTriggerPlan( k7000Device );
                    ScannerMeterTests.AssertBusTriggersStreamReadings( k7000Device, K2002Device );
                }

                _ = ScannerMeterTests.FetchBufferReadings( K2002Device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, k7000Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
            }
        }

        /// <summary>
        /// (Unit Test Method) tests bus triggers stream scan using meter bus triggers.
        /// </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void MeterScanCardBusTriggersStreamTest()
        {
            ScannerMeterSettings.Get().MeterArmSource = ArmSources.Bus;
            AssertScanCardBusTriggersStream();
        }

        /// <summary>
        /// (Unit Test Method) tests bus triggers stream scan using scanner bus triggers.
        /// </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void ScannerScanCardBusTriggersStreamTest()
        {
            ScannerMeterSettings.Get().MeterArmSource = ArmSources.External;
            AssertScanCardBusTriggersStream();
        }

        #endregion

    }
}
