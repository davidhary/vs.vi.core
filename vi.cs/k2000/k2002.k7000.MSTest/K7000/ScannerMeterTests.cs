using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K2002Tests
{

    /// <summary> K7000 Scanner Multimeter Scan Card unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k2002K7001" )]
    public class ScannerMeterTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        /// and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            _TestSite?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( K2002Properties.K2002ResourceInfo.Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( K2002Properties.K2002SubsystemsInfo.Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            Assert.IsTrue( K7000Properties.K7000SubsystemInfo.Exists, $"{typeof( K7000ResourceSettings )} settings should exist" );
            Assert.IsTrue( K7000Properties.K7000SubsystemInfo.Exists, $"{typeof( ScannerMeterSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " OPEN SESSIONS "

        /// <summary> (Unit Test Method) tests open sessions. </summary>
        [TestMethod()]
        public void OpenSessionsTest()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, k7000Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
            }
        }

        #endregion

        #region " BUS TRIGGER PLAN: INSTRUMENT CONFIGURATION "

        /// <summary> Assert configure digital output signal polarity. </summary>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="digitalOutputLineNumber"> The digital output line number. </param>
        /// <param name="subsystem">               The subsystem. </param>
        public static void AssertConfigureDigitalOutputSignalPolarity( int digitalOutputLineNumber, DigitalOutputSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            string deviceName = subsystem.ResourceNameCaption;
            string propertyName = $"{typeof( DigitalOutputSubsystemBase )}.{nameof( DigitalOutputSubsystemBase.CurrentDigitalActiveLevel )}";
            string activity = $"Reading [{deviceName}].[{propertyName}({digitalOutputLineNumber})] initial value";
            var initialPolarity = subsystem.QueryDigitalActiveLevel( digitalOutputLineNumber );
            Assert.IsTrue( initialPolarity.HasValue, $"{activity} should have a value" );
            var expectedPolarity = initialPolarity.Value == DigitalActiveLevels.High ? DigitalActiveLevels.Low : DigitalActiveLevels.High;
            activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}";
            var actualPolarity = subsystem.ApplyDigitalActiveLevel( digitalOutputLineNumber, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );
            expectedPolarity = initialPolarity.Value;
            activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}";
            actualPolarity = subsystem.ApplyDigitalActiveLevel( digitalOutputLineNumber, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );
        }

        /// <summary> Configure digital output signal polarity. </summary>
        /// <remarks> David, 2020-07-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="digitalOutputLineNumber"> The digital output line number. </param>
        /// <param name="subsystem">               The subsystem. </param>
        /// <param name="outputSignalPolarity">    The output signal polarity. </param>
        public static void ConfigureDigitalOutputSignalPolarity( int digitalOutputLineNumber, DigitalOutputSubsystemBase subsystem, DigitalActiveLevels outputSignalPolarity )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            string deviceName = subsystem.ResourceNameCaption;
            string propertyName = $"{typeof( DigitalOutputSubsystemBase )}.{nameof( DigitalOutputSubsystemBase.CurrentDigitalActiveLevel )}";
            string activity = $"Reading [{deviceName}].[{propertyName}({digitalOutputLineNumber})] initial value";
            var initialPolarity = subsystem.QueryDigitalActiveLevel( digitalOutputLineNumber );
            Assert.IsTrue( initialPolarity.HasValue, $"{activity} should have a value" );
            var expectedPolarity = outputSignalPolarity;
            activity = $"Setting [{deviceName}].[{propertyName}({digitalOutputLineNumber})] to {expectedPolarity}";
            var actualPolarity = subsystem.ApplyDigitalActiveLevel( digitalOutputLineNumber, expectedPolarity );
            Assert.IsTrue( actualPolarity.HasValue, $"{activity} should have a value" );
            Assert.AreEqual( expectedPolarity, ( object ) actualPolarity, $"{activity} should equal actual value" );
        }

        /// <summary> Configure limit binning. </summary>
        /// <param name="device">                 The device. </param>
        /// <param name="outputSignalPolarities"> The output signal polarities. </param>
        internal static void ConfigureLimitBinning( K2002.K2002Device device, DigitalActiveLevels outputSignalPolarities )
        {
            string propertyName = string.Empty;
            string title = device.OpenResourceTitle;
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.PassSource )}";
            int expectedBinValue = ScannerMeterSettings.Get().PassOutputValue;
            int actualBinValue = device.BinningSubsystem.ApplyPassSource( expectedBinValue ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2LowerLevel )}";
            double expectedValue = ScannerMeterSettings.Get().ExpectedResistance * (1d - ScannerMeterSettings.Get().ResistanceTolerance);
            double actualValue = device.BinningSubsystem.ApplyLimit2LowerLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2UpperLevel )}";
            expectedValue = ScannerMeterSettings.Get().ExpectedResistance * (1d + ScannerMeterSettings.Get().ResistanceTolerance);
            actualValue = device.BinningSubsystem.ApplyLimit2UpperLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2AutoClear )}";
            bool expectedBoolean = true;
            bool actualBoolean = device.BinningSubsystem.ApplyLimit2AutoClear( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2Enabled )}";
            expectedBoolean = true;
            actualBoolean = device.BinningSubsystem.ApplyLimit2Enabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.ApplyLimit2LowerSource )}";
            expectedBinValue = ScannerMeterSettings.Get().FailOutputValue;
            actualBinValue = device.BinningSubsystem.ApplyLimit2LowerSource( expectedBinValue ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.ApplyLimit2UpperSource )}";
            expectedBinValue = ScannerMeterSettings.Get().FailOutputValue;
            actualBinValue = device.BinningSubsystem.ApplyLimit2UpperSource( expectedBinValue ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1LowerLevel )}";
            expectedValue = ScannerMeterSettings.Get().ExpectedResistance * ScannerMeterSettings.Get().ResistanceTolerance;
            actualValue = device.BinningSubsystem.ApplyLimit1LowerLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1UpperLevel )}";
            expectedValue = ScannerMeterSettings.Get().OpenLimit;
            actualValue = device.BinningSubsystem.ApplyLimit1UpperLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1AutoClear )}";
            expectedBoolean = true;
            actualBoolean = device.BinningSubsystem.ApplyLimit1AutoClear( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1Enabled )}";
            expectedBoolean = true;
            actualBoolean = device.BinningSubsystem.ApplyLimit1Enabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.ApplyLimit1LowerSource )}";
            expectedBinValue = ScannerMeterSettings.Get().OutsideRangeOutputValue;
            actualBinValue = device.BinningSubsystem.ApplyLimit1LowerSource( expectedBinValue ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.ApplyLimit1UpperSource )}";
            expectedBinValue = ScannerMeterSettings.Get().OutsideRangeOutputValue;
            actualBinValue = device.BinningSubsystem.ApplyLimit1UpperSource( expectedBinValue ).GetValueOrDefault( 0 );
            Assert.AreEqual( expectedBinValue, actualBinValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.BinningStrobeEnabled )}";
            expectedBoolean = true;
            actualBoolean = device.BinningSubsystem.ApplyBinningStrobeEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            for ( int i = 1; i <= 4; i++ )
                ConfigureDigitalOutputSignalPolarity( i, device.DigitalOutputSubsystem, DigitalActiveLevels.Low );
        }

        /// <summary> Configure measurement. </summary>
        /// <param name="device"> The device. </param>
        internal static void ConfigureMeasurement( K2002.K2002Device device )
        {
            string title = device.OpenResourceTitle;
            string propertyName = $"{typeof( SystemSubsystemBase )}.{nameof( SystemSubsystemBase.AutoZeroEnabled )}";
            bool expectedBoolean = ScannerMeterSettings.Get().AutoZeroEnabled;
            bool actualBoolean = device.SystemSubsystem.ApplyAutoZeroEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.FunctionMode )}";
            var expectedFunctionMode = ScannerMeterSettings.Get().SenseFunction;
            var actualFunctionMode = device.SenseSubsystem.ApplyFunctionMode( expectedFunctionMode ).GetValueOrDefault( SenseFunctionModes.None );
            Assert.AreEqual( expectedFunctionMode, actualFunctionMode, $"[{title}].[{propertyName}] should equal expected value" );
            _ = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.Aperture )}";
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.PowerLineCycles )}";
            double expectedPowerLineCycles = ScannerMeterSettings.Get().PowerLineCycles;
            double actualPowerLineCycles = device.SenseFunctionSubsystem.ApplyPowerLineCycles( expectedPowerLineCycles ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, device.StatusSubsystemBase.LineFrequency.Value / TimeSpan.TicksPerSecond, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.AutoRangeEnabled )}";
            expectedBoolean = ScannerMeterSettings.Get().AutoRangeEnabled;
            actualBoolean = device.SenseFunctionSubsystem.ApplyAutoRangeEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.ResolutionDigits )}";
            double expectedResolution = ScannerMeterSettings.Get().ResolutionDigits;
            double actualResolution = device.SenseFunctionSubsystem.ApplyResolutionDigits( expectedResolution ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedResolution, actualResolution, $"[{title}].[{propertyName}] should equal expected value" );
        }

        /// <summary> Validates the trace. </summary>
        /// <param name="device">          The device. </param>
        /// <param name="triggerCount">    Number of triggers. </param>
        /// <param name="sampleCount">     Number of samples. </param>
        /// <param name="usingAutoPoints"> True to using automatic points. </param>
        private static void ValidateTrace( K2002.K2002Device device, int triggerCount, int sampleCount, bool usingAutoPoints )
        {
            _ = nameof( TraceSubsystemBase.FeedSource ).SplitWords();
            string title = device.OpenResourceTitle;
            string propertyName = nameof( MeasurementEventBitmaskKey.BufferAvailable ).SplitWords();
            Assert.IsTrue( device.StatusSubsystemBase.MeasurementEventsBitmasks.ContainsKey( ( int ) MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}] should be included in [{nameof( StatusSubsystemBase.MeasurementEventsBitmasks ).SplitWords()}]" );
            Assert.IsTrue( 0 < device.StatusSubsystemBase.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable], $"[{title}].[{propertyName}] bitmask value should be greater than 0" );
            propertyName = nameof( StatusSubsystemBase.MeasurementEventCondition ).SplitWords();
            device.StatusSubsystem.MeasurementEventsBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
            Assert.IsFalse( device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn( MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString().SplitWords()}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable]:X4} should be off after clearing the buffer" );
            var expectedFeedSource = FeedSources.Sense;
            var actualFeedSource = device.TraceSubsystem.QueryFeedSource();
            Assert.IsTrue( actualFeedSource.HasValue, $"[{propertyName}] should have a value" );
            Assert.AreEqual( expectedFeedSource, actualFeedSource.Value, $"{title}.[{propertyName}] should match" );
            bool expectedAutoEnabled = usingAutoPoints;
            bool? actualAutoPointsEnabeldCount;
            propertyName = nameof( TraceSubsystemBase.AutoPointsEnabled ).SplitWords();
            actualAutoPointsEnabeldCount = device.TraceSubsystem.QueryAutoPointsEnabled();
            Assert.AreEqual( expectedAutoEnabled, actualAutoPointsEnabeldCount.Value, $"{title}.[{propertyName}] should match" );
            if ( usingAutoPoints )
            {
            }
            else if ( triggerCount > 1 )
            {
                Assert.AreEqual( expectedAutoEnabled, actualAutoPointsEnabeldCount.Value, $"{title}.[{propertyName}] should match" );
                // was used to testing.
                propertyName = nameof( TraceSubsystemBase.PointsCount ).SplitWords();
                var actualTraceCount = device.TraceSubsystem.QueryPointsCount();
                Assert.IsTrue( actualTraceCount.HasValue, $"[{propertyName}] should have a value" );
                Assert.AreEqual( triggerCount, actualTraceCount.Value, $"[{propertyName}] should match" );
            }
            else
            {
                propertyName = nameof( TraceSubsystemBase.PointsCount ).SplitWords();
                var actualTraceCount = device.TraceSubsystem.QueryPointsCount();
                Assert.IsTrue( actualTraceCount.HasValue, $"[{propertyName}] should have a value" );
                Assert.AreEqual( sampleCount, actualTraceCount.Value, $"[{propertyName}] should match" );
            }

            propertyName = nameof( TraceSubsystemBase.FeedControl ).SplitWords();
            var expectedFeedControl = FeedControls.Next;
            var actualFeedControl = device.TraceSubsystem.QueryFeedControl();
            Assert.IsTrue( actualFeedControl.HasValue, $"[{propertyName}] should have a value" );
            Assert.AreEqual( expectedFeedControl, actualFeedControl.Value, $"[{propertyName}] should match" );
        }

        /// <summary> Configure trace. </summary>
        /// <param name="device">          The device. </param>
        /// <param name="triggerCount">    Number of triggers. </param>
        /// <param name="usingAutoPoints"> True to using automatic points. </param>
        private static void ConfigureTrace( K2002.K2002Device device, int triggerCount, bool usingAutoPoints )
        {
            _ = nameof( TraceSubsystemBase.FeedSource ).SplitWords();
            string title = device.OpenResourceTitle;
            device.TraceSubsystem.ClearBuffer();
            device.TraceSubsystem.BinningDuration = TimeSpan.FromMilliseconds( 100d );
            string propertyName = nameof( MeasurementEventBitmaskKey.BufferAvailable ).SplitWords();
            Assert.IsTrue( device.StatusSubsystemBase.MeasurementEventsBitmasks.ContainsKey( ( int ) MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}] should be included in [{nameof( StatusSubsystemBase.MeasurementEventsBitmasks ).SplitWords()}]" );
            Assert.IsTrue( 0 < device.StatusSubsystemBase.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable], $"[{title}].[{propertyName}] bitmask value should be greater than 0" );
            propertyName = nameof( StatusSubsystemBase.MeasurementEventCondition ).SplitWords();
            device.StatusSubsystem.MeasurementEventsBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
            Assert.IsFalse( device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn( MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString().SplitWords()}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable]:X4} should be off after clearing the buffer" );
            var expectedFeedSource = FeedSources.Sense;
            var actualFeedSource = device.TraceSubsystem.ApplyFeedSource( expectedFeedSource );
            Assert.IsTrue( actualFeedSource.HasValue, $"{title}.[{propertyName}] should have a value" );
            Assert.AreEqual( expectedFeedSource, actualFeedSource.Value, $"{title}.[{propertyName}] should match" );
            if ( usingAutoPoints )
            {
                propertyName = nameof( TraceSubsystemBase.AutoPointsEnabled ).SplitWords();
                bool expectedAutoPointsCount = true;
                var actualAutoPointsCount = device.TraceSubsystem.ApplyAutoPointsEnabled( expectedAutoPointsCount );
                Assert.AreEqual( expectedAutoPointsCount, actualAutoPointsCount.Value, $"{title}.[{propertyName}] should match" );
            }
            else if ( triggerCount > 1 )
            {
                // was used to testing.
                propertyName = nameof( TraceSubsystemBase.PointsCount ).SplitWords();
                var actualTraceCount = device.TraceSubsystem.ApplyPointsCount( triggerCount );
                Assert.IsTrue( actualTraceCount.HasValue, $"{title}.[{propertyName}] should have a value" );
                Assert.AreEqual( triggerCount, actualTraceCount.Value, $"{title}.[{propertyName}] should match" );
            }
            else
            {
                // minimum buffer count is 2.
                triggerCount = 2;
                propertyName = nameof( TraceSubsystemBase.PointsCount ).SplitWords();
                var actualTraceCount = device.TraceSubsystem.ApplyPointsCount( triggerCount );
                Assert.IsTrue( actualTraceCount.HasValue, $"{title}.[{propertyName}] should have a value" );
                Assert.AreEqual( triggerCount, actualTraceCount.Value, $"{title}.[{propertyName}] should match" );
            }

            // the traced buffer includes only status values.
            device.TraceSubsystem.OrderedReadingElementTypes = new List<ReadingElementTypes>() { ReadingElementTypes.Reading };
            propertyName = nameof( TraceSubsystemBase.FeedControl ).SplitWords();
            var expectedFeedControl = FeedControls.Next;
            var actualFeedControl = device.TraceSubsystem.ApplyFeedControl( expectedFeedControl );
            Assert.IsTrue( actualFeedControl.HasValue, $"{title}.[{propertyName}] should have a value" );
            Assert.AreEqual( expectedFeedControl, actualFeedControl.Value, $"{title}.[{propertyName}] should match" );
        }

        /// <summary> Configure trace for fetching only. </summary>
        /// <param name="device"> The device. </param>
        private static void ConfigureTrace( K2002.K2002Device device )
        {
            _ = nameof( TraceSubsystemBase.FeedSource ).SplitWords();
            string title = device.OpenResourceTitle;
            device.TraceSubsystem.ClearBuffer();
            device.TraceSubsystem.BinningDuration = TimeSpan.FromMilliseconds( 100d );
            string propertyName = nameof( MeasurementEventBitmaskKey.BufferAvailable ).SplitWords();
            Assert.IsTrue( device.StatusSubsystemBase.MeasurementEventsBitmasks.ContainsKey( ( int ) MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}] should be included in [{nameof( StatusSubsystemBase.MeasurementEventsBitmasks ).SplitWords()}]" );
            Assert.IsTrue( 0 < device.StatusSubsystemBase.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable], $"[{title}].[{propertyName}] bitmask value should be greater than 0" );
            propertyName = nameof( StatusSubsystemBase.MeasurementEventCondition ).SplitWords();
            device.StatusSubsystem.MeasurementEventsBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
            Assert.IsFalse( device.StatusSubsystem.MeasurementEventsBitmasks.IsAnyBitOn( MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString().SplitWords()}]=0x{device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable]:X4} should be off after clearing the buffer" );
            propertyName = nameof( TraceSubsystemBase.FeedSource ).SplitWords();
            var expectedFeedSource = FeedSources.Sense;
            var actualFeedSource = device.TraceSubsystem.ApplyFeedSource( expectedFeedSource );
            Assert.IsTrue( actualFeedSource.HasValue, $"{title}.[{propertyName}] should have a value" );
            Assert.AreEqual( expectedFeedSource, actualFeedSource.Value, $"{title}.[{propertyName}] should match" );

            // the fetched buffer includes only reading values.
            device.TraceSubsystem.OrderedReadingElementTypes = new List<ReadingElementTypes>() { ReadingElementTypes.Reading };
            propertyName = nameof( TraceSubsystemBase.FeedControl ).SplitWords();
            var expectedFeedControl = FeedControls.Never;
            var actualFeedControl = device.TraceSubsystem.ApplyFeedControl( expectedFeedControl );
            Assert.IsTrue( actualFeedControl.HasValue, $"{title}.[{propertyName}] should have a value" );
            Assert.AreEqual( expectedFeedControl, actualFeedControl.Value, $"{title}.[{propertyName}] should match" );
        }

        /// <summary> Configure external trigger measurements. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="device">        The device. </param>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="sampleCount">   Number of samples. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        internal static void ConfigureTriggerPlan( K2002.K2002Device device, int triggerCount, int sampleCount, TriggerSources triggerSource )
        {
            string title = device.OpenResourceTitle;
            _ = nameof( TriggerSubsystemBase.TriggerSource ).SplitWords();
            device.ClearExecutionState();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.TriggerSubsystem.Abort();
            device.Session.EnableServiceRequestWaitComplete();
            _ = device.Session.ReadStatusRegister();
            _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.RouteSubsystem.QueryClosedChannels();
            _ = device.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer1Subsystem.ApplyArmCount( 1 );
            string propertyName = nameof( ArmLayerSubsystemBase.ArmLayerBypassMode ).SplitWords();
            var expectedBypassMode = TriggerLayerBypassModes.Acceptor;
            var actualBypassMode = device.ArmLayer1Subsystem.ApplyArmLayerBypassMode( expectedBypassMode );
            Assert.AreEqual( expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for arm layer should match" );
            _ = device.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer2Subsystem.ApplyArmCount( sampleCount );
            _ = device.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );
            propertyName = nameof( ArmLayerSubsystemBase.ArmLayerBypassMode ).SplitWords();
            expectedBypassMode = TriggerLayerBypassModes.Acceptor;
            actualBypassMode = device.ArmLayer2Subsystem.ApplyArmLayerBypassMode( expectedBypassMode );
            Assert.AreEqual( expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for scan layer should match" );

            // device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.External)
            propertyName = nameof( TriggerSubsystemBase.TriggerSource ).SplitWords();
            var expectedTriggerSource = triggerSource;
            var actualTriggerSource = device.TriggerSubsystem.ApplyTriggerSource( expectedTriggerSource );
            Assert.IsTrue( actualTriggerSource.HasValue, $"{title}.[{propertyName}] should have a value" );
            Assert.AreEqual( expectedTriggerSource, actualTriggerSource.Value, $"{title}.[{propertyName}] should match" );
            propertyName = nameof( TriggerSubsystemBase.TriggerCount ).SplitWords();
            if ( triggerCount == int.MaxValue )
            {
                int actualTriggerCount = device.TriggerSubsystem.ApplyTriggerCount( triggerCount ).GetValueOrDefault( 0 );
                Assert.AreEqual( triggerCount, actualTriggerCount, $"{title}.[{propertyName}] should match" );
            }
            else
            {
                int actualTriggerCount = device.TriggerSubsystem.ApplyTriggerCount( triggerCount ).GetValueOrDefault( 0 );
                Assert.AreEqual( triggerCount, actualTriggerCount, $"{title}.[{propertyName}] should match" );
            }

            propertyName = nameof( TriggerSubsystemBase.Delay ).SplitWords();
            var actualDelay = device.TriggerSubsystem.ApplyDelay( TimeSpan.Zero ).GetValueOrDefault( TimeSpan.FromSeconds( 10d ) );
            Assert.AreEqual( TimeSpan.Zero, actualDelay, $"{title}.[{propertyName}] should match" );
            propertyName = nameof( TriggerSubsystemBase.TriggerLayerBypassMode ).SplitWords();
            expectedBypassMode = TriggerLayerBypassModes.Acceptor;
            actualBypassMode = device.TriggerSubsystem.ApplyTriggerLayerBypassMode( expectedBypassMode );
            Assert.AreEqual( expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for measure layer should match" );
        }

        /// <summary> Configure bus trigger plan. </summary>
        /// <param name="device"> The device. </param>
        internal static void ConfigureBusTriggerPlan( K7000.K7000Device device )
        {
            string title = device.OpenResourceTitle;
            _ = nameof( RouteSubsystemBase.ScanList ).SplitWords();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.TriggerSubsystem.Abort();
            device.Session.EnableServiceRequestWaitComplete();
            _ = device.RouteSubsystem.WriteOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.Session.ReadStatusRegister();
            _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.RouteSubsystem.QueryClosedChannels();
            string propertyName = nameof( RouteSubsystemBase.ScanList ).SplitWords();
            string expectedScanList = K7000Properties.K7000SubsystemInfo.BusTriggerTestScanList;
            string actualScanList = device.RouteSubsystem.ApplyScanList( expectedScanList );
            Assert.AreEqual( expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'" );
            _ = device.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer1Subsystem.ApplyArmCount( 1 );
            var expectedBypassMode = TriggerLayerBypassModes.Acceptor;
            var actualBypassMode = device.ArmLayer1Subsystem.ApplyArmLayerBypassMode( expectedBypassMode );
            Assert.AreEqual( expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for arm layer should match" );

            // 7001 has only a single arm layer
            // device.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
            // device.ArmLayer2Subsystem.ApplyArmCount(1)
            // device.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)

            // device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Bus)
            propertyName = nameof( TriggerSubsystemBase.TriggerSource ).SplitWords();
            var expectedTriggerSource = K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerSource;
            var actualTriggerSource = device.TriggerSubsystem.ApplyTriggerSource( expectedTriggerSource );
            Assert.IsTrue( actualTriggerSource.HasValue, $"{propertyName} should have a value" );
            Assert.AreEqual( expectedTriggerSource, actualTriggerSource.Value, $"{propertyName} should match" );
            _ = device.TriggerSubsystem.ApplyTriggerCount( 9999 ); // in place of infinite
            _ = device.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
            propertyName = nameof( TriggerSubsystemBase.TriggerLayerBypassMode ).SplitWords();
            expectedBypassMode = TriggerLayerBypassModes.Acceptor;
            actualBypassMode = device.TriggerSubsystem.ApplyTriggerLayerBypassMode( expectedBypassMode );
            Assert.AreEqual( expectedBypassMode, actualBypassMode.Value, $"{title}.[{propertyName}] for scan layer should match" );
        }

        /// <summary> Assert measured values. </summary>
        /// <param name="device"> Reference to the K2002 device. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process show buffer readings in this
        /// collection.
        /// </returns>
        internal static IEnumerable<BufferReading> ShowBufferReadings( K2002.K2002Device device )
        {
            _ = device.TraceSubsystem.AddAllBufferReadings();
            // string propertyName = nameof(Arebis.UnitsAmounts.Amount);
            _ = device.OpenResourceTitle;
            int readingNumber = 0;
            IEnumerable<BufferReading> bufferReadings = device.TraceSubsystem.DequeueRange( device.TraceSubsystem.NewReadingsCount );

            // build the buffer readings amounts
            foreach ( BufferReading bufferReading in bufferReadings )
            {
                readingNumber += 1;
                // ScannerMeterTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Reading}")
                bufferReading.BuildAmount( device.SenseSubsystem.FunctionUnit );
                TestInfo.TraceMessage( $"Reading #{readingNumber}={bufferReading.Amount}" );
            }

            return bufferReadings;
        }

        /// <summary> Assert measured values. </summary>
        /// <param name="bufferReadings"> The buffer readings. </param>
        /// <param name="expectedCount">  Number of expected. </param>
        private static void ValidateBufferReadings( IEnumerable<BufferReading> bufferReadings, int expectedCount )
        {
            Assert.AreEqual( expectedCount, bufferReadings.Count(), $"Buffer reading count should match" );
            string propertyName = nameof( Arebis.UnitsAmounts.Amount );
            double expectedResistance = ScannerMeterSettings.Get().ExpectedResistance;
            double epsilon = expectedResistance * ScannerMeterSettings.Get().ResistanceTolerance;
            double measuredResistance = 0d;
            int readingNumber = 0;
            propertyName = nameof( measuredResistance ).SplitWords();
            readingNumber = 0;
            foreach ( BufferReading bufferReading in bufferReadings )
            {
                readingNumber += 1;
                Assert.IsNotNull( bufferReading.Amount, $"Reading #{readingNumber} should not be null" );
                measuredResistance = bufferReading.Amount.Value;
                Assert.AreEqual( expectedResistance, measuredResistance, epsilon, $"{propertyName}={measuredResistance} but should equals {expectedResistance} within {epsilon}" );
            }
        }

        /// <summary> Initiate trigger plan. </summary>
        /// <param name="device"> The Keithley 2002 device. </param>
        private static void InitiateTriggerPlan( K2002.K2002Device device )
        {
            string title = device.OpenResourceTitle;
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            operationBitmasks.Status = device.StatusSubsystem.QueryOperationEventCondition().Value;
            bool actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
            Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is initiated" );
            TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{device.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
            if ( device.TraceSubsystem.FeedControl.Value == FeedControls.Next )
            {

                // clearing the buffer eliminated the buffer trace.
                _ = device.TraceSubsystem.ClearBuffer( FeedControls.Next );

                // clearing the buffer changes feed control to Never; the above command should restore the feed control but validating nonetheless:
                // Dim usingTraceAutoPoints As Boolean = device.TraceSubsystem.QueryAutoPointsEnabled.Value
                // ScannerMeterTests.ValidateTrace(device, device.TriggerSubsystem.TriggerCount.Value, device.ArmLayer2Subsystem.ArmCount.Value, usingTraceAutoPoints)
            }

            device.TraceSubsystem.BufferReadingsBindingList.Clear();
            device.TraceSubsystem.ClearBufferReadingsQueue();

            // start the K2002 trigger model
            device.TriggerSubsystem.Initiate();
            operationBitmasks.Status = device.StatusSubsystem.QueryOperationEventCondition().Value;
            actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
            Assert.IsFalse( actualOperationIdle, $"[{title}].[{propertyName}] should be false after triggering is initiated" );
            TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{device.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );

            // wait twice the aperture time.
            Core.ApplianceBase.DoEventsWait( TimeSpan.FromMilliseconds( 2d * device.SenseFunctionSubsystem.Aperture.Value ) );
            if ( device.TraceSubsystem.FeedControl.Value == FeedControls.Next )
            {
                // test if buffer is available; which means the instrument triggered already. 
                MeasurementEventBitmaskKey measurementEvent = MeasurementEventBitmaskKey.BufferAvailable;
                propertyName = nameof( MeasurementEventBitmaskKey.BufferAvailable ).SplitWords();
                measurementBitmasks.Status = device.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
                Assert.IsFalse( measurementBitmasks.IsAnyBitOn( measurementEvent ), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString().SplitWords()}]=0x{measurementBitmasks[( int ) measurementEvent]:X4} should be off upon initiating the trigger plan" );
            }
        }

        /// <summary> Fetches the buffer readings in this collection. </summary>
        /// <remarks> David, 2020-04-13. </remarks>
        /// <param name="device"> Reference to the K2002 device. </param>
        /// <returns>
        /// An enumerator that allows foreach to be used to process the buffer readings in this
        /// collection.
        /// </returns>
        internal static IEnumerable<BufferReading> FetchBufferReadings( K2002.K2002Device device )
        {
            _ = device.OpenResourceTitle;
            IEnumerable<BufferReading> bufferReadings = device.TraceSubsystem.DequeueRange( device.TraceSubsystem.NewReadingsCount );
            double expectedResistance = ScannerMeterSettings.Get().ExpectedResistance;
            _ = expectedResistance * ScannerMeterSettings.Get().ResistanceTolerance;

            // trace (display) the buffer readings amounts
            int readingNumber = 0;
            foreach ( BufferReading bufferReading in bufferReadings )
            {
                readingNumber += 1;
                TestInfo.TraceMessage( $"Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4} {bufferReading.FractionalTimespan.TotalMilliseconds:0}ms" );
            }

            return bufferReadings;
        }

        /// <summary> Assert buffer readings. </summary>
        /// <param name="device"> The 2002. </param>
        private static void AssertBufferReadings( K2002.K2002Device device )
        {
            string title = device.OpenResourceTitle;
            string propertyName;
            IEnumerable<BufferReading> bufferReadings = device.TraceSubsystem.DequeueRange( device.TraceSubsystem.NewReadingsCount );
            double expectedResistance = ScannerMeterSettings.Get().ExpectedResistance;
            double epsilon = expectedResistance * ScannerMeterSettings.Get().ResistanceTolerance;

            // trace (display) the buffer readings amounts
            int readingNumber = 0;
            foreach ( BufferReading bufferReading in bufferReadings )
            {
                readingNumber += 1;
                TestInfo.TraceMessage( $"Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4} {bufferReading.FractionalTimespan.TotalMilliseconds:0}ms" );
            }

            double measuredResistance;
            propertyName = nameof( measuredResistance ).SplitWords();
            if ( device.SenseSubsystem.FunctionMode.Value == ScannerMeterSettings.Get().SenseFunction )
            {
                readingNumber = 0;
                foreach ( BufferReading bufferReading in bufferReadings )
                {
                    readingNumber += 1;
                    Assert.IsNotNull( bufferReading.Amount, $"[{title}].[Reading #{readingNumber}] should not be null" );
                    measuredResistance = bufferReading.Amount.Value;
                    Assert.AreEqual( expectedResistance, measuredResistance, epsilon, $"[{title}].[{propertyName}]={measuredResistance} but should equals {expectedResistance} within {epsilon}" );
                }
            }

            // validate binding list count
            Assert.AreEqual( device.TraceSubsystem.BufferReadingsCount, bufferReadings.Count(), "Buffer readings count should match the binding list count" );
        }

        #endregion

        #region " BUS TRIGGERS BUFFER TRACE "

        /// <summary> A register value. </summary>
        private class RegisterValue
        {

            /// <summary> Constructor. </summary>
            /// <param name="formatString"> The format string. </param>
            public RegisterValue( string formatString ) : base()
            {
                this.FormatString = formatString;
            }

            /// <summary> Gets or sets the format string. </summary>
            /// <value> The format string. </value>
            public string FormatString { get; set; } = "SRE: 0x{0:X2} 0x{1:X2}";

            /// <summary> Gets or sets the pre trigger value. </summary>
            /// <value> The pre trigger value. </value>
            public int PreTriggerValue { get; set; }

            /// <summary> Gets or sets the post trigger value. </summary>
            /// <value> The post trigger value. </value>
            public int PostTriggerValue { get; set; }

            /// <summary> Gets the outcome. </summary>
            /// <returns> A String. </returns>
            public string Outcome()
            {
                return string.Format( this.FormatString, this.PreTriggerValue, this.PostTriggerValue );
            }
        }

        /// <summary> Assert bus trigger buffer trace. </summary>
        /// <remarks>
        /// Seeing the operation reading, suggests a plan for streaming the measurement status of an
        /// infinite meter trigger plan.
        /// <list type="bullet">SRQ and Operation Event Status<item>
        /// OperationEventCondition=0x0000  </item><item>
        /// SRQ: Pre;  Post. OSB: Pre;    Post  </item><item>
        /// #1: SRQ: 0x00; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #2: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #3: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #4: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #5: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #6: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #7: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #8: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #9: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0010  </item><item>
        /// #10: SRQ: 0x01; 0x81. OSB: 0x0000; 0x0410  </item><item>
        /// Reading #1=100.28211 Ω  </item><item>
        /// Reading #2=100.28249 Ω  </item><item>
        /// Reading #3=100.2822 Ω  </item><item>
        /// Reading #4=100.28211 Ω  </item><item>
        /// Reading #5=100.28218 Ω  </item><item>
        /// Reading #6=100.28239 Ω  </item><item>
        /// Reading #7=100.28221 Ω  </item><item>
        /// Reading #8=100.28232 Ω  </item><item>
        /// Reading #9=100.28233 Ω  </item><item>
        /// Reading #10=100.2827 Ω  </item><item>
        /// OperationEventCondition=0x0400  </item></list>
        /// </remarks>
        /// <param name="k7000"> The Keithley 7001 device. </param>
        /// <param name="k2002"> The Keithley 2002 device. </param>
        internal static void AssertBusTriggersBufferTrace( K7000.K7000Device k7000, K2002.K2002Device k2002 )
        {
            bool actualOperationIdle = true;
            string title = k2002.OpenResourceTitle;
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            // var measurementEvent = MeasurementEventBitmaskKey.BufferAvailable;
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );

            // enable all measurement status bits.
            _ = k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask( 0x7FFF );
            _ = k2002.StatusSubsystem.ApplyOperationEventEnableBitmask( 0x7FFF );
            var srq = new RegisterValue( "SRQ: 0x{0:X2} 0x{1:X2}" );
            var osb = new RegisterValue( "OSB: 0x{0:X4} 0x{1:X4}" );
            var msb = new RegisterValue( "MSB: 0x{0:X4} 0x{1:X4}" );
            InitiateTriggerPlan( k2002 );
            try
            {
                // start the K7000 trigger model
                k7000.TriggerSubsystem.Initiate();
                try
                {
                    // Note: device error -211 if query closed channels: Trigger ignored.
                    TestInfo.TraceMessage( $"    SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post  " );
                    for ( int i = 1, loopTo = K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount; i <= loopTo; i++ )
                    {
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        srq.PreTriggerValue = ( int ) k2002.Session.ReadStatusByte();
                        osb.PreTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus().Value;
                        msb.PreTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition().Value;
                        k7000.Session.AssertTrigger();
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        srq.PostTriggerValue = ( int ) k2002.Session.ReadStatusByte();
                        osb.PostTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus().Value;
                        msb.PostTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition().Value;
                        TestInfo.TraceMessage( $"#{i}: {srq.Outcome()} {osb.Outcome()} {msb.Outcome()}" );
                    }

                    propertyName = nameof( StatusSubsystemBase.MeasurementEventCondition ).SplitWords();
                    measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
                    Assert.IsTrue( measurementBitmasks.IsAnyBitOn( MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString().SplitWords()}]=0x{measurementBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable]:X4} should be on after triggering is completed" );
                    propertyName = nameof( StatusSubsystemBase.QueryOperationEventCondition ).SplitWords();
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                    actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks[( int ) OperationEventBitmaskKey.Idle]:X4} bit should high (idle) after triggering is completed" );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    k7000.TriggerSubsystem.Abort();
                    _ = k7000.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
                    _ = k7000.RouteSubsystem.QueryClosedChannels();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                k2002.TriggerSubsystem.Abort();
                TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted" );
            }
        }

        /// <summary> Request binning strobe. </summary>
        /// <remarks> David, 2020-08-25. </remarks>
        /// <param name="device"> The device. </param>
        /// <param name="pass">   True to pass. </param>
        public static void RequestBinningStrobe( K2002.K2002Device device, bool pass )
        {
            device.TraceSubsystem.BinningStrobeAction = new Action( () => device.DigitalOutputSubsystem.Strobe( 4, TimeSpan.FromMilliseconds( 10d ), pass ? 1 : 2, TimeSpan.FromMilliseconds( 20d ) ) );
            device.TraceSubsystem.BinningStrobeRequested = true;
        }

        /// <summary> Assert bus trigger buffer trace. </summary>
        /// <remarks>
        /// Seeing the operation reading, suggests a plan for streaming the measurement status of an
        /// infinite meter trigger plan.
        /// <list type="bullet">SRQ and Operation Event Status 2020/04/09<item>
        /// OperationEventCondition=0x0400 </item><item>
        /// OperationEventCondition=0x0000 </item><item>
        /// SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post   </item><item>
        /// #1: SRQ: 0x20 0xA1 OSB: 0x0000 0x0010 MSB: 0x0000 0x0080 Buffer available </item><item>
        /// #2: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
        /// #3: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
        /// #4: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0080 </item><item>
        /// #5: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0080 0x0180 Buffer available+ Half Full
        /// </item><item>
        /// #6: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #7: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #8: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180 </item><item>
        /// #9: SRQ: 0x21 0xA1 OSB: 0x0000 0x0010 MSB: 0x0180 0x0180  </item><item>
        /// #10: SRQ: 0x21 0xA1 OSB: 0x0000 0x0410 MSB: 0x0180 0x0380 Buffer available + Half Full + Full
        /// </item><item>
        /// Reading #1=100.138885 Ω  </item><item>
        /// Reading #2=100.13842 Ω </item><item>
        /// Reading #3=100.137993 Ω  </item><item>
        /// Reading #4=100.13868 Ω  </item><item>
        /// Reading #5=100.138382 Ω  </item><item>
        /// Reading #6=100.137764 Ω  </item><item>
        /// Reading #7=100.138092 Ω  </item><item>
        /// Reading #8=100.137527 Ω  </item><item>
        /// Reading #9=100.137726 Ω  </item><item>
        /// Reading #10=100.137268 Ω  </item></list>
        /// OperationEventCondition=0x0400.
        /// </remarks>
        /// <param name="k2002">        The Keithley 2002 device. </param>
        /// <param name="triggerCount"> Number of triggers. </param>
        /// <param name="delay">        The delay. </param>
        internal static void AssertBusTriggersBufferTrace( K2002.K2002Device k2002, int triggerCount, TimeSpan delay )
        {
            bool actualOperationIdle = true;
            string title = k2002.OpenResourceTitle;
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            // var measurementEvent = MeasurementEventBitmaskKey.BufferFull;
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );

            // enable all measurement status bits.
            _ = k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask( 0x7FFF );
            _ = k2002.StatusSubsystem.ApplyOperationEventEnableBitmask( 0x7FFF );
            var srq = new RegisterValue( "SRQ: 0x{0:X2} 0x{1:X2}" );
            var osb = new RegisterValue( "OSB: 0x{0:X4} 0x{1:X4}" );
            var msb = new RegisterValue( "MSB: 0x{0:X4} 0x{1:X4}" );
            InitiateTriggerPlan( k2002 );
            try
            {
                try
                {
                    // Note: device error -211 if query closed channels: Trigger ignored.
                    TestInfo.TraceMessage( $"    SRQ: Pre  Post OSB: Pre    Post   MSB: Pre    Post  " );
                    for ( int i = 1, loopTo = triggerCount; i <= loopTo; i++ )
                    {
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        srq.PreTriggerValue = ( int ) k2002.Session.ReadStatusByte();
                        osb.PreTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus().Value;
                        msb.PreTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition().Value;
                        k2002.Session.AssertTrigger();
                        Core.ApplianceBase.DoEventsWait( delay );
                        srq.PostTriggerValue = ( int ) k2002.Session.ReadStatusByte();
                        osb.PostTriggerValue = k2002.StatusSubsystem.QueryOperationEventStatus().Value;
                        msb.PostTriggerValue = k2002.StatusSubsystem.QueryMeasurementEventCondition().Value;
                        TestInfo.TraceMessage( $"#{i}: {srq.Outcome()} {osb.Outcome()} {msb.Outcome()}" );
                    }

                    propertyName = nameof( StatusSubsystemBase.MeasurementEventCondition ).SplitWords();
                    measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition().GetValueOrDefault( 0 );
                    Assert.IsTrue( measurementBitmasks.IsAnyBitOn( MeasurementEventBitmaskKey.BufferAvailable ), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} [{MeasurementEventBitmaskKey.BufferAvailable.ToString().SplitWords()}]=0x{measurementBitmasks[( int ) MeasurementEventBitmaskKey.BufferAvailable]:X4} should be on after triggering is completed" );
                    propertyName = nameof( StatusSubsystemBase.QueryOperationEventCondition ).SplitWords();
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                    actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks[( int ) OperationEventBitmaskKey.Idle]:X4} bit should high (idle) after triggering is completed" );
                }
                catch
                {
                    throw;
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                k2002.TriggerSubsystem.Abort();
                TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted" );
            }
        }

        /// <summary> (Unit Test Method) tests bus trigger buffer trace. </summary>
        /// <remarks> Passed 2020-01-10. </remarks>
        public static void AssertBusTriggersBufferTraceTest()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
                Assert.IsTrue( K2002Device.SystemSubsystem.QueryFrontTerminalsSelected().Value, $"Front terminal must be selected" );
                ConfigureTriggerPlan( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, 1, ScannerMeterSettings.Get().MeterTriggerSource );
                // ScannerMeterTests.ConfigureTriggerPlan(K2002Device, Integer.MaxValue, 1, ScannerMeterSettings.Get.MeterTriggerSource)
                ConfigureMeasurement( K2002Device );
                ConfigureLimitBinning( K2002Device, DigitalActiveLevels.Low );
                ConfigureTrace( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, true );
                if ( TriggerSources.External == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    ConfigureBusTriggerPlan( k7000Device );
                    AssertBusTriggersBufferTrace( k7000Device, K2002Device );
                }
                else if ( TriggerSources.Bus == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    AssertBusTriggersBufferTrace( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                }

                if ( K2002Device.TraceSubsystem.IsDataTraceEnabled() )
                {
                    ValidateBufferReadings( ShowBufferReadings( K2002Device ), K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, k7000Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
            }
        }

        /// <summary> (Unit Test Method) tests bus trigger buffer trace using meter bus. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void MeterBusTriggersBufferTraceTest()
        {
            ScannerMeterSettings.Get().MeterTriggerSource = TriggerSources.Bus;
            AssertBusTriggersBufferTraceTest();
        }

        /// <summary> (Unit Test Method) tests bus trigger buffer trace using scanner bus. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void ScannerBusTriggersBufferTraceTest()
        {
            ScannerMeterSettings.Get().MeterTriggerSource = TriggerSources.External;
            AssertBusTriggersBufferTraceTest();
        }


        #endregion

        #region " BUS TRIGGERS FETCH READING "

        /// <summary> Fetches buffer reading. </summary>
        /// <remarks> Does not assume the trigger plan is idle. </remarks>
        /// <param name="device">           The 2002. </param>
        /// <param name="triggerNumber">    The trigger number. </param>
        /// <param name="initialTimestamp"> The initial timestamp. </param>
        /// <param name="elapsedTime">      The elapsed time. </param>
        private static void FetchBufferReading( K2002.K2002Device device, int triggerNumber, DateTimeOffset initialTimestamp, TimeSpan elapsedTime )
        {
            string title = device.OpenResourceTitle;
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );

            // the measurement condition is read without affecting its value (unlike the measurement event status)
            // measurementEvent = MeasurementEventBitmask.ReadingAvailable
            // propertyName = NameOf(MeasurementEventBitmask.ReadingAvailable).SplitWords
            // measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
            // Assert.IsTrue(measurementBitmasks.IsAnyBitOn(measurementEvent), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} bitmask 0x{measurementBitmasks(measurementEvent):X4} should be on after the first trigger")

            // propertyName = NameOf(MeasurementEventBitmask.ReadingAvailable).SplitWords
            // measurementBitmasks.Status = k2002.StatusSubsystem.QueryMeasurementEventCondition.GetValueOrDefault(0)
            // Assert.IsTrue(measurementBitmasks.IsAnyBitOn(measurementEvent), $"[{title}].[{propertyName}]=0x{measurementBitmasks.Status:X4} bitmask 0x{measurementBitmasks(measurementEvent):X4} should be on after the first trigger")

            propertyName = MeasurementEventBitmaskKey.ReadingAvailable.ToString().SplitWords();
            bool timedOut;
            int? actualBitmask;
            int expectedBitmask = device.StatusSubsystem.MeasurementEventsBitmasks[( int ) MeasurementEventBitmaskKey.ReadingAvailable];
            var task = Pith.SessionBase.StartAwaitingBitmaskTask( expectedBitmask, TimeSpan.FromMilliseconds( 200d ), () => device.StatusSubsystem.QueryMeasurementEventStatus() );
            timedOut = task.Result.TimedOut;
            actualBitmask = task.Result.StatusByte;
            Assert.IsFalse( timedOut, $"Trigger#{triggerNumber}: {propertyName} should not timeout" );
            propertyName = nameof( StatusSubsystemBase.MeasurementEventStatus ).SplitWords();
            Assert.AreEqual( expectedBitmask, ( object ) (actualBitmask & expectedBitmask), $"Trigger#{triggerNumber}: {propertyName} should match" );
            int actualNewReadingsCount = device.TraceSubsystem.AddBufferReadings( device.SenseSubsystem.FunctionUnit, initialTimestamp, elapsedTime );
            int expectedReadingCount = 1;
            Assert.AreEqual( expectedReadingCount, actualNewReadingsCount, "Reading count should match" );
            Assert.AreEqual( triggerNumber, device.TraceSubsystem.BufferReadingsBindingList.Count, "Number of reading in binding list should match the trigger number" );
        }

        /// <summary> Assert bus trigger fetch stream. </summary>
        /// <remarks>
        /// The operation register triggering bit, which turns on after the trigger, can be used to
        /// identify the presence or a reading. At this time, it seems the MSB is not reading available
        /// is not turned on even after a long delay.
        /// <list type="bullet">2020-01-10<item>
        /// OperationEventCondition=0x0400  </item><item>
        /// OperationEventCondition=0x0000  </item><item>
        /// Reading #1=100.28147 Ω 0x0 1007ms  </item><item>
        /// Reading #2=100.28132 Ω 0x0 2086ms  </item><item>
        /// Reading #3=100.28106 Ω 0x0 3140ms  </item><item>
        /// Reading #4=100.28106 Ω 0x0 4192ms  </item><item>
        /// Reading #5=100.28121 Ω 0x0 5247ms  </item><item>
        /// Reading #6=100.28151 Ω 0x0 6300ms  </item><item>
        /// Reading #7=100.28113 Ω 0x0 7351ms  </item><item>
        /// Reading #8=100.28138 Ω 0x0 8401ms  </item><item>
        /// Reading #9=100.28144 Ω 0x0 9458ms  </item><item>
        /// Reading #10=100.28175 Ω 0x0 10519ms  </item><item>
        /// OperationEventCondition=0x0400      </item></list>
        /// </remarks>
        /// <param name="k7000"> The Keithley 7001 device. </param>
        /// <param name="k2002"> The Keithley 2002 device. </param>
        private static void AssertBusTriggersFetchReading( K7000.K7000Device k7000, K2002.K2002Device k2002 )
        {
            bool actualOperationIdle = true;
            string title = k2002.OpenResourceTitle;
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );

            // enable all measurement status bits.
            k2002.Session.ApplyServiceRequestEnableBitmask( k2002.Session.DefaultServiceRequestEnableBitmask );
            _ = k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask( 0x7FFF );
            _ = k2002.StatusSubsystem.ApplyOperationEventEnableBitmask( 0x7FFF );
            InitiateTriggerPlan( k2002 );
            try
            {
                // start the K7000 trigger model
                k7000.TriggerSubsystem.Initiate();
                var initalTimestamp = DateTimeOffset.Now;
                var sw = Stopwatch.StartNew();
                try
                {
                    for ( int i = 1, loopTo = K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount; i <= loopTo; i++ )
                    {
                        // this delay is used to allow the tester to see the readings come in on the instruments.
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        k7000.Session.AssertTrigger();
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        FetchBufferReading( k2002, i, initalTimestamp, sw.Elapsed );
                    }

                    propertyName = nameof( StatusSubsystemBase.QueryOperationEventCondition ).SplitWords();
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                    actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks[( int ) OperationEventBitmaskKey.Idle]:X4} bit should high (idle) after triggering is completed" );
                    AssertBufferReadings( k2002 );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    k7000.TriggerSubsystem.Abort();
                    _ = k7000.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
                    _ = k7000.RouteSubsystem.QueryClosedChannels();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                k2002.TriggerSubsystem.Abort();
                TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted" );
            }
        }

        /// <summary> Assert bus trigger fetch stream. </summary>
        /// <remarks>
        /// The operation register triggering bit, which turns on after the trigger, can be used to
        /// identify the presence or a reading. At this time, it seems the MSB is not reading available
        /// is not turned on even after a long delay.
        /// <list type="bullet">2020-04-09<item>
        /// OperationEventCondition=0x0400</item><item>
        /// OperationEventCondition=0x0000</item><item>
        /// Reading #1=100.181211 Ω 0x0000 1006ms</item><item>
        /// Reading #2=100.17957 Ω 0x0000 2071ms</item><item>
        /// Reading #3=100.178239 Ω 0x0000 3121ms</item><item>
        /// Reading #4=100.175739 Ω 0x0000 4175ms</item><item>
        /// Reading #5=100.176403 Ω 0x0000 5227ms</item><item>
        /// Reading #6=100.176956 Ω 0x0000 6279ms</item><item>
        /// Reading #7=100.176946 Ω 0x0000 7328ms</item><item>
        /// Reading #8=100.174168 Ω 0x0000 8380ms</item><item>
        /// Reading #9=100.171759 Ω 0x0000 9436ms</item><item>
        /// Reading #10=100.170275 Ω 0x0000 10487ms  </item><item>
        /// OperationEventCondition=0x0400 </item></list>
        /// </remarks>
        /// <param name="k2002">        The Keithley 2002 device. </param>
        /// <param name="triggerCount"> Number of triggers. </param>
        private static void AssertBusTriggersFetchReading( K2002.K2002Device k2002, int triggerCount )
        {
            bool actualOperationIdle = true;
            string title = k2002.OpenResourceTitle;
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );

            // enable all measurement status bits.
            k2002.Session.ApplyServiceRequestEnableBitmask( k2002.Session.DefaultServiceRequestEnableBitmask );
            _ = k2002.StatusSubsystem.ApplyMeasurementEventEnableBitmask( 0x7FFF );
            _ = k2002.StatusSubsystem.ApplyOperationEventEnableBitmask( 0x7FFF );
            InitiateTriggerPlan( k2002 );
            try
            {
                var initalTimestamp = DateTimeOffset.Now;
                var sw = Stopwatch.StartNew();
                try
                {
                    for ( int i = 1, loopTo = triggerCount; i <= loopTo; i++ )
                    {
                        // this delay is used to allow the tester to see the readings come in on the instruments.
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        k2002.Session.AssertTrigger();
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        FetchBufferReading( k2002, i, initalTimestamp, sw.Elapsed );
                    }

                    propertyName = nameof( StatusSubsystemBase.QueryOperationEventCondition ).SplitWords();
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                    actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] 0x{operationBitmasks[( int ) OperationEventBitmaskKey.Idle]:X4} bit should high (idle) after triggering is completed" );
                    AssertBufferReadings( k2002 );
                }
                catch
                {
                    throw;
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                k2002.TriggerSubsystem.Abort();
                TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                Assert.IsTrue( actualOperationIdle, $"[{title}].[{propertyName}] should be true before triggering is aborted" );
            }
        }

        /// <summary> (Unit Test Method) tests bus triggers fetch reading. </summary>
        /// <remarks> 2020-01-10. </remarks>
        public static void AssertBusTriggersFetchReading()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
                Assert.IsTrue( K2002Device.SystemSubsystem.QueryFrontTerminalsSelected().Value, $"Front terminal must be selected" );
                ConfigureTriggerPlan( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, 1, ScannerMeterSettings.Get().MeterTriggerSource );
                ConfigureMeasurement( K2002Device );
                ConfigureLimitBinning( K2002Device, DigitalActiveLevels.Low );
                ConfigureTrace( K2002Device );
                if ( TriggerSources.External == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    ConfigureBusTriggerPlan( k7000Device );
                    AssertBusTriggersFetchReading( k7000Device, K2002Device );
                }
                else if ( TriggerSources.Bus == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    ConfigureBusTriggerPlan( k7000Device );
                    AssertBusTriggersFetchReading( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, k7000Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
            }
        }

        /// <summary> (Unit Test Method) tests bus triggers fetch reading meter bus. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void MeterBusTriggersFetchReadingTest()
        {
            ScannerMeterSettings.Get().MeterTriggerSource = TriggerSources.Bus;
            AssertBusTriggersFetchReading();
        }

        /// <summary> (Unit Test Method) tests bus triggers fetch reading scanner bus. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void ScannerBusTriggersFetchReadingTest()
        {
            ScannerMeterSettings.Get().MeterTriggerSource = TriggerSources.External;
            AssertBusTriggersFetchReading();
        }


        #endregion

        #region " BUS TRIGGERS STREAM "

        /// <summary> Handles the property change. </summary>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private static void HandlePropertyChange( TraceSubsystemBase sender, string propertyName )
        {
            if ( sender is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( TraceSubsystemBase.BufferReadingsCount ):
                        {
                            if ( sender.BufferReadingsCount > 0 )
                            {
                                TestInfo.TraceMessage( $"Streaming reading #{sender.BufferReadingsCount}: {sender.LastBufferReading.Amount} 0x{sender.LastBufferReading.StatusWord:X4}" );
                            }

                            break;
                        }
                }
            }
        }

        /// <summary> Handles the trace subsystem property change. </summary>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static void HandleTraceSubsystemPropertyChange( object sender, PropertyChangedEventArgs e )
        {
            try
            {
                if ( sender is object && e is object )
                {
                    HandlePropertyChange( sender as TraceSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Assert.Fail( ex.ToString() );
            }
        }

        /// <summary> Restore Trigger State. </summary>
        /// <param name="device"> The device. </param>
        private static void RestoreState( K2002.K2002Device device )
        {
            device.TriggerSubsystem.Abort();
            _ = device.Session.QueryOperationCompleted();
            device.ResetKnownState();
            _ = device.Session.QueryOperationCompleted();
            device.ClearExecutionState();
            _ = device.Session.QueryOperationCompleted();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.Session.EnableServiceRequestWaitComplete();
        }

        /// <summary> Stops buffer streaming. </summary>
        /// <param name="device"> The device. </param>
        private static void StopBufferStreaming( K2002.K2002Device device )
        {
            var timeout = TraceSubsystemBase.EstimateStreamStopTimeoutInterval( device.TraceSubsystem.StreamCycleDuration, ScannerMeterSettings.Get().BufferStreamPollInteval, 1.5d );
            var (Success, Details) = device.TraceSubsystem.StopBufferStream( timeout );
            if ( Success )
            {
                device.TriggerSubsystem.Abort();
                _ = device.Session.ReadStatusRegister();
                _ = device.TriggerSubsystem.QueryTriggerState();
                _ = device.Session.ReadStatusRegister();
                TestInfo.TraceMessage( $"Streaming ended with {Details}" );
            }
            else
            {
                Assert.Fail( $"buffer streaming failed; {Details}" );
            }

            device.TraceSubsystem.PropertyChanged -= HandleTraceSubsystemPropertyChange;
        }

        /// <summary> Starts buffer streaming. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="device"> The device. </param>
        private static void StartBufferStreaming( K2002.K2002Device device )
        {
            device.TraceSubsystem.PropertyChanged += HandleTraceSubsystemPropertyChange;
            Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed before starting streaming" );
            device.TriggerSubsystem.Initiate();
            Core.ApplianceBase.DoEvents();
            device.TraceSubsystem.BufferStreamTasker.AsyncCompleted += ( sender, e ) => { try { if ( e.Error is object ) { Assert.IsNull( e.Error, $"Exception streaming: {e.Error.ToFullBlownString()}" ); } } finally { StopBufferStreaming( device ); } };
            device.TraceSubsystem.StartBufferStream( device.TriggerSubsystem, ScannerMeterSettings.Get().BufferStreamPollInteval, device.SenseSubsystem.FunctionUnit );
        }

        #endregion

        #region " BUS TRIGGERS STREAM READING "

        /// <summary> Assert bus trigger scan. </summary>
        /// <remarks>
        /// <list type="bullet">2020-01-10<item>
        /// OperationEventCondition=0x0400    </item><item>
        /// Streaming reading #1: 100.29333 Ω 0x0000    </item><item>
        /// Streaming reading #2: 100.29309 Ω 0x0000    </item><item>
        /// Streaming reading #3: 100.29311 Ω 0x0000    </item><item>
        /// Streaming reading #4: 100.29301 Ω 0x0000    </item><item>
        /// Streaming reading #5: 100.29288 Ω 0x0000    </item><item>
        /// Streaming reading #6: 100.29291 Ω 0x0000    </item><item>
        /// Streaming reading #7: 100.29242 Ω 0x0000    </item><item>
        /// Streaming reading #8: 100.29265 Ω 0x0000    </item><item>
        /// Streaming reading #9: 100.29249 Ω 0x0000    </item><item>
        /// Streaming reading #10: 100.2923 Ω 0x0000    </item><item>
        /// Reading #1=100.29333 Ω 0x0000 622ms     </item><item>
        /// Reading #2=100.29309 Ω 0x0000 1614ms    </item><item>
        /// Reading #3=100.29311 Ω 0x0000 2621ms    </item><item>
        /// Reading #4=100.29301 Ω 0x0000 3621ms    </item><item>
        /// Reading #5=100.29288 Ω 0x0000 4639ms    </item><item>
        /// Reading #6=100.29291 Ω 0x0000 5668ms    </item><item>
        /// Reading #7=100.29242 Ω 0x0000 6696ms    </item><item>
        /// Reading #8=100.29265 Ω 0x0000 7711ms    </item><item>
        /// Reading #9=100.29249 Ω 0x0000 8736ms    </item><item>
        /// Reading #10=100.2923 Ω 0x0000 9740ms    </item><item>
        /// OperationEventCondition=0x0400          </item></list>
        /// </remarks>
        /// <param name="k7000"> The Keithley 7001 device. </param>
        /// <param name="k2002"> The Keithley 2002 device. </param>
        internal static void AssertBusTriggersStreamReadings( K7000.K7000Device k7000, K2002.K2002Device k2002 )
        {
            bool actualOperationIdle = true;
            string K2002Title = k2002.OpenResourceTitle;
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
            actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
            Assert.IsTrue( actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true before triggering is initiated" );
            TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
            StartBufferStreaming( k2002 );

            // operationBitmasks.Status = K2002.StatusSubsystem.QueryOperationEventCondition.Value
            // actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmask.Idle)
            // Assert.IsFalse(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be false after triggering is initiated")
            // ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{K2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

            try
            {
                // start the K7000 trigger model
                k7000.TriggerSubsystem.Initiate();
                try
                {
                    for ( int i = 1, loopTo = K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount; i <= loopTo; i++ )
                    {
                        // this delay is used to allow the operator to see the values as they come in.
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                        k7000.Session.AssertTrigger();
                        Core.ApplianceBase.DoEventsWait( K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                    }

                    // NOTE: this causes a query interrupted error on occasion and needs to be investigated further.
                    StopBufferStreaming( k2002 );
                    propertyName = nameof( StatusSubsystemBase.QueryOperationEventCondition ).SplitWords();
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                    actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    Assert.IsTrue( actualOperationIdle, $"[{K2002Title}].[{propertyName}] 0x{operationBitmasks[( int ) OperationEventBitmaskKey.Idle]:X4} bit should high (idle) after triggering is completed" );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    k7000.TriggerSubsystem.Abort();
                    _ = k7000.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
                    _ = k7000.RouteSubsystem.QueryClosedChannels();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                StopBufferStreaming( k2002 );
                TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                Assert.IsTrue( actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true before triggering is aborted" );
            }
        }

        /// <summary> Assert bus trigger scan. </summary>
        /// <remarks>
        /// <list type="bullet">2020-04-09<item>
        /// OperationEventCondition=0x0400 </item><item>
        /// Streaming reading #1: 100.147557 Ω 0x0000  </item><item>
        /// Streaming reading #2: 100.14756 Ω 0x0000  </item><item>
        /// Streaming reading #3: 100.147442 Ω 0x0000  </item><item>
        /// Streaming reading #4: 100.147776 Ω 0x0000  </item><item>
        /// Streaming reading #5: 100.147449 Ω 0x0000  </item><item>
        /// Streaming reading #6: 100.147428 Ω 0x0000  </item><item>
        /// Streaming reading #7: 100.146959 Ω 0x0000  </item><item>
        /// Streaming reading #8: 100.147494 Ω 0x0000  </item><item>
        /// Streaming reading #9: 100.147884 Ω 0x0000  </item><item>
        /// Streaming reading #10: 100.14747 Ω 0x0000  </item><item>
        /// Reading #1=100.147557 Ω 0x0000 617ms  </item><item>
        /// Reading #2=100.14756 Ω 0x0000 1554ms  </item><item>
        /// Reading #3=100.147442 Ω 0x0000 2612ms  </item><item>
        /// Reading #4=100.147776 Ω 0x0000 3561ms  </item><item>
        /// Reading #5=100.147449 Ω 0x0000 4640ms  </item><item>
        /// Reading #6=100.147428 Ω 0x0000 5595ms  </item><item>
        /// Reading #7=100.146959 Ω 0x0000 6610ms  </item><item>
        /// Reading #8=100.147494 Ω 0x0000 7623ms  </item><item>
        /// Reading #9=100.147884 Ω 0x0000 8704ms  </item><item>
        /// Reading #10=100.14747 Ω 0x0000 9653ms  </item><item>
        /// OperationEventCondition=0x0400OperationEventCondition=0x0400 </item></list>
        /// </remarks>
        /// <param name="k2002">        The Keithley 2002 device. </param>
        /// <param name="triggerCount"> Number of triggers. </param>
        /// <param name="delay">        The delay. </param>
        internal static void AssertBusTriggersStreamReadings( K2002.K2002Device k2002, int triggerCount, TimeSpan delay )
        {
            bool actualOperationIdle = true;
            string K2002Title = k2002.OpenResourceTitle;
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );
            var measurementBitmasks = new MeasurementEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( measurementBitmasks );
            string operationConditionEventPropertyName = nameof( StatusSubsystemBase.OperationEventCondition );
            string propertyName = nameof( OperationEventBitmaskKey.Idle ).SplitWords();
            operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
            actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
            Assert.IsTrue( actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true before triggering is initiated" );
            TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
            StartBufferStreaming( k2002 );

            // operationBitmasks.Status = K2002.StatusSubsystem.QueryOperationEventCondition.Value
            // actualOperationIdle = operationBitmasks.IsAnyBitOn(OperationEventBitmask.Idle)
            // Assert.IsFalse(actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be false after triggering is initiated")
            // ScannerMeterTests.TestInfo.TraceMessage($"{operationConditionEventPropertyName}=0x{K2002.StatusSubsystem.QueryOperationEventCondition.Value:X4}")

            try
            {
                try
                {
                    for ( int i = 1, loopTo = triggerCount; i <= loopTo; i++ )
                    {
                        // this delay is used to allow the operator to see the values as they come in.
                        Core.ApplianceBase.DoEventsWait( delay );
                        k2002.TraceSubsystem.BusTriggerRequested = true;
                        // k2002.Session.AssertTrigger()
                        Core.ApplianceBase.DoEventsWait( delay );
                        RequestBinningStrobe( k2002, true );
                    }

                    // NOTE: this causes a query interrupted error on occasion and needs to be investigated further.
                    StopBufferStreaming( k2002 );
                    propertyName = nameof( StatusSubsystemBase.QueryOperationEventCondition ).SplitWords();
                    operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                    actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    Assert.IsTrue( actualOperationIdle, $"[{K2002Title}].[{propertyName}] 0x{operationBitmasks[( int ) OperationEventBitmaskKey.Idle]:X4} bit should high (idle) after triggering is completed" );
                }
                catch
                {
                    throw;
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                StopBufferStreaming( k2002 );
                TestInfo.TraceMessage( $"{operationConditionEventPropertyName}=0x{k2002.StatusSubsystem.QueryOperationEventCondition().Value:X4}" );
                operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                actualOperationIdle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                Assert.IsTrue( actualOperationIdle, $"[{K2002Title}].[{propertyName}] should be true after triggering is aborted" );
            }
        }

        /// <summary> Assert bus trigger scan. </summary>
        public static void AssertBusTriggersStreamReadings()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                Assert.IsTrue( K2002Device.SystemSubsystem.QueryFrontTerminalsSelected().Value, $"Front terminal must be selected" );
                ConfigureTriggerPlan( K2002Device, int.MaxValue, 1, ScannerMeterSettings.Get().MeterTriggerSource );
                ConfigureMeasurement( K2002Device );
                ConfigureLimitBinning( K2002Device, DigitalActiveLevels.Low );
                ConfigureTrace( K2002Device );
                if ( TriggerSources.Bus == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    AssertBusTriggersStreamReadings( K2002Device, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerCount, K7000Properties.K7000SubsystemInfo.BusTriggerTestTriggerDelay );
                }
                else if ( TriggerSources.External == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
                    ConfigureBusTriggerPlan( k7000Device );
                    AssertBusTriggersStreamReadings( k7000Device, K2002Device );
                }

                AssertBufferReadings( K2002Device );
            }
            catch
            {
                throw;
            }
            finally
            {
                RestoreState( K2002Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
                if ( TriggerSources.External == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    DeviceManager.CloseSession( TestInfo, k7000Device );
                }
            }
        }

        /// <summary> (Unit Test Method) tests stream readings using meter bus. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void MeterBusTriggersStreamReadingsTest()
        {
            ScannerMeterSettings.Get().MeterTriggerSource = TriggerSources.Bus;
            AssertBusTriggersStreamReadings();
        }

        /// <summary> (Unit Test Method) tests stream readings using scanner bus. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void ScannerBusTriggersStreamReadingsTest()
        {
            ScannerMeterSettings.Get().MeterTriggerSource = TriggerSources.External;
            AssertBusTriggersStreamReadings();
        }

        #endregion

        #region " TBA: AUTO TRIGGER AUTONOMOUS MEASUREMENTS "

        /// <summary> Configure externalrigger scan. </summary>
        /// <param name="device"> The device. </param>
        private static void ConfigureExternalriggerScan( K7000.K7000Device device )
        {
            _ = nameof( RouteSubsystemBase.ScanList ).SplitWords();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.TriggerSubsystem.Abort();
            device.Session.EnableServiceRequestWaitComplete();
            _ = device.RouteSubsystem.WriteOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.Session.ReadStatusRegister();
            _ = device.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
            _ = device.RouteSubsystem.QueryClosedChannels();
            string propertyName = nameof( RouteSubsystemBase.ScanList ).SplitWords();
            string expectedScanList = K7000Properties.K7000SubsystemInfo.AutonomousTriggerTestScanList;
            string actualScanList = device.RouteSubsystem.ApplyScanList( expectedScanList );
            Assert.AreEqual( expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'" );
            _ = device.ArmLayer1Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer1Subsystem.ApplyArmCount( 1 );
            _ = device.ArmLayer2Subsystem.ApplyArmSource( ArmSources.Immediate );
            _ = device.ArmLayer2Subsystem.ApplyArmCount( 1 );
            _ = device.ArmLayer2Subsystem.ApplyDelay( TimeSpan.Zero );

            // device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Autonomous)
            propertyName = nameof( TriggerSubsystemBase.TriggerSource ).SplitWords();
            var expectedTriggerSource = TriggerSources.External;
            var actualTriggerSource = device.TriggerSubsystem.ApplyTriggerSource( expectedTriggerSource );
            Assert.IsTrue( actualTriggerSource.HasValue, $"{propertyName} should have a value" );
            Assert.AreEqual( expectedTriggerSource, actualTriggerSource.Value, $"{propertyName} should match" );
            _ = device.TriggerSubsystem.ApplyTriggerCount( K7000Properties.K7000SubsystemInfo.AutonomousTriggerTestTriggerCount ); // in place of infinite
            _ = device.TriggerSubsystem.ApplyDelay( TimeSpan.Zero );
            _ = device.TriggerSubsystem.ApplyTriggerLayerBypassMode( TriggerLayerBypassModes.Source );
        }

        /// <summary> Assert autonomous trigger scan. </summary>
        /// <param name="k7000">   The Keithley 7001 device. </param>
        /// <param name="k2002">   The Keithley 2002 device. </param>
        /// <param name="timeout"> The timeout. </param>
        private static void AssertAutonomousTriggerScan( K7000.K7000Device k7000, K2002.K2002Device k2002, TimeSpan timeout )
        {
            var operationBitmasks = new OperationEventsBitmaskDictionary();
            K2002.StatusSubsystem.DefineBitmasks( operationBitmasks );

            // start the K2002 trigger model
            k2002.TriggerSubsystem.Initiate();
            try
            {
                // start the K7000 trigger model
                k7000.TriggerSubsystem.Initiate();
                try
                {
                    var sw = Stopwatch.StartNew();
                    bool K2002Idle = false;
                    bool timedout = sw.Elapsed > timeout;
                    while ( !K2002Idle && !timedout )
                    {
                        timedout = sw.Elapsed > timeout;
                        Core.ApplianceBase.DoEventsWait( TimeSpan.FromSeconds( 10d ) );
                        operationBitmasks.Status = k2002.StatusSubsystem.QueryOperationEventCondition().Value;
                        K2002Idle = operationBitmasks.IsAnyBitOn( OperationEventBitmaskKey.Idle );
                    }

                    Assert.IsFalse( timedout, $"{nameof( AssertAutonomousTriggerScan ).SplitWords()} should complete before timeout time {timeout:s.\\f}" );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    k7000.TriggerSubsystem.Abort();
                    _ = k7000.RouteSubsystem.ApplyOpenAll( TimeSpan.FromSeconds( 1d ) );
                    _ = k7000.RouteSubsystem.QueryClosedChannels();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                k2002.TriggerSubsystem.Abort();
            }
        }

        /// <summary> Tests autonomous scan. </summary>
        public static void AutonomousScanTest()
        {
            if ( !K2002Properties.K2002ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K2002Properties.K2002ResourceInfo.ResourceTitle} not found" );
            if ( !K7000Properties.K7000ResourceInfo.ResourcePinged )
                Assert.Inconclusive( $"{K7000Properties.K7000ResourceInfo.ResourceTitle} not found" );
            using var k7000Device = K7000.K7000Device.Create();
            k7000Device.AddListener( TestInfo.TraceMessagesQueueListener );
            using var K2002Device = K2002.K2002Device.Create();
            K2002Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K2002Device, K2002Properties.K2002ResourceInfo );
                DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, k7000Device, K7000Properties.K7000ResourceInfo );
                Assert.IsTrue( K2002Device.SystemSubsystem.QueryFrontTerminalsSelected().Value, $"Front terminal must be selected" );
                ConfigureTriggerPlan( K2002Device, K7000Properties.K7000SubsystemInfo.AutonomousTriggerTestTriggerCount, 1, ScannerMeterSettings.Get().MeterTriggerSource );
                if ( TriggerSources.Bus == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                }
                else if ( TriggerSources.External == ScannerMeterSettings.Get().MeterTriggerSource )
                {
                    ConfigureExternalriggerScan( k7000Device );
                    AssertAutonomousTriggerScan( k7000Device, K2002Device, TimeSpan.FromSeconds( 10d ) );
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, k7000Device );
                DeviceManager.CloseSession( TestInfo, K2002Device );
            }
        }

        #endregion

    }
}
