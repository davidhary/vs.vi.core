﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K7510.Forms
{
    [DesignerGenerated()]
    public partial class TraceBufferUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _TraceBufferView = new TraceBuffeView();
            SuspendLayout();
            // 
            // _TraceBufferView
            // 
            _TraceBufferView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _TraceBufferView.Dock = System.Windows.Forms.DockStyle.Fill;
            _TraceBufferView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TraceBufferView.Location = new System.Drawing.Point(1, 1);
            _TraceBufferView.Name = "_TraceBufferView";
            _TraceBufferView.Padding = new System.Windows.Forms.Padding(1);
            _TraceBufferView.Size = new System.Drawing.Size(407, 272);
            _TraceBufferView.TabIndex = 0;
            // 
            // TraceBufferUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_TraceBufferView);
            Name = "TraceBufferUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(409, 274);
            ResumeLayout(false);
        }

        private TraceBuffeView _TraceBufferView;
    }
}