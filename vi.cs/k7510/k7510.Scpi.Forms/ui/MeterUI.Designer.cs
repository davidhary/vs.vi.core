﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K7510.Forms
{
    [DesignerGenerated()]
    public partial class MeterUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _DataGridView = new System.Windows.Forms.DataGridView();
            _MeterView = new MeterView();
            ((System.ComponentModel.ISupportInitialize)_DataGridView).BeginInit();
            SuspendLayout();
            // 
            // _DataGridView
            // 
            _DataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _DataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            _DataGridView.Location = new System.Drawing.Point(1, 136);
            _DataGridView.Name = "_DataGridView";
            _DataGridView.Size = new System.Drawing.Size(375, 216);
            _DataGridView.TabIndex = 23;
            // 
            // _MeterView
            // 
            _MeterView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _MeterView.Dock = System.Windows.Forms.DockStyle.Top;
            _MeterView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _MeterView.Location = new System.Drawing.Point(1, 1);
            _MeterView.Name = "_MeterView";
            _MeterView.Padding = new System.Windows.Forms.Padding(1);
            _MeterView.Size = new System.Drawing.Size(375, 135);
            _MeterView.TabIndex = 24;
            // 
            // MeterView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_DataGridView);
            Controls.Add(_MeterView);
            Name = "MeterView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(377, 353);
            ((System.ComponentModel.ISupportInitialize)_DataGridView).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.DataGridView _DataGridView;
        private MeterView _MeterView;
    }
}