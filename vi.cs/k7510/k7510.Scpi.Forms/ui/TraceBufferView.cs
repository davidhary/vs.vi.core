using System.ComponentModel;

namespace isr.VI.K7510.Forms
{

    /// <summary>
    /// A Keithley 7510 edition of the basic <see cref="Facade.TraceBufferView"/> user interface.
    /// </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public class TraceBuffeView : Facade.TraceBufferView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="TraceBuffeView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="TraceBuffeView"/>. </returns>
        public static new TraceBuffeView Create()
        {
            TraceBuffeView view = null;
            try
            {
                view = new TraceBuffeView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K7510Device K7510Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        public void AssignDevice( K7510Device value )
        {
            base.AssignDevice( value );
            this.K7510Device = value;
            if ( value is null )
            {
                this.BindSubsystem( default, "Trace" );
            }
            else
            {
                this.BindSubsystem( this.K7510Device.TraceSubsystem, "Trace" );
            }
        }

        #endregion

    }
}
