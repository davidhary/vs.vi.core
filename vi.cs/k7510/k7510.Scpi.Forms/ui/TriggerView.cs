using System;
using System.Windows.Forms;

namespace isr.VI.K7510.Forms
{

    /// <summary>
    /// A Keithley 7510 edition of the basic <see cref="Facade.TriggerView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class TriggerView : Facade.TriggerView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public TriggerView() : base()
        {
        }

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static new TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K7510Device K7510Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K7510Device value )
        {
            base.AssignDevice( value );
            this.K7510Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 1 );
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 2 );
                this.BindSubsystem( ( TriggerSubsystemBase ) default, 1 );
            }
            else
            {
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 1 );
                this.BindSubsystem( ( ArmLayerSubsystemBase ) default, 2 );
                this.BindSubsystem( this.K7510Device.TriggerSubsystem, 1 );
            }
        }

        #endregion

        #region " INITIATE WAIT FETCH "

        /// <summary> Initiate wait read. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public override void InitiateWaitRead()
        {
            if ( this.InitializingComponents )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.K7510Device.ResourceNameCaption} clearing execution state";
                this.K7510Device.ClearExecutionState();
                activity = $"{this.K7510Device.ResourceNameCaption} initiating wait read";

                // Me.K7510device.ClearExecutionState()
                // set the service request
                // Me.K7510device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
                // Me.K7510device.StatusSubsystem.EnableServiceRequest(Me.K7510device.Session.DefaultOperationServiceRequestEnableBitmask)
                // Me.K7510device.Session.Write("*SRE 1") ' Set MSB bit of SRE register
                // Me.K7510device.Session.Write("stat:meas:ptr 32767; ntr 0; enab 512") ' Set all PTR bits and clear all NTR bits for measurement events Set Buffer Full bit of Measurement
                // Me.K7510device.Session.Write(":trac:feed calc") ' Select Calculate as reading source
                // Me.K7510device.Session.Write(":trac:poin 10")   ' Set buffer size to 10 points 
                // Me.K7510device.Session.Write(":trac:egr full")  ' Select Full element group

                // trigger the initiation of the measurement letting the triggering or service request do the rest.
                activity = $"{this.K7510Device.ResourceNameCaption} Initiating meter";
                _ = this.PublishVerbose( $"{activity};. " );
                this.K7510Device.TriggerSubsystem.Initiate();
            }
            catch ( Exception ex )
            {
                this.K7510Device.Session.StatusPrompt = $"failed {activity}";
                activity = this.PublishException( activity, ex );
                _ = this.InfoProvider.Annunciate( this, Core.Forma.InfoProviderLevel.Error, activity );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

    }
}
