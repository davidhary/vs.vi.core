using System;

namespace isr.VI.K7510.Forms
{

    /// <summary>
    /// A Keithley 7510 edition of the basic <see cref="Facade.BufferStreamView"/> user interface.
    /// </summary>
    /// <remarks>
    /// David, 2020-01-11 <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class BufferStreamView : Facade.BufferStreamView
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Creates a new <see cref="BufferStreamView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="BufferStreamView"/>. </returns>
        public static new BufferStreamView Create()
        {
            BufferStreamView view = null;
            try
            {
                view = new BufferStreamView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> Gets or sets the device for this class. </summary>
        /// <value> The device for this class. </value>
        private K7510Device K7510Device { get; set; }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K7510Device value )
        {
            base.AssignDevice( value );
            this.K7510Device = value;
            if ( value is null )
            {
                this.BindSubsystem( ( BinningSubsystemBase ) default );
                this.BindSubsystem( ( SenseFunctionSubsystemBase ) default );
                this.BindSubsystem( ( SenseSubsystemBase ) default );
                this.BindSubsystem( ( SystemSubsystemBase ) default );
                this.BindSubsystem( ( TraceSubsystemBase ) default );
                this.BindSubsystem( ( TriggerSubsystemBase ) default );
                this.BindSubsystem( ( DigitalOutputSubsystemBase ) default );
                this.BindSubsystem( ( RouteSubsystemBase ) default );
                this.BindSubsystem1( default );
                this.BindSubsystem1( default );
                this.BindSubsystem( default, string.Empty );
            }
            else
            {
                this.BindSubsystem( value.BinningResistanceFourWireSubsystem );
                this.BindSubsystem( value.SenseFunctionSubsystem );
                this.BindSubsystem( value.SenseSubsystem );
                this.BindSubsystem( value.SystemSubsystem );
                this.BindSubsystem( value.TraceSubsystem );
                this.BindSubsystem( value.TriggerSubsystem );
                this.BindSubsystem( value.DigitalOutputSubsystem );
                this.BindSubsystem1( default );
                this.BindSubsystem1( default );
                this.BindSubsystem( value.RouteSubsystem );
            }
        }

        #endregion

        #region " SENSE FUNCTION HANDLING "

        /// <summary> Handles the function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected override void HandleFunctionModesChanged( SenseSubsystemBase subsystem )
        {
            base.HandleFunctionModesChanged( subsystem );
            switch ( subsystem.FunctionMode )
            {
                case SenseFunctionModes.CurrentDC:
                    {
                        this.BindSubsystem( this.K7510Device.SenseCurrentSubsystem );
                        break;
                    }

                case SenseFunctionModes.VoltageDC:
                    {
                        this.BindSubsystem( this.K7510Device.SenseVoltageSubsystem );
                        break;
                    }

                case SenseFunctionModes.ResistanceFourWire:
                    {
                        this.BindSubsystem( this.K7510Device.SenseResistanceFourWireSubsystem );
                        break;
                    }

                case SenseFunctionModes.Resistance:
                    {
                        this.BindSubsystem( this.K7510Device.SenseResistanceSubsystem );
                        break;
                    }
            }
        }

        #endregion

        #region " BUFFER STREAM  "

        /// <summary> Configure trigger plan. </summary>
        /// <remarks> David, 2020-07-25. </remarks>
        /// <param name="triggerCount">  Number of triggers. </param>
        /// <param name="sampleCount">   Number of samples. </param>
        /// <param name="triggerSource"> The trigger source. </param>
        protected override void ConfigureTriggerPlan( int triggerCount, int sampleCount, TriggerSources triggerSource )
        {
            this.K7510Device.ClearExecutionState();
            _ = this.K7510Device.TriggerSubsystem.ApplyContinuousEnabled( false );
            var triggerState = this.K7510Device.TriggerSubsystem.QueryTriggerState().GetValueOrDefault( TriggerState.None );
            if ( TriggerState.Running == triggerState )
            {
                this.K7510Device.TriggerSubsystem.Abort();
            }

            this.K7510Device.TriggerSubsystem.ClearTriggerModel();
            this.K7510Device.Session.EnableServiceRequestWaitComplete();
            this.K7510Device.Session.Wait();
            _ = this.K7510Device.Session.ReadStatusRegister();
            this.K7510Device.TriggerSubsystem.ApplyGradeBinning( 10000000, TimeSpan.FromMilliseconds( 1000d * 0.01d ), Facade.My.Settings.Default.FailBitmask, Facade.My.Settings.Default.PasstBitmask, Facade.My.Settings.Default.OverflowBitmask, triggerSource );
            _ = this.K7510Device.TriggerSubsystem.ApplyAutoDelayEnabled( false );
            _ = this.K7510Device.TriggerSubsystem.QueryTriggerState();
        }

        /// <summary> Configure digital output. </summary>
        /// <remarks> David, 2020-08-06. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        protected override void ConfigureDigitalOutput( DigitalOutputSubsystemBase subsystem )
        {
            // not supported.
        }

        /// <summary> Configure limit binning. </summary>
        /// <remarks> David, 2020-07-25. </remarks>
        protected override void ConfigureLimitBinning()
        {
            base.ConfigureLimitBinning();
        }

        /// <summary> Configure trace for fetching only. </summary>
        /// <remarks> David, 2020-08-06. </remarks>
        /// <param name="binningStrokeDuration"> Duration of the binning stroke. </param>
        protected override void ConfigureTrace( TimeSpan binningStrokeDuration )
        {
            base.ConfigureTrace( binningStrokeDuration );
            // the buffer must have at least 10 data points; maximum is 11,000,020 (not 268,000,000)
            // but then the instrument reports a limit of 10,900,020
            _ = this.TraceSubsystem.ApplyPointsCount( 100 );
            // clear the buffer 
            this.TraceSubsystem.ClearBuffer();
        }

        #endregion

    }
}
