﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K7510.Forms
{
    [DesignerGenerated()]
    public partial class BufferStreamUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _ReadingsDataGridView = new System.Windows.Forms.DataGridView();
            _BufferStreamView = new BufferStreamView();
            ((System.ComponentModel.ISupportInitialize)_ReadingsDataGridView).BeginInit();
            SuspendLayout();
            // 
            // _ReadingsDataGridView
            // 
            _ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            _ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            _ReadingsDataGridView.Location = new System.Drawing.Point(1, 28);
            _ReadingsDataGridView.Name = "_ReadingsDataGridView";
            _ReadingsDataGridView.Size = new System.Drawing.Size(375, 324);
            _ReadingsDataGridView.TabIndex = 23;
            // 
            // _BufferStreamView
            // 
            _BufferStreamView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _BufferStreamView.Dock = System.Windows.Forms.DockStyle.Top;
            _BufferStreamView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _BufferStreamView.Location = new System.Drawing.Point(1, 1);
            _BufferStreamView.Name = "_BufferStreamView";
            _BufferStreamView.Size = new System.Drawing.Size(375, 27);
            _BufferStreamView.TabIndex = 24;
            // 
            // BufferView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_ReadingsDataGridView);
            Controls.Add(_BufferStreamView);
            Name = "BufferView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(377, 353);
            ((System.ComponentModel.ISupportInitialize)_ReadingsDataGridView).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.DataGridView _ReadingsDataGridView;
        private BufferStreamView _BufferStreamView;
    }
}