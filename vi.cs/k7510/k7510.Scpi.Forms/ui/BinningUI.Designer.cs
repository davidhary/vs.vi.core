﻿using System.Diagnostics;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K7510.Forms
{
    [DesignerGenerated()]
    public partial class BinningUI
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _BinningView = new BinningView();
            SuspendLayout();
            // 
            // _BinningView
            // 
            _BinningView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            _BinningView.Dock = System.Windows.Forms.DockStyle.Fill;
            _BinningView.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _BinningView.Location = new System.Drawing.Point(1, 1);
            _BinningView.Name = "_BinningView";
            _BinningView.Padding = new System.Windows.Forms.Padding(1);
            _BinningView.Size = new System.Drawing.Size(407, 272);
            _BinningView.TabIndex = 0;
            // 
            // BinningUI
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(_BinningView);
            Name = "BinningUI";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(409, 274);
            ResumeLayout(false);
        }

        private BinningView _BinningView;
    }
}