﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K7510 VI Tests" )]
[assembly: AssemblyDescription( "K7510 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K7510.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
