using System;

namespace isr.VI.K7510.Device.MSTest
{

    /// <summary> The Subsystems Test Information. </summary>
    /// <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-02-12 </para></remarks>
    [System.Runtime.CompilerServices.CompilerGenerated()]
    [System.CodeDom.Compiler.GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0")]
    [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Advanced)]
    internal class StreamSettings : VI.DeviceTests.TestSettingsBase
    {

        #region " SINGLETON "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private StreamSettings() : base()
        {
        }

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration($"{typeof(StreamSettings)} Editor", Get());
        }

        /// <summary>
        /// Gets the locking object to enforce thread safety when creating the singleton instance.
        /// </summary>
        /// <value> The sync locker. </value>
        private static object _SyncLocker { get; set; } = new object();

        /// <summary> Gets the instance. </summary>
        /// <value> The instance. </value>
        private static StreamSettings _Instance { get; set; }

        /// <summary> Instantiates the class. </summary>
        /// <remarks> Use this property to instantiate a single instance of this class. This class uses
        /// lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
        /// <returns> A new or existing instance of the class. </returns>
        public static StreamSettings Get()
        {
            if (_Instance is null)
            {
                lock (_SyncLocker)
                    _Instance = (StreamSettings)Synchronized(new StreamSettings());
            }

            return _Instance;
        }

        /// <summary> Returns true if an instance of the class was created and not disposed. </summary>
        /// <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
        public static bool Instantiated
        {
            get
            {
                lock (_SyncLocker)
                    return _Instance is object;
            }
        }

        #endregion

        #region " BUS TRIGGER TEST "

        /// <summary> Gets or sets the bus trigger test trigger source. </summary>
        /// <value> The bus trigger test trigger source. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Bus")]
        public TriggerSources BusTriggerTestTriggerSource
        {
            get
            {
                return AppSettingEnum<TriggerSources>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets a list of bus trigger test scans. </summary>
        /// <value> A list of bus trigger test scans. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("(@1!1:1!10)")]
        public string BusTriggerTestScanList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of bus trigger test triggers. </summary>
        /// <value> The number of bus trigger test triggers. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("10")]
        public int BusTriggerTestTriggerCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the bus trigger test trigger delay. </summary>
        /// <value> The bus trigger test trigger delay. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("00:00:00.5")]
        public TimeSpan BusTriggerTestTriggerDelay
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

        #region " METER SETTINGS "

        /// <summary> Gets or sets the auto zero Enabled settings. </summary>
        /// <value> The auto zero settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoZeroEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the automatic range enabled. </summary>
        /// <value> The automatic range enabled. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("True")]
        public bool AutoRangeEnabled
        {
            get
            {
                return AppSettingGetter(false);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the Sense Function settings. </summary>
        /// <value> The Sense Function settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("ResistanceFourWire")]
        public SenseFunctionModes SenseFunction
        {
            get
            {
                return AppSettingEnum<SenseFunctionModes>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets the power line cycles settings. </summary>
        /// <value> The power line cycles settings. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public double PowerLineCycles
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the resolution digits. </summary>
        /// <value> The resolution digits. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("9")]
        public double ResolutionDigits
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the expected resistance. </summary>
        /// <value> The expected resistance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("100")]
        public double ExpectedResistance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the resistance tolerance. </summary>
        /// <value> The resistance tolerance. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("0.01")]
        public double ResistanceTolerance
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the open limit. </summary>
        /// <value> The open limit. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("10000")]
        public double OpenLimit
        {
            get
            {
                return AppSettingGetter(0.0d);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the pass output value. </summary>
        /// <value> The pass output value. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1")]
        public byte PassOutputValue
        {
            get
            {
                return AppSettingGetter((byte)0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the fail output value. </summary>
        /// <value> The fail output value. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("2")]
        public byte FailOutputValue
        {
            get
            {
                return AppSettingGetter((byte)0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the outside range (overflow or short) output value. </summary>
        /// <value> The outside range output value. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("4")]
        public byte OutsideRangeOutputValue
        {
            get
            {
                return AppSettingGetter((byte)0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the meter arm source. </summary>
        /// <value> The meter arm source. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Bus")]
        public ArmSources MeterArmSource
        {
            get
            {
                return AppSettingEnum<ArmSources>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets the meter trigger source. </summary>
        /// <value> The meter trigger source. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("Bus")]
        public TriggerSources MeterTriggerSource
        {
            get
            {
                return AppSettingEnum<TriggerSources>();
            }

            set
            {
                AppSettingEnumSetter(value);
            }
        }

        /// <summary> Gets or sets a list of scan card scans. </summary>
        /// <value> A list of scan card scans. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("(@1:3)")]
        public string ScanCardScanList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the default scan card scan list. </summary>
        /// <value> The default scan card scan list. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("(@1:10)")]
        public string DefaultScanCardScanList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of scan card samples. </summary>
        /// <value> The number of scan card samples. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("3")]
        public int ScanCardSampleCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }


        #endregion

        #region " BUFFER STREAM TEST "

        /// <summary> Gets or sets a list of buffer stream trigger test scans. </summary>
        /// <value> A list of buffer stream trigger test scans. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("(@1!1:1!10)")]
        public string BufferStreamTriggerTestScanList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of buffer stream trigger test triggers. </summary>
        /// <value> The number of buffer stream trigger test triggers. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("10")]
        public int BufferStreamTriggerTestTriggerCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the buffer stream poll inteval. </summary>
        /// <value> The buffer stream poll inteval. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("00:00:00.020")]
        public TimeSpan BufferStreamPollInteval
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the duration of the binning strobe. </summary>
        /// <value> The binning strobe duration. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("00:00:00.012")]
        public TimeSpan BinningStrobeDuration
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of buffer stream completion cycles. </summary>
        /// <value> The number of buffer stream completion cycles. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("50")]
        public int BufferStreamCompletionCycleCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the size of the buffer stream test buffer. </summary>
        /// <value> The size of the buffer stream test buffer. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("1000000")]
        public int BufferStreamTestBufferSize
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the buffer stream start delay. </summary>
        /// <value> The buffer stream start delay. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("00:00:00.009")]
        public TimeSpan BufferStreamStartDelay
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }


        #endregion

        #region " AUTONOMOUS TRIGGER TEST "

        /// <summary> Gets or sets a list of autonomous trigger test scans. </summary>
        /// <value> A list of autonomous trigger test scans. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("(@1!1:1!10)")]
        public string AutonomousTriggerTestScanList
        {
            get
            {
                return AppSettingGetter(string.Empty);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the number of autonomous trigger test triggers. </summary>
        /// <value> The number of autonomous trigger test triggers. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("10")]
        public int AutonomousTriggerTestTriggerCount
        {
            get
            {
                return AppSettingGetter(0);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        /// <summary> Gets or sets the autonomous trigger test trigger delay. </summary>
        /// <value> The autonomous trigger test trigger delay. </value>
        [System.Configuration.UserScopedSetting()]
        [System.Configuration.DefaultSettingValue("00:00:00.5")]
        public TimeSpan AutonomousTriggerTestTriggerDelay
        {
            get
            {
                return AppSettingGetter(TimeSpan.Zero);
            }

            set
            {
                AppSettingSetter(value);
            }
        }

        #endregion

    }
}
