using System;
using System.Collections.Generic;
using System.Linq;

using isr.Core.SplitExtensions;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K7510.Device.MSTest
{

    /// <summary> K7000 Scanner Multimeter Scan Card unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k7510" )]
    public class StreamTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( testContext.FullyQualifiedTestClassName );
                _TestSite = new TestSite();
                _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                _TestSite.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                _TestSite.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            _TestSite?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( SubsystemsSettings.Get().Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            Assert.IsTrue( StreamSettings.Get().Exists, $"{typeof( StreamSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> The test site. </summary>
        private static TestSite _TestSite;

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo
        {
            get {
                if ( _TestSite is null )
                {
                    _TestSite = new TestSite();
                    _TestSite.AddTraceMessagesQueue( _TestSite.TraceMessagesQueueListener );
                    _TestSite.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                }

                return _TestSite;
            }
        }

        #endregion

        #region " BUS TRIGGER STREAM: INSTRUMENT CONFIGURATION "

        /// <summary> Asseret limit binning should configure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        internal static void AsseretLimitBinningShouldConfigure( K7510.K7510Device device )
        {
            string propertyName;
            string title = device.OpenResourceTitle;
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2LowerLevel )}";
            double expectedValue = StreamSettings.Get().ExpectedResistance * (1d - StreamSettings.Get().ResistanceTolerance);
            double actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit2LowerLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2UpperLevel )}";
            expectedValue = StreamSettings.Get().ExpectedResistance * (1d + StreamSettings.Get().ResistanceTolerance);
            actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit2UpperLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2AutoClear )}";
            bool expectedBoolean = true;
            bool actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit2AutoClear( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit2Enabled )}";
            expectedBoolean = true;
            actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit2Enabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1LowerLevel )}";
            expectedValue = StreamSettings.Get().ExpectedResistance * StreamSettings.Get().ResistanceTolerance;
            actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit1LowerLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1UpperLevel )}";
            expectedValue = StreamSettings.Get().OpenLimit;
            actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit1UpperLevel( expectedValue ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1AutoClear )}";
            expectedBoolean = true;
            actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit1AutoClear( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( BinningSubsystemBase )}.{nameof( BinningSubsystemBase.Limit1Enabled )}";
            expectedBoolean = true;
            actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit1Enabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            device.TriggerSubsystem.ApplyGradeBinning( StreamSettings.Get().BufferStreamTestBufferSize, StreamSettings.Get().BufferStreamStartDelay, StreamSettings.Get().FailOutputValue, StreamSettings.Get().PassOutputValue, StreamSettings.Get().OutsideRangeOutputValue, StreamSettings.Get().MeterTriggerSource );
        }

        /// <summary> Assert measurement should configure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        internal static void AssertMeasurementShouldConfigure( K7510.K7510Device device )
        {
            string propertyName;
            string title = device.OpenResourceTitle;
            propertyName = $"{typeof( SystemSubsystemBase )}.{nameof( SystemSubsystemBase.AutoZeroEnabled )}";
            bool expectedBoolean = StreamSettings.Get().AutoZeroEnabled;
            bool actualBoolean = device.SystemSubsystem.ApplyAutoZeroEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseSubsystemBase )}.{nameof( SenseSubsystemBase.FunctionMode )}";
            var expectedFunctionMode = StreamSettings.Get().SenseFunction;
            var actualFunctionMode = device.SenseSubsystem.ApplyFunctionMode( expectedFunctionMode ).GetValueOrDefault( SenseFunctionModes.None );
            Assert.AreEqual( expectedFunctionMode, actualFunctionMode, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.Aperture )}";
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.PowerLineCycles )}";
            double expectedPowerLineCycles = StreamSettings.Get().PowerLineCycles;
            double actualPowerLineCycles = device.SenseFunctionSubsystem.ApplyPowerLineCycles( expectedPowerLineCycles ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles, device.StatusSubsystemBase.LineFrequency.Value / TimeSpan.TicksPerSecond, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.AutoRangeEnabled )}";
            expectedBoolean = StreamSettings.Get().AutoRangeEnabled;
            actualBoolean = device.SenseFunctionSubsystem.ApplyAutoRangeEnabled( expectedBoolean ).GetValueOrDefault( !expectedBoolean );
            Assert.AreEqual( expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value" );
            propertyName = $"{typeof( SenseFunctionSubsystemBase )}.{nameof( SenseFunctionSubsystemBase.ResolutionDigits )}";
            double expectedResolution = StreamSettings.Get().ResolutionDigits;
            double actualResolution = device.SenseFunctionSubsystem.ApplyResolutionDigits( expectedResolution ).GetValueOrDefault( 0d );
            Assert.AreEqual( expectedResolution, actualResolution, $"[{title}].[{propertyName}] should equal expected value" );
#if false
                _ = device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled( true );
                _ = device.SenseResistanceFourWireSubsystem.ApplyAverageCount( 10 );
                _ = device.SenseResistanceFourWireSubsystem.ApplyAverageFilterType( AverageFilterTypes.Repeat );
                _ = device.SenseResistanceFourWireSubsystem.ApplyAveragePercentWindow( 100d * 0.01d );
#else
            _ = device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled( false );
#endif

            _ = device.SenseResistanceFourWireSubsystem.ApplyOpenLeadDetectorEnabled( true );
        }

        /// <summary> Assert trace should configure. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertTraceShouldConfigure( K7510.K7510Device device )
        {
            _ = nameof( TraceSubsystemBase.FeedSource ).SplitWords();
            _ = device.OpenResourceTitle;
            device.TraceSubsystem.ClearBuffer();

            // instrument reports: Reading buffer capacity must be between 10 and 8,496,306 readings
            _ = device.TraceSubsystem.ApplyPointsCount( StreamSettings.Get().BufferStreamTestBufferSize );

            // clear the buffer 
            device.TraceSubsystem.ClearBuffer();
            device.TraceSubsystem.BinningDuration = TimeSpan.FromMilliseconds( 100d );
        }

        /// <summary> Assert trigger plan should configure. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        /// <param name="device"> The device. </param>
        internal static void AssertTriggerPlanShouldConfigure( K7510.K7510Device device )
        {
            _ = device.OpenResourceTitle;
            device.ClearExecutionState();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            var triggerState = device.TriggerSubsystem.QueryTriggerState().GetValueOrDefault( TriggerState.None );
            if ( TriggerState.Running == triggerState )
            {
                device.TriggerSubsystem.Abort();
            }

            device.TriggerSubsystem.ClearTriggerModel();
            device.Session.EnableServiceRequestWaitComplete();
            device.Session.Wait();
            _ = device.Session.ReadStatusRegister();
            _ = device.TriggerSubsystem.ApplyAutoDelayEnabled( false );
            _ = device.TriggerSubsystem.QueryTriggerState();
        }

        /// <summary> Assert buffer readings should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The 2002. </param>
        private static void AssertBufferReadingsShouldMatch( K7510.K7510Device device )
        {
            string title = device.OpenResourceTitle;
            string propertyName;
            IEnumerable<BufferReading> bufferReadings = device.TraceSubsystem.DequeueRange( device.TraceSubsystem.NewReadingsCount );
            double expectedResistance = StreamSettings.Get().ExpectedResistance;
            double epsilon = expectedResistance * StreamSettings.Get().ResistanceTolerance;

            // trace (display) the buffer readings amounts
            int readingNumber = 0;
            foreach ( BufferReading bufferReading in bufferReadings )
            {
                readingNumber += 1;
                TestInfo.TraceMessage( $"Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4} {bufferReading.FractionalTimespan.TotalMilliseconds:0}ms" );
            }

            double measuredResistance;
            propertyName = nameof( measuredResistance ).SplitWords();
            if ( device.SenseSubsystem.FunctionMode.Value == StreamSettings.Get().SenseFunction )
            {
                readingNumber = 0;
                foreach ( BufferReading bufferReading in bufferReadings )
                {
                    readingNumber += 1;
                    Assert.IsNotNull( bufferReading.Amount, $"[{title}].[Reading #{readingNumber}] should not be null" );
                    measuredResistance = bufferReading.Amount.Value;
                    Assert.AreEqual( expectedResistance, measuredResistance, epsilon, $"[{title}].[{propertyName}]={measuredResistance} but should equals {expectedResistance} within {epsilon}" );
                }
            }

            // validate binding list count
            Assert.AreEqual( device.TraceSubsystem.BufferReadingsCount, bufferReadings.Count(), "Buffer readings count should match the binding list count" );
        }

        #endregion

        #region " BUS TRIGGERS STREAM "

        /// <summary> Handles the property change. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       The sender. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private static void HandlePropertyChange( TraceSubsystemBase sender, string propertyName )
        {
            if ( sender is object && !string.IsNullOrWhiteSpace( propertyName ) )
            {
                switch ( propertyName ?? "" )
                {
                    case nameof( TraceSubsystemBase.BufferReadingsCount ):
                        {
                            if ( sender.BufferReadingsCount > 0 )
                            {
                                TestInfo.TraceMessage( $"Streaming reading #{sender.BufferReadingsCount}: {sender.LastBufferReading.Amount} 0x{sender.LastBufferReading.StatusWord:X4}" );
                            }

                            break;
                        }
                }
            }
        }

        /// <summary> Handles the trace subsystem property change. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The sender. </param>
        /// <param name="e">      Property changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static void HandleTraceSubsystemPropertyChange( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            try
            {
                if ( sender is object && e is object )
                {
                    HandlePropertyChange( sender as TraceSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                Assert.Fail( ex.ToString() );
            }
        }

        /// <summary> Restore Trigger State. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void RestoreState( K7510.K7510Device device )
        {
            device.TriggerSubsystem.Abort();
            _ = device.Session.QueryOperationCompleted();
            device.ResetKnownState();
            _ = device.Session.QueryOperationCompleted();
            device.ClearExecutionState();
            _ = device.Session.QueryOperationCompleted();
            _ = device.TriggerSubsystem.ApplyContinuousEnabled( false );
            device.Session.EnableServiceRequestWaitComplete();
        }

        /// <summary> Assert buffer streaming should stop. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertBufferStreamingShouldStop( K7510.K7510Device device )
        {
            var timeout = TraceSubsystemBase.EstimateStreamStopTimeoutInterval( device.TraceSubsystem.StreamCycleDuration, StreamSettings.Get().BufferStreamPollInteval, 1.5d );
            var (success, details) = device.TraceSubsystem.StopBufferStream( timeout );
            if ( success )
            {
                device.TriggerSubsystem.Abort();
                _ = device.Session.ReadStatusRegister();
                TestInfo.TraceMessage( $"Streaming ended with {details}" );
            }
            else
            {
                Assert.Fail( $"buffer streaming failed; {details}" );
            }

            device.TraceSubsystem.PropertyChanged -= HandleTraceSubsystemPropertyChange;
        }

        /// <summary> Assert buffer streaming should start. </summary>
        /// <remarks> David, 2020-04-11. </remarks>
        /// <param name="device"> The device. </param>
        private static void AssertBufferStreamingShouldStart( K7510.K7510Device device )
        {
            device.TraceSubsystem.PropertyChanged += HandleTraceSubsystemPropertyChange;
            Assert.IsTrue( device.Session.QueryOperationCompleted().GetValueOrDefault( false ), $"Operation should be completed before starting streaming" );
            device.TriggerSubsystem.Initiate();
            Core.ApplianceBase.DoEvents();
            device.TraceSubsystem.StartBufferStream( device.TriggerSubsystem, StreamSettings.Get().BufferStreamPollInteval, device.SenseFunctionSubsystem.FunctionUnit );
        }

        #endregion

        #region " BUS TRIGGERS STREAM READING "

        /// <summary> Assert meter bus triggers stream readings should pass. </summary>
        /// <remarks>
        /// <list type="bullet">
        /// testhost.x86-2020-07-29.log <item>
        ///                         Amount   Status </item><item>
        /// Streaming reading #1: 100.0845 Ω 0x0008 </item><item>
        /// Streaming reading #2: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #3: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #4: 100.0845 Ω 0x0008 </item><item>
        /// Streaming reading #5: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #6: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #7: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #8: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #9: 100.0846 Ω 0x0008 </item><item>
        /// Streaming reading #10: 100.0847 Ω 0x0008 </item><item>
        /// Streaming ended with Done </item><item>
        /// Streaming ended with Done </item><item>
        /// Reading #1=100.0845 Ω 0x0008 449ms </item><item>
        /// Reading #2=100.0846 Ω 0x0008 919ms </item><item>
        /// Reading #3=100.0846 Ω 0x0008 428ms </item><item>
        /// Reading #4=100.0845 Ω 0x0008 922ms </item><item>
        /// Reading #5=100.0846 Ω 0x0008 414ms </item><item>
        /// Reading #6=100.0846 Ω 0x0008 922ms </item><item>
        /// Reading #7=100.0846 Ω 0x0008 431ms </item><item>
        /// Reading #8=100.0846 Ω 0x0008 938ms </item><item>
        /// Reading #9=100.0846 Ω 0x0008 425ms </item><item>
        /// Reading #10=100.0847 Ω 0x0008 932ms </item></list>
        /// </remarks>
        /// <param name="k7510">        The Keithley 2002 device. </param>
        /// <param name="triggerCount"> Number of triggers. </param>
        /// <param name="delay">        The delay. </param>
        internal static void AssertMeterBusTriggersStreamReadingsShouldPass( K7510.K7510Device k7510, int triggerCount, TimeSpan delay )
        {
            _ = k7510.OpenResourceTitle;

            // TO_DO: Check Trigger State instead of operation bit masks.
            AssertBufferStreamingShouldStart( k7510 );
            TestInfo.TraceMessage( "                        Amount   Status" );
            try
            {
                try
                {
                    for ( int i = 1, loopTo = triggerCount; i <= loopTo; i++ )
                    {
                        // this delay is used to allow the operator to see the values as they come in.
                        Core.ApplianceBase.DoEventsWait( delay );
                        k7510.TraceSubsystem.BusTriggerRequested = true;
                        // K7510.Session.AssertTrigger()
                        Core.ApplianceBase.DoEventsWait( delay );
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                AssertBufferStreamingShouldStop( k7510 );
            }
        }

        /// <summary> Assert meter bus triggers stream readings should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void AssertMeterBusTriggersStreamReadingsShouldPass()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var K7510Device = K7510.K7510Device.Create();
            K7510Device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, K7510Device, ResourceSettings.Get() );
                Assert.AreEqual( RouteTerminalsModes.Front, K7510Device.RouteSubsystem.QueryTerminalsMode().GetValueOrDefault( RouteTerminalsModes.None ), $"Front terminal must be selected" );
                AssertTriggerPlanShouldConfigure( K7510Device );
                AssertMeasurementShouldConfigure( K7510Device );
                AsseretLimitBinningShouldConfigure( K7510Device );
                AssertTraceShouldConfigure( K7510Device );
                if ( TriggerSources.Bus == StreamSettings.Get().MeterTriggerSource )
                {
                    AssertMeterBusTriggersStreamReadingsShouldPass( K7510Device, StreamSettings.Get().BufferStreamTriggerTestTriggerCount, StreamSettings.Get().BusTriggerTestTriggerDelay );
                }

                AssertBufferReadingsShouldMatch( K7510Device );
            }
            catch
            {
                throw;
            }
            finally
            {
                RestoreState( K7510Device );
                DeviceManager.CloseSession( TestInfo, K7510Device );
            }
        }

        /// <summary> (Unit Test Method) meter bus triggers stream readings should pass. </summary>
        /// <remarks> David, 2020-04-09. </remarks>
        [TestMethod()]
        public void MeterBusTriggersStreamReadingsShouldPass()
        {
            StreamSettings.Get().MeterTriggerSource = TriggerSources.Bus;
            AssertMeterBusTriggersStreamReadingsShouldPass();
        }

        #endregion

    }
}
