using System;
using System.Threading;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K7510.Device.MSTest
{

    /// <summary> K7510 Subsystems unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k7510" )]
    public class SubsystemsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.Write( testContext.FullyQualifiedTestClassName );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( SubsystemsSettings.Get().Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " STATUS SUSBSYSTEM "

        /// <summary> Opens session check status. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
        /// <param name="resourceInfo">     Information describing the resource. </param>
        /// <param name="subsystemsInfo">   Information describing the subsystems. </param>
        private static void AssertSessionOpenCheckStatusShouldPass( bool readErrorEnabled, ResourceSettings resourceInfo, SubsystemsSettings subsystemsInfo )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K7510.K7510Device.Create();
            try
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                VI.DeviceTests.DeviceManager.AssertSessionInitialValuesShouldMatch( device.Session, resourceInfo, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertDeviceModelShouldMatch( device.StatusSubsystemBase, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldMatch( device.StatusSubsystemBase, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertTerminationValuesShouldMatch( device.Session, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertLineFrequencyShouldMatch( device.StatusSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertIntegrationPeriodShouldMatch( device.StatusSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.MeasureSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.SenseSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSessionDeviceErrorsShouldClear( device, subsystemsInfo );
                if ( readErrorEnabled )
                    VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldRead( device, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertOrphanMessagesShouldBeEmpty( device.StatusSubsystemBase );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionOpenCheckStatusShouldPass()
        {
            AssertSessionOpenCheckStatusShouldPass( false, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        /// <summary>
        /// (Unit Test Method) session open check status device errors should pass.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionOpenCheckStatusDeviceErrorsShouldPass()
        {
            AssertSessionOpenCheckStatusShouldPass( true, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        #endregion

        #region " SENSE SUBSYSTEM TESTS "

        /// <summary> (Unit Test Method) sense subsystem initial values should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SenseSubsystemInitialValuesShouldMatch()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch( device.SenseSubsystem, SubsystemsSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) sense subsystem function mode should toggle. </summary>
        /// <remarks> David, 2020-07-29. </remarks>
        [TestMethod()]
        public void SenseSubsystemFunctionModeShouldToggle()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertFunctionModeShouldToggle( device.SenseSubsystem, SenseFunctionModes.ResistanceFourWire, SenseFunctionModes.Resistance );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Assert display functions should pass. </summary>
        /// <remarks> David, 2020-07-30. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertDisplayFunctionsShouldPass( DisplaySubsystemBase subsystem )
        {
            var display = subsystem.QueryDisplayScreens().GetValueOrDefault( DisplayScreens.Home );
            _ = subsystem.ApplyDisplayScreen( DisplayScreens.UserSwipe );
            subsystem.ClearDisplay();
            var endTime = DateTime.UtcNow.AddSeconds( 3d );
            while ( endTime >= DateTime.UtcNow )
            {
                subsystem.DisplayLine( 1, endTime.Subtract( DateTime.UtcNow ).ToString() );
                Thread.Sleep( 100 );
            }

            subsystem.ClearDisplay();
            _ = subsystem.ApplyDisplayScreen( display );
        }

        /// <summary> (Unit Test Method) displays the functions should pass. </summary>
        /// <remarks> David, 2020-07-30. </remarks>
        [TestMethod()]
        public void DisplayFunctionsShouldPass()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertDisplayFunctionsShouldPass( device.DisplaySubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
