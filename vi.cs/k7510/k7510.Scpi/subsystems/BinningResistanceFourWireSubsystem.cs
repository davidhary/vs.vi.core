namespace isr.VI.K7510
{

    /// <summary>
    /// Defines a Four Wire Resistance Binning Subsystem for a Keithley 7510 Meter.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class BinningResistanceFourWireSubsystem : BinningSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="BinningResistanceFourWireSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public BinningResistanceFourWireSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " LIMIT 1 "

        #region " LIMIT1 AUTO CLEAR "

        /// <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
        /// <value> The Limit1 AutoClear query command. </value>
        protected override string Limit1AutoClearCommandFormat { get; set; } = ":CALC2:FRES:LIM1:CLE:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
        /// <value> The Limit1 AutoClear query command. </value>
        protected override string Limit1AutoClearQueryCommand { get; set; } = ":CALC2:FRES:LIM1:CLE:AUTO?";

        #endregion

        #region " LIMIT1 ENABLED "

        /// <summary> Gets or sets the Limit1 enabled command Format. </summary>
        /// <value> The Limit1 enabled query command. </value>
        protected override string Limit1EnabledCommandFormat { get; set; } = ":CALC2:FRES:LIM1:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit1 enabled query command. </summary>
        /// <value> The Limit1 enabled query command. </value>
        protected override string Limit1EnabledQueryCommand { get; set; } = ":CALC2:FRES:LIM1:STAT?";

        #endregion

        #region " LIMIT1 LOWER LEVEL "

        /// <summary> Gets or sets the Limit1 Lower Level command format. </summary>
        /// <value> The Limit1LowerLevel command format. </value>
        protected override string Limit1LowerLevelCommandFormat { get; set; } = ":CALC2:FRES:LIM1:LOW {0}";

        /// <summary> Gets or sets the Limit1 Lower Level query command. </summary>
        /// <value> The Limit1LowerLevel query command. </value>
        protected override string Limit1LowerLevelQueryCommand { get; set; } = ":CALC2:FRES:LIM1:LOW?";

        #endregion

        #region " LIMIT1 UPPER LEVEL "

        /// <summary> Gets or sets the Limit1 Upper Level command format. </summary>
        /// <value> The Limit1UpperLevel command format. </value>
        protected override string Limit1UpperLevelCommandFormat { get; set; } = ":CALC2:FRES:LIM1:UPP {0}";

        /// <summary> Gets or sets the Limit1 Upper Level query command. </summary>
        /// <value> The Limit1UpperLevel query command. </value>
        protected override string Limit1UpperLevelQueryCommand { get; set; } = ":CALC2:FRES:LIM1:UPP?";

        #endregion

        #endregion

        #region " LIMIT 2 "

        #region " LIMIT2 AUTO CLEAR "

        /// <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
        /// <value> The Limit2 AutoClear query command. </value>
        protected override string Limit2AutoClearCommandFormat { get; set; } = ":CALC2:FRES:LIM2:CLE:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
        /// <value> The Limit2 AutoClear query command. </value>
        protected override string Limit2AutoClearQueryCommand { get; set; } = ":CALC2:FRES:LIM2:CLE:AUTO?";

        #endregion

        #region " LIMIT2 ENABLED "

        /// <summary> Gets or sets the Limit2 enabled command Format. </summary>
        /// <value> The Limit2 enabled query command. </value>
        protected override string Limit2EnabledCommandFormat { get; set; } = ":CALC2:FRES:LIM2:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Limit2 enabled query command. </summary>
        /// <value> The Limit2 enabled query command. </value>
        protected override string Limit2EnabledQueryCommand { get; set; } = ":CALC2:FRES:LIM2:STAT?";

        #endregion

        #region " LIMIT2 LOWER LEVEL "

        /// <summary> Gets or sets the Limit2 Lower Level command format. </summary>
        /// <value> The Limit2LowerLevel command format. </value>
        protected override string Limit2LowerLevelCommandFormat { get; set; } = ":CALC2:FRES:LIM2:LOW {0}";

        /// <summary> Gets or sets the Limit2 Lower Level query command. </summary>
        /// <value> The Limit2LowerLevel query command. </value>
        protected override string Limit2LowerLevelQueryCommand { get; set; } = ":CALC2:FRES:LIM2:LOW?";

        #endregion

        #region " LIMIT2 UPPER LEVEL "

        /// <summary> Gets or sets the Limit2 Upper Level command format. </summary>
        /// <value> The Limit2UpperLevel command format. </value>
        protected override string Limit2UpperLevelCommandFormat { get; set; } = ":CALC2:FRES:LIM2:UPP {0}";

        /// <summary> Gets or sets the Limit2 Upper Level query command. </summary>
        /// <value> The Limit2UpperLevel query command. </value>
        protected override string Limit2UpperLevelQueryCommand { get; set; } = ":CALC2:FRES:LIM2:UPP?";

        #endregion
        #endregion

    }
}
