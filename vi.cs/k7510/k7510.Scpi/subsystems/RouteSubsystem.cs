namespace isr.VI.K7510
{

    /// <summary> Defines a Route Subsystem for a Keithley 7510 Meter. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class RouteSubsystem : RouteSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public RouteSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.TerminalsMode = RouteTerminalsModes.Front;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " TERMINAL MODE "

        /// <summary> Gets or sets the terminals mode query command. </summary>
        /// <value> The terminals mode command. </value>
        protected override string TerminalsModeQueryCommand { get; set; } = ":ROUT:TERM?";

        /// <summary> Gets or sets the terminals mode command format. </summary>
        /// <value> The terminals mode command format. </value>
        protected override string TerminalsModeCommandFormat { get; set; } = string.Empty; // read only; ":ROUT:TERM {0}"

        #endregion

        #endregion

    }
}
