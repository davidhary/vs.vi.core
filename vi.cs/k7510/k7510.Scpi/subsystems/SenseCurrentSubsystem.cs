namespace isr.VI.K7510
{

    /// <summary> Defines a SCPI Sense Current Subsystem for a Keithley 7510 Meter. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2014-03-01, 3.0.5173. </para>
    /// </remarks>
    public class SenseCurrentSubsystem : SenseFunctionSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseCurrentSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ampere );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere;
            this.DefineFunctionModeReadWrites( "{0}", "'{0}'" );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.PowerLineCyclesRange = this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d ) == 60d ? new Core.Primitives.RangeR( 0.0005d, 15d ) : new Core.Primitives.RangeR( 0.0005d, 12d );
            this.FunctionRange = new Core.Primitives.RangeR( 0.001d, 10d );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            base.DefineKnownResetState();
            this.PowerLineCyclesRange = this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d ) == 60d ? new Core.Primitives.RangeR( 0.0005d, 15d ) : new Core.Primitives.RangeR( 0.0005d, 12d );
            this.FunctionRange = new Core.Primitives.RangeR( 0.001d, 10d );
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AUTO ZERO ENABLED "

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = ":SENS:CURR:AZER {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = ":SENS:CURR:AZER?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:CURR:RANG:AUTO?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:CURR:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:CURR:NPLC?";

        #endregion

        #region " PROTECTION LEVEL "

        /// <summary> Gets or sets the protection level command format. </summary>
        /// <value> the protection level command format. </value>
        protected override string ProtectionLevelCommandFormat { get; set; } = ":SENS:CURR:PROT {0}";

        /// <summary> Gets or sets the protection level query command. </summary>
        /// <value> the protection level query command. </value>
        protected override string ProtectionLevelQueryCommand { get; set; } = ":SENS:CURR:PROT?";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SENS:CURR:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SENS:CURR:RANG?";

        #endregion

        #endregion

        #region " READING AMOUNTS "

        /// <summary> Clears the function state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineFunctionClearKnownState()
        {
            base.DefineFunctionClearKnownState();
            if ( this.Readings is object )
            {
                this.Readings.PrimaryReading.ApplyUnit( this.FunctionUnit );
                _ = this.Readings.TryParse( string.Empty );
                this.NotifyPropertyChanged( nameof( this.Readings ) );
            }
        }

        /// <summary> The readings. </summary>
        private Readings _Readings;

        /// <summary> Gets or sets the readings. </summary>
        /// <value> The readings. </value>
        public Readings Readings
        {
            get => this._Readings;

            private set {
                this._Readings = value;
                this.AssignReadingAmounts( value );
            }
        }

        #endregion

    }
}
