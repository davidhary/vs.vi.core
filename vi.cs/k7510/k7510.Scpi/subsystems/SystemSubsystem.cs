using System.Diagnostics;

namespace isr.VI.K7510
{

    /// <summary> Defines a System Subsystem for a Keithley 7510 Meter. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            // reduce noise when using IDE.
            if ( Debugger.IsAttached )
                _ = this.ApplyFanLevel( VI.FanLevel.Quiet );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.FanLevel = VI.FanLevel.Normal;
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the initialize memory command. </summary>
        /// <value> The initialize memory command. </value>
        protected override string InitializeMemoryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the preset command. </summary>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the language revision query command. </summary>
        /// <value> The language revision query command. </value>
        protected override string LanguageRevisionQueryCommand { get; set; } = Pith.Scpi.Syntax.LanguageRevisionQueryCommand;

        #endregion

        #region " BEEPER ENABLED + IMMEDIATE "

        /// <summary> Gets or sets the Beeper enabled query command. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledQueryCommand { get; set; } = ":SYST:BEEP:STAT?";

        /// <summary> Gets or sets the Beeper enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledCommandFormat { get; set; } = ":SYST:BEEP:STAT {0:'1';'1';'0'}";

        /// <summary> Gets or sets the beeper immediate command format. </summary>
        /// <value> The beeper immediate command format. </value>
        protected override string BeeperImmediateCommandFormat { get; set; } = ":SYST:BEEP:IMM {0}, {1}";

        #endregion

        #region " FAN LEVEL "

        /// <summary> Gets or sets the Fan Level query command. </summary>
        /// <value> The Fan Level command. </value>
        protected override string FanLevelQueryCommand { get; set; } = string.Empty;

        /// <summary> Converts the specified value to string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="P:isr.VI.SystemSubsystemBase.FanLevel">Fan Level</see>. </param>
        /// <returns> A String. </returns>
        protected override string FromFanLevel( FanLevel value )
        {
            return value == VI.FanLevel.Normal ? "NORM" : "QUIET";
        }

        /// <summary> Gets or sets the Fan Level command format. </summary>
        /// <value> The Fan Level command format. </value>
        protected override string FanLevelCommandFormat { get; set; } = ":SYST:FAN:LEV {0}";

        #endregion

    }
}
