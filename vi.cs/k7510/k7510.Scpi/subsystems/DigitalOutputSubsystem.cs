﻿using isr.Core.EnumExtensions;

namespace isr.VI.K7510
{

    /// <summary>
    /// Defines the contract that must be implemented by a SCPI Digital Output Subsystem.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class DigitalOutputSubsystem : DigitalOutputSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="DigitalOutputSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public DigitalOutputSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
            this.DigitalActiveLevelReadWrites.Clear();
            this.DigitalActiveLevelReadWrites.Add( ( long ) DigitalActiveLevels.High, "AHIG", "AHIG", DigitalActiveLevels.High.DescriptionUntil() );
            this.DigitalActiveLevelReadWrites.Add( ( long ) DigitalActiveLevels.Low, "ALOW", "ALOW", DigitalActiveLevels.Low.DescriptionUntil() );
        }

        #endregion

        #region " OUTPUT SUBSYSTEM "

        // TO_DO:
        // Protected Overrides Property OutputSignalPolarityQueryCommand As String = "OUTP:TTL{0}:LSEN?"
        // 
        // Protected Overrides Property OutputSignalPolarityCommandFormat As String = "OUTP:TTL{0}:LSEN {{0}}"
        // 
        // Protected Overrides Property OutputSignalPolarityQueryCommand As String = "SOUR:TTL4:BST?"
        // 
        // Protected Overrides Property OutputSignalPolarityCommandFormat As String = "SOUR:TTL4:BST {0}"
        // 

        #endregion

        #region " SOURCE SUBSYSTEM "

        // TO_DO:
        // Protected Overrides Property LevelQueryCommand As String = ":SOUR:TTL:LEV?"
        // 
        // Protected Overrides Property LevelCommandFormat As String = ":SOUR:TTL:LEV {0}"
        // 
        // Protected Overrides Property LineLevelQueryCommand As String = ":SOUR:TTL{0}:LEV?"
        // 
        // Protected Overrides Property LineLevelCommandFormat As String = ":SOUR:TTL{0}:LEV {{0}}"
        // 

        #endregion

    }
}