using System;

namespace isr.VI.K7510
{

    /// <summary> Defines a Trigger Subsystem for a Keithley 7510 Meter. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TriggerSubsystem : TriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TriggerSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.SupportedTriggerSources = TriggerSources.Bus | TriggerSources.External | TriggerSources.Hold | TriggerSources.Immediate | TriggerSources.Manual | TriggerSources.Timer | TriggerSources.TriggerLink;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " ABORT / INIT COMMANDS "

        /// <summary> Gets or sets the Abort command. </summary>
        /// <value> The Abort command. </value>
        protected override string AbortCommand { get; set; } = ":ABOR";

        /// <summary> Gets or sets the initiate command. </summary>
        /// <value> The initiate command. </value>
        protected override string InitiateCommand { get; set; } = ":INIT";

        /// <summary> Gets or sets the clear command. </summary>
        /// <value> The clear command. </value>
        protected override string ClearCommand { get; set; } = ":TRIG:EXT:IN:CLE";

        /// <summary> Gets or sets the clear  trigger model command. </summary>
        /// <remarks> SCPI: ":TRIG:LOAD 'EMPTY'". </remarks>
        /// <value> The clear command. </value>
        protected override string ClearTriggerModelCommand { get; set; } = ":TRIG:LOAD 'EMPTY'";

        #endregion

        #region " TRIGGER STATE "

        /// <summary> Gets or sets the Trigger State query command. </summary>
        /// <remarks>
        ///    <c>SCPI: :TRIG:STAT? <para>
        /// TSP2: trigger.model.state()
        /// </para></c>
        /// </remarks>
        /// <value> The Trigger State query command. </value>
        protected override string TriggerStateQueryCommand { get; set; } = ":TRIG:STAT?";

        #endregion

        #region " TRIGGER COUNT "

        /// <summary> Gets or sets trigger count query command. </summary>
        /// <value> The trigger count query command. </value>
        protected override string TriggerCountQueryCommand { get; set; } = string.Empty; // ":TRIG:COUN?" 

        /// <summary> Gets or sets trigger count command format. </summary>
        /// <value> The trigger count command format. </value>
        protected override string TriggerCountCommandFormat { get; set; } = string.Empty; // ":TRIG:COUN {0}"

        #endregion

        #region " DELAY "

        /// <summary> Gets or sets the delay command format. </summary>
        /// <value> The delay command format. </value>
        protected override string DelayCommandFormat { get; set; } = string.Empty; // ":TRIG:DEL {0:s\.FFFFFFF}"

        /// <summary> Gets or sets the Delay format for converting the query to time span. </summary>
        /// <value> The Delay query command. </value>
        protected override string DelayFormat { get; set; } = @"s\.FFFFFFF";

        /// <summary> Gets or sets the delay query command. </summary>
        /// <value> The delay query command. </value>
        protected override string DelayQueryCommand { get; set; } = string.Empty; // ":TRIG:DEL?"

        #endregion

        #region " SOURCE "

        /// <summary> Gets or sets the Trigger Source query command. </summary>
        /// <value> The Trigger Source query command. </value>
        protected override string TriggerSourceQueryCommand { get; set; } = string.Empty; // ":TRIG:SOUR?"

        /// <summary> Gets or sets the Trigger Source command format. </summary>
        /// <value> The Trigger Source command format. </value>
        protected override string TriggerSourceCommandFormat { get; set; } = string.Empty; // ":TRIG:SOUR {0}"

        #endregion

        #endregion

        #region " SIMPLE LOOP "

        /// <summary> The simple loop model. </summary>
        private const string _SimpleLoopModel = "SimpleLoop";

        /// <summary> Loads simple loop. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count"> Number of. </param>
        /// <param name="delay"> The delay. </param>
        public void LoadSimpleLoop( int count, TimeSpan delay )
        {
            _ = this.WriteLine( ":TRIG:LOAD '{0}',{1},{2}", _SimpleLoopModel, count, delay.TotalSeconds );
        }

        #endregion

        #region " GRAD BINNING "

        /// <summary> The grade binning model. </summary>
        private const string _GradeBinningModel = "GradeBinning";

        /// <summary> Loads grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="triggerOption">    The trigger option (use 7 for external line). </param>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="endDelay">         The end delay. </param>
        /// <param name="highLimit">        The high limit. </param>
        /// <param name="lowLimit">         The low limit. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void LoadGradeBinning( int count, int triggerOption, TimeSpan startDelay, TimeSpan endDelay, double highLimit, double lowLimit, int failedBitPattern, int passBitPattern )
        {
            _ = this.WriteLine( ":TRIG:LOAD '{0}',{1},{2},{3},{4},{5},{6},{7},{8}", _GradeBinningModel, count, triggerOption, startDelay.TotalSeconds, endDelay.TotalSeconds, highLimit, lowLimit, failedBitPattern, passBitPattern );
        }

        /// <summary> Loads grade binning. </summary>
        /// <remarks> Uses external trigger (start line = 7). </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="endDelay">         The end delay. </param>
        /// <param name="highLimit">        The high limit. </param>
        /// <param name="lowLimit">         The low limit. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void LoadGradeBinning( int count, TimeSpan startDelay, TimeSpan endDelay, double highLimit, double lowLimit, int failedBitPattern, int passBitPattern )
        {
            this.LoadGradeBinning( count, 7, startDelay, endDelay, highLimit, lowLimit, failedBitPattern, passBitPattern );
        }

        /// <summary> Loads grade binning. </summary>
        /// <remarks> Uses external trigger (start line = 7) and maximum count (268000000). </remarks>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="endDelay">         The end delay. </param>
        /// <param name="highLimit">        The high limit. </param>
        /// <param name="lowLimit">         The low limit. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void LoadGradeBinning( TimeSpan startDelay, TimeSpan endDelay, double highLimit, double lowLimit, int failedBitPattern, int passBitPattern )
        {
            this.LoadGradeBinning( 268000000, 7, startDelay, endDelay, highLimit, lowLimit, failedBitPattern, passBitPattern );
        }

        #endregion

        #region " CUSTOM BINNING "

        /// <summary> Applies the grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void ApplyGradeBinning( int count, TimeSpan startDelay, int failedBitPattern, int passBitPattern )
        {
            int block = 0;

            // use the mask to assign digital outputs.
            int mask = failedBitPattern | passBitPattern;
            int bitPattern = 1;
            string cmd;
            for ( int i = 1; i <= 6; i++ )
            {
                if ( (mask & bitPattern) != 0 )
                {
                    cmd = $"DIG:LINE{i}:MODE DIG,OUT";
                    _ = this.WriteLine( cmd );
                }

                bitPattern <<= 1;
            }

            // clear the trigger model
            cmd = ":TRIG:LOAD 'EMPTY'";
            _ = this.WriteLine( cmd );

            // clear any pending trigger
            cmd = ":TRIG:EXT:IN:CLE";
            _ = this.WriteLine( cmd );

            // clear the default buffer
            cmd = ":TRAC:CLE";
            _ = this.WriteLine( cmd );
            bool autonomous = count == 1;

            // Block 1: 
            // -- if autonomous mode, clear the buffer
            // -- if program control, clear the buffer
            block += 1;
            cmd = autonomous ? $":TRIG:BLOC:BUFF:CLE {block}" : $":TRIG:BLOC:NOP {block}";
            _ = this.WriteLine( cmd );

            // Block 2: Wait for external trigger; this is the repeat block.
            int repeatBlock = block + 1;
            block += 1;
            cmd = $":TRIG:BLOC:WAIT {block}, EXT";
            _ = this.WriteLine( cmd );

            // Block 3: Pre-Measure Delay
            block += 1;
            cmd = $":TRIG:BLOC:DEL:CONS {block}, {0.001d * startDelay.TotalMilliseconds}";
            _ = this.WriteLine( cmd );

            // Block 4: Measure and save to the default buffer.
            block += 1;
            cmd = $":TRIG:BLOC:MEAS {block}";
            _ = this.WriteLine( cmd );

            // Block 5: Limit test and branch; the pass block is 3 blocks ahead (8)
            int passBlock = block + 4;
            block += 1;
            cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,1,{passBlock}";
            _ = this.WriteLine( cmd );

            // Block 6: Output failure bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{failedBitPattern},{mask}";
            _ = this.WriteLine( cmd );

            // Block 7: Skip the pass binning block
            block += 1;
            cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {block + 2}";
            _ = this.WriteLine( cmd );

            // Block 8: Output pass bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{passBitPattern},{mask}";
            _ = this.WriteLine( cmd );
            int notificationId = 1;

            // Block 9: Notify measurement completed
            block += 1;
            cmd = $":TRIG:BLOC:NOT {block},{notificationId}";
            _ = this.WriteLine( cmd );

            // set external output setting
            cmd = $":TRIG:EXT:OUT:STIM NOT{notificationId}";
            _ = this.WriteLine( cmd );

            // Block 10: Repeat Count times.
            block += 1;
            cmd = count <= 0 ? $":TRIG:BLOCk:BRAN:ALW {block},{repeatBlock}" : $":TRIG:BLOC:BRAN:COUN {block},{count},{repeatBlock}";
            _ = this.WriteLine( cmd );
        }

        /// <summary> Applies the grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">                 Number of. </param>
        /// <param name="startDelay">            The start delay. </param>
        /// <param name="failedBitPattern">      A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">        A pattern specifying the pass bit. </param>
        /// <param name="openContactBitPattern"> A pattern specifying the open contact bit. </param>
        /// <param name="triggerSource">         The trigger source. </param>
        public void ApplyGradeBinning( int count, TimeSpan startDelay, int failedBitPattern, int passBitPattern, int openContactBitPattern, TriggerSources triggerSource )
        {
            int block = 0;

            // use the mask to assign digital outputs.
            int mask = failedBitPattern | passBitPattern | openContactBitPattern;
            int bitPattern = 1;
            string cmd;
            for ( int i = 1; i <= 6; i++ )
            {
                if ( (mask & bitPattern) != 0 )
                {
                    cmd = $"DIG:LINE{i}:MODE DIG,OUT";
                    _ = this.WriteLine( cmd );
                }

                bitPattern <<= 1;
            }

            // clear the trigger model
            this.ClearTriggerModel();

            // clear any pending trigger
            this.ClearTriggers();
            bool autonomous = count == 1;

            // Block 1: 
            // -- if autonomous mode, clear the buffer
            // -- if program control, clear the buffer
            block += 1;
            cmd = autonomous ? $":TRIG:BLOC:BUFF:CLE {block}" : $":TRIG:BLOC:NOP {block}";
            _ = this.WriteLine( cmd );

            // Block 2: Wait for external trigger; this is the repeat block.
            int repeatBlock = block + 1;
            block += 1;
            cmd = $":TRIG:BLOC:WAIT {block}, EXT";
            if ( 0 != (triggerSource & TriggerSources.Bus) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, COMM";
            }
            else if ( 0 != (triggerSource & TriggerSources.Manual) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, DISP";
            }
            else if ( 0 != (triggerSource & TriggerSources.TriggerLink) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, TSPL(1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Timer) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, TIM(1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Digital) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, DIG(1)";
            }

            _ = this.WriteLine( cmd );

            // Block 3: Pre-Measure Delay
            block += 1;
            cmd = $":TRIG:BLOC:DEL:CONS {block}, {0.001d * startDelay.TotalMilliseconds}";
            _ = this.WriteLine( cmd );

            // Block 4: Measure and save to the default buffer.
            block += 1;
            cmd = $":TRIG:BLOC:MEAS {block}";
            _ = this.WriteLine( cmd );

            // Block 5: Limit 2 (open contact) test and branch if INside to the limit block; the limit 1 block is 3 blocks ahead (8)
            int limitBlock = block + 4;
            block += 1;
            cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,2,{limitBlock}";
            _ = this.WriteLine( cmd );

            // Block 6: Output open bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{openContactBitPattern},{mask}";
            _ = this.WriteLine( cmd );

            // Block 7: Jump to the notification block number
            int notificationBlock = block + 6; // = 12
            block += 1;
            cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {notificationBlock}";
            _ = this.WriteLine( cmd );

            // Block 8: Limit 1 test and branch if INside to the pass block; the pass block is 3 blocks ahead
            int passBlock = block + 4; // = 11
            block += 1;
            cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,1,{passBlock}";
            _ = this.WriteLine( cmd );

            // Block 9: Output failure bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{failedBitPattern},{mask}";
            _ = this.WriteLine( cmd );

            // Block 10: Jump to the notification block
            block += 1;
            cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {notificationBlock}";
            _ = this.WriteLine( cmd );

            // Block 11: Output pass bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{passBitPattern},{mask}";
            _ = this.WriteLine( cmd );
            int notificationId = 1;

            // Block 12: Notify measurement completed
            block += 1;
            cmd = $":TRIG:BLOC:NOT {block},{notificationId}";
            _ = this.WriteLine( cmd );

            // set external output setting
            cmd = $":TRIG:EXT:OUT:STIM NOT{notificationId}";
            _ = this.WriteLine( cmd );

            // Block 13: Repeat Count times.
            block += 1;
            cmd = count <= 0 ? $":TRIG:BLOCk:BRAN:ALW {block},{repeatBlock}" : $":TRIG:BLOC:BRAN:COUN {block},{count},{repeatBlock}";
            _ = this.WriteLine( cmd );
        }

        /// <summary> Applies the meter complete first grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">                 Number of. </param>
        /// <param name="startDelay">            The start delay. </param>
        /// <param name="failedBitPattern">      A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">        A pattern specifying the pass bit. </param>
        /// <param name="openContactBitPattern"> A pattern specifying the open contact bit. </param>
        /// <param name="triggerSource">         The trigger source. </param>
        public void ApplyMeterCompleteFirstGradeBinning( int count, TimeSpan startDelay, int failedBitPattern, int passBitPattern, int openContactBitPattern, TriggerSources triggerSource )
        {
            int block = 0;

            // use the mask to assign digital outputs.
            int mask = failedBitPattern | passBitPattern | openContactBitPattern;
            int bitPattern = 1;
            string cmd;
            for ( int i = 1; i <= 6; i++ )
            {
                if ( (mask & bitPattern) != 0 )
                {
                    cmd = $"DIG:LINE{i}:MODE DIG,OUT";
                    _ = this.WriteLine( cmd );
                }

                bitPattern <<= 1;
            }

            // clear the trigger model
            this.ClearTriggerModel();

            // clear any pending trigger
            this.ClearTriggers();
            bool autonomous = count == 1;

            // Block 1: 
            // -- if autonomous mode, clear the buffer
            // -- if program control, clear the buffer
            block += 1;
            cmd = autonomous ? $":TRIG:BLOC:BUFF:CLE {block}" : $":TRIG:BLOC:NOP {block}";
            _ = this.WriteLine( cmd );
            int notificationId = 1;

            // Block 2: Notify measurement completed; this is the loop start block.
            int loopStartBlock = block + 1;
            block += 1;
            cmd = $":TRIG:BLOC:NOT {block},{notificationId}";
            _ = this.WriteLine( cmd );

            // set external output setting
            cmd = $":TRIG:EXT:OUT:STIM NOT{notificationId}";
            _ = this.WriteLine( cmd );

            // Block 3: Wait for external trigger
            block += 1;
            cmd = $":TRIG:BLOC:WAIT {block}, EXT";
            if ( 0 != (triggerSource & TriggerSources.Bus) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, COMM";
            }
            else if ( 0 != (triggerSource & TriggerSources.Manual) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, DISP";
            }
            else if ( 0 != (triggerSource & TriggerSources.TriggerLink) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, TSPL(1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Timer) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, TIM(1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Digital) )
            {
                cmd = $":TRIG:BLOC:WAIT {block}, DIG(1)";
            }

            _ = this.WriteLine( cmd );

            // Block 4: Pre-Measure Delay
            block += 1;
            cmd = $":TRIG:BLOC:DEL:CONS {block}, {0.001d * startDelay.TotalMilliseconds}";
            _ = this.WriteLine( cmd );

            // Block 5: Measure and save to the default buffer.
            block += 1;
            cmd = $":TRIG:BLOC:MEAS {block}";
            _ = this.WriteLine( cmd );

            // Block 6: Limit 2 (open contact) test and branch if INside to the limit block; the limit 1 block is 3 blocks ahead (8)
            int limitBlock = block + 4;
            block += 1;
            cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,2,{limitBlock}";
            _ = this.WriteLine( cmd );

            // Block 7: Output open bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{openContactBitPattern},{mask}";
            _ = this.WriteLine( cmd );

            // Block 8: Jump to the loop back block number
            int loopBackBlock = block + 6; // = 12
            block += 1;
            cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {loopBackBlock}";
            _ = this.WriteLine( cmd );

            // Block 8: Limit 1 test and branch if INside to the pass block; the pass block is 3 blocks ahead
            int passBlock = block + 4; // = 11
            block += 1;
            cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,1,{passBlock}";
            _ = this.WriteLine( cmd );

            // Block 9: Output failure bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{failedBitPattern},{mask}";
            _ = this.WriteLine( cmd );

            // Block 10: Jump to the loop back block
            block += 1;
            cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {loopBackBlock}";
            _ = this.WriteLine( cmd );

            // Block 11: Output pass bit pattern
            block += 1;
            cmd = $":TRIG:BLOC:DIG:IO {block},{passBitPattern},{mask}";
            _ = this.WriteLine( cmd );

            // Block 13: Repeat Count times; Jump to the loop start block
            block += 1;
            cmd = count <= 0 ? $":TRIG:BLOCk:BRAN:ALW {block},{loopStartBlock}" : $":TRIG:BLOC:BRAN:COUN {block},{count},{loopStartBlock}";
            _ = this.WriteLine( cmd );
        }

        #endregion

    }
}
