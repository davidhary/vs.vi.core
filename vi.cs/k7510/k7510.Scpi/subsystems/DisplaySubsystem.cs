namespace isr.VI.K7510
{

    /// <summary> Display subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class DisplaySubsystem : DisplaySubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="DisplaySubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public DisplaySubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Enabled = true;
            this.Exists = true;
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " COMMANDS "

        /// <summary>
        /// Gets or sets the clear caution messages command. Impedance analyzer command.
        /// </summary>
        /// <value> The clear caution messages command. </value>
        protected override string ClearCautionMessagesCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the clear command. </summary>
        /// <value> The clear command. </value>
        protected override string ClearCommand { get; set; } = ":DISP:CLE";

        #endregion

        #region " ENABLED "

        /// <summary> Gets or sets the display enable command format. </summary>
        /// <value> The display enable command format. </value>
        protected override string DisplayEnableCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the display enabled query command. </summary>
        /// <value> The display enabled query command. </value>
        protected override string DisplayEnabledQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " DISPLAY SCREEN  "

        /// <summary> Gets or sets the display Screen command format. </summary>
        /// <value> The display Screen command format. </value>
        protected override string DisplayScreenCommandFormat { get; set; } = ":DISP:SCR {0}";

        /// <summary> Gets or sets the display Screen query command. </summary>
        /// <value> The display Screen query command. </value>
        protected override string DisplayScreenQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " DISPLAY TEXT "

        /// <summary> Gets or sets the Display Line command. </summary>
        /// <remarks> SCPI: ":DISP:USER{0}:TEXT ""{1}""". </remarks>
        /// <value> The Display Line command. </value>
        protected override string DisplayLineCommandFormat { get; set; } = ":DISP:USER{0}:TEXT \"{1}\"";

        #endregion

        #endregion

    }
}
