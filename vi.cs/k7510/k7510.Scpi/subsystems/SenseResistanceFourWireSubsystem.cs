namespace isr.VI.K7510
{

    /// <summary> A sense four wire resistance subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-04-08 </para>
    /// </remarks>
    public class SenseResistanceFourWireSubsystem : SenseResistanceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SenseResistanceFourWireSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Ohm );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm;
            this.DefineFunctionModeReadWrites( "{0}", "'{0}'" );
            this.ResistanceRangeCurrents.Clear();
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 0m, 0m, "Auto Range" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20m, 0.0072m, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 7.2 mA" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 200m, 0.00096m, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2000m, 0.00096m, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 20000m, 0.000096m, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 96 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 200000m, 0.0000096m, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 9.6 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
            this.ResistanceRangeCurrents.Add( new ResistanceRangeCurrent( 2000000m, 0.0000019m, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.9 {Arebis.StandardUnits.UnitSymbols.MU}A" ) );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Customizes the reset state. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            this.PowerLineCyclesRange = this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d ) == 60d ? new Core.Primitives.RangeR( 0.0005d, 15d ) : new Core.Primitives.RangeR( 0.0005d, 12d );
            this.FunctionRange = new Core.Primitives.RangeR( 1d, 1000000000.0d );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm;
            base.DefineKnownResetState();
            this.PowerLineCyclesRange = this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d ) == 60d ? new Core.Primitives.RangeR( 0.0005d, 15d ) : new Core.Primitives.RangeR( 0.0005d, 12d );
            this.FunctionRange = new Core.Primitives.RangeR( 1d, 1000000000.0d );
        }

        #endregion

        #region " COMMAND SYNTAX "

        #region " AVERAGE "

        /// <summary> Gets or sets the average enabled command Format. </summary>
        /// <value> The average enabled query command. </value>
        protected override string AverageEnabledCommandFormat { get; set; } = ":SENS:FRES:AVER {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the average enabled query command. </summary>
        /// <value> The average enabled query command. </value>
        protected override string AverageEnabledQueryCommand { get; set; } = ":SENS:FRES:AVER?";

        #endregion

        #region " AVERAGE COUNT "

        /// <summary> Gets or sets the Average Count command format. </summary>
        /// <value> The AverageCount command format. </value>
        protected override string AverageCountCommandFormat { get; set; } = ":SENS:FRES:AVER:COUNT {0}";

        /// <summary> Gets or sets the Average Count query command. </summary>
        /// <value> The AverageCount query command. </value>
        protected override string AverageCountQueryCommand { get; set; } = ":SENS:FRES:AVER:COUNT?";

        #endregion

        #region " AVERAGE FILTER TYPE "

        /// <summary> Gets or sets the Average FilterType command format. </summary>
        /// <value> The AverageFilterType command format. </value>
        protected override string AverageFilterTypeCommandFormat { get; set; } = ":SENS:FRES:AVER:TCON {0}";

        /// <summary> Gets or sets the Average FilterType query command. </summary>
        /// <value> The AverageFilterType query command. </value>
        protected override string AverageFilterTypeQueryCommand { get; set; } = ":SENS:FRES:AVER:TCON?";

        #endregion

        #region " AVERAGE PERCENT WINDOW "

        /// <summary> Gets or sets the Average Percent Window command format. </summary>
        /// <value> The AveragePercentWindow command format. </value>
        protected override string AveragePercentWindowCommandFormat { get; set; } = ":SENS:FRES:AVER:WIND {0}";

        /// <summary> Gets or sets the Average Percent Window query command. </summary>
        /// <value> The AveragePercentWindow query command. </value>
        protected override string AveragePercentWindowQueryCommand { get; set; } = ":SENS:FRES:AVER:WIND?";

        #endregion

        #region " AUTO ZERO "

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = ":SENS:FRES:AZER {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = ":SENS:FRES:AZER?";

        #endregion

        #region " AUTO RANGE "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = ":SENS:FRES:RANG:AUTO {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = ":SENS:FRES:RANG:AUTO?";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = ":SENS:FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = ":SENS:FUNC?";

        #endregion

        #region " OPEN LEAD DETECTOR "

        /// <summary> Gets or sets the Open Lead Detector enabled command Format. </summary>
        /// <value> The Open Lead Detector enabled query command. </value>
        protected override string OpenLeadDetectorEnabledCommandFormat { get; set; } = ":SENS:FRES:ODET {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Open Lead Detector enabled query command. </summary>
        /// <value> The Open Lead Detector enabled query command. </value>
        protected override string OpenLeadDetectorEnabledQueryCommand { get; set; } = ":SENS:FRES:ODET?";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = ":SENS:FRES:NPLC {0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = ":SENS:FRES:NPLC?";

        #endregion

        #region " RANGE "

        /// <summary> The maximum Kelvin test resistance. </summary>
        public const double MaximumKelvinTestResistance = 2100000.0d;

        /// <summary> Determine if we can use Kelvin (4-wire) measurements. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="resistance"> The resistance. </param>
        /// <returns> <c>true</c> if we can use Kelvin; otherwise <c>false</c> </returns>
        public static bool CanUseKelvin( double resistance )
        {
            return resistance <= MaximumKelvinTestResistance;
        }

        /// <summary> Gets or sets the range command format. </summary>
        /// <value> The range command format. </value>
        protected override string RangeCommandFormat { get; set; } = ":SENS:FRES:RANG {0}";

        /// <summary> Gets or sets the range query command. </summary>
        /// <value> The range query command. </value>
        protected override string RangeQueryCommand { get; set; } = ":SENS:FRES:RANG?";

        #endregion

        #endregion

    }
}
