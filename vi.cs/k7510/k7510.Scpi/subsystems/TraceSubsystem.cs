namespace isr.VI.K7510
{

    /// <summary> Defines a Trace Subsystem for a Keithley 7510 Meter. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TraceSubsystem : TraceSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TraceSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.PointsCount = 10;
            this.FeedSource = FeedSources.None;
            this.FeedControl = FeedControls.Never;
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the ActualPoint count query command. </summary>
        /// <value> The ActualPoint count query command. </value>
        protected override string ActualPointCountQueryCommand { get; set; } = ":TRAC:ACT?";

        /// <summary> Gets or sets the Clear Buffer command. </summary>
        /// <value> The ClearBuffer command. </value>
        protected override string ClearBufferCommand { get; set; } = ":TRAC:CLE";

        /// <summary> Gets or sets the data query command. </summary>
        /// <value> The points count query command. </value>
        protected override string DataQueryCommand { get; set; } = ":TRAC:DATA?";

        /// <summary> Gets or sets The First Point Number query command. </summary>
        /// <value> The First Point Number query command. </value>
        protected override string FirstPointNumberQueryCommand { get; set; } = ":TRAC:ACT:STAR?";

        /// <summary> Gets or sets The Last Point Number query command. </summary>
        /// <value> The Last Point Number query command. </value>
        protected override string LastPointNumberQueryCommand { get; set; } = ":TRAC:ACT:END?";

        /// <summary> Gets or sets the points count query command. </summary>
        /// <value> The points count query command. </value>
        protected override string PointsCountQueryCommand { get; set; } = ":TRAC:POIN?";

        /// <summary> Gets or sets the points count command format. </summary>
        /// <value> The points count command format. </value>
        protected override string PointsCountCommandFormat { get; set; } = ":TRAC:POIN 10; *WAI; :TRAC:POIN {0}";


        #endregion

    }
}
