namespace isr.VI.Tsp2.K7510
{

    /// <summary> System subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="P:isr.VI.SubsystemPlusStatusBase.StatusSubsystem">TSP
        ///                                status Subsystem</see>. </param>
        public SystemSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "



        #endregion

        #region " BEEPER IMMEDIATE "

        /// <summary> Gets or sets the Beeper enabled query command. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the Beeper enabled command Format. </summary>
        /// <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
        /// <value> The Beeper enabled query command. </value>
        protected override string BeeperEnabledCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the beeper immediate command format. </summary>
        /// <value> The beeper immediate command format. </value>
        protected override string BeeperImmediateCommandFormat { get; set; } = "beeper.beep({0},{1})";

        #endregion

        #region " FAN LEVEL "

        /// <summary> Converts the specified value to string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The <see cref="P:isr.VI.SystemSubsystemBase.FanLevel">Fan Level</see>. </param>
        /// <returns> A String. </returns>
        protected override string FromFanLevel( FanLevel value )
        {
            return value == VI.FanLevel.Normal ? "fan.LEVEL_NORMAL" : "fan.LEVEL_QUIET";
        }

        /// <summary> Gets or sets the Fan Level query command. </summary>
        /// <value> The Fan Level command. </value>
        protected override string FanLevelQueryCommand { get; set; } = "_G.print(_G.fan.level)";

        /// <summary> Gets or sets the Fan Level command format. </summary>
        /// <value> The Fan Level command format. </value>
        protected override string FanLevelCommandFormat { get; set; } = "_G.fan.level={0}";

        #endregion

    }
}
