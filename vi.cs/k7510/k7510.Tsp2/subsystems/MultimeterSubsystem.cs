using System;

namespace isr.VI.Tsp2.K7510
{

    /// <summary> Measure subsystem. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-01-03 </para>
    /// </remarks>
    public class MultimeterSubsystem : MultimeterSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Initializes a new instance of the <see cref="MultimeterSubsystem" /> class.
        /// </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">message
        ///                                based session</see>. </param>
        public MultimeterSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading | ReadingElementTypes.Timestamp | ReadingElementTypes.Status | ReadingElementTypes.Units );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.CurrentAC].SetRange( 0d, 10d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.CurrentDC].SetRange( 0d, 10d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.VoltageDC].SetRange( 0d, 1000d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.VoltageAC].SetRange( 0d, 700d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.ResistanceTwoWire].SetRange( 0d, 1000000000.0d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.ResistanceFourWire].SetRange( 0d, 1000000000.0d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.Continuity].SetRange( 0d, 1000d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.Diode].SetRange( 0d, 10d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.Capacitance].SetRange( 0d, 0.001d );
            this.FunctionModeRanges[( int ) MultimeterFunctionModes.Ratio].SetRange( 0d, 1000d );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            double lineFrequency = this.StatusSubsystem.LineFrequency.GetValueOrDefault( 60d );
            this.PowerLineCyclesRange = lineFrequency == 50d ? new Core.Primitives.RangeR( 0.0005d, 12d ) : new Core.Primitives.RangeR( 0.0005d, 15d );
            this.ApertureRange = new Core.Primitives.RangeR( this.PowerLineCyclesRange.Min / lineFrequency, this.PowerLineCyclesRange.Max / lineFrequency );
            this.FilterCountRange = new Core.Primitives.RangeI( 1, 100 );
            this.FilterWindowRange = new Core.Primitives.RangeR( 0d, 0.1d );
            _ = this.QueryFrontTerminalsSelected();
        }

        /// <summary> Sets the known reset (default) state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            base.DefineKnownResetState();
            this.FilterCountRange = new Core.Primitives.RangeI( 1, 100 );
            this.FilterWindowRange = new Core.Primitives.RangeR( 0d, 0.1d );
            this.PowerLineCycles = 0.5d;
            this.AutoZeroEnabled = true;
            this.FilterCount = 10;
            this.FilterEnabled = false;
            this.MovingAverageFilterEnabled = false;
            this.OpenDetectorEnabled = false;
            this.FilterWindow = 0.001d;
            this.FunctionMode = MultimeterFunctionModes.VoltageDC;
            this.Range = 100;
        }

        #endregion

        #region " APERTURE "

        /// <summary> Gets or sets the Aperture query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string ApertureQueryCommand { get; set; } = "_G.print(_G.dmm.measure.aperture)";

        /// <summary> Gets or sets the Aperture command format. </summary>
        /// <value> The Aperture command format. </value>
        protected override string ApertureCommandFormat { get; set; } = "_G.dmm.measure.aperture={0}";

        #endregion

        #region " AUTO DELAY ENABLED "

        /// <summary> Gets or sets the automatic delay enabled command Format. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledCommandFormat { get; set; } = "_G.dmm.measure.autodelay={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the automatic delay enabled query command. </summary>
        /// <value> The automatic delay enabled query command. </value>
        protected override string AutoDelayEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.autodelay==dmm.ON)";

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = "_G.dmm.measure.autorange={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.autorange==dmm.ON)";

        #endregion

        #region " AUTO ZERO ENABLED "

        /// <summary> Gets or sets the automatic Zero enabled command Format. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledCommandFormat { get; set; } = "_G.dmm.measure.autozero.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the automatic Zero enabled query command. </summary>
        /// <value> The automatic Zero enabled query command. </value>
        protected override string AutoZeroEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.autozero.enable==dmm.ON)";

        #endregion

        #region " AUTO ZERO ONCE "

        /// <summary> Gets or sets the automatic zero once command. </summary>
        /// <value> The automatic zero once command. </value>
        protected override string AutoZeroOnceCommand { get; set; } = "_G.dmm.measure.autozero.once()";

        #endregion

        #region " BIAS "

        #region " BIAS ACTUAL "

        /// <summary> Gets or sets the Bias Actual query command. </summary>
        /// <value> The BiasActual query command. </value>
        protected override string BiasActualQueryCommand { get; set; } = "_G.print(_G.dmm.measure.bias.Actual)";

        #endregion

        #region " BIAS LEVEL "

        /// <summary> Gets or sets the Bias Level query command. </summary>
        /// <value> The BiasLevel query command. </value>
        protected override string BiasLevelQueryCommand { get; set; } = "_G.dmm.measure.bias.Level={0}";

        /// <summary> Gets or sets the Bias Level command format. </summary>
        /// <value> The BiasLevel command format. </value>
        protected override string BiasLevelCommandFormat { get; set; } = "_G.print(_G.dmm.measure.bias.Level)";

        #endregion

        #endregion

        #region " FILTER "

        #region " FILTER COUNT "

        /// <summary> Gets or sets the Filter Count query command. </summary>
        /// <value> The FilterCount query command. </value>
        protected override string FilterCountQueryCommand { get; set; } = "_G.print(_G.dmm.measure.filter.count)";

        /// <summary> Gets or sets the Filter Count command format. </summary>
        /// <value> The FilterCount command format. </value>
        protected override string FilterCountCommandFormat { get; set; } = "_G.dmm.measure.filter.count={0}";

        #endregion

        #region " FILTER ENABLED "

        /// <summary> Gets or sets the Filter enabled command Format. </summary>
        /// <value> The Filter enabled query command. </value>
        protected override string FilterEnabledCommandFormat { get; set; } = "_G.dmm.measure.filter.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the Filter enabled query command. </summary>
        /// <value> The Filter enabled query command. </value>
        protected override string FilterEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.filter.enable==dmm.ON)";

        #endregion

        #region " MOVING AVERAGE ENABLED "

        /// <summary> Gets or sets the moving average filter enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string MovingAverageFilterEnabledCommandFormat { get; set; } = "_G.dmm.measure.filter.type={0:'dmm.FILTER_MOVING_AVG';'dmm.FILTER_MOVING_AVG';'dmm.FILTER_REPEAT_AVG'}";

        /// <summary> Gets or sets the moving average filter enabled query command. </summary>
        /// <value> The moving average filter enabled query command. </value>
        protected override string MovingAverageFilterEnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.filter.type==dmm.FILTER_MOVING_AVG)";

        #endregion

        #region " FILTER WINDOW "

        /// <summary> Gets or sets the Filter Window query command. </summary>
        /// <value> The FilterWindow query command. </value>
        protected override string FilterWindowQueryCommand { get; set; } = "_G.print(_G.dmm.measure.filter.window)";

        /// <summary> Gets or sets the Filter Window command format. </summary>
        /// <value> The FilterWindow command format. </value>
        protected override string FilterWindowCommandFormat { get; set; } = "_G.dmm.measure.filter.window={0}";

        #endregion

        #endregion

        #region " FRONT TERMINALS SELECTED "

        /// <summary> Gets or sets the front terminals selected command format. </summary>
        /// <value> The front terminals selected command format. </value>
        protected override string FrontTerminalsSelectedCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the front terminals selected query command. </summary>
        /// <value> The front terminals selected query command. </value>
        protected override string FrontTerminalsSelectedQueryCommand { get; set; } = "_G.print(_G.dmm.terminals==dmm.TERMINALS_FRONT)";

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Select reading unit. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="value"> The value. </param>
        /// <returns> An Arebis.TypedUnits.Unit. </returns>
        public static Arebis.TypedUnits.Unit SelectReadingUnit( MultimeterFunctionModes value )
        {
            switch ( value )
            {
                case MultimeterFunctionModes.Capacitance:
                    {
                        return Arebis.StandardUnits.ElectricUnits.Farad;
                    }

                case MultimeterFunctionModes.Continuity:
                    {
                        return Arebis.StandardUnits.ElectricUnits.Ohm;
                    }

                case MultimeterFunctionModes.CurrentAC:
                case MultimeterFunctionModes.CurrentDC:
                    {
                        return Arebis.StandardUnits.ElectricUnits.Ampere;
                    }

                case MultimeterFunctionModes.Diode:
                    {
                        return Arebis.StandardUnits.ElectricUnits.Ohm;
                    }

                case MultimeterFunctionModes.Frequency:
                    {
                        return Arebis.StandardUnits.FrequencyUnits.Hertz;
                    }

                case MultimeterFunctionModes.None:
                    {
                        return Arebis.StandardUnits.UnitlessUnits.Ratio;
                    }

                case MultimeterFunctionModes.Period:
                    {
                        return Arebis.StandardUnits.TimeUnits.Second;
                    }

                case MultimeterFunctionModes.Ratio:
                    {
                        return Arebis.StandardUnits.UnitlessUnits.Ratio;
                    }

                case MultimeterFunctionModes.ResistanceFourWire:
                case MultimeterFunctionModes.ResistanceTwoWire:
                    {
                        return Arebis.StandardUnits.ElectricUnits.Ohm;
                    }

                case MultimeterFunctionModes.VoltageAC:
                case MultimeterFunctionModes.VoltageDC:
                    {
                        return Arebis.StandardUnits.ElectricUnits.Volt;
                    }

                default:
                    {
                        throw new InvalidOperationException( $"Unhandled case {value} selecting reading unit" );
                    }
            }
        }

        /// <summary> Select function mode. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="value"> The value. </param>
        public void SelectFunctionMode( MultimeterFunctionModes value )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading | ReadingElementTypes.Timestamp | ReadingElementTypes.Status | ReadingElementTypes.Units );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( SelectReadingUnit( value ) );
        }

        /// <summary> Gets or sets the function mode query command. </summary>
        /// <value> The function mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = "_G.print(_G.dmm.measure.func)";

        /// <summary> Gets or sets the function mode command format. </summary>
        /// <value> The function mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = "_G.dmm.measure.func={0}";

        #region " UNIT "

        /// <summary> Gets or sets the Measure Unit query command. </summary>
        /// <remarks>
        /// The query command uses the
        /// <see cref="M:VI.Pith.SessionBase.QueryPrintTrimEnd(System.Int32,System.String)" />
        /// </remarks>
        /// <value> The Unit query command. </value>
        protected override string MultimeterMeasurementUnitQueryCommand { get; set; } = "_G.print(_G.smu.measure.unit)";

        /// <summary> Gets or sets the Measure Unit command format. </summary>
        /// <value> The Unit command format. </value>
        protected override string MultimeterMeasurementUnitCommandFormat { get; set; } = "_G.smu.measure.unit={0}";

        #endregion

        #endregion

        #region " INPUT IMPEDANCE "

        /// <summary> Gets or sets the Input Impedance Mode query command. </summary>
        /// <value> The Input Impedance Mode query command. </value>
        protected override string InputImpedanceModeQueryCommand { get; set; } = "_G.print(_G.dmm.measure.inputimpedance)";

        /// <summary> Gets or sets the Input Impedance Mode command format. </summary>
        /// <value> The Input Impedance Mode command format. </value>
        protected override string InputImpedanceModeCommandFormat { get; set; } = "_G.dmm.measure.inputimpedance={0}";

        #endregion

        #region " LIMIT 1 "

        #region " LIMIT1 AUTO CLEAR "

        /// <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
        /// <value> The Limit1 AutoClear query command. </value>
        protected override string Limit1AutoClearCommandFormat { get; set; } = "_G.dmm.measure.limit1.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
        /// <value> The Limit1 AutoClear query command. </value>
        protected override string Limit1AutoClearQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit1.autoclear==dmm.ON)";

        #endregion

        #region " LIMIT1 ENABLED "

        /// <summary> Gets or sets the Limit1 enabled command Format. </summary>
        /// <value> The Limit1 enabled query command. </value>
        protected override string Limit1EnabledCommandFormat { get; set; } = "_G.dmm.measure.limit1.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the Limit1 enabled query command. </summary>
        /// <value> The Limit1 enabled query command. </value>
        protected override string Limit1EnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit1.enable==dmm.ON)";

        #endregion

        #region " LIMIT1 LOWER LEVEL "

        /// <summary> Gets or sets the Limit1 Lower Level command format. </summary>
        /// <value> The Limit1LowerLevel command format. </value>
        protected override string Limit1LowerLevelCommandFormat { get; set; } = "_G.dmm.measure.limit1.low.value={0}";

        /// <summary> Gets or sets the Limit1 Lower Level query command. </summary>
        /// <value> The Limit1LowerLevel query command. </value>
        protected override string Limit1LowerLevelQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit1.low.value)";

        #endregion

        #region " Limit1 UPPER LEVEL "

        /// <summary> Gets or sets the Limit1 Upper Level command format. </summary>
        /// <value> The Limit1UpperLevel command format. </value>
        protected override string Limit1UpperLevelCommandFormat { get; set; } = "_G.dmm.measure.limit1.high.value={0}";

        /// <summary> Gets or sets the Limit1 Upper Level query command. </summary>
        /// <value> The Limit1UpperLevel query command. </value>
        protected override string Limit1UpperLevelQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit1.high.value)";

        #endregion

        #endregion

        #region " LIMIT 2 "

        #region " LIMIT2 AUTO CLEAR "

        /// <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
        /// <value> The Limit2 AutoClear query command. </value>
        protected override string Limit2AutoClearCommandFormat { get; set; } = "_G.dmm.measure.limit2.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
        /// <value> The Limit2 AutoClear query command. </value>
        protected override string Limit2AutoClearQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit2.autoclear==dmm.ON)";

        #endregion

        #region " LIMIT2 ENABLED "

        /// <summary> Gets or sets the Limit2 enabled command Format. </summary>
        /// <value> The Limit2 enabled query command. </value>
        protected override string Limit2EnabledCommandFormat { get; set; } = "_G.dmm.measure.limit2.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}";

        /// <summary> Gets or sets the Limit2 enabled query command. </summary>
        /// <value> The Limit2 enabled query command. </value>
        protected override string Limit2EnabledQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit2.enable==dmm.ON)";

        #endregion

        #region " LIMIT2 LOWER LEVEL "

        /// <summary> Gets or sets the Limit2 Lower Level command format. </summary>
        /// <value> The Limit2LowerLevel command format. </value>
        protected override string Limit2LowerLevelCommandFormat { get; set; } = "_G.dmm.measure.limit2.low.value={0}";

        /// <summary> Gets or sets the Limit2 Lower Level query command. </summary>
        /// <value> The Limit2LowerLevel query command. </value>
        protected override string Limit2LowerLevelQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit2.low.value)";

        #endregion

        #region " LIMIT2 UPPER LEVEL "

        /// <summary> Gets or sets the Limit2 Upper Level command format. </summary>
        /// <value> The Limit2UpperLevel command format. </value>
        protected override string Limit2UpperLevelCommandFormat { get; set; } = "_G.dmm.measure.limit1.high.value={0}";

        /// <summary> Gets or sets the Limit2 Upper Level query command. </summary>
        /// <value> The Limit2UpperLevel query command. </value>
        protected override string Limit2UpperLevelQueryCommand { get; set; } = "_G.print(_G.dmm.measure.limit2.high.value)";

        #endregion
        #endregion

        #region " OPEN DETECTOR ENABLED "

        /// <summary> Gets or sets the open detector enabled command Format. </summary>
        /// <value> The open detector enabled query command. </value>
        protected override string OpenDetectorEnabledCommandFormat { get; set; } = string.Empty;

        /// <summary> Gets or sets the open detector enabled query command. </summary>
        /// <value> The open detector enabled query command. </value>
        protected override string OpenDetectorEnabledQueryCommand { get; set; } = string.Empty;

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = "_G.dmm.measure.nplc={0}";

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = "_G.print(_G.dmm.measure.nplc)";

        #endregion

        #region " RANGE "

        /// <summary> Gets or sets the Range query command. </summary>
        /// <value> The Range query command. </value>
        protected override string RangeQueryCommand { get; set; } = "_G.print(_G.dmm.measure.range)";

        /// <summary> Gets or sets the Range command format. </summary>
        /// <value> The Range command format. </value>
        protected override string RangeCommandFormat { get; set; } = "_G.dmm.measure.range={0}";

        #endregion

        #region " READ "

        /// <summary> Gets or sets the read buffer query command format. </summary>
        /// <value> The read buffer query command format. </value>
        protected override string ReadBufferQueryCommandFormat { get; set; } = "_G.print(_G.dmm.measure.read({0}))";

        /// <summary> Gets or sets the Measure query command. </summary>
        /// <value> The Aperture query command. </value>
        protected override string MeasureQueryCommand { get; set; } = "_G.print(_G.dmm.measure.read())";

        #endregion

    }
}
