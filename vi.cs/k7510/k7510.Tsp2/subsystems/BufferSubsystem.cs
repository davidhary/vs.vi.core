namespace isr.VI.Tsp2.K7510
{

    /// <summary> Buffer subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class BufferSubsystem : BufferSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="BufferSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> A reference to a
        ///                                <see cref="T:isr.VI.StatusSubsystemBase">status
        ///                                subsystem</see>. </param>
        public BufferSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="bufferName">      Name of the buffer. </param>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public BufferSubsystem( string bufferName, VI.StatusSubsystemBase statusSubsystem ) : base( bufferName, statusSubsystem )
        {
        }

        #endregion

    }
}
