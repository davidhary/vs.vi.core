using System;

namespace isr.VI.Tsp2.K7510
{

    /// <summary> Defines a Trigger Subsystem for a Keithley 7510 Meter. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2012-09-26, 1.0.4652. </para>
    /// </remarks>
    public class TriggerSubsystem : TriggerSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public TriggerSubsystem( VI.StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.SupportedTriggerSources = TriggerSources.Bus | TriggerSources.External | TriggerSources.Hold | TriggerSources.Immediate | TriggerSources.Manual | TriggerSources.Timer | TriggerSources.TriggerLink;
        }

        #endregion

        #region " SIMPLE LOOP "

        /// <summary> The simple loop model. </summary>
        private const string _SimpleLoopModel = "SimpleLoop";

        /// <summary> Loads simple loop. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count"> Number of. </param>
        /// <param name="delay"> The delay. </param>
        public void LoadSimpleLoop( int count, TimeSpan delay )
        {
            _ = this.WriteLine( "trigger.mode.load('{0}',{1},{2})", _SimpleLoopModel, count, delay.TotalSeconds );
        }

        #endregion

        #region " GRAD BINNING "

        /// <summary> The grade binning model. </summary>
        private const string _GradeBinningModel = "GradeBinning";

        /// <summary> Loads grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="triggerOption">    The trigger option (use 7 for external line). </param>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="endDelay">         The end delay. </param>
        /// <param name="highLimit">        The high limit. </param>
        /// <param name="lowLimit">         The low limit. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void LoadGradeBinning( int count, int triggerOption, TimeSpan startDelay, TimeSpan endDelay, double highLimit, double lowLimit, int failedBitPattern, int passBitPattern )
        {
            _ = this.WriteLine( "trigger.mode.load('{0}',{1},{2},{3},{4},{5},{6},{7},{8})", _GradeBinningModel, count, triggerOption, startDelay.TotalSeconds, endDelay.TotalSeconds, highLimit, lowLimit, failedBitPattern, passBitPattern );
        }

        /// <summary> Loads grade binning. </summary>
        /// <remarks> Uses external trigger (start line = 7). </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="endDelay">         The end delay. </param>
        /// <param name="highLimit">        The high limit. </param>
        /// <param name="lowLimit">         The low limit. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void LoadGradeBinning( int count, TimeSpan startDelay, TimeSpan endDelay, double highLimit, double lowLimit, int failedBitPattern, int passBitPattern )
        {
            this.LoadGradeBinning( count, 7, startDelay, endDelay, highLimit, lowLimit, failedBitPattern, passBitPattern );
        }

        /// <summary> Loads grade binning. </summary>
        /// <remarks> Uses external trigger (start line = 7) and maximum count (268000000). </remarks>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="endDelay">         The end delay. </param>
        /// <param name="highLimit">        The high limit. </param>
        /// <param name="lowLimit">         The low limit. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void LoadGradeBinning( TimeSpan startDelay, TimeSpan endDelay, double highLimit, double lowLimit, int failedBitPattern, int passBitPattern )
        {
            this.LoadGradeBinning( 268000000, 7, startDelay, endDelay, highLimit, lowLimit, failedBitPattern, passBitPattern );
        }

        #endregion

        #region " CUSTOM BINNING "

        /// <summary> Applies the grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">            Number of. </param>
        /// <param name="startDelay">       The start delay. </param>
        /// <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">   A pattern specifying the pass bit. </param>
        public void ApplyGradeBinning( int count, TimeSpan startDelay, int failedBitPattern, int passBitPattern )
        {
            int block = 0;

            // use the mask to assign digital outputs.
            int mask = failedBitPattern | passBitPattern;
            int bitPattern = 1;
            string cmd;
            for ( int i = 1; i <= 6; i++ )
            {
                if ( (mask & bitPattern) != 0 )
                {
                    cmd = $"digio.line[{i}].mode=digio.MODE_DIGIIAL_OUT";
                    _ = this.WriteLine( cmd );
                }

                bitPattern <<= 1;
            }

            // clear the trigger model
            cmd = "trigger.model.load('Empty')";
            _ = this.WriteLine( cmd );

            // clear any pending trigger
            cmd = "trigger.extin.clear()";
            _ = this.WriteLine( cmd );

            // clear the default buffer
            cmd = "defbuffer1.clear()";
            _ = this.WriteLine( cmd );
            bool autonomous = count == 1;

            // Block 1: 
            // -- if autonomous mode, clear the buffer
            // -- if program control, clear the buffer
            block += 1;
            cmd = autonomous ? $"trigger.model.setblock({block},trigger.BLOCK_BUFFER_CLEAR)" : $"trigger.model.setblock({block},trigger.BLOCK_NOP)";
            _ = this.WriteLine( cmd );

            // Block 2: Wait for external trigger; this is the repeat block.
            int repeatBlock = block + 1;
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_EXTERNAL)";
            _ = this.WriteLine( cmd );

            // Block 3: Pre-Measure Delay
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DELAY_CONSTANT, {0.001d * startDelay.TotalMilliseconds})";
            _ = this.WriteLine( cmd );

            // Block 4: Measure and save to the default buffer.
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_MEASURE)";
            _ = this.WriteLine( cmd );

            // Block 5: Limit test and branch; the pass block is 3 blocks ahead (8)
            int passBlock = block + 4;
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_LIMIT_DYNAMIC, trigger.LIMIT_INSIDE, 1, {passBlock})";
            _ = this.WriteLine( cmd );

            // Block 6: Output failure bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {failedBitPattern},{mask})";
            _ = this.WriteLine( cmd );

            // Block 7: Skip the pass binning block
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {block + 2})";
            _ = this.WriteLine( cmd );

            // Block 8: Output pass bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {passBitPattern},{mask})";
            _ = this.WriteLine( cmd );
            int notificationId = 1;

            // Block 9: Notify measurement completed
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_NOTIFY, trigger.EVENT_NOTIFY{notificationId})";
            _ = this.WriteLine( cmd );

            // set external output setting
            cmd = $"trigger.extout.stimulus=trigger.EVENT_NOTIFY{notificationId}";
            _ = this.WriteLine( cmd );

            // Block 10: Repeat Count times.
            block += 1;
            cmd = count <= 0 ? $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {repeatBlock})" : $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_COUNTER, {count},{repeatBlock})";
            _ = this.WriteLine( cmd );
        }

        /// <summary> Applies the grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">                 Number of. </param>
        /// <param name="startDelay">            The start delay. </param>
        /// <param name="failedBitPattern">      A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">        A pattern specifying the pass bit. </param>
        /// <param name="openContactBitPattern"> A pattern specifying the open contact bit. </param>
        /// <param name="triggerSource">         The trigger source. </param>
        public void ApplyGradeBinning( int count, TimeSpan startDelay, int failedBitPattern, int passBitPattern, int openContactBitPattern, TriggerSources triggerSource )
        {
            int block = 0;

            // use the mask to assign digital outputs.
            int mask = failedBitPattern | passBitPattern | openContactBitPattern;
            int bitPattern = 1;
            string cmd;
            for ( int i = 1; i <= 6; i++ )
            {
                if ( (mask & bitPattern) != 0 )
                {
                    cmd = $"digio.line[{i}].mode=digio.MODE_DIGIIAL_OUT";
                    _ = this.WriteLine( cmd );
                }

                bitPattern <<= 1;
            }

            // clear the trigger model
            this.ClearTriggerModel();

            // clear any pending trigger
            this.ClearTriggers();
            bool autonomous = count == 1;

            // Block 1: 
            // -- if autonomous mode, clear the buffer
            // -- if program control, clear the buffer
            block += 1;
            cmd = autonomous ? $"trigger.model.setblock({block},trigger.BLOCK_BUFFER_CLEAR)" : $"trigger.model.setblock({block},trigger.BLOCK_NOP)";
            _ = this.WriteLine( cmd );

            // Block 2: Wait for external trigger; this is the repeat block.
            int repeatBlock = block + 1;
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_EXTERNAL)";
            if ( 0 != (triggerSource & TriggerSources.Bus) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_COMMAND)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Manual) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_DISPLAY)";
            }
            else if ( 0 != (triggerSource & TriggerSources.TriggerLink) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_TSPLINK1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Timer) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_TIMER1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Digital) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_DIGIO1)";
            }

            _ = this.WriteLine( cmd );

            // Block 3: Pre-Measure Delay
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DELAY_CONSTANT, {0.001d * startDelay.TotalMilliseconds})";
            _ = this.WriteLine( cmd );

            // Block 4: Measure and save to the default buffer.
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_MEASURE)";
            _ = this.WriteLine( cmd );

            // Block 5: Limit 2 (open contact) test and branch if INside to the limit block; the limit 1 block is 3 blocks ahead (8)
            int limitBlock = block + 4;
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_LIMIT_DYNAMIC, trigger.LIMIT_INSIDE, 2, {limitBlock})";
            _ = this.WriteLine( cmd );

            // Block 6: Output open bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {openContactBitPattern},{mask})";
            _ = this.WriteLine( cmd );

            // Block 7: Jump to the notification block number
            int notificationBlock = block + 6; // = 12
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {notificationBlock})";
            _ = this.WriteLine( cmd );

            // Block 8: Limit 1 test and branch if INside to the pass block; the pass block is 3 blocks ahead
            int passBlock = block + 4; // = 11
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_LIMIT_DYNAMIC, trigger.LIMIT_INSIDE, 1, {passBlock})";
            _ = this.WriteLine( cmd );

            // Block 9: Output failure bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {failedBitPattern},{mask})";
            _ = this.WriteLine( cmd );

            // Block 10: Jump to the notification block
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {notificationBlock})";
            _ = this.WriteLine( cmd );

            // Block 11: Output pass bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {passBitPattern},{mask})";
            _ = this.WriteLine( cmd );
            int notificationId = 1;

            // Block 12: Notify measurement completed
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_NOTIFY, trigger.EVENT_NOTIFY{notificationId})";
            _ = this.WriteLine( cmd );

            // set external output setting
            cmd = $"trigger.extout.stimulus=trigger.EVENT_NOTIFY{notificationId}";
            _ = this.WriteLine( cmd );

            // Block 13: Repeat Count times.
            block += 1;
            cmd = count <= 0 ? $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {repeatBlock})" : $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_COUNTER, {count},{repeatBlock})";
            _ = this.WriteLine( cmd );
        }

        /// <summary> Applies the meter complete first grade binning. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="count">                 Number of. </param>
        /// <param name="startDelay">            The start delay. </param>
        /// <param name="failedBitPattern">      A pattern specifying the failed bit. </param>
        /// <param name="passBitPattern">        A pattern specifying the pass bit. </param>
        /// <param name="openContactBitPattern"> A pattern specifying the open contact bit. </param>
        /// <param name="triggerSource">         The trigger source. </param>
        public void ApplyMeterCompleteFirstGradeBinning( int count, TimeSpan startDelay, int failedBitPattern, int passBitPattern, int openContactBitPattern, TriggerSources triggerSource )
        {
            int block = 0;

            // use the mask to assign digital outputs.
            int mask = failedBitPattern | passBitPattern | openContactBitPattern;
            int bitPattern = 1;
            string cmd;
            for ( int i = 1; i <= 6; i++ )
            {
                if ( (mask & bitPattern) != 0 )
                {
                    cmd = $"digio.line[{i}].mode=digio.MODE_DIGIIAL_OUT";
                    _ = this.WriteLine( cmd );
                }

                bitPattern <<= 1;
            }

            // clear the trigger model
            this.ClearTriggerModel();

            // clear any pending trigger
            this.ClearTriggers();
            bool autonomous = count == 1;

            // Block 1: 
            // -- if autonomous mode, clear the buffer
            // -- if program control, clear the buffer
            block += 1;
            cmd = autonomous ? $"trigger.model.setblock({block},trigger.BLOCK_BUFFER_CLEAR)" : $"trigger.model.setblock({block},trigger.BLOCK_NOP)";
            _ = this.WriteLine( cmd );
            int notificationId = 1;

            // Block 2: Notify measurement completed; this is the loop start block.
            int loopStartBlock = block + 1;
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_NOTIFY, trigger.EVENT_NOTIFY{notificationId})";
            _ = this.WriteLine( cmd );

            // set external output setting
            cmd = $"trigger.extout.stimulus=trigger.EVENT_NOTIFY{notificationId}";
            _ = this.WriteLine( cmd );

            // Block 3: Wait for external trigger
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_EXTERNAL)";
            if ( 0 != (triggerSource & TriggerSources.Bus) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_COMMAND)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Manual) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_DISPLAY)";
            }
            else if ( 0 != (triggerSource & TriggerSources.TriggerLink) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_TSPLINK1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Timer) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_TIMER1)";
            }
            else if ( 0 != (triggerSource & TriggerSources.Digital) )
            {
                cmd = $"trigger.model.setblock({block}, trigger.BLOCK_WAIT, trigger.EVENT_DIGIO1)";
            }

            _ = this.WriteLine( cmd );

            // Block 4: Pre-Measure Delay
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DELAY_CONSTANT, {0.001d * startDelay.TotalMilliseconds})";
            _ = this.WriteLine( cmd );

            // Block 5: Measure and save to the default buffer.
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_MEASURE)";
            _ = this.WriteLine( cmd );

            // Block 6: Limit 2 (open contact) test and branch if INside to the limit block; the limit 1 block is 3 blocks ahead (8)
            int limitBlock = block + 4;
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_LIMIT_DYNAMIC, trigger.LIMIT_INSIDE, 2, {limitBlock})";
            _ = this.WriteLine( cmd );

            // Block 7: Output open bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {openContactBitPattern},{mask})";
            _ = this.WriteLine( cmd );

            // Block 8: Jump to the loop back block number
            int loopBackBlock = block + 6; // = 12
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {loopBackBlock})";
            _ = this.WriteLine( cmd );

            // Block 8: Limit 1 test and branch if INside to the pass block; the pass block is 3 blocks ahead
            int passBlock = block + 4; // = 11
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_LIMIT_DYNAMIC, trigger.LIMIT_INSIDE, 1, {passBlock})";
            _ = this.WriteLine( cmd );

            // Block 9: Output failure bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {failedBitPattern},{mask})";
            _ = this.WriteLine( cmd );

            // Block 10: Jump to the loop back block
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {loopBackBlock})";
            _ = this.WriteLine( cmd );

            // Block 11: Output pass bit pattern
            block += 1;
            cmd = $"trigger.model.setblock({block}, trigger.BLOCK_DIGITAL_IO, {passBitPattern},{mask})";
            _ = this.WriteLine( cmd );

            // Block 13: Repeat Count times; Jump to the loop start block
            block += 1;
            cmd = count <= 0 ? $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_ALWAYS, {loopStartBlock})" : $"trigger.model.setblock({block}, trigger.BLOCK_BRANCH_COUNTER, {count},{loopStartBlock})";
            _ = this.WriteLine( cmd );
        }

        #endregion

    }
}
