using System;

namespace isr.VI.Tsp2.K7510
{

    /// <summary> Status subsystem. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2013-12-14 </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> The session. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( session )
        {
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Define measurement events bitmasks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( MeasurementEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            int failuresSummaryBitmask = 0;
            int bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.Questionable, 1 << 0 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.Origin, 1 << 1 | 1 << 2 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.MeasurementTerminal, 1 << 3 );
            bitmask = 1 << 4;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit2, bitmask );
            bitmask = 1 << 5;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit2, bitmask );
            bitmask = 1 << 6;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.LowLimit1, bitmask );
            bitmask = 1 << 7;
            failuresSummaryBitmask += bitmask;
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.HighLimit1, bitmask );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.FirstReadingInGroup, 1 << 8 );
            bitmaskDictionary.Add( MeasurementEventBitmaskKey.FailuresSummary, failuresSummaryBitmask );
        }

        /// <summary> Define measurement events bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineMeasurementEventsBitmasks()
        {
            base.DefineMeasurementEventsBitmasks();
            DefineBitmasks( this.MeasurementEventsBitmasks );
        }

        /// <summary> Define operation event bitmasks. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="bitmaskDictionary"> The bitmask dictionary. </param>
        public static void DefineBitmasks( OperationEventsBitmaskDictionary bitmaskDictionary )
        {
            if ( bitmaskDictionary is null )
                throw new ArgumentNullException( nameof( bitmaskDictionary ) );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Arming, 1 << 6 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Calibrating, 1 << 0 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Idle, 1 << 10 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Measuring, 1 << 4 );
            bitmaskDictionary.Add( OperationEventBitmaskKey.Triggering, 1 << 5 );
        }

        /// <summary> Defines the operation event bit values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void DefineOperationEventsBitmasks()
        {
            base.DefineOperationEventsBitmasks();
            DefineBitmasks( this.OperationEventsBitmasks );
        }

        #endregion

    }

    /// <summary> Gets or sets the status byte flags of the measurement event register. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    [Flags()]
    public enum MeasurementEvents
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "None" )]
        None = 0,

        /// <summary> An enum constant representing the reading overflow option. </summary>
        [System.ComponentModel.Description( "Reading Overflow" )]
        ReadingOverflow = 1,

        /// <summary> An enum constant representing the low limit 1 failed option. </summary>
        [System.ComponentModel.Description( "Low Limit 1 Failed" )]
        LowLimit1Failed = 2,

        /// <summary> An enum constant representing the high limit 1 failed option. </summary>
        [System.ComponentModel.Description( "High Limit 1 Failed" )]
        HighLimit1Failed = 4,

        /// <summary> An enum constant representing the low limit 2 failed option. </summary>
        [System.ComponentModel.Description( "Low Limit 2 Failed" )]
        LowLimit2Failed = 8,

        /// <summary> An enum constant representing the high limit 2 failed option. </summary>
        [System.ComponentModel.Description( "High Limit 2 Failed" )]
        HighLimit2Failed = 16,

        /// <summary> An enum constant representing the reading available option. </summary>
        [System.ComponentModel.Description( "Reading Available" )]
        ReadingAvailable = 32,

        /// <summary> An enum constant representing the not used 1 option. </summary>
        [System.ComponentModel.Description( "Not Used 1" )]
        NotUsed1 = 64,

        /// <summary> An enum constant representing the buffer available option. </summary>
        [System.ComponentModel.Description( "Buffer Available" )]
        BufferAvailable = 128,

        /// <summary> An enum constant representing the buffer half full option. </summary>
        [System.ComponentModel.Description( "Buffer Half Full" )]
        BufferHalfFull = 256,

        /// <summary> An enum constant representing the buffer full option. </summary>
        [System.ComponentModel.Description( "Buffer Full" )]
        BufferFull = 512,

        /// <summary> An enum constant representing the buffer overflow option. </summary>
        [System.ComponentModel.Description( "Buffer Overflow" )]
        BufferOverflow = 1024,

        /// <summary> An enum constant representing the hardware limit event option. </summary>
        [System.ComponentModel.Description( "HardwareLimit Event" )]
        HardwareLimitEvent = 2048,

        /// <summary> An enum constant representing the buffer quarter full option. </summary>
        [System.ComponentModel.Description( "Buffer Quarter Full" )]
        BufferQuarterFull = 4096,

        /// <summary> An enum constant representing the buffer three quarters full option. </summary>
        [System.ComponentModel.Description( "Buffer Three-Quarters Full" )]
        BufferThreeQuartersFull = 8192,

        /// <summary> An enum constant representing the master limit option. </summary>
        [System.ComponentModel.Description( "Master Limit" )]
        MasterLimit = 16384,

        /// <summary> An enum constant representing the not used 2 option. </summary>
        [System.ComponentModel.Description( "Not Used 2" )]
        NotUsed2 = 32768,

        /// <summary> An enum constant representing all option. </summary>
        [System.ComponentModel.Description( "All" )]
        All = 32767
    }
}
