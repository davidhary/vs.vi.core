using System;
using System.Collections.Generic;

namespace isr.VI.Tsp2.K7510
{

    /// <summary> Information about the version of a Keithley 7510 instrument. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary> Parses the instrument firmware revision. </summary>
        /// <remarks>
        /// KEITHLEY INSTRUMENTS, Model 7510, xxxxxxx, yyyyy<para>
        /// where</para><para>xxxxxxx is the serial number.</para><para>
        /// yyyyy Is the firmware revision level, such as 1.0.0i.</para>
        /// </remarks>
        /// <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
        /// <param name="revision"> Specifies the instrument
        ///                         <see cref="VersionInfoBase.FirmwareRevisionElements">firmware revision elements</see>
        ///                         e.g., <c>1.0.0i</c> for the primary, secondary, and ternary parts. 
        /// </param>
        protected override void ParseFirmwareRevision( string revision )
        {
            if ( revision is null )
            {
                throw new ArgumentNullException( nameof( revision ) );
            }
            else if ( string.IsNullOrWhiteSpace( revision ) )
            {
                base.ParseFirmwareRevision( revision );
            }
            else
            {
                base.ParseFirmwareRevision( revision );

                // get the revision sections
                var revSections = new Queue<string>( revision.Split( '.' ) );

                // Rev: 1.0.0i
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Primary.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Secondary.ToString(), revSections.Dequeue().Trim() );
                if ( revSections.Count > 0 )
                    this.FirmwareRevisionElements.Add( FirmwareRevisionElement.Ternary.ToString(), revSections.Dequeue().Trim() );
            }
        }
    }
}
