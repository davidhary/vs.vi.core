namespace isr.VI.Tsp2.K7510
{

    /// <summary> Holds a single set of instrument reading elements. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2008-01-15, 2.0.2936. Create based on the 24xx system classes. </para>
    /// </remarks>
    public class Readings : ReadingAmounts
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>

        // instantiate the base class
        public Readings() : base()
        {
            this.PrimaryReading = new MeasuredAmount( ReadingElementTypes.Reading, Arebis.StandardUnits.ElectricUnits.Volt ) {
                ComplianceLimit = Pith.Scpi.Syntax.Infinity,
                HighLimit = Pith.Scpi.Syntax.Infinity,
                LowLimit = Pith.Scpi.Syntax.NegativeInfinity,
                ReadingLength = 15
            };
            this.BaseReadings.Add( this.PrimaryReading );
        }

        /// <summary> Create a copy of the model. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="model"> The model. </param>
        public Readings( ReadingAmounts model ) : base( model )
        {
        }

        /// <summary> Create a copy of the model. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="model"> The model. </param>
        public Readings( Readings model ) : base( model )
        {
            if ( model is object )
            {
                this.PrimaryReading = new MeasuredAmount( model.PrimaryReading );
            }
        }

        /// <summary> Clones this class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="model"> The value. </param>
        /// <returns> A copy of this object. </returns>
        public override ReadingAmounts Clone( ReadingAmounts model )
        {
            return new Readings( model );
        }

        #endregion

        #region " PARSE "

        /// <summary> Builds meta status. </summary>
        /// <remarks> David, 2020-07-28. </remarks>
        /// <param name="status"> The status. </param>
        /// <returns>
        /// The <see cref="T:isr.VI.MetaStatus" /><see cref="P:isr.VI.MetaStatus.StatusValue" /> .
        /// </returns>
        protected override long BuildMetaStatus( long status )
        {
            var metaStatus = new MetaStatus();
            metaStatus.Preset( status );
            // To_DO: Add a mapper structure to map the status to meta status elements 
            // If status <> 0 Then
            // update the meta status based on the status reading.
            // If status.IsBit(StatusWordBit.FailedContactCheck) Then
            // metaStatus.FailedContactCheck = True
            // End If
            // If status.IsBit(StatusWordBit.HitCompliance) Then
            // metaStatus.HitStatusCompliance = True
            // End If
            // If status.IsBit(StatusWordBit.HitRangeCompliance) Then
            // metaStatus.HitRangeCompliance = True
            // End If
            // If status.IsBit(StatusWordBit.HitVoltageProtection) Then
            // metaStatus.HitVoltageProtection = True
            // End If
            // If status.IsBit(StatusWordBit.OverRange) Then
            // metaStatus.HitOverRange = True
            // End If
            // End If
            return metaStatus.StatusValue;
        }

        #endregion

    }
}
