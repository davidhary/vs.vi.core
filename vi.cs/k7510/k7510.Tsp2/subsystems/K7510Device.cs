using System;
using System.Diagnostics;

using isr.Core.TimeSpanExtensions;
using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp2.K7510
{

    /// <summary> A 7510 device. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public class K7510Device : VisaSessionBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="isr.VI.Tsp2.K7510.K7510Device" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public K7510Device() : this( StatusSubsystem.Create() )
        {
        }

        /// <summary> Specialized constructor for use only by derived class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The Status Subsystem. </param>
        protected K7510Device( StatusSubsystem statusSubsystem ) : base( statusSubsystem )
        {
            if ( statusSubsystem is object )
            {
                statusSubsystem.ExpectedLanguage = Pith.Ieee488.Syntax.LanguageTsp;
                My.MySettings.Default.PropertyChanged += this.HandleSettingsPropertyChanged;
            }

            this.StatusSubsystem = statusSubsystem;
        }

        /// <summary> Creates a new Device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A Device. </returns>
        public static K7510Device Create()
        {
            K7510Device device = null;
            try
            {
                device = new K7510Device();
            }
            catch
            {
                if ( device is object )
                    device.Dispose();
                throw;
            }

            return device;
        }

        /// <summary> Validated the given device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="device"> The device. </param>
        /// <returns> A Device. </returns>
        public static K7510Device Validated( K7510Device device )
        {
            return device is null ? throw new ArgumentNullException( nameof( device ) ) : device;
        }

        #region " I Disposable Support "

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                          release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.IsDeviceOpen )
                    {
                        this.OnClosing( new System.ComponentModel.CancelEventArgs() );
                        this.StatusSubsystem = null;
                    }
                }
            }
            // release unmanaged-only resources.
            catch ( Exception ex )
            {
                Debug.Assert( !Debugger.IsAttached, $"Exception disposing {typeof( K7510Device )}", ex.ToFullBlownString() );
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #endregion

        #region " SESSION "

        /// <summary>
        /// Allows the derived device to take actions before closing. Removes subsystems and event
        /// handlers.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnClosing( e );
            if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
            {
                this.BindDigitalInputOutputSubsystem( null );
                this.BindTriggerSubsystem( null );
                this.BindBuffer1Subsystem( null );
                this.BindMultimeterSubsystem( null );
                this.BindDisplaySubsystem( null );
                this.BindLocalNodeSubsystem( null );
                this.BindSystemSubsystem( null );
            }
        }

        /// <summary> Allows the derived device to take actions before opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected override void OnOpening( System.ComponentModel.CancelEventArgs e )
        {
            if ( e is null )
                throw new ArgumentNullException( nameof( e ) );
            base.OnOpening( e );
            if ( !e.Cancel )
            {
                e.Cancel = !this.StatusSubsystem.TryApplyExpectedLanguage().Success;
                if ( !e.Cancel && this.SubsystemSupportMode == SubsystemSupportMode.Full )
                {
                    this.BindSystemSubsystem( new SystemSubsystem( this.StatusSubsystem ) );
                    this.BindLocalNodeSubsystem( new LocalNodeSubsystem( this.StatusSubsystem ) );
                    this.BindDisplaySubsystem( new DisplaySubsystem( this.StatusSubsystem ) );
                    this.BindMultimeterSubsystem( new MultimeterSubsystem( this.StatusSubsystem ) );
                    this.BindTriggerSubsystem( new TriggerSubsystem( this.StatusSubsystem ) );
                    this.BindBuffer1Subsystem( new BufferSubsystem( BufferSubsystemBase.DefaultBuffer1Name, this.StatusSubsystem ) );
                    this.BindDigitalInputOutputSubsystem( new DigitalInputOutputSubsystem( this.StatusSubsystemBase ) );
                }
            }
        }

        #endregion

        #region " STATUS "

        /// <summary> Gets or sets the Status Subsystem. </summary>
        /// <value> The Status Subsystem. </value>
        public StatusSubsystem StatusSubsystem { get; private set; }

        #endregion

        #region " SYSTEM "

        /// <summary> Gets or sets the System Subsystem. </summary>
        /// <value> The System Subsystem. </value>
        public SystemSubsystem SystemSubsystem { get; private set; }

        /// <summary> Bind the System subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSystemSubsystem( SystemSubsystem subsystem )
        {
            if ( this.SystemSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.SystemSubsystem );
                this.SystemSubsystem = null;
            }

            this.SystemSubsystem = subsystem;
            if ( this.SystemSubsystem is object )
            {
                this.Subsystems.Add( this.SystemSubsystem );
            }
        }

        #endregion

        #region " BUFFER1 "

        /// <summary> Gets or sets the Buffer1 Subsystem. </summary>
        /// <value> The Buffer1 Subsystem. </value>
        public BufferSubsystem Buffer1Subsystem { get; private set; }

        /// <summary> Binds the Buffer1 subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindBuffer1Subsystem( BufferSubsystem subsystem )
        {
            if ( this.Buffer1Subsystem is object )
            {
                this.Buffer1Subsystem.PropertyChanged -= this.BufferSubsystemPropertyChanged;
                _ = this.Subsystems.Remove( this.Buffer1Subsystem );
                this.Buffer1Subsystem.Dispose();
                this.Buffer1Subsystem = null;
            }

            this.Buffer1Subsystem = subsystem;
            if ( this.Buffer1Subsystem is object )
            {
                this.Subsystems.Add( this.Buffer1Subsystem );
                this.Buffer1Subsystem.PropertyChanged += this.BufferSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Buffer subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( BufferSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( VI.BufferSubsystemBase.LastReading ):
                    {
                        _ = this.MultimeterSubsystem.ParsePrimaryReading( subsystem.LastReading?.Reading );
                        break;
                    }
            }
        }

        /// <summary> Buffer subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event inBufferion. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BufferSubsystemPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( BufferSubsystemBase )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as BufferSubsystemBase, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DIGITAL INPUT OUTPUT "

        /// <summary> Gets or sets the Digital Input Output Subsystem. </summary>
        /// <value> The Digital Input Output Subsystem. </value>
        public DigitalInputOutputSubsystem DigitalInputOutputSubsystem { get; private set; }

        /// <summary> Binds the Digital Input Output subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDigitalInputOutputSubsystem( DigitalInputOutputSubsystem subsystem )
        {
            if ( this.DigitalInputOutputSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DigitalInputOutputSubsystem );
                this.DigitalInputOutputSubsystem = null;
            }

            this.DigitalInputOutputSubsystem = subsystem;
            if ( this.DigitalInputOutputSubsystem is object )
            {
                this.Subsystems.Add( this.DigitalInputOutputSubsystem );
            }
        }

        #endregion

        #region " DISPLAY "

        /// <summary> Gets or sets the Display Subsystem. </summary>
        /// <value> The Display Subsystem. </value>
        public DisplaySubsystem DisplaySubsystem { get; private set; }

        /// <summary> Binds the Display subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindDisplaySubsystem( DisplaySubsystem subsystem )
        {
            if ( this.DisplaySubsystem is object )
            {
                _ = this.Subsystems.Remove( this.DisplaySubsystem );
                this.DisplaySubsystem = null;
            }

            this.DisplaySubsystem = subsystem;
            if ( this.DisplaySubsystem is object )
            {
                this.Subsystems.Add( this.DisplaySubsystem );
            }
        }

        #endregion

        #region " LOCAL NODE "

        /// <summary> Gets or sets the Local Node Subsystem. </summary>
        /// <value> The Local Node Subsystem. </value>
        public LocalNodeSubsystem LocalNodeSubsystem { get; private set; }

        /// <summary> Binds the Local Node subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindLocalNodeSubsystem( LocalNodeSubsystem subsystem )
        {
            if ( this.LocalNodeSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.LocalNodeSubsystem );
                this.LocalNodeSubsystem = null;
            }

            this.LocalNodeSubsystem = subsystem;
            if ( this.LocalNodeSubsystem is object )
            {
                this.Subsystems.Add( this.LocalNodeSubsystem );
            }
        }

        #endregion

        #region " MULTIMETER "

        /// <summary> Gets or sets the Multimeter Subsystem. </summary>
        /// <value> The Multimeter Subsystem. </value>
        public MultimeterSubsystem MultimeterSubsystem { get; private set; }

        /// <summary> Binds the Multimeter subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindMultimeterSubsystem( MultimeterSubsystem subsystem )
        {
            if ( this.MultimeterSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.MultimeterSubsystem );
                this.MultimeterSubsystem = null;
            }

            this.MultimeterSubsystem = subsystem;
            if ( this.MultimeterSubsystem is object )
            {
                this.Subsystems.Add( this.MultimeterSubsystem );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets or sets the Trigger Subsystem. </summary>
        /// <value> The Trigger Subsystem. </value>
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Binds the Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindTriggerSubsystem( TriggerSubsystem subsystem )
        {
            if ( this.TriggerSubsystem is object )
            {
                _ = this.Subsystems.Remove( this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            this.TriggerSubsystem = subsystem;
            if ( this.TriggerSubsystem is object )
            {
                this.Subsystems.Add( this.TriggerSubsystem );
            }
        }

        #endregion

        #region " SERVICE REQUEST "

        /// <summary> Processes the service request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ProcessServiceRequest()
        {
            // device errors will be read if the error available bit is set upon reading the status byte.
            _ = this.Session.ReadStatusRegister(); // this could have lead to a query interrupted error: Me.ReadEventRegisters()
            if ( this.ServiceRequestAutoRead )
            {
                if ( this.Session.ErrorAvailable )
                {
                }
                else if ( this.Session.MessageAvailable )
                {
                    TimeSpan.FromMilliseconds( 10 ).SpinWait();
                    // result is also stored in the last message received.
                    this.ServiceRequestReading = this.Session.ReadFreeLineTrimEnd();
                    _ = this.Session.ReadStatusRegister();
                }
            }
        }

        #endregion

        #region " MY SETTINGS "

        /// <summary> Opens the settings editor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public static void OpenSettingsEditor()
        {
            Core.WindowsForms.EditConfiguration( $"{typeof( K7510Device )} Settings editor", My.MySettings.Default );
        }

        /// <summary> Applies the settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected override void ApplySettings()
        {
            var settings = My.MySettings.Default;
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceLogLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.TraceShowLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitializeTimeout ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ResetRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.DeviceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InterfaceClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.InitRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ClearRefractoryPeriod ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.SessionMessageNotificationLevel ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadTurnaroundTime ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.ReadDelay ) );
            this.HandlePropertyChanged( settings, nameof( My.MySettings.StatusReadDelay ) );
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender">       Source of the event. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( My.MySettings sender, string propertyName )
        {
            if ( sender is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( My.MySettings.TraceLogLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Logger, sender.TraceLogLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {sender.TraceLogLevel}" );
                        break;
                    }

                case nameof( My.MySettings.TraceShowLevel ):
                    {
                        this.ApplyTalkerTraceLevel( Core.ListenerType.Display, sender.TraceShowLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {sender.TraceShowLevel}" );
                        break;
                    }

                case nameof( My.MySettings.ClearRefractoryPeriod ):
                    {
                        this.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.ClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.DeviceClearRefractoryPeriod ):
                    {
                        this.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.DeviceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InitializeTimeout ):
                    {
                        this.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InitializeTimeout}" );
                        break;
                    }

                case nameof( My.MySettings.InitRefractoryPeriod ):
                    {
                        this.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InitRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.InterfaceClearRefractoryPeriod ):
                    {
                        this.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.InterfaceClearRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.ResetRefractoryPeriod ):
                    {
                        this.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod;
                        _ = this.PublishInfo( $"{propertyName} set to {sender.ResetRefractoryPeriod}" );
                        break;
                    }

                case nameof( My.MySettings.SessionMessageNotificationLevel ):
                    {
                        this.Session.MessageNotificationLevel = ( Pith.NotifySyncLevel ) Conversions.ToInteger( sender.SessionMessageNotificationLevel );
                        _ = this.PublishInfo( $"{propertyName} set to {this.Session.MessageNotificationLevel}" );
                        break;
                    }
            }
        }

        /// <summary> Handles the settings property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleSettingsPropertyChanged( object sender, System.ComponentModel.PropertyChangedEventArgs e )
        {
            if ( this.IsDisposed || sender is null || e is null )
                return;
            string activity = $"handling {nameof( My.MySettings )}.{e.PropertyName} change";
            try
            {
                this.HandlePropertyChanged( sender as My.MySettings, e.PropertyName );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
