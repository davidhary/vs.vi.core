﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K7510 TSP2 VI Tests" )]
[assembly: AssemblyDescription( "K7510 TSP2 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.Tsp2.K7510.Tests" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
