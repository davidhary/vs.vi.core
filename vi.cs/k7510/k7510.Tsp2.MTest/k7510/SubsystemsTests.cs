using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp2.K7510.Device.MSTest
{

    /// <summary> K7510 Subsystems unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k7510tsp" )]
    public class SubsystemsTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( testContext.FullyQualifiedTestClassName );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            Assert.IsTrue( SubsystemsSettings.Get().Exists, $"{typeof( SubsystemsSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " STATUS SUSBSYSTEM "

        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
        /// <param name="resourceInfo">     Information describing the resource. </param>
        /// <param name="subsystemsInfo">   Information describing the subsystems. </param>
        private static void AssertSessionOpenCheckStatusShouldPass( bool readErrorEnabled, ResourceSettings resourceInfo, SubsystemsSettings subsystemsInfo )
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertSessionInitialValuesShouldMatch( device.Session, resourceInfo, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertSessionOpenValuesShouldMatch( device.Session, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceModelShouldMatch( device.StatusSubsystemBase, resourceInfo );
                VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldMatch( device.StatusSubsystemBase, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertTerminationValuesShouldMatch( device.Session, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertLineFrequencyShouldMatch( device.StatusSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertIntegrationPeriodShouldMatch( device.StatusSubsystem, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertSessionDeviceErrorsShouldClear( device, subsystemsInfo );
                if ( readErrorEnabled )
                    VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldRead( device, subsystemsInfo );
                VI.DeviceTests.DeviceManager.AssertOrphanMessagesShouldBeEmpty( device.StatusSubsystemBase );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) session open check status should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionOpenCheckStatusShouldPass()
        {
            AssertSessionOpenCheckStatusShouldPass( false, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        /// <summary>
        /// (Unit Test Method) session open check status device errors should pass.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void SessionOpenCheckStatusDeviceErrorsShouldPass()
        {
            AssertSessionOpenCheckStatusShouldPass( true, ResourceSettings.Get(), SubsystemsSettings.Get() );
        }

        #endregion

        #region " BUFFER SUBSYSTEM TEST "

        /// <summary> Assert buffer subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertBufferSubsystemInfoShouldPass( VI.BufferSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            int actualCapacilty = subsystem.QueryCapacity().GetValueOrDefault( -1 );
            int expectedcapacilty = SubsystemsSettings.Get().BufferCapacity;
            Assert.AreEqual( expectedcapacilty, actualCapacilty, $"Buffer capacity" );
            int actualFirstPointNumber = subsystem.QueryFirstPointNumber().GetValueOrDefault( -1 );
            int expectedFirstPointNumber = SubsystemsSettings.Get().BufferFirstPointNumber;
            Assert.AreEqual( expectedFirstPointNumber, actualFirstPointNumber, $"Buffer First Point Number" );
            int actualLastPointNumber = subsystem.QueryLastPointNumber().GetValueOrDefault( -1 );
            int expectedLastPointNumber = SubsystemsSettings.Get().BufferLastPointNumber;
            Assert.AreEqual( expectedLastPointNumber, actualLastPointNumber, $"Buffer Last Point Number" );
            bool expectedFillOnceEnabled = SubsystemsSettings.Get().BufferFillOnceEnabled;
            bool actualFillOnceEnabled = subsystem.QueryFillOnceEnabled().GetValueOrDefault( !expectedFillOnceEnabled );
            Assert.AreEqual( expectedFillOnceEnabled, actualFillOnceEnabled, $"Initial fill once enabled" );
        }

        /// <summary> (Unit Test Method) buffer subsystem information should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void BufferSubsystemInfoShouldPass()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertBufferSubsystemInfoShouldPass( device.Buffer1Subsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " MULTIMETER SUBSYSTEM INITIAL VALUES TEST "

        /// <summary> Assert subsystem initial value should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private static void AssertSubsystemInitialValueShouldMatch( MultimeterSubsystemBase subsystem )
        {
            double expectedPowerLineCycles = SubsystemsSettings.Get().InitialPowerLineCycles;
            var actualPowerLineCycles = subsystem.QueryPowerLineCycles();
            Assert.IsTrue( actualPowerLineCycles.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )}" );
            Assert.AreEqual( expectedPowerLineCycles, actualPowerLineCycles.Value, SubsystemsSettings.Get().LineFrequency / TimeSpan.TicksPerSecond, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )}" );
            bool expectedBoolean = SubsystemsSettings.Get().InitialAutoRangeEnabled;
            var actualBoolean = subsystem.QueryAutoRangeEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoRangeEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoRangeEnabled )}" );
            var expectedFunctionMode = SubsystemsSettings.Get().InitialMultimeterFunction;
            var actualFunctionMode = subsystem.QueryFunctionMode();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed reading {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FunctionMode )}" );
            Assert.AreEqual( expectedFunctionMode, actualFunctionMode.Value, $"Failed initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.FunctionMode )}" );
            var expectedInputImpedanceMode = SubsystemsSettings.Get().InitialInputImpedanceMode;
            var actualInputImpedanceMode = subsystem.QueryInputImpedanceMode();
            Assert.IsTrue( actualInputImpedanceMode.HasValue, $"Failed reading {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.InputImpedanceMode )}" );
            Assert.AreEqual( expectedInputImpedanceMode, actualInputImpedanceMode.Value, $"Failed initial {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.InputImpedanceMode )}" );
            expectedBoolean = SubsystemsSettings.Get().InitialFilterEnabled;
            actualBoolean = subsystem.QueryFilterEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterEnabled )}" );
            expectedBoolean = SubsystemsSettings.Get().InitialMovingAverageFilterEnabled;
            actualBoolean = subsystem.QueryMovingAverageFilterEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.MovingAverageFilterEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.MovingAverageFilterEnabled )}" );
            double expectedFilterWindow = SubsystemsSettings.Get().InitialFilterWindow;

            var actualFilterWindow = subsystem.QueryFilterWindow();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterWindow )}" );
            Assert.AreEqual( expectedFilterWindow, actualFilterWindow.Value, 0.1d * expectedFilterWindow, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterWindow )}" );

            int expectedFilterCount = SubsystemsSettings.Get().InitialFilterCount;
            var actualFilterCount = subsystem.QueryFilterCount();
            Assert.IsTrue( actualFilterCount.HasValue, $"Failed reading {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterCount )}" );
            Assert.AreEqual( expectedFilterCount, actualFilterCount.Value, $"Failed initial {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterCount )}" );
        }

        /// <summary> (Unit Test Method) multimeter subsystem initial value should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MultimeterSubsystemInitialValueShouldMatch()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertSubsystemInitialValueShouldMatch( device.MultimeterSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " MULTIMETER SUBSYSTEM: TEST VOLTAGE MEASUREMENT "

        /// <summary> Assert input impedance mode should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertInputImpedanceModeShouldApply( MultimeterSubsystemBase subsystem, InputImpedanceModes value )
        {
            var expectedInputImpedanceMode = value;
            var actualInputImpedanceMode = subsystem.ApplyInputImpedanceMode( expectedInputImpedanceMode );
            Assert.IsTrue( actualInputImpedanceMode.HasValue, $"Failed applying {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.InputImpedanceMode )}" );
            Assert.AreEqual( expectedInputImpedanceMode, actualInputImpedanceMode.Value, $"Failed applying {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.InputImpedanceMode )}" );
        }

        /// <summary> Assert automatic zero once should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private static void AssertAutoZeroOnceShouldApply( MultimeterSubsystemBase subsystem )
        {
            subsystem.AutoZeroOnce();
            bool expectedBoolean = false;
            var actualBoolean = subsystem.QueryAutoZeroEnabled();
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroOnce )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroOnce )} {nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )} still enabled" );
        }

        /// <summary> Assert automatic zero enabled should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertAutoZeroEnabledShouldApply( MultimeterSubsystemBase subsystem, bool value )
        {
            bool expectedBoolean = value;
            var actualBoolean = subsystem.ApplyAutoZeroEnabled( value );
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.AutoZeroEnabled )}" );
        }

        /// <summary> Assert filter enabled should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertFilterEnabledShouldApply( MultimeterSubsystemBase subsystem, bool value )
        {
            bool expectedBoolean = value;
            var actualBoolean = subsystem.ApplyFilterEnabled( value );
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterEnabled )}" );
        }

        /// <summary> Assert moving average filter enabled should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertMovingAverageFilterEnabledShouldApply( MultimeterSubsystemBase subsystem, bool value )
        {
            bool expectedBoolean = value;
            var actualBoolean = subsystem.ApplyMovingAverageFilterEnabled( value );
            Assert.IsTrue( actualBoolean.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.MovingAverageFilterEnabled )}" );
            Assert.AreEqual( expectedBoolean, actualBoolean.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.MovingAverageFilterEnabled )}" );
        }

        /// <summary> Assert filter count should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertFilterCountShouldApply( MultimeterSubsystemBase subsystem, int value )
        {
            int expectedInteger = value;
            var actualInteger = subsystem.ApplyFilterCount( value );
            Assert.IsTrue( actualInteger.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterCount )}" );
            Assert.AreEqual( expectedInteger, actualInteger.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterCount )}" );
        }

        /// <summary> Assert filter window should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertFilterWindowShouldApply( MultimeterSubsystemBase subsystem, double value )
        {
            double expectedDouble = value;
            var actualDouble = subsystem.ApplyFilterWindow( value );
            Assert.IsTrue( actualDouble.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterWindow )}" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.FilterWindow )}" );
        }

        /// <summary> Assert power line cycles should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertPowerLineCyclesShouldApply( MultimeterSubsystemBase subsystem, double value )
        {
            double expectedDouble = value;
            var actualDouble = subsystem.ApplyPowerLineCycles( value );
            Assert.IsTrue( actualDouble.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )}" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PowerLineCycles )}" );
        }

        /// <summary> Assert range should apply. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        /// <param name="value">     The value. </param>
        private static void AssertRangeShouldApply( MultimeterSubsystemBase subsystem, double value )
        {
            double expectedDouble = value;
            var actualDouble = subsystem.ApplyRange( value );
            Assert.IsTrue( actualDouble.HasValue, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )}" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Failed applying {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.Range )}" );
        }

        /// <summary> Assert measure should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private static void AssertMeasureShouldPass( MultimeterSubsystemBase subsystem )
        {
            var expectedTimeSpan = subsystem.EstimateMeasurementTime();
            int timeout = Math.Max( 10000, 4 * ( int ) Math.Round( expectedTimeSpan.TotalMilliseconds ) );
            subsystem.Session.StoreCommunicationTimeout( TimeSpan.FromMilliseconds( timeout ) );
            var sw = Stopwatch.StartNew();
            var value = subsystem.MeasurePrimaryReading();
            var actualTimespan = sw.Elapsed;
            Assert.IsTrue( value.HasValue, $"Failed reading {typeof( MultimeterSubsystemBase )}.{nameof( MultimeterSubsystemBase.PrimaryReadingValue )}" );
            Assert.IsTrue( value.Value > -0.001d, $"Failed reading positive {typeof( VI.MultimeterSubsystemBase )}.{nameof( VI.MultimeterSubsystemBase.PrimaryReadingValue )}" );
            Assert.IsTrue( actualTimespan >= expectedTimeSpan, $"Reading too short; expected {expectedTimeSpan} actual {actualTimespan}" );
            var twiceInterval = expectedTimeSpan.Add( expectedTimeSpan );
            Assert.IsTrue( actualTimespan < twiceInterval, $"Reading too long; expected {twiceInterval} actual {actualTimespan}" );
            subsystem.Session.RestoreCommunicationTimeout();
        }

        /// <summary>
        /// (Unit Test Method) multimeter subsystem voltage measurement should pass.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MultimeterSubsystemVoltageMeasurementShouldPass()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                double powerLineCycles = 5d;
                int FilterCount = 20;
                double window = 0.095d;
                double range = 0.1d;
                AssertPowerLineCyclesShouldApply( device.MultimeterSubsystem, powerLineCycles );
                AssertRangeShouldApply( device.MultimeterSubsystem, range );
                AssertAutoZeroEnabledShouldApply( device.MultimeterSubsystem, false );
                AssertAutoZeroOnceShouldApply( device.MultimeterSubsystem );
                AssertInputImpedanceModeShouldApply( device.MultimeterSubsystem, SubsystemsSettings.Get().TestInputImpedanceMode );
                AssertMovingAverageFilterEnabledShouldApply( device.MultimeterSubsystem, false ); // use repeat filter.
                AssertFilterCountShouldApply( device.MultimeterSubsystem, FilterCount );
                AssertFilterWindowShouldApply( device.MultimeterSubsystem, window );
                AssertFilterEnabledShouldApply( device.MultimeterSubsystem, true );
                AssertMeasureShouldPass( device.MultimeterSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

        #region " DIGITAL INPUT OUTPUT SUBSYSTEM TEST "

        /// <summary> Assert digital input output subsystem information should match. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertDigitalInputOutputSubsystemInfoShouldMatch( DigitalInputOutputSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            int actualLineCount = subsystem.DigitalLines.Count;
            int expectedLineCount = SubsystemsSettings.Get().DigitalLineCount;
            Assert.AreEqual( expectedLineCount, actualLineCount, $"Digital Input Output line count" );
            var expectedMode = DigitalLineMode.DigitalInput;
            var actualMode = subsystem.QueryDigitalLineMode( SubsystemsSettings.Get().DigitalInputLineNumber );
            Assert.IsTrue( actualMode.HasValue, $"Digital line mode {SubsystemsSettings.Get().DigitalInputLineNumber} has mode value" );
            Assert.AreEqual( expectedMode, actualMode.Value, $"Digital {SubsystemsSettings.Get().DigitalInputLineNumber} line mode" );
        }

        /// <summary>
        /// (Unit Test Method) digital input output subsystem information should match.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DigitalInputOutputSubsystemInfoShouldMatch()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertDigitalInputOutputSubsystemInfoShouldMatch( device.DigitalInputOutputSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> Assert digital input output write read should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="subsystem"> The subsystem. </param>
        public static void AssertDigitalInputOutputWriteReadShouldPass( DigitalInputOutputSubsystemBase subsystem )
        {
            if ( subsystem is null )
                throw new ArgumentNullException( nameof( subsystem ) );
            var expectedMode = DigitalLineMode.DigitalOutput;
            var actualMode = subsystem.ApplyDigitalLineMode( SubsystemsSettings.Get().DigitalOutputLineNumber, expectedMode );
            Assert.IsTrue( actualMode.HasValue, $"Digital line mode {SubsystemsSettings.Get().DigitalOutputLineNumber} has mode value" );
            Assert.AreEqual( expectedMode, actualMode.Value, $"Digital {SubsystemsSettings.Get().DigitalOutputLineNumber} Line mode" );
            var expectedOutputValue = DigitalLineState.High;
            _ = subsystem.WriteDigitalLineState( SubsystemsSettings.Get().DigitalOutputLineNumber, expectedOutputValue );
            var actualOutputValue = subsystem.QueryDigitalLineState( SubsystemsSettings.Get().DigitalInputLineNumber );
            Assert.IsTrue( actualOutputValue.HasValue, $"Digital input {SubsystemsSettings.Get().DigitalInputLineNumber} state has value" );
            Assert.AreEqual( expectedOutputValue, actualOutputValue.Value, $"Digital input #{SubsystemsSettings.Get().DigitalInputLineNumber} equals digital output #{SubsystemsSettings.Get().DigitalOutputLineNumber} " );
            expectedOutputValue = DigitalLineState.Low;
            _ = subsystem.WriteDigitalLineState( SubsystemsSettings.Get().DigitalOutputLineNumber, expectedOutputValue );
            actualOutputValue = subsystem.QueryDigitalLineState( SubsystemsSettings.Get().DigitalInputLineNumber );
            Assert.IsTrue( actualOutputValue.HasValue, $"Digital output {SubsystemsSettings.Get().DigitalInputLineNumber} state has value" );
            Assert.AreEqual( expectedOutputValue, actualOutputValue.Value, $"Digital input #{SubsystemsSettings.Get().DigitalInputLineNumber} equals digital output #{SubsystemsSettings.Get().DigitalOutputLineNumber} " );
        }

        /// <summary> (Unit Test Method) digital input output write read should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DigitalInputOutputWriteReadShouldPass()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                AssertDigitalInputOutputWriteReadShouldPass( device.DigitalInputOutputSubsystem );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
