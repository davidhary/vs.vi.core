using System;
using System.Diagnostics;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp2.K7510.Device.MSTest
{


    /// <summary> K7510 Device unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k7510tsp" )]
    public class DeviceTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.Write( testContext.FullyQualifiedTestClassName );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " DEVICE TESTS: OPEN, CLOSE, CHECK SUSBSYSTEMS "

        /// <summary> (Unit Test Method) device trace message should be queued. </summary>
        /// <remarks> Checks if the device adds a trace message to a listener. </remarks>
        [TestMethod()]
        public void DeviceTraceMessageShouldBeQueued()
        {
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            string payload = "Device message";
            int traceEventId = 1;
            _ = device.Talker.Publish( TraceEventType.Warning, traceEventId, payload );

            // with the new talker, the device identifies the following libraries: 
            // 0x0100 core agnostic; 0x01006 vi device and 0x010xx Keithley xxx
            // so these test looks for the first warning
            int fetchNumber = 0;
            Core.TraceMessage traceMessage = null;
            while ( TestInfo.TraceMessagesQueueListener.Any )
            {
                traceMessage = TestInfo.TraceMessagesQueueListener.TryDequeue();
                fetchNumber += 1;
                if ( traceMessage.EventType <= TraceEventType.Warning )
                {
                    // we expect a single such message
                    break;
                }
            }

            if ( traceMessage is null )
                Assert.Fail( $"{payload} failed to trace fetch number {fetchNumber}" );
            Assert.AreEqual( traceEventId, traceMessage.Id, $"{payload} trace event id mismatch fetch #{fetchNumber} message {traceMessage.Details}" );
            Assert.AreEqual( 0, TestInfo.TraceMessagesQueueListener.Count, $"{payload} expected no more messages after fetch #{fetchNumber} message {traceMessage.Details}" );
            traceEventId = 1;
            payload = "Status subsystem message";
            _ = device.Talker.Publish( TraceEventType.Warning, traceEventId, payload );
            traceMessage = TestInfo.TraceMessagesQueueListener.TryDequeue();
            if ( traceMessage is null )
                Assert.Fail( $"{payload} failed to trace" );
            Assert.AreEqual( traceEventId, traceMessage.Id, $"{payload} trace event id mismatch" );
        }

        /// <summary> (Unit Test Method) device should open without device errors. </summary>
        /// <remarks> Tests opening and closing a VISA session. </remarks>
        [TestMethod()]
        public void DeviceShouldOpenWithoutDeviceErrors()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K7510.K7510Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                VI.DeviceTests.DeviceManager.AssertSessionInitialValuesShouldMatch( device.Session, ResourceSettings.Get(), SubsystemsSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertSessionOpenValuesShouldMatch( device.Session, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
