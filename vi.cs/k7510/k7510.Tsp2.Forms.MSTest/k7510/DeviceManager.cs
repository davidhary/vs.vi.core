namespace isr.VI.Tsp2.K7510.Forms.MSTest
{

    /// <summary> Static class for managing the common functions. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    internal sealed partial class DeviceManager
    {

        #region " CONSTRUCTION "

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private DeviceManager() : base()
        {
        }

        #endregion

    }
}
