using System;
using System.Threading.Tasks;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.Tsp2.K7510.Forms.MSTest
{

    /// <summary> K7510 Visa View unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k7510tsp" )]
    public class VisaViewTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.Write( testContext.FullyQualifiedTestClassName );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " VISA VIEW: DEVICE OPEN TEST "

        /// <summary>   (Unit Test Method) resource name should be selected. </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ResourceNameShouldBeSelected()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaView( device );
                FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected( TestInfo, view, ResourceSettings.Get() );
            }

            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaTreeView( device );
                FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected( TestInfo, view, ResourceSettings.Get() );
            }
        }

        /// <summary>   (Unit Test Method) visa view trace message should emit. </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaViewTraceMessageShouldEmit()
        {
            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaView( device );
                FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit( view, TestInfo.TraceMessagesQueueListener );
            }

            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaTreeView( device );
                FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit( view, TestInfo.TraceMessagesQueueListener );
            }
        }

        /// <summary>
        /// (Unit Test Method) visa view session should open and resource name match.
        /// </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaViewSessionShouldOpenAndResourceNameMatch()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaView( device );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen( TestInfo, 0, view, ResourceSettings.Get() );
                    FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch( view.VisaSessionBase.Session, ResourceSettings.Get() );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 0, view );
                }
            }

            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using var view = new Facade.VisaTreeView( device );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen( TestInfo, 0, view, ResourceSettings.Get() );
                    FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch( view.VisaSessionBase.Session, ResourceSettings.Get() );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 0, view );
                }
            }
        }

        /// <summary>   (Unit Test Method) visa view session should open and close twice. </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaViewSessionShouldOpenAndCloseTwice()
        {
            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using ( Facade.IVisaView view = new Facade.VisaView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
                }

                using ( Facade.IVisaView view = new Facade.VisaView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 2, view, ResourceSettings.Get() );
                }
            }

            using ( var device = K7510.K7510Device.Create() )
            {
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                using ( var view = new Facade.VisaTreeView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
                }

                using ( var view = new Facade.VisaTreeView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 2, view, ResourceSettings.Get() );
                }

                using ( var view = new Facade.VisaTreeView( device ) )
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 3, view, ResourceSettings.Get() );
                    Task.Delay( TimeSpan.FromMilliseconds( 100d ) ).Wait();
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 4, view, ResourceSettings.Get() );
                }
            }
        }

        #endregion

        #region " VISA VIEW: ASSIGNED DEVICE TESTS "

        /// <summary>   (Unit Test Method) visa view session should open and close. </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaViewSessionShouldOpenAndClose()
        {
            using ( var view = new K7510.Forms.K7510View() )
            {
                using var device = K7510.K7510Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                view.AssignDevice( device );
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
            }

            using ( var view = new K7510.Forms.K7510TreeView() )
            {
                using var device = K7510.K7510Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                view.AssignDevice( device );
                FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose( TestInfo, 1, view, ResourceSettings.Get() );
            }
        }

        /// <summary>   (Unit Test Method) visa session base should open and then close. </summary>
        /// <remarks>   David, 2020-10-12. </remarks>
        [TestMethod()]
        public void VisaSessionBaseShouldOpenAndThenClose()
        {
            using ( var view = new K7510.Forms.K7510View() )
            {
                using var device = K7510.K7510Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, 1, device, ResourceSettings.Get() );
                    view.AssignDevice( device );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 1, view );
                }
            }

            using ( var view = new K7510.Forms.K7510TreeView() )
            {
                using var device = K7510.K7510Device.Create();
                device.AddListener( TestInfo.TraceMessagesQueueListener );
                try
                {
                    FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen( TestInfo, 1, device, ResourceSettings.Get() );
                    view.AssignDevice( device );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose( TestInfo, 1, view );
                }
            }
        }

        #endregion

    }
}
