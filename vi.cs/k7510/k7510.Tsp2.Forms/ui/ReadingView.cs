using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.WinForms.ComboBoxEnumExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.DataGridViewExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp2.K7510.Forms
{

    /// <summary> A reading view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._BufferSizeNumeric.NumericUpDownControl.CausesValidation = true;
            this._BufferSizeNumeric.NumericUpDownControl.Minimum = 0m;
            this._BufferSizeNumeric.NumericUpDownControl.Maximum = 27510000m;
            this.__BufferDataGridView.Name = "_BufferDataGridView";
            this.__ReadBufferButton.Name = "_ReadBufferButton";
            this.__ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton";
            this.__BufferSizeNumeric.Name = "_BufferSizeNumeric";
            this.__ReadButton.Name = "_ReadButton";
            this.__ReadingComboBox.Name = "_ReadingComboBox";
            this.__AbortButton.Name = "_AbortButton";
            this.__InitiateTriggerPlanMenuItem.Name = "_InitiateTriggerPlanMenuItem";
            this.__AbortStartTriggerPlanMenuItem.Name = "_AbortStartTriggerPlanMenuItem";
            this.__MonitorActiveTriggerPlanMenuItem.Name = "_MonitorActiveTriggerPlanMenuItem";
            this.__InitMonitorReadRepeatMenuItem.Name = "_InitMonitorReadRepeatMenuItem";
            this.__StreamBufferMenuItem.Name = "_StreamBufferMenuItem";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K7510Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K7510Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindBufferSubsystem( value );
            this.BindMultimeterSubsystem( value );
            this.BindTriggerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K7510Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " BUFFER SUBSYSTEM "

        /// <summary> Gets the Buffer subsystem. </summary>
        /// <value> The Buffer subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public BufferSubsystem BufferSubsystem { get; private set; }

        /// <summary> Bind Buffer subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindBufferSubsystem( K7510Device device )
        {
            if ( this.BufferSubsystem is object )
            {
                this.BindSubsystem( false, this.BufferSubsystem );
                this.BufferSubsystem = null;
            }

            if ( device is object )
            {
                this.BufferSubsystem = device.Buffer1Subsystem;
                this.BindSubsystem( true, this.BufferSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, BufferSubsystem subsystem )
        {
            if ( add )
                _ = this._BufferDataGridView.Bind( subsystem.BufferReadingsBindingList, true );
            var binding = this.AddRemoveBinding( this._BufferSizeNumeric, add, nameof( NumericUpDown.Value ), subsystem, nameof( BufferSubsystemBase.Capacity ) );
            // has to apply the value.
            binding.DataSourceUpdateMode = DataSourceUpdateMode.Never;
            _ = this.AddRemoveBinding( this._BufferCountLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.ActualPointCount ) );
            _ = this.AddRemoveBinding( this._FirstPointNumberLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.FirstPointNumber ) );
            _ = this.AddRemoveBinding( this._LastPointNumberLabel, add, nameof( Control.Text ), subsystem, nameof( BufferSubsystemBase.LastPointNumber ) );
        }

        /// <summary> Buffer size text box validating. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Cancel event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BufferSizeNumeric_Validating( object sender, CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                if ( this.Device.IsDeviceOpen )
                {
                    int value = ( int ) Math.Round( this._BufferSizeNumeric.Value );
                    activity = $"{this.Device.ResourceNameCaption} setting {typeof( BufferSubsystem )}.{nameof( this.BufferSubsystem.Capacity )} to {value}";
                    _ = this.PublishVerbose( activity );
                    // overrides and set to the minimum size: Me._BufferSizeNumeric.Value = Me._BufferSizeNumeric.NumericUpDownControl.Minimum
                    _ = this.Device.Buffer1Subsystem.ApplyCapacity( value );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " MUTLIMETER "

        /// <summary> Gets the Multimeter subsystem. </summary>
        /// <value> The Multimeter subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public MultimeterSubsystem MultimeterSubsystem { get; private set; }

        /// <summary> Bind Multimeter subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindMultimeterSubsystem( K7510Device device )
        {
            if ( this.MultimeterSubsystem is object )
            {
                this.BindSubsystem( false, this.MultimeterSubsystem );
                this.MultimeterSubsystem = null;
            }

            if ( device is object )
            {
                this.MultimeterSubsystem = device.MultimeterSubsystem;
                this.BindSubsystem( true, this.MultimeterSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MultimeterSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.MultimeterSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K7510.MultimeterSubsystem.ReadingAmounts ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.MultimeterSubsystemPropertyChanged;
            }
        }

        /// <summary> Handles the Multimeter subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MultimeterSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                // TO_DO: Bind readings. 
                case nameof( K7510.MultimeterSubsystem.ReadingAmounts ):
                    {
                        try
                        {
                            this.InitializingComponents = true;
                            if ( this._ReadingComboBox.ComboBox.DataSource is null )
                            {
                                this._ReadingComboBox.ComboBox.ListEnumDescriptions( subsystem.ReadingAmounts.Elements, ReadingElementTypes.Units );
                            }
                        }
                        catch
                        {
                            throw;
                        }
                        finally
                        {
                            this.InitializingComponents = false;
                        }

                        break;
                    }
            }
        }

        /// <summary> Multimeter subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MultimeterSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                if ( this.InvokeRequired )
                {
                    activity = $"invoking {nameof( this.MultimeterSubsystem )}.{e.PropertyName} change";
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.MultimeterSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    activity = $"handling {nameof( this.MultimeterSubsystem )}.{e.PropertyName} change";
                    this.HandlePropertyChanged( sender as MultimeterSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( K7510Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K7510.TriggerSubsystem.TriggerState ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K7510.TriggerSubsystem.TriggerState ):
                    {
                        // TO_DO: Bind trigger state label caption to the Subsystem reading label in the display view.
                        // TO_DO: Move functionality (e.g., handler trigger plan state change) from the user interface to the subsystem
                        this._TriggerStateLabel.Visible = subsystem.TriggerState.HasValue;
                        if ( subsystem.TriggerState.HasValue )
                        {
                            this._TriggerStateLabel.Text = subsystem.TriggerState.Value.ToString();
                            if ( this.TriggerPlanStateChangeHandlerEnabled )
                            {
                                this.HandleTriggerPlanStateChange( subsystem.TriggerState.Value );
                            }

                            if ( this.BufferStreamingHandlerEnabled && !subsystem.IsTriggerStateActive() && this._StreamBufferMenuItem.Checked )
                            {
                                this._StreamBufferMenuItem.Checked = false;
                            }
                        }

                        break;
                    }
                    // ?? this causes a cross thread exception. 
                    // Me._TriggerStateLabel.Invalidate()
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " STREAM BUFFER "

        /// <summary> Gets the buffer streaming handler enabled. </summary>
        /// <value> The buffer streaming handler enabled. </value>
        private bool BufferStreamingHandlerEnabled { get; set; }

        /// <summary> Stream buffer menu item check state changed. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StreamBufferMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this._StreamBufferMenuItem.Checked )
                {
                    this.BufferStreamingHandlerEnabled = false;
                    activity = $"{this.Device.ResourceNameCaption} start buffer streaming";
                    _ = this.PublishInfo( $"{activity};. " );
                    this._BufferDataGridView.DataSource = null;
                    this.Device.TriggerSubsystem.Initiate();
                    Core.ApplianceBase.DoEvents();
                    this.Device.Buffer1Subsystem.StartBufferStream( this.Device.TriggerSubsystem, TimeSpan.FromMilliseconds( 5d ) );
                    this.BufferStreamingHandlerEnabled = true;
                }
                else
                {
                    this.BufferStreamingHandlerEnabled = false;
                    activity = $"{this.Device.ResourceNameCaption} Aborting trigger plan to stop buffer streaming";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.AbortTriggerPlan( sender );
                    _ = this.Device.TriggerSubsystem.QueryTriggerState();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: READING "

        #region " READ "

        /// <summary> Selects a new reading to display. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.ReadingElementTypes. </returns>
        internal ReadingElementTypes SelectReading( ReadingElementTypes value )
        {
            if ( this.Device.IsDeviceOpen && value != ReadingElementTypes.None && value != this.SelectedReadingType )
            {
                _ = this._ReadingComboBox.ComboBox.SelectItem( value.ValueDescriptionPair() );
            }

            return this.SelectedReadingType;
        }

        /// <summary> Gets the type of the selected reading. </summary>
        /// <value> The type of the selected reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private ReadingElementTypes SelectedReadingType => ( ReadingElementTypes ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._ReadingComboBox.SelectedItem).Key );

        /// <summary> Reading combo box selected value changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadingComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} selecting a reading to display";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.Device.MultimeterSubsystem.SelectActiveReading( this.SelectedReadingType );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishVerbose( $"{activity};. " );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by _ReadButton for click events. Query the Device for a reading.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} querying terminal mode";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.StartElapsedStopwatch();
                _ = this.Device.MultimeterSubsystem.QueryFrontTerminalsSelected();
                this.Device.MultimeterSubsystem.StopElapsedStopwatch();
                activity = $"{this.Device.ResourceNameCaption} measuring";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.StartElapsedStopwatch();
                _ = this.Device.MultimeterSubsystem.MeasurePrimaryReading();
                this.Device.MultimeterSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " EVENTS "

        /// <summary> Toggles auto trigger. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/>
        ///                                             instance of this
        ///                                             <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AutoInitiateMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} toggling re-trigger mode";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishVerbose( $"{activity};. " );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Gets or sets the trace readings. </summary>
        /// <value> The trace readings. </value>
        private BufferReadingBindingList TraceReadings { get; set; } = new BufferReadingBindingList();

        #region " INITIATE AND WAIT "

        /// <summary> Values that represent trigger plan states. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private enum TriggerPlanState
        {

            /// <summary> An enum constant representing the none option. </summary>
            None,

            /// <summary> An enum constant representing the started option. </summary>
            Started,

            /// <summary> An enum constant representing the completed option. </summary>
            Completed
        }

        /// <summary> Gets or sets the trigger plan state change handler enabled. </summary>
        /// <value> The trigger plan state change handler enabled. </value>
        private bool TriggerPlanStateChangeHandlerEnabled { get; set; }

        /// <summary> Gets or sets the state of the local trigger plan. </summary>
        /// <value> The local trigger plan state. </value>
        private TriggerPlanState LocalTriggerPlanState { get; set; }

        /// <summary> Handles the trigger plan state change described by triggerState. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="triggerState"> State of the trigger. </param>
        private void HandleTriggerPlanStateChange( TriggerState triggerState )
        {
            if ( triggerState == TriggerState.Running || triggerState == TriggerState.Waiting )
            {
                this.LocalTriggerPlanState = TriggerPlanState.Started;
            }
            else if ( triggerState == TriggerState.Idle && this.LocalTriggerPlanState == TriggerPlanState.Started )
            {
                this.LocalTriggerPlanState = TriggerPlanState.Completed;
                this.TryReadBuffer();
                if ( this._RepeatMenuItem.Checked )
                {
                    this.InitiateMonitorTriggerPlan( true );
                }
            }
            else
            {
                this.LocalTriggerPlanState = TriggerPlanState.None;
            }
        }

        /// <summary> Try read buffer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TryReadBuffer()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading";
            try
            {
                this.ReadBuffer();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Reads the buffer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ReadBuffer()
        {
            string activity = $"{this.Device.ResourceNameCaption} fetching buffer count";
            _ = this.PublishVerbose( $"{activity};. " );
            // this assume buffer is cleared upon each new cycle
            int newBufferCount = this.Device.Buffer1Subsystem.QueryActualPointCount().GetValueOrDefault( 0 );
            activity = $"buffer count {newBufferCount}";
            _ = this.PublishVerbose( $"{activity};. " );
            if ( newBufferCount > 0 )
            {
                activity = $"{this.Device.ResourceNameCaption} fetching buffered readings";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Buffer1Subsystem.StartElapsedStopwatch();
                var values = this.Device.Buffer1Subsystem.QueryBufferReadings();
                this.TraceReadings.Add( values );
                this.Device.Buffer1Subsystem.StopElapsedStopwatch();
            }

            Core.ApplianceBase.DoEvents();
        }

        /// <summary> Initiate monitor trigger plan. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="stateChangeHandlingEnabled"> True to enable, false to disable the state change
        ///                                           handling. </param>
        private void InitiateMonitorTriggerPlan( bool stateChangeHandlingEnabled )
        {
            string activity = $"{this.Device.ResourceNameCaption} Initiating trigger plan and monitor";
            _ = this.PublishVerbose( $"{activity};. " );
            this.Device.TriggerSubsystem.Initiate();
            Core.ApplianceBase.DoEvents();
            _ = this._BufferDataGridView.Bind( this.TraceReadings, true );
            this.TriggerPlanStateChangeHandlerEnabled = stateChangeHandlingEnabled;
            _ = this.Device.TriggerSubsystem.AsyncMonitorTriggerState( TimeSpan.FromMilliseconds( 5d ) );
        }

        /// <summary> Try restart trigger plan. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void TryRestartTriggerPlan()
        {
            string activity = $"{this.Device.ResourceNameCaption} Initiating trigger plan and monitor";
            try
            {
                this.InitiateMonitorTriggerPlan( true );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        /// <summary> Monitor active trigger plan menu item click. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MonitorActiveTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} start monitoring trigger plan";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                this.TriggerPlanStateChangeHandlerEnabled = false;
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.Device.TriggerSubsystem.AsyncMonitorTriggerState( TimeSpan.FromMilliseconds( 5d ) );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Initializes the monitor read repeat menu item click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitMonitorReadRepeatMenuItem_Click( object sender, EventArgs e )
        {
            string activity = $"{this.Device.ResourceNameCaption} Initiating trigger plan and monitor";
            try
            {
                this.InitiateMonitorTriggerPlan( true );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
            }
        }

        #endregion

        #region " TRIGGER PLAN EVENT HANDLING "

        /// <summary> Handles the measurement completed request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/>
        ///                                             instance of this
        ///                                             <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleMeasurementCompletedRequest( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Device.ResourceNameCaption} handling SRQ: {this.Device.Session.ServiceRequestStatus:X}";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} kludge: reading buffer count";
                _ = this.PublishVerbose( $"{activity};. " );

                // this assume buffer is cleared upon each new cycle
                int newBufferCount = this.Device.Buffer1Subsystem.QueryActualPointCount().GetValueOrDefault( 0 );
                if ( newBufferCount > 0 )
                {
                    activity = $"{this.Device.ResourceNameCaption} kludge: buffer has data...";
                    _ = this.PublishVerbose( $"{activity};. " );
                    activity = $"{this.Device.ResourceNameCaption} handling measurement available";
                    _ = this.PublishVerbose( $"{activity};. " );
                    if ( false )
                    {
#pragma warning disable CS0162 // Unreachable code detected
                        activity = $"{this.Device.ResourceNameCaption} fetching a single reading";
#pragma warning restore CS0162 // Unreachable code detected
                        _ = this.PublishVerbose( $"{activity};. " );
                        _ = this.Device.MultimeterSubsystem.MeasurePrimaryReading();
                    }
                    else
                    {
                        activity = $"{this.Device.ResourceNameCaption} fetching buffered readings";
                        _ = this.PublishVerbose( $"{activity};. " );
                        var values = this.Device.Buffer1Subsystem.QueryBufferReadings();
                        activity = $"{this.Device.ResourceNameCaption} updating the display";
                        _ = this.PublishVerbose( $"{activity};. " );
                        this.TraceReadings.Add( values );
                        this._BufferDataGridView.Invalidate();
                        this.Device.Buffer1Subsystem.StopElapsedStopwatch();
                    }

                    if ( this._RepeatMenuItem.Checked )
                    {
                        activity = $"{this.Device.ResourceNameCaption} initiating next measurement(s)";
                        _ = this.PublishVerbose( $"{activity};. " );
                        this.Device.Buffer1Subsystem.StartElapsedStopwatch();
                        this.Device.Buffer1Subsystem.ClearBuffer(); // ?@3 removed 17-7-6
                        this.Device.TriggerSubsystem.Initiate();
                    }
                }
                else
                {
                    activity = $"{this.Device.ResourceNameCaption} trigger plan started; buffer empty";
                    _ = this.PublishVerbose( $"{activity};. " );
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( this._ReadBufferButton, Core.Forma.InfoProviderLevel.Error, $"{activity} {ex.Message}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Gets or sets the measurement complete handler added. </summary>
        /// <value> The measurement complete handler added. </value>
        private bool MeasurementCompleteHandlerAdded { get; set; }

        /// <summary> Adds measurement complete event handler. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AddMeasurementCompleteEventHandler()
        {
            string activity = string.Empty;
            if ( !this.MeasurementCompleteHandlerAdded )
            {

                // clear execution state before enabling events
                activity = $"{this.Device.ResourceNameCaption} Clearing execution state";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.ClearExecutionState();
                activity = $"{this.Device.ResourceNameCaption} Enabling session service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Session.EnableServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Adding device service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.AddServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Turning on measurement events";
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.Device.StatusSubsystem.ApplyQuestionableEventEnableBitmask( ( int ) MeasurementEvents.All );
                // 
                // if handling buffer full, use the 4917 event to detect buffer full. 

                activity = $"{this.Device.ResourceNameCaption} Turning on status service request";
                _ = this.PublishVerbose( $"{activity};. " );
                // Me.Device.StatusSubsystem.EnableServiceRequest(VI.Pith.ServiceRequests.MeasurementEvent)
                this.Device.Session.ApplyServiceRequestEnableBitmask( this.Device.Session.DefaultOperationServiceRequestEnableBitmask );
                activity = $"{this.Device.ResourceNameCaption} Adding re-triggering event handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.ServiceRequested += this.HandleMeasurementCompletedRequest;
                this.MeasurementCompleteHandlerAdded = true;
            }
        }

        /// <summary> Removes the measurement complete event handler. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void RemoveMeasurementCompleteEventHandler()
        {
            string activity = string.Empty;
            if ( this.MeasurementCompleteHandlerAdded )
            {
                activity = $"{this.Device.ResourceNameCaption} Disabling session service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Session.DisableServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Removing device service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.RemoveServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Turning off measurement events";
                _ = this.PublishVerbose( $"{activity};. " );
                _ = this.Device.StatusSubsystem.ApplyQuestionableEventEnableBitmask( ( int ) MeasurementEvents.None );
                activity = $"{this.Device.ResourceNameCaption} Turning off status service request";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Session.ApplyServiceRequestEnableBitmask( Pith.ServiceRequests.None );
                activity = $"{this.Device.ResourceNameCaption} Removing re-triggering event handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.ServiceRequested -= this.HandleMeasurementCompletedRequest;
                this.MeasurementCompleteHandlerAdded = false;
            }
        }

        /// <summary>
        /// Event handler. Called by HandleMeasurementEventMenuItem for check state changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleMeasurementEventMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( sender is not ToolStripMenuItem menuItem )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Aborting trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.AbortTriggerPlan( sender );
                if ( menuItem.Checked )
                {
                    activity = $"{this.Device.ResourceNameCaption} Adding measurement completion handler";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.AddMeasurementCompleteEventHandler();
                }
                else
                {
                    activity = $"{this.Device.ResourceNameCaption} Removing measurement completion handler";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.RemoveMeasurementCompleteEventHandler();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " BUFFER HANDLER "

        /// <summary> Handles the buffer full request. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleBufferFullRequest( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} handling SRQ: {this.Device.Session.ServiceRequestStatus:X2}";
                _ = this.PublishVerbose( $"{activity};. " );
                if ( this.Device.Session.OperationCompleted == true )
                {
                    activity = $"{this.Device.ResourceNameCaption} handling operation completed";
                    _ = this.PublishVerbose( $"{activity};. " );

                    // TO_DO: See if can only do a set condition and not read this.
                    int condition = this.Device.StatusSubsystem.QueryOperationEventCondition().GetValueOrDefault( 0 );
                    activity = $"{this.Device.ResourceNameCaption} OPER: {condition:X2}";
                    _ = this.PublishVerbose( $"{activity};. " );

                    // If Bit 0 Is set then the buffer is full
                    if ( (condition & 1 << this.BufferFullOperationConditionBitNumber) != 0 )
                    {
                        activity = $"{this.Device.ResourceNameCaption} handling buffer full";
                        _ = this.PublishVerbose( $"{activity};. " );
                        activity = $"{this.Device.ResourceNameCaption} fetching buffered readings";
                        _ = this.PublishVerbose( $"{activity};. " );
                        var values = this.Device.Buffer1Subsystem.QueryBufferReadings();
                        activity = $"{this.Device.ResourceNameCaption} updating the display";
                        _ = this.PublishVerbose( $"{activity};. " );
                        this.TraceReadings.Add( values );
                        this.Device.Buffer1Subsystem.LastReading = this.TraceReadings.LastReading;
                        this.Device.Buffer1Subsystem.StopElapsedStopwatch();
                        if ( this._RepeatMenuItem.Checked )
                        {
                            activity = $"{this.Device.ResourceNameCaption} initiating next measurement(s)";
                            _ = this.PublishVerbose( $"{activity};. " );
                            this.Device.Buffer1Subsystem.StartElapsedStopwatch();
                            this.Device.Buffer1Subsystem.ClearBuffer(); // ?@# removed 17-7-6
                            this.Device.TriggerSubsystem.Initiate();
                        }
                    }
                    else
                    {
                        activity = $"{this.Device.ResourceNameCaption} handling buffer clear: NOP";
                        _ = this.PublishVerbose( $"{activity};. " );
                    }
                }
                else
                {
                    activity = $"{this.Device.ResourceNameCaption} operation not completed";
                    _ = this.PublishVerbose( $"{activity};. " );
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( this._ReadBufferButton, Core.Forma.InfoProviderLevel.Error, ex.Message );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Gets or sets the Buffer Full handler added. </summary>
        /// <value> The Buffer Full handler added. </value>
        private bool BufferFullHandlerAdded { get; set; }

        /// <summary> Gets or sets the buffer full operation condition bit number. </summary>
        /// <value> The buffer full operation condition bit number. </value>
        private int BufferFullOperationConditionBitNumber { get; set; }

        /// <summary> Adds Buffer Full event handler. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void AddBufferFullEventHandler()
        {
            string activity = string.Empty;
            if ( !this.BufferFullHandlerAdded )
            {

                // clear execution state before enabling events
                activity = $"{this.Device.ResourceNameCaption} Clearing execution state";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.ClearExecutionState();
                activity = $"{this.Device.ResourceNameCaption} Enabling session service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Session.EnableServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Adding device service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.AddServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Turning on Buffer events";
                _ = this.PublishVerbose( $"{activity};. " );
                this.BufferFullOperationConditionBitNumber = 0;
                _ = this.Device.StatusSubsystem.ApplyOperationEventMap( this.BufferFullOperationConditionBitNumber, this.Device.StatusSubsystem.BufferFullEventNumber, this.Device.StatusSubsystem.BufferEmptyEventNumber );
                _ = this.Device.StatusSubsystem.ApplyOperationEventEnableBitmask( 1 << this.BufferFullOperationConditionBitNumber );
                activity = $"{this.Device.ResourceNameCaption} Turning on status service request";
                _ = this.PublishVerbose( $"{activity};. " );
                // Me.Device.StatusSubsystem.EnableServiceRequest(VI.Pith.ServiceRequests.BufferEvent)
                this.Device.Session.ApplyServiceRequestEnableBitmask( this.Device.Session.DefaultOperationServiceRequestEnableBitmask );
                activity = $"{this.Device.ResourceNameCaption} Adding re-triggering event handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.ServiceRequested += this.HandleBufferFullRequest;
                this.BufferFullHandlerAdded = true;
                this.TraceReadings.Clear();
                _ = this._BufferDataGridView.Bind( this.TraceReadings, true );
            }
        }

        /// <summary> Removes the Buffer Full event handler. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void RemoveBufferFullEventHandler()
        {
            string activity = string.Empty;
            if ( this.BufferFullHandlerAdded )
            {
                activity = $"{this.Device.ResourceNameCaption} Disabling session service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Session.DisableServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Removing device service request handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.RemoveServiceRequestEventHandler();
                activity = $"{this.Device.ResourceNameCaption} Turning off Buffer events";
                _ = this.PublishVerbose( $"{activity};. " );
                this.BufferFullOperationConditionBitNumber = 0;
                _ = this.Device.StatusSubsystem.ApplyOperationEventMap( this.BufferFullOperationConditionBitNumber, 0, 0 );
                _ = this.Device.StatusSubsystem.ApplyOperationEventEnableBitmask( 0 );
                activity = $"{this.Device.ResourceNameCaption} Turning off status service request";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Session.ApplyServiceRequestEnableBitmask( Pith.ServiceRequests.None );
                activity = $"{this.Device.ResourceNameCaption} Removing re-triggering event handler";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.ServiceRequested -= this.HandleBufferFullRequest;
                this.BufferFullHandlerAdded = false;
            }
        }

        /// <summary>
        /// Event handler. Called by _HandleBufferEventMenuItem for check state changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleBufferEventMenuItem_CheckStateChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( sender is not ToolStripMenuItem menuItem )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Aborting trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.AbortTriggerPlan( sender );
                if ( menuItem.Checked )
                {
                    activity = $"{this.Device.ResourceNameCaption} Adding Buffer completion handler";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.AddBufferFullEventHandler();
                }
                else
                {
                    activity = $"{this.Device.ResourceNameCaption} Removing Buffer completion handler";
                    _ = this.PublishVerbose( $"{activity};. " );
                    this.RemoveBufferFullEventHandler();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TRIGGER CONTROLS ON THE READING PANEL "

        /// <summary> Aborts trigger plan. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/>
        ///                                             instance of this
        ///                                             <see cref="Control"/> </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortTriggerPlan( object sender )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Aborting trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.TriggerSubsystem.Abort();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Starts trigger plan. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void StartTriggerPlan( object sender )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing buffer and display";
                _ = this.PublishVerbose( $"{activity};. " );
                this.TraceReadings.Clear();
                _ = this._BufferDataGridView.Bind( this.TraceReadings, true );
                this.Device.Buffer1Subsystem.ClearBuffer();
                activity = $"{this.Device.ResourceNameCaption} initiating trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.TriggerSubsystem.Initiate();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Event handler. Called by the Initiate Button for click events. Initiates a reading for
        /// retrieval by way of the service request event.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortStartTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Aborting trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.AbortTriggerPlan( sender );
                activity = $"{this.Device.ResourceNameCaption} Starting trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.StartTriggerPlan( sender );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Initiate trigger plan menu item click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void InitiateTriggerPlanMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} initiating trigger plan";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.TriggerSubsystem.StartElapsedStopwatch();
                this.Device.TriggerSubsystem.Initiate();
                _ = this.Device.TriggerSubsystem.QueryTriggerState();
                this.Device.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Abort button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                       <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void AbortButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} aborting trigger plan";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishVerbose( $"{activity};. " );
                this.AbortTriggerPlan( sender );
                _ = this.Device.TriggerSubsystem.QueryTriggerState();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region " BUFFER "

        /// <summary> Handles the DataError event of the _dataGridView control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
        ///                       event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void BufferDataGridView_DataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            try
            {
                // prevent error reporting when adding a new row or editing a cell
                if ( sender is DataGridView grid )
                {
                    if ( grid.CurrentRow is object && grid.CurrentRow.IsNewRow )
                        return;
                    if ( grid.IsCurrentCellInEditMode )
                        return;
                    if ( grid.IsCurrentRowDirty )
                        return;
                    string activity = $"{this.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString()}";
                    _ = this.PublishVerbose( activity );
                    _ = this.InfoProvider.Annunciate( grid, Core.Forma.InfoProviderLevel.Error, activity );
                }
            }
            catch
            {
            }
        }

        /// <summary> Reads buffer button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadBufferButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading buffer";
                _ = this.PublishVerbose( $"{activity};. " );
                this.Device.Buffer1Subsystem.StartElapsedStopwatch();
                var br = new BufferReadingBindingList();
                _ = this._BufferDataGridView.Bind( br, true );
                br.Add( this.Device.Buffer1Subsystem.QueryBufferReadings() );
                this.Device.Buffer1Subsystem.LastReading = br.LastReading;
                this.Device.Buffer1Subsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the buffer display button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearBufferDisplayButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing buffer display";
                _ = this.PublishVerbose( $"{activity};. " );
                var br = new BufferReadingBindingList();
                _ = this._BufferDataGridView.Bind( br, true );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
