﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.EnumExtensions;
using isr.Core.WinForms.WindowsFormsExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp2.K7510.Forms
{

    /// <summary> A Trigger view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class TriggerView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public TriggerView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._StartTriggerDelayNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._StartTriggerDelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._StartTriggerDelayNumeric.NumericUpDownControl.Maximum = 10m;
            this._StartTriggerDelayNumeric.NumericUpDownControl.Value = 0.02m;
            this._EndTriggerDelayNumeric.NumericUpDownControl.DecimalPlaces = 3;
            this._EndTriggerDelayNumeric.NumericUpDownControl.Minimum = 0m;
            this._EndTriggerDelayNumeric.NumericUpDownControl.Maximum = 10m;
            this._EndTriggerDelayNumeric.NumericUpDownControl.Value = 0.0m;
            this._OpenLeadsBitPatternNumeric.NumericUpDownControl.Minimum = 1m;
            this._OpenLeadsBitPatternNumeric.NumericUpDownControl.Maximum = 63m;
            this._OpenLeadsBitPatternNumeric.NumericUpDownControl.Value = 16m;
            this._PassBitPatternNumeric.NumericUpDownControl.Minimum = 1m;
            this._PassBitPatternNumeric.NumericUpDownControl.Maximum = 63m;
            this._PassBitPatternNumeric.NumericUpDownControl.Value = 32m;
            this._FailLimit1BitPatternNumeric.NumericUpDownControl.Minimum = 1m;
            this._FailLimit1BitPatternNumeric.NumericUpDownControl.Maximum = 63m;
            this._FailLimit1BitPatternNumeric.NumericUpDownControl.Value = 48m;
            this._LowerLimit1Numeric.NumericUpDownControl.Minimum = 0m;
            this._LowerLimit1Numeric.NumericUpDownControl.Maximum = 5000000m;
            this._LowerLimit1Numeric.NumericUpDownControl.DecimalPlaces = 3;
            this._LowerLimit1Numeric.NumericUpDownControl.Value = 9m;
            this._UpperLimit1Numeric.NumericUpDownControl.Minimum = 0m;
            this._UpperLimit1Numeric.NumericUpDownControl.Maximum = 5000000m;
            this._UpperLimit1Numeric.NumericUpDownControl.DecimalPlaces = 3;
            this._UpperLimit1Numeric.NumericUpDownControl.Value = 11m;
            this._TriggerCountNumeric.NumericUpDownControl.Minimum = 0m;
            this._TriggerCountNumeric.NumericUpDownControl.Maximum = 268000000m;
            this._TriggerCountNumeric.NumericUpDownControl.DecimalPlaces = 0;
            this._TriggerCountNumeric.NumericUpDownControl.Value = 10m;
            this.__Limit1DecimalsNumeric.Name = "_Limit1DecimalsNumeric";
            this.__PassBitPatternNumericButton.Name = "_PassBitPatternNumericButton";
            this.__OpenLeadsBitPatternNumericButton.Name = "_OpenLeadsBitPatternNumericButton";
            this.__FailBitPatternNumericButton.Name = "_FailBitPatternNumericButton";
            this.__ReadTriggerStateMenuItem.Name = "_ReadTriggerStateMenuItem";
            this.__LoadGradeBinTriggerModelMenuItem.Name = "_LoadGradeBinTriggerModelMenuItem";
            this.__RunSimpleLoopMenuItem.Name = "_RunSimpleLoopMenuItem";
            this.__ClearTriggerModelMenuItem.Name = "_ClearTriggerModelMenuItem";
            this.__MeterCompleterFirstGradingBinningMenuItem.Name = "_MeterCompleterFirstGradingBinningMenuItem";
        }

        /// <summary> Creates a new <see cref="TriggerView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="TriggerView"/>. </returns>
        public static TriggerView Create()
        {
            TriggerView view = null;
            try
            {
                view = new TriggerView();
                return view;
            }
            catch
            {
                view?.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components?.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K7510Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K7510Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( this.Device.Talker );
            }

            this.BindTriggerSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K7510Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " TRIGGER "

        /// <summary> Gets the Trigger subsystem. </summary>
        /// <value> The Trigger subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public TriggerSubsystem TriggerSubsystem { get; private set; }

        /// <summary> Bind Trigger subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindTriggerSubsystem( K7510Device device )
        {
            if ( this.TriggerSubsystem is object )
            {
                this.BindSubsystem( false, this.TriggerSubsystem );
                this.TriggerSubsystem = null;
            }

            if ( device is object )
            {
                this.TriggerSubsystem = device.TriggerSubsystem;
                this.BindSubsystem( true, this.TriggerSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, TriggerSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.TriggerSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( K7510.TriggerSubsystem.TriggerState ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.TriggerSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Trigger subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( TriggerSubsystem subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            switch ( propertyName ?? "" )
            {
                case nameof( K7510.TriggerSubsystem.TriggerCount ):
                    {
                        if ( subsystem.TriggerCount.HasValue )
                        {
                            this._TriggerCountNumeric.Value = subsystem.TriggerCount.Value;
                        }

                        break;
                    }

                case nameof( K7510.TriggerSubsystem.ContinuousEnabled ):
                    {
                        this._ContinuousTriggerEnabledMenuItem.CheckState = subsystem.ContinuousEnabled.ToCheckState();
                        break;
                    }

                case nameof( K7510.TriggerSubsystem.TriggerSource ):
                    {
                        if ( subsystem.TriggerSource.HasValue && this._TriggerSourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._TriggerSourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair();
                        }

                        break;
                    }

                case nameof( K7510.TriggerSubsystem.SupportedTriggerSources ):
                    {
                        this._TriggerSourceComboBox.ComboBox.ListSupportedTriggerSources( subsystem.SupportedTriggerSources );
                        if ( subsystem.TriggerSource.HasValue && this._TriggerSourceComboBox.ComboBox.Items.Count > 0 )
                        {
                            this._TriggerSourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair();
                        }

                        break;
                    }
            }
        }

        /// <summary> Trigger subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void TriggerSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( this.TriggerSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.TriggerSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as TriggerSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: TRIGGER "

        /// <summary> Fail bit pattern numeric button check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void FailBitPatternNumericButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._FailLimit1BitPatternNumeric.NumericUpDownControl.Hexadecimal = this._FailBitPatternNumericButton.Checked;
            this._FailBitPatternNumericButton.Text = $"Fail {(this._FailBitPatternNumericButton.Checked ? "0x" : "0d")}";
        }

        /// <summary> Pass bit pattern numeric button check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PassBitPatternNumericButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._PassBitPatternNumeric.NumericUpDownControl.Hexadecimal = this._PassBitPatternNumericButton.Checked;
            this._PassBitPatternNumericButton.Text = $"Pass {(this._FailBitPatternNumericButton.Checked ? "0x" : "0d")}";
        }

        /// <summary> Opens leads bit pattern numeric button check state changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OpenLeadsBitPatternNumericButton_CheckStateChanged( object sender, EventArgs e )
        {
            this._OpenLeadsBitPatternNumeric.NumericUpDownControl.Hexadecimal = this._OpenLeadsBitPatternNumericButton.Checked;
            this._OpenLeadsBitPatternNumeric.Text = $"Open {(this._FailBitPatternNumericButton.Checked ? "0x" : "0d")}";
        }

        /// <summary> Limit 1 decimals numeric value changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Limit1DecimalsNumeric_ValueChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            this._LowerLimit1Numeric.NumericUpDownControl.DecimalPlaces = ( int ) Math.Round( this._Limit1DecimalsNumeric.Value );
            this._UpperLimit1Numeric.NumericUpDownControl.DecimalPlaces = ( int ) Math.Round( this._Limit1DecimalsNumeric.Value );
        }

        /// <summary> Gets the selected trigger source. </summary>
        /// <value> The selected trigger source. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private TriggerSources SelectedTriggerSource => ( TriggerSources ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._TriggerSourceComboBox.ComboBox.SelectedItem).Key );

        /// <summary> Prepare grade binning model. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void PrepareGradeBinningModel()
        {
            _ = this.Device.MultimeterSubsystem.ApplyFunctionMode( MultimeterFunctionModes.ResistanceFourWire );
            if ( this._LowerLimit1Numeric.Value <= 100m )
            {
                _ = this.Device.MultimeterSubsystem.ApplyFilterEnabled( true );
                _ = this.Device.MultimeterSubsystem.ApplyFilterCount( 10 );
                // use repeat filter
                _ = this.Device.MultimeterSubsystem.ApplyMovingAverageFilterEnabled( false );
                _ = this.Device.MultimeterSubsystem.ApplyFilterWindow( 0.1d );
            }
            else
            {
                _ = this.Device.MultimeterSubsystem.ApplyFilterEnabled( false );
            }

            _ = this.Device.MultimeterSubsystem.ApplyLimit1AutoClear( true );
            _ = this.Device.MultimeterSubsystem.ApplyLimit1Enabled( true );
            _ = this.Device.MultimeterSubsystem.ApplyLimit1LowerLevel( ( double ) this._LowerLimit1Numeric.Value );
            _ = this.Device.MultimeterSubsystem.ApplyLimit1UpperLevel( ( double ) this._UpperLimit1Numeric.Value );

            // set limits for open circuit to 10 times the range limit
            _ = this.Device.MultimeterSubsystem.ApplyLimit2AutoClear( true );
            _ = this.Device.MultimeterSubsystem.ApplyLimit2Enabled( true );
            _ = this.Device.MultimeterSubsystem.ApplyLimit2LowerLevel( ( double ) (-10 * this._UpperLimit1Numeric.Value) );
            _ = this.Device.MultimeterSubsystem.ApplyLimit2UpperLevel( ( double ) (10m * this._UpperLimit1Numeric.Value) );

            // enable open detection
            _ = this.Device.MultimeterSubsystem.ApplyOpenDetectorEnabled( true );
            int count = ( int ) Math.Round( this._TriggerCountNumeric.Value );
            // the buffer must have at least 10 data points
            _ = this.Device.Buffer1Subsystem.ApplyCapacity( Math.Max( 10, count ) );

            // clear the buffer 
            this.Device.Buffer1Subsystem.ClearBuffer();
        }

        /// <summary> Loads grade bin trigger model button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadGradeBinTriggerModelButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.Device.IsDeviceOpen )
                {
                    activity = $"{this.Device.ResourceNameCaption} loading grade binning trigger model";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.PrepareGradeBinningModel();
                    this.Device.TriggerSubsystem.ApplyGradeBinning( ( int ) Math.Round( this._TriggerCountNumeric.Value ), TimeSpan.FromSeconds( ( double ) this._StartTriggerDelayNumeric.Value ), ( int ) Math.Round( this._FailLimit1BitPatternNumeric.Value ), ( int ) Math.Round( this._PassBitPatternNumeric.Value ), ( int ) Math.Round( this._OpenLeadsBitPatternNumeric.Value ), this.SelectedTriggerSource );
                    _ = this.Device.TriggerSubsystem.QueryTriggerState();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Simple loop load run button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="Object"/>
        ///                                             instance of this
        ///                                             <see cref="Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void LoadSimpleLoopModelButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.Device.IsDeviceOpen )
                {
                    activity = $"{this.Device.ResourceNameCaption} loading simple loop trigger model";
                    _ = this.PublishInfo( $"{activity};. " );
                    int count = ( int ) Math.Round( this._TriggerCountNumeric.Value );
                    var startDelay = TimeSpan.FromSeconds( ( double ) this._StartTriggerDelayNumeric.Value );
                    this.Device.TriggerSubsystem.LoadSimpleLoop( count, startDelay );
                    _ = this.Device.TriggerSubsystem.QueryTriggerState();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Executes the 'simple loop trigger model button click' operation. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void RunSimpleLoopTriggerModelButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} Initiating simple loop trigger model";
                _ = this.PublishInfo( $"{activity};. " );
                // TO_DO: 
                // Buffer1 Subsystem: Add buffer readings binding list; bind to the buffer data grid view. 
                // Dim br As New VI.BufferReadingCollection : br.DisplayReadings(Me._BufferDataGridView, True)
                this.Device.Buffer1Subsystem.ClearBuffer();
                this.Device.Buffer1Subsystem.StartElapsedStopwatch();
                this.Device.TriggerSubsystem.Initiate();
                this.Device.Session.Wait();
                // br.Add(Me.Device.Buffer1Subsystem.QueryBufferReadings)
                // br.DisplayReadings(Me._BufferDataGridView, False)
                this.Device.Buffer1Subsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Clears the trigger model menu item click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearTriggerModelMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} clearing the trigger model";
                _ = this.PublishInfo( $"{activity};. " );
                // TO_DO: Bind trigger model to the display in place of the channel status. 
                this.Device.TriggerSubsystem.StartElapsedStopwatch();
                this.Device.TriggerSubsystem.Abort();
                this.Device.TriggerSubsystem.ClearTriggerModel();
                this.Device.Session.Wait();
                this.Device.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Reads trigger state menu item click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadTriggerStateMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading trigger state";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.TriggerSubsystem.StartElapsedStopwatch();
                _ = this.Device.TriggerSubsystem.QueryTriggerState();
                this.Device.TriggerSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Meter completer first grading binning menu item click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void MeterCompleterFirstGradingBinningMenuItem_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                if ( this.Device.IsDeviceOpen )
                {
                    activity = $"{this.Device.ResourceNameCaption} loading meter complete first grade binning trigger model";
                    _ = this.PublishInfo( $"{activity};. " );
                    this.PrepareGradeBinningModel();
                    this.Device.TriggerSubsystem.ApplyMeterCompleteFirstGradeBinning( ( int ) Math.Round( this._TriggerCountNumeric.Value ), TimeSpan.FromSeconds( ( double ) this._StartTriggerDelayNumeric.Value ), ( int ) Math.Round( this._FailLimit1BitPatternNumeric.Value ), ( int ) Math.Round( this._PassBitPatternNumeric.Value ), ( int ) Math.Round( this._OpenLeadsBitPatternNumeric.Value ), this.SelectedTriggerSource );
                    _ = this.Device.TriggerSubsystem.QueryTriggerState();
                }
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}