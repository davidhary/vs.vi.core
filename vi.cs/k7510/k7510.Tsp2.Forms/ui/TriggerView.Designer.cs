﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp2.K7510.Forms
{
    [DesignerGenerated()]
    public partial class TriggerView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(TriggerView));
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            _TriggerToolStripPanel = new System.Windows.Forms.ToolStripPanel();
            _TriggerDelayToolStrip = new System.Windows.Forms.ToolStrip();
            _TriggerDelaysToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _StartTriggerDelayNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _StartTriggerDelayNumeric = new Core.Controls.ToolStripNumericUpDown();
            _EndTriggerDelayNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _EndTriggerDelayNumeric = new Core.Controls.ToolStripNumericUpDown();
            _Limit1ToolStrip = new System.Windows.Forms.ToolStrip();
            _Limits1Label = new System.Windows.Forms.ToolStripLabel();
            __Limit1DecimalsNumeric = new Core.Controls.ToolStripNumericUpDown();
            __Limit1DecimalsNumeric.ValueChanged += new EventHandler<EventArgs>(Limit1DecimalsNumeric_ValueChanged);
            _LowerLimit1NumericLabel = new System.Windows.Forms.ToolStripLabel();
            _LowerLimit1Numeric = new Core.Controls.ToolStripNumericUpDown();
            _UpperLimit1NumericLabel = new System.Windows.Forms.ToolStripLabel();
            _UpperLimit1Numeric = new Core.Controls.ToolStripNumericUpDown();
            _GradeBinningToolStrip = new System.Windows.Forms.ToolStrip();
            _BinningToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            __PassBitPatternNumericButton = new System.Windows.Forms.ToolStripButton();
            __PassBitPatternNumericButton.CheckStateChanged += new EventHandler(PassBitPatternNumericButton_CheckStateChanged);
            _PassBitPatternNumeric = new Core.Controls.ToolStripNumericUpDown();
            __OpenLeadsBitPatternNumericButton = new System.Windows.Forms.ToolStripButton();
            __OpenLeadsBitPatternNumericButton.CheckStateChanged += new EventHandler(OpenLeadsBitPatternNumericButton_CheckStateChanged);
            _OpenLeadsBitPatternNumeric = new Core.Controls.ToolStripNumericUpDown();
            __FailBitPatternNumericButton = new System.Windows.Forms.ToolStripButton();
            __FailBitPatternNumericButton.CheckStateChanged += new EventHandler(FailBitPatternNumericButton_CheckStateChanged);
            _FailLimit1BitPatternNumeric = new Core.Controls.ToolStripNumericUpDown();
            _TriggerLayerToolStrip = new System.Windows.Forms.ToolStrip();
            _TriggerLayerToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            _TriggerCountNumericLabel = new System.Windows.Forms.ToolStripLabel();
            _TriggerCountNumeric = new Core.Controls.ToolStripNumericUpDown();
            _TriggerSourceComboBox = new System.Windows.Forms.ToolStripComboBox();
            _TriggerSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            _ContinuousTriggerEnabledMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ReadTriggerStateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ReadTriggerStateMenuItem.Click += new EventHandler(ReadTriggerStateMenuItem_Click);
            _TriggerModelsToolStrip = new System.Windows.Forms.ToolStrip();
            _TriggerModelLabel = new System.Windows.Forms.ToolStripLabel();
            _ApplyTriggerModelSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            __LoadGradeBinTriggerModelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __LoadGradeBinTriggerModelMenuItem.Click += new EventHandler(LoadGradeBinTriggerModelButton_Click);
            _LoadSimpleLoopMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __RunSimpleLoopMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __RunSimpleLoopMenuItem.Click += new EventHandler(RunSimpleLoopTriggerModelButton_Click);
            __ClearTriggerModelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __ClearTriggerModelMenuItem.Click += new EventHandler(ClearTriggerModelMenuItem_Click);
            __MeterCompleterFirstGradingBinningMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __MeterCompleterFirstGradingBinningMenuItem.Click += new EventHandler(MeterCompleterFirstGradingBinningMenuItem_Click);
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            _TriggerToolStripPanel.SuspendLayout();
            _TriggerDelayToolStrip.SuspendLayout();
            _Limit1ToolStrip.SuspendLayout();
            _GradeBinningToolStrip.SuspendLayout();
            _TriggerLayerToolStrip.SuspendLayout();
            _TriggerModelsToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(1, 1);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0f));
            _Layout.Size = new System.Drawing.Size(379, 322);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_TriggerToolStripPanel);
            _Panel.Dock = System.Windows.Forms.DockStyle.Fill;
            _Panel.Location = new System.Drawing.Point(6, 6);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(367, 310);
            _Panel.TabIndex = 0;
            // 
            // _TriggerToolStripPanel
            // 
            _TriggerToolStripPanel.Controls.Add(_TriggerDelayToolStrip);
            _TriggerToolStripPanel.Controls.Add(_Limit1ToolStrip);
            _TriggerToolStripPanel.Controls.Add(_GradeBinningToolStrip);
            _TriggerToolStripPanel.Controls.Add(_TriggerLayerToolStrip);
            _TriggerToolStripPanel.Controls.Add(_TriggerModelsToolStrip);
            _TriggerToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top;
            _TriggerToolStripPanel.Location = new System.Drawing.Point(0, 0);
            _TriggerToolStripPanel.Name = "_TriggerToolStripPanel";
            _TriggerToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal;
            _TriggerToolStripPanel.RowMargin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            _TriggerToolStripPanel.Size = new System.Drawing.Size(367, 137);
            // 
            // _TriggerDelayToolStrip
            // 
            _TriggerDelayToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _TriggerDelayToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _TriggerDelayToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _TriggerDelayToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _TriggerDelaysToolStripLabel, _StartTriggerDelayNumericLabel, _StartTriggerDelayNumeric, _EndTriggerDelayNumericLabel, _EndTriggerDelayNumeric });
            _TriggerDelayToolStrip.Location = new System.Drawing.Point(3, 0);
            _TriggerDelayToolStrip.Name = "_TriggerDelayToolStrip";
            _TriggerDelayToolStrip.Size = new System.Drawing.Size(230, 28);
            _TriggerDelayToolStrip.TabIndex = 1;
            // 
            // _TriggerDelaysToolStripLabel
            // 
            _TriggerDelaysToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerDelaysToolStripLabel.Name = "_TriggerDelaysToolStripLabel";
            _TriggerDelaysToolStripLabel.Size = new System.Drawing.Size(43, 25);
            _TriggerDelaysToolStripLabel.Text = "DELAY";
            // 
            // _StartTriggerDelayNumericLabel
            // 
            _StartTriggerDelayNumericLabel.Name = "_StartTriggerDelayNumericLabel";
            _StartTriggerDelayNumericLabel.Size = new System.Drawing.Size(27, 25);
            _StartTriggerDelayNumericLabel.Text = "Pre:";
            // 
            // _StartTriggerDelayNumeric
            // 
            _StartTriggerDelayNumeric.AutoSize = false;
            _StartTriggerDelayNumeric.Name = "_StartTriggerDelayNumeric";
            _StartTriggerDelayNumeric.Size = new System.Drawing.Size(62, 25);
            _StartTriggerDelayNumeric.Text = "0";
            _StartTriggerDelayNumeric.ToolTipText = "Start delay in seconds";
            _StartTriggerDelayNumeric.Value = new decimal(new int[] { 20, 0, 0, 196608 });
            // 
            // _EndTriggerDelayNumericLabel
            // 
            _EndTriggerDelayNumericLabel.Name = "_EndTriggerDelayNumericLabel";
            _EndTriggerDelayNumericLabel.Size = new System.Drawing.Size(33, 25);
            _EndTriggerDelayNumericLabel.Text = "Post:";
            // 
            // _EndTriggerDelayNumeric
            // 
            _EndTriggerDelayNumeric.AutoSize = false;
            _EndTriggerDelayNumeric.Name = "_EndTriggerDelayNumeric";
            _EndTriggerDelayNumeric.Size = new System.Drawing.Size(62, 23);
            _EndTriggerDelayNumeric.Text = "0";
            _EndTriggerDelayNumeric.ToolTipText = "End delay in seconds";
            _EndTriggerDelayNumeric.Value = new decimal(new int[] { 1, 0, 0, 196608 });
            // 
            // _Limit1ToolStrip
            // 
            _Limit1ToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _Limit1ToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _Limit1ToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _Limit1ToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _Limits1Label, __Limit1DecimalsNumeric, _LowerLimit1NumericLabel, _LowerLimit1Numeric, _UpperLimit1NumericLabel, _UpperLimit1Numeric });
            _Limit1ToolStrip.Location = new System.Drawing.Point(3, 28);
            _Limit1ToolStrip.Name = "_Limit1ToolStrip";
            _Limit1ToolStrip.Size = new System.Drawing.Size(251, 28);
            _Limit1ToolStrip.TabIndex = 2;
            // 
            // _Limits1Label
            // 
            _Limits1Label.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _Limits1Label.Name = "_Limits1Label";
            _Limits1Label.Size = new System.Drawing.Size(44, 25);
            _Limits1Label.Text = "LIMIT1:";
            // 
            // _Limit1DecimalsNumeric
            // 
            __Limit1DecimalsNumeric.Name = "__Limit1DecimalsNumeric";
            __Limit1DecimalsNumeric.Size = new System.Drawing.Size(41, 25);
            __Limit1DecimalsNumeric.Text = "3";
            __Limit1DecimalsNumeric.ToolTipText = "Number of decimal places for setting the limits";
            __Limit1DecimalsNumeric.Value = new decimal(new int[] { 3, 0, 0, 0 });
            // 
            // _LowerLimit1NumericLabel
            // 
            _LowerLimit1NumericLabel.Name = "_LowerLimit1NumericLabel";
            _LowerLimit1NumericLabel.Size = new System.Drawing.Size(42, 25);
            _LowerLimit1NumericLabel.Text = "Lower:";
            // 
            // _LowerLimit1Numeric
            // 
            _LowerLimit1Numeric.Name = "_LowerLimit1Numeric";
            _LowerLimit1Numeric.Size = new System.Drawing.Size(41, 25);
            _LowerLimit1Numeric.Text = "0";
            _LowerLimit1Numeric.ToolTipText = "Lower limit";
            _LowerLimit1Numeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _UpperLimit1NumericLabel
            // 
            _UpperLimit1NumericLabel.Name = "_UpperLimit1NumericLabel";
            _UpperLimit1NumericLabel.Size = new System.Drawing.Size(39, 25);
            _UpperLimit1NumericLabel.Text = "Upper";
            // 
            // _UpperLimit1Numeric
            // 
            _UpperLimit1Numeric.Name = "_UpperLimit1Numeric";
            _UpperLimit1Numeric.Size = new System.Drawing.Size(41, 25);
            _UpperLimit1Numeric.Text = "0";
            _UpperLimit1Numeric.ToolTipText = "Upper limit";
            _UpperLimit1Numeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _GradeBinningToolStrip
            // 
            _GradeBinningToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _GradeBinningToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _GradeBinningToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _GradeBinningToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _BinningToolStripLabel, __PassBitPatternNumericButton, _PassBitPatternNumeric, __OpenLeadsBitPatternNumericButton, _OpenLeadsBitPatternNumeric, __FailBitPatternNumericButton, _FailLimit1BitPatternNumeric });
            _GradeBinningToolStrip.Location = new System.Drawing.Point(3, 56);
            _GradeBinningToolStrip.Name = "_GradeBinningToolStrip";
            _GradeBinningToolStrip.Size = new System.Drawing.Size(300, 28);
            _GradeBinningToolStrip.TabIndex = 0;
            // 
            // _BinningToolStripLabel
            // 
            _BinningToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _BinningToolStripLabel.Name = "_BinningToolStripLabel";
            _BinningToolStripLabel.Size = new System.Drawing.Size(26, 25);
            _BinningToolStripLabel.Text = "BIN";
            // 
            // _PassBitPatternNumericButton
            // 
            __PassBitPatternNumericButton.CheckOnClick = true;
            __PassBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __PassBitPatternNumericButton.Image = (System.Drawing.Image)resources.GetObject("_PassBitPatternNumericButton.Image");
            __PassBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __PassBitPatternNumericButton.Name = "__PassBitPatternNumericButton";
            __PassBitPatternNumericButton.Size = new System.Drawing.Size(49, 25);
            __PassBitPatternNumericButton.Text = "Pass 0x";
            // 
            // _PassBitPatternNumeric
            // 
            _PassBitPatternNumeric.Name = "_PassBitPatternNumeric";
            _PassBitPatternNumeric.Size = new System.Drawing.Size(41, 25);
            _PassBitPatternNumeric.Text = "2";
            _PassBitPatternNumeric.ToolTipText = "Pass bit pattern";
            _PassBitPatternNumeric.Value = new decimal(new int[] { 2, 0, 0, 0 });
            // 
            // _OpenLeadsBitPatternNumericButton
            // 
            __OpenLeadsBitPatternNumericButton.CheckOnClick = true;
            __OpenLeadsBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __OpenLeadsBitPatternNumericButton.Image = (System.Drawing.Image)resources.GetObject("_OpenLeadsBitPatternNumericButton.Image");
            __OpenLeadsBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __OpenLeadsBitPatternNumericButton.Name = "__OpenLeadsBitPatternNumericButton";
            __OpenLeadsBitPatternNumericButton.Size = new System.Drawing.Size(55, 25);
            __OpenLeadsBitPatternNumericButton.Text = "Open 0x";
            // 
            // _OpenLeadsBitPatternNumeric
            // 
            _OpenLeadsBitPatternNumeric.Name = "_OpenLeadsBitPatternNumeric";
            _OpenLeadsBitPatternNumeric.Size = new System.Drawing.Size(41, 25);
            _OpenLeadsBitPatternNumeric.Text = "0";
            _OpenLeadsBitPatternNumeric.ToolTipText = "Open leads bit pattern";
            _OpenLeadsBitPatternNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _FailBitPatternNumericButton
            // 
            __FailBitPatternNumericButton.CheckOnClick = true;
            __FailBitPatternNumericButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __FailBitPatternNumericButton.Image = (System.Drawing.Image)resources.GetObject("_FailBitPatternNumericButton.Image");
            __FailBitPatternNumericButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __FailBitPatternNumericButton.Name = "__FailBitPatternNumericButton";
            __FailBitPatternNumericButton.Size = new System.Drawing.Size(44, 25);
            __FailBitPatternNumericButton.Text = "Fail 0x";
            // 
            // _FailLimit1BitPatternNumeric
            // 
            _FailLimit1BitPatternNumeric.Name = "_FailLimit1BitPatternNumeric";
            _FailLimit1BitPatternNumeric.Size = new System.Drawing.Size(41, 25);
            _FailLimit1BitPatternNumeric.Text = "1";
            _FailLimit1BitPatternNumeric.ToolTipText = "Failed bit pattern";
            _FailLimit1BitPatternNumeric.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // _TriggerLayerToolStrip
            // 
            _TriggerLayerToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _TriggerLayerToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _TriggerLayerToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _TriggerLayerToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _TriggerLayerToolStripLabel, _TriggerCountNumericLabel, _TriggerCountNumeric, _TriggerSourceComboBox, _TriggerSplitButton });
            _TriggerLayerToolStrip.Location = new System.Drawing.Point(3, 84);
            _TriggerLayerToolStrip.Name = "_TriggerLayerToolStrip";
            _TriggerLayerToolStrip.Size = new System.Drawing.Size(250, 28);
            _TriggerLayerToolStrip.TabIndex = 7;
            _TriggerLayerToolStrip.Text = "ToolStrip1";
            // 
            // _TriggerLayerToolStripLabel
            // 
            _TriggerLayerToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerLayerToolStripLabel.Name = "_TriggerLayerToolStripLabel";
            _TriggerLayerToolStripLabel.Size = new System.Drawing.Size(34, 25);
            _TriggerLayerToolStripLabel.Text = "TRIG:";
            // 
            // _TriggerCountNumericLabel
            // 
            _TriggerCountNumericLabel.Name = "_TriggerCountNumericLabel";
            _TriggerCountNumericLabel.Size = new System.Drawing.Size(19, 25);
            _TriggerCountNumericLabel.Text = "N:";
            // 
            // _TriggerCountNumeric
            // 
            _TriggerCountNumeric.Name = "_TriggerCountNumeric";
            _TriggerCountNumeric.Size = new System.Drawing.Size(41, 25);
            _TriggerCountNumeric.Text = "0";
            _TriggerCountNumeric.ToolTipText = "Trigger Count";
            _TriggerCountNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _TriggerSourceComboBox
            // 
            _TriggerSourceComboBox.Name = "_TriggerSourceComboBox";
            _TriggerSourceComboBox.Size = new System.Drawing.Size(91, 28);
            _TriggerSourceComboBox.Text = "Immediate";
            _TriggerSourceComboBox.ToolTipText = "Trigger Source";
            // 
            // _TriggerSplitButton
            // 
            _TriggerSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _ContinuousTriggerEnabledMenuItem, __ReadTriggerStateMenuItem });
            _TriggerSplitButton.Name = "_TriggerSplitButton";
            _TriggerSplitButton.Size = new System.Drawing.Size(60, 25);
            _TriggerSplitButton.Text = "More...";
            _TriggerSplitButton.ToolTipText = "Continuous On/Off";
            // 
            // _ContinuousTriggerEnabledMenuItem
            // 
            _ContinuousTriggerEnabledMenuItem.CheckOnClick = true;
            _ContinuousTriggerEnabledMenuItem.Name = "_ContinuousTriggerEnabledMenuItem";
            _ContinuousTriggerEnabledMenuItem.Size = new System.Drawing.Size(136, 22);
            _ContinuousTriggerEnabledMenuItem.Text = "Continuous";
            _ContinuousTriggerEnabledMenuItem.ToolTipText = "Check to enable";
            // 
            // _ReadTriggerStateMenuItem
            // 
            __ReadTriggerStateMenuItem.Name = "__ReadTriggerStateMenuItem";
            __ReadTriggerStateMenuItem.Size = new System.Drawing.Size(136, 22);
            __ReadTriggerStateMenuItem.Text = "Read State";
            __ReadTriggerStateMenuItem.ToolTipText = "Reads trigger state";
            // 
            // _TriggerModelsToolStrip
            // 
            _TriggerModelsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            _TriggerModelsToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _TriggerModelsToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _TriggerModelsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _TriggerModelLabel, _ApplyTriggerModelSplitButton });
            _TriggerModelsToolStrip.Location = new System.Drawing.Point(5, 112);
            _TriggerModelsToolStrip.Name = "_TriggerModelsToolStrip";
            _TriggerModelsToolStrip.Size = new System.Drawing.Size(142, 25);
            _TriggerModelsToolStrip.TabIndex = 3;
            // 
            // _TriggerModelLabel
            // 
            _TriggerModelLabel.Font = new System.Drawing.Font("Segoe UI", 9.0f, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerModelLabel.Name = "_TriggerModelLabel";
            _TriggerModelLabel.Size = new System.Drawing.Size(45, 22);
            _TriggerModelLabel.Text = "APPLY:";
            // 
            // _ApplyTriggerModelSplitButton
            // 
            _ApplyTriggerModelSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _ApplyTriggerModelSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __LoadGradeBinTriggerModelMenuItem, _LoadSimpleLoopMenuItem, __RunSimpleLoopMenuItem, __ClearTriggerModelMenuItem, __MeterCompleterFirstGradingBinningMenuItem });
            _ApplyTriggerModelSplitButton.Image = (System.Drawing.Image)resources.GetObject("_ApplyTriggerModelSplitButton.Image");
            _ApplyTriggerModelSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ApplyTriggerModelSplitButton.Name = "_ApplyTriggerModelSplitButton";
            _ApplyTriggerModelSplitButton.Size = new System.Drawing.Size(63, 22);
            _ApplyTriggerModelSplitButton.Text = "Select...";
            _ApplyTriggerModelSplitButton.ToolTipText = "Select option:";
            // 
            // _LoadGradeBinTriggerModelMenuItem
            // 
            __LoadGradeBinTriggerModelMenuItem.Name = "__LoadGradeBinTriggerModelMenuItem";
            __LoadGradeBinTriggerModelMenuItem.Size = new System.Drawing.Size(352, 22);
            __LoadGradeBinTriggerModelMenuItem.Text = "Grading/Binning Trigger Loop";
            __LoadGradeBinTriggerModelMenuItem.ToolTipText = "Loads the grading binning trigger model";
            // 
            // _LoadSimpleLoopMenuItem
            // 
            _LoadSimpleLoopMenuItem.Name = "_LoadSimpleLoopMenuItem";
            _LoadSimpleLoopMenuItem.Size = new System.Drawing.Size(352, 22);
            _LoadSimpleLoopMenuItem.Text = "Load Simple Loop";
            _LoadSimpleLoopMenuItem.ToolTipText = "Loads a simple loop trigger model.";
            // 
            // _RunSimpleLoopMenuItem
            // 
            __RunSimpleLoopMenuItem.Name = "__RunSimpleLoopMenuItem";
            __RunSimpleLoopMenuItem.Size = new System.Drawing.Size(352, 22);
            __RunSimpleLoopMenuItem.Text = "Run Simple Loop";
            __RunSimpleLoopMenuItem.ToolTipText = "Initiates a single loop, waits for completion and displays the values";
            // 
            // _ClearTriggerModelMenuItem
            // 
            __ClearTriggerModelMenuItem.Name = "__ClearTriggerModelMenuItem";
            __ClearTriggerModelMenuItem.Size = new System.Drawing.Size(352, 22);
            __ClearTriggerModelMenuItem.Text = "Clear Trigger Model";
            __ClearTriggerModelMenuItem.ToolTipText = "Clears the instrument trigger model";
            // 
            // _MeterCompleterFirstGradingBinningMenuItem
            // 
            __MeterCompleterFirstGradingBinningMenuItem.Name = "__MeterCompleterFirstGradingBinningMenuItem";
            __MeterCompleterFirstGradingBinningMenuItem.Size = new System.Drawing.Size(352, 22);
            __MeterCompleterFirstGradingBinningMenuItem.Text = "Meter Complete First Grading/Binning Trigger Model";
            __MeterCompleterFirstGradingBinningMenuItem.ToolTipText = "Applies a trigger model where meter complete is output first.";
            // 
            // TriggerView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "TriggerView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(381, 324);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            _TriggerToolStripPanel.ResumeLayout(false);
            _TriggerToolStripPanel.PerformLayout();
            _TriggerDelayToolStrip.ResumeLayout(false);
            _TriggerDelayToolStrip.PerformLayout();
            _Limit1ToolStrip.ResumeLayout(false);
            _Limit1ToolStrip.PerformLayout();
            _GradeBinningToolStrip.ResumeLayout(false);
            _GradeBinningToolStrip.PerformLayout();
            _TriggerLayerToolStrip.ResumeLayout(false);
            _TriggerLayerToolStrip.PerformLayout();
            _TriggerModelsToolStrip.ResumeLayout(false);
            _TriggerModelsToolStrip.PerformLayout();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.ToolStripPanel _TriggerToolStripPanel;
        private System.Windows.Forms.ToolStrip _TriggerDelayToolStrip;
        private System.Windows.Forms.ToolStripLabel _TriggerDelaysToolStripLabel;
        private System.Windows.Forms.ToolStripLabel _StartTriggerDelayNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _StartTriggerDelayNumeric;
        private System.Windows.Forms.ToolStripLabel _EndTriggerDelayNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _EndTriggerDelayNumeric;
        private System.Windows.Forms.ToolStrip _Limit1ToolStrip;
        private System.Windows.Forms.ToolStripLabel _Limits1Label;
        private Core.Controls.ToolStripNumericUpDown __Limit1DecimalsNumeric;

        private Core.Controls.ToolStripNumericUpDown _Limit1DecimalsNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __Limit1DecimalsNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__Limit1DecimalsNumeric != null)
                {
                    __Limit1DecimalsNumeric.ValueChanged -= Limit1DecimalsNumeric_ValueChanged;
                }

                __Limit1DecimalsNumeric = value;
                if (__Limit1DecimalsNumeric != null)
                {
                    __Limit1DecimalsNumeric.ValueChanged += Limit1DecimalsNumeric_ValueChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripLabel _LowerLimit1NumericLabel;
        private Core.Controls.ToolStripNumericUpDown _LowerLimit1Numeric;
        private System.Windows.Forms.ToolStripLabel _UpperLimit1NumericLabel;
        private Core.Controls.ToolStripNumericUpDown _UpperLimit1Numeric;
        private System.Windows.Forms.ToolStrip _GradeBinningToolStrip;
        private System.Windows.Forms.ToolStripLabel _BinningToolStripLabel;
        private System.Windows.Forms.ToolStripButton __PassBitPatternNumericButton;

        private System.Windows.Forms.ToolStripButton _PassBitPatternNumericButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PassBitPatternNumericButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PassBitPatternNumericButton != null)
                {
                    __PassBitPatternNumericButton.CheckStateChanged -= PassBitPatternNumericButton_CheckStateChanged;
                }

                __PassBitPatternNumericButton = value;
                if (__PassBitPatternNumericButton != null)
                {
                    __PassBitPatternNumericButton.CheckStateChanged += PassBitPatternNumericButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _PassBitPatternNumeric;
        private System.Windows.Forms.ToolStripButton __OpenLeadsBitPatternNumericButton;

        private System.Windows.Forms.ToolStripButton _OpenLeadsBitPatternNumericButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenLeadsBitPatternNumericButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenLeadsBitPatternNumericButton != null)
                {
                    __OpenLeadsBitPatternNumericButton.CheckStateChanged -= OpenLeadsBitPatternNumericButton_CheckStateChanged;
                }

                __OpenLeadsBitPatternNumericButton = value;
                if (__OpenLeadsBitPatternNumericButton != null)
                {
                    __OpenLeadsBitPatternNumericButton.CheckStateChanged += OpenLeadsBitPatternNumericButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _OpenLeadsBitPatternNumeric;
        private System.Windows.Forms.ToolStripButton __FailBitPatternNumericButton;

        private System.Windows.Forms.ToolStripButton _FailBitPatternNumericButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __FailBitPatternNumericButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__FailBitPatternNumericButton != null)
                {
                    __FailBitPatternNumericButton.CheckStateChanged -= FailBitPatternNumericButton_CheckStateChanged;
                }

                __FailBitPatternNumericButton = value;
                if (__FailBitPatternNumericButton != null)
                {
                    __FailBitPatternNumericButton.CheckStateChanged += FailBitPatternNumericButton_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown _FailLimit1BitPatternNumeric;
        private System.Windows.Forms.ToolStrip _TriggerLayerToolStrip;
        private System.Windows.Forms.ToolStripLabel _TriggerLayerToolStripLabel;
        private System.Windows.Forms.ToolStripLabel _TriggerCountNumericLabel;
        private Core.Controls.ToolStripNumericUpDown _TriggerCountNumeric;
        private System.Windows.Forms.ToolStripComboBox _TriggerSourceComboBox;
        private System.Windows.Forms.ToolStripSplitButton _TriggerSplitButton;
        private System.Windows.Forms.ToolStripMenuItem _ContinuousTriggerEnabledMenuItem;
        private System.Windows.Forms.ToolStripMenuItem __ReadTriggerStateMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ReadTriggerStateMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadTriggerStateMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadTriggerStateMenuItem != null)
                {
                    __ReadTriggerStateMenuItem.Click -= ReadTriggerStateMenuItem_Click;
                }

                __ReadTriggerStateMenuItem = value;
                if (__ReadTriggerStateMenuItem != null)
                {
                    __ReadTriggerStateMenuItem.Click += ReadTriggerStateMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStrip _TriggerModelsToolStrip;
        private System.Windows.Forms.ToolStripLabel _TriggerModelLabel;
        private System.Windows.Forms.ToolStripSplitButton _ApplyTriggerModelSplitButton;
        private System.Windows.Forms.ToolStripMenuItem __LoadGradeBinTriggerModelMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _LoadGradeBinTriggerModelMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __LoadGradeBinTriggerModelMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__LoadGradeBinTriggerModelMenuItem != null)
                {
                    __LoadGradeBinTriggerModelMenuItem.Click -= LoadGradeBinTriggerModelButton_Click;
                }

                __LoadGradeBinTriggerModelMenuItem = value;
                if (__LoadGradeBinTriggerModelMenuItem != null)
                {
                    __LoadGradeBinTriggerModelMenuItem.Click += LoadGradeBinTriggerModelButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _LoadSimpleLoopMenuItem;
        private System.Windows.Forms.ToolStripMenuItem __RunSimpleLoopMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _RunSimpleLoopMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __RunSimpleLoopMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__RunSimpleLoopMenuItem != null)
                {
                    __RunSimpleLoopMenuItem.Click -= RunSimpleLoopTriggerModelButton_Click;
                }

                __RunSimpleLoopMenuItem = value;
                if (__RunSimpleLoopMenuItem != null)
                {
                    __RunSimpleLoopMenuItem.Click += RunSimpleLoopTriggerModelButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __ClearTriggerModelMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _ClearTriggerModelMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearTriggerModelMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearTriggerModelMenuItem != null)
                {
                    __ClearTriggerModelMenuItem.Click -= ClearTriggerModelMenuItem_Click;
                }

                __ClearTriggerModelMenuItem = value;
                if (__ClearTriggerModelMenuItem != null)
                {
                    __ClearTriggerModelMenuItem.Click += ClearTriggerModelMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __MeterCompleterFirstGradingBinningMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _MeterCompleterFirstGradingBinningMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MeterCompleterFirstGradingBinningMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MeterCompleterFirstGradingBinningMenuItem != null)
                {
                    __MeterCompleterFirstGradingBinningMenuItem.Click -= MeterCompleterFirstGradingBinningMenuItem_Click;
                }

                __MeterCompleterFirstGradingBinningMenuItem = value;
                if (__MeterCompleterFirstGradingBinningMenuItem != null)
                {
                    __MeterCompleterFirstGradingBinningMenuItem.Click += MeterCompleterFirstGradingBinningMenuItem_Click;
                }
            }
        }
    }
}