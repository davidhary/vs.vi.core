﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Tsp2.K7510.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadingView));
            __BufferDataGridView = new System.Windows.Forms.DataGridView();
            __BufferDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(BufferDataGridView_DataError);
            _BufferToolStrip = new System.Windows.Forms.ToolStrip();
            __ReadBufferButton = new System.Windows.Forms.ToolStripButton();
            __ReadBufferButton.Click += new EventHandler(ReadBufferButton_Click);
            _BufferCountLabel = new Core.Controls.ToolStripLabel();
            _LastPointNumberLabel = new Core.Controls.ToolStripLabel();
            _FirstPointNumberLabel = new Core.Controls.ToolStripLabel();
            _BufferNameLabel = new Core.Controls.ToolStripLabel();
            __ClearBufferDisplayButton = new System.Windows.Forms.ToolStripButton();
            __ClearBufferDisplayButton.Click += new EventHandler(ClearBufferDisplayButton_Click);
            __BufferSizeNumeric = new Core.Controls.ToolStripNumericUpDown();
            __BufferSizeNumeric.Validating += new System.ComponentModel.CancelEventHandler(BufferSizeNumeric_Validating);
            _ReadingToolStrip = new System.Windows.Forms.ToolStrip();
            __ReadButton = new System.Windows.Forms.ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __ReadingComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReadingComboBox.SelectedIndexChanged += new EventHandler(ReadingComboBox_SelectedIndexChanged);
            __AbortButton = new System.Windows.Forms.ToolStripButton();
            __AbortButton.Click += new EventHandler(AbortButton_Click);
            _GoSplitButton = new System.Windows.Forms.ToolStripSplitButton();
            __InitiateTriggerPlanMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __InitiateTriggerPlanMenuItem.Click += new EventHandler(InitiateTriggerPlanMenuItem_Click);
            __AbortStartTriggerPlanMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __AbortStartTriggerPlanMenuItem.Click += new EventHandler(AbortStartTriggerPlanMenuItem_Click);
            __MonitorActiveTriggerPlanMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __MonitorActiveTriggerPlanMenuItem.Click += new EventHandler(MonitorActiveTriggerPlanMenuItem_Click);
            __InitMonitorReadRepeatMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __InitMonitorReadRepeatMenuItem.Click += new EventHandler(InitMonitorReadRepeatMenuItem_Click);
            _RepeatMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __StreamBufferMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            __StreamBufferMenuItem.CheckStateChanged += new EventHandler(StreamBufferMenuItem_CheckStateChanged);
            _TriggerStateLabel = new Core.Controls.ToolStripLabel();
            ((System.ComponentModel.ISupportInitialize)__BufferDataGridView).BeginInit();
            _BufferToolStrip.SuspendLayout();
            _ReadingToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _BufferDataGridView
            // 
            __BufferDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            __BufferDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            __BufferDataGridView.Location = new System.Drawing.Point(1, 54);
            __BufferDataGridView.Name = "__BufferDataGridView";
            __BufferDataGridView.Size = new System.Drawing.Size(345, 313);
            __BufferDataGridView.TabIndex = 12;
            // 
            // _BufferToolStrip
            // 
            _BufferToolStrip.GripMargin = new System.Windows.Forms.Padding(0);
            _BufferToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _BufferToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ReadBufferButton, _BufferCountLabel, _LastPointNumberLabel, _FirstPointNumberLabel, _BufferNameLabel, __ClearBufferDisplayButton, __BufferSizeNumeric });
            _BufferToolStrip.Location = new System.Drawing.Point(1, 26);
            _BufferToolStrip.Name = "_BufferToolStrip";
            _BufferToolStrip.Size = new System.Drawing.Size(345, 28);
            _BufferToolStrip.TabIndex = 11;
            _BufferToolStrip.Text = "Buffer";
            // 
            // _ReadBufferButton
            // 
            __ReadBufferButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadBufferButton.Image = (System.Drawing.Image)resources.GetObject("_ReadBufferButton.Image");
            __ReadBufferButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadBufferButton.Name = "__ReadBufferButton";
            __ReadBufferButton.Size = new System.Drawing.Size(72, 25);
            __ReadBufferButton.Text = "Read Buffer";
            // 
            // _BufferCountLabel
            // 
            _BufferCountLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _BufferCountLabel.Name = "_BufferCountLabel";
            _BufferCountLabel.Size = new System.Drawing.Size(13, 25);
            _BufferCountLabel.Text = "0";
            _BufferCountLabel.ToolTipText = "Buffer count";
            // 
            // _LastPointNumberLabel
            // 
            _LastPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _LastPointNumberLabel.Name = "_LastPointNumberLabel";
            _LastPointNumberLabel.Size = new System.Drawing.Size(13, 25);
            _LastPointNumberLabel.Text = "2";
            _LastPointNumberLabel.ToolTipText = "Number of last buffer reading";
            // 
            // _FirstPointNumberLabel
            // 
            _FirstPointNumberLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _FirstPointNumberLabel.Name = "_FirstPointNumberLabel";
            _FirstPointNumberLabel.Size = new System.Drawing.Size(13, 25);
            _FirstPointNumberLabel.Text = "1";
            _FirstPointNumberLabel.ToolTipText = "Number of the first buffer reading";
            // 
            // _BufferNameLabel
            // 
            _BufferNameLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _BufferNameLabel.Name = "_BufferNameLabel";
            _BufferNameLabel.Size = new System.Drawing.Size(62, 25);
            _BufferNameLabel.Text = "defbuffer1";
            // 
            // _ClearBufferDisplayButton
            // 
            __ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ClearBufferDisplayButton.Image = (System.Drawing.Image)resources.GetObject("_ClearBufferDisplayButton.Image");
            __ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearBufferDisplayButton.Name = "__ClearBufferDisplayButton";
            __ClearBufferDisplayButton.Size = new System.Drawing.Size(38, 25);
            __ClearBufferDisplayButton.Text = "Clear";
            __ClearBufferDisplayButton.ToolTipText = "Clears the display";
            // 
            // _BufferSizeNumeric
            // 
            __BufferSizeNumeric.AutoSize = false;
            __BufferSizeNumeric.Name = "__BufferSizeNumeric";
            __BufferSizeNumeric.Size = new System.Drawing.Size(71, 25);
            __BufferSizeNumeric.Text = "0";
            __BufferSizeNumeric.Value = new decimal(new int[] { 0, 0, 0, 0 });
            // 
            // _ReadingToolStrip
            // 
            _ReadingToolStrip.GripMargin = new System.Windows.Forms.Padding(20);
            _ReadingToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            _ReadingToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ReadButton, __ReadingComboBox, __AbortButton, _GoSplitButton, _TriggerStateLabel });
            _ReadingToolStrip.Location = new System.Drawing.Point(1, 1);
            _ReadingToolStrip.Name = "_ReadingToolStrip";
            _ReadingToolStrip.Size = new System.Drawing.Size(345, 25);
            _ReadingToolStrip.TabIndex = 10;
            _ReadingToolStrip.Text = "Reading Tool Strip";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(37, 22);
            __ReadButton.Text = "Read";
            // 
            // _ReadingComboBox
            // 
            __ReadingComboBox.Name = "__ReadingComboBox";
            __ReadingComboBox.Size = new System.Drawing.Size(121, 25);
            __ReadingComboBox.ToolTipText = "Select reading type to display";
            // 
            // _AbortButton
            // 
            __AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            __AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            __AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __AbortButton.Name = "__AbortButton";
            __AbortButton.Size = new System.Drawing.Size(41, 22);
            __AbortButton.Text = "Abort";
            __AbortButton.ToolTipText = "Aborts triggering";
            // 
            // _GoSplitButton
            // 
            _GoSplitButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _GoSplitButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _GoSplitButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { __InitiateTriggerPlanMenuItem, __AbortStartTriggerPlanMenuItem, __MonitorActiveTriggerPlanMenuItem, __InitMonitorReadRepeatMenuItem, _RepeatMenuItem, __StreamBufferMenuItem });
            _GoSplitButton.Image = (System.Drawing.Image)resources.GetObject("_GoSplitButton.Image");
            _GoSplitButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _GoSplitButton.Name = "_GoSplitButton";
            _GoSplitButton.Size = new System.Drawing.Size(47, 22);
            _GoSplitButton.Text = "Go...";
            _GoSplitButton.ToolTipText = "Select initiation options";
            // 
            // _InitiateTriggerPlanMenuItem
            // 
            __InitiateTriggerPlanMenuItem.Name = "__InitiateTriggerPlanMenuItem";
            __InitiateTriggerPlanMenuItem.Size = new System.Drawing.Size(221, 22);
            __InitiateTriggerPlanMenuItem.Text = "Initiate";
            __InitiateTriggerPlanMenuItem.ToolTipText = "Sends the Initiate command to the instrument";
            // 
            // _AbortStartTriggerPlanMenuItem
            // 
            __AbortStartTriggerPlanMenuItem.Name = "__AbortStartTriggerPlanMenuItem";
            __AbortStartTriggerPlanMenuItem.Size = new System.Drawing.Size(221, 22);
            __AbortStartTriggerPlanMenuItem.Text = "Abort, Clear, Initiate";
            __AbortStartTriggerPlanMenuItem.ToolTipText = "Aborts, clears buffer and starts the trigger plan";
            // 
            // _MonitorActiveTriggerPlanMenuItem
            // 
            __MonitorActiveTriggerPlanMenuItem.Name = "__MonitorActiveTriggerPlanMenuItem";
            __MonitorActiveTriggerPlanMenuItem.Size = new System.Drawing.Size(221, 22);
            __MonitorActiveTriggerPlanMenuItem.Text = "Monitor Active Trigger State";
            __MonitorActiveTriggerPlanMenuItem.ToolTipText = "Monitors active trigger state. Exits if trigger plan inactive";
            // 
            // _InitMonitorReadRepeatMenuItem
            // 
            __InitMonitorReadRepeatMenuItem.Name = "__InitMonitorReadRepeatMenuItem";
            __InitMonitorReadRepeatMenuItem.Size = new System.Drawing.Size(221, 22);
            __InitMonitorReadRepeatMenuItem.Text = "Init, Monitor, Read, Repeat";
            __InitMonitorReadRepeatMenuItem.ToolTipText = "Initiates a trigger plan, monitors it, reads and displays buffer and repeats if a" + "uto repeat is checked";
            // 
            // _RepeatMenuItem
            // 
            _RepeatMenuItem.CheckOnClick = true;
            _RepeatMenuItem.Name = "_RepeatMenuItem";
            _RepeatMenuItem.Size = new System.Drawing.Size(221, 22);
            _RepeatMenuItem.Text = "Repeat";
            _RepeatMenuItem.ToolTipText = "Repeat initiating the trigger plan";
            // 
            // _StreamBufferMenuItem
            // 
            __StreamBufferMenuItem.CheckOnClick = true;
            __StreamBufferMenuItem.Name = "__StreamBufferMenuItem";
            __StreamBufferMenuItem.Size = new System.Drawing.Size(221, 22);
            __StreamBufferMenuItem.Text = "Stream Buffer";
            __StreamBufferMenuItem.ToolTipText = "Continuously reads new values while a trigger plan is active";
            // 
            // _TriggerStateLabel
            // 
            _TriggerStateLabel.Name = "_TriggerStateLabel";
            _TriggerStateLabel.Size = new System.Drawing.Size(26, 22);
            _TriggerStateLabel.Text = "idle";
            _TriggerStateLabel.ToolTipText = "Trigger state";
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(__BufferDataGridView);
            Controls.Add(_BufferToolStrip);
            Controls.Add(_ReadingToolStrip);
            Name = "ReadingView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(347, 368);
            ((System.ComponentModel.ISupportInitialize)__BufferDataGridView).EndInit();
            _BufferToolStrip.ResumeLayout(false);
            _BufferToolStrip.PerformLayout();
            _ReadingToolStrip.ResumeLayout(false);
            _ReadingToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.DataGridView __BufferDataGridView;

        private System.Windows.Forms.DataGridView _BufferDataGridView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BufferDataGridView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BufferDataGridView != null)
                {
                    __BufferDataGridView.DataError -= BufferDataGridView_DataError;
                }

                __BufferDataGridView = value;
                if (__BufferDataGridView != null)
                {
                    __BufferDataGridView.DataError += BufferDataGridView_DataError;
                }
            }
        }

        private System.Windows.Forms.ToolStrip _BufferToolStrip;
        private System.Windows.Forms.ToolStripButton __ReadBufferButton;

        private System.Windows.Forms.ToolStripButton _ReadBufferButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadBufferButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadBufferButton != null)
                {
                    __ReadBufferButton.Click -= ReadBufferButton_Click;
                }

                __ReadBufferButton = value;
                if (__ReadBufferButton != null)
                {
                    __ReadBufferButton.Click += ReadBufferButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripLabel _BufferCountLabel;
        private Core.Controls.ToolStripLabel _LastPointNumberLabel;
        private Core.Controls.ToolStripLabel _FirstPointNumberLabel;
        private Core.Controls.ToolStripLabel _BufferNameLabel;
        private System.Windows.Forms.ToolStripButton __ClearBufferDisplayButton;

        private System.Windows.Forms.ToolStripButton _ClearBufferDisplayButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearBufferDisplayButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearBufferDisplayButton != null)
                {
                    __ClearBufferDisplayButton.Click -= ClearBufferDisplayButton_Click;
                }

                __ClearBufferDisplayButton = value;
                if (__ClearBufferDisplayButton != null)
                {
                    __ClearBufferDisplayButton.Click += ClearBufferDisplayButton_Click;
                }
            }
        }

        private Core.Controls.ToolStripNumericUpDown __BufferSizeNumeric;

        private Core.Controls.ToolStripNumericUpDown _BufferSizeNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __BufferSizeNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__BufferSizeNumeric != null)
                {
                    __BufferSizeNumeric.Validating -= BufferSizeNumeric_Validating;
                }

                __BufferSizeNumeric = value;
                if (__BufferSizeNumeric != null)
                {
                    __BufferSizeNumeric.Validating += BufferSizeNumeric_Validating;
                }
            }
        }

        private System.Windows.Forms.ToolStrip _ReadingToolStrip;
        private System.Windows.Forms.ToolStripButton __ReadButton;

        private System.Windows.Forms.ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripComboBox __ReadingComboBox;

        private System.Windows.Forms.ToolStripComboBox _ReadingComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged -= ReadingComboBox_SelectedIndexChanged;
                }

                __ReadingComboBox = value;
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged += ReadingComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton __AbortButton;

        private System.Windows.Forms.ToolStripButton _AbortButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortButton != null)
                {
                    __AbortButton.Click -= AbortButton_Click;
                }

                __AbortButton = value;
                if (__AbortButton != null)
                {
                    __AbortButton.Click += AbortButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripSplitButton _GoSplitButton;
        private System.Windows.Forms.ToolStripMenuItem __InitiateTriggerPlanMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _InitiateTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitiateTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitiateTriggerPlanMenuItem != null)
                {
                    __InitiateTriggerPlanMenuItem.Click -= InitiateTriggerPlanMenuItem_Click;
                }

                __InitiateTriggerPlanMenuItem = value;
                if (__InitiateTriggerPlanMenuItem != null)
                {
                    __InitiateTriggerPlanMenuItem.Click += InitiateTriggerPlanMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __AbortStartTriggerPlanMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _AbortStartTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AbortStartTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AbortStartTriggerPlanMenuItem != null)
                {
                    __AbortStartTriggerPlanMenuItem.Click -= AbortStartTriggerPlanMenuItem_Click;
                }

                __AbortStartTriggerPlanMenuItem = value;
                if (__AbortStartTriggerPlanMenuItem != null)
                {
                    __AbortStartTriggerPlanMenuItem.Click += AbortStartTriggerPlanMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __MonitorActiveTriggerPlanMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _MonitorActiveTriggerPlanMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __MonitorActiveTriggerPlanMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__MonitorActiveTriggerPlanMenuItem != null)
                {
                    __MonitorActiveTriggerPlanMenuItem.Click -= MonitorActiveTriggerPlanMenuItem_Click;
                }

                __MonitorActiveTriggerPlanMenuItem = value;
                if (__MonitorActiveTriggerPlanMenuItem != null)
                {
                    __MonitorActiveTriggerPlanMenuItem.Click += MonitorActiveTriggerPlanMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem __InitMonitorReadRepeatMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _InitMonitorReadRepeatMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __InitMonitorReadRepeatMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__InitMonitorReadRepeatMenuItem != null)
                {
                    __InitMonitorReadRepeatMenuItem.Click -= InitMonitorReadRepeatMenuItem_Click;
                }

                __InitMonitorReadRepeatMenuItem = value;
                if (__InitMonitorReadRepeatMenuItem != null)
                {
                    __InitMonitorReadRepeatMenuItem.Click += InitMonitorReadRepeatMenuItem_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripMenuItem _RepeatMenuItem;
        private System.Windows.Forms.ToolStripMenuItem __StreamBufferMenuItem;

        private System.Windows.Forms.ToolStripMenuItem _StreamBufferMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __StreamBufferMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__StreamBufferMenuItem != null)
                {
                    __StreamBufferMenuItem.CheckStateChanged -= StreamBufferMenuItem_CheckStateChanged;
                }

                __StreamBufferMenuItem = value;
                if (__StreamBufferMenuItem != null)
                {
                    __StreamBufferMenuItem.CheckStateChanged += StreamBufferMenuItem_CheckStateChanged;
                }
            }
        }

        private Core.Controls.ToolStripLabel _TriggerStateLabel;
    }
}