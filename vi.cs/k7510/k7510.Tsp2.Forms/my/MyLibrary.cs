﻿
namespace isr.VI.Tsp2.K7510.Forms.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.K7500Tsp;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "VI TSP2 K7510 Meter Forms Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "K7510 TSP2 Meter Forms Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "VI.Tsp2.K7510.Forms";
    }
}