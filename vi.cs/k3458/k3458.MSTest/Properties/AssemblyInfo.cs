﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K3458 VI Tests" )]
[assembly: AssemblyDescription( "K3458 Virtual Instrument Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K3458.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
