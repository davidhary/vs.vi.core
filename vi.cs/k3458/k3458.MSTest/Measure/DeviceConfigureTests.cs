using System;

using Microsoft.VisualBasic.CompilerServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K3458.MSTest
{

    /// <summary> K3458 Device Configuration unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3458" )]
    public class DeviceConfigureTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new TestSite();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
                InitializeClass();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
            CleanupClass();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// 	''' <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// 	''' <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// 	''' Gets the test context which provides information about and functionality for the current test
        /// 	''' run.
        /// 	''' </summary>
        /// 	''' <value> The test context. </value>
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// 	''' <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " ADDITIONAL TEST ATTRIBUTES "

        /// <summary> Gets or sets the device. </summary>
        /// 	''' <value> The device. </value>
        public static K3458Device Device { get; set; }

        /// <summary> Initialize class. </summary>
        /// 	''' <remarks> David, 2020-10-12. </remarks>
        private static void InitializeClass()
        {
            Device = new K3458Device();
            _ = Device.TryOpenSession( ResourceSettings.Get().ResourceName, ResourceSettings.Get().ResourceTitle );
        }

        /// <summary> Cleanup class. </summary>
        /// 	''' <remarks> David, 2020-10-12. </remarks>
        private static void CleanupClass()
        {
            if ( Device is object )
            {
                if ( Device.IsDeviceOpen )
                {
                    Device.CloseSession();
                }

                Device.Dispose();
                Device = null;
            }
        }

        #endregion

        #region " DEVICE CONFIGURATION TESTS "

        /// <summary> (Unit Test Method) queries if a given device should be open. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DeviceShouldBeOpen()
        {
            bool expectedBoolean = true;
            bool actualBoolean = Device.IsDeviceOpen;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"Open {ResourceSettings.Get().ResourceName}" );
        }

        /// <summary> (Unit Test Method) device configuration should pass. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DeviceConfigurationShouldPass()
        {
            bool expectedBoolean = true;
            bool actualBoolean = Device.IsDeviceOpen;
            Assert.AreEqual( expectedBoolean, actualBoolean, $"Open {ResourceSettings.Get().ResourceName}" );

            // ! AUTO ZERO ONCE RETURNS OFF
            AutoZeroMode expectedAutoZeroMode = ( AutoZeroMode ) Conversions.ToInteger( TestAssist.AutoZero );
            var actualAutoZeroMode = Device.MeasureSubsystem.ApplyAutoZeroMode( expectedAutoZeroMode );
            Assert.AreEqual( true, actualAutoZeroMode.HasValue, $"Auto zero mode has value" );
            Assert.AreEqual( expectedAutoZeroMode, actualAutoZeroMode.Value, $"Auto zero mode applied" );
            SenseFunctionModes expectedSenseFunctionMode = ( SenseFunctionModes ) Conversions.ToInteger( TestAssist.SenseFunction );
            var actualSenseFunctionMode = Device.MeasureSubsystem.ApplyFunctionMode( expectedSenseFunctionMode );
            Assert.AreEqual( true, actualSenseFunctionMode.HasValue, $"Sense function mode has value" );
            Assert.AreEqual( expectedSenseFunctionMode, actualSenseFunctionMode.Value, $"Sense function mode applied" );
            double expectedDouble = TestAssist.PowerLineCycles;
            var actualDouble = Device.MeasureSubsystem.ApplyPowerLineCycles( expectedDouble );
            Assert.AreEqual( true, actualDouble.HasValue, $"Power line cycles has value" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Power line cycles applied" );
            MathMode expectedMathMode = ( MathMode ) Conversions.ToInteger( TestAssist.MathMode );
            var actualMathMode = Device.MeasureSubsystem.ApplyMathMode( expectedMathMode );
            Assert.AreEqual( true, actualMathMode.HasValue, $"Math mode has value" );
            Assert.AreEqual( expectedMathMode, actualMathMode.Value, $"Math mode applied" );
            StoreMathRegister expectedStoreMathRegister = ( StoreMathRegister ) Conversions.ToInteger( TestAssist.StoreMathRegister );
            expectedDouble = TestAssist.StoreMathValue;
            var actualStoreMathRegister = Device.MeasureSubsystem.WriteStoreMath( expectedStoreMathRegister, expectedDouble );
            actualDouble = Device.MeasureSubsystem.StoreMathValue;
            Assert.AreEqual( true, actualStoreMathRegister.HasValue, $"Store math register has value" );
            Assert.AreEqual( expectedStoreMathRegister, actualStoreMathRegister.Value, $"Store Math Register applied" );
            Assert.AreEqual( true, actualDouble.HasValue, $"Store math value has value" );
            Assert.AreEqual( expectedDouble, actualDouble.Value, $"Store Math value applied" );
        }

        #endregion

    }
}
