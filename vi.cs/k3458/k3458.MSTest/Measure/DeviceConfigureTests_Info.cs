using System;
using System.Configuration;

namespace isr.VI.K3458.MSTest
{
    internal sealed partial class TestAssist
    {

        #region " DEVICE INFORMATION "

        /// <summary> Gets the auto zero settings. </summary>
        /// <value> The auto zero settings. </value>
        public static int AutoZero => Convert.ToInt32( ConfigurationManager.AppSettings[nameof( AutoZero )] );

        /// <summary> Gets the Sense Function settings. </summary>
        /// <value> The Sense Function settings. </value>
        public static int SenseFunction => Convert.ToInt32( ConfigurationManager.AppSettings[nameof( SenseFunction )] );

        /// <summary> Gets the power line cycles settings. </summary>
        /// <value> The power line cycles settings. </value>
        public static double PowerLineCycles => Convert.ToInt32( ConfigurationManager.AppSettings[nameof( PowerLineCycles )] );

        /// <summary> Gets the math mode settings. </summary>
        /// <value> The math mode settings. </value>
        public static int MathMode => Convert.ToInt32( ConfigurationManager.AppSettings[nameof( MathMode )] );

        /// <summary> Gets the store mathematics register. </summary>
        /// <value> The store  mathematics register. </value>
        public static int StoreMathRegister => Convert.ToInt32( ConfigurationManager.AppSettings[nameof( StoreMathRegister )] );

        /// <summary> Gets the store math value settings. </summary>
        /// <value> The store math value settings. </value>
        public static double StoreMathValue => Convert.ToInt32( ConfigurationManager.AppSettings[nameof( StoreMathValue )] );

        #endregion

    }
}
