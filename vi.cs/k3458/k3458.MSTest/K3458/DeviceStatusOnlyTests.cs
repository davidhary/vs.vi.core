using System;

using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace isr.VI.K3458.MSTest
{

    /// <summary> K3458 Device status only unit tests. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-10 </para>
    /// </remarks>
    [TestClass()]
    [TestCategory( "k3458" )]
    public class DeviceStatusOnlyTests
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> My class initialize. </summary>
        /// <remarks>
        /// Use ClassInitialize to run code before running the first test in the class.
        /// </remarks>
        /// <param name="testContext"> Gets or sets the test context which provides information about
        ///                            and functionality for the current test run. </param>
        [ClassInitialize()]
        [CLSCompliant( false )]
        public static void MyClassInitialize( TestContext testContext )
        {
            try
            {
                Console.Out.WriteLine( $"{testContext.FullyQualifiedTestClassName} {DateTime.Now:o}" );
                TestInfo = new();
                TestInfo.AddTraceMessagesQueue( TestInfo.TraceMessagesQueueListener );
                TestInfo.AddTraceMessagesQueue( VI.My.MyLibrary.UnpublishedTraceMessages );
                TestInfo.InitializeTraceListener();
            }
            catch
            {
                // cleanup to meet strong guarantees
                try
                {
                    MyClassCleanup();
                }
                finally
                {
                }

                throw;
            }
        }

        /// <summary> My class cleanup. </summary>
        /// <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
        [ClassCleanup()]
        public static void MyClassCleanup()
        {
            TestInfo?.Dispose();
        }

        /// <summary> Initializes before each test runs. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestInitialize()]
        public void MyTestInitialize()
        {
            // assert reading of test settings from the configuration file.
            Assert.IsTrue( TestInfo.Exists, $"{nameof( TestInfo )} settings should exist" );
            double expectedUpperLimit = 12d;
            Assert.IsTrue( Math.Abs( TestInfo.TimeZoneOffset ) < expectedUpperLimit, $"{nameof( TestSite.TimeZoneOffset )} should be lower than {expectedUpperLimit}" );
            Assert.IsTrue( ResourceSettings.Get().Exists, $"{typeof( ResourceSettings )} settings should exist" );
            _ = TestInfo.ClearMessageQueue();
        }

        /// <summary> Cleans up after each test has run. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestCleanup()]
        public void MyTestCleanup()
        {
            TestInfo.AssertMessageQueue();
        }

        /// <summary>
        /// Gets the test context which provides information about and functionality for the current test
        /// run.
        /// </summary>
        /// <value> The test context. </value>
        [CLSCompliant( false )]
        public TestContext TestContext { get; set; }

        /// <summary> Gets information describing the test. </summary>
        /// <value> Information describing the test. </value>
        private static TestSite TestInfo { get; set; }

        #endregion

        #region " DEVICE EVENT TESTS "

        /// <summary> (Unit Test Method) device should open without device errors. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DeviceShouldOpenWithoutDeviceErrors()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K3458Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) message available should wait for. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void MessageAvailableShouldWaitFor()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K3458Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertMessageAvailableShouldWaitFor( TestInfo, device.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) service request handling should toggle. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ServiceRequestHandlingShouldToggle()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K3458Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertServiceRequestHandlingShouldToggle( TestInfo, device.Session );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) service request should be handled by session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ServiceRequestShouldBeHandledBySession()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K3458Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertServiceRequestShouldBeHandledBySession( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) service request should be handled by device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void ServiceRequestShouldBeHandledByDevice()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K3458Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertServiceRequestShouldBeHandledByDevice( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        /// <summary> (Unit Test Method) device should be polled. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [TestMethod()]
        public void DeviceShouldBePolled()
        {
            if ( !ResourceSettings.Get().ResourcePinged )
                Assert.Inconclusive( $"{ResourceSettings.Get().ResourceTitle} not found" );
            using var device = K3458Device.Create();
            device.AddListener( TestInfo.TraceMessagesQueueListener );
            try
            {
                device.SubsystemSupportMode = SubsystemSupportMode.StatusOnly;
                VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors( TestInfo, device, ResourceSettings.Get() );
                VI.DeviceTests.DeviceManager.AssertDeviceShouldBePolled( device );
            }
            catch
            {
                throw;
            }
            finally
            {
                DeviceManager.CloseSession( TestInfo, device );
            }
        }

        #endregion

    }
}
