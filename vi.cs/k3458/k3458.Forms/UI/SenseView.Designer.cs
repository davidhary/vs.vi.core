﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K3458.Forms
{
    [DesignerGenerated()]
    public partial class SenseView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            _Layout = new System.Windows.Forms.TableLayoutPanel();
            _Panel = new System.Windows.Forms.Panel();
            _TriggerDelayNumeric = new System.Windows.Forms.NumericUpDown();
            _SenseRangeNumeric = new System.Windows.Forms.NumericUpDown();
            _IntegrationPeriodNumeric = new System.Windows.Forms.NumericUpDown();
            _TriggerDelayNumericLabel = new System.Windows.Forms.Label();
            _SenseRangeNumericLabel = new System.Windows.Forms.Label();
            _IntegrationPeriodNumericLabel = new System.Windows.Forms.Label();
            __SenseFunctionComboBox = new System.Windows.Forms.ComboBox();
            __SenseFunctionComboBox.SelectedIndexChanged += new EventHandler(SenseFunctionComboBox_SelectedIndexChanged);
            _SenseFunctionComboBoxLabel = new System.Windows.Forms.Label();
            _SenseAutoRangeToggle = new System.Windows.Forms.CheckBox();
            __ApplySenseSettingsButton = new System.Windows.Forms.Button();
            __ApplySenseSettingsButton.Click += new EventHandler(ApplySenseSettingsButton_Click);
            _Layout.SuspendLayout();
            _Panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)_IntegrationPeriodNumeric).BeginInit();
            SuspendLayout();
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            _Layout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_Panel, 1, 1);
            _Layout.Dock = System.Windows.Forms.DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 3;
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            _Layout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0f));
            _Layout.Size = new System.Drawing.Size(369, 345);
            _Layout.TabIndex = 0;
            // 
            // _Panel
            // 
            _Panel.Controls.Add(_TriggerDelayNumeric);
            _Panel.Controls.Add(_SenseRangeNumeric);
            _Panel.Controls.Add(_IntegrationPeriodNumeric);
            _Panel.Controls.Add(_TriggerDelayNumericLabel);
            _Panel.Controls.Add(_SenseRangeNumericLabel);
            _Panel.Controls.Add(_IntegrationPeriodNumericLabel);
            _Panel.Controls.Add(__SenseFunctionComboBox);
            _Panel.Controls.Add(_SenseFunctionComboBoxLabel);
            _Panel.Controls.Add(_SenseAutoRangeToggle);
            _Panel.Controls.Add(__ApplySenseSettingsButton);
            _Panel.Location = new System.Drawing.Point(12, 48);
            _Panel.Name = "_Panel";
            _Panel.Size = new System.Drawing.Size(345, 249);
            _Panel.TabIndex = 0;
            // 
            // _TriggerDelayNumeric
            // 
            _TriggerDelayNumeric.DecimalPlaces = 3;
            _TriggerDelayNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _TriggerDelayNumeric.Location = new System.Drawing.Point(118, 120);
            _TriggerDelayNumeric.Name = "_TriggerDelayNumeric";
            _TriggerDelayNumeric.Size = new System.Drawing.Size(76, 25);
            _TriggerDelayNumeric.TabIndex = 18;
            // 
            // _SenseRangeNumeric
            // 
            _SenseRangeNumeric.DecimalPlaces = 3;
            _SenseRangeNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SenseRangeNumeric.Location = new System.Drawing.Point(118, 86);
            _SenseRangeNumeric.Maximum = new decimal(new int[] { 1010, 0, 0, 0 });
            _SenseRangeNumeric.Name = "_SenseRangeNumeric";
            _SenseRangeNumeric.Size = new System.Drawing.Size(76, 25);
            _SenseRangeNumeric.TabIndex = 15;
            _SenseRangeNumeric.Value = new decimal(new int[] { 105, 0, 0, 196608 });
            // 
            // _IntegrationPeriodNumeric
            // 
            _IntegrationPeriodNumeric.DecimalPlaces = 3;
            _IntegrationPeriodNumeric.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _IntegrationPeriodNumeric.Increment = new decimal(new int[] { 1, 0, 0, 65536 });
            _IntegrationPeriodNumeric.Location = new System.Drawing.Point(118, 52);
            _IntegrationPeriodNumeric.Maximum = new decimal(new int[] { 16667, 0, 0, 131072 });
            _IntegrationPeriodNumeric.Minimum = new decimal(new int[] { 16, 0, 0, 196608 });
            _IntegrationPeriodNumeric.Name = "_IntegrationPeriodNumeric";
            _IntegrationPeriodNumeric.Size = new System.Drawing.Size(76, 25);
            _IntegrationPeriodNumeric.TabIndex = 13;
            _IntegrationPeriodNumeric.Value = new decimal(new int[] { 16667, 0, 0, 196608 });
            // 
            // _TriggerDelayNumericLabel
            // 
            _TriggerDelayNumericLabel.AutoSize = true;
            _TriggerDelayNumericLabel.Location = new System.Drawing.Point(8, 124);
            _TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel";
            _TriggerDelayNumericLabel.Size = new System.Drawing.Size(107, 17);
            _TriggerDelayNumericLabel.TabIndex = 17;
            _TriggerDelayNumericLabel.Text = "Trigger Delay [s]:";
            _TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseRangeNumericLabel
            // 
            _SenseRangeNumericLabel.AutoSize = true;
            _SenseRangeNumericLabel.Location = new System.Drawing.Point(48, 90);
            _SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel";
            _SenseRangeNumericLabel.Size = new System.Drawing.Size(68, 17);
            _SenseRangeNumericLabel.TabIndex = 14;
            _SenseRangeNumericLabel.Text = "Range [V]:";
            _SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _IntegrationPeriodNumericLabel
            // 
            _IntegrationPeriodNumericLabel.AutoSize = true;
            _IntegrationPeriodNumericLabel.Location = new System.Drawing.Point(25, 56);
            _IntegrationPeriodNumericLabel.Name = "_IntegrationPeriodNumericLabel";
            _IntegrationPeriodNumericLabel.Size = new System.Drawing.Size(91, 17);
            _IntegrationPeriodNumericLabel.TabIndex = 12;
            _IntegrationPeriodNumericLabel.Text = "Aperture [ms]:";
            _IntegrationPeriodNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseFunctionComboBox
            // 
            __SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            __SenseFunctionComboBox.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __SenseFunctionComboBox.Location = new System.Drawing.Point(118, 18);
            __SenseFunctionComboBox.Name = "__SenseFunctionComboBox";
            __SenseFunctionComboBox.Size = new System.Drawing.Size(220, 25);
            __SenseFunctionComboBox.TabIndex = 11;
            // 
            // _SenseFunctionComboBoxLabel
            // 
            _SenseFunctionComboBoxLabel.AutoSize = true;
            _SenseFunctionComboBoxLabel.Location = new System.Drawing.Point(57, 22);
            _SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel";
            _SenseFunctionComboBoxLabel.Size = new System.Drawing.Size(59, 17);
            _SenseFunctionComboBoxLabel.TabIndex = 10;
            _SenseFunctionComboBoxLabel.Text = "Function:";
            _SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // _SenseAutoRangeToggle
            // 
            _SenseAutoRangeToggle.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            _SenseAutoRangeToggle.Location = new System.Drawing.Point(200, 88);
            _SenseAutoRangeToggle.Name = "_SenseAutoRangeToggle";
            _SenseAutoRangeToggle.Size = new System.Drawing.Size(103, 21);
            _SenseAutoRangeToggle.TabIndex = 16;
            _SenseAutoRangeToggle.Text = "Auto Range";
            _SenseAutoRangeToggle.UseVisualStyleBackColor = true;
            // 
            // _ApplySenseSettingsButton
            // 
            __ApplySenseSettingsButton.Anchor = System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right;
            __ApplySenseSettingsButton.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Bold);
            __ApplySenseSettingsButton.Location = new System.Drawing.Point(280, 207);
            __ApplySenseSettingsButton.Name = "__ApplySenseSettingsButton";
            __ApplySenseSettingsButton.Size = new System.Drawing.Size(58, 30);
            __ApplySenseSettingsButton.TabIndex = 19;
            __ApplySenseSettingsButton.Text = "&Apply";
            __ApplySenseSettingsButton.UseVisualStyleBackColor = true;
            // 
            // SenseView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            Controls.Add(_Layout);
            Name = "SenseView";
            Padding = new System.Windows.Forms.Padding(1);
            Size = new System.Drawing.Size(369, 345);
            _Layout.ResumeLayout(false);
            _Panel.ResumeLayout(false);
            _Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)_TriggerDelayNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_SenseRangeNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)_IntegrationPeriodNumeric).EndInit();
            ResumeLayout(false);
        }

        private System.Windows.Forms.TableLayoutPanel _Layout;
        private System.Windows.Forms.Panel _Panel;
        private System.Windows.Forms.NumericUpDown _TriggerDelayNumeric;
        private System.Windows.Forms.NumericUpDown _SenseRangeNumeric;
        private System.Windows.Forms.NumericUpDown _IntegrationPeriodNumeric;
        private System.Windows.Forms.Label _TriggerDelayNumericLabel;
        private System.Windows.Forms.Label _SenseRangeNumericLabel;
        private System.Windows.Forms.Label _IntegrationPeriodNumericLabel;
        private System.Windows.Forms.ComboBox __SenseFunctionComboBox;

        private System.Windows.Forms.ComboBox _SenseFunctionComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SenseFunctionComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged -= SenseFunctionComboBox_SelectedIndexChanged;
                }

                __SenseFunctionComboBox = value;
                if (__SenseFunctionComboBox != null)
                {
                    __SenseFunctionComboBox.SelectedIndexChanged += SenseFunctionComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.Label _SenseFunctionComboBoxLabel;
        private System.Windows.Forms.CheckBox _SenseAutoRangeToggle;
        private System.Windows.Forms.Button __ApplySenseSettingsButton;

        private System.Windows.Forms.Button _ApplySenseSettingsButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ApplySenseSettingsButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click -= ApplySenseSettingsButton_Click;
                }

                __ApplySenseSettingsButton = value;
                if (__ApplySenseSettingsButton != null)
                {
                    __ApplySenseSettingsButton.Click += ApplySenseSettingsButton_Click;
                }
            }
        }
    }
}