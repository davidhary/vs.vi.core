using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.WinForms.NumericUpDownExtensions;
using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;

namespace isr.VI.K3458.Forms
{

    /// <summary> A measure view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class SenseView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public SenseView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__SenseFunctionComboBox.Name = "_SenseFunctionComboBox";
            this.__ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton";
        }

        /// <summary> Creates a new <see cref="SenseView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static SenseView Create()
        {
            SenseView view = null;
            try
            {
                view = new SenseView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3458Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K3458Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( value.Talker );
            }

            this.BindSenseSubsystem( value );
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K3458Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE "

        /// <summary> Gets the Sense subsystem. </summary>
        /// <value> The Sense subsystem. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public MeasureSubsystem SenseSubsystem { get; private set; }

        /// <summary> Bind Sense subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void BindSenseSubsystem( K3458Device device )
        {
            if ( this.SenseSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseSubsystem );
                this.SenseSubsystem = null;
            }

            if ( device is object )
            {
                this.SenseSubsystem = device.MeasureSubsystem;
                this.BindSubsystem( true, this.SenseSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, MeasureSubsystem subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseSubsystemPropertyChanged;
                this.HandlePropertyChanged( subsystem, nameof( MeasureSubsystem.SupportedFunctionModes ) );
                this.HandlePropertyChanged( subsystem, nameof( MeasureSubsystem.FunctionMode ) );
                this.HandlePropertyChanged( subsystem, nameof( MeasureSubsystem.FunctionRange ) );
                this.HandlePropertyChanged( subsystem, nameof( MeasureSubsystem.FunctionRangeDecimalPlaces ) );
                this.HandlePropertyChanged( subsystem, nameof( MeasureSubsystem.FunctionUnit ) );
            }
            else
            {
                subsystem.PropertyChanged -= this.SenseSubsystemPropertyChanged;
                this.BindSenseFunctionSubsystem( null );
            }
        }

        /// <summary> Gets the selected function mode. </summary>
        /// <value> The selected function mode. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private SenseFunctionModes SelectedFunctionMode => this._SenseFunctionComboBox.SelectedSenseFunctionModes();

        /// <summary> Handles the supported function modes changed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void HandleSupportedFunctionModesChanged( MeasureSubsystem subsystem )
        {
            if ( subsystem is object && subsystem.SupportedFunctionModes != SenseFunctionModes.None )
            {
                bool init = this.InitializingComponents;
                try
                {
                    this.InitializingComponents = true;
                    this._SenseFunctionComboBox.ListSupportedSenseFunctionModes( subsystem.SupportedFunctionModes );
                }
                catch
                {
                    throw;
                }
                finally
                {
                    this.InitializingComponents = init;
                }
            }
        }

        /// <summary> Handles the function modes changed described by subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void HandleFunctionModesChanged( MeasureSubsystem subsystem )
        {
            _ = this._SenseFunctionComboBox.SelectSenseFunctionModes( subsystem.FunctionMode.GetValueOrDefault( SenseFunctionModes.VoltageDC ) );
            var value = subsystem.FunctionMode.GetValueOrDefault( SenseFunctionModes.VoltageDC );
            // this is done at the device level by applying the function mode to the measure subsystem
            // Me.Device.MeasureSubsystem.Readings.Reading.ApplyUnit(subsystem.ToUnit(value))
            _ = this._SenseFunctionComboBox.SafeSelectSenseFunctionModes( value );
            switch ( subsystem.FunctionMode )
            {
                case SenseFunctionModes.CurrentDC:
                    {
                        break;
                    }
                // Me.BindSenseFunctionSubsystem(Me.Device.SenseCurrentSubsystem)
                case SenseFunctionModes.VoltageDC:
                    {
                        break;
                    }
                // Me.BindSenseFunctionSubsystem(Me.Device.SenseVoltageSubsystem)
                case SenseFunctionModes.ResistanceFourWire:
                    {
                        break;
                    }
                // Me.BindSenseFunctionSubsystem(Me.Device.SenseResistanceFourWireSubsystem)
                case SenseFunctionModes.Resistance:
                    {
                        break;
                    }
                    // Me.BindSenseFunctionSubsystem(Me.Device.SenseResistanceSubsystem)
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( MeasureSubsystem subsystem, string propertyName )
        {
            if ( this.InitializingComponents || subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( MeasureSubsystem.FunctionMode ):
                    {
                        this.HandleFunctionModesChanged( subsystem );
                        break;
                    }

                case nameof( MeasureSubsystem.FunctionRange ):
                    {
                        _ = this._SenseRangeNumeric.RangeSetter( subsystem.FunctionRange.Min, subsystem.FunctionRange.Max );
                        break;
                    }

                case nameof( MeasureSubsystem.FunctionRangeDecimalPlaces ):
                    {
                        this._SenseRangeNumeric.DecimalPlaces = subsystem.FunctionRangeDecimalPlaces;
                        break;
                    }

                case nameof( MeasureSubsystem.FunctionUnit ):
                    {
                        this._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:";
                        this._SenseRangeNumericLabel.Left = this._SenseRangeNumeric.Left - this._SenseRangeNumericLabel.Width;
                        break;
                    }

                case nameof( MeasureSubsystem.SupportedFunctionModes ):
                    {
                        this.HandleSupportedFunctionModesChanged( subsystem );
                        break;
                    }
            }
        }

        /// <summary> Sense subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( MeasureSubsystem )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as MeasureSubsystem, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " SENSE FUNCTION "

        /// <summary> Gets or sets the sense function subsystem. </summary>
        /// <value> The sense function subsystem. </value>
        public SenseFunctionSubsystemBase SenseFunctionSubsystem { get; private set; }

        /// <summary> Bind Sense function subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSenseFunctionSubsystem( SenseFunctionSubsystemBase subsystem )
        {
            if ( this.SenseFunctionSubsystem is object )
            {
                this.BindSubsystem( false, this.SenseFunctionSubsystem );
                this.SenseFunctionSubsystem = null;
            }

            this.SenseFunctionSubsystem = subsystem;
            if ( subsystem is object )
            {
                this.BindSubsystem( true, this.SenseFunctionSubsystem );
            }
        }

        /// <summary> Bind subsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="subsystem"> The subsystem. </param>
        private void BindSubsystem( bool add, SenseFunctionSubsystemBase subsystem )
        {
            if ( add )
            {
                subsystem.PropertyChanged += this.SenseFunctionSubsystemPropertyChanged;
            }
            // must Not read setting when biding because the instrument may be locked Or in a trigger mode
            // The bound values should be sent when binding Or when applying propert change.
            // TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            // subsystem.QueryAutoRangeEnabled()
            // subsystem.QueryPowerLineCycles()
            // subsystem.QueryRange()
            else
            {
                subsystem.PropertyChanged -= this.SenseFunctionSubsystemPropertyChanged;
            }
        }

        /// <summary> Handle the Sense subsystem property changed event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="subsystem">    The subsystem. </param>
        /// <param name="propertyName"> Name of the property. </param>
        private void HandlePropertyChanged( SenseFunctionSubsystemBase subsystem, string propertyName )
        {
            if ( subsystem is null || string.IsNullOrWhiteSpace( propertyName ) )
                return;
            // Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
            // Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
            switch ( propertyName ?? "" )
            {
                case nameof( SenseFunctionSubsystemBase.AutoRangeEnabled ):
                    {
                        if ( this.Device is object && subsystem.AutoRangeEnabled.HasValue )
                        {
                            this._SenseAutoRangeToggle.Checked = subsystem.AutoRangeEnabled.Value;
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.PowerLineCycles ):
                    {
                        if ( this.Device is object && subsystem.PowerLineCycles.HasValue )
                        {
                            double nplc = subsystem.PowerLineCycles.Value;
                            _ = this._IntegrationPeriodNumeric.ValueSetter( StatusSubsystemBase.FromPowerLineCycles( nplc ).TotalMilliseconds );
                        }

                        break;
                    }

                case nameof( SenseFunctionSubsystemBase.Range ):
                    {
                        if ( this.Device is object && subsystem.Range.HasValue )
                        {
                            _ = this._SenseRangeNumeric.ValueSetter( subsystem.Range.Value );
                        }

                        break;
                    }
            }
        }

        /// <summary> Sense function subsystem property changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property Changed event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void SenseFunctionSubsystemPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"handling {nameof( SenseFunctionSubsystemBase )}.{e.PropertyName} change";
            try
            {
                if ( this.InvokeRequired )
                {
                    _ = this.Invoke( new Action<object, PropertyChangedEventArgs>( this.SenseFunctionSubsystemPropertyChanged ), new object[] { sender, e } );
                }
                else
                {
                    this.HandlePropertyChanged( sender as SenseFunctionSubsystemBase, e.PropertyName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: SENSE "

        /// <summary>
        /// Event handler. Called by _SenseFunctionComboBox for selected index changed events.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SenseFunctionComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            if ( sender is Control )
            {
                // !@# Me.Device.MeasureSubsystem.ApplyFunctionMode(Me.SelectedFunctionMode)
            }
        }

        /// <summary> Applies the selected measurements settings. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private void ApplySenseSettings()
        {
            _ = this.SenseFunctionSubsystem.ApplyPowerLineCycles( StatusSubsystemBase.ToPowerLineCycles( TimeSpan.FromMilliseconds( ( double ) this._IntegrationPeriodNumeric.Value ) ) );
            _ = this.SenseFunctionSubsystem.ApplyAutoRangeEnabled( this._SenseAutoRangeToggle.Checked );
            if ( !this._SenseAutoRangeToggle.Checked )
                _ = this.SenseFunctionSubsystem.ApplyRange( ( double ) this._SenseRangeNumeric.Value );
        }

        /// <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ApplySenseSettingsButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} applying sense settings";
                this.ApplySenseSettings();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
