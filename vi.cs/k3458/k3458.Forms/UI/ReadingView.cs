using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;
using isr.VI.Facade.ComboBoxExtensions;
using isr.VI.Facade.DataGridViewExtensions;

namespace isr.VI.K3458.Forms
{

    /// <summary> A reading view. </summary>
    /// <remarks>
    /// (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2018-12-31 </para>
    /// </remarks>
    public partial class ReadingView : Core.Forma.ModelViewTalkerBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public ReadingView() : base()
        {
            this.InitializingComponents = true;
            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this.__ReadButton.Name = "_ReadButton";
            this.__ReadingComboBox.Name = "_ReadingComboBox";
            this.__ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton";
            this.__ReadingsDataGridView.Name = "_ReadingsDataGridView";
        }

        /// <summary> Creates a new <see cref="ReadingView"/> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A <see cref="ReadingView"/>. </returns>
        public static ReadingView Create()
        {
            ReadingView view = null;
            try
            {
                view = new ReadingView();
                return view;
            }
            catch
            {
                view.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        ///                                                   <c>False</c> to release only unmanaged
        ///                                                   resources when called from the runtime
        ///                                                   finalize. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.IsDisposed )
                return;
            try
            {
                if ( disposing )
                {
                    this.InitializingComponents = true;
                    // make sure the device is unbound in case the form is closed without closing the device.
                    this.AssignDeviceThis( null );
                    if ( this.components is object )
                    {
                        this.components.Dispose();
                        this.components = null;
                    }
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENTS "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            try
            {
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3458Device Device { get; private set; }

        /// <summary> Assigns the device and binds the relevant subsystem values. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        private void AssignDeviceThis( K3458Device value )
        {
            if ( this.Device is object )
            {
                this.AssignTalker( null );
                this.Device = null;
            }

            this.Device = value;
            if ( value is object )
            {
                this.AssignTalker( value.Talker );
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> True to show or False to hide the control. </param>
        public void AssignDevice( K3458Device value )
        {
            this.AssignDeviceThis( value );
        }

        /// <summary> Reads the status register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected void ReadStatusRegister()
        {
            string activity = $"{this.Device.ResourceNameCaption} reading service request";
            try
            {
                _ = this.Device.Session.ReadStatusRegister();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS: READING "

        #region " READ "

        /// <summary> Selects a new reading to display. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> The VI.ReadingElementTypes. </returns>
        internal ReadingElementTypes SelectReading( ReadingElementTypes value )
        {
            return this._ReadingComboBox.ComboBox.SafeSelectReadingElementType( value );
        }

        /// <summary> Gets the type of the selected reading. </summary>
        /// <value> The type of the selected reading. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        private ReadingElementTypes SelectedReadingType => this._ReadingComboBox.ComboBox.SelectedReadingElementType();

        /// <summary> Reading combo box selected index changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/> instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadingComboBox_SelectedIndexChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} selecting a reading to display";
                _ = this.PublishInfo( $"{activity};. " );
                this.Device.MeasureSubsystem.SelectActiveReading( this.SelectedReadingType );
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Read button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                activity = $"{this.Device.ResourceNameCaption} reading";
                _ = this.PublishInfo( $"{activity};. " );
                // !@# Me.Device.SystemSubsystem.QueryFrontTerminalsSelected()
                this.Device.MeasureSubsystem.StartElapsedStopwatch();
                // Me.Device.MeasureSubsystem.MeasureValue()
                _ = this.Device.MeasureSubsystem.Read();
                this.Device.MeasureSubsystem.StopElapsedStopwatch();
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString()}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " BUFFER "

        /// <summary> Handles the DataError event of the _dataGridView control. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
        ///                       event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ReadingsDataGridView_DataError( object sender, DataGridViewDataErrorEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                // prevent error reporting when adding a new row or editing a cell
                if ( sender is DataGridView grid )
                {
                    if ( grid.CurrentRow is object && grid.CurrentRow.IsNewRow )
                        return;
                    if ( grid.IsCurrentCellInEditMode )
                        return;
                    if ( grid.IsCurrentRowDirty )
                        return;
                    _ = this.PublishException( activity, e.Exception );
                    activity = $"{this.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString()}";
                    _ = this.PublishVerbose( activity );
                    _ = this.InfoProvider.Annunciate( grid, Core.Forma.InfoProviderLevel.Error, activity );
                }
            }
            catch
            {
            }
        }

        /// <summary> Clears the buffer display button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> <see cref="System.Object"/>
        ///                                             instance of this
        ///                                             <see cref="System.Windows.Forms.Control"/> </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ClearBufferDisplayButton_Click( object sender, EventArgs e )
        {
            if ( this.InitializingComponents || sender is null || e is null )
                return;
            string activity = $"{this.Device.ResourceNameCaption} clearing buffer display";
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.InfoProvider.Clear();
                _ = this.PublishInfo( $"{activity};. " );
                _ = this._ReadingsDataGridView.Display( new List<Readings>(), false );
                this._ReadingsCountLabel.Text = "0";
            }
            catch ( Exception ex )
            {
                _ = this.InfoProvider.Annunciate( sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}" );
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.ReadStatusRegister();
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyLibrary.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
