﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K3458.Forms
{
    [DesignerGenerated()]
    public partial class ReadingView
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadingView));
            _ReadingToolStrip = new System.Windows.Forms.ToolStrip();
            __ReadButton = new System.Windows.Forms.ToolStripButton();
            __ReadButton.Click += new EventHandler(ReadButton_Click);
            __ReadingComboBox = new System.Windows.Forms.ToolStripComboBox();
            __ReadingComboBox.SelectedIndexChanged += new EventHandler(ReadingComboBox_SelectedIndexChanged);
            _AbortButton = new System.Windows.Forms.ToolStripButton();
            _InitiateTriggerButton = new System.Windows.Forms.ToolStripButton();
            _EventsMenuButton = new System.Windows.Forms.ToolStripSplitButton();
            _HandleMeasurementEventMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _AutoInitiateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            _BufferToolStrip = new System.Windows.Forms.ToolStrip();
            _ReadBufferButton = new System.Windows.Forms.ToolStripButton();
            _ReadingsCountLabel = new Core.Controls.ToolStripLabel();
            __ClearBufferDisplayButton = new System.Windows.Forms.ToolStripButton();
            __ClearBufferDisplayButton.Click += new EventHandler(ClearBufferDisplayButton_Click);
            __ReadingsDataGridView = new System.Windows.Forms.DataGridView();
            __ReadingsDataGridView.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(ReadingsDataGridView_DataError);
            _ReadingToolStrip.SuspendLayout();
            _BufferToolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__ReadingsDataGridView).BeginInit();
            SuspendLayout();
            // 
            // _ReadingToolStrip
            // 
            _ReadingToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { __ReadButton, __ReadingComboBox, _AbortButton, _InitiateTriggerButton, _EventsMenuButton });
            _ReadingToolStrip.Location = new System.Drawing.Point(0, 0);
            _ReadingToolStrip.Name = "_ReadingToolStrip";
            _ReadingToolStrip.Size = new System.Drawing.Size(379, 25);
            _ReadingToolStrip.TabIndex = 19;
            _ReadingToolStrip.Text = "ToolStrip1";
            // 
            // _ReadButton
            // 
            __ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ReadButton.Image = (System.Drawing.Image)resources.GetObject("_ReadButton.Image");
            __ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ReadButton.Name = "__ReadButton";
            __ReadButton.Size = new System.Drawing.Size(37, 22);
            __ReadButton.Text = "Read";
            __ReadButton.ToolTipText = "Read single reading";
            // 
            // _ReadingComboBox
            // 
            __ReadingComboBox.Name = "__ReadingComboBox";
            __ReadingComboBox.Size = new System.Drawing.Size(121, 25);
            __ReadingComboBox.Text = "<reading>";
            __ReadingComboBox.ToolTipText = "Select reading type";
            // 
            // _AbortButton
            // 
            _AbortButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _AbortButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _AbortButton.Image = (System.Drawing.Image)resources.GetObject("_AbortButton.Image");
            _AbortButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _AbortButton.Name = "_AbortButton";
            _AbortButton.Size = new System.Drawing.Size(41, 22);
            _AbortButton.Text = "Abort";
            _AbortButton.ToolTipText = "Aborts active trigger";
            // 
            // _InitiateTriggerButton
            // 
            _InitiateTriggerButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _InitiateTriggerButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _InitiateTriggerButton.Image = (System.Drawing.Image)resources.GetObject("_InitiateTriggerButton.Image");
            _InitiateTriggerButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _InitiateTriggerButton.Name = "_InitiateTriggerButton";
            _InitiateTriggerButton.Size = new System.Drawing.Size(47, 22);
            _InitiateTriggerButton.Text = "Initiate";
            _InitiateTriggerButton.ToolTipText = "Initiates a trigger";
            // 
            // _EventsMenuButton
            // 
            _EventsMenuButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            _EventsMenuButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _EventsMenuButton.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] { _HandleMeasurementEventMenuItem, _AutoInitiateMenuItem });
            _EventsMenuButton.Image = (System.Drawing.Image)resources.GetObject("_EventsMenuButton.Image");
            _EventsMenuButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _EventsMenuButton.Name = "_EventsMenuButton";
            _EventsMenuButton.Size = new System.Drawing.Size(57, 22);
            _EventsMenuButton.Text = "Events";
            _EventsMenuButton.ToolTipText = "Enable event handling";
            // 
            // _HandleMeasurementEventMenuItem
            // 
            _HandleMeasurementEventMenuItem.CheckOnClick = true;
            _HandleMeasurementEventMenuItem.Name = "_HandleMeasurementEventMenuItem";
            _HandleMeasurementEventMenuItem.Size = new System.Drawing.Size(147, 22);
            _HandleMeasurementEventMenuItem.Text = "Measurement";
            _HandleMeasurementEventMenuItem.ToolTipText = "Check to handle measurement events";
            // 
            // _AutoInitiateMenuItem
            // 
            _AutoInitiateMenuItem.CheckOnClick = true;
            _AutoInitiateMenuItem.Name = "_AutoInitiateMenuItem";
            _AutoInitiateMenuItem.Size = new System.Drawing.Size(147, 22);
            _AutoInitiateMenuItem.Text = "Auto Initiate";
            _AutoInitiateMenuItem.ToolTipText = "Check to automatically initiates a new trigger upon handling the event";
            // 
            // _BufferToolStrip
            // 
            _BufferToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] { _ReadBufferButton, _ReadingsCountLabel, __ClearBufferDisplayButton });
            _BufferToolStrip.Location = new System.Drawing.Point(0, 25);
            _BufferToolStrip.Name = "_BufferToolStrip";
            _BufferToolStrip.Size = new System.Drawing.Size(379, 25);
            _BufferToolStrip.TabIndex = 22;
            _BufferToolStrip.Text = "ToolStrip1";
            // 
            // _ReadBufferButton
            // 
            _ReadBufferButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            _ReadBufferButton.Image = (System.Drawing.Image)resources.GetObject("_ReadBufferButton.Image");
            _ReadBufferButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            _ReadBufferButton.Name = "_ReadBufferButton";
            _ReadBufferButton.Size = new System.Drawing.Size(72, 22);
            _ReadBufferButton.Text = "Read Buffer";
            _ReadBufferButton.ToolTipText = "Reads the buffer";
            // 
            // _ReadingsCountLabel
            // 
            _ReadingsCountLabel.Name = "_ReadingsCountLabel";
            _ReadingsCountLabel.Size = new System.Drawing.Size(13, 22);
            _ReadingsCountLabel.Text = "0";
            _ReadingsCountLabel.ToolTipText = "Buffer count";
            // 
            // _ClearBufferDisplayButton
            // 
            __ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            __ClearBufferDisplayButton.Font = new System.Drawing.Font("Wingdings", 9.0f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(2));
            __ClearBufferDisplayButton.Image = (System.Drawing.Image)resources.GetObject("_ClearBufferDisplayButton.Image");
            __ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            __ClearBufferDisplayButton.Name = "__ClearBufferDisplayButton";
            __ClearBufferDisplayButton.Size = new System.Drawing.Size(25, 22);
            __ClearBufferDisplayButton.Text = "";
            __ClearBufferDisplayButton.ToolTipText = "Clears the grid";
            // 
            // _ReadingsDataGridView
            // 
            __ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            __ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            __ReadingsDataGridView.Location = new System.Drawing.Point(0, 50);
            __ReadingsDataGridView.Name = "__ReadingsDataGridView";
            __ReadingsDataGridView.Size = new System.Drawing.Size(379, 305);
            __ReadingsDataGridView.TabIndex = 23;
            // 
            // ReadingView
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            Controls.Add(__ReadingsDataGridView);
            Controls.Add(_BufferToolStrip);
            Controls.Add(_ReadingToolStrip);
            Name = "ReadingView";
            Size = new System.Drawing.Size(379, 355);
            _ReadingToolStrip.ResumeLayout(false);
            _ReadingToolStrip.PerformLayout();
            _BufferToolStrip.ResumeLayout(false);
            _BufferToolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__ReadingsDataGridView).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        private System.Windows.Forms.ToolStrip _ReadingToolStrip;
        private System.Windows.Forms.ToolStripButton __ReadButton;

        private System.Windows.Forms.ToolStripButton _ReadButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadButton != null)
                {
                    __ReadButton.Click -= ReadButton_Click;
                }

                __ReadButton = value;
                if (__ReadButton != null)
                {
                    __ReadButton.Click += ReadButton_Click;
                }
            }
        }

        private System.Windows.Forms.ToolStripComboBox __ReadingComboBox;

        private System.Windows.Forms.ToolStripComboBox _ReadingComboBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingComboBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged -= ReadingComboBox_SelectedIndexChanged;
                }

                __ReadingComboBox = value;
                if (__ReadingComboBox != null)
                {
                    __ReadingComboBox.SelectedIndexChanged += ReadingComboBox_SelectedIndexChanged;
                }
            }
        }

        private System.Windows.Forms.ToolStripButton _AbortButton;
        private System.Windows.Forms.ToolStripButton _InitiateTriggerButton;
        private System.Windows.Forms.ToolStripSplitButton _EventsMenuButton;
        private System.Windows.Forms.ToolStripMenuItem _HandleMeasurementEventMenuItem;
        private System.Windows.Forms.ToolStripMenuItem _AutoInitiateMenuItem;
        private System.Windows.Forms.ToolStrip _BufferToolStrip;
        private System.Windows.Forms.ToolStripButton _ReadBufferButton;
        private Core.Controls.ToolStripLabel _ReadingsCountLabel;
        private System.Windows.Forms.ToolStripButton __ClearBufferDisplayButton;

        private System.Windows.Forms.ToolStripButton _ClearBufferDisplayButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ClearBufferDisplayButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ClearBufferDisplayButton != null)
                {
                    __ClearBufferDisplayButton.Click -= ClearBufferDisplayButton_Click;
                }

                __ClearBufferDisplayButton = value;
                if (__ClearBufferDisplayButton != null)
                {
                    __ClearBufferDisplayButton.Click += ClearBufferDisplayButton_Click;
                }
            }
        }

        private System.Windows.Forms.DataGridView __ReadingsDataGridView;

        private System.Windows.Forms.DataGridView _ReadingsDataGridView
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ReadingsDataGridView;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ReadingsDataGridView != null)
                {
                    __ReadingsDataGridView.DataError -= ReadingsDataGridView_DataError;
                }

                __ReadingsDataGridView = value;
                if (__ReadingsDataGridView != null)
                {
                    __ReadingsDataGridView.DataError += ReadingsDataGridView_DataError;
                }
            }
        }
    }
}