using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K3458.Forms
{
    [DesignerGenerated()]
    public partial class MeterView
    {

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            _SelectorOpener = new Core.Controls.SelectorOpener();
            _Layout = new TableLayoutPanel();
            _PartPanel = new Panel();
            __SubmitButton = new Button();
            __SubmitButton.Click += new EventHandler(SubmitButton_Click);
            _OffsetValueNumericLabel = new Label();
            __OffsetValueNumeric = new NumericUpDown();
            __OffsetValueNumeric.Validated += new EventHandler(OffsetValueNumeric_Validated);
            _NominalValueNumericLabel = new Label();
            __NominalValueNumeric = new NumericUpDown();
            __NominalValueNumeric.Validated += new EventHandler(NominalValueNumeric_Validated);
            __ParseButton = new Button();
            __ParseButton.Click += new EventHandler(ParseButton_Click);
            __PartNumberTextBox = new TextBox();
            __PartNumberTextBox.Validated += new EventHandler(PartNumberTextBox_Validated);
            __PartNumberTextBox.KeyUp += new KeyEventHandler(EnterKeyHandler);
            __PartNumberTextBox.KeyDown += new KeyEventHandler(SuppressDingHandler);
            _PartNumberTextBoxLabel = new Label();
            _SelectorLabel = new Label();
            _BottomToolStrip = new ToolStrip();
            _ResourceNameLabel = new ToolStripLabel();
            __AutoConnectCheckBox = new Core.Controls.ToolStripCheckBox();
            __AutoConnectCheckBox.CheckedChanged += new EventHandler<EventArgs>(AutoConnectCheckBox_CheckedChanged);
            _NominalValueLabelLabel = new ToolStripLabel();
            _NominalValueLabel = new ToolStripLabel();
            _NominalOffsetLabelLabel = new ToolStripLabel();
            _NominalOffsetLabel = new ToolStripLabel();
            _AssignedValueLabelLabel = new ToolStripLabel();
            _AssignedValueLabel = new ToolStripLabel();
            __AutoStartCheckBox = new Core.Controls.ToolStripCheckBox();
            __AutoStartCheckBox.CheckedChanged += new EventHandler<EventArgs>(AutoStartCheckBox_CheckedChanged);
            _Layout.SuspendLayout();
            _PartPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)__OffsetValueNumeric).BeginInit();
            ((System.ComponentModel.ISupportInitialize)__NominalValueNumeric).BeginInit();
            _BottomToolStrip.SuspendLayout();
            SuspendLayout();
            // 
            // _ResourceSelectorConnector
            // 
            _SelectorOpener.BackColor = System.Drawing.Color.Transparent;
            _SelectorOpener.Dock = DockStyle.Top;
            _SelectorOpener.Font = new System.Drawing.Font("Segoe UI", 9.75f, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, Conversions.ToByte(0));
            _SelectorOpener.Location = new System.Drawing.Point(120, 41);
            _SelectorOpener.Margin = new Padding(0);
            _SelectorOpener.Name = "_ResourceSelectorConnector";
            _SelectorOpener.Size = new System.Drawing.Size(522, 29);
            _SelectorOpener.TabIndex = 0;
            // 
            // _Layout
            // 
            _Layout.ColumnCount = 3;
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _Layout.ColumnStyles.Add(new ColumnStyle());
            _Layout.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50.0f));
            _Layout.Controls.Add(_SelectorOpener, 1, 2);
            _Layout.Controls.Add(_PartPanel, 1, 3);
            _Layout.Controls.Add(_SelectorLabel, 1, 1);
            _Layout.Dock = DockStyle.Fill;
            _Layout.Location = new System.Drawing.Point(0, 0);
            _Layout.Name = "_Layout";
            _Layout.RowCount = 5;
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 6.0f));
            _Layout.RowStyles.Add(new RowStyle(SizeType.Percent, 100.0f));
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle());
            _Layout.RowStyles.Add(new RowStyle(SizeType.Absolute, 6.0f));
            _Layout.Size = new System.Drawing.Size(762, 160);
            _Layout.TabIndex = 1;
            // 
            // _PartPanel
            // 
            _PartPanel.Controls.Add(__SubmitButton);
            _PartPanel.Controls.Add(_OffsetValueNumericLabel);
            _PartPanel.Controls.Add(__OffsetValueNumeric);
            _PartPanel.Controls.Add(_NominalValueNumericLabel);
            _PartPanel.Controls.Add(__NominalValueNumeric);
            _PartPanel.Controls.Add(__ParseButton);
            _PartPanel.Controls.Add(__PartNumberTextBox);
            _PartPanel.Controls.Add(_PartNumberTextBoxLabel);
            _PartPanel.Location = new System.Drawing.Point(123, 73);
            _PartPanel.Name = "_PartPanel";
            _PartPanel.Size = new System.Drawing.Size(516, 78);
            _PartPanel.TabIndex = 1;
            // 
            // _SubmitButton
            // 
            __SubmitButton.Location = new System.Drawing.Point(434, 40);
            __SubmitButton.Name = "__SubmitButton";
            __SubmitButton.Size = new System.Drawing.Size(75, 28);
            __SubmitButton.TabIndex = 7;
            __SubmitButton.Text = "&SUBMIT";
            __SubmitButton.UseVisualStyleBackColor = true;
            // 
            // _OffsetValueNumericLabel
            // 
            _OffsetValueNumericLabel.AutoSize = true;
            _OffsetValueNumericLabel.Location = new System.Drawing.Point(214, 46);
            _OffsetValueNumericLabel.Name = "_OffsetValueNumericLabel";
            _OffsetValueNumericLabel.Size = new System.Drawing.Size(46, 17);
            _OffsetValueNumericLabel.TabIndex = 6;
            _OffsetValueNumericLabel.Text = "Offset:";
            // 
            // _OffsetValueNumeric
            // 
            __OffsetValueNumeric.DecimalPlaces = 4;
            __OffsetValueNumeric.Location = new System.Drawing.Point(265, 42);
            __OffsetValueNumeric.Minimum = new decimal(new int[] { 100, 0, 0, (int)-2147483648L });
            __OffsetValueNumeric.Name = "__OffsetValueNumeric";
            __OffsetValueNumeric.Size = new System.Drawing.Size(120, 25);
            __OffsetValueNumeric.TabIndex = 5;
            // 
            // _NominalValueNumericLabel
            // 
            _NominalValueNumericLabel.AutoSize = true;
            _NominalValueNumericLabel.Location = new System.Drawing.Point(6, 46);
            _NominalValueNumericLabel.Name = "_NominalValueNumericLabel";
            _NominalValueNumericLabel.Size = new System.Drawing.Size(60, 17);
            _NominalValueNumericLabel.TabIndex = 4;
            _NominalValueNumericLabel.Text = "Nominal:";
            // 
            // _NominalValueNumeric
            // 
            __NominalValueNumeric.DecimalPlaces = 3;
            __NominalValueNumeric.Location = new System.Drawing.Point(69, 42);
            __NominalValueNumeric.Maximum = new decimal(new int[] { 1000000000, 0, 0, 0 });
            __NominalValueNumeric.Name = "__NominalValueNumeric";
            __NominalValueNumeric.Size = new System.Drawing.Size(120, 25);
            __NominalValueNumeric.TabIndex = 3;
            // 
            // _ParseButton
            // 
            __ParseButton.Location = new System.Drawing.Point(435, 6);
            __ParseButton.Name = "__ParseButton";
            __ParseButton.Size = new System.Drawing.Size(75, 28);
            __ParseButton.TabIndex = 2;
            __ParseButton.Text = "&PARSE";
            __ParseButton.UseVisualStyleBackColor = true;
            // 
            // _PartNumberTextBox
            // 
            __PartNumberTextBox.Location = new System.Drawing.Point(69, 8);
            __PartNumberTextBox.Name = "__PartNumberTextBox";
            __PartNumberTextBox.Size = new System.Drawing.Size(363, 25);
            __PartNumberTextBox.TabIndex = 1;
            // 
            // _PartNumberTextBoxLabel
            // 
            _PartNumberTextBoxLabel.AutoSize = true;
            _PartNumberTextBoxLabel.Location = new System.Drawing.Point(32, 12);
            _PartNumberTextBoxLabel.Name = "_PartNumberTextBoxLabel";
            _PartNumberTextBoxLabel.Size = new System.Drawing.Size(34, 17);
            _PartNumberTextBoxLabel.TabIndex = 0;
            _PartNumberTextBoxLabel.Text = "Part:";
            // 
            // _SelectorLabel
            // 
            _SelectorLabel.AutoSize = true;
            _SelectorLabel.Dock = DockStyle.Bottom;
            _SelectorLabel.Location = new System.Drawing.Point(123, 24);
            _SelectorLabel.Name = "_SelectorLabel";
            _SelectorLabel.Size = new System.Drawing.Size(516, 17);
            _SelectorLabel.TabIndex = 2;
            _SelectorLabel.Text = "Instrument Resource Name Finder and Connector";
            // 
            // _BottomToolStrip
            // 
            _BottomToolStrip.Dock = DockStyle.Bottom;
            _BottomToolStrip.Items.AddRange(new ToolStripItem[] { _ResourceNameLabel, __AutoConnectCheckBox, _NominalValueLabelLabel, _NominalValueLabel, _NominalOffsetLabelLabel, _NominalOffsetLabel, _AssignedValueLabelLabel, _AssignedValueLabel, __AutoStartCheckBox });
            _BottomToolStrip.Location = new System.Drawing.Point(0, 160);
            _BottomToolStrip.Name = "_BottomToolStrip";
            _BottomToolStrip.Size = new System.Drawing.Size(762, 25);
            _BottomToolStrip.TabIndex = 2;
            _BottomToolStrip.Text = "ToolStrip1";
            // 
            // _ResourceNameLabel
            // 
            _ResourceNameLabel.Name = "_ResourceNameLabel";
            _ResourceNameLabel.Size = new System.Drawing.Size(101, 22);
            _ResourceNameLabel.Text = "<resource name>";
            // 
            // _AutoConnectCheckBox
            // 
            __AutoConnectCheckBox.Checked = false;
            __AutoConnectCheckBox.Margin = new Padding(0, 1, 6, 2);
            __AutoConnectCheckBox.Name = "__AutoConnectCheckBox";
            __AutoConnectCheckBox.Size = new System.Drawing.Size(100, 22);
            __AutoConnectCheckBox.Text = "Auto Connect";
            // 
            // _NominalValueLabelLabel
            // 
            _NominalValueLabelLabel.Name = "_NominalValueLabelLabel";
            _NominalValueLabelLabel.Size = new System.Drawing.Size(56, 22);
            _NominalValueLabelLabel.Text = "Nominal:";
            // 
            // _NominalValueLabel
            // 
            _NominalValueLabel.Name = "_NominalValueLabel";
            _NominalValueLabel.Size = new System.Drawing.Size(29, 22);
            _NominalValueLabel.Text = "<0>";
            _NominalValueLabel.ToolTipText = "Part nominal value";
            // 
            // _NominalOffsetLabelLabel
            // 
            _NominalOffsetLabelLabel.Name = "_NominalOffsetLabelLabel";
            _NominalOffsetLabelLabel.Size = new System.Drawing.Size(15, 22);
            _NominalOffsetLabelLabel.Text = "+";
            // 
            // _NominalOffsetLabel
            // 
            _NominalOffsetLabel.Name = "_NominalOffsetLabel";
            _NominalOffsetLabel.Size = new System.Drawing.Size(29, 22);
            _NominalOffsetLabel.Text = "<0>";
            _NominalOffsetLabel.ToolTipText = "Offset value";
            // 
            // _AssignedValueLabelLabel
            // 
            _AssignedValueLabelLabel.Name = "_AssignedValueLabelLabel";
            _AssignedValueLabelLabel.Size = new System.Drawing.Size(15, 22);
            _AssignedValueLabelLabel.Text = "=";
            // 
            // _AssignedValueLabel
            // 
            _AssignedValueLabel.Name = "_AssignedValueLabel";
            _AssignedValueLabel.Size = new System.Drawing.Size(29, 22);
            _AssignedValueLabel.Text = "<0>";
            _AssignedValueLabel.ToolTipText = "Assigned nominal value";
            // 
            // _AutoStartCheckBox
            // 
            __AutoStartCheckBox.Checked = false;
            __AutoStartCheckBox.Name = "__AutoStartCheckBox";
            __AutoStartCheckBox.Size = new System.Drawing.Size(79, 22);
            __AutoStartCheckBox.Text = "Auto Start";
            __AutoStartCheckBox.ToolTipText = "When checked, the instrument is configured upon entering the part number";
            // 
            // MeterControl
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(_Layout);
            Controls.Add(_BottomToolStrip);
            Name = "MeterControl";
            BorderStyle = BorderStyle.FixedSingle;
            Padding = new Padding(1);
            Size = new System.Drawing.Size(762, 185);
            _Layout.ResumeLayout(false);
            _Layout.PerformLayout();
            _PartPanel.ResumeLayout(false);
            _PartPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)__OffsetValueNumeric).EndInit();
            ((System.ComponentModel.ISupportInitialize)__NominalValueNumeric).EndInit();
            _BottomToolStrip.ResumeLayout(false);
            _BottomToolStrip.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        private Core.Controls.SelectorOpener _SelectorOpener;
        private TableLayoutPanel _Layout;
        private Panel _PartPanel;
        private Button __SubmitButton;

        private Button _SubmitButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __SubmitButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__SubmitButton != null)
                {
                    __SubmitButton.Click -= SubmitButton_Click;
                }

                __SubmitButton = value;
                if (__SubmitButton != null)
                {
                    __SubmitButton.Click += SubmitButton_Click;
                }
            }
        }

        private Label _OffsetValueNumericLabel;
        private NumericUpDown __OffsetValueNumeric;

        private NumericUpDown _OffsetValueNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OffsetValueNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OffsetValueNumeric != null)
                {
                    __OffsetValueNumeric.Validated -= OffsetValueNumeric_Validated;
                }

                __OffsetValueNumeric = value;
                if (__OffsetValueNumeric != null)
                {
                    __OffsetValueNumeric.Validated += OffsetValueNumeric_Validated;
                }
            }
        }

        private Label _NominalValueNumericLabel;
        private NumericUpDown __NominalValueNumeric;

        private NumericUpDown _NominalValueNumeric
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __NominalValueNumeric;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__NominalValueNumeric != null)
                {
                    __NominalValueNumeric.Validated -= NominalValueNumeric_Validated;
                }

                __NominalValueNumeric = value;
                if (__NominalValueNumeric != null)
                {
                    __NominalValueNumeric.Validated += NominalValueNumeric_Validated;
                }
            }
        }

        private Button __ParseButton;

        private Button _ParseButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __ParseButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__ParseButton != null)
                {
                    __ParseButton.Click -= ParseButton_Click;
                }

                __ParseButton = value;
                if (__ParseButton != null)
                {
                    __ParseButton.Click += ParseButton_Click;
                }
            }
        }

        private TextBox __PartNumberTextBox;

        private TextBox _PartNumberTextBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __PartNumberTextBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__PartNumberTextBox != null)
                {
                    __PartNumberTextBox.Validated -= PartNumberTextBox_Validated;
                    __PartNumberTextBox.KeyUp -= EnterKeyHandler;
                    __PartNumberTextBox.KeyDown -= SuppressDingHandler;
                }

                __PartNumberTextBox = value;
                if (__PartNumberTextBox != null)
                {
                    __PartNumberTextBox.Validated += PartNumberTextBox_Validated;
                    __PartNumberTextBox.KeyUp += EnterKeyHandler;
                    __PartNumberTextBox.KeyDown += SuppressDingHandler;
                }
            }
        }

        private Label _PartNumberTextBoxLabel;
        private Label _SelectorLabel;
        private ToolStrip _BottomToolStrip;
        private ToolStripLabel _ResourceNameLabel;
        private Core.Controls.ToolStripCheckBox __AutoConnectCheckBox;

        private Core.Controls.ToolStripCheckBox _AutoConnectCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoConnectCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoConnectCheckBox != null)
                {
                    __AutoConnectCheckBox.CheckedChanged -= AutoConnectCheckBox_CheckedChanged;
                }

                __AutoConnectCheckBox = value;
                if (__AutoConnectCheckBox != null)
                {
                    __AutoConnectCheckBox.CheckedChanged += AutoConnectCheckBox_CheckedChanged;
                }
            }
        }

        private ToolStripLabel _NominalValueLabelLabel;
        private ToolStripLabel _NominalValueLabel;
        private ToolStripLabel _NominalOffsetLabelLabel;
        private ToolStripLabel _NominalOffsetLabel;
        private ToolStripLabel _AssignedValueLabelLabel;
        private ToolStripLabel _AssignedValueLabel;
        private Core.Controls.ToolStripCheckBox __AutoStartCheckBox;

        private Core.Controls.ToolStripCheckBox _AutoStartCheckBox
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __AutoStartCheckBox;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__AutoStartCheckBox != null)
                {
                    __AutoStartCheckBox.CheckedChanged -= AutoStartCheckBox_CheckedChanged;
                }

                __AutoStartCheckBox = value;
                if (__AutoStartCheckBox != null)
                {
                    __AutoStartCheckBox.CheckedChanged += AutoStartCheckBox_CheckedChanged;
                }
            }
        }
    }
}
