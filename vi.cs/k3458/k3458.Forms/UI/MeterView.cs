using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core;

namespace isr.VI.K3458.Forms
{

    /// <summary> A meter control. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-10-09 </para>
    /// </remarks>
    public partial class MeterView : Core.Forma.ModelViewTalkerBase
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        public MeterView( K3458Device device ) : base()
        {
            this.NewThis( device );
            this.__SubmitButton.Name = "_SubmitButton";
            this.__OffsetValueNumeric.Name = "_OffsetValueNumeric";
            this.__NominalValueNumeric.Name = "_NominalValueNumeric";
            this.__ParseButton.Name = "_ParseButton";
            this.__PartNumberTextBox.Name = "_PartNumberTextBox";
            this.__AutoConnectCheckBox.Name = "_AutoConnectCheckBox";
            this.__AutoStartCheckBox.Name = "_AutoStartCheckBox";
        }

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public MeterView() : this( K3458Device.Create() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="device"> The device. </param>
        private void NewThis( K3458Device device )
        {
            this.InitializingComponents = true;
            // This call is required by the designer.
            this.InitializeComponent();
            this.InitializingComponents = false;
            this._BottomToolStrip.Renderer = new CustomProfessionalRenderer();
            this.AssignDeviceThis( device );
            this._SubmitButton.Visible = !this._AutoStartCheckBox.Checked;
            this._ParseButton.Visible = !this._AutoStartCheckBox.Checked;
        }

        /// <summary>
        /// Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
        /// and its child controls and optionally releases the managed resources.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        ///                                                   release only unmanaged resources. </param>
        [DebuggerNonUserCode()]
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing )
                {
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " DEVICE "

        /// <summary> The device. </summary>

        /// <summary> Gets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public K3458Device Device { get; private set; }

        /// <summary> Assign device. </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        private void AssignDeviceThis( K3458Device value )
        {
            if ( this.Device is object || this.VisaSessionBase is object )
            {
                this.Device = null;
            }

            this.Device = value;
            this.Assign_Session( value );
            if ( value is object )
            {
            }
        }

        /// <summary> Assigns a device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The assigned device or nothing to release the previous assignment. </param>
        public void AssignDevice( K3458Device value )
        {
            this.AssignDeviceThis( value );
        }

        #endregion

        #region " VISA SESSION OPENER "

        /// <summary> Gets or sets the device. </summary>
        /// <value> The device. </value>
        [DesignerSerializationVisibility( DesignerSerializationVisibility.Hidden )]
        [Browsable( false )]
        public VisaSessionBase VisaSessionBase { get; private set; }

        /// <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
        /// <remarks>
        /// The synchronization context is captured as part of the property change and other event
        /// handlers and is no longer needed here.
        /// </remarks>
        /// <param name="visaSessionBase"> The assigned visa session base (device base) or nothing to
        ///                                release the session. </param>
        private void Assign_Session( VisaSessionBase visaSessionBase )
        {
            if ( this.VisaSessionBase is object )
            {
                if ( this.VisaSessionBase.Talker is object )
                    this.VisaSessionBase.Talker.RemoveListeners();
                this.VisaSessionBase.Opening -= this.HandleDeviceOpening;
                this.VisaSessionBase.Opened -= this.HandleDeviceOpened;
                this.VisaSessionBase.Closing -= this.HandleDeviceClosing;
                this.VisaSessionBase.Closed -= this.HandleDeviceClosed;
                this._SelectorOpener.AssignSelectorViewModel( null );
                this._SelectorOpener.AssignOpenerViewModel( null );
                this.BindVisaSession( false, this.VisaSessionBase );
                this.BindSession( false, this.VisaSessionBase.Session );
                this.VisaSessionBase = null;
            }

            this.VisaSessionBase = visaSessionBase;
            if ( visaSessionBase is object )
            {
                this.VisaSessionBase.Opening += this.HandleDeviceOpening;
                this.VisaSessionBase.Opened += this.HandleDeviceOpened;
                this.VisaSessionBase.Closing += this.HandleDeviceClosing;
                this.VisaSessionBase.Closed += this.HandleDeviceClosed;
                this._SelectorOpener.AssignSelectorViewModel( visaSessionBase.SessionFactory );
                this._SelectorOpener.AssignOpenerViewModel( visaSessionBase );
                this.BindVisaSession( true, this.VisaSessionBase );
                this.BindSession( true, this.VisaSessionBase.Session );
                this.VisaSessionBase.Session.ApplySettings();
                if ( !string.IsNullOrWhiteSpace( this.VisaSessionBase.CandidateResourceName ) )
                    this.VisaSessionBase.Session.CandidateResourceName = this.VisaSessionBase.CandidateResourceName;
                if ( !string.IsNullOrWhiteSpace( this.VisaSessionBase.CandidateResourceTitle ) )
                    this.VisaSessionBase.Session.CandidateResourceTitle = this.VisaSessionBase.CandidateResourceTitle;
                if ( this.VisaSessionBase.IsDeviceOpen )
                {
                    this.VisaSessionBase.Session.OpenResourceName = this.VisaSessionBase.OpenResourceName;
                    this.VisaSessionBase.Session.OpenResourceTitle = this.VisaSessionBase.OpenResourceTitle;
                }

                this.AssignTalker( visaSessionBase.Talker );
                this.ApplyListenerTraceLevel( ListenerType.Display, visaSessionBase.Talker.TraceShowLevel );
            }
        }

        /// <summary> Bind visa session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindVisaSession( bool add, VisaSessionBase viewModel )
        {
            _ = this.AddRemoveBinding( this._PartPanel, add, nameof( this.Enabled ), viewModel, nameof( this.VisaSessionBase.IsDeviceOpen ) );
        }

        /// <summary> Bind session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="add">       True to add. </param>
        /// <param name="viewModel"> The view model. </param>
        private void BindSession( bool add, Pith.SessionBase viewModel )
        {
            _ = this.AddRemoveBinding( this._BottomToolStrip, add, nameof( Control.Text ), viewModel, nameof( Pith.SessionBase.StatusPrompt ) );
        }

        /// <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="visaSessionBase"> The assigned visa session base (device base) or nothing to
        ///                                release the session. </param>
        public virtual void AssignSession( VisaSessionBase visaSessionBase )
        {
            this.Assign_Session( visaSessionBase );
        }

        #endregion

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Gets or sets the resource name caption. </summary>
        /// <value> The resource name caption. </value>
        protected string ResourceNameCaption
        {
            get => this._ResourceNameLabel.Text;

            set {
                if ( !string.Equals( value, this.ResourceNameCaption ) )
                {
                    this._ResourceNameLabel.Text = this.VisaSessionBase.OpenResourceName;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Executes the device Closing action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDeviceClosing( CancelEventArgs e )
        {
        }

        /// <summary> Handles the device Closing. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceClosing( object sender, CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceNameCaption} UI handling device closing";
                _ = this.PublishVerbose( $"{activity};. " );
                this.OnDeviceClosing( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the device Closed action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected virtual void OnDeviceClosed()
        {
        }

        /// <summary> Handles the device Close. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceClosed( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.ResourceNameCaption} UI handling device closed";
                _ = this.PublishVerbose( $"{activity};. " );
                this.OnDeviceClosed();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the device Opening action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnDeviceOpening( CancelEventArgs e )
        {
            this.ResourceNameCaption = this.VisaSessionBase.ResourceNameCaption;
        }

        /// <summary> Handles the device Opening. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceOpening( object sender, CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device Opening";
                _ = this.PublishVerbose( $"{activity};. " );
                this.OnDeviceOpening( e );
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        /// <summary> Executes the device opened action. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        protected virtual void OnDeviceOpened()
        {
            this.ResourceNameCaption = this.VisaSessionBase.ResourceNameCaption;
        }

        /// <summary> Handles the device opened. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void HandleDeviceOpened( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.VisaSessionBase.ResourceNameCaption} UI handling device opened";
                _ = this.PublishVerbose( $"{activity};. " );
                this.OnDeviceOpened();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
        }

        #endregion

        #region " DEVICE EVENT HANDLERS "

        /// <summary> Configure meter. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="nominalValue"> The nominal value. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void ConfigureMeter( double nominalValue )
        {
            string action = $"Configuring {this.Device.ResourceNameCaption}";
            try
            {
                var expectedSenseFunctionMode = SenseFunctionModes.ResistanceFourWire;
                action = $"Configuring function mode {expectedSenseFunctionMode}";
                var actualSenseFunctionMode = this.Device.MeasureSubsystem.ApplyFunctionMode( expectedSenseFunctionMode );
                if ( actualSenseFunctionMode.HasValue )
                {
                    if ( expectedSenseFunctionMode != actualSenseFunctionMode.Value )
                    {
                        _ = this.PublishWarning( $"Failed {action} Expected {expectedSenseFunctionMode} <> Actual {actualSenseFunctionMode.Value}" );
                        return;
                    }
                }
                else
                {
                    _ = this.PublishWarning( $"Failed {action}--no value set" );
                    return;
                }

                var expectedAutoZeroMode = this.AutoZeroMode;
                action = $"Configuring auto zero {expectedAutoZeroMode}";
                var actualAutoZeroMode = this.Device.MeasureSubsystem.ApplyAutoZeroMode( expectedAutoZeroMode );
                if ( actualAutoZeroMode.HasValue )
                {
                    if ( expectedAutoZeroMode != actualAutoZeroMode.Value )
                    {
                        _ = this.PublishWarning( $"Failed {action} Expected {expectedAutoZeroMode} <> Actual {actualAutoZeroMode.Value}" );
                        return;
                    }
                }
                else
                {
                    _ = this.PublishWarning( $"Failed {action}--no value set" );
                    return;
                }

                this.AutoZeroMode = actualAutoZeroMode.Value;
                double expectedPowerLineCycles = this.PowerLineCycles;
                action = $"Configuring power line cycles {expectedAutoZeroMode}";
                var actualPowerLineCycles = this.Device.MeasureSubsystem.ApplyPowerLineCycles( expectedPowerLineCycles );
                if ( actualPowerLineCycles.HasValue )
                {
                    if ( expectedPowerLineCycles != actualPowerLineCycles.Value )
                    {
                        _ = this.PublishWarning( $"Failed {action} Expected {expectedPowerLineCycles} <> Actual {actualPowerLineCycles.Value}" );
                        return;
                    }
                }
                else
                {
                    _ = this.PublishWarning( $"Failed {action}--no value set" );
                    return;
                }

                this.PowerLineCycles = actualPowerLineCycles.Value;
                var expectedMathMode = MathMode.Percent;
                action = $"Configuring math mode {expectedMathMode}";
                var actualMathMode = this.Device.MeasureSubsystem.ApplyMathMode( expectedMathMode );
                if ( actualMathMode.HasValue )
                {
                    if ( expectedMathMode != actualMathMode.Value )
                    {
                        _ = this.PublishWarning( $"Failed {action} Expected {expectedMathMode} <> Actual {actualMathMode.Value}" );
                        return;
                    }
                }
                else
                {
                    _ = this.PublishWarning( $"Failed {action}--no value set" );
                    return;
                }

                // nominalValue = Me.NominalOffsetInfo.NominalValue + Me.NominalOffsetInfo.NominalLimit
                var expectedStoreMathRegister = StoreMathRegister.Percent;
                double expectedStoreMathValue = nominalValue;
                action = $"Configuring store math mode {expectedStoreMathRegister} {nominalValue}";
                var actualStoreMathRegister = this.Device.MeasureSubsystem.WriteStoreMath( expectedStoreMathRegister, expectedStoreMathValue );
                if ( actualStoreMathRegister.HasValue )
                {
                    if ( expectedStoreMathRegister != actualStoreMathRegister.Value )
                    {
                        _ = this.PublishWarning( $"Failed {action} Expected {expectedStoreMathRegister} <> Actual {actualStoreMathRegister.Value}" );
                        return;
                    }
                }
                else
                {
                    _ = this.PublishWarning( $"Failed {action}--no value set" );
                    return;
                }

                this._AssignedValueLabel.Text = nominalValue.ToString();
                this._NominalOffsetLabel.Text = this.OffsetValue.ToString();
                this._NominalValueLabel.Text = this.NominalValue.ToString();
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( action, ex );
            }
        }

        #endregion

        #region " METER SETTIGNS "

        /// <summary> Name of the resource. </summary>
        private string _ResourceName;

        /// <summary> Gets or sets the name of the resource. </summary>
        /// <value> The name of the resource. </value>
        public string ResourceName
        {
            get => this._ResourceName;

            set {
                if ( !string.Equals( value, this.ResourceName ) || !string.Equals( value, this.Device.CandidateResourceName ) )
                {
                    this._ResourceName = value;
                    this.Device.CandidateResourceName = value;
                    // TO_DO: Do we need this? Me.Device.SessionFactory.EnumerateResources(False)
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The automatic zero mode. </summary>
        private AutoZeroMode _AutoZeroMode;

        /// <summary> Gets or sets the automatic zero mode. </summary>
        /// <value> The automatic zero mode. </value>
        public AutoZeroMode AutoZeroMode
        {
            get => this._AutoZeroMode;

            set {
                if ( value != this.AutoZeroMode )
                {
                    this._AutoZeroMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The power line cycles. </summary>
        private double _PowerLineCycles;

        /// <summary> Gets or sets the power line cycles. </summary>
        /// <value> The power line cycles. </value>
        public double PowerLineCycles
        {
            get => this._PowerLineCycles;

            set {
                if ( value != this.PowerLineCycles )
                {
                    this._PowerLineCycles = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region " CONTROL VISIBILITY "

        /// <summary> True to enable, false to disable the automatic configure. </summary>
        private bool _AutoConfigureEnabled;

        /// <summary> Gets or sets the automatic configure enabled. </summary>
        /// <value> The automatic configure enabled. </value>
        public bool AutoConfigureEnabled
        {
            get => this._AutoConfigureEnabled;

            set {
                if ( value != this.AutoConfigureEnabled || value != this._AutoStartCheckBox.Checked )
                {
                    this._AutoConfigureEnabled = value;
                    this._AutoStartCheckBox.Checked = value;
                    this._SubmitButton.Visible = !this.AutoConfigureEnabled;
                    this._ParseButton.Visible = !this.AutoConfigureEnabled;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Automatic start check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AutoStartCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.AutoConfigureEnabled = this._AutoStartCheckBox.Checked;
        }

        /// <summary> True to enable, false to disable the automatic connect. </summary>
        private bool _AutoConnectEnabled;

        /// <summary> Gets or sets the automatic connect enabled. </summary>
        /// <value> The automatic connect enabled. </value>
        public bool AutoConnectEnabled
        {
            get => this._AutoConnectEnabled;

            set {
                if ( value != this.AutoConnectEnabled || value != this._AutoConnectCheckBox.Checked )
                {
                    this._AutoConnectEnabled = value;
                    this._AutoConnectCheckBox.Checked = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Automatic connect check box checked changed. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void AutoConnectCheckBox_CheckedChanged( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.AutoConfigureEnabled = this._AutoConnectCheckBox.Checked;
        }

        /// <summary> Focus part number. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public void FocusPartNumber()
        {
            _ = this._PartNumberTextBox.Focus();
        }

        #endregion

        #region " PART NUMBER PARSING "

        /// <summary> The part number. </summary>
        private string _PartNumber;

        /// <summary> Gets or sets the part number. </summary>
        /// <value> The part number. </value>
        public string PartNumber
        {
            get => this._PartNumber;

            set {
                if ( !string.Equals( value, this.PartNumber ) || !string.Equals( value, this._PartNumberTextBox.Text ) )
                {
                    this._PartNumber = value;
                    this._PartNumberTextBox.Text = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Part number text box validated. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void PartNumberTextBox_Validated( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.PartNumber = this._PartNumberTextBox.Text;
        }

        /// <summary> Filename of the nominal offsets file. </summary>
        private string _NominalOffsetsFileName;

        /// <summary> Gets or sets the filename of the nominal offsets file. </summary>
        /// <value> The filename of the nominal offsets file. </value>
        public string NominalOffsetsFileName
        {
            get => this._NominalOffsetsFileName;

            set {
                if ( !string.Equals( value, this.NominalOffsetsFileName ) )
                {
                    this._NominalOffsetsFileName = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The nominal value. </summary>
        private decimal _NominalValue;

        /// <summary> Gets or sets the nominal value. </summary>
        /// <value> The nominal value. </value>
        public decimal NominalValue
        {
            get => this._NominalValue;

            set {
                if ( value != this.NominalValue || value != this._NominalValueNumeric.Value )
                {
                    this._NominalValue = value;
                    this._NominalValueNumeric.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Nominal value numeric validated. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void NominalValueNumeric_Validated( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this._NominalValue = this._NominalValueNumeric.Value;
        }

        /// <summary> The offset value. </summary>
        private decimal _OffsetValue;

        /// <summary> Gets or sets the offset value. </summary>
        /// <value> The offset value. </value>
        public decimal OffsetValue
        {
            get => this._OffsetValue;

            set {
                if ( value != this.OffsetValue || value != this._OffsetValueNumeric.Value )
                {
                    this._OffsetValue = value;
                    this._OffsetValueNumeric.Value = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Offset value numeric validated. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void OffsetValueNumeric_Validated( object sender, EventArgs e )
        {
            if ( this.InitializingComponents )
                return;
            this.OffsetValue = this._OffsetValueNumeric.Value;
        }

        /// <summary> Attempts to parse from the given data. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="partNumber"> The part number. </param>
        /// <param name="fileName">   Filename of the file. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public virtual bool TryParse( string partNumber, string fileName )
        {
            bool affirmative = true;
            return affirmative;
        }

        #endregion

        #region " CONTROL EVENTS "

        /// <summary> Parse button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void ParseButton_Click( object sender, EventArgs e )
        {
            _ = this.TryParse( this.PartNumber, this.NominalOffsetsFileName );
        }

        /// <summary> Submit button click. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void SubmitButton_Click( object sender, EventArgs e )
        {
            var args = new ActionEventArgs();
            if ( this._SelectorOpener.OpenerViewModel.TryOpen( args ) )
            {
                this.ConfigureMeter( ( double ) (this.NominalValue + this.OffsetValue) );
            }
            else
            {
                _ = this.Publish( args );
            }
        }

        /// <summary> Event handler. Called by _PartNumberTextBox for key up events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        private void EnterKeyHandler( object sender, KeyEventArgs e )
        {
            if ( e is null )
                return;
            if ( e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return )
            {
                var args = new ActionEventArgs();
                if ( this.TryParse( this.PartNumber, this.NominalOffsetsFileName ) )
                {
                    if ( this.AutoConfigureEnabled )
                    {
                        if ( this._SelectorOpener.OpenerViewModel.TryOpen( args ) )
                        {
                            this.ConfigureMeter( ( double ) (this._NominalValueNumeric.Value + this._OffsetValueNumeric.Value) );
                        }
                        else
                        {
                            _ = this.Publish( args );
                        }
                    }
                    else if ( this.AutoConnectEnabled )
                    {
                        if ( this._SelectorOpener.OpenerViewModel.TryOpen( args ) )
                        {
                        }
                        else
                        {
                            _ = this.Publish( args );
                        }
                    }
                }

                this._PartNumberTextBox.SelectAll();
            }
        }

        /// <summary> Event handler. Called by _PartNumberTextBox for key down events. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Key event information. </param>
        private void SuppressDingHandler( object sender, KeyEventArgs e )
        {
            if ( e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return )
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyLibrary.Appliance.Identify( this.Talker );
        }

        #endregion

        #region " TOOL STRIP RENDERER "

        /// <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            // Me._TopToolStrip.Renderer = New CustomProfessionalRenderer
            // Me._TopToolStrip.Invalidate()
            base.OnLoad( e );
        }

        /// <summary> A custom professional renderer. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        private class CustomProfessionalRenderer : ToolStripProfessionalRenderer
        {

            /// <summary>
            /// Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
            /// event.
            /// </summary>
            /// <remarks> David, 2020-10-12. </remarks>
            /// <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
            ///                  contains the event data. </param>
            protected override void OnRenderLabelBackground( ToolStripItemRenderEventArgs e )
            {
                if ( e is object && e.Item.BackColor != System.Drawing.SystemColors.ControlDark )
                {
                    using var brush = new System.Drawing.SolidBrush( e.Item.BackColor );
                    e.Graphics.FillRectangle( brush, e.Item.ContentRectangle );
                }
            }
        }

        #endregion

    }
}
