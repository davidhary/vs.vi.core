namespace isr.VI.K3458
{

    /// <summary> Defines a System Subsystem for a Keysight K3458 Meter. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-07 </para>
    /// </remarks>
    public class SystemSubsystem : SystemSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public SystemSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem )
        {
        }

        #endregion

        #region " COMMAND SYNTAX "

        /// <summary> Gets or sets the initialize memory command. </summary>
        /// <value> The initialize memory command. </value>
        protected override string InitializeMemoryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the preset command. </summary>
        /// <value> The preset command. </value>
        protected override string PresetCommand { get; set; } = string.Empty;

        #endregion

    }
}
