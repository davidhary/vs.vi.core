﻿using isr.Core.EscapeSequencesExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.K3458
{

    /// <summary> Defines a Status Subsystem for a Keysight K3458 meter. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-07 </para>
    /// </remarks>
    public class StatusSubsystem : StatusSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="Pith.SessionBase">message based
        ///                        session</see>. </param>
        public StatusSubsystem( Pith.SessionBase session ) : base( Pith.SessionBase.Validated( session ) )
        {
            this.VersionInfo = new VersionInfo();
            this.VersionInfoBase = this.VersionInfo;
            InitializeSession( session );
        }

        /// <summary> Creates a new StatusSubsystem. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> A StatusSubsystem. </returns>
        public static StatusSubsystem Create()
        {
            StatusSubsystem subsystem = null;
            try
            {
                subsystem = new StatusSubsystem( SessionFactory.Get.Factory.Session() );
            }
            catch
            {
                if ( subsystem is object )
                {
                }

                throw;
            }

            return subsystem;
        }

        #endregion


        #region " SESSION "

        /// <summary> Initializes the session. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
        ///                        session</see>. </param>
        private static void InitializeSession( Pith.SessionBase session )
        {
            session.ClearExecutionStateCommand = string.Empty;
            session.ResetKnownStateCommand = string.Empty;
            session.ErrorAvailableBit = Pith.ServiceRequests.StandardEvent;
            session.MeasurementEventBit = Pith.ServiceRequests.MeasurementEvent;
            session.MessageAvailableBit = Pith.ServiceRequests.None;
            session.OperationCompletedQueryCommand = string.Empty;
            session.StandardEventBit = Pith.ServiceRequests.None;
            session.StandardEventStatusQueryCommand = string.Empty;
            session.StandardEventEnableQueryCommand = string.Empty;
            session.StandardServiceEnableCommandFormat = string.Empty;
            session.StandardServiceEnableCompleteCommandFormat = string.Empty;
            session.ServiceRequestEnableCommandFormat = "RQS {0}";
            session.ServiceRequestEnableQueryCommand = "RQS?";
            session.OperationCompleted = true;
            session.ReadTerminationCharacterEnabled = true;
            session.ReadTerminationCharacter = EscapeSequencesExtensionMethods.NewLineValue;
            session.ServiceRequestEnabledBitmask = ( Pith.ServiceRequests ) Conversions.ToInteger( 63 );
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.Session.StandardEventStatus = 0;
            this.Session.OperationCompleted = true;
            this.VersionInfo = new VersionInfo();
        }

        #endregion

        #region " LINE FREQUENCY "

        /// <summary> Gets or sets line frequency query command. </summary>
        /// <value> The line frequency query command. </value>
        protected override string LineFrequencyQueryCommand { get; set; } = "LINE?";

        #endregion

        #region " IDENTITY "

        /// <summary> Gets or sets the identity query command. </summary>
        /// <value> The identity query command. </value>
        protected override string IdentityQueryCommand { get; set; } = "ID?";

        /// <summary> Queries the Identity. </summary>
        /// <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
        /// <returns> System.String. </returns>
        public override string QueryIdentity()
        {
            if ( !string.IsNullOrWhiteSpace( this.IdentityQueryCommand ) )
            {
                _ = this.PublishVerbose( "Requesting identity;. " );
                Core.ApplianceBase.DoEvents();
                this.WriteIdentityQueryCommand();
                _ = this.PublishVerbose( "Trying to read identity;. " );
                Core.ApplianceBase.DoEvents();
                // wait for the delay time.
                // Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
                string value = this.Session.ReadLineTrimEnd();
                value = value.ReplaceCommonEscapeSequences().Trim();
                _ = this.PublishVerbose( $"Setting identity to {value};. " );
                this.VersionInfo.Parse( value );
                this.VersionInfoBase = this.VersionInfo;
                this.Identity = this.VersionInfo.Identity;
            }

            return this.Identity;
        }

        /// <summary> Gets or sets the information describing the version. </summary>
        /// <value> Information describing the version. </value>
        public VersionInfo VersionInfo { get; private set; }

        #endregion

        #region " DEVICE ERRORS "

        /// <summary> Gets or sets the clear error queue command. </summary>
        /// <value> The clear error queue command. </value>
        protected override string ClearErrorQueueCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the error queue query command. </summary>
        /// <remarks> Last error supported. </remarks>
        /// <value> The error queue query command. </value>
        protected override string NextDeviceErrorQueryCommand { get; set; } = "ERRSTR?";

        /// <summary> Gets or sets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected override string DequeueErrorQueryCommand { get; set; } = string.Empty;

        /// <summary> Gets or sets the last error query command. </summary>
        /// <value> The last error query command. </value>
        protected override string DeviceErrorQueryCommand { get; set; } = "ERRSTR?";

        /// <summary> Queue device error. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="compoundErrorMessage"> Message describing the compound error. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        protected override VI.DeviceError EnqueueDeviceError( string compoundErrorMessage )
        {
            var de = new DeviceError();
            de.Parse( compoundErrorMessage );
            if ( de.IsError )
                this.DeviceErrorQueue.Enqueue( de );
            return de;
        }

        #endregion

    }
}