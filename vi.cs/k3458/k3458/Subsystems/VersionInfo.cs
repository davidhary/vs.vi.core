using System.Collections.Generic;

namespace isr.VI.K3458
{

    /// <summary> Information about the version of a Keysight 34980 Meter/Scanner. </summary>
    /// <remarks>
    /// (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-09-22, 3.0.5013. </para>
    /// </remarks>
    public class VersionInfo : VersionInfoBase
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public VersionInfo() : base()
        {
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void Clear()
        {
            base.Clear();
        }

        /// <summary> Parses the instrument identity string. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> Specifies the instrument identity string, which includes at a minimum the
        ///                      following information:
        ///                      <see cref="VersionInfoBase.ManufacturerName">manufacturer</see>,
        ///                      <see cref="VersionInfoBase.Model">model</see>,
        ///                      <see cref="VersionInfoBase.SerialNumber">serial number</see>, e.g., <para>
        ///                      <c>H3458A</c>.</para> </param>
        public override void Parse( string value )
        {
            if ( string.IsNullOrEmpty( value ) )
            {
                this.Clear();
            }
            else
            {

                // save the identity.
                this.Identity = value;

                // Parse the id to get the revision number
                var idItems = new Queue<string>( value.Split( ',' ) );
                this.ManufacturerName = "KEYSIGHT";
                this.Model = idItems.Dequeue();
            }
        }
    }
}
