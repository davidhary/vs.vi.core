using System;

using isr.Core.EnumExtensions;

namespace isr.VI.K3458
{

    /// <summary>
    /// Defines a Measure Subsystem for a Keysight 1750 Resistance Measuring System.
    /// </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>  
    /// David, 2013-10-07 </para>
    /// </remarks>
    public class MeasureSubsystem : MeasureSubsystemBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="statusSubsystem"> The status subsystem. </param>
        public MeasureSubsystem( StatusSubsystemBase statusSubsystem ) : base( statusSubsystem, new Readings() )
        {
            this.ReadingAmounts.Initialize( ReadingElementTypes.Reading );
            this.ReadingAmounts.PrimaryReading.ApplyUnit( Arebis.StandardUnits.ElectricUnits.Volt );
            this.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt;
            this.FunctionModeReadWrites.Clear();
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.None, "0", "", SenseFunctionModes.None.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.VoltageDC, "1", "DCV", SenseFunctionModes.VoltageDC.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.VoltageAC, "2", "ACV", SenseFunctionModes.VoltageAC.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.VoltageACDC, "3", "ACDCV", SenseFunctionModes.VoltageACDC.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.Resistance, "4", "OHM", SenseFunctionModes.Resistance.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.ResistanceFourWire, "5", "OHMF", SenseFunctionModes.ResistanceFourWire.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.CurrentDC, "6", "DCI", SenseFunctionModes.CurrentDC.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.CurrentAC, "7", "ACI", SenseFunctionModes.CurrentAC.DescriptionUntil() );
            this.FunctionModeReadWrites.Add( ( long ) SenseFunctionModes.CurrentACDC, "8", "ACDCI", SenseFunctionModes.CurrentACDC.DescriptionUntil() );
            this.SupportedFunctionModes = SenseFunctionModes.ResistanceFourWire | SenseFunctionModes.Resistance | SenseFunctionModes.CurrentDC | SenseFunctionModes.VoltageDC;
        }

        #endregion

        #region " I PRESETTABLE "

        /// <summary> Sets the known initial post reset state. </summary>
        /// <remarks> Use this method to customize the reset. </remarks>
        public override void InitKnownState()
        {
            base.InitKnownState();
            // TO_DO: this came from the 2450 and needs to be corrected to match this instrument.
            this.ApertureRange = new Core.Primitives.RangeR( 0.000166667d, 0.166667d );
            this.FilterCountRange = new Core.Primitives.RangeI( 1, 100 );
            this.FilterWindowRange = new Core.Primitives.RangeR( 0d, 0.1d );
            this.PowerLineCyclesRange = new Core.Primitives.RangeR( 0.01d, 10d );
        }

        /// <summary>
        /// Defines the know reset state (RST) by setting system properties to the their Reset (RST)
        /// default values.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        public override void DefineKnownResetState()
        {
            base.DefineKnownResetState();
            this.FunctionMode = SenseFunctionModes.VoltageDC;
        }

        #endregion

        #region "  INIT, READ, FETCH "

#pragma warning disable CS0809 // Obsolete member overrides non-obsolete member
        /// <summary> Gets the fetch command. </summary>
        /// <value> The fetch command. </value>
        [Obsolete( "Not supported" )]
        protected override string FetchCommand { get; set; } = string.Empty;

        /// <summary> Gets the read command. </summary>
        /// <value> The read command. </value>
        [Obsolete( "Not supported" )]
        protected override string ReadCommand { get; set; } = string.Empty;
#pragma warning restore CS0809 // Obsolete member overrides non-obsolete member

        #endregion

        #region " AVERAGE ENABLED "

        /// <summary> Gets or sets the Average enabled command Format. </summary>
        /// <value> The Average enabled query command. </value>
        protected virtual string AverageEnabledCommandFormat { get; set; } = ":SENS:RES:AVER:STAT {0:'ON';'ON';'OFF'}";

        /// <summary> Gets or sets the Average enabled query command. </summary>
        /// <value> The Average enabled query command. </value>
        protected virtual string AverageEnabledQueryCommand { get; set; } = ":SENS:RES:AVER:STAT?";

        #endregion

        #region " AUTO ZERO MODE "

        /// <summary> Gets or sets the auto zero command Format. </summary>
        /// <value> The auto zero enabled query command. </value>
        protected virtual string AutoZeroModeCommandFormat { get; set; } = "AZERO {0}";

        /// <summary> Gets or sets the auto zero query command. </summary>
        /// <value> The auto Zero enabled query command. </value>
        protected virtual string AutoZeroModeQueryCommand { get; set; } = "AZERO?";

        /// <summary> The automatic zero mode. </summary>
        private AutoZeroMode? _AutoZeroMode;

        /// <summary> Gets or sets the cached Auto Zero Mode. </summary>
        /// <value>
        /// The <see cref="AutoZeroMode">Auto Zero Mode</see> or none if not set or unknown.
        /// </value>
        public AutoZeroMode? AutoZeroMode
        {
            get => this._AutoZeroMode;

            protected set {
                if ( !this.AutoZeroMode.Equals( value ) )
                {
                    this._AutoZeroMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Auto Zero Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The  Auto Zero Mode. </param>
        /// <returns> The <see cref="AutoZeroMode">Auto Zero Mode</see> or none if unknown. </returns>
        public AutoZeroMode? ApplyAutoZeroMode( AutoZeroMode value )
        {
            _ = this.WriteAutoZeroMode( value );
            return this.QueryAutoZeroMode();
        }

        /// <summary> Queries the auto Zero Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The <see cref="AutoZeroMode">auto Zero Mode</see> or none if unknown. </returns>
        public AutoZeroMode? QueryAutoZeroMode()
        {
            this.AutoZeroMode = this.QueryValue( this.AutoZeroModeQueryCommand, this.AutoZeroMode );
            return this.AutoZeroMode;
        }

        /// <summary> Writes the auto Zero Mode without reading back the value from the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The autoZeroMode. </param>
        /// <returns> The <see cref="AutoZeroMode">autoZeroMode</see> or none if unknown. </returns>
        public AutoZeroMode? WriteAutoZeroMode( AutoZeroMode value )
        {
            this.AutoZeroMode = this.WriteValue( this.AutoZeroModeCommandFormat, value );
            return this.AutoZeroMode;
        }

        #endregion

        #region " FETCH "

        /// <summary> Fetches the data. </summary>
        /// <remarks> the K3458 meter does not require issuing a read command. </remarks>
        /// <returns> A Double? </returns>
        public override double? Fetch()
        {
            return this.Read();
        }

        /// <summary> Fetches the data. </summary>
        /// <remarks> the K3458 meter does not require issuing a read command. </remarks>
        /// <returns> A Double? </returns>
        public override double? Read()
        {
            return this.MeasurePrimaryReading( this.Session.ReadLineTrimEnd() );
            // Dim reading As String = Me.Session.ReadLineTrimEnd
            // Return If(Me.ReadingAmounts.HasReadingElements, Me.ParseReadingAmounts(reading), Me.ParsePrimaryReading(reading))
        }

        #endregion

        #region " MATH MODE "

        /// <summary> Gets or sets the Math command Format. </summary>
        /// <value> The Math enabled query command. </value>
        protected virtual string MathModeCommandFormat { get; set; } = "MATH {0}";

        /// <summary> Gets or sets the Math query command. </summary>
        /// <value> The Math enabled query command. </value>
        protected virtual string MathModeQueryCommand { get; set; } = "MATH?";

        /// <summary> The mathematics mode. </summary>
        private MathMode? _MathMode;

        /// <summary> Gets or sets the cached Math Mode. </summary>
        /// <value> The <see cref="MathMode">Math Mode</see> or none if not set or unknown. </value>
        public MathMode? MathMode
        {
            get => this._MathMode;

            protected set {
                if ( !this.MathMode.Equals( value ) )
                {
                    this._MathMode = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Math Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The  Math Mode. </param>
        /// <returns> The <see cref="MathMode">Math Mode</see> or none if unknown. </returns>
        public MathMode? ApplyMathMode( MathMode value )
        {
            _ = this.WriteMathMode( value );
            return this.QueryMathMode();
        }

        /// <summary> Queries the Math Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The <see cref="MathMode">Math Mode</see> or none if unknown. </returns>
        public MathMode? QueryMathMode()
        {
            this.MathMode = this.QueryFirstValue( this.MathModeQueryCommand, this.MathMode );
            return this.MathMode;
        }

        /// <summary> Writes the Math Mode without reading back the value from the device. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="value"> The MathMode. </param>
        /// <returns> The <see cref="MathMode">MathMode</see> or none if unknown. </returns>
        public MathMode? WriteMathMode( MathMode value )
        {
            this.MathMode = this.WriteValue( this.MathModeCommandFormat, value );
            return this.MathMode;
        }

        #endregion

        #region " FUNCTION MODE "

        /// <summary> Gets or sets the Function Mode command format. </summary>
        /// <value> The Function Mode command format. </value>
        protected override string FunctionModeCommandFormat { get; set; } = "FUNC {0}";

        /// <summary> Gets or sets the Function Mode query command. </summary>
        /// <value> The Function Mode query command. </value>
        protected override string FunctionModeQueryCommand { get; set; } = "FUNC?";

        /// <summary> Queries the Sense Function Mode. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The <see cref="isr.VI.SenseFunctionModes">Sense Function Mode</see> or none if unknown. </returns>
        public override SenseFunctionModes? QueryFunctionMode()
        {
            this.FunctionMode = this.QueryFirstValue( this.FunctionModeQueryCommand, this.FunctionMode.GetValueOrDefault( SenseFunctionModes.None ), this.FunctionModeReadWrites );
            return this.FunctionMode;
        }

        #endregion

        #region " AUTO RANGE ENABLED "

        /// <summary> Gets or sets the automatic Range enabled query command. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledQueryCommand { get; set; } = "ARANG?";

        /// <summary> Gets or sets the automatic Range enabled command Format. </summary>
        /// <value> The automatic Range enabled query command. </value>
        protected override string AutoRangeEnabledCommandFormat { get; set; } = "ARANGE {0:'ON';'ON';'OFF'}";

        #endregion

        #region " POWER LINE CYCLES "

        /// <summary> Gets or sets The Power Line Cycles query command. </summary>
        /// <value> The Power Line Cycles query command. </value>
        protected override string PowerLineCyclesQueryCommand { get; set; } = "NPLC?";

        /// <summary> Gets or sets The Power Line Cycles command format. </summary>
        /// <value> The Power Line Cycles command format. </value>
        protected override string PowerLineCyclesCommandFormat { get; set; } = "NPLC {0}";

        #endregion

        #region " STORE MATH "

        /// <summary> The store mathematics value. </summary>
        private double? _StoreMathValue;

        /// <summary> Gets or sets the cached Store Math Value. </summary>
        /// <value> The cached Store Math Value or none if not set or unknown. </value>
        public double? StoreMathValue
        {
            get => this._StoreMathValue;

            protected set {
                if ( !Nullable.Equals( this.StoreMathValue, value ) )
                {
                    this._StoreMathValue = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> The store mathematics register. </summary>
        private StoreMathRegister? _StoreMathRegister;

        /// <summary> Gets or sets the cached Store Math Register. </summary>
        /// <value>
        /// The <see cref="StoreMathRegister">Store Math Register</see> or none if not set or unknown.
        /// </value>
        public StoreMathRegister? StoreMathRegister
        {
            get => this._StoreMathRegister;

            protected set {
                if ( !this.StoreMathRegister.Equals( value ) )
                {
                    this._StoreMathRegister = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Writes and reads back the Store Math Register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="register"> The register. </param>
        /// <param name="value">    The  Auto Zero Mode. </param>
        /// <returns>
        /// The <see cref="StoreMathRegister">Store Math Register</see> or none if unknown.
        /// </returns>
        [Obsolete( "Apply does not work with store math because reading back returns the measured value and the register cannot be read" )]
        public StoreMathRegister? ApplyStoreMath( StoreMathRegister register, double value )
        {
            _ = this.WriteStoreMath( register, value );
            _ = this.QueryStoreMathValue();
            return this.StoreMathRegister;
        }

        /// <summary> Gets or sets the auto zero query command. </summary>
        /// <value> The auto Zero enabled query command. </value>
        protected virtual string StoreMathQueryCommand { get; set; } = "SMATH?";

        /// <summary> Queries the Store Math Register. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns>
        /// The <see cref="StoreMathRegister">Store Math Register</see> or none if unknown.
        /// </returns>
        [Obsolete( "Reading the register is not available" )]
        public StoreMathRegister? QueryStoreMathRegister()
        {
            this.StoreMathRegister = this.QueryFirstValue( this.StoreMathQueryCommand, this.StoreMathRegister );
            return this.StoreMathRegister;
        }

        /// <summary> Queries store mathematics value. </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <returns> The store mathematics value. </returns>
        [Obsolete( "Reading back from the stored math reads the measured value" )]
        public double? QueryStoreMathValue()
        {
            this.StoreMathValue = this.Query( this.StoreMathValue, this.StoreMathQueryCommand );
            return this.StoreMathValue;
        }

        /// <summary> Gets or sets the store mathematics command format. </summary>
        /// <value> The store mathematics command format. </value>
        protected virtual string StoreMathCommandFormat { get; set; } = "SMATH {0},{1}";

        /// <summary>
        /// Writes the Store Math Register and value without reading back the value from the device.
        /// </summary>
        /// <remarks> David, 2020-10-12. </remarks>
        /// <param name="register"> The register. </param>
        /// <param name="value">    The  Auto Zero Mode. </param>
        /// <returns>
        /// The <see cref="StoreMathRegister">StoreMathRegister</see> or none if unknown.
        /// </returns>
        public StoreMathRegister? WriteStoreMath( StoreMathRegister register, double value )
        {
            this.StoreMathValue = value;
            this.StoreMathRegister = register;
            this.Session.Execute( string.Format( this.StoreMathCommandFormat, register.ExtractBetween(), value ) );
            return this.StoreMathRegister;
        }

        #endregion

    }

    /// <summary> Enumerates the auto zero mode. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum AutoZeroMode
    {

        /// <summary> Zero measurement Is updated once, Then only after a
        /// Function, range, aperture, NPLC, Or resolution change. </summary>
        [System.ComponentModel.Description( "Off (OFF)" )]
        Off = 0,

        /// <summary> Zero measurement Is updated after every measurement. </summary>
        [System.ComponentModel.Description( "On (ON)" )]
        On = 1,

        /// <summary> Zero measurement is updated once, then only after a
        /// Function, range, aperture, NPLC, Or resolution change. </summary>
        [System.ComponentModel.Description( "Once (ONCE)" )]
        Once = 2
    }

    /// <summary> Values that represent Mathematics modes. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum MathMode
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "OFF (OFF)" )]
        None = 0,

        /// <summary> An enum constant representing the percent option. </summary>
        [System.ComponentModel.Description( "Percent (PERC)" )]
        Percent = 10
    }

    /// <summary> Values that represent store Mathematics registers. </summary>
    /// <remarks> David, 2020-10-12. </remarks>
    public enum StoreMathRegister
    {

        /// <summary> An enum constant representing the none option. </summary>
        [System.ComponentModel.Description( "Not specified" )]
        None = 0,

        /// <summary> An enum constant representing the offset option. </summary>
        [System.ComponentModel.Description( "Offset (OFFSET)" )]
        Offset = 7,

        /// <summary> An enum constant representing the percent option. </summary>
        [System.ComponentModel.Description( "Percent (PERC)" )]
        Percent = 8
    }
}
