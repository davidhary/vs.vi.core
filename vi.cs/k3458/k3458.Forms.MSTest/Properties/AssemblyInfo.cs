﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( "K3458 VI Forms Tests" )]
[assembly: AssemblyDescription( "K3458 Virtual Instrument Forms Unit Tests Library" )]
[assembly: AssemblyProduct( "isr.VI.K3458.Forms.MSTest" )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
