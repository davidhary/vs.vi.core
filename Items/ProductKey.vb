#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr.VI.My
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Information about this and related projects in this solution. </summary>
    ''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module SolutionInfo

        ''' <summary> The shared public key. </summary>
        Public Const SharedPublicKey As String = "0024000004800000940000000602000000240000525341310004000001000100dba29c0e6dba92" &
                                                 "7c84e79ce36609bb871da7c63c8ef88addafa51ccc967604e03c21afeb0cb5f5e74028e1f151bf" &
                                                 "92e9fe73166bfab3a61c4db81e8dad98c020f06368cce665735e9a2e42741b80ea60a3dcc6b4ad" &
                                                 "212928baf132b99c0ce9a2d0eae0b14d4fb2b7d2e4d5eba898b61e1e69c1d47db03a92eb932413" &
                                                 "7084bffb"

        ''' <summary> The strain public key. </summary>
        Public Const StrainPublicKey As String = "0024000004800000940000000602000000240000525341310004000001000100f980efcb2f397d" &
                                                 "2b714f750390d86efd0eed0b8c95fecd2001ea4d8c5b38ea9c69465e5a21e902a93992acc7672e" &
                                                 "eb7f6c7bf92f0dc525561ff6e66b6fcd5168e2ba7b2ed35a4898ee95adc91e8278c6a5cbc180b9" &
                                                 "3ec2062c683938fab31d3a15d281ce51c7646bfad59cd1a93686ba28d890f57fb6d1af9477e073" &
                                                 "49eebada"
    End Module
End Namespace
