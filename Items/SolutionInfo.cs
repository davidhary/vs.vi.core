﻿using System.Reflection;
using System.Resources;

[assembly: AssemblyCompany( "Integrated Scientific Resources" )]
[assembly: AssemblyCopyright( "(c) 2012 Scientific Resources, Inc. All rights reserved." )]
[assembly: AssemblyTrademark( "Licensed under The MIT License." )]
[assembly: NeutralResourcesLanguage( "en-US", UltimateResourceFallbackLocation.MainAssembly )]
[assembly: AssemblyCulture( "" )]
