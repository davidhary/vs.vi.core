Imports Ivi.Visa

''' <summary> Summary description for SelectResource. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class SelectResource
    Inherits System.Windows.Forms.Form

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        '
        ' Required for Windows Form Designer support
        '
        Me.InitializeComponent()

    End Sub

    ''' <summary> Clean up any resources being used. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me.components IsNot Nothing Then
                Me.components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ''' <summary> Event handler. Called by MyBase for load events. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Overloads Sub OnLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' This example uses an instance of the NationalInstruments.Visa.ResourceManager class to find resources on the system.
        ' Alternatively, static methods provided by the Ivi.Visa.ResourceManager class may be used when an application
        ' requires additional VISA .NET implementations.
        Dim resources = Ivi.Visa.GlobalResourceManager.Find("(ASRL|GPIB|TCPIP|USB)?*")
        For Each s As String In resources
            Me.AvailableResourcesListBox.Items.Add(s)
        Next s
    End Sub

    ''' <summary> Available resources list box double click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AvailableResourcesListBox_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles AvailableResourcesListBox.DoubleClick
        Dim selectedString As String = CStr(Me.AvailableResourcesListBox.SelectedItem)
        Me.ResourceName = selectedString
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary> Available resources list box selected index changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AvailableResourcesListBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles AvailableResourcesListBox.SelectedIndexChanged
        Dim selectedString As String = CStr(Me.AvailableResourcesListBox.SelectedItem)
        Me.ResourceName = selectedString
    End Sub

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName() As String
        Get
            Return Me.visaResourceNameTextBox.Text
        End Get
        Set(ByVal value As String)
            Me.visaResourceNameTextBox.Text = value
        End Set
    End Property
End Class
