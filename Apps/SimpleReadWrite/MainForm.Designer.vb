
Partial Public Class MainForm

    Private _WriteTextBox As System.Windows.Forms.TextBox
    Private _ReadTextBox As System.Windows.Forms.TextBox
    Private WithEvents QueryButton As System.Windows.Forms.Button
    Private WithEvents WriteButton As System.Windows.Forms.Button
    Private WithEvents ReadButton As System.Windows.Forms.Button
    Private WithEvents OpenSessionButton As System.Windows.Forms.Button
    Private WithEvents ClearButton As System.Windows.Forms.Button
    Private WithEvents CloseSessionButton As System.Windows.Forms.Button
    Private _WraiteTextBoxLabel As System.Windows.Forms.Label
    Private _ReadTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents ResourcesComboBox As ComboBox
    Private WithEvents ResourcesComboBoxLabel As Label
    Private WithEvents MultipleResourcesCheckBox As CheckBox

    ''' <summary>
    ''' Required designer variable.
    ''' </summary>
    Private _Components As System.ComponentModel.Container = Nothing

#Region "Windows Form Designer generated code"

    ''' <summary>
    ''' Required method for Designer support - do not modify
    ''' the contents of this method with the code editor.
    ''' </summary>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me.QueryButton = New System.Windows.Forms.Button()
        Me.WriteButton = New System.Windows.Forms.Button()
        Me.ReadButton = New System.Windows.Forms.Button()
        Me.OpenSessionButton = New System.Windows.Forms.Button()
        Me._WriteTextBox = New System.Windows.Forms.TextBox()
        Me._ReadTextBox = New System.Windows.Forms.TextBox()
        Me.ClearButton = New System.Windows.Forms.Button()
        Me.CloseSessionButton = New System.Windows.Forms.Button()
        Me._WraiteTextBoxLabel = New System.Windows.Forms.Label()
        Me._ReadTextBoxLabel = New System.Windows.Forms.Label()
        Me.ResourcesComboBox = New System.Windows.Forms.ComboBox()
        Me.ResourcesComboBoxLabel = New System.Windows.Forms.Label()
        Me.MultipleResourcesCheckBox = New System.Windows.Forms.CheckBox()
        Me.ReadStatusByte = New System.Windows.Forms.Button()
        Me.ReadTerminationLabel = New System.Windows.Forms.Label()
        Me.ReadTerminationEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me.ReadTerminationCharacterNumeric = New System.Windows.Forms.NumericUpDown()
        Me.TimeoutNumerc = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.ReadTerminationCharacterNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TimeoutNumerc, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'QueryButton
        '
        Me.QueryButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.QueryButton.Location = New System.Drawing.Point(219, 147)
        Me.QueryButton.Name = "QueryButton"
        Me.QueryButton.Size = New System.Drawing.Size(48, 23)
        Me.QueryButton.TabIndex = 3
        Me.QueryButton.Text = "&Query"
        '
        'WriteButton
        '
        Me.WriteButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.WriteButton.Location = New System.Drawing.Point(274, 147)
        Me.WriteButton.Name = "WriteButton"
        Me.WriteButton.Size = New System.Drawing.Size(48, 23)
        Me.WriteButton.TabIndex = 4
        Me.WriteButton.Text = "&Write"
        '
        'ReadButton
        '
        Me.ReadButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ReadButton.Location = New System.Drawing.Point(327, 147)
        Me.ReadButton.Name = "ReadButton"
        Me.ReadButton.Size = New System.Drawing.Size(48, 23)
        Me.ReadButton.TabIndex = 5
        Me.ReadButton.Text = "&Read"
        '
        'OpenSessionButton
        '
        Me.OpenSessionButton.Location = New System.Drawing.Point(5, 5)
        Me.OpenSessionButton.Name = "OpenSessionButton"
        Me.OpenSessionButton.Size = New System.Drawing.Size(92, 22)
        Me.OpenSessionButton.TabIndex = 0
        Me.OpenSessionButton.Text = "Open Session"
        '
        '_WriteTextBox
        '
        Me._WriteTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._WriteTextBox.Location = New System.Drawing.Point(5, 118)
        Me._WriteTextBox.Name = "_WriteTextBox"
        Me._WriteTextBox.Size = New System.Drawing.Size(368, 20)
        Me._WriteTextBox.TabIndex = 2
        Me._WriteTextBox.Text = "*IDN?\n"
        '
        '_ReadTextBox
        '
        Me._ReadTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ReadTextBox.Location = New System.Drawing.Point(5, 178)
        Me._ReadTextBox.Multiline = True
        Me._ReadTextBox.Name = "_ReadTextBox"
        Me._ReadTextBox.ReadOnly = True
        Me._ReadTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._ReadTextBox.Size = New System.Drawing.Size(368, 191)
        Me._ReadTextBox.TabIndex = 6
        Me._ReadTextBox.TabStop = False
        '
        'ClearButton
        '
        Me.ClearButton.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ClearButton.Location = New System.Drawing.Point(8, 375)
        Me.ClearButton.Name = "ClearButton"
        Me.ClearButton.Size = New System.Drawing.Size(367, 24)
        Me.ClearButton.TabIndex = 7
        Me.ClearButton.Text = "Clear"
        '
        'CloseSessionButton
        '
        Me.CloseSessionButton.Location = New System.Drawing.Point(97, 5)
        Me.CloseSessionButton.Name = "CloseSessionButton"
        Me.CloseSessionButton.Size = New System.Drawing.Size(92, 22)
        Me.CloseSessionButton.TabIndex = 1
        Me.CloseSessionButton.Text = "Close Session"
        '
        '_WraiteTextBoxLabel
        '
        Me._WraiteTextBoxLabel.AutoSize = True
        Me._WraiteTextBoxLabel.Location = New System.Drawing.Point(5, 102)
        Me._WraiteTextBoxLabel.Name = "_WraiteTextBoxLabel"
        Me._WraiteTextBoxLabel.Size = New System.Drawing.Size(77, 13)
        Me._WraiteTextBoxLabel.TabIndex = 8
        Me._WraiteTextBoxLabel.Text = "String to Write:"
        '
        '_ReadTextBoxLabel
        '
        Me._ReadTextBoxLabel.AutoSize = True
        Me._ReadTextBoxLabel.Location = New System.Drawing.Point(5, 158)
        Me._ReadTextBoxLabel.Name = "_ReadTextBoxLabel"
        Me._ReadTextBoxLabel.Size = New System.Drawing.Size(56, 13)
        Me._ReadTextBoxLabel.TabIndex = 9
        Me._ReadTextBoxLabel.Text = "Received:"
        '
        'ResourcesComboBox
        '
        Me.ResourcesComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ResourcesComboBox.FormattingEnabled = True
        Me.ResourcesComboBox.Location = New System.Drawing.Point(5, 73)
        Me.ResourcesComboBox.Name = "ResourcesComboBox"
        Me.ResourcesComboBox.Size = New System.Drawing.Size(367, 21)
        Me.ResourcesComboBox.TabIndex = 10
        '
        'ResourcesComboBoxLabel
        '
        Me.ResourcesComboBoxLabel.AutoSize = True
        Me.ResourcesComboBoxLabel.Location = New System.Drawing.Point(5, 57)
        Me.ResourcesComboBoxLabel.Name = "ResourcesComboBoxLabel"
        Me.ResourcesComboBoxLabel.Size = New System.Drawing.Size(92, 13)
        Me.ResourcesComboBoxLabel.TabIndex = 11
        Me.ResourcesComboBoxLabel.Text = "Resource Names:"
        '
        'MultipleResourcesCheckBox
        '
        Me.MultipleResourcesCheckBox.AutoSize = True
        Me.MultipleResourcesCheckBox.Location = New System.Drawing.Point(195, 8)
        Me.MultipleResourcesCheckBox.Name = "MultipleResourcesCheckBox"
        Me.MultipleResourcesCheckBox.Size = New System.Drawing.Size(90, 17)
        Me.MultipleResourcesCheckBox.TabIndex = 12
        Me.MultipleResourcesCheckBox.Text = "> 1 Resource"
        Me.MultipleResourcesCheckBox.UseVisualStyleBackColor = True
        '
        'ReadStatusByte
        '
        Me.ReadStatusByte.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ReadStatusByte.Location = New System.Drawing.Point(166, 147)
        Me.ReadStatusByte.Name = "ReadStatusByte"
        Me.ReadStatusByte.Size = New System.Drawing.Size(48, 23)
        Me.ReadStatusByte.TabIndex = 13
        Me.ReadStatusByte.Text = "&STB"
        Me.ReadStatusByte.UseVisualStyleBackColor = True
        '
        'ReadTerminationLabel
        '
        Me.ReadTerminationLabel.AutoSize = True
        Me.ReadTerminationLabel.Location = New System.Drawing.Point(5, 37)
        Me.ReadTerminationLabel.Name = "ReadTerminationLabel"
        Me.ReadTerminationLabel.Size = New System.Drawing.Size(118, 13)
        Me.ReadTerminationLabel.TabIndex = 14
        Me.ReadTerminationLabel.Text = "Read Termination Byte:"
        '
        'ReadTerminationEnabledCheckBox
        '
        Me.ReadTerminationEnabledCheckBox.AutoSize = True
        Me.ReadTerminationEnabledCheckBox.Checked = True
        Me.ReadTerminationEnabledCheckBox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ReadTerminationEnabledCheckBox.Location = New System.Drawing.Point(169, 35)
        Me.ReadTerminationEnabledCheckBox.Name = "ReadTerminationEnabledCheckBox"
        Me.ReadTerminationEnabledCheckBox.Size = New System.Drawing.Size(65, 17)
        Me.ReadTerminationEnabledCheckBox.TabIndex = 15
        Me.ReadTerminationEnabledCheckBox.Text = "&Enabled"
        Me.ReadTerminationEnabledCheckBox.UseVisualStyleBackColor = True
        '
        'ReadTerminationCharacterNumeric
        '
        Me.ReadTerminationCharacterNumeric.Location = New System.Drawing.Point(126, 33)
        Me.ReadTerminationCharacterNumeric.Maximum = New Decimal(New Integer() {255, 0, 0, 0})
        Me.ReadTerminationCharacterNumeric.Name = "ReadTerminationCharacterNumeric"
        Me.ReadTerminationCharacterNumeric.Size = New System.Drawing.Size(40, 20)
        Me.ReadTerminationCharacterNumeric.TabIndex = 16
        Me.ReadTerminationCharacterNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        'TimeoutNumerc
        '
        Me.TimeoutNumerc.Location = New System.Drawing.Point(286, 33)
        Me.TimeoutNumerc.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me.TimeoutNumerc.Name = "TimeoutNumerc"
        Me.TimeoutNumerc.Size = New System.Drawing.Size(51, 20)
        Me.TimeoutNumerc.TabIndex = 17
        Me.TimeoutNumerc.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(238, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 18
        Me.Label1.Text = "T/O ms:"
        '
        'MainForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(378, 405)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TimeoutNumerc)
        Me.Controls.Add(Me.ReadTerminationCharacterNumeric)
        Me.Controls.Add(Me.ReadTerminationEnabledCheckBox)
        Me.Controls.Add(Me.ReadTerminationLabel)
        Me.Controls.Add(Me.ReadStatusByte)
        Me.Controls.Add(Me.MultipleResourcesCheckBox)
        Me.Controls.Add(Me.ResourcesComboBoxLabel)
        Me.Controls.Add(Me.ResourcesComboBox)
        Me.Controls.Add(Me._ReadTextBoxLabel)
        Me.Controls.Add(Me._WraiteTextBoxLabel)
        Me.Controls.Add(Me.CloseSessionButton)
        Me.Controls.Add(Me.ClearButton)
        Me.Controls.Add(Me._ReadTextBox)
        Me.Controls.Add(Me._WriteTextBox)
        Me.Controls.Add(Me.OpenSessionButton)
        Me.Controls.Add(Me.ReadButton)
        Me.Controls.Add(Me.WriteButton)
        Me.Controls.Add(Me.QueryButton)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimumSize = New System.Drawing.Size(295, 316)
        Me.Name = "MainForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Simple Read/Write"
        CType(Me.ReadTerminationCharacterNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TimeoutNumerc, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ReadStatusByte As Button
    Private WithEvents ReadTerminationLabel As Label
    Private WithEvents ReadTerminationEnabledCheckBox As CheckBox
    Private WithEvents ReadTerminationCharacterNumeric As NumericUpDown
    Private WithEvents TimeoutNumerc As NumericUpDown
    Private WithEvents Label1 As Label
#End Region

End Class
