Imports Ivi.Visa

''' <summary> A select resources. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class SelectResources

    ''' <summary> Event handler. Called by MyBase for load events. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information to send to registered event handlers. </param>
    Private Overloads Sub OnLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' This example uses an instance of the NationalInstruments.Visa.ResourceManager class to find resources on the system.
        ' Alternatively, static methods provided by the Ivi.Visa.ResourceManager class may be used when an application
        ' requires additional VISA .NET implementations.
        Dim resources = Ivi.Visa.GlobalResourceManager.Find("(ASRL|GPIB|TCPIP|USB)?*")
        For Each s As String In resources
            Me._AvailableResourcesListBox.Items.Add(s)
        Next s
    End Sub

    ''' <summary> Adds a button click to 'e'. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AddButton_Click(sender As Object, e As EventArgs) Handles _AddButton.Click, _AvailableResourcesListBox.DoubleClick
        Dim selectedString As String = CStr(Me._AvailableResourcesListBox.SelectedItem)
        Me._ResourceStringsListBox.Items.Add(selectedString)
    End Sub

    ''' <summary> Removes the button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RemoveButton_Click(sender As Object, e As EventArgs) Handles _RemoveButton.Click, _ResourceStringsListBox.DoubleClick
        Me._ResourceStringsListBox.Items.Remove(Me._ResourceStringsListBox.SelectedItem)
    End Sub

    ''' <summary> Gets a list of names of the resources. </summary>
    ''' <value> A list of names of the resources. </value>
    Public ReadOnly Property ResourceNames As IEnumerable(Of String)
        Get
            Dim l As New List(Of String)
            For Each item As Object In Me._ResourceStringsListBox.Items
                l.Add(CStr(item))
            Next
            Return l.ToArray
        End Get
    End Property


End Class
