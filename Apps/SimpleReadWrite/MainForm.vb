Imports System.Threading

Imports Ivi.Visa

''' <summary> Summary description for Form1. </summary>
''' <remarks> David, 2020-10-11. </remarks>
Public Class MainForm
    Inherits System.Windows.Forms.Form

    ''' <summary> The last resource string. </summary>
    Private _LastResourceString As String = Nothing

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Windows.Forms.Form" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        '
        ' Required for Windows Form Designer support
        '
        Me.InitializeComponent()
        Me.SetupControlState(False)
    End Sub

    ''' <summary> Clean up any resources being used. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me._Session IsNot Nothing Then
                Me._Session.Dispose()
            End If
            If Me._Components IsNot Nothing Then
                Me._Components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

#Region " OPEN / CLOSE SESSIONS "

    ''' <summary> Opens the session. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenSession()
        Using sr As New SelectResource()
            If Me._LastResourceString IsNot Nothing Then
                sr.ResourceName = Me._LastResourceString
            End If
            Dim result As DialogResult = sr.ShowDialog(Me)
            If result = System.Windows.Forms.DialogResult.OK Then
                Me._LastResourceString = sr.ResourceName
                Cursor.Current = Cursors.WaitCursor
                Try
                    Me.ResourcesComboBox.DataSource = Nothing
                    Me.ResourcesComboBox.Items.Clear()
                    Me.ResourcesComboBox.Text = sr.ResourceName
                    Me._Session = CType(Ivi.Visa.GlobalResourceManager.Open(sr.ResourceName), IMessageBasedSession)
                    ' setting to 1000 -- 1000; 1001 -- 1, 4000 -- 10000'
                    Me._Session.TerminationCharacter = CByte(Me.ReadTerminationCharacterNumeric.Value)
                    Me._Session.TerminationCharacterEnabled = Me.ReadTerminationEnabledCheckBox.Checked
                    Me._Session.TimeoutMilliseconds = CInt(Me.TimeoutNumerc.Value)
                    Me.SetupControlState(True)
                Catch e1 As InvalidCastException
                    System.Windows.Forms.MessageBox.Show("Resource selected must be a message-based session")
                Catch exp As Exception
                    System.Windows.Forms.MessageBox.Show(exp.Message)
                Finally
                    Cursor.Current = Cursors.Default
                End Try
            End If
        End Using
    End Sub

    ''' <summary> Opens the sessions. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OpenSessions()
        Using sr As New SelectResources()
            Dim result As DialogResult = sr.ShowDialog(Me)
            If result = System.Windows.Forms.DialogResult.OK Then
                Cursor.Current = Cursors.WaitCursor
                Try
                    Me._Sessions = New List(Of IMessageBasedSession)
                    Dim names As IEnumerable(Of String) = sr.ResourceNames
                    Me.ResourcesComboBox.DataSource = Nothing
                    Me.ResourcesComboBox.Items.Clear()
                    Me.ResourcesComboBox.DataSource = names
                    Dim builder As New System.Text.StringBuilder
                    For Each rs As String In names
                        Me._LastResourceString = rs
                        Dim visaSession As IMessageBasedSession = CType(Ivi.Visa.GlobalResourceManager.Open(rs), IMessageBasedSession)
                        ' setting to 1000 -- 1000; 1001 -- 1, 4000 -- 10000'
                        visaSession.TimeoutMilliseconds = 1001
                        Me._Sessions.Add(visaSession)
                        ' TSP INITIALIZATION CODE:
                        visaSession.RawIO.Write($"_G.status.request_enable=0{vbCr}")

                        Dim solution As Integer = 3
                        If solution = 0 Then
                            ' Error
                            ' getting error -286 TSP Runtime error 
                            ' instrument reports User Abort Error
                            ' Visa reports USRE ABORT
                        ElseIf solution = 1 Then
                            ' still getting error -286 TSP Runtime error
                            ' but instrument connects and trace shows no error.
                        ElseIf solution = 2 Then
                            ' no longer getting error -286 TSP Runtime error
                            Thread.Sleep(11)
                        ElseIf solution = 3 Then
                            ' no longer getting error -286 TSP Runtime error
                            Threading.Thread.Sleep(1)
                        End If

                        ' this is the culprit: 
                        visaSession.Clear()

                        If solution = 0 Then
                            ' error 
                        ElseIf solution = 1 Then
                            Thread.Sleep(11)
                        ElseIf solution = 2 Then
                        ElseIf solution = 3 Then
                        End If

                        visaSession.RawIO.Write($"_G.waitcomplete() _G.print('1'){vbCr}")
                        builder.AppendLine(MainForm.InsertCommonEscapeSequences(visaSession.RawIO.ReadString))
                    Next
                    Me._ReadTextBox.Text = builder.ToString
                    If Me._Sessions.Count > 0 Then
                        Me._Session = Me._Sessions(0)
                        Me.ResourcesComboBox.Text = Me._Session.ResourceName
                        Me.SetupControlState(Me._Session IsNot Nothing)
                    End If
                Catch e1 As InvalidCastException
                    System.Windows.Forms.MessageBox.Show("Resource selected must be a message-based session")
                Catch exp As Exception
                    System.Windows.Forms.MessageBox.Show(exp.Message)
                Finally
                    Cursor.Current = Cursors.Default
                End Try
            End If
        End Using
    End Sub

    ''' <summary> Opens session click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles OpenSessionButton.Click
        If Me.MultipleResourcesCheckBox.Checked Then
            Me.OpenSessions()
        Else
            Me.OpenSession()
        End If
    End Sub

    ''' <summary> Closes session click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub CloseSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CloseSessionButton.Click
        Me.SetupControlState(False)
        If Me._Sessions Is Nothing Then
            If Me._Session IsNot Nothing Then
                Me._Session.Dispose()
            End If
        Else
            Dim sq As New Queue(Of IMessageBasedSession)(Me._Sessions)
            Me._Sessions.Clear()
            Do While sq.Count > 0
                Dim s As IMessageBasedSession = sq.Dequeue
                s.Dispose()
            Loop
        End If
        Me._Session = Nothing
    End Sub

#End Region

#Region " MISC "

    ''' <summary> Sets up the control state. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="isSessionOpen"> True if session is open, false if not. </param>
    Private Sub SetupControlState(ByVal isSessionOpen As Boolean)
        Me.OpenSessionButton.Enabled = Not isSessionOpen
        Me.CloseSessionButton.Enabled = isSessionOpen
        Me.QueryButton.Enabled = isSessionOpen
        Me.WriteButton.Enabled = isSessionOpen
        Me.ReadButton.Enabled = isSessionOpen
        Me._WriteTextBox.Enabled = isSessionOpen
        Me.ClearButton.Enabled = isSessionOpen
        If isSessionOpen Then
            ' _ReadTextBox.Text = String.Empty
            Me._WriteTextBox.Focus()
        End If
    End Sub

    ''' <summary> Replace common escape sequences. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="s"> The string. </param>
    ''' <returns> A String. </returns>
    Private Shared Function ReplaceCommonEscapeSequences(ByVal s As String) As String
        Return s.Replace("\n", vbLf).Replace("\r", vbCr)
    End Function

    ''' <summary> Inserts a common escape sequences described by s. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="s"> The string. </param>
    ''' <returns> A String. </returns>
    Private Shared Function InsertCommonEscapeSequences(ByVal s As String) As String
        Return s.Replace(vbLf, "\n").Replace(vbCr, "\r")
    End Function

    ''' <summary> Multiple resources check box checked changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MultipleResourcesCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles MultipleResourcesCheckBox.CheckedChanged
        If Me.MultipleResourcesCheckBox.Checked Then
            Me.OpenSessionButton.Text = "Open Sessions"
            Me.CloseSessionButton.Text = "Close Sessions"
        Else
            Me.OpenSessionButton.Text = "Open Session"
            Me.CloseSessionButton.Text = "Close Session"
        End If

    End Sub

#End Region

#Region " SESSIONS "

    ''' <summary> The session. </summary>
    Private _Session As IMessageBasedSession

    ''' <summary> The sessions. </summary>
    Private _Sessions As List(Of IMessageBasedSession)

    ''' <summary> Resources combo box selected index changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ResourcesComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ResourcesComboBox.SelectedIndexChanged
        If Me._Sessions IsNot Nothing AndAlso Me.ResourcesComboBox.SelectedIndex >= 0 AndAlso Me._Sessions.Count > Me.ResourcesComboBox.SelectedIndex Then
            Me._Session = Me._Sessions(Me.ResourcesComboBox.SelectedIndex)
        End If
    End Sub

#End Region

#Region " READ / WRITE "

    ''' <summary> Writes. </summary>
    ''' <remarks> David, 2021-03-30. </remarks>
    ''' <param name="value"> The value to write. </param>
    Private Sub Write(ByVal value As String)
        Me._ReadTextBox.AppendText("Write: ")
        Me._Session.RawIO.Write(ReplaceCommonEscapeSequences(value))
        Me._ReadTextBox.AppendText($"{value}{Environment.NewLine}")
        Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
    End Sub

    ''' <summary> Reads this object. </summary>
    ''' <remarks> David, 2021-03-30. </remarks>
    Private Sub Read()
        Me._ReadTextBox.AppendText("Read: ")
        Me._ReadTextBox.AppendText($"{InsertCommonEscapeSequences(Me._Session.RawIO.ReadString())}{Environment.NewLine}")
        Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
    End Sub

    ''' <summary> Queries a click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Query_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles QueryButton.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Write(Me._WriteTextBox.Text)
            Me.Read()
        Catch exp As Exception
            Me._ReadTextBox.AppendText($"Error: {exp.Message}{Environment.NewLine}")
            Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Writes a click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Write_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles WriteButton.Click
        Try
            Me.Write(Me._WriteTextBox.Text)
        Catch exp As Exception
            Me._ReadTextBox.AppendText($"Error: {exp.Message}{Environment.NewLine}")
            Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
            ' System.Windows.Forms.MessageBox.Show(exp.ToString)
        End Try
    End Sub

    ''' <summary> Reads a click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Read_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReadButton.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me.Read()
        Catch exp As Exception
            Me._ReadTextBox.AppendText($"Error: {exp.Message}{Environment.NewLine}")
            Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
            ' System.Windows.Forms.MessageBox.Show(exp.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads status byte click. </summary>
    ''' <remarks> David, 2021-03-30. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Private Sub ReadStatusByte_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ReadStatusByte.Click
        Cursor.Current = Cursors.WaitCursor
        Try
            Me._ReadTextBox.AppendText("Status: ")
            Me._ReadTextBox.AppendText($"{CInt(Me._Session.ReadStatusByte):X2}{Environment.NewLine}")
            Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
        Catch exp As Exception
            Me._ReadTextBox.AppendText($"Error: {exp.Message}{Environment.NewLine}")
            Me._ReadTextBox.SelectionStart = Me._ReadTextBox.Text.Length
            ' System.Windows.Forms.MessageBox.Show(exp.ToString)
        Finally
            Cursor.Current = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Clear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClearButton.Click
        Me._ReadTextBox.Text = String.Empty
    End Sub

    Private Sub TimeoutNumerc_ValueChanged(sender As Object, e As EventArgs) Handles TimeoutNumerc.ValueChanged
        If Me._Session IsNot Nothing Then
            Me._Session.TimeoutMilliseconds = CInt(Me.TimeoutNumerc.Value)
        End If
    End Sub

    Private Sub ReadTerminationCharacter_ValueChanged(sender As Object, e As EventArgs) Handles ReadTerminationCharacterNumeric.ValueChanged,
                                                                                                ReadTerminationEnabledCheckBox.CheckedChanged
        If Me._Session IsNot Nothing Then
            Me._Session.TerminationCharacter = CByte(Me.ReadTerminationCharacterNumeric.Value)
            Me._Session.TerminationCharacterEnabled = Me.ReadTerminationEnabledCheckBox.Checked
        End If
    End Sub

#End Region


End Class
