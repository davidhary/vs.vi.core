﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.VI.Device.Facade.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public MySplashScreen m_MySplashScreen;

            public MySplashScreen MySplashScreen
            {
                [DebuggerHidden]
                get
                {
                    m_MySplashScreen = Create__Instance__(m_MySplashScreen);
                    return m_MySplashScreen;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_MySplashScreen))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_MySplashScreen);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public SplitVisaForm m_SplitVisaForm;

            public SplitVisaForm SplitVisaForm
            {
                [DebuggerHidden]
                get
                {
                    m_SplitVisaForm = Create__Instance__(m_SplitVisaForm);
                    return m_SplitVisaForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_SplitVisaForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_SplitVisaForm);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public Switchboard m_Switchboard;

            public Switchboard Switchboard
            {
                [DebuggerHidden]
                get
                {
                    m_Switchboard = Create__Instance__(m_Switchboard);
                    return m_Switchboard;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_Switchboard))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_Switchboard);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public VisaForm m_VisaForm;

            public VisaForm VisaForm
            {
                [DebuggerHidden]
                get
                {
                    m_VisaForm = Create__Instance__(m_VisaForm);
                    return m_VisaForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_VisaForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_VisaForm);
                }
            }

            [EditorBrowsable(EditorBrowsableState.Never)]
            public VisaTreeViewForm m_VisaTreeViewForm;

            public VisaTreeViewForm VisaTreeViewForm
            {
                [DebuggerHidden]
                get
                {
                    m_VisaTreeViewForm = Create__Instance__(m_VisaTreeViewForm);
                    return m_VisaTreeViewForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_VisaTreeViewForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_VisaTreeViewForm);
                }
            }
        }
    }
}