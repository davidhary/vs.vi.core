﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle( isr.VI.Device.Facade.My.MyApplication.AssemblyTitle )]
[assembly: AssemblyDescription( isr.VI.Device.Facade.My.MyApplication.AssemblyDescription )]
[assembly: AssemblyProduct( isr.VI.Device.Facade.My.MyApplication.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: ComVisible( false )]
