## ISR VI Device Facade<sub>&trade;</sub>: Test Program
\(C\) 2016 Integrated Scientific Resources, Inc. All rights reserved.
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)
* [Runtime Pre-Requisites](#Runtime-Pre-Requisites)

### Revision History

*7.3.7642 2020-12-03*  
Converted to C#.

*7.1.7525 2020-08-08*  
Created from device console.

*6.1.6946 2018-01-07*  
Created from device console.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

## Runtime Pre-Requisites[](#){name=Runtime-Pre-Requisites}

### .Net Framework 4.7.2
[Microsoft /.NET Framework](https://dotnet.microsoft.com/download)
.NET Framework must installed before proceeding with this installation.

### IVI Visa
[IVI VISA](http://www.ivifoundation.org) 5.11 and above is required for accessing devices.
The IVI VISA implementation can be obtained from either one of the following vendors: 
* Compiled using VISA Shared Components version: 5.12.0

#### Keysight I/O Suite
The [Keysight](https://www.keysight.com/en/pd-1985909/io-libraries-suite) I/O Suite 18.1 and above is recommended.
* Compiled using I/O Library Suite revision: 18.1.26209.5 released 2020-10-15.

#### NI VISA 
[NI VISA](https://www.ni.com/en-us/support/downloads/drivers/download.ni-visa.html#346210)
revision 19.0 and above can be used.
