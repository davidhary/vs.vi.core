using System;
using System.Linq;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Device.Facade
{

    /// <summary>
    /// A sealed class the parses the command line and provides the command line values.
    /// </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-02-02, x.x.4050.x. </para>
    /// </remarks>
    public sealed class CommandLineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private CommandLineInfo() : base()
        {
        }

        #endregion

        #region " PARSER "

        /// <summary> Gets the no-operation command line option. </summary>
        /// <value> The no operation option. </value>
        public static string NoOperationOption => "/nop";

        /// <summary> Gets the Device-Enabled option. </summary>
        /// <value> The Device enabled option. </value>
        public static string DevicesEnabledOption => "/d:";

        /// <summary> Gets the virtual instrument name option. </summary>
        /// <value> The virtual instrument name option. </value>
        public static string VirtualInstrumentNameOption => "/vi:";

        /// <summary> Validate the command line. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        /// not handled. </exception>
        /// <param name="commandLineArguments"> The command line arguments. </param>
        public static void ValidateCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArguments )
        {
            if ( commandLineArguments is object )
            {
                foreach ( string argument in commandLineArguments )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else if ( argument.StartsWith( VirtualInstrumentNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else
                    {
                        Nop = argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase )
                            ? true
                            : throw new ArgumentException( $"Unknown command line argument '{argument}' was detected. Should be Ignored.", nameof( commandLineArguments ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( string commandLineArgs )
        {
            if ( !string.IsNullOrWhiteSpace( commandLineArgs ) )
                ParseCommandLine( new System.Collections.ObjectModel.ReadOnlyCollection<string>( commandLineArgs.Split( ' ' ) ) );
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            var commandLineBuilder = new System.Text.StringBuilder();
            if ( commandLineArgs is object )
            {
                foreach ( string argument in commandLineArgs )
                {
                    _ = commandLineBuilder.AppendFormat( "{0} ", argument );
                    if ( argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        Nop = true;
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        DevicesEnabled = argument.Substring( VirtualInstrumentNameOption.Length ).StartsWith( "n", StringComparison.OrdinalIgnoreCase );
                    }
                    else if ( argument.StartsWith( VirtualInstrumentNameOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        VirtualInstrumentName = argument.Substring( VirtualInstrumentNameOption.Length );
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }

            CommandLine = commandLineBuilder.ToString();
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-07-20. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        /// <returns> The (Success As Boolean, Details As String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            string activity = "joining the command line arguments";
            (bool Success, string Details) result = (true, string.Empty);
            if ( commandLineArgs is null || !commandLineArgs.Any() )
            {
                CommandLine = string.Empty;
            }
            else
            {
                try
                {
                    CommandLine = string.Join( ",", commandLineArgs );
                    activity = $"Parsing the commandLine {CommandLine}";
                    // Data.ParseCommandLine(commandLineArgs)
                    ParseCommandLine( commandLineArgs );
                }
                catch ( ArgumentException ex )
                {
                    result = (false, $"Unknown argument value for '{ex.ParamName}' ignored");
                }
                catch ( Exception ex )
                {
                    result = (false, $"Failed {activity};. {ex.ToFullBlownString()}");
                }
            }

            return result;
        }

        #endregion

        #region " COMMAND LINE ELEMENTS "

        /// <summary> Gets or sets the command line. </summary>
        /// <value> The command line. </value>
        public static string CommandLine { get; private set; }

        /// <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
        /// <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
        public static bool? DevicesEnabled { get; set; }

        /// <summary> Gets or sets the no operation option. </summary>
        /// <value> The no-op. </value>
        public static bool Nop { get; set; }

        /// <summary> Name of the virtual instrument. </summary>
        private static string _VirtualInstrumentName = string.Empty;

        /// <summary> Gets or sets the name of the virtual instrument. </summary>
        /// <value> The name of the virtual instrument. </value>
        public static string VirtualInstrumentName
        {
            get => _VirtualInstrumentName;

            set {
                _VirtualInstrumentName = value;
                if ( !Enum.TryParse( value, true, out VirtualInstrument vi ) )
                {
                    vi = Facade.VirtualInstrument.ScpiSession;
                }

                VirtualInstrument = vi;
            }
        }

        /// <summary> Gets or sets the virtual instrument. </summary>
        /// <value> The virtual instrument. </value>
        public static VirtualInstrument? VirtualInstrument { get; set; }

        #endregion

    }

    /// <summary> Values that represent virtual instruments. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public enum VirtualInstrument
    {

        /// <summary> An enum constant representing the switchboard option. </summary>
        [System.ComponentModel.Description( "Switchboard" )]
        Switchboard,

        /// <summary> An enum constant representing the ats 600 option. </summary>
        [System.ComponentModel.Description( "ATS600" )]
        Ats600,

        /// <summary> An enum constant representing the CLT 10 option. </summary>
        [System.ComponentModel.Description( "CLT10" )]
        Clt10,

        /// <summary> An enum constant representing the 4990 option. </summary>
        [System.ComponentModel.Description( "E4990" )]
        E4990,

        /// <summary> An enum constant representing the eg 2000 option. </summary>
        [System.ComponentModel.Description( "EG2000" )]
        EG2000,

        /// <summary> An enum constant representing the 2002 option. </summary>
        [System.ComponentModel.Description( "K2002" )]
        K2002,

        /// <summary> An enum constant representing the 2400 option. </summary>
        [System.ComponentModel.Description( "K2400" )]
        K2400,

        /// <summary> An enum constant representing the 2450 option. </summary>
        [System.ComponentModel.Description( "K2450" )]
        K2450,

        /// <summary> An enum constant representing the 2600 option. </summary>
        [System.ComponentModel.Description( "K2600" )]
        K2600,

        /// <summary> An enum constant representing the 2700 option. </summary>
        [System.ComponentModel.Description( "K2700" )]
        K2700,

        /// <summary> An enum constant representing the 3458 option. </summary>
        [System.ComponentModel.Description( "K3458" )]
        K3458,

        /// <summary> An enum constant representing the 34980 option. </summary>
        [System.ComponentModel.Description( "K34980" )]
        K34980,

        /// <summary> An enum constant representing the 3700rs option. </summary>
        [System.ComponentModel.Description( "K3700 R Meter" )]
        K3700rs,

        /// <summary> An enum constant representing the 3700 option. </summary>
        [System.ComponentModel.Description( "K3700" )]
        K3700,

        /// <summary> An enum constant representing the 6500 option. </summary>
        [System.ComponentModel.Description( "K6500" )]
        K6500,

        /// <summary> An enum constant representing the 7000 option. </summary>
        [System.ComponentModel.Description( "K7000" )]
        K7000,

        /// <summary> An enum constant representing the 7500 option. </summary>
        [System.ComponentModel.Description( "K7500" )]
        K7500,

        /// <summary> An enum constant representing the 7500 s option. </summary>
        [System.ComponentModel.Description( "K7500S" )]
        K7500S,

        /// <summary> An enum constant representing the 1750 option. </summary>
        [System.ComponentModel.Description( "T1750" )]
        T1750,

        /// <summary> An enum constant representing the tsp rig option. </summary>
        [System.ComponentModel.Description( "TspRig" )]
        TspRig,

        /// <summary> An enum constant representing the scpi session option. </summary>
        [System.ComponentModel.Description( "SCPI Session" )]
        ScpiSession,

        /// <summary> An enum constant representing the tsp session option. </summary>
        [System.ComponentModel.Description( "Tsp Session" )]
        TspSession,

        /// <summary> An enum constant representing the tsp 2 session option. </summary>
        [System.ComponentModel.Description( "Tsp2 Session" )]
        Tsp2Session
    }
}
