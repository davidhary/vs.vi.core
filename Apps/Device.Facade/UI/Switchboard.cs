using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Device.Facade
{

    /// <summary> Switches between test panels. </summary>
    /// <remarks>
    /// (c) 2003 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2014-04-05, . based on legacy code. </para>
    /// </remarks>
    public partial class Switchboard : Core.Forma.ListenerFormBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of this class. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public Switchboard() : base()
        {

            // This call is required by the Windows Form Designer.
            this.InitializeComponent();
            this.__DisplayExecutablePathMenuItem.Name = "_DisplayExecutablePathMenuItem";
            this.__OpenVirtualInstrumentConsoleButton.Name = "_OpenVirtualInstrumentConsoleButton";

            // Add any initialization after the InitializeComponent() call

        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            try
            {
                if ( disposing && this.components is object )
                {
                    this.components?.Dispose();
                }
            }
            finally
            {
                base.Dispose( disposing );
            }
        }

        #endregion

        #region " FORM EVENT HANDLERS "

        /// <summary> Event handler. Called by form for load events. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void Form_Load( object sender, EventArgs e )
        {
            try
            {


                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                this._MessagesTextBox.AddMessage( "Loading..." );

                // instantiate form objects
                this.PopulateTestPanelSelector();

                // set the form caption
                this.Text = Extensions.BuildDefaultCaption( "VI Switchboard" );

                // center the form
                this.CenterToScreen();
            }
            catch
            {
                this._MessagesTextBox.AddMessage( "Exception..." );

                // Use throw without an argument in order to preserve the stack location 
                // where the exception was initially raised.
                throw;
            }
            finally
            {
                this._MessagesTextBox.AddMessage( "Loaded." );
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " CONTROL EVENT HANDLERS "

        /// <summary> Displays an executable path menu item click. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void DisplayExecutablePathMenuItem_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"Getting {nameof( Application.ExecutablePath )}";
                var fi = new System.IO.FileInfo( Application.ExecutablePath );
                activity = $"Displaying {nameof( System.IO.FileInfo.FullName )}";
                this._MessagesTextBox.AppendText( fi.FullName );
            }
            catch ( Exception ex )
            {
                this.Cursor = Cursors.Default;
                _ = this.PublishException( activity, ex );
                ex.Data.Add( "@isr", "Unhandled Exception." );
                _ = MessageBox.Show( ex.ToFullBlownString(), "Unhandled Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        #endregion

        #region " SWITCH BOARD "

        /// <summary> Open selected items. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private void OpenButton_Click( object sender, EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                activity = $"Selecting VI";
                VirtualInstrument vi = ( VirtualInstrument ) Conversions.ToInteger( (( KeyValuePair<Enum, string> ) this._VirtualInstrumentsComboBox.SelectedItem).Key );
                My.Settings.Default.LastVirtualInstrument = ( int ) vi;
                activity = $"opening {vi.Description()}";
                var myForm = new VisaTreeViewForm( vi );
                myForm.Show();
            }
            catch ( Exception ex )
            {
                this.Cursor = Cursors.Default;
                _ = this.PublishException( activity, ex );
                ex.Data.Add( "@isr", "Unhandled Exception." );
                _ = MessageBox.Show( ex.ToFullBlownString(), "Unhandled Exception", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly );
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Populates the list of test panels. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        private void PopulateTestPanelSelector()
        {
            this._VirtualInstrumentsComboBox.ComboBox.DataSource = null;
            this._VirtualInstrumentsComboBox.ComboBox.Items.Clear();
            this._VirtualInstrumentsComboBox.ComboBox.DataSource = typeof( VirtualInstrument ).ValueDescriptionPairs();
            this._VirtualInstrumentsComboBox.ComboBox.DisplayMember = nameof( KeyValuePair<Enum, string>.Value );
            this._VirtualInstrumentsComboBox.ComboBox.ValueMember = nameof( KeyValuePair<Enum, string>.Key );
            if ( Enum.IsDefined( typeof( VirtualInstrument ), My.Settings.Default.LastVirtualInstrument ) )
            {
                this._VirtualInstrumentsComboBox.ComboBox.SelectedItem = (( VirtualInstrument ) Conversions.ToInteger( My.Settings.Default.LastVirtualInstrument )).ValueDescriptionPair();
            }
        }

        #endregion

        #region " TALKER "

        /// <summary> Identify talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            VI.My.MyLibrary.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }

    /// <summary>   An extensions. </summary>
    /// <remarks>   David, 2020-12-02. </remarks>
    public static class Extensions
    {

        /// <summary> Adds a message to 'message'. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="box">     The box control. </param>
        /// <param name="message"> The message. </param>
        public static void AddMessage( this TextBox box, string message )
        {
            if ( box is object )
            {
                box.SelectionStart = box.Text.Length;
                box.SelectionLength = 0;
                box.SelectedText = message + Environment.NewLine;
            }
        }

        /// <summary>
        /// Gets the <see cref="DescriptionAttribute"/> of an <see cref="System.Enum"/> type value.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The <see cref="System.Enum"/> type value. </param>
        /// <returns> A string containing the text of the <see cref="DescriptionAttribute"/>. </returns>
        public static string Description( this Enum value )
        {
            if ( value is null )
                return string.Empty;
            string candidate = value.ToString();
            var fieldInfo = value.GetType().GetField( candidate );
            DescriptionAttribute[] attributes = ( DescriptionAttribute[] ) fieldInfo.GetCustomAttributes( typeof( DescriptionAttribute ), false );
            if ( attributes is object && attributes.Length > 0 )
            {
                candidate = attributes[0].Description;
            }

            return candidate;
        }

        /// <summary> Gets a Key Value Pair description item. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="value"> The <see cref="System.Enum"/> type value. </param>
        /// <returns> A list of. </returns>
        public static KeyValuePair<Enum, string> ValueDescriptionPair( this Enum value )
        {
            return new KeyValuePair<Enum, string>( value, value.Description() );
        }

        /// <summary> Value description pairs. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="type"> The type. </param>
        /// <returns> An IList. </returns>
        public static IList ValueDescriptionPairs( this Type type )
        {
            var keyValuePairs = new ArrayList();
            foreach ( Enum value in Enum.GetValues( type ) )
                _ = keyValuePairs.Add( value.ValueDescriptionPair() );
            return keyValuePairs;
        }

        /// <summary> Builds the default caption. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="subtitle"> The subtitle. </param>
        /// <returns> System.String. </returns>
        public static string BuildDefaultCaption( string subtitle )
        {
            var builder = new System.Text.StringBuilder();
            _ = builder.Append( My.MyProject.Application.Info.Title );
            _ = builder.Append( " " );
            _ = builder.Append( My.MyProject.Application.Info.Version.ToString() );
            if ( My.MyProject.Application.Info.Version.Major < 1 )
            {
                _ = builder.Append( "." );
                switch ( My.MyProject.Application.Info.Version.Minor )
                {
                    case 0:
                        {
                            _ = builder.Append( "Alpha" );
                            break;
                        }

                    case 1:
                        {
                            _ = builder.Append( "Beta" );
                            break;
                        }

                    case var @case when 2 <= @case && @case <= 8:
                        {
                            _ = builder.Append( $"RC{My.MyProject.Application.Info.Version.Minor - 1}" );
                            break;
                        }

                    default:
                        {
                            _ = builder.Append( "Gold" );
                            break;
                        }
                }
            }

            if ( !string.IsNullOrWhiteSpace( subtitle ) )
            {
                _ = builder.Append( ": " );
                _ = builder.Append( subtitle );
            }

            return builder.ToString();
        }
    }
}
