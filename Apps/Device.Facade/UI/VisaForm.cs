using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Device.Facade
{

    /// <summary> Form for viewing the visa. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public class VisaForm : VI.Facade.VisaViewForm
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public VisaForm() : this( CommandLineInfo.VirtualInstrument.GetValueOrDefault( VirtualInstrument.ScpiSession ) )
        {
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="instrumentOption"> The instrument option. </param>
        public VisaForm( VirtualInstrument instrumentOption ) : base()
        {
            if ( instrumentOption == VirtualInstrument.Switchboard )
                instrumentOption = VirtualInstrument.ScpiSession;
            switch ( instrumentOption )
            {
                case VirtualInstrument.Ats600:
                    {
                        var device = new Ats600.Ats600Device() {
                            CandidateResourceTitle = My.Settings.Default.ATS600ResourceTitle,
                            CandidateResourceName = My.Settings.Default.ATS600ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new Ats600.Forms.Ats600View( device );
                        break;
                    }

                case VirtualInstrument.EG2000:
                    {
                        var device = new EG2000.EG2000Device() {
                            CandidateResourceTitle = My.Settings.Default.EG2000ResourceTitle,
                            CandidateResourceName = My.Settings.Default.EG2000ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new EG2000.Forms.EG2000View( device );
                        break;
                    }

                case VirtualInstrument.K2400:
                    {
                        var device = new K2400.K2400Device() {
                            CandidateResourceTitle = My.Settings.Default.K2400ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2400ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new K2400.Forms.K2400View( device );
                        break;
                    }

                case VirtualInstrument.K2450:
                    {
                        var device = new Tsp2.K2450.K2450Device() {
                            CandidateResourceTitle = My.Settings.Default.K2450ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2450ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new Tsp2.K2450.Forms.K2450View( device );
                        break;
                    }

                case VirtualInstrument.K2600:
                    {
                        var device = new Tsp.K2600.K2600Device() {
                            CandidateResourceTitle = My.Settings.Default.K2600ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2600ResourceName
                        };
                        this.VisaSessionBase = device;
                        this.VisaView = new Tsp.K2600.Forms.K2600View( device );
                        break;
                    }

                case VirtualInstrument.K2700:
                    {
                        var device = new K2700.K2700Device() {
                            CandidateResourceTitle = My.Settings.Default.K2700ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2700ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new K2700.Forms.K2700View( device );
                        break;
                    }

                case VirtualInstrument.K3700rs:
                    {
                        var device = new Tsp.K3700.K3700Device() {
                            CandidateResourceTitle = My.Settings.Default.K3700ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K3700ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new Tsp.K3700.Forms.K3700View( device ) { WithResistancesMetersView = true };
                        break;
                    }

                case VirtualInstrument.K3700:
                    {
                        var device = new Tsp.K3700.K3700Device() {
                            CandidateResourceTitle = My.Settings.Default.K3700ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K3700ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new Tsp.K3700.Forms.K3700View( device ) { WithResistancesMetersView = false };
                        break;
                    }

                case VirtualInstrument.K7500:
                    {
                        var device = new Tsp2.K7500.K7500Device() {
                            CandidateResourceTitle = My.Settings.Default.K7500ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K7500ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new Tsp2.K7500.Forms.K7500View( device );
                        break;
                    }

                case VirtualInstrument.T1750:
                    {
                        var device = new T1700.T1700Device() {
                            CandidateResourceTitle = My.Settings.Default.T1750ResourceTitle,
                            CandidateResourceName = My.Settings.Default.T1750ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new T1700.Forms.T1700View( device );
                        break;
                    }

                case VirtualInstrument.TspRig:
                    {
                        var device = new Tsp.Rig.TspDevice() {
                            CandidateResourceTitle = My.Settings.Default.K2600ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2600ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaSessionBase = device;
                        this.VisaView = new Tsp.Rig.Forms.RigView( device );
                        break;
                    }

                case VirtualInstrument.ScpiSession:
                    {
                        var session = new VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.ScpiSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.ScpiSessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaSessionBase = session;
                        this.VisaView = new VI.Facade.VisaView( session );
                        break;
                    }

                case VirtualInstrument.TspSession:
                    {
                        var session = new VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.TspSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.TspSessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaSessionBase = session;
                        this.VisaView = new VI.Facade.VisaView( session );
                        break;
                    }

                case VirtualInstrument.Tsp2Session:
                    {
                        var session = new VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.Tsp2SessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.Tsp2SessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaSessionBase = session;
                        this.VisaView = new VI.Facade.VisaView( session );
                        break;
                    }

                default:
                    {
                        var session = new VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.ScpiSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.ScpiSessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaSessionBase = session;
                        this.VisaView = new VI.Facade.VisaView( session );
                        break;
                    }
            }

            this.VisaViewDisposeEnabled = true;
            if ( this.VisaViewEnabled )
            {
                this.VisaView.ApplyTalkerTraceLevel( Core.ListenerType.Display, TraceEventType.Verbose );
                this.VisaView.ApplyTalkerTraceLevel( Core.ListenerType.Logger, TraceEventType.Verbose );
            }
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.VisaView is object )
            {
                this.VisaView.Dispose();
                this.VisaView = null;
            }
            // the session needs to be disposed after the device releases the session.
            if ( this.VisaSessionBase is object )
            {
                this.VisaSessionBase.Dispose();
                this.VisaSessionBase = null;
            }

            base.Dispose( disposing );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            if ( !this.VisaViewEnabled )
                return;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                var inst = CommandLineInfo.VirtualInstrument.GetValueOrDefault( VirtualInstrument.ScpiSession );
                if ( inst == VirtualInstrument.Switchboard )
                    inst = VirtualInstrument.ScpiSession;
                switch ( inst )
                {
                    case VirtualInstrument.Ats600:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ATS600ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ATS600ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.EG2000:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.EG2000ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.EG2000ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2002:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2002ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2002ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2400:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2400ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2400ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2450:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2450ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2450ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2600:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2600ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                            }

                            if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                            {
                                My.Settings.Default.K2600ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                            }

                            break;
                        }

                    case VirtualInstrument.K2700:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2700ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2700ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K3700rs:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K3700ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K3700ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K3700:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K3700ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K3700ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K7000:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K7000ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K7000ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K7500:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K7500ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K7500ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K7500S:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K7500ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K7500ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.T1750:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.T1750ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.T1750ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.TspRig:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2600ResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2600ResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.ScpiSession:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ScpiSessionResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ScpiSessionResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.TspSession:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.TspSessionResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.TspSessionResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.Tsp2Session:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.Tsp2SessionResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.Tsp2SessionResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    default:
                        {
                            if ( this.VisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ScpiSessionResourceName = this.VisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ScpiSessionResourceTitle = this.VisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary> Gets the visa view enabled. </summary>
        /// <value> The visa view enabled. </value>
        private bool VisaViewEnabled => this.VisaView is object;

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            if ( this.VisaViewEnabled )
            {
                this.AddListener( My.MyProject.Application.Logger );
            }

            base.OnLoad( e );
        }

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyApplication.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
