using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Device.Facade
{

    /// <summary> Form for viewing the split visa. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public class SplitVisaForm : VI.Facade.SplitVisaViewForm
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public SplitVisaForm() : this( CommandLineInfo.VirtualInstrument.GetValueOrDefault( VirtualInstrument.ScpiSession ) )
        {
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="instrumentOption"> The instrument option. </param>
        public SplitVisaForm( VirtualInstrument instrumentOption ) : base()
        {
            if ( instrumentOption == VirtualInstrument.Switchboard )
                instrumentOption = VirtualInstrument.ScpiSession;
            switch ( instrumentOption )
            {
                case VirtualInstrument.ScpiSession:
                    {
                        this.VisaSessionBase = new VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.ScpiSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.ScpiSessionResourceName
                        };
                        break;
                    }

                case VirtualInstrument.TspSession:
                    {
                        this.VisaSessionBase = new Tsp.VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.TspSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.TspSessionResourceName
                        };
                        break;
                    }

                case VirtualInstrument.Tsp2Session:
                    {
                        this.VisaSessionBase = new Tsp2.VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.Tsp2SessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.Tsp2SessionResourceName
                        };
                        break;
                    }

                default:
                    {
                        this.VisaSessionBase = new VisaSession() {
                            CandidateResourceTitle = My.Settings.Default.ScpiSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.ScpiSessionResourceName
                        };
                        break;
                    }
            }

            var view = VI.Facade.SplitVisaSessionView.Create();
            this.VisaSessionBase.AddListeners( this.Talker );
            view.BindVisaSessionBase( this.VisaSessionBase );
            this.SplitVisaView = view;
            this.VisaViewDisposeEnabled = true;
            if ( this.VisaViewEnabled )
            {
                this.SplitVisaView.ApplyTalkerTraceLevel( Core.ListenerType.Display, TraceEventType.Verbose );
                this.SplitVisaView.ApplyTalkerTraceLevel( Core.ListenerType.Logger, TraceEventType.Verbose );
            }
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.SplitVisaView is object )
            {
                this.SplitVisaView.Dispose();
                this.SplitVisaView = null;
            }
            // the session needs to be disposed after the device releases the session.
            if ( this.VisaSessionBase is object )
                this.VisaSessionBase.Dispose();
            this.VisaSessionBase = null;
            base.Dispose( disposing );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            if ( !this.VisaViewEnabled )
                return;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                var inst = CommandLineInfo.VirtualInstrument.GetValueOrDefault( VirtualInstrument.ScpiSession );
                if ( inst == VirtualInstrument.Switchboard )
                    inst = VirtualInstrument.ScpiSession;
                switch ( inst )
                {
                    case VirtualInstrument.ScpiSession:
                        {
                            if ( this.SplitVisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ScpiSessionResourceName = this.SplitVisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.SplitVisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ScpiSessionResourceTitle = this.SplitVisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.TspSession:
                        {
                            if ( this.SplitVisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.TspSessionResourceName = this.SplitVisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.SplitVisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.TspSessionResourceTitle = this.SplitVisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.Tsp2Session:
                        {
                            if ( this.SplitVisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.Tsp2SessionResourceName = this.SplitVisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.SplitVisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.Tsp2SessionResourceTitle = this.SplitVisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    default:
                        {
                            if ( this.SplitVisaView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ScpiSessionResourceName = this.SplitVisaView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.SplitVisaView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ScpiSessionResourceTitle = this.SplitVisaView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary> Gets the visa view enabled. </summary>
        /// <value> The visa view enabled. </value>
        private bool VisaViewEnabled => this.SplitVisaView is object;

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        protected override void OnLoad( EventArgs e )
        {
            if ( this.VisaViewEnabled )
            {
                this.AddListener( My.MyProject.Application.Logger );
            }

            base.OnLoad( e );
        }

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyApplication.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
