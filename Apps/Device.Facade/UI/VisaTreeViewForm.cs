using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core.SplitExtensions;
using isr.VI.ExceptionExtensions;

namespace isr.VI.Device.Facade
{

    /// <summary> Form for viewing the visa tree view. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public class VisaTreeViewForm : VI.Facade.VisaTreeViewForm
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public VisaTreeViewForm() : this( CommandLineInfo.VirtualInstrument.GetValueOrDefault( VirtualInstrument.ScpiSession ) )
        {
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="instrumentOption"> The instrument option. </param>
        public VisaTreeViewForm( VirtualInstrument instrumentOption ) : base()
        {
            this.InstrumentOption = instrumentOption;
        }

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.VisaTreeView is object )
            {
                var session = this.VisaTreeView.VisaSessionBase;
                this.VisaTreeView.Dispose();
                this.VisaTreeView = null;
                // the session needs to be disposed after the device releases the session.
                if ( session is object )
                    session.Dispose();
            }

            base.Dispose( disposing );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            if ( !this.VisaTreeViewEnabled )
                return;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                var inst = CommandLineInfo.VirtualInstrument.GetValueOrDefault( VirtualInstrument.ScpiSession );
                if ( inst == VirtualInstrument.Switchboard )
                    inst = VirtualInstrument.ScpiSession;
                switch ( inst )
                {
                    case VirtualInstrument.Ats600:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ATS600ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ATS600ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.Clt10:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.Clt10ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.Clt10ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.EG2000:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.EG2000ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.EG2000ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2002:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2002ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2002ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2400:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2400ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2400ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2450:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2450ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2450ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K2600:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2600ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                            }

                            if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                            {
                                My.Settings.Default.K2600ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                            }

                            break;
                        }

                    case VirtualInstrument.K2700:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2700ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                            }

                            if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                            {
                                My.Settings.Default.K2700ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                            }

                            break;
                        }

                    case VirtualInstrument.K3700rs:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K3700ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K3700ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K3700:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K3700ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K3700ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K7000:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K7000ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K7000ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K7500:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K7500ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K7500ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.K7500S:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K7500ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K7500ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.T1750:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.T1750ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.T1750ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.TspRig:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.K2600ResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.K2600ResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.ScpiSession:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ScpiSessionResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ScpiSessionResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.TspSession:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.TspSessionResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.TspSessionResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    case VirtualInstrument.Tsp2Session:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.Tsp2SessionResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.Tsp2SessionResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }

                    default:
                        {
                            if ( this.VisaTreeView.VisaSessionBase.ResourceNameConnected )
                            {
                                My.Settings.Default.ScpiSessionResourceName = this.VisaTreeView.VisaSessionBase.OpenResourceName;
                                if ( !string.IsNullOrWhiteSpace( this.VisaTreeView.VisaSessionBase.OpenResourceTitle ) )
                                {
                                    My.Settings.Default.ScpiSessionResourceTitle = this.VisaTreeView.VisaSessionBase.OpenResourceTitle;
                                }
                            }

                            break;
                        }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary> Gets the visa tree view enabled. </summary>
        /// <value> The visa tree view enabled. </value>
        private bool VisaTreeViewEnabled => this.VisaTreeView is object;

        /// <summary> Gets or sets the instrument option. </summary>
        /// <value> The instrument option. </value>
        public VirtualInstrument InstrumentOption { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            try
            {
                // Turn on the form hourglass cursor
                this.Cursor = Cursors.WaitCursor;
                Trace.CorrelationManager.StartLogicalOperation( System.Reflection.MethodBase.GetCurrentMethod().Name );
                if ( !this.DesignMode )
                {
                    // add listeners before the first talker publish command
                    this.AddPrivateListeners();
                    this.AddListener( My.MyProject.Application.Logger );
                    this.LoadInstrumentView( this.InstrumentOption );
                }
                // set the form caption
                string suffix = Core.ApplianceBase.InDesignMode ? ".IDE" : "";

#if rc
                suffix = My.MyApplication.InDesignMode ? ".IDE.RC" : ".RC";
#endif
                this.Text = Core.ApplicationInfo.BuildApplicationDescriptionCaption( $"{nameof( VisaTreeViewForm ).SplitWords()}{suffix}" );
                Application.DoEvents();
            }
            catch ( Exception ex )
            {
                ex.Data.Add( "@isr", "Exception loading the form." );
                My.MyProject.Application.Logger.WriteExceptionDetails( ex, My.MyApplication.TraceEventId );
                if ( Core.MyDialogResult.Abort == Core.WindowsForms.ShowDialogAbortIgnore( ex, Core.MyMessageBoxIcon.Error ) )
                {
                    Application.Exit();
                }
            }
            finally
            {
                try
                {
                    base.OnLoad( e );
                    Trace.CorrelationManager.StopLogicalOperation();
                }
                finally
                {
                }

                this.Cursor = Cursors.Default;
            }
        }

        /// <summary> Loads instrument view. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="instrumentOption"> The instrument option. </param>
        public void LoadInstrumentView( VirtualInstrument instrumentOption )
        {
            if ( instrumentOption == VirtualInstrument.Switchboard )
                instrumentOption = VirtualInstrument.ScpiSession;
            switch ( instrumentOption )
            {
                case VirtualInstrument.Ats600:
                    {
                        var device = new Ats600.Ats600Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.ATS600ResourceTitle,
                            CandidateResourceName = My.Settings.Default.ATS600ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Ats600.Forms.Ats600TreeView( device );
                        break;
                    }

                case VirtualInstrument.Clt10:
                    {
                        var device = new Clt10.Clt10Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.Clt10ResourceTitle,
                            CandidateResourceName = My.Settings.Default.Clt10ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Clt10.Forms.TreeView( device );
                        break;
                    }

                case VirtualInstrument.EG2000:
                    {
                        var device = new EG2000.EG2000Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.EG2000ResourceTitle,
                            CandidateResourceName = My.Settings.Default.EG2000ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new EG2000.Forms.EG2000TreeView( device );
                        break;
                    }

                case VirtualInstrument.K2002:
                    {
                        var device = new K2002.K2002Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K2002ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2002ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new K2002.Forms.TreeView( device );
                        break;
                    }

                case VirtualInstrument.K2400:
                    {
                        var device = new K2400.K2400Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K2400ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2400ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new K2400.Forms.K2400TreeView( device );
                        break;
                    }

                case VirtualInstrument.K2450:
                    {
                        var device = new Tsp2.K2450.K2450Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K2450ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2450ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Tsp2.K2450.Forms.K2450TreeView( device );
                        break;
                    }

                case VirtualInstrument.K2600:
                    {
                        var device = new Tsp.K2600.K2600Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K2600ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2600ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Tsp.K2600.Forms.K2600TreeView( device );
                        break;
                    }

                case VirtualInstrument.K2700:
                    {
                        var device = new K2700.K2700Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K2700ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2700ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new K2700.Forms.K2700TreeView( device );
                        break;
                    }

                case VirtualInstrument.K3700rs:
                    {
                        var device = new Tsp.K3700.K3700Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K3700ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K3700ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Tsp.K3700.Forms.K3700TreeView( device ) { WithResistancesMetersView = true };
                        break;
                    }

                case VirtualInstrument.K3700:
                    {
                        var device = new Tsp.K3700.K3700Device() {
                            CandidateResourceTitle = My.Settings.Default.K3700ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K3700ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Tsp.K3700.Forms.K3700TreeView( device ) { WithResistancesMetersView = false };
                        break;
                    }

                case VirtualInstrument.K7500:
                    {
                        var device = new Tsp2.K7500.K7500Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K7500ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K7500ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new Tsp2.K7500.Forms.K7500TreeView( device );
                        break;
                    }

                case VirtualInstrument.T1750:
                    {
                        var device = new T1700.T1700Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.T1750ResourceTitle,
                            CandidateResourceName = My.Settings.Default.T1750ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new T1700.Forms.T1700TreeView( device );
                        break;
                    }

                case VirtualInstrument.TspRig:
                    {
                        var device = new Tsp.Rig.TspDevice() {
                            SubsystemSupportMode = SubsystemSupportMode.Native,
                            CandidateResourceTitle = My.Settings.Default.K2600ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K2600ResourceName
                        };
                        this.VisaTreeView = new Tsp.Rig.Forms.RigTreeView( device );
                        break;
                    }

                case VirtualInstrument.K7000:
                    {
                        var device = new K7000.K7000Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K7000ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K7000ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new K7000.Forms.TreeView( device );
                        break;
                    }

                case VirtualInstrument.K7500S:
                    {
                        var device = new K7500.K7500Device() {
                            SubsystemSupportMode = SubsystemSupportMode.Full,
                            CandidateResourceTitle = My.Settings.Default.K7500ResourceTitle,
                            CandidateResourceName = My.Settings.Default.K7500ResourceName
                        };
                        device.AddListeners( this.Talker );
                        this.VisaTreeView = new K7500.Forms.TreeView( device );
                        break;
                    }

                case VirtualInstrument.ScpiSession:
                    {
                        var session = new VisaSession() {
                            SubsystemSupportMode = SubsystemSupportMode.Native,
                            CandidateResourceTitle = My.Settings.Default.ScpiSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.ScpiSessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaTreeView = new VI.Facade.VisaTreeView( session );
                        break;
                    }

                case VirtualInstrument.TspSession:
                    {
                        var session = new VisaSession() {
                            SubsystemSupportMode = SubsystemSupportMode.Native,
                            CandidateResourceTitle = My.Settings.Default.TspSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.TspSessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaTreeView = new VI.Facade.VisaTreeView( session );
                        break;
                    }

                case VirtualInstrument.Tsp2Session:
                    {
                        var session = new VisaSession() {
                            SubsystemSupportMode = SubsystemSupportMode.Native,
                            CandidateResourceTitle = My.Settings.Default.Tsp2SessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.Tsp2SessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaTreeView = new VI.Facade.VisaTreeView( session );
                        break;
                    }

                default:
                    {
                        var session = new VisaSession() {
                            SubsystemSupportMode = SubsystemSupportMode.Native,
                            CandidateResourceTitle = My.Settings.Default.ScpiSessionResourceTitle,
                            CandidateResourceName = My.Settings.Default.ScpiSessionResourceName
                        };
                        session.AddListeners( this.Talker );
                        this.VisaTreeView = new VI.Facade.VisaTreeView( session );
                        break;
                    }
            }

            this.VisaTreeViewDisposeEnabled = true;
            if ( this.VisaTreeViewEnabled )
            {
                this.VisaTreeView.ApplyTalkerTraceLevel( Core.ListenerType.Display, TraceEventType.Verbose );
                this.VisaTreeView.ApplyTalkerTraceLevel( Core.ListenerType.Logger, TraceEventType.Verbose );
            }
        }

        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyApplication.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
