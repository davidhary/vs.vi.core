using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Microsoft.VisualBasic.CompilerServices;

namespace isr.VI.Device.Facade
{
    [DesignerGenerated()]
    public partial class Switchboard
    {

        // Required by the Windows Form Designer
        private System.ComponentModel.IContainer components;

        // NOTE: The following procedure is required by the Windows Form Designer
        // It can be modified using the Windows Form Designer.  
        // Do not modify it using the code editor.
        [DebuggerStepThrough()]
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            var resources = new System.ComponentModel.ComponentResourceManager(typeof(Switchboard));
            _ControlsPanel = new Panel();
            ToolStrip1 = new ToolStrip();
            _OptionsSplitButton = new ToolStripSplitButton();
            __DisplayExecutablePathMenuItem = new ToolStripMenuItem();
            __DisplayExecutablePathMenuItem.Click += new EventHandler(DisplayExecutablePathMenuItem_Click);
            __OpenVirtualInstrumentConsoleButton = new ToolStripButton();
            __OpenVirtualInstrumentConsoleButton.Click += new EventHandler(OpenButton_Click);
            _VirtualInstrumentsComboBox = new ToolStripComboBox();
            _MessagesTextBox = new TextBox();
            _ControlsPanel.SuspendLayout();
            ToolStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // _ControlsPanel
            // 
            _ControlsPanel.Controls.Add(ToolStrip1);
            _ControlsPanel.Dock = DockStyle.Bottom;
            _ControlsPanel.Location = new Point(0, 328);
            _ControlsPanel.Margin = new Padding(3, 4, 3, 4);
            _ControlsPanel.Name = "_ControlsPanel";
            _ControlsPanel.Size = new Size(516, 34);
            _ControlsPanel.TabIndex = 15;
            // 
            // ToolStrip1
            // 
            ToolStrip1.Items.AddRange(new ToolStripItem[] { _OptionsSplitButton, __OpenVirtualInstrumentConsoleButton, _VirtualInstrumentsComboBox });
            ToolStrip1.Location = new Point(0, 0);
            ToolStrip1.Name = "ToolStrip1";
            ToolStrip1.Size = new Size(516, 25);
            ToolStrip1.TabIndex = 13;
            ToolStrip1.Text = "ToolStrip1";
            // 
            // _OptionsSplitButton
            // 
            _OptionsSplitButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            _OptionsSplitButton.DropDownItems.AddRange(new ToolStripItem[] { __DisplayExecutablePathMenuItem });
            _OptionsSplitButton.Image = (Image)resources.GetObject("_OptionsSplitButton.Image");
            _OptionsSplitButton.ImageTransparentColor = Color.Magenta;
            _OptionsSplitButton.Name = "_OptionsSplitButton";
            _OptionsSplitButton.Size = new Size(63, 22);
            _OptionsSplitButton.Text = "Select...";
            _OptionsSplitButton.ToolTipText = "Select option";
            // 
            // _DisplayExecutablePathMenuItem
            // 
            __DisplayExecutablePathMenuItem.Name = "__DisplayExecutablePathMenuItem";
            __DisplayExecutablePathMenuItem.Size = new Size(194, 22);
            __DisplayExecutablePathMenuItem.Text = "Display Execution Path";
            __DisplayExecutablePathMenuItem.ToolTipText = "Displays the execution path";
            // 
            // _OpenVirtualInstrumentConsoleButton
            // 
            __OpenVirtualInstrumentConsoleButton.Alignment = ToolStripItemAlignment.Right;
            __OpenVirtualInstrumentConsoleButton.DisplayStyle = ToolStripItemDisplayStyle.Text;
            __OpenVirtualInstrumentConsoleButton.Image = (Image)resources.GetObject("_OpenVirtualInstrumentConsoleButton.Image");
            __OpenVirtualInstrumentConsoleButton.ImageTransparentColor = Color.Magenta;
            __OpenVirtualInstrumentConsoleButton.Name = "__OpenVirtualInstrumentConsoleButton";
            __OpenVirtualInstrumentConsoleButton.Size = new Size(49, 22);
            __OpenVirtualInstrumentConsoleButton.Text = "Open...";
            // 
            // _VirtualInstrumentsComboBox
            // 
            _VirtualInstrumentsComboBox.Alignment = ToolStripItemAlignment.Right;
            _VirtualInstrumentsComboBox.AutoSize = false;
            _VirtualInstrumentsComboBox.Name = "_VirtualInstrumentsComboBox";
            _VirtualInstrumentsComboBox.Size = new Size(160, 25);
            _VirtualInstrumentsComboBox.Text = "Select virtual instrument";
            // 
            // _MessagesTextBox
            // 
            _MessagesTextBox.BackColor = SystemColors.Info;
            _MessagesTextBox.CausesValidation = false;
            _MessagesTextBox.Dock = DockStyle.Fill;
            _MessagesTextBox.Location = new Point(0, 0);
            _MessagesTextBox.Margin = new Padding(3, 4, 3, 4);
            _MessagesTextBox.Multiline = true;
            _MessagesTextBox.Name = "_MessagesTextBox";
            _MessagesTextBox.ReadOnly = true;
            _MessagesTextBox.ScrollBars = ScrollBars.Both;
            _MessagesTextBox.Size = new Size(516, 328);
            _MessagesTextBox.TabIndex = 16;
            // 
            // Switchboard
            // 
            AutoScaleDimensions = new SizeF(7.0f, 17.0f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(516, 362);
            Controls.Add(_MessagesTextBox);
            Controls.Add(_ControlsPanel);
            Icon = (Icon)resources.GetObject("$this.Icon");
            Margin = new Padding(3, 4, 3, 4);
            Name = "Switchboard";
            Text = "Form1";
            _ControlsPanel.ResumeLayout(false);
            _ControlsPanel.PerformLayout();
            ToolStrip1.ResumeLayout(false);
            ToolStrip1.PerformLayout();
            Load += new EventHandler(Form_Load);
            ResumeLayout(false);
            PerformLayout();
        }

        private Panel _ControlsPanel;

        /// <summary> The with events control. </summary>
        private TextBox _MessagesTextBox;
        private ToolStrip ToolStrip1;
        private ToolStripSplitButton _OptionsSplitButton;
        private ToolStripMenuItem __DisplayExecutablePathMenuItem;

        private ToolStripMenuItem _DisplayExecutablePathMenuItem
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __DisplayExecutablePathMenuItem;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__DisplayExecutablePathMenuItem != null)
                {
                    __DisplayExecutablePathMenuItem.Click -= DisplayExecutablePathMenuItem_Click;
                }

                __DisplayExecutablePathMenuItem = value;
                if (__DisplayExecutablePathMenuItem != null)
                {
                    __DisplayExecutablePathMenuItem.Click += DisplayExecutablePathMenuItem_Click;
                }
            }
        }

        private ToolStripButton __OpenVirtualInstrumentConsoleButton;

        private ToolStripButton _OpenVirtualInstrumentConsoleButton
        {
            [MethodImpl(MethodImplOptions.Synchronized)]
            get
            {
                return __OpenVirtualInstrumentConsoleButton;
            }

            [MethodImpl(MethodImplOptions.Synchronized)]
            set
            {
                if (__OpenVirtualInstrumentConsoleButton != null)
                {
                    __OpenVirtualInstrumentConsoleButton.Click -= OpenButton_Click;
                }

                __OpenVirtualInstrumentConsoleButton = value;
                if (__OpenVirtualInstrumentConsoleButton != null)
                {
                    __OpenVirtualInstrumentConsoleButton.Click += OpenButton_Click;
                }
            }
        }

        private ToolStripComboBox _VirtualInstrumentsComboBox;
    }
}
