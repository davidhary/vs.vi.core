
using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.Core;
using isr.VI.Ttm.ExceptionExtensions;

namespace isr.VI.Ttm.My
{

    /// <summary> my application. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    internal partial class MyApplication
    {

        /// <summary> Gets the identifier of the trace source. </summary>
        public const int TraceEventId = ( int ) Pith.My.ProjectTraceEventId.TtmConsole;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "TTM Driver Console";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Thermal Transient Meter Driver Console";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "TTM.Driver.Console";

        #region " APPLIANCE METHODS "

        /// <summary> Applies the given value. </summary>
        /// <remarks> David, 2020-10-26. </remarks>
        /// <param name="value"> The value. </param>
        public void Apply( Logger value )
        {
            Appliance.Apply( value );
            VI.My.MyLibrary.Appliance.Apply( value );
        }

        #endregion

        #region " EVENT HANDLING METHODS "

        /// <summary>   Releases the splash. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        internal void ReleaseSplashScreen()
        {
            this.SplashScreen = null;
        }

        /// <summary>   Creates splash screen. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        internal void CreateSplashScreen()
        {
            this.SplashScreen = null;
        }

        /// <summary> Writes a log entry and displays it on the splash screen if exists. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="id">        The identifier. </param>
        /// <param name="details">   The details. </param>
        private void WriteLogEntry( TraceEventType eventType, int id, string details )
        {
            (( Core.Forma.BlueSplash ) this.SplashScreen)?.DisplayMessage( details );
            _ = this.Logger.WriteLogEntry( eventType, id, details );
        }

        /// <summary>   Determines if user requested a close. </summary>
        /// <remarks>   David, 2020-09-30. </remarks>
        /// <returns>   True if close requested; false otherwise. </returns>
        internal bool UserCloseRequested()
        {
            return ((( Core.Forma.BlueSplash ) this.SplashScreen)?.IsCloseRequested).GetValueOrDefault( false );
        }

        /// <summary> Instantiates the application to its known state. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <returns> <c>True</c> if success or <c>False</c> if failed. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private bool TryInitializeKnownState()
        {
            string activity = string.Empty;
            try
            {
                Cursor.Current = Cursors.AppStarting;

                // show status
                activity = Appliance.InDesignMode ? "Application is initializing in design mode." : "Application is initializing in runtime mode.";
                _ = this.Logger.WriteLogEntry( TraceEventType.Verbose, TraceEventId, activity );

                // Apply command line results.
                if ( CommandLineInfo.DevicesEnabled.HasValue )
                {
                    activity = $"command line {(CommandLineInfo.DevicesEnabled.Value ? "allows" : "disallows")} devices";
                    _ = this.Logger.WriteLogEntry( TraceEventType.Information, MyApplication.TraceEventId, activity );
                }

                activity = "handle network availability change";
                this.HandleNetworkAvailabilityChanged();

                return true;
            }
            catch ( Exception ex )
            {

                // Turn off the hourglass
                Cursor.Current = Cursors.Default;
                _ = this.Logger.WriteLogEntry( TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity};. {ex.ToFullBlownString()}" );
                try
                {
                    this.ReleaseSplashScreen();
                }
                finally
                {
                }

                return false;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        /// <summary> Processes the shut down. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        private void ProcessShutDown()
        {
            MyProject.Application.SaveMySettingsOnExit = true;
            if ( MyProject.Application.SaveMySettingsOnExit )
            {
                // Save library settings here. Assembly settings are saved elsewhere.
                isr.VI.Ttm.Properties.Settings.Default.Save();
            }
        }


        /// <summary>
        /// Processes the startup. Sets the event arguments
        /// <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs"/> cancel
        /// value if failed.
        /// </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="e"> The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" />
        /// instance containing the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private (bool Success, string Details) ProcessStartup( Microsoft.VisualBasic.ApplicationServices.StartupEventArgs e )
        {
            (bool Success, string Details) result = (true, string.Empty);
            if ( !e.Cancel )
            {
                string activity = string.Empty;
                try
                {
                    activity = "parsing the command line";
                    _ = this.Logger.WriteLogEntry( TraceEventType.Verbose, MyApplication.TraceEventId, activity );
                    result = CommandLineInfo.TryParseCommandLine( e.CommandLine );
                }
                catch ( Exception ex )
                {
                    result = (false, $"Exception {activity};. {ex.ToFullBlownString()}");
                    this.Logger.WriteExceptionDetails( ex, TraceEventType.Error, MyApplication.TraceEventId, $"Exception {activity}" );
                }
            }
            e.Cancel = !result.Success;
            return result;
        }

        #endregion

        #region " NETWORK EVENTS "

        /// <summary> Occurs when the network connection is connected or disconnected. </summary>
        /// <remarks> David, 2020-09-30. </remarks>
        /// <param name="sender"> The source of the event. </param>
        /// <param name="e">      Network available event information. </param>
        private void HandleNetworkAvailabilityChanged( object sender, Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs e )
        {
        }

        #endregion

    }
}

