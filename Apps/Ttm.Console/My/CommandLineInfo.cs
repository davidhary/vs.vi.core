using System;
using System.Diagnostics;
using System.Linq;

using isr.VI.Ttm.ExceptionExtensions;

namespace isr.VI.Ttm
{

    /// <summary>
    /// A sealed class the parses the command line and provides the command line values.
    /// </summary>
    /// <remarks>
    /// (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2011-02-02, x.x.4050.x. </para>
    /// </remarks>
    public sealed class CommandLineInfo
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private CommandLineInfo() : base()
        {
        }

        #endregion

        #region " PARSER "

        /// <summary> Gets the no-operation command line option. </summary>
        /// <value> The no operation option. </value>
        public static string NoOperationOption => "/nop";

        /// <summary> Gets the Device-Enabled option. </summary>
        /// <value> The Device enabled option. </value>
        public static string DevicesEnabledOption => "/d:";

        /// <summary> Validate the command line. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> This exception is raised if a command line argument is
        /// not handled. </exception>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ValidateCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            if ( commandLineArgs is null || !commandLineArgs.Any() )
            {
                CommandLine = string.Empty;
            }
            else
            {
                CommandLine = string.Join( ",", commandLineArgs );
                foreach ( string argument in commandLineArgs )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                    }
                    else
                    {
                        Nop = argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase )
                            ? true
                            : throw new ArgumentException( $"Unknown command line argument '{argument}' was detected. Should be Ignored.", nameof( commandLineArgs ) );
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        public static void ParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            if ( commandLineArgs is null || !commandLineArgs.Any() )
            {
                CommandLine = string.Empty;
            }
            else
            {
                CommandLine = string.Join( ",", commandLineArgs );
                foreach ( string argument in commandLineArgs )
                {
                    if ( false )
                    {
                    }
                    else if ( argument.StartsWith( DevicesEnabledOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        string value = argument.Substring( DevicesEnabledOption.Length );
                        DevicesEnabled = !value.StartsWith( "n", StringComparison.OrdinalIgnoreCase );
                    }
                    else if ( argument.StartsWith( NoOperationOption, StringComparison.OrdinalIgnoreCase ) )
                    {
                        Nop = true;
                    }
                    else
                    {
                        // do nothing
                    }
                }
            }
        }

        /// <summary> Parses the command line. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="commandLineArgs"> The command line arguments. </param>
        /// <returns> True if success or false if Exception occurred. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        public static (bool Success, string Details) TryParseCommandLine( System.Collections.ObjectModel.ReadOnlyCollection<string> commandLineArgs )
        {
            (bool Success, string Details) result = (true, string.Empty);
            string activity = string.Empty;
            if ( commandLineArgs is null || !commandLineArgs.Any() )
            {
                CommandLine = string.Empty;
            }
            else
            {
                try
                {
                    CommandLine = string.Join( ",", commandLineArgs );
                    activity = $"Parsing the commandLine {CommandLine}";
                    CommandLineInfo.ParseCommandLine( commandLineArgs );
                }
                catch ( ArgumentException ex )
                {
                    result = (false, $"Failed {activity};  Unknown argument ignored");
                    My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, result.Details );
                }
                catch ( Exception ex )
                {
                    result = (false, $"Failed {activity};, {ex.ToFullBlownString()} ");
                    My.MyProject.Application.Logger.WriteExceptionDetails( ex, TraceEventType.Error, My.MyApplication.TraceEventId, $"Failed {activity}" );
                }
            }

            return result;
        }

        #endregion

        #region " COMMAND LINE ELEMENTS "

        /// <summary> Gets or sets the command line. </summary>
        /// <value> The command line. </value>
        public static string CommandLine { get; private set; }

        /// <summary> Gets or sets a value indicating whether the system uses actual devices. </summary>
        /// <value> <c>True</c> if using devices; otherwise, <c>False</c>. </value>
        public static bool? DevicesEnabled { get; set; }

        /// <summary> Gets or sets the no operation option. </summary>
        /// <value> The nop. </value>
        public static bool Nop { get; set; }

        #endregion

    }
}
