## ISR VI TTM Console<sub>&trade;</sub>: Thermal Transient Meter Console
\(C\) 2013 Integrated Scientific Resources, Inc. All rights reserved.
* [History](#Revision-History)
* [Supported Instruments](#Supported-Instruments)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History

*7.3.7642 2020-12-03*  
Converted to C#.

*4.0.5850 2016-01-07*  
Uses .NET 4.6.1, IVI 5.6.0 and NI Visa 15.1.

*4.0.5362 2014-09-06*  
Updates NI Libraries to .NET 4.0.

*4.0.5105 2013-12-23*  
Created based on TTM 2.x software.

### Supported Instruments [](#){name=Supported-Instruments}

2600A, 2600B with TTM instrument personality revision 2.3.4009.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  
Firmware for the Thermal Transient Meter instrument was developed and
tested using Eclipse ® from the [Eclipse Foundation](http://www.eclipse.org).

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IO.vi

#### Lua Global Support Libraries:
[LUA](http://www.lua.org)  
[table.copy](http://www.loop.org)  
[Working LUA base-64 codec](http://www.it-rfc.de)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IO,vi)
