﻿using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.VI.Ttm.My
{
    internal static partial class MyProject
    {
        internal partial class MyForms
        {
            [EditorBrowsable(EditorBrowsableState.Never)]
            public TtmForm m_TtmForm;

            public TtmForm TtmForm
            {
                [DebuggerHidden]
                get
                {
                    m_TtmForm = Create__Instance__(m_TtmForm);
                    return m_TtmForm;
                }

                [DebuggerHidden]
                set
                {
                    if (ReferenceEquals(value, m_TtmForm))
                        return;
                    if (value is object)
                        throw new ArgumentException("Property can only be set to Nothing");
                    Dispose__Instance__(ref m_TtmForm);
                }
            }
        }
    }
}