using System;
using System.Diagnostics;
using System.Windows.Forms;

using isr.VI.ExceptionExtensions;

namespace isr.VI.Ttm
{

    /// <summary> Form for viewing the ttm. </summary>
    /// <remarks> David, 2020-10-11. </remarks>
    public class TtmForm : Core.Forma.ConsoleForm
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public TtmForm() : base()
        {
        }

        /// <summary> Gets or sets the meter view. </summary>
        /// <value> The meter view. </value>
        private Forms.MeterView MeterView { get; set; } = null;

        /// <summary> Gets or sets the visa session. </summary>
        /// <value> The visa session. </value>
        private VisaSessionBase VisaSession { get; set; } = null;

        /// <summary>
        /// Disposes of the resources (other than memory) used by the
        /// <see cref="T:System.Windows.Forms.Form" />.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="disposing"> true to release both managed and unmanaged resources; false to
        /// release only unmanaged resources. </param>
        protected override void Dispose( bool disposing )
        {
            if ( this.VisaSession is object )
            {
                this.VisaSession.Dispose();
                this.VisaSession = null;
            }

            if ( this.MeterView is object )
            {
                this.MeterView.Dispose();
                this.MeterView = null;
            }

            base.Dispose( disposing );
        }

        /// <summary>
        /// Raises the <see cref="E:System.Windows.Forms.Form.Closing" /> event. Releases all publishers.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> A <see cref="T:System.ComponentModel.CancelEventArgs" /> that contains the
        /// event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnClosing( System.ComponentModel.CancelEventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if ( this.VisaSession is object )
                {
                    if ( this.VisaSession.CandidateResourceNameValidated )
                    {
                        Properties.Settings.Default.K2600ResourceName = this.VisaSession.ValidatedResourceName;
                    }

                    if ( !string.IsNullOrWhiteSpace( this.VisaSession.OpenResourceTitle ) )
                    {
                        Properties.Settings.Default.K2600ResourceTitle = this.VisaSession.OpenResourceTitle;
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                base.OnClosing( e );
            }
        }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnLoad( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                activity = $"{this.Name} adding Meter View";
                // the talker control publishes the device messages which thus get published to the form message box.
                this.MeterView = new Forms.MeterView();
                this.AddTalkerControl( Properties.Settings.Default.K2600ResourceTitle, this.MeterView, false, false );
                // any form messages will be logged.
                activity = $"{this.Name}; adding log listener";
                this.AddListener( My.MyProject.Application.Logger );
                this.MeterView.ResourceName = Properties.Settings.Default.K2600ResourceName;
                this.VisaSession = this.MeterView.Device;
                this.VisaSession.CandidateResourceName = Properties.Settings.Default.K2600ResourceName;
                this.VisaSession.CandidateResourceTitle = Properties.Settings.Default.K2600ResourceTitle;
                if ( !string.IsNullOrWhiteSpace( this.VisaSession.CandidateResourceName ) )
                {
                    activity = $"{this.Name}; starting {this.VisaSession.CandidateResourceName} selection task";
                    _ = this.VisaSession.AsyncValidateResourceName( this.VisaSession.CandidateResourceName );
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                base.OnLoad( e );
            }
        }

        /// <summary> Gets or sets the await the resource name validation task enabled. </summary>
        /// <value> The await the resource name validation task enabled. </value>
        protected bool AwaitResourceNameValidationTaskEnabled { get; set; }

        /// <summary>
        /// Called upon receiving the <see cref="E:System.Windows.Forms.Form.Shown" /> event.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="e"> A <see cref="T:System.EventArgs" /> that contains the event data. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected override void OnShown( EventArgs e )
        {
            string activity = string.Empty;
            try
            {
                this.Cursor = Cursors.WaitCursor;
                this.MeterView.Cursor = Cursors.WaitCursor;
                activity = $"{this.Name} showing dialog";
                base.OnShown( e );
                if ( this.VisaSession is object && this.VisaSession.IsValidatingResourceName() )
                {
                    if ( this.AwaitResourceNameValidationTaskEnabled )
                    {
                        activity = $"{this.Name}; awaiting {this.VisaSession.CandidateResourceName} validation";
                        this.VisaSession.AwaitResourceNameValidation( Properties.Settings.Default.ResourceNameSelectionTimeout );
                    }
                    else
                    {
                        activity = $"{this.Name}; validating {this.VisaSession.CandidateResourceName}";
                    }
                }
            }
            catch ( Exception ex )
            {
                _ = this.PublishException( activity, ex );
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if ( this.MeterView is object )
                    this.MeterView.Cursor = Cursors.Default;
            }
        }


        #region " TALKER "

        /// <summary> Identifies talkers. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        public override void IdentifyTalkers()
        {
            base.IdentifyTalkers();
            My.MyApplication.Appliance.Identify( this.Talker );
        }

        /// <summary>
        /// Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
        /// </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="eventType"> Type of the event. </param>
        /// <param name="activity">  The activity. </param>
        /// <returns> A String. </returns>
        protected override string Publish( TraceEventType eventType, string activity )
        {
            return this.Publish( new Core.TraceMessage( eventType, My.MyApplication.TraceEventId, activity ) );
        }

        /// <summary> Publish exception. </summary>
        /// <remarks> David, 2020-10-11. </remarks>
        /// <param name="activity"> The activity. </param>
        /// <param name="ex">       The ex. </param>
        /// <returns> A String. </returns>
        protected override string PublishException( string activity, Exception ex )
        {
            return this.Publish( TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString()}" );
        }

        #endregion

    }
}
