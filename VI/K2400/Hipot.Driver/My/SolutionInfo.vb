﻿Imports System.Reflection

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyCompany("Integrated Scientific Resources")>
<Assembly: AssemblyCopyright("(c) 2010 Integrated Scientific Resources, Inc. All rights reserved.")>
<Assembly: AssemblyTrademark("Licensed under ISR Fair End User Use License Version 1.0 http://www.integratedscientificresources.com/licenses/FairEndUserUseLicense.pdf")>
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)>
<Assembly: AssemblyVersion("1.3.*")>
