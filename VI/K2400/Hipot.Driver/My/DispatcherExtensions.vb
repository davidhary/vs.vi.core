﻿Imports System.Runtime.CompilerServices
Imports System.Windows.Threading

Namespace DispatcherExtensions

    ''' <summary> Dispatcher extension methods. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Lets Windows process all the messages currently in the message queue. </summary>
        ''' <remarks>
        ''' https://stackoverflow.com/questions/4502037/where-is-the-application-doevents-in-wpf. Well, I
        ''' just hit a case where I start work on a method that runs on the Dispatcher thread, and it
        ''' needs to block without blocking the UI Thread. Turns out that MSDN topic on the Dispatcher
        ''' Push Frame method explains how to implement a DoEvents() based on the Dispatcher itself:
        ''' https://goo.gl/WGFZEU
        ''' <code>
        ''' Imports System.Windows.Threading
        ''' Imports isr.Core.DispatcherExtensions
        ''' isr.Core.ApplianceBase.DoEvents()
        ''' </code>
        ''' <list type="bullet">Benchmarks<item>
        ''' 1000 iterations of Application Do Events:   7.8,  8.0, 11,  8.4 us </item><item>
        ''' 1000 iterations of Dispatcher Do Events:   57.6, 92.5, 93, 61.6 us </item></list>
        ''' </remarks>
        ''' <param name="dispatcher"> The dispatcher. </param>
        <Extension>
        Public Sub DoEvents(ByVal dispatcher As Dispatcher)
            Dim frame As New DispatcherFrame()
            dispatcher.BeginInvoke(DispatcherPriority.Background, New DispatcherOperationCallback(Function(ByVal f As Object)
                                                                                                      TryCast(f, DispatcherFrame).Continue = False
                                                                                                      Return Nothing
                                                                                                  End Function), frame)
            Dispatcher.PushFrame(frame)
        End Sub

        ''' <summary>
        ''' Executes the events operation before and after a  <see cref="Threading.Tasks.Task"/> delay.
        ''' </summary>
        ''' <remarks>
        ''' <code>
        ''' Imports System.Windows.Threading
        ''' 
        ''' Windows.Forms.Application.DoEvents()(Timespan.FromMilliseconds(10))
        ''' </code>
        ''' </remarks>
        ''' <param name="dispatcher"> The dispatcher. </param>
        ''' <param name="delay">      The delay. </param>
        <Extension>
        Public Sub DoEventsTaskDelay(ByVal dispatcher As Dispatcher, ByVal delay As TimeSpan)
            dispatcher.DoEvents
            Threading.Tasks.Task.Delay(delay).Wait()
            dispatcher.DoEvents
        End Sub

    End Module

End Namespace

Namespace My

    ''' <summary> my library. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Lets Windows process all the messages currently in the message queue. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Public Shared Sub DoEvents()
            DispatcherExtensions.DoEvents(Dispatcher.CurrentDispatcher)
        End Sub

        ''' <summary> Executes the events task delay operation. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="delayTime"> The delay time. </param>
        Public Shared Sub DoEventsTaskDelay(ByVal delayTime As TimeSpan)
            DispatcherExtensions.DoEventsTaskDelay(Dispatcher.CurrentDispatcher, delayTime)
        End Sub

    End Class

End Namespace

