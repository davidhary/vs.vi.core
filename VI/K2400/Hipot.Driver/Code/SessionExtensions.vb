Imports System.Runtime.CompilerServices

Public Module Methods

    ''' <summary> Queries. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <param name="dataToWrite">         The data to write. </param>
    ''' <returns> A String. </returns>
    <Extension, CLSCompliant(False)>
    Public Function Query(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession, ByVal dataToWrite As String) As String
        If messageBasedSession Is Nothing Then Throw New ArgumentNullException(NameOf(messageBasedSession))
        messageBasedSession.Write(dataToWrite)
        Return messageBasedSession.Read
    End Function

    ''' <summary>
    ''' Synchronously writes ASCII-encoded string data to the device or interface. Terminates the
    ''' data with the read termination character.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <param name="dataToWrite">         The data to write. </param>
    <Extension, CLSCompliant(False)>
    Public Sub Write(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession, ByVal dataToWrite As String)
        If messageBasedSession IsNot Nothing Then messageBasedSession.RawIO.Write(dataToWrite)
    End Sub

    ''' <summary> Reads the given message based session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <returns> A String. </returns>
    <Extension, CLSCompliant(False)>
    Public Function Read(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession) As String
        Dim result As String = String.Empty
        If messageBasedSession IsNot Nothing Then result = messageBasedSession.RawIO.ReadString()
        Return result
    End Function

    ''' <summary> Reads an attribute. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <param name="attribute">           The attribute. </param>
    ''' <param name="defaultValue">        The default value. </param>
    ''' <returns> The attribute. </returns>
    <Extension, CLSCompliant(False)>
    Public Function ReadAttribute(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession, ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal defaultValue As Integer) As Integer
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(messageBasedSession, Ivi.Visa.INativeVisaSession)
        Return If(s IsNot Nothing, s.GetAttributeInt32(attribute), defaultValue)
    End Function

    ''' <summary> Writes an attribute. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <param name="attribute">           The attribute. </param>
    ''' <param name="value">               The value. </param>
    <Extension, CLSCompliant(False)>
    Public Sub WriteAttribute(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession, ByVal attribute As Ivi.Visa.NativeVisaAttribute, ByVal value As Integer)
        Dim s As Ivi.Visa.INativeVisaSession = TryCast(messageBasedSession, Ivi.Visa.INativeVisaSession)
        If s IsNot Nothing Then
            s.SetAttributeInt32(attribute, value)
        End If
    End Sub

    ''' <summary> Reads a timeout. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <returns> The timeout. </returns>
    <Extension, CLSCompliant(False)>
    Public Function ReadTimeout(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession) As Integer
        Return messageBasedSession.ReadAttribute(Ivi.Visa.NativeVisaAttribute.TimeoutValue, 2000I)
    End Function

    ''' <summary> Writes a timeout. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="messageBasedSession"> The message based session. </param>
    ''' <param name="timeout">             The timeout. </param>
    <Extension, CLSCompliant(False)>
    Public Sub WriteTimeout(ByVal messageBasedSession As Ivi.Visa.IMessageBasedSession, ByVal timeout As Integer)
        messageBasedSession.WriteAttribute(Ivi.Visa.NativeVisaAttribute.TimeoutValue, timeout)
    End Sub

End Module
