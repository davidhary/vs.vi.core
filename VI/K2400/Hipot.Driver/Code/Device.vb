''' <summary> Implements the high potential insulation resistance meter device. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-12-25, 1.0.4011. Created </para>
''' </remarks>
Public Class Device
    Implements IDevice

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Primary Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()

        MyBase.New()
        Me.ResourceName = "GPIB0::24::INSTR"
        Me._MessageAvailableBits = 16
        Me._ErrorAvailableBits = 4
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Me.Dispose(True)

        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    ''' <summary> True if is disposed, false if not. </summary>
    Private _IsDisposed As Boolean

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> The is disposed. </value>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._IsDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._IsDisposed = value
        End Set
    End Property

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter.  If True, the
    ''' method has been called directly or indirectly by a user's code--managed and unmanaged
    ''' resources can be disposed. If disposing equals False, the method has been called by the
    ''' runtime from inside the finalizer and you should not reference other objects--only unmanaged
    ''' resources can be disposed.
    ''' </remarks>
    ''' <param name="disposing"> True if this method releases both managed and unmanaged resources;
    '''                          False if this method releases only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me._Identity = String.Empty
                If Me.ExceptionAvailableEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.ExceptionAvailableEvent.GetInvocationList
                        RemoveHandler ExceptionAvailable, CType(d, Global.System.EventHandler(Of Threading.ThreadExceptionEventArgs))
                    Next
                End If
                If Me.MessageAvailableEvent IsNot Nothing Then
                    For Each d As [Delegate] In Me.MessageAvailableEvent.GetInvocationList
                        RemoveHandler MessageAvailable, CType(d, Global.System.EventHandler(Of MessageEventArgs))
                    Next
                End If
                If Me._Session IsNot Nothing Then Me._Session.Dispose() : Me._Session = Nothing
            End If
        Finally
            Me.IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " I DEVICE: CONNECT "

    ''' <summary> Gets the last error status. </summary>
    ''' <exception cref="VisaException">         Thrown when a Visa error condition occurs. </exception>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The last error status. </value>
    Public Property LastErrorStatus As Integer

    ''' <summary>
    ''' Initializes a new instance of the <see cref="NationalInstruments.Visa.Session" /> class.
    ''' </summary>
    ''' <remarks>
    ''' This method does not lock the resource. Rev 4.1 and 5.0 of VISA did not support this call and
    ''' could not verify the resource.
    ''' </remarks>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <param name="timeout">      The open timeout. </param>
    Protected Sub CreateSession(ByVal resourceName As String, ByVal timeout As TimeSpan)
        Try
            Me.LastErrorStatus = 0
            Dim result As Ivi.Visa.ResourceOpenStatus = Ivi.Visa.ResourceOpenStatus.Success

            Dim activity As String = "creating visa session"

            Using resMgr As New NationalInstruments.Visa.ResourceManager
                result = Ivi.Visa.ResourceOpenStatus.Unknown
                Me._Session = CType(resMgr.Open(resourceName, Ivi.Visa.AccessModes.None, CInt(timeout.TotalMilliseconds), result),
                                           Ivi.Visa.IMessageBasedSession)
            End Using
            If result <> Ivi.Visa.ResourceOpenStatus.Success Then Throw New VisaException($"Failed {activity}; '{result}';. {resourceName}")
            If Me.Session Is Nothing Then Throw New VisaException($"Failed {activity};. {resourceName}")
        Catch ex As Ivi.Visa.NativeVisaException
            If Me.Session IsNot Nothing Then Me.Session.Dispose() : Me._Session = Nothing
            Me.LastErrorStatus = ex.ErrorCode
            Throw New VisaException(Me.LastErrorStatus, ex)
        Catch
            If Me.Session IsNot Nothing Then Me.Session.Dispose() : Me._Session = Nothing
            Throw
        End Try
    End Sub

    ''' <summary> Connects to the instrument and clears its known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resourceName"> . </param>
    ''' <returns> <c>True</c> if the device connected. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Connect(ByVal resourceName As String) As Boolean Implements IDevice.Connect
        If resourceName Is Nothing Then
            Throw New ArgumentNullException(NameOf(resourceName))
        End If

        Me.ResourceName = resourceName
        Try

            Me.OnMessageAvailable(TraceEventType.Verbose, "OPENING VISA SESSION", "Opening VISA session to {0}", resourceName)

            Me.CreateSession(resourceName, TimeSpan.FromSeconds(3))

            Me.OnMessageAvailable(TraceEventType.Information, "VISA SESSION OPEN", "VISA session opened to {0}", resourceName)

            Me.OnMessageAvailable(TraceEventType.Verbose, "READING STATUS BYTE", "Reading {0} status byte", resourceName)

            Me.ReadStatusByte()
            Me.DiscardUnreadData()
            Me.ResetAndClear()

            Me._IsConnected = True

            Return True

        Catch ex As Exception

            Me.OnExceptionAvailable(ex, "Exception occurred opening a VISA Session to {0}. Disconnecting.", resourceName)

            Try
                Me.Disconnect()
            Finally
            End Try

            Return False

        Finally

        End Try

    End Function

    ''' <summary> Disconnects form the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> <c>True</c> if the device  disconnected. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Disconnect() As Boolean Implements IDevice.Disconnect

        Try

            Me._Identity = String.Empty
            Me.OnMessageAvailable(TraceEventType.Verbose, "DISCONNECTING", "disconnecting {0}", Me.ResourceName)

            If Me._Session IsNot Nothing Then
                Me._Session.Dispose()
                Me._Session = Nothing
            End If

            Me._IsConnected = False
            Return Not Me.IsConnected

        Catch ex As Exception

            Me.OnExceptionAvailable(ex, "Exception occurred closing the VISA Session to {0}.", Me.ResourceName)
            Return False

        End Try

    End Function

    ''' <summary> Clears the read buffers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function FlushRead() As Boolean Implements IDevice.FlushRead
        Me.DiscardUnreadData()
    End Function

    ''' <summary> The identity. </summary>
    Private _Identity As String

    ''' <summary>
    ''' Returns the instrument identification code. KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11
    ''' Oct 10 1997 09:51:36/A02 /D/B/E.
    ''' </summary>
    ''' <value> The identity. </value>
    Public ReadOnly Property Identity() As String Implements IDevice.Identity
        Get
            If String.IsNullOrWhiteSpace(Me._Identity) Then
                Me._Identity = Me._Session.Query(Device.IdentifyQueryCommand)
            End If
            Return Me._Identity
        End Get
    End Property

    ''' <summary> True if is connected, false if not. </summary>
    Private _IsConnected As Boolean

    ''' <summary> Gets the connection status. </summary>
    ''' <value> The is connected. </value>
    Public ReadOnly Property IsConnected() As Boolean Implements IDevice.IsConnected
        Get
            Return Me._IsConnected
        End Get
    End Property

    ''' <summary> Reseting and clearing the instrument to its known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ResetAndClear() As Boolean Implements IDevice.ResetAndClear

        Dim timeout As Integer
        Try

            timeout = Me.Session.ReadTimeout

            Me.OnMessageAvailable(TraceEventType.Verbose, "RESETTING AND CLEARING TO KNOWN STATE", "Resetting and clearing {0} to its known state", Me.ResourceName)
            Me.ClearActiveState()
            Me.ResetKnownState()
            Me.ClearExecutionState()

            Return Me.LastErrorStatus = 0

        Catch ex As Exception

            Me.OnExceptionAvailable(ex, "Exception occurred resetting the {0} to its known state.", Me.ResourceName)

        Finally

            ' restore timeout.
            Me.Session.WriteTimeout(timeout)

        End Try

    End Function

    ''' <summary> Gets the instrument resource name. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName() As String Implements IDevice.ResourceName

#End Region

#Region " I DEVICE: CONFIGURE "

    ''' <summary> Selects the voltage range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function SelectVoltageRange(ByVal value As Single) As String
        Select Case value
            Case Is <= 20
                Return "20"
            Case Is <= 40
                Return "40"
            Case Is <= 100
                Return "100"
            Case Is <= 200
                Return "200"
            Case Is <= 300
                Return "300"
            Case Is <= 400
                Return "400"
            Case Is <= 500
                Return "500"
            Case Else
                Return "MAX"
        End Select
    End Function

    ''' <summary> Selects the resistance range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> System.String. </returns>
    Public Shared Function SelectResistanceRange(ByVal value As Single) As String
        Select Case value
            Case Is <= 21
                Return "21"
            Case Is <= 210
                Return "210"
            Case Is <= 2100
                Return "2100"
            Case Is <= 21000
                Return "21000"
            Case Is <= 210000
                Return "210000"
            Case Is <= 2100000
                Return "2100000"
            Case Is <= 21000000
                Return "21000000"
            Case Is <= 210000000
                Return "210000000"
            Case Else
                Return "MAX"
        End Select
    End Function

    ''' <summary> Configures the measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> True if success or False if failed. </returns>
    Public Function Configure() As Boolean Implements IDevice.Configure

        If Me.Aperture < 0.001 OrElse Me.CurrentLimit < 0.000001 OrElse Me.VoltageLevel < 0.001 Then
            ' ignores configuration if settings were not applied to instrument cached values.
            Return True
        End If

        ' make sure the 2400 output is off
        Me.Session.Write(":OUTP OFF")

        '  Me.Session.Write("*RST")
        '  Me.Session.Write(":SENS:FUNC ""RES""")
        '  Me.Session.Write(":SENS:RES:MODE MAN")
        '  Me.Session.Write(":SYST:RSEN ON")
        '  Me.Session.Write(":SOUR:FUNC VOLT")
        '  Me.Session.Write(":SOUR:VOLT:RANG 5")
        '  Me.Session.Write(":SOUR:VOLT 5")
        '  Me.Session.Write(":SENS:CURR:RANG 10e-6")
        '  Me.Session.Write(":OUTPut ON")
        '  Dim value As String = Me.Session.Query(":READ?")
        '  Me.Session.Write(":OUTPut OFF")

        ' reset the instrument to its know state. 
        Me.ResetKnownState()

        ' Use rear terminals
        Me.Session.Write(":ROUT:TERM REAR")

        Me.Session.Write(":SENS:FUNC ""RES""")

        ' no need to set in manual mode.
        'Me.Session.Write(":SENS:RES:RANG " & selectResistanceRange(Me.ResistanceRange))
        'Me.ResistanceRange = CSng(Me.Session.Query(":SENS:RES:RANG?"))
        Me.Session.Write(":SENS:RES:MODE MAN")

        ' the source must be set first.
        Me.Session.Write(":SOUR:CLE:AUTO ON")
        Me.Session.Write(":SOUR:DEL " & CStr(Me.DwellTime))
        Me.DwellTime = CSng(Me.Session.Query(":SOUR:DEL?"))
        Me.Session.Write(":SOUR:FUNC VOLT")
        ' default: Me.Session.Write(":SOUR:VOLT:MODE FIX")
        Me.Session.Write(":SOUR:VOLT:RANG " & SelectVoltageRange(Me.VoltageLevel))
        Me.Session.Write(":SOUR:VOLT:LEV " & CStr(Me.VoltageLevel))
        Me.VoltageLevel = CSng(Me.Session.Query(":SOUR:VOLT:LEV?"))

        'Me.Session.Write(":SENS:FUNC <function>")
        'Me.Session.Write(":SENS:CURR:PROT " & CStr(Me.CurrentLimit))
        'Me.Session.Write(":SENS:CURR:RANG " & CStr(Me.CurrentLimit))
        'Me.Session.Write(":SENS:RES:RANG <n>")

        Me.Session.Write(":SENS:CURR:RANG " & CStr(Me.CurrentRange))
        'Debug.Write(Me.Session.Query(":SENS:CURR:RANG?"))
        Me.Session.Write(":SENS:CURR:PROT " & CStr(Me.CurrentLimit))
        Me.CurrentLimit = CSng(Me.Session.Query(":SENS:CURR:PROT?"))
        Me.Session.Write(":SENS:VOLT:NPLC " & CStr(Me.Aperture))
        Me.Session.Write(":SENS:CURR:NPLC " & CStr(Me.Aperture))
        Me.Aperture = CSng(Me.Session.Query(":SENS:CURR:NPLC?"))

        ' Enable four wire connection
        Me.Session.Write(":SYST:RSEN ON")

        ' force immediate update of auto zero
        Me.Session.Write(":SYST:AZER ONCE")

        ' default is cable: Me.Session.Write(":SYST:GUAR CABL") ' or OHMS

        ' =================
        ' ARM MODEL:

        ' arm to immediate mode.
        ' default is IMM:  Me.Session.Write(":ARM:SOUR IMM")

        ' Set ARM counter to 1
        ' default is 1: Me.Session.Write(":ARM:COUN 1")

        ' Define Trigger layer to interface with the
        ' Trigger Master board:

        ' clear any pending triggers.
        Me.Session.Write(":TRIG:CLE")

        ' Set TRIGGER input line to Trigger Link line number
        ' default. to be set when starting: Me.Session.Write(":TRIG:ILIN 1")

        ' Set TRIGGER output line to Trigger Link line number
        ' default: Me.Session.Write(":TRIG:OLIN 2")

        ' Set Input trigger to Acceptor
        Me.Session.Write(":TRIG:DIR ACC")
        Me.Session.Write(":TRIG:DIR SOUR")

        ' start with immediate trigger allowing non-triggered measurements.
        Me.Session.Write(":TRIG:SOUR IMM")

        ' no outputs triggers
        ' default is none: Me.Session.Write(":TRIG:OUTP NONE")

        ' A single trigger.
        ' default is 1: Me.Session.Write(":TRIG:COUN 1")

        ' default: Me.Session.Write(":TRIG:INP NONE")

        ' ======
        ' FORMAT

        ' Set time stamp to start from when first items stored to memory
        ' not using timestamps. Me.Session.Write(":TRAC:TST:FORM ABS")

        ' Define data transfer format
        ' default is ASC: Me.Session.Write(":FORM:DATA ASC")

        Me.Session.Write(":FORM:ELEM VOLT,CURR,RES,STATUS")

        ' ======
        ' CALC2

        ' default: Me.Session.Write(":CALC2:CLIM:BCON IMM")
        Me.Session.Write(":CALC2:CLIM:CLE")
        Me.Session.Write(":CALC2:CLIM:MODE GRAD")
        ' default Me.Session.Write(":CALC2:CLIM:CLE:AUTO ON")

        ' Make limit comparisons on resistance reading
        Me.Session.Write(":CALC2:FEED RES")

        ' Limit 1: send BAD code if measurement is in
        ' compliance
        ' default: Me.Session.Write(":CALC2:LIM1:COMP:FAIL IN")

        ' Set fail bin
        Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM1:COMP:SOUR2 {0}", Me.FailBitPattern))
        Me.Session.Write(":CALC2:LIM1:STAT 1")

        ' Limit 2: send BAD code if out-of-range

        ' Set upper limit
        ' Me.Session.Write(":CALC2:LIM2:UPP " & CStr(0.995 * Me.ResistanceRange))
        ' Me.Session.Write(":CALC2:LIM2:UPP:SOUR2 " & CStr(7 - 2 ^ (3 - 1)))  ' Bit = 3

        ' Set lower limit
        ' TO_DO:  Check why I cannot set a limit of 0.1 on
        ' the current test?  0 or -10 will work!
        Me.Session.Write(":CALC2:LIM2:UPP 1E+19")
        Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM2:LOW {0}", Me.ResistanceLowLimit))
        Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM2:UPP:SOUR2 {0}", Me.FailBitPattern))
        Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM2:LOW:SOUR2 {0}", Me.FailBitPattern))
        Me.Session.Write(":CALC2:LIM2:STAT 1")

        ' Send GOOD code if measurement is in range
        Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:CLIM:PASS:SOUR2 {0}", Me.PassBitPattern))
        Me.Session.Write(":CALC2:CLIM:BCON IMM")

        ' enable contact check
        If Me.ContactCheckSupported Then
            If Me.ContactCheckEnabled Then
                ' Enable contact check failure detection
                Me.Session.Write(":CALC2:LIM4:STAT ON")
                If Me.ContactCheckBitPattern > 0 Then
                    ' set the contact check failure bit pattern
                    Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture,
                                                   ":CALC2:LIM4:SOUR2 {0}", Me.ContactCheckBitPattern))
                    ' Enable contact check event detection
                    Me.Session.Write(":TRIG:SEQ2:SOUR CCH")
                    ' Set 2 second contact check timeout
                    Me.Session.Write(":TRIG:SEQ2:TOUT 1")
                End If
                ' set contact check resistance
                Me.Session.Write(":SYST:CCH:RES 2")
                ' enable contact check
                Me.Session.Write(":SYST:CCH ON")
            Else
                Me.Session.Write(":CALC2:LIM4:STAT OFF")
                Me.Session.Write(":SYST:CCH OFF")
            End If
        End If

        ' ======
        ' EOT
        ' Set byte size to 3 enabling EOT mode.
        Me.Session.Write(":SOUR2:BSIZ 3")

        ' Set the output logic to high
        Me.Session.Write(":SOUR2:TTL #b000")

        ' Set Digital I/O Mode to EOT
        Me.Session.Write(":SOUR2:TTL4:MODE EOT")

        ' Set EOT polarity to HI
        Me.Session.Write(":SOUR2:TTL4:BST HI")

        ' Set the output level to set automatically to the
        ' :TTL level after the pass or fail output bit
        ' pattern or a limit test is sent to the handler.
        ' this can be turned off and the clear command issued on each start.
        Me.Session.Write(":SOUR2:CLE")
        Me.Session.Write(":SOUR2:CLE:AUTO ON")

        ' Set the duration of the EOT strobe
        Me.Session.Write(":SOUR2:CLE:AUTO:DEL " & CStr(Me.EotStrobeDuration))

        Return True

    End Function

    ''' <summary> Gets the aperture in power line cycles. </summary>
    ''' <value> The aperture. </value>
    Public Property Aperture() As Single Implements IDevice.Aperture

    ''' <summary> The contact check option. </summary>
    Private Const _ContactCheckOption As String = "CONTACT-CHECK"

    ''' <summary> Query if this object is contact check supported. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if contact check supported, false if not. </returns>
    Private Function IsContactCheckSupported() As Boolean?
        If Me.IsConnected Then
            Dim options As String = Me.Session.Query("*OPT?")
            Return If(String.IsNullOrWhiteSpace(options), New Boolean?, options.Contains(Device._ContactCheckOption))
        Else
            Return New Boolean?
        End If
    End Function

    ''' <summary> The contact check supported. </summary>
    Private _ContactCheckSupported As Boolean?

    ''' <summary> Gets the sentinel indicating that contact check is supported. </summary>
    ''' <value> The contact check supported. </value>
    Public ReadOnly Property ContactCheckSupported As Boolean Implements IDevice.ContactCheckSupported
        Get
            If Not Me._ContactCheckSupported.HasValue Then
                Me._ContactCheckSupported = Me.IsContactCheckSupported
            End If
            Return Me._ContactCheckSupported.Value
        End Get
    End Property

    ''' <summary> Gets the contact check enabled. </summary>
    ''' <value> The contact check enabled. </value>
    Public Property ContactCheckEnabled As Boolean Implements IDevice.ContactCheckEnabled

    ''' <summary> Gets the contact check bit pattern. </summary>
    ''' <value> The contact check bit pattern. </value>
    Public Property ContactCheckBitPattern As Integer Implements IDevice.ContactCheckBitPattern

    ''' <summary>
    ''' Gets the current range. Based on the ratio of the voltage to the minimum resistance. If the
    ''' resistance is zero, returns the current limit.
    ''' </summary>
    ''' <value> The current range. </value>
    Private ReadOnly Property CurrentRange() As Single
        Get
            Return If(Me.ResistanceLowLimit > 0, Me.VoltageLevel / Me.ResistanceLowLimit, Me.CurrentLimit)
        End Get
    End Property

    ''' <summary> Gets the current limit in Amperes. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The current limit. </value>
    Public Property CurrentLimit() As Single Implements IDevice.CurrentLimit

    ''' <summary> Gets the dwell time in seconds. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The dwell time. </value>
    Public Property DwellTime() As Single Implements IDevice.DwellTime

    ''' <summary> Gets the resistance limit in ohms. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The resistance low limit. </value>
    Public Property ResistanceLowLimit() As Single Implements IDevice.ResistanceLowLimit

    ''' <summary> Gets the resistance range in ohms. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The resistance range. </value>
    Public Property ResistanceRange() As Single Implements IDevice.ResistanceRange

    ''' <summary> Gets the voltage level in volts. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The voltage level. </value>
    Public Property VoltageLevel() As Single Implements IDevice.VoltageLevel


#End Region

#Region " I DEVICE: BINNING "

    ''' <summary> Gets the duration of the end-of=test strobe. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The duration of the end-of-test strobe. </value>
    Public Property EotStrobeDuration() As Single Implements IDevice.EotStrobeDuration

    ''' <summary> Gets the pass bit pattern. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The pass bit pattern. </value>
    Public Property PassBitPattern As Integer Implements IDevice.PassBitPattern

    ''' <summary> Gets the fail bit pattern. </summary>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <value> The fail bit pattern. </value>
    Public Property FailBitPattern As Integer Implements IDevice.FailBitPattern

#End Region

#Region " I DEVICE: EVENTS "

    ''' <summary>
    ''' Sends an Exception message to the client.
    ''' </summary>
    Public Event ExceptionAvailable As EventHandler(Of System.Threading.ThreadExceptionEventArgs) Implements IDevice.ExceptionAvailable

    ''' <summary> Raises the exception available event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="DriverException"> Thrown when a Driver error condition occurs. </exception>
    ''' <param name="ex"> Specifies the exception. </param>
    Public Sub OnExceptionAvailable(ByVal ex As Exception) Implements IDevice.OnExceptionAvailable
        If Me.ExceptionAvailableEvent Is Nothing Then
            Throw New DriverException("An exception occurred but no handlers were set to receive the exception message. Check inner exception for details.", ex)
        Else
            Me.ExceptionAvailableEvent(Me, New Threading.ThreadExceptionEventArgs(ex))
        End If
    End Sub

    ''' <summary> Raises the exception available event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="ex">     Specifies the exception. </param>
    ''' <param name="format"> Specifies the event message format. </param>
    ''' <param name="args">   Specifies the event message format arguments. </param>
    Public Sub OnExceptionAvailable(ByVal ex As Exception, ByVal format As String, ByVal ParamArray args() As Object) Implements IDevice.OnExceptionAvailable
        If ex IsNot Nothing Then
            ex.Data.Add("Details", String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            Me.OnExceptionAvailable(ex)
        End If
    End Sub

    ''' <summary>Sends a message to the client.
    ''' </summary>
    Public Event MessageAvailable As EventHandler(Of MessageEventArgs) Implements IDevice.MessageAvailable

    ''' <summary> Raises the Message Available event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> The message event arguments. </param>
    Public Sub OnMessageAvailable(ByVal e As MessageEventArgs) Implements IDevice.OnMessageAvailable
        If Me.MessageAvailableEvent IsNot Nothing Then
            Me.MessageAvailableEvent(Me, e)
        End If
    End Sub

    ''' <summary> Raises the Message Available event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="traceLevel"> Specifies the event arguments message
    '''                           <see cref="Diagnostics.TraceEventType">trace level</see>. </param>
    ''' <param name="synopsis">   Specifies the message Synopsis. </param>
    ''' <param name="format">     Specifies the message details. </param>
    ''' <param name="args">       Arguments to use in the format statement. </param>
    Public Overridable Sub OnMessageAvailable(ByVal traceLevel As Diagnostics.TraceEventType,
                                              ByVal synopsis As String, ByVal format As String,
                                              ByVal ParamArray args() As Object) Implements IDevice.OnMessageAvailable
        Me.OnMessageAvailable(New MessageEventArgs(traceLevel, synopsis, format, args))
    End Sub

#End Region

#Region " I DEVICE MEASURE "

    ''' <summary> Makes the HIPOT measurements manually. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function Measure() As Boolean Implements IDevice.Measure

        Me._Outcome = MeasurementOutcomes.None

        Dim autoMeasureState As Integer = 0
        If Integer.TryParse(Me.Session.Query(":SOUR:CLE:AUTO?"), autoMeasureState) Then
        End If
        Dim openCheckFailed As Integer = 0

        If autoMeasureState = 0 Then Me.Session.Write(":OUTP ON")
        Dim timeout As Integer = Me.Session.ReadTimeout
        Me.Session.WriteTimeout(10000)
        Me.Session.Write(":INIT")
        If Me.ContactCheckEnabled Then
            If Integer.TryParse(Me.Session.Query(":CALC2:LIM4:FAIL?"), openCheckFailed) Then
            End If
        End If
        If openCheckFailed = 0 Then
            Me._Reading = Me.Session.Query(":FETCH?")
        Else
        End If
        If autoMeasureState = 0 Then Me.Session.Write(":OUTP OFF")
        Me.Session.WriteTimeout(timeout)
        Return If(openCheckFailed = 0, Me.ParseReading(0), Me.ParseReading(CInt(2 ^ StatusBit.FailedContactCheck)))

    End Function

    ''' <summary> The outcome. </summary>
    Private _Outcome As MeasurementOutcomes

    ''' <summary> Gets the measurement outcome for the measurements. </summary>
    ''' <value> The outcome. </value>
    Public ReadOnly Property Outcome() As MeasurementOutcomes Implements IDevice.Outcome
        Get
            Return Me._Outcome
        End Get
    End Property

    ''' <summary> Parse status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ParseStatus()

        'Bit 0 (OFLO) — Set to 1 if measurement was made while in over-range.
        'Bit 1 (Filter) — Set to 1 if measurement was made with the filter enabled. 
        'Bit 3 (Compliance) — Set to 1 if in real compliance. 
        'Bit 4 (OVP) — Set to 1 if the over voltage protection limit was reached. '
        'Bit 16 (Range Compliance) — Set to 1 if in range compliance. 
        'Bit 18 — Contact check failure (see Appendix F). 
        If (Me.Status And CLng(2 ^ StatusBit.HitCompliance + 2 ^ StatusBit.HitRangeCompliance)) <> 0 Then Me._Outcome = Me._Outcome Or MeasurementOutcomes.HitCompliance
        If (Me.Status And CLng(2 ^ StatusBit.FailedContactCheck)) <> 0 Then Me._Outcome = Me._Outcome Or MeasurementOutcomes.FailedContactCheck


        'TO_DO: Parse status to get the compliance issue. 
        ' Parse the limit to get the level compliance. 
        If Me.Outcome = MeasurementOutcomes.None Then
            Me._Outcome = If(Me._Resistance < Me.ResistanceLowLimit, MeasurementOutcomes.PartFailed, MeasurementOutcomes.PartPassed)
        End If
    End Sub

    ''' <summary> Parses the reading. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Private Function ParseReading(ByVal status As Integer) As Boolean
        Me._VoltageReading = String.Empty
        Me._Voltage = 0
        Me._Current = 0
        Me._CurrentReading = String.Empty
        Me._Resistance = 0
        If False Then
        ElseIf status > 0 Then
            Me._Status = status
            Me.ParseStatus()
        ElseIf String.IsNullOrWhiteSpace(Me._Reading) Then
            Me._Status = -1
            Return False
        Else
            Dim values As String() = Me._Reading.Split(","c)
            Me._VoltageReading = values(0)
            Dim numberStyle As Globalization.NumberStyles = Globalization.NumberStyles.Number Or
                                                            Globalization.NumberStyles.AllowDecimalPoint Or
                                                            Globalization.NumberStyles.AllowExponent Or
                                                            Globalization.NumberStyles.AllowLeadingSign
            If Not Single.TryParse(Me._VoltageReading, numberStyle, Globalization.CultureInfo.CurrentCulture, Me._Voltage) Then
                Me._Voltage = -1.0F
                Me._Outcome = Me._Outcome Or MeasurementOutcomes.UnexpectedReadingFormat
            End If
            Me._CurrentReading = values(1)
            If Not Single.TryParse(Me._CurrentReading, numberStyle, Globalization.CultureInfo.CurrentCulture, Me._Current) Then
                Me._Current = -1.0F
                Me._Outcome = Me._Outcome Or MeasurementOutcomes.UnexpectedReadingFormat
            End If
            Me._ResistanceReading = values(2)
            If Not Single.TryParse(Me._ResistanceReading, numberStyle, Globalization.CultureInfo.CurrentCulture, Me._Resistance) Then
                Me._Resistance = -1.0F
                Me._Outcome = Me._Outcome Or MeasurementOutcomes.UnexpectedReadingFormat
            End If

            Dim temp As Single
            If Not Single.TryParse(values(3), numberStyle, Globalization.CultureInfo.CurrentCulture, temp) Then
                temp = -1.0F
                Me._Outcome = Me._Outcome Or MeasurementOutcomes.UnexpectedReadingFormat
            End If
            Me._Status = CLng(temp)
            Me.ParseStatus()
        End If
        Return True
    End Function

    ''' <summary> Read the measurements from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function ReadMeasurements() As Boolean Implements IDevice.ReadMeasurements
        Me._Reading = Me.Session.Query(":FETCH?")
        Return Me.ParseReading(0)
    End Function

    ''' <summary> The current. </summary>
    Private _Current As Single

    ''' <summary> Gets the measured Current. </summary>
    ''' <value> The current. </value>
    Public ReadOnly Property Current() As Single Implements IDevice.Current
        Get
            Return Me._Current
        End Get
    End Property

    ''' <summary> The current reading. </summary>
    Private _CurrentReading As String

    ''' <summary> Gets the measured Current reading. </summary>
    ''' <value> The current reading. </value>
    Public ReadOnly Property CurrentReading() As String Implements IDevice.CurrentReading
        Get
            Return Me._CurrentReading
        End Get
    End Property

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary> Gets the reading as fetched from the instrument. </summary>
    ''' <value> The reading. </value>
    Public ReadOnly Property Reading() As String Implements IDevice.Reading
        Get
            Return Me._Reading
        End Get
    End Property

    ''' <summary> The resistance. </summary>
    Private _Resistance As Single

    ''' <summary> Gets the measurement Resistance. </summary>
    ''' <value> The resistance. </value>
    Public ReadOnly Property Resistance() As Single Implements IDevice.Resistance
        Get
            Return Me._Resistance
        End Get
    End Property

    ''' <summary> The resistance reading. </summary>
    Private _ResistanceReading As String

    ''' <summary> Gets the measurement Resistance reading. </summary>
    ''' <value> The resistance reading. </value>
    Public ReadOnly Property ResistanceReading() As String Implements IDevice.ResistanceReading
        Get
            Return Me._ResistanceReading
        End Get
    End Property

    ''' <summary> The status. </summary>
    Private _Status As Long

    ''' <summary> Gets the parsed measurement status. </summary>
    ''' <value> The status. </value>
    Public ReadOnly Property Status() As Long Implements IDevice.Status
        Get
            Return Me._Status
        End Get
    End Property

    ''' <summary> The voltage. </summary>
    Private _Voltage As Single

    ''' <summary> Gets the measured voltage. </summary>
    ''' <value> The voltage. </value>
    Public ReadOnly Property Voltage() As Single Implements IDevice.Voltage
        Get
            Return Me._Voltage
        End Get
    End Property

    ''' <summary> The voltage reading. </summary>
    Private _VoltageReading As String

    ''' <summary> Gets the measured voltage reading. </summary>
    ''' <value> The voltage reading. </value>
    Public ReadOnly Property VoltageReading() As String Implements IDevice.VoltageReading
        Get
            Return Me._VoltageReading
        End Get
    End Property

#End Region

#Region " I DEVICE SETTINGS "

    ''' <summary> Saves the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub SaveSettings() Implements IDevice.SaveSettings
        My.MySettings.Default.Save()
    End Sub

    ''' <summary>
    ''' Reads the settings and set the device values -- this does not send the settings to the
    ''' instrument.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ReadSettings() Implements IDevice.ReadSettings
        My.Application.Log.WriteEntry("Storing configuration settings in the device.", TraceEventType.Verbose)
        Me.Aperture = My.MySettings.Default.Aperture
        Me.CurrentLimit = My.MySettings.Default.CurrentLimit
        Me.DwellTime = My.MySettings.Default.DwellTime
        Me.ResistanceLowLimit = My.MySettings.Default.ResistanceLowLimit
        Me.ResistanceRange = My.MySettings.Default.ResistanceRange
        Me.VoltageLevel = My.MySettings.Default.VoltageLevel
        Me.EotStrobeDuration = My.MySettings.Default.EotStrobeDuration
        Me.FailBitPattern = My.MySettings.Default.FailBitPattern
        Me.PassBitPattern = My.MySettings.Default.PassBitPattern
        If Me._ContactCheckSupported.GetValueOrDefault(False) Then
            Me.ContactCheckEnabled = My.MySettings.Default.ContactCheckEnabled
            Me.ContactCheckBitPattern = My.MySettings.Default.ContactCheckBitPattern
        End If
    End Sub

    ''' <summary>
    ''' Updates the current settings from the device -- this does not read the settings from the
    ''' instrument.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub UpdateSettings() Implements IDevice.UpdateSettings
        My.Application.Log.WriteEntry("Updating configuration settings from the device.", TraceEventType.Verbose)
        My.MySettings.Default.Aperture = CDec(Me.Aperture)
        My.MySettings.Default.CurrentLimit = CDec(Me.CurrentLimit)
        My.MySettings.Default.DwellTime = CDec(Me.DwellTime)
        My.MySettings.Default.ResistanceLowLimit = CDec(Me.ResistanceLowLimit)
        My.MySettings.Default.ResistanceRange = CDec(Me.ResistanceRange)
        My.MySettings.Default.VoltageLevel = CDec(Me.VoltageLevel)
        My.MySettings.Default.EotStrobeDuration = CDec(Me.EotStrobeDuration)
        My.MySettings.Default.FailBitPattern = Me.FailBitPattern
        My.MySettings.Default.PassBitPattern = Me.PassBitPattern
        If Me._ContactCheckSupported.GetValueOrDefault(False) Then
            My.MySettings.Default.ContactCheckEnabled = Me.ContactCheckEnabled
            My.MySettings.Default.ContactCheckBitPattern = Me.ContactCheckBitPattern
        End If
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Aborts waiting for a triggered measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function AbortMeasurement() As Boolean Implements IDevice.AbortMeasurement
        Me.Session.Write(":ABORT")
        Me.Session.Clear()
        Return Me.LastErrorStatus = 0
    End Function

    ''' <summary> Asserts a trigger for making a triggered measurement. </summary>
    ''' <remarks>
    ''' This works with the 2600 instrument but not with the 2400 instrument. This method was devised
    ''' for testing. It is replaced with Manual Trigger from the front panel of the 2400.
    ''' </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function TriggerMeasurement() As Boolean Implements IDevice.TriggerMeasurement
        Me.Session.AssertTrigger()
        Return Me.LastErrorStatus = 0
    End Function

    ''' <summary> Returns true if measurement completed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if measurement completed, false if not. </returns>
    Public Function IsMeasurementCompleted() As Boolean Implements IDevice.IsMeasurementCompleted
        Return (Me.ReadStatusByte And ServiceRequests.StandardEvent) <> 0
    End Function

    ''' <summary>
    ''' Prime the instrument to wait for a trigger. This clears the digital outputs and loops until
    ''' trigger or external TRG command.
    ''' </summary>
    ''' <remarks>
    ''' Using manual trigger was required because the 2400 instrument fails to respond to the bus
    ''' trigger.
    ''' </remarks>
    ''' <param name="useManualTrigger"> True if using manual trigger. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function PrepareForTrigger(ByVal useManualTrigger As Boolean) As Boolean Implements IDevice.PrepareForTrigger

        Me.Session.Write(":TRIG:SOUR IMM")
        If useManualTrigger Then
            ' start when the bus trigger line is asserted.
            ' Me.Session.Write(":ARM:SOUR BUS")
            Me.Session.Write(":ARM:SOUR MAN")
            Me.Session.Write(":ARM:COUNT 1")
        Else
            ' start when the start of test is pulsed high
            Me.Session.Write(":ARM:SOUR PST")
        End If

        If Me.EnableServiceRequest(StandardEvents.All And Not StandardEvents.RequestControl,
                                   ServiceRequests.StandardEvent Or ServiceRequests.OperationEvent) Then
            Me.Session.Write(":INIT; *OPC")
            Return Me.LastErrorStatus = 0
        Else
            Return False
        End If

    End Function

#End Region

#Region " SYNCHRONIZER "

    ''' <summary>
    ''' Gets or sets the <see cref="System.ComponentModel.ISynchronizeInvoke">object</see>
    ''' to use for marshaling events.
    ''' </summary>
    ''' <value> The synchronizer. </value>
    Public Property Synchronizer() As System.ComponentModel.ISynchronizeInvoke Implements IDevice.Synchronizer

    ''' <summary>
    ''' Sets the <see cref="System.ComponentModel.ISynchronizeInvoke">synchronization object</see>
    ''' to use for marshaling events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub SynchronizerSetter(ByVal value As System.ComponentModel.ISynchronizeInvoke) Implements IDevice.SynchronizerSetter
        Me._Synchronizer = value
    End Sub

#End Region

End Class

