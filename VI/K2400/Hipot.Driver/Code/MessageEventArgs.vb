''' <summary> Defines an event arguments class for messages. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-05-01, 1.1.3408. Created </para>
''' </remarks>
Public Class MessageEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="broadcast"> The broadcast. </param>
    ''' <param name="trace">     The trace. </param>
    ''' <param name="synopsis">  The synopsis. </param>
    ''' <param name="details">   The details. </param>
    Public Sub New(ByVal broadcast As TraceEventType, ByVal trace As TraceEventType, ByVal synopsis As String, ByVal details As String)
        MyBase.New()
        Me._Timestamp = DateTimeOffset.Now
        Me._BroadcastLevel = broadcast
        Me._TraceLevel = trace
        Me._Synopsis = synopsis
        Me._Details = details
        Me._Format = "{0:HH:mm:ss.fff}, {1}, {2}, {3}"
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="synopsis"> The synopsis. </param>
    ''' <param name="format">   The format. </param>
    ''' <param name="args">     The arguments. </param>
    Public Sub New(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(TraceEventType.Information, TraceEventType.Information, synopsis, format, args)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="trace">    The trace. </param>
    ''' <param name="synopsis"> The synopsis. </param>
    ''' <param name="format">   The format. </param>
    ''' <param name="args">     The arguments. </param>
    Public Sub New(ByVal trace As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(trace, trace, synopsis, format, args)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="broadcast"> The broadcast. </param>
    ''' <param name="trace">     The trace. </param>
    ''' <param name="synopsis">  The synopsis. </param>
    ''' <param name="format">    The format. </param>
    ''' <param name="args">      The arguments. </param>
    Public Sub New(ByVal broadcast As TraceEventType, ByVal trace As TraceEventType, ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(broadcast, trace, synopsis, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="MessageEventArgs" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="synopsis"> The synopsis. </param>
    ''' <param name="details">  The details. </param>
    Public Sub New(ByVal synopsis As String, ByVal details As String)
        Me.New(TraceEventType.Information, TraceEventType.Information, synopsis, details)
    End Sub


#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the message details. </summary>
    ''' <value> The details. </value>
    Public ReadOnly Property Details() As String

    ''' <summary> Gets or sets the event synopsis. </summary>
    ''' <value> The synopsis. </value>
    Public ReadOnly Property Synopsis() As String

    ''' <summary> Gets or sets the event time stamp. </summary>
    ''' <value> The timestamp. </value>
    Public ReadOnly Property Timestamp() As DateTimeOffset

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public ReadOnly Property TraceLevel() As TraceEventType

    ''' <summary> Gets or sets the Broadcast level. </summary>
    ''' <value> The broadcast level. </value>
    Public ReadOnly Property BroadcastLevel() As TraceEventType

#End Region

#Region " METHODS "

    ''' <summary> Describes the format to use. </summary>
    Private ReadOnly _Format As String

    ''' <summary> Returns a message based on the default format. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, Me._Format,
                             Me.Timestamp, Me.TraceLevel, Me.Synopsis, Me.Details)
    End Function

#End Region

End Class
