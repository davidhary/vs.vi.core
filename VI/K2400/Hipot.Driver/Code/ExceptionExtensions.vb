﻿Imports System.Text

Namespace ExceptionExtensions

    ''' <summary> Includes extensions for <see cref="Exception">Exceptions</see>. </summary>
    ''' <remarks> (c) 2017 Marco Bertschi.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2017-03-30, 3.1.6298.x">
    ''' https://www.codeproject.com/script/Membership/View.aspx?mid=8888914
    ''' https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc
    ''' </para></remarks>
    Public Module Methods

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception) As String
            Return Methods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Dim builder As New StringBuilder()
            Dim counter As Integer = 1
            Do While value IsNot Nothing AndAlso counter <= level
                builder.AppendLine($"{counter}-> Level: {counter}")
                If Not String.IsNullOrEmpty(value.Message) Then builder.AppendLine($"{counter}-> Message: {value.Message}")
                If Not String.IsNullOrEmpty(value.Source) Then builder.AppendLine($"{counter}-> Source: {value.Source}")
                If value.TargetSite IsNot Nothing Then builder.AppendLine($"{counter}-> Target Site: {value.TargetSite}")
                If Not String.IsNullOrEmpty(value.StackTrace) Then builder.AppendLine($"{counter}-> Stack Trace: {value.StackTrace}")
                If value.Data IsNot Nothing Then
                    For Each keyValuePair As System.Collections.DictionaryEntry In value.Data
                        builder.AppendLine($"{counter}-> {keyValuePair.Key}: {keyValuePair.Value}")
                    Next
                End If
                value = value.InnerException
                counter += 1
            Loop
            Return builder.ToString()
        End Function

    End Module
End Namespace