Imports isr.Hipot.Driver.ExceptionExtensions

Partial Public Class Device

#Region " SCPI SYNTAX "

    ''' <summary> Gets the Clear Status (CLS) command. </summary>
    Public Const ClearExecutionStateCommand As String = "*CLS"

    ''' <summary> The default error queue clear command. </summary>
    Public Const ClearErrorQueueCommand As String = ":STAT:QUE:CLEAR"

    ''' <summary> The default error queue query command. </summary>
    Public Const ErrorQueueQueryCommand As String = ":STAT:QUE?"

    ''' <summary> Gets the Identify query (*IDN?) command. </summary>
    Public Const IdentifyQueryCommand As String = "*IDN?"

    ''' <summary> Gets the reset to know state (*RST) command. </summary>
    Public Const ResetKnownStateCommand As String = "*RST"

    ''' <summary> Gets the Service Request Enable (*SRE) enable command. </summary>
    Public Const ServiceRequestEnableCommand As String = "*SRE {0:D}"

    ''' <summary> Gets the Standard Event Enable (*ESE) command. </summary>
    Public Const StandardEventEnableCommand As String = "*ESE {0:D}"

    ''' <summary> Gets the Standard Event Enable (*ESR?) command. </summary>
    Public Const StandardEventStatusQueryCommand As String = "*ESR?"

#End Region

#Region " STATUS REGISTER "

    ''' <summary>
    ''' Sets the device to issue an SRQ upon any of the SCPI events. Uses *ESE to select (mask) the
    ''' events that will issue SRQ and  *SRE to select (mask) the event registers to be included in
    ''' the bits that will issue an SRQ.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="standardEventEnableBits">  Specifies standard events will issue an SRQ. </param>
    ''' <param name="serviceRequestEnableBits"> Specifies which status registers will issue an SRQ. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Overridable Function EnableServiceRequest(ByVal standardEventEnableBits As Integer,
                                                     ByVal serviceRequestEnableBits As Integer) As Boolean

        Dim commandBuilder As New System.Text.StringBuilder
        commandBuilder.Append(ClearExecutionStateCommand)
        commandBuilder.Append("; ")
        commandBuilder.Append(String.Format(Globalization.CultureInfo.InvariantCulture, StandardEventEnableCommand, standardEventEnableBits))
        commandBuilder.Append("; ")
        commandBuilder.Append(String.Format(Globalization.CultureInfo.InvariantCulture, ServiceRequestEnableCommand, serviceRequestEnableBits))

        ' Check if necessary; clear the service request bit
        Me.ReadStatusByte()

        Me.Session.FormattedIO.WriteLine(commandBuilder.ToString)

        Return Me.IsLastVisaOperationSuccess

    End Function

    ''' <summary> Gets the last read status byte. </summary>
    ''' <value> The cached status byte. </value>
    Public Property CachedStatusByte() As Integer

    ''' <summary> Returns the service request register status byte. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The status byte. </returns>
    Public Function ReadStatusByte() As Integer
        If Me._Session IsNot Nothing Then
            Me._CachedStatusByte = CType(Me._Session.ReadStatusByte(), Integer)
            Return If(Me._CachedStatusByte >= 0, Me._CachedStatusByte, 256 + Me._CachedStatusByte)
        End If
        Return Me._CachedStatusByte
    End Function

#End Region

#Region " STANDARD EVENTS "

    ''' <summary>
    ''' Returns a True is the service request register status byte reports error available.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="standardErrorBits"> Specifies the standard device register error bits. </param>
    ''' <returns> true if standard error, false if not. </returns>
    Public Overridable Function HasStandardError(ByVal standardErrorBits As Integer) As Boolean
        Return (Me.ReadStandardEventStatus() And standardErrorBits) <> 0
    End Function

    ''' <summary> Reads the standard event status from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The standard event status. </returns>
    Public Overridable Function ReadStandardEventStatus() As Integer
        Me._Session.FormattedIO.WriteLine(StandardEventStatusQueryCommand)
        Me.StandardEventStatus = CInt(Me._Session.FormattedIO.ReadInt64)
        Return Me.StandardEventStatus
    End Function

    ''' <summary> Gets the standard event status. </summary>
    ''' <value> The standard event status. </value>
    Public Property StandardEventStatus() As Integer


#End Region

#Region " IEEE 488.2: VISA AND DEVICE ERROR MANAGEMENT "

    ''' <summary>
    ''' Gets a report of the error stored in the cached error queue event status register (ESR)
    ''' information.
    ''' </summary>
    ''' <value> The device errors. </value>
    Public Overridable ReadOnly Property DeviceErrors() As String
        Get
            Dim message As New System.Text.StringBuilder
            If Me._ErrorQueue.Length > 0 Then
                If message.Length > 0 Then
                    message.AppendLine()
                End If
                message.AppendLine("The device reported the following error(s) from the error queue:")
                message.Append(Me._ErrorQueue.ToString)
            End If
            Return message.ToString
        End Get
    End Property

    ''' <summary> Queue of errors. </summary>
    Private _ErrorQueue As System.Text.StringBuilder

    ''' <summary> Gets the error queue cache. </summary>
    ''' <value> The error queue cache. </value>
    Public ReadOnly Property ErrorQueueCache() As String
        Get
            Return If(Me._ErrorQueue Is Nothing OrElse Me._ErrorQueue.Length = 0, String.Empty, Me._ErrorQueue.ToString)
        End Get
    End Property

    ''' <summary> true to had error. </summary>
    Private _HadError As Boolean

    ''' <summary>
    ''' Returns True if the last operation reported an error by way of a service request. The error
    ''' is reported if the cached error indicator is true or a new error is reported by the service
    ''' request register.
    ''' </summary>
    ''' <value> The had error. </value>
    Public Property HadError() As Boolean
        Get
            Me._HadError = Me._HadError OrElse Me.HasError()
            Return Me._HadError
        End Get
        Set(ByVal value As Boolean)
            Me._HadError = value
        End Set
    End Property

    ''' <summary> The error available bits. </summary>
    Private ReadOnly _ErrorAvailableBits As Integer

    ''' <summary>
    ''' Returns a True is the service request register status byte reports error available.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if error, false if not. </returns>
    Public Function HasError() As Boolean
        Return Me.HasError(Me._ErrorAvailableBits)
    End Function

    ''' <summary>
    ''' Returns a True is the service request register status byte reports error available.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="errorBits"> The error bits. </param>
    ''' <returns> true if error, false if not. </returns>
    Public Function HasError(ByVal errorBits As Integer) As Boolean
        Return Me._Session IsNot Nothing AndAlso (Me._Session.ReadStatusByte() And errorBits) <> 0
    End Function

    ''' <summary> Check the status byte for error bits and latches the last error sentinel. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if an error is available, false if not. </returns>
    Public Overridable Function IsErrorAvailable() As Boolean
        Me.HadError = False
        Return Me.HadError
    End Function

    ''' <summary>
    ''' Returns true if the device had an error include standard query or execution errors. Clears
    ''' the error queue and status by reading the error queue.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <returns> True if error occurred. </returns>
    Public Overridable Function HasDeviceError(ByVal flushReadFirst As Boolean) As Boolean

        If Not Me.IsErrorAvailable AndAlso (Me.CachedStatusByte And ServiceRequests.StandardEvent) = 0 Then
            Return True
        End If

        If flushReadFirst Then
            Me.DiscardUnreadData()
        End If

        '  In order to keep compatibility, we clear the error here by reading the error status.
        Me.ReadErrorQueue()

        ' check for query and other errors reported by the standard event register
        Dim standardErrorBits As StandardEvents = StandardEvents.CommandError Or
                                                  StandardEvents.DeviceDependentError Or
                                                  StandardEvents.ExecutionError Or
                                                  StandardEvents.QueryError

        Return Me.HadError OrElse Me.HasStandardError(standardErrorBits)

    End Function

    ''' <summary>
    ''' Gets or sets the last read error queue. Setting the error allows emulating or clearing errors
    ''' when the error queue command is not supported or not using devices.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The error queue. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function ReadErrorQueue() As String
        Try

            ' clear the queue string
            Me._ErrorQueue = New System.Text.StringBuilder

            ' loop while we have an error
            Do While (Me.ReadStatusByte And Me._ErrorAvailableBits) <> 0

                If Me._ErrorQueue.Length > 0 Then
                    Me._ErrorQueue.Append(Environment.NewLine)
                End If

                ' save the error status
                Me.HadError = True

                ' send the command to get the queue
                Me._Session.Write(ErrorQueueQueryCommand)

                ' Add the error string
                Me._ErrorQueue.AppendFormat("Device error {0}", Me.Session.RawIO.ReadString())

            Loop

            ' return the queue
            Return Me._ErrorQueue.ToString

        Catch ex As Exception

            ' if we have an error, return a note of it
            Return "Exception occurred getting the error queue. " & ex.ToFullBlownString

        End Try
    End Function

    ''' <summary>
    ''' Reports on VISA or device errors. Can be used with queries because the report does not
    ''' require querying the instrument.
    ''' </summary>
    ''' <remarks>
    ''' This method is called after a VISA operation, such as a query, that could cause a query
    ''' terminated failure if accessing the instrument.
    ''' </remarks>
    ''' <param name="synopsis"> The report synopsis. </param>
    ''' <param name="format">   The report details format. </param>
    ''' <param name="args">     The report details arguments. </param>
    ''' <returns> True if no error occurred. </returns>
    Public Function ReportVisaOperationOkay(ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If Me.IsLastVisaOperationFailed Then
            Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "{0}. Instrument {1} encountered VISA errors: {2}{3}{4}",
                                                 String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                 Me.ResourceName,
                                                 Device.BuildVisaStatusDetails(Me.LastErrorStatus), Environment.NewLine,
                                                 StackTraceParser.UserCallStack(4, 10))
            Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' Reports on device errors. Can only be used after receiving a full reply from the instrument.
    ''' Use <see cref="ReportVisaOperationOkay">report visa operation</see> to avoid accessing the
    ''' information from the instrument.
    ''' </summary>
    ''' <remarks>
    ''' This method is called after a VISA operation, such as a query, that could cause a query
    ''' terminated failure if accessing the instrument.
    ''' </remarks>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="synopsis">       The report synopsis. </param>
    ''' <param name="format">         The report details format. </param>
    ''' <param name="args">           The report details arguments. </param>
    ''' <returns> True if no error occurred. </returns>
    Public Function ReportDeviceOperationOkay(ByVal flushReadFirst As Boolean,
                                                  ByVal synopsis As String, ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If Me.HasDeviceError(flushReadFirst) Then
            Me.OnMessageAvailable(TraceEventType.Verbose, synopsis, "{0}. Instrument {1} encountered errors: {2}{3}{4}",
                                                 String.Format(Globalization.CultureInfo.CurrentCulture, format, args),
                                                 Me.ResourceName,
                                                 Me.DeviceErrors, Environment.NewLine,
                                                 StackTraceParser.UserCallStack(4, 10))
            Return False
        End If
        Return True
    End Function

    ''' <summary>
    ''' Reports on VISA or device errors. Can only be used after receiving a full reply from the
    ''' instrument. Use <see cref="ReportVisaOperationOkay">report visa operation</see> to avoid
    ''' accessing the information from the instrument.
    ''' </summary>
    ''' <remarks>
    ''' This method is called after a VISA operation. It checked for error and sends
    ''' <see cref="MessageAvailable">message available</see> if an error occurred.
    ''' </remarks>
    ''' <param name="flushReadFirst"> Flushes the read buffer before processing the error. </param>
    ''' <param name="synopsis">       The report synopsis. </param>
    ''' <param name="format">         The report details format. </param>
    ''' <param name="args">           The report details arguments. </param>
    ''' <returns> True if no error occurred. </returns>
    Public Function ReportVisaDeviceOperationOkay(ByVal flushReadFirst As Boolean, ByVal synopsis As String,
                                                  ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.ReportVisaOperationOkay(synopsis, format, args) AndAlso Me.ReportDeviceOperationOkay(flushReadFirst, synopsis, format, args)
    End Function

    ''' <summary>
    ''' Throws a <see cref="VisaException">VISA exception</see>
    ''' Can be used with queries.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="VisaException"> Thrown when a Visa error condition occurs. </exception>
    ''' <param name="format"> The report details format. </param>
    ''' <param name="args">   The report details arguments. </param>
    ''' <returns> True if success. </returns>
    Public Function ThrowVisaExceptionIfError(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If Me.IsLastVisaOperationFailed AndAlso Me.Session IsNot Nothing Then
            Dim builder As New System.Text.StringBuilder()
            builder.AppendFormat(format, args)
            builder.AppendFormat(". Visa Status: {0}", Me.LastErrorStatus)
            Throw New VisaException(builder.ToString)
        End If
        Return True
    End Function

    ''' <summary>
    ''' Throws a <see cref="isr.Hipot.Driver.DeviceException">device exceptions</see>. Flushes the read buffer if an
    ''' error occurred. Cannot be used before a query is completed.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Hipot.Driver.DeviceException"> Thrown when a Device error condition occurs. </exception>
    ''' <param name="format"> The report details format. </param>
    ''' <param name="args">   The report details arguments. </param>
    ''' <returns> True if success. </returns>
    Public Function ThrowDeviceExceptionIfError(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        If Me.HasDeviceError(True) Then
            Dim builder As New System.Text.StringBuilder()
            builder.AppendFormat(format, args)
            builder.AppendFormat(". The device '{0}' reported the following errors: {1}", Me.ResourceName, Me.DeviceErrors)
            Throw New DeviceException(builder.ToString)
        End If
        Return True
    End Function

    ''' <summary>
    ''' Throws an <see cref="isr.Hipot.Driver.DeviceException">instrument exceptions</see>. Flushes the read buffer if
    ''' an error occurred.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="format"> The report details format. </param>
    ''' <param name="args">   The report details arguments. </param>
    ''' <returns> True if success. </returns>
    Public Function RaiseVisaOrDeviceExceptionIfError(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
        Return Me.ThrowVisaExceptionIfError(format, args) AndAlso Me.ThrowDeviceExceptionIfError(format, args)
    End Function

#End Region

#Region " VISA "

    ''' <summary>
    ''' Builds a VISA warning or error message. This method does not require additional information
    ''' from the instrument.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The state code from the last operation. </param>
    ''' <returns> A String. </returns>
    Public Shared Function BuildVisaStatusDetails(ByVal value As Integer) As String

        If value = 0 Then
            Return "OK"
        Else
            Dim visaMessage As New System.Text.StringBuilder
            If value > 0 Then
                visaMessage.Append("VISA Warning")
            Else
                visaMessage.Append("VISA Error")
            End If
            Dim description As String = Ivi.Visa.NativeErrorCode.GetMacroNameFromStatusCode(value)
            If String.Equals(value.ToString, description, StringComparison.CurrentCultureIgnoreCase) Then
                visaMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, ": {0}.", value)
            Else
                visaMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, " {0}: {1}.", value, description)
            End If
            Return visaMessage.ToString
        End If

    End Function

    ''' <summary> Selectively clears the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function ClearActiveState() As Boolean
        If Me._Session IsNot Nothing Then
            Me._Session.Clear()
        End If
        Return Not Me.HasError()
    End Function

    ''' <summary>
    ''' Clears the error Queue and resets all event registers to zero by issuing the IEEE488.2 common
    ''' command.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function ClearExecutionState() As Boolean
        Return Me.Execute(ClearErrorQueueCommand)
    End Function

    ''' <summary> The message available bits. </summary>
    Private ReadOnly _MessageAvailableBits As Integer

    ''' <summary>
    ''' Reads and discards all data from the VISA session until the END indicator is read.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub DiscardUnreadData()
        If Me._Session IsNot Nothing Then
            Do While Me.IsMessageAvailable()
                Me.Session.RawIO.ReadString()
            Loop
        End If
    End Sub

    ''' <summary> Executes the specified command. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function Execute(ByVal value As String) As Boolean
        If Me._Session IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(value) Then
            Me._Session.FormattedIO.WriteLine(value)
        End If
        Return Me.IsLastVisaOperationSuccess
    End Function

    ''' <summary> Returns true if the last VISA operation ended with a warning. </summary>
    ''' <value> The is last visa operation success. </value>
    Public ReadOnly Property IsLastVisaOperationSuccess() As Boolean
        Get
            Return Me._Session Is Nothing OrElse Me.LastErrorStatus = 0
        End Get
    End Property

    ''' <summary> Returns true if the last VISA operation ended with a warning. </summary>
    ''' <value> The is last visa operation failed. </value>
    Public ReadOnly Property IsLastVisaOperationFailed() As Boolean
        Get
            Return Me._Session IsNot Nothing AndAlso Me.LastErrorStatus < 0
        End Get
    End Property

    ''' <summary>
    ''' Returns the device to its default known state by issuing the *RST IEEE488.2 common command.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true if it succeeds, false if it fails. </returns>
    Public Function ResetKnownState() As Boolean
        Return Me.Execute(Device.ResetKnownStateCommand)
    End Function

    ''' <summary>
    ''' Reads the device status data byte and returns True if the massage available bits were set.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> True if the device has data in the queue. </returns>
    Public Overridable Function IsMessageAvailable() As Boolean
        Return Me._Session IsNot Nothing AndAlso (Me._Session.ReadStatusByte() And Me._MessageAvailableBits) <> 0
    End Function

    ''' <summary> Gets or sets reference to the VISA session. </summary>
    ''' <value> The session. </value>
    Private ReadOnly Property Session() As Ivi.Visa.IMessageBasedSession

#End Region

End Class

#Region " IEEE 488.2 EVENT FLAGS "

''' <summary> Gets or sets the status byte bits of the service request register. </summary>
''' <remarks>
''' Extends the VISA event status flags. Because the VISA status returns a signed byte, we need
''' to use Int16. Enumerates the Status Byte Register Bits. Use STB? or status.request_event to
''' read this register. Use *SRE or status.request_enable to enable these services. This
''' attribute is used to read the status byte, which is returned as a numeric value. The binary
''' equivalent of the returned value indicates which register bits are set. (c) 2010 Integrated
''' Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
<System.Flags()> Public Enum ServiceRequests

    ''' <summary> Not specified. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> Bit B0, Measurement Summary Bit (MSB). Set summary bit indicates
    ''' that an enabled measurement event has occurred. </summary>
    <ComponentModel.Description("Measurement Event (MSB)")>
    MeasurementEvent = &H1

    ''' <summary> Bit B1, System Summary Bit (SSB). Set summary bit indicates
    ''' that an enabled system event has occurred. </summary>
    <ComponentModel.Description("System Event (SSB)")>
    SystemEvent = &H2

    ''' <summary> Bit B2, Error Available (EAV). Set summary bit indicates that
    ''' an error or status message is present in the Error Queue. </summary>
    <ComponentModel.Description("Error Available (EAV)")>
    ErrorAvailable = &H4

    ''' <summary> Bit B3, Questionable Summary Bit (QSB). Set summary bit indicates
    ''' that an enabled questionable event has occurred. </summary>
    <ComponentModel.Description("Questionable Event (QSB)")>
    QuestionableEvent = &H8

    ''' <summary> Bit B4 (16), Message Available (MAV). Set summary bit indicates that
    ''' a response message is present in the Output Queue. </summary>
    <ComponentModel.Description("Message Available (MAV)")>
    MessageAvailable = &H10

    ''' <summary> Bit B5, Event Summary Bit (ESB). Set summary bit indicates 
    ''' that an enabled standard event has occurred. </summary>
    <ComponentModel.Description("Standard Event (ESB)")>
    StandardEvent = &H20 ' (32) ESB

    ''' <summary> Bit B6 (64), Request Service (RQS)/Master Summary Status (MSS).
    ''' Set bit indicates that an enabled summary bit of the Status Byte Register
    ''' is set. Depending on how it is used, Bit B6 of the Status Byte Register
    ''' is either the Request for Service (RQS) bit or the Master Summary Status
    ''' (MSS) bit: When using the GPIB serial poll sequence of the unit to obtain
    ''' the status byte (serial poll byte), B6 is the RQS bit. When using
    ''' status.condition or the *STB? common command to read the status byte,
    ''' B6 is the MSS bit. </summary>
    <ComponentModel.Description("Request Service (RQS)/Master Summary Status (MSS)")>
    RequestingService = &H40

    ''' <summary> Bit B7 (128), Operation Summary (OSB). Set summary bit indicates that
    ''' an enabled operation event has occurred. </summary>
    <ComponentModel.Description("Operation Event (OSB)")>
    OperationEvent = &H80

    ''' <summary> Includes all bits. </summary>
    <ComponentModel.Description("All")>
    All = &HFF ' 255

    ''' <summary> Unknown value due to, for example, error trying to get value from the
    ''' instrument. </summary>
    <ComponentModel.Description("Unknown")>
    Unknown = &H100
End Enum

''' <summary> Enumerates the status byte flags of the standard event register. </summary>
''' <remarks>
''' Enumerates the Standard Event Status Register Bits. Read this information using ESR? or
''' status.standard.event. Use *ESE or status.standard.enable or event status enable to enable
''' this register. These values are used when reading or writing to the standard event registers.
''' Reading a status register returns a value. The binary equivalent of the returned value
''' indicates which register bits are set. The least significant bit of the binary number is bit
''' 0, and the most significant bit is bit 15. For example, assume value 9 is returned for the
''' enable register. The binary equivalent is 0000000000001001. This value indicates that bit 0
''' (OPC) and bit 3 (DDE)
''' are set.
''' </remarks>
<System.Flags()> Public Enum StandardEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> Bit B0, Operation Complete (OPC). Set bit indicates that all
    ''' pending selected device operations are completed and the unit is ready to
    ''' accept new commands. The bit is set in response to an *OPC command.
    ''' The ICL function opc() can be used in place of the *OPC command. </summary>
    <ComponentModel.Description("Operation Complete (OPC)")>
    OperationComplete = 1

    ''' <summary> Bit B1, Request Control (RQC). Set bit indicates that.... </summary>
    <ComponentModel.Description("Request Control (RQC)")>
    RequestControl = &H2

    ''' <summary> Bit B2, Query Error (QYE). Set bit indicates that you attempted
    ''' to read data from an empty Output Queue. </summary>
    <ComponentModel.Description("Query Error (QYE)")>
    QueryError = &H4

    ''' <summary> Bit B3, Device-Dependent Error (DDE). Set bit indicates that an
    ''' instrument operation did not execute properly due to some internal
    ''' condition. </summary>
    <ComponentModel.Description("Device Dependent Error (DDE)")>
    DeviceDependentError = &H8

    ''' <summary> Bit B4 (16), Execution Error (EXE). Set bit indicates that the unit
    ''' detected an error while trying to execute a command. 
    ''' This is used by Quatech to report No Contact. </summary>
    <ComponentModel.Description("Execution Error (EXE)")>
    ExecutionError = &H10

    ''' <summary> Bit B5 (32), Command Error (CME). Set bit indicates that a
    ''' command error has occurred. Command errors include:<p>
    ''' IEEE-488.2 syntax error — unit received a message that does not follow
    ''' the defined syntax of the IEEE-488.2 standard.  </p><p>
    ''' Semantic error — unit received a command that was misspelled or received
    ''' an optional IEEE-488.2 command that is not implemented.  </p><p>
    ''' The instrument received a Group Execute Trigger (GET) inside a program
    ''' message.  </p> </summary>
    <ComponentModel.Description("Command Error (CME)")>
    CommandError = &H20

    ''' <summary> Bit B6 (64), User Request (URQ). Set bit indicates that the LOCAL
    ''' key on the SourceMeter front panel was pressed. </summary>
    <ComponentModel.Description("User Request (URQ)")>
    UserRequest = &H40

    ''' <summary> Bit B7 (128), Power ON (PON). Set bit indicates that the instrument
    ''' has been turned off and turned back on since the last time this register
    ''' has been read. </summary>
    <ComponentModel.Description("Power Toggled (PON)")>
    PowerToggled = &H80

    ''' <summary> Unknown value due to, for example, error trying to get value from the
    ''' instrument. </summary>
    <ComponentModel.Description("Unknown")>
    Unknown = &H100

    ''' <summary> Includes all bits. </summary>
    <ComponentModel.Description("All")>
    All = &HFF ' 255
End Enum

#End Region

Public Module [Extensions]

    ''' <summary>
    ''' Gets the <see cref="ComponentModel.DescriptionAttribute"/> of an
    ''' <see cref="System.Enum"/> type value.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="System.Enum"/> type value. </param>
    ''' <returns>
    ''' A string containing the text of the
    ''' <see cref="ComponentModel.DescriptionAttribute"/>.
    ''' </returns>
    <Runtime.CompilerServices.Extension()>
    Public Function Description(ByVal value As System.Enum) As String

        If value Is Nothing Then
            Return String.Empty
        Else
            Dim candidate As String = value.ToString()
            Dim fieldInfo As Reflection.FieldInfo = value.GetType().GetField(candidate)
            Dim attributes As ComponentModel.DescriptionAttribute() = CType(fieldInfo.GetCustomAttributes(GetType(ComponentModel.DescriptionAttribute), False), ComponentModel.DescriptionAttribute())

            If attributes IsNot Nothing AndAlso attributes.Length > 0 Then
                candidate = attributes(0).Description
            End If
            Return candidate
        End If

    End Function

End Module

