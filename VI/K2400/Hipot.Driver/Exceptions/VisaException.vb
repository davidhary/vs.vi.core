''' <summary> Handles General VISA exceptions. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-12-25, 1.0.4011. Created </para>
''' </remarks>
<Serializable()>
Public Class VisaException
    Inherits BaseException

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(VisaException.UnknownErrorCode, "Unknown VISA Exception")
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="VisaException"/> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        Me.ErrorCode = VisaException.UnknownErrorCode
    End Sub

    ''' <summary> The unknown error code. </summary>
    Public Const UnknownErrorCode As Integer = -9999

    ''' <summary> Gets or sets the error code. </summary>
    ''' <value> The error code. </value>
    Public ReadOnly Property ErrorCode As Integer

    ''' <summary> Initializes a new instance of the <see cref="VisaException"/> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="errorCode"> The error code. </param>
    Public Sub New(ByVal errorCode As Integer)
        Me.New(errorCode, "VISA Exception")
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="errorCode"> The error code. </param>
    ''' <param name="message">   The message. </param>
    Public Sub New(ByVal errorCode As Integer, ByVal message As String)
        MyBase.New(message)
        Me.ErrorCode = errorCode
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="BaseException" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="errorCode">      The error code. </param>
    ''' <param name="innerException"> The inner exception. </param>
    Public Sub New(ByVal errorCode As Integer, ByVal innerException As System.Exception)
        MyBase.New("Visa Exception", innerException)
        Me.ErrorCode = errorCode
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with serialized
    ''' data.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="info">    The <see cref="T:System.Runtime.Serialization.SerializationInfo" />
    '''                        that holds the serialized object data about the exception being
    '''                        thrown. </param>
    ''' <param name="context"> The <see cref="T:System.Runtime.Serialization.StreamingContext" />
    '''                        that contains contextual information about the source or destination. 
    ''' </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo,
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData" /> method to serialize custom values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="info">    The <see cref="Runtime.Serialization.SerializationInfo">serialization
    '''                        information</see>. </param>
    ''' <param name="context"> The <see cref="Runtime.Serialization.StreamingContext">streaming
    '''                        context</see> for the exception. </param>
    <Security.Permissions.SecurityPermission(Security.Permissions.SecurityAction.Demand, SerializationFormatter:=True)>
    Public Overrides Sub GetObjectData(ByVal info As Runtime.Serialization.SerializationInfo, ByVal context As Runtime.Serialization.StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue(NameOf(ErrorCode), Me.ErrorCode, GetType(Integer))
        MyBase.GetObjectData(info, context)
    End Sub

#End Region

End Class

