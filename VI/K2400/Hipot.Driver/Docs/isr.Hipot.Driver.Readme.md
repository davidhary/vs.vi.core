## ISR HIPOT Driver<sub>&trade;</sub>: ISR High Potential Insulation Meter Driver.
Revision History

*1.3.6667 2018-04-03*  
2018 release.

*1.2.6319 2017-04-20*  
Adds exception extensions to display full blown
exception information.

*1.2.6276 2017-03-08*  
Uses .NET 4.

*1.2.5362 2014-09-06*  
Adds contact check. Supports VS2013.

*1.1.4819 2013-03-11*  
Converted to VS2010.

*1.0.4123 2011-04-16*  
Sets current range instead of resistance range.

*1.0.4070 2011-02-27*  
Adds End-Of-Test strobe duration.

*1.0.4063 2011-02-15*  
Adds Driver exception. Raises the driver exception if
exception message is not handled by the calling application.

*1.0.4059 2011-02-11*  
No longer requires dependent libraries.

*1.0.4057 2011-02-09*  
Uses binary pattern for setting instrument outputs.

*1.0.4031 2010-01-14*  
Adds binaries to the developer installer.

*1.0.4014 2010-12-28*  
Beta release.

\(c\) 2010 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed using the Microsoft [This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:

[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[
### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
