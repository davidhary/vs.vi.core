<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Console

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Console))
        Me._MainLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ConfigurationLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ConfigGroupBox = New System.Windows.Forms.GroupBox()
        Me._CurrentLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._CurrentLimitNumericLabel = New System.Windows.Forms.Label()
        Me._VoltageLevelNumericLabel = New System.Windows.Forms.Label()
        Me._VoltageLevelNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ResistanceRangeNumericLabel = New System.Windows.Forms.Label()
        Me._ResistanceLowLimitNumericLabel = New System.Windows.Forms.Label()
        Me._ResistanceRangeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ResistanceLowLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DwellTimeNumericLabel = New System.Windows.Forms.Label()
        Me._DwellTimeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ApertureNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ApertureNumericLabel = New System.Windows.Forms.Label()
        Me._BinningGroupBox = New System.Windows.Forms.GroupBox()
        Me._ContactCheckSupportLabel = New System.Windows.Forms.Label()
        Me._FailBitPatternNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PassBitPatternNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ContactCheckBitPatternNumeric = New System.Windows.Forms.NumericUpDown()
        Me._FailBitPatternNumericLabel = New System.Windows.Forms.Label()
        Me._PassBitPatternNumericLabel = New System.Windows.Forms.Label()
        Me._ContactCheckBitPatternNumericLabel = New System.Windows.Forms.Label()
        Me._ContactCheckToggle = New System.Windows.Forms.CheckBox()
        Me._EotStrobeDurationNumericLabel = New System.Windows.Forms.Label()
        Me._EotStrobeDurationNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ConnectGroupBox = New System.Windows.Forms.GroupBox()
        Me._ConnectToggle = New System.Windows.Forms.CheckBox()
        Me._ResourceNameTextBoxLabel = New System.Windows.Forms.Label()
        Me._IdentityTextBox = New System.Windows.Forms.TextBox()
        Me._ResourceNameTextBox = New System.Windows.Forms.TextBox()
        Me._OutcomeLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._CurrentTextBox = New System.Windows.Forms.TextBox()
        Me._CurrentTextBoxLabel = New System.Windows.Forms.Label()
        Me._ResistanceTextBox = New System.Windows.Forms.TextBox()
        Me._VoltageTextBox = New System.Windows.Forms.TextBox()
        Me._ResistanceTextBoxLabel = New System.Windows.Forms.Label()
        Me._VoltageTextBoxLabel = New System.Windows.Forms.Label()
        Me._ManualGroupBox = New System.Windows.Forms.GroupBox()
        Me._MeasureControlsLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._MeasureButton = New System.Windows.Forms.Button()
        Me._ApplyLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._ApplyConfigButton = New System.Windows.Forms.Button()
        Me._TriggerToolStrip = New System.Windows.Forms.ToolStrip()
        Me._AwaitTriggerToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._WaitHourglassLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TriggersDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._ManualTriggerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._SotTriggerMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AssertTriggerToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._TriggerActionToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._ErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        Me._MeterTimer = New System.Windows.Forms.Timer(Me.components)
        Me._StatusStrip = New System.Windows.Forms.StatusStrip()
        Me._StatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me._MainLayout.SuspendLayout()
        Me._ConfigurationLayout.SuspendLayout()
        Me._ConfigGroupBox.SuspendLayout()
        CType(Me._CurrentLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._VoltageLevelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ResistanceRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ResistanceLowLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DwellTimeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ApertureNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._BinningGroupBox.SuspendLayout()
        CType(Me._FailBitPatternNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._PassBitPatternNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ContactCheckBitPatternNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EotStrobeDurationNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ConnectGroupBox.SuspendLayout()
        Me._OutcomeLayout.SuspendLayout()
        Me._ManualGroupBox.SuspendLayout()
        Me._MeasureControlsLayout.SuspendLayout()
        Me._ApplyLayout.SuspendLayout()
        Me._TriggerToolStrip.SuspendLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_mainLayout
        '
        Me._MainLayout.ColumnCount = 3
        Me._MainLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MainLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._MainLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MainLayout.Controls.Add(Me._ConfigurationLayout, 1, 5)
        Me._MainLayout.Controls.Add(Me._ConnectGroupBox, 1, 3)
        Me._MainLayout.Controls.Add(Me._OutcomeLayout, 1, 1)
        Me._MainLayout.Controls.Add(Me._ManualGroupBox, 1, 8)
        Me._MainLayout.Controls.Add(Me._ApplyLayout, 1, 6)
        Me._MainLayout.Controls.Add(Me._TriggerToolStrip, 1, 10)
        Me._MainLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MainLayout.Location = New System.Drawing.Point(0, 0)
        Me._MainLayout.Name = "_mainLayout"
        Me._MainLayout.RowCount = 12
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me._MainLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._MainLayout.Size = New System.Drawing.Size(660, 683)
        Me._MainLayout.TabIndex = 0
        '
        '_configurationLayout
        '
        Me._ConfigurationLayout.ColumnCount = 4
        Me._ConfigurationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConfigurationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ConfigurationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ConfigurationLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ConfigurationLayout.Controls.Add(Me._ConfigGroupBox, 1, 0)
        Me._ConfigurationLayout.Controls.Add(Me._BinningGroupBox, 2, 0)
        Me._ConfigurationLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._ConfigurationLayout.Location = New System.Drawing.Point(15, 233)
        Me._ConfigurationLayout.Name = "_configurationLayout"
        Me._ConfigurationLayout.RowCount = 1
        Me._ConfigurationLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ConfigurationLayout.Size = New System.Drawing.Size(629, 197)
        Me._ConfigurationLayout.TabIndex = 1
        '
        '_ConfigGroupBox
        '
        Me._ConfigGroupBox.Controls.Add(Me._CurrentLimitNumeric)
        Me._ConfigGroupBox.Controls.Add(Me._CurrentLimitNumericLabel)
        Me._ConfigGroupBox.Controls.Add(Me._VoltageLevelNumericLabel)
        Me._ConfigGroupBox.Controls.Add(Me._VoltageLevelNumeric)
        Me._ConfigGroupBox.Controls.Add(Me._ResistanceRangeNumericLabel)
        Me._ConfigGroupBox.Controls.Add(Me._ResistanceLowLimitNumericLabel)
        Me._ConfigGroupBox.Controls.Add(Me._ResistanceRangeNumeric)
        Me._ConfigGroupBox.Controls.Add(Me._ResistanceLowLimitNumeric)
        Me._ConfigGroupBox.Controls.Add(Me._DwellTimeNumericLabel)
        Me._ConfigGroupBox.Controls.Add(Me._DwellTimeNumeric)
        Me._ConfigGroupBox.Controls.Add(Me._ApertureNumeric)
        Me._ConfigGroupBox.Controls.Add(Me._ApertureNumericLabel)
        Me._ConfigGroupBox.Location = New System.Drawing.Point(15, 3)
        Me._ConfigGroupBox.Name = "_ConfigGroupBox"
        Me._ConfigGroupBox.Size = New System.Drawing.Size(296, 191)
        Me._ConfigGroupBox.TabIndex = 1
        Me._ConfigGroupBox.TabStop = False
        Me._ConfigGroupBox.Text = "TEST SETTINGS"
        '
        '_CurrentLimitNumeric
        '
        Me._CurrentLimitNumeric.DecimalPlaces = 7
        Me._CurrentLimitNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 327680})
        Me._CurrentLimitNumeric.Location = New System.Drawing.Point(178, 106)
        Me._CurrentLimitNumeric.Maximum = New Decimal(New Integer() {1, 0, 0, 131072})
        Me._CurrentLimitNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 393216})
        Me._CurrentLimitNumeric.Name = "_CurrentLimitNumeric"
        Me._CurrentLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CurrentLimitNumeric.Size = New System.Drawing.Size(87, 20)
        Me._CurrentLimitNumeric.TabIndex = 7
        Me._CurrentLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._CurrentLimitNumeric, "Maximum allowed current in Amperes. If measured current hits this limit, the " &
        "measurement is said to hit compliance.")
        Me._CurrentLimitNumeric.Value = New Decimal(New Integer() {1, 0, 0, 196608})
        '
        '_CurrentLimitNumericLabel
        '
        Me._CurrentLimitNumericLabel.AutoSize = True
        Me._CurrentLimitNumericLabel.Location = New System.Drawing.Point(66, 110)
        Me._CurrentLimitNumericLabel.Name = "_CurrentLimitNumericLabel"
        Me._CurrentLimitNumericLabel.Size = New System.Drawing.Size(110, 13)
        Me._CurrentLimitNumericLabel.TabIndex = 2
        Me._CurrentLimitNumericLabel.Text = "CURRENT LIMIT [A]:"
        '
        '_VoltageLevelNumericLabel
        '
        Me._VoltageLevelNumericLabel.AutoSize = True
        Me._VoltageLevelNumericLabel.Location = New System.Drawing.Point(64, 83)
        Me._VoltageLevelNumericLabel.Name = "_VoltageLevelNumericLabel"
        Me._VoltageLevelNumericLabel.Size = New System.Drawing.Size(112, 13)
        Me._VoltageLevelNumericLabel.TabIndex = 2
        Me._VoltageLevelNumericLabel.Text = "VOLTAGE LEVEL [V]:"
        '
        '_VoltageLevelNumeric
        '
        Me._VoltageLevelNumeric.DecimalPlaces = 3
        Me._VoltageLevelNumeric.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._VoltageLevelNumeric.Location = New System.Drawing.Point(178, 79)
        Me._VoltageLevelNumeric.Maximum = New Decimal(New Integer() {1100, 0, 0, 0})
        Me._VoltageLevelNumeric.Name = "_VoltageLevelNumeric"
        Me._VoltageLevelNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltageLevelNumeric.Size = New System.Drawing.Size(87, 20)
        Me._VoltageLevelNumeric.TabIndex = 5
        Me._VoltageLevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._VoltageLevelNumeric, "Applied voltage in volts")
        Me._VoltageLevelNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_ResistanceRangeNumericLabel
        '
        Me._ResistanceRangeNumericLabel.AutoSize = True
        Me._ResistanceRangeNumericLabel.Location = New System.Drawing.Point(39, 164)
        Me._ResistanceRangeNumericLabel.Name = "_ResistanceRangeNumericLabel"
        Me._ResistanceRangeNumericLabel.Size = New System.Drawing.Size(137, 13)
        Me._ResistanceRangeNumericLabel.TabIndex = 4
        Me._ResistanceRangeNumericLabel.Text = "RESISTANCE RANGE [Ω]:"
        '
        '_resistanceLowLimitNumericLabel
        '
        Me._ResistanceLowLimitNumericLabel.AutoSize = True
        Me._ResistanceLowLimitNumericLabel.Location = New System.Drawing.Point(21, 137)
        Me._ResistanceLowLimitNumericLabel.Name = "_resistanceLowLimitNumericLabel"
        Me._ResistanceLowLimitNumericLabel.Size = New System.Drawing.Size(155, 13)
        Me._ResistanceLowLimitNumericLabel.TabIndex = 4
        Me._ResistanceLowLimitNumericLabel.Text = "RESISTANCE LOW LIMIT [Ω]:"
        '
        '_ResistanceRangeNumeric
        '
        Me._ResistanceRangeNumeric.Increment = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me._ResistanceRangeNumeric.Location = New System.Drawing.Point(178, 160)
        Me._ResistanceRangeNumeric.Maximum = New Decimal(New Integer() {1000000000, 0, 0, 0})
        Me._ResistanceRangeNumeric.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._ResistanceRangeNumeric.Name = "_ResistanceRangeNumeric"
        Me._ResistanceRangeNumeric.Size = New System.Drawing.Size(87, 20)
        Me._ResistanceRangeNumeric.TabIndex = 3
        Me._ResistanceRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ResistanceRangeNumeric.ThousandsSeparator = True
        Me._ToolTip.SetToolTip(Me._ResistanceRangeNumeric, "Range of insulation resistance. Also high limit for overflow indication.")
        Me._ResistanceRangeNumeric.Value = New Decimal(New Integer() {200000000, 0, 0, 0})
        '
        '_ResistanceLowLimitNumeric
        '
        Me._ResistanceLowLimitNumeric.Increment = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me._ResistanceLowLimitNumeric.Location = New System.Drawing.Point(178, 133)
        Me._ResistanceLowLimitNumeric.Maximum = New Decimal(New Integer() {100000000, 0, 0, 0})
        Me._ResistanceLowLimitNumeric.Minimum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._ResistanceLowLimitNumeric.Name = "_ResistanceLowLimitNumeric"
        Me._ResistanceLowLimitNumeric.Size = New System.Drawing.Size(87, 20)
        Me._ResistanceLowLimitNumeric.TabIndex = 3
        Me._ResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ResistanceLowLimitNumeric.ThousandsSeparator = True
        Me._ToolTip.SetToolTip(Me._ResistanceLowLimitNumeric, "Low limit of insulation resistance")
        Me._ResistanceLowLimitNumeric.Value = New Decimal(New Integer() {10000000, 0, 0, 0})
        '
        '_dwellTimeNumericLabel
        '
        Me._DwellTimeNumericLabel.AutoSize = True
        Me._DwellTimeNumericLabel.Location = New System.Drawing.Point(83, 56)
        Me._DwellTimeNumericLabel.Name = "_dwellTimeNumericLabel"
        Me._DwellTimeNumericLabel.Size = New System.Drawing.Size(93, 13)
        Me._DwellTimeNumericLabel.TabIndex = 2
        Me._DwellTimeNumericLabel.Text = "DWELL TIME [S]:"
        '
        '_dwellTimeNumeric
        '
        Me._DwellTimeNumeric.DecimalPlaces = 2
        Me._DwellTimeNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._DwellTimeNumeric.Location = New System.Drawing.Point(178, 52)
        Me._DwellTimeNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._DwellTimeNumeric.Name = "_dwellTimeNumeric"
        Me._DwellTimeNumeric.Size = New System.Drawing.Size(87, 20)
        Me._DwellTimeNumeric.TabIndex = 3
        Me._DwellTimeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._DwellTimeNumeric, "Dwell time. Time the source is on before measurement is made.")
        Me._DwellTimeNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_ApertureNumeric
        '
        Me._ApertureNumeric.DecimalPlaces = 2
        Me._ApertureNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._ApertureNumeric.Location = New System.Drawing.Point(178, 25)
        Me._ApertureNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ApertureNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 131072})
        Me._ApertureNumeric.Name = "_ApertureNumeric"
        Me._ApertureNumeric.Size = New System.Drawing.Size(87, 20)
        Me._ApertureNumeric.TabIndex = 1
        Me._ApertureNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._ApertureNumeric, "Aperture in power line cycles")
        Me._ApertureNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_ApertureNumericLabel
        '
        Me._ApertureNumericLabel.AutoSize = True
        Me._ApertureNumericLabel.Location = New System.Drawing.Point(73, 29)
        Me._ApertureNumericLabel.Name = "_ApertureNumericLabel"
        Me._ApertureNumericLabel.Size = New System.Drawing.Size(103, 13)
        Me._ApertureNumericLabel.TabIndex = 0
        Me._ApertureNumericLabel.Text = "APERTURE [NPLC]"
        '
        '_BinningGroupBox
        '
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckSupportLabel)
        Me._BinningGroupBox.Controls.Add(Me._FailBitPatternNumeric)
        Me._BinningGroupBox.Controls.Add(Me._PassBitPatternNumeric)
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckBitPatternNumeric)
        Me._BinningGroupBox.Controls.Add(Me._FailBitPatternNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._PassBitPatternNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckBitPatternNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckToggle)
        Me._BinningGroupBox.Controls.Add(Me._EotStrobeDurationNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._EotStrobeDurationNumeric)
        Me._BinningGroupBox.Location = New System.Drawing.Point(317, 3)
        Me._BinningGroupBox.Name = "_BinningGroupBox"
        Me._BinningGroupBox.Size = New System.Drawing.Size(296, 191)
        Me._BinningGroupBox.TabIndex = 1
        Me._BinningGroupBox.TabStop = False
        Me._BinningGroupBox.Text = "SOT, EOT, BINNING"
        '
        '_ContactCheckSupportLabel
        '
        Me._ContactCheckSupportLabel.AutoSize = True
        Me._ContactCheckSupportLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ContactCheckSupportLabel.Location = New System.Drawing.Point(3, 175)
        Me._ContactCheckSupportLabel.Name = "_ContactCheckSupportLabel"
        Me._ContactCheckSupportLabel.Size = New System.Drawing.Size(0, 13)
        Me._ContactCheckSupportLabel.TabIndex = 9
        Me._ContactCheckSupportLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_FailBitPatternNumeric
        '
        Me._FailBitPatternNumeric.Location = New System.Drawing.Point(186, 79)
        Me._FailBitPatternNumeric.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me._FailBitPatternNumeric.Name = "_FailBitPatternNumeric"
        Me._FailBitPatternNumeric.Size = New System.Drawing.Size(35, 20)
        Me._FailBitPatternNumeric.TabIndex = 5
        Me._FailBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._FailBitPatternNumeric.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        '_PassBitPatternNumeric
        '
        Me._PassBitPatternNumeric.Location = New System.Drawing.Point(186, 52)
        Me._PassBitPatternNumeric.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me._PassBitPatternNumeric.Name = "_PassBitPatternNumeric"
        Me._PassBitPatternNumeric.Size = New System.Drawing.Size(35, 20)
        Me._PassBitPatternNumeric.TabIndex = 3
        Me._PassBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._PassBitPatternNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_ContactCheckBitPatternNumeric
        '
        Me._ContactCheckBitPatternNumeric.Location = New System.Drawing.Point(186, 106)
        Me._ContactCheckBitPatternNumeric.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me._ContactCheckBitPatternNumeric.Name = "_ContactCheckBitPatternNumeric"
        Me._ContactCheckBitPatternNumeric.Size = New System.Drawing.Size(35, 20)
        Me._ContactCheckBitPatternNumeric.TabIndex = 7
        Me._ContactCheckBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ContactCheckBitPatternNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_FailBitPatternNumericLabel
        '
        Me._FailBitPatternNumericLabel.AutoSize = True
        Me._FailBitPatternNumericLabel.Location = New System.Drawing.Point(75, 83)
        Me._FailBitPatternNumericLabel.Name = "_FailBitPatternNumericLabel"
        Me._FailBitPatternNumericLabel.Size = New System.Drawing.Size(106, 13)
        Me._FailBitPatternNumericLabel.TabIndex = 4
        Me._FailBitPatternNumericLabel.Text = "FAIL BIT PATTERN:"
        Me._FailBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PassBitPatternNumericLabel
        '
        Me._PassBitPatternNumericLabel.AutoSize = True
        Me._PassBitPatternNumericLabel.Location = New System.Drawing.Point(69, 56)
        Me._PassBitPatternNumericLabel.Name = "_PassBitPatternNumericLabel"
        Me._PassBitPatternNumericLabel.Size = New System.Drawing.Size(112, 13)
        Me._PassBitPatternNumericLabel.TabIndex = 2
        Me._PassBitPatternNumericLabel.Text = "PASS BIT PATTERN:"
        Me._PassBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ContactCheckBitPatternNumericLabel
        '
        Me._ContactCheckBitPatternNumericLabel.AutoSize = True
        Me._ContactCheckBitPatternNumericLabel.Location = New System.Drawing.Point(9, 110)
        Me._ContactCheckBitPatternNumericLabel.Name = "_ContactCheckBitPatternNumericLabel"
        Me._ContactCheckBitPatternNumericLabel.Size = New System.Drawing.Size(174, 13)
        Me._ContactCheckBitPatternNumericLabel.TabIndex = 6
        Me._ContactCheckBitPatternNumericLabel.Text = "CONTACT CHECK BIT PATTERN:"
        Me._ContactCheckBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ContactCheckToggle
        '
        Me._ContactCheckToggle.AutoSize = True
        Me._ContactCheckToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContactCheckToggle.Location = New System.Drawing.Point(28, 135)
        Me._ContactCheckToggle.Name = "_ContactCheckToggle"
        Me._ContactCheckToggle.Size = New System.Drawing.Size(172, 17)
        Me._ContactCheckToggle.TabIndex = 8
        Me._ContactCheckToggle.Text = "CONTACT CHECK ENABLED:"
        Me._ContactCheckToggle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContactCheckToggle.UseVisualStyleBackColor = True
        '
        '_EotStrobeDurationNumericLabel
        '
        Me._EotStrobeDurationNumericLabel.AutoSize = True
        Me._EotStrobeDurationNumericLabel.Location = New System.Drawing.Point(26, 29)
        Me._EotStrobeDurationNumericLabel.Name = "_EotStrobeDurationNumericLabel"
        Me._EotStrobeDurationNumericLabel.Size = New System.Drawing.Size(155, 13)
        Me._EotStrobeDurationNumericLabel.TabIndex = 0
        Me._EotStrobeDurationNumericLabel.Text = "EOT STROBE DURATION [S]:"
        '
        '_EotStrobeDurationNumeric
        '
        Me._EotStrobeDurationNumeric.DecimalPlaces = 5
        Me._EotStrobeDurationNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 196608})
        Me._EotStrobeDurationNumeric.Location = New System.Drawing.Point(185, 25)
        Me._EotStrobeDurationNumeric.Maximum = New Decimal(New Integer() {60, 0, 0, 0})
        Me._EotStrobeDurationNumeric.Name = "_EotStrobeDurationNumeric"
        Me._EotStrobeDurationNumeric.Size = New System.Drawing.Size(87, 20)
        Me._EotStrobeDurationNumeric.TabIndex = 1
        Me._EotStrobeDurationNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._EotStrobeDurationNumeric, "The duration of the end-of-test strobe.")
        Me._EotStrobeDurationNumeric.Value = New Decimal(New Integer() {1, 0, 0, 327680})
        '
        '_connectGroupBox
        '
        Me._ConnectGroupBox.Controls.Add(Me._ConnectToggle)
        Me._ConnectGroupBox.Controls.Add(Me._ResourceNameTextBoxLabel)
        Me._ConnectGroupBox.Controls.Add(Me._IdentityTextBox)
        Me._ConnectGroupBox.Controls.Add(Me._ResourceNameTextBox)
        Me._ConnectGroupBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ConnectGroupBox.Location = New System.Drawing.Point(15, 155)
        Me._ConnectGroupBox.Name = "_connectGroupBox"
        Me._ConnectGroupBox.Size = New System.Drawing.Size(629, 72)
        Me._ConnectGroupBox.TabIndex = 0
        Me._ConnectGroupBox.TabStop = False
        Me._ConnectGroupBox.Text = "CONNECT"
        '
        '_connectToggle
        '
        Me._ConnectToggle.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ConnectToggle.Appearance = System.Windows.Forms.Appearance.Button
        Me._ConnectToggle.Location = New System.Drawing.Point(485, 20)
        Me._ConnectToggle.Name = "_connectToggle"
        Me._ConnectToggle.Size = New System.Drawing.Size(121, 23)
        Me._ConnectToggle.TabIndex = 3
        Me._ConnectToggle.Text = "CONNECT"
        Me._ConnectToggle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me._ToolTip.SetToolTip(Me._ConnectToggle, "Depress to connect or release to disconnect the meter")
        Me._ConnectToggle.UseVisualStyleBackColor = True
        '
        '_resourceNameTextBoxLabel
        '
        Me._ResourceNameTextBoxLabel.AutoSize = True
        Me._ResourceNameTextBoxLabel.Location = New System.Drawing.Point(21, 25)
        Me._ResourceNameTextBoxLabel.Name = "_resourceNameTextBoxLabel"
        Me._ResourceNameTextBoxLabel.Size = New System.Drawing.Size(87, 13)
        Me._ResourceNameTextBoxLabel.TabIndex = 2
        Me._ResourceNameTextBoxLabel.Text = "Resource Name:"
        '
        '_identityTextBox
        '
        Me._IdentityTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._IdentityTextBox.Location = New System.Drawing.Point(24, 47)
        Me._IdentityTextBox.Name = "_identityTextBox"
        Me._IdentityTextBox.ReadOnly = True
        Me._IdentityTextBox.Size = New System.Drawing.Size(582, 20)
        Me._IdentityTextBox.TabIndex = 1
        Me._ToolTip.SetToolTip(Me._IdentityTextBox, "Displays the meter identity information")
        '
        '_resourceNameTextBox
        '
        Me._ResourceNameTextBox.Location = New System.Drawing.Point(108, 21)
        Me._ResourceNameTextBox.Name = "_resourceNameTextBox"
        Me._ResourceNameTextBox.Size = New System.Drawing.Size(374, 20)
        Me._ResourceNameTextBox.TabIndex = 0
        Me._ResourceNameTextBox.Text = "TCPIP0::192.168.1.140::gpib0,24::INSTR"
        '
        '_outcomeLayout
        '
        Me._OutcomeLayout.ColumnCount = 7
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._OutcomeLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22.0!))
        Me._OutcomeLayout.Controls.Add(Me._CurrentTextBox, 3, 2)
        Me._OutcomeLayout.Controls.Add(Me._CurrentTextBoxLabel, 3, 1)
        Me._OutcomeLayout.Controls.Add(Me._ResistanceTextBox, 5, 2)
        Me._OutcomeLayout.Controls.Add(Me._VoltageTextBox, 1, 2)
        Me._OutcomeLayout.Controls.Add(Me._ResistanceTextBoxLabel, 5, 1)
        Me._OutcomeLayout.Controls.Add(Me._VoltageTextBoxLabel, 1, 1)
        Me._OutcomeLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._OutcomeLayout.Location = New System.Drawing.Point(15, 41)
        Me._OutcomeLayout.Name = "_outcomeLayout"
        Me._OutcomeLayout.RowCount = 4
        Me._OutcomeLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._OutcomeLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._OutcomeLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me._OutcomeLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._OutcomeLayout.Size = New System.Drawing.Size(629, 70)
        Me._OutcomeLayout.TabIndex = 2
        '
        '_currentTextBox
        '
        Me._CurrentTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._CurrentTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._CurrentTextBox.Location = New System.Drawing.Point(222, 33)
        Me._CurrentTextBox.Name = "_currentTextBox"
        Me._CurrentTextBox.ReadOnly = True
        Me._CurrentTextBox.Size = New System.Drawing.Size(183, 22)
        Me._CurrentTextBox.TabIndex = 0
        Me._CurrentTextBox.Text = "0.000"
        Me._CurrentTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._CurrentTextBox, "Measured Current")
        '
        '_currentTextBoxLabel
        '
        Me._CurrentTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._CurrentTextBoxLabel.Location = New System.Drawing.Point(222, 10)
        Me._CurrentTextBoxLabel.Name = "_currentTextBoxLabel"
        Me._CurrentTextBoxLabel.Size = New System.Drawing.Size(183, 20)
        Me._CurrentTextBoxLabel.TabIndex = 1
        Me._CurrentTextBoxLabel.Text = "CURRENT [A]"
        Me._CurrentTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_ResistanceTextBox
        '
        Me._ResistanceTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ResistanceTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ResistanceTextBox.Location = New System.Drawing.Point(421, 33)
        Me._ResistanceTextBox.Name = "_ResistanceTextBox"
        Me._ResistanceTextBox.ReadOnly = True
        Me._ResistanceTextBox.Size = New System.Drawing.Size(183, 22)
        Me._ResistanceTextBox.TabIndex = 2
        Me._ResistanceTextBox.Text = "0.000"
        Me._ResistanceTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._ResistanceTextBox, "Measured insulation resistance")
        '
        '_voltageTextBox
        '
        Me._VoltageTextBox.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._VoltageTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._VoltageTextBox.Location = New System.Drawing.Point(23, 33)
        Me._VoltageTextBox.Name = "_voltageTextBox"
        Me._VoltageTextBox.ReadOnly = True
        Me._VoltageTextBox.Size = New System.Drawing.Size(183, 22)
        Me._VoltageTextBox.TabIndex = 2
        Me._VoltageTextBox.Text = "0.000"
        Me._VoltageTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ToolTip.SetToolTip(Me._VoltageTextBox, "Measured Voltage")
        '
        '_ResistanceTextBoxLabel
        '
        Me._ResistanceTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ResistanceTextBoxLabel.Location = New System.Drawing.Point(421, 10)
        Me._ResistanceTextBoxLabel.Name = "_ResistanceTextBoxLabel"
        Me._ResistanceTextBoxLabel.Size = New System.Drawing.Size(183, 20)
        Me._ResistanceTextBoxLabel.TabIndex = 3
        Me._ResistanceTextBoxLabel.Text = "RESISTANCE [Ω]"
        Me._ResistanceTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_voltageTextBoxLabel
        '
        Me._VoltageTextBoxLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._VoltageTextBoxLabel.Location = New System.Drawing.Point(23, 17)
        Me._VoltageTextBoxLabel.Name = "_voltageTextBoxLabel"
        Me._VoltageTextBoxLabel.Size = New System.Drawing.Size(183, 13)
        Me._VoltageTextBoxLabel.TabIndex = 3
        Me._VoltageTextBoxLabel.Text = "VOLTAGE [V]"
        Me._VoltageTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        '_manualGroupBox
        '
        Me._ManualGroupBox.Controls.Add(Me._MeasureControlsLayout)
        Me._ManualGroupBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._ManualGroupBox.Location = New System.Drawing.Point(15, 511)
        Me._ManualGroupBox.Name = "_manualGroupBox"
        Me._ManualGroupBox.Size = New System.Drawing.Size(629, 67)
        Me._ManualGroupBox.TabIndex = 1
        Me._ManualGroupBox.TabStop = False
        Me._ManualGroupBox.Text = "TAKE A MEASURMENT"
        '
        '_measureControlsLayout
        '
        Me._MeasureControlsLayout.ColumnCount = 3
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._MeasureControlsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._MeasureControlsLayout.Controls.Add(Me._MeasureButton, 1, 1)
        Me._MeasureControlsLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MeasureControlsLayout.Location = New System.Drawing.Point(3, 16)
        Me._MeasureControlsLayout.Name = "_measureControlsLayout"
        Me._MeasureControlsLayout.RowCount = 3
        Me._MeasureControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._MeasureControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MeasureControlsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me._MeasureControlsLayout.Size = New System.Drawing.Size(623, 48)
        Me._MeasureControlsLayout.TabIndex = 3
        '
        '_measureButton
        '
        Me._MeasureButton.Location = New System.Drawing.Point(231, 13)
        Me._MeasureButton.Name = "_measureButton"
        Me._MeasureButton.Size = New System.Drawing.Size(161, 23)
        Me._MeasureButton.TabIndex = 0
        Me._MeasureButton.Text = "MEASURE RESISTANCE"
        Me._ToolTip.SetToolTip(Me._MeasureButton, "Click to measure high potential insulation resistance")
        Me._MeasureButton.UseVisualStyleBackColor = True
        '
        '_ApplyLayout
        '
        Me._ApplyLayout.ColumnCount = 3
        Me._ApplyLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ApplyLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._ApplyLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ApplyLayout.Controls.Add(Me._ApplyConfigButton, 1, 0)
        Me._ApplyLayout.Dock = System.Windows.Forms.DockStyle.Top
        Me._ApplyLayout.Location = New System.Drawing.Point(15, 436)
        Me._ApplyLayout.Name = "_ApplyLayout"
        Me._ApplyLayout.RowCount = 1
        Me._ApplyLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._ApplyLayout.Size = New System.Drawing.Size(629, 31)
        Me._ApplyLayout.TabIndex = 4
        '
        '_applyConfigButton
        '
        Me._ApplyConfigButton.Location = New System.Drawing.Point(277, 3)
        Me._ApplyConfigButton.Name = "_applyConfigButton"
        Me._ApplyConfigButton.Size = New System.Drawing.Size(75, 23)
        Me._ApplyConfigButton.TabIndex = 8
        Me._ApplyConfigButton.Text = "APPLY"
        Me._ToolTip.SetToolTip(Me._ApplyConfigButton, "Click to apply the meter configuration")
        Me._ApplyConfigButton.UseVisualStyleBackColor = True
        '
        '_TriggerToolStrip
        '
        Me._TriggerToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AwaitTriggerToolStripButton, Me._WaitHourglassLabel, Me._TriggersDropDownButton, Me._AssertTriggerToolStripButton, Me._TriggerActionToolStripLabel})
        Me._TriggerToolStrip.Location = New System.Drawing.Point(12, 619)
        Me._TriggerToolStrip.Name = "_TriggerToolStrip"
        Me._TriggerToolStrip.Size = New System.Drawing.Size(635, 25)
        Me._TriggerToolStrip.Stretch = True
        Me._TriggerToolStrip.TabIndex = 5
        Me._TriggerToolStrip.Text = "Trigger Tool Strip"
        '
        '_AwaitTriggerToolStripButton
        '
        Me._AwaitTriggerToolStripButton.CheckOnClick = True
        Me._AwaitTriggerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AwaitTriggerToolStripButton.Image = CType(resources.GetObject("_AwaitTriggerToolStripButton.Image"), System.Drawing.Image)
        Me._AwaitTriggerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AwaitTriggerToolStripButton.Name = "_AwaitTriggerToolStripButton"
        Me._AwaitTriggerToolStripButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._AwaitTriggerToolStripButton.Size = New System.Drawing.Size(114, 22)
        Me._AwaitTriggerToolStripButton.Text = "WAIT FOR TRIGGER"
        Me._AwaitTriggerToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._AwaitTriggerToolStripButton.ToolTipText = "Depress to wait for trigger or release to abort."
        '
        '_WaitHourglassLabel
        '
        Me._WaitHourglassLabel.Name = "_WaitHourglassLabel"
        Me._WaitHourglassLabel.Size = New System.Drawing.Size(20, 22)
        Me._WaitHourglassLabel.Text = "[-]"
        Me._WaitHourglassLabel.ToolTipText = "Waiting for trigger"
        '
        '_TriggersDropDownButton
        '
        Me._TriggersDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TriggersDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ManualTriggerMenuItem, Me._SotTriggerMenuItem})
        Me._TriggersDropDownButton.Image = CType(resources.GetObject("_TriggersDropDownButton.Image"), System.Drawing.Image)
        Me._TriggersDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TriggersDropDownButton.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._TriggersDropDownButton.Name = "_TriggersDropDownButton"
        Me._TriggersDropDownButton.Size = New System.Drawing.Size(62, 22)
        Me._TriggersDropDownButton.Text = "Triggers"
        Me._TriggersDropDownButton.ToolTipText = "Click to select trigger option"
        '
        '_ManualTriggerMenuItem
        '
        Me._ManualTriggerMenuItem.CheckOnClick = True
        Me._ManualTriggerMenuItem.Name = "_ManualTriggerMenuItem"
        Me._ManualTriggerMenuItem.Size = New System.Drawing.Size(137, 22)
        Me._ManualTriggerMenuItem.Text = "Manual"
        '
        '_SotTriggerMenuItem
        '
        Me._SotTriggerMenuItem.Checked = True
        Me._SotTriggerMenuItem.CheckOnClick = True
        Me._SotTriggerMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._SotTriggerMenuItem.Name = "_SotTriggerMenuItem"
        Me._SotTriggerMenuItem.Size = New System.Drawing.Size(137, 22)
        Me._SotTriggerMenuItem.Text = "SOT (Line 6)"
        '
        '_AssertTriggerToolStripButton
        '
        Me._AssertTriggerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AssertTriggerToolStripButton.Image = CType(resources.GetObject("_AssertTriggerToolStripButton.Image"), System.Drawing.Image)
        Me._AssertTriggerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AssertTriggerToolStripButton.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._AssertTriggerToolStripButton.Name = "_AssertTriggerToolStripButton"
        Me._AssertTriggerToolStripButton.Size = New System.Drawing.Size(38, 22)
        Me._AssertTriggerToolStripButton.Text = "*TRG"
        '
        '_TriggerActionToolStripLabel
        '
        Me._TriggerActionToolStripLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TriggerActionToolStripLabel.Name = "_TriggerActionToolStripLabel"
        Me._TriggerActionToolStripLabel.Size = New System.Drawing.Size(0, 22)
        Me._TriggerActionToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_errorProvider
        '
        Me._ErrorProvider.ContainerControl = Me
        '
        '_meterTimer
        '
        '
        '_StatusStrip
        '
        Me._StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._StatusLabel})
        Me._StatusStrip.Location = New System.Drawing.Point(0, 661)
        Me._StatusStrip.Name = "_StatusStrip"
        Me._StatusStrip.Size = New System.Drawing.Size(660, 22)
        Me._StatusStrip.TabIndex = 1
        Me._StatusStrip.Text = "StatusStrip1"
        '
        '_StatusLabel
        '
        Me._StatusLabel.Name = "_StatusLabel"
        Me._StatusLabel.Size = New System.Drawing.Size(54, 17)
        Me._StatusLabel.Text = "<status>"
        '
        'Console
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(660, 683)
        Me.Controls.Add(Me._StatusStrip)
        Me.Controls.Add(Me._MainLayout)
        Me.Name = "Console"
        Me.Text = "HIPOT Console"
        Me._MainLayout.ResumeLayout(False)
        Me._MainLayout.PerformLayout()
        Me._ConfigurationLayout.ResumeLayout(False)
        Me._ConfigGroupBox.ResumeLayout(False)
        Me._ConfigGroupBox.PerformLayout()
        CType(Me._CurrentLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._VoltageLevelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ResistanceRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ResistanceLowLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DwellTimeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ApertureNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._BinningGroupBox.ResumeLayout(False)
        Me._BinningGroupBox.PerformLayout()
        CType(Me._FailBitPatternNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._PassBitPatternNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ContactCheckBitPatternNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EotStrobeDurationNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ConnectGroupBox.ResumeLayout(False)
        Me._ConnectGroupBox.PerformLayout()
        Me._OutcomeLayout.ResumeLayout(False)
        Me._OutcomeLayout.PerformLayout()
        Me._ManualGroupBox.ResumeLayout(False)
        Me._MeasureControlsLayout.ResumeLayout(False)
        Me._ApplyLayout.ResumeLayout(False)
        Me._TriggerToolStrip.ResumeLayout(False)
        Me._TriggerToolStrip.PerformLayout()
        CType(Me._ErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me._StatusStrip.ResumeLayout(False)
        Me._StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

        Private WithEvents _MainLayout As System.Windows.Forms.TableLayoutPanel

        Private WithEvents _ConnectGroupBox As System.Windows.Forms.GroupBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResourceNameTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _IdentityTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResourceNameTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ConnectToggle As System.Windows.Forms.CheckBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ToolTip As System.Windows.Forms.ToolTip

        Private WithEvents _ConfigurationLayout As System.Windows.Forms.TableLayoutPanel

        Private WithEvents _ConfigGroupBox As System.Windows.Forms.GroupBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _CurrentLimitNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _VoltageLevelNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _VoltageLevelNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResistanceLowLimitNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _DwellTimeNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _DwellTimeNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ApertureNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _ApertureNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResistanceLowLimitNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _ApplyConfigButton As System.Windows.Forms.Button

        Private WithEvents _OutcomeLayout As System.Windows.Forms.TableLayoutPanel

    ''' <summary> The with events control. </summary>
    Private WithEvents _CurrentTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _CurrentTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResistanceTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _VoltageTextBox As System.Windows.Forms.TextBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResistanceTextBoxLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _VoltageTextBoxLabel As System.Windows.Forms.Label

        Private WithEvents _ErrorProvider As System.Windows.Forms.ErrorProvider

        Private WithEvents _ManualGroupBox As System.Windows.Forms.GroupBox

        Private WithEvents _MeasureControlsLayout As System.Windows.Forms.TableLayoutPanel

    ''' <summary> The with events control. </summary>
    Private WithEvents _MeasureButton As System.Windows.Forms.Button

        Private WithEvents _MeterTimer As System.Windows.Forms.Timer

    ''' <summary> The with events control. </summary>
    Private WithEvents _CurrentLimitNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResistanceRangeNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ResistanceRangeNumeric As System.Windows.Forms.NumericUpDown

        Private WithEvents _StatusStrip As System.Windows.Forms.StatusStrip

        Private WithEvents _StatusLabel As System.Windows.Forms.ToolStripStatusLabel

        Private WithEvents _BinningGroupBox As System.Windows.Forms.GroupBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _EotStrobeDurationNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _EotStrobeDurationNumeric As System.Windows.Forms.NumericUpDown

        Private WithEvents _ApplyLayout As System.Windows.Forms.TableLayoutPanel

    ''' <summary> The with events control. </summary>
    Private WithEvents _ContactCheckBitPatternNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ContactCheckToggle As System.Windows.Forms.CheckBox

    ''' <summary> The with events control. </summary>
    Private WithEvents _ContactCheckBitPatternNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _FailBitPatternNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _PassBitPatternNumeric As System.Windows.Forms.NumericUpDown

    ''' <summary> The with events control. </summary>
    Private WithEvents _FailBitPatternNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _PassBitPatternNumericLabel As System.Windows.Forms.Label

    ''' <summary> The with events control. </summary>
    Private WithEvents _ContactCheckSupportLabel As System.Windows.Forms.Label

        Private WithEvents _AwaitTriggerToolStripButton As ToolStripButton

        Private WithEvents _WaitHourglassLabel As ToolStripLabel

        Private WithEvents _TriggersDropDownButton As ToolStripDropDownButton

        Private WithEvents _ManualTriggerMenuItem As ToolStripMenuItem

        Private WithEvents _SotTriggerMenuItem As ToolStripMenuItem

        Private WithEvents _AssertTriggerToolStripButton As ToolStripButton

        Private WithEvents _TriggerActionToolStripLabel As ToolStripLabel

        Private WithEvents _TriggerToolStrip As ToolStrip
End Class
