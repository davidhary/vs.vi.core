''' <summary> Thermal Transient Meter Tester Console. </summary>
''' <remarks>
''' (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-09-20, 2.3.3915. Created </para>
''' </remarks>
Public Class Console
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Gets or sets the initializing components. </summary>
    ''' <value> The initializing components. </value>
    Private Property InitializingComponents As Boolean

    ''' <summary>
    ''' This constructor is public to allow using this form as a startup form for the project.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        Me._InitializingComponents = True
        Me.InitializeComponent()

        Me._ResourceNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding(
                                                 "Text", Global.isr.Hipot.Driver.My.MySettings.Default, "ResourceName", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._DwellTimeNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "DwellTime", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._ApertureNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "Aperture", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._VoltageLevelNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "VoltageLevel", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._CurrentLimitNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "CurrentLimit", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._ResistanceLowLimitNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                   "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "ResistanceLowLimit", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._ResistanceRangeNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                   "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "ResistanceRange", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._EotStrobeDurationNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                   "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "EotStrobeDuration", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._PassBitPatternNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                   "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "PassBitPattern", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._FailBitPatternNumeric.DataBindings.Add(New System.Windows.Forms.Binding(
                                                   "Value", Global.isr.Hipot.Driver.My.MySettings.Default, "FailBitPattern", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._ContactCheckToggle.DataBindings.Add(New System.Windows.Forms.Binding(
                                                "Checked", Global.isr.Hipot.Driver.My.MySettings.Default, "ContactCheckEnabled", True, System.Windows.Forms.DataSourceUpdateMode.OnValidation))
        Me._InitializingComponents = False
        Me.Meter = New isr.Hipot.Driver.Device

    End Sub

    ''' <summary>
    ''' Disposes of the resources (other than memory) used by the
    ''' <see cref="T:System.Windows.Forms.Form" />.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <see langword="true" /> to release both managed and unmanaged
    '''                          resources; <see langword="false" /> to release only unmanaged
    '''                          resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing Then
                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
                If Me._Meter IsNot Nothing Then
                    Me._Meter.Dispose()
                    Me._Meter = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


#End Region

#Region " EVENT HANDLERS:  FORM "

    ''' <summary> Sets the caption for this form. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Friend Sub RefreshCaption()
        Me.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1}.{2}.{3}: CONSOLE",
                                My.Application.Info.Title, My.Application.Info.Version.Major,
                                My.Application.Info.Version.Minor, My.Application.Info.Version.Build)
    End Sub

    ''' <summary> Terminates objects before closing the form. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Form closing event information. </param>
    Private Sub ConsolePanel_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try

            ' dispose of the meter.
            If Me._Meter IsNot Nothing Then
                Me._Meter.Dispose()
                Me._Meter = Nothing
            End If

            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()

            ' wait for timer to terminate all is actions
            isr.Hipot.Driver.My.MyLibrary.DoEvents()
            isr.Hipot.Driver.My.MyLibrary.DoEventsTaskDelay(TimeSpan.FromMilliseconds(400))

            ' allow all events requiring the panel to execute on their thread.
            ' this allows all timer events that were in progress to be consummated before closing the form.
            ' this does not prevent timer exceptions in design mode.
            For i As Integer = 1 To 1000
                Windows.Forms.Application.DoEvents()
            Next


        Finally


            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try
    End Sub

    ''' <summary> Loads this module. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConsolePanel_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            Me.Cursor = System.Windows.Forms.Cursors.AppStarting

            ' Center the form
            Me.CenterToScreen()

        Catch

            Throw

        Finally

            ' turn on the default pointer
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    ''' <summary> Displays final values after the form is shown. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ConsolePanel_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Try

            Me.Cursor = System.Windows.Forms.Cursors.AppStarting

            Windows.Forms.Application.DoEvents()

            Me.RefreshCaption()

            My.Application.Log.WriteEntry(Me.Text & " shown", TraceEventType.Verbose)

            Me.OnConnectionChanged()

            Me.DisplayStatus("Logging to {0}", My.Application.Log.DefaultFileLogWriter.FullLogFileName)

        Catch

            Throw

        Finally

            ' turn on the default pointer
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CONNECT "

    ''' <summary> Updates the availability of the controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub OnMeasurementStatusChanged()
        Me._AwaitTriggerToolStripButton.Enabled = Me.Meter.IsConnected
        Me._AssertTriggerToolStripButton.Enabled = Me.Meter.IsConnected AndAlso Me._MeterTimer.Enabled AndAlso Me._ManualTriggerMenuItem.Checked
        Me._ApplyConfigButton.Enabled = Me.Meter.IsConnected AndAlso Not Me._MeterTimer.Enabled
        Me._MeasureButton.Enabled = Me.Meter.IsConnected AndAlso Not Me._MeterTimer.Enabled
    End Sub

    ''' <summary> Updates the connection related controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub OnConnectionChanged()
        If Me.Meter.IsConnected Then
            Me._ContactCheckBitPatternNumeric.Enabled = Me.Meter.ContactCheckSupported
            Me._ContactCheckToggle.Enabled = Me.Meter.ContactCheckSupported
            Me._ContactCheckSupportLabel.Text = If(Me.Meter.ContactCheckSupported, "SUPPORTS CONTACT CHECK", "CONTACT CHECK NOT SUPPORTED")
        Else
            Me._MeterTimer.Enabled = False
        End If
        Me.OnMeasurementStatusChanged()
    End Sub

    ''' <summary>
    ''' Silently toggles the control checked value. Allows using the enabled state of the control to
    ''' control addressing control events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="control"> The control. </param>
    ''' <param name="checked"> if set to <c>True</c> [checked]. </param>
    Private Shared Sub SilentToggle(ByVal control As CheckBox, ByVal checked As Boolean)
        Dim wasEnabled As Boolean = control.Enabled
        control.Enabled = False
        control.Checked = checked
        control.Enabled = wasEnabled
    End Sub

    ''' <summary> Displays the status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="format"> The format. </param>
    ''' <param name="args">   The args. </param>
    ''' <returns> A String. </returns>
    Private Function DisplayStatus(ByVal format As String, ByVal ParamArray args() As Object) As String
        Dim status As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        Me._StatusLabel.Text = status
        Return status
    End Function

    ''' <summary>
    ''' Connects or disconnected from the instrument using the
    ''' <see cref="_resourceNameTextBox">resource name</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConnectToggle_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ConnectToggle.CheckedChanged

        If Me._ConnectToggle.Enabled Then

            Me._ErrorProvider.SetError(Me._ConnectToggle, "")
            Me._ErrorProvider.SetError(Me._ApplyConfigButton, "")
            Me._ErrorProvider.SetError(Me._MeasureButton, "")
            Me._ErrorProvider.SetError(Me._TriggerToolStrip, "")

            Dim resourceName As String = Me._ResourceNameTextBox.Text
            If Me._ConnectToggle.Checked Then

                Try

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                    If String.IsNullOrWhiteSpace(resourceName) Then
                        My.Application.Log.WriteEntry("Empty resource name", TraceEventType.Warning)
                        Me._StatusLabel.Text = "EMPTY RESOURCE NAME"
                        SilentToggle(Me._ConnectToggle, False)
                        Return
                    End If

                    My.Application.Log.WriteEntry(Me.DisplayStatus("Connecting to {0} ", resourceName), TraceEventType.Verbose)

                    Me._IdentityTextBox.Text = "<connecting>"

                    If Me.Meter.Connect(resourceName) Then

                        My.Application.Log.WriteEntry(Me.DisplayStatus("Connected to {0} ", resourceName), TraceEventType.Verbose)

                        ' display the device information.
                        Me._IdentityTextBox.Text = Me.Meter.Identity

                    Else

                        Me._ErrorProvider.SetError(Me._ConnectToggle, "Connection failed")
                        Me._IdentityTextBox.Text = "<failed connecting>"
                        My.Application.Log.WriteEntry(Me.DisplayStatus("Failed connecting to {0} ", resourceName), TraceEventType.Error)
                        SilentToggle(Me._ConnectToggle, False)

                    End If

                Catch ex As Exception

                    Me._ErrorProvider.SetError(Me._ConnectToggle, "Connection failed")
                    Me._IdentityTextBox.Text = "<failed connecting>"
                    My.Application.Log.WriteEntry(Me.DisplayStatus("Failed connecting to {0} ", resourceName), TraceEventType.Error)
                    My.Application.Log.WriteException(ex, TraceEventType.Error, "failed connecting")
                    SilentToggle(Me._ConnectToggle, False)

                Finally

                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Me.OnConnectionChanged()

                End Try

            Else

                Try

                    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

                    My.Application.Log.WriteEntry(Me.DisplayStatus("Disconnecting from {0} ", resourceName), TraceEventType.Verbose)

                    Me._IdentityTextBox.Text = "<disconnecting>"

                    ' abort any ongoing triggered measurements. Otherwise, the instrument will hang.
                    Me.AbortMeasurement()

                    If Me.Meter.Disconnect() Then

                        Me._IdentityTextBox.Text = "<disconnected>"
                        My.Application.Log.WriteEntry(Me.DisplayStatus("Disconnected from {0} ", resourceName), TraceEventType.Verbose)

                    Else

                        Me._ErrorProvider.SetError(Me._ConnectToggle, "Disconnection failed")
                        Me._IdentityTextBox.Text = "<failed disconnecting>"
                        My.Application.Log.WriteEntry(Me.DisplayStatus("Failed disconnecting from {0} ", resourceName), TraceEventType.Warning)

                    End If

                Catch ex As Exception

                    Me._ErrorProvider.SetError(Me._ConnectToggle, "Disconnection failed")
                    Me._IdentityTextBox.Text = "<failed disconnecting>"
                    My.Application.Log.WriteException(ex, TraceEventType.Error, "failed connecting")

                Finally

                    Me.Cursor = System.Windows.Forms.Cursors.Default
                    Me.OnConnectionChanged()

                End Try

            End If

        End If
        Me._ConnectToggle.Text = If(Me._ConnectToggle.Checked, "DISCONNECT", "CONNECT")
    End Sub

#End Region

#Region " CONFIGURE "

    ''' <summary>
    ''' Read the current settings in the device -- this does not send the settings to the instrument.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ReadSettings()
        My.Application.Log.WriteEntry("Storing configuration settings in the device.", TraceEventType.Verbose)
        Me.Meter.ReadSettings()
    End Sub

    ''' <summary>
    ''' Updates the current settings from the device -- this does not read the settings from the
    ''' instrument.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub SaveSettings()
        My.Application.Log.WriteEntry("Updating configuration settings from the device.", TraceEventType.Verbose)
        Me.Meter.UpdateSettings()
        Me.Meter.SaveSettings()
    End Sub

    ''' <summary> Saves the configuration settings and sends them to the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ApplyConfigButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ApplyConfigButton.Click

        Me._ErrorProvider.SetError(Me._TriggerToolStrip, "")
        Me._ErrorProvider.SetError(Me._MeasureButton, "")
        Me._ErrorProvider.SetError(Me._ApplyConfigButton, "")

        Me.ReadSettings()

        If Me.Meter.IsConnected Then

            My.Application.Log.WriteEntry("Sending meter configuration settings to the instrument", TraceEventType.Verbose)
            If Me.Meter.Configure Then
                My.Application.Log.WriteEntry("meter measurement configured successfully.", TraceEventType.Verbose)
                Me.SaveSettings()

            Else
                Me._ErrorProvider.SetError(Me._ApplyConfigButton, "Failed sending meter configuration settings to the instrument")
                My.Application.Log.WriteEntry("Failed sending meter configuration settings to the instrument", TraceEventType.Warning)
            End If

        Else

            Me._ErrorProvider.SetError(Me._ApplyConfigButton, "Instrument not connected")
            My.Application.Log.WriteEntry("Instrument not connected", TraceEventType.Warning)

        End If

    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> The device access locker. </summary>
    Private ReadOnly _DeviceAccessLocker As New Object

    ''' <summary> Displays the results. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ShowResults()

        Me._VoltageTextBox.Text = Me.Meter.VoltageReading
        Me._CurrentTextBox.Text = Me.Meter.CurrentReading
        Me._ResistanceTextBox.Text = Me.Meter.ResistanceReading

        If (Me.Meter.Outcome And Driver.MeasurementOutcomes.PartFailed) <> 0 Then
            Me._ErrorProvider.SetError(Me._MeasureButton, "Resistance out of range")
        ElseIf (Me.Meter.Outcome And Driver.MeasurementOutcomes.MeasurementFailed) <> 0 Then
            Me._ErrorProvider.SetError(Me._MeasureButton, "Measurement failed")
        ElseIf (Me.Meter.Outcome And Driver.MeasurementOutcomes.MeasurementNotMade) <> 0 Then
            Me._ErrorProvider.SetError(Me._MeasureButton, "Measurement not made")
        ElseIf (Me.Meter.Outcome And Driver.MeasurementOutcomes.FailedContactCheck) <> 0 Then
            Me._ErrorProvider.SetError(Me._MeasureButton, "Failed contact check")
        End If

    End Sub

    ''' <summary> Measures this object. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub Measure()
        SyncLock Me._DeviceAccessLocker
            Me._VoltageTextBox.Text = String.Empty
            Me._CurrentTextBox.Text = String.Empty
            Me._ResistanceTextBox.Text = String.Empty
            Me._ErrorProvider.SetError(Me._MeasureButton, "")
            Me._ErrorProvider.SetIconPadding(Me._MeasureButton, -10)
            If Me.Meter.Measure Then
                Me.ShowResults()
            Else
                Me._ErrorProvider.SetError(Me._MeasureButton, "Failed Measuring Resistance")
                My.Application.Log.WriteEntry("Failed Measuring Resistance", TraceEventType.Warning)
            End If
        End SyncLock
    End Sub

    ''' <summary> Measures the thermal transient voltage. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _MeasureButton.Click
        Dim ctrl As Control = TryCast(sender, Control)
        Try
            Me._ErrorProvider.Clear()
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
            My.Application.Log.WriteEntry(Me.DisplayStatus("Starting a measurements", TraceEventType.Verbose))
            Me.Measure()
        Catch ex As Exception
            Me._ErrorProvider.SetError(ctrl, "Measurement failed")
            My.Application.Log.WriteException(ex, TraceEventType.Error, "measurement failed")
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Aborts the measurement cycle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AbortMeasurementThis()
        Me._MeterTimer.Enabled = False
        Me.Meter.AbortMeasurement()
        Me._AwaitTriggerToolStripButton.Enabled = False
        Me._AwaitTriggerToolStripButton.Checked = False
        Me.OnMeasurementStatusChanged()
        Me.Meter.Configure()
    End Sub

    ''' <summary> Aborts the measurement cycle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AbortMeasurement()
        SyncLock Me._DeviceAccessLocker
            Me.AbortMeasurementThis()
        End SyncLock
    End Sub

    ''' <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AssertTriggerToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AssertTriggerToolStripButton.Click
        Me._ErrorProvider.SetError(Me._TriggerToolStrip, "")
        Try
            If Me._ManualTriggerMenuItem.Checked AndAlso Me._MeterTimer.Enabled Then
                Me.AssertTrigger()
            ElseIf Not Me._ManualTriggerMenuItem.Checked Then
                Me._ErrorProvider.SetError(Me._TriggerToolStrip, "Manual trigger ignored -- not manual mode")
            ElseIf Not Me._MeterTimer.Enabled Then
                Me._ErrorProvider.SetError(Me._TriggerToolStrip, "Manual trigger ignored -- triggering is not active")
            End If
        Catch ex As Exception
            Me._ErrorProvider.SetError(Me._TriggerToolStrip, "Exception occurred aborting")
            My.Application.Log.WriteException(ex, TraceEventType.Error, "Exception occurred aborting.")
        End Try
    End Sub

    ''' <summary> Outputs a trigger to make a measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AssertTrigger()
        SyncLock Me._DeviceAccessLocker
            If Not Me.Meter.TriggerMeasurement Then
                My.Application.Log.WriteEntry("Failed triggering instrument", TraceEventType.Warning)
            End If
        End SyncLock
    End Sub

    ''' <summary> True if abort requested. </summary>
    Private _AbortRequested As Boolean

    ''' <summary> Turns on or aborts waiting for trigger. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="enabled"> True to enable, false to disable. </param>
    ''' <param name="checked"> if set to <c>True</c> [checked]. </param>
    Private Sub ToggleAwaitTrigger(ByVal enabled As Boolean, ByVal checked As Boolean)
        Me._ErrorProvider.SetIconPadding(Me._TriggerToolStrip, -10)
        Me._ErrorProvider.SetError(Me._TriggerToolStrip, "")
        If enabled Then
            Me._AbortRequested = Not Me._AwaitTriggerToolStripButton.Checked
            If checked Then
                SyncLock Me._DeviceAccessLocker
                    My.Application.Log.WriteEntry("Preparing instrument for waiting for trigger", TraceEventType.Verbose)
                    Me._TriggerActionToolStripLabel.Text = "PREPARING"
                    If Me.Meter.PrepareForTrigger(Me._ManualTriggerMenuItem.Checked) Then
                        Me._TriggerActionToolStripLabel.Text = "WAITING FOR TRIGGERED MEASUREMENT"
                        My.Application.Log.WriteEntry("Monitoring instrument for measurements.", TraceEventType.Verbose)
                        Me._MeterTimer.Interval = 100
                        Me._MeterTimer.Enabled = True
                        Me.OnMeasurementStatusChanged()
                    Else
                        Me._TriggerActionToolStripLabel.Text = "FAILED PREPARING"
                        Me._ErrorProvider.SetError(Me._TriggerToolStrip, "Failed preparing instrument for waiting for trigger")
                        My.Application.Log.WriteEntry("Failed preparing instrument for waiting for trigger", TraceEventType.Warning)
                        Me.AbortMeasurementThis()
                    End If
                End SyncLock
            Else
                Me._TriggerActionToolStripLabel.Text = "ABORT REQUESTED"
                Me.AbortMeasurementThis()
            End If
        End If
        Me._AwaitTriggerToolStripButton.Text = If(Me._AwaitTriggerToolStripButton.Checked, "ABORT TRIGGERING", "WAIT FOR A TRIGGER")
    End Sub

    ''' <summary> Turns on or aborts waiting for trigger. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AwaitTriggerToolStripButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AwaitTriggerToolStripButton.CheckedChanged
        If Me._InitializingComponents Then Return
        Me.ToggleAwaitTrigger(Me._AwaitTriggerToolStripButton.Enabled, Me._AwaitTriggerToolStripButton.Checked)
    End Sub

    ''' <summary> Manual trigger menu item check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ManualTriggerMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _ManualTriggerMenuItem.CheckStateChanged
        If Me._SotTriggerMenuItem.Checked Xor Me._ManualTriggerMenuItem.Checked Then
            Me._SotTriggerMenuItem.Checked = Not Me._ManualTriggerMenuItem.Checked
        End If
    End Sub

    ''' <summary> Sot trigger menu item check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SotTriggerMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _SotTriggerMenuItem.CheckStateChanged
        If Me._SotTriggerMenuItem.Checked Xor Me._ManualTriggerMenuItem.Checked Then
            Me._ManualTriggerMenuItem.Checked = Not Me._SotTriggerMenuItem.Checked
        End If
    End Sub

    ''' <summary> The status bar. </summary>
    Private _StatusBar As String = "|"

    ''' <summary>
    ''' Monitors measurements. Once found, reads and displays and restarts the cycle.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeterTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MeterTimer.Tick
        If Me._StatusBar = "|" Then
            Me._StatusBar = "/"
        ElseIf Me._StatusBar = "/" Then
            Me._StatusBar = "-"
        ElseIf Me._StatusBar = "-" Then
            Me._StatusBar = "\"
        ElseIf Me._StatusBar = "\" Then
            Me._StatusBar = "|"
        End If
        SyncLock Me._DeviceAccessLocker
            Try
                Me._MeterTimer.Enabled = False
                If Me._AbortRequested Then
                    Me.AbortMeasurementThis()
                    Return
                End If
                If Me.Meter.IsMeasurementCompleted Then
                    Me._TriggerActionToolStripLabel.Text = "READING..."
                    If Me.Meter.ReadMeasurements() Then
                        Me._TriggerActionToolStripLabel.Text = "DATA AVAILABLE..."
                        Me.ShowResults()
                    Else
                        Me._VoltageTextBox.Text = String.Empty
                        Me._ResistanceTextBox.Text = String.Empty
                        Me._CurrentTextBox.Text = String.Empty
                        Me._TriggerActionToolStripLabel.Text = "FAILED READING..."
                    End If
                    Me._TriggerActionToolStripLabel.Text = "PREPARING..."
                    If Me.Meter.PrepareForTrigger(Me._ManualTriggerMenuItem.Checked) Then
                        Me._TriggerActionToolStripLabel.Text = "WAITING FOR TRIGGRED MEASUREMENT..."
                        My.Application.Log.WriteEntry("Monitoring instrument for measurements.", TraceEventType.Verbose)
                    Else
                        Me._TriggerActionToolStripLabel.Text = "FAILED PREPARING..."
                        Me._ErrorProvider.SetError(Me._TriggerToolStrip, "Failed preparing instrument for waiting for trigger")
                        My.Application.Log.WriteEntry("Failed preparing instrument for waiting for trigger", TraceEventType.Warning)
                    End If
                Else
                    Me._TriggerActionToolStripLabel.Text = "WAITING FOR TRIGGERED MEASUREMENT. " & Me._StatusBar
                End If
                Me._MeterTimer.Enabled = Not Me._AbortRequested
            Catch ex As Exception
                Me._ErrorProvider.SetError(Me._TriggerToolStrip, "Exception occurred monitoring instrument for data")
                My.Application.Log.WriteException(ex, TraceEventType.Error, "Exception occurred monitoring instrument for data.")
                Me._AwaitTriggerToolStripButton.Checked = False
            End Try
        End SyncLock
    End Sub

#End Region

#Region " HIPOT METER "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Meter As isr.Hipot.Driver.IDevice
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets reference to the thermal transient meter device. </summary>
    ''' <value> The meter. </value>
    Private Property Meter() As isr.Hipot.Driver.IDevice
        Get
            Return Me._Meter
        End Get
        Set(ByVal value As isr.Hipot.Driver.IDevice)
            Me._Meter = value
        End Set
    End Property

    ''' <summary> Meter exception available. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Thread exception event information. </param>
    Private Sub Meter_ExceptionAvailable(ByVal sender As Object, ByVal e As System.Threading.ThreadExceptionEventArgs) Handles _Meter.ExceptionAvailable
        My.Application.Log.WriteException(e.Exception, TraceEventType.Error, "Meter exception occurred")
    End Sub

    ''' <summary> Meter message available. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Message event information. </param>
    Private Sub Meter_MessageAvailable(ByVal sender As Object, ByVal e As isr.Hipot.Driver.MessageEventArgs) Handles _Meter.MessageAvailable
        My.Application.Log.WriteEntry(e.Details, e.TraceLevel)
    End Sub

#End Region

End Class
