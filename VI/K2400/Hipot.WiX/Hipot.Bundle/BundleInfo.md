\(c\) 2010 by Integrated Scientific Resources, Inc.

Licensed under The Fair End User^1^ and MIT Licenses^2^.

**Installation Information.** This program installs the HIPOT program
and source code.

**Required Hardware.** This software uses hardware from Keithley
Instruments ([www.keihtley.com](www.keihtley.com))

\(1\) Licensed under The Fair End User Use License, a copy of which you
can download [Fair End User Use License](http://www.isr.cc/licenses/Fair%20End%20User%20Use%20License.pdf).

\(2\) Source code is licensed under [The MIT
License.](http://opensource.org/licenses/MIT)

Unless required by applicable law or agreed to in writing, this software
is provided \"AS IS\" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.
