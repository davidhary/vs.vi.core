﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SenseView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._EnabledSenseFunctionsListBoxLabel = New System.Windows.Forms.Label()
        Me._EnabledSenseFunctionsListBox = New System.Windows.Forms.CheckedListBox()
        Me._ApplySenseFunctionButton = New System.Windows.Forms.Button()
        Me._ConcurrentSenseCheckBox = New System.Windows.Forms.CheckBox()
        Me._FourWireSenseCheckBox = New System.Windows.Forms.CheckBox()
        Me._SenseRangeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._NplcNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SenseRangeNumericLabel = New System.Windows.Forms.Label()
        Me._NplcNumericLabel = New System.Windows.Forms.Label()
        Me._SenseFunctionComboBox = New System.Windows.Forms.ComboBox()
        Me._SenseFunctionComboBoxLabel = New System.Windows.Forms.Label()
        Me._SenseAutoRangeToggle = New System.Windows.Forms.CheckBox()
        Me._ApplySenseSettingsButton = New System.Windows.Forms.Button()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        CType(Me._SenseRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._NplcNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Panel.SuspendLayout()
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_EnabledSenseFunctionsListBoxLabel
        '
        Me._EnabledSenseFunctionsListBoxLabel.AutoSize = True
        Me._EnabledSenseFunctionsListBoxLabel.Location = New System.Drawing.Point(16, 5)
        Me._EnabledSenseFunctionsListBoxLabel.Name = "_EnabledSenseFunctionsListBoxLabel"
        Me._EnabledSenseFunctionsListBoxLabel.Size = New System.Drawing.Size(65, 17)
        Me._EnabledSenseFunctionsListBoxLabel.TabIndex = 40
        Me._EnabledSenseFunctionsListBoxLabel.Text = "Functions:"
        '
        '_EnabledSenseFunctionsListBox
        '
        Me._EnabledSenseFunctionsListBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._EnabledSenseFunctionsListBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._EnabledSenseFunctionsListBox.FormattingEnabled = True
        Me._EnabledSenseFunctionsListBox.Location = New System.Drawing.Point(84, 5)
        Me._EnabledSenseFunctionsListBox.Name = "_EnabledSenseFunctionsListBox"
        Me._EnabledSenseFunctionsListBox.Size = New System.Drawing.Size(144, 64)
        Me._EnabledSenseFunctionsListBox.TabIndex = 39
        '
        '_ApplySenseFunctionButton
        '
        Me._ApplySenseFunctionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplySenseFunctionButton.Location = New System.Drawing.Point(291, 56)
        Me._ApplySenseFunctionButton.Name = "_ApplySenseFunctionButton"
        Me._ApplySenseFunctionButton.Size = New System.Drawing.Size(53, 30)
        Me._ApplySenseFunctionButton.TabIndex = 38
        Me._ApplySenseFunctionButton.Text = "Apply"
        Me._ApplySenseFunctionButton.UseVisualStyleBackColor = True
        '
        '_ConcurrentSenseCheckBox
        '
        Me._ConcurrentSenseCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ConcurrentSenseCheckBox.AutoSize = True
        Me._ConcurrentSenseCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ConcurrentSenseCheckBox.Location = New System.Drawing.Point(236, 5)
        Me._ConcurrentSenseCheckBox.Name = "_ConcurrentSenseCheckBox"
        Me._ConcurrentSenseCheckBox.Size = New System.Drawing.Size(95, 21)
        Me._ConcurrentSenseCheckBox.TabIndex = 37
        Me._ConcurrentSenseCheckBox.Text = "Concurrent"
        Me._ConcurrentSenseCheckBox.UseVisualStyleBackColor = True
        '
        '_FourWireSenseCheckBox
        '
        Me._FourWireSenseCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._FourWireSenseCheckBox.AutoSize = True
        Me._FourWireSenseCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FourWireSenseCheckBox.Location = New System.Drawing.Point(236, 31)
        Me._FourWireSenseCheckBox.Name = "_FourWireSenseCheckBox"
        Me._FourWireSenseCheckBox.Size = New System.Drawing.Size(88, 21)
        Me._FourWireSenseCheckBox.TabIndex = 36
        Me._FourWireSenseCheckBox.Text = "Four Wire"
        Me._FourWireSenseCheckBox.UseVisualStyleBackColor = True
        '
        '_SenseRangeNumeric
        '
        Me._SenseRangeNumeric.DecimalPlaces = 3
        Me._SenseRangeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SenseRangeNumeric.Location = New System.Drawing.Point(119, 150)
        Me._SenseRangeNumeric.Maximum = New Decimal(New Integer() {1010, 0, 0, 0})
        Me._SenseRangeNumeric.Name = "_SenseRangeNumeric"
        Me._SenseRangeNumeric.Size = New System.Drawing.Size(76, 25)
        Me._SenseRangeNumeric.TabIndex = 33
        Me._SenseRangeNumeric.Value = New Decimal(New Integer() {105, 0, 0, 196608})
        '
        '_NplcNumeric
        '
        Me._NplcNumeric.DecimalPlaces = 3
        Me._NplcNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._NplcNumeric.Location = New System.Drawing.Point(119, 179)
        Me._NplcNumeric.Maximum = New Decimal(New Integer() {50, 0, 0, 0})
        Me._NplcNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 131072})
        Me._NplcNumeric.Name = "_NplcNumeric"
        Me._NplcNumeric.Size = New System.Drawing.Size(76, 25)
        Me._NplcNumeric.TabIndex = 31
        Me._NplcNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_SenseRangeNumericLabel
        '
        Me._SenseRangeNumericLabel.AutoSize = True
        Me._SenseRangeNumericLabel.Location = New System.Drawing.Point(48, 153)
        Me._SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel"
        Me._SenseRangeNumericLabel.Size = New System.Drawing.Size(68, 17)
        Me._SenseRangeNumericLabel.TabIndex = 32
        Me._SenseRangeNumericLabel.Text = "Range [V]:"
        Me._SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_NplcNumericLabel
        '
        Me._NplcNumericLabel.AutoSize = True
        Me._NplcNumericLabel.Location = New System.Drawing.Point(11, 183)
        Me._NplcNumericLabel.Name = "_NplcNumericLabel"
        Me._NplcNumericLabel.Size = New System.Drawing.Size(105, 17)
        Me._NplcNumericLabel.TabIndex = 30
        Me._NplcNumericLabel.Text = "Aperture [NPLC]:"
        Me._NplcNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SenseFunctionComboBox
        '
        Me._SenseFunctionComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SenseFunctionComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._SenseFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
        Me._SenseFunctionComboBox.Location = New System.Drawing.Point(119, 119)
        Me._SenseFunctionComboBox.Name = "_SenseFunctionComboBox"
        Me._SenseFunctionComboBox.Size = New System.Drawing.Size(187, 25)
        Me._SenseFunctionComboBox.TabIndex = 29
        '
        '_SenseFunctionComboBoxLabel
        '
        Me._SenseFunctionComboBoxLabel.AutoSize = True
        Me._SenseFunctionComboBoxLabel.Location = New System.Drawing.Point(60, 123)
        Me._SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel"
        Me._SenseFunctionComboBoxLabel.Size = New System.Drawing.Size(59, 17)
        Me._SenseFunctionComboBoxLabel.TabIndex = 28
        Me._SenseFunctionComboBoxLabel.Text = "Function:"
        Me._SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SenseAutoRangeToggle
        '
        Me._SenseAutoRangeToggle.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._SenseAutoRangeToggle.Location = New System.Drawing.Point(201, 152)
        Me._SenseAutoRangeToggle.Name = "_SenseAutoRangeToggle"
        Me._SenseAutoRangeToggle.Size = New System.Drawing.Size(103, 21)
        Me._SenseAutoRangeToggle.TabIndex = 34
        Me._SenseAutoRangeToggle.Text = "Auto Range"
        Me._SenseAutoRangeToggle.UseVisualStyleBackColor = True
        '
        '_ApplySenseSettingsButton
        '
        Me._ApplySenseSettingsButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplySenseSettingsButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ApplySenseSettingsButton.Location = New System.Drawing.Point(293, 176)
        Me._ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton"
        Me._ApplySenseSettingsButton.Size = New System.Drawing.Size(58, 30)
        Me._ApplySenseSettingsButton.TabIndex = 35
        Me._ApplySenseSettingsButton.Text = "&Apply"
        Me._ApplySenseSettingsButton.UseVisualStyleBackColor = True
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._EnabledSenseFunctionsListBoxLabel)
        Me._Panel.Controls.Add(Me._EnabledSenseFunctionsListBox)
        Me._Panel.Controls.Add(Me._ApplySenseFunctionButton)
        Me._Panel.Controls.Add(Me._ConcurrentSenseCheckBox)
        Me._Panel.Controls.Add(Me._FourWireSenseCheckBox)
        Me._Panel.Controls.Add(Me._SenseRangeNumeric)
        Me._Panel.Controls.Add(Me._NplcNumeric)
        Me._Panel.Controls.Add(Me._SenseRangeNumericLabel)
        Me._Panel.Controls.Add(Me._NplcNumericLabel)
        Me._Panel.Controls.Add(Me._SenseFunctionComboBox)
        Me._Panel.Controls.Add(Me._SenseFunctionComboBoxLabel)
        Me._Panel.Controls.Add(Me._SenseAutoRangeToggle)
        Me._Panel.Controls.Add(Me._ApplySenseSettingsButton)
        Me._Panel.Location = New System.Drawing.Point(8, 51)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(363, 219)
        Me._Panel.TabIndex = 0
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(379, 322)
        Me._Layout.TabIndex = 1
        '
        'SenseView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "SenseView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        CType(Me._SenseRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._NplcNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me._Layout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _EnabledSenseFunctionsListBoxLabel As Windows.Forms.Label
    Private WithEvents _EnabledSenseFunctionsListBox As Windows.Forms.CheckedListBox
    Private WithEvents _ApplySenseFunctionButton As Windows.Forms.Button
    Private WithEvents _ConcurrentSenseCheckBox As Windows.Forms.CheckBox
    Private WithEvents _FourWireSenseCheckBox As Windows.Forms.CheckBox
    Private WithEvents _SenseRangeNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _NplcNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SenseRangeNumericLabel As Windows.Forms.Label
    Private WithEvents _NplcNumericLabel As Windows.Forms.Label
    Private WithEvents _SenseFunctionComboBox As Windows.Forms.ComboBox
    Private WithEvents _SenseFunctionComboBoxLabel As Windows.Forms.Label
    Private WithEvents _SenseAutoRangeToggle As Windows.Forms.CheckBox
    Private WithEvents _ApplySenseSettingsButton As Windows.Forms.Button
End Class
