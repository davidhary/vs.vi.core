Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.Primitives
Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Source view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SourceView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SourceView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="SourceView"/>. </returns>
    Public Shared Function Create() As SourceView
        Dim view As SourceView = Nothing
        Try
            view = New SourceView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K2400Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K2400Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K2400Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindSourceSubsystem(value)
        Me.BindSystemSubsystem(value)
        Me.BindTriggerSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K2400Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SOURCE "

    ''' <summary> Gets the Source subsystem. </summary>
    ''' <value> The Source subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SourceSubsystem As VI.K2400.SourceSubsystem

    ''' <summary> Bind Source subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSourceSubsystem(ByVal device As K2400.K2400Device)
        If Me.SourceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SourceSubsystem)
            Me._SourceSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SourceSubsystem = device.SourceSubsystem
            Me.BindSubsystem(True, Me.SourceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SourceSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SourceSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SourceSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Source subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SourceSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.SourceSubsystem.Delay)
                If subsystem.Delay.HasValue Then Me._SourceDelayNumeric.ValueSetter(subsystem.Delay.Value.TotalMilliseconds)
            Case NameOf(VI.K2400.SourceSubsystem.AutoClearEnabled)
                If subsystem.AutoClearEnabled.HasValue Then
                    Me._SourceAutoClearCheckBox.Checked = subsystem.AutoClearEnabled.Value
                End If
            Case NameOf(VI.K2400.SourceSubsystem.Delay)
                If subsystem.Delay.HasValue Then
                    Me._SourceDelayNumeric.ValueSetter(subsystem.Delay.Value.Ticks / TimeSpan.TicksPerMillisecond)
                End If
            Case NameOf(VI.K2400.SourceSubsystem.FunctionMode)
                If subsystem.FunctionMode.HasValue Then
                    Me._SourceFunctionComboBox.SelectedItem = subsystem.FunctionMode.Value.ValueDescriptionPair
                    Me.OnSourceFunctionChanged(subsystem)
                End If

            Case NameOf(VI.K2400.SourceSubsystem.SupportedFunctionModes)
                Me.OnSupportedFunctionModesChanged(subsystem)
        End Select
    End Sub

    ''' <summary> Source subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inSourceion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SourceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SourceSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SourceSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SourceSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Handles the supported function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnSupportedFunctionModesChanged(ByVal subsystem As SourceSubsystem)
        If subsystem IsNot Nothing AndAlso subsystem.SupportedFunctionModes <> VI.SourceFunctionModes.None Then
            Me._SourceFunctionComboBox.DataSource = Nothing
            Me._SourceFunctionComboBox.Items.Clear()
            Me._SourceFunctionComboBox.DataSource = GetType(VI.SourceFunctionModes).EnumValues.IncludeFilter(subsystem.SupportedFunctionModes).ValueDescriptionPairs.ToList
            Me._SourceFunctionComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            Me._SourceFunctionComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If Me._SourceFunctionComboBox.Items.Count > 0 Then
                Me._SourceFunctionComboBox.SelectedItem = VI.SourceFunctionModes.Voltage.ValueDescriptionPair()
            End If
        End If
    End Sub

    ''' <summary> Gets the selected source function mode. </summary>
    ''' <value> The selected source function mode. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedSourceFunctionMode() As VI.SourceFunctionModes
        Get
            Return CType(CType(Me._SourceFunctionComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.SourceFunctionModes)
        End Get
    End Property

    ''' <summary> Executes the 'source function changed' action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnSourceFunctionChanged(ByVal subsystem As SourceSubsystem)
        If subsystem Is Nothing Then Return
        Dim unit As Arebis.TypedUnits.Unit = Arebis.StandardUnits.ElectricUnits.Volt
        Dim range As New RangeR(-1, 1)
        Dim decimalPlaces As Integer = 3
        Dim increment As Decimal = 0
        Dim limitUnit As Arebis.TypedUnits.Unit = Arebis.StandardUnits.ElectricUnits.Ampere
        Dim limitRange As New RangeR(-1, 1)
        Dim limitDecimalPlaces As Integer = 3
        Dim limitIncrement As Decimal = 0
        Select Case subsystem.FunctionMode.Value
            Case VI.SourceFunctionModes.Current
                unit = Arebis.StandardUnits.ElectricUnits.Ampere
                range = Me.Device.SourceCurrentSubsystem.FunctionRange
                decimalPlaces = 6
                increment = 0.0001D
                limitUnit = Arebis.StandardUnits.ElectricUnits.Volt
                limitRange = Me.Device.SourceVoltageSubsystem.FunctionRange
                limitDecimalPlaces = 3
                limitIncrement = 1
            Case VI.SourceFunctionModes.Voltage
                range = Me.Device.SourceVoltageSubsystem.FunctionRange
                decimalPlaces = 3
                limitRange = Me.Device.SourceCurrentSubsystem.FunctionRange
                limitDecimalPlaces = 6
                increment = 1D
                limitIncrement = 0.0001D
        End Select
        Me._SourceLevelNumeric.Maximum = CDec(range.Max)
        Me._SourceLevelNumeric.Minimum = CDec(range.Min)
        Me._SourceLevelNumeric.DecimalPlaces = decimalPlaces
        Me._SourceLevelNumeric.Increment = increment

        Me._SourceRangeNumeric.Maximum = CDec(range.Max)
        Me._SourceRangeNumeric.Minimum = CDec(range.Min)
        Me._SourceRangeNumeric.DecimalPlaces = decimalPlaces
        Me._SourceRangeNumeric.Increment = increment

        Me._SourceLimitNumeric.Maximum = CDec(limitRange.Max)
        Me._SourceLimitNumeric.Minimum = CDec(limitRange.Min)
        Me._SourceLimitNumeric.DecimalPlaces = limitDecimalPlaces
        Me._SourceLimitNumeric.Increment = limitIncrement

        Me._SourceLevelNumericLabel.Text = $"Level [{unit.Symbol}]"
        Me._SourceLevelNumericLabel.Left = Me._SourceLevelNumeric.Left - Me._SourceLevelNumericLabel.Width
        Me._SourceRangeNumericLabel.Text = $"Range [{unit.Symbol}]"
        Me._SourceRangeNumericLabel.Left = Me._SourceRangeNumeric.Left - Me._SourceRangeNumericLabel.Width
        Me._SourceLimitNumericLabel.Text = $"Limit [{limitUnit.Symbol}]"
        Me._SourceLimitNumericLabel.Left = Me._SourceLimitNumeric.Left - Me._SourceLimitNumericLabel.Width

        Select Case subsystem.FunctionMode.Value
            Case VI.SourceFunctionModes.Current
                Me.BindSourceVoltageSubsystem(Nothing)
                Me.BindSourceCurrentSubsystem(Me.Device)
            Case VI.SourceFunctionModes.Voltage
                Me.BindSourceCurrentSubsystem(Nothing)
                Me.BindSourceVoltageSubsystem(Me.Device)
        End Select
    End Sub

#End Region

#Region " SOURCE CURRENT "

    ''' <summary> Gets or sets the Source Current subsystem. </summary>
    ''' <value> The Source Current subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SourceCurrentSubsystem As VI.K2400.SourceCurrentSubsystem

    ''' <summary> Bind Source Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSourceCurrentSubsystem(ByVal device As K2400.K2400Device)
        If Me.SourceCurrentSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SourceCurrentSubsystem)
            Me._SourceCurrentSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SourceCurrentSubsystem = device.SourceCurrentSubsystem
            Me.BindSubsystem(True, Me.SourceCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SourceCurrentSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SourceCurrentSubsystemPropertyChanged
            ' must Not read setting when biding because the instrument may be locked Or in a trigger mode
            ' The bound values should be sent when binding Or when applying propert change.
            ' TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            ' subsystem.QueryLevel()
            ' subsystem.QueryRange()
            ' subsystem.QueryProtectionLevel()
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SourceCurrentSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Source Current subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SourceCurrentSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If Me.Device.SourceSubsystem.FunctionMode.GetValueOrDefault(VI.SourceFunctionModes.None) <> VI.SourceFunctionModes.Current Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.SourceCurrentSubsystem.Level)
                If subsystem.Level.HasValue Then Me._SourceLevelNumeric.ValueSetter(subsystem.Level.Value)
            Case NameOf(VI.K2400.SourceCurrentSubsystem.Range)
                If subsystem.Range.HasValue Then Me._SourceRangeNumeric.ValueSetter(subsystem.Range.Value)
        End Select
    End Sub

    ''' <summary> Source Current subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source Current of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SourceCurrentSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SourceCurrentSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SourceCurrentSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SourceCurrentSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SOURCE VOLTAGE "

    ''' <summary> Gets or sets the Source Voltage subsystem. </summary>
    ''' <value> The Source Voltage subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SourceVoltageSubsystem As VI.K2400.SourceVoltageSubsystem

    ''' <summary> Bind Source Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSourceVoltageSubsystem(ByVal device As K2400.K2400Device)
        If Me.SourceVoltageSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SourceVoltageSubsystem)
            Me._SourceVoltageSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SourceVoltageSubsystem = device.SourceVoltageSubsystem
            Me.BindSubsystem(True, Me.SourceVoltageSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SourceVoltageSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SourceVoltageSubsystemPropertyChanged
            ' must Not read setting when biding because the instrument may be locked Or in a trigger mode
            ' The bound values should be sent when binding Or when applying propert change.
            ' TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            ' subsystem.QueryLevel()
            ' subsystem.QueryRange()
            ' subsystem.QueryProtectionLevel()
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SourceVoltageSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Source Voltage subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SourceVoltageSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If Me.Device.SourceSubsystem.FunctionMode.GetValueOrDefault(VI.SourceFunctionModes.None) <> VI.SourceFunctionModes.Voltage Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.SourceVoltageSubsystem.Level)
                If subsystem.Level.HasValue Then Me._SourceLevelNumeric.ValueSetter(subsystem.Level.Value)
            Case NameOf(VI.K2400.SourceVoltageSubsystem.Range)
                If subsystem.Range.HasValue Then Me._SourceRangeNumeric.ValueSetter(subsystem.Range.Value)
        End Select
    End Sub

    ''' <summary> Source Voltage subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source Voltage of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SourceVoltageSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SourceVoltageSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SourceVoltageSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SourceVoltageSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System subsystem. </summary>
    ''' <value> The System subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SystemSubsystem As VI.K2400.SystemSubsystem

    ''' <summary> Bind System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSystemSubsystem(ByVal device As K2400.K2400Device)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SystemSubsystem = device.SystemSubsystem
            Me.BindSubsystem(True, Me.SystemSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SystemSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Contact check enabled menu item check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ContactCheckEnabledMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _ContactCheckEnabledCheckBox.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.InfoProvider.Clear()
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying enabling contact check"
            If Not Me.Device.SystemSubsystem.ContactCheckEnabled.HasValue OrElse Me.Device.SystemSubsystem.ContactCheckEnabled.Value <> menuItem.Checked Then
                Me._ContactCheckEnabledCheckBox.CheckState = Me.Device.SystemSubsystem.ContactCheckEnabled.ToCheckState
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handle the System subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.SystemSubsystem.ContactCheckEnabled)
                Me._ContactCheckEnabledCheckBox.CheckState = subsystem.ContactCheckEnabled.ToCheckState
            Case NameOf(VI.K2400.SystemSubsystem.ContactCheckSupported)
                Me._ContactCheckEnabledCheckBox.Enabled = subsystem.ContactCheckSupported.GetValueOrDefault(False)
        End Select
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> System subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return

        Dim activity As String = $"handling {NameOf(SystemSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SystemSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets or sets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As VI.K2400.TriggerSubsystem

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindTriggerSubsystem(ByVal device As K2400.K2400Device)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._TriggerSubsystem = device.TriggerSubsystem
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.TriggerSubsystem.Delay)
                If subsystem.Delay.HasValue Then Me._TriggerDelayNumeric.ValueSetter(subsystem.Delay.Value.TotalMilliseconds)
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inTriggerion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary> Source automatic clear check box check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SourceAutoClearCheckBox_CheckStateChanged(sender As Object, e As EventArgs) Handles _SourceAutoClearCheckBox.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.InfoProvider.Clear()
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} setting output auto on mode"
            If Not Me.Device.SourceSubsystem.AutoClearEnabled.HasValue OrElse
                Me.Device.SourceSubsystem.AutoClearEnabled.Value <> menuItem.Checked Then
                Me.Device.SourceSubsystem.ApplyAutoClearEnabled(menuItem.Checked)
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Applies the source function button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySourceFunctionButton_Click(sender As Object, e As EventArgs) Handles _ApplySourceFunctionButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying source function"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.ClearExecutionState()

            ' make sure output is off.
            Me.Device.OutputSubsystem.WriteOutputOnState(False)

            Me.Device.SourceSubsystem.ApplyFunctionMode(Me.SelectedSourceFunctionMode)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the source setting button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySourceSettingButton_Click(sender As Object, e As EventArgs) Handles _ApplySourceSettingButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying source settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.ClearExecutionState()

            ' make sure output is off.
            Me.Device.OutputSubsystem.WriteOutputOnState(False)

            ' get the delay time
            If Me._TriggerDelayNumeric.Value >= 0 Then
                Me.Device.TriggerSubsystem.ApplyDelay(TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * Me._TriggerDelayNumeric.Value)))
            Else
                Me.Device.TriggerSubsystem.ApplyAutoDelayEnabled(True)
            End If

            Me.Device.SourceSubsystem.ApplyDelay(TimeSpan.FromMilliseconds(Me._SourceDelayNumeric.Value))
            Select Case Me.Device.SourceSubsystem.FunctionMode.Value
                Case VI.SourceFunctionModes.Current
                    Me.Device.SourceCurrentSubsystem.ApplyRange(1.1 * Me._SourceLevelNumeric.Value)
                    Me.Device.SourceCurrentSubsystem.ApplyLevel(Me._SourceLevelNumeric.Value)

                    Me.Device.SenseVoltageSubsystem.ApplyProtectionLevel(Me._SourceLimitNumeric.Value)
                Case VI.SourceFunctionModes.Voltage

                    Me.Device.SourceVoltageSubsystem.ApplyRange(1.1 * Me._SourceLevelNumeric.Value)
                    Me.Device.SourceVoltageSubsystem.ApplyLevel(Me._SourceLevelNumeric.Value)

                    Me.Device.SenseCurrentSubsystem.ApplyProtectionLevel(Me._SourceLimitNumeric.Value)
            End Select
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
