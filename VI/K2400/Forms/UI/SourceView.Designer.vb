﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SourceView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._OutputAutoOnCheckBox = New System.Windows.Forms.CheckBox()
        Me._SourceAutoClearCheckBox = New System.Windows.Forms.CheckBox()
        Me._ApplySourceFunctionButton = New System.Windows.Forms.Button()
        Me._SourceLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SourceLevelNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SourceRangeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SourceDelayNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SourceDelayTextBoxLabel = New System.Windows.Forms.Label()
        Me._SourceLevelNumericLabel = New System.Windows.Forms.Label()
        Me._ApplySourceSettingButton = New System.Windows.Forms.Button()
        Me._SourceRangeNumericLabel = New System.Windows.Forms.Label()
        Me._SourceFunctionComboBox = New System.Windows.Forms.ComboBox()
        Me._SourceLimitNumericLabel = New System.Windows.Forms.Label()
        Me._SourceFunctionComboBoxLabel = New System.Windows.Forms.Label()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._TriggerDelayNumeric = New System.Windows.Forms.NumericUpDown()
        Me._TriggerDelayNumericLabel = New System.Windows.Forms.Label()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._OutputPanel = New System.Windows.Forms.Panel()
        Me._ContactCheckEnabledCheckBox = New System.Windows.Forms.CheckBox()
        CType(Me._SourceLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SourceLevelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SourceRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SourceDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Panel.SuspendLayout()
        CType(Me._TriggerDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Layout.SuspendLayout()
        Me._OutputPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_OutputAutoOnCheckBox
        '
        Me._OutputAutoOnCheckBox.AutoSize = True
        Me._OutputAutoOnCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._OutputAutoOnCheckBox.Location = New System.Drawing.Point(70, 33)
        Me._OutputAutoOnCheckBox.Name = "_OutputAutoOnCheckBox"
        Me._OutputAutoOnCheckBox.Size = New System.Drawing.Size(122, 21)
        Me._OutputAutoOnCheckBox.TabIndex = 2
        Me._OutputAutoOnCheckBox.Text = "Output Auto On:"
        Me._OutputAutoOnCheckBox.ThreeState = True
        Me._OutputAutoOnCheckBox.UseVisualStyleBackColor = True
        '
        '_SourceAutoClearCheckBox
        '
        Me._SourceAutoClearCheckBox.AutoSize = True
        Me._SourceAutoClearCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._SourceAutoClearCheckBox.Location = New System.Drawing.Point(38, 187)
        Me._SourceAutoClearCheckBox.Name = "_SourceAutoClearCheckBox"
        Me._SourceAutoClearCheckBox.Size = New System.Drawing.Size(91, 21)
        Me._SourceAutoClearCheckBox.TabIndex = 55
        Me._SourceAutoClearCheckBox.Text = "Auto Clear:"
        Me._SourceAutoClearCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._SourceAutoClearCheckBox.UseVisualStyleBackColor = True
        '
        '_ApplySourceFunctionButton
        '
        Me._ApplySourceFunctionButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplySourceFunctionButton.Location = New System.Drawing.Point(293, 5)
        Me._ApplySourceFunctionButton.Name = "_ApplySourceFunctionButton"
        Me._ApplySourceFunctionButton.Size = New System.Drawing.Size(61, 30)
        Me._ApplySourceFunctionButton.TabIndex = 48
        Me._ApplySourceFunctionButton.Text = "Apply"
        Me._ApplySourceFunctionButton.UseVisualStyleBackColor = True
        '
        '_SourceLimitNumeric
        '
        Me._SourceLimitNumeric.DecimalPlaces = 4
        Me._SourceLimitNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SourceLimitNumeric.Location = New System.Drawing.Point(115, 126)
        Me._SourceLimitNumeric.Name = "_SourceLimitNumeric"
        Me._SourceLimitNumeric.Size = New System.Drawing.Size(77, 25)
        Me._SourceLimitNumeric.TabIndex = 47
        '
        '_SourceLevelNumeric
        '
        Me._SourceLevelNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SourceLevelNumeric.Location = New System.Drawing.Point(115, 96)
        Me._SourceLevelNumeric.Maximum = New Decimal(New Integer() {1100, 0, 0, 0})
        Me._SourceLevelNumeric.Name = "_SourceLevelNumeric"
        Me._SourceLevelNumeric.Size = New System.Drawing.Size(77, 25)
        Me._SourceLevelNumeric.TabIndex = 45
        '
        '_SourceRangeNumeric
        '
        Me._SourceRangeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SourceRangeNumeric.Location = New System.Drawing.Point(115, 67)
        Me._SourceRangeNumeric.Maximum = New Decimal(New Integer() {1100, 0, 0, 0})
        Me._SourceRangeNumeric.Name = "_SourceRangeNumeric"
        Me._SourceRangeNumeric.Size = New System.Drawing.Size(77, 25)
        Me._SourceRangeNumeric.TabIndex = 46
        '
        '_SourceDelayNumeric
        '
        Me._SourceDelayNumeric.DecimalPlaces = 2
        Me._SourceDelayNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SourceDelayNumeric.Location = New System.Drawing.Point(115, 38)
        Me._SourceDelayNumeric.Name = "_SourceDelayNumeric"
        Me._SourceDelayNumeric.Size = New System.Drawing.Size(77, 25)
        Me._SourceDelayNumeric.TabIndex = 44
        '
        '_SourceDelayTextBoxLabel
        '
        Me._SourceDelayTextBoxLabel.AutoSize = True
        Me._SourceDelayTextBoxLabel.Location = New System.Drawing.Point(40, 42)
        Me._SourceDelayTextBoxLabel.Name = "_SourceDelayTextBoxLabel"
        Me._SourceDelayTextBoxLabel.Size = New System.Drawing.Size(72, 17)
        Me._SourceDelayTextBoxLabel.TabIndex = 43
        Me._SourceDelayTextBoxLabel.Text = "Delay [ms]:"
        Me._SourceDelayTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SourceLevelNumericLabel
        '
        Me._SourceLevelNumericLabel.AutoSize = True
        Me._SourceLevelNumericLabel.Location = New System.Drawing.Point(52, 100)
        Me._SourceLevelNumericLabel.Name = "_SourceLevelNumericLabel"
        Me._SourceLevelNumericLabel.Size = New System.Drawing.Size(60, 17)
        Me._SourceLevelNumericLabel.TabIndex = 37
        Me._SourceLevelNumericLabel.Text = "Level [V]:"
        Me._SourceLevelNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ApplySourceSettingButton
        '
        Me._ApplySourceSettingButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplySourceSettingButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ApplySourceSettingButton.Location = New System.Drawing.Point(293, 176)
        Me._ApplySourceSettingButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._ApplySourceSettingButton.Name = "_ApplySourceSettingButton"
        Me._ApplySourceSettingButton.Size = New System.Drawing.Size(58, 30)
        Me._ApplySourceSettingButton.TabIndex = 42
        Me._ApplySourceSettingButton.Text = "&Apply"
        Me._ApplySourceSettingButton.UseVisualStyleBackColor = True
        '
        '_SourceRangeNumericLabel
        '
        Me._SourceRangeNumericLabel.AutoSize = True
        Me._SourceRangeNumericLabel.Location = New System.Drawing.Point(44, 71)
        Me._SourceRangeNumericLabel.Name = "_SourceRangeNumericLabel"
        Me._SourceRangeNumericLabel.Size = New System.Drawing.Size(68, 17)
        Me._SourceRangeNumericLabel.TabIndex = 38
        Me._SourceRangeNumericLabel.Text = "Range [V]:"
        Me._SourceRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SourceFunctionComboBox
        '
        Me._SourceFunctionComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SourceFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SourceFunctionComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SourceFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
        Me._SourceFunctionComboBox.Location = New System.Drawing.Point(115, 7)
        Me._SourceFunctionComboBox.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._SourceFunctionComboBox.Name = "_SourceFunctionComboBox"
        Me._SourceFunctionComboBox.Size = New System.Drawing.Size(173, 25)
        Me._SourceFunctionComboBox.TabIndex = 41
        '
        '_SourceLimitNumericLabel
        '
        Me._SourceLimitNumericLabel.AutoSize = True
        Me._SourceLimitNumericLabel.Location = New System.Drawing.Point(54, 130)
        Me._SourceLimitNumericLabel.Name = "_SourceLimitNumericLabel"
        Me._SourceLimitNumericLabel.Size = New System.Drawing.Size(58, 17)
        Me._SourceLimitNumericLabel.TabIndex = 39
        Me._SourceLimitNumericLabel.Text = "Limit [A]:"
        Me._SourceLimitNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SourceFunctionComboBoxLabel
        '
        Me._SourceFunctionComboBoxLabel.AutoSize = True
        Me._SourceFunctionComboBoxLabel.Location = New System.Drawing.Point(53, 11)
        Me._SourceFunctionComboBoxLabel.Name = "_SourceFunctionComboBoxLabel"
        Me._SourceFunctionComboBoxLabel.Size = New System.Drawing.Size(59, 17)
        Me._SourceFunctionComboBoxLabel.TabIndex = 40
        Me._SourceFunctionComboBoxLabel.Text = "Function:"
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._SourceAutoClearCheckBox)
        Me._Panel.Controls.Add(Me._TriggerDelayNumeric)
        Me._Panel.Controls.Add(Me._TriggerDelayNumericLabel)
        Me._Panel.Controls.Add(Me._ApplySourceFunctionButton)
        Me._Panel.Controls.Add(Me._SourceLimitNumeric)
        Me._Panel.Controls.Add(Me._SourceLevelNumeric)
        Me._Panel.Controls.Add(Me._SourceRangeNumeric)
        Me._Panel.Controls.Add(Me._SourceDelayNumeric)
        Me._Panel.Controls.Add(Me._SourceDelayTextBoxLabel)
        Me._Panel.Controls.Add(Me._SourceLevelNumericLabel)
        Me._Panel.Controls.Add(Me._ApplySourceSettingButton)
        Me._Panel.Controls.Add(Me._SourceRangeNumericLabel)
        Me._Panel.Controls.Add(Me._SourceFunctionComboBox)
        Me._Panel.Controls.Add(Me._SourceLimitNumericLabel)
        Me._Panel.Controls.Add(Me._SourceFunctionComboBoxLabel)
        Me._Panel.Location = New System.Drawing.Point(8, 15)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(363, 216)
        Me._Panel.TabIndex = 0
        '
        '_TriggerDelayNumeric
        '
        Me._TriggerDelayNumeric.DecimalPlaces = 3
        Me._TriggerDelayNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerDelayNumeric.Location = New System.Drawing.Point(115, 156)
        Me._TriggerDelayNumeric.Name = "_TriggerDelayNumeric"
        Me._TriggerDelayNumeric.Size = New System.Drawing.Size(56, 25)
        Me._TriggerDelayNumeric.TabIndex = 54
        '
        '_TriggerDelayNumericLabel
        '
        Me._TriggerDelayNumericLabel.AutoSize = True
        Me._TriggerDelayNumericLabel.Location = New System.Drawing.Point(5, 160)
        Me._TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel"
        Me._TriggerDelayNumericLabel.Size = New System.Drawing.Size(107, 17)
        Me._TriggerDelayNumericLabel.TabIndex = 53
        Me._TriggerDelayNumericLabel.Text = "Trigger Delay [s]:"
        Me._TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Controls.Add(Me._OutputPanel, 1, 2)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 4
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me._Layout.Size = New System.Drawing.Size(379, 322)
        Me._Layout.TabIndex = 1
        '
        '_OutputPanel
        '
        Me._OutputPanel.Controls.Add(Me._OutputAutoOnCheckBox)
        Me._OutputPanel.Controls.Add(Me._ContactCheckEnabledCheckBox)
        Me._OutputPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me._OutputPanel.Location = New System.Drawing.Point(8, 237)
        Me._OutputPanel.Name = "_OutputPanel"
        Me._OutputPanel.Size = New System.Drawing.Size(363, 62)
        Me._OutputPanel.TabIndex = 1
        '
        '_ContactCheckEnabledCheckBox
        '
        Me._ContactCheckEnabledCheckBox.AutoSize = True
        Me._ContactCheckEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContactCheckEnabledCheckBox.Location = New System.Drawing.Point(29, 9)
        Me._ContactCheckEnabledCheckBox.Name = "_ContactCheckEnabledCheckBox"
        Me._ContactCheckEnabledCheckBox.Size = New System.Drawing.Size(163, 21)
        Me._ContactCheckEnabledCheckBox.TabIndex = 0
        Me._ContactCheckEnabledCheckBox.Text = "Contact Check Enabled:"
        Me._ContactCheckEnabledCheckBox.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContactCheckEnabledCheckBox.ThreeState = True
        Me._ContactCheckEnabledCheckBox.UseVisualStyleBackColor = True
        '
        'SourceView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "SourceView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        CType(Me._SourceLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SourceLevelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SourceRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SourceDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        CType(Me._TriggerDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._Layout.ResumeLayout(False)
        Me._OutputPanel.ResumeLayout(False)
        Me._OutputPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _ApplySourceFunctionButton As Windows.Forms.Button
    Private WithEvents _SourceLimitNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SourceLevelNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SourceRangeNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SourceDelayNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SourceDelayTextBoxLabel As Windows.Forms.Label
    Private WithEvents _SourceLevelNumericLabel As Windows.Forms.Label
    Private WithEvents _ApplySourceSettingButton As Windows.Forms.Button
    Private WithEvents _SourceRangeNumericLabel As Windows.Forms.Label
    Private WithEvents _SourceFunctionComboBox As Windows.Forms.ComboBox
    Private WithEvents _SourceLimitNumericLabel As Windows.Forms.Label
    Private WithEvents _SourceFunctionComboBoxLabel As Windows.Forms.Label
    Private WithEvents _TriggerDelayNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _TriggerDelayNumericLabel As Windows.Forms.Label
    Private WithEvents _OutputPanel As Windows.Forms.Panel
    Private WithEvents _OutputAutoOnCheckBox As Windows.Forms.CheckBox
    Private WithEvents _ContactCheckEnabledCheckBox As Windows.Forms.CheckBox
    Private WithEvents _SourceAutoClearCheckBox As Windows.Forms.CheckBox
End Class
