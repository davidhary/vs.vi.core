<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class HipotView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(HipotView))
        Me._Tabs = New System.Windows.Forms.TabControl()
        Me._HipotTabPage = New System.Windows.Forms.TabPage()
        Me._HipotLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._HipotGroupBox = New System.Windows.Forms.GroupBox()
        Me._ContactCheckToggle = New System.Windows.Forms.CheckBox()
        Me._ApplyHipotSettingsButton = New System.Windows.Forms.Button()
        Me._CurrentLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._CurrentLimitNumericLabel = New System.Windows.Forms.Label()
        Me._VoltageLevelNumericLabel = New System.Windows.Forms.Label()
        Me._VoltageLevelNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ResistanceRangeNumericLabel = New System.Windows.Forms.Label()
        Me._ResistanceLowLimitNumericLabel = New System.Windows.Forms.Label()
        Me._ResistanceRangeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ResistanceLowLimitNumeric = New System.Windows.Forms.NumericUpDown()
        Me._DwellTimeNumericLabel = New System.Windows.Forms.Label()
        Me._DwellTimeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ApertureNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ApertureNumericLabel = New System.Windows.Forms.Label()
        Me._StobeTabPage = New System.Windows.Forms.TabPage()
        Me._BinningLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._TriggerToolStrip = New System.Windows.Forms.ToolStrip()
        Me._AwaitTriggerToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._WaitHourglassLabel = New System.Windows.Forms.ToolStripLabel()
        Me._AssertTriggerToolStripButton = New System.Windows.Forms.ToolStripButton()
        Me._TriggerActionToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._BinningGroupBox = New System.Windows.Forms.GroupBox()
        Me._ArmComboBoxLabel = New System.Windows.Forms.Label()
        Me._ArmSourceComboBox = New System.Windows.Forms.ComboBox()
        Me._ApplySotSettingsButton = New System.Windows.Forms.Button()
        Me._ContactCheckSupportLabel = New System.Windows.Forms.Label()
        Me._FailBitPatternNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PassBitPatternNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ContactCheckBitPatternNumeric = New System.Windows.Forms.NumericUpDown()
        Me._FailBitPatternNumericLabel = New System.Windows.Forms.Label()
        Me._PassBitPatternNumericLabel = New System.Windows.Forms.Label()
        Me._ContactCheckBitPatternNumericLabel = New System.Windows.Forms.Label()
        Me._EotStrobeDurationNumericLabel = New System.Windows.Forms.Label()
        Me._EotStrobeDurationNumeric = New System.Windows.Forms.NumericUpDown()
        Me._TriggerDelayNumeric = New System.Windows.Forms.NumericUpDown()
        Me._TriggerDelayNumericLabel = New System.Windows.Forms.Label()
        Me._Tabs.SuspendLayout()
        Me._HipotTabPage.SuspendLayout()
        Me._HipotLayout.SuspendLayout()
        Me._HipotGroupBox.SuspendLayout()
        CType(Me._CurrentLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._VoltageLevelNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ResistanceRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ResistanceLowLimitNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._DwellTimeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ApertureNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._StobeTabPage.SuspendLayout()
        Me._BinningLayout.SuspendLayout()
        Me._TriggerToolStrip.SuspendLayout()
        Me._BinningGroupBox.SuspendLayout()
        CType(Me._FailBitPatternNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._PassBitPatternNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._ContactCheckBitPatternNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._EotStrobeDurationNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._TriggerDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Tabs
        '
        Me._Tabs.Controls.Add(Me._HipotTabPage)
        Me._Tabs.Controls.Add(Me._StobeTabPage)

        Me._Tabs.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Tabs.Location = New System.Drawing.Point(0, 0)
        Me._Tabs.Name = "_Tabs"
        Me._Tabs.SelectedIndex = 0
        Me._Tabs.Size = New System.Drawing.Size(383, 326)
        Me._Tabs.TabIndex = 0
        '
        '_HipotTabPage
        '
        Me._HipotTabPage.Controls.Add(Me._HipotLayout)
        Me._HipotTabPage.Location = New System.Drawing.Point(4, 26)
        Me._HipotTabPage.Name = "_HipotTabPage"
        Me._HipotTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._HipotTabPage.Size = New System.Drawing.Size(375, 296)
        Me._HipotTabPage.TabIndex = 0
        Me._HipotTabPage.Text = "Hipot"
        Me._HipotTabPage.UseVisualStyleBackColor = True
        '
        '_HipotLayout
        '
        Me._HipotLayout.ColumnCount = 3
        Me._HipotLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._HipotLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._HipotLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._HipotLayout.Controls.Add(Me._HipotGroupBox, 1, 1)
        Me._HipotLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._HipotLayout.Location = New System.Drawing.Point(3, 3)
        Me._HipotLayout.Name = "_HipotLayout"
        Me._HipotLayout.RowCount = 3
        Me._HipotLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._HipotLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._HipotLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._HipotLayout.Size = New System.Drawing.Size(369, 290)
        Me._HipotLayout.TabIndex = 5
        '
        '_HipotGroupBox
        '
        Me._HipotGroupBox.Controls.Add(Me._ContactCheckToggle)
        Me._HipotGroupBox.Controls.Add(Me._ApplyHipotSettingsButton)
        Me._HipotGroupBox.Controls.Add(Me._CurrentLimitNumeric)
        Me._HipotGroupBox.Controls.Add(Me._CurrentLimitNumericLabel)
        Me._HipotGroupBox.Controls.Add(Me._VoltageLevelNumericLabel)
        Me._HipotGroupBox.Controls.Add(Me._VoltageLevelNumeric)
        Me._HipotGroupBox.Controls.Add(Me._ResistanceRangeNumericLabel)
        Me._HipotGroupBox.Controls.Add(Me._ResistanceLowLimitNumericLabel)
        Me._HipotGroupBox.Controls.Add(Me._ResistanceRangeNumeric)
        Me._HipotGroupBox.Controls.Add(Me._ResistanceLowLimitNumeric)
        Me._HipotGroupBox.Controls.Add(Me._DwellTimeNumericLabel)
        Me._HipotGroupBox.Controls.Add(Me._DwellTimeNumeric)
        Me._HipotGroupBox.Controls.Add(Me._ApertureNumeric)
        Me._HipotGroupBox.Controls.Add(Me._ApertureNumericLabel)
        Me._HipotGroupBox.Location = New System.Drawing.Point(43, 36)
        Me._HipotGroupBox.Name = "_HipotGroupBox"
        Me._HipotGroupBox.Size = New System.Drawing.Size(283, 217)
        Me._HipotGroupBox.TabIndex = 2
        Me._HipotGroupBox.TabStop = False
        Me._HipotGroupBox.Text = "HIPOT SETTINGS"
        '
        '_ContactCheckToggle
        '
        Me._ContactCheckToggle.AutoSize = True
        Me._ContactCheckToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContactCheckToggle.Location = New System.Drawing.Point(19, 187)
        Me._ContactCheckToggle.Name = "_ContactCheckToggle"
        Me._ContactCheckToggle.Size = New System.Drawing.Size(161, 21)
        Me._ContactCheckToggle.TabIndex = 13
        Me._ContactCheckToggle.Text = "Contact check enabled:"
        Me._ContactCheckToggle.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._ContactCheckToggle.UseVisualStyleBackColor = True
        '
        '_ApplyHipotSettingsButton
        '
        Me._ApplyHipotSettingsButton.Location = New System.Drawing.Point(197, 183)
        Me._ApplyHipotSettingsButton.Name = "_ApplyHipotSettingsButton"
        Me._ApplyHipotSettingsButton.Size = New System.Drawing.Size(75, 28)
        Me._ApplyHipotSettingsButton.TabIndex = 12
        Me._ApplyHipotSettingsButton.Text = "Apply"
        Me._ApplyHipotSettingsButton.UseVisualStyleBackColor = True
        '
        '_CurrentLimitNumeric
        '
        Me._CurrentLimitNumeric.DecimalPlaces = 1
        Me._CurrentLimitNumeric.Location = New System.Drawing.Point(191, 102)
        Me._CurrentLimitNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._CurrentLimitNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._CurrentLimitNumeric.Name = "_CurrentLimitNumeric"
        Me._CurrentLimitNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._CurrentLimitNumeric.Size = New System.Drawing.Size(79, 25)
        Me._CurrentLimitNumeric.TabIndex = 7
        Me._CurrentLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._CurrentLimitNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_CurrentLimitNumericLabel
        '
        Me._CurrentLimitNumericLabel.AutoSize = True
        Me._CurrentLimitNumericLabel.Location = New System.Drawing.Point(76, 106)
        Me._CurrentLimitNumericLabel.Name = "_CurrentLimitNumericLabel"
        Me._CurrentLimitNumericLabel.Size = New System.Drawing.Size(112, 17)
        Me._CurrentLimitNumericLabel.TabIndex = 6
        Me._CurrentLimitNumericLabel.Text = "Current Limit [uA]:"
        '
        '_VoltageLevelNumericLabel
        '
        Me._VoltageLevelNumericLabel.AutoSize = True
        Me._VoltageLevelNumericLabel.Location = New System.Drawing.Point(80, 79)
        Me._VoltageLevelNumericLabel.Name = "_VoltageLevelNumericLabel"
        Me._VoltageLevelNumericLabel.Size = New System.Drawing.Size(108, 17)
        Me._VoltageLevelNumericLabel.TabIndex = 4
        Me._VoltageLevelNumericLabel.Text = "Voltage Level [V]:"
        '
        '_VoltageLevelNumeric
        '
        Me._VoltageLevelNumeric.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._VoltageLevelNumeric.Location = New System.Drawing.Point(191, 75)
        Me._VoltageLevelNumeric.Maximum = New Decimal(New Integer() {1100, 0, 0, 0})
        Me._VoltageLevelNumeric.Name = "_VoltageLevelNumeric"
        Me._VoltageLevelNumeric.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._VoltageLevelNumeric.Size = New System.Drawing.Size(79, 25)
        Me._VoltageLevelNumeric.TabIndex = 5
        Me._VoltageLevelNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._VoltageLevelNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_ResistanceRangeNumericLabel
        '
        Me._ResistanceRangeNumericLabel.AutoSize = True
        Me._ResistanceRangeNumericLabel.Location = New System.Drawing.Point(19, 160)
        Me._ResistanceRangeNumericLabel.Name = "_ResistanceRangeNumericLabel"
        Me._ResistanceRangeNumericLabel.Size = New System.Drawing.Size(169, 17)
        Me._ResistanceRangeNumericLabel.TabIndex = 10
        Me._ResistanceRangeNumericLabel.Text = "Resistance Range [M Ohm]:"
        '
        '_ResistanceLowLimitNumericLabel
        '
        Me._ResistanceLowLimitNumericLabel.AutoSize = True
        Me._ResistanceLowLimitNumericLabel.Location = New System.Drawing.Point(5, 133)
        Me._ResistanceLowLimitNumericLabel.Name = "_ResistanceLowLimitNumericLabel"
        Me._ResistanceLowLimitNumericLabel.Size = New System.Drawing.Size(183, 17)
        Me._ResistanceLowLimitNumericLabel.TabIndex = 8
        Me._ResistanceLowLimitNumericLabel.Text = "Resistance low Limit [M Ohm]:"
        '
        '_ResistanceRangeNumeric
        '
        Me._ResistanceRangeNumeric.Increment = New Decimal(New Integer() {1000000, 0, 0, 0})
        Me._ResistanceRangeNumeric.Location = New System.Drawing.Point(191, 156)
        Me._ResistanceRangeNumeric.Maximum = New Decimal(New Integer() {10000, 0, 0, 0})
        Me._ResistanceRangeNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._ResistanceRangeNumeric.Name = "_ResistanceRangeNumeric"
        Me._ResistanceRangeNumeric.Size = New System.Drawing.Size(79, 25)
        Me._ResistanceRangeNumeric.TabIndex = 11
        Me._ResistanceRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ResistanceRangeNumeric.ThousandsSeparator = True
        Me._ResistanceRangeNumeric.Value = New Decimal(New Integer() {1000, 0, 0, 0})
        '
        '_ResistanceLowLimitNumeric
        '
        Me._ResistanceLowLimitNumeric.Increment = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ResistanceLowLimitNumeric.Location = New System.Drawing.Point(191, 129)
        Me._ResistanceLowLimitNumeric.Maximum = New Decimal(New Integer() {1000, 0, 0, 0})
        Me._ResistanceLowLimitNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._ResistanceLowLimitNumeric.Name = "_ResistanceLowLimitNumeric"
        Me._ResistanceLowLimitNumeric.Size = New System.Drawing.Size(79, 25)
        Me._ResistanceLowLimitNumeric.TabIndex = 9
        Me._ResistanceLowLimitNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ResistanceLowLimitNumeric.ThousandsSeparator = True
        Me._ResistanceLowLimitNumeric.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        '_DwellTimeNumericLabel
        '
        Me._DwellTimeNumericLabel.AutoSize = True
        Me._DwellTimeNumericLabel.Location = New System.Drawing.Point(98, 52)
        Me._DwellTimeNumericLabel.Name = "_DwellTimeNumericLabel"
        Me._DwellTimeNumericLabel.Size = New System.Drawing.Size(90, 17)
        Me._DwellTimeNumericLabel.TabIndex = 2
        Me._DwellTimeNumericLabel.Text = "Dwell time [S]:"
        '
        '_DwellTimeNumeric
        '
        Me._DwellTimeNumeric.DecimalPlaces = 2
        Me._DwellTimeNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._DwellTimeNumeric.Location = New System.Drawing.Point(191, 48)
        Me._DwellTimeNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._DwellTimeNumeric.Name = "_DwellTimeNumeric"
        Me._DwellTimeNumeric.Size = New System.Drawing.Size(79, 25)
        Me._DwellTimeNumeric.TabIndex = 3
        Me._DwellTimeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._DwellTimeNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_ApertureNumeric
        '
        Me._ApertureNumeric.DecimalPlaces = 2
        Me._ApertureNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._ApertureNumeric.Location = New System.Drawing.Point(191, 21)
        Me._ApertureNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._ApertureNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 131072})
        Me._ApertureNumeric.Name = "_ApertureNumeric"
        Me._ApertureNumeric.Size = New System.Drawing.Size(79, 25)
        Me._ApertureNumeric.TabIndex = 1
        Me._ApertureNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ApertureNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_ApertureNumericLabel
        '
        Me._ApertureNumericLabel.AutoSize = True
        Me._ApertureNumericLabel.Location = New System.Drawing.Point(86, 25)
        Me._ApertureNumericLabel.Name = "_ApertureNumericLabel"
        Me._ApertureNumericLabel.Size = New System.Drawing.Size(102, 17)
        Me._ApertureNumericLabel.TabIndex = 0
        Me._ApertureNumericLabel.Text = "Aperture [NPLC]"
        '
        '_StobeTabPage
        '
        Me._StobeTabPage.Controls.Add(Me._BinningLayout)
        Me._StobeTabPage.Location = New System.Drawing.Point(4, 26)
        Me._StobeTabPage.Name = "_StobeTabPage"
        Me._StobeTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._StobeTabPage.Size = New System.Drawing.Size(375, 296)
        Me._StobeTabPage.TabIndex = 1
        Me._StobeTabPage.Text = "Strobe"
        Me._StobeTabPage.UseVisualStyleBackColor = True
        '
        '_BinningLayout
        '
        Me._BinningLayout.ColumnCount = 3
        Me._BinningLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._BinningLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._BinningLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._BinningLayout.Controls.Add(Me._TriggerToolStrip, 1, 3)
        Me._BinningLayout.Controls.Add(Me._BinningGroupBox, 1, 1)
        Me._BinningLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._BinningLayout.Location = New System.Drawing.Point(3, 3)
        Me._BinningLayout.Name = "_BinningLayout"
        Me._BinningLayout.RowCount = 5
        Me._BinningLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._BinningLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._BinningLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._BinningLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._BinningLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me._BinningLayout.Size = New System.Drawing.Size(369, 290)
        Me._BinningLayout.TabIndex = 2
        '
        '_TriggerToolStrip
        '
        Me._TriggerToolStrip.Dock = System.Windows.Forms.DockStyle.None
        Me._TriggerToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AwaitTriggerToolStripButton, Me._WaitHourglassLabel, Me._AssertTriggerToolStripButton, Me._TriggerActionToolStripLabel})
        Me._TriggerToolStrip.Location = New System.Drawing.Point(20, 243)
        Me._TriggerToolStrip.Name = "_TriggerToolStrip"
        Me._TriggerToolStrip.Size = New System.Drawing.Size(270, 25)
        Me._TriggerToolStrip.Stretch = True
        Me._TriggerToolStrip.TabIndex = 6
        Me._TriggerToolStrip.Text = "Trigger Tool Strip"
        '
        '_AwaitTriggerToolStripButton
        '
        Me._AwaitTriggerToolStripButton.CheckOnClick = True
        Me._AwaitTriggerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AwaitTriggerToolStripButton.Image = CType(resources.GetObject("_AwaitTriggerToolStripButton.Image"), System.Drawing.Image)
        Me._AwaitTriggerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AwaitTriggerToolStripButton.Name = "_AwaitTriggerToolStripButton"
        Me._AwaitTriggerToolStripButton.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never
        Me._AwaitTriggerToolStripButton.Size = New System.Drawing.Size(34, 22)
        Me._AwaitTriggerToolStripButton.Text = "Arm"
        Me._AwaitTriggerToolStripButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        Me._AwaitTriggerToolStripButton.ToolTipText = "Depress to wait for trigger or release to abort."
        '
        '_WaitHourglassLabel
        '
        Me._WaitHourglassLabel.Name = "_WaitHourglassLabel"
        Me._WaitHourglassLabel.Size = New System.Drawing.Size(20, 22)
        Me._WaitHourglassLabel.Text = "[-]"
        Me._WaitHourglassLabel.ToolTipText = "Waiting for trigger"
        '
        '_AssertTriggerToolStripButton
        '
        Me._AssertTriggerToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._AssertTriggerToolStripButton.Image = CType(resources.GetObject("_AssertTriggerToolStripButton.Image"), System.Drawing.Image)
        Me._AssertTriggerToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._AssertTriggerToolStripButton.Margin = New System.Windows.Forms.Padding(6, 1, 0, 2)
        Me._AssertTriggerToolStripButton.Name = "_AssertTriggerToolStripButton"
        Me._AssertTriggerToolStripButton.Size = New System.Drawing.Size(38, 22)
        Me._AssertTriggerToolStripButton.Text = "*TRG"
        '
        '_TriggerActionToolStripLabel
        '
        Me._TriggerActionToolStripLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TriggerActionToolStripLabel.Name = "_TriggerActionToolStripLabel"
        Me._TriggerActionToolStripLabel.Size = New System.Drawing.Size(160, 22)
        Me._TriggerActionToolStripLabel.Text = "Click wait to 'Arm' for trigger"
        Me._TriggerActionToolStripLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_BinningGroupBox
        '
        Me._BinningGroupBox.Controls.Add(Me._TriggerDelayNumeric)
        Me._BinningGroupBox.Controls.Add(Me._TriggerDelayNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._ArmComboBoxLabel)
        Me._BinningGroupBox.Controls.Add(Me._ArmSourceComboBox)
        Me._BinningGroupBox.Controls.Add(Me._ApplySotSettingsButton)
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckSupportLabel)
        Me._BinningGroupBox.Controls.Add(Me._FailBitPatternNumeric)
        Me._BinningGroupBox.Controls.Add(Me._PassBitPatternNumeric)
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckBitPatternNumeric)
        Me._BinningGroupBox.Controls.Add(Me._FailBitPatternNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._PassBitPatternNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._ContactCheckBitPatternNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._EotStrobeDurationNumericLabel)
        Me._BinningGroupBox.Controls.Add(Me._EotStrobeDurationNumeric)
        Me._BinningGroupBox.Location = New System.Drawing.Point(23, 25)
        Me._BinningGroupBox.Name = "_BinningGroupBox"
        Me._BinningGroupBox.Size = New System.Drawing.Size(323, 193)
        Me._BinningGroupBox.TabIndex = 2
        Me._BinningGroupBox.TabStop = False
        Me._BinningGroupBox.Text = "SOT, EOT, Bin"
        '
        '_ArmComboBoxLabel
        '
        Me._ArmComboBoxLabel.AutoSize = True
        Me._ArmComboBoxLabel.Location = New System.Drawing.Point(73, 23)
        Me._ArmComboBoxLabel.Name = "_ArmComboBoxLabel"
        Me._ArmComboBoxLabel.Size = New System.Drawing.Size(91, 17)
        Me._ArmComboBoxLabel.TabIndex = 0
        Me._ArmComboBoxLabel.Text = "Trigger signal:"
        '
        '_ArmSourceComboBox
        '
        Me._ArmSourceComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._ArmSourceComboBox.FormattingEnabled = True
        Me._ArmSourceComboBox.Location = New System.Drawing.Point(167, 19)
        Me._ArmSourceComboBox.Name = "_ArmSourceComboBox"
        Me._ArmSourceComboBox.Size = New System.Drawing.Size(149, 25)
        Me._ArmSourceComboBox.TabIndex = 1
        '
        '_ApplySotSettingsButton
        '
        Me._ApplySotSettingsButton.Location = New System.Drawing.Point(253, 152)
        Me._ApplySotSettingsButton.Name = "_ApplySotSettingsButton"
        Me._ApplySotSettingsButton.Size = New System.Drawing.Size(57, 28)
        Me._ApplySotSettingsButton.TabIndex = 11
        Me._ApplySotSettingsButton.Text = "Apply"
        Me._ApplySotSettingsButton.UseVisualStyleBackColor = True
        '
        '_ContactCheckSupportLabel
        '
        Me._ContactCheckSupportLabel.AutoSize = True
        Me._ContactCheckSupportLabel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ContactCheckSupportLabel.Location = New System.Drawing.Point(3, 173)
        Me._ContactCheckSupportLabel.Name = "_ContactCheckSupportLabel"
        Me._ContactCheckSupportLabel.Size = New System.Drawing.Size(0, 17)
        Me._ContactCheckSupportLabel.TabIndex = 9
        Me._ContactCheckSupportLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        '_FailBitPatternNumeric
        '
        Me._FailBitPatternNumeric.Location = New System.Drawing.Point(167, 100)
        Me._FailBitPatternNumeric.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me._FailBitPatternNumeric.Name = "_FailBitPatternNumeric"
        Me._FailBitPatternNumeric.Size = New System.Drawing.Size(35, 25)
        Me._FailBitPatternNumeric.TabIndex = 7
        Me._FailBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._FailBitPatternNumeric.Value = New Decimal(New Integer() {4, 0, 0, 0})
        '
        '_PassBitPatternNumeric
        '
        Me._PassBitPatternNumeric.Location = New System.Drawing.Point(167, 73)
        Me._PassBitPatternNumeric.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me._PassBitPatternNumeric.Name = "_PassBitPatternNumeric"
        Me._PassBitPatternNumeric.Size = New System.Drawing.Size(35, 25)
        Me._PassBitPatternNumeric.TabIndex = 5
        Me._PassBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._PassBitPatternNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_ContactCheckBitPatternNumeric
        '
        Me._ContactCheckBitPatternNumeric.Location = New System.Drawing.Point(167, 127)
        Me._ContactCheckBitPatternNumeric.Maximum = New Decimal(New Integer() {7, 0, 0, 0})
        Me._ContactCheckBitPatternNumeric.Name = "_ContactCheckBitPatternNumeric"
        Me._ContactCheckBitPatternNumeric.Size = New System.Drawing.Size(35, 25)
        Me._ContactCheckBitPatternNumeric.TabIndex = 9
        Me._ContactCheckBitPatternNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._ContactCheckBitPatternNumeric.Value = New Decimal(New Integer() {2, 0, 0, 0})
        '
        '_FailBitPatternNumericLabel
        '
        Me._FailBitPatternNumericLabel.AutoSize = True
        Me._FailBitPatternNumericLabel.Location = New System.Drawing.Point(69, 104)
        Me._FailBitPatternNumericLabel.Name = "_FailBitPatternNumericLabel"
        Me._FailBitPatternNumericLabel.Size = New System.Drawing.Size(95, 17)
        Me._FailBitPatternNumericLabel.TabIndex = 6
        Me._FailBitPatternNumericLabel.Text = "Fail bit pattern:"
        Me._FailBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PassBitPatternNumericLabel
        '
        Me._PassBitPatternNumericLabel.AutoSize = True
        Me._PassBitPatternNumericLabel.Location = New System.Drawing.Point(62, 77)
        Me._PassBitPatternNumericLabel.Name = "_PassBitPatternNumericLabel"
        Me._PassBitPatternNumericLabel.Size = New System.Drawing.Size(102, 17)
        Me._PassBitPatternNumericLabel.TabIndex = 4
        Me._PassBitPatternNumericLabel.Text = "Pass bit pattern:"
        Me._PassBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_ContactCheckBitPatternNumericLabel
        '
        Me._ContactCheckBitPatternNumericLabel.AutoSize = True
        Me._ContactCheckBitPatternNumericLabel.Location = New System.Drawing.Point(8, 131)
        Me._ContactCheckBitPatternNumericLabel.Name = "_ContactCheckBitPatternNumericLabel"
        Me._ContactCheckBitPatternNumericLabel.Size = New System.Drawing.Size(156, 17)
        Me._ContactCheckBitPatternNumericLabel.TabIndex = 8
        Me._ContactCheckBitPatternNumericLabel.Text = "Contact check bit pattern:"
        Me._ContactCheckBitPatternNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_EotStrobeDurationNumericLabel
        '
        Me._EotStrobeDurationNumericLabel.AutoSize = True
        Me._EotStrobeDurationNumericLabel.Location = New System.Drawing.Point(4, 50)
        Me._EotStrobeDurationNumericLabel.Name = "_EotStrobeDurationNumericLabel"
        Me._EotStrobeDurationNumericLabel.Size = New System.Drawing.Size(160, 17)
        Me._EotStrobeDurationNumericLabel.TabIndex = 2
        Me._EotStrobeDurationNumericLabel.Text = "EOT Strobe duration [mS]:"
        '
        '_EotStrobeDurationNumeric
        '
        Me._EotStrobeDurationNumeric.DecimalPlaces = 3
        Me._EotStrobeDurationNumeric.Location = New System.Drawing.Point(167, 46)
        Me._EotStrobeDurationNumeric.Name = "_EotStrobeDurationNumeric"
        Me._EotStrobeDurationNumeric.Size = New System.Drawing.Size(71, 25)
        Me._EotStrobeDurationNumeric.TabIndex = 3
        Me._EotStrobeDurationNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._EotStrobeDurationNumeric.Value = New Decimal(New Integer() {1, 0, 0, 131072})
        '
        '_TriggerDelayNumeric
        '
        Me._TriggerDelayNumeric.DecimalPlaces = 3
        Me._TriggerDelayNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerDelayNumeric.Location = New System.Drawing.Point(167, 156)
        Me._TriggerDelayNumeric.Name = "_TriggerDelayNumeric"
        Me._TriggerDelayNumeric.Size = New System.Drawing.Size(56, 25)
        Me._TriggerDelayNumeric.TabIndex = 24
        '
        '_TriggerDelayNumericLabel
        '
        Me._TriggerDelayNumericLabel.AutoSize = True
        Me._TriggerDelayNumericLabel.Location = New System.Drawing.Point(57, 160)
        Me._TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel"
        Me._TriggerDelayNumericLabel.Size = New System.Drawing.Size(107, 17)
        Me._TriggerDelayNumericLabel.TabIndex = 23
        Me._TriggerDelayNumericLabel.Text = "Trigger Delay [s]:"
        Me._TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'HipotView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Tabs)
        Me.Name = "HipotView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(383, 326)
        Me._Tabs.ResumeLayout(False)
        Me._HipotTabPage.ResumeLayout(False)
        Me._HipotLayout.ResumeLayout(False)
        Me._HipotGroupBox.ResumeLayout(False)
        Me._HipotGroupBox.PerformLayout()
        CType(Me._CurrentLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._VoltageLevelNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ResistanceRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ResistanceLowLimitNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._DwellTimeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ApertureNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._StobeTabPage.ResumeLayout(False)
        Me._BinningLayout.ResumeLayout(False)
        Me._BinningLayout.PerformLayout()
        Me._TriggerToolStrip.ResumeLayout(False)
        Me._TriggerToolStrip.PerformLayout()
        Me._BinningGroupBox.ResumeLayout(False)
        Me._BinningGroupBox.PerformLayout()
        CType(Me._FailBitPatternNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._PassBitPatternNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._ContactCheckBitPatternNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._EotStrobeDurationNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._TriggerDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _Tabs As Windows.Forms.TabControl
    Private WithEvents _HipotTabPage As Windows.Forms.TabPage
    Private WithEvents _StobeTabPage As Windows.Forms.TabPage
    Private WithEvents _BinningLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _TriggerToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _AwaitTriggerToolStripButton As Windows.Forms.ToolStripButton
    Private WithEvents _WaitHourglassLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _AssertTriggerToolStripButton As Windows.Forms.ToolStripButton
    Private WithEvents _TriggerActionToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _BinningGroupBox As Windows.Forms.GroupBox
    Private WithEvents _ArmComboBoxLabel As Windows.Forms.Label
    Private WithEvents _ArmSourceComboBox As Windows.Forms.ComboBox
    Private WithEvents _ApplySotSettingsButton As Windows.Forms.Button
    Private WithEvents _ContactCheckSupportLabel As Windows.Forms.Label
    Private WithEvents _FailBitPatternNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _PassBitPatternNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ContactCheckBitPatternNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _FailBitPatternNumericLabel As Windows.Forms.Label
    Private WithEvents _PassBitPatternNumericLabel As Windows.Forms.Label
    Private WithEvents _ContactCheckBitPatternNumericLabel As Windows.Forms.Label
    Private WithEvents _EotStrobeDurationNumericLabel As Windows.Forms.Label
    Private WithEvents _EotStrobeDurationNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _HipotLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _HipotGroupBox As Windows.Forms.GroupBox
    Private WithEvents _ContactCheckToggle As Windows.Forms.CheckBox
    Private WithEvents _ApplyHipotSettingsButton As Windows.Forms.Button
    Private WithEvents _CurrentLimitNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _CurrentLimitNumericLabel As Windows.Forms.Label
    Private WithEvents _VoltageLevelNumericLabel As Windows.Forms.Label
    Private WithEvents _VoltageLevelNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ResistanceRangeNumericLabel As Windows.Forms.Label
    Private WithEvents _ResistanceLowLimitNumericLabel As Windows.Forms.Label
    Private WithEvents _ResistanceRangeNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ResistanceLowLimitNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _DwellTimeNumericLabel As Windows.Forms.Label
    Private WithEvents _DwellTimeNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ApertureNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ApertureNumericLabel As Windows.Forms.Label
    Private WithEvents _TriggerDelayNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _TriggerDelayNumericLabel As Windows.Forms.Label
End Class
