Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Hipot view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class HipotView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="HipotView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="HipotView"/>. </returns>
    Public Shared Function Create() As HipotView
        Dim view As HipotView = Nothing
        Try
            view = New HipotView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K2400Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K2400Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K2400Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me.AssignInsulationTest(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
            Me.AssignInsulationTest()
        End If
        Me.BindArmLayerSubsystem(value)
        Me.BindContactCheckLimit(value)
        Me.BindSystemSubsystem(value)
        Me.BindTriggerSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K2400Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTACT CHECK LIMIT "

    ''' <summary> Gets the contact check limit. </summary>
    ''' <value> The contact check limit. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ContactCheckLimit As VI.K2400.ContactCheckLimit

    ''' <summary> Bind contact check limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindContactCheckLimit(ByVal device As K2400.K2400Device)
        If Me.ContactCheckLimit IsNot Nothing Then
            Me.BindLimit(False, Me.ContactCheckLimit)
            Me._ContactCheckLimit = Nothing
        End If
        If device IsNot Nothing Then
            Me._ContactCheckLimit = device.ContactCheckLimit
            Me.BindLimit(True, Me.ContactCheckLimit)
        End If
    End Sub

    ''' <summary> Bind limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">   True to add. </param>
    ''' <param name="limit"> The limit. </param>
    Private Sub BindLimit(ByVal add As Boolean, ByVal limit As ContactCheckLimit)
        If add Then
            AddHandler limit.PropertyChanged, AddressOf Me.ContactCheckLimitPropertyChanged
        Else
            RemoveHandler limit.PropertyChanged, AddressOf Me.ContactCheckLimitPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the contact check property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ContactCheckLimit, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.ContactCheckLimit.FailureBits)
                If subsystem.FailureBits.HasValue Then Me._ContactCheckBitPatternNumeric.ValueSetter(subsystem.FailureBits.Value)
        End Select
    End Sub

    ''' <summary> Contact Check Limit subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ContactCheckLimitPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ContactCheckLimit)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ContactCheckLimitPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ContactCheckLimit), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " ARM LAYER "

    ''' <summary> Gets the ArmLayer subsystem. </summary>
    ''' <value> The ArmLayer subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ArmLayerSubsystem As VI.K2400.ArmLayerSubsystem

    ''' <summary> Bind ArmLayer subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindArmLayerSubsystem(ByVal device As K2400.K2400Device)
        If Me.ArmLayerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ArmLayerSubsystem)
            Me._ArmLayerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._ArmLayerSubsystem = device.ArmLayerSubsystem
            Me.BindSubsystem(True, Me.ArmLayerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ArmLayerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ArmLayerSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ArmLayerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Arm subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ArmLayerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.ArmLayerSubsystem.InputLineNumber)
            Case NameOf(VI.K2400.ArmLayerSubsystem.ArmSource)
                Me._ArmSourceComboBox.SelectedItem = subsystem.ArmSource.Value.ValueDescriptionPair
            Case NameOf(VI.K2400.ArmLayerSubsystem.SupportedArmSources)
                If Me.Device IsNot Nothing AndAlso subsystem.SupportedArmSources <> VI.ArmSources.None Then
                    Dim selectedIndex As Integer = Me._ArmSourceComboBox.SelectedIndex
                    Me._ArmSourceComboBox.DataSource = Nothing
                    Me._ArmSourceComboBox.Items.Clear()
                    Me._ArmSourceComboBox.DataSource = GetType(VI.ArmSources).EnumValues.IncludeFilter(subsystem.SupportedArmSources).ValueDescriptionPairs.ToList
                    Me._ArmSourceComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
                    Me._ArmSourceComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
                    If Me._ArmSourceComboBox.Items.Count > 0 Then
                        Me._ArmSourceComboBox.SelectedIndex = Math.Max(selectedIndex, 0)
                    End If
                End If
        End Select
    End Sub

    ''' <summary> Arm subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inArmion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ArmLayerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ArmLayerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ArmLayerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ArmLayerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Gets the selected arm source. </summary>
    ''' <value> The selected arm source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedArmSource() As VI.ArmSources
        Get
            Return CType(CType(Me._ArmSourceComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.ArmSources)
        End Get
    End Property

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System subsystem. </summary>
    ''' <value> The System subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SystemSubsystem As VI.K2400.SystemSubsystem

    ''' <summary> Bind System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSystemSubsystem(ByVal device As K2400.K2400Device)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SystemSubsystem = device.SystemSubsystem
            Me.BindSubsystem(True, Me.SystemSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SystemSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Contact check enabled menu item check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ContactCheckToggle_CheckStateChanged(sender As Object, e As EventArgs) Handles _ContactCheckToggle.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.InfoProvider.Clear()
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying enabling contact check"
            If Not Me.Device.SystemSubsystem.ContactCheckEnabled.HasValue OrElse Me.Device.SystemSubsystem.ContactCheckEnabled.Value <> menuItem.Checked Then
                Me._ContactCheckToggle.CheckState = Me.Device.SystemSubsystem.ContactCheckEnabled.ToCheckState
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Handle the System subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.SystemSubsystem.ContactCheckEnabled)
                Me._ContactCheckToggle.CheckState = subsystem.ContactCheckEnabled.ToCheckState
            Case NameOf(VI.K2400.SystemSubsystem.ContactCheckSupported)
                Me._ContactCheckToggle.Enabled = subsystem.ContactCheckSupported.GetValueOrDefault(False)
                Me._ContactCheckBitPatternNumeric.Enabled = subsystem.ContactCheckSupported.GetValueOrDefault(False)
        End Select
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> System subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SystemSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SystemSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets or sets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As VI.K2400.TriggerSubsystem

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindTriggerSubsystem(ByVal device As K2400.K2400Device)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._TriggerSubsystem = device.TriggerSubsystem
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.TriggerSubsystem.Delay)
                If subsystem.Delay.HasValue Then Me._TriggerDelayNumeric.ValueSetter(subsystem.Delay.Value.TotalMilliseconds)
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inTriggerion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> The device access locker. </summary>
    Private ReadOnly _DeviceAccessLocker As New Object

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _MeterTimer As System.Windows.Forms.Timer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Aborts the measurement cycle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AbortMeasurementThis()
        Me._MeterTimer.Enabled = False
        Me.Device.TriggerSubsystem.Abort()
    End Sub

    ''' <summary> Aborts the measurement cycle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AbortMeasurement()
        SyncLock Me._DeviceAccessLocker
            Me.AbortMeasurementThis()
        End SyncLock
    End Sub

    ''' <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AssertTriggerToolStripButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AssertTriggerToolStripButton.Click
        Me.InfoProvider.Clear()
        Try
            Dim mode As VI.ArmSources = Me.Device.ArmLayerSubsystem.ArmSource.GetValueOrDefault(VI.ArmSources.None)
            If (mode = VI.ArmSources.Manual) AndAlso Me._MeterTimer.Enabled Then
                Me.AssertTrigger()
            ElseIf mode <> VI.ArmSources.Manual Then
                Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Info, $"Manual trigger ignored in '{mode.Description}' mode")
            ElseIf Not Me._MeterTimer.Enabled Then
                Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Info, "Manual trigger ignored -- triggering is not active")
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, "Exception occurred aborting")
            My.Application.Log.WriteException(ex, TraceEventType.Error, "Exception occurred aborting.")
        End Try
    End Sub

    ''' <summary> Outputs a trigger to make a measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AssertTrigger()
        SyncLock Me._DeviceAccessLocker

            Me.Device.Session.AssertTrigger()
        End SyncLock
    End Sub

    ''' <summary> True if abort requested. </summary>
    Private _AbortRequested As Boolean

    ''' <summary> Turns on or aborts waiting for trigger. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="enabled"> True to enable, false to disable. </param>
    ''' <param name="checked"> True if checked. </param>
    Private Sub ToggleAwaitTrigger(ByVal enabled As Boolean, ByVal checked As Boolean)
        Me.InfoProvider.SetIconPadding(Me._TriggerToolStrip, -10)
        Me.InfoProvider.SetError(Me._TriggerToolStrip, "")
        If enabled Then
            Me._AbortRequested = Not checked
            If checked Then
                SyncLock Me._DeviceAccessLocker
                    Me.PublishVerbose("Preparing instrument for waiting for trigger;. ")
                    Me.TriggerAction = "INITIATING"
                    ' clear execution state before enabling events
                    Me.Device.ClearExecutionState()

                    ' set the service request
                    Me.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
                    Me.Device.Session.ApplyStandardServiceRequestEnableBitmasks(Me.Device.Session.DefaultStandardEventEnableBitmask,
                                                                                Me.Device.Session.DefaultOperationCompleteBitmask Or VI.Pith.ServiceRequests.OperationEvent)
                    Me.Device.ClearExecutionState()
                    Me.Device.TriggerSubsystem.Initiate()
                    Me.TriggerAction = "WAITING FOR TRIGGERED MEASUREMENT"
                    Me.PublishVerbose("Monitoring instrument for measurements;. ")
                    Me._MeterTimer.Interval = 100
                    Me._MeterTimer.Enabled = True
                End SyncLock
            Else
                Me.TriggerAction = "ABORT REQUESTED"
                Me.AbortMeasurementThis()
            End If
        End If
        Me._AwaitTriggerToolStripButton.Text = If(Me._AwaitTriggerToolStripButton.Checked, "ABORT TRIGGERING", "WAIT FOR A TRIGGER")
    End Sub

    ''' <summary> Turns on or aborts waiting for trigger. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AwaitTriggerToolStripButton_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AwaitTriggerToolStripButton.CheckedChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.InfoProvider.Clear()
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger"
            Me.ToggleAwaitTrigger(Me._AwaitTriggerToolStripButton.Enabled, Me._AwaitTriggerToolStripButton.Checked)
        Catch ex As Exception
            Me.AbortMeasurementThis()
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Gets or sets the trigger action. </summary>
    ''' <value> The trigger action. </value>
    Private Property TriggerAction As String
        Get
            Return Me._TriggerActionToolStripLabel.Text
        End Get
        Set(value As String)
            If Not String.Equals(Me.TriggerAction, value) Then
                Me._TriggerActionToolStripLabel.Text = value
            End If
        End Set
    End Property

    ''' <summary> The status bar. </summary>
    Private Shared _StatusBar As String = "|"

    ''' <summary> Gets the next status bar. </summary>
    ''' <value> The next status bar. </value>
    Private Shared ReadOnly Property NextStatusBar As String
        Get
            If _StatusBar = "|" Then
                _StatusBar = "/"
            ElseIf _StatusBar = "/" Then
                _StatusBar = "-"
            ElseIf _StatusBar = "-" Then
                _StatusBar = "\"
            ElseIf _StatusBar = "\" Then
                _StatusBar = "|"
            End If
            Return _StatusBar
        End Get
    End Property

    ''' <summary>
    ''' Monitors measurements. Once found, reads and displays and restarts the cycle.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeterTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MeterTimer.Tick
        SyncLock Me._DeviceAccessLocker
            Dim activity As String = String.Empty
            Try
                activity = $"{Me.Device.ResourceNameCaption} monitoring instrument for data"
                Me._MeterTimer.Enabled = False
                If Me._AbortRequested Then
                    Me.AbortMeasurementThis()
                    Return
                End If
                Me.Device.Session.ReadStatusRegister()
                If Me.Device.StatusSubsystem.HasMeasurementEvent Then
                    Me.TriggerAction = "READING..."

                    ' update display modalities if changed.
                    Me.Device.MeasureSubsystem.Read()
                    Me.TriggerAction = "DATA AVAILABLE..."

                    Me._TriggerActionToolStripLabel.Text = "PREPARING..."

                    ' clear execution state before enabling events
                    Me.Device.ClearExecutionState()

                    ' set the service request
                    Me.Device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
                    Me.Device.Session.ApplyStandardServiceRequestEnableBitmasks(Me.Device.Session.DefaultStandardEventEnableBitmask,
                                                                                Me.Device.Session.DefaultOperationCompleteBitmask Or VI.Pith.ServiceRequests.OperationEvent)
                    Me.Device.ClearExecutionState()
                    Me.Device.TriggerSubsystem.Initiate()
                    Me.TriggerAction = "WAITING FOR TRIGGRED MEASUREMENT..."
                    Me.PublishVerbose("Monitoring instrument for measurements;. ")
                Else
                    Me.TriggerAction = "WAITING FOR TRIGGERED MEASUREMENT"
                    Me._WaitHourglassLabel.Text = HipotView.NextStatusBar
                End If
                Me._MeterTimer.Enabled = Not Me._AbortRequested
            Catch ex As Exception
                Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
                Me.PublishException(activity, ex)
                Me._AwaitTriggerToolStripButton.Checked = False
            End Try
        End SyncLock
    End Sub

#End Region

#Region " INSULATION TEST "

    ''' <summary> True if is insulation test owner, false if not. </summary>
    Private _IsInsulationTestOwner As Boolean

    ''' <summary> The insulation test. </summary>
    Private _InsulationTest As InsulationTest

    ''' <summary> Tests assign insulation. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub AssignInsulationTest()
        Me._InsulationTest = New InsulationTest
        Me._IsInsulationTestOwner = False
        Me.AssignInsulationTest(New InsulationTest)
        Me._IsInsulationTestOwner = True
    End Sub

    ''' <summary> Tests assign insulation. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignInsulationTest(ByVal value As InsulationTest)
        If Me._IsInsulationTestOwner AndAlso Me._InsulationTest IsNot Nothing Then
            Me._InsulationTest = Nothing
        End If
        Me._IsInsulationTestOwner = False
        Me.BindBinningInfo(value)
        Me.BindActiveInsulationResistance(value)
    End Sub

#End Region

#Region " BINNING INFO "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _BinningInfo As VI.BinningInfo
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets information describing the binning. </summary>
    ''' <value> Information describing the binning. </value>
    Public ReadOnly Property BinningInfo As VI.BinningInfo
        Get
            Return Me._BinningInfo
        End Get
    End Property

    ''' <summary> Bind binning information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The insulation test to use for the binning info source. </param>
    Private Sub BindBinningInfo(ByVal value As InsulationTest)
        If Me.BinningInfo IsNot Nothing Then
            Me.BindEntity(False, Me.BinningInfo)
            Me._BinningInfo = Nothing
        End If
        If value IsNot Nothing Then
            Me._BinningInfo = value.Binning
            Me.BindEntity(True, Me.BinningInfo)
        End If
    End Sub

    ''' <summary> Bind the entity. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">   True to add. </param>
    ''' <param name="value"> The entity. </param>
    Private Sub BindEntity(ByVal add As Boolean, ByVal value As VI.BinningInfo)
        If add Then
            AddHandler value.PropertyChanged, AddressOf Me.BinningInfoPropertyChanged
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.ArmSource))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.PassBits))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.LowerLimit))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.LowerLimitFailureBits))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.StrobePulseWidth))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.FailureBits))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.UpperLimitFailureBits))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.LowerLimitFailureBits))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.LowerLimit))
            Me.OnPropertyChanged(value, NameOf(VI.BinningInfo.PassBits))
        Else
            RemoveHandler value.PropertyChanged, AddressOf Me.BinningInfoPropertyChanged
        End If
    End Sub

    ''' <summary> Executes the 'property changed' action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub OnPropertyChanged(ByVal sender As VI.BinningInfo, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.BinningInfo.ArmSource)
                Me._ArmSourceComboBox.SelectItem(sender.ArmSource.ValueDescriptionPair)
            Case NameOf(VI.BinningInfo.PassBits)
                Me._PassBitPatternNumeric.ValueSetter(sender.PassBits)
            Case NameOf(VI.BinningInfo.LowerLimit)
                Me._ResistanceLowLimitNumeric.ValueSetter(0.00001 * sender.LowerLimit)
            Case NameOf(VI.BinningInfo.LowerLimitFailureBits)
                Me._FailBitPatternNumeric.ValueSetter(sender.LowerLimitFailureBits)
            Case NameOf(VI.BinningInfo.StrobePulseWidth)
                Me._EotStrobeDurationNumeric.ValueSetter(sender.StrobePulseWidth.Ticks / TimeSpan.TicksPerMillisecond)
            Case NameOf(VI.BinningInfo.FailureBits)
                Me._FailBitPatternNumeric.Value = sender.FailureBits
            Case NameOf(VI.BinningInfo.UpperLimitFailureBits)
                Me._FailBitPatternNumeric.Value = sender.UpperLimitFailureBits
            Case NameOf(VI.BinningInfo.LowerLimitFailureBits)
                Me._FailBitPatternNumeric.Value = sender.LowerLimitFailureBits
            Case NameOf(VI.BinningInfo.LowerLimit)
                Me._ResistanceLowLimitNumeric.Value = CDec(0.000001 * sender.LowerLimit)
            Case NameOf(VI.BinningInfo.PassBits)
                Me._PassBitPatternNumeric.Value = sender.PassBits
        End Select
    End Sub

    ''' <summary> Event handler. Called by _BinningInfo for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BinningInfoPropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _BinningInfo.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} handling the binning info {e?.PropertyName} changed Event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.BinningInfoPropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, VI.BinningInfo), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " INSULATION RESISTANCE "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _ActiveInsulationResistance As InsulationResistance
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the active insulation resistance. </summary>
    ''' <value> The active insulation resistance. </value>
    Public ReadOnly Property ActiveInsulationResistance As InsulationResistance
        Get
            Return Me._ActiveInsulationResistance
        End Get
    End Property

    ''' <summary> Bind active insulation resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub BindActiveInsulationResistance(value As InsulationTest)
        If Me.ActiveInsulationResistance IsNot Nothing Then
            Me.BindEntity(False, Me.ActiveInsulationResistance)
            Me._ActiveInsulationResistance = Nothing
        End If
        If value IsNot Nothing Then
            Me._ActiveInsulationResistance = value.Insulation
            Me.BindEntity(True, Me.ActiveInsulationResistance)
        End If
    End Sub

    ''' <summary> Bind the entity. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">   True to add. </param>
    ''' <param name="value"> The entity. </param>
    Private Sub BindEntity(ByVal add As Boolean, ByVal value As InsulationResistance)
        If add Then
            AddHandler value.PropertyChanged, AddressOf Me.ActiveInsulationResistancePropertyChanged
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.ContactCheckEnabled))
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.CurrentLimit))
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.DwellTime))
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.PowerLineCycles))
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.ResistanceLowLimit))
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.ResistanceRange))
            Me.OnPropertyChanged(value, NameOf(VI.InsulationResistance.VoltageLevel))
        Else
            RemoveHandler value.PropertyChanged, AddressOf Me.ActiveInsulationResistancePropertyChanged
        End If
    End Sub

    ''' <summary> Executes the 'property changed' action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub OnPropertyChanged(ByVal sender As InsulationResistance, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.InsulationResistance.ContactCheckEnabled)
                Me._ContactCheckToggle.Checked = sender.ContactCheckEnabled
            Case NameOf(VI.InsulationResistance.CurrentLimit)
                Me._CurrentLimitNumeric.ValueSetter(0.00001 * sender.CurrentLimit)
            Case NameOf(VI.InsulationResistance.DwellTime)
                Me._DwellTimeNumeric.ValueSetter(sender.DwellTime.TotalSeconds)
            Case NameOf(VI.InsulationResistance.PowerLineCycles)
                Me._ApertureNumeric.ValueSetter(sender.PowerLineCycles)
            Case NameOf(VI.InsulationResistance.ResistanceLowLimit)
                Me._ResistanceLowLimitNumeric.ValueSetter(0.00001 * sender.ResistanceLowLimit)
            Case NameOf(VI.InsulationResistance.ResistanceRange)
                Me._ResistanceRangeNumeric.ValueSetter(0.000001 * sender.ResistanceRange)

            Case NameOf(VI.InsulationResistance.VoltageLevel)
                Me._VoltageLevelNumeric.ValueSetter(sender.VoltageLevel)
        End Select
    End Sub

    ''' <summary>
    ''' Event handler. Called by _ActiveInsulationResistance for property changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ActiveInsulationResistancePropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _ActiveInsulationResistance.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} handling the insulation resistance {e?.PropertyName} changed Event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ActiveInsulationResistancePropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, InsulationResistance), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " HIPOT TEST CONFIGURATION "

    ''' <summary> Tests configure hipot start. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="binning">    The binning info. </param>
    ''' <param name="insulation"> The insulation info. </param>
    Private Sub ConfigureHipotStartTest(ByVal binning As VI.BinningInfo, ByVal insulation As InsulationResistance)

        If binning Is Nothing Then Throw New ArgumentNullException(NameOf(binning))
        If insulation Is Nothing Then Throw New ArgumentNullException(NameOf(insulation))

        Me.InfoProvider.Clear()
        Me.Device.ClearExecutionState()
        Me.Device.OutputSubsystem.WriteOutputOnState(False)

        ' ======
        ' CALC2

        ' default: Me.Session.Write(":CALC2:CLIM:BCON IMM")
        ' Me.Session.Write(":CALC2:CLIM:CLE")
        Me.Device.CompositeLimit.ClearLimits()

        ' default: Me.Session.Write(":CALC2:CLIM:CLE:AUTO ON")
        Me.Device.CompositeLimit.ApplyAutoClearEnabled(True)

        ' default: Me.Session.Write(":CALC2:CLIM:MODE GRAD")
        Me.Device.CompositeLimit.ApplyLimitMode(LimitMode.Grading)

        ' Make limit comparisons on resistance reading
        ' Me.Session.Write(":CALC2:FEED RES")
        Me.Device.BinningSubsystem.ApplyFeedSource(VI.FeedSources.Resistance)

        ' Compliance Limit: send BAD code if measurement is in compliance
        ' default: Me.Session.Write(":CALC2:LIM1:COMP:FAIL IN")
        Me.Device.ComplianceLimit.ApplyIncomplianceCondition(True)

        ' Set fail bin
        ' Me.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC2:LIM1:COMP:SOUR2 {0}", Me._FailBitPatternNumeric.Value))
        Me.Device.ComplianceLimit.ApplyFailureBits(binning.FailureBits)
        binning.FailureBits = Me.Device.ComplianceLimit.FailureBits.GetValueOrDefault(0)

        ' Me.Session.Write(":CALC2:LIM1:STAT 1")
        Me.Device.ComplianceLimit.ApplyEnabled(binning.Enabled)
        binning.Enabled = Me.Device.ComplianceLimit.Enabled.GetValueOrDefault(False)

        ' Lower Limit
        ' Me.Session.Write(":CALC2:LIM2:UPP 1E+19")
        Me.Device.UpperLowerLimit.ApplyUpperLimit(binning.UpperLimit)
        binning.UpperLimit = Me.Device.UpperLowerLimit.UpperLimit.GetValueOrDefault(0)
        ' ":CALC2:LIM2:UPP:SOUR2 {0}", Me._FailBitPatternNumeric.Value
        Me.Device.UpperLowerLimit.ApplyUpperLimitFailureBits(binning.UpperLimitFailureBits)
        binning.UpperLimitFailureBits = Me.Device.UpperLowerLimit.UpperLimitFailureBits.GetValueOrDefault(0)
        ' ":CALC2:LIM2:LOW {0}", Me._ResistanceLowLimitNumeric.Value
        Me.Device.UpperLowerLimit.ApplyLowerLimit(binning.LowerLimit)
        binning.LowerLimit = Me.Device.UpperLowerLimit.LowerLimit.GetValueOrDefault(0)
        ' ":CALC2:LIM2:LOW:SOUR2 {0}", Me._FailBitPatternNumeric.Value))
        Me.Device.UpperLowerLimit.ApplyLowerLimitFailureBits(binning.LowerLimitFailureBits)
        binning.LowerLimitFailureBits = Me.Device.UpperLowerLimit.LowerLimitFailureBits.GetValueOrDefault(0)
        ' Me.Session.Write(":CALC2:LIM2:STAT 1")
        Me.Device.UpperLowerLimit.ApplyEnabled(binning.Enabled)
        binning.Enabled = Me.Device.UpperLowerLimit.Enabled.GetValueOrDefault(False)

        ' Send GOOD code if measurement is in range
        ' ":CALC2:CLIM:PASS:SOUR2 {0}", Me._PassBitPatternNumeric.Value
        Me.Device.CompositeLimit.ApplyPassBits(binning.PassBits)
        binning.PassBits = Me.Device.CompositeLimit.PassBits.GetValueOrDefault(0)
        ' Me.Session.Write(":CALC2:CLIM:BCON IMM")
        Me.Device.CompositeLimit.ApplyBinningControl(BinningControl.Immediate)


        ' enable contact check
        If insulation.ContactCheckEnabled Then
            ' Me.Session.Write(":CALC2:LIM4:STAT ON")
            Me.Device.ContactCheckLimit.ApplyEnabled(insulation.ContactCheckEnabled)
            insulation.ContactCheckEnabled = Me.Device.ContactCheckLimit.Enabled.GetValueOrDefault(False)
            If Me._ContactCheckBitPatternNumeric.Value > 0 Then
                Me.Device.ContactCheckLimit.ApplyFailureBits(CInt(Me._ContactCheckBitPatternNumeric.Value))
            End If
            ' this is done when setting the insulation test: 
            ' Me.Session.Write(":SYSTem:CCH ON")
        Else
            ' Me.Session.Write(":CALC2:LIM4:STAT OFF")
            Me.Device.ContactCheckLimit.ApplyEnabled(insulation.ContactCheckEnabled)
            insulation.ContactCheckEnabled = Me.Device.ContactCheckLimit.Enabled.GetValueOrDefault(True)
            ' this is done when setting the insulation test: 
            ' Me.Session.Write(":SYSTem:CCH OFF")
        End If

        ' ======
        ' EOT
        ' Set byte size to 3 enabling EOT mode.
        ' Me.Session.Write(":SOUR2:BSIZ 3")
        Me.Device.DigitalOutput.ApplyBitSize(3)

        ' Set the output logic to high
        ' Me.Session.Write(":SOUR2:TTL #b000")
        Me.Device.DigitalOutput.ApplyLevel(0)

        ' Set Digital I/O Mode to EOT
        ' Me.Session.Write(":SOUR2:TTL4:MODE EOT")
        Me.Device.DigitalOutput.ApplyOutputMode(VI.OutputModes.EndTest)

        ' Set EOT polarity to HI
        ' Me.Session.Write(":SOUR2:TTL4:BST HI")
        Me.Device.DigitalOutput.ApplyDigitalActiveLevel(VI.DigitalActiveLevels.High)

        ' Set the output level to set automatically to the
        ' :TTL level after the pass or fail output bit
        ' pattern or a limit test is sent to the handler.
        ' this can be turned off and the clear command issued on each start.
        ' Me.Session.Write(":SOUR2:CLE")
        Me.Device.DigitalOutput.ClearOutput()
        ' Me.Session.Write(":SOUR2:CLE:AUTO ON")
        Me.Device.DigitalOutput.ApplyAutoClearEnabled(True)

        ' Set the duration of the EOT strobe
        ' Me.Session.Write(":SOUR2:CLE:AUTO:DEL " & CStr(Me._EotStrobeDurationNumeric.Value))
        Me.Device.DigitalOutput.ApplyDelay(TimeSpan.FromMilliseconds(Me._EotStrobeDurationNumeric.Value))

        ' =================
        ' ARM MODEL:

        ' arm to immediate mode.
        ' default is IMM:  Me.Session.Write(":ARM:SOUR IMM")

        ' Set ARM counter to 1
        ' default is 1: Me.Session.Write(":ARM:COUN 1")

        ' Define Trigger layer to interface with the
        ' Trigger Master board:

        ' clear any pending triggers.
        ' Me.Session.Write(":TRIG:CLE")
        Me.Device.TriggerSubsystem.ClearTriggers()

        ' Set TRIGGER input line to Trigger Link line number
        ' default. to be set when starting: Me.Session.Write(":TRIG:ILIN 1")

        ' Set TRIGGER output line to Trigger Link line number
        ' default: Me.Session.Write(":TRIG:OLIN 2")

        ' Set Input trigger to Acceptor
        ' Me.Session.Write(":TRIG:DIR ACC")
        'Me.Device.TriggerSubsystem.ApplyDirection(Direction.Acceptor)

        ' Me.Session.Write(":TRIG:DIR SOUR")
        'Me.Device.TriggerSubsystem.ApplyDirection(Direction.Source)
        Me.Device.TriggerSubsystem.ApplyTriggerLayerBypassMode(binning.TriggerDirection)
        If binning.TriggerDirection <> Me.Device.TriggerSubsystem.TriggerLayerBypassMode.GetValueOrDefault(VI.TriggerLayerBypassModes.None) Then
            Throw New isr.Core.OperationFailedException($"Failed setting trigger direction to {binning.TriggerDirection};. Value set to {Me.Device.TriggerSubsystem.TriggerLayerBypassMode}")
        End If

        ' start with immediate trigger allowing non-triggered measurements.
        ' Me.Session.Write(":TRIG:SOUR IMM")
        ' see below: Me.Device.TriggerSubsystem.ApplyTriggerSource(TriggerSource.Immediate)

        ' no outputs triggers
        ' default is none: Me.Session.Write(":TRIG:OUTP NONE")

        ' A single trigger.
        ' default is 1: Me.Session.Write(":TRIG:COUN 1")

        ' default: Me.Session.Write(":TRIG:INP NONE")

        ' set trigger source to immediate
        Me.Device.TriggerSubsystem.ApplyTriggerSource(binning.TriggerSource)
        If binning.TriggerSource <> Me.Device.TriggerSubsystem.TriggerSource.GetValueOrDefault(VI.TriggerSources.None) Then
            Throw New isr.Core.OperationFailedException($"Failed setting trigger source to {binning.TriggerSource};. Value set to {Me.Device.TriggerSubsystem.TriggerSource}")
        End If

        Me.Device.ArmLayerSubsystem.ApplyArmLayerBypassMode(binning.ArmDirection)
        If binning.ArmDirection <> Me.Device.ArmLayerSubsystem.ArmLayerBypassMode.GetValueOrDefault(VI.TriggerLayerBypassModes.None) Then
            Throw New isr.Core.OperationFailedException($"Failed setting Arm Layer Bypass Mode to {binning.ArmDirection};. Value set to {Me.Device.ArmLayerSubsystem.ArmLayerBypassMode}")
        End If

        Me.Device.ArmLayerSubsystem.ApplyArmSource(binning.ArmSource)
        binning.ArmSource = Me.Device.ArmLayerSubsystem.ArmSource.GetValueOrDefault(VI.ArmSources.None)

        Me.Device.ArmLayerSubsystem.ApplyArmCount(binning.ArmCount)
        binning.ArmCount = Me.Device.ArmLayerSubsystem.ArmCount.GetValueOrDefault(0)

    End Sub

    ''' <summary> Applies the hipot binning information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ApplyHipotBinningInfo()
        Me.BinningInfo.TriggerDirection = VI.TriggerLayerBypassModes.Source
        Me.BinningInfo.TriggerSource = VI.TriggerSources.Immediate
        Me.BinningInfo.ArmSource = Me.SelectedArmSource
        Me.BinningInfo.ArmCount = If(Me.BinningInfo.ArmSource = VI.ArmSources.Manual, 1, 0)
        Me.BinningInfo.FailureBits = CInt(Me._FailBitPatternNumeric.Value)
        Me.BinningInfo.UpperLimit = isr.VI.Pith.Scpi.Syntax.Infinity
        Me.BinningInfo.UpperLimitFailureBits = CInt(Me._FailBitPatternNumeric.Value)
        Me.BinningInfo.LowerLimit = 1000000.0 * Me._ResistanceLowLimitNumeric.Value
        Me.BinningInfo.LowerLimitFailureBits = CInt(Me._FailBitPatternNumeric.Value)
        Me.BinningInfo.PassBits = CInt(Me._PassBitPatternNumeric.Value)
        Me.BinningInfo.Enabled = True
    End Sub

    ''' <summary> Configures the high potential measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="hipotSettings"> The hipot settings. </param>
    Public Sub ConfigureHipot(ByVal hipotSettings As InsulationResistance)
        If hipotSettings Is Nothing Then Throw New ArgumentNullException(NameOf(hipotSettings))

        ' make sure the 2400 output is off
        Me.Device.OutputSubsystem.ApplyOutputOnState(False)

        ' Me.Session.Write("*RST")
        ' Me.Session.Write(":OUTP OFF")
        ' Me.Session.Write(":ROUT:TERM REAR")
        ' Me.Session.Write(":SENS:FUNC ""RES""")
        ' Me.Session.Write(":SENS:RES:MODE MAN")
        ' Me.Session.Write(":SOUR:CLE:AUTO ON")
        ' Me.Session.Write(":SOUR:DEL 5")
        ' Me.Session.Write(":SYST:RSEN ON")
        ' Me.Session.Write(":SOUR:FUNC VOLT")
        ' Me.Session.Write(":SOUR:VOLT:RANG 5")
        ' Me.Session.Write(":SOUR:VOLT 5")
        ' Me.Session.Write(":SENS:CURR:RANG 10e-6")
        ' Me.Session.Write(":SENS:CURR:PROT 10e-6")
        ' Me.Session.Write(":SENS:VOLT:NPLC 1")
        ' Me.Session.Write(":SENS:CURR:NPLC 1")
        ' Me.Session.Write(":SENS:RES:NPLC 1")
        ' Me.Session.Write(":SYST:AZER ONCE")
        ' Me.Session.Write(":FORM:ELEM VOLT,CURR,RES,STATUS")
        ' Me.Session.Write(":SOUR:CLE:AUTO ON")
        ' Dim value As String = Me.Session.Query(":READ?")

        ' initialize the known state.
        Me.Device.ResetClearInit()
        Me.Device.RouteSubsystem.ApplyTerminalsMode(VI.RouteTerminalsModes.Rear)
        Me.Device.SenseSubsystem.ApplyFunctionMode(VI.SenseFunctionModes.Resistance)
        Me.Device.SenseSubsystem.FunctionModes = VI.SenseFunctionModes.Resistance
        Me.Device.SenseSubsystem.SupportsMultiFunctions = False

        ' set to manual mode to require manually setting the measurement as voltage source
        Me.Device.SenseResistanceSubsystem.ApplyConfigurationMode(VI.ConfigurationModes.Manual)

        ' the source must be set first.
        Me.Device.SourceSubsystem.ApplyAutoClearEnabled(True)

        Me.Device.SourceSubsystem.ApplyDelay(hipotSettings.DwellTime)
        hipotSettings.DwellTime = Me.Device.SourceSubsystem.Delay.GetValueOrDefault(TimeSpan.Zero)

        Me.Device.SourceSubsystem.ApplyFunctionMode(VI.SourceFunctionModes.Voltage)

        Me.Device.SourceVoltageSubsystem.ApplyRange(hipotSettings.VoltageLevel)

        Me.Device.SourceVoltageSubsystem.ApplyLevel(hipotSettings.VoltageLevel)
        hipotSettings.VoltageLevel = Me.Device.SourceVoltageSubsystem.Level.GetValueOrDefault(0)

        Me.Device.SenseCurrentSubsystem.ApplyRange(hipotSettings.CurrentRange)

        Me.Device.SenseCurrentSubsystem.ApplyProtectionLevel(hipotSettings.CurrentLimit)
        hipotSettings.CurrentLimit = Me.Device.SenseCurrentSubsystem.ProtectionLevel.GetValueOrDefault(0)

        Me.Device.SenseResistanceSubsystem.ApplyPowerLineCycles(hipotSettings.PowerLineCycles)
        hipotSettings.PowerLineCycles = Me.Device.SenseResistanceSubsystem.PowerLineCycles.GetValueOrDefault(0)

        ' Enable four wire connection
        Me.Device.SystemSubsystem.ApplyFourWireSenseEnabled(True)

        ' force immediate update of auto zero
        Me.Device.SystemSubsystem.ApplyAutoZeroEnabled(True)

        Me.Device.SystemSubsystem.ApplyContactCheckEnabled(hipotSettings.ContactCheckEnabled)
        hipotSettings.ContactCheckEnabled = Me.Device.SystemSubsystem.ContactCheckEnabled.GetValueOrDefault(False)
        Me.Device.ContactCheckLimit.ApplyEnabled(hipotSettings.ContactCheckEnabled)
        hipotSettings.ContactCheckEnabled = Me.Device.ContactCheckLimit.Enabled.GetValueOrDefault(False)

        Me.Device.FormatSubsystem.ApplyElements(ReadingElementTypes.Voltage Or ReadingElementTypes.Current Or
                                                ReadingElementTypes.Resistance Or ReadingElementTypes.Status)

        ' Me.Device.MeasureSubsystem.Readings.ResistanceReading.LowLimit = 1000000.0 * Me._ResistanceRangeNumeric.Value
        Me.Device.MeasureSubsystem.ReadingAmounts.ResistanceReading.LowLimit = hipotSettings.ResistanceLowLimit

        ' update the timeout to reflect the dwell time.
        If Me.Device.SourceSubsystem.Delay.HasValue Then
            Dim totalTestTime As TimeSpan = Me.Device.SourceSubsystem.Delay.Value
            totalTestTime = totalTestTime.Add(Me.Device.SenseResistanceSubsystem.IntegrationPeriod.GetValueOrDefault(TimeSpan.Zero))
            totalTestTime = totalTestTime.Add(Me.Device.TriggerSubsystem.Delay.GetValueOrDefault(TimeSpan.Zero))
            ' set the minimum timeout
            totalTestTime = totalTestTime.Add(TimeSpan.FromSeconds(2))
            If totalTestTime > Me.Device.Session.CommunicationTimeout Then Me.Device.Session.StoreCommunicationTimeout(totalTestTime)
        End If
    End Sub

    ''' <summary> Configure hipot change. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="hipotSettings"> The hipot settings. </param>
    Public Sub ConfigureHipotChange(ByVal hipotSettings As InsulationResistance)
        If hipotSettings Is Nothing Then Throw New ArgumentNullException(NameOf(hipotSettings))

        ' make sure the 2400 output is off
        Me.Device.OutputSubsystem.ApplyOutputOnState(False)

        If Not Nullable.Equals(Me.Device.RouteSubsystem.TerminalsMode, VI.OutputTerminalsModes.Rear) Then
            Me.Device.RouteSubsystem.ApplyTerminalsMode(VI.RouteTerminalsModes.Rear)
        End If
        If Not Nullable.Equals(Me.Device.SenseSubsystem.FunctionMode, VI.SenseFunctionModes.Resistance) Then
            Me.Device.SenseSubsystem.ApplyFunctionMode(VI.SenseFunctionModes.Resistance)
            Me.Device.SenseSubsystem.FunctionModes = VI.SenseFunctionModes.Resistance
            Me.Device.SenseSubsystem.SupportsMultiFunctions = False
        End If

        ' set to manual mode to require manually setting the measurement as voltage source
        If Not Nullable.Equals(Me.Device.SenseResistanceSubsystem.ConfigurationMode, VI.ConfigurationModes.Manual) Then
            Me.Device.SenseResistanceSubsystem.ApplyConfigurationMode(VI.ConfigurationModes.Manual)
        End If


        ' the source must be set first.
        If Not Nullable.Equals(Me.Device.SourceSubsystem.AutoClearEnabled, True) Then
            Me.Device.SourceSubsystem.ApplyAutoClearEnabled(True)
        End If

        If Not Nullable.Equals(Me.Device.SourceSubsystem.Delay, hipotSettings.DwellTime) Then
            Me.Device.SourceSubsystem.ApplyDelay(hipotSettings.DwellTime)
            hipotSettings.DwellTime = Me.Device.SourceSubsystem.Delay.GetValueOrDefault(TimeSpan.Zero)
        End If

        If Not Nullable.Equals(Me.Device.SourceSubsystem.FunctionMode, VI.SourceFunctionModes.Voltage) Then
            Me.Device.SourceSubsystem.ApplyFunctionMode(VI.SourceFunctionModes.Voltage)
        End If

        If Not Nullable.Equals(Me.Device.SourceVoltageSubsystem.Range, hipotSettings.VoltageLevel) Then
            Me.Device.SourceVoltageSubsystem.ApplyRange(hipotSettings.VoltageLevel)
        End If

        If Not Nullable.Equals(Me.Device.SourceVoltageSubsystem.Level, hipotSettings.VoltageLevel) Then
            Me.Device.SourceVoltageSubsystem.ApplyLevel(hipotSettings.VoltageLevel)
            hipotSettings.VoltageLevel = Me.Device.SourceVoltageSubsystem.Level.GetValueOrDefault(0)
        End If

        If Not Nullable.Equals(Me.Device.SenseCurrentSubsystem.Range, hipotSettings.CurrentRange) Then
            Me.Device.SenseCurrentSubsystem.ApplyRange(hipotSettings.CurrentRange)
        End If

        If Not Nullable.Equals(Me.Device.SenseCurrentSubsystem.ProtectionLevel, hipotSettings.CurrentRange) Then
            Me.Device.SenseCurrentSubsystem.ApplyProtectionLevel(hipotSettings.CurrentLimit)
            hipotSettings.CurrentLimit = Me.Device.SenseCurrentSubsystem.ProtectionLevel.GetValueOrDefault(0)
        End If

        If Not Nullable.Equals(Me.Device.SenseResistanceSubsystem.PowerLineCycles, hipotSettings.PowerLineCycles) Then
            Me.Device.SenseResistanceSubsystem.ApplyPowerLineCycles(hipotSettings.PowerLineCycles)
            hipotSettings.PowerLineCycles = Me.Device.SenseResistanceSubsystem.PowerLineCycles.GetValueOrDefault(0)
        End If

        ' Enable four wire connection
        If Not Nullable.Equals(Me.Device.SystemSubsystem.FourWireSenseEnabled, True) Then
            Me.Device.SystemSubsystem.ApplyFourWireSenseEnabled(True)
        End If

        ' force immediate update of auto zero
        If Not Nullable.Equals(Me.Device.SystemSubsystem.AutoZeroEnabled, True) Then
            Me.Device.SystemSubsystem.ApplyAutoZeroEnabled(True)
        End If

        If Not Nullable.Equals(Me.Device.SystemSubsystem.ContactCheckEnabled, hipotSettings.ContactCheckEnabled) Then
            Me.Device.SystemSubsystem.ApplyContactCheckEnabled(hipotSettings.ContactCheckEnabled)
            hipotSettings.ContactCheckEnabled = Me.Device.SystemSubsystem.ContactCheckEnabled.GetValueOrDefault(False)
        End If

        If Not Nullable.Equals(Me.Device.ContactCheckLimit.Enabled, hipotSettings.ContactCheckEnabled) Then
            Me.Device.ContactCheckLimit.ApplyEnabled(hipotSettings.ContactCheckEnabled)
            hipotSettings.ContactCheckEnabled = Me.Device.ContactCheckLimit.Enabled.GetValueOrDefault(False)
        End If

        Dim elements As ReadingElementTypes = ReadingElementTypes.Voltage Or ReadingElementTypes.Current Or
                                                ReadingElementTypes.Resistance Or ReadingElementTypes.Status
        If Not Nullable.Equals(Me.Device.FormatSubsystem.Elements, elements) Then
            Me.Device.FormatSubsystem.ApplyElements(elements)
        End If

        If Not Nullable.Equals(Me.Device.MeasureSubsystem.ReadingAmounts.ResistanceReading.LowLimit, hipotSettings.ResistanceLowLimit) Then
            Me.Device.MeasureSubsystem.ReadingAmounts.ResistanceReading.LowLimit = hipotSettings.ResistanceLowLimit
        End If

        ' update the timeout to reflect the dwell time.
        If Me.Device.SourceSubsystem.Delay.HasValue Then
            Dim totalTestTime As TimeSpan = Me.Device.SourceSubsystem.Delay.Value
            totalTestTime = totalTestTime.Add(Me.Device.SenseResistanceSubsystem.IntegrationPeriod.GetValueOrDefault(TimeSpan.Zero))
            totalTestTime = totalTestTime.Add(Me.Device.TriggerSubsystem.Delay.GetValueOrDefault(TimeSpan.Zero))
            ' set the minimum timeout
            totalTestTime = totalTestTime.Add(TimeSpan.FromSeconds(2))
            If totalTestTime > Me.Device.Session.CommunicationTimeout Then Me.Device.Session.StoreCommunicationTimeout(totalTestTime)
        End If
    End Sub

    ''' <summary> Applies the hipot settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ApplyHipotSettings()
        Me.ActiveInsulationResistance.DwellTime = TimeSpan.FromSeconds(Me._DwellTimeNumeric.Value)
        Me.ActiveInsulationResistance.VoltageLevel = Me._VoltageLevelNumeric.Value
        Me.ActiveInsulationResistance.CurrentLimit = 0.000001 * Me._CurrentLimitNumeric.Value
        Me.ActiveInsulationResistance.PowerLineCycles = Me._ApertureNumeric.Value
        Me.ActiveInsulationResistance.ContactCheckEnabled = Me._ContactCheckToggle.Checked
        Me.ActiveInsulationResistance.ResistanceLowLimit = 1000000.0 * Me._ResistanceLowLimitNumeric.Value
        Me.ActiveInsulationResistance.ResistanceRange = 1000000.0 * Me._ResistanceRangeNumeric.Value
    End Sub

    ''' <summary> Applies the hipot settings button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyHipotSettingsButton_Click(sender As Object, e As EventArgs) Handles _ApplyHipotSettingsButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying high potential settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.ApplyHipotSettings()
            Me.ConfigureHipot(Me.ActiveInsulationResistance)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Applies the sot settings button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySotSettingsButton_Click(sender As Object, e As EventArgs) Handles _ApplySotSettingsButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying trigger settings" : Me.PublishInfo($"{activity};. ")
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.ApplyHipotBinningInfo()
            Me.ApplyHipotSettings()
            Me.ConfigureHipotStartTest(Me.BinningInfo, Me.ActiveInsulationResistance)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
