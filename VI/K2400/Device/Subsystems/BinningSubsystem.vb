''' <summary> Defines the instrument Binning Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class BinningSubsystem
    Inherits VI.BinningSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="BinningSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " SYNTAX "

#Region " FEED SOURCE "

    ''' <summary> Gets or sets the feed source query command. </summary>
    ''' <value> The feed source query command. </value>
    Protected Overrides Property FeedSourceQueryCommand As String = ":CALC2:FEED?"

    ''' <summary> Gets or sets the feed source command format. </summary>
    ''' <value> The write feed source command format. </value>
    Protected Overrides Property FeedSourceCommandFormat As String = "CALC2:FEED {0}"

#End Region

#End Region

End Class
