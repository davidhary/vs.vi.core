''' <summary> Defines a SCPI Sense Resistance Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class SenseResistanceSubsystem
    Inherits VI.SenseResistanceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SenseResistanceSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ohm)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm

        MyBase.ResistanceRangeCurrents.Clear()
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0, 0, "Auto Range"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20, 0.00072D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 7.2 mA"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200, 0.00096D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000, 0.00096D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000, 0.000096D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 96 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200000, 0.0000096D, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 9.6 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000000, 0.0000019D, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.9 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000000, 0.0000014D, $"20 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.4 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200000000, 0.0000014D, $"200 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.4 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        MyBase.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(1000000000, 0.0000014D, $"1 G{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.4 {Arebis.StandardUnits.UnitSymbols.MU}A"))

    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.PowerLineCyclesRange = New isr.Core.Primitives.RangeR(0.01, 10)
        Dim model As String = Me.StatusSubsystem.VersionInfoBase.Model
        Select Case True
            Case model.StartsWith("2400", StringComparison.OrdinalIgnoreCase)
                Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 210000000.0)
            Case model.StartsWith("2410", StringComparison.OrdinalIgnoreCase)
                Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 210000000.0)
            Case model.StartsWith("242", StringComparison.OrdinalIgnoreCase)
                Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 21000000.0)
            Case model.StartsWith("243", StringComparison.OrdinalIgnoreCase)
                Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 21000000.0)
            Case model.StartsWith("244", StringComparison.OrdinalIgnoreCase)
                Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 21000000.0)
            Case Else
                Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 21000000.0)
        End Select
        Me.NotifyPropertyChanged(NameOf(VI.SenseFunctionSubsystemBase.FunctionModeRanges))
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm
        MyBase.DefineKnownResetState()
        Me.PowerLineCyclesRange = New isr.Core.Primitives.RangeR(0.01, 10)
        Me.FunctionRange = New isr.Core.Primitives.RangeR(0, 21000000.0)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:RES:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:RES:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:RES:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:RES:NPLC?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:RES:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SENS:RES:RANG?"

#End Region

#Region " CONFIGURATION MODE "

    ''' <summary> Gets or sets the Configuration Mode query command. </summary>
    ''' <value> The Configuration Mode query command. </value>
    Protected Overrides Property ConfigurationModeQueryCommand As String = ":SENS:RES:MODE?"

    ''' <summary> Gets or sets the Configuration Mode command format. </summary>
    ''' <value> The Configuration Mode command format. </value>
    Protected Overrides Property ConfigurationModeCommandFormat As String = ":SENS:RES:MODE {0}"

#End Region

#End Region

End Class
