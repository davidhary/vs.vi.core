Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements a Keithley 2400 source meter. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001. </para>
''' </remarks>
Public Class K2400Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="K2400Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As K2400Device
        Dim device As K2400Device = Nothing
        Try
            device = New K2400Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function


#Region " IDisposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(K2400Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindMeasureSubsystem(Nothing)
            Me.BindFormatSubsystem(Nothing)
            Me.BindOutputSubsystem(Nothing)
            Me.BindRouteSubsystem(Nothing)
            Me.BindSenseCurrentSubsystem(Nothing)
            Me.BindSenseVoltageSubsystem(Nothing)
            Me.BindSenseResistanceSubsystem(Nothing)
            Me.BindSenseSubsystem(Nothing)
            Me.BindSourceCurrentSubsystem(Nothing)
            Me.BindSourceVoltageSubsystem(Nothing)
            Me.BindSourceSubsystem(Nothing)
            Me.BindArmLayerSubsystem(Nothing)
            Me.BindTriggerSubsystem(Nothing)
            Me.BindBinningSubsystem(Nothing)
            Me.BindContactCheckLimit(Nothing)
            Me.BindComplianceLimit(Nothing)
            Me.BindCompositeLimit(Nothing)
            Me.BindDigitalOutput(Nothing)
            Me.BindUpperLowerLimit(Nothing)
            Me.BindSystemSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            ' better add before the format subsystem, which reset initializes the readings.
            Me.BindMeasureSubsystem(New MeasureSubsystem(Me.StatusSubsystem))
            ' the measure subsystem reading are initialized when the format system is reset
            Me.BindFormatSubsystem(New FormatSubsystem(Me.StatusSubsystem))
            Me.BindOutputSubsystem(New OutputSubsystem(Me.StatusSubsystem))
            Me.BindRouteSubsystem(New RouteSubsystem(Me.StatusSubsystem))
            Me.BindSenseCurrentSubsystem(New SenseCurrentSubsystem(Me.StatusSubsystem))
            Me.BindSenseVoltageSubsystem(New SenseVoltageSubsystem(Me.StatusSubsystem))
            Me.BindSenseResistanceSubsystem(New SenseResistanceSubsystem(Me.StatusSubsystem))
            Me.BindSenseSubsystem(New SenseSubsystem(Me.StatusSubsystem))
            Me.BindSourceCurrentSubsystem(New SourceCurrentSubsystem(Me.StatusSubsystem))
            Me.BindSourceVoltageSubsystem(New SourceVoltageSubsystem(Me.StatusSubsystem))
            Me.BindSourceSubsystem(New SourceSubsystem(Me.StatusSubsystem))
            Me.BindArmLayerSubsystem(New ArmLayerSubsystem(Me.StatusSubsystem))
            Me.BindTriggerSubsystem(New TriggerSubsystem(Me.StatusSubsystem))
            Me.BindBinningSubsystem(New BinningSubsystem(Me.StatusSubsystem))
            Me.BindContactCheckLimit(New ContactCheckLimit(Me.StatusSubsystem))
            Me.BindComplianceLimit(New ComplianceLimit(Me.StatusSubsystem))
            Me.BindCompositeLimit(New CompositeLimit(Me.StatusSubsystem))
            Me.BindDigitalOutput(New DigitalOutputSubsystem(Me.StatusSubsystem))
            Me.BindUpperLowerLimit(New UpperLowerLimit(Me.StatusSubsystem))
        End If
    End Sub

#End Region

#Region " LIMITS "

    ''' <summary> Gets or sets the composite limit. </summary>
    ''' <value> The composite limit. </value>
    Public ReadOnly Property CompositeLimit As CompositeLimit

    ''' <summary> Binds the composite limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="limit"> The limit. </param>
    Private Sub BindCompositeLimit(ByVal limit As CompositeLimit)
        If Me.CompositeLimit IsNot Nothing Then
            Me._CompositeLimit = Nothing
        End If
        Me._CompositeLimit = limit
    End Sub

    ''' <summary> Gets or sets the Compliance limit. </summary>
    ''' <value> The Compliance limit. </value>
    Public ReadOnly Property ComplianceLimit As ComplianceLimit

    ''' <summary> Binds the Compliance limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="limit"> The limit. </param>
    Private Sub BindComplianceLimit(ByVal limit As ComplianceLimit)
        If Me.ComplianceLimit IsNot Nothing Then
            Me._ComplianceLimit = Nothing
        End If
        Me._ComplianceLimit = limit
    End Sub

    ''' <summary> Gets or sets the Contact Check limit. </summary>
    ''' <value> The Contact Check limit. </value>
    Public ReadOnly Property ContactCheckLimit As ContactCheckLimit

    ''' <summary> Binds the Contact Check limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="limit"> The limit. </param>
    Private Sub BindContactCheckLimit(ByVal limit As ContactCheckLimit)
        If Me.ContactCheckLimit IsNot Nothing Then
            Me._ContactCheckLimit = Nothing
        End If
        Me._ContactCheckLimit = limit
    End Sub

    ''' <summary> Gets or sets the Upper-Lower limit. </summary>
    ''' <value> The Upper-Lower limit. </value>
    Public ReadOnly Property UpperLowerLimit As UpperLowerLimit

    ''' <summary> Binds the Upper-Lower limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="limit"> The limit. </param>
    Private Sub BindUpperLowerLimit(ByVal limit As UpperLowerLimit)
        If Me.UpperLowerLimit IsNot Nothing Then
            Me._UpperLowerLimit = Nothing
        End If
        Me._UpperLowerLimit = limit
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Gets or sets the arm layer subsystem. </summary>
    ''' <value> The arm layer subsystem. </value>
    Public ReadOnly Property ArmLayerSubsystem As ArmLayerSubsystem

    ''' <summary> Binds the Arm Layer subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindArmLayerSubsystem(ByVal subsystem As ArmLayerSubsystem)
        If Me.ArmLayerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ArmLayerSubsystem)
            Me._ArmLayerSubsystem = Nothing
        End If
        Me._ArmLayerSubsystem = subsystem
        If Me.ArmLayerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ArmLayerSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Binning subsystem. </summary>
    ''' <value> The Binning subsystem. </value>
    Public ReadOnly Property BinningSubsystem As BinningSubsystem

    ''' <summary> Binds the Binning subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindBinningSubsystem(ByVal subsystem As BinningSubsystem)
        If Me.BinningSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.BinningSubsystem)
            Me._BinningSubsystem = Nothing
        End If
        Me._BinningSubsystem = subsystem
        If Me.BinningSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.BinningSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Digital Output Subsystem. </summary>
    ''' <value> The Digital Output Subsystem. </value>
    Public ReadOnly Property DigitalOutput As DigitalOutputSubsystem

    ''' <summary> Binds the Digital Output subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindDigitalOutput(ByVal subsystem As DigitalOutputSubsystem)
        If Me.DigitalOutput IsNot Nothing Then
            Me.Subsystems.Remove(Me.DigitalOutput)
            Me._DigitalOutput = Nothing
        End If
        Me._DigitalOutput = subsystem
        If Me.DigitalOutput IsNot Nothing Then
            Me.Subsystems.Add(Me.DigitalOutput)
        End If
    End Sub

    ''' <summary> Gets or sets the Measure Subsystem. </summary>
    ''' <value> The Measure Subsystem. </value>
    Public ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Binds the Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureSubsystem(ByVal subsystem As MeasureSubsystem)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        Me._MeasureSubsystem = subsystem
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Output Subsystem. </summary>
    ''' <value> The Output Subsystem. </value>
    Public ReadOnly Property OutputSubsystem As OutputSubsystem

    ''' <summary> Binds the Output subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindOutputSubsystem(ByVal subsystem As OutputSubsystem)
        If Me.OutputSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.OutputSubsystem)
            Me._OutputSubsystem = Nothing
        End If
        Me._OutputSubsystem = subsystem
        If Me.OutputSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.OutputSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Route Subsystem. </summary>
    ''' <value> The Route Subsystem. </value>
    Public ReadOnly Property RouteSubsystem As RouteSubsystem

    ''' <summary> Binds the Route subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindRouteSubsystem(ByVal subsystem As RouteSubsystem)
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.RouteSubsystem)
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.RouteSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Current Subsystem. </summary>
    ''' <value> The Sense Current Subsystem. </value>
    Public ReadOnly Property SenseCurrentSubsystem As SenseCurrentSubsystem

    ''' <summary> Binds the Sense Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseCurrentSubsystem(ByVal subsystem As SenseCurrentSubsystem)
        If Me.SenseCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseCurrentSubsystem)
            Me._SenseCurrentSubsystem = Nothing
        End If
        Me._SenseCurrentSubsystem = subsystem
        If Me.SenseCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Resistance Subsystem. </summary>
    ''' <value> The Sense Resistance Subsystem. </value>
    Public ReadOnly Property SenseResistanceSubsystem As SenseResistanceSubsystem

    ''' <summary> Binds the Sense Resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseResistanceSubsystem(ByVal subsystem As SenseResistanceSubsystem)
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseResistanceSubsystem)
            Me._SenseResistanceSubsystem = Nothing
        End If
        Me._SenseResistanceSubsystem = subsystem
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseResistanceSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Voltage Subsystem. </summary>
    ''' <value> The Sense Voltage Subsystem. </value>
    Public ReadOnly Property SenseVoltageSubsystem As SenseVoltageSubsystem

    ''' <summary> Binds the Sense Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseVoltageSubsystem(ByVal subsystem As SenseVoltageSubsystem)
        If Me.SenseVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseVoltageSubsystem)
            Me._SenseVoltageSubsystem = Nothing
        End If
        Me._SenseVoltageSubsystem = subsystem
        If Me.SenseVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseVoltageSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Subsystem. </summary>
    ''' <value> The Sense Subsystem. </value>
    Public ReadOnly Property SenseSubsystem As SenseSubsystem

    ''' <summary> Binds the Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseSubsystem(ByVal subsystem As SenseSubsystem)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        Me._SenseSubsystem = subsystem
        If Me.SenseSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Source Current Subsystem. </summary>
    ''' <value> The Source Current Subsystem. </value>
    Public ReadOnly Property SourceCurrentSubsystem As SourceCurrentSubsystem

    ''' <summary> Binds the Source Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceCurrentSubsystem(ByVal subsystem As SourceCurrentSubsystem)
        If Me.SourceCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SourceCurrentSubsystem)
            Me._SourceCurrentSubsystem = Nothing
        End If
        Me._SourceCurrentSubsystem = subsystem
        If Me.SourceCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Source Voltage Subsystem. </summary>
    ''' <value> The Source Voltage Subsystem. </value>
    Public ReadOnly Property SourceVoltageSubsystem As SourceVoltageSubsystem

    ''' <summary> Binds the Source Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceVoltageSubsystem(ByVal subsystem As SourceVoltageSubsystem)
        If Me.SourceVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SourceVoltageSubsystem)
            Me._SourceVoltageSubsystem = Nothing
        End If
        Me._SourceVoltageSubsystem = subsystem
        If Me.SourceVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceVoltageSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Source Subsystem. </summary>
    ''' <value> The Source Subsystem. </value>
    Public ReadOnly Property SourceSubsystem As SourceSubsystem

    ''' <summary> Binds the Source subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceSubsystem(ByVal subsystem As SourceSubsystem)
        If Me.SourceSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SourceSubsystem)
            Me._SourceSubsystem = Nothing
        End If
        Me._SourceSubsystem = subsystem
        If Me.SourceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Trigger Subsystem. </summary>
    ''' <value> The Trigger Subsystem. </value>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Binds the Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystem)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TriggerSubsystem)
        End If
    End Sub

#Region " FORMAT "

    ''' <summary> Gets or sets the Format Subsystem. </summary>
    ''' <value> The Format Subsystem. </value>
    Public ReadOnly Property FormatSubsystem As FormatSubsystem

    ''' <summary> Binds the Format subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindFormatSubsystem(ByVal subsystem As FormatSubsystem)
        If Me.FormatSubsystem IsNot Nothing Then
            RemoveHandler Me.FormatSubsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.FormatSubsystem)
            Me._FormatSubsystem = Nothing
        End If
        Me._FormatSubsystem = subsystem
        If Me.FormatSubsystem IsNot Nothing Then
            AddHandler Me.FormatSubsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            Me.Subsystems.Add(Me.FormatSubsystem)
            Me.HandlePropertyChanged(Me.FormatSubsystem, NameOf(VI.FormatSubsystemBase.Elements))
        End If
    End Sub

    ''' <summary> Handle the Format subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As FormatSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.FormatSubsystemBase.Elements)
                Me.MeasureSubsystem.ReadingAmounts.Initialize(subsystem.Elements)
        End Select
    End Sub

    ''' <summary> Format subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FormatSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(FormatSubsystem)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, FormatSubsystem), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Binds the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            RemoveHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            AddHandler Me.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            Me.Subsystems.Add(Me.SystemSubsystem)
            Me.HandlePropertyChanged(Me.SystemSubsystem, NameOf(VI.K2400.SystemSubsystem.Options))
        End If
    End Sub

    ''' <summary> Handles the Status subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As SystemSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K2400.SystemSubsystem.Options)
                ' read the contact check option after reading options.
                subsystem.QueryContactCheckSupported()
        End Select
    End Sub

    ''' <summary> System subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Try
            Me.HandlePropertyChanged(TryCast(sender, SystemSubsystem), e?.PropertyName)
        Catch ex As Exception
            Me.Talker.Publish(TraceEventType.Error, My.MyLibrary.TraceEventId,
                               "Exception handling property '{0}' changed event;. {1}", e.PropertyName, ex.ToFullBlownString)
        End Try
    End Sub

#End Region

#End Region

#Region " MIXED SUBSYSTEMS: OUTPUT / MEASURE "

    ''' <summary> Sets the specified voltage and current limit and turns on the output. </summary>
    ''' <remarks> See 2400 system manual page 102 of 592. </remarks>
    ''' <param name="voltage">          The voltage. </param>
    ''' <param name="currentLimit">     The current limit. </param>
    ''' <param name="overvoltageLimit"> The over voltage limit. </param>
    Public Sub OutputOn(ByVal voltage As Double, ByVal currentLimit As Double, ByVal overvoltageLimit As Double)

        ' turn output off.
        Me.OutputSubsystem.ApplyOutputOnState(False)

        Me.SourceSubsystem.WriteFunctionMode(VI.SourceFunctionModes.VoltageDC)

        ' Set the voltage
        Me.SourceVoltageSubsystem.ApplyLevel(voltage)

        ' Set the over voltage level
        Me.SourceVoltageSubsystem.ApplyProtectionLevel(overvoltageLimit)

        ' Turn on over current protection
        Me.SenseCurrentSubsystem.ApplyProtectionLevel(currentLimit)

        Me.SenseSubsystem.FunctionModes = VI.SenseFunctionModes.Current Or VI.SenseFunctionModes.Voltage
        Me.SenseSubsystem.SupportsMultiFunctions = True

        Me.SenseSubsystem.ApplyConcurrentSenseEnabled(True)

        ' turn output on.
        Me.OutputSubsystem.ApplyOutputOnState(True)

        Me.Session.QueryOperationCompleted()

        ' operation completion is not sufficient to ensure that the Device has hit the correct voltage.

    End Sub

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("K2400 Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.MySettings.ReadDelay)
                Me.Session.ReadDelay = TimeSpan.FromMilliseconds(sender.ReadDelay)
            Case NameOf(My.MySettings.StatusReadDelay)
                Me.Session.StatusReadDelay = TimeSpan.FromMilliseconds(sender.StatusReadDelay)
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

