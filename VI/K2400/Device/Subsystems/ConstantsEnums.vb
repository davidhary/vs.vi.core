#Region " TYPES "

''' <summary> Gets or sets the status byte flags of the measurement event register. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum MeasurementEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the limit 1 failed option. </summary>
    <ComponentModel.Description("Limit 1 Failed")>
    Limit1Failed = 1

    ''' <summary> An enum constant representing the low limit 2 failed option. </summary>
    <ComponentModel.Description("Low Limit 2 Failed")>
    LowLimit2Failed = 2

    ''' <summary> An enum constant representing the high limit 2 failed option. </summary>
    <ComponentModel.Description("High Limit 2 Failed")>
    HighLimit2Failed = 4

    ''' <summary> An enum constant representing the low limit 3 failed option. </summary>
    <ComponentModel.Description("Low Limit 3 Failed")>
    LowLimit3Failed = 8

    ''' <summary> An enum constant representing the high limit 3 failed option. </summary>
    <ComponentModel.Description("High Limit 3 Failed")>
    HighLimit3Failed = 16

    ''' <summary> An enum constant representing the limits passed option. </summary>
    <ComponentModel.Description("Limits Passed")>
    LimitsPassed = 32

    ''' <summary> An enum constant representing the reading available option. </summary>
    <ComponentModel.Description("Reading Available")>
    ReadingAvailable = 64

    ''' <summary> An enum constant representing the reading overflow option. </summary>
    <ComponentModel.Description("Reading Overflow")>
    ReadingOverflow = 128

    ''' <summary> An enum constant representing the buffer available option. </summary>
    <ComponentModel.Description("Buffer Available")>
    BufferAvailable = 256

    ''' <summary> An enum constant representing the buffer full option. </summary>
    <ComponentModel.Description("Buffer Full")>
    BufferFull = 512

    ''' <summary> An enum constant representing the contact check failed option. </summary>
    <ComponentModel.Description("Contact Check Failed")>
    ContactCheckFailed = 1024

    ''' <summary> An enum constant representing the interlock asserted option. </summary>
    <ComponentModel.Description("Interlock Asserted")>
    InterlockAsserted = 2048

    ''' <summary> An enum constant representing the over temperature option. </summary>
    <ComponentModel.Description("Over Temperature")>
    OverTemperature = 4096

    ''' <summary> An enum constant representing the overvoltage protection option. </summary>
    <ComponentModel.Description("Over Voltage Protection")>
    OvervoltageProtection = 8192

    ''' <summary> An enum constant representing the compliance option. </summary>
    <ComponentModel.Description("Compliance")>
    Compliance = 16384

    ''' <summary> An enum constant representing the not used option. </summary>
    <ComponentModel.Description("Not Used")>
    NotUsed = 32768

    ''' <summary> An enum constant representing all option. </summary>
    <ComponentModel.Description("All")>
    All = 32767
End Enum

''' <summary> Enumerates the status bits of a source measure status word. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum StatusWordBit

    ''' <summary>Measurement was made while in over-range</summary>
    <ComponentModel.Description("Over Range")>
    OverRange = 0

    ''' <summary> An enum constant representing the filter enabled option. </summary>
    <ComponentModel.Description("Filter Enabled")>
    FilterEnabled = 1

    ''' <summary> An enum constant representing the front terminals option. </summary>
    <ComponentModel.Description("Front Terminals")>
    FrontTerminals = 2

    ''' <summary> An enum constant representing the hit compliance option. </summary>
    <ComponentModel.Description("Hit Compliance")>
    HitCompliance = 3

    ''' <summary> An enum constant representing the hit voltage protection option. </summary>
    <ComponentModel.Description("Hit Voltage Protection")>
    HitVoltageProtection = 4

    ''' <summary> An enum constant representing the Mathematics expression enabled option. </summary>
    <ComponentModel.Description("Math Expression Enabled")>
    MathExpressionEnabled = 5

    ''' <summary> An enum constant representing the null enabled option. </summary>
    <ComponentModel.Description("Null Enabled")>
    NullEnabled = 6

    ''' <summary> An enum constant representing the limits enabled option. </summary>
    <ComponentModel.Description("Limits Enabled")>
    LimitsEnabled = 7

    ''' <summary> An enum constant representing the limit result bit 0 option. </summary>
    <ComponentModel.Description("Limit Result Bit 0")>
    LimitResultBit0 = 8

    ''' <summary> An enum constant representing the limit result bit 1 option. </summary>
    <ComponentModel.Description("Limit Result Bit 1")>
    LimitResultBit1 = 9

    ''' <summary> An enum constant representing the Automatic ohms enabled option. </summary>
    <ComponentModel.Description("Auto Ohms Enabled")>
    AutoOhmsEnabled = 10

    ''' <summary> An enum constant representing the voltage measure enabled option. </summary>
    <ComponentModel.Description("Voltage Measure Enabled")>
    VoltageMeasureEnabled = 11

    ''' <summary> An enum constant representing the current measure enabled option. </summary>
    <ComponentModel.Description("Current Measure Enabled")>
    CurrentMeasureEnabled = 12

    ''' <summary> An enum constant representing the resistance measure enabled option. </summary>
    <ComponentModel.Description("Resistance Measure Enabled")>
    ResistanceMeasureEnabled = 13

    ''' <summary> An enum constant representing the voltage source used option. </summary>
    <ComponentModel.Description("Voltage Source Used")>
    VoltageSourceUsed = 14

    ''' <summary> An enum constant representing the current source used option. </summary>
    <ComponentModel.Description("Current Source Used")>
    CurrentSourceUsed = 15

    ''' <summary> An enum constant representing the hit range compliance option. </summary>
    <ComponentModel.Description("Hit Range Compliance")>
    HitRangeCompliance = 16

    ''' <summary> An enum constant representing the offset compensation ohms enabled option. </summary>
    <ComponentModel.Description("Offset Compensation Ohms Enabled")>
    OffsetCompensationOhmsEnabled = 17

    ''' <summary> An enum constant representing the failed contact check option. </summary>
    <ComponentModel.Description("Failed Contact Check")>
    FailedContactCheck = 18

    ''' <summary> An enum constant representing the limit result bit 2 option. </summary>
    <ComponentModel.Description("Limit Result Bit 2")>
    LimitResultBit2 = 19

    ''' <summary> An enum constant representing the limit result bit 3 option. </summary>
    <ComponentModel.Description("Limit Result Bit 3")>
    LimitResultBit3 = 20

    ''' <summary> An enum constant representing the limit result bit 4 option. </summary>
    <ComponentModel.Description("Limit Result Bit 4")>
    LimitResultBit4 = 21 '1048576

    ''' <summary> An enum constant representing the four wire enabled option. </summary>
    <ComponentModel.Description("Four Wire Enabled")>
    FourWireEnabled = 22

    ''' <summary> An enum constant representing the in pulse mode option. </summary>
    <ComponentModel.Description("In Pulse Mode")>
    InPulseMode = 23
End Enum

#End Region

