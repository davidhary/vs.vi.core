''' <summary> Defines a SCPI Sense Current Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class SenseCurrentSubsystem
    Inherits VI.SenseFunctionSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SenseCurrentSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ampere)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.PowerLineCyclesRange = New isr.Core.Primitives.RangeR(0.01, 10)
        Dim model As String = Me.StatusSubsystem.VersionInfoBase.Model
        Select Case True
            Case model.StartsWith("2400", StringComparison.OrdinalIgnoreCase)
                Me.FunctionModeRanges(SenseFunctionModes.CurrentDC).SetRange(0.001, 1.05)
                Me.FunctionModeRanges(SenseFunctionModes.VoltageDC).SetRange(0.001, 40)
            Case model.StartsWith("2410", StringComparison.OrdinalIgnoreCase)
                Me.FunctionModeRanges(SenseFunctionModes.CurrentDC).SetRange(0.001, 1.05)
                Me.FunctionModeRanges(SenseFunctionModes.VoltageDC).SetRange(0.001, 500)
            Case model.StartsWith("242", StringComparison.OrdinalIgnoreCase)
                Me.FunctionModeRanges(SenseFunctionModes.CurrentDC).SetRange(0.001, 3.05)
            Case model.StartsWith("243", StringComparison.OrdinalIgnoreCase)
                Me.FunctionModeRanges(SenseFunctionModes.CurrentDC).SetRange(0.001, 3.15)
                Me.FunctionModeRanges(SenseFunctionModes.VoltageDC).SetRange(0.001, 40)
            Case model.StartsWith("244", StringComparison.OrdinalIgnoreCase)
                Me.FunctionModeRanges(SenseFunctionModes.CurrentDC).SetRange(0.001, 5.25)
                Me.FunctionModeRanges(SenseFunctionModes.VoltageDC).SetRange(0.001, 40)
            Case Else
                Me.FunctionModeRanges(SenseFunctionModes.CurrentDC).SetRange(0.001, 1.05)
                Me.FunctionModeRanges(SenseFunctionModes.VoltageDC).SetRange(0.001, 40)
        End Select
        Me.NotifyPropertyChanged(NameOf(VI.SenseFunctionSubsystemBase.FunctionModeRanges))
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PowerLineCyclesRange = New isr.Core.Primitives.RangeR(0.01, 10)
        Me.FunctionRange = New isr.Core.Primitives.RangeR(0.001, 1.05)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:CURR:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:CURR:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:CURR:NPLC?"

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overrides Property ProtectionLevelCommandFormat As String = ":SENS:CURR:PROT {0}"

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overrides Property ProtectionLevelQueryCommand As String = ":SENS:CURR:PROT?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:CURR:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SENS:CURR:RANG?"

#End Region

#End Region

End Class
