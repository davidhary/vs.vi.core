﻿''' <summary> Information about the version of a Keithley 2400 instrument. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class VersionInfo
    Inherits isr.VI.VersionInfoBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()

        Me._FirmwareLevelMajorRevision = String.Empty
        Me._FirmwareLevelMinorRevision = String.Empty
        Me._FirmwareLevelDate = Date.MinValue
        Me._ContactCheckSupported = False
    End Sub

    ''' <summary> Returns the instrument firmware level major revision name . </summary>
    ''' <value> The firmware level major revision. </value>
    Public ReadOnly Property FirmwareLevelMajorRevision() As String

    ''' <summary> Gets or sets the firmware level minor revision. </summary>
    ''' <value> The firmware level minor revision. </value>
    Public ReadOnly Property FirmwareLevelMinorRevision() As String

    ''' <summary> Returns the instrument firmware level date. </summary>
    ''' <value> The firmware level date. </value>
    Public ReadOnly Property FirmwareLevelDate() As DateTimeOffset

    ''' <summary> Returns True if the instrument supports contact check. </summary>
    ''' <value> The contact check supported. </value>
    Public ReadOnly Property ContactCheckSupported() As Boolean

    ''' <summary> Parses the instrument firmware revision. </summary>
    ''' <remarks>
    ''' Identity: <para>
    ''' <c>KEITHLEY INSTRUMENTS INC.,MODEL 2420,0669977,C11 Oct 10 1997 09:51:36/A02
    ''' /D/B/E.</c>.</para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="revision"> Specifies the instrument firmware revision with the following items:
    '''                         <see cref="FirmwareLevelMajorRevision">firmware level major
    '''                         revision</see>
    '''                         <see cref="FirmwareLevelDate">date</see>
    '''                         <see cref="FirmwareLevelMinorRevision">firmware level minor
    '''                         revision</see>
    '''                         <see cref="FirmwareRevisionElements">board revisions</see>
    '''                         e.g., <c>C11 Oct 10 1997 09:51:36/A02 /D/B/E.</c>.  
    '''                         The last three letters separated by "/" indicate the board revisions
    '''                         (i.e. /Analog/Digital/Contact Check). Contact Check board revisions
    '''                         have the following features:<p>
    '''                         Revisions A through C have only one resistance range.</p><p>
    '''                         Revisions D and above have selectable resistance ranges.</p> </param>
    Protected Overrides Sub ParseFirmwareRevision(ByVal revision As String)

        If revision Is Nothing Then
            Throw New ArgumentNullException(NameOf(revision))
        ElseIf String.IsNullOrWhiteSpace(revision) Then
            MyBase.ParseFirmwareRevision(revision)
        Else
            MyBase.ParseFirmwareRevision(revision)

            ' get the revision sections
            Dim revSections As Queue(Of String) = New Queue(Of String)(revision.Split("/"c))

            ' Rev: C11; revision: Oct 10 1997 09:51:36
            Dim firmwareLevel As String = revSections.Dequeue.Trim
            Me._FirmwareLevelMajorRevision = firmwareLevel.Split(" "c)(0)

            ' date string: Oct 10 1997 09:51:36
            If Not DateTimeOffset.TryParse(firmwareLevel.Substring(Me._FirmwareLevelMajorRevision.Length).Trim,
                                           Globalization.CultureInfo.InvariantCulture, Globalization.DateTimeStyles.AssumeLocal,
                                           Me._FirmwareLevelDate) Then
                Me._FirmwareLevelDate = DateTimeOffset.MinValue
            End If

            ' Minor revision: A02
            Me._FirmwareLevelMinorRevision = revSections.Dequeue.Trim

            If revSections.Any Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.Analog.ToString, revSections.Dequeue.Trim)
            If revSections.Any Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.Digital.ToString, revSections.Dequeue.Trim)
            If revSections.Any Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.ContactCheck.ToString, revSections.Dequeue.Trim)
            Me._ContactCheckSupported = Me.FirmwareRevisionElements.ContainsKey(FirmwareRevisionElement.ContactCheck.ToString)

        End If

    End Sub

End Class

