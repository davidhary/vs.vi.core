''' <summary> Defines a SCPI Source Voltage Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class SourceVoltageSubsystem
    Inherits VI.SourceVoltageSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceVoltageSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    Public Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Dim model As String = CType(Me.StatusSubsystem, StatusSubsystem).VersionInfo.Model
        Dim voltageRange As Core.Primitives.RangeR
        Dim currentRange As Core.Primitives.RangeR
        Select Case True
            Case model.StartsWith("2400", StringComparison.OrdinalIgnoreCase)
                voltageRange = New Core.Primitives.RangeR(-210, +210)
                currentRange = New Core.Primitives.RangeR(-1.05, +1.05)
            Case model.StartsWith("2410", StringComparison.OrdinalIgnoreCase)
                voltageRange = New Core.Primitives.RangeR(-1100, +1100)
                currentRange = New Core.Primitives.RangeR(-1.05, +1.05)
            Case model.StartsWith("2420", StringComparison.OrdinalIgnoreCase)
                voltageRange = New Core.Primitives.RangeR(-63, +63)
                currentRange = New Core.Primitives.RangeR(-3.15, +3.05)
            Case model.StartsWith("2425", StringComparison.OrdinalIgnoreCase)
                voltageRange = New Core.Primitives.RangeR(-105, +105)
                currentRange = New Core.Primitives.RangeR(-1.05, +1.05)
            Case model.StartsWith("243", StringComparison.OrdinalIgnoreCase)
                voltageRange = New Core.Primitives.RangeR(-105, +105)
                currentRange = New Core.Primitives.RangeR(-3.15, +3.15)
            Case model.StartsWith("244", StringComparison.OrdinalIgnoreCase)
                voltageRange = New Core.Primitives.RangeR(-42, +42)
                currentRange = New Core.Primitives.RangeR(-5.25, +5.25)
            Case Else
                voltageRange = New Core.Primitives.RangeR(-210, +210)
                currentRange = New Core.Primitives.RangeR(-1.05, +1.05)
        End Select
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.Voltage) = voltageRange
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.VoltageDC) = voltageRange
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.Current) = currentRange
        Me.FunctionModeRanges.Item(VI.SourceFunctionModes.CurrentDC) = currentRange
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " PROTECTION LEVEL "

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overrides Property ProtectionLevelCommandFormat As String = ":SOUR:VOLT:PROT:LEV {0}"

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overrides Property ProtectionLevelQueryCommand As String = ":SOUR:VOLT:PROT:LEV?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SOUR:VOLT:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SOUR:VOLT:RANG?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SOUR:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SOUR:FUNC?"

#End Region

#End Region

End Class
