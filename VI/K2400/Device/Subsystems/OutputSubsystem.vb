''' <summary> Defines a SCPI Output Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class OutputSubsystem
    Inherits VI.OutputSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="OutputSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "



#End Region

#Region " COMMAND SYNTAX "

#Region " OFF MODE "

    ''' <summary> Gets or sets the Output Off Mode query command. </summary>
    ''' <value> The Off Mode query command. </value>
    Protected Overrides Property OutputOffModeQueryCommand As String = ":OUTP:SMOD?"

    ''' <summary> Gets or sets the output Off Mode command format. </summary>
    ''' <value> The write Off Mode command format. </value>
    Protected Overrides Property OutputOffModeCommandFormat As String = ":OUTP:SMOD {0}"

#End Region

#Region " ON/OFF STATE "

    ''' <summary> Gets or sets the output on state query command. </summary>
    ''' <value> The output on state query command. </value>
    Protected Overrides Property OutputOnStateQueryCommand As String = ":OUTP:STAT?"

    ''' <summary> Gets or sets the output on state command format. </summary>
    ''' <value> The output on state command format. </value>
    Protected Overrides Property OutputOnStateCommandFormat As String = ":OUTP:STAT {0:'1';'1';'0'}"

#End Region

#Region " TERMINAL MODE "

    ''' <summary> Gets or sets the terminals mode query command. </summary>
    ''' <value> The terminals mode query command. </value>
    Protected Overrides Property OutputTerminalsModeQueryCommand As String = ":OUTP:ROUT:TERM?"

    ''' <summary> Gets or sets the terminals mode command format. </summary>
    ''' <value> The terminals mode command format. </value>
    Protected Overrides Property OutputTerminalsModeCommandFormat As String = ":OUTP:ROUT:TERM {0}"

#End Region

#End Region

End Class
