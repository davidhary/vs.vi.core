
Partial Public Class SourceSubsystem

#Region " AUTO CLEAR ENABLED "

    ''' <summary> Gets or sets the automatic Clear enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR:CLE:AUTO?". </remarks>
    ''' <value> The automatic Clear enabled query command. </value>
    Protected Overrides Property AutoClearEnabledQueryCommand As String = ":SOUR:CLE:AUTO?"

    ''' <summary> Gets or sets the automatic Clear enabled command Format. </summary>
    ''' <value> The automatic Clear enabled query command. </value>
    Protected Overrides Property AutoClearEnabledCommandFormat As String = ":SOUR:CLE:AUTO {0:'ON';'ON';'OFF'}"

#End Region

#Region " AUTO DELAY ENABLED "

    ''' <summary> Gets or sets the automatic Delay enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR:CLE:AUTO?". </remarks>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledQueryCommand As String = ":SOUR:DEL:AUTO?"

    ''' <summary> Gets or sets the automatic Delay enabled command Format. </summary>
    ''' <value> The automatic Delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledCommandFormat As String = ":SOUR:DEL:AUTO {0:'ON';'ON';'OFF'}"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = ":SOUR:DEL {0:s\.FFFFFFF}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = ":SOUR:DEL?"

#End Region

#Region " FUNCTION MODE "
    ' 2018 12 27 not sure why these are commented out

    ' <summary> Gets or sets the function mode query command. </summary>
    ' <value> The function mode query command, e.g., :SOUR:FUNC? </value>
    ' Protected Overrides Property FunctionModeQueryCommand As String = ":SOUR:FUNC?"

    ' <summary> Gets or sets the function mode command. </summary>
    ' <value> The function mode command, e.g., :SOUR:FUNC {0}. </value>
    ' Protected Overrides Property FunctionModeCommandFormat As String = ":SOUR:FUNC {0}"

#End Region

#Region " SWEEP POINTS "

    ''' <summary> Gets or sets Sweep Points query command. </summary>
    ''' <value> The Sweep Points query command. </value>
    Protected Overrides Property SweepPointsQueryCommand As String = ":SOUR:SWE:POIN?"

    ''' <summary> Gets or sets Sweep Points command format. </summary>
    ''' <value> The Sweep Points command format. </value>
    Protected Overrides Property SweepPointsCommandFormat As String = ":SOUR:SWE:POIN {0}"

#End Region

End Class

