''' <summary>
''' Defines the contract that must be implemented by a SCPI Digital Output Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class DigitalOutputSubsystem
    Inherits VI.DigitalOutputSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DigitalOutputSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " SYNTAX "

    ''' <summary> Gets or sets the digital output clear command. </summary>
    ''' <remarks> SCPI: ":SOUR2:CLE". </remarks>
    ''' <value> The digital output clear command. </value>
    Protected Overrides Property ClearCommand As String = ":SOUR2:CLE"

    ''' <summary> Gets or sets the digital output Auto Clear enabled query command. </summary>
    ''' <remarks> SCPI: ":SOUR2:CLE:AUTO?". </remarks>
    ''' <value> The digital output Auto Clear enabled query command. </value>
    Protected Overrides Property AutoClearEnabledQueryCommand As String = ":SOUR2:CLE:AUTO?"

    ''' <summary> Gets or sets the digital output Auto Clear enabled command Format. </summary>
    ''' <remarks> SCPI: ":SOUR2:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The digital output Auto Clear enabled query command. </value>
    Protected Overrides Property AutoClearEnabledCommandFormat As String = ":SOUR2:CLE:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the digital output Delay query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property DelayQueryCommand As String = ":SOUR2:CLE:AUTO:DEL?"

    ''' <summary> Gets or sets the digital output Delay query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property DelayCommandFormat As String = ":SOUR2:CLE:AUTO:DEL {0}"

    ''' <summary> Gets or sets the digital output Bit Size query command. </summary>
    ''' <value> The 'bit size query' command. </value>
    Protected Overrides Property BitSizeQueryCommand As String = ":SOUR2:BSIZ?"

    ''' <summary> Gets or sets the digital output Bit Size query command. </summary>
    ''' <value> The bit size command format. </value>
    Protected Overrides Property BitSizeCommandFormat As String = ":SOUR2:BSIZ {0}"

    ''' <summary> Gets or sets the digital output Level query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LevelQueryCommand As String = ":SOUR2:TTL:ACT?"

    ''' <summary> Gets or sets the digital output Level query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LevelCommandFormat As String = ":SOUR2:TTL:LEV {0}"

    ''' <summary> Gets or sets the Output Mode query command. </summary>
    ''' <remarks> SCPI: ":SOUR2:TTL4:MODE?". </remarks>
    ''' <value> The Output Mode query command. </value>
    Protected Overrides Property OutputModeQueryCommand As String = ":SOUR2:TTL4:MODE?"

    ''' <summary> Gets or sets the Output Mode command format. </summary>
    ''' <remarks> SCPI: ":SOUR2:TTL4:MODE {0}". </remarks>
    ''' <value> The Output Mode query command format. </value>
    Protected Overrides Property OutputModeCommandFormat As String = ":SOUR2:TTL4:MODE {0}"

    ''' <summary> Gets or sets the Output Polarity query command. </summary>
    ''' <remarks> SCPI: "SOUR2:TTL4:BST?". </remarks>
    ''' <value> The Output Polarity query command. </value>
    Protected Overrides Property DigitalActiveLevelQueryCommand As String = "SOUR2:TTL4:BST?"

    ''' <summary> Gets or sets the Output Polarity command format. </summary>
    ''' <remarks> SCPI: "SOUR2:TTL4:BST {0}". </remarks>
    ''' <value> The Output Polarity command format. </value>
    Protected Overrides Property DigitalActiveLevelCommandFormat As String = "SOUR2:TTL4:BST {0}"

#End Region

End Class
