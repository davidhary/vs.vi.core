''' <summary> Defines a SCPI Measure Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class MeasureSubsystem
    Inherits VI.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ampere)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " READ, FETCH "

    ''' <summary> Gets or sets the fetch command. </summary>
    ''' <value> The fetch command. </value>
    Protected Overrides Property FetchCommand As String = ":FETCH?"

    ''' <summary> Gets or sets the read command. </summary>
    ''' <value> The read command. </value>
    Protected Overrides Property ReadCommand As String = ":READ?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#End Region

End Class

