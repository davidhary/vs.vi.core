Imports isr.Core.EnumExtensions

''' <summary> Defines the contract that must be implemented by a Sense Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific ReSenses, Inc.<para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public MustInherit Class SenseSubsystemBase
    Inherits VI.SenseSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystemBase" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading Or ReadingElementTypes.Voltage Or ReadingElementTypes.Current)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ampere)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ampere
    End Sub

#End Region

#Region " CONCURRENT SENSE FUNCTION MODE "

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <remarks> SCPI: ":SENS:FUNC:CONC?". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property ConcurrentSenseEnabledQueryCommand As String = ":SENS:FUNC:CONC?"

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <remarks> SCPI: ":SENS:FUNC:CONC {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property ConcurrentSenseEnabledCommandFormat As String = ":SENS:FUNC:CONC {0:'ON';'ON';'OFF'}"

#End Region

#Region " LATEST DATA "

    ''' <summary> Gets or sets the latest data query command. </summary>
    ''' <remarks> SCPI: ":SENSE:DATA:LAT?". </remarks>
    ''' <value> The latest data query command. </value>
    Protected Overrides Property LatestDataQueryCommand As String = ":SENSE:DATA:LAT?"

#End Region

#Region " FUNCTION MODES "

    ''' <summary> Builds the Modes record for the specified Modes. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="modes"> Sense Function Modes. </param>
    ''' <returns> The record. </returns>
    Public Shared Function BuildRecord(ByVal modes As VI.SenseFunctionModes) As String
        If modes = VI.SenseFunctionModes.None Then
            Return String.Empty
        Else
            Dim reply As New System.Text.StringBuilder
            For Each code As Integer In [Enum].GetValues(GetType(VI.SenseFunctionModes))
                If (modes And code) <> 0 Then
                    Dim value As String = CType(code, VI.SenseFunctionModes).ExtractBetween()
                    If Not String.IsNullOrWhiteSpace(value) Then
                        If reply.Length > 0 Then
                            reply.Append(",")
                        End If
                        reply.Append(value)
                    End If
                End If
            Next
            Return reply.ToString
        End If
    End Function

    ''' <summary>
    ''' Get the composite Sense Function Modes based on the message from the instrument.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="record"> Specifies the comma delimited Modes record. </param>
    ''' <returns> The sense function modes. </returns>
    Public Shared Function ParseSenseFunctionModes(ByVal record As String) As SenseFunctionModes
        Dim parsed As SenseFunctionModes = SenseFunctionModes.None
        If Not String.IsNullOrWhiteSpace(record) Then
            For Each modeValue As String In record.Split(","c)
                parsed = parsed Or SenseSubsystemBase.ParseSenseFunctionMode(modeValue)
            Next
        End If
        Return parsed
    End Function

    ''' <summary>
    ''' Returns the <see cref="SenseFunctionModes"></see> from the specified value.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The Modes. </param>
    ''' <returns> The sense function mode. </returns>
    Public Shared Function ParseSenseFunctionMode(ByVal value As String) As SenseFunctionModes
        If String.IsNullOrWhiteSpace(value) Then
            Return SenseFunctionModes.None
        Else
            Dim se As New isr.Core.StringEnumerator(Of SenseFunctionModes)
            Return se.ParseContained(value.BuildDelimitedValue)
        End If
    End Function

    ''' <summary> The Sense Function Modes. </summary>
    Private _FunctionModes As VI.SenseFunctionModes?

    ''' <summary> Gets or sets the cached Sense Function Modes. </summary>
    ''' <value> The Function Modes or null if unknown. </value>
    Public Property FunctionModes As VI.SenseFunctionModes?
        Get
            Return Me._FunctionModes
        End Get
        Set(ByVal value As VI.SenseFunctionModes?)
            If Not Nullable.Equals(Me.FunctionModes, value) Then
                Me._FunctionModes = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Sense function mode query command. </summary>
    ''' <value> The Sense function mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = String.Empty ' not supported

    ''' <summary> Gets or sets the Sense function mode command format. </summary>
    ''' <value> The Sense function mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = String.Empty ' not supported

    ''' <summary> True to supports multi functions. </summary>
    Private _SupportsMultiFunctions As Boolean

    ''' <summary>
    ''' Gets or sets the condition telling if the instrument supports multi-functions. For example,
    ''' the 2400 source-measure instrument support measuring voltage, current, and resistance
    ''' concurrently whereas the 2700 supports a single function at a time.
    ''' </summary>
    ''' <value> The supports multi functions. </value>
    Public Property SupportsMultiFunctions() As Boolean
        Get
            Return Me._SupportsMultiFunctions
        End Get
        Set(ByVal value As Boolean)
            If Not Me.SupportsMultiFunctions.Equals(value) Then
                Me._SupportsMultiFunctions = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets The Range query command. </summary>
    ''' <value> The Range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SOUR:RANG?"

    ''' <summary> Gets or sets The Range command format. </summary>
    ''' <value> The Range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:RANG {0}"

#End Region

End Class

