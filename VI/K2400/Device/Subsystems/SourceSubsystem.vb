''' <summary> Defines a SCPI Source Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class SourceSubsystem
    Inherits VI.SourceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SourceSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        ' TO_DO: Implements elements from Source Subsystem base to define the range and supported function and use the 
        ' proper function calls to write and query the function mode.
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        MyBase.DefineKnownResetState()
        Me.FunctionMode = SourceFunctionModes.Voltage
        Me.SupportedFunctionModes = SourceFunctionModes.Current Or SourceFunctionModes.Voltage
    End Sub

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Clears the function state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub DefineFunctionClearKnownState()
        MyBase.DefineFunctionClearKnownState()
        Me.ParseReadBackAmount(String.Empty)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SOUR:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SOUR:FUNC?"

#End Region

#End Region

End Class
