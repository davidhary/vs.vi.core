﻿''' <summary>
''' Defines the contract that must be implemented by a SCPI Composite Limit Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class CompositeLimit
    Inherits VI.CompositeLimitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="CompositeLimit" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " SYNTAX "

    ''' <summary> Gets or sets the composite limits clear command. </summary>
    ''' <remarks> SCPI: ":CLAC2:CLIM:CLE". </remarks>
    ''' <value> The composite limits clear command. </value>
    Protected Overrides Property ClearCommand As String = ":CLAC2:CLIM:CLE"

    ''' <summary> Gets or sets the Composite Limits Auto Clear enabled query command. </summary>
    ''' <remarks> SCPI: ":CALC2:CLIM:CLE:AUTO?". </remarks>
    ''' <value> The Composite Limits Auto Clear enabled query command. </value>
    Protected Overrides Property AutoClearEnabledQueryCommand As String = ":CALC2:CLIM:CLE:AUTO?"

    ''' <summary> Gets or sets the Composite Limits Auto Clear enabled command Format. </summary>
    ''' <remarks> SCPI: ":CALC2:CLIM:CLE:AUTO {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Composite Limits Auto Clear enabled query command. </value>
    Protected Overrides Property AutoClearEnabledCommandFormat As String = ":CALC2:CLIM:CLE:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Composite Limits failure Bits query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property FailureBitsQueryCommand As String = ":CALC2:CLIM:FAIL:SOUR2?"

    ''' <summary> Gets or sets the Composite Limits Failure Bits query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property FailureBitsCommandFormat As String = ":CALC2:CLIM:FAIL:SOUR2 {0}"

    ''' <summary> Gets or sets the Composite Limits Pass Bits query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property PassBitsQueryCommand As String = ":CALC2:CLIM:PASS:SOUR2?"

    ''' <summary> Gets or sets the Composite Limits Pass Bits query command. </summary>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property PassBitsCommandFormat As String = ":CALC2:CLIM:PASS:SOUR2 {0}"

    ''' <summary> Gets or sets the Binning Control query command. </summary>
    ''' <remarks> SCPI: ":CALC2:CLIM:BCON?". </remarks>
    ''' <value> The Binning Control query command. </value>
    Protected Overrides Property BinningControlQueryCommand As String = ":CALC2:CLIM:BCON?"

    ''' <summary> Gets or sets the Binning Control command format. </summary>
    ''' <remarks> SCPI: ":CALC2:CLIM:BCON {0}". </remarks>
    ''' <value> The Binning Control query command format. </value>
    Protected Overrides Property BinningControlCommandFormat As String = ":CALC2:CLIM:BCON {0}"

    ''' <summary> Gets or sets the Limit Mode query command. </summary>
    ''' <remarks> SCPI: "CALC2:CLIM:MODE?". </remarks>
    ''' <value> The Limit Mode query command. </value>
    Protected Overrides Property LimitModeQueryCommand As String = "CALC2:CLIM:MODE?"

    ''' <summary> Gets or sets the Limit Mode command format. </summary>
    ''' <remarks> SCPI: "CALC2:CLIM:MODE {0}". </remarks>
    ''' <value> The Limit Mode command format. </value>
    Protected Overrides Property LimitModeCommandFormat As String = "CALC2:CLIM:MODE {0}"

#End Region

End Class
