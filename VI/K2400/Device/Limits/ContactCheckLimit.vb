﻿''' <summary>
''' Defines the contract that must be implemented by a SCPI Contact Check Limit Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class ContactCheckLimit
    Inherits VI.ContactCheckLimitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ContactCheckLimit" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(1, statusSubsystem)
    End Sub

#End Region

#Region " SYNTAX "

#Region " COMPLIANCE FAILURE BITS "

    ''' <summary> Gets or sets Contact Check Failure Bits query command. </summary>
    ''' <value> The Compliance Failure Bits query command. </value>
    Protected Overrides Property FailureBitsQueryCommand As String = ":CALC2:LIM{0}:SOUR2?"

    ''' <summary> Gets or sets Contact Check Failure Bits command format. </summary>
    ''' <value> The Compliance Failure Bits command format. </value>
    Protected Overrides Property FailureBitsCommandFormat As String = ":CALC2:LIM{0}:SOUR2 {{0}}"

#End Region

#Region " LIMIT FAILED "

    ''' <summary> Gets or sets the Limit Failed query command. </summary>
    ''' <value> The Limit Failed query command. </value>
    Protected Overrides Property FailedQueryCommand As String = ":CALC2:LIM{0}:FAIL?"

#End Region

#Region " LIMIT ENABLED "

    ''' <summary> Gets or sets the Limit enabled command Format. </summary>
    ''' <remarks> SCPI: "CALC2:LIM[#]:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property EnabledCommandFormat As String = "CALC2:LIM{0}:STAT {{0:'ON';'ON';'OFF'}}"

    ''' <summary> Gets or sets the Limit enabled query command. </summary>
    ''' <remarks> SCPI: "CALC2:LIM[#]:STAT?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property EnabledQueryCommand As String = ":CALC2:LIM{0}:STAT?"

#End Region

#End Region

End Class
