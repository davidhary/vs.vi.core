﻿''' <summary>
''' Defines the contract that must be implemented by a SCPI Upper/Lower Limit Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class UpperLowerLimit
    Inherits VI.UpperLowerLimitBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="UpperLowerLimit" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " SYNTAX "

#Region " FAILED "

    ''' <summary> Gets or sets the Limit Failed query command. </summary>
    ''' <value> The Limit Failed query command. </value>
    Protected Overrides Property FailedQueryCommand As String = ":CALC2:LIM{0}:FAIL?"

#End Region

#Region " ENABLED "

    ''' <summary> Gets or sets the Limit enabled command Format. </summary>
    ''' <remarks> SCPI: "CALC2:LIM[#]:STAT {0:'ON';'ON';'OFF'}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property EnabledCommandFormat As String = "CALC2:LIM{0}:STAT {{0:'ON';'ON';'OFF'}}"

    ''' <summary> Gets or sets the Limit enabled query command. </summary>
    ''' <remarks> SCPI: "CALC2:LIM[#]:STAT?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property EnabledQueryCommand As String = ":CALC2:LIM{0}:STAT?"

#End Region

#Region " LIMIT "

    ''' <summary> Gets or sets trigger Pass Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:SOUR2?". </remarks>
    ''' <value> The trigger PassBits query command. </value>
    Protected Overrides Property PassBitsQueryCommand As String = ":CALC2:LIM{0}:PASS:SOUR2?"

    ''' <summary> Gets or sets Pass Bits command format. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:SOUR2 {0}". </remarks>
    ''' <value> The Pass Bits command format. </value>
    Protected Overrides Property PassBitsCommandFormat As String = ":CALC2:LIM{0}:PASS:SOUR2 {{0}}"

    ''' <summary> Gets or sets the Lower Limit query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:LOW?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LowerLimitQueryCommand As String = ":CALC2:LIM{0}:LOW?"

    ''' <summary> Gets or sets the Lower Limit query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:LOW {0}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LowerLimitCommandFormat As String = ":CALC2:LIM{0}:LOW {{0}}"

    ''' <summary> Gets or sets the Lower Limit failure Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:LOW:SOUR2?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LowerLimitFailureBitsQueryCommand As String = ":CALC2:LIM{0}:LOW:SOUR2?"

    ''' <summary> Gets or sets the Lower Limit Failure Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:LOW:SOUR2 {0}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property LowerLimitFailureBitsCommandFormat As String = ":CALC2:LIM{0}:LOW:SOUR2 {{0}}"

    ''' <summary> Gets or sets the Upper Limit query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:UPP?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property UpperLimitQueryCommand As String = ":CALC2:LIM{0}:UPP?"

    ''' <summary> Gets or sets the Upper Limit query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:UPP {0}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property UpperLimitCommandFormat As String = ":CALC2:LIM{0}:UPP {{0}}"

    ''' <summary> Gets or sets the Upper Limit failure Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:UPP:SOUR2?". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property UpperLimitFailureBitsQueryCommand As String = ":CALC2:LIM{0}:UPP:SOUR2?"

    ''' <summary> Gets or sets the Upper Limit Failure Bits query command. </summary>
    ''' <remarks> SCPI: ":CALC2:LIM2:UPP:SOUR2 {0}". </remarks>
    ''' <value> The Limit enabled query command. </value>
    Protected Overrides Property UpperLimitFailureBitsCommandFormat As String = ":CALC2:LIM{0}:UPP:SOUR2 {{0}}"

#End Region

#End Region

End Class
