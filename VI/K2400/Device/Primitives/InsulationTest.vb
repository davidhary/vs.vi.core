Imports isr.Core.Models

''' <summary> An insulation test. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-03-09 </para>
''' </remarks>
Public Class InsulationTest
    Inherits ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets values to their known clear execution state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub DefineClearExecutionState()
    End Sub

    ''' <summary> Initializes the known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub InitKnownState()
        Me.ResetKnownState()
        Me.DefineClearExecutionState()
    End Sub

    ''' <summary> Preset known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub PresetKnownState()
    End Sub

    ''' <summary> Resets the known state this. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ResetKnownStateThis()
        Me._Binning = New VI.BinningInfo
        Me._Insulation = New InsulationResistance
        ' not using upper limit
        Me._Binning.UpperLimit = VI.Pith.Scpi.Syntax.Infinity
        Me._Binning.UpperLimitFailureBits = 4
        ' using lower limit
        Me._Binning.LowerLimit = 10000000
        Me._Binning.LowerLimitFailureBits = 4
        Me._Binning.PassBits = 1

        Me._Binning.ArmCount = 0
        Me._Binning.ArmDirection = VI.TriggerLayerBypassModes.Source
        ' using SOT Line
        Me._Binning.InputLineNumber = 1
        ' USING EOT Line
        Me._Binning.OutputLineNumber = 2
        Me._Binning.ArmSource = VI.ArmSources.StartTestBoth
        Me._Binning.ArmDirection = VI.TriggerLayerBypassModes.Acceptor
        Me._Binning.TriggerDirection = VI.TriggerLayerBypassModes.Source
        Me._Binning.TriggerSource = VI.TriggerSources.Immediate

        Me._Insulation.DwellTime = TimeSpan.FromSeconds(2)
        Me._Insulation.CurrentLimit = 0.00001
        Me._Insulation.PowerLineCycles = 1
        Me._Insulation.VoltageLevel = 10
        Me._Insulation.ResistanceLowLimit = 10000000
        Me._Insulation.ResistanceRange = 1000000000
        Me._Insulation.ContactCheckEnabled = True

    End Sub

    ''' <summary> Resets the known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ResetKnownState()
        Me.ResetKnownStateThis()
    End Sub


#End Region

#Region " FIELDS "

    ''' <summary> Gets or sets the binning. </summary>
    ''' <value> The binning. </value>
    Public Property Binning As VI.BinningInfo

    ''' <summary> Gets or sets the insulation. </summary>
    ''' <value> The insulation. </value>
    Public Property Insulation As InsulationResistance

#End Region

End Class
