''' <summary>
''' This is a test class for DeviceTest and is intended to contain all DeviceTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-10-12. </remarks>
<TestClass(), TestCategory("k2400")>
Public Class DeviceTests

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  _
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  _
    'Public Shared Sub MyClassCleanup()
    ' _TestSite?.Dispose()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  _
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  _
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    ''' <summary> Select resource name. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> A String. </returns>
    Friend Function SelectResourceName(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As String
        Select Case interfaceType
            Case VI.Pith.HardwareInterfaceType.Gpib
                Return "GPIB0::24::INSTR"
            Case VI.Pith.HardwareInterfaceType.Tcpip
                Return "TCPIP0::A-N5767A-K4381"
            Case VI.Pith.HardwareInterfaceType.Usb
                Return "USB0::0x0957::0x0807::N5767A-US11K4381H::0::INSTR"
            Case Else
                Return "GPIB0::24::INSTR"
        End Select
    End Function

    ''' <summary> (Unit Test Method) session should open and close. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SessionShouldOpenAndClose()
        Dim usingInterfaceType As VI.Pith.HardwareInterfaceType = VI.Pith.HardwareInterfaceType.Gpib
        Using device As New K2400.K2400Device()
            Dim r As (Success As Boolean, Details As String) = device.TryOpenSession(Me.SelectResourceName(usingInterfaceType), "Source Measure")
            Assert.IsTrue(r.Success, $"Open Session; {r.Details}")
            device.CloseSession()
        End Using
    End Sub

    ''' <summary> (Unit Test Method) output on off should toggle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OutputOnOffShouldToggle()
        Dim actualBoolean As Boolean

        Using device As New K2400.K2400Device()
            Dim e As New isr.Core.ActionEventArgs
            Dim r As (Success As Boolean, Details As String) = device.TryOpenSession(Me.SelectResourceName(VI.Pith.HardwareInterfaceType.Gpib), "Source Measure")
            Assert.IsTrue(r.Success, $"Open Session; { r.Details}")

            ' do a device clear and reset.
            device.ResetClearInit()
            actualBoolean = True

            ' output should be off after a device clear.
            Dim expectedBoolean As Boolean = False
            actualBoolean = device.OutputSubsystem.QueryOutputOnState.GetValueOrDefault(Not expectedBoolean)
            Assert.AreEqual(expectedBoolean, actualBoolean,
                        "Output {0:'ON';'ON';'OFF'};", CInt(expectedBoolean))

            ' turn it on
            expectedBoolean = True
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
            Assert.AreEqual(expectedBoolean, actualBoolean,
                        "Output {0:'ON';'ON';'OFF'};", CInt(expectedBoolean))

            ' turn it off
            expectedBoolean = False
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
            Assert.AreEqual(expectedBoolean, actualBoolean,
                        "Output {0:'ON';'ON';'OFF'};", CInt(expectedBoolean))

            Dim expectedString As String = "no error"
            If device.StatusSubsystem.Session.IsErrorBitSet Then
                r = device.StatusSubsystemBase.TryQueryExistingDeviceErrors()
                Assert.IsTrue(r.Success, $"Device error query failed: { r.Details}")
            End If
            Dim actualString As String = device.StatusSubsystem.CompoundErrorMessage

            Assert.AreEqual(expectedString, actualString, True, Globalization.CultureInfo.CurrentCulture, "Device error mismatch")
        End Using

    End Sub

    ''' <summary> (Unit Test Method) device voltage current resistance should measure. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DeviceVoltageCurrentResistanceShouldMeasure()
        Dim voltage As Double = 5.0!
        Dim resistance As Double = 9910
        Dim currentLimit As Double = 0.001!
        Dim voltageLimit As Double = voltage + 1.0!
        Dim actualBoolean As Boolean

        Using device As New K2400.K2400Device()

            Dim r As (Success As Boolean, Details As String) = device.TryOpenSession(Me.SelectResourceName(VI.Pith.HardwareInterfaceType.Gpib), "Source Measure")
            Assert.IsTrue(r.Success, $"Open Session; { r.Details}")

            Dim expectedBoolean As Boolean = True
            device.ResetClearInit()
            actualBoolean = True
            Assert.AreEqual(expectedBoolean, actualBoolean, "Reset;")

            Dim expectedString As String = "Keithley"
            Dim actualString As String = device.StatusSubsystem.QueryIdentity
            Assert.IsFalse(String.IsNullOrWhiteSpace(actualString), "Identity is empty")

            actualString = actualString.Substring(0, Len(expectedString))
            Assert.AreEqual(expectedString, actualString, True, Globalization.CultureInfo.CurrentCulture)

            expectedBoolean = True
            device.OutputOn(voltage, currentLimit, voltageLimit)
            actualBoolean = True
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output On;")

            actualBoolean = device.OutputSubsystem.QueryOutputOnState.GetValueOrDefault(False)
            Assert.AreEqual(expectedBoolean, actualBoolean,
                        "Output {0:'ON';'ON';'OFF'};", CInt(expectedBoolean))

            device.MeasureSubsystem.Read()
            Dim expectedDouble As Double = voltage

            Assert.AreEqual(expectedDouble, device.MeasureSubsystem.ReadingAmounts.VoltageReading.Value.GetValueOrDefault(0), 0.1)

            expectedDouble = voltage / resistance
            Assert.AreEqual(expectedDouble, device.MeasureSubsystem.ReadingAmounts.CurrentReading.Value.GetValueOrDefault(0), 0.1 / resistance)

            expectedDouble = resistance
            Assert.AreEqual(expectedDouble, device.MeasureSubsystem.ReadingAmounts.ResistanceReading.Value.GetValueOrDefault(0), 0.1 / resistance)

            expectedBoolean = False
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
            Assert.AreEqual(expectedBoolean, actualBoolean,
                        "Output {0:'ON';'ON';'OFF'};", CInt(expectedBoolean))

            expectedString = "no error"
            If device.StatusSubsystem.Session.IsErrorBitSet Then
                r = device.StatusSubsystem.TryQueryExistingDeviceErrors()
                Assert.IsTrue(r.Success, $"Device error query failed: { r.Details}")
            End If
            actualString = device.StatusSubsystem.CompoundErrorMessage
            Assert.AreEqual(expectedString, actualString, True, Globalization.CultureInfo.CurrentCulture, "Device error mismatch")
        End Using
    End Sub

End Class

