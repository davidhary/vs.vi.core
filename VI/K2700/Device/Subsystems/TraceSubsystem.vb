''' <summary> Defines a Trace Subsystem for a Keithley 2700 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TraceSubsystem
    Inherits VI.TraceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TraceSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PointsCount = 10
        Me.FeedSource = VI.FeedSources.None
        Me.FeedControl = VI.FeedControls.Never
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the Clear Buffer command. </summary>
    ''' <value> The ClearBuffer command. </value>
    Protected Overrides Property ClearBufferCommand As String = ":TRAC:CLE"

    ''' <summary> Gets or sets the points count query command. </summary>
    ''' <value> The points count query command. </value>
    Protected Overrides Property PointsCountQueryCommand As String = ":TRAC:POIN:COUN?"

    ''' <summary> Gets or sets the points count command format. </summary>
    ''' <value> The points count command format. </value>
    Protected Overrides Property PointsCountCommandFormat As String = ":TRAC:POIN:COUN {0}"

    ''' <summary> Gets or sets the feed Control query command. </summary>
    ''' <value> The write feed Control query command. </value>
    Protected Overrides Property FeedControlQueryCommand As String = ":TRAC:FEED:CONTROL?"

    ''' <summary> Gets or sets the feed Control command format. </summary>
    ''' <value> The write feed Control command format. </value>
    Protected Overrides Property FeedControlCommandFormat As String = ":TRAC:FEED:CONTROL {0}"

    ''' <summary> Gets or sets the feed source query command. </summary>
    ''' <value> The write feed source query command. </value>
    Protected Overrides Property FeedSourceQueryCommand As String = ":TRAC:FEED?"

    ''' <summary> Gets or sets the feed source command format. </summary>
    ''' <value> The write feed source command format. </value>
    Protected Overrides Property FeedSourceCommandFormat As String = ":TRAC:FEED {0}"

#End Region

End Class

