#Region " TYPES "

''' <summary> Gets or sets the status byte flags of the measurement event register. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum MeasurementEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the reading overflow option. </summary>
    <ComponentModel.Description("Reading Overflow")>
    ReadingOverflow = 1

    ''' <summary> An enum constant representing the low limit 1 failed option. </summary>
    <ComponentModel.Description("Low Limit 1 Failed")>
    LowLimit1Failed = 2

    ''' <summary> An enum constant representing the high limit 1 failed option. </summary>
    <ComponentModel.Description("High Limit 1 Failed")>
    HighLimit1Failed = 4

    ''' <summary> An enum constant representing the low limit 2 failed option. </summary>
    <ComponentModel.Description("Low Limit 2 Failed")>
    LowLimit2Failed = 8

    ''' <summary> An enum constant representing the high limit 2 failed option. </summary>
    <ComponentModel.Description("High Limit 2 Failed")>
    HighLimit2Failed = 16

    ''' <summary> An enum constant representing the reading available option. </summary>
    <ComponentModel.Description("Reading Available")>
    ReadingAvailable = 32

    ''' <summary> An enum constant representing the not used 1 option. </summary>
    <ComponentModel.Description("Not Used 1")>
    NotUsed1 = 64

    ''' <summary> An enum constant representing the buffer available option. </summary>
    <ComponentModel.Description("Buffer Available")>
    BufferAvailable = 128

    ''' <summary> An enum constant representing the buffer half full option. </summary>
    <ComponentModel.Description("Buffer Half Full")>
    BufferHalfFull = 256

    ''' <summary> An enum constant representing the buffer full option. </summary>
    <ComponentModel.Description("Buffer Full")>
    BufferFull = 512

    ''' <summary> An enum constant representing the buffer overflow option. </summary>
    <ComponentModel.Description("Buffer Overflow")>
    BufferOverflow = 1024

    ''' <summary> An enum constant representing the hardware limit event option. </summary>
    <ComponentModel.Description("HardwareLimit Event")>
    HardwareLimitEvent = 2048

    ''' <summary> An enum constant representing the buffer quarter full option. </summary>
    <ComponentModel.Description("Buffer Quarter Full")>
    BufferQuarterFull = 4096

    ''' <summary> An enum constant representing the buffer three quarters full option. </summary>
    <ComponentModel.Description("Buffer Three-Quarters Full")>
    BufferThreeQuartersFull = 8192

    ''' <summary> An enum constant representing the master limit option. </summary>
    <ComponentModel.Description("Master Limit")>
    MasterLimit = 16384

    ''' <summary> An enum constant representing the not used 2 option. </summary>
    <ComponentModel.Description("Not Used 2")>
    NotUsed2 = 32768

    ''' <summary> An enum constant representing all option. </summary>
    <ComponentModel.Description("All")>
    All = 32767
End Enum

#End Region
