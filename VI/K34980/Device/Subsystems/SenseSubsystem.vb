''' <summary> Defines a SCPI Sense Subsystem for a Keysight 34980 Meter/Scanner. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class SenseSubsystem
    Inherits VI.SenseSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.SupportedFunctionModes = VI.SenseFunctionModes.CurrentDC Or
                                        VI.SenseFunctionModes.VoltageDC Or
                                        VI.SenseFunctionModes.Resistance Or
                                        VI.SenseFunctionModes.ResistanceFourWire
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.Current).SetRange(0, 10)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.CurrentAC).SetRange(0, 10)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.CurrentDC).SetRange(0, 10)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.Resistance).SetRange(0, 2000000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.ResistanceFourWire).SetRange(0, 1000000000D)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.Voltage).SetRange(0, 1000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.VoltageAC).SetRange(0, 1000)
        Me.FunctionModeRanges.Item(VI.SenseFunctionModes.VoltageDC).SetRange(0, 1000)

        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.Current) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.CurrentAC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.CurrentDC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.Resistance) = 0
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.ResistanceFourWire) = 0
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.Voltage) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.VoltageAC) = 3
        Me.FunctionModeDecimalPlaces.Item(VI.SenseFunctionModes.VoltageDC) = 3
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.FunctionMode = VI.SenseFunctionModes.VoltageDC
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:CURR:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:CURR:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:CURR:NPLC?"

#End Region

#Region " PROTECTION LEVEL "

    ''' <summary> Gets or sets the protection level command format. </summary>
    ''' <value> the protection level command format. </value>
    Protected Overrides Property ProtectionLevelCommandFormat As String = ":SENS:CURR:PROT {0}"

    ''' <summary> Gets or sets the protection level query command. </summary>
    ''' <value> the protection level query command. </value>
    Protected Overrides Property ProtectionLevelQueryCommand As String = ":SENS:CURR:PROT?"

#End Region

#Region " LATEST DATA "

    ''' <summary> Gets or sets the latest data query command. </summary>
    ''' <value> The latest data query command. </value>
    Protected Overrides Property LatestDataQueryCommand As String = ":DATA:LAT?"

#End Region

#End Region

End Class
