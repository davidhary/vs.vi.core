﻿''' <summary> Information about the version of a Keysight 34980 Meter/Scanner. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class VersionInfo
    Inherits isr.VI.VersionInfoBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Parses the instrument firmware revision. </summary>
    ''' <remarks>
    ''' KEYSIGHT, Model 34980, xxxxxxx,m.mm–b.bb–f.ff–d.dd <para>
    ''' where</para><para>xxxxxxx is the serial number.</para>
    ''' </remarks>
    ''' <exception cref="ArgumentNullException" guarantee="strong"> . </exception>
    ''' <param name="revision"> Specifies the instrument
    '''                         <see cref="FirmwareRevisionElements">revisions</see>
    '''                         e.g., <c>m.mm–b.bb–f.ff–d.dd</c> where <para>
    '''                         m.mm - Mainframe version number; </para>. </param>
    Protected Overrides Sub ParseFirmwareRevision(ByVal revision As String)

        If revision Is Nothing Then
            Throw New ArgumentNullException(NameOf(revision))
        ElseIf String.IsNullOrWhiteSpace(revision) Then
            MyBase.ParseFirmwareRevision(revision)
        Else
            MyBase.ParseFirmwareRevision(revision)

            ' get the revision sections
            Dim revSections As Queue(Of String) = New Queue(Of String)(revision.Split("-"c))
            If revSections.Count > 0 Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.Mainframe.ToString, revSections.Dequeue.Trim)
            If revSections.Count > 0 Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.BootCode.ToString, revSections.Dequeue.Trim)
            If revSections.Count > 0 Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.FrontPanel.ToString, revSections.Dequeue.Trim)
            If revSections.Count > 0 Then Me.FirmwareRevisionElements.Add(FirmwareRevisionElement.InternalMeter.ToString, revSections.Dequeue.Trim)

        End If

    End Sub

End Class

