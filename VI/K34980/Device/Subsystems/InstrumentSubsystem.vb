''' <summary> Defines a Instrument Subsystem for a Keysight 34980 Meter/Scanner. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class InstrumentSubsystem
    Inherits VI.InstrumentSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="InstrumentSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.QueryDmmInstalled()
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " DMM INSTALLED "

    ''' <summary> Gets or sets the DMM Installed query command. </summary>
    ''' <value> The DMM Installed query command. </value>
    Protected Overrides Property DmmInstalledQueryCommand As String = ":INST:DMM:INST?"

#End Region

#End Region

End Class

