Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements a Keysight 34980 Meter/Scanner device. </summary>
''' <remarks>
''' An instrument is defined, for the purpose of this library, as a device with a front panel.
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class K34980Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="K34980Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As K34980Device
        Dim device As K34980Device = Nothing
        Try
            device = New K34980Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As K34980Device) As K34980Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(K34980Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(Nothing)
            ' must be added before the measure system
            Me.BindRouteSubsystem(Nothing)
            Me.BindTriggerSubsystem(Nothing)
            If Me.InstrumentSubsystem?.DmmInstalled Then
                ' must be added before the format system
                Me.BindMeasureSubsystem(Nothing)
                Me.BindFormatSubsystem(Nothing)
                Me.BindSenseSubsystem(Nothing)
                Me.BindSenseVoltageSubsystem(Nothing)
                Me.BindSenseCurrentSubsystem(Nothing)
                Me.BindSenseResistanceFourWireSubsystem(Nothing)
                Me.BindSenseResistanceSubsystem(Nothing)
            End If
            Me.BindInstrumentSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            ' must be added before the measure system
            Me.BindInstrumentSubsystem(New InstrumentSubsystem(Me.StatusSubsystem))
            Me.BindRouteSubsystem(New RouteSubsystem(Me.StatusSubsystem))
            Me.BindTriggerSubsystem(New TriggerSubsystem(Me.StatusSubsystem))
            If Me.InstrumentSubsystem.DmmInstalled Then
                ' must be added before the format system
                Me.BindMeasureSubsystem(New MeasureSubsystem(Me.StatusSubsystem))
                ' the measure subsystem reading are initialized when the format system is reset
                Me.BindFormatSubsystem(New FormatSubsystem(Me.StatusSubsystem))
                Me.BindSenseSubsystem(New SenseSubsystem(Me.StatusSubsystem))
                Me.BindSenseVoltageSubsystem(New SenseVoltageSubsystem(Me.StatusSubsystem))
                Me.BindSenseCurrentSubsystem(New SenseCurrentSubsystem(Me.StatusSubsystem))
                Me.BindSenseResistanceFourWireSubsystem(New SenseResistanceFourWireSubsystem(Me.StatusSubsystem))
                Me.BindSenseResistanceSubsystem(New SenseResistanceSubsystem(Me.StatusSubsystem))
            End If
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Gets or sets the Instrument Subsystem. </summary>
    ''' <value> The Instrument Subsystem. </value>
    Public ReadOnly Property InstrumentSubsystem As InstrumentSubsystem

    ''' <summary> Binds the Instrument subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindInstrumentSubsystem(ByVal subsystem As InstrumentSubsystem)
        If Me.InstrumentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.InstrumentSubsystem)
            Me._InstrumentSubsystem = Nothing
        End If
        Me._InstrumentSubsystem = subsystem
        If Me.InstrumentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.InstrumentSubsystem)
            Me.InstrumentSubsystem.QueryDmmInstalled()
            Me.StatusSubsystem.DmmInstalled = Me.InstrumentSubsystem.DmmInstalled.GetValueOrDefault(False)
        End If
    End Sub

    ''' <summary> Gets or sets the Measure Subsystem. </summary>
    ''' <value> The Measure Subsystem. </value>
    Public ReadOnly Property MeasureSubsystem As MeasureSubsystem

    ''' <summary> Binds the Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureSubsystem(ByVal subsystem As MeasureSubsystem)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        Me._MeasureSubsystem = subsystem
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Route Subsystem. </summary>
    ''' <value> The Route Subsystem. </value>
    Public ReadOnly Property RouteSubsystem As RouteSubsystem

    ''' <summary> Binds the Route subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindRouteSubsystem(ByVal subsystem As RouteSubsystem)
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.RouteSubsystem)
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.RouteSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Subsystem. </summary>
    ''' <value> The Sense Subsystem. </value>
    Public ReadOnly Property SenseSubsystem As SenseSubsystem

    ''' <summary> Binds the Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseSubsystem(ByVal subsystem As SenseSubsystem)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        Me._SenseSubsystem = subsystem
        If Me.SenseSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Current Subsystem. </summary>
    ''' <value> The Sense Current Subsystem. </value>
    Public ReadOnly Property SenseCurrentSubsystem As SenseCurrentSubsystem

    ''' <summary> Binds the Sense Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseCurrentSubsystem(ByVal subsystem As SenseCurrentSubsystem)
        If Me.SenseCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseCurrentSubsystem)
            Me._SenseCurrentSubsystem = Nothing
        End If
        Me._SenseCurrentSubsystem = subsystem
        If Me.SenseCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Voltage Subsystem. </summary>
    ''' <value> The Sense Voltage Subsystem. </value>
    Public ReadOnly Property SenseVoltageSubsystem As SenseVoltageSubsystem

    ''' <summary> Binds the Sense Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseVoltageSubsystem(ByVal subsystem As SenseVoltageSubsystem)
        If Me.SenseVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseVoltageSubsystem)
            Me._SenseVoltageSubsystem = Nothing
        End If
        Me._SenseVoltageSubsystem = subsystem
        If Me.SenseVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseVoltageSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Four Wire Resistance Subsystem. </summary>
    ''' <value> The Sense Four Wire Resistance Subsystem. </value>
    Public ReadOnly Property SenseResistanceFourWireSubsystem As SenseResistanceFourWireSubsystem

    ''' <summary> Binds the Sense Four Wire Resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseResistanceFourWireSubsystem(ByVal subsystem As SenseResistanceFourWireSubsystem)
        If Me.SenseResistanceFourWireSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseResistanceFourWireSubsystem)
            Me._SenseResistanceFourWireSubsystem = Nothing
        End If
        Me._SenseResistanceFourWireSubsystem = subsystem
        If Me.SenseResistanceFourWireSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseResistanceFourWireSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Resistance Subsystem. </summary>
    ''' <value> The Sense Resistance Subsystem. </value>
    Public ReadOnly Property SenseResistanceSubsystem As SenseResistanceSubsystem

    ''' <summary> Binds the Sense Resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseResistanceSubsystem(ByVal subsystem As SenseResistanceSubsystem)
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseResistanceSubsystem)
            Me._SenseResistanceSubsystem = Nothing
        End If
        Me._SenseResistanceSubsystem = subsystem
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseResistanceSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Trigger Subsystem. </summary>
    ''' <value> The Trigger Subsystem. </value>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Binds the Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystem)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TriggerSubsystem)
        End If
    End Sub

#Region " FORMAT "

    ''' <summary> Gets or sets the Format Subsystem. </summary>
    ''' <value> The Format Subsystem. </value>
    Public ReadOnly Property FormatSubsystem As FormatSubsystem

    ''' <summary> Binds the Format subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindFormatSubsystem(ByVal subsystem As FormatSubsystem)
        If Me.FormatSubsystem IsNot Nothing Then
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.FormatSubsystem)
            Me._FormatSubsystem = Nothing
        End If
        Me._FormatSubsystem = subsystem
        If Me.FormatSubsystem IsNot Nothing Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.FormatSubsystemPropertyChanged
            Me.Subsystems.Add(Me.FormatSubsystem)
            Me.HandlePropertyChanged(Me.FormatSubsystem, NameOf(VI.FormatSubsystemBase.Elements))
        End If
    End Sub

    ''' <summary> Handle the Format subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As FormatSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.FormatSubsystemBase.Elements)
                Me.MeasureSubsystem.ReadingAmounts.Initialize(subsystem.Elements)
        End Select
    End Sub

    ''' <summary> Format subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub FormatSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(FormatSubsystem)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, FormatSubsystem), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("K34980 Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.MySettings.ReadDelay)
                Me.Session.ReadDelay = TimeSpan.FromMilliseconds(sender.ReadDelay)
            Case NameOf(My.MySettings.StatusReadDelay)
                Me.Session.StatusReadDelay = TimeSpan.FromMilliseconds(sender.StatusReadDelay)
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
