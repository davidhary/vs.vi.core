Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Sense view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SenseView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SenseView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="SenseView"/>. </returns>
    Public Shared Function Create() As SenseView
        Dim view As SenseView = Nothing
        Try
            view = New SenseView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K34980Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K34980Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K34980Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindSenseSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K34980Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE "

    ''' <summary> Gets the Sense subsystem. </summary>
    ''' <value> The Sense subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseSubsystem As SenseSubsystem

    ''' <summary> Bind Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSenseSubsystem(ByVal device As K34980Device)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SenseSubsystem = device.SenseSubsystem
            Me.BindSubsystem(True, Me.SenseSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K34980.SenseSubsystem.SupportedFunctionModes))
            Me.HandlePropertyChanged(subsystem, NameOf(K34980.SenseSubsystem.FunctionMode))
            Me.HandlePropertyChanged(subsystem, NameOf(K34980.SenseSubsystem.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(K34980.SenseSubsystem.FunctionRangeDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(K34980.SenseSubsystem.FunctionUnit))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the supported function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnSupportedFunctionModesChanged(ByVal subsystem As SenseSubsystem)
        If subsystem IsNot Nothing AndAlso subsystem.SupportedFunctionModes <> VI.SenseFunctionModes.None Then
            Me._SenseFunctionComboBox.DataSource = Nothing
            Me._SenseFunctionComboBox.Items.Clear()
            Me._SenseFunctionComboBox.DataSource = GetType(VI.SenseFunctionModes).EnumValues.IncludeFilter(subsystem.SupportedFunctionModes).ValueDescriptionPairs.ToList
            Me._SenseFunctionComboBox.DisplayMember = NameOf(KeyValuePair(Of System.Enum, String).Value)
            Me._SenseFunctionComboBox.ValueMember = NameOf(KeyValuePair(Of System.Enum, String).Key)
            If Me._SenseFunctionComboBox.Items.Count > 0 Then
                Me._SenseFunctionComboBox.SelectedItem = VI.SenseFunctionModes.VoltageDC.ValueDescriptionPair()
            End If
        End If
    End Sub

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Shared Sub OnFunctionModesChanged(ByVal value As SenseFunctionSubsystemBase)
        value.QueryRange()
        value.QueryAutoRangeEnabled()
        value.QueryAutoZeroEnabled()
        value.QueryPowerLineCycles()
    End Sub

    ''' <summary> Gets the selected sense subsystem. </summary>
    ''' <value> The selected sense subsystem. </value>
    Private ReadOnly Property SelectedSenseSubsystem() As SenseFunctionSubsystemBase
        Get
            Return Me.SelectSenseSubsystem(Me.SelectedFunctionMode)
        End Get
    End Property

    ''' <summary> Select sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A SenseFunctionSubsystemBase. </returns>
    Private Function SelectSenseSubsystem(ByVal value As VI.SenseFunctionModes) As SenseFunctionSubsystemBase
        Dim result As SenseFunctionSubsystemBase = Me.Device.SenseVoltageSubsystem
        Select Case value
            Case VI.SenseFunctionModes.CurrentDC
                Me.BindSenseSubsystem(Me.Device.SenseCurrentSubsystem)
            Case VI.SenseFunctionModes.VoltageDC
                Me.BindSenseSubsystem(Me.Device.SenseVoltageSubsystem)
            Case VI.SenseFunctionModes.Resistance
                Me.BindSenseSubsystem(Me.Device.SenseResistanceSubsystem)
            Case VI.SenseFunctionModes.ResistanceFourWire
                Me.BindSenseSubsystem(Me.Device.SenseResistanceFourWireSubsystem)
            Case Else
        End Select
        Return result
    End Function

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Sub OnFunctionModesChanged(ByVal value As VI.SenseFunctionModes)
        SenseView.OnFunctionModesChanged(Me.SelectSenseSubsystem(value))
    End Sub

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub OnFunctionModesChanged(ByVal subsystem As SenseSubsystem)
        If subsystem IsNot Nothing AndAlso subsystem.FunctionMode.HasValue Then
            Dim value As VI.SenseFunctionModes = subsystem.FunctionMode.GetValueOrDefault(VI.SenseFunctionModes.None)
            If value <> VI.SenseFunctionModes.None Then
                Me._SenseFunctionComboBox.SafeSelectSenseFunctionModes(value)
                Me.OnFunctionModesChanged(value)
            End If
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SenseSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(K34980.SenseSubsystem.SupportedFunctionModes)
                Me.OnSupportedFunctionModesChanged(subsystem)
            Case NameOf(K34980.SenseSubsystem.FunctionMode)
                Me.OnFunctionModesChanged(subsystem)
            Case NameOf(K34980.SenseSubsystem.FunctionRange)
                Me._SenseRangeNumeric.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)
            Case NameOf(K34980.SenseSubsystem.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.DecimalPlaces = subsystem.FunctionRangeDecimalPlaces
            Case NameOf(K34980.SenseSubsystem.FunctionUnit)
                Me.Device.MeasureSubsystem.ReadingAmounts.PrimaryReading.ApplyUnit(subsystem.FunctionUnit)
                subsystem.ReadingAmounts.PrimaryReading.ApplyUnit(subsystem.FunctionUnit)
                Me._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:"
                Me._SenseRangeNumericLabel.Left = Me._SenseRangeNumeric.Left - Me._SenseRangeNumericLabel.Width
        End Select
    End Sub

    ''' <summary> Sense subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SENSE FUNCTION "

    ''' <summary> Gets the sense function subsystem. </summary>
    ''' <value> The sense function subsystem. </value>
    Private ReadOnly Property SenseFunctionSubsystem As SenseFunctionSubsystemBase

    ''' <summary> Bind Sense function subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseSubsystem(ByVal subsystem As SenseFunctionSubsystemBase)
        If Me.SenseFunctionSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseFunctionSubsystem)
            Me._SenseFunctionSubsystem = Nothing
        End If
        If subsystem IsNot Nothing Then
            Me._SenseFunctionSubsystem = subsystem
            Me.BindSubsystem(True, Me.SenseFunctionSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseFunctionSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseFunctionSubsystemBasePropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.AutoRangeEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.PowerLineCycles))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.PowerLineCyclesRange))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.Range))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.FunctionRangeDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseFunctionSubsystemBase.FunctionUnit))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseFunctionSubsystemBasePropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SenseFunctionSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(VI.SenseFunctionSubsystemBase.AutoRangeEnabled)
                If Me.Device IsNot Nothing AndAlso subsystem.AutoRangeEnabled.HasValue Then
                    Me._SenseAutoRangeToggle.Checked = subsystem.AutoRangeEnabled.Value
                End If
            Case NameOf(VI.SenseFunctionSubsystemBase.PowerLineCycles)
                If Me.Device IsNot Nothing AndAlso subsystem.PowerLineCycles.HasValue Then
                    Me._PowerLineCyclesNumeric.ValueSetter(subsystem.PowerLineCycles.Value)
                End If
            Case NameOf(VI.SenseFunctionSubsystemBase.PowerLineCyclesRange)
                Me._PowerLineCyclesNumeric.Maximum = CDec(subsystem.PowerLineCyclesRange.Max)
                Me._PowerLineCyclesNumeric.Minimum = CDec(subsystem.PowerLineCyclesRange.Min)
                Me._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces
            Case NameOf(VI.SenseFunctionSubsystemBase.Range)
                If Me.Device IsNot Nothing AndAlso subsystem.Range.HasValue Then
                    Me._SenseRangeNumeric.ValueSetter(subsystem.Range.Value)
                End If
            Case NameOf(VI.SenseFunctionSubsystemBase.FunctionRange)
                Me._SenseRangeNumeric.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)
            Case NameOf(VI.SenseFunctionSubsystemBase.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces
            Case NameOf(VI.SenseFunctionSubsystemBase.FunctionUnit)
                Me._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:"
                Me._SenseRangeNumericLabel.Left = Me._SenseRangeNumeric.Left - Me._SenseRangeNumericLabel.Width
        End Select
    End Sub

    ''' <summary> Sense voltage subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseFunctionSubsystemBasePropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseFunctionSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseFunctionSubsystemBasePropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseFunctionSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " DEVICE SETTINGS: FUNCTION MODE "

    ''' <summary> Selects a new sense mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Friend Sub ApplyFunctionMode(ByVal value As VI.SenseFunctionModes)
        If Me.Device?.IsDeviceOpen Then
            Me._Device.SenseSubsystem.ApplyFunctionMode(value)
        End If
    End Sub

    ''' <summary> Gets the selected function mode. </summary>
    ''' <value> The selected function mode. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedFunctionMode() As VI.SenseFunctionModes
        Get
            Return CType(CType(Me._SenseFunctionComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(
                  Of [Enum], String)).Key, VI.SenseFunctionModes)
        End Get
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS: SENSE "

    ''' <summary> Applies the function mode button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyFunctionModeButton_Click(sender As Object, e As EventArgs) Handles _ApplyFunctionModeButton.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} applying function mode"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.ApplyFunctionMode(Me.SelectedFunctionMode)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Sense range setter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub SenseRangeSetter(ByVal value As Double)
        If value <= Me._SenseRangeNumeric.Maximum AndAlso value >= Me._SenseRangeNumeric.Minimum Then Me._SenseRangeNumeric.Value = CDec(value)
    End Sub

    ''' <summary> Applies the selected measurements settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ApplySenseSettings()
        Dim subsystem As SenseFunctionSubsystemBase = Me.SelectedSenseSubsystem
        If Not Nullable.Equals(subsystem.PowerLineCycles, Me._PowerLineCyclesNumeric.Value) Then
            subsystem.ApplyPowerLineCycles(Me._PowerLineCyclesNumeric.Value)
        End If

        If Not Nullable.Equals(subsystem.AutoRangeEnabled, Me._SenseAutoRangeToggle.Checked) Then
            subsystem.ApplyAutoRangeEnabled(Me._SenseAutoRangeToggle.Checked)
        End If

        If subsystem.AutoRangeEnabled Then
            subsystem.QueryRange()
        ElseIf Not Nullable.Equals(subsystem.Range, Me._SenseRangeNumeric.Value) Then
            subsystem.ApplyRange(Me._SenseRangeNumeric.Value)
        End If
    End Sub

    ''' <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ApplySenseSettingsButton.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} applying sense settings"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.ApplySenseSettings()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally

            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
