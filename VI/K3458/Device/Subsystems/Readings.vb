''' <summary> Holds a single set of 27xx instrument reading elements. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2008-01-15, 2.0.2936. Create based on the 24xx system classes. </para>
''' </remarks>
Public Class Readings
    Inherits isr.VI.ReadingAmounts

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()

        ' instantiate the base class
        MyBase.New()

        Me.PrimaryReading = New isr.VI.MeasuredAmount(ReadingElementTypes.Voltage, Arebis.StandardUnits.ElectricUnits.Volt) With {
            .ComplianceLimit = VI.Pith.Scpi.Syntax.Infinity,
            .HighLimit = VI.Pith.Scpi.Syntax.Infinity,
            .LowLimit = VI.Pith.Scpi.Syntax.NegativeInfinity,
            .ReadingLength = 13}
        Me.BaseReadings.Add(Me.PrimaryReading)

        Me.VoltageReading = New isr.VI.MeasuredAmount(ReadingElementTypes.Voltage, Arebis.StandardUnits.ElectricUnits.Volt) With {
            .ComplianceLimit = VI.Pith.Scpi.Syntax.Infinity,
            .HighLimit = VI.Pith.Scpi.Syntax.Infinity,
            .LowLimit = VI.Pith.Scpi.Syntax.NegativeInfinity,
            .ReadingLength = 13}
        Me.BaseReadings.Add(Me.VoltageReading)

        Me.CurrentReading = New isr.VI.MeasuredAmount(ReadingElementTypes.Current, Arebis.StandardUnits.ElectricUnits.Ampere) With {
            .ComplianceLimit = VI.Pith.Scpi.Syntax.Infinity,
            .HighLimit = VI.Pith.Scpi.Syntax.Infinity,
            .LowLimit = VI.Pith.Scpi.Syntax.NegativeInfinity,
            .ReadingLength = 13}
        Me.BaseReadings.Add(Me.CurrentReading)

        Me.ResistanceReading = New isr.VI.MeasuredAmount(ReadingElementTypes.Resistance, Arebis.StandardUnits.ElectricUnits.Ohm) With {
            .ComplianceLimit = VI.Pith.Scpi.Syntax.Infinity,
            .HighLimit = VI.Pith.Scpi.Syntax.Infinity,
            .LowLimit = VI.Pith.Scpi.Syntax.NegativeInfinity,
            .ReadingLength = 13}
        Me.BaseReadings.Add(Me.ResistanceReading)

        Me.Timestamp = New isr.VI.ReadingAmount(ReadingElementTypes.Time, Arebis.StandardUnits.TimeUnits.Second) With {.ReadingLength = 13}
        Me.BaseReadings.Add(Me.Timestamp)

        Me.StatusReading = New isr.VI.ReadingStatus(ReadingElementTypes.Status) With {.ReadingLength = 13}
        Me.BaseReadings.Add(Me.StatusReading)

    End Sub

    ''' <summary> Create a copy of the model. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As Readings)
        MyBase.New(model)
        If model IsNot Nothing Then
            Me.PrimaryReading = New isr.VI.MeasuredAmount(model.PrimaryReading)
            Me.CurrentReading = New isr.VI.MeasuredAmount(model.CurrentReading)
            Me.ResistanceReading = New isr.VI.MeasuredAmount(model.ResistanceReading)
            Me.VoltageReading = New isr.VI.MeasuredAmount(model.VoltageReading)
            Me.Timestamp = New isr.VI.ReadingAmount(model.Timestamp)
            Me.StatusReading = New isr.VI.ReadingStatus(model.StatusReading)
        End If
    End Sub

    ''' <summary> Create a copy of the model. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="model"> The model. </param>
    Public Sub New(ByVal model As ReadingAmounts)
        MyBase.New(model)
    End Sub

    ''' <summary> Clones this class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="model"> The value. </param>
    ''' <returns> A copy of this object. </returns>
    Public Overrides Function Clone(ByVal model As ReadingAmounts) As ReadingAmounts
        Return New Readings(model)
    End Function

#End Region

#Region " PARSE "

    ''' <summary> Builds meta status. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns>
    ''' The <see cref="T:isr.VI.MetaStatus" /><see cref="P:isr.VI.MetaStatus.StatusValue" /> .
    ''' </returns>
    Protected Overrides Function BuildMetaStatus(ByVal status As Long) As Long
        Dim metaStatus As New MetaStatus
        metaStatus.Preset(status)
        ' To_DO: Add a mapper structure to the meta status to map the status to meta status elements 
        '         If status <> 0 Then
        ' update the meta status based on the status reading.
        '        If status.IsBit(StatusWordBit.FailedContactCheck) Then
        '            metaStatus.FailedContactCheck = True
        '        End If
        '        If status.IsBit(StatusWordBit.HitCompliance) Then
        '            metaStatus.HitStatusCompliance = True
        '        End If
        '        If status.IsBit(StatusWordBit.HitRangeCompliance) Then
        '            metaStatus.HitRangeCompliance = True
        '        End If
        '        If status.IsBit(StatusWordBit.HitVoltageProtection) Then
        '            metaStatus.HitVoltageProtection = True
        '        End If
        '        If status.IsBit(StatusWordBit.OverRange) Then
        '            metaStatus.HitOverRange = True
        '        End If
        '        End If
        Return metaStatus.StatusValue
    End Function

#End Region

End Class

