﻿''' <summary> Information about the version of a Keysight 34980 Meter/Scanner. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class VersionInfo
    Inherits isr.VI.VersionInfoBase

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub Clear()
        MyBase.Clear()
    End Sub

    ''' <summary> Parses the instrument identity string. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> Specifies the instrument identity string, which includes at a minimum the
    '''                      following information:
    '''                      <see cref="ManufacturerName">manufacturer</see>,
    '''                      <see cref="Model">model</see>,
    '''                      <see cref="SerialNumber">serial number</see>, e.g., <para>
    '''                      <c>H3458A</c>.</para> </param>
    Public Overrides Sub Parse(ByVal value As String)

        If String.IsNullOrEmpty(value) Then
            Me.Clear()
        Else

            ' save the identity.
            Me.Identity = value

            ' Parse the id to get the revision number
            Dim idItems As Queue(Of String) = New Queue(Of String)(value.Split(","c))

            Me.ManufacturerName = "KEYSIGHT"

            Me.Model = idItems.Dequeue

        End If

    End Sub

End Class

