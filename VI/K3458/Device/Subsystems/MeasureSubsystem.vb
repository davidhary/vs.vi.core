Imports isr.Core.EnumExtensions

''' <summary>
''' Defines a Measure Subsystem for a Keysight 1750 Resistance Measuring System.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public Class MeasureSubsystem
    Inherits VI.MeasureSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="MeasureSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.FunctionModeReadWrites.Clear()
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.None, "0", "", VI.SenseFunctionModes.None.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.VoltageDC, "1", "DCV", VI.SenseFunctionModes.VoltageDC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.VoltageAC, "2", "ACV", VI.SenseFunctionModes.VoltageAC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.VoltageACDC, "3", "ACDCV", VI.SenseFunctionModes.VoltageACDC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.Resistance, "4", "OHM", VI.SenseFunctionModes.Resistance.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.ResistanceFourWire, "5", "OHMF", VI.SenseFunctionModes.ResistanceFourWire.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.CurrentDC, "6", "DCI", VI.SenseFunctionModes.CurrentDC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.CurrentAC, "7", "ACI", VI.SenseFunctionModes.CurrentAC.DescriptionUntil)
        Me.FunctionModeReadWrites.Add(VI.SenseFunctionModes.CurrentACDC, "8", "ACDCI", VI.SenseFunctionModes.CurrentACDC.DescriptionUntil)
        Me.SupportedFunctionModes = VI.SenseFunctionModes.ResistanceFourWire Or VI.SenseFunctionModes.Resistance Or
                                    VI.SenseFunctionModes.CurrentDC Or VI.SenseFunctionModes.VoltageDC
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        ' TO_DO: this came from the 2450 and needs to be corrected to match this instrument.
        Me.ApertureRange = New isr.Core.Primitives.RangeR(0.000166667, 0.166667)
        Me.FilterCountRange = New isr.Core.Primitives.RangeI(1, 100)
        Me.FilterWindowRange = New isr.Core.Primitives.RangeR(0, 0.1)
        Me.PowerLineCyclesRange = New isr.Core.Primitives.RangeR(0.01, 10)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.FunctionMode = VI.SenseFunctionModes.VoltageDC
    End Sub

#End Region

#Region "  INIT, READ, FETCH "

    ''' <summary> Gets the fetch command. </summary>
    ''' <value> The fetch command. </value>
    <Obsolete("Not supported")>
    Protected Overrides Property FetchCommand As String = String.Empty

    ''' <summary> Gets the read command. </summary>
    ''' <value> The read command. </value>
    <Obsolete("Not supported")>
    Protected Overrides Property ReadCommand As String = String.Empty

#End Region

#Region " AVERAGE ENABLED "

    ''' <summary> Gets or sets the Average enabled command Format. </summary>
    ''' <value> The Average enabled query command. </value>
    Protected Overridable Property AverageEnabledCommandFormat As String = ":SENS:RES:AVER:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Average enabled query command. </summary>
    ''' <value> The Average enabled query command. </value>
    Protected Overridable Property AverageEnabledQueryCommand As String = ":SENS:RES:AVER:STAT?"

#End Region

#Region " AUTO ZERO MODE "

    ''' <summary> Gets or sets the auto zero command Format. </summary>
    ''' <value> The auto zero enabled query command. </value>
    Protected Overridable Property AutoZeroModeCommandFormat As String = "AZERO {0}"

    ''' <summary> Gets or sets the auto zero query command. </summary>
    ''' <value> The auto Zero enabled query command. </value>
    Protected Overridable Property AutoZeroModeQueryCommand As String = "AZERO?"

    ''' <summary> The automatic zero mode. </summary>
    Private _AutoZeroMode As AutoZeroMode?

    ''' <summary> Gets or sets the cached Auto Zero Mode. </summary>
    ''' <value>
    ''' The <see cref="AutoZeroMode">Auto Zero Mode</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property AutoZeroMode As AutoZeroMode?
        Get
            Return Me._AutoZeroMode
        End Get
        Protected Set(ByVal value As AutoZeroMode?)
            If Not Me.AutoZeroMode.Equals(value) Then
                Me._AutoZeroMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Auto Zero Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The  Auto Zero Mode. </param>
    ''' <returns> The <see cref="AutoZeroMode">Auto Zero Mode</see> or none if unknown. </returns>
    Public Function ApplyAutoZeroMode(ByVal value As AutoZeroMode) As AutoZeroMode?
        Me.WriteAutoZeroMode(value)
        Return Me.QueryAutoZeroMode()
    End Function

    ''' <summary> Queries the auto Zero Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The <see cref="AutoZeroMode">auto Zero Mode</see> or none if unknown. </returns>
    Public Function QueryAutoZeroMode() As AutoZeroMode?
        Me.AutoZeroMode = Me.QueryValue(Of AutoZeroMode)(Me.AutoZeroModeQueryCommand, Me.AutoZeroMode)
        Return Me.AutoZeroMode
    End Function

    ''' <summary> Writes the auto Zero Mode without reading back the value from the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The autoZeroMode. </param>
    ''' <returns> The <see cref="AutoZeroMode">autoZeroMode</see> or none if unknown. </returns>
    Public Function WriteAutoZeroMode(ByVal value As AutoZeroMode) As AutoZeroMode?
        Me.AutoZeroMode = Me.WriteValue(Of AutoZeroMode)(Me.AutoZeroModeCommandFormat, value)
        Return Me.AutoZeroMode
    End Function

#End Region

#Region " FETCH "

    ''' <summary> Fetches the data. </summary>
    ''' <remarks> the K3458 meter does not require issuing a read command. </remarks>
    ''' <returns> A Double? </returns>
    Public Overrides Function Fetch() As Double?
        Return Me.Read()
    End Function

    ''' <summary> Fetches the data. </summary>
    ''' <remarks> the K3458 meter does not require issuing a read command. </remarks>
    ''' <returns> A Double? </returns>
    Public Overrides Function Read() As Double?
        Return Me.MeasurePrimaryReading(Me.Session.ReadLineTrimEnd)
        'Dim reading As String = Me.Session.ReadLineTrimEnd
        'Return If(Me.ReadingAmounts.HasReadingElements, Me.ParseReadingAmounts(reading), Me.ParsePrimaryReading(reading))
    End Function

#End Region

#Region " MATH MODE "

    ''' <summary> Gets or sets the Math command Format. </summary>
    ''' <value> The Math enabled query command. </value>
    Protected Overridable Property MathModeCommandFormat As String = "MATH {0}"

    ''' <summary> Gets or sets the Math query command. </summary>
    ''' <value> The Math enabled query command. </value>
    Protected Overridable Property MathModeQueryCommand As String = "MATH?"

    ''' <summary> The mathematics mode. </summary>
    Private _MathMode As MathMode?

    ''' <summary> Gets or sets the cached Math Mode. </summary>
    ''' <value> The <see cref="MathMode">Math Mode</see> or none if not set or unknown. </value>
    Public Overloads Property MathMode As MathMode?
        Get
            Return Me._MathMode
        End Get
        Protected Set(ByVal value As MathMode?)
            If Not Me.MathMode.Equals(value) Then
                Me._MathMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Math Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The  Math Mode. </param>
    ''' <returns> The <see cref="MathMode">Math Mode</see> or none if unknown. </returns>
    Public Function ApplyMathMode(ByVal value As MathMode) As MathMode?
        Me.WriteMathMode(value)
        Return Me.QueryMathMode()
    End Function

    ''' <summary> Queries the Math Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The <see cref="MathMode">Math Mode</see> or none if unknown. </returns>
    Public Function QueryMathMode() As MathMode?
        Me.MathMode = Me.QueryFirstValue(Of MathMode)(Me.MathModeQueryCommand, Me.MathMode)
        Return Me.MathMode
    End Function

    ''' <summary> Writes the Math Mode without reading back the value from the device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The MathMode. </param>
    ''' <returns> The <see cref="MathMode">MathMode</see> or none if unknown. </returns>
    Public Function WriteMathMode(ByVal value As MathMode) As MathMode?
        Me.MathMode = Me.WriteValue(Of MathMode)(Me.MathModeCommandFormat, value)
        Return Me.MathMode
    End Function

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = "FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = "FUNC?"

    ''' <summary> Queries the Sense Function Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The <see cref="FunctionMode">Sense Function Mode</see> or none if unknown. </returns>
    Public Overrides Function QueryFunctionMode() As SenseFunctionModes?
        Me.FunctionMode = Me.QueryFirstValue(Of SenseFunctionModes)(Me.FunctionModeQueryCommand, Me.FunctionMode.GetValueOrDefault(SenseFunctionModes.None),
                                                                    Me.FunctionModeReadWrites)
        Return Me.FunctionMode
    End Function

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = "ARANG?"

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = "ARANGE {0:'ON';'ON';'OFF'}"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = "NPLC?"

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = "NPLC {0}"

#End Region

#Region " STORE MATH "

    ''' <summary> The store mathematics value. </summary>
    Private _StoreMathValue As Double?

    ''' <summary> Gets or sets the cached Store Math Value. </summary>
    ''' <value> The cached Store Math Value or none if not set or unknown. </value>
    Public Overloads Property StoreMathValue As Double?
        Get
            Return Me._StoreMathValue
        End Get
        Protected Set(ByVal value As Double?)
            If Not Nullable.Equals(Me.StoreMathValue, value) Then
                Me._StoreMathValue = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The store mathematics register. </summary>
    Private _StoreMathRegister As StoreMathRegister?

    ''' <summary> Gets or sets the cached Store Math Register. </summary>
    ''' <value>
    ''' The <see cref="StoreMathRegister">Store Math Register</see> or none if not set or unknown.
    ''' </value>
    Public Overloads Property StoreMathRegister As StoreMathRegister?
        Get
            Return Me._StoreMathRegister
        End Get
        Protected Set(ByVal value As StoreMathRegister?)
            If Not Me.StoreMathRegister.Equals(value) Then
                Me._StoreMathRegister = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Store Math Register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="register"> The register. </param>
    ''' <param name="value">    The  Auto Zero Mode. </param>
    ''' <returns>
    ''' The <see cref="StoreMathRegister">Store Math Register</see> or none if unknown.
    ''' </returns>
    <Obsolete("Apply does not work with store math because reading back returns the measured value and the register cannot be read")>
    Public Function ApplyStoreMath(ByVal register As StoreMathRegister, ByVal value As Double) As StoreMathRegister?
        Me.WriteStoreMath(register, value)
        Me.QueryStoreMathValue()
        Return Me.StoreMathRegister
    End Function

    ''' <summary> Gets or sets the auto zero query command. </summary>
    ''' <value> The auto Zero enabled query command. </value>
    Protected Overridable Property StoreMathQueryCommand As String = "SMATH?"

    ''' <summary> Queries the Store Math Register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' The <see cref="StoreMathRegister">Store Math Register</see> or none if unknown.
    ''' </returns>
    <Obsolete("Reading the register is not available")>
    Public Function QueryStoreMathRegister() As StoreMathRegister?
        Me.StoreMathRegister = Me.QueryFirstValue(Of StoreMathRegister)(Me.StoreMathQueryCommand, Me.StoreMathRegister)
        Return Me.StoreMathRegister
    End Function

    ''' <summary> Queries store mathematics value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The store mathematics value. </returns>
    <Obsolete("Reading back from the stored math reads the measured value")>
    Public Function QueryStoreMathValue() As Double?
        Me.StoreMathValue = Me.Query(Me.StoreMathValue, Me.StoreMathQueryCommand)
        Return Me.StoreMathValue
    End Function

    ''' <summary> Gets or sets the store mathematics command format. </summary>
    ''' <value> The store mathematics command format. </value>
    Protected Overridable Property StoreMathCommandFormat As String = "SMATH {0},{1}"

    ''' <summary>
    ''' Writes the Store Math Register and value without reading back the value from the device.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="register"> The register. </param>
    ''' <param name="value">    The  Auto Zero Mode. </param>
    ''' <returns>
    ''' The <see cref="StoreMathRegister">StoreMathRegister</see> or none if unknown.
    ''' </returns>
    Public Function WriteStoreMath(ByVal register As StoreMathRegister, ByVal value As Double) As StoreMathRegister?
        Me.StoreMathValue = value
        Me.StoreMathRegister = register
        Me.Session.Execute(String.Format(Me.StoreMathCommandFormat, register.ExtractBetween(), value))
        Return Me.StoreMathRegister
    End Function

#End Region

End Class

''' <summary> Enumerates the auto zero mode. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum AutoZeroMode

    ''' <summary> Zero measurement Is updated once, Then only after a
    ''' Function, range, aperture, NPLC, Or resolution change. </summary>
    <ComponentModel.Description("Off (OFF)")>
    [Off] = 0

    ''' <summary> Zero measurement Is updated after every measurement. </summary>
    <ComponentModel.Description("On (ON)")>
    [On] = 1

    ''' <summary> Zero measurement is updated once, then only after a
    ''' Function, range, aperture, NPLC, Or resolution change. </summary>
    <ComponentModel.Description("Once (ONCE)")>
    [Once] = 2
End Enum

''' <summary> Values that represent Mathematics modes. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum MathMode

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("OFF (OFF)")>
    None = 0

    ''' <summary> An enum constant representing the percent option. </summary>
    <ComponentModel.Description("Percent (PERC)")>
    Percent = 10
End Enum

''' <summary> Values that represent store Mathematics registers. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum StoreMathRegister

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("Not specified")>
    None = 0

    ''' <summary> An enum constant representing the offset option. </summary>
    <ComponentModel.Description("Offset (OFFSET)")>
    Offset = 7

    ''' <summary> An enum constant representing the percent option. </summary>
    <ComponentModel.Description("Percent (PERC)")>
    Percent = 8
End Enum
