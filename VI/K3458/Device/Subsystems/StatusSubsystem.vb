Imports isr.Core.EscapeSequencesExtensions

''' <summary> Defines a Status Subsystem for a Keysight K3458 meter. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-07 </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits VI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="Pith.SessionBase">message based
    '''                        session</see>. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(VI.Pith.SessionBase.Validated(session))
        Me._VersionInfo = New VersionInfo
        Me.VersionInfoBase = Me._VersionInfo
        StatusSubsystem.InitializeSession(session)
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region


#Region " SESSION "

    ''' <summary> Initializes the session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Private Shared Sub InitializeSession(ByVal session As VI.Pith.SessionBase)
        session.ClearExecutionStateCommand = String.Empty
        session.ResetKnownStateCommand = String.Empty
        session.ErrorAvailableBit = VI.Pith.ServiceRequests.StandardEvent
        session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        session.MessageAvailableBit = VI.Pith.ServiceRequests.None
        session.OperationCompletedQueryCommand = String.Empty
        session.StandardEventBit = VI.Pith.ServiceRequests.None

        session.StandardEventStatusQueryCommand = String.Empty
        session.StandardEventEnableQueryCommand = String.Empty
        session.StandardServiceEnableCommandFormat = String.Empty
        session.StandardServiceEnableCompleteCommandFormat = String.Empty
        session.ServiceRequestEnableCommandFormat = "RQS {0}"
        session.ServiceRequestEnableQueryCommand = "RQS?"
        session.OperationCompleted = True
        session.ReadTerminationCharacterEnabled = True
        session.ReadTerminationCharacter = isr.Core.EscapeSequencesExtensions.EscapeSequencesExtensionMethods.NewLineValue
        session.ServiceRequestEnabledBitmask = CType(63, VI.Pith.ServiceRequests)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Session.StandardEventStatus = 0
        Me.Session.OperationCompleted = True
        Me._VersionInfo = New VersionInfo
    End Sub

#End Region

#Region " LINE FREQUENCY "

    ''' <summary> Gets or sets line frequency query command. </summary>
    ''' <value> The line frequency query command. </value>
    Protected Overrides Property LineFrequencyQueryCommand As String = "LINE?"

#End Region

#Region " IDENTITY "

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = "ID?"

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
    ''' <returns> System.String. </returns>
    Public Overrides Function QueryIdentity() As String
        If Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.PublishVerbose("Requesting identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Me.WriteIdentityQueryCommand()
            Me.PublishVerbose("Trying to read identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            ' wait for the delay time.
            ' Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
            Dim value As String = Me.Session.ReadLineTrimEnd
            value = value.ReplaceCommonEscapeSequences.Trim
            Me.PublishVerbose($"Setting identity to {value};. ")
            Me.VersionInfo.Parse(value)
            MyBase.VersionInfoBase = Me.VersionInfo
            Me.Identity = Me.VersionInfo.Identity
        End If
        Return Me.Identity
    End Function

    ''' <summary> Gets or sets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public ReadOnly Property VersionInfo As VersionInfo

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = String.Empty

    ''' <summary> Gets or sets the error queue query command. </summary>
    ''' <remarks> Last error supported. </remarks>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = "ERRSTR?"

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DequeueErrorQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String = "ERRSTR?"

    ''' <summary> Queue device error. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="compoundErrorMessage"> Message describing the compound error. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected Overrides Function EnqueueDeviceError(ByVal compoundErrorMessage As String) As VI.DeviceError
        Dim de As New DeviceError
        de.Parse(compoundErrorMessage)
        If de.IsError Then Me.DeviceErrorQueue.Enqueue(de)
        Return de
    End Function

#End Region

End Class
