Imports System.Windows.Forms
Imports System.ComponentModel
Imports isr.Core

''' <summary> A meter control. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-09 </para>
''' </remarks>
Public Class MeterView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(K3458.K3458Device.Create())
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As K3458.K3458Device)
        MyBase.New()
        Me.NewThis(device)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub NewThis(ByVal device As K3458.K3458Device)

        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._BottomToolStrip.Renderer = New CustomProfessionalRenderer
        Me.AssignDeviceThis(device)
        Me._SubmitButton.Visible = Not Me._AutoStartCheckBox.Checked
        Me._ParseButton.Visible = Not Me._AutoStartCheckBox.Checked

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso Me.components IsNot Nothing Then
                If Me.components IsNot Nothing Then
                    Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K3458Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K3458Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Private Sub AssignDeviceThis(ByVal value As K3458Device)
        If Me._Device IsNot Nothing OrElse Me.VisaSessionBase IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        Me.Assign_Session(value)
        If value IsNot Nothing Then
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Public Overloads Sub AssignDevice(ByVal value As K3458Device)
        Me.AssignDeviceThis(value)
    End Sub

#End Region

#Region " VISA SESSION OPENER "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property VisaSessionBase As VisaSessionBase

    ''' <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="visaSessionBase"> The assigned visa session base (device base) or nothing to
    '''                                release the session. </param>
    Private Sub Assign_Session(ByVal visaSessionBase As VisaSessionBase)
        If Me.VisaSessionBase IsNot Nothing Then
            If Me.VisaSessionBase.Talker IsNot Nothing Then Me.VisaSessionBase.Talker.RemoveListeners()
            RemoveHandler Me.VisaSessionBase.Opening, AddressOf Me.HandleDeviceOpening
            RemoveHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            RemoveHandler Me.VisaSessionBase.Closing, AddressOf Me.HandleDeviceClosing
            RemoveHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed

            Me._SelectorOpener.AssignSelectorViewModel(Nothing)
            Me._SelectorOpener.AssignOpenerViewModel(Nothing)

            Me.BindVisaSession(False, Me.VisaSessionBase)
            Me.BindSession(False, Me.VisaSessionBase.Session)

            Me._VisaSessionBase = Nothing
        End If
        Me._VisaSessionBase = visaSessionBase
        If visaSessionBase IsNot Nothing Then
            AddHandler Me.VisaSessionBase.Opening, AddressOf Me.HandleDeviceOpening
            AddHandler Me.VisaSessionBase.Opened, AddressOf Me.HandleDeviceOpened
            AddHandler Me.VisaSessionBase.Closing, AddressOf Me.HandleDeviceClosing
            AddHandler Me.VisaSessionBase.Closed, AddressOf Me.HandleDeviceClosed

            Me._SelectorOpener.AssignSelectorViewModel(visaSessionBase.SessionFactory)
            Me._SelectorOpener.AssignOpenerViewModel(visaSessionBase)

            Me.BindVisaSession(True, Me.VisaSessionBase)
            Me.BindSession(True, Me.VisaSessionBase.Session)

            Me.VisaSessionBase.Session.ApplySettings()
            If Not String.IsNullOrWhiteSpace(Me.VisaSessionBase.CandidateResourceName) Then Me.VisaSessionBase.Session.CandidateResourceName = Me.VisaSessionBase.CandidateResourceName
            If Not String.IsNullOrWhiteSpace(Me.VisaSessionBase.CandidateResourceTitle) Then Me.VisaSessionBase.Session.CandidateResourceTitle = Me.VisaSessionBase.CandidateResourceTitle
            If Me.VisaSessionBase.IsDeviceOpen Then
                Me.VisaSessionBase.Session.OpenResourceName = Me.VisaSessionBase.OpenResourceName
                Me.VisaSessionBase.Session.OpenResourceTitle = Me.VisaSessionBase.OpenResourceTitle
            End If

            Me.AssignTalker(visaSessionBase.Talker)
            Me.ApplyListenerTraceLevel(ListenerType.Display, visaSessionBase.Talker.TraceShowLevel)

        End If
    End Sub

    ''' <summary> Bind visa session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindVisaSession(ByVal add As Boolean, ByVal viewModel As VisaSessionBase)
        Me.AddRemoveBinding(Me._PartPanel, add, NameOf(Control.Enabled), viewModel, NameOf(VisaSessionBase.IsDeviceOpen))
    End Sub

    ''' <summary> Bind session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="viewModel"> The view model. </param>
    Private Sub BindSession(ByVal add As Boolean, ByVal viewModel As VI.Pith.SessionBase)
        Me.AddRemoveBinding(Me._BottomToolStrip, add, NameOf(Control.Text), viewModel, NameOf(VI.Pith.SessionBase.StatusPrompt))
    End Sub

    ''' <summary> Assigns a <see cref="VisaSessionBase">Visa session</see> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="visaSessionBase"> The assigned visa session base (device base) or nothing to
    '''                                release the session. </param>
    Public Overridable Sub AssignSession(ByVal visaSessionBase As VisaSessionBase)
        Me.Assign_Session(visaSessionBase)
    End Sub

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Gets or sets the resource name caption. </summary>
    ''' <value> The resource name caption. </value>
    Protected Property ResourceNameCaption As String
        Get
            Return Me._ResourceNameLabel.Text
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ResourceNameCaption) Then
                Me._ResourceNameLabel.Text = Me.VisaSessionBase.OpenResourceName
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Executes the device Closing action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Handles the device Closing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceNameCaption} UI handling device closing" : Me.PublishVerbose($"{activity};. ")
            Me.OnDeviceClosing(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device Closed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overridable Sub OnDeviceClosed()
    End Sub

    ''' <summary> Handles the device Close. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceNameCaption} UI handling device closed" : Me.PublishVerbose($"{activity};. ")
            Me.OnDeviceClosed()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device Opening action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnDeviceOpening(ByVal e As System.ComponentModel.CancelEventArgs)
        Me.ResourceNameCaption = Me.VisaSessionBase.ResourceNameCaption
    End Sub

    ''' <summary> Handles the device Opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device Opening" : Me.PublishVerbose($"{activity};. ")
            Me.OnDeviceOpening(e)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overridable Sub OnDeviceOpened()
        Me.ResourceNameCaption = Me.VisaSessionBase.ResourceNameCaption
    End Sub

    ''' <summary> Handles the device opened. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleDeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.VisaSessionBase.ResourceNameCaption} UI handling device opened" : Me.PublishVerbose($"{activity};. ")
            Me.OnDeviceOpened()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Configure meter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="nominalValue"> The nominal value. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ConfigureMeter(ByVal nominalValue As Double)
        Dim action As String = $"Configuring {Me.Device.ResourceNameCaption}"
        Try
            Dim expectedSenseFunctionMode As SenseFunctionModes = SenseFunctionModes.ResistanceFourWire
            action = $"Configuring function mode {expectedSenseFunctionMode}"
            Dim actualSenseFunctionMode As SenseFunctionModes? = Me.Device.MeasureSubsystem.ApplyFunctionMode(expectedSenseFunctionMode)
            If actualSenseFunctionMode.HasValue Then
                If expectedSenseFunctionMode <> actualSenseFunctionMode.Value Then
                    Me.PublishWarning($"Failed {action} Expected {expectedSenseFunctionMode} <> Actual {actualSenseFunctionMode.Value}")
                    Return
                End If
            Else
                Me.PublishWarning($"Failed {action}--no value set")
                Return
            End If

            Dim expectedAutoZeroMode As AutoZeroMode = Me.AutoZeroMode
            action = $"Configuring auto zero {expectedAutoZeroMode}"
            Dim actualAutoZeroMode As AutoZeroMode? = Me.Device.MeasureSubsystem.ApplyAutoZeroMode(expectedAutoZeroMode)
            If actualAutoZeroMode.HasValue Then
                If expectedAutoZeroMode <> actualAutoZeroMode.Value Then
                    Me.PublishWarning($"Failed {action} Expected {expectedAutoZeroMode} <> Actual {actualAutoZeroMode.Value}")
                    Return
                End If
            Else
                Me.PublishWarning($"Failed {action}--no value set")
                Return
            End If
            Me.AutoZeroMode = actualAutoZeroMode.Value

            Dim expectedPowerLineCycles As Double = Me.PowerLineCycles
            action = $"Configuring power line cycles {expectedAutoZeroMode}"
            Dim actualPowerLineCycles As Double? = Me.Device.MeasureSubsystem.ApplyPowerLineCycles(expectedPowerLineCycles)
            If actualPowerLineCycles.HasValue Then
                If expectedPowerLineCycles <> actualPowerLineCycles.Value Then
                    Me.PublishWarning($"Failed {action} Expected {expectedPowerLineCycles} <> Actual {actualPowerLineCycles.Value}")
                    Return
                End If
            Else
                Me.PublishWarning($"Failed {action}--no value set")
                Return
            End If
            Me.PowerLineCycles = actualPowerLineCycles.Value

            Dim expectedMathMode As MathMode = MathMode.Percent
            action = $"Configuring math mode {expectedMathMode}"
            Dim actualMathMode As MathMode? = Me.Device.MeasureSubsystem.ApplyMathMode(expectedMathMode)
            If actualMathMode.HasValue Then
                If expectedMathMode <> actualMathMode.Value Then
                    Me.PublishWarning($"Failed {action} Expected {expectedMathMode} <> Actual {actualMathMode.Value}")
                    Return
                End If
            Else
                Me.PublishWarning($"Failed {action}--no value set")
                Return
            End If

            ' nominalValue = Me.NominalOffsetInfo.NominalValue + Me.NominalOffsetInfo.NominalLimit
            Dim expectedStoreMathRegister As StoreMathRegister = StoreMathRegister.Percent
            Dim expectedStoreMathValue As Double = nominalValue
            action = $"Configuring store math mode {expectedStoreMathRegister} {nominalValue}"
            Dim actualStoreMathRegister As StoreMathRegister? = Me.Device.MeasureSubsystem.WriteStoreMath(expectedStoreMathRegister, expectedStoreMathValue)
            If actualStoreMathRegister.HasValue Then
                If expectedStoreMathRegister <> actualStoreMathRegister.Value Then
                    Me.PublishWarning($"Failed {action} Expected {expectedStoreMathRegister} <> Actual {actualStoreMathRegister.Value}")
                    Return
                End If
            Else
                Me.PublishWarning($"Failed {action}--no value set")
                Return
            End If
            Me._AssignedValueLabel.Text = nominalValue.ToString
            Me._NominalOffsetLabel.Text = Me.OffsetValue.ToString
            Me._NominalValueLabel.Text = Me.NominalValue.ToString
        Catch ex As Exception
            Me.PublishException(action, ex)
        End Try
    End Sub

#End Region

#Region " METER SETTIGNS "

    ''' <summary> Name of the resource. </summary>
    Private _ResourceName As String

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName As String
        Get
            Return Me._ResourceName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.ResourceName) OrElse Not String.Equals(value, Me.Device.CandidateResourceName) Then
                Me._ResourceName = value
                Me._Device.CandidateResourceName = value
                ' TO_DO: Do we need this? Me.Device.SessionFactory.EnumerateResources(False)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The automatic zero mode. </summary>
    Private _AutoZeroMode As AutoZeroMode

    ''' <summary> Gets or sets the automatic zero mode. </summary>
    ''' <value> The automatic zero mode. </value>
    Public Property AutoZeroMode As AutoZeroMode
        Get
            Return Me._AutoZeroMode
        End Get
        Set(value As AutoZeroMode)
            If value <> Me.AutoZeroMode Then
                Me._AutoZeroMode = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The power line cycles. </summary>
    Private _PowerLineCycles As Double

    ''' <summary> Gets or sets the power line cycles. </summary>
    ''' <value> The power line cycles. </value>
    Public Property PowerLineCycles As Double
        Get
            Return Me._PowerLineCycles
        End Get
        Set(value As Double)
            If value <> Me.PowerLineCycles Then
                Me._PowerLineCycles = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CONTROL VISIBILITY "

    ''' <summary> True to enable, false to disable the automatic configure. </summary>
    Private _AutoConfigureEnabled As Boolean

    ''' <summary> Gets or sets the automatic configure enabled. </summary>
    ''' <value> The automatic configure enabled. </value>
    Public Property AutoConfigureEnabled As Boolean
        Get
            Return Me._AutoConfigureEnabled
        End Get
        Set(value As Boolean)
            If value <> Me.AutoConfigureEnabled OrElse value <> Me._AutoStartCheckBox.Checked Then
                Me._AutoConfigureEnabled = value
                Me._AutoStartCheckBox.Checked = value
                Me._SubmitButton.Visible = Not Me.AutoConfigureEnabled
                Me._ParseButton.Visible = Not Me.AutoConfigureEnabled
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Automatic start check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoStartCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _AutoStartCheckBox.CheckedChanged
        If Me.InitializingComponents Then Return
        Me.AutoConfigureEnabled = Me._AutoStartCheckBox.Checked
    End Sub

    ''' <summary> True to enable, false to disable the automatic connect. </summary>
    Private _AutoConnectEnabled As Boolean

    ''' <summary> Gets or sets the automatic connect enabled. </summary>
    ''' <value> The automatic connect enabled. </value>
    Public Property AutoConnectEnabled As Boolean
        Get
            Return Me._AutoConnectEnabled
        End Get
        Set(value As Boolean)
            If value <> Me.AutoConnectEnabled OrElse value <> Me._AutoConnectCheckBox.Checked Then
                Me._AutoConnectEnabled = value
                Me._AutoConnectCheckBox.Checked = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Automatic connect check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoConnectCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _AutoConnectCheckBox.CheckedChanged
        If Me.InitializingComponents Then Return
        Me.AutoConfigureEnabled = Me._AutoConnectCheckBox.Checked
    End Sub

    ''' <summary> Focus part number. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub FocusPartNumber()
        Me._PartNumberTextBox.Focus()
    End Sub

#End Region

#Region " PART NUMBER PARSING "

    ''' <summary> The part number. </summary>
    Private _PartNumber As String

    ''' <summary> Gets or sets the part number. </summary>
    ''' <value> The part number. </value>
    Public Property PartNumber As String
        Get
            Return Me._PartNumber
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.PartNumber) OrElse Not String.Equals(value, Me._PartNumberTextBox.Text) Then
                Me._PartNumber = value
                Me._PartNumberTextBox.Text = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Part number text box validated. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PartNumberTextBox_Validated(sender As Object, e As EventArgs) Handles _PartNumberTextBox.Validated
        If Me.InitializingComponents Then Return
        Me.PartNumber = Me._PartNumberTextBox.Text
    End Sub

    ''' <summary> Filename of the nominal offsets file. </summary>
    Private _NominalOffsetsFileName As String

    ''' <summary> Gets or sets the filename of the nominal offsets file. </summary>
    ''' <value> The filename of the nominal offsets file. </value>
    Public Property NominalOffsetsFileName As String
        Get
            Return Me._NominalOffsetsFileName
        End Get
        Set(value As String)
            If Not String.Equals(value, Me.NominalOffsetsFileName) Then
                Me._NominalOffsetsFileName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The nominal value. </summary>
    Private _NominalValue As Decimal

    ''' <summary> Gets or sets the nominal value. </summary>
    ''' <value> The nominal value. </value>
    Public Property NominalValue As Decimal
        Get
            Return Me._NominalValue
        End Get
        Set(value As Decimal)
            If value <> Me.NominalValue OrElse value <> Me._NominalValueNumeric.Value Then
                Me._NominalValue = value
                Me._NominalValueNumeric.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Nominal value numeric validated. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub NominalValueNumeric_Validated(sender As Object, e As EventArgs) Handles _NominalValueNumeric.Validated
        If Me.InitializingComponents Then Return
        Me._NominalValue = Me._NominalValueNumeric.Value
    End Sub

    ''' <summary> The offset value. </summary>
    Private _OffsetValue As Decimal

    ''' <summary> Gets or sets the offset value. </summary>
    ''' <value> The offset value. </value>
    Public Property OffsetValue As Decimal
        Get
            Return Me._OffsetValue
        End Get
        Set(value As Decimal)
            If value <> Me.OffsetValue OrElse value <> Me._OffsetValueNumeric.Value Then
                Me._OffsetValue = value
                Me._OffsetValueNumeric.Value = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Offset value numeric validated. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OffsetValueNumeric_Validated(sender As Object, e As EventArgs) Handles _OffsetValueNumeric.Validated
        If Me.InitializingComponents Then Return
        Me.OffsetValue = Me._OffsetValueNumeric.Value
    End Sub

    ''' <summary> Attempts to parse from the given data. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="partNumber"> The part number. </param>
    ''' <param name="fileName">   Filename of the file. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Overridable Function TryParse(ByVal partNumber As String, ByVal fileName As String) As Boolean
        Dim affirmative As Boolean = True
        Return affirmative
    End Function

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Parse button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub ParseButton_Click(sender As Object, e As EventArgs) Handles _ParseButton.Click
        Me.TryParse(Me.PartNumber, Me.NominalOffsetsFileName)
    End Sub

    ''' <summary> Submit button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SubmitButton_Click(sender As Object, e As EventArgs) Handles _SubmitButton.Click
        Dim args As New isr.Core.ActionEventArgs
        If Me._SelectorOpener.OpenerViewModel.TryOpen(args) Then
            Me.ConfigureMeter(CDbl(Me.NominalValue + Me.OffsetValue))
        Else
            Me.Publish(args)
        End If
    End Sub

    ''' <summary> Event handler. Called by _PartNumberTextBox for key up events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub EnterKeyHandler(sender As Object, e As KeyEventArgs) Handles _PartNumberTextBox.KeyUp
        If e Is Nothing Then Return
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Return Then
            Dim args As New isr.Core.ActionEventArgs
            If Me.TryParse(Me.PartNumber, Me.NominalOffsetsFileName) Then
                If Me.AutoConfigureEnabled Then
                    If Me._SelectorOpener.OpenerViewModel.TryOpen(args) Then
                        Me.ConfigureMeter(CDbl(Me._NominalValueNumeric.Value + Me._OffsetValueNumeric.Value))
                    Else
                        Me.Publish(args)
                    End If
                ElseIf Me.AutoConnectEnabled Then
                    If Me._SelectorOpener.OpenerViewModel.TryOpen(args) Then
                    Else
                        Me.Publish(args)
                    End If
                End If
            End If
            Me._PartNumberTextBox.SelectAll()
        End If
    End Sub

    ''' <summary> Event handler. Called by _PartNumberTextBox for key down events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Key event information. </param>
    Private Sub SuppressDingHandler(sender As Object, e As KeyEventArgs) Handles _PartNumberTextBox.KeyDown
        If e.KeyCode = Keys.Enter OrElse e.KeyCode = Keys.Return Then
            e.Handled = True
            e.SuppressKeyPress = True
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

#End Region

#Region " TOOL STRIP RENDERER "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        'Me._TopToolStrip.Renderer = New CustomProfessionalRenderer
        'Me._TopToolStrip.Invalidate()
        MyBase.OnLoad(e)
    End Sub

    ''' <summary> A custom professional renderer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Class CustomProfessionalRenderer
        Inherits ToolStripProfessionalRenderer

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
        ''' event.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
        '''                  contains the event data. </param>
        Protected Overrides Sub OnRenderLabelBackground(ByVal e As ToolStripItemRenderEventArgs)
            If e IsNot Nothing AndAlso (e.Item.BackColor <> System.Drawing.SystemColors.ControlDark) Then
                Using brush As New System.Drawing.SolidBrush(e.Item.BackColor)
                    e.Graphics.FillRectangle(brush, e.Item.ContentRectangle)
                End Using
            End If
        End Sub
    End Class

#End Region

End Class
