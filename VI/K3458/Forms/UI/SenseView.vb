Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A measure view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SenseView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SenseView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As SenseView
        Dim view As SenseView = Nothing
        Try
            view = New SenseView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K3458Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K3458Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K3458Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(value.Talker)
        End If
        Me.BindSenseSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K3458Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE "

    ''' <summary> Gets the Sense subsystem. </summary>
    ''' <value> The Sense subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseSubsystem As VI.K3458.MeasureSubsystem

    ''' <summary> Bind Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSenseSubsystem(ByVal device As K3458.K3458Device)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SenseSubsystem = device.MeasureSubsystem
            Me.BindSubsystem(True, Me.SenseSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K3458.MeasureSubsystem.SupportedFunctionModes))
            Me.HandlePropertyChanged(subsystem, NameOf(K3458.MeasureSubsystem.FunctionMode))
            Me.HandlePropertyChanged(subsystem, NameOf(K3458.MeasureSubsystem.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(K3458.MeasureSubsystem.FunctionRangeDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(K3458.MeasureSubsystem.FunctionUnit))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.BindSenseFunctionSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Gets the selected function mode. </summary>
    ''' <value> The selected function mode. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedFunctionMode() As VI.SenseFunctionModes
        Get
            Return Me._SenseFunctionComboBox.SelectedSenseFunctionModes
        End Get
    End Property

    ''' <summary> Handles the supported function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub HandleSupportedFunctionModesChanged(ByVal subsystem As MeasureSubsystem)
        If subsystem IsNot Nothing AndAlso subsystem.SupportedFunctionModes <> VI.SenseFunctionModes.None Then
            Dim init As Boolean = Me.InitializingComponents
            Try
                Me.InitializingComponents = True
                Me._SenseFunctionComboBox.ListSupportedSenseFunctionModes(subsystem.SupportedFunctionModes)
            Catch
                Throw
            Finally
                Me.InitializingComponents = init
            End Try
        End If
    End Sub

    ''' <summary> Handles the function modes changed described by subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub HandleFunctionModesChanged(ByVal subsystem As MeasureSubsystem)
        Me._SenseFunctionComboBox.SelectSenseFunctionModes(subsystem.FunctionMode.GetValueOrDefault(VI.SenseFunctionModes.VoltageDC))
        Dim value As SenseFunctionModes = subsystem.FunctionMode.GetValueOrDefault(SenseFunctionModes.VoltageDC)
        ' this is done at the device level by applying the function mode to the measure subsystem
        ' Me.Device.MeasureSubsystem.Readings.Reading.ApplyUnit(subsystem.ToUnit(value))
        Me._SenseFunctionComboBox.SafeSelectSenseFunctionModes(value)
        Select Case subsystem.FunctionMode
            Case VI.SenseFunctionModes.CurrentDC
                'Me.BindSenseFunctionSubsystem(Me.Device.SenseCurrentSubsystem)
            Case VI.SenseFunctionModes.VoltageDC
                'Me.BindSenseFunctionSubsystem(Me.Device.SenseVoltageSubsystem)
            Case VI.SenseFunctionModes.ResistanceFourWire
                'Me.BindSenseFunctionSubsystem(Me.Device.SenseResistanceFourWireSubsystem)
            Case VI.SenseFunctionModes.Resistance
                'Me.BindSenseFunctionSubsystem(Me.Device.SenseResistanceSubsystem)
        End Select
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MeasureSubsystem, ByVal propertyName As String)
        If Me.InitializingComponents OrElse subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(K3458.MeasureSubsystem.FunctionMode)
                Me.HandleFunctionModesChanged(subsystem)
            Case NameOf(K3458.MeasureSubsystem.FunctionRange)
                Me._SenseRangeNumeric.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)
            Case NameOf(K3458.MeasureSubsystem.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.DecimalPlaces = subsystem.FunctionRangeDecimalPlaces
            Case NameOf(K3458.MeasureSubsystem.FunctionUnit)
                Me._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:"
                Me._SenseRangeNumericLabel.Left = Me._SenseRangeNumeric.Left - Me._SenseRangeNumericLabel.Width
            Case NameOf(K3458.MeasureSubsystem.SupportedFunctionModes)
                Me.HandleSupportedFunctionModesChanged(subsystem)
        End Select
    End Sub

    ''' <summary> Sense subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(MeasureSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, MeasureSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE FUNCTION "

    ''' <summary> Gets or sets the sense function subsystem. </summary>
    ''' <value> The sense function subsystem. </value>
    Public ReadOnly Property SenseFunctionSubsystem As VI.SenseFunctionSubsystemBase

    ''' <summary> Bind Sense function subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseFunctionSubsystem(ByVal subsystem As VI.SenseFunctionSubsystemBase)
        If Me.SenseFunctionSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseFunctionSubsystem)
            Me._SenseFunctionSubsystem = Nothing
        End If
        Me._SenseFunctionSubsystem = subsystem
        If subsystem IsNot Nothing Then
            Me.BindSubsystem(True, Me.SenseFunctionSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As VI.SenseFunctionSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseFunctionSubsystemPropertyChanged
            ' must Not read setting when biding because the instrument may be locked Or in a trigger mode
            ' The bound values should be sent when binding Or when applying propert change.
            ' TO_DO: Implement this: Me.ApplyPropertyChanged(subsystem)
            ' subsystem.QueryAutoRangeEnabled()
            ' subsystem.QueryPowerLineCycles()
            ' subsystem.QueryRange()
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseFunctionSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.SenseFunctionSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(VI.SenseFunctionSubsystemBase.AutoRangeEnabled)
                If Me.Device IsNot Nothing AndAlso subsystem.AutoRangeEnabled.HasValue Then
                    Me._SenseAutoRangeToggle.Checked = subsystem.AutoRangeEnabled.Value
                End If
            Case NameOf(VI.SenseFunctionSubsystemBase.PowerLineCycles)
                If Me.Device IsNot Nothing AndAlso subsystem.PowerLineCycles.HasValue Then
                    Dim nplc As Double = subsystem.PowerLineCycles.Value
                    Me._IntegrationPeriodNumeric.ValueSetter(StatusSubsystemBase.FromPowerLineCycles(nplc).TotalMilliseconds)
                End If
            Case NameOf(VI.SenseFunctionSubsystemBase.Range)
                If Me.Device IsNot Nothing AndAlso subsystem.Range.HasValue Then
                    Me._SenseRangeNumeric.ValueSetter(subsystem.Range.Value)
                End If

        End Select
    End Sub

    ''' <summary> Sense function subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseFunctionSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.SenseFunctionSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseFunctionSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SenseFunctionSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SENSE "

    ''' <summary>
    ''' Event handler. Called by _SenseFunctionComboBox for selected index changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub SenseFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SenseFunctionComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim control As Windows.Forms.Control = TryCast(sender, Windows.Forms.Control)
        If control IsNot Nothing Then
            '!@# Me.Device.MeasureSubsystem.ApplyFunctionMode(Me.SelectedFunctionMode)
        End If
    End Sub

    ''' <summary> Applies the selected measurements settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ApplySenseSettings()
        Me.SenseFunctionSubsystem.ApplyPowerLineCycles(StatusSubsystemBase.ToPowerLineCycles(TimeSpan.FromMilliseconds(Me._IntegrationPeriodNumeric.Value)))
        Me.SenseFunctionSubsystem.ApplyAutoRangeEnabled(Me._SenseAutoRangeToggle.Checked)
        If Not Me._SenseAutoRangeToggle.Checked Then Me.SenseFunctionSubsystem.ApplyRange(Me._SenseRangeNumeric.Value)
    End Sub

    ''' <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ApplySenseSettingsButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} applying sense settings"
            Me.ApplySenseSettings()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
