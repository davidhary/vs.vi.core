Imports System.Configuration

Partial Friend NotInheritable Class TestAssist

#Region " DEVICE INFORMATION "

    ''' <summary> Gets the auto zero settings. </summary>
    ''' <value> The auto zero settings. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Shared ReadOnly Property AutoZero As Integer
        Get
            Return Convert.ToInt32(ConfigurationManager.AppSettings(NameOf(TestAssist.AutoZero)))
        End Get
    End Property

    ''' <summary> Gets the Sense Function settings. </summary>
    ''' <value> The Sense Function settings. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Shared ReadOnly Property SenseFunction As Integer
        Get
            Return Convert.ToInt32(ConfigurationManager.AppSettings(NameOf(TestAssist.SenseFunction)))
        End Get
    End Property

    ''' <summary> Gets the power line cycles settings. </summary>
    ''' <value> The power line cycles settings. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Shared ReadOnly Property PowerLineCycles As Double
        Get
            Return Convert.ToInt32(ConfigurationManager.AppSettings(NameOf(TestAssist.PowerLineCycles)))
        End Get
    End Property

    ''' <summary> Gets the math mode settings. </summary>
    ''' <value> The math mode settings. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Shared ReadOnly Property MathMode As Integer
        Get
            Return Convert.ToInt32(ConfigurationManager.AppSettings(NameOf(TestAssist.MathMode)))
        End Get
    End Property

    ''' <summary> Gets the store mathematics register. </summary>
    ''' <value> The store  mathematics register. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Shared ReadOnly Property StoreMathRegister As Integer
        Get
            Return Convert.ToInt32(ConfigurationManager.AppSettings(NameOf(TestAssist.StoreMathRegister)))
        End Get
    End Property

    ''' <summary> Gets the store math value settings. </summary>
    ''' <value> The store math value settings. </value>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Shared ReadOnly Property StoreMathValue As Double
        Get
            Return Convert.ToInt32(ConfigurationManager.AppSettings(NameOf(TestAssist.StoreMathValue)))
        End Get
    End Property

#End Region

End Class

