Imports isr.VI.K3458

''' <summary> K3458 Device Configuration unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k3458")>
Public Class DeviceConfigureTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
			_TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
			_TestSite.InitializeTraceListener()
			DeviceConfigureTests.InitializeClass()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
		_TestSite?.Dispose()
		DeviceConfigureTests.CleanupClass()
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	''' <remarks> David, 2020-10-12. </remarks>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
		Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
		Dim expectedUpperLimit As Double = 12
		Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
		TestInfo.ClearMessageQueue()
	End Sub

	''' <summary> Cleans up after each test has run. </summary>
	''' <remarks> David, 2020-10-12. </remarks>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	''' <summary>
	''' Gets the test context which provides information about and functionality for the current test
	''' run.
	''' </summary>
	''' <value> The test context. </value>
	Public Property TestContext() As TestContext
	''' <summary> The test site. </summary>
	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " ADDITIONAL TEST ATTRIBUTES "

	''' <summary> Gets or sets the device. </summary>
	''' <value> The device. </value>
	Public Shared Property Device As K3458Device

	''' <summary> Initialize class. </summary>
	''' <remarks> David, 2020-10-12. </remarks>
	Private Shared Sub InitializeClass()
		DeviceConfigureTests.Device = New K3458Device()
		DeviceConfigureTests.Device.TryOpenSession(ResourceSettings.Get.ResourceName, ResourceSettings.Get.ResourceTitle)
	End Sub

	''' <summary> Cleanup class. </summary>
	''' <remarks> David, 2020-10-12. </remarks>
	Private Shared Sub CleanupClass()
		If DeviceConfigureTests.Device IsNot Nothing Then
			If DeviceConfigureTests.Device.IsDeviceOpen Then
				DeviceConfigureTests.Device.CloseSession()
			End If
			DeviceConfigureTests.Device.Dispose()
			DeviceConfigureTests.Device = Nothing
		End If
	End Sub

#End Region

#Region " DEVICE CONFIGURATION TESTS "

    ''' <summary> (Unit Test Method) queries if a given device should be open. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DeviceShouldBeOpen()
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean = DeviceConfigureTests.Device.IsDeviceOpen
        Assert.AreEqual(expectedBoolean, actualBoolean, $"Open {ResourceSettings.Get.ResourceName}")
    End Sub

    ''' <summary> (Unit Test Method) device configuration should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub DeviceConfigurationShouldPass()
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean = DeviceConfigureTests.Device.IsDeviceOpen
        Assert.AreEqual(expectedBoolean, actualBoolean, $"Open {ResourceSettings.Get.ResourceName}")

        ' ! AUTO ZERO ONCE RETURNS OFF
        Dim expectedAutoZeroMode As AutoZeroMode = CType(TestAssist.AutoZero, AutoZeroMode)
        Dim actualAutoZeroMode As AutoZeroMode? = Device.MeasureSubsystem.ApplyAutoZeroMode(expectedAutoZeroMode)
        Assert.AreEqual(True, actualAutoZeroMode.HasValue, $"Auto zero mode has value")
        Assert.AreEqual(expectedAutoZeroMode, actualAutoZeroMode.Value, $"Auto zero mode applied")

        Dim expectedSenseFunctionMode As VI.SenseFunctionModes = CType(TestAssist.SenseFunction, VI.SenseFunctionModes)
        Dim actualSenseFunctionMode As VI.SenseFunctionModes? = Device.MeasureSubsystem.ApplyFunctionMode(expectedSenseFunctionMode)
        Assert.AreEqual(True, actualSenseFunctionMode.HasValue, $"Sense function mode has value")
        Assert.AreEqual(expectedSenseFunctionMode, actualSenseFunctionMode.Value, $"Sense function mode applied")

        Dim expectedDouble As Double = TestAssist.PowerLineCycles
        Dim actualDouble As Double? = Device.MeasureSubsystem.ApplyPowerLineCycles(expectedDouble)
        Assert.AreEqual(True, actualDouble.HasValue, $"Power line cycles has value")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Power line cycles applied")

        Dim expectedMathMode As MathMode = CType(TestAssist.MathMode, MathMode)
        Dim actualMathMode As MathMode? = Device.MeasureSubsystem.ApplyMathMode(expectedMathMode)
        Assert.AreEqual(True, actualMathMode.HasValue, $"Math mode has value")
        Assert.AreEqual(expectedMathMode, actualMathMode.Value, $"Math mode applied")

        Dim expectedStoreMathRegister As StoreMathRegister = CType(TestAssist.StoreMathRegister, StoreMathRegister)
        expectedDouble = TestAssist.StoreMathValue
        Dim actualStoreMathRegister As StoreMathRegister? = Device.MeasureSubsystem.WriteStoreMath(expectedStoreMathRegister, expectedDouble)
        actualDouble = Device.MeasureSubsystem.StoreMathValue
        Assert.AreEqual(True, actualStoreMathRegister.HasValue, $"Store math register has value")
        Assert.AreEqual(expectedStoreMathRegister, actualStoreMathRegister.Value, $"Store Math Register applied")

        Assert.AreEqual(True, actualDouble.HasValue, $"Store math value has value")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Store Math value applied")

    End Sub

#End Region

End Class
