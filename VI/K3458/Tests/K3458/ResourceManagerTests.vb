''' <summary> K3458 resource manager unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k3458")>
Public Class ResourceManagerTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(K3458Tests.ResourceSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " VISA RESOURCE TESTS "

    ''' <summary> (Unit Test Method) visa resource manager should include resource. </summary>
    ''' <remarks> Finds the resource using the session factory resources manager. </remarks>
    <TestMethod()>
    Public Sub VisaResourceManagerShouldIncludeResource()
        isr.VI.DeviceTests.DeviceManager.AssertVisaResourceManagerShouldIncludeResource(ResourceSettings.Get)
    End Sub

    ''' <summary> (Unit Test Method) visa session base should find resource. </summary>
    ''' <remarks> Finds the resource using the device class. </remarks>
    <TestMethod()>
    Public Sub VisaSessionBaseShouldFindResource()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As isr.VI.K3458.K3458Device = VI.K3458.K3458Device.Create
            isr.VI.DeviceTests.DeviceManager.AssertVisaSessionBaseShouldFindResource(device, ResourceSettings.Get)
        End Using
    End Sub

#End Region

#Region " TRACE MESSAGES TESTS "

    ''' <summary> (Unit Test Method) device trace message should be queued. </summary>
    ''' <remarks> Checks if the device adds a trace message to a listener. </remarks>
    <TestMethod()>
    Public Sub DeviceTraceMessageShouldBeQueued()
        Using device As VI.K3458.K3458Device = VI.K3458.K3458Device.Create
            isr.VI.DeviceTests.DeviceManager.AssertDeviceTraceMessageShouldBeQueued(TestInfo, device)
        End Using
    End Sub

#End Region

End Class
