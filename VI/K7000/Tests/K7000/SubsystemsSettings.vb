''' <summary> The Subsystems Test Settings. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class SubsystemsSettings
    Inherits isr.VI.DeviceTests.SubsystemsSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(SubsystemsSettings)} Editor", SubsystemsSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As SubsystemsSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SubsystemsSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New SubsystemsSettings()), SubsystemsSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " INITIAL VALUES: ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the initial closed channels. </summary>
    ''' <value> The initial closed channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@)")>
    Public Overrides Property InitialClosedChannels As String
        Get
            Return MyBase.InitialClosedChannels
        End Get
        Set(value As String)
            MyBase.InitialClosedChannels = value
        End Set
    End Property

    ''' <summary> Gets or sets the Initial scan list settings. </summary>
    ''' <value> The initial scan list settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1!1:1!10)")>
    Public Overrides Property InitialScanList As String
        Get
            Return MyBase.InitialScanList
        End Get
        Set(value As String)
            MyBase.InitialScanList = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: TRIGGER SUBSYSTEM "

    ''' <summary> Gets or sets the initial trigger source. </summary>
    ''' <value> The initial trigger source. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Immediate")>
    Public Overrides Property InitialTriggerSource As VI.TriggerSources
        Get
            Return MyBase.InitialTriggerSource
        End Get
        Set(value As VI.TriggerSources)
            MyBase.InitialTriggerSource = value
        End Set
    End Property

#End Region

#Region " INITIAL VALUES: SYSTEM SUBSYSTEM "

    ''' <summary> Gets or sets the number of scan cards. </summary>
    ''' <value> The number of scan cards. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("2")>
    Public Overrides Property ScanCardCount As Integer?
        Get
            Return MyBase.ScanCardCount
        End Get
        Set(value As Integer?)
            MyBase.ScanCardCount = value
        End Set
    End Property

#End Region

#Region " ROUTE SUBSYSTEM "

    ''' <summary> Gets or sets the names of the candidate resources. </summary>
    ''' <value> The names of the candidate resources. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
            Global.System.Configuration.DefaultSettingValueAttribute("C7054|C7054")>
    Public Property SlotCardNames As IEnumerable(Of String)
        Get
            Return MyBase.AppSettingGetter(Array.Empty(Of String))
        End Get
        Set(value As IEnumerable(Of String))
            MyBase.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets scan list from one to ten. </summary>
    ''' <value> A List of one ten scans. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1!1:1!10)")>
    Public Property OneTenScanList As String
        Get
            Return MyBase.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            MyBase.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the state of the one ten channels. </summary>
    ''' <value> The one ten channels state. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1,1,1,1,1,1,1,1,1,1")>
    Public Property OneTenChannelsState As String
        Get
            Return MyBase.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            MyBase.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " BUS TRIGGER TEST "

    ''' <summary> Gets or sets the bus trigger test trigger source. </summary>
    ''' <value> The bus trigger test trigger source. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Bus")>
    Public Property BusTriggerTestTriggerSource As VI.TriggerSources
        Get
            Return Me.AppSettingEnum(Of VI.TriggerSources)
        End Get
        Set(value As VI.TriggerSources)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of bus trigger test scans. </summary>
    ''' <value> A list of bus trigger test scans. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1!1:1!10)")>
    Public Property BusTriggerTestScanList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of bus trigger test triggers. </summary>
    ''' <value> The number of bus trigger test triggers. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Property BusTriggerTestTriggerCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the bus trigger test trigger delay. </summary>
    ''' <value> The bus trigger test trigger delay. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.5")>
    Public Property BusTriggerTestTriggerDelay As TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

Partial Friend Module K7000Properties

    ''' <summary> Gets information describing the 7000 subsystems. </summary>
    ''' <value> Information describing the 7000 subsystems. </value>
    Friend ReadOnly Property SubsystemsInfo As Global.isr.VI.K7000Tests.SubsystemsSettings
        Get
            Return SubsystemsSettings.Get
        End Get
    End Property
End Module


