Imports isr.Core.SplitExtensions

Partial Public Class SubsystemsTests

    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SlotCardInfoShouldMatch()
        Using device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                K7000Tests.DeviceManager.OpenSession(TestInfo, device)
                Dim slotCardNumber As Integer = 0
                For Each slotCardName As String In SubsystemsSettings.Get.SlotCardNames
                    slotCardNumber += 1
                    TestInfo.TraceMessage($"Running {NameOf(isr.VI.DeviceTests.DeviceManager)}.{NameOf(isr.VI.DeviceTests.DeviceManager.AssertSlotCardInfoShouldMatch)} Slot Card: #{slotCardNumber}={slotCardName}")
                    isr.VI.DeviceTests.DeviceManager.AssertSlotCardInfoShouldMatch(device.RouteSubsystem, slotCardNumber, slotCardName)
                Next
            Catch
                Throw
            Finally
                device.TriggerSubsystem.Abort()
                K7000Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) scans the list should toggle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ScanListShouldToggle()
        Using device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                K7000Tests.DeviceManager.OpenSession(TestInfo, device)
                device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromMilliseconds(200))
                Dim propertyName As String = NameOf(RouteSubsystemBase.ClosedChannels).SplitWords
                Assert.IsNull(device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should be null")
                propertyName = NameOf(RouteSubsystemBase.ClosedChannelsState).SplitWords
                Assert.IsNull(device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should be null")
                Try
                    device.RouteSubsystem.ApplyClosedChannels(SubsystemsSettings.Get.OneTenScanList, TimeSpan.FromMilliseconds(200))
                    propertyName = NameOf(RouteSubsystemBase.ClosedChannels).SplitWords
                    Assert.IsNotNull(device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should be closed")
                    Assert.AreEqual(SubsystemsSettings.Get.OneTenScanList, device.RouteSubsystem.ClosedChannels, $"'{propertyName}' should match")

                    propertyName = NameOf(RouteSubsystemBase.ClosedChannelsState).SplitWords
                    Assert.IsNotNull(device.RouteSubsystem.ClosedChannelsState, $"'{propertyName}' should be set")
                    Assert.AreEqual(SubsystemsSettings.Get.OneTenChannelsState, device.RouteSubsystem.ClosedChannelsState, $"'{propertyName}' should match")

                Catch
                    Throw
                Finally
                    device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromMilliseconds(200))
                End Try
            Catch
                Throw
            Finally
                device.TriggerSubsystem.Abort()
                K7000Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

End Class
