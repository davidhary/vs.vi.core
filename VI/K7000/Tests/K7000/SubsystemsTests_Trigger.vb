Imports isr.Core.SplitExtensions

Partial Public Class SubsystemsTests

    ''' <summary> Assert bus trigger scan should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertBusTriggerScanShouldPass(ByVal device As VI.K7000.K7000Device)

        Dim propertyName As String = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords
        Dim expectedScanList As String = SubsystemsSettings.Get.InitialScanList
        Dim actualScanList As String = device.RouteSubsystem.QueryScanList
        Assert.AreEqual(expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'")

        device.ClearExecutionState()
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.TriggerSubsystem.Abort()
        device.Session.EnableServiceRequestWaitComplete()
        device.RouteSubsystem.WriteOpenAll(TimeSpan.FromSeconds(1))
        device.Session.ReadStatusRegister()

        device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
        device.RouteSubsystem.QueryClosedChannels()

        propertyName = NameOf(VI.RouteSubsystemBase.ScanList).SplitWords
        expectedScanList = SubsystemsSettings.Get.BusTriggerTestScanList
        actualScanList = device.RouteSubsystem.ApplyScanList(expectedScanList)
        Assert.AreEqual(expectedScanList, actualScanList, $"{propertyName} should be '{expectedScanList}'")

        device.ArmLayer1Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer1Subsystem.ApplyArmCount(1)
        device.ArmLayer2Subsystem.ApplyArmSource(VI.ArmSources.Immediate)
        device.ArmLayer2Subsystem.ApplyArmCount(1)
        device.ArmLayer2Subsystem.ApplyDelay(TimeSpan.Zero)
        ' device.TriggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Bus)
        propertyName = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords
        Dim expectedTriggerSource As VI.TriggerSources = SubsystemsSettings.Get.BusTriggerTestTriggerSource
        Dim actualTriggerSource As VI.TriggerSources? = device.TriggerSubsystem.ApplyTriggerSource(expectedTriggerSource)
        Assert.IsTrue(actualTriggerSource.HasValue, $"{propertyName} should have a value")
        Assert.AreEqual(Of VI.TriggerSources)(expectedTriggerSource, actualTriggerSource.Value, $"{propertyName} should match")

        device.TriggerSubsystem.ApplyTriggerCount(9999) ' in place of infinite
        device.TriggerSubsystem.ApplyDelay(TimeSpan.Zero)
        device.TriggerSubsystem.ApplyTriggerLayerBypassMode(VI.TriggerLayerBypassModes.Source)

        ' start the K7000 trigger model
        device.TriggerSubsystem.Initiate()
        ' Note: device error -211 if query closed channels: Trigger ignored.
        For i As Integer = 1 To SubsystemsSettings.Get.BusTriggerTestTriggerCount
            isr.Core.ApplianceBase.DoEventsWait(SubsystemsSettings.Get.BusTriggerTestTriggerDelay)
            device.Session.AssertTrigger()
        Next
        device.TriggerSubsystem.Abort()
        device.RouteSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(1))
        device.RouteSubsystem.QueryClosedChannels()


    End Sub

    ''' <summary> (Unit Test Method) bus trigger scan should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub BusTriggerScanShouldPass()
        Using device As VI.K7000.K7000Device = VI.K7000.K7000Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            K7000Tests.DeviceManager.OpenSession(TestInfo, device)
            Try
                SubsystemsTests.AssertBusTriggerScanShouldPass(device)
            Catch
                Throw
            Finally
                device.TriggerSubsystem.Abort()
                K7000Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

End Class
