''' <summary> K2002 Visa Session unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k7001")>
Partial Public Class VisaSessionTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(ResourceSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " OPEN/CLOSE "

    ''' <summary> (Unit Test Method) visa session should open and close. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub VisaSessionShouldOpenAndClose()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen(TestInfo, session, ResourceSettings.Get)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) visa session base should open and close. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub VisaSessionBaseShouldOpenAndClose()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionBaseShouldOpen(TestInfo, session, ResourceSettings.Get)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionBaseShouldClose(TestInfo, session)
            End Try
        End Using
    End Sub

#End Region

    ''' <summary> (Unit Test Method) status bitmask should wait for. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub StatusBitmaskShouldWaitFor()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertStatusBitmaskShouldWaitFor(TestInfo, session.Session)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) message available should wait for. </summary>
    ''' <remarks>
    ''' Initial Service Request Wait Complete Bitmask is 0x00<para>
    ''' Initial Standard Event Enable Bitmask Is 0x00</para><para>
    ''' DMM2002 status Byte 0x50 0x10 bitmask was Set</para>
    ''' </remarks>
    <TestMethod()>
    Public Sub MessageAvailableShouldWaitFor()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertMessageAvailableShouldWaitFor(TestInfo, session.Session)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) operation completion should wait for. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OperationCompletionShouldWaitFor()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertOperationCompletionShouldWaitFor(TestInfo, session.Session, ":ROUT:OPEN ALL; *OPC")
                isr.VI.DeviceTests.DeviceManager.AssertServiceRequestOperationCompletionWaitFor(TestInfo, session.Session, ":ROUT:OPEN ALL; *OPC")
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose(TestInfo, session)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) service request handling should toggle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ServiceRequestHandlingShouldToggle()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using session As isr.VI.VisaSession = VI.VisaSession.Create
            session.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldOpen(TestInfo, session, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertServiceRequestHandlingShouldToggle(TestInfo, session.Session)
            Catch
                Throw
            Finally
                isr.VI.DeviceTests.DeviceManager.AssertVisaSessionShouldClose(TestInfo, session)
            End Try
        End Using
    End Sub

End Class
