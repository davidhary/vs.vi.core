
''' <summary>
''' Static class for managing the common functions.
''' </summary>
Partial Friend NotInheritable Class DeviceManager

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

#Region " DEVICE OPEN, CLOSE "

    ''' <summary> Opens a session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="device">   The device. </param>
    Public Shared Sub OpenSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal device As VI.VisaSessionBase)
        isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(testInfo, device, K7000ResourceInfo)
    End Sub

    ''' <summary> Closes a session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="testInfo"> Information describing the test. </param>
    ''' <param name="device">   The device. </param>
    Public Shared Sub CloseSession(ByVal testInfo As isr.VI.DeviceTests.TestSite, ByVal device As VI.VisaSessionBase)
        isr.VI.DeviceTests.DeviceManager.AssertDeviceShopuldCloseWithoutErrors(testInfo, device)
    End Sub

#End Region

End Class

