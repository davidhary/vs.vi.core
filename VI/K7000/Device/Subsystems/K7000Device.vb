Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements the Keithley 7000 series switch device. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001. </para>
''' </remarks>
Public Class K7000Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="K7000Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(K7000.StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.Settings_PropertyChanged
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As K7000Device
        Dim device As K7000Device = Nothing
        Try
            device = New K7000Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As K7000Device) As K7000Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception disposing device", $"Exception {ex.ToFullBlownString}")
        Finally

            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " COMMAND SYNTAX "

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(Nothing)
            Me.BindRouteSubsystem(Nothing)
            Me.BindTriggerSubsystem(Nothing)
            Me.BindArmLayer1Subsystem(Nothing)
            Me.BindArmLayer2Subsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            Me.BindRouteSubsystem(New RouteSubsystem(Me.StatusSubsystem))
            Me.BindTriggerSubsystem(New TriggerSubsystem(Me.StatusSubsystem))
            Me.BindArmLayer1Subsystem(New ArmLayer1Subsystem(Me.StatusSubsystem))
            Me.BindArmLayer2Subsystem(New ArmLayer2Subsystem(Me.StatusSubsystem))
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Gets or sets the arm layer 1 subsystem. </summary>
    ''' <value> The arm layer 1 subsystem. </value>
    Public ReadOnly Property ArmLayer1Subsystem As ArmLayer1Subsystem

    ''' <summary> Binds the Arm Layer 1 subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindArmLayer1Subsystem(ByVal subsystem As ArmLayer1Subsystem)
        If Me.ArmLayer1Subsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ArmLayer1Subsystem)
            Me._ArmLayer1Subsystem = Nothing
        End If
        Me._ArmLayer1Subsystem = subsystem
        If Me.ArmLayer1Subsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ArmLayer1Subsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the arm layer 2 subsystem. </summary>
    ''' <value> The arm layer 2 subsystem. </value>
    Public ReadOnly Property ArmLayer2Subsystem As ArmLayer2Subsystem

    ''' <summary> Binds the ArmLayer2 subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindArmLayer2Subsystem(ByVal subsystem As ArmLayer2Subsystem)
        If Me.ArmLayer2Subsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ArmLayer2Subsystem)
            Me._ArmLayer2Subsystem = Nothing
        End If
        Me._ArmLayer2Subsystem = subsystem
        If Me.ArmLayer2Subsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ArmLayer2Subsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Route Subsystem. </summary>
    ''' <value> The Route Subsystem. </value>
    Public ReadOnly Property RouteSubsystem As RouteSubsystem

    ''' <summary> Binds the Route subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindRouteSubsystem(ByVal subsystem As RouteSubsystem)
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.RouteSubsystem)
            Me._RouteSubsystem = Nothing
        End If
        Me._RouteSubsystem = subsystem
        If Me.RouteSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.RouteSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Trigger Subsystem. </summary>
    ''' <value> The Trigger Subsystem. </value>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Binds the Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystem)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.TriggerSubsystem)
            If Me.TriggerSubsystem.Session.IsSessionOpen Then Me.TriggerSubsystem.Abort()
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TriggerSubsystem)
        End If
    End Sub

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#Region " SERVICE REQUEST "

    ''' <summary>
    ''' Enables or disables the operation service status registers requesting a service request upon
    ''' a negative transition.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="isOn"> if set to <c>True</c> [is on]. </param>
    Public Sub ToggleEndOfScanService(ByVal isOn As Boolean)
        Me.ToggleEndOfScanService(isOn, Me.Session.DefaultServiceRequestEnableBitmask And (Not VI.Pith.ServiceRequests.MessageAvailable))
    End Sub

    ''' <summary>
    ''' Enables or disables the operation service status registers requesting a service request upon
    ''' a negative transition.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="turnOn">             True to turn on or false to turn off the service request. </param>
    ''' <param name="serviceRequestMask"> Specifies the
    '''                                   <see cref="VI.Pith.ServiceRequests">service request
    '''                                   flags</see> </param>
    Public Sub ToggleEndOfScanService(ByVal turnOn As Boolean, ByVal serviceRequestMask As VI.Pith.ServiceRequests)
        If turnOn Then
            Me.StatusSubsystem.ApplyOperationNegativeTransitionEventEnableBitmask(OperationTransitionEvents.Settling)
            Me.StatusSubsystem.ApplyOperationPositiveTransitionEventEnableBitmask(OperationTransitionEvents.None)
            Me.StatusSubsystem.ApplyOperationEventEnableBitmask(OperationEvents.Settling)
        Else
            Me.StatusSubsystem.ApplyOperationNegativeTransitionEventEnableBitmask(OperationTransitionEvents.None)
            Me.StatusSubsystem.ApplyOperationPositiveTransitionEventEnableBitmask(OperationTransitionEvents.None)
            Me.StatusSubsystem.ApplyOperationEventEnableBitmask(OperationEvents.None)
        End If
        Me.ToggleServiceRequest(turnOn, serviceRequestMask)
    End Sub

    ''' <summary> Enables or disables service requests. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="turnOn">             True to turn on or false to turn off the service request. </param>
    ''' <param name="serviceRequestMask"> Specifies the
    '''                                   <see cref="VI.Pith.ServiceRequests">service request
    '''                                   flags</see> </param>
    Public Sub ToggleServiceRequest(ByVal turnOn As Boolean, ByVal serviceRequestMask As VI.Pith.ServiceRequests)
        Me.Session.ApplyServiceRequestEnableBitmask(If(turnOn, serviceRequestMask, VI.Pith.ServiceRequests.None))
    End Sub

#End Region

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("K7000 Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.MySettings.ReadDelay)
                Me.Session.ReadDelay = TimeSpan.FromMilliseconds(sender.ReadDelay)
            Case NameOf(My.MySettings.StatusReadDelay)
                Me.Session.StatusReadDelay = TimeSpan.FromMilliseconds(sender.StatusReadDelay)
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} set to {Me.Session.MessageNotificationLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Settings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
