''' <summary> Defines a Trigger Subsystem for a Keithley 7000 instrument. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TriggerSubsystem
    Inherits VI.TriggerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.SupportedTriggerSources = VI.TriggerSources.Bus Or VI.TriggerSources.External Or
                                     VI.TriggerSources.Hold Or VI.TriggerSources.Immediate Or
                                     VI.TriggerSources.Manual Or VI.TriggerSources.Timer Or
                                     VI.TriggerSources.TriggerLink
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " COMMANDS "

    ''' <summary> Gets or sets the Abort command. </summary>
    ''' <value> The Abort command. </value>
    Protected Overrides Property AbortCommand As String = ":ABOR"

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <value> The initiate command. </value>
    Protected Overrides Property InitiateCommand As String = ":INIT"

    ''' <summary> Gets or sets the Immediate command. </summary>
    ''' <value> The Immediate command. </value>
    Protected Overrides Property ImmediateCommand As String = ":TRIG:IMM"

#End Region

#Region " AUTO DELAY "

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledCommandFormat As String = ":TRIG:DEL:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledQueryCommand As String = ":TRIG:DEL:AUTO?"

#End Region

#Region " CONTINUOUS "

    ''' <summary> Gets or sets the Continuous enabled command Format. </summary>
    ''' <value> The Continuous enabled query command. </value>
    Protected Overrides Property ContinuousEnabledCommandFormat As String = ":INIT:CONT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Continuous enabled query command. </summary>
    ''' <value> The Continuous enabled query command. </value>
    Protected Overrides Property ContinuousEnabledQueryCommand As String = ":INIT:CONT?"

#End Region

#Region " TRIGGER COUNT "

    ''' <summary> Gets or sets trigger count query command. </summary>
    ''' <value> The trigger count query command. </value>
    Protected Overrides Property TriggerCountQueryCommand As String = ":TRIG:COUN?"

    ''' <summary> Gets or sets trigger count command format. </summary>
    ''' <value> The trigger count command format. </value>
    Protected Overrides Property TriggerCountCommandFormat As String = ":TRIG:COUN {0}"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = ":TRIG:DEL {0:s\.FFFFFFF}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = ":TRIG:DEL?"

#End Region

#Region " DIRECTION (BYPASS) "

    ''' <summary> Gets or sets the Trigger Direction query command. </summary>
    ''' <value> The Trigger Direction query command. </value>
    Protected Overrides Property TriggerLayerBypassModeQueryCommand As String = ":TRIG:TCON:DIR?"

    ''' <summary> Gets or sets the Trigger Direction command format. </summary>
    ''' <value> The Trigger Direction command format. </value>
    Protected Overrides Property TriggerLayerBypassModeCommandFormat As String = ":TRIG:TCON:DIR {0}"

#End Region

#Region " INPUT LINE NUMBER "

    ''' <summary> Gets or sets the Input Line Number command format. </summary>
    ''' <value> The Input Line Number command format. </value>
    Protected Overrides Property InputLineNumberCommandFormat As String = ":TRIG:TCON:ASYN:ILIN {0}"

    ''' <summary> Gets or sets the Input Line Number query command. </summary>
    ''' <value> The Input Line Number query command. </value>
    Protected Overrides Property InputLineNumberQueryCommand As String = ":TRIG:TCON:ASYN:ILIN?"

#End Region

#Region " OUTPUT LINE NUMBER "

    ''' <summary> Gets or sets the Output Line Number command format. </summary>
    ''' <value> The Output Line Number command format. </value>
    Protected Overrides Property OutputLineNumberCommandFormat As String = ":TRIG:TCON:ASYN:OLIN {0}"

    ''' <summary> Gets or sets the Output Line Number query command. </summary>
    ''' <value> The Output Line Number query command. </value>
    Protected Overrides Property OutputLineNumberQueryCommand As String = ":TRIG:TCON:ASYN:OLIN?"

#End Region

#Region " SOURCE "

    ''' <summary> Gets or sets the Trigger Source query command. </summary>
    ''' <value> The Trigger Source query command. </value>
    Protected Overrides Property TriggerSourceQueryCommand As String = ":TRIG:SOUR?"

    ''' <summary> Gets or sets the Trigger Source command format. </summary>
    ''' <value> The Trigger Source command format. </value>
    Protected Overrides Property TriggerSourceCommandFormat As String = ":TRIG:SOUR {0}"

#End Region

#Region " TIMER TIME SPAN "

    ''' <summary> Gets or sets the Timer Interval command format. </summary>
    ''' <value> The query command format. </value>
    Protected Overrides Property TimerIntervalCommandFormat As String = ":TRIG:TIM {0:s\.FFFFFFF}"

    ''' <summary>
    ''' Gets or sets the Timer Interval format for converting the query to time span.
    ''' </summary>
    ''' <value> The Timer Interval query command. </value>
    Protected Overrides Property TimerIntervalFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the Timer Interval query command. </summary>
    ''' <value> The Timer Interval query command. </value>
    Protected Overrides Property TimerIntervalQueryCommand As String = ":TRIG:TIM?"

#End Region

#End Region

End Class
