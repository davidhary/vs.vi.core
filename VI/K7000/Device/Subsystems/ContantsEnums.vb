#Region " TYPES "

''' <summary> Enumerates the status byte flags of the operation event register. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum OperationEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the calibrating option. </summary>
    <ComponentModel.Description("Calibrating")>
    Calibrating = 1

    ''' <summary> An enum constant representing the settling option. </summary>
    <ComponentModel.Description("Settling")>
    Settling = 2

    ''' <summary> An enum constant representing the sweeping option. </summary>
    <ComponentModel.Description("Sweeping")>
    Sweeping = 8

    ''' <summary> An enum constant representing the waiting for trigger option. </summary>
    <ComponentModel.Description("Waiting For Trigger")>
    WaitingForTrigger = 32

    ''' <summary> An enum constant representing the waiting for arm option. </summary>
    <ComponentModel.Description("Waiting For Arm")>
    WaitingForArm = 64

    ''' <summary> An enum constant representing the idle option. </summary>
    <ComponentModel.Description("Idle")>
    Idle = 1024
End Enum

''' <summary>
''' Enumerates the status byte flags of the operation transition event register.
''' </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum OperationTransitionEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the settling option. </summary>
    <ComponentModel.Description("Settling")>
    Settling = 2

    ''' <summary> An enum constant representing the waiting for trigger option. </summary>
    <ComponentModel.Description("Waiting For Trigger")>
    WaitingForTrigger = 32

    ''' <summary> An enum constant representing the waiting for arm option. </summary>
    <ComponentModel.Description("Waiting For Arm")>
    WaitingForArm = 64

    ''' <summary> An enum constant representing the idle option. </summary>
    <ComponentModel.Description("Idle ")>
    Idle = 1024
End Enum

''' <summary> Enumerates the status byte flags of the questionable event register. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum QuestionableEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the calibration event option. </summary>
    <ComponentModel.Description("Calibration Event")>
    CalibrationEvent = 256

    ''' <summary> An enum constant representing the command warning option. </summary>
    <ComponentModel.Description("Command Warning")>
    CommandWarning = 16384
    'All = 32767
End Enum

#End Region
