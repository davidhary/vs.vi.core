''' <summary> Defines a SCPI Route Subsystem for Switch SCPI devices. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class RouteSubsystem
    Inherits VI.RouteSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="RouteSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " CHANNEL "

    ''' <summary> Gets or sets the closed Channel query command. </summary>
    ''' <value> The closed Channel query command. </value>
    Protected Overrides Property ClosedChannelQueryCommand As String = String.Empty

    ''' <summary> Gets or sets the closed Channel command format. </summary>
    ''' <value> The closed Channel command format. </value>
    Protected Overrides Property ClosedChannelCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the open Channel command format. </summary>
    ''' <value> The open Channel command format. </value>
    Protected Overrides Property OpenChannelCommandFormat As String = String.Empty

#End Region

#Region " CHANNELS "

    ''' <summary> Gets or sets the closed channels state query command. </summary>
    ''' <value> The closed channels state query command. </value>
    Protected Overrides Property ClosedChannelsStateQueryCommand As String = ":ROUT:CLOS? {0}"

    ''' <summary> Gets or sets the closed channels query command. </summary>
    ''' <value> The closed channels query command. </value>
    Protected Overrides Property ClosedChannelsQueryCommand As String = ":ROUT:CLOS:STATE?"

    ''' <summary> Gets or sets the closed channels command format. </summary>
    ''' <value> The closed channels command format. </value>
    Protected Overrides Property ClosedChannelsCommandFormat As String = ":ROUT:CLOS {0}"

    ''' <summary> Gets or sets the open channels command format. </summary>
    ''' <value> The open channels command format. </value>
    Protected Overrides Property OpenChannelsCommandFormat As String = ":ROUT:OPEN {0}"

    ''' <summary> Gets or sets the Open channels query command. </summary>
    ''' <value> The Open channels query command. </value>
    Protected Overrides Property OpenChannelsStateQueryCommand As String = ":ROUT:OPEN? {0}"

    ''' <summary> Gets or sets the Open channels query command. </summary>
    ''' <value> The Open channels query command. </value>
    Protected Overrides Property OpenChannelsQueryCommand As String = String.Empty

#End Region

#Region " CHANNEL PATTERN / MEMORY "

    ''' <summary> Gets or sets the recall channel pattern command format. </summary>
    ''' <value> The recall channel pattern command format. </value>
    Protected Overrides Property RecallChannelPatternCommandFormat As String = ":ROUT:MEM:REC M{0}"

    ''' <summary> Gets or sets the save channel pattern command format. </summary>
    ''' <value> The save channel pattern command format. </value>
    Protected Overrides Property SaveChannelPatternCommandFormat As String = ":ROUT:MEM:SAVE M{0}"

    ''' <summary> Gets or sets the open channels command. </summary>
    ''' <value> The open channels command. </value>
    Protected Overrides Property OpenChannelsCommand As String = ":ROUT:OPEN ALL"

#End Region

#Region " SCAN LIST "

    ''' <summary> Gets or sets the scan list persists after toggling power. </summary>
    ''' <remarks>
    ''' If the scan lists persists after toggling power, the scan list initial values cannot be
    ''' checked.
    ''' </remarks>
    ''' <value> The scan list persists. </value>
    Public Overrides Property ScanListPersists As Boolean = True

    ''' <summary> Gets or sets the scan list command query. </summary>
    ''' <value> The scan list query command. </value>
    Protected Overrides Property ScanListQueryCommand As String = ":ROUT:SCAN?"

    ''' <summary> Gets or sets the scan list command format. </summary>
    ''' <value> The scan list command format. </value>
    Protected Overrides Property ScanListCommandFormat As String = ":ROUT:SCAN {0}"

#End Region

#Region " SLOT CARD TYPE "

    ''' <summary> Gets or sets the slot card type query command format. </summary>
    ''' <value> The slot card type query command format. </value>
    Protected Overrides Property SlotCardTypeQueryCommandFormat As String = ":ROUT:CONF:SLOT{0}:CTYPE?"

    ''' <summary> Gets or sets the slot card type command format. </summary>
    ''' <value> The slot card type command format. </value>
    Protected Overrides Property SlotCardTypeCommandFormat As String = ":ROUT:CONF:SLOT{0}:CTYPE{1}"

#End Region

#Region " SLOT CARD SETTLING TIME "

    ''' <summary> Gets or sets the slot card settling time command format. </summary>
    ''' <value> The slot card settling time command format. </value>
    Protected Overrides Property SlotCardSettlingTimeCommandFormat As String = ":ROUT:CONF:SLOT{0}:STIME {1}"

    ''' <summary> Gets or sets the slot card settling time query command format. </summary>
    ''' <value> The slot card settling time query command format. </value>
    Protected Overrides Property SlotCardSettlingTimeQueryCommandFormat As String = ":ROUT:CONF:SLOT{0}:STIME?"

#End Region

#Region " TERMINAL MODE "

    ''' <summary> Gets the terminals mode query command. </summary>
    ''' <value> The terminals mode command. </value>
    <Obsolete("Not supported for 7000 Series Switches.")>
    Protected Overrides Property TerminalsModeQueryCommand As String = String.Empty

    ''' <summary> Gets the terminals mode command format. </summary>
    ''' <value> The terminals mode command format. </value>
    <Obsolete("Not supported for 7000 Series Switches.")>
    Protected Overrides Property TerminalsModeCommandFormat As String = String.Empty

#End Region

End Class
