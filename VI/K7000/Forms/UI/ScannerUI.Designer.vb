﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ScannerUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._ScannerView = New isr.VI.K7000.Forms.ScannerView()
        Me.SuspendLayout()
        '
        '_ScannerView
        '
        Me._ScannerView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._ScannerView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._ScannerView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ScannerView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ScannerView.Location = New System.Drawing.Point(1, 1)
        Me._ScannerView.Name = "_ScannerView"
        Me._ScannerView.Padding = New System.Windows.Forms.Padding(1)
        Me._ScannerView.Size = New System.Drawing.Size(430, 342)
        Me._ScannerView.TabIndex = 0
        '
        'ScannerUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._ScannerView)
        Me.Name = "ScannerUI"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(432, 344)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _ScannerView As ScannerView
End Class
