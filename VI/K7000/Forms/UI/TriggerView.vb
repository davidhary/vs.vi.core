''' <summary>
''' A Keithley 7000 edition of the basic <see cref="Facade.TriggerView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-01-11  <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class TriggerView
    Inherits isr.VI.Facade.TriggerView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="TriggerView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="TriggerView"/>. </returns>
    Public Overloads Shared Function Create() As TriggerView
        Dim view As TriggerView = Nothing
        Try
            view = New TriggerView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K7000Device As K7000Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K7000Device)
        MyBase.AssignDevice(value)
        Me._K7000Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 1)
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 2)
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase), 1)
        Else
            Me.BindSubsystem(Me.K7000Device.ArmLayer1Subsystem, 1)
            Me.BindSubsystem(Me.K7000Device.ArmLayer2Subsystem, 2)
            Me.BindSubsystem(Me.K7000Device.TriggerSubsystem, 1)
        End If
    End Sub

#End Region

#Region " TRIGGER LAYER 1 VIEW "

    ''' <summary> Handle the TriggerLayer1 view property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="view">         The view. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overrides Sub HandlePropertyChanged(ByVal view As VI.Facade.TriggerLayerView, ByVal propertyName As String)
        If view Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.Facade.TriggerLayerView.Count)
                Settings.Default.TriggerViewTriggerCount = view.Count
        End Select
        MyBase.HandlePropertyChanged(view, propertyName)
    End Sub

#End Region

End Class
