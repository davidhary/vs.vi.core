<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TriggerUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._TriggerView = New isr.VI.K7000.Forms.TriggerView()
        Me.SuspendLayout()
        '
        '_TriggerView
        '
        Me._TriggerView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._TriggerView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me._TriggerView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TriggerView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerView.Location = New System.Drawing.Point(1, 1)
        Me._TriggerView.Name = "_TriggerView"
        Me._TriggerView.Padding = New System.Windows.Forms.Padding(1)
        Me._TriggerView.Size = New System.Drawing.Size(375, 351)
        Me._TriggerView.TabIndex = 24
        '
        'TriggerUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._TriggerView)
        Me.Name = "TriggerUI"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(377, 353)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _TriggerView As isr.VI.K7000.Forms.TriggerView
End Class
