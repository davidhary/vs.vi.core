Imports System.ComponentModel

''' <summary>
''' A Keithley 7000 edition of the basic <see cref="Facade.ScannerView"/> user interface.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ScannerView
    Inherits isr.VI.Facade.ScannerView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="ScannerView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ScannerView"/>. </returns>
    Public Overloads Shared Function Create() As ScannerView
        Dim view As ScannerView = Nothing
        Try
            view = New ScannerView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property K7000Device As K7000Device

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Shadows Sub AssignDevice(ByVal value As K7000Device)
        MyBase.AssignDevice(value)
        Me._K7000Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, RouteSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase))
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem2(CType(Nothing, ArmLayerSubsystemBase))
        Else
            Me.BindSubsystem(Me.K7000Device.RouteSubsystem)
            Me.BindSubsystem(Me.K7000Device.TriggerSubsystem)
            Me.BindSubsystem1(Me.K7000Device.ArmLayer1Subsystem)
            Me.BindSubsystem2(Me.K7000Device.ArmLayer2Subsystem)
        End If
    End Sub

#End Region

End Class
