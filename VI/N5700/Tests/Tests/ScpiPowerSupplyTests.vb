''' <summary>
''' This is a test class for PowerSupplyTest and is intended to contain all PowerSupplyTest Unit
''' Tests.
''' </summary>
''' <remarks> David, 2020-10-12. </remarks>
<TestClass(), TestCategory("n5700")>
Public Class ScpiPowerSupplyTests

    ''' <summary>
    ''' Gets or sets the test context which provides information about and functionality for the
    ''' current test run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    ''' <summary> Select resource name. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="interfaceType"> Type of the interface. </param>
    ''' <returns> A String. </returns>
    Friend Function SelectResourceName(ByVal interfaceType As VI.Pith.HardwareInterfaceType) As String
        Select Case interfaceType
            Case VI.Pith.HardwareInterfaceType.Gpib
                Return "GPIB0::5::INSTR"
            Case VI.Pith.HardwareInterfaceType.Tcpip
                Return "TCPIP0::A-N5767A-K4381"
            Case VI.Pith.HardwareInterfaceType.Usb
                Return "USB0::0x0957::0x0807::N5767A-US11K4381H::0::INSTR"
            Case Else
                Return "GPIB0::5::INSTR"
        End Select
    End Function

    ''' <summary> (Unit Test Method) opens close session should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenCloseSessionShouldPass()
        Using device As New N5700.N5700Device()
            Try
                Me.AssertOpenSession(device, Pith.HardwareInterfaceType.Usb)
            Catch
                Throw
            Finally
                If device.IsDeviceOpen Then device.CloseSession()
            End Try
        End Using
    End Sub

    ''' <summary> Assert open session. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="device">             The device. </param>
    ''' <param name="usingInterfaceType"> Type of the using interface. </param>
    Public Sub AssertOpenSession(device As N5700.N5700Device, usingInterfaceType As VI.Pith.HardwareInterfaceType)
        Dim r As (Success As Boolean, Details As String) = device.TryOpenSession(Me.SelectResourceName(usingInterfaceType), "Power Supply")
        Assert.IsTrue(r.Success, $"session should open; {r.Details}")
        Assert.IsTrue(device.IsDeviceOpen, $"session to device {device.ResourceNameCaption} should be open")
    End Sub

    ''' <summary> (Unit Test Method) output off should toggle. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OutputOffShouldToggle()
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean
        Using device As New N5700.N5700Device()
            Me.AssertOpenSession(device, Pith.HardwareInterfaceType.Gpib)
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState(False).GetValueOrDefault(True)
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output Off;")
            expectedBoolean = False
            actualBoolean = device.OutputSubsystem.QueryOutputOnState.GetValueOrDefault(True)
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output State")

            Dim expectedString As String = "no error"
            If device.StatusSubsystem.Session.IsErrorBitSet Then
                Dim r As (Success As Boolean, Details As String) = device.StatusSubsystem.TryQueryExistingDeviceErrors()
                Assert.IsTrue(r.Success, $"Device error query failed {r.Details}")
            End If
            Dim actualString As String = device.StatusSubsystem.CompoundErrorMessage

            Assert.AreEqual(expectedString, actualString, True, Globalization.CultureInfo.CurrentCulture, "Device error mismatch")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) turn and keep on should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TurnAndKeepOnShouldPass()
        Dim voltage As Double = 28.0!
        Dim currentLimit As Double = 2.0!
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean

        Using device As New N5700.N5700Device()
            Dim e As New isr.Core.ActionEventArgs
            Me.AssertOpenSession(device, Pith.HardwareInterfaceType.Gpib)
            device.ResetClearInit()
            actualBoolean = True
            Assert.AreEqual(expectedBoolean, actualBoolean, "Reset;")
            Dim expectedString As String = "Agilent"
            Dim actualString As String = device.StatusSubsystem.Identity.Substring(0, Len(expectedString))
            Assert.AreEqual(expectedString, actualString)
            actualBoolean = device.OutputOn(voltage, currentLimit, 0.1, voltage + 2, TimeSpan.FromMilliseconds(1000))
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output On;")
            expectedBoolean = True
            actualBoolean = device.OutputSubsystem.QueryOutputOnState.GetValueOrDefault(False)
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output State;")
            Dim expectedDouble As Double = voltage
            Dim actualDouble As Double = device.MeasureVoltageSubsystem.MeasureReadingAmounts.GetValueOrDefault(0)

            Assert.AreEqual(expectedDouble, actualDouble, 0.1)
            expectedDouble = 0.1
            actualDouble = device.MeasureCurrentSubsystem.MeasureReadingAmounts.GetValueOrDefault(0)
            If actualDouble < expectedDouble Then
                Assert.AreEqual(expectedDouble, actualDouble, 0.1, "Actual must be greater")
            End If

            expectedString = "no error"
            If device.StatusSubsystem.Session.IsErrorBitSet Then
                Dim r As (Success As Boolean, Details As String) = device.StatusSubsystemBase.TryQueryExistingDeviceErrors()
                Assert.IsTrue(r.Success, $"Device error query failed: {r.Details}")
            End If
            actualString = device.StatusSubsystem.CompoundErrorMessage
            Assert.AreEqual(expectedString, actualString, True, Globalization.CultureInfo.CurrentCulture, "Device error mismatch")
        End Using
    End Sub

    ''' <summary> (Unit Test Method) output on off should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OutputOnOffShouldPass()
        Dim voltage As Double = 28.0!
        Dim currentLimit As Double = 1.0!
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean

        Using device As New N5700.N5700Device()
            Me.AssertOpenSession(device, Pith.HardwareInterfaceType.Gpib)
            device.ResetClearInit()
            actualBoolean = True
            Assert.AreEqual(expectedBoolean, actualBoolean, "Reset;")
            Dim expectedString As String = "Agilent"
            Dim actualString As String = device.StatusSubsystem.Identity.Substring(0, Len(expectedString))
            Assert.AreEqual(expectedString, actualString)
            actualBoolean = device.OutputOn(voltage, currentLimit, 0.1, voltage + 2, TimeSpan.FromMilliseconds(1000))
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output On;")
            expectedBoolean = True
            actualBoolean = device.OutputSubsystem.QueryOutputOnState.GetValueOrDefault(False)
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output State;")
            Dim expectedDouble As Double = voltage
            Dim actualDouble As Double = device.MeasureVoltageSubsystem.MeasureReadingAmounts.GetValueOrDefault(0)

            Assert.AreEqual(expectedDouble, actualDouble, 0.1)
            expectedDouble = 0.1
            actualDouble = device.MeasureCurrentSubsystem.MeasureReadingAmounts.GetValueOrDefault(0)
            If actualDouble < expectedDouble Then
                Assert.AreEqual(expectedDouble, actualDouble, 0.1, "Actual must be greater")
            End If
            actualBoolean = device.OutputSubsystem.ApplyOutputOnState(False).GetValueOrDefault(True)
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output Off;")
            expectedBoolean = False
            actualBoolean = device.OutputSubsystem.QueryOutputOnState.GetValueOrDefault(True)
            Assert.AreEqual(expectedBoolean, actualBoolean, "Output State;")

            expectedString = "no error"
            If device.Session.IsErrorBitSet Then
                Dim r As (Success As Boolean, Details As String) = device.StatusSubsystem.TryQueryExistingDeviceErrors()
                Assert.IsTrue(r.Success, $"Device error query failed: {r.Details}")
            End If
            actualString = device.StatusSubsystem.CompoundErrorMessage
            Assert.AreEqual(expectedString, actualString, True, Globalization.CultureInfo.CurrentCulture, "Device error mismatch")

        End Using
    End Sub

    ''' <summary> (Unit Test Method) measure current should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MeasureCurrentShouldPass()
        Dim expectedDouble As Double = 0.0!
        Dim actualDouble As Double
        Using device As New N5700.N5700Device()
            Me.AssertOpenSession(device, Pith.HardwareInterfaceType.Gpib)
            actualDouble = device.MeasureCurrentSubsystem.MeasureReadingAmounts.GetValueOrDefault(0)
            Assert.AreEqual(expectedDouble, actualDouble)
        End Using

    End Sub

End Class
