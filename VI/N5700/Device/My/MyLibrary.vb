Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = VI.Pith.My.ProjectTraceEventId.N5700

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "N5700 Power Supply VI Library"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "N5700 Power Supply Virtual Instrument Library"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "VI.N5700.PowerSupply"

    End Class

End Namespace

