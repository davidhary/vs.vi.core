Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements a Power Supply device. </summary>
''' <remarks>
''' An instrument is defined, for the purpose of this library, as a device with a front panel.
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class N5700Device
    Inherits VI.VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="N5700Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As N5700Device
        Dim device As N5700Device = Nothing
        Try
            device = New N5700Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As N5700Device) As N5700Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(N5700Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindMeasureCurrentSubsystem(Nothing)
            Me.BindMeasureVoltageSubsystem(Nothing)
            Me.BindOutputSubsystem(Nothing)
            Me.BindSourceCurrentSubsystem(Nothing)
            Me.BindSourceVoltageSubsystem(Nothing)
            Me.BindSystemSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            Me.BindMeasureCurrentSubsystem(New MeasureCurrentSubsystem(Me.StatusSubsystem))
            Me.BindMeasureVoltageSubsystem(New MeasureVoltageSubsystem(Me.StatusSubsystem))
            Me.BindOutputSubsystem(New OutputSubsystem(Me.StatusSubsystem))
            Me.BindSourceCurrentSubsystem(New SourceCurrentSubsystem(Me.StatusSubsystem))
            Me.BindSourceVoltageSubsystem(New SourceVoltageSubsystem(Me.StatusSubsystem))
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Gets or sets the Measure Current Subsystem. </summary>
    ''' <value> The Measure Current Subsystem. </value>
    Public ReadOnly Property MeasureCurrentSubsystem As MeasureCurrentSubsystem

    ''' <summary> Binds the Measure Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureCurrentSubsystem(ByVal subsystem As MeasureCurrentSubsystem)
        If Me.MeasureCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.MeasureCurrentSubsystem)
            Me._MeasureCurrentSubsystem = Nothing
        End If
        Me._MeasureCurrentSubsystem = subsystem
        If Me.MeasureCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Measure Voltage Subsystem. </summary>
    ''' <value> The Measure Voltage Subsystem. </value>
    Public ReadOnly Property MeasureVoltageSubsystem As MeasureVoltageSubsystem

    ''' <summary> Binds the Measure Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMeasureVoltageSubsystem(ByVal subsystem As MeasureVoltageSubsystem)
        If Me.MeasureVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.MeasureVoltageSubsystem)
            Me._MeasureVoltageSubsystem = Nothing
        End If
        Me._MeasureVoltageSubsystem = subsystem
        If Me.MeasureVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MeasureVoltageSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Output Subsystem. </summary>
    ''' <value> The Output Subsystem. </value>
    Public ReadOnly Property OutputSubsystem As OutputSubsystem

    ''' <summary> Binds the Output subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindOutputSubsystem(ByVal subsystem As OutputSubsystem)
        If Me.OutputSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.OutputSubsystem)
            Me._OutputSubsystem = Nothing
        End If
        Me._OutputSubsystem = subsystem
        If Me.OutputSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.OutputSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Source Current Subsystem. </summary>
    ''' <value> The Source Current Subsystem. </value>
    Public ReadOnly Property SourceCurrentSubsystem As SourceCurrentSubsystem

    ''' <summary> Binds the Source Current subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceCurrentSubsystem(ByVal subsystem As SourceCurrentSubsystem)
        If Me.SourceCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SourceCurrentSubsystem)
            Me._SourceCurrentSubsystem = Nothing
        End If
        Me._SourceCurrentSubsystem = subsystem
        If Me.SourceCurrentSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceCurrentSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Source Voltage Subsystem. </summary>
    ''' <value> The Source Voltage Subsystem. </value>
    Public ReadOnly Property SourceVoltageSubsystem As SourceVoltageSubsystem

    ''' <summary> Binds the Source Voltage subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceVoltageSubsystem(ByVal subsystem As SourceVoltageSubsystem)
        If Me.SourceVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SourceVoltageSubsystem)
            Me._SourceVoltageSubsystem = Nothing
        End If
        Me._SourceVoltageSubsystem = subsystem
        If Me.SourceVoltageSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceVoltageSubsystem)
        End If
    End Sub

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#End Region

#Region " MIXED SUBSYSTEMS: OUTPUT / MEASURE "

    ''' <summary>
    ''' Sets the specified voltage and current limit and turns on the power supply.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="voltage">          The voltage. </param>
    ''' <param name="currentLimit">     The current limit. </param>
    ''' <param name="overvoltageLimit"> The over voltage limit. </param>
    Public Sub OutputOn(ByVal voltage As Double, ByVal currentLimit As Double, ByVal overvoltageLimit As Double)

        ' Set the voltage
        Me.SourceVoltageSubsystem.ApplyLevel(voltage)

        ' Set the over voltage level
        Me.SourceVoltageSubsystem.ApplyProtectionLevel(overvoltageLimit)

        ' Turn on over current protection
        Me.SourceCurrentSubsystem.ApplyProtectionEnabled(True)

        ' Set the current level
        Me.SourceCurrentSubsystem.ApplyLevel(currentLimit)

        ' turn output on.
        Me.OutputSubsystem.ApplyOutputOnState(True)

        Me.Session.QueryOperationCompleted()

        ' operation completion is not sufficient to ensure that the Device has hit the correct voltage.

    End Sub

    ''' <summary>
    ''' Turns the power supply on waiting for the voltage to reach the desired level.
    ''' </summary>
    ''' <remarks> SCPI Command: OUTP ON. </remarks>
    ''' <param name="voltage">      The voltage. </param>
    ''' <param name="currentLimit"> The current limit. </param>
    ''' <param name="delta">        The delta. </param>
    ''' <param name="voltageLimit"> The over voltage limit. </param>
    ''' <param name="timeout">      The timeout for awaiting for the voltage to reach the level. </param>
    ''' <returns>
    ''' <c>True</c> if successful <see cref="OutputOn">Output On</see> and in range;
    '''          <c>False</c> otherwise.
    ''' </returns>
    Public Function OutputOn(ByVal voltage As Double, ByVal currentLimit As Double,
                                 ByVal delta As Double, ByVal voltageLimit As Double, ByVal timeout As TimeSpan) As Boolean
        Me.OutputOn(voltage, currentLimit, voltageLimit)
        Return Me.MeasureVoltageSubsystem.AwaitLevel(voltage, delta, timeout)
    End Function

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("Power Supply Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"Trace log level changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"Trace show level changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.MySettings.ReadDelay)
                Me.Session.ReadDelay = TimeSpan.FromMilliseconds(sender.ReadDelay)
            Case NameOf(My.MySettings.StatusReadDelay)
                Me.Session.StatusReadDelay = TimeSpan.FromMilliseconds(sender.StatusReadDelay)
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
