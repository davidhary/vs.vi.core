''' <summary> Defines a SCPI Source Current Subsystem for power supplies. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class SourceCurrentSubsystem
    Inherits SourceCurrentSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceCurrentSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SOUR:CURR:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SOUR:CURR:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SOUR:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SOUR:FUNC?"

#End Region

#Region " PROTECTION "

    ''' <summary> Gets or sets the Protection enabled command Format. </summary>
    ''' <value> The Protection enabled query command. </value>
    Protected Overrides Property ProtectionEnabledCommandFormat As String = ":SOUR:CURR:PROT:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Protection enabled query command. </summary>
    ''' <value> The Protection enabled query command. </value>
    Protected Overrides Property ProtectionEnabledQueryCommand As String = ":SOUR:CURR:PROT:STAT?"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SOUR:CURR:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SOUR:CURR:RANG?"

#End Region

#End Region

End Class
