''' <summary> Defines a SCPI Source Channel Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class SourceChannelSubsystem
    Inherits VI.SourceChannelSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="channelNumber">   The channel number. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(channelNumber, statusSubsystem)
        Me.SupportedFunctionModes = VI.SourceFunctionModes.Current Or VI.SourceFunctionModes.Voltage
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.FunctionMode = VI.SourceFunctionModes.Voltage
        Me.Level = 0.5 ' volt RMS
        Me.SupportedFunctionModes = VI.SourceFunctionModes.Current Or VI.SourceFunctionModes.Voltage
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " FUNCTION MODE "

    ''' <summary> Gets the function mode query command. </summary>
    ''' <value> The function mode query command, e.g., :SOUR:FUNC? </value>
    Protected Overrides Property FunctionModeQueryCommand As String
        Get
            Return $":SOUR{Me.ChannelNumber}:MODE?"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets the function mode command. </summary>
    ''' <value> The function mode command, e.g., :SOUR:FUNC {0}. </value>
    Protected Overrides Property FunctionModeCommandFormat As String
        Get
            Return $":SOUR{Me.ChannelNumber}:MODE {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#Region " LEVEL "

    ''' <summary> Gets the Level command format. </summary>
    ''' <value> The Level command format. </value>
    Protected Overrides Property LevelCommandFormat As String
        Get
            Return $":SOUR{Me.ChannelNumber}:{Me.FunctionCode}:LEV {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets the Level query command. </summary>
    ''' <value> The Level query command. </value>
    Protected Overrides Property LevelQueryCommand As String
        Get
            Return $":SOUR{Me.ChannelNumber}:{Me.FunctionCode}:LEV?"
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#End Region

End Class
