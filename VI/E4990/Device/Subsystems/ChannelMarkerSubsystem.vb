''' <summary> Defines a SCPI Channel marker Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class ChannelMarkerSubsystem
    Inherits VI.ChannelMarkerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="markerNumber">    The marker number. </param>
    ''' <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal markerNumber As Integer, ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(markerNumber, channelNumber, statusSubsystem, New MarkerReadings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Primary Or ReadingElementTypes.Secondary)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets subsystem values to their known execution clear state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.MarkerReadings.Reset()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.MarkerReadings.Initialize(ReadingElementTypes.Primary Or ReadingElementTypes.Secondary)
        Me.NotifyPropertyChanged(NameOf(E4990.ChannelMarkerSubsystem.MarkerReadings))
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " LATEST DATA "

    ''' <summary> Gets the latest data query command. </summary>
    ''' <remarks> Not exactly the same as reading the latest data. </remarks>
    ''' <value> The latest data query command. </value>
    Protected Overrides Property LatestDataQueryCommand As String = ":CALC{0}:MARK{1}:Y?"

#End Region

#Region " ABSCISSA "

    ''' <summary> Gets the Abscissa command format. </summary>
    ''' <value> The Abscissa command format. </value>
    Protected Overrides Property AbscissaCommandFormat As String = ":CALC{0}:MARK{1}:X {2}"

    ''' <summary> Gets the Abscissa query command. </summary>
    ''' <value> The Abscissa query command. </value>
    Protected Overrides Property AbscissaQueryCommand As String = ":CALC{0}:MARK{1}:X?"

#End Region

#Region " ENABLED "

    ''' <summary> Gets the automatic delay enabled query command. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property EnabledQueryCommand As String = ":CALC{0}:MARK{1}:STAT?"

    ''' <summary> Gets the automatic delay enabled command Format. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property EnabledCommandFormat As String = ":CALC{0}:MARK{1}:STAT {2:1;1;0}"

#End Region

#End Region

#Region " LATEST DATA "

    ''' <summary> The marker readings. </summary>
    Private _MarkerReadings As MarkerReadings

    ''' <summary> Returns the readings. </summary>
    ''' <value> The readings. </value>
    Public Property MarkerReadings() As MarkerReadings
        Get
            Return Me._MarkerReadings
        End Get
        Set(value As MarkerReadings)
            Me._MarkerReadings = value
            MyBase.AssignReadingAmounts(value)
        End Set
    End Property

    ''' <summary> Parses a new set of reading elements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="reading"> Specifies the measurement text to parse into the new reading. </param>
    ''' <returns> A Double? </returns>
    Public Overrides Function ParsePrimaryReading(ByVal reading As String) As Double?
        Return MyBase.ParseReadingAmounts(reading)
    End Function

    ''' <summary> Initializes the marker average. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="triggerSubsystem">          The trigger subsystem. </param>
    ''' <param name="calculateChannelSubsystem"> The calculate channel subsystem. </param>
    Public Sub InitializeMarkerAverage(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal calculateChannelSubsystem As CalculateChannelSubsystemBase)
        If triggerSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(triggerSubsystem))
        If calculateChannelSubsystem Is Nothing Then Throw New ArgumentNullException(NameOf(calculateChannelSubsystem))
        Me.MarkerReadings.Reset()
        triggerSubsystem.ApplyTriggerSource(VI.TriggerSources.Bus)
        calculateChannelSubsystem.ClearAverage()
        triggerSubsystem.ApplyAveragingEnabled(True)
        triggerSubsystem.Immediate()
    End Sub

    ''' <summary> Reads marker average. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="triggerSubsystem">          The trigger subsystem. </param>
    ''' <param name="calculateChannelSubsystem"> The calculate channel subsystem. </param>
    Public Sub ReadMarkerAverage(ByVal triggerSubsystem As TriggerSubsystemBase, ByVal calculateChannelSubsystem As CalculateChannelSubsystemBase)
        Me.InitializeMarkerAverage(triggerSubsystem, calculateChannelSubsystem)
        Me.Session.QueryOperationCompleted.GetValueOrDefault(False)
        Me.FetchLatestData()
    End Sub

#End Region

End Class
