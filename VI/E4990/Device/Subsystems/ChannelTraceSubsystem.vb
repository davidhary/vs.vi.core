''' <summary> Defines a SCPI Channel Trace Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class ChannelTraceSubsystem
    Inherits VI.ChannelTraceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="traceNumber">     The Trace number. </param>
    ''' <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal traceNumber As Integer, ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(traceNumber, channelNumber, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.SupportedParameters = CType(-1 + (VI.TraceParameters.ComplexAdmittance << 1), VI.TraceParameters)
        Me.Parameter = If(Me.TraceNumber = 1, TraceParameters.AbsoluteImpedance, TraceParameters.ImpedancePhase)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AUTO SCALE "

    ''' <summary> Gets the auto scale command. </summary>
    ''' <remarks> SCPI: ":DISP:WIND{0}:TRAC{1}:Y:AUTO". </remarks>
    ''' <value> The auto scale command. </value>
    Protected Overrides Property AutoScaleCommand As String = ":DISP:WIND{0}:TRAC{1}:Y:AUTO"

#End Region

#Region " PARAMETER "

    ''' <summary> Gets the trace parameter command format. </summary>
    ''' <value> The trace parameter command format. </value>
    Protected Overrides Property ParameterCommandFormat As String = ":CALC{0}:PAR{1}:DEF {{0}}"

    ''' <summary> Gets the trace parameter query command. </summary>
    ''' <value> The trace parameter query command. </value>
    Protected Overrides Property ParameterQueryCommand As String = ":CALC{0}:PAR{1}:DEF?"

#End Region

#Region " SELECT "

    ''' <summary> Gets the Select command. </summary>
    ''' <remarks> SCPI: ":CALC{0}:PAR{1}:SEL". </remarks>
    ''' <value> The Select command. </value>
    Protected Overrides Property SelectCommand As String = ":CALC{0}:PAR{1}:SEL"

#End Region

#End Region

End Class
