''' <summary> Defines a SCPI Calculate Channel Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class CalculateChannelSubsystem
    Inherits VI.CalculateChannelSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(channelNumber, statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.TraceCount = 2
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AVERAGE "

    ''' <summary> Gets the clear command. </summary>
    ''' <remarks> SCPI: ":CALC{0}:AVER:CLE". </remarks>
    ''' <value> The clear command. </value>
    Protected Overrides Property AverageClearCommand As String = ":CALC{0}:AVER:CLE"

    ''' <summary> Gets Average Count command format. </summary>
    ''' <remarks> SCPI: ":CALC{0}:AVER:COUN {1}". </remarks>
    ''' <value> The Average Count command format. </value>
    Protected Overrides Property AverageCountCommandFormat As String = ":CALC{0}:AVER:COUN {1}"

    ''' <summary> Gets Average Count query command. </summary>
    ''' <remarks> SCPI: ":CALC{0}:AVER:COUN?". </remarks>
    ''' <value> The Average Count query command. </value>
    Protected Overrides Property AverageCountQueryCommand As String = ":CALC{0}:AVER:COUN?"

#Region " AVERAGING ENABLED "

    ''' <summary> Gets the averaging enabled query command. </summary>
    ''' <value> The averaging enabled query command. </value>
    Protected Overrides Property AveragingEnabledQueryCommand As String = ":CALC{0}:AVER?"

    ''' <summary> Gets the averaging enabled command Format. </summary>
    ''' <value> The averaging enabled query command. </value>
    Protected Overrides Property AveragingEnabledCommandFormat As String = ":CALC{0}:AVER {1:1;1;0}"

#End Region

#End Region

#Region " PARAMETER "

    ''' <summary> Gets or sets the channel Trace Count command format. </summary>
    ''' <value> The Trace Count command format. </value>
    Protected Overrides Property TraceCountCommandFormat As String = ":CALC{0}:PAR:COUN {1}"

    ''' <summary> Gets Trace Count query command. </summary>
    ''' <value> The Trace Count query command. </value>
    Protected Overrides Property TraceCountQueryCommand As String = ":CALC{0}:PAR:COUN?"

#End Region

#End Region

End Class
