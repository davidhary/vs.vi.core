''' <summary> Defines a SCPI Sense Channel Subsystem. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-07-06, 4.0.6031. </para>
''' </remarks>
Public Class SenseChannelSubsystem
    Inherits VI.SenseChannelSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SourceChannelSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="channelNumber">   A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal channelNumber As Integer, ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(channelNumber, statusSubsystem, New MarkerReadings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Primary Or ReadingElementTypes.Secondary)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ohm)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt

        Me.SupportedAdapterTypes = VI.AdapterTypes.E4M1 Or VI.AdapterTypes.E4M2 Or VI.AdapterTypes.None
        Me.SupportedSweepTypes = VI.SweepTypes.Linear Or VI.SweepTypes.Logarithmic
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ApertureRange = New isr.Core.Primitives.RangeR(1, 5)
        Me.AdapterType = VI.AdapterTypes.None
        Me.SweepPoints = 201
        Me.SweepStart = 20
        Me.SweepStop = 10000000.0
        Me.SweepType = VI.SweepTypes.Linear
        Me.FrequencyPointsType = VI.FrequencyPointsTypes.Fixed
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " COMMANDS "

    ''' <summary> Gets the clear compensations command. </summary>
    ''' <remarks> SCPI: ":SENS{0}:CORR2:CLE". </remarks>
    ''' <value> The clear compensations command. </value>
    Protected Overrides Property ClearCompensationsCommand As String
        Get
            Return $":SENS{0}:CORR2:CLE"
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#Region " ADAPTER TYPE "

    ''' <summary> Gets the Adapter Type query command. </summary>
    ''' <value> The Adapter Type query command, e.g., :SENSE:ADAPT? </value>
    Protected Overrides Property AdapterTypeQueryCommand As String = ":SENS:ADAP?"

    ''' <summary> Gets the Adapter Type command. </summary>
    ''' <value> The Adapter Type command, e.g., :SENSE:ADAPT {0}. </value>
    Protected Overrides Property AdapterTypeCommandFormat As String = "SENS:ADAP {0}"

#End Region

#Region " APERTURE "

    ''' <summary> Gets The Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overrides Property ApertureCommandFormat As String
        Get
            Return $":SENS{0}:APER {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets The Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property ApertureQueryCommand As String
        Get
            Return $":SENS{0}:APER?"
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#Region " FREQUENCY POINTS TYPE "

    ''' <summary> Gets Frequency Points Type query command. </summary>
    ''' <value> The Frequency Points Type query command. </value>
    Protected Overrides Property FrequencyPointsTypeQueryCommand As String
        Get
            Return $":SENS{0}:CORR:COLL:FPO?"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Frequency Points Type command format. </summary>
    ''' <value> The Frequency Points Type command format. </value>
    Protected Overrides Property FrequencyPointsTypeCommandFormat As String
        Get
            Return $":SENS{0}:CORR:COLL:FPO {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = $":SENS{0}:FUNC {{0}}"

    ''' <summary> Gets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = $":SENS{0}:FUNC?"

#End Region

#Region " SWEEP "

    ''' <summary> Gets Sweep Points query command. </summary>
    ''' <value> The Sweep Points query command. </value>
    Protected Overrides Property SweepPointsQueryCommand As String
        Get
            Return $":SENS{0}:SWE:POIN?"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Points command format. </summary>
    ''' <value> The Sweep Points command format. </value>
    Protected Overrides Property SweepPointsCommandFormat As String
        Get
            Return $":SENS{0}:SWE:POIN {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Start query command. </summary>
    ''' <value> The Sweep Start query command. </value>
    Protected Overrides Property SweepStartQueryCommand As String
        Get
            Return $":SENS{0}:FREQ:STAR?"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Start command format. </summary>
    ''' <value> The Sweep Start command format. </value>
    Protected Overrides Property SweepStartCommandFormat As String
        Get
            Return $":SENS{0}:FREQ:STAR {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Stop query command. </summary>
    ''' <value> The Sweep Stop query command. </value>
    Protected Overrides Property SweepStopQueryCommand As String
        Get
            Return $":SENS{0}:FREQ:STOP?"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Stop command format. </summary>
    ''' <value> The Sweep Stop command format. </value>
    Protected Overrides Property SweepStopCommandFormat As String
        Get
            Return $":SENS{0}:FREQ:STOP {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Type query command. </summary>
    ''' <value> The Sweep Type query command. </value>
    Protected Overrides Property SweepTypeQueryCommand As String
        Get
            Return $":SENS{0}:SWE:TYPE?"
        End Get
        Set(value As String)
        End Set
    End Property

    ''' <summary> Gets Sweep Type command format. </summary>
    ''' <value> The Sweep Type command format. </value>
    Protected Overrides Property SweepTypeCommandFormat As String
        Get
            Return $":SENS{0}:SWE:TYPE {{0}}"
        End Get
        Set(value As String)
        End Set
    End Property

#End Region

#End Region

End Class
