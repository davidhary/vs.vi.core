Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> Implements a E4990 Impedance Meter device. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001. </para>
''' </remarks>
Public Class E4990Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="E4990Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As E4990Device
        Dim device As E4990Device = Nothing
        Try
            device = New E4990Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As E4990Device) As E4990Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(E4990Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(Nothing)
            Me.BindCalculateChannelSubsystem(Nothing)
            Me.BindCompensateOpenSubsystem(Nothing)
            Me.BindCompensateShortSubsystem(Nothing)
            Me.BindCompensateLoadSubsystem(Nothing)
            Me.BindChannelMarkerSubsystem(Nothing)
            Me.BindPrimaryChannelTraceSubsystem(Nothing)
            Me.BindSecondaryChannelTraceSubsystem(Nothing)
            Me.BindChannelTriggerSubsystem(Nothing)
            Me.BindDisplaySubsystem(Nothing)
            Me.BindSenseChannelSubsystem(Nothing)
            Me.BindSourceChannelSubsystem(Nothing)
            Me.BindTriggerSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            Me.BindCalculateChannelSubsystem(New CalculateChannelSubsystem(1, Me.StatusSubsystem))
            Me.BindCompensateOpenSubsystem(New CompensateChannelSubsystem(VI.CompensationTypes.OpenCircuit, 1, Me.StatusSubsystem))
            Me.BindCompensateShortSubsystem(New CompensateChannelSubsystem(VI.CompensationTypes.ShortCircuit, 1, Me.StatusSubsystem))
            Me.BindCompensateLoadSubsystem(New CompensateChannelSubsystem(VI.CompensationTypes.Load, 1, Me.StatusSubsystem))
            Me.BindChannelMarkerSubsystem(New ChannelMarkerSubsystem(1, 1, Me.StatusSubsystem))
            Me.BindPrimaryChannelTraceSubsystem(New ChannelTraceSubsystem(1, 1, Me.StatusSubsystem))
            Me.BindSecondaryChannelTraceSubsystem(New ChannelTraceSubsystem(2, 1, Me.StatusSubsystem))
            Me.BindChannelTriggerSubsystem(New ChannelTriggerSubsystem(1, Me.StatusSubsystem))
            Me.BindDisplaySubsystem(New DisplaySubsystem(Me.StatusSubsystem))
            Me.BindSenseChannelSubsystem(New SenseChannelSubsystem(1, Me.StatusSubsystem))
            Me.BindSourceChannelSubsystem(New SourceChannelSubsystem(1, Me.StatusSubsystem))
            Me.BindTriggerSubsystem(New TriggerSubsystem(Me.StatusSubsystem))
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

    ''' <summary> Gets or sets the CalculateChannel Subsystem. </summary>
    ''' <value> The CalculateChannel Subsystem. </value>
    Public ReadOnly Property CalculateChannelSubsystem As CalculateChannelSubsystem

    ''' <summary> Binds the CalculateChannel subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindCalculateChannelSubsystem(ByVal subsystem As CalculateChannelSubsystem)
        If Me.CalculateChannelSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.CalculateChannelSubsystem)
            Me._CalculateChannelSubsystem = Nothing
        End If
        Me._CalculateChannelSubsystem = subsystem
        If Me.CalculateChannelSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.CalculateChannelSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the ChannelMarker Subsystem. </summary>
    ''' <value> The ChannelMarker Subsystem. </value>
    Public ReadOnly Property ChannelMarkerSubsystem As ChannelMarkerSubsystem

    ''' <summary> Binds the ChannelMarker subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindChannelMarkerSubsystem(ByVal subsystem As ChannelMarkerSubsystem)
        If Me.ChannelMarkerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ChannelMarkerSubsystem)
            Me._ChannelMarkerSubsystem = Nothing
        End If
        Me._ChannelMarkerSubsystem = subsystem
        If Me.ChannelMarkerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ChannelMarkerSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Primary Channel Trace Subsystem. </summary>
    ''' <value> The Primary Channel Trace Subsystem. </value>
    Public ReadOnly Property PrimaryChannelTraceSubsystem As ChannelTraceSubsystem

    ''' <summary> Binds the Primary Channel Trace subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindPrimaryChannelTraceSubsystem(ByVal subsystem As ChannelTraceSubsystem)
        If Me.PrimaryChannelTraceSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.PrimaryChannelTraceSubsystem)
            Me._PrimaryChannelTraceSubsystem = Nothing
        End If
        Me._PrimaryChannelTraceSubsystem = subsystem
        If Me.PrimaryChannelTraceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.PrimaryChannelTraceSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Secondary Channel Trace Subsystem. </summary>
    ''' <value> The Secondary Channel Trace Subsystem. </value>
    Public ReadOnly Property SecondaryChannelTraceSubsystem As ChannelTraceSubsystem

    ''' <summary> Binds the Secondary Channel Trace subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSecondaryChannelTraceSubsystem(ByVal subsystem As ChannelTraceSubsystem)
        If Me.SecondaryChannelTraceSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SecondaryChannelTraceSubsystem)
            Me._SecondaryChannelTraceSubsystem = Nothing
        End If
        Me._SecondaryChannelTraceSubsystem = subsystem
        If Me.SecondaryChannelTraceSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SecondaryChannelTraceSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the ChannelTrigger Subsystem. </summary>
    ''' <value> The ChannelTrigger Subsystem. </value>
    Public ReadOnly Property ChannelTriggerSubsystem As ChannelTriggerSubsystem

    ''' <summary> Binds the ChannelTrigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindChannelTriggerSubsystem(ByVal subsystem As ChannelTriggerSubsystem)
        If Me.ChannelTriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.ChannelTriggerSubsystem)
            Me._ChannelTriggerSubsystem = Nothing
        End If
        Me._ChannelTriggerSubsystem = subsystem
        If Me.ChannelTriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.ChannelTriggerSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Compensate Open Subsystem. </summary>
    ''' <value> The Compensate Open Subsystem. </value>
    Public ReadOnly Property CompensateOpenSubsystem As CompensateChannelSubsystem

    ''' <summary> Binds the Compensate Open subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindCompensateOpenSubsystem(ByVal subsystem As CompensateChannelSubsystem)
        If Me.CompensateOpenSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.CompensateOpenSubsystem)
            Me._CompensateOpenSubsystem = Nothing
        End If
        Me._CompensateOpenSubsystem = subsystem
        If Me.CompensateOpenSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.CompensateOpenSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Compensate Short Subsystem. </summary>
    ''' <value> The Compensate Short Subsystem. </value>
    Public ReadOnly Property CompensateShortSubsystem As CompensateChannelSubsystem

    ''' <summary> Binds the Compensate Short subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindCompensateShortSubsystem(ByVal subsystem As CompensateChannelSubsystem)
        If Me.CompensateShortSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.CompensateShortSubsystem)
            Me._CompensateShortSubsystem = Nothing
        End If
        Me._CompensateShortSubsystem = subsystem
        If Me.CompensateShortSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.CompensateShortSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Compensate Load Subsystem. </summary>
    ''' <value> The Compensate Load Subsystem. </value>
    Public ReadOnly Property CompensateLoadSubsystem As CompensateChannelSubsystem

    ''' <summary> Binds the Compensate Load subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindCompensateLoadSubsystem(ByVal subsystem As CompensateChannelSubsystem)
        If Me.CompensateLoadSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.CompensateLoadSubsystem)
            Me._CompensateLoadSubsystem = Nothing
        End If
        Me._CompensateLoadSubsystem = subsystem
        If Me.CompensateLoadSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.CompensateLoadSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Display Subsystem. </summary>
    ''' <value> The Display Subsystem. </value>
    Public ReadOnly Property DisplaySubsystem As DisplaySubsystem

    ''' <summary> Binds the Display subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindDisplaySubsystem(ByVal subsystem As DisplaySubsystem)
        If Me.DisplaySubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.DisplaySubsystem)
            Me._DisplaySubsystem = Nothing
        End If
        Me._DisplaySubsystem = subsystem
        If Me.DisplaySubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.DisplaySubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Sense Channel Subsystem. </summary>
    ''' <value> The Sense Channel Subsystem. </value>
    Public ReadOnly Property SenseChannelSubsystem As SenseChannelSubsystem

    ''' <summary> Binds the Sense Channel subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseChannelSubsystem(ByVal subsystem As SenseChannelSubsystem)
        If Me.SenseChannelSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SenseChannelSubsystem)
            Me._SenseChannelSubsystem = Nothing
        End If
        Me._SenseChannelSubsystem = subsystem
        If Me.SenseChannelSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SenseChannelSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Source Channel Subsystem. </summary>
    ''' <value> The Source Channel Subsystem. </value>
    Public ReadOnly Property SourceChannelSubsystem As SourceChannelSubsystem

    ''' <summary> Binds the Source Channel subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSourceChannelSubsystem(ByVal subsystem As SourceChannelSubsystem)
        If Me.SourceChannelSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SourceChannelSubsystem)
            Me._SourceChannelSubsystem = Nothing
        End If
        Me._SourceChannelSubsystem = subsystem
        If Me.SourceChannelSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SourceChannelSubsystem)
        End If
    End Sub

    ''' <summary> Gets or sets the Trigger Subsystem. </summary>
    ''' <value> The Trigger Subsystem. </value>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Binds the Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystem)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TriggerSubsystem)
        End If
    End Sub

#Region " COMPENSATION "

    ''' <summary> Clears the compensations. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ClearCompensations()

        ' clear the calibration parameters
        Me.SenseChannelSubsystem.ClearCompensations()
        Me.Session.QueryOperationCompleted()
        Me.CompensateOpenSubsystem.ClearMeasurements()
        Me.Session.QueryOperationCompleted()
        Me.CompensateShortSubsystem.ClearMeasurements()
        Me.Session.QueryOperationCompleted()
        Me.CompensateLoadSubsystem.ClearMeasurements()
        Me.Session.QueryOperationCompleted()

    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("E4990 Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} changed to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} changed to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.StatusSubsystemBase.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.SessionMessageNotificationLevel}")
            Case NameOf(My.MySettings.ReadDelay)
                Me.Session.ReadDelay = TimeSpan.FromMilliseconds(sender.ReadDelay)
            Case NameOf(My.MySettings.StatusReadDelay)
                Me.Session.StatusReadDelay = TimeSpan.FromMilliseconds(sender.StatusReadDelay)
            Case NameOf(My.MySettings.StatusReadTurnaroundTime)
                Me.Session.StatusReadTurnaroundTime = sender.StatusReadTurnaroundTime
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
