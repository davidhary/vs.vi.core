Imports isr.Core.EscapeSequencesExtensions

''' <summary> Defines a SCPI Status Subsystem for a generic Switch. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-10, 3.0.5001. </para>
''' </remarks>
Partial Public Class StatusSubsystem
    Inherits VI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(VI.Pith.SessionBase.Validated(session))
        Me._VersionInfo = New VersionInfo
        Me.VersionInfoBase = Me._VersionInfo
        StatusSubsystem.InitializeSession(session)
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region


#Region " SESSION "

    ''' <summary> Initializes the session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="Session">message based TSP session</see>. </param>
    Private Shared Sub InitializeSession(ByVal session As VI.Pith.SessionBase)
        session.ClearExecutionStateCommand = VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand
        session.ResetKnownStateCommand = VI.Pith.Ieee488.Syntax.ResetKnownStateCommand
        session.ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        session.StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
        session.OperationCompletedQueryCommand = VI.Pith.Ieee488.Syntax.OperationCompletedQueryCommand
        session.StandardServiceEnableCommandFormat = VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat
        session.StandardServiceEnableCompleteCommandFormat = VI.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat
        session.ServiceRequestEnableCommandFormat = VI.Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat
        session.ServiceRequestEnableQueryCommand = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand
        session.StandardEventStatusQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand
        session.StandardEventEnableQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand
        session.WaitCommand = VI.Pith.Ieee488.Syntax.WaitCommand
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <value> The preset command. </value>
    Protected Overrides Property PresetCommand As String = VI.Pith.Scpi.Syntax.StatusPresetCommand

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = VI.Pith.Scpi.Syntax.ClearErrorQueueCommand

    ''' <summary> Gets or sets the error queue query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = VI.Pith.Scpi.Syntax.NextErrorQueryCommand

#End Region

#Region " MEASUREMENT REGISTER EVENTS "

    ''' <summary> Gets or sets the measurement status query command. </summary>
    ''' <value> The measurement status query command. </value>
    Protected Overrides Property MeasurementStatusQueryCommand As String = VI.Pith.Scpi.Syntax.MeasurementEventQueryCommand

    ''' <summary> Gets or sets the measurement event condition query command. </summary>
    ''' <value> The measurement event condition query command. </value>
    Protected Overrides Property MeasurementEventConditionQueryCommand As String = VI.Pith.Scpi.Syntax.MeasurementEventConditionQueryCommand

#End Region

#Region " OPERATION REGISTER EVENTS "

    ''' <summary> Gets or sets the operation event enable Query command. </summary>
    ''' <value> The operation event enable Query command. </value>
    Protected Overrides Property OperationEventEnableQueryCommand As String = VI.Pith.Scpi.Syntax.OperationEventEnableQueryCommand

    ''' <summary> Gets or sets the operation event enable command format. </summary>
    ''' <value> The operation event enable command format. </value>
    Protected Overrides Property OperationEventEnableCommandFormat As String = VI.Pith.Scpi.Syntax.OperationEventEnableCommandFormat

    ''' <summary> Gets or sets the operation event status query command. </summary>
    ''' <value> The operation event status query command. </value>
    Protected Overrides Property OperationEventStatusQueryCommand As String = VI.Pith.Scpi.Syntax.OperationEventQueryCommand

#End Region

#Region " QUESTIONABLE REGISTER "

    ''' <summary> Gets or sets the questionable status query command. </summary>
    ''' <value> The questionable status query command. </value>
    Protected Overrides Property QuestionableStatusQueryCommand As String = VI.Pith.Scpi.Syntax.QuestionableEventQueryCommand

#End Region

#Region " IDENTITY "

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = VI.Pith.Ieee488.Syntax.IdentityQueryCommand

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
    ''' <returns> System.String. </returns>
    Public Overrides Function QueryIdentity() As String
        If Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.PublishVerbose("Requesting identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Me.WriteIdentityQueryCommand()
            Me.PublishVerbose("Trying to read identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            ' wait for the delay time.
            ' Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
            Dim value As String = Me.Session.ReadLineTrimEnd
            value = value.ReplaceCommonEscapeSequences.Trim
            Me.PublishVerbose($"Setting identity to {value};. ")
            Me.VersionInfo.Parse(value)
            MyBase.VersionInfoBase = Me.VersionInfo
            Me.Identity = Me.VersionInfo.Identity
        End If
        Return Me.Identity
    End Function

    ''' <summary> Gets or sets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public ReadOnly Property VersionInfo As VersionInfo

#End Region

#Region " MEASUREMENET AVAILABLE "

    ''' <summary> Enables the measurement available. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub EnableMeasurementAvailable()

        ' Sets the positive transition filter to 0 and the negative transition filter to 1 
        ' so that the operation status event register at bit 4 is set to 1 only when the
        ' operation status condition register at bit 4 is changed from 1 to 0.
        ' Me.Device.Session.Write(":STAT:OPER:PTR 0")
        Me.ApplyOperationPositiveTransitionEventEnableBitmask(0)
        ' Me.Device.Session.Write(":STAT:OPER:NTR 16")
        Me.ApplyOperationNegativeTransitionEventEnableBitmask(16)

        ' Enables bit 4 in the operation status event register and bit 8 in the status byte register.

        ' Me.Device.Session.Write(":STAT:OPER:ENAB 16")
        Me.ApplyOperationEventEnableBitmask(16)

        ' Me.Device.Session.Write("*SRE 128")
        Me.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.OperationEvent) ' 128

    End Sub

#End Region

End Class
