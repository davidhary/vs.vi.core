Imports System.Windows.Forms
Imports isr.Core.Controls
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.Core.EnumExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Facade.ComboBoxExtensions

''' <summary> Wizard for setting the compensation. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-07-08 </para>
''' </remarks>
Public Class CompensationWizard
    Inherits isr.Core.Forma.ListenerFormBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new instance of the <see cref="CompensationWizard"/> class. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        ' required for designer support
        Me.InitializingComponents = True
        Me.InitializeComponent()
        Me.InitializingComponents = False
        ' Me.ConstructorSafeSetter(New TraceMessageTalker)
        Dim builder As New System.Text.StringBuilder
        builder.AppendLine("This wizard will guide you through the steps of performing a compensation task.")
        builder.AppendLine("The following items are required:")
        builder.AppendLine("-- An Open fixture for open compensation;")
        builder.AppendLine("-- A Short fixture for short compensation;")

        builder.AppendLine("-- A Load Resistance fixture for load compensation; and")
        builder.AppendLine("-- A Yardstick Resistance fixture for validation.")
        Me._WelcomeWizardPage.Description = builder.ToString

        Me._Wizard.HeaderImage = Global.isr.VI.E4990.Forms.My.Resources.HeaderIcon
        Me._Wizard.WelcomeImage = Global.isr.VI.E4990.Forms.My.Resources.WelcomeImage

        Me._LoadResistanceNumeric.Value = isr.VI.E4990.My.MySettings.Default.LoadResistance
        Me._YardstickResistanceNumeric.Value = isr.VI.E4990.My.MySettings.Default.YardstickResistance
        Me._YardstickAcceptanceToleranceNumeric.Value = 100 * isr.VI.E4990.My.MySettings.Default.YardstickResistanceTolerance
        Me._YardstickInductanceLimitNumeric.Value = CDec(1000000.0 * isr.VI.E4990.My.MySettings.Default.YardstickInductanceLimit)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    Me.Device = Nothing
                    If Me.components IsNot Nothing Then Me.components.Dispose()
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE AND SUBSYSTEMS "

    ''' <summary> The device. </summary>
    Private _Device As E4990Device

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    Public Property Device As E4990Device
        Get
            Return Me._Device
        End Get
        Set(value As E4990Device)
            If Me._Device IsNot Nothing Then
                RemoveHandler Me.Device.CalculateChannelSubsystem.PropertyChanged, AddressOf Me.CalculateChannelSubsystemPropertyChanged
                RemoveHandler Me.Device.CompensateOpenSubsystem.PropertyChanged, AddressOf Me.CompensateChannelSubsystemPropertyChanged
                RemoveHandler Me.Device.CompensateShortSubsystem.PropertyChanged, AddressOf Me.CompensateChannelSubsystemPropertyChanged
                RemoveHandler Me.Device.CompensateLoadSubsystem.PropertyChanged, AddressOf Me.CompensateChannelSubsystemPropertyChanged
                RemoveHandler Me.Device.ChannelMarkerSubsystem.PropertyChanged, AddressOf Me.ChannelMarkerSubsystemPropertyChanged
                RemoveHandler Me.Device.PrimaryChannelTraceSubsystem.PropertyChanged, AddressOf Me.ChannelTraceSubsystemPropertyChanged
                RemoveHandler Me.Device.SecondaryChannelTraceSubsystem.PropertyChanged, AddressOf Me.ChannelTraceSubsystemPropertyChanged
                RemoveHandler Me.Device.ChannelTriggerSubsystem.PropertyChanged, AddressOf Me.ChannelTriggerSubsystemPropertyChanged
                RemoveHandler Me.Device.TriggerSubsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
                RemoveHandler Me.Device.DisplaySubsystem.PropertyChanged, AddressOf Me.DisplaySubsystemPropertyChanged
                RemoveHandler Me.Device.SenseChannelSubsystem.PropertyChanged, AddressOf Me.SenseChannelSubsystemPropertyChanged
                RemoveHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                RemoveHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
            End If
            Me._Device = value
            If Me._Device IsNot Nothing Then
                AddHandler Me.Device.StatusSubsystem.PropertyChanged, AddressOf Me.StatusSubsystemPropertyChanged
                AddHandler Me.Device.SystemSubsystem.PropertyChanged, AddressOf Me.SystemSubsystemPropertyChanged
                AddHandler Me.Device.SenseChannelSubsystem.PropertyChanged, AddressOf Me.SenseChannelSubsystemPropertyChanged
                AddHandler Me.Device.TriggerSubsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
                AddHandler Me.Device.DisplaySubsystem.PropertyChanged, AddressOf Me.DisplaySubsystemPropertyChanged
                AddHandler Me.Device.CalculateChannelSubsystem.PropertyChanged, AddressOf Me.CalculateChannelSubsystemPropertyChanged
                AddHandler Me.Device.CompensateOpenSubsystem.PropertyChanged, AddressOf Me.CompensateChannelSubsystemPropertyChanged
                AddHandler Me.Device.CompensateShortSubsystem.PropertyChanged, AddressOf Me.CompensateChannelSubsystemPropertyChanged
                AddHandler Me.Device.CompensateLoadSubsystem.PropertyChanged, AddressOf Me.CompensateChannelSubsystemPropertyChanged
                AddHandler Me.Device.PrimaryChannelTraceSubsystem.PropertyChanged, AddressOf Me.ChannelTraceSubsystemPropertyChanged
                AddHandler Me.Device.SecondaryChannelTraceSubsystem.PropertyChanged, AddressOf Me.ChannelTraceSubsystemPropertyChanged
                AddHandler Me.Device.ChannelTriggerSubsystem.PropertyChanged, AddressOf Me.ChannelTriggerSubsystemPropertyChanged
                AddHandler Me.Device.ChannelMarkerSubsystem.PropertyChanged, AddressOf Me.ChannelMarkerSubsystemPropertyChanged
                Me._AdapterComboBox.ListSupportedAdapters(Me.Device.SenseChannelSubsystem.SupportedAdapterTypes)
                ' TO_DO: Bind subsystem and handle relevant property changes to display current values.
            End If
        End Set
    End Property

#Region " SUBSYSTEMS "

#Region " CALCULATE "

    ''' <summary> Handle the Calculate channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As CalculateChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(E4990.CalculateChannelSubsystem.AveragingEnabled)
                Me._AveragingEnabledCheckBox.Checked = subsystem.AveragingEnabled.GetValueOrDefault(False)
            Case NameOf(E4990.CalculateChannelSubsystem.AverageCount)
                Me._AveragingCountNumeric.Value = subsystem.AverageCount.GetValueOrDefault(0)
        End Select
    End Sub

    ''' <summary> Calculate channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CalculateChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(CalculateChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.CalculateChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, CalculateChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " COMPENSATE "

    ''' <summary> Handle the Compensate channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.CompensateChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(E4990.CompensateChannelSubsystem.HasCompleteCompensationValues)
                Dim value As String = String.Empty
                If subsystem.HasCompleteCompensationValues Then
                    value = CompensateChannelSubsystem.Merge(subsystem.FrequencyArrayReading, subsystem.ImpedanceArrayReading)
                End If
                If subsystem.CompensationType = VI.CompensationTypes.OpenCircuit Then
                    Me._OpenCompensationValuesTextBox.Text = value
                ElseIf subsystem.CompensationType = VI.CompensationTypes.ShortCircuit Then
                    Me._ShortCompensationValuesTextBox.Text = value
                ElseIf subsystem.CompensationType = VI.CompensationTypes.Load Then
                    Me._LoadCompensationTextBox.Text = value
                End If
        End Select

        If subsystem.CompensationType = VI.CompensationTypes.OpenCircuit Then
            Select Case propertyName
                Case NameOf(E4990.CompensateChannelSubsystem.HasCompleteCompensationValues)
            End Select
        ElseIf subsystem.CompensationType = VI.CompensationTypes.ShortCircuit Then
            Select Case propertyName
                Case NameOf(E4990.CompensateChannelSubsystem.FrequencyArrayReading)
            End Select
        ElseIf subsystem.CompensationType = VI.CompensationTypes.Load Then
            Select Case propertyName
                Case NameOf(E4990.CompensateChannelSubsystem.FrequencyArrayReading)
                Case NameOf(E4990.CompensateChannelSubsystem.ModelResistance)
                    If subsystem.ModelResistance.HasValue Then Me._LoadResistanceNumeric.Value = CDec(subsystem.ModelResistance.Value)
            End Select
        End If
    End Sub

    ''' <summary> Compensate channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CompensateChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(CompensateChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.CompensateChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, CompensateChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CHANNEL MARKER "

    ''' <summary> Handle the channel marker subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As ChannelMarkerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(E4990.ChannelMarkerSubsystem.LastReading)
                Me.UpdateYardstickValues()
        End Select
    End Sub

    ''' <summary> Channel marker subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelMarkerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ChannelMarkerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ChannelMarkerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ChannelMarkerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CHANNEL TRACE "

    ''' <summary> Handle the channel Trace subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As ChannelTraceSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            ' Case NameOf(VI.ChannelTraceSubsystemBase.ChannelNumber)
        End Select
    End Sub

    ''' <summary> Channel Trace subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelTraceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ChannelTraceSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ChannelTraceSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ChannelTraceSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CHANNEL TRIGGER "

    ''' <summary> Handle the channel Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As ChannelTriggerSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            'Case NameOf(VI.ChannelTriggerSubsystemBase.ChannelNumber)
        End Select
    End Sub

    ''' <summary> Channel Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelTriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ChannelTriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ChannelTriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ChannelTriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Handle the Display subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As DisplaySubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            ' Case NameOf(VI.DisplaySubsystemBase.Enabled)
        End Select
    End Sub

    ''' <summary> Display subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DisplaySubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(DisplaySubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.DisplaySubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, DisplaySubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SENSE "

    ''' <summary> Selects a new Adapter to display. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The VI.AdapterTypes. </returns>
    Friend Function SelectAdapter(ByVal value As VI.AdapterTypes) As VI.AdapterTypes
        If Me.InitializingComponents OrElse value = VI.AdapterTypes.None Then Return VI.AdapterTypes.None
        If (value <> VI.AdapterTypes.None) AndAlso (value <> Me.SelectedAdapterType) Then
            Me._AdapterComboBox.SelectItem(value.ValueDescriptionPair)
        End If
        Return Me.SelectedAdapterType
    End Function

    ''' <summary> Gets the type of the selected Adapter. </summary>
    ''' <value> The type of the selected Adapter. </value>
    Private ReadOnly Property SelectedAdapterType() As VI.AdapterTypes
        Get
            Return CType(CType(Me._AdapterComboBox.SelectedItem,
                               System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.AdapterTypes)
        End Get
    End Property

    ''' <summary> Handle the sense channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.SenseChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.SenseChannelSubsystemBase.AdapterType)
                If subsystem.AdapterType.HasValue Then Me.SelectAdapter(subsystem.AdapterType.Value)
            Case NameOf(VI.SenseChannelSubsystemBase.Aperture)
                If subsystem.Aperture.HasValue Then Me._ApertureNumeric.Value = CDec(subsystem.Aperture.Value)
        End Select
    End Sub

    ''' <summary> Sense channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.TriggerSubsystemBase.Delay)
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Reports the last error. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="lastError"> The last error. </param>
    Private Sub OnLastError(ByVal lastError As DeviceError)
        Me._StatusLabel.Text = isr.Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact(lastError.CompoundErrorMessage, Me._StatusLabel)
        Me._StatusLabel.ToolTipText = lastError.CompoundErrorMessage
    End Sub

    ''' <summary> Handle the Status subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.StatusSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(StatusSubsystemBase.DeviceErrorReport)
                Me.OnLastError(subsystem.DeviceErrorQueue.LastError)
            Case NameOf(StatusSubsystemBase.DeviceErrorQueue)
                Me.OnLastError(subsystem.DeviceErrorQueue.LastError)
        End Select
    End Sub

    ''' <summary> Status subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StatusSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(StatusSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.StatusSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, StatusSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub ReadStatusRegister()
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} reading service request"
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SYSTEM "

    ''' <summary> Handle the System subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As SystemSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Windows.Forms.Application.DoEvents()
    End Sub

    ''' <summary> System subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SystemSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SystemSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SystemSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SystemSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#End Region

#End Region

#Region " METHODS "

    ''' <summary> Starts a sample task simulation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub StartTask()
        ' setup wizard page
        Me._Wizard.BackEnabled = False
        Me._Wizard.NextEnabled = False
        Me._LongTaskProgressBar.Value = Me._LongTaskProgressBar.Minimum

        ' start timer to simulate a long running task
        Me._LongTaskTimer.Enabled = True
    End Sub

#End Region

#Region " CONTROL EVENTS HANDLERS "

#Region " SETTINGS "

    ''' <summary> Restart averaging button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RestartAveragingButton_Click(sender As Object, e As EventArgs) Handles _RestartAveragingButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} restarting average"
            Me._InfoProvider.Clear()
            Me.Device.CalculateChannelSubsystem.ClearAverage()
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

    ''' <summary> Applies the settings button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySettingsButton_Click(sender As Object, e As EventArgs) Handles _ApplySettingsButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} apply settings"
            Me._InfoProvider.Clear()
            Me.Device.SenseChannelSubsystem.ApplyAdapterType(Me.SelectedAdapterType)
            Me.Device.SenseChannelSubsystem.ApplyAperture(Me._ApertureNumeric.Value)
            Me.Device.CalculateChannelSubsystem.ApplyAverageSettings(Me._AveragingEnabledCheckBox.Checked, CInt(Me._AveragingCountNumeric.Value))
            Me.Device.CalculateChannelSubsystem.ClearAverage()
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

#End Region

#Region " OPEN "

    ''' <summary> Acquires the open compensation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub AcquireOpenCompensation()

        ' Set user-specified frequencies
        Me.Device.SenseChannelSubsystem.ApplyFrequencyPointsType(VI.FrequencyPointsTypes.User)

        ' Acquire open fixture compensation
        Me.Device.CompensateOpenSubsystem.AcquireMeasurements()

        ' Wait for measurement end
        Me.Device.Session.QueryOperationCompleted()

    End Sub

    ''' <summary> Reads open compensation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub ReadOpenCompensation()

        ' read the frequencies
        Me.Device.CompensateOpenSubsystem.QueryFrequencyArray()

        ' read the data
        Me.Device.CompensateOpenSubsystem.QueryImpedanceArray()

    End Sub

    ''' <summary> Acquires the open compensation button context menu strip changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AcquireOpenCompensationButton_ContextMenuStripChanged(sender As Object, e As EventArgs) Handles _AcquireOpenCompensationButton.ContextMenuStripChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} acquiring open compensation"
            Me._InfoProvider.Clear()
            Me.Cursor = Cursors.WaitCursor
            Me.Device.ClearCompensations()
            Me.Device.ChannelMarkerSubsystem.MarkerReadings.Reset()
            Me.AcquireOpenCompensation()
            Me.ReadOpenCompensation()
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub


#End Region

#Region " SHORT "

    ''' <summary> Acquires the Short compensation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub AcquireShortCompensation()

        ' Set user-specified frequencies
        Me.Device.SenseChannelSubsystem.ApplyFrequencyPointsType(VI.FrequencyPointsTypes.User)

        ' Acquire Short fixture compensation
        Me.Device.CompensateShortSubsystem.AcquireMeasurements()

        ' Wait for measurement end
        Me.Device.Session.QueryOperationCompleted()

    End Sub

    ''' <summary> Reads Short compensation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub ReadShortCompensation()

        ' read the frequencies
        Me.Device.CompensateShortSubsystem.QueryFrequencyArray()

        ' read the data
        Me.Device.CompensateShortSubsystem.QueryImpedanceArray()

    End Sub

    ''' <summary> Acquires the Short compensation button context menu strip changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AcquireShortCompensationButton_ContextMenuStripChanged(sender As Object, e As EventArgs) Handles _AcquireShortCompensationButton.ContextMenuStripChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} acquiring short compensation"
            Me._InfoProvider.Clear()
            Me.Cursor = Cursors.WaitCursor
            Me.AcquireShortCompensation()
            Me.ReadShortCompensation()
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " LOAD "

    ''' <summary> Acquires the Load compensation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub AcquireLoadCompensation()

        ' Set user-specified frequencies
        Me.Device.SenseChannelSubsystem.ApplyFrequencyPointsType(VI.FrequencyPointsTypes.User)

        Me.Device.CompensateLoadSubsystem.ApplyModelResistance(Me._LoadResistanceNumeric.Value)
        Me.Device.CompensateLoadSubsystem.ApplyModelCapacitance(0)
        Me.Device.CompensateLoadSubsystem.ApplyModelInductance(0)
        Me.Device.Session.QueryOperationCompleted()

        ' Acquire Load fixture compensation
        Me.Device.CompensateLoadSubsystem.AcquireMeasurements()

        ' Wait for measurement end
        Me.Device.Session.QueryOperationCompleted()

    End Sub

    ''' <summary> Reads Load compensation. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub ReadLoadCompensation()

        ' read the frequencies
        Me.Device.CompensateLoadSubsystem.QueryFrequencyArray()

        ' read the data
        Me.Device.CompensateLoadSubsystem.QueryImpedanceArray()

    End Sub

    ''' <summary> Acquires the Load compensation button context menu strip changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AcquireLoadCompensationButton_ContextMenuStripChanged(sender As Object, e As EventArgs) Handles _AcquireLoadCompensationButton.ContextMenuStripChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} acquiring load compensation"
            Me._InfoProvider.Clear()
            Me.Cursor = Cursors.WaitCursor
            Me.AcquireLoadCompensation()
            Me.ReadLoadCompensation()
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " YARDSTICK "

    ''' <summary> Yardstick acceptance tolerance numeric value changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub YardstickAcceptanceToleranceNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _YardstickAcceptanceToleranceNumeric.ValueChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.AnnunciateYardstickResult()
    End Sub

    ''' <summary> Yardstick inductance limit numeric value changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub YardstickInductanceLimitNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _YardstickInductanceLimitNumeric.ValueChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me.AnnunciateYardstickResult()
    End Sub

    ''' <summary> Gets the yardstick resistance validated. </summary>
    ''' <value> The yardstick resistance validated. </value>
    Private ReadOnly Property YardstickResistanceValidated As Boolean?
        Get
            If Me.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.HasValue Then
                Dim r As Double = Me.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.Value
                Dim delta As Double = 100 * (r / Me._YardstickResistanceNumeric.Value - 1)
                Return Math.Abs(delta) <= Me._YardstickAcceptanceToleranceNumeric.Value
            Else
                Return New Boolean?
            End If
        End Get
    End Property

    ''' <summary> Gets the yardstick impedance validated. </summary>
    ''' <value> The yardstick impedance validated. </value>
    Private ReadOnly Property YardstickInductanceValidated As Boolean?
        Get
            If Me.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.HasValue Then
                Dim l As Double = 1000000.0 * Me.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.Value
                Me._YardstickMeasuredInductanceTextBox.Text = String.Format("{0:0.###}", l)
                Return Math.Abs(l) <= Me._YardstickInductanceLimitNumeric.Value
            Else
                Return New Boolean?
            End If
        End Get
    End Property

    ''' <summary> Gets the yardstick validated. </summary>
    ''' <value> The yardstick validated. </value>
    Private ReadOnly Property YardstickValidated As Boolean
        Get
            Return Me.YardstickResistanceValidated.GetValueOrDefault(False) AndAlso Me.YardstickInductanceValidated.GetValueOrDefault(False)
        End Get
    End Property

    ''' <summary> Updates the yardstick values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub UpdateYardstickValues()

        Me._InfoProvider.Clear()
        Me._YardstickValuesTextBox.Text = Me.Device.ChannelMarkerSubsystem.MarkerReadings.RawReading
        Dim r As Double = Me.Device.ChannelMarkerSubsystem.MarkerReadings.PrimaryReading.Value.GetValueOrDefault(0)
        Me._MeasuredYardstickResistanceTextBox.Text = String.Format("{0:0.###}", r)
        Dim delta As Double = 100 * (r / Me._YardstickResistanceNumeric.Value - 1)
        Me._YardstickResistanceDeviationTextBox.Text = String.Format("{0:0.##}", delta)

        Dim l As Double = 1000000.0 * Me.Device.ChannelMarkerSubsystem.MarkerReadings.SecondaryReading.Value.GetValueOrDefault(0)
        Me._YardstickMeasuredInductanceTextBox.Text = String.Format("{0:0.###}", l)

        Me.AnnunciateYardstickResult()

    End Sub

    ''' <summary> Annunciate yardstick result. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub AnnunciateYardstickResult()
        If Not Me.YardstickResistanceValidated Then
            Me._InfoProvider.Annunciate(Me._YardstickAcceptanceToleranceNumeric, Core.Forma.InfoProviderLevel.Alert, "Exceeds minimum")
        End If
        If Not Me.YardstickInductanceValidated Then
            Me._InfoProvider.Annunciate(Me._YardstickInductanceLimitNumeric, Core.Forma.InfoProviderLevel.Alert, "Exceeds minimum")
        End If
    End Sub

    ''' <summary> Measure yardstick button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureYardstickButton_Click(sender As Object, e As EventArgs) Handles _MeasureYardstickButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} acquiring reading meter; yardstick"
            Me._InfoProvider.Clear()
            Me.Cursor = Cursors.WaitCursor
            Me.Device.ChannelMarkerSubsystem.ReadMarkerAverage(Me.Device.TriggerSubsystem, Me.Device.CalculateChannelSubsystem)
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#End Region

#Region " WIZARD EVENTS HANDLERS "

    ''' <summary> Handles the AfterSwitchPages event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Page changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Wizard_AfterSwitchPages(ByVal sender As Object, ByVal e As PageChangedEventArgs) Handles _Wizard.PageChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} after moving page"
            Me._InfoProvider.Clear()
            Me.Cursor = Cursors.WaitCursor
            Select Case True
                Case Me._Wizard.NewPage Is Me._SettingsWizardPage
                Case Me._Wizard.NewPage Is Me._OpenWizardPage
                Case Me._Wizard.NewPage Is Me._ShortWizardPage
                Case Me._Wizard.NewPage Is Me._LoadWizardPage
                Case Me._Wizard.NewPage Is Me._YardstickWizardPage
                Case Me._Wizard.NewPage Is Me._FinishWizardPage
                Case Me._Wizard.NewPage Is Me._LicenseWizardPage
                    ' check if license page
                    ' sync next button's state with check box
                    Me._Wizard.NextEnabled = Me._AgreeCheckBox.Checked
                Case Me._Wizard.NewPage Is Me._ProgressWizardPage
                    ' check if progress page
                    ' start the sample task
                    Me.StartTask()
            End Select
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Handles the BeforeSwitchPages event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Page changing event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Wizard_BeforeSwitchPages(ByVal sender As Object, ByVal e As PageChangingEventArgs) Handles _Wizard.PageChanging
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        If e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} before moving page"
            Me._InfoProvider.Clear()
            Select Case True
                Case Me._Wizard.OldPage Is Me._SettingsWizardPage AndAlso e.Direction = Core.Controls.Direction.Next
                Case Me._Wizard.OldPage Is Me._OpenWizardPage
                Case Me._Wizard.OldPage Is Me._ShortWizardPage
                Case Me._Wizard.OldPage Is Me._LoadWizardPage
                Case Me._Wizard.OldPage Is Me._YardstickWizardPage
                Case Me._Wizard.OldPage Is Me._LicenseWizardPage
                Case Me._Wizard.OldPage Is Me._ProgressWizardPage
            End Select
            Me.Device.Session.QueryOperationCompleted()
            Me.Device.Session.ReadStatusRegister()

            ' check if we're going forward from options page
            If Me._Wizard.OldPage Is Me._OptionsWizardPage AndAlso e.NewIndex > e.OldIndex Then
                ' check if user selected one option
                If Me._CheckOptionRadioButton.Checked = False AndAlso Me._SkipOptionRadioButton.Checked = False Then
                    ' display hint & cancel step
                    System.Windows.Forms.MessageBox.Show("Please chose one of the options presented.",
                                    Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information,
                                    MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                    e.Cancel = True
                    ' check if user chose to skip validation
                ElseIf Me._SkipOptionRadioButton.Checked Then
                    ' skip the validation page
                    e.NewIndex += 1
                End If
            End If
        Catch ex As Exception
            e.Cancel = True
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

    ''' <summary> Handles the Cancel event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Wizard_Cancel(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _Wizard.Cancel
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} canceling a wizard step"
            Me._InfoProvider.Clear()
            ' check if task is running
            Dim isTaskRunning As Boolean = Me._LongTaskTimer.Enabled
            ' stop the task
            Me._LongTaskTimer.Enabled = False

            ' ask user to confirm
            If System.Windows.Forms.MessageBox.Show($"Are you sure you wand to exit {Me.Text}?", Me.Text,
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1,
                               MessageBoxOptions.DefaultDesktopOnly) <> System.Windows.Forms.DialogResult.Yes Then
                ' cancel closing
                e.Cancel = True
                ' restart the task
                Me._LongTaskTimer.Enabled = isTaskRunning
            Else
                ' ensure parent form is closed (even when ShowDialog is not used)
                Me.DialogResult = DialogResult.Cancel
                Me.Close()
            End If
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

    ''' <summary> Handles the Finish event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Wizard_Finish(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Wizard.Finish
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} closing the wizard"
            Me._InfoProvider.Clear()
            If Me.YardstickValidated Then
                System.Windows.Forms.MessageBox.Show($"The {Me.Text} finished successfully.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information,
                                MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                ' ensure parent form is closed (even when ShowDialog is not used)
                Me.DialogResult = DialogResult.OK
                Me.Close()
            Else
                If System.Windows.Forms.MessageBox.Show($"The {Me.Text} yardstick was not validated or is out of range. Are you sure?", $"{Me.Text} Save Data; Are you sure?",
                                   MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2,
                                   MessageBoxOptions.DefaultDesktopOnly) = DialogResult.Yes Then
                    Me.DialogResult = DialogResult.OK
                    Me.Close()
                End If
            End If
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

    ''' <summary> Handles the Help event of the wizard form. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub Wizard_Help(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Wizard.Help
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} showing help info"
            Me._InfoProvider.Clear()
            Dim text As String = $"This is a really cool wizard control!{Environment.NewLine}:-)"
            Select Case True
                Case Me._Wizard.NewPage Is Me._SettingsWizardPage
                    text = My.Resources.SettingsHelp
                Case Me._Wizard.NewPage Is Me._OpenWizardPage
                    text = My.Resources.OpenHelp
                Case Me._Wizard.NewPage Is Me._ShortWizardPage
                    text = My.Resources.ShortHelp
                Case Me._Wizard.NewPage Is Me._LoadWizardPage
                    text = My.Resources.LoadHelp
                Case Me._Wizard.NewPage Is Me._YardstickWizardPage
                    text = My.Resources.YardstickHelp
                Case Me._Wizard.NewPage Is Me._FinishWizardPage
                    text = My.Resources.FinishHelp
                Case Me._Wizard.NewPage Is Me._LicenseWizardPage
                Case Me._Wizard.NewPage Is Me._ProgressWizardPage
            End Select
            System.Windows.Forms.MessageBox.Show(text, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information,
                            MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        Catch ex As Exception
            Me._InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

#End Region

#Region " SIMULATION EVENT HANDLERS "

    ''' <summary> Handles the CheckedChanged event of checkIAgree. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AgreeCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _AgreeCheckBox.CheckedChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        ' sync next button's state with check box
        Me._Wizard.NextEnabled = Me._AgreeCheckBox.Checked
    End Sub

    ''' <summary> Handles the Tick event of timerTask. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub LongTaskTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _LongTaskTimer.Tick
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        ' check if task completed
        If Me._LongTaskProgressBar.Value = Me._LongTaskProgressBar.Maximum Then
            ' stop the timer & switch to next page
            Me._LongTaskTimer.Enabled = False
            Me._Wizard.Next()
        Else
            ' update progress bar
            Me._LongTaskProgressBar.PerformStep()
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

