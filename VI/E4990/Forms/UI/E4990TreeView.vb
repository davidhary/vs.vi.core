Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> Keysight 4990 Device User Interface. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-30 </para></remarks>
<System.ComponentModel.DisplayName("E4990 User Interface"),
      System.ComponentModel.Description("Keysight 4990 Device User Interface"),
      System.Drawing.ToolboxBitmap(GetType(E4990TreeView))>
Public Class E4990TreeView
    Inherits isr.VI.Facade.VisaTreeView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me._ReadingView = ReadingView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Read", "Read", Me._ReadingView)
        Me._SourceView = SourceView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Source", "Source", Me._SourceView)
        Me._SenseView = SenseView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Sense", "Sense", Me._SenseView)
        Me._TriggerView = TriggerView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Trigger", "Trigger", Me._TriggerView)
        Me._CalibrateView = CalibrateView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Calibrate", "Calibrate", Me._CalibrateView)
        Me._CompensateView = CompensateView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Compensate", "Compensate", Me._CompensateView)
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As E4990Device)
        Me.New()
        Me.AssignDeviceThis(device)
    End Sub

    ''' <summary> Gets source view. </summary>
    ''' <value> The source view. </value>
    Private ReadOnly Property SourceView As SourceView

    ''' <summary> Gets the sense view. </summary>
    ''' <value> The sense view. </value>
    Private ReadOnly Property SenseView As SenseView

    ''' <summary> Gets the reading view. </summary>
    ''' <value> The reading view. </value>
    Private ReadOnly Property ReadingView As ReadingView

    ''' <summary> Gets the calibrate view. </summary>
    ''' <value> The calibrate view. </value>
    Private ReadOnly Property CalibrateView As CalibrateView

    ''' <summary> Gets the compensate view. </summary>
    ''' <value> The compensate view. </value>
    Private ReadOnly Property CompensateView As CompensateView

    ''' <summary> Gets the trigger view. </summary>
    ''' <value> The trigger view. </value>
    Private ReadOnly Property TriggerView As TriggerView

    ''' <summary>
    ''' Releases the unmanaged resources used by the E4990 View and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                If Me.SourceView IsNot Nothing Then Me.SourceView.Dispose() : Me._SourceView = Nothing
                If Me.SenseView IsNot Nothing Then Me.SenseView.Dispose() : Me._SenseView = Nothing
                If Me.TriggerView IsNot Nothing Then Me.TriggerView.Dispose() : Me._TriggerView = Nothing
                If Me.CompensateView IsNot Nothing Then Me.CompensateView.Dispose() : Me._CompensateView = Nothing
                If Me.CalibrateView IsNot Nothing Then Me.CalibrateView.Dispose() : Me._CalibrateView = Nothing
                If Me.ReadingView IsNot Nothing Then Me.ReadingView.Dispose() : Me._ReadingView = Nothing
                Me.AssignDeviceThis(Nothing)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As E4990Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As E4990Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Private Sub AssignDeviceThis(ByVal value As E4990Device)
        If Me._Device IsNot Nothing OrElse MyBase.VisaSessionBase IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = Nothing
            MyBase.StatusView.UserInterfaceSettings = Nothing
            Me._Device = Nothing
        End If
        Me._Device = value
        MyBase.BindVisaSessionBase(value)
        If value IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = isr.VI.E4990.My.MySettings.Default
            MyBase.StatusView.UserInterfaceSettings = Nothing
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Public Overloads Sub AssignDevice(ByVal value As E4990Device)
        Me.AssignDeviceThis(value)
    End Sub

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Executes the device closing action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.OnDeviceClosing(e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            ' release the device before subsystems are disposed
            Me.ReadingView.AssignDevice(Nothing)
            Me.SourceView.AssignDevice(Nothing)
            Me.SenseView.AssignDevice(Nothing)
            Me.TriggerView.AssignDevice(Nothing)
            Me.CalibrateView.AssignDevice(Nothing)
            Me.CompensateView.AssignDevice(Nothing)
        End If
    End Sub

    ''' <summary> Executes the device closed action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Protected Overrides Sub OnDeviceClosed()
        MyBase.OnDeviceClosed()
        ' remove binding after subsystems are disposed
        ' because the device closed the subsystems are null and binding will be removed.
        ' TO_DO:
        ' MyBase.DisplayView.BindSubsystemViewModel(Me.Device.ChannelMarkerSubsystem)
        ' MyBase.DisplayView.BindSubsystemViewModel(Me.Device.SourceChannelSubsystem)
        ' MyBase.StatusView.ReadTerminalsState = nothing
        ' MyBase.DisplayView.BindTerminalsDisplay(Me.Device.SystemSubsystem)
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Protected Overrides Sub OnDeviceOpened()
        MyBase.OnDeviceOpened()
        ' assigning device and subsystems after the subsystems are created
        Me.ReadingView.AssignDevice(Me.Device)
        Me.SourceView.AssignDevice(Me.Device)
        Me.SenseView.AssignDevice(Me.Device)
        Me.TriggerView.AssignDevice(Me.Device)
        Me.CalibrateView.AssignDevice(Me.Device)
        Me.CompensateView.AssignDevice(Me.Device)
        ' TO_DO
        ' MyBase.DisplayView.BindSubsystemViewModel(Me.Device.ChannelMarkerSubsystem)
        ' MyBase.DisplayView.BindSubsystemViewModel(Me.Device.SourceChannelSubsystem)
        ' MyBase.StatusView.ReadTerminalsState = AddressOf Me.Device.SystemSubsystem.QueryFrontTerminalsSelected
        ' MyBase.DisplayView.BindTerminalsDisplay(Me.Device.SystemSubsystem)
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
