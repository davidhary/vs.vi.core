Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Facade.ComboBoxExtensions

''' <summary> A Calibrate view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class CalibrateView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="CalibrateView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="CalibrateView"/>. </returns>
    Public Shared Function Create() As CalibrateView
        Dim view As CalibrateView = Nothing
        Try
            view = New CalibrateView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As E4990Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As E4990Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As E4990Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindSenseChannelSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As E4990Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DATA BINDING "

    ''' <summary> Bind controls settings. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub BindControlsSettings()
        Dim MySettings2 As isr.VI.E4990.My.MySettings = isr.VI.E4990.My.MySettings.Default
        Me._FrequencyStimulusTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings2, "FrequencyArrayReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._FrequencyStimulusTextBox.Text = MySettings2.FrequencyArrayReading
        Me._LoadCompensationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings2, "LoadCompensationReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._LoadCompensationTextBox.Text = MySettings2.LoadCompensationReading
        Me._ShortCompensationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings2, "ShortCompensationReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._ShortCompensationTextBox.Text = MySettings2.ShortCompensationReading
        Me._OpenCompensationTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", MySettings2, "OpenCompensationReading", True, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged))
        Me._OpenCompensationTextBox.Text = MySettings2.OpenCompensationReading
    End Sub

#End Region

#Region " SENSE "

    ''' <summary> Gets the SenseChannel subsystem. </summary>
    ''' <value> The SenseChannel subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseChannelSubsystem As SenseChannelSubsystem

    ''' <summary> Bind SenseChannel subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSenseChannelSubsystem(ByVal device As E4990Device)
        If Me.SenseChannelSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseChannelSubsystem)
            Me._SenseChannelSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SenseChannelSubsystem = device.SenseChannelSubsystem
            Me.BindSubsystem(True, Me.SenseChannelSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseChannelSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseChannelSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.SenseChannelSubsystem.SupportedAdapterTypes))
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.SenseChannelSubsystem.AdapterType))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseChannelSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the sense channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.SenseChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.SenseChannelSubsystemBase.SupportedAdapterTypes)
                Me._AdapterComboBox.ListSupportedAdapters(subsystem.SupportedAdapterTypes)
            Case NameOf(VI.SenseChannelSubsystemBase.AdapterType)
                If subsystem.AdapterType.HasValue Then Me.SelectAdapter(subsystem.AdapterType.Value)
        End Select
    End Sub

    ''' <summary> Sense channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SENSE / CAL "

    ''' <summary> Selects a new Adapter to display. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    ''' <returns> A Scpi.AdapterType. </returns>
    Friend Function SelectAdapter(ByVal value As String) As VI.AdapterTypes
        Dim v As VI.AdapterTypes = VI.AdapterTypes.None
        If Me.InitializingComponents OrElse String.IsNullOrWhiteSpace(value) Then Return v
        If [Enum].GetNames(GetType(VI.AdapterTypes)).Contains(value) Then
            v = CType([Enum].Parse(GetType(VI.AdapterTypes), value), VI.AdapterTypes)
        End If
        Return Me.SelectAdapter(v)
    End Function

    ''' <summary> Selects a new Adapter to display. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    ''' <returns> A Scpi.AdapterType. </returns>
    Friend Function SelectAdapter(ByVal value As VI.AdapterTypes) As VI.AdapterTypes
        If Me.InitializingComponents OrElse value = VI.AdapterTypes.None Then Return VI.AdapterTypes.None
        If (value <> VI.AdapterTypes.None) AndAlso (value <> Me.SelectedAdapterType) Then
            Me._AdapterComboBox.SelectItem(value.ValueDescriptionPair)
        End If
        Return Me.SelectedAdapterType
    End Function

    ''' <summary> Gets the type of the selected Adapter. </summary>
    ''' <value> The type of the selected Adapter. </value>
    Private ReadOnly Property SelectedAdapterType() As VI.AdapterTypes
        Get
            Return CType(CType(Me._AdapterComboBox.SelectedItem,
                               System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.AdapterTypes)
        End Get
    End Property

    ''' <summary> Acquires the compensation button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AcquireCompensationButton_Click(sender As Object, e As EventArgs) Handles _AcquireCompensationButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} acquiring compensation"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Using w As New CompensationWizard
                Dim result As Windows.Forms.DialogResult = w.ShowDialog(Me)
                If result = Windows.Forms.DialogResult.OK Then
                    isr.VI.E4990.My.MySettings.Default.AdapterType = Me.Device.SenseChannelSubsystem.AdapterType.ToString
                    isr.VI.E4990.My.MySettings.Default.FrequencyArrayReading = Me.Device.CompensateOpenSubsystem.FrequencyArrayReading
                    isr.VI.E4990.My.MySettings.Default.OpenCompensationReading = Me.Device.CompensateOpenSubsystem.ImpedanceArrayReading
                    isr.VI.E4990.My.MySettings.Default.ShortCompensationReading = Me.Device.CompensateShortSubsystem.ImpedanceArrayReading
                    isr.VI.E4990.My.MySettings.Default.LoadCompensationReading = Me.Device.CompensateLoadSubsystem.ImpedanceArrayReading
                End If
            End Using
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try


    End Sub

    ''' <summary> Applies the load button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyLoadButton_Click(sender As Object, e As EventArgs) Handles _ApplyLoadButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying load compensation"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.CompensateLoadSubsystem.ApplyImpedanceArray(Me._LoadCompensationTextBox.Text)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try


    End Sub

    ''' <summary> Applies the short button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyShortButton_Click(sender As Object, e As EventArgs) Handles _ApplyShortButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying short compensation"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.CompensateShortSubsystem.ApplyImpedanceArray(Me._ShortCompensationTextBox.Text)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Applies the open button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyOpenButton_Click(sender As Object, e As EventArgs) Handles _ApplyOpenButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying open compensation"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.SenseChannelSubsystem.ApplyAdapterType(Me.SelectedAdapterType)
            Me.Device.CompensateOpenSubsystem.ApplyImpedanceArray(Me._OpenCompensationTextBox.Text)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
