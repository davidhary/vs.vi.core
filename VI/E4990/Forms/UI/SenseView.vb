Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions
Imports isr.VI.Facade.ComboBoxExtensions

''' <summary> A Sense view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SenseView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SenseView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="SenseView"/>. </returns>
    Public Shared Function Create() As SenseView
        Dim view As SenseView = Nothing
        Try
            view = New SenseView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As E4990Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As E4990Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As E4990Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindCalculateChannelSubsystem(value)
        Me.BindChannelMarkerSubsystem(value)
        Me.BindChannelTraceSubsystem(value.PrimaryChannelTraceSubsystem)
        Me.BindSenseChannelSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As E4990Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CALCULATE "

    ''' <summary> Gets the CalculateChannel subsystem. </summary>
    ''' <value> The CalculateChannel subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property CalculateChannelSubsystem As CalculateChannelSubsystem

    ''' <summary> Bind CalculateChannel subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindCalculateChannelSubsystem(ByVal device As E4990Device)
        If Me.CalculateChannelSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.CalculateChannelSubsystem)
            Me._CalculateChannelSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._CalculateChannelSubsystem = device.CalculateChannelSubsystem
            Me.BindSubsystem(True, Me.CalculateChannelSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As CalculateChannelSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.CalculateChannelSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.CalculateChannelSubsystem.AveragingEnabled))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.CalculateChannelSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Calculate channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As CalculateChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.CalculateChannelSubsystemBase.AveragingEnabled)
                Me._AveragingEnabledCheckBox.Checked = subsystem.AveragingEnabled.GetValueOrDefault(False)
            Case NameOf(VI.CalculateChannelSubsystemBase.AverageCount)
                Me._AveragingCountNumeric.Value = subsystem.AverageCount.GetValueOrDefault(0)
            Case NameOf(VI.CalculateChannelSubsystemBase.TraceCount)
                If subsystem.TraceCount.HasValue Then Me._TraceGroupBox.Text = $"Traces ({subsystem.TraceCount.Value})"
        End Select
    End Sub

    ''' <summary> Calculate channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CalculateChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(E4990.CalculateChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.CalculateChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, E4990.CalculateChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CHANNEL MARKER "

    ''' <summary> Gets the ChannelMarker subsystem. </summary>
    ''' <value> The ChannelMarker subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ChannelMarkerSubsystem As ChannelMarkerSubsystem

    ''' <summary> Bind ChannelMarker subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindChannelMarkerSubsystem(ByVal device As E4990Device)
        If Me.ChannelMarkerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ChannelMarkerSubsystem)
            Me._ChannelMarkerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._ChannelMarkerSubsystem = device.ChannelMarkerSubsystem
            Me.BindSubsystem(True, Me.ChannelMarkerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ChannelMarkerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ChannelmarkerSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.ChannelMarkerSubsystem.Abscissa))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ChannelmarkerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the channel marker subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As ChannelMarkerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(E4990.ChannelMarkerSubsystem.Abscissa)
                Me._MarkerFrequencyComboBox.Text = subsystem.Abscissa.ToString
        End Select
    End Sub

    ''' <summary> Channel marker subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelmarkerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(E4990.ChannelMarkerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ChannelmarkerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, E4990.ChannelMarkerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try


    End Sub

#End Region

#Region " CHANNEL TRACE "

    ''' <summary> Gets the ChannelTrace subsystem. </summary>
    ''' <value> The ChannelTrace subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ChannelTraceSubsystem As ChannelTraceSubsystem

    ''' <summary> Bind ChannelTrace subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindChannelTraceSubsystem(ByVal subsystem As ChannelTraceSubsystem)
        If Me.ChannelTraceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ChannelTraceSubsystem)
            Me._ChannelTraceSubsystem = Nothing
        End If
        If Me.Device IsNot Nothing Then
            Me._ChannelTraceSubsystem = subsystem
            Me.BindSubsystem(True, Me.ChannelTraceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ChannelTraceSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ChannelTraceSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.ChannelTraceSubsystem.ChannelNumber))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ChannelTraceSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the channel Trace subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.ChannelTraceSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Dim combo As ComboBox = If(subsystem.TraceNumber = 1, Me._PrimaryTraceParameterComboBox, Me._SecondaryTraceParameterComboBox)
        Select Case propertyName
            Case NameOf(VI.ChannelTraceSubsystemBase.SupportedParameters)
                combo.ListSupportedTraceParameters(subsystem.SupportedParameters)
            Case NameOf(VI.ChannelTraceSubsystemBase.Parameter)
                combo.SafeSelectTraceParameters(subsystem.Parameter)
            Case NameOf(VI.ChannelTraceSubsystemBase.TraceNumber)
        End Select
    End Sub

    ''' <summary> Channel Trace subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelTraceSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ChannelTraceSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.ChannelTraceSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ChannelTraceSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " SENSE "

    ''' <summary> Gets the SenseChannel subsystem. </summary>
    ''' <value> The SenseChannel subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseChannelSubsystem As SenseChannelSubsystem

    ''' <summary> Bind SenseChannel subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSenseChannelSubsystem(ByVal device As E4990Device)
        If Me.SenseChannelSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseChannelSubsystem)
            Me._SenseChannelSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SenseChannelSubsystem = device.SenseChannelSubsystem
            Me.BindSubsystem(True, Me.SenseChannelSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseChannelSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseChannelSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.SenseChannelSubsystem.Aperture))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseChannelSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the sense channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.SenseChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.SenseChannelSubsystemBase.Aperture)
                If subsystem.Aperture.HasValue Then Me._ApertureNumeric.Value = CDec(subsystem.Aperture.Value)
            Case NameOf(VI.SenseChannelSubsystemBase.SweepPoints)
                If subsystem.SweepPoints.HasValue Then Me._SweepGroupBox.Text = $"Sweep points: {subsystem.SweepPoints.Value}"
            Case NameOf(VI.SenseChannelSubsystemBase.SweepStart)
                If subsystem.SweepStart.HasValue Then Me._LowFrequencyNumeric.Value = CDec(subsystem.SweepStart.Value)
            Case NameOf(VI.SenseChannelSubsystemBase.SweepStart)
                If subsystem.SweepStart.HasValue Then Me._HighFrequencyNumeric.Value = CDec(subsystem.SweepStop.Value)
        End Select
    End Sub

    ''' <summary> Sense channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SENSE "

#Region " SENSE / AVERAGING "

    ''' <summary> Applies the averaging button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyAveragingButton_Click(sender As Object, e As EventArgs) Handles _ApplyAveragingButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying average settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.SenseChannelSubsystem.ApplyAperture(Me._ApertureNumeric.Value)
            Me.Device.CalculateChannelSubsystem.ApplyAverageSettings(Me._AveragingEnabledCheckBox.Checked, CInt(Me._AveragingCountNumeric.Value))
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Restart averaging button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RestartAveragingButton_Click(sender As Object, e As EventArgs) Handles _RestartAveragingButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} restarting average"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.CalculateChannelSubsystem.ClearAverage()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " SENSE / SWEEP "

    ''' <summary> Configure sweep. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="lowFrequency">  The low frequency. </param>
    ''' <param name="highFrequency"> The high frequency. </param>
    Public Sub ConfigureSweep(ByVal lowFrequency As Double, ByVal highFrequency As Double)

        ' Set number of points
        'Me.Device.Session.Write(":SENS1:SWE:POIN 2")
        Me.Device.SenseChannelSubsystem.ApplySweepPoints(2)

        ' Set start frequency
        'Me.Device.Session.WriteLine(":SENS1:FREQ:STAR {0}", lowFrequency)
        Me.Device.SenseChannelSubsystem.ApplySweepStart(lowFrequency)

        ' Set stop frequency
        'Me.Device.Session.WriteLine(":SENS1:FREQ:STOP {0}", highFrequency)
        Me.Device.SenseChannelSubsystem.ApplySweepStop(highFrequency)

    End Sub

    ''' <summary> Applies the sweep settings button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySweepSettingsButton_Click(sender As Object, e As EventArgs) Handles _ApplySweepSettingsButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying sweep settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me._MarkerFrequencyComboBox.Items.Clear()
            Me._MarkerFrequencyComboBox.Items.Add(Me._LowFrequencyNumeric.Value.ToString)
            Me._MarkerFrequencyComboBox.Items.Add(Me._HighFrequencyNumeric.Value.ToString)
            Me._MarkerFrequencyComboBox.SelectedIndex = 1
            ' set a two point sweep.
            Me.ConfigureSweep(Me._LowFrequencyNumeric.Value, Me._HighFrequencyNumeric.Value)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SENSE / TRACES "

    ''' <summary> Configure sweep. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub ConfigureTrace()

        ' Setup Channel 1
        Me.Device.CalculateChannelSubsystem.ApplyTraceCount(2)

        ' Allocate measurement parameter for trace 1: Rs
        ' Me.Device.Session.Write(":CALC1:PAR1:DEF RS")
        Me.Device.PrimaryChannelTraceSubsystem.ApplyParameter(Me._PrimaryTraceParameterComboBox.SelectedTraceParameters())
        Me.Device.PrimaryChannelTraceSubsystem.AutoScale()

        ' Allocate measurement parameter for trace 2: Ls
        'Me.Device.Session.Write(":CALC1:PAR2:DEF LS")
        Me.Device.SecondaryChannelTraceSubsystem.ApplyParameter(Me._SecondaryTraceParameterComboBox.SelectedTraceParameters())
        Me.Device.SecondaryChannelTraceSubsystem.AutoScale()

    End Sub

    ''' <summary> Select primary trace parameter. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The VI.TraceParameters. </returns>
    Private Function SelectPrimaryTraceParameter(ByVal value As VI.TraceParameters) As VI.TraceParameters
        If Me.InitializingComponents OrElse value = VI.TraceParameters.None Then Return VI.TraceParameters.None
        If (value <> VI.TraceParameters.None) AndAlso (value <> Me.SelectedPrimaryTraceParameter) Then
            Me._PrimaryTraceParameterComboBox.SafeSelectTraceParameters(value)
        End If
        Return Me.SelectedPrimaryTraceParameter
    End Function

    ''' <summary> Gets the selected primary trace parameter. </summary>
    ''' <value> The selected primary trace parameter. </value>
    Private ReadOnly Property SelectedPrimaryTraceParameter() As VI.TraceParameters
        Get
            Return CType(CType(Me._PrimaryTraceParameterComboBox.SelectedItem,
                System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.TraceParameters)
        End Get
    End Property

    ''' <summary> Select secondary trace parameter. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The VI.TraceParameters. </returns>
    Private Function SelectSecondaryTraceParameter(ByVal value As VI.TraceParameters) As VI.TraceParameters
        If Me.InitializingComponents OrElse value = VI.TraceParameters.None Then Return VI.TraceParameters.None
        If (value <> VI.TraceParameters.None) AndAlso (value <> Me.SelectedSecondaryTraceParameter) Then
            Me._SecondaryTraceParameterComboBox.SafeSelectTraceParameters(value)
        End If
        Return Me.SelectedSecondaryTraceParameter
    End Function

    ''' <summary> Gets the selected secondary trace parameter. </summary>
    ''' <value> The selected secondary trace parameter. </value>
    Private ReadOnly Property SelectedSecondaryTraceParameter() As VI.TraceParameters
        Get
            Return CType(CType(Me._SecondaryTraceParameterComboBox.SelectedItem,
                System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.TraceParameters)
        End Get
    End Property

    ''' <summary> Applies the traces button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyTracesButton_Click(sender As Object, e As EventArgs) Handles _ApplyTracesButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying trace settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            ' set a two point sweep.
            Me.ConfigureTrace()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " SENSE / MARKERS "

    ''' <summary> Applies the marker settings button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyMarkerSettingsButton_Click(sender As Object, e As EventArgs) Handles _ApplyMarkerSettingsButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying marker settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Dim f As Double = Double.Parse(Me._MarkerFrequencyComboBox.Text)
            ' to_do: use sense trace function to set the reading units.
            ' Turn on marker 1
            ' Me.Device.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC{0}:MARK{1} ON", channelNumber, markerNumber))
            Me.Device.ChannelMarkerSubsystem.ApplyEnabled(True)
            ' set marker position
            ' Me.Device.Session.Write(String.Format(Globalization.CultureInfo.InvariantCulture, ":CALC{0}:MARK{1}:X {2}", channelNumber, markerNumber, frequency))
            Me.Device.ChannelMarkerSubsystem.ApplyAbscissa(f)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try


    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
