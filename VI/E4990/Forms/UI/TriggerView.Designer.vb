<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TriggerView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(TriggerView))
        Me._TriggerToolStrip = New System.Windows.Forms.ToolStrip()
        Me._TriggerToolStripLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TriggerSourceComboLabel = New System.Windows.Forms.ToolStripLabel()
        Me._TriggerSourceCombo = New isr.Core.Controls.ToolStripComboBox()
        Me._ContinuousTriggerCheckBox = New isr.Core.Controls.ToolStripCheckBox()
        Me._ApplyTriggerModeButton = New System.Windows.Forms.ToolStripButton()
        Me._TriggerToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_TriggerToolStrip
        '
        Me._TriggerToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._TriggerToolStripLabel, Me._TriggerSourceComboLabel, Me._TriggerSourceCombo, Me._ContinuousTriggerCheckBox, Me._ApplyTriggerModeButton})
        Me._TriggerToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._TriggerToolStrip.Name = "_TriggerToolStrip"
        Me._TriggerToolStrip.Size = New System.Drawing.Size(379, 25)
        Me._TriggerToolStrip.TabIndex = 0
        Me._TriggerToolStrip.Text = "ToolStrip1"
        '
        '_TriggerToolStripLabel
        '
        Me._TriggerToolStripLabel.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerToolStripLabel.Name = "_TriggerToolStripLabel"
        Me._TriggerToolStripLabel.Size = New System.Drawing.Size(50, 22)
        Me._TriggerToolStripLabel.Text = "Trigger:"
        '
        '_TriggerSourceComboLabel
        '
        Me._TriggerSourceComboLabel.Name = "_TriggerSourceComboLabel"
        Me._TriggerSourceComboLabel.Size = New System.Drawing.Size(46, 22)
        Me._TriggerSourceComboLabel.Text = "Source:"
        '
        '_TriggerSourceCombo
        '
        Me._TriggerSourceCombo.Name = "_TriggerSourceCombo"
        Me._TriggerSourceCombo.Size = New System.Drawing.Size(100, 25)
        Me._TriggerSourceCombo.ToolTipText = "Trigger source"
        '
        '_ContinuousTriggerCheckBox
        '
        Me._ContinuousTriggerCheckBox.Checked = False
        Me._ContinuousTriggerCheckBox.Name = "_ContinuousTriggerCheckBox"
        Me._ContinuousTriggerCheckBox.Size = New System.Drawing.Size(88, 22)
        Me._ContinuousTriggerCheckBox.Text = "Continuous"
        Me._ContinuousTriggerCheckBox.ToolTipText = "Checked if continuous trigger"
        '
        '_ApplyTriggerModeButton
        '
        Me._ApplyTriggerModeButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._ApplyTriggerModeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ApplyTriggerModeButton.Image = CType(resources.GetObject("_ApplyTriggerModeButton.Image"), System.Drawing.Image)
        Me._ApplyTriggerModeButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ApplyTriggerModeButton.Name = "_ApplyTriggerModeButton"
        Me._ApplyTriggerModeButton.Size = New System.Drawing.Size(42, 22)
        Me._ApplyTriggerModeButton.Text = "Apply"
        Me._ApplyTriggerModeButton.ToolTipText = "Apply trigger settings"
        '
        'TriggerView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._TriggerToolStrip)
        Me.Name = "TriggerView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._TriggerToolStrip.ResumeLayout(False)
        Me._TriggerToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _TriggerToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _TriggerToolStripLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _TriggerSourceComboLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _TriggerSourceCombo As Core.Controls.ToolStripComboBox
    Private WithEvents _ContinuousTriggerCheckBox As Core.Controls.ToolStripCheckBox
    Private WithEvents _ApplyTriggerModeButton As Windows.Forms.ToolStripButton
End Class
