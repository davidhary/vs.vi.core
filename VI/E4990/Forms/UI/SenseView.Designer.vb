﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SenseView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._SenseTabControl = New System.Windows.Forms.TabControl()
        Me._AverageTabPage = New System.Windows.Forms.TabPage()
        Me._AveragingLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._AveragingGroupBox = New System.Windows.Forms.GroupBox()
        Me._ApertureNumeric = New System.Windows.Forms.NumericUpDown()
        Me._ApplyAveragingButton = New System.Windows.Forms.Button()
        Me._AveragingEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._ApertureNumericLabel = New System.Windows.Forms.Label()
        Me._RestartAveragingButton = New System.Windows.Forms.Button()
        Me._AveragingCountNumericLabel = New System.Windows.Forms.Label()
        Me._AveragingCountNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SweepTabPage = New System.Windows.Forms.TabPage()
        Me._SweepLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._SweepGroupBox = New System.Windows.Forms.GroupBox()
        Me._LowFrequencyNumericLabel = New System.Windows.Forms.Label()
        Me._ApplySweepSettingsButton = New System.Windows.Forms.Button()
        Me._HighFrequencyNumeric = New System.Windows.Forms.NumericUpDown()
        Me._LowFrequencyNumeric = New System.Windows.Forms.NumericUpDown()
        Me._HighFrequencyNumericLabel = New System.Windows.Forms.Label()
        Me._TraceTabPage = New System.Windows.Forms.TabPage()
        Me._TraceLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._TraceGroupBox = New System.Windows.Forms.GroupBox()
        Me._ApplyTracesButton = New System.Windows.Forms.Button()
        Me._PrimaryTraceParameterComboBox = New System.Windows.Forms.ComboBox()
        Me._PrimaryTraceParameterComboBoxLabel = New System.Windows.Forms.Label()
        Me._SecondaryTraceParameterComboBoxLabel = New System.Windows.Forms.Label()
        Me._SecondaryTraceParameterComboBox = New System.Windows.Forms.ComboBox()
        Me._MarkersTabPage = New System.Windows.Forms.TabPage()
        Me._MarkersLayout = New System.Windows.Forms.TableLayoutPanel()
        Me._MarkersGroupBox = New System.Windows.Forms.GroupBox()
        Me._MarkerFrequencyComboBox = New System.Windows.Forms.ComboBox()
        Me._MarkerFrequencyComboBoxLabel = New System.Windows.Forms.Label()
        Me._ApplyMarkerSettingsButton = New System.Windows.Forms.Button()
        Me._SenseTabControl.SuspendLayout()
        Me._AverageTabPage.SuspendLayout()
        Me._AveragingLayout.SuspendLayout()
        Me._AveragingGroupBox.SuspendLayout()
        CType(Me._ApertureNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._AveragingCountNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._SweepTabPage.SuspendLayout()
        Me._SweepLayout.SuspendLayout()
        Me._SweepGroupBox.SuspendLayout()
        CType(Me._HighFrequencyNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._LowFrequencyNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._TraceTabPage.SuspendLayout()
        Me._TraceLayout.SuspendLayout()
        Me._TraceGroupBox.SuspendLayout()
        Me._MarkersTabPage.SuspendLayout()
        Me._MarkersLayout.SuspendLayout()
        Me._MarkersGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SenseTabControl
        '
        Me._SenseTabControl.Controls.Add(Me._AverageTabPage)
        Me._SenseTabControl.Controls.Add(Me._SweepTabPage)
        Me._SenseTabControl.Controls.Add(Me._TraceTabPage)
        Me._SenseTabControl.Controls.Add(Me._MarkersTabPage)
        Me._SenseTabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SenseTabControl.Location = New System.Drawing.Point(1, 1)
        Me._SenseTabControl.Name = "_SenseTabControl"
        Me._SenseTabControl.SelectedIndex = 0
        Me._SenseTabControl.Size = New System.Drawing.Size(379, 322)
        Me._SenseTabControl.TabIndex = 18
        '
        '_AverageTabPage
        '
        Me._AverageTabPage.Controls.Add(Me._AveragingLayout)
        Me._AverageTabPage.Location = New System.Drawing.Point(4, 26)
        Me._AverageTabPage.Name = "_AverageTabPage"
        Me._AverageTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._AverageTabPage.Size = New System.Drawing.Size(371, 292)
        Me._AverageTabPage.TabIndex = 0
        Me._AverageTabPage.Text = "Average"
        Me._AverageTabPage.UseVisualStyleBackColor = True
        '
        '_AveragingLayout
        '
        Me._AveragingLayout.ColumnCount = 3
        Me._AveragingLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._AveragingLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._AveragingLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._AveragingLayout.Controls.Add(Me._AveragingGroupBox, 1, 1)
        Me._AveragingLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._AveragingLayout.Location = New System.Drawing.Point(3, 3)
        Me._AveragingLayout.Name = "_AveragingLayout"
        Me._AveragingLayout.RowCount = 3
        Me._AveragingLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._AveragingLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._AveragingLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._AveragingLayout.Size = New System.Drawing.Size(365, 286)
        Me._AveragingLayout.TabIndex = 2
        '
        '_AveragingGroupBox
        '
        Me._AveragingGroupBox.Controls.Add(Me._ApertureNumeric)
        Me._AveragingGroupBox.Controls.Add(Me._ApplyAveragingButton)
        Me._AveragingGroupBox.Controls.Add(Me._AveragingEnabledCheckBox)
        Me._AveragingGroupBox.Controls.Add(Me._ApertureNumericLabel)
        Me._AveragingGroupBox.Controls.Add(Me._RestartAveragingButton)
        Me._AveragingGroupBox.Controls.Add(Me._AveragingCountNumericLabel)
        Me._AveragingGroupBox.Controls.Add(Me._AveragingCountNumeric)
        Me._AveragingGroupBox.Location = New System.Drawing.Point(97, 62)
        Me._AveragingGroupBox.Name = "_AveragingGroupBox"
        Me._AveragingGroupBox.Size = New System.Drawing.Size(171, 161)
        Me._AveragingGroupBox.TabIndex = 1
        Me._AveragingGroupBox.TabStop = False
        Me._AveragingGroupBox.Text = "Averaging"
        '
        '_ApertureNumeric
        '
        Me._ApertureNumeric.Location = New System.Drawing.Point(73, 30)
        Me._ApertureNumeric.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me._ApertureNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._ApertureNumeric.Name = "_ApertureNumeric"
        Me._ApertureNumeric.Size = New System.Drawing.Size(41, 25)
        Me._ApertureNumeric.TabIndex = 1
        Me._ApertureNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_ApplyAveragingButton
        '
        Me._ApplyAveragingButton.Location = New System.Drawing.Point(35, 123)
        Me._ApplyAveragingButton.Name = "_ApplyAveragingButton"
        Me._ApplyAveragingButton.Size = New System.Drawing.Size(101, 28)
        Me._ApplyAveragingButton.TabIndex = 2
        Me._ApplyAveragingButton.Text = "Apply"
        Me._ApplyAveragingButton.UseVisualStyleBackColor = True
        '
        '_AveragingEnabledCheckBox
        '
        Me._AveragingEnabledCheckBox.AutoSize = True
        Me._AveragingEnabledCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._AveragingEnabledCheckBox.Location = New System.Drawing.Point(10, 94)
        Me._AveragingEnabledCheckBox.Name = "_AveragingEnabledCheckBox"
        Me._AveragingEnabledCheckBox.Size = New System.Drawing.Size(77, 21)
        Me._AveragingEnabledCheckBox.TabIndex = 4
        Me._AveragingEnabledCheckBox.Text = "Enabled:"
        Me._AveragingEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_ApertureNumericLabel
        '
        Me._ApertureNumericLabel.AutoSize = True
        Me._ApertureNumericLabel.Location = New System.Drawing.Point(9, 34)
        Me._ApertureNumericLabel.Name = "_ApertureNumericLabel"
        Me._ApertureNumericLabel.Size = New System.Drawing.Size(62, 17)
        Me._ApertureNumericLabel.TabIndex = 0
        Me._ApertureNumericLabel.Text = "Aperture:"
        '
        '_RestartAveragingButton
        '
        Me._RestartAveragingButton.Location = New System.Drawing.Point(94, 89)
        Me._RestartAveragingButton.Name = "_RestartAveragingButton"
        Me._RestartAveragingButton.Size = New System.Drawing.Size(63, 28)
        Me._RestartAveragingButton.TabIndex = 5
        Me._RestartAveragingButton.Text = "Restart"
        Me._RestartAveragingButton.UseVisualStyleBackColor = True
        '
        '_AveragingCountNumericLabel
        '
        Me._AveragingCountNumericLabel.AutoSize = True
        Me._AveragingCountNumericLabel.Location = New System.Drawing.Point(26, 67)
        Me._AveragingCountNumericLabel.Name = "_AveragingCountNumericLabel"
        Me._AveragingCountNumericLabel.Size = New System.Drawing.Size(45, 17)
        Me._AveragingCountNumericLabel.TabIndex = 2
        Me._AveragingCountNumericLabel.Text = "Count:"
        '
        '_AveragingCountNumeric
        '
        Me._AveragingCountNumeric.Location = New System.Drawing.Point(73, 63)
        Me._AveragingCountNumeric.Maximum = New Decimal(New Integer() {999, 0, 0, 0})
        Me._AveragingCountNumeric.Name = "_AveragingCountNumeric"
        Me._AveragingCountNumeric.Size = New System.Drawing.Size(50, 25)
        Me._AveragingCountNumeric.TabIndex = 3
        Me._AveragingCountNumeric.Value = New Decimal(New Integer() {999, 0, 0, 0})
        '
        '_SweepTabPage
        '
        Me._SweepTabPage.Controls.Add(Me._SweepLayout)
        Me._SweepTabPage.Location = New System.Drawing.Point(4, 26)
        Me._SweepTabPage.Name = "_SweepTabPage"
        Me._SweepTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._SweepTabPage.Size = New System.Drawing.Size(375, 296)
        Me._SweepTabPage.TabIndex = 1
        Me._SweepTabPage.Text = "Sweep"
        Me._SweepTabPage.UseVisualStyleBackColor = True
        '
        '_SweepLayout
        '
        Me._SweepLayout.ColumnCount = 3
        Me._SweepLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SweepLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._SweepLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SweepLayout.Controls.Add(Me._SweepGroupBox, 1, 1)
        Me._SweepLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._SweepLayout.Location = New System.Drawing.Point(3, 3)
        Me._SweepLayout.Name = "_SweepLayout"
        Me._SweepLayout.RowCount = 3
        Me._SweepLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SweepLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._SweepLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._SweepLayout.Size = New System.Drawing.Size(369, 290)
        Me._SweepLayout.TabIndex = 18
        '
        '_SweepGroupBox
        '
        Me._SweepGroupBox.Controls.Add(Me._LowFrequencyNumericLabel)
        Me._SweepGroupBox.Controls.Add(Me._ApplySweepSettingsButton)
        Me._SweepGroupBox.Controls.Add(Me._HighFrequencyNumeric)
        Me._SweepGroupBox.Controls.Add(Me._LowFrequencyNumeric)
        Me._SweepGroupBox.Controls.Add(Me._HighFrequencyNumericLabel)
        Me._SweepGroupBox.Location = New System.Drawing.Point(58, 76)
        Me._SweepGroupBox.Name = "_SweepGroupBox"
        Me._SweepGroupBox.Size = New System.Drawing.Size(253, 138)
        Me._SweepGroupBox.TabIndex = 17
        Me._SweepGroupBox.TabStop = False
        Me._SweepGroupBox.Text = "Sweep"
        '
        '_LowFrequencyNumericLabel
        '
        Me._LowFrequencyNumericLabel.AutoSize = True
        Me._LowFrequencyNumericLabel.Location = New System.Drawing.Point(26, 34)
        Me._LowFrequencyNumericLabel.Name = "_LowFrequencyNumericLabel"
        Me._LowFrequencyNumericLabel.Size = New System.Drawing.Size(124, 17)
        Me._LowFrequencyNumericLabel.TabIndex = 12
        Me._LowFrequencyNumericLabel.Text = "Low Frequency [Hz]:"
        '
        '_ApplySweepSettingsButton
        '
        Me._ApplySweepSettingsButton.Location = New System.Drawing.Point(163, 93)
        Me._ApplySweepSettingsButton.Name = "_ApplySweepSettingsButton"
        Me._ApplySweepSettingsButton.Size = New System.Drawing.Size(75, 28)
        Me._ApplySweepSettingsButton.TabIndex = 16
        Me._ApplySweepSettingsButton.Text = "Apply"
        Me._ApplySweepSettingsButton.UseVisualStyleBackColor = True
        '
        '_HighFrequencyNumeric
        '
        Me._HighFrequencyNumeric.Increment = New Decimal(New Integer() {100000, 0, 0, 0})
        Me._HighFrequencyNumeric.Location = New System.Drawing.Point(153, 61)
        Me._HighFrequencyNumeric.Maximum = New Decimal(New Integer() {10000000, 0, 0, 0})
        Me._HighFrequencyNumeric.Minimum = New Decimal(New Integer() {20, 0, 0, 0})
        Me._HighFrequencyNumeric.Name = "_HighFrequencyNumeric"
        Me._HighFrequencyNumeric.Size = New System.Drawing.Size(85, 25)
        Me._HighFrequencyNumeric.TabIndex = 15
        Me._HighFrequencyNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._HighFrequencyNumeric.ThousandsSeparator = True
        Me._HighFrequencyNumeric.Value = New Decimal(New Integer() {10000000, 0, 0, 0})
        '
        '_LowFrequencyNumeric
        '
        Me._LowFrequencyNumeric.Increment = New Decimal(New Integer() {100000, 0, 0, 0})
        Me._LowFrequencyNumeric.Location = New System.Drawing.Point(153, 30)
        Me._LowFrequencyNumeric.Maximum = New Decimal(New Integer() {10000000, 0, 0, 0})
        Me._LowFrequencyNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._LowFrequencyNumeric.Name = "_LowFrequencyNumeric"
        Me._LowFrequencyNumeric.Size = New System.Drawing.Size(85, 25)
        Me._LowFrequencyNumeric.TabIndex = 13
        Me._LowFrequencyNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._LowFrequencyNumeric.ThousandsSeparator = True
        Me._LowFrequencyNumeric.Value = New Decimal(New Integer() {1000000, 0, 0, 0})
        '
        '_HighFrequencyNumericLabel
        '
        Me._HighFrequencyNumericLabel.AutoSize = True
        Me._HighFrequencyNumericLabel.Location = New System.Drawing.Point(22, 65)
        Me._HighFrequencyNumericLabel.Name = "_HighFrequencyNumericLabel"
        Me._HighFrequencyNumericLabel.Size = New System.Drawing.Size(128, 17)
        Me._HighFrequencyNumericLabel.TabIndex = 14
        Me._HighFrequencyNumericLabel.Text = "High Frequency [Hz]:"
        '
        '_TraceTabPage
        '
        Me._TraceTabPage.Controls.Add(Me._TraceLayout)
        Me._TraceTabPage.Location = New System.Drawing.Point(4, 26)
        Me._TraceTabPage.Name = "_TraceTabPage"
        Me._TraceTabPage.Size = New System.Drawing.Size(375, 296)
        Me._TraceTabPage.TabIndex = 3
        Me._TraceTabPage.Text = "Trace"
        Me._TraceTabPage.UseVisualStyleBackColor = True
        '
        '_TraceLayout
        '
        Me._TraceLayout.ColumnCount = 3
        Me._TraceLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TraceLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._TraceLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TraceLayout.Controls.Add(Me._TraceGroupBox, 1, 1)
        Me._TraceLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TraceLayout.Location = New System.Drawing.Point(0, 0)
        Me._TraceLayout.Name = "_TraceLayout"
        Me._TraceLayout.RowCount = 3
        Me._TraceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TraceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._TraceLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._TraceLayout.Size = New System.Drawing.Size(375, 296)
        Me._TraceLayout.TabIndex = 0
        '
        '_TraceGroupBox
        '
        Me._TraceGroupBox.Controls.Add(Me._ApplyTracesButton)
        Me._TraceGroupBox.Controls.Add(Me._PrimaryTraceParameterComboBox)
        Me._TraceGroupBox.Controls.Add(Me._PrimaryTraceParameterComboBoxLabel)
        Me._TraceGroupBox.Controls.Add(Me._SecondaryTraceParameterComboBoxLabel)
        Me._TraceGroupBox.Controls.Add(Me._SecondaryTraceParameterComboBox)
        Me._TraceGroupBox.Location = New System.Drawing.Point(39, 76)
        Me._TraceGroupBox.Name = "_TraceGroupBox"
        Me._TraceGroupBox.Size = New System.Drawing.Size(297, 143)
        Me._TraceGroupBox.TabIndex = 1
        Me._TraceGroupBox.TabStop = False
        Me._TraceGroupBox.Text = "Traces"
        '
        '_ApplyTracesButton
        '
        Me._ApplyTracesButton.Location = New System.Drawing.Point(212, 104)
        Me._ApplyTracesButton.Name = "_ApplyTracesButton"
        Me._ApplyTracesButton.Size = New System.Drawing.Size(75, 28)
        Me._ApplyTracesButton.TabIndex = 6
        Me._ApplyTracesButton.Text = "Apply"
        Me._ApplyTracesButton.UseVisualStyleBackColor = True
        '
        '_PrimaryTraceParameterComboBox
        '
        Me._PrimaryTraceParameterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._PrimaryTraceParameterComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._PrimaryTraceParameterComboBox.Items.AddRange(New Object() {"I", "V"})
        Me._PrimaryTraceParameterComboBox.Location = New System.Drawing.Point(81, 31)
        Me._PrimaryTraceParameterComboBox.Name = "_PrimaryTraceParameterComboBox"
        Me._PrimaryTraceParameterComboBox.Size = New System.Drawing.Size(206, 25)
        Me._PrimaryTraceParameterComboBox.TabIndex = 4
        '
        '_PrimaryTraceParameterComboBoxLabel
        '
        Me._PrimaryTraceParameterComboBoxLabel.AutoSize = True
        Me._PrimaryTraceParameterComboBoxLabel.Location = New System.Drawing.Point(23, 35)
        Me._PrimaryTraceParameterComboBoxLabel.Name = "_PrimaryTraceParameterComboBoxLabel"
        Me._PrimaryTraceParameterComboBoxLabel.Size = New System.Drawing.Size(55, 17)
        Me._PrimaryTraceParameterComboBoxLabel.TabIndex = 2
        Me._PrimaryTraceParameterComboBoxLabel.Text = "Primary:"
        Me._PrimaryTraceParameterComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SecondaryTraceParameterComboBoxLabel
        '
        Me._SecondaryTraceParameterComboBoxLabel.AutoSize = True
        Me._SecondaryTraceParameterComboBoxLabel.Location = New System.Drawing.Point(6, 69)
        Me._SecondaryTraceParameterComboBoxLabel.Name = "_SecondaryTraceParameterComboBoxLabel"
        Me._SecondaryTraceParameterComboBoxLabel.Size = New System.Drawing.Size(72, 17)
        Me._SecondaryTraceParameterComboBoxLabel.TabIndex = 3
        Me._SecondaryTraceParameterComboBoxLabel.Text = "Secondary:"
        Me._SecondaryTraceParameterComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SecondaryTraceParameterComboBox
        '
        Me._SecondaryTraceParameterComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SecondaryTraceParameterComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._SecondaryTraceParameterComboBox.Items.AddRange(New Object() {"I", "V"})
        Me._SecondaryTraceParameterComboBox.Location = New System.Drawing.Point(81, 65)
        Me._SecondaryTraceParameterComboBox.Name = "_SecondaryTraceParameterComboBox"
        Me._SecondaryTraceParameterComboBox.Size = New System.Drawing.Size(206, 25)
        Me._SecondaryTraceParameterComboBox.TabIndex = 5
        '
        '_MarkersTabPage
        '
        Me._MarkersTabPage.Controls.Add(Me._MarkersLayout)
        Me._MarkersTabPage.Location = New System.Drawing.Point(4, 26)
        Me._MarkersTabPage.Name = "_MarkersTabPage"
        Me._MarkersTabPage.Size = New System.Drawing.Size(375, 296)
        Me._MarkersTabPage.TabIndex = 2
        Me._MarkersTabPage.Text = "Markers"
        Me._MarkersTabPage.UseVisualStyleBackColor = True
        '
        '_MarkersLayout
        '
        Me._MarkersLayout.ColumnCount = 3
        Me._MarkersLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MarkersLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._MarkersLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MarkersLayout.Controls.Add(Me._MarkersGroupBox, 1, 1)
        Me._MarkersLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._MarkersLayout.Location = New System.Drawing.Point(0, 0)
        Me._MarkersLayout.Name = "_MarkersLayout"
        Me._MarkersLayout.RowCount = 3
        Me._MarkersLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MarkersLayout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._MarkersLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._MarkersLayout.Size = New System.Drawing.Size(375, 296)
        Me._MarkersLayout.TabIndex = 3
        '
        '_MarkersGroupBox
        '
        Me._MarkersGroupBox.Controls.Add(Me._MarkerFrequencyComboBox)
        Me._MarkersGroupBox.Controls.Add(Me._MarkerFrequencyComboBoxLabel)
        Me._MarkersGroupBox.Controls.Add(Me._ApplyMarkerSettingsButton)
        Me._MarkersGroupBox.Location = New System.Drawing.Point(48, 96)
        Me._MarkersGroupBox.Name = "_MarkersGroupBox"
        Me._MarkersGroupBox.Size = New System.Drawing.Size(279, 103)
        Me._MarkersGroupBox.TabIndex = 4
        Me._MarkersGroupBox.TabStop = False
        Me._MarkersGroupBox.Text = "Markers"
        '
        '_MarkerFrequencyComboBox
        '
        Me._MarkerFrequencyComboBox.FormattingEnabled = True
        Me._MarkerFrequencyComboBox.Location = New System.Drawing.Point(83, 23)
        Me._MarkerFrequencyComboBox.Name = "_MarkerFrequencyComboBox"
        Me._MarkerFrequencyComboBox.Size = New System.Drawing.Size(186, 25)
        Me._MarkerFrequencyComboBox.TabIndex = 6
        '
        '_MarkerFrequencyComboBoxLabel
        '
        Me._MarkerFrequencyComboBoxLabel.AutoSize = True
        Me._MarkerFrequencyComboBoxLabel.Location = New System.Drawing.Point(10, 27)
        Me._MarkerFrequencyComboBoxLabel.Name = "_MarkerFrequencyComboBoxLabel"
        Me._MarkerFrequencyComboBoxLabel.Size = New System.Drawing.Size(70, 17)
        Me._MarkerFrequencyComboBoxLabel.TabIndex = 5
        Me._MarkerFrequencyComboBoxLabel.Text = "Frequency:"
        '
        '_ApplyMarkerSettingsButton
        '
        Me._ApplyMarkerSettingsButton.Location = New System.Drawing.Point(194, 57)
        Me._ApplyMarkerSettingsButton.Name = "_ApplyMarkerSettingsButton"
        Me._ApplyMarkerSettingsButton.Size = New System.Drawing.Size(75, 28)
        Me._ApplyMarkerSettingsButton.TabIndex = 2
        Me._ApplyMarkerSettingsButton.Text = "Apply"
        Me._ApplyMarkerSettingsButton.UseVisualStyleBackColor = True
        '
        'SenseView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._SenseTabControl)
        Me.Name = "SenseView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        Me._SenseTabControl.ResumeLayout(False)
        Me._AverageTabPage.ResumeLayout(False)
        Me._AveragingLayout.ResumeLayout(False)
        Me._AveragingGroupBox.ResumeLayout(False)
        Me._AveragingGroupBox.PerformLayout()
        CType(Me._ApertureNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._AveragingCountNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._SweepTabPage.ResumeLayout(False)
        Me._SweepLayout.ResumeLayout(False)
        Me._SweepGroupBox.ResumeLayout(False)
        Me._SweepGroupBox.PerformLayout()
        CType(Me._HighFrequencyNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._LowFrequencyNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me._TraceTabPage.ResumeLayout(False)
        Me._TraceLayout.ResumeLayout(False)
        Me._TraceGroupBox.ResumeLayout(False)
        Me._TraceGroupBox.PerformLayout()
        Me._MarkersTabPage.ResumeLayout(False)
        Me._MarkersLayout.ResumeLayout(False)
        Me._MarkersGroupBox.ResumeLayout(False)
        Me._MarkersGroupBox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _SenseTabControl As Windows.Forms.TabControl
    Private WithEvents _AverageTabPage As Windows.Forms.TabPage
    Private WithEvents _AveragingLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _AveragingGroupBox As Windows.Forms.GroupBox
    Private WithEvents _ApertureNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _ApplyAveragingButton As Windows.Forms.Button
    Private WithEvents _AveragingEnabledCheckBox As Windows.Forms.CheckBox
    Private WithEvents _ApertureNumericLabel As Windows.Forms.Label
    Private WithEvents _RestartAveragingButton As Windows.Forms.Button
    Private WithEvents _AveragingCountNumericLabel As Windows.Forms.Label
    Private WithEvents _AveragingCountNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SweepTabPage As Windows.Forms.TabPage
    Private WithEvents _SweepLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _SweepGroupBox As Windows.Forms.GroupBox
    Private WithEvents _LowFrequencyNumericLabel As Windows.Forms.Label
    Private WithEvents _ApplySweepSettingsButton As Windows.Forms.Button
    Private WithEvents _HighFrequencyNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _LowFrequencyNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _HighFrequencyNumericLabel As Windows.Forms.Label
    Private WithEvents _TraceTabPage As Windows.Forms.TabPage
    Private WithEvents _TraceLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _TraceGroupBox As Windows.Forms.GroupBox
    Private WithEvents _ApplyTracesButton As Windows.Forms.Button
    Private WithEvents _PrimaryTraceParameterComboBox As Windows.Forms.ComboBox
    Private WithEvents _PrimaryTraceParameterComboBoxLabel As Windows.Forms.Label
    Private WithEvents _SecondaryTraceParameterComboBoxLabel As Windows.Forms.Label
    Private WithEvents _SecondaryTraceParameterComboBox As Windows.Forms.ComboBox
    Private WithEvents _MarkersTabPage As Windows.Forms.TabPage
    Private WithEvents _MarkersLayout As Windows.Forms.TableLayoutPanel
    Private WithEvents _MarkersGroupBox As Windows.Forms.GroupBox
    Private WithEvents _MarkerFrequencyComboBox As Windows.Forms.ComboBox
    Private WithEvents _MarkerFrequencyComboBoxLabel As Windows.Forms.Label
    Private WithEvents _ApplyMarkerSettingsButton As Windows.Forms.Button
End Class
