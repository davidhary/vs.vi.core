Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.Core.WinForms.ErrorProviderExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A Reading view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ReadingView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._TraceDropDownButton.Visible = False
        Me._ReadingsCountLabel.Visible = False
        Me._ClearBufferDisplayButton.Visible = False

        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ReadingView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As ReadingView
        Dim view As ReadingView = Nothing
        Try
            view = New ReadingView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As E4990Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As E4990Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As E4990Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindCalculateChannelSubsystem(value)
        Me.BindChannelMarkerSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As E4990Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CALCULATE "

    ''' <summary> Gets the CalculateChannel subsystem. </summary>
    ''' <value> The CalculateChannel subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property CalculateChannelSubsystem As CalculateChannelSubsystem

    ''' <summary> Bind CalculateChannel subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindCalculateChannelSubsystem(ByVal device As E4990Device)
        If Me.CalculateChannelSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.CalculateChannelSubsystem)
            Me._CalculateChannelSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._CalculateChannelSubsystem = device.CalculateChannelSubsystem
            Me.BindSubsystem(True, Me.CalculateChannelSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As CalculateChannelSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.CalculateChannelSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.CalculateChannelSubsystem.AveragingEnabled))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.CalculateChannelSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Calculate channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As CalculateChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.CalculateChannelSubsystemBase.AveragingEnabled)
                Me._AverageEnabledMenuItem.Checked = subsystem.AveragingEnabled.GetValueOrDefault(False)
            Case NameOf(VI.CalculateChannelSubsystemBase.AverageCount)
        End Select
    End Sub

    ''' <summary> Calculate channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub CalculateChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(E4990.CalculateChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.CalculateChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, E4990.CalculateChannelSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CHANNEL MARKER "

    ''' <summary> Gets the ChannelMarker subsystem. </summary>
    ''' <value> The ChannelMarker subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property ChannelMarkerSubsystem As ChannelMarkerSubsystem

    ''' <summary> Bind ChannelMarker subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindChannelMarkerSubsystem(ByVal device As E4990Device)
        If Me.ChannelMarkerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.ChannelMarkerSubsystem)
            Me._ChannelMarkerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._ChannelMarkerSubsystem = device.ChannelMarkerSubsystem
            Me.BindSubsystem(True, Me.ChannelMarkerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As ChannelMarkerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.ChannelmarkerSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.ChannelMarkerSubsystem.MarkerReadings))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.ChannelmarkerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the channel marker subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As ChannelMarkerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(E4990.ChannelMarkerSubsystem.MarkerReadings)
                If subsystem.MarkerReadings Is Nothing Then
                    Me._ReadingComboBox.Items.Clear()
                Else
                    Me._ReadingComboBox.ComboBox.ListEnumDescriptions(Of VI.ReadingElementTypes)(subsystem.MarkerReadings.Elements, VI.ReadingElementTypes.Units)
                End If
            Case NameOf(E4990.ChannelMarkerSubsystem.LastReading)
                ' To_DO: Bind the sense subsystem to display the data
                Me.Device.SenseChannelSubsystem.ParsePrimaryReading(subsystem.LastReading)
            Case NameOf(E4990.ChannelMarkerSubsystem.LastActionElapsedTime)
                'Me._LastReadingTextBox.Text = $"{subsystem.LastReading} @{subsystem.LastActionElapsedTime.ToExactMilliseconds:0.0}ms"
            Case NameOf(E4990.ChannelMarkerSubsystem.FailureCode)
                'Me._FailureToolStripStatusLabel.Text = subsystem.FailureCode
            Case NameOf(E4990.ChannelMarkerSubsystem.FailureLongDescription)
                'Me._FailureToolStripStatusLabel.ToolTipText = subsystem.FailureLongDescription
            Case NameOf(E4990.ChannelMarkerSubsystem.ReadingCaption)
                'Me._ReadingToolStripStatusLabel.Text = subsystem.ReadingCaption
        End Select
    End Sub

    ''' <summary> Channel marker subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ChannelmarkerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(E4990.ChannelMarkerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ChannelmarkerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, E4990.ChannelMarkerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try


    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: READING "

    ''' <summary> Selects a new reading to display. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The VI.ReadingElementTypes. </returns>
    Friend Function SelectReading(ByVal value As VI.ReadingElementTypes) As VI.ReadingElementTypes
        If Me.Device.IsDeviceOpen AndAlso (value <> VI.ReadingElementTypes.None) AndAlso (value <> Me.SelectedReadingType) Then
            Me._ReadingComboBox.ComboBox.SelectItem(value.ValueDescriptionPair)
        End If
        Return Me.SelectedReadingType
    End Function

    ''' <summary> Gets the type of the selected reading. </summary>
    ''' <value> The type of the selected reading. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedReadingType() As VI.ReadingElementTypes
        Get
            Return CType(CType(Me._ReadingComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.ReadingElementTypes)
        End Get
    End Property

    ''' <summary> Abort button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> <see cref="T:System.Object" />
    '''                                                                   instance of this
    '''                                             <see cref="T:System.Windows.Forms.Control" /> 
    ''' </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortMenuItem_Click(sender As Object, e As EventArgs) Handles _AbortMenuItem.Click

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} aborting measurements(s)"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.Device.IsDeviceOpen Then
                Me.PublishInfo($"{activity};. ")
                Me.Device.TriggerSubsystem.Abort()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by InitButton for click events. Initiates a reading for retrieval by
    ''' way of the service request event.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitiateMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _InitiateMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} aborting measurements(s)"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            Me.PublishInfo($"{activity};. ")

            ' clear execution state before enabling events
            Me.Device.ClearExecutionState()

            ' set the service request
            Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)

            ' trigger the initiation of the measurement letting the service request do the rest.
            Me.Device.ClearExecutionState()
            ' Me.Device.TriggerSubsystem.Initiate()

        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Reading combo box selected value changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadingComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _ReadingComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} selecting a reading to display"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.PublishInfo($"{activity};. ")
            Me.Device.ChannelMarkerSubsystem.SelectActiveReading(Me.SelectedReadingType)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads a marker. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    Public Sub ReadMarker(ByVal e As ActionEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Me.Device.ChannelMarkerSubsystem.Enabled Then
            Dim activity As String = $"{Me.Device.ResourceNameCaption} reading a marker"
            Me.PublishInfo($"{activity};. ")
            Me.Device.ClearExecutionState()
            ' clear the device display from warnings
            ' Me.Device.Session.Write(":DISP:CCL")
            Me.Device.DisplaySubsystem.ClearCautionMessages()
            If Me.Device.CalculateChannelSubsystem.AveragingEnabled.GetValueOrDefault(False) Then
                Me.Device.StatusSubsystem.EnableMeasurementAvailable()
                Me.Device.ChannelMarkerSubsystem.InitializeMarkerAverage(Me.Device.TriggerSubsystem, Me.Device.CalculateChannelSubsystem)
            Else
                ' Me.Device.Session.WriteLine(":TRIG")
                Me.Device.TriggerSubsystem.Initiate()
            End If
            Dim r As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests, Elapsed As TimeSpan) = Me.Device.Session.AwaitStatusBitmask(VI.Pith.ServiceRequests.RequestingService,
                                                                                                                     TimeSpan.FromMilliseconds(100),
                                                                                                                     Me.Device.Session.StatusReadDelay,
                                                                                                                     Me.Device.Session.EffectiveStatusReadTurnaroundTime)

            If r.TimedOut Then
                e.RegisterFailure("timeout")
            Else
                Me.Device.Session.ApplyServiceRequest(r.Status)
                ' auto scale after measurement completes
                Me.Device.PrimaryChannelTraceSubsystem.AutoScale()
                Me.Device.SecondaryChannelTraceSubsystem.AutoScale()
                Me.Device.PrimaryChannelTraceSubsystem.Select()
                Me.Device.ChannelMarkerSubsystem.FetchLatestData()
            End If
        Else
            e.RegisterFailure("Marker not defined")
        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by _ReadButton for click events. Query the Device for a reading.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading a marker"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Dim args As New ActionEventArgs
            Me.ReadMarker(args)
            If args.Failed Then
                Me.InfoProvider.Annunciate(sender, args.Details)
            End If

            If Me.Device.ChannelMarkerSubsystem.Enabled Then
                Me.PublishInfo($"{activity};. ")
                Me.Device.Session.StartElapsedStopwatch()
                Me.Device.ClearExecutionState()
                Me.Device.Session.ReadElapsedTime(True)
                ' clear the device display from warnings
                ' Me.Device.Session.Write(":DISP:CCL")
                Me.Device.DisplaySubsystem.ClearCautionMessages()
                If Me._AverageEnabledMenuItem.Checked Then
                    Me.Device.StatusSubsystem.EnableMeasurementAvailable()
                    Me.Device.ChannelMarkerSubsystem.InitializeMarkerAverage(Me.Device.TriggerSubsystem, Me.Device.CalculateChannelSubsystem)
                Else
                    ' Me.Device.Session.WriteLine(":TRIG")
                    Me.Device.TriggerSubsystem.Initiate()
                End If
                Dim r As (TimedOut As Boolean, Status As VI.Pith.ServiceRequests, Elpased As TimeSpan) = Me.Device.Session.AwaitStatusBitmask(VI.Pith.ServiceRequests.RequestingService,
                                                                                                                          TimeSpan.FromMilliseconds(100),
                                                                                                                          Me.Device.Session.StatusReadDelay,
                                                                                                                          Me.Device.Session.EffectiveStatusReadTurnaroundTime)
                If r.TimedOut Then
                    Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Alert, "timeout")
                Else
                    Me.Device.Session.ApplyServiceRequest(r.Status)
                    ' auto scale after measurement completes
                    Me.Device.PrimaryChannelTraceSubsystem.AutoScale()
                    Me.Device.SecondaryChannelTraceSubsystem.AutoScale()
                    Me.Device.PrimaryChannelTraceSubsystem.Select()
                    Me.Device.ChannelMarkerSubsystem.FetchLatestData()
                End If
            Else

                Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Info, "Define a marker first.")
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub


#End Region

#Region " CONTROL EVENT HANDLERS: SCALE "

    ''' <summary> Automatic scale menu item click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> <see cref="T:System.Object" />
    '''                                                                   instance of this
    '''                                             <see cref="T:System.Windows.Forms.Control" /> 
    ''' </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AutoScaleCheckBox_Click(sender As Object, e As EventArgs) Handles _AutoScaleEnabledMenuItem.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying auto scale"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.PrimaryChannelTraceSubsystem.AutoScale()
            Me.Device.SecondaryChannelTraceSubsystem.AutoScale()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
