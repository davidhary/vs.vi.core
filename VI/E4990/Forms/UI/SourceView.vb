Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions
Imports isr.VI.Facade.ComboBoxExtensions

''' <summary> A Source view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class SourceView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="SourceView"/> </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <returns> A <see cref="SourceView"/>. </returns>
    Public Shared Function Create() As SourceView
        Dim view As SourceView = Nothing
        Try
            view = New SourceView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As E4990Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As E4990Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As E4990Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindSourceChannelSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As E4990Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SOURCE "

    ''' <summary> Gets or sets the SourceChannel subsystem. </summary>
    ''' <value> The SourceChannel subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SourceChannelSubsystem As SourceChannelSubsystem

    ''' <summary> Bind SourceChannel subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSourceChannelSubsystem(ByVal device As E4990Device)
        If Me.SourceChannelSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SourceChannelSubsystem)
            Me._SourceChannelSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SourceChannelSubsystem = device.SourceChannelSubsystem
            Me.BindSubsystem(True, Me.SourceChannelSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SourceChannelSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SourceChannelSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(E4990.SourceChannelSubsystem.SupportedFunctionModes))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SourceChannelSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Source channel subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Protected Overridable Overloads Sub HandlePropertyChanged(ByVal subsystem As VI.SourceChannelSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.SourceChannelSubsystemBase.SupportedFunctionModes)
                Me._SourceFunctionComboBox.ListSupportedSourceFunctionModes(subsystem.SupportedFunctionModes)
                ' Imports isr.VI.Facade.ComboBoxExtensions
            Case NameOf(VI.SourceChannelSubsystemBase.FunctionMode)
                Me.SelectSourceFunctionMode()
            Case NameOf(VI.SourceChannelSubsystemBase.Level)
                If subsystem.Level.HasValue Then Me.SourceLevel = subsystem.Level.Value
        End Select
    End Sub

    ''' <summary> Source channel subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SourceChannelSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SourceChannelSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SourceChannelSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SourceChannelSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: SOURCE "

    ''' <summary> Gets or sets source level. </summary>
    ''' <value> The source level. </value>
    Private Property SourceLevel As Double
        Get
            Return If(Me.SelectedSourceFunctionMode = VI.SourceFunctionModes.Voltage, Me._LevelNumeric.Value, 0.001 * Me._LevelNumeric.Value)
        End Get
        Set(value As Double)
            Me._LevelNumeric.Value = If(Me.SelectedSourceFunctionMode = VI.SourceFunctionModes.Voltage, CDec(value), CDec(1000 * value))
        End Set
    End Property

    ''' <summary> Gets the selected source function mode. </summary>
    ''' <value> The selected source function mode. </value>
    Public ReadOnly Property SelectedSourceFunctionMode As VI.SourceFunctionModes
        Get
            Return Me._SourceFunctionComboBox.SelectedSourceFunctionModes()
        End Get
    End Property

    ''' <summary> Applies the source setting button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySourceSettingButton_Click(sender As Object, e As EventArgs) Handles _ApplySourceSettingButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying source settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            ' Set OSC mode
            Me.Device.SourceChannelSubsystem.ApplyFunctionMode(Me.SelectedSourceFunctionMode)
            Me.Device.SourceChannelSubsystem.ApplyLevel(Me.SourceLevel)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Select source function mode. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Private Sub SelectSourceFunctionMode()
        Me._SourceFunctionComboBox.SafeSelectSourceFunctionModes(Me.Device.SourceChannelSubsystem.FunctionMode)
        If Me.Device.SourceChannelSubsystem.FunctionMode.HasValue Then
            If Me.Device.SourceChannelSubsystem.FunctionMode.Value = VI.SourceFunctionModes.Current Then
                Me._LevelNumeric.Minimum = CDec(1000 * Me.Device.SourceChannelSubsystem.FunctionRange.Min)
                Me._LevelNumeric.Maximum = CDec(1000 * Me.Device.SourceChannelSubsystem.FunctionRange.Max)
                Me._LevelNumeric.DecimalPlaces = Math.Max(0, Me.Device.SourceChannelSubsystem.FunctionRange.DecimalPlaces - 3)
                Me._LevelNumeric.Increment = If(Me._LevelNumeric.Minimum > 0, Me._LevelNumeric.Minimum, 0.1D * (Me._LevelNumeric.Maximum - Me._LevelNumeric.Minimum))
            Else
                Me._LevelNumeric.Minimum = CDec(Me.Device.SourceChannelSubsystem.FunctionRange.Min)
                Me._LevelNumeric.Maximum = CDec(Me.Device.SourceChannelSubsystem.FunctionRange.Max)
                Me._LevelNumeric.DecimalPlaces = Me.Device.SourceChannelSubsystem.FunctionRange.DecimalPlaces
                Me._LevelNumeric.Increment = If(Me._LevelNumeric.Minimum > 0, Me._LevelNumeric.Minimum, 0.1D * (Me._LevelNumeric.Maximum - Me._LevelNumeric.Minimum))
            End If
        End If
    End Sub

    ''' <summary> Source function combo box selected value changed. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SourceFunctionComboBox_SelectedValueChanged(sender As Object, e As EventArgs) Handles _SourceFunctionComboBox.SelectedValueChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} toggling source settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me._LevelNumericLabel.Text = If(Me.SelectedSourceFunctionMode = VI.SourceFunctionModes.Voltage, "Level [V]:", "Level [mA]:")
            Me._LevelNumericLabel.Left = Me._LevelNumeric.Left - Me._LevelNumericLabel.Width
            Me._LevelNumericLabel.Invalidate()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Applies the source function button click. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySourceFunctionButton_Click(sender As Object, e As EventArgs) Handles _ApplySourceFunctionButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} applying source settings"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            ' Set OSC mode
            Me.Device.SourceChannelSubsystem.ApplyFunctionMode(Me.SelectedSourceFunctionMode)
            Me.Device.SourceChannelSubsystem.QueryLevel()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Error, $"Exception {activity}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-11. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
