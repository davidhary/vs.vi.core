<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReadingView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ReadingView))
        Me._ReadingsDataGridView = New System.Windows.Forms.DataGridView()
        Me._ReadingToolStrip = New System.Windows.Forms.ToolStrip()
        Me._ReadButton = New System.Windows.Forms.ToolStripButton()
        Me._ReadingOptionsDropDownButton = New isr.Core.Controls.ToolStripDropDownButton()
        Me._AutoScaleEnabledMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AverageEnabledMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TraceDropDownButton = New System.Windows.Forms.ToolStripDropDownButton()
        Me._InitiateMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._TraceMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._AbortMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me._ReadingsCountLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReadingComboBox = New System.Windows.Forms.ToolStripComboBox()
        Me._ClearBufferDisplayButton = New System.Windows.Forms.ToolStripButton()
        CType(Me._ReadingsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._ReadingToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_ReadingsDataGridView
        '
        Me._ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ReadingsDataGridView.Location = New System.Drawing.Point(1, 26)
        Me._ReadingsDataGridView.Name = "_ReadingsDataGridView"
        Me._ReadingsDataGridView.Size = New System.Drawing.Size(379, 297)
        Me._ReadingsDataGridView.TabIndex = 23
        '
        '_ReadingToolStrip
        '
        Me._ReadingToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._ReadButton, Me._ReadingOptionsDropDownButton, Me._TraceDropDownButton, Me._ReadingsCountLabel, Me._ReadingComboBox, Me._ClearBufferDisplayButton})
        Me._ReadingToolStrip.Location = New System.Drawing.Point(1, 1)
        Me._ReadingToolStrip.Name = "_ReadingToolStrip"
        Me._ReadingToolStrip.Size = New System.Drawing.Size(379, 25)
        Me._ReadingToolStrip.TabIndex = 22
        Me._ReadingToolStrip.Text = "ToolStrip1"
        '
        '_ReadButton
        '
        Me._ReadButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadButton.Image = CType(resources.GetObject("_ReadButton.Image"), System.Drawing.Image)
        Me._ReadButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(37, 22)
        Me._ReadButton.Text = "Read"
        Me._ReadButton.ToolTipText = "Read single reading"
        '
        '_ReadingOptionsDropDownButton
        '
        Me._ReadingOptionsDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ReadingOptionsDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._AutoScaleEnabledMenuItem, Me._AverageEnabledMenuItem})
        Me._ReadingOptionsDropDownButton.Image = CType(resources.GetObject("_ReadingOptionsDropDownButton.Image"), System.Drawing.Image)
        Me._ReadingOptionsDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ReadingOptionsDropDownButton.Name = "_ReadingOptionsDropDownButton"
        Me._ReadingOptionsDropDownButton.Size = New System.Drawing.Size(62, 22)
        Me._ReadingOptionsDropDownButton.Text = "Options"
        '
        '_AutoScaleEnabledMenuItem
        '
        Me._AutoScaleEnabledMenuItem.CheckOnClick = True
        Me._AutoScaleEnabledMenuItem.Name = "_AutoScaleEnabledMenuItem"
        Me._AutoScaleEnabledMenuItem.Size = New System.Drawing.Size(130, 22)
        Me._AutoScaleEnabledMenuItem.Text = "Auto Scale"
        Me._AutoScaleEnabledMenuItem.ToolTipText = "Check to auto scale"
        '
        '_AverageEnabledMenuItem
        '
        Me._AverageEnabledMenuItem.CheckOnClick = True
        Me._AverageEnabledMenuItem.Name = "_AverageEnabledMenuItem"
        Me._AverageEnabledMenuItem.Size = New System.Drawing.Size(130, 22)
        Me._AverageEnabledMenuItem.Text = "Average"
        Me._AverageEnabledMenuItem.ToolTipText = "Check to average"
        '
        '_TraceDropDownButton
        '
        Me._TraceDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._TraceDropDownButton.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._InitiateMenuItem, Me._TraceMenuItem, Me._AbortMenuItem})
        Me._TraceDropDownButton.Image = CType(resources.GetObject("_TraceDropDownButton.Image"), System.Drawing.Image)
        Me._TraceDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._TraceDropDownButton.Name = "_TraceDropDownButton"
        Me._TraceDropDownButton.Size = New System.Drawing.Size(67, 22)
        Me._TraceDropDownButton.Text = "Initiation"
        '
        '_InitiateMenuItem
        '
        Me._InitiateMenuItem.CheckOnClick = True
        Me._InitiateMenuItem.Name = "_InitiateMenuItem"
        Me._InitiateMenuItem.Size = New System.Drawing.Size(110, 22)
        Me._InitiateMenuItem.Text = "Initiate"
        '
        '_TraceMenuItem
        '
        Me._TraceMenuItem.Name = "_TraceMenuItem"
        Me._TraceMenuItem.Size = New System.Drawing.Size(110, 22)
        Me._TraceMenuItem.Text = "Trace"
        '
        '_AbortMenuItem
        '
        Me._AbortMenuItem.Name = "_AbortMenuItem"
        Me._AbortMenuItem.Size = New System.Drawing.Size(110, 22)
        Me._AbortMenuItem.Text = "Abort"
        '
        '_ReadingsCountLabel
        '
        Me._ReadingsCountLabel.Name = "_ReadingsCountLabel"
        Me._ReadingsCountLabel.Size = New System.Drawing.Size(13, 22)
        Me._ReadingsCountLabel.Text = "0"
        Me._ReadingsCountLabel.ToolTipText = "Buffer count"
        '
        '_ReadingComboBox
        '
        Me._ReadingComboBox.Name = "_ReadingComboBox"
        Me._ReadingComboBox.Size = New System.Drawing.Size(121, 25)
        Me._ReadingComboBox.ToolTipText = "Select reading type"
        '
        '_ClearBufferDisplayButton
        '
        Me._ClearBufferDisplayButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._ClearBufferDisplayButton.Font = New System.Drawing.Font("Wingdings", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(2, Byte))
        Me._ClearBufferDisplayButton.Image = CType(resources.GetObject("_ClearBufferDisplayButton.Image"), System.Drawing.Image)
        Me._ClearBufferDisplayButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._ClearBufferDisplayButton.Name = "_ClearBufferDisplayButton"
        Me._ClearBufferDisplayButton.Size = New System.Drawing.Size(25, 22)
        Me._ClearBufferDisplayButton.Text = String.Empty
        Me._ClearBufferDisplayButton.ToolTipText = "Clears the buffer display"
        '
        'ReadingView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._ReadingsDataGridView)
        Me.Controls.Add(Me._ReadingToolStrip)
        Me.Name = "ReadingView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(381, 324)
        CType(Me._ReadingsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me._ReadingToolStrip.ResumeLayout(False)
        Me._ReadingToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _ReadingsDataGridView As Windows.Forms.DataGridView
    Private WithEvents _ReadingToolStrip As Windows.Forms.ToolStrip
    Private WithEvents _ReadButton As Windows.Forms.ToolStripButton
    Private WithEvents _ReadingsCountLabel As Windows.Forms.ToolStripLabel
    Private WithEvents _ReadingComboBox As Windows.Forms.ToolStripComboBox
    Private WithEvents _ClearBufferDisplayButton As Windows.Forms.ToolStripButton
    Private WithEvents _TraceDropDownButton As Windows.Forms.ToolStripDropDownButton
    Private WithEvents _InitiateMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _TraceMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AbortMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _ReadingOptionsDropDownButton As Core.Controls.ToolStripDropDownButton
    Private WithEvents _AutoScaleEnabledMenuItem As Windows.Forms.ToolStripMenuItem
    Private WithEvents _AverageEnabledMenuItem As Windows.Forms.ToolStripMenuItem
End Class
