''' <summary> E4990 Visa View unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("e4990")>
Public Class VisaViewTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(E4990ResourceInfo.Exists, $"{GetType(E4990FormsTests.ResourceSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SELECTOR OPENER: SELECT, OPEN, CLOSE "

    ''' <summary> (Unit Test Method) tests selector opener. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SelectorOpenerTest()
        isr.VI.FacadeTests.DeviceManager.AssertResourceShouldOpenAndClose(TestInfo, E4990ResourceInfo)
    End Sub

#End Region

#Region " VISA VIEW: DEVICE OPEN TEST "

    ''' <summary> (Unit Test Method) tests selected resource name visa view. </summary>
    ''' <remarks>
    ''' Visa View Timing:<para>
    ''' Ping elapsed 0.123   </para><para>
    ''' Create device 0.097   </para><para>
    ''' Check selected resource name 2.298   </para><para>
    '''  </para>
    ''' </remarks>
    <TestMethod()>
    Public Sub SelectedResourceNameVisaViewTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        TestInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            TestInfo.TraceMessage($"Create device {sw.Elapsed:s\.fff}")
            sw.Restart()
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected(TestInfo, view, E4990ResourceInfo)
                TestInfo.TraceMessage($"Check selected resource name {sw.Elapsed:s\.fff}")
                sw.Restart()
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests selected resource name visa tree view. </summary>
    ''' <remarks>
    ''' Visa Tree View Timing:<para>
    ''' Ping elapsed 0.119   </para><para>
    ''' Create device 0.125   </para><para>
    ''' Check selected resource name 2.293   </para><para>
    '''  </para>
    ''' </remarks>
    <TestMethod()>
    Public Sub SelectedResourceNameVisaTreeViewTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        TestInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            TestInfo.TraceMessage($"Create device {sw.Elapsed:s\.fff}")
            sw.Restart()
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected(TestInfo, view, E4990ResourceInfo)
                TestInfo.TraceMessage($"Check selected resource name {sw.Elapsed:s\.fff}")
                sw.Restart()
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests talker trace message visa view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TalkerTraceMessageVisaViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Dim messageCount As Integer = TestInfo.TraceMessagesQueueListener.Count
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                ' when assigning the device to the view, the listener is assigned.
                Assert.IsTrue(TestInfo.TraceMessagesQueueListener.Count > messageCount, "Constructing a device should add trace messages")
                ' view.AddListener(TestInfo.TraceMessagesQueueListener)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit(view, TestInfo.TraceMessagesQueueListener)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests talker trace message visa tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TalkerTraceMessageVisaTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit(view, TestInfo.TraceMessagesQueueListener)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session visa view. </summary>
    ''' <remarks>
    ''' <para>
    ''' Ping elapsed 0.126   </para><para>
    ''' Session open 5.423   </para><para>
    ''' Session checked 0.001   </para><para>
    ''' Session closed 0.024   </para><para>
    '''    </para>
    ''' </remarks>
    <TestMethod()>
    Public Sub OpenSessionVisaViewTest()
        Dim sw As Stopwatch = Stopwatch.StartNew
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        TestInfo.TraceMessage($"Ping elapsed {sw.Elapsed:s\.fff}")
        sw.Restart()
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            sw.Restart()
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen(TestInfo, 0, view, E4990ResourceInfo)
                    TestInfo.TraceMessage($"Session open {sw.Elapsed:s\.fff}")
                    sw.Restart()
                    isr.VI.FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch(view.VisaSessionBase.Session, E4990ResourceInfo)
                    TestInfo.TraceMessage($"Session checked {sw.Elapsed:s\.fff}")
                    sw.Restart()
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 0, view)
                    TestInfo.TraceMessage($"Session closed {sw.Elapsed:s\.fff}")
                    sw.Restart()
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session visa tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionVisaTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen(TestInfo, 0, view, E4990ResourceInfo)
                    isr.VI.FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch(view.VisaSessionBase.Session, E4990ResourceInfo)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 0, view)
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session twice visa view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTwiceVisaViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, E4990ResourceInfo)
            End Using
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 2, view, E4990ResourceInfo)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests open session twice visa tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTwiceVisaTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, E4990ResourceInfo)
            End Using
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 2, view, E4990ResourceInfo)
            End Using
        End Using
    End Sub

#End Region

#Region " VISA VIEW: ASSIGNED DEVICE TESTS "

    ''' <summary> (Unit Test Method) tests assign device view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignDeviceViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using view As VI.E4990.Forms.E4990View = New VI.E4990.Forms.E4990View
            Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                view.AssignDevice(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, E4990ResourceInfo)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests assign device tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignDeviceTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using view As VI.E4990.Forms.E4990TreeView = New VI.E4990.Forms.E4990TreeView
            Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                view.AssignDevice(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, E4990ResourceInfo)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests assign open device view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignOpenDeviceViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using view As VI.E4990.Forms.E4990View = New VI.E4990.Forms.E4990View
            Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen(TestInfo, 1, device, E4990ResourceInfo)
                    view.AssignDevice(device)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 1, view)
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests assign open device tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignOpenDeviceTreeViewTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using view As VI.E4990.Forms.E4990TreeView = New VI.E4990.Forms.E4990TreeView
            Using device As VI.E4990.E4990Device = VI.E4990.E4990Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen(TestInfo, 1, device, E4990ResourceInfo)
                    view.AssignDevice(device)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 1, view)
                End Try
            End Using
        End Using
    End Sub

#End Region

End Class
