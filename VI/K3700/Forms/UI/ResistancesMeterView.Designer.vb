Imports System.Windows.Forms
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ResistancesMeterView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ResistancesMeterView))
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._DataGrid = New System.Windows.Forms.DataGridView()
        Me._BottomToolStrip = New System.Windows.Forms.ToolStrip()
        Me._GageTitleLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ReadingLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ChannelLabel = New System.Windows.Forms.ToolStripLabel()
        Me._ComplianceLabel = New System.Windows.Forms.ToolStripLabel()
        Me._MeasureButton = New System.Windows.Forms.ToolStripButton()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        CType(Me._DataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._BottomToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._Layout.Size = New System.Drawing.Size(341, 360)
        Me._Layout.TabIndex = 1
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._DataGrid)
        Me._Panel.Controls.Add(Me._BottomToolStrip)
        Me._Panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Panel.Location = New System.Drawing.Point(6, 6)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(329, 348)
        Me._Panel.TabIndex = 2
        '
        '_DataGrid
        '
        Me._DataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._DataGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me._DataGrid.Location = New System.Drawing.Point(0, 0)
        Me._DataGrid.Name = "_DataGrid"
        Me._DataGrid.Size = New System.Drawing.Size(329, 323)
        Me._DataGrid.TabIndex = 8
        '
        '_BottomToolStrip
        '
        Me._BottomToolStrip.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._BottomToolStrip.GripMargin = New System.Windows.Forms.Padding(0)
        Me._BottomToolStrip.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden
        Me._BottomToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._GageTitleLabel, Me._ReadingLabel, Me._ChannelLabel, Me._ComplianceLabel, Me._MeasureButton})
        Me._BottomToolStrip.Location = New System.Drawing.Point(0, 323)
        Me._BottomToolStrip.Name = "_BottomToolStrip"
        Me._BottomToolStrip.Size = New System.Drawing.Size(329, 25)
        Me._BottomToolStrip.TabIndex = 2
        Me._BottomToolStrip.Text = "Tool Strip"
        '
        '_GageTitleLabel
        '
        Me._GageTitleLabel.Name = "_GageTitleLabel"
        Me._GageTitleLabel.Size = New System.Drawing.Size(20, 22)
        Me._GageTitleLabel.Text = "R1"
        Me._GageTitleLabel.ToolTipText = "Gage"
        '
        '_ReadingLabel
        '
        Me._ReadingLabel.Name = "_ReadingLabel"
        Me._ReadingLabel.Size = New System.Drawing.Size(70, 22)
        Me._ReadingLabel.Text = "0.0000 Ohm"
        Me._ReadingLabel.ToolTipText = "Measured value"
        '
        '_ChannelLabel
        '
        Me._ChannelLabel.Name = "_ChannelLabel"
        Me._ChannelLabel.Size = New System.Drawing.Size(112, 22)
        Me._ChannelLabel.Text = "1001;1031;1912;1921"
        Me._ChannelLabel.ToolTipText = "Channel List"
        '
        '_ComplianceLabel
        '
        Me._ComplianceLabel.Name = "_ComplianceLabel"
        Me._ComplianceLabel.Size = New System.Drawing.Size(21, 22)
        Me._ComplianceLabel.Text = ".C."
        Me._ComplianceLabel.ToolTipText = "Measurement Compliance"
        '
        '_MeasureButton
        '
        Me._MeasureButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me._MeasureButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me._MeasureButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasureButton.Image = CType(resources.GetObject("_MeasureButton.Image"), System.Drawing.Image)
        Me._MeasureButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me._MeasureButton.Name = "_MeasureButton"
        Me._MeasureButton.Size = New System.Drawing.Size(64, 22)
        Me._MeasureButton.Text = "Measure"
        Me._MeasureButton.ToolTipText = "Measure resistances"
        '
        'ResistancesMeterView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._Layout)
        Me.Name = "ResistancesMeterView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(343, 362)
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        CType(Me._DataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me._BottomToolStrip.ResumeLayout(False)
        Me._BottomToolStrip.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _Layout As TableLayoutPanel
    Private WithEvents _BottomToolStrip As ToolStrip
    Private WithEvents _ReadingLabel As ToolStripLabel
    Private WithEvents _DataGrid As DataGridView
    Private WithEvents _GageTitleLabel As ToolStripLabel
    Private WithEvents _ChannelLabel As ToolStripLabel
    Private WithEvents _ComplianceLabel As ToolStripLabel
    Private WithEvents _Panel As Panel
    Private WithEvents _MeasureButton As ToolStripButton
End Class
