Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.ExceptionExtensions


''' <summary> A multimeter view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class MultimeterView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="MultimeterView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As MultimeterView
        Dim view As MultimeterView = Nothing
        Try
            view = New MultimeterView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K3700Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K3700Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K3700Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindMultimeterSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K3700Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " MUTLIMETER "

    ''' <summary> Gets or sets the Multimeter subsystem. </summary>
    ''' <value> The Multimeter subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MultimeterSubsystem As VI.Tsp.K3700.MultimeterSubsystem

    ''' <summary> Bind Multimeter subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMultimeterSubsystem(ByVal device As VI.Tsp.K3700.K3700Device)
        If Me.MultimeterSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MultimeterSubsystem)
            Me._MultimeterSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._MultimeterSubsystem = device.MultimeterSubsystem
            Me.BindSubsystem(True, Me.MultimeterSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MultimeterSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MultimeterSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.SupportedAutoDelayModes))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.AutoDelayMode))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.AutoRangeEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.AutoZeroEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.AutoDelayEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FilterCount))

            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FilterCountRange))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FilterEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FilterWindow))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FilterWindowRange))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.MovingAverageFilterEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FunctionRange))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FunctionRangeDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.FunctionUnit))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.OpenDetectorEnabled))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.PowerLineCycles))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.PowerLineCyclesDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.PowerLineCyclesRange))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.PowerLineCyclesDecimalPlaces))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.Tsp.K3700.MultimeterSubsystem.Range))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MultimeterSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the Multimeter subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MultimeterSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K3700.MultimeterSubsystem.AutoDelayMode)
                If subsystem.AutoDelayMode.HasValue Then Me.AutoDelayMode = subsystem.AutoDelayMode.Value
            Case NameOf(K3700.MultimeterSubsystem.AutoRangeEnabled)
                If subsystem.AutoRangeEnabled.HasValue Then Me._AutoRangeCheckBox.Checked = subsystem.AutoRangeEnabled.Value
            Case NameOf(K3700.MultimeterSubsystem.AutoZeroEnabled)
                If subsystem.AutoZeroEnabled.HasValue Then Me._AutoZeroCheckBox.Checked = subsystem.AutoZeroEnabled.Value
            Case NameOf(K3700.MultimeterSubsystem.FilterCount)
                If subsystem.FilterCount.HasValue Then Me._FilterCountNumeric.Value = subsystem.FilterCount.Value
            Case NameOf(K3700.MultimeterSubsystem.FilterCountRange)
                Me._FilterCountNumeric.RangeSetter(subsystem.FilterCountRange.Min, subsystem.FilterCountRange.Max)
                Me._FilterCountNumeric.DecimalPlaces = 0
            Case NameOf(K3700.MultimeterSubsystem.FilterEnabled)
                If subsystem.FilterEnabled.HasValue Then Me._FilterEnabledCheckBox.Checked = subsystem.FilterEnabled.Value
                If Me._FilterEnabledCheckBox.Checked <> Me._FilterGroupBox.Enabled Then Me._FilterGroupBox.Enabled = Me._FilterEnabledCheckBox.Checked
            Case NameOf(K3700.MultimeterSubsystem.FilterWindow)
                If subsystem.FilterWindow.HasValue Then Me._FilterWindowNumeric.Value = CDec(100 * subsystem.FilterWindow.Value)
            Case NameOf(K3700.MultimeterSubsystem.FilterWindowRange)
                Dim range As Core.Primitives.RangeR = subsystem.FilterWindowRange.TransposedRange(0, 100)
                Me._FilterWindowNumeric.RangeSetter(range.Min, range.Max)
                Me._FilterWindowNumeric.DecimalPlaces = 0
            Case NameOf(K3700.MultimeterSubsystem.MovingAverageFilterEnabled)
                If subsystem.MovingAverageFilterEnabled.HasValue Then Me._MovingAverageRadioButton.Checked = subsystem.MovingAverageFilterEnabled.Value
                If subsystem.MovingAverageFilterEnabled.HasValue Then Me._RepeatingAverageRadioButton.Checked = Not subsystem.MovingAverageFilterEnabled.Value

            Case NameOf(K3700.MultimeterSubsystem.FunctionMode)
                Dim value As VI.MultimeterFunctionModes = subsystem.FunctionMode.GetValueOrDefault(VI.MultimeterFunctionModes.VoltageDC)
                Me._SenseFunctionComboBox.SelectedEnumValue(value)
                ' changing the function mode changes range, auto delay mode and open detector enabled. 
                subsystem.QueryRange()
                subsystem.QueryAutoDelayMode()
                If subsystem.FunctionOpenDetectorCapable Then subsystem.QueryOpenDetectorEnabled()

            Case NameOf(K3700.MultimeterSubsystem.FunctionRange)
                Me._SenseRangeNumeric.RangeSetter(subsystem.FunctionRange.Min, subsystem.FunctionRange.Max)
            Case NameOf(K3700.MultimeterSubsystem.FunctionRangeDecimalPlaces)
                Me._SenseRangeNumeric.DecimalPlaces = subsystem.DefaultFunctionModeDecimalPlaces
            Case NameOf(K3700.MultimeterSubsystem.FunctionUnit)
                Me._SenseRangeNumericLabel.Text = $"Range [{subsystem.FunctionUnit}]:"
                Me._SenseRangeNumericLabel.Left = Me._SenseRangeNumeric.Left - Me._SenseRangeNumericLabel.Width
            Case NameOf(K3700.MultimeterSubsystem.OpenDetectorEnabled)
                If subsystem.OpenDetectorEnabled.HasValue Then Me._OpenDetectorCheckBox.Checked = subsystem.OpenDetectorEnabled.Value
            Case NameOf(K3700.MultimeterSubsystem.PowerLineCycles)
                If subsystem.PowerLineCycles.HasValue Then Me._PowerLineCyclesNumeric.Value = CDec(subsystem.PowerLineCycles.Value)
            Case NameOf(K3700.MultimeterSubsystem.PowerLineCyclesRange)
                Me._PowerLineCyclesNumeric.RangeSetter(subsystem.PowerLineCyclesRange.Min, subsystem.PowerLineCyclesRange.Max)
                Me._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces
            Case NameOf(K3700.MultimeterSubsystem.PowerLineCyclesDecimalPlaces)
                Me._PowerLineCyclesNumeric.DecimalPlaces = subsystem.PowerLineCyclesDecimalPlaces
            Case NameOf(K3700.MultimeterSubsystem.Range)
                If subsystem.Range.HasValue Then Me.SenseRangeSetter(subsystem.Range.Value)
            Case NameOf(K3700.MultimeterSubsystem.LastReading)
                ' Dim value As String = subsystem.LastReading
                'Me._LastReadingTextBox.Text = If(String.IsNullOrWhiteSpace(value), "<last reading>", value)
                'subsystem.LastActionElapsedTime = Me.ReadElapsedTime(True)
            Case NameOf(K3700.MultimeterSubsystem.LastActionElapsedTime)
                ' Dim value As String = subsystem.LastReading
                'Me._LastReadingTextBox.Text = $"{If(String.IsNullOrWhiteSpace(value), "<last reading>", value)} @{subsystem.LastActionElapsedTime.ToExactMilliseconds:0.0}ms"
            Case NameOf(K3700.MultimeterSubsystem.FailureCode)
                'Me._FailureToolStripStatusLabel.Text = subsystem.FailureCode
            Case NameOf(K3700.MultimeterSubsystem.FailureLongDescription)
                'Me._FailureToolStripStatusLabel.ToolTipText = subsystem.FailureLongDescription
            Case NameOf(K3700.MultimeterSubsystem.ReadingCaption)
                'Me._ReadingToolStripStatusLabel.Text = subsystem.ReadingCaption

            Case NameOf(K3700.MultimeterSubsystem.SupportedAutoDelayModes)
                Dim init As Boolean = Me.InitializingComponents
                Me.InitializingComponents = True
                Me._SenseFunctionComboBox.ListEnumDescriptions(subsystem.SupportedFunctionModes, Not subsystem.SupportedFunctionModes)
                Me.InitializingComponents = init
        End Select
    End Sub

    ''' <summary> Multimeter subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MultimeterSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MultimeterSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, MultimeterSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE SETTINGS: FUNCTION MODE "

    ''' <summary> Gets or sets the selected function mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The VI.MultimeterFunctionModes. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private Function SelectedFunctionMode() As VI.MultimeterFunctionModes
        Dim activity As String = $"Selecting function mode {Me._SenseFunctionComboBox.SelectedItem}"
        Dim result As VI.MultimeterFunctionModes = MultimeterFunctionModes.CurrentAC
        Try
            result = Me._SenseFunctionComboBox.SelectedEnumValue(result)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._SenseFunctionComboBox, Core.Forma.InfoProviderLevel.Error, ex.ToString)
            Me.PublishException(activity, ex)
        End Try
        Return result
    End Function

#End Region

#Region " CONTROL EVENT HANDLERS: SENSE "

    ''' <summary> Sense range setter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub SenseRangeSetter(ByVal value As Double)
        If value <= Me._SenseRangeNumeric.Maximum AndAlso value >= Me._SenseRangeNumeric.Minimum Then Me._SenseRangeNumeric.Value = CDec(value)
    End Sub

    ''' <summary>
    ''' Event handler. Called by _SenseFunctionComboBox for selected index changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseFunctionComboBox_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _SenseFunctionComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} applying function mode"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me._Device.MultimeterSubsystem.ApplyFunctionMode(Me.SelectedFunctionMode)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the function mode button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyFunctionModeButton_Click(sender As Object, e As EventArgs) Handles _ApplyFunctionModeButton.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} applying function mode"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me._Device.MultimeterSubsystem.ApplyFunctionMode(Me.SelectedFunctionMode)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Applies the selected measurements settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ApplySenseSettings()
        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.PowerLineCycles, Me._PowerLineCyclesNumeric.Value) Then
            Me.Device.MultimeterSubsystem.ApplyPowerLineCycles(Me._PowerLineCyclesNumeric.Value)
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.AutoDelayMode, Me._AutoDelayMode) Then
            Me.Device.MultimeterSubsystem.ApplyAutoDelayMode(Me._AutoDelayMode)
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.AutoRangeEnabled, Me._AutoRangeCheckBox.Checked) Then
            Me.Device.MultimeterSubsystem.ApplyAutoRangeEnabled(Me._AutoRangeCheckBox.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.AutoZeroEnabled, Me._AutoZeroCheckBox.Checked) Then
            Me.Device.MultimeterSubsystem.ApplyAutoZeroEnabled(Me._AutoZeroCheckBox.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.FilterEnabled, Me._FilterEnabledCheckBox.Checked) Then
            Me.Device.MultimeterSubsystem.ApplyFilterEnabled(Me._FilterEnabledCheckBox.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.FilterCount, Me._FilterCountNumeric.Value) Then
            Me.Device.MultimeterSubsystem.ApplyFilterCount(CInt(Me._FilterCountNumeric.Value))
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.MovingAverageFilterEnabled, Me._MovingAverageRadioButton.Checked) Then
            Me.Device.MultimeterSubsystem.ApplyMovingAverageFilterEnabled(Me._MovingAverageRadioButton.Checked)
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.OpenDetectorEnabled, Me._OpenDetectorCheckBox.Checked) Then
            Me.Device.MultimeterSubsystem.ApplyOpenDetectorEnabled(Me._OpenDetectorCheckBox.Checked)
        End If

        If Me.Device.MultimeterSubsystem.AutoRangeEnabled Then
            Me.Device.MultimeterSubsystem.QueryRange()
        ElseIf Not Nullable.Equals(Me.Device.MultimeterSubsystem.Range, Me._SenseRangeNumeric.Value) Then
            Me.Device.MultimeterSubsystem.ApplyRange(CInt(Me._SenseRangeNumeric.Value))
        End If

        If Not Nullable.Equals(Me.Device.MultimeterSubsystem.FilterWindow, 0.01 * Me._FilterWindowNumeric.Value) Then
            Me.Device.MultimeterSubsystem.ApplyFilterWindow(0.01 * Me._FilterWindowNumeric.Value)
        End If
    End Sub

    ''' <summary> Event handler. Called by ApplySenseSettingsButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplySenseSettingsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _ApplySenseSettingsButton.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} applying sense functions"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.SelectedFunctionMode <> Me.Device.MultimeterSubsystem.FunctionMode.Value Then
                Me.InfoProvider.Annunciate(sender, Core.Forma.InfoProviderLevel.Info, "Set function first")
            Else
                Me.ApplySenseSettings()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary> Filter enabled check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FilterEnabledCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _FilterEnabledCheckBox.CheckedChanged
        Me._FilterGroupBox.Enabled = Me._FilterEnabledCheckBox.Checked
    End Sub

    ''' <summary> Opens detector check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenDetectorCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _OpenDetectorCheckBox.CheckedChanged
        Me._OpenDetectorCheckBox.Text = $"Open Detector {Me._OpenDetectorCheckBox.Checked.GetHashCode:'ON';'ON';'OFF'}"
    End Sub

    ''' <summary> The automatic delay mode. </summary>
    Private _AutoDelayMode As MultimeterAutoDelayModes

    ''' <summary> Gets or sets the automatic delay mode. </summary>
    ''' <value> The automatic delay mode. </value>
    Private Property AutoDelayMode As MultimeterAutoDelayModes
        Get
            If Me._AutoDelayCheckBox.CheckState = CheckState.Checked Then
                Me._AutoDelayMode = MultimeterAutoDelayModes.On
            ElseIf Me._AutoDelayCheckBox.CheckState = CheckState.Indeterminate Then
                Me._AutoDelayMode = MultimeterAutoDelayModes.Once
            Else
                Me._AutoDelayMode = MultimeterAutoDelayModes.Off
            End If
            Return Me._AutoDelayMode
        End Get
        Set(value As MultimeterAutoDelayModes)
            Me._AutoDelayMode = value
            Select Case value
                Case MultimeterAutoDelayModes.Off
                    Me._AutoDelayCheckBox.CheckState = CheckState.Unchecked
                Case MultimeterAutoDelayModes.On
                    Me._AutoDelayCheckBox.CheckState = CheckState.Checked
                Case MultimeterAutoDelayModes.Once
                    Me._AutoDelayCheckBox.CheckState = CheckState.Indeterminate
            End Select
        End Set
    End Property

    ''' <summary> Automatic delay check box checked changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub AutoDelayCheckBox_CheckedChanged(sender As Object, e As EventArgs) Handles _AutoDelayCheckBox.CheckedChanged
        If Me._AutoDelayCheckBox.CheckState = CheckState.Checked Then
            Me.AutoDelayMode = MultimeterAutoDelayModes.On
            Me._AutoDelayCheckBox.Text = "Auto Delay ON"
        ElseIf Me._AutoDelayCheckBox.CheckState = CheckState.Indeterminate Then
            Me.AutoDelayMode = MultimeterAutoDelayModes.Once
            Me._AutoDelayCheckBox.Text = "Auto Delay ONCE"
        Else
            Me.AutoDelayMode = MultimeterAutoDelayModes.Off
            Me._AutoDelayCheckBox.Text = "Auto Delay OFF"
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

