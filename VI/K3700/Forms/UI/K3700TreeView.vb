Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> Keithley 3700 Device User Interface. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-30 </para></remarks>
<System.ComponentModel.DisplayName("K3700 User Interface"),
      System.ComponentModel.Description("Keithley 3700 Device User Interface"),
      System.Drawing.ToolboxBitmap(GetType(K3700TreeView))>
Public Class K3700TreeView
    Inherits isr.VI.Facade.VisaTreeView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        Me._ReadingView = ReadingView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Read", "Read", Me._ReadingView)
        Me._MultimeterView = MultimeterView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Meter", "Meter", Me._MultimeterView)
        Me._ChannelView = ChannelView.Create
        MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Channel", "Channel", Me._ChannelView)
        Me.InitializingComponents = False

    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As K3700Device)
        Me.New()
        Me.AssignDeviceThis(device)
    End Sub

#Region " VIEWS  "

    ''' <summary> Gets or sets the channel view. </summary>
    ''' <value> The channel view. </value>
    Private ReadOnly Property ChannelView As ChannelView

    ''' <summary> Gets or sets the reading view. </summary>
    ''' <value> The reading view. </value>
    Private ReadOnly Property ReadingView As ReadingView

    ''' <summary> Gets or sets the multimeter view. </summary>
    ''' <value> The multimeter view. </value>
    Private ReadOnly Property MultimeterView As MultimeterView

    ''' <summary> Gets or sets the resistances meter view. </summary>
    ''' <value> The resistances meter view. </value>
    Private ReadOnly Property ResistancesMeterView As ResistancesMeterView

    ''' <summary> Gets or sets the with resistances meters view. </summary>
    ''' <value> The with resistances meters view. </value>
    Public Property WithResistancesMetersView As Boolean
        Get
            Return Me.ResistancesMeterView IsNot Nothing
        End Get
        Set(value As Boolean)
            If value <> Me.WithResistancesMetersView Then
                ' TO_DO: Resistance meter device must not inherit the device but rather use it.
                Me._ResistancesMeterView = New ResistancesMeterView()
                MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Rs", "Rs", Me._ResistancesMeterView)
            End If
        End Set
    End Property

#End Region

    ''' <summary>
    ''' Releases the unmanaged resources used by the K3700 View and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                Me.AssignDeviceThis(Nothing)
                If Me.ChannelView IsNot Nothing Then Me.ChannelView.Dispose() : Me._ChannelView = Nothing
                If Me.MultimeterView IsNot Nothing Then Me.MultimeterView.Dispose() : Me._MultimeterView = Nothing
                If Me.ReadingView IsNot Nothing Then Me.ReadingView.Dispose() : Me._ReadingView = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K3700Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K3700Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Private Sub AssignDeviceThis(ByVal value As K3700Device)
        If Me._Device IsNot Nothing OrElse MyBase.VisaSessionBase IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = Nothing
            MyBase.StatusView.UserInterfaceSettings = Nothing
            Me._Device = Nothing
        End If
        Me._Device = value
        MyBase.BindVisaSessionBase(value)
        If value IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = isr.VI.Tsp.K3700.My.MySettings.Default
            MyBase.StatusView.UserInterfaceSettings = Nothing
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Public Overloads Sub AssignDevice(ByVal value As K3700Device)
        Me.AssignDeviceThis(value)
    End Sub

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Executes the device closing action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.OnDeviceClosing(e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            ' release the device before subsystems are disposed
            Me.ReadingView.AssignDevice(Nothing)
            Me.MultimeterView.AssignDevice(Nothing)
            Me.ChannelView.AssignDevice(Nothing)
        End If
    End Sub

    ''' <summary> Executes the device closed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub OnDeviceClosed()
        MyBase.OnDeviceClosed()
        ' remove binding after subsystems are disposed
        ' because the device closed the subsystems are null and binding will be removed.
        MyBase.DisplayView.BindMeasureToolStrip(Me.Device.MultimeterSubsystem)
        MyBase.DisplayView.BindSubsystemToolStrip(Me.Device.ChannelSubsystem)
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub OnDeviceOpened()
        MyBase.OnDeviceOpened()
        ' assigning device and subsystems after the subsystems are created
        Me.ReadingView.AssignDevice(Me.Device)
        Me.MultimeterView.AssignDevice(Me.Device)
        Me.ChannelView.AssignDevice(Me.Device)
        MyBase.DisplayView.BindMeasureToolStrip(Me.Device.MultimeterSubsystem)
        MyBase.DisplayView.BindSubsystemToolStrip(Me.Device.ChannelSubsystem)
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
