Imports System.ComponentModel

Partial Public Class MovingWindowMeter

    ''' <summary> Initializes the worker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub InitializeWorker()
        Me.Worker = New System.ComponentModel.BackgroundWorker() With {
            .WorkerSupportsCancellation = True,
            .WorkerReportsProgress = True
        }
    End Sub

    ''' <summary> Dispose worker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub DisposeWorker()
        If Me._Worker IsNot Nothing Then Me._Worker.Dispose() : Me._Worker = Nothing
    End Sub

    ''' <summary> Gets the measurement rate. </summary>
    ''' <value> The measurement rate. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MeasurementRate As Double = 25

    ''' <summary> Worker payload. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Class WorkerPayLoad

        ''' <summary> Gets the device. </summary>
        ''' <value> The device. </value>
        Public Property Device As K3700.K3700Device

        ''' <summary> Gets the moving window. </summary>
        ''' <value> The moving window. </value>
        Public Property MovingWindow As isr.Core.MovingFilters.MovingWindow

        ''' <summary> Gets the estimated count-out. </summary>
        ''' <value> The estimated count-out. </value>
        Public Property EstimatedCountout As Integer

        ''' <summary> Gets the number of do events. </summary>
        ''' <value> The number of do events. </value>
        Public Property DoEventCount As Integer = 10

        ''' <summary> Clears the known state. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Public Sub ClearKnownState()
            Me.MovingWindow.ClearKnownState()
        End Sub

        ''' <summary> Initializes the known state. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="measurementRate"> The measurement rate. </param>
        Public Sub InitializeKnownState(ByVal measurementRate As Double)
            Me.EstimatedCountout = CInt(measurementRate * Me.MovingWindow.TimeoutInterval.TotalSeconds)
        End Sub
    End Class

    ''' <summary> A user state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Class UserState

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Public Sub New()
            MyBase.New
            Me.Result = New TaskResult
        End Sub

        ''' <summary> Gets the result. </summary>
        ''' <value> The result. </value>
        Public Property Result As TaskResult

        ''' <summary> Gets the moving average. </summary>
        ''' <value> The moving average. </value>
        Public Property MovingAverage As isr.Core.MovingFilters.MovingWindow

        ''' <summary> Gets the estimated count-out. </summary>
        ''' <value> The estimated count-out. </value>
        Public Property EstimatedCountout As Integer

        ''' <summary> Gets the percent progress. </summary>
        ''' <value> The percent progress. </value>
        Public ReadOnly Property PercentProgress As Integer
            Get
                Dim baseCount As Integer = 0
                Dim count As Integer = CType(Me.MovingAverage?.TotalReadingsCount, Integer?).GetValueOrDefault(0)
                If count < 2 * Me.MovingAverage.Length Then
                    baseCount = 2 * Me.MovingAverage.Length
                ElseIf Me.EstimatedCountout > 0 Then
                    baseCount = Me.EstimatedCountout
                End If
                If baseCount > 0 Then
                    Return CInt(100 * count / baseCount)
                ElseIf Me.MovingAverage.TimeoutInterval > TimeSpan.Zero Then
                    Return CInt(100 * Me.MovingAverage.ElapsedMilliseconds / Me.MovingAverage.TimeoutInterval.TotalMilliseconds)
                Else
                    Return Me.LogPercentProgress
                End If
            End Get
        End Property

        ''' <summary> Gets the log percent progress. </summary>
        ''' <value> The log percent progress. </value>
        Public ReadOnly Property LogPercentProgress As Integer
            Get
                Dim count As Integer = CType(Me.MovingAverage?.TotalReadingsCount, Integer?).GetValueOrDefault(0)
                If Me.EstimatedCountout > 0 Then
                    Return CInt(100 * Math.Log(count) / Math.Log(Me.EstimatedCountout))
                ElseIf Me.MovingAverage.TimeoutInterval > TimeSpan.Zero Then
                    Return CInt(100 * Math.Log(Me.MovingAverage.ElapsedMilliseconds) / Math.Log(Me.MovingAverage.TimeoutInterval.TotalMilliseconds))
                Else
                    Return CInt(100 * count / Me.MovingAverage.Length)
                End If
            End Get
        End Property
    End Class

        Private WithEvents Worker As System.ComponentModel.BackgroundWorker

    ''' <summary> Worker do work. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Do work event information. </param>
    Private Sub Worker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles Worker.DoWork

        ' TO_DO: Add to view model. Me.ApplyCapturedSyncContext()
        Dim w As BackgroundWorker = TryCast(sender, BackgroundWorker)
        If w Is Nothing OrElse Me.IsDisposed OrElse e Is Nothing OrElse e.Cancel Then Return

        Dim userState As New UserState

        Dim payload As WorkerPayLoad = TryCast(e.Argument, WorkerPayLoad)
        If payload Is Nothing Then

            userState.Result.RegisterFailure("Payload Not assigned to worker")
            e.Result = userState.Result
            e.Cancel = True
            Return
        End If

        payload.MovingWindow.ClearKnownState()
        userState.MovingAverage = Me.MovingWindow
        userState.EstimatedCountout = payload.EstimatedCountout
        Do
            If payload.MovingWindow.ReadValue(Function() payload.Device.MultimeterSubsystem.MeasureReadingAmounts()) Then
                w.ReportProgress(userState.PercentProgress, userState)
            Else
                userState.Result.RegisterFailure("device returned a null value")
            End If
            e.Result = userState.Result
            e.Cancel = userState.Result.Failed
            If e IsNot Nothing AndAlso Not e.Cancel Then
                Dim eventCount As Integer = payload.DoEventCount
                Do While eventCount > 0
                    isr.Core.ApplianceBase.DoEvents()
                    eventCount -= 1
                Loop
            End If
        Loop Until w.CancellationPending OrElse e.Cancel OrElse payload.MovingWindow.IsStopStatus

        Do Until e.Cancel OrElse payload.MovingWindow.IsStopStatus
            Dim eventCount As Integer = payload.DoEventCount
            Do While eventCount > 0
                isr.Core.ApplianceBase.DoEvents()
                eventCount -= 1
            Loop
        Loop

    End Sub

    ''' <summary> Handles run worker completed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Private Sub OnWorkerRunWorkerCompleted(ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
        ' TO_DO: Add to ViewModel. Me.ApplyCapturedSyncContext()
        If e Is Nothing Then Return
        Dim result As TaskResult = TryCast(e.Result, TaskResult)
        If result Is Nothing Then Return
        If e.Cancelled AndAlso Not result.Failed Then
            result.RegisterFailure("Worker canceled")
        End If

        If e.Error IsNot Nothing AndAlso result.Exception Is Nothing Then
            result.RegisterFailure(e.Error, "Worker exception")
        End If
        Me.ProcessCompletion(result)
    End Sub

    ''' <summary> Worker run worker completed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Run worker completed event information. </param>
    Private Sub Worker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles Worker.RunWorkerCompleted
        Me.OnWorkerRunWorkerCompleted(e)
    End Sub

    ''' <summary> Worker progress changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Progress changed event information. </param>
    Private Sub Worker_ProgressChanged(sender As Object, e As ProgressChangedEventArgs) Handles Worker.ProgressChanged
        ' TO_DO: Add to view model Me.ApplyCapturedSyncContext()

        Dim us As UserState = TryCast(e.UserState, UserState)
        Dim ma As New isr.Core.MovingFilters.MovingWindow(TryCast(us.MovingAverage, isr.Core.MovingFilters.MovingWindow))
        Me.ReportProgressChanged(ma, us.PercentProgress, us.Result)
    End Sub

    ''' <summary> Stops measure asynchronous if. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function StopMeasureAsyncIf(ByVal timeout As TimeSpan) As Boolean
        Dim stopped As Boolean = Me.Worker Is Nothing OrElse Not Me.Worker.IsBusy
        If Not stopped Then
            ' wait for previous operation to complete.
            Dim sw As Stopwatch = Stopwatch.StartNew
            Me.PublishInfo("Waiting for worker to complete previous task")
            Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy OrElse sw.Elapsed > timeout
                isr.Core.ApplianceBase.DoEvents()
            Loop
            If Me.Worker.IsBusy Then
                Me.Worker.CancelAsync()
                Me.PublishInfo("Waiting for worker to cancel previous task")
                sw.Restart()
                Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy OrElse sw.Elapsed > timeout
                    isr.Core.ApplianceBase.DoEvents()
                Loop
            End If
            stopped = Not Me.Worker.IsBusy
            If Not stopped Then
                Me.PublishWarning("Failed when waiting for worker to complete previous task")
                Return False
            End If
        End If
        Return stopped
    End Function

    ''' <summary> Starts measure asynchronous. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function StartMeasureWork() As Boolean
        Me._MovingWindowTaskResult = New TaskResult
        Dim stopped As Boolean = Me.StopMeasureAsyncIf(TimeSpan.FromSeconds(1))
        If Not stopped Then Return False
        Me.InitializeWorker()
        Dim payload As New WorkerPayLoad With {
            .Device = Me.Device,
            .MovingWindow = Me.MovingWindow
        }

        payload.ClearKnownState()
        payload.InitializeKnownState(Me.MeasurementRate)

        If Not (Me.IsDisposed OrElse Me.Worker.IsBusy) Then
            Me.Worker.RunWorkerAsync(payload)
            Me.NotifyTaskStart()
            ' Me.MeasurementStarted = True
        Else
            Me.ClearTaskStart()
            ' Me.MeasurementStarted = False
        End If
        Return Me.TaskStart <> NotificationSemaphores.None '  Me.MeasurementStarted
    End Function

    ''' <summary> Starts a measure. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    Public Sub StartMeasureWorkSync()
        If Me.StartMeasureWork() Then
            ' wait for worker to get busy.
            Do While Not (Me.IsDisposed OrElse Me.Worker.IsBusy)
                isr.Core.ApplianceBase.DoEvents()
            Loop
            ' wait till worker is done
            Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy
                isr.Core.ApplianceBase.DoEvents()
            Loop
        End If
    End Sub

End Class

