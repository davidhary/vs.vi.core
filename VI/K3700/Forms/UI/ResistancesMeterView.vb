Imports System.ComponentModel
Imports System.Windows.Forms

Imports isr.VI.ExceptionExtensions

''' <summary> A meter control. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-09 </para>
''' </remarks>
Public Class ResistancesMeterView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(ResistancesMeterDevice.Create, True)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device">        The device. </param>
    ''' <param name="isDeviceOwner"> True if is device owner, false if not. </param>
    Public Sub New(ByVal device As ResistancesMeterDevice, ByVal isDeviceOwner As Boolean)
        MyBase.New()
        Me.NewThis(device, isDeviceOwner)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device">        The device. </param>
    ''' <param name="isDeviceOwner"> True if is device owner, false if not. </param>
    Private Sub NewThis(ByVal device As ResistancesMeterDevice, ByVal isDeviceOwner As Boolean)
        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._BottomToolStrip.Renderer = New CustomProfessionalRenderer
        Me.AssignDeviceThis(device, isDeviceOwner)

    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.VI.Instrument.ResourcePanelBase and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing, Me.IsDeviceOwner)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
            Me._BottomToolStrip.Renderer = New CustomProfessionalRenderer
            Me._BottomToolStrip.Invalidate()
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As ResistancesMeterDevice

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As ResistancesMeterDevice
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Gets or sets the is device that owns this item. </summary>
    ''' <value> The is device owner. </value>
    Private Property IsDeviceOwner As Boolean

    ''' <summary> Assign device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">         The value. </param>
    ''' <param name="isDeviceOwner"> True if is device owner, false if not. </param>
    Private Sub AssignDeviceThis(ByVal value As ResistancesMeterDevice, ByVal isDeviceOwner As Boolean)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            If Me.IsDeviceOwner Then Me._Device?.Dispose()
            Me._Device = Nothing
        End If
        Me._Device = value
        Me.IsDeviceOwner = isDeviceOwner
        If value IsNot Nothing Then
            ' If Me.Device.ChannelSubsystem IsNot Nothing Then AddHandler Me.Device.ChannelSubsystem.PropertyChanged, AddressOf Me.ChannelSubsystemPropertyChanged
            Me.AssignTalker(Me.Device.Talker)

            ' TO_DO: Bind resistors to data grid
            ' Me.Device.Resistors?.DisplayValues(Me._DataGrid)

            ' TO_D: Bind measurement enabled to 
            ' Me._MeasureGroupBox.Enabled = sender.MeasurementEnabled
            ' Me._MeasureGroupBox.Enabled = Me.Device.IsDeviceOpen AndAlso Me.Device.MultimeterSubsystem.FunctionMode.HasValue AndAlso Me.Device.MultimeterSubsystem.FunctionMode.Value = MultimeterFunctionMode.ResistanceFourWire

            ' TO_DO: Add to the 3700 an option to add the resistance view
            ' Use the 3700 Multimeter tab to set the filtering.
            ' Include in the K3700 device as an

            ' bind resistance title, resistance value, closed channels. 

        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">         True to show or False to hide the control. </param>
    ''' <param name="isDeviceOwner"> True if is device owner, false if not. </param>
    Public Overloads Sub AssignDevice(ByVal value As ResistancesMeterDevice, ByVal isDeviceOwner As Boolean)
        Me.AssignDeviceThis(value, isDeviceOwner)
    End Sub

#End Region

#Region " CONTROL EVENTS "

    ''' <summary> Measure button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeasureButton_Click(sender As Object, e As EventArgs) Handles _MeasureButton.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim args As New isr.Core.ActionEventArgs
            If Me.Device.IsMeasurementEnabled Then
                Dim activity As String = $"{Me.Device.ResourceNameCaption} measuring resistances"
                Me.Device.TryMeasureResistors()
            Else
                Me.PublishInfo("measurement not enabled; configure first")
            End If
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TOOL STRIP RENDERER "

    ''' <summary> A custom professional renderer. </summary>
    ''' <remarks>
    ''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2017-10-30 </para>
    ''' </remarks>
    Private Class CustomProfessionalRenderer
        Inherits ToolStripProfessionalRenderer

        ''' <summary>
        ''' Raises the <see cref="E:System.Windows.Forms.ToolStripRenderer.RenderLabelBackground" />
        ''' event.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="e"> A <see cref="T:System.Windows.Forms.ToolStripItemRenderEventArgs" /> that
        '''                  contains the event data. </param>
        Protected Overrides Sub OnRenderLabelBackground(ByVal e As ToolStripItemRenderEventArgs)
            If e IsNot Nothing AndAlso (e.Item.BackColor <> System.Drawing.SystemColors.ControlDark) Then
                Using brush As New Drawing.SolidBrush(e.Item.BackColor)
                    e.Graphics.FillRectangle(brush, e.Item.ContentRectangle)
                End Using
            End If
        End Sub
    End Class

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
