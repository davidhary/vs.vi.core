Imports System.ComponentModel
Imports System.Threading
Imports System.Windows.Forms
Imports isr.Core.Primitives
Imports isr.Core.Forma
Imports isr.Core.WinForms.ProgressBarExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.Core.WinForms.ControlExtensions

''' <summary> A moving window averaging meter. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-01-30 </para>
''' </remarks>
Public Class MovingWindowMeter
    Inherits ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> The default moving window length. </summary>
    Private Const _DefaultMovingWindowLength As Integer = 40

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()

        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._MeasurementFormatString = "G8"
        Me._MovingWindow = New isr.Core.MovingFilters.MovingWindow(MovingWindowMeter._DefaultMovingWindowLength)

        Me._ExpectedStopTimeout = TimeSpan.FromMilliseconds(100)

        ' set very long in case operator starts browsing the application
        Me._TaskCompleteNotificationTimeout = TimeSpan.FromSeconds(10)

        ' set very long in case operator starts browsing the application
        Me._TaskStartNotificationTimeout = TimeSpan.FromSeconds(10)

        Me._TaskStopTimeout = TimeSpan.FromSeconds(1)
    End Sub

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    If Me._MovingWindow IsNot Nothing Then
                        Me.MovingWindow.ClearKnownState()
                        Me._MovingWindow = Nothing
                    End If
                    If Me._Task IsNot Nothing Then Me._Task.Dispose() : Me._Task = Nothing
                    ' release the device.
                    If Me._Device IsNot Nothing Then Me._Device = Nothing
                    If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
                End If
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS  "

    ''' <summary> Moving window meter load. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MovingWindowMeter_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me._StartMovingWindowButton.Enabled = False
    End Sub

#End Region

#Region " MOVING WINDOW AVERAGE "

    ''' <summary> Device open changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Device_OpenChanged(sender As Object, e As EventArgs) Handles _Device.Opened, _Device.Closed
        Me._StartMovingWindowButton.Enabled = Me._Device.IsDeviceOpen
    End Sub

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _Device As K3700.K3700Device
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Device As K3700.K3700Device
        Get
            Return Me._Device
        End Get
        Set(value As K3700.K3700Device)
            Me._Device = value
            If value IsNot Nothing Then
                Me._StartMovingWindowButton.Enabled = value.IsDeviceOpen
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the moving window. </summary>
    ''' <value> The moving window. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MovingWindow As isr.Core.MovingFilters.MovingWindow

    ''' <summary> Gets or sets the length. </summary>
    ''' <value> The length. </value>
    Public Property Length As Integer
        Get
            Return Me.MovingWindow.Length
        End Get
        Set(value As Integer)
            If Length <> value Then
                Me.MovingWindow.Length = value
                Me.MovingWindow.ClearKnownState()
            End If
            Me._LengthTextBox.Text = value.ToString
        End Set
    End Property

    ''' <summary> Gets or sets the timeout interval. </summary>
    ''' <value> The timeout interval. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property TimeoutInterval As TimeSpan
        Get
            Return Me.MovingWindow.TimeoutInterval
        End Get
        Set(value As TimeSpan)
            Me.MovingWindow.TimeoutInterval = value
            Me._TimeoutTextBox.Text = value.TotalSeconds.ToString
        End Set
    End Property

    ''' <summary> Gets or sets the update rule. </summary>
    ''' <value> The update rule. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property UpdateRule As isr.Core.MovingFilters.MovingWindowUpdateRule
        Get
            Return Me.MovingWindow.UpdateRule
        End Get
        Set(value As isr.Core.MovingFilters.MovingWindowUpdateRule)
            Me.MovingWindow.UpdateRule = value
        End Set
    End Property

    ''' <summary> Gets or sets the window. </summary>
    ''' <value> The window. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Window As Core.MovingFilters.FilterWindow(Of Double)
        Get
            Return Me.MovingWindow.RelativeWindow
        End Get
        Set(value As Core.MovingFilters.FilterWindow(Of Double))
            If value <> Me.Window Then
                Me.MovingWindow.RelativeWindow = New Core.MovingFilters.FilterWindow(Of Double)(value)
                Me._WindowTextBox.Text = $"{(100 * (value.Max - value.Min)):0.####}"
            End If
        End Set
    End Property

    ''' <summary> The reading timespan. </summary>
    Private _ReadingTimespan As TimeSpan

    ''' <summary> Gets or sets the reading timespan. </summary>
    ''' <value> The reading timespan. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ReadingTimespan As TimeSpan
        Get
            Return Me._ReadingTimespan
        End Get
        Set(value As TimeSpan)
            If value <> Me.ReadingTimespan Then
                Me._ReadingTimespan = value
                Me.UpdateReadingTimespanCaption(value)
            End If
        End Set
    End Property

    ''' <summary> Updates the reading timespan caption described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub UpdateReadingTimespanCaption(ByVal value As TimeSpan)
        Dim caption As String = $"{value.TotalMilliseconds:0}"
        If Not String.Equals(caption, Me._ReadingTimeSpanLabel.Text) Then Me._ReadingTimeSpanLabel.Text = caption
    End Sub

    ''' <summary> Gets the has moving window task result. </summary>
    ''' <value> The has moving window task result. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property HasMovingWindowTaskResult As Boolean
        Get
            Return Me.MovingWindowTaskResult IsNot Nothing
        End Get
    End Property

    ''' <summary> The measurement format string. </summary>
    Private _MeasurementFormatString As String

    ''' <summary> Gets or sets the measurement Format String. </summary>
    ''' <value> The measurement FormatString. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MeasurementFormatString As String
        Get
            Return Me._MeasurementFormatString
        End Get
        Protected Set(value As String)
            Me._MeasurementFormatString = value
            ' Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets the moving window measurement failed. </summary>
    ''' <value> The moving window measurement failed. </value>
    Public ReadOnly Property MovingWindowMeasurementFailed As Boolean
        Get
            Return Me.MovingWindow.Status <> Core.MovingFilters.MovingWindowStatus.Within
        End Get
    End Property

#End Region

#Region " MOVING WINDOW TASK "

#Region " TASK FAILURE INFO "

    ''' <summary> Gets the sentinel indicating if the moving window average task failed. </summary>
    ''' <value> The sentinel indicating if the moving window task failed. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MovingWindowTaskFailed As Boolean
        Get
            Return Me.HasMovingWindowTaskResult AndAlso Me.MovingWindowTaskResult.Failed
        End Get
    End Property

    ''' <summary> Gets the task failure details. </summary>
    ''' <value> The failure details. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MovingWindowTaskFailureDetails As String
        Get
            Return If(Me.MovingWindowTaskFailed,
                If(Me.MovingWindowTaskResult.Exception IsNot Nothing,
                    Me.MovingWindowTaskResult.Exception.ToFullBlownString,
                    Me.MovingWindowTaskResult.Details),
                String.Empty)
        End Get
    End Property

    ''' <summary> Gets the failure exception. </summary>
    ''' <value> The failure exception. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MovingWindowTaskFailureException As Exception
        Get
            Return Me.MovingWindowTaskResult.Exception
        End Get
    End Property

#End Region

#Region " TASK COMPLETE "

    ''' <summary> Gets or sets the task complete notification timeout. </summary>
    ''' <remarks>
    ''' Defaults to 10 seconds to allow for user interface browsing while collecting data.
    ''' </remarks>
    ''' <value> The task complete notification timeout. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property TaskCompleteNotificationTimeout As TimeSpan

    ''' <summary> Clears the task complete semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    Private Sub ClearTaskComplete()
        Me._TaskComplete = NotificationSemaphores.None
        ' TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3700.MovingWindowMeter.TaskComplete))
    End Sub

    ''' <summary> Set the task complete semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub NotifyTaskComplete()
        Me._TaskComplete = NotificationSemaphores.Sent
        ' TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3700.MovingWindowMeter.TaskComplete))
    End Sub

    ''' <summary> Set the task complete semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Private Function NotifyTaskComplete(timeout As TimeSpan) As Boolean
        Dim sw As Stopwatch = Stopwatch.StartNew
        Me.NotifyTaskComplete()
        Do
            isr.Core.ApplianceBase.DoEvents()
        Loop Until ((Me.TaskComplete And NotificationSemaphores.Acknowledged) <> 0) OrElse sw.Elapsed > timeout
        Return (Me.TaskComplete And NotificationSemaphores.Acknowledged) <> 0
    End Function

    ''' <summary> Acknowledge task complete semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub AcknowledgeTaskComplete()
        Me._TaskComplete = Me.TaskComplete Or NotificationSemaphores.Acknowledged
    End Sub

    ''' <summary> Gets or sets the task complete semaphore. </summary>
    ''' <value> The measurement Completed. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TaskComplete As NotificationSemaphores

#End Region

#Region " TASK START "

    ''' <summary> Gets or sets the task start notification timeout. </summary>
    ''' <remarks>
    ''' Defaults to 10 seconds to allow for user interface browsing while collecting data.
    ''' </remarks>
    ''' <value> The task start notification timeout. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property TaskStartNotificationTimeout As TimeSpan

    ''' <summary> Clears the task Start semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ClearTaskStart()
        Me._TaskStart = NotificationSemaphores.None
        ' TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3700.MovingWindowMeter.TaskStart))
    End Sub

    ''' <summary> Set the task Start semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub NotifyTaskStart()
        Me._TaskStart = NotificationSemaphores.Sent
        ' TO_DO: Move to Moving WIndow Meter Model     Me.NotifyPropertyChanged(NameOf(K3700.MovingWindowMeter.TaskStart))
    End Sub

    ''' <summary> Set the task Start semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Private Function NotifyTaskStart(timeout As TimeSpan) As Boolean
        Dim sw As Stopwatch = Stopwatch.StartNew
        Me.NotifyTaskStart()
        Do
            isr.Core.ApplianceBase.DoEvents()
        Loop Until ((Me.TaskStart And NotificationSemaphores.Acknowledged) <> 0) OrElse sw.Elapsed > timeout
        Return (Me.TaskStart And NotificationSemaphores.Acknowledged) <> 0
    End Function

    ''' <summary> Acknowledge task Start semaphore. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub AcknowledgeTaskStart()
        Me._TaskStart = Me.TaskStart Or NotificationSemaphores.Acknowledged
    End Sub

    ''' <summary> Gets or sets the task Start semaphore. </summary>
    ''' <value> The measurement Started. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TaskStart As NotificationSemaphores

#End Region

#End Region

#Region " PROGRESS REPORTING "

    ''' <summary> Number of. </summary>
    Private _Count As Integer

    ''' <summary> Gets or sets the number of. </summary>
    ''' <value> The count. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Count As Integer
        Get
            Return Me._Count
        End Get
        Protected Set(value As Integer)
            If Me.Count <> value Then
                Me._Count = value
                Me._CountLabel.Text = value.ToString
            End If
        End Set
    End Property

    ''' <summary> Number of readings. </summary>
    Private _ReadingsCount As Integer

    ''' <summary> Gets or sets the number of readings. </summary>
    ''' <value> The number of readings. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ReadingsCount As Integer
        Get
            Return Me._ReadingsCount
        End Get
        Protected Set(value As Integer)
            If Me.ReadingsCount <> value Then
                Me._ReadingsCount = value
                Me._ReadingsCountLabel.Text = value.ToString
            End If
        End Set
    End Property

    ''' <summary> The percent progress. </summary>
    Private _PercentProgress As Integer

    ''' <summary> Gets or sets the percent progress. </summary>
    ''' <value> The percent progress. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property PercentProgress As Integer
        Get
            Return Me._PercentProgress
        End Get
        Protected Set(value As Integer)
            If Me.PercentProgress <> value Then
                Me._PercentProgress = value
                Me._TaskProgressBar.SafelyInvoke(Sub() Me._TaskProgressBar.Value = value)
            End If
        End Set
    End Property

    ''' <summary> The elapsed time. </summary>
    Private _ElapsedTime As TimeSpan

    ''' <summary> Gets or sets the elapsed time. </summary>
    ''' <value> The elapsed time. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ElapsedTime As TimeSpan
        Get
            Return Me._ElapsedTime
        End Get
        Protected Set(value As TimeSpan)
            If Me.ElapsedTime <> value Then
                Me._ElapsedTime = value
                Me._ElapsedTimeLabel.Text = value.ToString("mm\:ss\.ff", Globalization.CultureInfo.CurrentCulture)
            End If
        End Set
    End Property

    ''' <summary> The maximum reading. </summary>
    Private _MaximumReading As Double

    ''' <summary> Gets or sets the maximum reading. </summary>
    ''' <value> The maximum reading. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MaximumReading As Double
        Get
            Return Me._MaximumReading
        End Get
        Protected Set(value As Double)
            If Me.MaximumReading <> value Then
                Me._MaximumReading = value
                Me._MaximumLabel.Text = value.ToString(Me.MeasurementFormatString)
            End If
        End Set
    End Property

    ''' <summary> The minimum reading. </summary>
    Private _MinimumReading As Double

    ''' <summary> Gets or sets the minimum reading. </summary>
    ''' <value> The minimum reading. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MinimumReading As Double
        Get
            Return Me._MinimumReading
        End Get
        Protected Set(value As Double)
            If Me.MinimumReading <> value Then
                Me._MinimumReading = value
                Me._MinimumLabel.Text = value.ToString(Me.MeasurementFormatString)
            End If
        End Set
    End Property

    ''' <summary> The reading. </summary>
    Private _Reading As Double

    ''' <summary> Gets or sets the reading. </summary>
    ''' <value> The reading. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property Reading As Double
        Get
            Return Me._Reading
        End Get
        Protected Set(value As Double)
            If value <> Me.Reading Then
                Me._Reading = value
                Me._AverageLabel.Text = value.ToString(Me.MeasurementFormatString)
            End If
        End Set
    End Property

    ''' <summary> The last reading. </summary>
    Private _LastReading As Double?

    ''' <summary> Gets or sets the last reading. </summary>
    ''' <value> The last reading. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property LastReading As Double?
        Get
            Return Me._LastReading
        End Get
        Protected Set(value As Double?)
            If Not Nullable.Equals(value, Me.LastReading) Then
                Me._LastReading = value
                If value.HasValue Then Me._ReadingLabel.Text = value.Value.ToString(Me.MeasurementFormatString)
            End If
        End Set
    End Property

    ''' <summary> The reading status. </summary>
    Private _ReadingStatus As Core.MovingFilters.MovingWindowStatus

    ''' <summary> Gets or sets the reading status. </summary>
    ''' <value> The reading status. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property ReadingStatus As Core.MovingFilters.MovingWindowStatus
        Get
            Return Me._ReadingStatus
        End Get
        Protected Set(value As Core.MovingFilters.MovingWindowStatus)
            If Me.ReadingStatus <> value Then
                Me._ReadingStatus = value
                Dim v As String = Core.MovingFilters.MovingWindow.StatusAnnunciationCaption(value)
                Me._StatusLabel.Text = isr.Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact(v, Me._StatusLabel)
                Me.ToolTip.SetToolTip(Me._StatusLabel, v)
            End If
        End Set
    End Property

    ''' <summary> Reports progress changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="movingWindow">    The moving window. </param>
    ''' <param name="percentProgress"> The percent progress. </param>
    ''' <param name="result">          The result. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReportProgressChanged(ByVal movingWindow As isr.Core.MovingFilters.MovingWindow, ByVal percentProgress As Integer, ByVal result As TaskResult)
        If Me.InvokeRequired Then
            Me.Invoke(New Action(Of isr.Core.MovingFilters.MovingWindow, Integer, TaskResult)(AddressOf Me.ReportProgressChanged), New Object() {movingWindow, percentProgress, result})
        Else
            Dim ma As isr.Core.MovingFilters.MovingWindow = movingWindow
            If ma IsNot Nothing AndAlso ma.TotalReadingsCount > 0 Then
                Try
                    Dim value As Double = 0
                    Dim hasReading As Boolean = Core.MovingFilters.MovingWindow.HasReading(ma.Status)
                    If hasReading Then value = ma.Mean
                    Me.Window = ma.RelativeWindow
                    Me.ReadingTimespan = ma.ReadingTimespan
                    Me.PercentProgress = percentProgress
                    Me.ElapsedTime = ma.ElapsedTime
                    Me.Count = ma.Count
                    Me.ReadingsCount = ma.TotalReadingsCount
                    Me.MaximumReading = ma.Maximum
                    Me.MinimumReading = ma.Minimum
                    Me.LastReading = ma.LastAmenableReading.Value
                    Me.ReadingStatus = ma.Status
                    If hasReading Then Me.Reading = value
                    ' this helps flash out exceptions:
                    isr.Core.ApplianceBase.DoEvents()
                Catch ex As Exception
                    result.RegisterFailure(ex, "exception reporting progress")
                End Try
            End If
        End If
    End Sub

    ''' <summary> Reports progress changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="movingWindow"> The moving window. </param>
    Private Sub ReportProgressChanged(ByVal movingWindow As isr.Core.MovingFilters.MovingWindow)
        Me.ReportProgressChanged(movingWindow, movingWindow.PercentProgress, Me.MovingWindowTaskResult)
    End Sub

    ''' <summary> Process the completion described by result. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="result"> The result. </param>
    Private Sub ProcessCompletion(ByVal result As TaskResult)
        If result Is Nothing Then
            Me._MovingWindowTaskResult = New TaskResult
            Me.MovingWindowTaskResult.RegisterFailure("Unexpected null task result when completing the task;. Contact the developer")
        Else
            Me._MovingWindowTaskResult = result
        End If
        If Not Me.NotifyTaskComplete(Me.TaskCompleteNotificationTimeout) Then
            If Me.MovingWindowTaskResult.Failed Then
                Me.MovingWindowTaskResult.RegisterFailure($"{Me.MovingWindowTaskResult.Details}; also, timeout receiving completion acknowledgment")
            Else
                Me.MovingWindowTaskResult.RegisterFailure("Timeout receiving completion acknowledgment")
            End If
        End If
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Gets the moving window task result. </summary>
    ''' <value> The moving window task result. </value>
    Private ReadOnly Property MovingWindowTaskResult As New TaskResult

    ''' <summary> Measure moving window average. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="progress"> The progress reporter. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureMovingWindowAverage(progress As IProgress(Of isr.Core.MovingFilters.MovingWindow))
        Me._MovingWindowTaskResult = New TaskResult
        Try
            ' TO_DO: Move to Moving WIndow Meter Model Me.ApplyCapturedSyncContext()
            Me.MovingWindow.ClearKnownState()
            If Me.NotifyTaskStart(Me.TaskStartNotificationTimeout) Then
                Do
                    ' measure and time
                    If Me.MovingWindow.ReadValue(Function() Me.Device.MultimeterSubsystem.MeasureReadingAmounts()) Then
                        progress.Report(New isr.Core.MovingFilters.MovingWindow(Me.MovingWindow))
                    Else
                        Me.MovingWindowTaskResult.RegisterFailure("device returned a null value")
                    End If
                Loop Until Me.IsCancellationRequested OrElse Me.MovingWindowTaskResult.Failed OrElse Me.MovingWindow.IsStopStatus
            Else
                Me.MovingWindowTaskResult.RegisterFailure("Timeout receiving start acknowledgment")
            End If
        Catch ex As Exception
            Me.MovingWindowTaskResult.RegisterFailure(ex)
        Finally
            Me.ProcessCompletion(Me.MovingWindowTaskResult)
        End Try
    End Sub

#End Region

#Region " ASYNC TASK "

    ''' <summary> The cancellation token source. </summary>
    ''' <value> The cancellation token source. </value>
    Private Property CancellationTokenSource As CancellationTokenSource

    ''' <summary> The cancellation token. </summary>
    ''' <value> The cancellation token. </value>
    Private Property CancellationToken As CancellationToken

    ''' <summary> Requests the state machine to immediately stop operation. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub RequestCancellation()
        If Not Me.IsCancellationRequested Then
            ' stops the wait.
            Me.CancellationTokenSource.Cancel()
            ' TO_DO: Move to Moving WIndow Meter Model Me.NotifyPropertyChanged(NameOf(K3700.MovingWindowMeter.IsCancellationRequested))
        End If

    End Sub

    ''' <summary> Query if cancellation requested. </summary>
    ''' <value> <c>true</c> if cancellation requested; otherwise <c>false</c> </value>
    Public ReadOnly Property IsCancellationRequested() As Boolean
        Get
            Return Me.CancellationToken.IsCancellationRequested
        End Get
    End Property

    ''' <summary> Gets the sentinel indicating if the meter is running. </summary>
    ''' <value> The sentinel indicating if the meter is running. </value>
    Public ReadOnly Property IsRunning As Boolean
        Get
            Return Me.Task IsNot Nothing AndAlso Me.Task.Status = TaskStatus.Running
        End Get
    End Property

    ''' <summary> Gets the is stopped. </summary>
    ''' <value> The is stopped. </value>
    Public ReadOnly Property IsStopped As Boolean
        Get
            Return Me.Task Is Nothing OrElse Me.Task.Status <> TaskStatus.Running
        End Get
    End Property

    ''' <summary> Gets the expected stop timeout. </summary>
    ''' <value> The expected stop timeout. </value>
    Public Property ExpectedStopTimeout As TimeSpan

    ''' <summary> Stops measure asynchronous if. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function StopAsyncTask() As Boolean
        Return Me.StopAsyncTask(Me.ExpectedStopTimeout)
    End Function

    ''' <summary> Stops measure asynchronous if. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function StopAsyncTask(ByVal timeout As TimeSpan) As Boolean
        If Not Me.IsStopped AndAlso Not Me.IsCancellationRequested Then
            ' wait for previous operation to complete.
            Dim sw As Stopwatch = Stopwatch.StartNew
            Me.PublishInfo("Waiting for previous task to complete")
            Do Until Me.IsStopped OrElse sw.Elapsed > timeout
                isr.Core.ApplianceBase.DoEvents()
            Loop
            If Not Me.IsStopped AndAlso Not Me.IsCancellationRequested Then
                Me.PublishInfo("Requesting cancellation of previous tasks")
                Me.RequestCancellation()
                Me.PublishInfo("Waiting for previous task to stop")
                sw.Restart()
                Do Until Me.IsStopped OrElse sw.Elapsed > timeout
                    isr.Core.ApplianceBase.DoEvents()
                Loop

            End If
        End If
        If Not Me.IsStopped Then Me.PublishWarning("Attempt to stop previous task failed")
        Return Me.IsStopped
    End Function

    ''' <summary> The task. </summary>
    Private _Task As Task

    ''' <summary> Gets the task. </summary>
    ''' <value> The task. </value>
    Protected ReadOnly Property Task As Task
        Get
            Return Me._Task
        End Get
    End Property

    ''' <summary> Gets or sets the task stopping timeout. </summary>
    ''' <value> The task stopping timeout. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property TaskStopTimeout As TimeSpan

    ''' <summary> Activates the machine asynchronous task. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="runSynchronously"> True to run synchronously. </param>
    Public Sub StartMeasureTask(ByVal runSynchronously As Boolean)
        ' TO_DO: Move to Moving WIndow Meter Model Me.CaptureSyncContext(syncContext)
        Me.StopAsyncTask(Me.TaskStopTimeout)
        ' proceed even if not stopped
        Me._TaskStart = NotificationSemaphores.None
        Me.CancellationTokenSource = New CancellationTokenSource
        Me.CancellationToken = Me.CancellationTokenSource.Token
        Dim progress As New Progress(Of Core.MovingFilters.MovingWindow)(AddressOf Me.ReportProgressChanged)
        Me._Task = New Task(Sub() Me.MeasureMovingWindowAverage(progress))
        If runSynchronously Then
            Me.Task.RunSynchronously(TaskScheduler.FromCurrentSynchronizationContext)
        Else
            Me.Task.Start()
        End If
    End Sub

    ''' <summary> Starts measure asynchronous. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    Public Sub StartMeasureAsync()
        Me.StartMeasureTask(False)
    End Sub

    ''' <summary> Activates the machine asynchronous task. </summary>
    ''' <remarks>
    ''' This was suspected to cause issues. The synchronization context is captured as part of the
    ''' property change and other event handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="runSynchronously"> True to run synchronously. </param>
    Public Sub StartMeasureTaskStopRequired(ByVal runSynchronously As Boolean)
        If Me.StopAsyncTask(Me.TaskStopTimeout) Then
            Me._TaskStart = NotificationSemaphores.None
            Me.CancellationTokenSource = New CancellationTokenSource
            Me.CancellationToken = Me.CancellationTokenSource.Token
            Dim progress As New Progress(Of Core.MovingFilters.MovingWindow)(AddressOf Me.ReportProgressChanged)
            Me._Task = New Task(Sub() Me.MeasureMovingWindowAverage(progress))
            If runSynchronously Then
                Me.Task.RunSynchronously(TaskScheduler.FromCurrentSynchronizationContext)
            Else
                Me.Task.Start()
            End If
        Else
            Me._TaskStart = NotificationSemaphores.None
            'Me.MeasurementStarted = False
        End If
    End Sub

    ''' <summary> Starts measure task stop required asynchronous. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    Public Sub StartMeasureTaskStopRequiredAsync()
        Me.StartMeasureTaskStopRequired(False)
    End Sub

    ''' <summary> Asynchronous task. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> An asynchronous result. </returns>
    Public Async Function AsyncTask() As Task
        Dim progress As New Progress(Of Core.MovingFilters.MovingWindow)(AddressOf Me.ReportProgressChanged)
        Await Task.Run(Sub() Me.MeasureMovingWindowAverage(progress))
    End Function

#End Region

#Region " START STOP "

    ''' <summary> Starts moving window button check state changed. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartMovingWindowButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _StartMovingWindowButton.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim button As ToolStripButton = TryCast(sender, ToolStripButton)
        If button Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If button.Checked Then
                Me.MovingWindow.Length = CInt(Me._LengthTextBox.Text)
                Dim span As Double = 0.01 * CDbl(Me._WindowTextBox.Text)
                Me.MovingWindow.RelativeWindow = New Core.MovingFilters.FilterWindow(Of Double)(-0.5 * span, 0.5 * span)
                Me.MovingWindow.UpdateRule = Core.MovingFilters.MovingWindowUpdateRule.StopOnWithinWindow
                Me.MovingWindow.TimeoutInterval = TimeSpan.FromSeconds(CDbl(Me._TimeoutTextBox.Text))
                activity = $"{Me.Device.ResourceNameCaption} starting the moving window task"
                Me.StartMeasureAsync()
                If Not Me.IsRunning Then
                    Me.PublishWarning($"Failed {activity}")
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} stopping the moving window task"
                Me.StopAsyncTask(Me.TaskStopTimeout)
                If Not Me.IsStopped Then
                    Me.PublishWarning($"Failed {activity}")
                End If
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            button.Text = $"{button.Checked.GetHashCode:'Stop';'Stop';'Start'}"
        End Try
    End Sub

#End Region

#Region " TASK RESULT "

    ''' <summary> Encapsulates the result of a task. </summary>
    ''' <remarks>
    ''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 2016-09-24 </para>
    ''' </remarks>
    Private Class TaskResult

        ''' <summary> Registers the failure described by exception. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="details"> The details. </param>
        Public Sub RegisterFailure(ByVal details As String)
            Me._Failed = True
            Me._Details = details
        End Sub

        ''' <summary> Registers the failure described by exception. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="exception"> The exception. </param>
        Public Sub RegisterFailure(ByVal exception As Exception)
            Me.RegisterFailure(exception, "Exception occurred")
        End Sub

        ''' <summary> Registers the failure described by exception. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <param name="details">   The details. </param>
        Public Sub RegisterFailure(ByVal exception As Exception, ByVal details As String)
            Me._Failed = True
            Me._Details = details
            Me._Exception = exception
        End Sub

        ''' <summary> Gets or sets the failed sentinel. </summary>
        ''' <value> The failed sentinel. </value>
        Public ReadOnly Property Failed As Boolean

        ''' <summary> Gets or sets the failure details. </summary>
        ''' <value> The details. </value>
        Public ReadOnly Property Details As String

        ''' <summary> Gets or sets the exception. </summary>
        ''' <value> The exception. </value>
        Public ReadOnly Property Exception As Exception

    End Class

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

''' <summary> Values that represent notification semaphores. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<Flags>
Public Enum NotificationSemaphores

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not set")>
    None = 0

    ''' <summary> An enum constant representing the sent option. </summary>
    <Description("Notification Sent")>
    Sent = 1

    ''' <summary> An enum constant representing the acknowledged option. </summary>
    <Description("Notification Acknowledged")>
    Acknowledged = 2
End Enum

