''' <summary> Resistances meter unit tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-15 </para>
''' </remarks>
<TestClass(), TestCategory("k3700")>
Public Class ResistancesMeterTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
            ResistancesMeterTests._ResistancesMeterDevice = VI.Tsp.K3700.ResistancesMeterDevice.Create
            ResistancesMeterTests._ResistancesMeter = New VI.Tsp.K3700.Forms.ResistancesMeterView
            ResistancesMeterTests._ResistancesMeterDevice.AddListener(TestInfo.TraceMessagesQueueListener)
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
        If ResistancesMeterTests._ResistancesMeter IsNot Nothing Then ResistancesMeterTests._ResistancesMeter.Dispose() : ResistancesMeterTests._ResistancesMeter = Nothing
        If ResistancesMeterTests._ResistancesMeterDevice IsNot Nothing Then ResistancesMeterTests._ResistancesMeterDevice.Dispose() : ResistancesMeterTests._ResistancesMeterDevice = Nothing
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")

        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(ResourceSettings)} settings should exist")
        Assert.IsTrue(ResistancesMeterSettings.Get.Exists, $"{GetType(ResistancesMeterSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SHARED DEVICE "

    ''' <summary> The resistances meter. </summary>
    Private Shared _ResistancesMeter As VI.Tsp.K3700.Forms.ResistancesMeterView

    ''' <summary> The resistances meter device. </summary>
    Private Shared _ResistancesMeterDevice As VI.Tsp.K3700.ResistancesMeterDevice

    ''' <summary> Opens a session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="trialNumber"> The trial number. </param>
    ''' <param name="device">      The resistance meter device. </param>
    Friend Shared Sub OpenSession(ByVal trialNumber As Integer, ByVal device As VI.Tsp.K3700.ResistancesMeterDevice)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Dim r As (Success As Boolean, Details As String) = device.TryOpenSession(ResourceSettings.Get.ResourceName, ResourceSettings.Get.ResourceTitle)
        Assert.IsTrue(r.Success, $"{trialNumber} session should open; { r.Details}")
        Assert.IsTrue(device.IsDeviceOpen, $"{trialNumber} session to device {device.ResourceNameCaption} should be open")

        ' check the MODEL
        Assert.AreEqual(ResourceSettings.Get.ResourceModel, device.StatusSubsystem.VersionInfo.Model,
                        $"Version Info Model {device.ResourceNameCaption}", Globalization.CultureInfo.CurrentCulture)

    End Sub

    ''' <summary> Closes a session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="trialNumber"> The trial number. </param>
    ''' <param name="device">      The resistance meter device. </param>
    Friend Shared Sub CloseSession(ByVal trialNumber As Integer, ByVal device As VI.Tsp.K3700.ResistancesMeterDevice)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Dim actualBoolean As Boolean
        device.TryCloseSession()

        actualBoolean = device.IsDeviceOpen
        Dim expectedBoolean As Boolean = False
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} Disconnect still connected {device.ResourceNameCaption}")

        actualBoolean = device.IsDeviceOpen
        expectedBoolean = False
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{trialNumber} Close still open {device.ResourceNameCaption}")
    End Sub

#End Region

#Region " ASSIGNED DEVICE TESTS "

    ''' <summary> (Unit Test Method) tests assigned device measure resistor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignedDeviceMeasureResistorTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Try
            ResistancesMeterTests._ResistancesMeter.AssignDevice(ResistancesMeterTests._ResistancesMeterDevice, False)
            ResistancesMeterTests.OpenSession(1, ResistancesMeterTests._ResistancesMeter.Device)
        Catch
            Throw
        Finally
            ResistancesMeterTests.CloseSession(1, ResistancesMeterTests._ResistancesMeter.Device)
            ResistancesMeterTests._ResistancesMeter.AssignDevice(Nothing, True)
        End Try
    End Sub

    ''' <summary> (Unit Test Method) tests assigned open device measure resistor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignedOpenDeviceMeasureResistorTest()
        Try
            ResistancesMeterTests.OpenSession(0, ResistancesMeterTests._ResistancesMeterDevice)
            ResistancesMeterTests._ResistancesMeter.AssignDevice(ResistancesMeterTests._ResistancesMeterDevice, False)
        Catch
            Throw

        Finally
            ResistancesMeterTests.CloseSession(1, ResistancesMeterTests._ResistancesMeter.Device)
            ResistancesMeterTests._ResistancesMeter.AssignDevice(Nothing, True)
        End Try
	End Sub

#End Region

End Class

