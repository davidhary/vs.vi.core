''' <summary> K3700 Device with K3720 Multiplexer Resistance Measurements unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k3720")>
Public Class K3720MultimeterTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K3700Tests.K3720Settings.Get.Exists, $"{GetType(K3700Tests.K3720Settings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " MULTIMETER SUBSYSTEM: RESISTANCE RANGE TEST "

    ''' <summary> (Unit Test Method) multimeter subsystem resistance range should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemResistanceRangeShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            Try
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                Dim initialRange As Double = 0.1 * SubsystemsSettings.Get.ExpectedResistance
                Dim retryFactor As Double = 2
                Dim retryCount As Integer = 10
                K3720MultimeterTests.AssertCloseChannelsShouldMatch(device.ChannelSubsystem, SubsystemsSettings.Get.ResistorChannelList)
                K3720MultimeterTests.AssertFunctionModeShouldApply(device.MultimeterSubsystem, VI.MultimeterFunctionModes.ResistanceFourWire)
                Dim status As VI.MetaStatus = device.MultimeterSubsystem.EstablishRange(initialRange, retryFactor, retryCount)
                Dim expectedRange As Double = SubsystemsSettings.Get.ExpectedResistance
                Assert.IsTrue(device.MultimeterSubsystem.Range.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
                Assert.IsTrue(status.HasValue, $"Failed reading {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MeasuredAmount)}.{NameOf(VI.MetaStatus)}")
                Assert.IsTrue(device.MultimeterSubsystem.Range.Value - expectedRange < 0.05 * expectedRange,
                          $"actual {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)} {device.MultimeterSubsystem.Range.Value} too low < {expectedRange}")
                expectedRange *= 10
                Assert.IsTrue(expectedRange - device.MultimeterSubsystem.Range.Value > 0.05 * expectedRange,
                          $"actual {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)} {device.MultimeterSubsystem.Range.Value} too high > {expectedRange}")
            Catch
                Throw
            Finally
                K3700Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MULTIMETER SUBSYSTEM: TEST VOLTAGE MEASUREMENT "

    ''' <summary> Assert close channels should match. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">   The subsystem. </param>
    ''' <param name="channelList"> List of channels. </param>
    Private Shared Sub AssertCloseChannelsShouldMatch(ByVal subsystem As VI.ChannelSubsystemBase, ByVal channelList As String)

        Dim expectedChannelList As String = String.Empty
        Dim actualChannelList As String = subsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList, $"Open All failed {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)}")

        expectedChannelList = channelList
        actualChannelList = subsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList, $"Close channels failed {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)}")

    End Sub

    ''' <summary> Assert function mode should apply. </summary>
    ''' <remarks> David, 2021-07-05. </remarks>
    ''' <param name="subsystem">            The subsystem. </param>
    ''' <param name="expectedFunctionMode"> The expected function mode. </param>
    Private Shared Sub AssertFunctionModeShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal expectedFunctionMode As VI.MultimeterFunctionModes)
        Dim actualFunctionMode? As VI.MultimeterFunctionModes = subsystem.ApplyFunctionMode(expectedFunctionMode)
        Assert.IsTrue(actualFunctionMode.HasValue, $"Failed reading {GetType(VI.Tsp.K3700.MultimeterSubsystem)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.FunctionMode)}")
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"Failed initial {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.FunctionMode)}")
    End Sub

    ''' <summary> Assert automatic zero once should apply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub AssertAutoZeroOnceShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase)
        subsystem.AutoZeroOnce()
        Dim expectedBoolean As Boolean = False
        Dim actualBoolean As Boolean? = subsystem.QueryAutoZeroEnabled()
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroOnce)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroOnce)} {NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)} still enabled")
    End Sub

    ''' <summary> Assert automatic zero enabled should apply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AssertAutoZeroEnabledShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyAutoZeroEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)}")
    End Sub

    ''' <summary> Assert filter enabled should apply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AssertFilterEnabledShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyFilterEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterEnabled)}")
    End Sub

    ''' <summary> Assert moving average filter enabled should apply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AssertMovingAverageFilterEnabledShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Boolean)
        Dim expectedBoolean As Boolean = value
        Dim actualBoolean As Boolean? = subsystem.ApplyMovingAverageFilterEnabled(value)
        Assert.IsTrue(actualBoolean.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
        Assert.AreEqual(expectedBoolean, actualBoolean.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.MovingAverageFilterEnabled)}")
    End Sub

    ''' <summary> Aseert filter count should apply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AseertFilterCountShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Integer)
        Dim expectedInteger As Integer = value
        Dim actualInteger As Integer? = subsystem.ApplyFilterCount(value)
        Assert.IsTrue(actualInteger.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
        Assert.AreEqual(expectedInteger, actualInteger.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterCount)}")
    End Sub

    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AssertFilterWindowShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyFilterWindow(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.FilterWindow)}")
    End Sub

    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AssertPowerLineCyclesShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyPowerLineCycles(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)}")
    End Sub

    ''' <summary> Assert range should apply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    ''' <param name="value">     The value. </param>
    Private Shared Sub AssertRangeShouldApply(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase, ByVal value As Double)
        Dim expectedDouble As Double = value
        Dim actualDouble As Double? = subsystem.ApplyRange(value)
        Assert.IsTrue(actualDouble.HasValue, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
        Assert.AreEqual(expectedDouble, actualDouble.Value, $"Failed applying {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.Range)}")
    End Sub

    ''' <summary> Assert measure should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Shared Sub AssertMeasureShouldPass(ByVal subsystem As VI.Tsp.MultimeterSubsystemBase)
        Dim expectedTimeSpan As TimeSpan = subsystem.EstimateMeasurementTime
        Dim timeout As Integer = Math.Max(10000, 4 * CInt(expectedTimeSpan.TotalMilliseconds))
        subsystem.Session.StoreCommunicationTimeout(TimeSpan.FromMilliseconds(timeout))
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim value As Double? = subsystem.MeasurePrimaryReading()
        Dim actualTimespan As TimeSpan = sw.Elapsed
        Assert.IsTrue(value.HasValue, $"Failed reading {GetType(VI.Tsp.MultimeterSubsystemBase)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.PrimaryReadingValue)}")
        Assert.IsTrue(value.Value > -0.001, $"Failed reading positive {GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PrimaryReadingValue)}")
        Assert.IsTrue(actualTimespan >= expectedTimeSpan, $"Reading too short; expected {expectedTimeSpan} actual {actualTimespan}")
        Dim twiceInterval As TimeSpan = expectedTimeSpan.Add(expectedTimeSpan)
        Assert.IsTrue(actualTimespan < twiceInterval, $"Reading too long; expected {twiceInterval} actual {actualTimespan}")
        subsystem.Session.RestoreCommunicationTimeout()
    End Sub

    ''' <summary>
    ''' (Unit Test Method) multimeter subsystem voltage measurement timing should pass.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemVoltageMeasurementTimingShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                Dim powerLineCycles As Double = 5
                Dim FilterCount As Integer = 20
                Dim window As Double = 0.095
                Dim range As Double = 0.1
                K3720MultimeterTests.AssertCloseChannelsShouldMatch(device.ChannelSubsystem, SubsystemsSettings.Get.ShortChannelList)
                K3720MultimeterTests.AssertPowerLineCyclesShouldApply(device.MultimeterSubsystem, powerLineCycles)
                K3720MultimeterTests.AssertRangeShouldApply(device.MultimeterSubsystem, range)
                K3720MultimeterTests.AssertAutoZeroEnabledShouldApply(device.MultimeterSubsystem, False)
                K3720MultimeterTests.AssertAutoZeroOnceShouldApply(device.MultimeterSubsystem)
                K3720MultimeterTests.AssertMovingAverageFilterEnabledShouldApply(device.MultimeterSubsystem, False) ' use repeat filter.
                K3720MultimeterTests.AseertFilterCountShouldApply(device.MultimeterSubsystem, FilterCount)
                K3720MultimeterTests.AssertFilterWindowShouldApply(device.MultimeterSubsystem, window)
                K3720MultimeterTests.AssertFilterEnabledShouldApply(device.MultimeterSubsystem, True)
                K3720MultimeterTests.AssertMeasureShouldPass(device.MultimeterSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) multimeter subsystem resistance measurement timing should pass.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemResistanceMeasurementTimingShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                Dim powerLineCycles As Double = 5
                Dim FilterCount As Integer = 20
                Dim window As Double = 0.095
                Dim range As Double = 1
                K3720MultimeterTests.AssertCloseChannelsShouldMatch(device.ChannelSubsystem, SubsystemsSettings.Get.ShortChannelList)
                K3720MultimeterTests.AssertFunctionModeShouldApply(device.MultimeterSubsystem, VI.MultimeterFunctionModes.ResistanceFourWire)
                K3720MultimeterTests.AssertPowerLineCyclesShouldApply(device.MultimeterSubsystem, powerLineCycles)
                K3720MultimeterTests.AssertRangeShouldApply(device.MultimeterSubsystem, range)
                K3720MultimeterTests.AssertAutoZeroEnabledShouldApply(device.MultimeterSubsystem, False)
                K3720MultimeterTests.AssertAutoZeroOnceShouldApply(device.MultimeterSubsystem)
                K3720MultimeterTests.AssertMovingAverageFilterEnabledShouldApply(device.MultimeterSubsystem, False) ' use repeat filter.
                K3720MultimeterTests.AseertFilterCountShouldApply(device.MultimeterSubsystem, FilterCount)
                K3720MultimeterTests.AssertFilterWindowShouldApply(device.MultimeterSubsystem, window)
                K3720MultimeterTests.AssertFilterEnabledShouldApply(device.MultimeterSubsystem, True)
                K3720MultimeterTests.AssertMeasureShouldPass(device.MultimeterSubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region


End Class
