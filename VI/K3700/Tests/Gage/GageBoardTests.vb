Imports isr.VI.Tsp.K3700

''' <summary> K3700 Device Gage Board measurements unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("GageBoard")>
Public Class GageBoardTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(GageBoardSettings.Get.Exists, $"{GetType(GageBoardSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " CHANNEL SUBSYSTEM TEST "

    ''' <summary> Reads channel subsystem information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertChannelSubsystemInfoShouldPass(ByVal device As VI.Tsp.K3700.K3700Device)

        Dim expectedChannelList As String = String.Empty
        Dim actualChannelList As String = device.ChannelSubsystem.QueryClosedChannels()
        Assert.AreEqual(expectedChannelList, actualChannelList,
                    $"Initial {NameOf(ChannelSubsystem)}.{NameOf(ChannelSubsystem.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

        expectedChannelList = GageBoardSettings.Get.FirstRtdChannelList
        actualChannelList = device.ChannelSubsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"{NameOf(ChannelSubsystem)}.{NameOf(ChannelSubsystem.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

        expectedChannelList = String.Empty
        actualChannelList = device.ChannelSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"Open All {NameOf(ChannelSubsystem)}.{NameOf(ChannelSubsystem.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

    End Sub

    ''' <summary> (Unit Test Method) tests read channel subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ChannelSubsystemInfoShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                GageBoardTests.AssertChannelSubsystemInfoShouldPass(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " GAGE BOARD SUBSYSTEM TEST "

    ''' <summary> Assert multimeter subsystem information should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertMultimeterSubsystemInfoShouldPass(ByVal device As VI.Tsp.K3700.K3700Device)

        Dim expectedPowerLineCycles As Double = K3700Tests.SubsystemsSettings.Get.InitialPowerLineCycles
        Dim actualPowerLineCycles As Double = device.MultimeterSubsystem.QueryPowerLineCycles().GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, K3700Tests.SubsystemsSettings.Get.LineFrequency / TimeSpan.TicksPerSecond,
                        $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.PowerLineCycles)} is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}")

        Dim expectedBoolean As Boolean = K3700Tests.SubsystemsSettings.Get.InitialAutoRangeEnabled
        Dim actualBoolean As Boolean = device.MultimeterSubsystem.QueryAutoRangeEnabled.GetValueOrDefault(False)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.AutoRangeEnabled)} Is {actualBoolean }; expected {expectedBoolean}")

        expectedBoolean = K3700Tests.SubsystemsSettings.Get.InitialAutoZeroEnabled
        actualBoolean = device.MultimeterSubsystem.QueryAutoZeroEnabled.GetValueOrDefault(False)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.AutoZeroEnabled)} Is {actualBoolean }; expected {expectedBoolean }")

        Dim expectedFunction As MultimeterFunctionModes = K3700Tests.SubsystemsSettings.Get.InitialMultimeterFunction
        Dim senseFn As VI.MultimeterFunctionModes = device.MultimeterSubsystem.QueryFunctionMode.GetValueOrDefault(MultimeterFunctionModes.ResistanceTwoWire)
        Assert.AreEqual(expectedFunction, senseFn,
                        $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.FunctionMode)} Is {senseFn} ; expected {expectedFunction}")

    End Sub

    ''' <summary> (Unit Test Method) multimeter subsystem information should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MultimeterSubsystemInfoShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                GageBoardTests.AssertMultimeterSubsystemInfoShouldPass(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " GAGE BOARD RESISTANCE "

    ''' <summary> Assert gage board resistance should prepare. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertGageBoardResistanceShouldPrepare(ByVal device As VI.Tsp.K3700.K3700Device)

        Dim expectedPowerLineCycles As Double = GageBoardSettings.Get.PowerLineCycles
        Dim actualPowerLineCycles As Double = device.MultimeterSubsystem.ApplyPowerLineCycles(expectedPowerLineCycles).GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, 60 / TimeSpan.TicksPerSecond,
                    $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.PowerLineCycles)} Is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}")

        Dim expectedBoolean As Boolean = GageBoardSettings.Get.AutoRangeEnabled
        Dim actualBoolean As Boolean = device.MultimeterSubsystem.ApplyAutoRangeEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.AutoRangeEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        expectedBoolean = GageBoardSettings.Get.AutoZeroEnabled
        actualBoolean = device.MultimeterSubsystem.ApplyAutoZeroEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.AutoZeroEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        Dim expectedFunction As MultimeterFunctionModes = GageBoardSettings.Get.SenseFunctionMode
        Dim actualFunction As MultimeterFunctionModes = device.MultimeterSubsystem.ApplyFunctionMode(expectedFunction).GetValueOrDefault(MultimeterFunctionModes.ResistanceFourWire)
        Assert.AreEqual(expectedFunction, actualFunction, $"{NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.FunctionMode)} is {actualFunction} ; expected {expectedFunction}")

        Dim expectedDetectorEnabled As Boolean = False
        device.MultimeterSubsystem.ApplyOpenDetectorEnabled(expectedDetectorEnabled)
        Assert.IsTrue(device.MultimeterSubsystem.OpenDetectorEnabled.HasValue, $"Detector enabled has value: {device.MultimeterSubsystem.OpenDetectorEnabled.HasValue}")
        Dim detectorEnabled As Boolean = device.MultimeterSubsystem.OpenDetectorEnabled.Value
        Assert.AreEqual(expectedDetectorEnabled, detectorEnabled, $"Detector enabled {device.MultimeterSubsystem.OpenDetectorEnabled.Value}")
    End Sub

    ''' <summary> Assert gage board resistance should match. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="trialNumber">   The trial number. </param>
    ''' <param name="device">        The device. </param>
    ''' <param name="expectedValue"> The expected value. </param>
    ''' <param name="epsilon">       The epsilon. </param>
    ''' <param name="channelList">   List of channels. </param>
    Private Shared Sub AssertGageBoardResistanceShouldMatch(ByVal trialNumber As Integer, ByVal device As VI.Tsp.K3700.K3700Device,
                                                            ByVal expectedValue As Double, ByVal epsilon As Double, ByVal channelList As String)

        Dim expectedChannelList As String = String.Empty
        Dim actualChannelList As String = device.ChannelSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"Open All {trialNumber} {NameOf(ChannelSubsystem)}.{NameOf(ChannelSubsystem.ClosedChannels)}='{actualChannelList}'; expected {expectedChannelList}")

        expectedChannelList = channelList
        actualChannelList = device.ChannelSubsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"{trialNumber} {NameOf(ChannelSubsystem)}.{NameOf(ChannelSubsystem.ClosedChannels)}='{actualChannelList}'; expected {expectedChannelList}")

        Dim expectedResistance As Double = expectedValue
        Dim resistance As Double = device.MultimeterSubsystem.MeasureReadingAmounts.GetValueOrDefault(-1)
        Assert.AreEqual(expectedValue, resistance, epsilon,
                        $"{trialNumber} {NameOf(ChannelSubsystem)}.{NameOf(ChannelSubsystem.ClosedChannels)}='{actualChannelList}' {NameOf(MultimeterSubsystem)}.{NameOf(MultimeterSubsystem.PrimaryReadingValue)} {channelList} is {resistance}; expected {expectedResistance } within {epsilon}")

    End Sub

    ''' <summary> (Unit Test Method) gage board resistance should match. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub GageBoardResistanceShouldMatch()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                GageBoardTests.AssertGageBoardResistanceShouldPrepare(device)
                Dim trialNumber As Integer = 0
                Dim expectedValue As Double = GageBoardSettings.Get.RtdAmbientResistance
                Dim epsilon As Double = GageBoardSettings.Get.RtdResistanceEpsilon
                Dim channelList As String = GageBoardSettings.Get.FirstRtdChannelList
                trialNumber += 1 : GageBoardTests.AssertGageBoardResistanceShouldMatch(trialNumber, device, expectedValue, epsilon, channelList)
                expectedValue = GageBoardSettings.Get.GaugeAmbientResistance
                epsilon = GageBoardSettings.Get.GaugeResistanceEpsilon
                channelList = GageBoardSettings.Get.FirstGaugeChannelList
                trialNumber += 1 : GageBoardTests.AssertGageBoardResistanceShouldMatch(trialNumber, device, expectedValue, epsilon, channelList)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) two wire circuit should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TwoWireCircuitShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                GageBoardTests.AssertGageBoardResistanceShouldPrepare(device)
                Dim trialNumber As Integer = 0
                Dim expectedValue As Double = GageBoardSettings.Get.RtdAmbientResistance
                Dim epsilon As Double = GageBoardSettings.Get.RtdResistanceEpsilon
                Dim channelList As String = GageBoardSettings.Get.TwoWireRtdSenseChannelList
                trialNumber += 1 : GageBoardTests.AssertGageBoardResistanceShouldMatch(trialNumber, device, expectedValue, epsilon, channelList)

                expectedValue = GageBoardSettings.Get.RtdAmbientResistance + GageBoardSettings.Get.GaugeAmbientResistance
                epsilon = GageBoardSettings.Get.RtdResistanceEpsilon + GageBoardSettings.Get.GaugeResistanceEpsilon
                channelList = GageBoardSettings.Get.TwoWireRtdSourceChannelList
                trialNumber += 1 : GageBoardTests.AssertGageBoardResistanceShouldMatch(trialNumber, device, expectedValue, epsilon, channelList)

                expectedValue = GageBoardSettings.Get.GaugeAmbientResistance
                epsilon = GageBoardSettings.Get.GaugeResistanceEpsilon
                channelList = GageBoardSettings.Get.TwoWireGageSenseChannelList
                trialNumber += 1 : GageBoardTests.AssertGageBoardResistanceShouldMatch(trialNumber, device, expectedValue, epsilon, channelList)

                expectedValue = 3 * GageBoardSettings.Get.GaugeAmbientResistance
                epsilon = 3 * GageBoardSettings.Get.GaugeResistanceEpsilon
                channelList = GageBoardSettings.Get.TwoWireGageSourceChannelList
                trialNumber += 1 : GageBoardTests.AssertGageBoardResistanceShouldMatch(trialNumber, device, expectedValue, epsilon, channelList)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class
