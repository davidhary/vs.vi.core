''' <summary> K3700 Device with K3730 Matrix Resistance Measurements unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k3730")>
Public Class K3730Tests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
	Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
		Try
			_TestSite = New TestSite
			_TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
			_TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
			_TestSite.InitializeTraceListener()
		Catch
			' cleanup to meet strong guarantees
			Try
				MyClassCleanup()
			Finally
			End Try
			Throw
		End Try
	End Sub

	''' <summary> My class cleanup. </summary>
	''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
	<ClassCleanup()>
	Public Shared Sub MyClassCleanup()
		_TestSite?.Dispose()
	End Sub

	''' <summary> Initializes before each test runs. </summary>
	''' <remarks> David, 2020-10-12. </remarks>
	<TestInitialize()> Public Sub MyTestInitialize()
		' assert reading of test settings from the configuration file.
		Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
		Dim expectedUpperLimit As Double = 12
		Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
		TestInfo.ClearMessageQueue()
		Assert.IsTrue(K3700Tests.K3730Settings.Get.Exists, $"{GetType(K3700Tests.K3730Settings)} settings should exist")
		TestInfo.ClearMessageQueue()
	End Sub

	''' <summary> Cleans up after each test has run. </summary>
	''' <remarks> David, 2020-10-12. </remarks>
	<TestCleanup()> Public Sub MyTestCleanup()
		TestInfo.AssertMessageQueue()
	End Sub

	''' <summary>
	''' Gets the test context which provides information about and functionality for the current test
	''' run.
	''' </summary>
	''' <value> The test context. </value>
	Public Property TestContext() As TestContext
	''' <summary> The test site. </summary>
	Private Shared _TestSite As TestSite

	''' <summary> Gets information describing the test. </summary>
	''' <value> Information describing the test. </value>
	Private Shared ReadOnly Property TestInfo() As TestSite
		Get
			Return _TestSite
		End Get
	End Property

#End Region

#Region " CHANNEL SUBSYSTEM TEST "

    ''' <summary> Assert channel subsystem information should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertChannelSubsystemInfoShouldPass(ByVal device As VI.Tsp.K3700.K3700Device)

        Dim expectedChannelList As String = String.Empty
        Dim actualChannelList As String = device.ChannelSubsystem.QueryClosedChannels()
        Assert.AreEqual(expectedChannelList, actualChannelList,
                    $"Initial {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

        expectedChannelList = K3730Settings.Get.ResistorChannelList
        actualChannelList = device.ChannelSubsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"{GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

        expectedChannelList = String.Empty
        actualChannelList = device.ChannelSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"Open All {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

    End Sub

    ''' <summary> (Unit Test Method) channel subsystem information should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub ChannelSubsystemInfoShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                K3730Tests.AssertChannelSubsystemInfoShouldPass(device)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " MEASURE RESISTANCE "

    ''' <summary> Measure resistance should prepare. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub MeasureResistanceShouldPrepare(ByVal device As VI.Tsp.K3700.K3700Device)

        Dim expectedPowerLineCycles As Double = K3730Settings.Get.PowerLineCycles
        Dim actualPowerLineCycles As Double = device.MultimeterSubsystem.ApplyPowerLineCycles(expectedPowerLineCycles).GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, 60 / TimeSpan.TicksPerSecond,
                    $"{GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PowerLineCycles)} Is {actualPowerLineCycles:G5}; expected {expectedPowerLineCycles:G5}")

        Dim expectedBoolean As Boolean = K3730Settings.Get.AutoRangeEnabled
        Dim actualBoolean As Boolean = device.MultimeterSubsystem.ApplyAutoRangeEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoRangeEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        expectedBoolean = K3730Settings.Get.AutoZeroEnabled
        actualBoolean = device.MultimeterSubsystem.ApplyAutoZeroEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.IsTrue(actualBoolean, $"{GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.AutoZeroEnabled)} is {actualBoolean}; expected {expectedBoolean}")

        Dim expectedFunction As VI.MultimeterFunctionModes = K3730Settings.Get.SenseFunction
        Dim actualFunction As VI.MultimeterFunctionModes = device.MultimeterSubsystem.ApplyFunctionMode(expectedFunction).GetValueOrDefault(VI.MultimeterFunctionModes.ResistanceFourWire)
        Assert.AreEqual(expectedFunction, actualFunction, $"{GetType(VI.Tsp.MultimeterSubsystemBase)}.{NameOf(VI.Tsp.MultimeterSubsystemBase.FunctionMode)} is {actualFunction} ; expected {expectedFunction}")

    End Sub

    ''' <summary> Measure resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="trialNumber">   The trial number. </param>
    ''' <param name="device">        The device. </param>
    ''' <param name="expectedValue"> The expected value. </param>
    ''' <param name="epsilon">       The epsilon. </param>
    ''' <param name="channelList">   List of channels. </param>
    Private Shared Sub AssertMeasureResistanceShouldPass(ByVal trialNumber As Integer, ByVal device As VI.Tsp.K3700.K3700Device,
                                                         ByVal expectedValue As Double, ByVal epsilon As Double,
                                                         ByVal channelList As String)

        Dim expectedChannelList As String = String.Empty
        Dim actualChannelList As String = device.ChannelSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"Open All {trialNumber} {GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)} is {actualChannelList}; expected {expectedChannelList}")

        expectedChannelList = channelList
        actualChannelList = device.ChannelSubsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
        Assert.AreEqual(expectedChannelList, actualChannelList,
                        $"{GetType(VI.ChannelSubsystemBase)}.{NameOf(VI.ChannelSubsystemBase.ClosedChannels)}='{actualChannelList}'; expected {expectedChannelList}")

        Dim expectedResistance As Double = expectedValue
        Dim resistance As Double = device.MultimeterSubsystem.MeasureReadingAmounts.GetValueOrDefault(-1)
        Assert.AreEqual(expectedValue, resistance, epsilon,
                        $"{GetType(VI.MultimeterSubsystemBase)}.{NameOf(VI.MultimeterSubsystemBase.PrimaryReadingValue)} {channelList} is {resistance}; expected {expectedResistance } within {epsilon}")

    End Sub

    ''' <summary> (Unit Test Method) measure resistance should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub MeasureResistanceShouldPass()
        Using device As VI.Tsp.K3700.K3700Device = VI.Tsp.K3700.K3700Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                K3730Tests.MeasureResistanceShouldPrepare(device)
                Dim trialNumber As Integer = 0
                trialNumber += 1 : K3730Tests.AssertMeasureResistanceShouldPass(trialNumber, device, 0, K3730Settings.Get.ExpectedShort32ResistanceEpsilon,
                                                                 K3730Settings.Get.ShortChannelList)
                trialNumber += 1 : K3730Tests.AssertMeasureResistanceShouldPass(trialNumber, device, K3730Settings.Get.ExpectedResistance,
                                                                 K3730Settings.Get.ExpectedResistanceEpsilon, K3730Settings.Get.ResistorChannelList)
                trialNumber += 1 : K3730Tests.AssertMeasureResistanceShouldPass(trialNumber, device, K3730Settings.Get.ExpectedOpen,
                                                                 1, K3730Settings.Get.OpenChannelList)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class


