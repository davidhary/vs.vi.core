''' <summary> A Measure Test Info. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class ResistanceSettings
    Inherits isr.VI.DeviceTests.TestSettingsBase

#Region " SINGLETON "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(ResistanceSettings)} Editor", ResistanceSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As ResistanceSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ResistanceSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New ResistanceSettings()), ResistanceSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " MEASURE SUBSYSTEM INFORMATION "

    ''' <summary> Gets or sets the auto zero Enabled settings. </summary>
    ''' <value> The auto zero settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property AutoZeroEnabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the automatic range enabled. </summary>
    ''' <value> The automatic range enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property AutoRangeEnabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Sense Function settings. </summary>
    ''' <value> The Sense Function settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("ResistanceFourWire")>
    Public Property SenseFunction As VI.MultimeterFunctionModes
        Get
            Return Me.AppSettingEnum(Of VI.MultimeterFunctionModes)
        End Get
        Set(value As VI.MultimeterFunctionModes)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the power line cycles settings. </summary>
    ''' <value> The power line cycles settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Property PowerLineCycles As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of short channels. </summary>
    ''' <value> A List of short channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1030;1060;1912;1921")>
    Public Property ShortChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of resistor channels. </summary>
    ''' <value> A List of resistor channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1028;1058;1912;1921")>
    Public Property ResistorChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of open channels. </summary>
    ''' <value> A List of open channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1029;1059;1912;1921")>
    Public Property OpenChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the expected resistance. </summary>
    ''' <value> The expected resistance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Property ExpectedResistance As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the expected open. </summary>
    ''' <value> The expected open. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("9.9E+37")>
    Public Property ExpectedOpen As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the expected resistance epsilon. </summary>
    ''' <value> The expected resistance epsilon. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.4")>
    Public Property ExpectedResistanceEpsilon As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the expected zero resistance epsilon. </summary>
    ''' <value> The expected zero resistance epsilon. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.001")>
    Public Property ExpectedZeroResistanceEpsilon As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the expected short 32 resistance epsilon. </summary>
    ''' <value> The expected short 32 resistance epsilon. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.05")>
    Public Property ExpectedShort32ResistanceEpsilon As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

