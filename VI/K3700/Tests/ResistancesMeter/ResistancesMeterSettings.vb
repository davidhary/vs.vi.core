''' <summary> A resistances meter Test Info. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class ResistancesMeterSettings
    Inherits isr.VI.DeviceTests.SubsystemsSettingsBase

#Region " SINGLETON "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(ResistancesMeterSettings)} Editor", ResistancesMeterSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As ResistancesMeterSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ResistancesMeterSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New ResistancesMeterSettings()), ResistancesMeterSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " MEASURE SUBSYSTEM INFORMATION "

    ''' <summary> Gets or sets the auto zero Enabled settings. </summary>
    ''' <value> The auto zero settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property AutoZeroEnabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the automatic range enabled. </summary>
    ''' <value> The automatic range enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property AutoRangeEnabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Sense Function settings. </summary>
    ''' <value> The Sense Function settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("ResistanceFourWire")>
    Public Property SenseFunctionMode As VI.MultimeterFunctionModes
        Get
            Return Me.AppSettingEnum(Of VI.MultimeterFunctionModes)
        End Get
        Set(value As VI.MultimeterFunctionModes)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the power line cycles settings. </summary>
    ''' <value> The power line cycles settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Property PowerLineCycles As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " RESISTANCES METER INFORMATION "

    ''' <summary> Gets or sets a list of 1 channels. </summary>
    ''' <value> A List of 1 channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1001;1031;1912;1921")>
    Public Property R1ChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of 2 channels. </summary>
    ''' <value> A List of 2 channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1002;1032;1912;1921")>
    Public Property R2ChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of 3 channels. </summary>
    ''' <value> A List of 3 channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1003;1033;1912;1921")>
    Public Property R3ChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of 4 channels. </summary>
    ''' <value> A List of 4 channels. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1004;1034;1912;1921")>
    Public Property R4ChannelList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the 1 values. </summary>
    ''' <value> The r 1 values. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("500|464")>
    Public Property R1Values As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the r 1. </summary>
    ''' <value> The r 1. </value>
    Public ReadOnly Property R1 As Double
        Get
            Return Double.Parse(Me.R1Values.Split("|"c)(ResourceSettings.Get.HostInfoIndex))
        End Get
    End Property

    ''' <summary> Gets or sets the 2 values. </summary>
    ''' <value> The r 2 values. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("500|462")>
    Public Property R2Values As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the r 2. </summary>
    ''' <value> The r 2. </value>
    Public ReadOnly Property R2 As Double
        Get
            Return Double.Parse(Me.R2Values.Split("|"c)(ResourceSettings.Get.HostInfoIndex))
        End Get
    End Property

    ''' <summary> Gets or sets the 3 values. </summary>
    ''' <value> The r 3 values. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("500|464")>
    Public Property R3Values As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the r 3. </summary>
    ''' <value> The r 3. </value>
    Public ReadOnly Property R3 As Double
        Get
            Return Double.Parse(Me.R3Values.Split("|"c)(ResourceSettings.Get.HostInfoIndex))
        End Get
    End Property

    ''' <summary> Gets or sets the 4 values. </summary>
    ''' <value> The r 4 values. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("500|465")>
    Public Property R4Values As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets the r 4. </summary>
    ''' <value> The r 4. </value>
    Public ReadOnly Property R4 As Double
        Get
            Return Double.Parse(Me.R4Values.Split("|"c)(ResourceSettings.Get.HostInfoIndex))
        End Get
    End Property

    ''' <summary> Gets or sets the resistance epsilon. </summary>
    ''' <remarks> Is rather high to allow temperature effects. </remarks>
    ''' <value> The resistance epsilon. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("20")>
    Public Property ResistanceEpsilon As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

''' <summary> Collection of resistors. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-04-18 </para>
''' </remarks>
Public Class ResistorCollection
    Inherits ChannelResistorCollection

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
        Me.AddResistor("R1", ResistancesMeterSettings.Get.R1ChannelList)
        Me.AddResistor("R2", ResistancesMeterSettings.Get.R2ChannelList)
        Me.AddResistor("R3", ResistancesMeterSettings.Get.R3ChannelList)
        Me.AddResistor("R4", ResistancesMeterSettings.Get.R4ChannelList)
    End Sub

End Class

