<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <sectionGroup name="userSettings" type="System.Configuration.UserSettingsGroup">
      <section name="isr.VI.Tsp.K3700Tests.ResourceSettings" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.Tsp.K3700Tests.SubsystemsSettings" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.Tsp.K3700Tests.ResistanceSettings" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.Tsp.K3700Tests.K3720Settings" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.Tsp.K3700Tests.K3730Settings" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.Tsp.K3700Tests.ResistancesMeterSettings" type="System.Configuration.ClientSettingsSection" />
      <section name="isr.VI.Tsp.K3700Tests.TestSite" type="System.Configuration.ClientSettingsSection" />
    </sectionGroup>
  </configSections>

  <userSettings>

    <!-- TEST SITE SETTINGS -->
    <isr.VI.Tsp.K3700Tests.TestSite>
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="All" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="IPv4Prefixes" serializeAs="String">
        <value>192.168|10.1</value>
      </setting>
      <setting name="TimeZones" serializeAs="String">
        <value>Pacific Standard Time|Central Standard Time</value>
      </setting>
      <setting name="TimeZoneOffsets" serializeAs="String">
        <value>-8|-6</value>
      </setting>
    </isr.VI.Tsp.K3700Tests.TestSite>

    <!-- K3700 RESOURCE -->
    <isr.VI.Tsp.K3700Tests.ResourceSettings>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>

      <!-- RESOURCE -->
      <setting name="ResourceModel" serializeAs="String">
        <value>3706A</value>
      </setting>
      <setting name="ResourceNames" serializeAs="String">
        <value>
          TCPIP0::192.168.0.132::inst0::INSTR|
          TCPIP0::192.168.0.148::inst0::INSTR|
          TCPIP0::10.1.1.21::inst0::INSTR|
          TCPIP0::10.1.1.24::inst0::INSTR
        </value>
      </setting>
      <setting name="ResourceTitle" serializeAs="String">
        <value>3706A</value>
      </setting>
      <setting name="LanguageXX" serializeAs="String">
        <value>TSP</value>
      </setting>
    </isr.VI.Tsp.K3700Tests.ResourceSettings>
    
    <!-- K3700 SUBSYSTEMS -->
    <isr.VI.Tsp.K3700Tests.SubsystemsSettings>

      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>

      <!-- VISA SESSION -->
      <setting name="KeepAliveQueryCommand" serializeAs="String">
        <value>*OPC?</value>
      </setting>
      <setting name="KeepAliveCommand" serializeAs="String">
        <value>*OPC</value>
      </setting>
      <setting name="ReadTerminationEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="ReadTerminationCharacter" serializeAs="String">
        <value>10</value>
      </setting>

      <!-- STATUS -->
      <setting name="LineFrequency" serializeAs="String">
        <value>60</value>
      </setting>

      <!-- INITIAL VALUES -->
      <setting name="InitialPowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
      <setting name="InitialAutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="InitialAutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="InitialSenseFunction" serializeAs="String">
        <value>VoltageDC</value>
      </setting>
      <setting name="InitialFilterWindow" serializeAs="String">
        <value>0.001</value>
      </setting>
      <setting name="InitialFilterCount" serializeAs="String">
        <value>10</value>
      </setting>
      <setting name="InitialFilterEnabled" serializeAs="String">
        <value>False</value>
      </setting>
      <setting name="InitialMovingAverageFilterEnabled " serializeAs="String">
        <value>False</value>
      </setting>
      
      <!-- DEVICE ERRORS -->
      <setting name="ErroneousCommand" serializeAs="String">
        <value>*CLL</value>
      </setting>
      <setting name="ErrorAvailableMillisecondsDelay" serializeAs="String">
        <value>0</value>
      </setting>
      <setting name="ExpectedCompoundErrorMessage" serializeAs="String">
        <value>-285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20</value>
      </setting>
      <setting name="ExpectedErrorNumber" serializeAs="String">
        <value>-285</value>
      </setting>
      <setting name="ExpectedErrorMessage" serializeAs="String">
        <value>TSP Syntax error at line 1: unexpected symbol near `*'</value>
      </setting>
      <setting name="ExpectedErrorLevel" serializeAs="String">
        <value>20</value>
      </setting>

      <!-- VOLTAGE AND RESISTANCE TIMING TEST -->
      <setting name="ShortChannelList" serializeAs="String">
        <value>1030;1060;1912;1921</value>
      </setting>

      <!-- RESISTANCE MEASUREMENT -->
      <setting name="ExpectedResistance" serializeAs="String">
        <value>10</value>
      </setting>
      <setting name="ResistorChannelList" serializeAs="String">
        <value>1028;1058;1912;1921</value>
      </setting>

    </isr.VI.Tsp.K3700Tests.SubsystemsSettings>

    <!-- K3720 MULTIPLEXER RESISTANCE  SLOT 1 -->
    <isr.VI.Tsp.K3700Tests.K3720Settings>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- MEASUREMENT SETTINGS -->
      <setting name="AutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="AutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="SenseFunction" serializeAs="String">
        <value>ResistanceFourWire</value>
      </setting>
      <setting name="PowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
      
      <!-- RESISTANCE MEASUREMENT SLOT 1 -->
      <setting name="ShortChannelList" serializeAs="String">
        <value>1030;1060;1912;1921</value>
      </setting>
      <setting name="ResistorChannelList" serializeAs="String">
        <value>1028;1058;1912;1921</value>
      </setting>
      <setting name="OpenChannelList" serializeAs="String">
        <value>1029;1059;1912;1921</value>
      </setting>
      <setting name="ExpectedZeroResistanceEpsilon" serializeAs="String">
        <value>0.001</value>
      </setting>
      <setting name="ExpectedShort32ResistanceEpsilon" serializeAs="String">
        <value>0.05</value>
      </setting>
      <setting name="ExpectedResistance" serializeAs="String">
        <value>10</value>
      </setting>
      <setting name="ExpectedResistanceEpsilon" serializeAs="String">
        <value>0.4</value>
      </setting>
      <setting name="ExpectedOpen" serializeAs="String">
        <value>9.9E+37</value>
      </setting>
    </isr.VI.Tsp.K3700Tests.K3720Settings>
    
    <!-- K3720 MATRIX RESISTANCE SLOT 2 -->
    <isr.VI.Tsp.K3700Tests.K3730Settings>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- MEASUREMENT SETTINGS -->
      <setting name="AutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="AutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="SenseFunction" serializeAs="String">
        <value>ResistanceFourWire</value>
      </setting>
      <setting name="PowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
      <!-- RESISTANCE MEASUREMENT -->
      <setting name="ShortChannelList" serializeAs="String">
        <value>2109;2209;2911;2912</value>
      </setting>
      <setting name="ResistorChannelList" serializeAs="String">
        <value>2101;2201;2911;2912</value>
      </setting>
      <setting name="OpenChannelList" serializeAs="String">
        <value>2110;2210;2911;2912</value>
      </setting>
      <setting name="ExpectedZeroResistanceEpsilon" serializeAs="String">
        <value>0.2</value>
      </setting>
      <setting name="ExpectedShort32ResistanceEpsilon" serializeAs="String">
        <value>0.30</value>
      </setting>
      <setting name="ExpectedResistance" serializeAs="String">
        <value>15</value>
      </setting>
      <setting name="ExpectedResistanceEpsilon" serializeAs="String">
        <value>0.6</value>
      </setting>
      <setting name="ExpectedOpen" serializeAs="String">
        <value>9.9E+37</value>
      </setting>
    </isr.VI.Tsp.K3700Tests.K3730Settings>
    
    <!-- RESISTANCE SLOT 1 -->
    <isr.VI.Tsp.K3700Tests.ResistanceSettings>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- MEASUREMENT SETTINGS -->
      <setting name="AutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="AutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="SenseFunction" serializeAs="String">
        <value>ResistanceFourWire</value>
      </setting>
      <setting name="PowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
      <!-- RESISTANCE MEASUREMENT -->
      <setting name="ShortChannelList" serializeAs="String">
        <value>1030;1060;1912;1921</value>
      </setting>
      <setting name="ResistorChannelList" serializeAs="String">
        <value>1028;1058;1912;1921</value>
      </setting>
      <setting name="OpenChannelList" serializeAs="String">
        <value>1029;1059;1912;1921</value>
      </setting>
      <setting name="ExpectedZeroResistanceEpsilon" serializeAs="String">
        <value>0.001</value>
      </setting>
      <setting name="ExpectedShort32ResistanceEpsilon" serializeAs="String">
        <value>0.05</value>
      </setting>
      <setting name="ExpectedResistance" serializeAs="String">
        <value>10</value>
      </setting>
      <setting name="ExpectedResistanceEpsilon" serializeAs="String">
        <value>0.4</value>
      </setting>
      <setting name="ExpectedOpen" serializeAs="String">
        <value>9.9E+37</value>
      </setting>
    </isr.VI.Tsp.K3700Tests.ResistanceSettings>
    
    <!-- RESISTANCES METER SLOT 1  -->
    <isr.VI.Tsp.K3700Tests.ResistancesMeterSettings>
      <!-- CONFIGURATION -->
      <setting name="Exists" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Verbose" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="Enabled" serializeAs="String">
        <value>True</value>
      </setting>
      <!-- TEST DATA -->
      <setting name="R1ChannelList" serializeAs="String">
        <value>1001;1031;1912;1921</value>
      </setting>
      <setting name="R2ChannelList" serializeAs="String">
        <value>1002;1032;1912;1921</value>
      </setting>
      <setting name="R3ChannelList" serializeAs="String">
        <value>1003;1033;1912;1921</value>
      </setting>
      <setting name="R4ChannelList" serializeAs="String">
        <value>1004;1034;1912;1921</value>
      </setting>
      <setting name="R1Values" serializeAs="String">
        <value>500|464</value>
      </setting>
      <setting name="R2Values" serializeAs="String">
        <value>500|462</value>
      </setting>
      <setting name="R3Values" serializeAs="String">
        <value>500|464</value>
      </setting>
      <setting name="R4Values" serializeAs="String">
        <value>500|465</value>
      </setting>
      <!--Is rather high to allow temperature effects. -->
      <setting name="ResistanceEpsilon" serializeAs="String">
        <value>20</value>
      </setting>
      <setting name="AutoZeroEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="AutoRangeEnabled" serializeAs="String">
        <value>True</value>
      </setting>
      <setting name="SenseFunctionMode" serializeAs="String">
        <value>CurrentDC</value>
      </setting>
      <setting name="PowerLineCycles" serializeAs="String">
        <value>1</value>
      </setting>
    </isr.VI.Tsp.K3700Tests.ResistancesMeterSettings>
  </userSettings>
  
  <system.web>
    <membership defaultProvider="ClientAuthenticationMembershipProvider">
      <providers>
        <add name="ClientAuthenticationMembershipProvider" type="System.Web.ClientServices.Providers.ClientFormsAuthenticationMembershipProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" />
      </providers>
    </membership>
    <roleManager defaultProvider="ClientRoleProvider" enabled="true">
      <providers>
        <add name="ClientRoleProvider" type="System.Web.ClientServices.Providers.ClientRoleProvider, System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" serviceUri="" cacheTimeout="86400" />
      </providers>
    </roleManager>
  </system.web>
  
</configuration>
