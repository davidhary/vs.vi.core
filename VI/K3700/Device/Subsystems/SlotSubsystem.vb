
''' <summary> A slot subsystem. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Class SlotSubsystem
    Inherits isr.VI.Tsp.SlotSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="slotNumber">      The slot number. </param>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="VI.Tsp.StatusSubsystemBase">message based
    '''                                session</see>. </param>
    Public Sub New(ByVal slotNumber As Integer, ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase)
        MyBase.New(slotNumber, statusSubsystem)
    End Sub

#End Region

End Class
