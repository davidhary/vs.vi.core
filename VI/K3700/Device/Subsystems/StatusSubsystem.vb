''' <summary> Status subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits isr.VI.Tsp.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> The session. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(session)
        Me.VersionInfoBase = New VersionInfo
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Define measurement events bitmasks. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Overloads Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.MeasurementEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        Dim failuresSummaryBitmask As Integer = 0
        Dim bitmask As Integer
        bitmask = 1 << 0 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit1, bitmask)
        bitmask = 1 << 1 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit1, bitmask)
        bitmask = 1 << 2 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit2, bitmask)
        bitmask = 1 << 3 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit2, bitmask)
        bitmask = 1 << 7 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.ReadingOverflow, bitmask)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.BufferAvailable, 1 << 8)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.Questionable, 1 << 31, True)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.FailuresSummary, failuresSummaryBitmask)
    End Sub

    ''' <summary> Define measurement events bit values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub DefineMeasurementEventsBitmasks()
        MyBase.DefineMeasurementEventsBitmasks()
        StatusSubsystem.DefineBitmasks(Me.MeasurementEventsBitmasks)
    End Sub

#End Region

End Class

