''' <summary> Defines a Multimeter Subsystem for a Keithley 3700 instrument. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2016-01-15 </para>
''' </remarks>
Public Class MultimeterSubsystem
    Inherits VI.Tsp.MultimeterSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        Me.OpenDetectorCapabilities.Item(MultimeterFunctionModes.ResistanceFourWire) = True

        Me.FunctionModeDecimalPlaces.Item(MultimeterFunctionModes.ResistanceCommonSide) = 0
        Me.FunctionModeDecimalPlaces.Item(MultimeterFunctionModes.ResistanceFourWire) = 0
        Me.FunctionModeDecimalPlaces.Item(MultimeterFunctionModes.ResistanceTwoWire) = 0

        Me.FunctionModeRanges.Item(MultimeterFunctionModes.CurrentAC).SetRange(0, 3.1)
        Me.FunctionModeRanges.Item(MultimeterFunctionModes.CurrentDC).SetRange(0, 3.1)
        Me.FunctionModeRanges.Item(MultimeterFunctionModes.VoltageDC).SetRange(0, 303)
        Me.FunctionModeRanges.Item(MultimeterFunctionModes.VoltageAC).SetRange(0, 303)
        Me.FunctionModeRanges.Item(MultimeterFunctionModes.ResistanceCommonSide).SetRange(0, 120000000.0)
        Me.FunctionModeRanges.Item(MultimeterFunctionModes.ResistanceTwoWire).SetRange(0, 120000000.0)
        Me.FunctionModeRanges.Item(MultimeterFunctionModes.ResistanceFourWire).SetRange(0, 120000000.0)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.ApertureRange = New isr.Core.Primitives.RangeR(0.00000833, 0.25)
        Me.FilterCountRange = New isr.Core.Primitives.RangeI(1, 100)
        Me.FilterWindowRange = New isr.Core.Primitives.RangeR(0, 0.1)
        Me.PowerLineCyclesRange = New isr.Core.Primitives.RangeR(0.0005, 15)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PowerLineCycles = 1
        Me.AutoDelayMode = MultimeterAutoDelayModes.Once
        Me.AutoRangeEnabled = True
        Me.AutoZeroEnabled = True
        Me.FilterCount = 10
        Me.FilterEnabled = False
        Me.MovingAverageFilterEnabled = False
        Me.OpenDetectorEnabled = False
        Me.FilterWindow = 0.001
        Me.FunctionMode = MultimeterFunctionModes.VoltageDC
        Me.Range = 303 'defaults volts range
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " APERTURE "

    ''' <summary> Gets or sets the Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property ApertureQueryCommand As String = "_G.print(_G.dmm.aperture)"

    ''' <summary> Gets or sets the Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overrides Property ApertureCommandFormat As String = "_G.dmm.aperture={0}"

#End Region

#Region " AUTO DELAY MODE "

    ''' <summary> Queries the Multimeter Auto Delay Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' The <see cref="MultimeterAutoDelayModes">Multimeter Auto Delay Mode</see> or none if unknown.
    ''' </returns>
    Public Overrides Function QueryAutoDelayMode() As MultimeterAutoDelayModes?
        Dim mode As String = Me.AutoDelayMode.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.Session.QueryTrimEnd(Tsp.Syntax.Lua.PrintCommandStringIntegerFormat, "_G.dmm.autodelay")
        If String.IsNullOrWhiteSpace(mode) Then
            Dim message As String = "Failed fetching Multimeter Auto Delay Mode"
            Debug.Assert(Not Debugger.IsAttached, message)
            Me.AutoDelayMode = New MultimeterAutoDelayModes?
        Else
            Dim value As MultimeterAutoDelayModes
            If [Enum].TryParse(Of MultimeterAutoDelayModes)(mode, value) Then
                Me.AutoDelayMode = value
            Else
                Dim message As String = $"Failed parsing Multimeter Auto Delay Mode value of '{mode}'"
                Debug.Assert(Not Debugger.IsAttached, message)
                Me.AutoDelayMode = New MultimeterAutoDelayModes?
            End If
        End If
        Return Me.AutoDelayMode
    End Function

    ''' <summary>
    ''' Writes the Multimeter Auto Delay Mode without reading back the value from the device.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The Auto Delay Mode. </param>
    ''' <returns>
    ''' The <see cref="MultimeterAutoDelayModes">Multimeter Auto Delay Mode</see> or none if unknown.
    ''' </returns>
    Public Overrides Function WriteAutoDelayMode(ByVal value As MultimeterAutoDelayModes) As MultimeterAutoDelayModes?
        Me.Session.WriteLine("_G.dmm.autodelay={0}", CInt(value))
        Me.AutoDelayMode = value
        Return Me.AutoDelayMode
    End Function

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = "_G.dmm.autorange={0:'1';'1';'0'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = "_G.print(_G.dmm.autorange==1)"

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = "_G.dmm.autozero={0:'1';'1';'0'}"

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = "_G.print(_G.dmm.autozero==1)"

#End Region

#Region " CONNECT/DISCONNECT "

    ''' <summary> Gets or sets the 'connect' command. </summary>
    ''' <value> The 'connect' command. </value>
    Protected Overrides Property ConnectCommand As String = "if nil ~= dmm then dmm.connect = 7 end"

    ''' <summary> Gets or sets the 'disconnect' command. </summary>
    ''' <value> The 'disconnect' command. </value>
    Protected Overrides Property DisconnectCommand As String = "if nil ~= dmm then dmm.connect = 0 end"

#End Region

#Region " FILTER "

#Region " FILTER COUNT "

    ''' <summary> Gets or sets the Filter Count query command. </summary>
    ''' <value> The FilterCount query command. </value>
    Protected Overrides Property FilterCountQueryCommand As String = "_G.print(_G.dmm.filter.count)"

    ''' <summary> Gets or sets the Filter Count command format. </summary>
    ''' <value> The FilterCount command format. </value>
    Protected Overrides Property FilterCountCommandFormat As String = "_G.dmm.filter.count={0}"

#End Region

#Region " FILTER ENABLED "

    ''' <summary> Gets or sets the Filter enabled command Format. </summary>
    ''' <value> The Filter enabled query command. </value>
    Protected Overrides Property FilterEnabledCommandFormat As String = "_G.dmm.filter.enable={0:'1';'1';'0'}"

    ''' <summary> Gets or sets the Filter enabled query command. </summary>
    ''' <value> The Filter enabled query command. </value>
    Protected Overrides Property FilterEnabledQueryCommand As String = "_G.print(_G.dmm.filter.enable==1)"

#End Region

#Region " MOVING AVERAGE ENABLED "

    ''' <summary> Gets or sets the moving average filter enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property MovingAverageFilterEnabledCommandFormat As String = "_G.dmm.filter.type={0:'0';'0';'1'}"

    ''' <summary> Gets or sets the moving average filter enabled query command. </summary>
    ''' <value> The moving average filter enabled query command. </value>
    Protected Overrides Property MovingAverageFilterEnabledQueryCommand As String = "_G.print(_G.dmm.filter.type==0)"

#End Region

#Region " FILTER Window "

    ''' <summary> Gets or sets the Filter Window query command. </summary>
    ''' <value> The FilterWindow query command. </value>
    Protected Overrides Property FilterWindowQueryCommand As String = "_G.print(_G.dmm.filter.window)"

    ''' <summary> Gets or sets the Filter Window command format. </summary>
    ''' <value> The FilterWindow command format. </value>
    Protected Overrides Property FilterWindowCommandFormat As String = "_G.dmm.filter.window={0}"

#End Region

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = "_G.dmm.func='{0}'"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = "_G.print(_G.dmm.func)"

#End Region

#Region " MEASURE "

    ''' <summary> Gets or sets the Measure query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property MeasureQueryCommand As String = "_G.print(_G.dmm.measure())"

#End Region

#Region " OPEN DETECTOR ENABLED "

    ''' <summary> Gets or sets the open detector enabled command Format. </summary>
    ''' <value> The open detector enabled query command. </value>
    Protected Overrides Property OpenDetectorEnabledCommandFormat As String = "_G.dmm.opendetector={0:'1';'1';'0'}"

    ''' <summary> Gets or sets the open detector enabled query command. </summary>
    ''' <value> The open detector enabled query command. </value>
    Protected Overrides Property OpenDetectorEnabledQueryCommand As String = "_G.print(_G.dmm.opendetector==1)"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = "_G.dmm.nplc={0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = "_G.print(_G.dmm.nplc)"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the Range query command. </summary>
    ''' <value> The Range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = "_G.print(_G.dmm.range)"

    ''' <summary> Gets or sets the Range command format. </summary>
    ''' <value> The Range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = "_G.dmm.range={0}"

#End Region

#End Region

#Region " MEASURE "

    ''' <summary> Reads a value in to the primary reading and converts it to Double. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The measured value or none if unknown. </returns>
    Public Overrides Function MeasurePrimaryReading() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return MyBase.MeasurePrimaryReading()
    End Function

    ''' <summary> Queries the reading and parse the reading amounts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The reading or none if unknown. </returns>
    Public Overrides Function MeasureReadingAmounts() As Double?
        Me.Session.MakeEmulatedReplyIfEmpty(Me.ReadingAmounts.PrimaryReading.Generator.Value.ToString)
        Return MyBase.MeasureReadingAmounts()
    End Function

#End Region

End Class
