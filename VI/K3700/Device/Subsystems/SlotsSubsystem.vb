
''' <summary> The slots subsystem. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Class SlotsSubsystem
    Inherits isr.VI.Tsp.SlotsSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="ChannelSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="maxSlotCount">    A reference to a
    '''                                <see cref="VI.Tsp.StatusSubsystemBase">message based
    '''                                session</see>. </param>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal maxSlotCount As Integer, ByVal statusSubsystem As VI.Tsp.StatusSubsystemBase)
        MyBase.New(maxSlotCount, statusSubsystem)
        Me._SlotList = New List(Of SlotSubsystem)
        For i As Integer = 1 To Me.MaximumSlotCount
            Dim s As New SlotSubsystem(i, statusSubsystem)
            Me._SlotList.Add(s)
        Next
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Gets or sets a list of slots. </summary>
    ''' <value> A list of slots. </value>
    Public ReadOnly Property SlotList As IList(Of SlotSubsystem)

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        For Each s As SlotSubsystemBase In Me.SlotList
            Me.Slots.Add(s)
        Next
        MyBase.InitKnownState()
    End Sub

#End Region


End Class
