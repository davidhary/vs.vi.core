Imports isr.VI.ExceptionExtensions

''' <summary> A resistances meter device using the K3700 instrument. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-03-22 </para>
''' </remarks>
Public Class ResistancesMeterDevice
    Inherits K3700Device

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
        Me._Resistors = New ChannelResistorCollection
        Me._MultimeterSenseChannel = 912
        Me._MultimeterSourceChannel = 921
        Me.SlotCapacity = 30
        Me.ResistorPrefix = "R"
    End Sub

    ''' <summary> Validated the given value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A ResistancesMeterDevice. </returns>
    Public Overloads Shared Function Validated(ByVal device As ResistancesMeterDevice) As ResistancesMeterDevice
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

    ''' <summary> Creates a new Me. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Me. </returns>
    Public Overloads Shared Function Create() As ResistancesMeterDevice
        Dim device As ResistancesMeterDevice = Nothing
        Try
            device = New ResistancesMeterDevice
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

#End Region

#Region " DEVICE "

    ''' <summary>
    ''' Allows the derived device to take actions after initialization is completed.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnInitialized(ByVal e As EventArgs)
        MyBase.OnInitialized(e)
        If Me.IsDeviceOpen Then
            Me.TryConfigureMeter(Me.MultimeterSubsystem.PowerLineCycles.GetValueOrDefault(1))
        End If
        Me.SyncNotifyPropertyChanged(NameOf(ResistancesMeterDevice.Resistors))
    End Sub

    ''' <summary> Checks if measurements are enabled. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> True if the measurement is enabled, false if not. </returns>
    Public Function IsMeasurementEnabled() As Boolean
        Return Me.IsDeviceOpen AndAlso
               MultimeterFunctionModes.ResistanceFourWire = Me.MultimeterSubsystem.FunctionMode.GetValueOrDefault(MultimeterFunctionModes.None) AndAlso
               Me.MultimeterSubsystem.PowerLineCycles.HasValue
    End Function


#End Region

#Region " SUBSYSTEMS "

#Region " MUTLIMETER "

    ''' <summary> True to enable, false to disable the measurement. </summary>
    Private _MeasurementEnabled As Boolean

    ''' <summary> Gets or sets the measurement enabled. </summary>
    ''' <value> The measurement enabled. </value>
    Public Property MeasurementEnabled As Boolean
        Get
            Return Me._MeasurementEnabled
        End Get
        Set(value As Boolean)
            If value <> Me.MeasurementEnabled Then
                Me._MeasurementEnabled = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Number of filters. </summary>
    Private _FilterCount As Integer

    ''' <summary> Gets or sets the number of filters. </summary>
    ''' <value> The number of filters. </value>
    Public Overridable Property FilterCount As Integer
        Get
            Return Me._FilterCount
        End Get
        Set(value As Integer)
            If value <> Me.FilterCount Then
                Me._FilterCount = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The filter window. </summary>
    Private _FilterWindow As Double

    ''' <summary> Gets or sets the filter window. </summary>
    ''' <value> The filter window. </value>
    Public Overridable Property FilterWindow As Double
        Get
            Return Me._FilterWindow
        End Get
        Set(value As Double)
            If value <> Me.FilterWindow Then
                Me._FilterWindow = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The power line cycles. </summary>
    Private _PowerLineCycles As Double

    ''' <summary> Gets or sets the power line cycles. </summary>
    ''' <value> The power line cycles. </value>
    Public Overridable Property PowerLineCycles As Double
        Get
            Return Me._PowerLineCycles
        End Get
        Set(value As Double)
            If value <> Me.PowerLineCycles Then
                Me._PowerLineCycles = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#End Region

#Region " MEASURE "

    ''' <summary> The multimeter sense channel. </summary>
    Private _MultimeterSenseChannel As Integer

    ''' <summary> Gets or sets a list of multimeter channels. </summary>
    ''' <value> A List of multimeter channels. </value>
    Public Property MultimeterSenseChannel As Integer
        Get
            Return Me._MultimeterSenseChannel
        End Get
        Set(value As Integer)
            If Not String.Equals(Me.MultimeterSenseChannel, value) Then
                Me._MultimeterSenseChannel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The multimeter source channel. </summary>
    Private _MultimeterSourceChannel As Integer

    ''' <summary> Gets or sets a list of multimeter channels. </summary>
    ''' <value> A List of multimeter channels. </value>
    Public Property MultimeterSourceChannel As Integer
        Get
            Return Me._MultimeterSourceChannel
        End Get
        Set(value As Integer)
            If Not String.Equals(Me.MultimeterSourceChannel, value) Then
                Me._MultimeterSourceChannel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The slot capacity. </summary>
    Private _SlotCapacity As Integer

    ''' <summary> Gets or sets the slot capacity. </summary>
    ''' <value> The slot capacity. </value>
    Public Property SlotCapacity As Integer
        Get
            Return Me._SlotCapacity
        End Get
        Set(value As Integer)
            If Not Integer.Equals(Me.SlotCapacity, value) Then
                Me._SlotCapacity = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The resistor prefix. </summary>
    Private _ResistorPrefix As String

    ''' <summary> Gets or sets the resistor prefix. </summary>
    ''' <value> The resistor prefix. </value>
    Public Property ResistorPrefix As String
        Get
            Return Me._ResistorPrefix
        End Get
        Set(value As String)
            If Not String.Equals(Me.ResistorPrefix, value) Then
                Me._ResistorPrefix = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets the number of sets of <paramref name="setSize"/> size that fits in a slot.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setSize"> Number of elements in a set. </param>
    ''' <returns> An Integer. </returns>
    Public Function SlotSetCapacity(ByVal setSize As Integer) As Integer
        Return CInt(Math.Floor(Me.SlotCapacity / setSize))
    End Function

    ''' <summary> Slot net capacity. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setSize"> Number of elements in a set. </param>
    ''' <returns> An Integer. </returns>
    Public Function SlotNetCapacity(ByVal setSize As Integer) As Integer
        Return setSize * Me.SlotSetCapacity(setSize)
    End Function

    ''' <summary> Linear relay number. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setNumber">     The set number. </param>
    ''' <param name="ordinalNumber"> The ordinal number within the set. </param>
    ''' <param name="setSize">       Number of elements in a set. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function LinearRelayNumber(ByVal setNumber As Integer, ByVal ordinalNumber As Integer, ByVal setSize As Integer) As Integer
        Return ordinalNumber + (setNumber - 1) * setSize
    End Function

    ''' <summary>
    ''' Get the slot number for the specified resistance set ordinal number and set size.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setNumber">     The set number. </param>
    ''' <param name="ordinalNumber"> The ordinal number within the set. </param>
    ''' <param name="setSize">       Number of. </param>
    ''' <returns> An Integer. </returns>
    Public Function SlotNumber(ByVal setNumber As Integer, ByVal ordinalNumber As Integer, ByVal setSize As Integer) As Integer
        Return 1 + CInt(Math.Floor(ResistancesMeterDevice.LinearRelayNumber(setNumber, ordinalNumber, setSize) / Me.SlotNetCapacity(setSize)))
    End Function

    ''' <summary> Relay number. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setNumber">     The set number. </param>
    ''' <param name="ordinalNumber"> The ordinal number within the set. </param>
    ''' <param name="setSize">       Number of elements in a set. </param>
    ''' <returns> An Integer. </returns>
    Public Function SenseRelayNumber(ByVal setNumber As Integer, ByVal ordinalNumber As Integer, ByVal setSize As Integer) As Integer
        Return ResistancesMeterDevice.LinearRelayNumber(setNumber, ordinalNumber, setSize) Mod Me.SlotNetCapacity(setSize)
    End Function

    ''' <summary> Source relay number. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setNumber">     The set number. </param>
    ''' <param name="ordinalNumber"> The ordinal number within the set. </param>
    ''' <param name="setSize">       Number of elements in a set. </param>
    ''' <returns> An Integer. </returns>
    Public Function SourceRelayNumber(ByVal setNumber As Integer, ByVal ordinalNumber As Integer, ByVal setSize As Integer) As Integer
        Return Me.SlotCapacity + Me.SenseRelayNumber(setNumber, ordinalNumber, setSize)
    End Function

    ''' <summary> Builds channel list. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="setNumber">     The set number. </param>
    ''' <param name="ordinalNumber"> The ordinal number within the set. </param>
    ''' <param name="setSize">       Number of elements in a set. </param>
    ''' <returns> A String. </returns>
    Public Function BuildChannelList(ByVal setNumber As Integer, ByVal ordinalNumber As Integer, ByVal setSize As Integer) As String
        Dim slotBaseNumber As Integer = 1000 * Me.SlotNumber(setNumber, ordinalNumber, setSize)
        Dim relayNumbers As New List(Of Integer) From {
            slotBaseNumber + Me.SenseRelayNumber(setNumber, ordinalNumber, setSize),
            slotBaseNumber + Me.SourceRelayNumber(setNumber, ordinalNumber, setSize),
            slotBaseNumber + Me.MultimeterSenseChannel,
            slotBaseNumber + Me.MultimeterSourceChannel
            }
        relayNumbers.Sort()
        Dim builder As New System.Text.StringBuilder(relayNumbers(0).ToString)
        Dim delimiter As String = ";"
        For i As Integer = 1 To relayNumbers.Count - 1
            builder.Append($"{delimiter}{relayNumbers(i)}")
        Next
        Return builder.ToString
    End Function

    ''' <summary> Populates the given resistors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="prefix">    The prefix. </param>
    ''' <param name="setNumber"> The set number. </param>
    ''' <param name="setSize">   Number of elements in a set. </param>
    Public Sub Populate(ByVal prefix As String, ByVal setNumber As Integer, ByVal setSize As Integer)
        Me._Resistors.Clear()
        For resistorOrdinalNumber As Integer = 1 To setSize
            Me._Resistors.Add(New ChannelResistor($"{prefix}{resistorOrdinalNumber}", Me.BuildChannelList(setNumber, resistorOrdinalNumber, setSize)))
        Next
    End Sub

    ''' <summary> Populates the given resistors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="resistors"> The resistors. </param>
    Public Sub Populate(ByVal resistors As ChannelResistorCollection)
        Me._Resistors = resistors
    End Sub

    ''' <summary> Gets or sets the resistors. </summary>
    ''' <value> The resistors. </value>
    Public ReadOnly Property Resistors As ChannelResistorCollection

    ''' <summary> Configure automatic zero once. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Public Sub ConfigureAutoZeroOnce()
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
#Disable Warning IDE0059 ' Unnecessary assignment of a value
            activity = Me.PublishVerbose($"Configuring auto Zero once")
#Enable Warning IDE0059 ' Unnecessary assignment of a value
            Me.MultimeterSubsystem.AutoZeroOnce()
            activity = Me.PublishVerbose($"Check auto Zero enabled (false if once)")
            Dim autoZero As Boolean? = Me.MultimeterSubsystem.QueryAutoZeroEnabled()
            If autoZero.HasValue Then
                If autoZero.Value Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {False} <> Actual {autoZero.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Configure automatic zero. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="autoZeroEnabled"> True to enable, false to disable the automatic zero. </param>
    Public Sub ConfigureAutoZero(ByVal autoZeroEnabled As Boolean)
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            activity = Me.PublishVerbose($"Configuring auto Zero {autoZeroEnabled}")
            Dim autoZero As Boolean? = Me.MultimeterSubsystem.ApplyAutoZeroEnabled(autoZeroEnabled)
            If autoZero.HasValue Then
                If autoZeroEnabled <> autoZero.Value Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {autoZeroEnabled} <> Actual {autoZero.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Configure function mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="expectedFunctionMode"> The expected function mode. </param>
    Public Sub ConfigureFunctionMode(ByVal expectedFunctionMode As VI.MultimeterFunctionModes)
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            activity = Me.PublishVerbose($"Configuring function mode {expectedFunctionMode}")
            Dim actualFunctionMode As VI.MultimeterFunctionModes? = Me.MultimeterSubsystem.ApplyFunctionMode(expectedFunctionMode)
            If actualFunctionMode.HasValue Then
                If Nullable.Equals(actualFunctionMode, expectedFunctionMode) Then
                    ' changing the function mode changes range, auto delay mode and open detector enabled. 
                    Me.MultimeterSubsystem.QueryAperture()
                    Me.MultimeterSubsystem.QueryRange()
                    Me.MultimeterSubsystem.QueryAutoDelayEnabled()
                    Me.MultimeterSubsystem.QueryOpenDetectorEnabled()
                Else
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedFunctionMode} <> Actual {actualFunctionMode.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Configure power line cycles. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentException">        Thrown when one or more arguments have
    '''                                             unsupported or illegal values. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="powerLineCycles"> The power line cycles. </param>
    Public Sub ConfigurePowerLineCycles(ByVal powerLineCycles As Double)
        If powerLineCycles <= 0 Then Throw New ArgumentException($"Value {powerLineCycles} must be greater than zero", NameOf(powerLineCycles))
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            activity = Me.PublishVerbose($"Configuring power line cycles {powerLineCycles}")
            Dim actualPowerLineCycles As Double? = Me.MultimeterSubsystem.ApplyPowerLineCycles(powerLineCycles)
            If actualPowerLineCycles.HasValue Then
                If powerLineCycles <> actualPowerLineCycles.Value Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {powerLineCycles} <> Actual {actualPowerLineCycles.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Configure automatic range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="autoRangeEnabled"> True to enable, false to disable the automatic range. </param>
    Public Sub ConfigureAutoRange(ByVal autoRangeEnabled As Boolean)
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            activity = Me.PublishVerbose($"Configuring auto range {autoRangeEnabled}")
            Dim autoRange As Boolean? = Me.MultimeterSubsystem.ApplyAutoRangeEnabled(autoRangeEnabled)
            If autoRange.HasValue Then
                If autoRangeEnabled <> autoRange.Value Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {autoRangeEnabled} <> Actual {autoRange.Value}")
                End If
                ' read the range
                Me.MultimeterSubsystem.QueryRange()
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Configure range. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentException">        Thrown when one or more arguments have
    '''                                             unsupported or illegal values. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="range"> The range. </param>
    Public Sub ConfigureRange(ByVal range As Double)
        If range <= 0 Then Throw New ArgumentException($"Value {range} must be greater than zero", NameOf(range))
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            Me.ConfigureAutoRange(False)
            activity = Me.PublishVerbose($"Configuring range {range}")
            Dim actualRange As Double? = Me.MultimeterSubsystem.ApplyRange(range)
            If actualRange.HasValue Then
                If Math.Abs(actualRange.Value / range - 1) > 0.01 Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {range} <> Actual {actualRange.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Configure moving average filter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="filterCount">  Number of filters. </param>
    ''' <param name="filterWindow"> The filter window. </param>
    Public Sub ConfigureMovingAverageFilter(ByVal filterCount As Integer, ByVal filterWindow As Double)
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            Dim expectedFilterEnabled As Boolean = filterCount > 0
            Dim actualFilterEnabled As Boolean?
            If expectedFilterEnabled Then

                ' using repeat filter returns a measurement after the filter takes the expected number of in-Window values.
                Dim expectedMovingAverageFilterEnabled As Boolean = False
                activity = Me.PublishVerbose($"{If(expectedMovingAverageFilterEnabled, "Enabling", "Disabling")} moving average filter")
                Dim actualMovingAverageFilterEnabled As Boolean? = Me.MultimeterSubsystem.ApplyMovingAverageFilterEnabled(expectedMovingAverageFilterEnabled)
                If actualMovingAverageFilterEnabled.HasValue Then
                    If actualMovingAverageFilterEnabled.Value <> expectedMovingAverageFilterEnabled Then
                        Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedMovingAverageFilterEnabled } <> Actual {actualMovingAverageFilterEnabled.Value}")
                    End If
                Else
                    Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
                End If

                activity = Me.PublishVerbose($"Setting filter count {filterCount}")
                Dim actualFilterCount As Integer? = Me.MultimeterSubsystem.ApplyFilterCount(filterCount)
                If actualFilterCount.HasValue Then
                    If actualFilterCount.Value <> filterCount Then
                        Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {filterCount} <> Actual {actualFilterCount.Value}")
                    End If
                Else
                    Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
                End If

                activity = Me.PublishVerbose($"Setting filter window {filterWindow}")
                Dim actualFilterWindow As Double? = Me.MultimeterSubsystem.ApplyFilterWindow(filterWindow)
                If actualFilterWindow.HasValue Then
                    If Math.Abs(actualFilterWindow.Value / filterWindow - 1) > 0.01 Then
                        Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {filterWindow} <> Actual {actualFilterWindow.Value}")
                    End If
                Else
                    Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
                End If
                activity = Me.PublishVerbose($"Enabling filter")
                actualFilterEnabled = Me.MultimeterSubsystem.ApplyFilterEnabled(expectedFilterEnabled)
            Else
                activity = Me.PublishVerbose($"Disabling filter")
                actualFilterEnabled = Me.MultimeterSubsystem.ApplyFilterEnabled(expectedFilterEnabled)
            End If
            If actualFilterEnabled.HasValue Then
                If actualFilterEnabled.Value <> expectedFilterEnabled Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedFilterEnabled} <> Actual {actualFilterEnabled.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--value not set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Measure value into the reading amounts. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <returns> A Double. </returns>
    Public Function MeasureReadingAmounts() As Double
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        Dim result As Double
        If Me.IsDeviceOpen Then
            activity = Me.PublishVerbose($"measuring {Me.MultimeterSubsystem.FunctionMode}")
            If Me.MultimeterSubsystem.MeasureReadingAmounts.HasValue Then
                result = Me.MultimeterSubsystem.PrimaryReadingValue.Value
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}; driver returned nothing")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
        Return result
    End Function

    ''' <summary> Estimates the lower bound on voltage measurement time. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A TimeSpan. </returns>
    Public Function EstimateVoltageMeasurementTime() As TimeSpan
        Dim aperture As Double = Me.PowerLineCycles / Me.StatusSubsystem.LineFrequency.GetValueOrDefault(60)
        Dim timeSeconds As Double

        If Me.FilterCount > 0 AndAlso Me.FilterWindow > 0 Then
            ' if auto zero once is included the time maybe too long
            timeSeconds = aperture * Me.FilterCount
        Else
            ' assumes auto zero
            timeSeconds = aperture * 2
        End If
        Return TimeSpan.FromSeconds(timeSeconds)
    End Function

    ''' <summary> Configure measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    <CodeAnalysis.SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification:="<Pending>")>
    Public Sub ConfigureMeasurement()
        Dim activity As String = $"Checking {Me.ResourceNameCaption} is open"
        If Me.IsDeviceOpen Then
            If Not Nullable.Equals(VI.MultimeterFunctionModes.ResistanceFourWire, Me.MultimeterSubsystem.FunctionMode) Then
                activity = $"Configuring function {VI.MultimeterFunctionModes.ResistanceFourWire}"
                Me.ConfigureFunctionMode(VI.MultimeterFunctionModes.ResistanceFourWire)
            End If
            If Not Nullable.Equals(Me.PowerLineCycles, Me.MultimeterSubsystem.PowerLineCycles) Then
                activity = $"Configuring power line cycles {Me.PowerLineCycles}"
                Me.ConfigurePowerLineCycles(Me.PowerLineCycles)
            End If
            If Nullable.Equals(True, Me.MultimeterSubsystem.AutoZeroEnabled) Then
                activity = $"Setting auto zero once"
                Me.ConfigureAutoZeroOnce()
            End If
            If Not Nullable.Equals(False, Me.MultimeterSubsystem.AutoRangeEnabled) Then
                activity = $"Configuring auto range"
                Me.ConfigureAutoRange(True)
            End If
            If Me.FilterCount > 0 Then
                If Not Nullable.Equals(True, Me.MultimeterSubsystem.FilterEnabled) OrElse
                   Not Nullable.Equals(Me.FilterCount, Me.MultimeterSubsystem.FilterCount) OrElse
                   Not Nullable.Equals(True, Me.MultimeterSubsystem.MovingAverageFilterEnabled) OrElse
                    Not Nullable.Equals(Me.FilterWindow, Me.MultimeterSubsystem.FilterWindow) Then
                    activity = $"Enabling filter"
                    Me.ConfigureMovingAverageFilter(Me.FilterCount, Me.FilterWindow)
                End If
            ElseIf Not Nullable.Equals(False, Me.MultimeterSubsystem.FilterEnabled) Then
                activity = $"Disabling filter"
                Me.ConfigureMovingAverageFilter(Me.FilterCount, Me.FilterWindow)
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Attempts to configure resistance measurement from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryConfigureMeasurement() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = String.Empty
        Try
            activity = Me.PublishVerbose($"Configuring measurement")
            Me.ConfigureMeasurement()
        Catch ex As Exception
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Configure meter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="powerLineCycles"> The power line cycles. </param>
    Public Sub ConfigureMeter(ByVal powerLineCycles As Double)
        Dim activity As String = $"Checking {Me.Session.ResourceNameCaption} Is open"
        If Me.IsDeviceOpen Then
            activity = $"Configuring function mode {MultimeterFunctionModes.ResistanceFourWire}"
            Dim expectedMeasureFunction As MultimeterFunctionModes = MultimeterFunctionModes.ResistanceFourWire
            Dim measureFunction As MultimeterFunctionModes? = Me.MultimeterSubsystem.ApplyFunctionMode(expectedMeasureFunction)
            If measureFunction.HasValue Then
                If Nullable.Equals(measureFunction, expectedMeasureFunction) Then
                    ' changing the function mode changes range, auto delay mode and open detector enabled. 
                    Me.MultimeterSubsystem.QueryAperture()
                    Me.MultimeterSubsystem.QueryRange()
                    Me.MultimeterSubsystem.QueryAutoDelayEnabled()
                    Me.MultimeterSubsystem.QueryOpenDetectorEnabled()
                Else
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedMeasureFunction } <> Actual {measureFunction.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--no value set")
            End If
            activity = $"Configuring power line cycles {powerLineCycles}"
            Dim actualPowerLineCycles As Double? = Me.MultimeterSubsystem.ApplyPowerLineCycles(powerLineCycles)
            If actualPowerLineCycles.HasValue Then
                If powerLineCycles <> actualPowerLineCycles.Value Then
                    Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {powerLineCycles} <> Actual {actualPowerLineCycles.Value}")
                End If
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}--no value set")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device Is Not open")
        End If
    End Sub

    ''' <summary> Attempts to configure meter from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="powerLineCycles"> The power line cycles. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryConfigureMeter(ByVal powerLineCycles As Double) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = $"Configuring resistances meter {Me.Session.ResourceNameCaption}"
        Try
            Me.ConfigureMeter(powerLineCycles)
        Catch ex As Exception
            result = (False, Me.PublishException(activity, ex))
        End Try
        Return result
    End Function

    ''' <summary> Try measure. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resistor"> The resistor. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryMeasure(ByVal resistor As ChannelResistor) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If resistor Is Nothing Then Throw New ArgumentNullException(NameOf(resistor))
        Dim activity As String = String.Empty
        Try
            activity = Me.PublishVerbose($"Configuring measurement")
            Me.ConfigureMeasurement()

            activity = $"{resistor.Title} opening channels"
            Dim expectedChannelList As String = String.Empty
            Dim actualChannelList As String = Me.ChannelSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
            If expectedChannelList <> actualChannelList Then
                Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedChannelList } <> Actual {actualChannelList}")
            End If

            activity = $"{resistor.Title} closing {resistor.ChannelList}"
            expectedChannelList = resistor.ChannelList
            actualChannelList = Me.ChannelSubsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
            If expectedChannelList <> actualChannelList Then
                Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedChannelList } <> Actual {actualChannelList}")
            End If

            activity = $"measuring {resistor.Title}"
            If Me.MultimeterSubsystem.MeasureReadingAmounts.HasValue Then
                resistor.Resistance = Me.MultimeterSubsystem.PrimaryReading.Value.GetValueOrDefault(-1)
                resistor.Status = Me.MultimeterSubsystem.PrimaryReading.MetaStatus
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}; driver returned nothing")
            End If
        Catch ex As Exception
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Try assign measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resistor"> The resistor. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryAssignMeasurement(ByVal resistor As ChannelResistor) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If resistor Is Nothing Then Throw New ArgumentNullException(NameOf(resistor))
        Dim activity As String = String.Empty
        Try
            activity = Me.PublishVerbose($"assigning resistance")
            If Me.MultimeterSubsystem.PrimaryReading.Value.HasValue Then
                resistor.Resistance = Me.MultimeterSubsystem.PrimaryReading.Value.GetValueOrDefault(-1)
                resistor.Status = Me.MultimeterSubsystem.PrimaryReading.MetaStatus
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}; driver returned nothing")
            End If
        Catch ex As Exception
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        End Try
        Return result
    End Function

    ''' <summary> Measure resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
    '''                                             null. </exception>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="resistor"> The resistor. </param>
    Public Sub MeasureResistance(ByVal resistor As ChannelResistor)
        If resistor Is Nothing Then Throw New ArgumentNullException(NameOf(resistor))
        Dim activity As String = $"measuring {resistor.Title}"
        If Me.IsDeviceOpen Then
            activity = $"{resistor.Title} opening channels"
            Dim expectedChannelList As String = String.Empty
            Dim actualChannelList As String = Me.ChannelSubsystem.ApplyOpenAll(TimeSpan.FromSeconds(2))
            If expectedChannelList <> actualChannelList Then
                Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedChannelList } <> Actual {actualChannelList}")
            End If

            activity = $"{resistor.Title} closing {resistor.ChannelList}"
            expectedChannelList = resistor.ChannelList
            actualChannelList = Me.ChannelSubsystem.ApplyClosedChannels(expectedChannelList, TimeSpan.FromSeconds(2))
            If expectedChannelList <> actualChannelList Then
                Throw New isr.Core.OperationFailedException($"Failed {activity} Expected {expectedChannelList } <> Actual {actualChannelList}")
            End If

            activity = $"measuring {resistor.Title}"
            If Me.MultimeterSubsystem.MeasureReadingAmounts.HasValue Then
                resistor.Resistance = Me.MultimeterSubsystem.PrimaryReading.Value.GetValueOrDefault(-1)
                resistor.Status = Me.MultimeterSubsystem.PrimaryReading.MetaStatus
            Else
                Throw New isr.Core.OperationFailedException($"Failed {activity}; driver returned nothing")
            End If
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Attempts to measure resistance from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistor"> The resistor. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryMeasureResistance(ByVal resistor As ChannelResistor) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        If resistor Is Nothing Then Throw New ArgumentNullException(NameOf(resistor))
        Dim activity As String = $"measuring {resistor.Title}"
        Try
            Me.MeasureResistance(resistor)
        Catch ex As Exception
            result = (False, Me.PublishException(activity, ex))
        End Try
        Return result
    End Function

    ''' <summary> Measure resistors. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    Public Sub MeasureResistors()
        Dim activity As String = $"Measuring resistors at {Me.Session.ResourceNameCaption}"
        If Me.IsDeviceOpen Then
            For Each resistor As ChannelResistor In Me.Resistors
                activity = $"Measuring {resistor.Title} at {Me.Session.ResourceNameCaption}"
                Me.MeasureResistance(resistor)
            Next
            Me.SyncNotifyPropertyChanged(NameOf(ResistancesMeterDevice.Resistors))
        Else
            Throw New isr.Core.OperationFailedException($"Failed {activity}; VISA session to this device is not open")
        End If
    End Sub

    ''' <summary> Attempts to measure resistors from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryMeasureResistors() As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = $"Measuring resistors at {Me.Session.ResourceNameCaption}"
        Try
            Me.MeasureResistors()
        Catch ex As Exception
            result = (False, Me.PublishException(activity, ex))
        End Try
        Return result
    End Function

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

