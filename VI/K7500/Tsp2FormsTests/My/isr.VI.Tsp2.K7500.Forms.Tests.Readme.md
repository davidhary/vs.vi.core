## ISR VI TSP2 K7500 Forms Tests Library<sub>&trade;</sub>: Unit Tests Library for the Keithley 7510 TSP2 Mutimeter Forms
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*6.1.7019 2019-03-21*  
Uses the Facade Views framework.

*6.1.6968 2019-01-29*  
Created from K7500 Tests.

*5.0.6754 2018-06-29*  
Adds digital I/O subsystem.

*5.0.6730 2018-06-05*  
Tests passed.

*5.0.6635 2018-03-01*  
Created from K7500 unit Tests. Library.

\(C\) 2017 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)  
[Lua Global Support Libraries](https://bitbucket.org/davidhary/tsp.core)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[IVI VISA](http://www.ivifoundation.org)  
[Test Script Builder](http://www.keithley.com)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
