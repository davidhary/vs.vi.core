''' <summary> Static class for managing the common functions. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Partial Friend NotInheritable Class DeviceManager

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

#End Region

End Class

