Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.WinForms.WindowsFormsExtensions
Imports isr.Core.EnumExtensions
Imports isr.VI.ExceptionExtensions
Imports isr.VI.Facade.ComboBoxExtensions

''' <summary> A Trigger view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class TriggerView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False

        Me._StartTriggerDelayNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._StartTriggerDelayNumeric.NumericUpDownControl.Minimum = 0
        Me._StartTriggerDelayNumeric.NumericUpDownControl.Maximum = 10
        Me._StartTriggerDelayNumeric.NumericUpDownControl.Value = 0.02D

        Me._EndTriggerDelayNumeric.NumericUpDownControl.DecimalPlaces = 3
        Me._EndTriggerDelayNumeric.NumericUpDownControl.Minimum = 0
        Me._EndTriggerDelayNumeric.NumericUpDownControl.Maximum = 10
        Me._EndTriggerDelayNumeric.NumericUpDownControl.Value = 0.0D

        Me._OpenLeadsBitPatternNumeric.NumericUpDownControl.Minimum = 1
        Me._OpenLeadsBitPatternNumeric.NumericUpDownControl.Maximum = 63
        Me._OpenLeadsBitPatternNumeric.NumericUpDownControl.Value = 16

        Me._PassBitPatternNumeric.NumericUpDownControl.Minimum = 1
        Me._PassBitPatternNumeric.NumericUpDownControl.Maximum = 63
        Me._PassBitPatternNumeric.NumericUpDownControl.Value = 32

        Me._FailLimit1BitPatternNumeric.NumericUpDownControl.Minimum = 1
        Me._FailLimit1BitPatternNumeric.NumericUpDownControl.Maximum = 63
        Me._FailLimit1BitPatternNumeric.NumericUpDownControl.Value = 48

        Me._LowerLimit1Numeric.NumericUpDownControl.Minimum = 0
        Me._LowerLimit1Numeric.NumericUpDownControl.Maximum = 5000000D
        Me._LowerLimit1Numeric.NumericUpDownControl.DecimalPlaces = 3
        Me._LowerLimit1Numeric.NumericUpDownControl.Value = 9

        Me._UpperLimit1Numeric.NumericUpDownControl.Minimum = 0
        Me._UpperLimit1Numeric.NumericUpDownControl.Maximum = 5000000D
        Me._UpperLimit1Numeric.NumericUpDownControl.DecimalPlaces = 3
        Me._UpperLimit1Numeric.NumericUpDownControl.Value = 11

        Me._TriggerCountNumeric.NumericUpDownControl.Minimum = 0
        Me._TriggerCountNumeric.NumericUpDownControl.Maximum = 268000000D
        Me._TriggerCountNumeric.NumericUpDownControl.DecimalPlaces = 0
        Me._TriggerCountNumeric.NumericUpDownControl.Value = 10
    End Sub

    ''' <summary> Creates a new <see cref="TriggerView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="TriggerView"/>. </returns>
    Public Shared Function Create() As TriggerView
        Dim view As TriggerView = Nothing
        Try
            view = New TriggerView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K7500Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K7500Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K7500Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindTriggerSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K7500Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindTriggerSubsystem(ByVal device As K7500Device)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._TriggerSubsystem = device.TriggerSubsystem
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K7500.TriggerSubsystem.TriggerState))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K7500.TriggerSubsystem.TriggerCount)
                If subsystem.TriggerCount.HasValue Then
                    Me._TriggerCountNumeric.Value = subsystem.TriggerCount.Value
                End If
            Case NameOf(K7500.TriggerSubsystem.ContinuousEnabled)
                Me._ContinuousTriggerEnabledMenuItem.CheckState = subsystem.ContinuousEnabled.ToCheckState
            Case NameOf(K7500.TriggerSubsystem.TriggerSource)
                If subsystem.TriggerSource.HasValue AndAlso Me._TriggerSourceComboBox.ComboBox.Items.Count > 0 Then
                    Me._TriggerSourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair
                End If
            Case NameOf(K7500.TriggerSubsystem.SupportedTriggerSources)
                Me._TriggerSourceComboBox.ComboBox.ListSupportedTriggerSources(subsystem.SupportedTriggerSources)
                If subsystem.TriggerSource.HasValue AndAlso Me._TriggerSourceComboBox.ComboBox.Items.Count > 0 Then
                    Me._TriggerSourceComboBox.ComboBox.SelectedItem = subsystem.TriggerSource.Value.ValueNamePair
                End If
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: TRIGGER "

    ''' <summary> Fail bit pattern numeric button check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub FailBitPatternNumericButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _FailBitPatternNumericButton.CheckStateChanged
        Me._FailLimit1BitPatternNumeric.NumericUpDownControl.Hexadecimal = Me._FailBitPatternNumericButton.Checked
        Me._FailBitPatternNumericButton.Text = $"Fail {If(Me._FailBitPatternNumericButton.Checked, "0x", "0d")}"
    End Sub

    ''' <summary> Pass bit pattern numeric button check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub PassBitPatternNumericButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _PassBitPatternNumericButton.CheckStateChanged
        Me._PassBitPatternNumeric.NumericUpDownControl.Hexadecimal = Me._PassBitPatternNumericButton.Checked
        Me._PassBitPatternNumericButton.Text = $"Pass {If(Me._FailBitPatternNumericButton.Checked, "0x", "0d")}"
    End Sub

    ''' <summary> Opens leads bit pattern numeric button check state changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub OpenLeadsBitPatternNumericButton_CheckStateChanged(sender As Object, e As EventArgs) Handles _OpenLeadsBitPatternNumericButton.CheckStateChanged
        Me._OpenLeadsBitPatternNumeric.NumericUpDownControl.Hexadecimal = Me._OpenLeadsBitPatternNumericButton.Checked
        Me._OpenLeadsBitPatternNumeric.Text = $"Open {If(Me._FailBitPatternNumericButton.Checked, "0x", "0d")}"
    End Sub

    ''' <summary> Limit 1 decimals numeric value changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub Limit1DecimalsNumeric_ValueChanged(sender As Object, e As EventArgs) Handles _Limit1DecimalsNumeric.ValueChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Me._LowerLimit1Numeric.NumericUpDownControl.DecimalPlaces = CInt(Me._Limit1DecimalsNumeric.Value)
        Me._UpperLimit1Numeric.NumericUpDownControl.DecimalPlaces = CInt(Me._Limit1DecimalsNumeric.Value)
    End Sub

    ''' <summary> Gets the selected trigger source. </summary>
    ''' <value> The selected trigger source. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedTriggerSource() As VI.TriggerSources
        Get
            Return CType(CType(Me._TriggerSourceComboBox.ComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.TriggerSources)
        End Get
    End Property

    ''' <summary> Prepare grade binning model. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub PrepareGradeBinningModel()

        Me.Device.MultimeterSubsystem.ApplyFunctionMode(VI.MultimeterFunctionModes.ResistanceFourWire)

        If Me._LowerLimit1Numeric.Value <= 100 Then
            Me.Device.MultimeterSubsystem.ApplyFilterEnabled(True)
            Me.Device.MultimeterSubsystem.ApplyFilterCount(10)
            ' use repeat filter
            Me.Device.MultimeterSubsystem.ApplyMovingAverageFilterEnabled(False)
            Me.Device.MultimeterSubsystem.ApplyFilterWindow(0.1)
        Else
            Me.Device.MultimeterSubsystem.ApplyFilterEnabled(False)
        End If

        Me.Device.MultimeterSubsystem.ApplyLimit1AutoClear(True)
        Me.Device.MultimeterSubsystem.ApplyLimit1Enabled(True)
        Me.Device.MultimeterSubsystem.ApplyLimit1LowerLevel(Me._LowerLimit1Numeric.Value)
        Me.Device.MultimeterSubsystem.ApplyLimit1UpperLevel(Me._UpperLimit1Numeric.Value)

        ' set limits for open circuit to 10 times the range limit
        Me.Device.MultimeterSubsystem.ApplyLimit2AutoClear(True)
        Me.Device.MultimeterSubsystem.ApplyLimit2Enabled(True)
        Me.Device.MultimeterSubsystem.ApplyLimit2LowerLevel(-10 * Me._UpperLimit1Numeric.Value)
        Me.Device.MultimeterSubsystem.ApplyLimit2UpperLevel(10 * Me._UpperLimit1Numeric.Value)

        ' enable open detection
        Me.Device.MultimeterSubsystem.ApplyOpenDetectorEnabled(True)

        Dim count As Integer = CInt(Me._TriggerCountNumeric.Value)
        ' the buffer must have at least 10 data points
        Me.Device.Buffer1Subsystem.ApplyCapacity(Math.Max(10, count))

        ' clear the buffer 
        Me.Device.Buffer1Subsystem.ClearBuffer()

    End Sub

    ''' <summary> Loads grade bin trigger model button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LoadGradeBinTriggerModelButton_Click(sender As Object, e As EventArgs) Handles _LoadGradeBinTriggerModelMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.Device.IsDeviceOpen Then
                activity = $"{Me.Device.ResourceNameCaption} loading grade binning trigger model" : Me.PublishInfo($"{activity};. ")
                Me.PrepareGradeBinningModel()
                Me.Device.TriggerSubsystem.ApplyGradeBinning(CInt(Me._TriggerCountNumeric.Value),
                                                             TimeSpan.FromSeconds(Me._StartTriggerDelayNumeric.Value),
                                                             CInt(Me._FailLimit1BitPatternNumeric.Value),
                                                             CInt(Me._PassBitPatternNumeric.Value), CInt(Me._OpenLeadsBitPatternNumeric.Value),
                                                             Me.SelectedTriggerSource)
                Me.Device.TriggerSubsystem.QueryTriggerState()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Simple loop load run button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/>
    '''                                             instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub LoadSimpleLoopModelButton_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.Device.IsDeviceOpen Then
                activity = $"{Me.Device.ResourceNameCaption} loading simple loop trigger model" : Me.PublishInfo($"{activity};. ")
                Dim count As Integer = CInt(Me._TriggerCountNumeric.Value)
                Dim startDelay As TimeSpan = TimeSpan.FromSeconds(Me._StartTriggerDelayNumeric.Value)
                Me.Device.TriggerSubsystem.LoadSimpleLoop(count, startDelay)
                Me.Device.TriggerSubsystem.QueryTriggerState()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Executes the 'simple loop trigger model button click' operation. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub RunSimpleLoopTriggerModelButton_Click(sender As Object, e As EventArgs) Handles _RunSimpleLoopMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Initiating simple loop trigger model" : Me.PublishInfo($"{activity};. ")
            ' TO_DO: 
            ' Buffer1 Subsystem: Add buffer readings binding list; bind to the buffer data grid view. 
            ' Dim br As New VI.BufferReadingCollection : br.DisplayReadings(Me._BufferDataGridView, True)
            Me.Device.Buffer1Subsystem.ClearBuffer()
            Me.Device.Buffer1Subsystem.StartElapsedStopwatch()
            Me.Device.TriggerSubsystem.Initiate()
            Me.Device.Session.Wait()
            ' br.Add(Me.Device.Buffer1Subsystem.QueryBufferReadings)
            ' br.DisplayReadings(Me._BufferDataGridView, False)
            Me.Device.Buffer1Subsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the trigger model menu item click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearTriggerModelMenuItem_Click(sender As Object, e As EventArgs) Handles _ClearTriggerModelMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing the trigger model" : Me.PublishInfo($"{activity};. ")
            ' TO_DO: Bind trigger model to the display in place of the channel status. 
            Me.Device.TriggerSubsystem.StartElapsedStopwatch()
            Me.Device.TriggerSubsystem.Abort()
            Me.Device.TriggerSubsystem.ClearTriggerModel()
            Me.Device.Session.Wait()
            Me.Device.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Reads trigger state menu item click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadTriggerStateMenuItem_Click(sender As Object, e As EventArgs) Handles _ReadTriggerStateMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading trigger state" : Me.PublishInfo($"{activity};. ")
            Me.Device.TriggerSubsystem.StartElapsedStopwatch()
            Me.Device.TriggerSubsystem.QueryTriggerState()
            Me.Device.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Meter completer first grading binning menu item click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeterCompleterFirstGradingBinningMenuItem_Click(sender As Object, e As EventArgs) Handles _MeterCompleterFirstGradingBinningMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me.Device.IsDeviceOpen Then
                activity = $"{Me.Device.ResourceNameCaption} loading meter complete first grade binning trigger model" : Me.PublishInfo($"{activity};. ")
                Me.PrepareGradeBinningModel()
                Me.Device.TriggerSubsystem.ApplyMeterCompleteFirstGradeBinning(CInt(Me._TriggerCountNumeric.Value),
                                                             TimeSpan.FromSeconds(Me._StartTriggerDelayNumeric.Value),
                                                             CInt(Me._FailLimit1BitPatternNumeric.Value),
                                                             CInt(Me._PassBitPatternNumeric.Value), CInt(Me._OpenLeadsBitPatternNumeric.Value),
                                                             Me.SelectedTriggerSource)
                Me.Device.TriggerSubsystem.QueryTriggerState()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
