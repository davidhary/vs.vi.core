Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core.EnumExtensions
Imports isr.Core.WinForms.ComboBoxEnumExtensions
Imports isr.VI.Facade.DataGridViewExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A reading view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ReadingView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
        Me._BufferSizeNumeric.NumericUpDownControl.CausesValidation = True
        Me._BufferSizeNumeric.NumericUpDownControl.Minimum = 0
        Me._BufferSizeNumeric.NumericUpDownControl.Maximum = 27500000
    End Sub

    ''' <summary> Creates a new <see cref="ReadingView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As ReadingView
        Dim view As ReadingView = Nothing
        Try
            view = New ReadingView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K7500Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K7500Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K7500Device)
        If Me._Device IsNot Nothing Then
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
        Me.BindBufferSubsystem(value)
        Me.BindMultimeterSubsystem(value)
        Me.BindTriggerSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K7500Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " BUFFER SUBSYSTEM "

    ''' <summary> Gets the Buffer subsystem. </summary>
    ''' <value> The Buffer subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property BufferSubsystem As K7500.BufferSubsystem

    ''' <summary> Bind Buffer subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindBufferSubsystem(ByVal device As K7500.K7500Device)
        If Me.BufferSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.BufferSubsystem)
            Me._BufferSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._BufferSubsystem = device.Buffer1Subsystem
            Me.BindSubsystem(True, Me.BufferSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As BufferSubsystem)
        If add Then Me._BufferDataGridView.Bind(subsystem.BufferReadingsBindingList, True)
        Dim binding As Binding = Me.AddRemoveBinding(Me._BufferSizeNumeric, add, NameOf(NumericUpDown.Value), subsystem, NameOf(BufferSubsystemBase.Capacity))
        ' has to apply the value.
        binding.DataSourceUpdateMode = DataSourceUpdateMode.Never
        Me.AddRemoveBinding(Me._BufferCountLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.ActualPointCount))
        Me.AddRemoveBinding(Me._FirstPointNumberLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.FirstPointNumber))
        Me.AddRemoveBinding(Me._LastPointNumberLabel, add, NameOf(Control.Text), subsystem, NameOf(BufferSubsystemBase.LastPointNumber))
    End Sub

    ''' <summary> Buffer size text box validating. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BufferSizeNumeric_Validating(sender As Object, e As CancelEventArgs) Handles _BufferSizeNumeric.Validating
        Dim activity As String = String.Empty
        Try
            If Me.Device.IsDeviceOpen Then
                Dim value As Integer = CInt(Me._BufferSizeNumeric.Value)
                activity = $"{Me.Device.ResourceNameCaption} setting {GetType(BufferSubsystem)}.{NameOf(BufferSubsystem.Capacity)} to {value}"
                Me.PublishVerbose(activity)
                ' overrides and set to the minimum size: Me._BufferSizeNumeric.Value = Me._BufferSizeNumeric.NumericUpDownControl.Minimum
                Me.Device.Buffer1Subsystem.ApplyCapacity(CInt(value))
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " MUTLIMETER "

    ''' <summary> Gets the Multimeter subsystem. </summary>
    ''' <value> The Multimeter subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MultimeterSubsystem As MultimeterSubsystem

    ''' <summary> Bind Multimeter subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMultimeterSubsystem(ByVal device As K7500Device)
        If Me.MultimeterSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MultimeterSubsystem)
            Me._MultimeterSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._MultimeterSubsystem = device.MultimeterSubsystem
            Me.BindSubsystem(True, Me.MultimeterSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MultimeterSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MultimeterSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K7500.MultimeterSubsystem.ReadingAmounts))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MultimeterSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handles the Multimeter subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As MultimeterSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            ' TO_DO: Bind readings. 
            Case NameOf(K7500.MultimeterSubsystem.ReadingAmounts)
                Try
                    Me.InitializingComponents = True
                    If Me._ReadingComboBox.ComboBox.DataSource Is Nothing Then
                        Me._ReadingComboBox.ComboBox.ListEnumDescriptions(Of VI.ReadingElementTypes)(subsystem.ReadingAmounts.Elements, ReadingElementTypes.Units)
                    End If
                Catch
                    Throw
                Finally
                    Me.InitializingComponents = False
                End Try
        End Select
    End Sub

    ''' <summary> Multimeter subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MultimeterSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            If Me.InvokeRequired Then
                activity = $"invoking {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MultimeterSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"handling {NameOf(MultimeterSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, MultimeterSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindTriggerSubsystem(ByVal device As K7500Device)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._TriggerSubsystem = device.TriggerSubsystem
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K7500.TriggerSubsystem.TriggerState))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K7500.TriggerSubsystem.TriggerState)
                ' TO_DO: Bind trigger state label caption to the Subsystem reading label in the display view.
                ' TO_DO: Move functionality (e.g., handler trigger plan state change) from the user interface to the subsystem
                Me._TriggerStateLabel.Visible = subsystem.TriggerState.HasValue
                If subsystem.TriggerState.HasValue Then
                    Me._TriggerStateLabel.Text = subsystem.TriggerState.Value.ToString
                    If Me.TriggerPlanStateChangeHandlerEnabled Then
                        Me.HandleTriggerPlanStateChange(subsystem.TriggerState.Value)
                    End If
                    If Me.BufferStreamingHandlerEnabled AndAlso Not subsystem.IsTriggerStateActive AndAlso Me._StreamBufferMenuItem.Checked Then
                        Me._StreamBufferMenuItem.Checked = False
                    End If
                End If
                ' ?? this causes a cross thread exception. 
                ' Me._TriggerStateLabel.Invalidate()
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(TriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " STREAM BUFFER "

    ''' <summary> Gets the buffer streaming handler enabled. </summary>
    ''' <value> The buffer streaming handler enabled. </value>
    Private Property BufferStreamingHandlerEnabled As Boolean

    ''' <summary> Stream buffer menu item check state changed. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StreamBufferMenuItem_CheckStateChanged(sender As Object, e As EventArgs) Handles _StreamBufferMenuItem.CheckStateChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            If Me._StreamBufferMenuItem.Checked Then
                Me.BufferStreamingHandlerEnabled = False
                activity = $"{Me.Device.ResourceNameCaption} start buffer streaming" : Me.PublishInfo($"{activity};. ")
                Me._BufferDataGridView.DataSource = Nothing
                Me.Device.TriggerSubsystem.Initiate()
                isr.Core.ApplianceBase.DoEvents()
                Me.Device.Buffer1Subsystem.StartBufferStream(Me.Device.TriggerSubsystem, TimeSpan.FromMilliseconds(5))
                Me.BufferStreamingHandlerEnabled = True
            Else
                Me.BufferStreamingHandlerEnabled = False
                activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan to stop buffer streaming" : Me.PublishInfo($"{activity};. ")
                Me.AbortTriggerPlan(sender)
                Me.Device.TriggerSubsystem.QueryTriggerState()
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS: READING "

#Region " READ "

    ''' <summary> Selects a new reading to display. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> The VI.ReadingElementTypes. </returns>
    Friend Function SelectReading(ByVal value As VI.ReadingElementTypes) As VI.ReadingElementTypes
        If Me.Device.IsDeviceOpen AndAlso (value <> VI.ReadingElementTypes.None) AndAlso (value <> Me.SelectedReadingType) Then
            Me._ReadingComboBox.ComboBox.SelectItem(value.ValueDescriptionPair)
        End If
        Return Me.SelectedReadingType
    End Function

    ''' <summary> Gets the type of the selected reading. </summary>
    ''' <value> The type of the selected reading. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Private ReadOnly Property SelectedReadingType() As VI.ReadingElementTypes
        Get
            Return CType(CType(Me._ReadingComboBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, VI.ReadingElementTypes)
        End Get
    End Property

    ''' <summary> Reading combo box selected value changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadingComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles _ReadingComboBox.SelectedIndexChanged
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} selecting a reading to display"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.Device.MultimeterSubsystem.SelectActiveReading(Me.SelectedReadingType)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishVerbose($"{activity};. ")
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary>
    ''' Event handler. Called by _ReadButton for click events. Query the Device for a reading.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} querying terminal mode" : Me.PublishVerbose($"{activity};. ")
            Me.Device.StartElapsedStopwatch()
            Me.Device.MultimeterSubsystem.QueryFrontTerminalsSelected()
            Me.Device.MultimeterSubsystem.StopElapsedStopwatch()
            activity = $"{Me.Device.ResourceNameCaption} measuring" : Me.PublishVerbose($"{activity};. ")
            Me.Device.StartElapsedStopwatch()
            Me.Device.MultimeterSubsystem.MeasurePrimaryReading()
            Me.Device.MultimeterSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " EVENTS "

    ''' <summary> Toggles auto trigger. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/>
    '''                                             instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AutoInitiateMenuItem_Click(sender As Object, e As EventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} toggling re-trigger mode"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.PublishVerbose($"{activity};. ")
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the trace readings. </summary>
    ''' <value> The trace readings. </value>
    Private ReadOnly Property TraceReadings As New VI.BufferReadingBindingList

#Region " INITIATE AND WAIT "

    ''' <summary> Values that represent trigger plan states. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Enum TriggerPlanState

        ''' <summary> An enum constant representing the none option. </summary>
        None

        ''' <summary> An enum constant representing the started option. </summary>
        Started

        ''' <summary> An enum constant representing the completed option. </summary>
        Completed
    End Enum

    ''' <summary> Gets or sets the trigger plan state change handler enabled. </summary>
    ''' <value> The trigger plan state change handler enabled. </value>
    Private Property TriggerPlanStateChangeHandlerEnabled As Boolean

    ''' <summary> Gets or sets the state of the local trigger plan. </summary>
    ''' <value> The local trigger plan state. </value>
    Private Property LocalTriggerPlanState As TriggerPlanState

    ''' <summary> Handles the trigger plan state change described by triggerState. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="triggerState"> State of the trigger. </param>
    Private Sub HandleTriggerPlanStateChange(ByVal triggerState As VI.TriggerState)
        If triggerState = TriggerState.Running OrElse triggerState = TriggerState.Waiting Then
            Me.LocalTriggerPlanState = TriggerPlanState.Started
        ElseIf triggerState = TriggerState.Idle AndAlso Me.LocalTriggerPlanState = TriggerPlanState.Started Then
            Me.LocalTriggerPlanState = TriggerPlanState.Completed
            Me.TryReadBuffer()
            If Me._RepeatMenuItem.Checked Then
                Me.InitiateMonitorTriggerPlan(True)
            End If
        Else
            Me.LocalTriggerPlanState = TriggerPlanState.None
        End If
    End Sub

    ''' <summary> Try read buffer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryReadBuffer()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading"
        Try
            Me.ReadBuffer()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

    ''' <summary> Reads the buffer. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ReadBuffer()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} fetching buffer count"
        Me.PublishVerbose($"{activity};. ")
        ' this assume buffer is cleared upon each new cycle
        Dim newBufferCount As Integer = Me.Device.Buffer1Subsystem.QueryActualPointCount.GetValueOrDefault(0)
        activity = $"buffer count {newBufferCount}"
        Me.PublishVerbose($"{activity};. ")
        If newBufferCount > 0 Then
            activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Buffer1Subsystem.StartElapsedStopwatch()
            Dim values As IList(Of BufferReading) = Me.Device.Buffer1Subsystem.QueryBufferReadings()
            Me.TraceReadings.Add(values)
            Me.Device.Buffer1Subsystem.StopElapsedStopwatch()
        End If
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Initiate monitor trigger plan. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="stateChangeHandlingEnabled"> True to enable, false to disable the state change
    '''                                           handling. </param>
    Private Sub InitiateMonitorTriggerPlan(ByVal stateChangeHandlingEnabled As Boolean)
        Dim activity As String = $"{Me.Device.ResourceNameCaption} Initiating trigger plan and monitor"
        Me.PublishVerbose($"{activity};. ")
        Me.Device.TriggerSubsystem.Initiate()
        isr.Core.ApplianceBase.DoEvents()
        Me._BufferDataGridView.Bind(Me.TraceReadings, True)
        Me.TriggerPlanStateChangeHandlerEnabled = stateChangeHandlingEnabled
        Me.Device.TriggerSubsystem.AsyncMonitorTriggerState(TimeSpan.FromMilliseconds(5))
    End Sub

    ''' <summary> Try restart trigger plan. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TryRestartTriggerPlan()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} Initiating trigger plan and monitor"
        Try
            Me.InitiateMonitorTriggerPlan(True)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

    ''' <summary> Monitor active trigger plan menu item click. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MonitorActiveTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _MonitorActiveTriggerPlanMenuItem.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} start monitoring trigger plan"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.TriggerPlanStateChangeHandlerEnabled = False
            Me.PublishVerbose($"{activity};. ")
            Me.Device.TriggerSubsystem.AsyncMonitorTriggerState(TimeSpan.FromMilliseconds(5))
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initializes the monitor read repeat menu item click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitMonitorReadRepeatMenuItem_Click(sender As Object, e As EventArgs) Handles _InitMonitorReadRepeatMenuItem.Click
        Dim activity As String = $"{Me.Device.ResourceNameCaption} Initiating trigger plan and monitor"
        Try
            Me.InitiateMonitorTriggerPlan(True)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
        End Try
    End Sub

#End Region

#Region " TRIGGER PLAN EVENT HANDLING "

    ''' <summary> Handles the measurement completed request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/>
    '''                                             instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementCompletedRequest(sender As Object, e As EventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Device.ResourceNameCaption} handling SRQ: {Me.Device.Session.ServiceRequestStatus:X}" : Me.PublishVerbose($"{activity};. ")
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} kludge: reading buffer count"
            Me.PublishVerbose($"{activity};. ")

            ' this assume buffer is cleared upon each new cycle
            Dim newBufferCount As Integer = Me.Device.Buffer1Subsystem.QueryActualPointCount.GetValueOrDefault(0)

            If newBufferCount > 0 Then

                activity = $"{Me.Device.ResourceNameCaption} kludge: buffer has data..."
                Me.PublishVerbose($"{activity};. ")

                activity = $"{Me.Device.ResourceNameCaption} handling measurement available"
                Me.PublishVerbose($"{activity};. ")

                If False Then
                    activity = $"{Me.Device.ResourceNameCaption} fetching a single reading"
                    Me.PublishVerbose($"{activity};. ")
                    Me.Device.MultimeterSubsystem.MeasurePrimaryReading()
                Else
                    activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
                    Me.PublishVerbose($"{activity};. ")
                    Dim values As IList(Of BufferReading) = Me.Device.Buffer1Subsystem.QueryBufferReadings()

                    activity = $"{Me.Device.ResourceNameCaption} updating the display" : Me.PublishVerbose($"{activity};. ")
                    Me.TraceReadings.Add(values)

                    Me._BufferDataGridView.Invalidate()
                    Me.Device.Buffer1Subsystem.StopElapsedStopwatch()
                End If
                If Me._RepeatMenuItem.Checked Then
                    activity = $"{Me.Device.ResourceNameCaption} initiating next measurement(s)" : Me.PublishVerbose($"{activity};. ")
                    Me.Device.Buffer1Subsystem.StartElapsedStopwatch()
                    Me.Device.Buffer1Subsystem.ClearBuffer() ' ?@3 removed 17-7-6
                    Me.Device.TriggerSubsystem.Initiate()
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} trigger plan started; buffer empty"
                Me.PublishVerbose($"{activity};. ")
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._ReadBufferButton, Core.Forma.InfoProviderLevel.Error, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the measurement complete handler added. </summary>
    ''' <value> The measurement complete handler added. </value>
    Private Property MeasurementCompleteHandlerAdded As Boolean

    ''' <summary> Adds measurement complete event handler. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AddMeasurementCompleteEventHandler()

        Dim activity As String = String.Empty
        If Not Me.MeasurementCompleteHandlerAdded Then

            ' clear execution state before enabling events
            activity = $"{Me.Device.ResourceNameCaption} Clearing execution state"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} Enabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.EnableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Adding device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.AddServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning on measurement events"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.StatusSubsystem.ApplyQuestionableEventEnableBitmask(MeasurementEvents.All)
            ' 
            ' if handling buffer full, use the 4917 event to detect buffer full. 

            activity = $"{Me.Device.ResourceNameCaption} Turning on status service request"
            Me.PublishVerbose($"{activity};. ")
            ' Me.Device.StatusSubsystem.EnableServiceRequest(VI.Pith.ServiceRequests.MeasurementEvent)
            Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)

            activity = $"{Me.Device.ResourceNameCaption} Adding re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            AddHandler Me.Device.ServiceRequested, AddressOf Me.HandleMeasurementCompletedRequest
            Me.MeasurementCompleteHandlerAdded = True
        End If
    End Sub

    ''' <summary> Removes the measurement complete event handler. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub RemoveMeasurementCompleteEventHandler()

        Dim activity As String = String.Empty
        If Me.MeasurementCompleteHandlerAdded Then

            activity = $"{Me.Device.ResourceNameCaption} Disabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.DisableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Removing device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.RemoveServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning off measurement events"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.StatusSubsystem.ApplyQuestionableEventEnableBitmask(MeasurementEvents.None)

            activity = $"{Me.Device.ResourceNameCaption} Turning off status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.None)

            activity = $"{Me.Device.ResourceNameCaption} Removing re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.HandleMeasurementCompletedRequest

            Me.MeasurementCompleteHandlerAdded = False

        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by HandleMeasurementEventMenuItem for check state changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleMeasurementEventMenuItem_CheckStateChanged(sender As Object, e As EventArgs)

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan(sender)

            If menuItem.Checked Then

                activity = $"{Me.Device.ResourceNameCaption} Adding measurement completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.AddMeasurementCompleteEventHandler()

            Else

                activity = $"{Me.Device.ResourceNameCaption} Removing measurement completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.RemoveMeasurementCompleteEventHandler()

            End If

        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " BUFFER HANDLER "

    ''' <summary> Handles the buffer full request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleBufferFullRequest(sender As Object, e As EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} handling SRQ: {Me.Device.Session.ServiceRequestStatus:X2}"
            Me.PublishVerbose($"{activity};. ")

            If Me.Device.Session.OperationCompleted Then

                activity = $"{Me.Device.ResourceNameCaption} handling operation completed"
                Me.PublishVerbose($"{activity};. ")

                ' TO_DO: See if can only do a set condition and not read this.
                Dim condition As Integer = Me.Device.StatusSubsystem.QueryOperationEventCondition().GetValueOrDefault(0)
                activity = $"{Me.Device.ResourceNameCaption} OPER: {condition:X2}" : Me.PublishVerbose($"{activity};. ")

                ' If Bit 0 Is set then the buffer is full
                If (condition And (1 << Me.BufferFullOperationConditionBitNumber)) <> 0 Then
                    activity = $"{Me.Device.ResourceNameCaption} handling buffer full"
                    Me.PublishVerbose($"{activity};. ")

                    activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
                    Me.PublishVerbose($"{activity};. ")
                    Dim values As IList(Of BufferReading) = Me.Device.Buffer1Subsystem.QueryBufferReadings()

                    activity = $"{Me.Device.ResourceNameCaption} updating the display" : Me.PublishVerbose($"{activity};. ")
                    Me.TraceReadings.Add(values)
                    Me.Device.Buffer1Subsystem.LastReading = Me.TraceReadings.LastReading
                    Me.Device.Buffer1Subsystem.StopElapsedStopwatch()
                    If Me._RepeatMenuItem.Checked Then
                        activity = $"{Me.Device.ResourceNameCaption} initiating next measurement(s)"
                        Me.PublishVerbose($"{activity};. ")
                        Me.Device.Buffer1Subsystem.StartElapsedStopwatch()
                        Me.Device.Buffer1Subsystem.ClearBuffer() ' ?@# removed 17-7-6
                        Me.Device.TriggerSubsystem.Initiate()
                    End If
                Else
                    activity = $"{Me.Device.ResourceNameCaption} handling buffer clear: NOP"
                    Me.PublishVerbose($"{activity};. ")
                End If
            Else
                activity = $"{Me.Device.ResourceNameCaption} operation not completed"
                Me.PublishVerbose($"{activity};. ")
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(Me._ReadBufferButton, Core.Forma.InfoProviderLevel.Error, ex.Message)
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Gets or sets the Buffer Full handler added. </summary>
    ''' <value> The Buffer Full handler added. </value>
    Private Property BufferFullHandlerAdded As Boolean

    ''' <summary> Gets or sets the buffer full operation condition bit number. </summary>
    ''' <value> The buffer full operation condition bit number. </value>
    Private Property BufferFullOperationConditionBitNumber As Integer

    ''' <summary> Adds Buffer Full event handler. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub AddBufferFullEventHandler()

        Dim activity As String = String.Empty
        If Not Me.BufferFullHandlerAdded Then

            ' clear execution state before enabling events
            activity = $"{Me.Device.ResourceNameCaption} Clearing execution state"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.ClearExecutionState()

            activity = $"{Me.Device.ResourceNameCaption} Enabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.EnableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Adding device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.AddServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning on Buffer events"
            Me.PublishVerbose($"{activity};. ")
            Me.BufferFullOperationConditionBitNumber = 0
            Me.Device.StatusSubsystem.ApplyOperationEventMap(Me.BufferFullOperationConditionBitNumber, Me.Device.StatusSubsystem.BufferFullEventNumber, Me.Device.StatusSubsystem.BufferEmptyEventNumber)
            Me.Device.StatusSubsystem.ApplyOperationEventEnableBitmask(1 << Me.BufferFullOperationConditionBitNumber)

            activity = $"{Me.Device.ResourceNameCaption} Turning on status service request"
            Me.PublishVerbose($"{activity};. ")
            ' Me.Device.StatusSubsystem.EnableServiceRequest(VI.Pith.ServiceRequests.BufferEvent)
            Me.Device.Session.ApplyServiceRequestEnableBitmask(Me.Device.Session.DefaultOperationServiceRequestEnableBitmask)

            activity = $"{Me.Device.ResourceNameCaption} Adding re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            AddHandler Me.Device.ServiceRequested, AddressOf Me.HandleBufferFullRequest
            Me.BufferFullHandlerAdded = True

            Me.TraceReadings.Clear()
            Me._BufferDataGridView.Bind(Me.TraceReadings, True)
        End If
    End Sub

    ''' <summary> Removes the Buffer Full event handler. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub RemoveBufferFullEventHandler()

        Dim activity As String = String.Empty
        If Me.BufferFullHandlerAdded Then

            activity = $"{Me.Device.ResourceNameCaption} Disabling session service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.DisableServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Removing device service request handler"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.RemoveServiceRequestEventHandler()

            activity = $"{Me.Device.ResourceNameCaption} Turning off Buffer events"
            Me.PublishVerbose($"{activity};. ")
            Me.BufferFullOperationConditionBitNumber = 0
            Me.Device.StatusSubsystem.ApplyOperationEventMap(Me.BufferFullOperationConditionBitNumber, 0, 0)
            Me.Device.StatusSubsystem.ApplyOperationEventEnableBitmask(0)

            activity = $"{Me.Device.ResourceNameCaption} Turning off status service request"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.Session.ApplyServiceRequestEnableBitmask(VI.Pith.ServiceRequests.None)

            activity = $"{Me.Device.ResourceNameCaption} Removing re-triggering event handler"
            Me.PublishVerbose($"{activity};. ")
            RemoveHandler Me.Device.ServiceRequested, AddressOf Me.HandleBufferFullRequest

            Me.BufferFullHandlerAdded = False

        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by _HandleBufferEventMenuItem for check state changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleBufferEventMenuItem_CheckStateChanged(sender As Object, e As EventArgs)

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim menuItem As ToolStripMenuItem = TryCast(sender, ToolStripMenuItem)
        If menuItem Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan(sender)

            If menuItem.Checked Then

                activity = $"{Me.Device.ResourceNameCaption} Adding Buffer completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.AddBufferFullEventHandler()

            Else

                activity = $"{Me.Device.ResourceNameCaption} Removing Buffer completion handler"
                Me.PublishVerbose($"{activity};. ")
                Me.RemoveBufferFullEventHandler()

            End If

        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

#End Region

#Region " TRIGGER CONTROLS ON THE READING PANEL "

    ''' <summary> Aborts trigger plan. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/>
    '''                                             instance of this
    '''                                             <see cref="Control"/> </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortTriggerPlan(ByVal sender As System.Object)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.Device.TriggerSubsystem.Abort()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Starts trigger plan. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub StartTriggerPlan(ByVal sender As System.Object)

        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()

            activity = $"{Me.Device.ResourceNameCaption} clearing buffer and display"
            Me.PublishVerbose($"{activity};. ")
            Me._TraceReadings.Clear()
            Me._BufferDataGridView.Bind(Me._TraceReadings, True)

            Me.Device.Buffer1Subsystem.ClearBuffer()

            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.TriggerSubsystem.Initiate()

        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Event handler. Called by the Initiate Button for click events. Initiates a reading for
    ''' retrieval by way of the service request event.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortStartTriggerPlanMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _AbortStartTriggerPlanMenuItem.Click

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Aborting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan(sender)
            activity = $"{Me.Device.ResourceNameCaption} Starting trigger plan" : Me.PublishVerbose($"{activity};. ")
            Me.StartTriggerPlan(sender)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Initiate trigger plan menu item click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub InitiateTriggerPlanMenuItem_Click(sender As Object, e As EventArgs) Handles _InitiateTriggerPlanMenuItem.Click

        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} initiating trigger plan"
            Me.PublishVerbose($"{activity};. ")
            Me.Device.TriggerSubsystem.StartElapsedStopwatch()
            Me.Device.TriggerSubsystem.Initiate()
            Me.Device.TriggerSubsystem.QueryTriggerState()
            Me.Device.TriggerSubsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Abort button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub AbortButton_Click(sender As Object, e As EventArgs) Handles _AbortButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"{Me.Device.ResourceNameCaption} aborting trigger plan"
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.PublishVerbose($"{activity};. ")
            Me.AbortTriggerPlan(sender)
            Me.Device.TriggerSubsystem.QueryTriggerState()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#End Region

#Region " BUFFER "

    ''' <summary> Handles the DataError event of the _dataGridView control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="DataGridViewDataErrorEventArgs"/> instance containing the
    '''                       event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BufferDataGridView_DataError(sender As Object, e As DataGridViewDataErrorEventArgs) Handles _BufferDataGridView.DataError
        Try
            ' prevent error reporting when adding a new row or editing a cell
            Dim grid As DataGridView = TryCast(sender, DataGridView)
            If grid IsNot Nothing Then
                If grid.CurrentRow IsNot Nothing AndAlso grid.CurrentRow.IsNewRow Then Return
                If grid.IsCurrentCellInEditMode Then Return
                If grid.IsCurrentRowDirty Then Return
                Dim activity As String = $"{Me.Device.ResourceNameCaption} exception editing row {e.RowIndex} column {e.ColumnIndex};. {e.Exception.ToFullBlownString}"

                Me.PublishVerbose(activity)
                Me.InfoProvider.Annunciate(grid, isr.Core.Forma.InfoProviderLevel.Error, activity)
            End If
        Catch
        End Try
    End Sub

    ''' <summary> Reads buffer button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadBufferButton_Click(sender As Object, e As EventArgs) Handles _ReadBufferButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} reading buffer" : Me.PublishVerbose($"{activity};. ")
            Me.Device.Buffer1Subsystem.StartElapsedStopwatch()
            Dim br As New VI.BufferReadingBindingList()
            Me._BufferDataGridView.Bind(br, True)
            br.Add(Me.Device.Buffer1Subsystem.QueryBufferReadings)
            Me.Device.Buffer1Subsystem.LastReading = br.LastReading
            Me.Device.Buffer1Subsystem.StopElapsedStopwatch()
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    ''' <summary> Clears the buffer display button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/>
    '''                                             instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ClearBufferDisplayButton_Click(sender As Object, e As EventArgs) Handles _ClearBufferDisplayButton.Click
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} clearing buffer display" : Me.PublishVerbose($"{activity};. ")
            Dim br As New VI.BufferReadingBindingList()
            Me._BufferDataGridView.Bind(br, True)
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class


