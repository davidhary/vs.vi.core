<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class BufferStreamUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._ReadingsDataGridView = New System.Windows.Forms.DataGridView()
        Me._BufferStreamView = New BufferStreamView()
        CType(Me._ReadingsDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_ReadingsDataGridView
        '
        Me._ReadingsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me._ReadingsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._ReadingsDataGridView.Location = New System.Drawing.Point(1, 28)
        Me._ReadingsDataGridView.Name = "_ReadingsDataGridView"
        Me._ReadingsDataGridView.Size = New System.Drawing.Size(375, 324)
        Me._ReadingsDataGridView.TabIndex = 23
        '
        '_BufferStreamView
        '
        Me._BufferStreamView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._BufferStreamView.Dock = System.Windows.Forms.DockStyle.Top
        Me._BufferStreamView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._BufferStreamView.Location = New System.Drawing.Point(1, 1)
        Me._BufferStreamView.Name = "_BufferStreamView"
        Me._BufferStreamView.Size = New System.Drawing.Size(375, 27)
        Me._BufferStreamView.TabIndex = 24
        '
        'BufferView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._ReadingsDataGridView)
        Me.Controls.Add(Me._BufferStreamView)
        Me.Name = "BufferView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(377, 353)
        CType(Me._ReadingsDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _ReadingsDataGridView As Windows.Forms.DataGridView
    Private WithEvents _BufferStreamView As BufferStreamView
End Class
