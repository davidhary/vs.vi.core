Imports System.ComponentModel

''' <summary>
''' A Keithley 7510 edition of the basic <see cref="Facade.BufferStreamView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-01-11 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class BufferStreamView
    Inherits isr.VI.Facade.BufferStreamView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="BufferStreamView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="BufferStreamView"/>. </returns>
    Public Overloads Shared Function Create() As BufferStreamView
        Dim view As BufferStreamView = Nothing
        Try
            view = New BufferStreamView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K7500Device As K7500Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K7500Device)
        MyBase.AssignDevice(value)
        Me._K7500Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, BinningSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SenseFunctionSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SenseSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SystemSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TraceSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase))
            Me.BindSubsystem(CType(Nothing, DigitalOutputSubsystemBase))
            Me.BindSubsystem(CType(Nothing, RouteSubsystemBase))
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem(CType(Nothing, BufferSubsystemBase), String.Empty)
        Else
            Me.BindSubsystem(value.BinningResistanceFourWireSubsystem)
            Me.BindSubsystem(value.SenseFunctionSubsystem)
            Me.BindSubsystem(value.SenseSubsystem)
            Me.BindSubsystem(value.SystemSubsystem)
            Me.BindSubsystem(value.TraceSubsystem)
            Me.BindSubsystem(value.TriggerSubsystem)
            Me.BindSubsystem(value.DigitalOutputSubsystem)
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem1(CType(Nothing, ArmLayerSubsystemBase))
            Me.BindSubsystem(value.RouteSubsystem)
        End If
    End Sub

#End Region

#Region " SENSE FUNCTION HANDLING "

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overrides Sub HandleFunctionModesChanged(ByVal subsystem As SenseSubsystemBase)
        MyBase.HandleFunctionModesChanged(subsystem)
        Select Case subsystem.FunctionMode
            Case VI.SenseFunctionModes.CurrentDC
                Me.BindSubsystem(Me.K7500Device.SenseCurrentSubsystem)
            Case VI.SenseFunctionModes.VoltageDC
                Me.BindSubsystem(Me.K7500Device.SenseVoltageSubsystem)
            Case VI.SenseFunctionModes.ResistanceFourWire
                Me.BindSubsystem(Me.K7500Device.SenseResistanceFourWireSubsystem)
            Case VI.SenseFunctionModes.Resistance
                Me.BindSubsystem(Me.K7500Device.SenseResistanceSubsystem)
        End Select
    End Sub

#End Region

#Region " BUFFER STREAM  "

    ''' <summary> Configure trigger plan. </summary>
    ''' <remarks> David, 2020-07-25. </remarks>
    ''' <param name="triggerCount">  Number of triggers. </param>
    ''' <param name="sampleCount">   Number of samples. </param>
    ''' <param name="triggerSource"> The trigger source. </param>
    Protected Overrides Sub ConfigureTriggerPlan(ByVal triggerCount As Integer, ByVal sampleCount As Integer, ByVal triggerSource As isr.VI.TriggerSources)

        Me.K7500Device.ClearExecutionState()
        Me.K7500Device.TriggerSubsystem.ApplyContinuousEnabled(False)
        Dim triggerState As TriggerState = Me.K7500Device.TriggerSubsystem.QueryTriggerState.GetValueOrDefault(TriggerState.None)
        If TriggerState.Running = triggerState Then
            Me.K7500Device.TriggerSubsystem.Abort()
        End If
        Me.K7500Device.TriggerSubsystem.ClearTriggerModel()
        Me.K7500Device.Session.EnableServiceRequestWaitComplete()
        Me.K7500Device.Session.Wait()
        Me.K7500Device.Session.ReadStatusRegister()

        Me.K7500Device.TriggerSubsystem.ApplyGradeBinning(10000000, TimeSpan.FromMilliseconds(1000 * 0.01),
                                                     isr.VI.Facade.My.Settings.Default.FailBitmask, isr.VI.Facade.My.Settings.Default.PasstBitmask,
                                                     isr.VI.Facade.My.Settings.Default.OverflowBitmask,
                                                     triggerSource)

        Me.K7500Device.TriggerSubsystem.ApplyAutoDelayEnabled(False)

        Me.K7500Device.TriggerSubsystem.QueryTriggerState()

    End Sub

    ''' <summary> Configure digital output. </summary>
    ''' <remarks> David, 2020-08-06. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overrides Sub ConfigureDigitalOutput(ByVal subsystem As DigitalOutputSubsystemBase)
        ' not supported.
    End Sub

    ''' <summary> Configure limit binning. </summary>
    ''' <remarks> David, 2020-07-25. </remarks>
    Protected Overrides Sub ConfigureLimitBinning()
        MyBase.ConfigureLimitBinning()
    End Sub

    ''' <summary> Configure trace for fetching only. </summary>
    ''' <remarks> David, 2020-08-06. </remarks>
    ''' <param name="binningStrokeDuration"> Duration of the binning stroke. </param>
    Protected Overrides Sub ConfigureTrace(ByVal binningStrokeDuration As TimeSpan)
        MyBase.ConfigureTrace(binningStrokeDuration)
        ' the buffer must have at least 10 data points; maximum is 11,000,020 (not 268,000,000)
        ' but then the instrument reports a limit of 10,900,020
        Me.TraceSubsystem.ApplyPointsCount(100)
        ' clear the buffer 
        Me.TraceSubsystem.ClearBuffer()
    End Sub

#End Region

End Class
