Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A Keithley 7510 Device User Interface based on the <see cref="Facade.VisaTreeView"/>. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-30 </para></remarks>
<System.ComponentModel.DisplayName("K7500 User Interface"),
      System.ComponentModel.Description("Keithley 7510 Device User Interface"),
      System.Drawing.ToolboxBitmap(GetType(TreeView))>
Public Class TreeView
    Inherits isr.VI.Facade.VisaTreeView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.Width = 540
        Me.Height = 520
        Me.InitializingComponents = True
        Me._MeterUI = MeterUI.Create : MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Meter", "DMM", Me._MeterUI)
        Me._BufferStreamUI = BufferStreamUI.Create : MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Buffer", "Stream", Me._BufferStreamUI)
        Me._BinningUI = BinningUI.Create : MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Binning", "Binning", Me._BinningUI)
        Me._TriggerUI = TriggerUI.Create : MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Trigger", "Trigger", Me._TriggerUI)
        Me._TraceBufferUI = TraceBuffeView.Create : MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Trace", "Buffer", Me._TraceBufferUI)
        Me._MeasureUI = MeasureUI.Create : MyBase.InsertViewControl(MyBase.ViewsCount - 1, "Measure", "Measure", Me._MeasureUI)
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Public Sub New(ByVal device As K7500Device)
        Me.New()
        Me.AssignDeviceThis(device)
    End Sub

    ''' <summary> Gets the binning user interface. </summary>
    ''' <value> The binning user interface. </value>
    Private ReadOnly Property BinningUI As BinningUI

    ''' <summary> Gets the measure user interface. </summary>
    ''' <value> The measure user interface. </value>
    Private ReadOnly Property MeasureUI As MeasureUI

    ''' <summary> Gets the buffer stream user interface. </summary>
    ''' <value> The buffer stream user interface. </value>
    Private ReadOnly Property BufferStreamUI As BufferStreamUI

    ''' <summary> Gets the meter user interface. </summary>
    ''' <value> The meter user interface. </value>
    Private ReadOnly Property MeterUI As MeterUI

    ''' <summary> Gets the trace buffer user interface. </summary>
    ''' <value> The trace buffer user interface. </value>
    Private ReadOnly Property TraceBufferUI As TraceBuffeView

    ''' <summary> Gets the trigger user interface. </summary>
    ''' <value> The trigger user interface. </value>
    Private ReadOnly Property TriggerUI As TriggerUI

    ''' <summary>
    ''' Releases the unmanaged resources used by the K7500 View and optionally releases the managed
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                If Me.MeasureUI IsNot Nothing Then Me._MeasureUI.Dispose() : Me._MeasureUI = Nothing
                If Me.BufferStreamUI IsNot Nothing Then Me._BufferStreamUI.Dispose() : Me._BufferStreamUI = Nothing
                If Me.BinningUI IsNot Nothing Then Me._BinningUI.Dispose() : Me._BinningUI = Nothing
                If Me.BufferStreamUI IsNot Nothing Then Me._BufferStreamUI.Dispose() : Me._BufferStreamUI = Nothing
                If Me.MeterUI IsNot Nothing Then Me._MeterUI.Dispose() : Me._MeterUI = Nothing
                If Me.TriggerUI IsNot Nothing Then Me._TriggerUI.Dispose() : Me._TriggerUI = Nothing
                If Me.TraceBufferUI IsNot Nothing Then Me._TraceBufferUI.Dispose() : Me._TraceBufferUI = Nothing
                Me.AssignDeviceThis(Nothing)
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K7500Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K7500Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Private Sub AssignDeviceThis(ByVal value As K7500Device)
        If Me._Device IsNot Nothing OrElse MyBase.VisaSessionBase IsNot Nothing Then
            MyBase.StatusView.DeviceSettings = Nothing
            MyBase.StatusView.UserInterfaceSettings = Nothing
            Me._Device = Nothing
        End If
        Me._Device = value
        MyBase.BindVisaSessionBase(value)
        If value IsNot Nothing Then
            Dim listenerCount As Integer = value.Talker.Listeners.Count
            MyBase.StatusView.DeviceSettings = isr.VI.K7500.My.MySettings.Default
            MyBase.StatusView.UserInterfaceSettings = Nothing
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The assigned device or nothing to release the previous assignment. </param>
    Public Overloads Sub AssignDevice(ByVal value As K7500Device)
        Me.AssignDeviceThis(value)
    End Sub

#End Region

#Region " DEVICE EVENT HANDLERS "

    ''' <summary> Executes the device closing action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnDeviceClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        MyBase.OnDeviceClosing(e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            ' release the device before subsystems are disposed
            Me.BinningUI.AssignDevice(Nothing)
            Me.BufferStreamUI.AssignDevice(Nothing)
            Me.MeasureUI.AssignDevice(Nothing)
            Me.MeterUI.AssignDevice(Nothing)
            Me.TraceBufferUI.AssignDevice(Nothing)
            Me.TriggerUI.AssignDevice(Nothing)
        End If
    End Sub

    ''' <summary> Executes the device closed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub OnDeviceClosed()
        MyBase.OnDeviceClosed()
        ' remove binding after subsystems are disposed
        ' because the device closed the subsystems are null and binding will be removed.
        MyBase.DisplayView.BindMeasureToolStrip(Me.Device.MeasureSubsystem)
        MyBase.DisplayView.BindSubsystemToolStrip(Me.Device.TriggerSubsystem)
        MyBase.StatusView.ReadTerminalsState = Nothing
        MyBase.DisplayView.BindTerminalsDisplay(Me.Device.SystemSubsystem)
    End Sub

    ''' <summary> Executes the device opened action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub OnDeviceOpened()
        MyBase.OnDeviceOpened()
        ' assigning device and subsystems after the subsystems are created
        Me.BinningUI.AssignDevice(Me.Device)
        Me.BufferStreamUI.AssignDevice(Me.Device)
        Me.MeasureUI.AssignDevice(Me.Device)
        Me.MeterUI.AssignDevice(Me.Device)
        Me.TraceBufferUI.AssignDevice(Me.Device)
        Me.TriggerUI.AssignDevice(Me.Device)
        MyBase.DisplayView.BindMeasureToolStrip(Me.Device.MeasureSubsystem)
        MyBase.DisplayView.BindSubsystemToolStrip(Me.Device.TriggerSubsystem)
        MyBase.StatusView.ReadTerminalsState = AddressOf Me.Device.SystemSubsystem.QueryFrontTerminalsSelected
        MyBase.DisplayView.BindTerminalsDisplay(Me.Device.SystemSubsystem)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Assigns talker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(talker As isr.Core.ITraceMessageTalker)
        Me.BinningUI.AssignTalker(talker)
        Me.BufferStreamUI.AssignTalker(talker)
        Me.MeasureUI.AssignTalker(talker)
        Me.MeterUI.AssignTalker(talker)
        Me.TraceBufferUI.AssignTalker(talker)
        Me.TriggerUI.AssignTalker(talker)
        ' assigned last as this identifies all talkers.
        MyBase.AssignTalker(talker)
    End Sub

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
