Imports System.ComponentModel

''' <summary>
''' A Keithley 7510 edition of the basic <see cref="Facade.TraceBufferView"/> user interface.
''' </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class TraceBuffeView
    Inherits isr.VI.Facade.TraceBufferView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="TraceBuffeView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="TraceBuffeView"/>. </returns>
    Public Overloads Shared Function Create() As TraceBuffeView
        Dim view As TraceBuffeView = Nothing
        Try
            view = New TraceBuffeView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property K7500Device As K7500Device

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Shadows Sub AssignDevice(ByVal value As K7500Device)
        MyBase.AssignDevice(value)
        Me._K7500Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, TraceSubsystemBase), "Trace")
        Else
            Me.BindSubsystem(Me.K7500Device.TraceSubsystem, "Trace")
        End If
    End Sub

#End Region

End Class
