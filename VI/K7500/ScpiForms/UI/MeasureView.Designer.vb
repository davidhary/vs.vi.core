﻿Imports isr.Core.Forma
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MeasureView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._MeterRangeNumeric = New isr.Core.Controls.NumericUpDown()
        Me._MeterCurrentNumeric = New isr.Core.Controls.NumericUpDown()
        Me._MeterRangeNumericLabel = New System.Windows.Forms.Label()
        Me._MeterCurrentNumericLabel = New System.Windows.Forms.Label()
        Me._TriggerDelayNumericLabel = New System.Windows.Forms.Label()
        Me._TriggerDelayNumeric = New isr.Core.Controls.NumericUpDown()
        Me._ReadButton = New System.Windows.Forms.Button()
        Me._MeasureButton = New System.Windows.Forms.Button()
        Me._MeasuredValueTextBox = New System.Windows.Forms.TextBox()
        Me._MeterRangeComboBox = New isr.Core.Controls.ComboBox()
        Me._MeterRangeComboBoxLabel = New System.Windows.Forms.Label()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        CType(Me._MeterRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._MeterCurrentNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._TriggerDelayNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._Panel.SuspendLayout()
        Me._Layout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_MeterRangeNumeric
        '
        Me._MeterRangeNumeric.BackColor = System.Drawing.SystemColors.Control
        Me._MeterRangeNumeric.DecimalPlaces = 3
        Me._MeterRangeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._MeterRangeNumeric.ForeColor = System.Drawing.SystemColors.WindowText
        Me._MeterRangeNumeric.Location = New System.Drawing.Point(96, 43)
        Me._MeterRangeNumeric.Maximum = New Decimal(New Integer() {1000000000, 0, 0, 0})
        Me._MeterRangeNumeric.Name = "_MeterRangeNumeric"
        Me._MeterRangeNumeric.NullValue = New Decimal(New Integer() {2000000, 0, 0, 0})
        Me._MeterRangeNumeric.ReadOnly = True
        Me._MeterRangeNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._MeterRangeNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._MeterRangeNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._MeterRangeNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._MeterRangeNumeric.Size = New System.Drawing.Size(107, 25)
        Me._MeterRangeNumeric.TabIndex = 3
        Me._MeterRangeNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._MeterRangeNumeric.Value = New Decimal(New Integer() {2000000, 0, 0, 0})
        '
        '_MeterCurrentNumeric
        '
        Me._MeterCurrentNumeric.BackColor = System.Drawing.SystemColors.Control
        Me._MeterCurrentNumeric.DecimalPlaces = 7
        Me._MeterCurrentNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._MeterCurrentNumeric.ForeColor = System.Drawing.SystemColors.WindowText
        Me._MeterCurrentNumeric.Location = New System.Drawing.Point(285, 43)
        Me._MeterCurrentNumeric.Maximum = New Decimal(New Integer() {1, 0, 0, 0})
        Me._MeterCurrentNumeric.Name = "_MeterCurrentNumeric"
        Me._MeterCurrentNumeric.NullValue = New Decimal(New Integer() {0, 0, 0, 0})
        Me._MeterCurrentNumeric.ReadOnly = True
        Me._MeterCurrentNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._MeterCurrentNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._MeterCurrentNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._MeterCurrentNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._MeterCurrentNumeric.Size = New System.Drawing.Size(98, 25)
        Me._MeterCurrentNumeric.TabIndex = 5
        Me._MeterCurrentNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._MeterCurrentNumeric.Value = New Decimal(New Integer() {0, 0, 0, 0})
        '
        '_MeterRangeNumericLabel
        '
        Me._MeterRangeNumericLabel.AutoSize = True
        Me._MeterRangeNumericLabel.Location = New System.Drawing.Point(5, 47)
        Me._MeterRangeNumericLabel.Name = "_MeterRangeNumericLabel"
        Me._MeterRangeNumericLabel.Size = New System.Drawing.Size(88, 17)
        Me._MeterRangeNumericLabel.TabIndex = 2
        Me._MeterRangeNumericLabel.Text = "Range [Ohm]:"
        Me._MeterRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_MeterCurrentNumericLabel
        '
        Me._MeterCurrentNumericLabel.AutoSize = True
        Me._MeterCurrentNumericLabel.Location = New System.Drawing.Point(209, 47)
        Me._MeterCurrentNumericLabel.Name = "_MeterCurrentNumericLabel"
        Me._MeterCurrentNumericLabel.Size = New System.Drawing.Size(74, 17)
        Me._MeterCurrentNumericLabel.TabIndex = 4
        Me._MeterCurrentNumericLabel.Text = "Current [A]:"
        Me._MeterCurrentNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_TriggerDelayNumericLabel
        '
        Me._TriggerDelayNumericLabel.AutoSize = True
        Me._TriggerDelayNumericLabel.Location = New System.Drawing.Point(21, 85)
        Me._TriggerDelayNumericLabel.Name = "_TriggerDelayNumericLabel"
        Me._TriggerDelayNumericLabel.Size = New System.Drawing.Size(72, 17)
        Me._TriggerDelayNumericLabel.TabIndex = 6
        Me._TriggerDelayNumericLabel.Text = "Delay [ms]:"
        Me._TriggerDelayNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_TriggerDelayNumeric
        '
        Me._TriggerDelayNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TriggerDelayNumeric.Location = New System.Drawing.Point(95, 81)
        Me._TriggerDelayNumeric.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me._TriggerDelayNumeric.Name = "_TriggerDelayNumeric"
        Me._TriggerDelayNumeric.NullValue = New Decimal(New Integer() {111, 0, 0, 0})
        Me._TriggerDelayNumeric.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._TriggerDelayNumeric.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._TriggerDelayNumeric.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._TriggerDelayNumeric.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._TriggerDelayNumeric.Size = New System.Drawing.Size(59, 25)
        Me._TriggerDelayNumeric.TabIndex = 7
        Me._TriggerDelayNumeric.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me._TriggerDelayNumeric.Value = New Decimal(New Integer() {111, 0, 0, 0})
        '
        '_ReadButton
        '
        Me._ReadButton.Location = New System.Drawing.Point(160, 79)
        Me._ReadButton.Name = "_ReadButton"
        Me._ReadButton.Size = New System.Drawing.Size(50, 28)
        Me._ReadButton.TabIndex = 8
        Me._ReadButton.Text = "Read"
        Me._ReadButton.UseVisualStyleBackColor = True
        '
        '_MeasureButton
        '
        Me._MeasureButton.Location = New System.Drawing.Point(213, 79)
        Me._MeasureButton.Name = "_MeasureButton"
        Me._MeasureButton.Size = New System.Drawing.Size(68, 28)
        Me._MeasureButton.TabIndex = 9
        Me._MeasureButton.Text = "Measure"
        Me._MeasureButton.UseVisualStyleBackColor = True
        '
        '_MeasuredValueTextBox
        '
        Me._MeasuredValueTextBox.BackColor = System.Drawing.Color.Black
        Me._MeasuredValueTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MeasuredValueTextBox.ForeColor = System.Drawing.Color.Aqua
        Me._MeasuredValueTextBox.Location = New System.Drawing.Point(285, 81)
        Me._MeasuredValueTextBox.Name = "_MeasuredValueTextBox"
        Me._MeasuredValueTextBox.ReadOnly = True
        Me._MeasuredValueTextBox.Size = New System.Drawing.Size(100, 25)
        Me._MeasuredValueTextBox.TabIndex = 10
        Me._MeasuredValueTextBox.Text = "---.-"
        Me._MeasuredValueTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        '_MeterRangeComboBox
        '
        Me._MeterRangeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._MeterRangeComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._MeterRangeComboBox.FormattingEnabled = True
        Me._MeterRangeComboBox.Location = New System.Drawing.Point(54, 4)
        Me._MeterRangeComboBox.Name = "_MeterRangeComboBox"
        Me._MeterRangeComboBox.ReadOnlyBackColor = System.Drawing.SystemColors.Control
        Me._MeterRangeComboBox.ReadOnlyForeColor = System.Drawing.SystemColors.WindowText
        Me._MeterRangeComboBox.ReadWriteBackColor = System.Drawing.SystemColors.Window
        Me._MeterRangeComboBox.ReadWriteForeColor = System.Drawing.SystemColors.ControlText
        Me._MeterRangeComboBox.Size = New System.Drawing.Size(329, 25)
        Me._MeterRangeComboBox.TabIndex = 1
        '
        '_MeterRangeComboBoxLabel
        '
        Me._MeterRangeComboBoxLabel.AutoSize = True
        Me._MeterRangeComboBoxLabel.Location = New System.Drawing.Point(4, 8)
        Me._MeterRangeComboBoxLabel.Name = "_MeterRangeComboBoxLabel"
        Me._MeterRangeComboBoxLabel.Size = New System.Drawing.Size(48, 17)
        Me._MeterRangeComboBoxLabel.TabIndex = 0
        Me._MeterRangeComboBoxLabel.Text = "Range:"
        Me._MeterRangeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._MeterRangeComboBox)
        Me._Panel.Controls.Add(Me._MeasuredValueTextBox)
        Me._Panel.Controls.Add(Me._TriggerDelayNumericLabel)
        Me._Panel.Controls.Add(Me._MeasureButton)
        Me._Panel.Controls.Add(Me._MeterCurrentNumericLabel)
        Me._Panel.Controls.Add(Me._ReadButton)
        Me._Panel.Controls.Add(Me._TriggerDelayNumeric)
        Me._Panel.Controls.Add(Me._MeterRangeNumericLabel)
        Me._Panel.Controls.Add(Me._MeterRangeComboBoxLabel)
        Me._Panel.Controls.Add(Me._MeterCurrentNumeric)
        Me._Panel.Controls.Add(Me._MeterRangeNumeric)
        Me._Panel.Location = New System.Drawing.Point(18, 137)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(389, 112)
        Me._Panel.TabIndex = 11
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(426, 387)
        Me._Layout.TabIndex = 12
        '
        'MeasureView
        '
        Me.BackColor = System.Drawing.Color.Transparent
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "MeasureView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(428, 389)
        CType(Me._MeterRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._MeterCurrentNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._TriggerDelayNumeric, System.ComponentModel.ISupportInitialize).EndInit()

        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me._Layout.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _MeterRangeNumeric As isr.Core.Controls.NumericUpDown
    Private WithEvents _MeterCurrentNumeric As isr.Core.Controls.NumericUpDown
    Private WithEvents _MeterRangeNumericLabel As System.Windows.Forms.Label
    Private WithEvents _MeterCurrentNumericLabel As System.Windows.Forms.Label
    Private WithEvents _TriggerDelayNumericLabel As System.Windows.Forms.Label
    Private WithEvents _TriggerDelayNumeric As isr.Core.Controls.NumericUpDown
    Private WithEvents _MeterRangeComboBox As isr.Core.Controls.ComboBox
    Private WithEvents _MeterRangeComboBoxLabel As System.Windows.Forms.Label
    Private WithEvents _ReadButton As Windows.Forms.Button
    Private WithEvents _MeasureButton As Windows.Forms.Button
    Private WithEvents _MeasuredValueTextBox As Windows.Forms.TextBox
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
End Class
