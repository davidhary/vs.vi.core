Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.VI.Facade.ComboBoxExtensions
Imports isr.Core.WinForms.NumericUpDownExtensions
Imports isr.Core.SplitExtensions
Imports isr.VI.ExceptionExtensions

''' <summary>
''' Measure user interface -- defines the Four Wire Resistance Sense subsystem  settings.
''' </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-03-04 </para>
''' </remarks>
Public Class MeasureUI
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()

        Me.InitializingComponents = True
        ' This call is required by the designer.
        Me.InitializeComponent()

        Me._ReadButton.Text = MeasureUI._ReadLabel
        Me._ReadButton.Enabled = False
        Me._MeasureButton.Enabled = False
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="MeasureUI"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="MeasureUI"/>. </returns>
    Public Shared Function Create() As MeasureUI
        Dim view As MeasureUI = Nothing
        Try
            view = New MeasureUI
            Return view
        Catch
            If view IsNot Nothing Then
                view.Dispose()
            End If

            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As K7500Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K7500Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assign device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K7500Device)
        If Me._Device IsNot Nothing Then
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.PublishInfo($"{value.ResourceNameCaption} assigned to {NameOf(MeasureUI).SplitWords}")
        End If
        Me.BindTriggerSubsystem(value)
        Me.BindSenseSubsystem(value)
        Me.BindMeasureSubsystem(value)
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K7500Device)
        Me.AssignDeviceThis(value)
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets or sets the Trigger subsystem. </summary>
    ''' <value> The Trigger subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property TriggerSubsystem As VI.K7500.TriggerSubsystem

    ''' <summary> Bind Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindTriggerSubsystem(ByVal device As K7500.K7500Device)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._TriggerSubsystem = device.TriggerSubsystem
            Me.BindSubsystem(True, Me.TriggerSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As TriggerSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(VI.K7500.TriggerSubsystem.Delay))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.TriggerSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Trigger subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As TriggerSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K7500.TriggerSubsystem.Delay)
                If subsystem.Delay.HasValue Then
                    Me.TriggerDelay = subsystem.Delay.Value
                End If
        End Select
    End Sub

    ''' <summary> Trigger subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(K7500.TriggerSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.TriggerSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TriggerSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> Gets or sets the Trigger Delay. </summary>
    ''' <value> The Trigger Delay. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property TriggerDelay As TimeSpan
        Get
            Return TimeSpan.FromMilliseconds(CInt(Me._TriggerDelayNumeric.Value))
        End Get
        Set(value As TimeSpan)
            If Not TimeSpan.Equals(value, Me.TriggerDelay) Then
                Me._TriggerDelayNumeric.ValueSetter(CInt(value.TotalMilliseconds))
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Event handler. Called by _TriggerDelayNumeric for value changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub TriggerDelayNumeric_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _TriggerDelayNumeric.ValueChanged
        If Me.InitializingComponents Then Return
        If Me.Device.IsDeviceOpen AndAlso Not Nullable.Equals(Me.TriggerSubsystem.Delay, Me.TriggerDelay) Then
            Me.PublishVerbose($"Applying trigger delay {Me.TriggerDelay};. ")
            Me.TriggerSubsystem.ApplyDelay(Me.TriggerDelay)
            Me.Device.Session.ReadStatusRegister()
        End If
    End Sub

#End Region

#Region " SENSE SUBSYSTEM "

    ''' <summary> Gets or sets the Sense subsystem. </summary>
    ''' <value> The Sense subsystem. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property SenseSubsystem As VI.K7500.SenseSubsystem

    ''' <summary> Bind Sense subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindSenseSubsystem(ByVal device As K7500.K7500Device)
        If Me.SenseSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseSubsystem)
            Me._SenseSubsystem = Nothing
        End If
        If device IsNot Nothing Then
            Me._SenseSubsystem = device.SenseSubsystem
            Me.BindSubsystem(True, Me.SenseSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As SenseSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(K7500.SenseSubsystem.FunctionMode))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseSubsystemPropertyChanged
            Me.BindSenseResistanceSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As SenseSubsystem, ByVal propertyName As String)
        If Me.InitializingComponents OrElse subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K7500.SenseSubsystem.FunctionMode)
                If subsystem.FunctionMode.HasValue Then
                    If subsystem.FunctionMode.Value = VI.SenseFunctionModes.ResistanceFourWire Then
                        Me.BindSenseResistanceSubsystem(Me.Device.SenseResistanceFourWireSubsystem)
                    ElseIf subsystem.FunctionMode.Value = VI.SenseFunctionModes.Resistance Then
                        Me.BindSenseResistanceSubsystem(Me.Device.SenseResistanceSubsystem)
                    End If
                End If
        End Select
    End Sub

    ''' <summary> Sense subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(SenseSubsystem)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseSubsystemPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, SenseSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " SENSE RESISTANCE "

    ''' <summary> Gets or sets the sense resistance subsystem. </summary>
    ''' <value> The sense resistance subsystem. </value>
    Public ReadOnly Property SenseResistanceSubsystem As VI.SenseResistanceSubsystemBase

    ''' <summary> Bind Sense resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSenseResistanceSubsystem(ByVal subsystem As VI.SenseResistanceSubsystemBase)
        If Me.SenseResistanceSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.SenseResistanceSubsystem)
            Me._SenseResistanceSubsystem = Nothing
        End If
        If subsystem IsNot Nothing Then
            Me._SenseResistanceSubsystem = subsystem
            Me.BindSubsystem(True, Me.SenseResistanceSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add; otherwise, remove binding. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As VI.SenseResistanceSubsystemBase)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.SenseResistanceSubsystemBaseChanged
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseResistanceSubsystemBase.Range))
            Me.HandlePropertyChanged(subsystem, NameOf(VI.SenseResistanceSubsystemBase.Current))
            ' 20210621: No commands should be allowed at this point. This caused an error in the Taper Program
            ' and risked modifying device settings.
            ' Me.SenseResistanceSubsystem.ApplyPowerLineCycles(1)
            ' Me.SenseResistanceSubsystem.ApplyAutoRangeEnabled(False)
            ' Me.Device.Session.ReadStatusRegister()
            ' populate the range mode selector
            Dim int As Boolean = Me.InitializingComponents
            Me.InitializingComponents = True
            Me._MeterRangeComboBox.ListResistanceRangeCurrents(subsystem.ResistanceRangeCurrents, New Integer() {0})
            Me.InitializingComponents = int
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.SenseResistanceSubsystemBaseChanged
        End If
    End Sub

    ''' <summary> Handle the Sense subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal subsystem As VI.SenseResistanceSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        ' Me._senseRangeTextBox.SafeTextSetter(Me.Device.SenseRange(VI.ResourceAccessLevels.Cache).ToString(Globalization.CultureInfo.CurrentCulture))
        ' Me._integrationPeriodTextBox.SafeTextSetter(Me.Device.SenseIntegrationPeriodCaption)
        Select Case propertyName
            Case NameOf(VI.SenseResistanceSubsystemBase.Range)
                If subsystem.Range.HasValue Then Me._MeterRangeNumeric.SilentValueSetter(subsystem.Range.Value)
            Case NameOf(VI.SenseResistanceSubsystemBase.Current)
                Me._MeterCurrentNumeric.SilentValueSetter(subsystem.Current)
        End Select
    End Sub

    ''' <summary> Sense function subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SenseResistanceSubsystemBaseChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(VI.SenseResistanceSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.SenseResistanceSubsystemBaseChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SenseResistanceSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the meter current. </summary>
    ''' <value> The meter current. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MeterCurrent As Decimal
        Get
            Return Me._MeterCurrentNumeric.Value
        End Get
        Set(value As Decimal)
            If Not Decimal.Equals(value, Me.MeterCurrent) Then
                Me._MeterCurrentNumeric.SilentValueSetter(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the meter range. </summary>
    ''' <value> The meter range. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property MeterRange As Decimal
        Get
            Return Me._MeterRangeNumeric.Value
        End Get
        Set(value As Decimal)
            If Not Decimal.Equals(value, Me.MeterRange) Then
                Me._MeterRangeNumeric.SilentValueSetter(value)
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the selected meter range. </summary>
    ''' <value> The selected meter range. </value>
    Public ReadOnly Property SelectedMeterRange As ResistanceRangeCurrent
        Get
            Return Me._MeterRangeComboBox.SelectedResistanceRangeCurrent
        End Get
    End Property

    ''' <summary> Selects the meter range based on the range mode description. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="description"> The description. </param>
    ''' <returns> A ResistanceRangeCurrent. </returns>
    Public Function SelectMeterRange(ByVal description As String) As ResistanceRangeCurrent
        Return Me._MeterRangeComboBox.SelectResistanceRangeCurrent(Me.SenseResistanceSubsystem.ResistanceRangeCurrents.MatchResistanceRange(description))
    End Function

    ''' <summary> Selects the meter range based on the range settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="range"> The range. </param>
    ''' <returns> A ResistanceRangeCurrent. </returns>
    Public Function SelectMeterRange(ByVal range As Double) As ResistanceRangeCurrent
        Return Me._MeterRangeComboBox.SelectResistanceRangeCurrent(Me.SenseResistanceSubsystem.ResistanceRangeCurrents.MatchResistanceRange(CDec(range)))
    End Function

    ''' <summary> Selects the meter range based on the range mode description. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="range">   The range. </param>
    ''' <param name="current"> The current. </param>
    ''' <returns> A ResistanceRangeCurrent. </returns>
    Public Function SelectMeterRange(ByVal range As Double, ByVal current As Double) As ResistanceRangeCurrent
        Return Me._MeterRangeComboBox.SelectResistanceRangeCurrent(Me.SenseResistanceSubsystem.ResistanceRangeCurrents.MatchResistanceRange(CDec(range), CDec(current)))
    End Function

    ''' <summary> Gets or sets the range selection as read only. </summary>
    ''' <value> The sentinel indicating if the meter range is read only. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Property RangeReadOnly As Boolean
        Get
            Return Me._MeterRangeComboBox.ReadOnly
        End Get
        Set(value As Boolean)
            If Me.RangeReadOnly <> value Then
                Me._MeterRangeComboBox.ReadOnly = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Executes the meter range changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="resistanceRangeCurrent"> The resistance range current. </param>
    Private Sub OnMeterRangeChanged(ByVal resistanceRangeCurrent As ResistanceRangeCurrent)
        If Me.SenseResistanceSubsystem Is Nothing OrElse Me.SenseResistanceSubsystem.Range.HasValue AndAlso
            Me.SenseResistanceSubsystem.Range.Value <> resistanceRangeCurrent.ResistanceRange Then
            'select a subsystem based on the range.
            If resistanceRangeCurrent.ResistanceRange <= Me.Device.SenseResistanceFourWireSubsystem.ResistanceRangeCurrents.Last.ResistanceRange Then
                Me.BindSenseResistanceSubsystem(Me.Device.SenseResistanceFourWireSubsystem)
            Else
                Me.BindSenseResistanceSubsystem(Me.Device.SenseResistanceSubsystem)
            End If
            Me.SenseResistanceSubsystem.ResistanceRangeCurrent = resistanceRangeCurrent
        End If
    End Sub

    ''' <summary>
    ''' Event handler. Called by _MeterRangeComboBox for selected value changed events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MeterRangeComboBox_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MeterRangeComboBox.SelectedValueChanged
        If Me.InitializingComponents Then Return
        Me.OnMeterRangeChanged(Me._MeterRangeComboBox.SelectedResistanceRangeCurrent)
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Gets or sets the resistance sense subsystem . </summary>
    ''' <value> The Resistance Sense subsystem . </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MeasureSubsystem As VI.K7500.MeasureSubsystem

    ''' <summary> Bind Measure subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Sub BindMeasureSubsystem(ByVal device As K7500.K7500Device)
        If Me.MeasureSubsystem IsNot Nothing Then
            Me.BindSubsystem(False, Me.MeasureSubsystem)
            Me._MeasureSubsystem = Nothing
        End If
        If device Is Nothing Then
            Me._ReadButton.Enabled = False
            Me._MeasureButton.Enabled = False
        Else
            Me._ReadButton.Enabled = True
            Me._MeasureButton.Enabled = True
            Me._MeasureSubsystem = device.MeasureSubsystem
            Me.BindSubsystem(True, Me.MeasureSubsystem)
        End If
    End Sub

    ''' <summary> Bind subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="add">       True to add. </param>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSubsystem(ByVal add As Boolean, ByVal subsystem As MeasureSubsystem)
        If add Then
            AddHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
            Me.HandlePropertyChanged(subsystem, NameOf(VI.K7500.MeasureSubsystem.PrimaryReadingValue))
        Else
            RemoveHandler subsystem.PropertyChanged, AddressOf Me.MeasureSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Measure subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(sender As VI.K7500.MeasureSubsystem, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.K7500.MeasureSubsystem.PrimaryReadingValue)
                Me._MeasuredValueTextBox.Text = sender.PrimaryReadingValue?.ToString("G5")
        End Select
    End Sub

    ''' <summary> Measure subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSubsystemPropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.InitializingComponents OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"Handling {GetType(VI.MeasureSubsystemBase)}.{e.PropertyName} change"
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, System.ComponentModel.PropertyChangedEventArgs)(AddressOf Me.MeasureSubsystemPropertyChanged), New Object() {sender, e})
            Else
                activity = $"Handling {GetType(VI.K7500.MeasureSubsystem)}.{e.PropertyName} change"
                Me.HandlePropertyChanged(TryCast(sender, VI.K7500.MeasureSubsystem), e.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> The read label. </summary>
    Private Const _ReadLabel As String = "Read"

    ''' <summary> Reads button click. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ReadButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ReadButton.Click, _MeasureButton.Click
        Dim activity As String = String.Empty
        Try
            Dim button As Button = TryCast(sender, Button)
            If button IsNot Nothing Then
                Me.Cursor = Cursors.WaitCursor
                activity = $"{Me.Device.ResourceNameCaption} reading"
                Me.PublishInfo($"{activity};. {Me.Device.ResourceNameCaption}")
                Me.Device.RouteSubsystem.QueryTerminalsMode()
                If String.Equals(button.Text, _ReadLabel, StringComparison.OrdinalIgnoreCase) Then
                    Me.MeasureSubsystem.Read()
                Else
                    Me.MeasureSubsystem.MeasurePrimaryReading()
                End If

            End If
        Catch ex As Exception
            Me.Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.Device.Session.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

