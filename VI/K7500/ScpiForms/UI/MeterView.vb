Imports isr.VI.Facade.DataGridViewExtensions

''' <summary>
''' A Keithley 7510 edition of the basic <see cref="Facade.MeterView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-01-11 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class MeterView
    Inherits isr.VI.Facade.MeterView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new <see cref="MeterView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="MeterView"/>. </returns>
    Public Overloads Shared Function Create() As MeterView
        Dim view As MeterView = Nothing
        Try
            view = New MeterView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K7500Device As K7500Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K7500Device)
        MyBase.AssignDevice(value)
        Me._K7500Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, BufferSubsystemBase))
            Me.BindSubsystem(CType(Nothing, FormatSubsystemBase))
            Me.BindSubsystem(CType(Nothing, MeasureSubsystemBase))
            Me.BindSubsystem(CType(Nothing, MultimeterSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SenseSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TraceSubsystemBase))
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase))
            Me.BindSubsystem(CType(Nothing, SystemSubsystemBase))
        Else
            Me.BindSubsystem(value.SystemSubsystem)
            Me.BindSubsystem(value.SenseSubsystem)
            ' Me.BindSubsystem(value.BufferSubsystem)
            Me.BindSubsystem(value.FormatSubsystem)
            Me.BindSubsystem(value.MeasureSubsystem)
            ' Me.BindSubsystem(value.MultimeterSubsystem)
            Me.BindSubsystem(value.TraceSubsystem)
            Me.BindSubsystem(value.TriggerSubsystem)
        End If
    End Sub

#End Region

#Region " SENSE FUNCTION HANDLING "

    ''' <summary> Handles the function modes changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Protected Overrides Sub HandleFunctionModesChanged(ByVal subsystem As SenseSubsystemBase)
        MyBase.HandleFunctionModesChanged(subsystem)
        Select Case subsystem.FunctionMode
            Case VI.SenseFunctionModes.CurrentDC
                Me.BindSubsystem(Me.K7500Device.SenseCurrentSubsystem, "Current")
            Case VI.SenseFunctionModes.VoltageDC
                Me.BindSubsystem(Me.K7500Device.SenseVoltageSubsystem, "Voltage")
            Case VI.SenseFunctionModes.ResistanceFourWire
                Me.BindSubsystem(Me.K7500Device.SenseResistanceFourWireSubsystem, "Ohm 4W")
            Case VI.SenseFunctionModes.Resistance
                Me.BindSubsystem(Me.K7500Device.SenseResistanceSubsystem, "Ohm")
        End Select
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Fetches and displays buffered readings. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub FetchAndDisplayBufferedReadings()
        Dim activity As String = String.Empty
        Try
            Me.PublishVerbose($"{activity};. ")
            Me.Cursor = Windows.Forms.Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} fetching buffered readings"
            Me.PublishVerbose($"{activity};. ")
            Me.DisplayBufferedReadings(Me.K7500Device.TraceSubsystem.QueryReadings(Me.K7500Device.MeasureSubsystem.ReadingAmounts))
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Cursor = Windows.Forms.Cursors.Default
        End Try

    End Sub

#End Region

End Class
