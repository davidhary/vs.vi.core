Imports System.Windows.Forms

Imports isr.Core.SplitExtensions

''' <summary>
''' A Keithley 7510 edition of the basic <see cref="Facade.TriggerView"/> user interface.
''' </summary>
''' <remarks>
''' David, 2020-01-11 <para>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class TriggerView
    Inherits isr.VI.Facade.TriggerView

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Creates a new <see cref="TriggerView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="TriggerView"/>. </returns>
    Public Overloads Shared Function Create() As TriggerView
        Dim view As TriggerView = Nothing
        Try
            view = New TriggerView
            Return view
        Catch
            view?.Dispose()
            Throw
        End Try
    End Function

#End Region

#Region " DEVICE "

    ''' <summary> Gets or sets the device for this class. </summary>
    ''' <value> The device for this class. </value>
    Private ReadOnly Property K7500Device As K7500Device

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Shadows Sub AssignDevice(ByVal value As K7500Device)
        MyBase.AssignDevice(value)
        Me._K7500Device = value
        If value Is Nothing Then
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 1)
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 2)
            Me.BindSubsystem(CType(Nothing, TriggerSubsystemBase), 1)
        Else
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 1)
            Me.BindSubsystem(CType(Nothing, ArmLayerSubsystemBase), 2)
            Me.BindSubsystem(Me.K7500Device.TriggerSubsystem, 1)
        End If
    End Sub

#End Region

#Region " INITIATE WAIT FETCH "

    ''' <summary> Initiate wait read. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overrides Sub InitiateWaitRead()
        If Me.InitializingComponents Then Return
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            activity = $"{Me.K7500Device.ResourceNameCaption} clearing execution state"
            Me.K7500Device.ClearExecutionState()

            activity = $"{Me.K7500Device.ResourceNameCaption} initiating wait read"

            ' Me.K7500device.ClearExecutionState()
            ' set the service request
            ' Me.K7500device.StatusSubsystem.ApplyMeasurementEventEnableBitmask(MeasurementEvents.All)
            ' Me.K7500device.StatusSubsystem.EnableServiceRequest(Me.K7500device.Session.DefaultOperationServiceRequestEnableBitmask)
            ' Me.K7500device.Session.Write("*SRE 1") ' Set MSB bit of SRE register
            ' Me.K7500device.Session.Write("stat:meas:ptr 32767; ntr 0; enab 512") ' Set all PTR bits and clear all NTR bits for measurement events Set Buffer Full bit of Measurement
            ' Me.K7500device.Session.Write(":trac:feed calc") ' Select Calculate as reading source
            ' Me.K7500device.Session.Write(":trac:poin 10")   ' Set buffer size to 10 points 
            ' Me.K7500device.Session.Write(":trac:egr full")  ' Select Full element group

            ' trigger the initiation of the measurement letting the triggering or service request do the rest.
            activity = $"{Me.K7500Device.ResourceNameCaption} Initiating meter" : Me.PublishVerbose($"{activity};. ")
            Me.K7500Device.TriggerSubsystem.Initiate()
        Catch ex As Exception
            Me.K7500Device.Session.StatusPrompt = $"failed {activity}"
            activity = Me.PublishException(activity, ex)
            Me.InfoProvider.Annunciate(Me, isr.Core.Forma.InfoProviderLevel.Error, activity)
        Finally
            Me.ReadStatusRegister()
            Me.Cursor = Cursors.Default
        End Try
    End Sub

#End Region

End Class
