<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TraceBufferUI

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._TraceBufferView = New isr.VI.K7500.Forms.TraceBuffeView()
        Me.SuspendLayout()
        '
        '_TraceBufferView
        '
        Me._TraceBufferView.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me._TraceBufferView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._TraceBufferView.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._TraceBufferView.Location = New System.Drawing.Point(1, 1)
        Me._TraceBufferView.Name = "_TraceBufferView"
        Me._TraceBufferView.Padding = New System.Windows.Forms.Padding(1)
        Me._TraceBufferView.Size = New System.Drawing.Size(407, 272)
        Me._TraceBufferView.TabIndex = 0
        '
        'TraceBufferUI
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me._TraceBufferView)
        Me.Name = "TraceBufferUI"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(409, 274)
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _TraceBufferView As TraceBuffeView
End Class
