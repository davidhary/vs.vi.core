''' <summary> Status subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits VI.Tsp2.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> The session. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(session)
        Me.VersionInfoBase = New VersionInfo
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Define measurement events bitmasks. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Overloads Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.MeasurementEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        Dim failuresSummaryBitmask As Integer = 0
        Dim bitmask As Integer
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.Questionable, 1 << 0)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.Origin, 1 << 1 Or 1 << 2)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.MeasurementTerminal, 1 << 3)
        bitmask = 1 << 4 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit2, bitmask)
        bitmask = 1 << 5 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit2, bitmask)
        bitmask = 1 << 6 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit1, bitmask)
        bitmask = 1 << 7 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit1, bitmask)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.FirstReadingInGroup, 1 << 8)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.FailuresSummary, failuresSummaryBitmask)
    End Sub

    ''' <summary> Define measurement events bit values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub DefineMeasurementEventsBitmasks()
        MyBase.DefineMeasurementEventsBitmasks()
        StatusSubsystem.DefineBitmasks(Me.MeasurementEventsBitmasks)
    End Sub

    ''' <summary> Define operation event bitmasks. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Overloads Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.OperationEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        bitmaskDictionary.Add(OperationEventBitmaskKey.Arming, 1 << 6)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Calibrating, 1 << 0)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Idle, 1 << 10)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Measuring, 1 << 4)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Triggering, 1 << 5)
    End Sub

    ''' <summary> Defines the operation event bit values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub DefineOperationEventsBitmasks()
        MyBase.DefineOperationEventsBitmasks()
        StatusSubsystem.DefineBitmasks(Me.OperationEventsBitmasks)
    End Sub

#End Region

End Class

''' <summary> Gets or sets the status byte flags of the measurement event register. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum MeasurementEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the reading overflow option. </summary>
    <ComponentModel.Description("Reading Overflow")>
    ReadingOverflow = 1

    ''' <summary> An enum constant representing the low limit 1 failed option. </summary>
    <ComponentModel.Description("Low Limit 1 Failed")>
    LowLimit1Failed = 2

    ''' <summary> An enum constant representing the high limit 1 failed option. </summary>
    <ComponentModel.Description("High Limit 1 Failed")>
    HighLimit1Failed = 4

    ''' <summary> An enum constant representing the low limit 2 failed option. </summary>
    <ComponentModel.Description("Low Limit 2 Failed")>
    LowLimit2Failed = 8

    ''' <summary> An enum constant representing the high limit 2 failed option. </summary>
    <ComponentModel.Description("High Limit 2 Failed")>
    HighLimit2Failed = 16

    ''' <summary> An enum constant representing the reading available option. </summary>
    <ComponentModel.Description("Reading Available")>
    ReadingAvailable = 32

    ''' <summary> An enum constant representing the not used 1 option. </summary>
    <ComponentModel.Description("Not Used 1")>
    NotUsed1 = 64

    ''' <summary> An enum constant representing the buffer available option. </summary>
    <ComponentModel.Description("Buffer Available")>
    BufferAvailable = 128

    ''' <summary> An enum constant representing the buffer half full option. </summary>
    <ComponentModel.Description("Buffer Half Full")>
    BufferHalfFull = 256

    ''' <summary> An enum constant representing the buffer full option. </summary>
    <ComponentModel.Description("Buffer Full")>
    BufferFull = 512

    ''' <summary> An enum constant representing the buffer overflow option. </summary>
    <ComponentModel.Description("Buffer Overflow")>
    BufferOverflow = 1024

    ''' <summary> An enum constant representing the hardware limit event option. </summary>
    <ComponentModel.Description("HardwareLimit Event")>
    HardwareLimitEvent = 2048

    ''' <summary> An enum constant representing the buffer quarter full option. </summary>
    <ComponentModel.Description("Buffer Quarter Full")>
    BufferQuarterFull = 4096

    ''' <summary> An enum constant representing the buffer three quarters full option. </summary>
    <ComponentModel.Description("Buffer Three-Quarters Full")>
    BufferThreeQuartersFull = 8192

    ''' <summary> An enum constant representing the master limit option. </summary>
    <ComponentModel.Description("Master Limit")>
    MasterLimit = 16384

    ''' <summary> An enum constant representing the not used 2 option. </summary>
    <ComponentModel.Description("Not Used 2")>
    NotUsed2 = 32768

    ''' <summary> An enum constant representing all option. </summary>
    <ComponentModel.Description("All")>
    All = 32767
End Enum

