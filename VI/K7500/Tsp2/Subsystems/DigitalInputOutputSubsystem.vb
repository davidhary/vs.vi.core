''' <summary> Digital Input Output subsystem. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-06-29 </para>
''' </remarks>
Public Class DigitalInputOutputSubsystem
    Inherits VI.Tsp2.DigitalInputOutputSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DigitalInputOutputSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.Tsp2.StatusSubsystemBase">status
    '''                                Subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Resets the known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ResetKnownStateThis()
        Me.DigitalLines = New DigitalLineCollection(6, False)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " DIGITAL LINE MODE "

    ''' <summary> Get the Digital Line Mode query command. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lineNumber"> The line n umber. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function DigitalLineModeQueryCommand(ByVal lineNumber As Integer) As String
        Return $"_G.print(_G.digio.line[{lineNumber}].mode)"
    End Function

    ''' <summary> Gets Digital Line Mode command format. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lineNumber"> The line n umber. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function DigitalLineModeCommandFormat(ByVal lineNumber As Integer) As String
        Return $"_G.digio.line[{lineNumber}].mode={{0}}"
    End Function

#End Region

#Region " DIGITAL LINE RESET "

    ''' <summary> Digital line reset command. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lineNumber"> The line number. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function DigitalLineResetCommand(ByVal lineNumber As Integer) As String
        Return $"_G.digio.line[{lineNumber}].reset()"
    End Function

#End Region

#Region " DIGITAL LINE STATE "

    ''' <summary> Get the Digital Line State query command. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lineNumber"> The line n umber. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function DigitalLineStateQueryCommand(ByVal lineNumber As Integer) As String
        Return $"_G.print(_G.digio.line[{lineNumber}].state)"
    End Function

    ''' <summary> Gets Digital Line State command format. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lineNumber"> The line n umber. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function DigitalLineStateCommandFormat(ByVal lineNumber As Integer) As String
        Return $"_G.digio.line[{lineNumber}].state={{0}}"
    End Function

#End Region

#Region " READ PORT "

    ''' <summary> Gets or sets the level command format. </summary>
    ''' <value> The level command format. </value>
    Protected Overrides Property LevelCommandFormat As String = "_G.digio.writeport({0})"

    ''' <summary> Gets or sets the level query command. </summary>
    ''' <value> The level query command. </value>
    Protected Overrides Property LevelQueryCommand As String = "_G.print(_G.digio.readport())"

#End Region

#End Region
End Class
