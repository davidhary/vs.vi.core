Imports isr.Core.TimeSpanExtensions
Imports isr.VI.ExceptionExtensions

''' <summary> A 7500 device. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class K7500Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="k7500Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        If statusSubsystem IsNot Nothing Then
            statusSubsystem.ExpectedLanguage = VI.Pith.Ieee488.Syntax.LanguageTsp
            AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.HandleSettingsPropertyChanged
        End If
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As K7500Device
        Dim device As K7500Device = Nothing
        Try
            device = New K7500Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As K7500Device) As K7500Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
            ' release unmanaged-only resources.
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(K7500Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindDigitalInputOutputSubsystem(Nothing)
            Me.BindTriggerSubsystem(Nothing)
            Me.BindBuffer1Subsystem(Nothing)
            Me.BindMultimeterSubsystem(Nothing)
            Me.BindDisplaySubsystem(Nothing)
            Me.BindLocalNodeSubsystem(Nothing)
            Me.BindSystemSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel Then
            e.Cancel = Not Me.StatusSubsystem.TryApplyExpectedLanguage().Success
            If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
                Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
                Me.BindLocalNodeSubsystem(New LocalNodeSubsystem(Me.StatusSubsystem))
                Me.BindDisplaySubsystem(New DisplaySubsystem(Me.StatusSubsystem))
                Me.BindMultimeterSubsystem(New MultimeterSubsystem(Me.StatusSubsystem))
                Me.BindTriggerSubsystem(New TriggerSubsystem(Me.StatusSubsystem))

                Me.BindBuffer1Subsystem(New BufferSubsystem(VI.Tsp2.BufferSubsystemBase.DefaultBuffer1Name, Me.StatusSubsystem))
                Me.BindDigitalInputOutputSubsystem(New DigitalInputOutputSubsystem(Me.StatusSubsystemBase))
            End If
        End If
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#Region " BUFFER1 "

    ''' <summary> Gets or sets the Buffer1 Subsystem. </summary>
    ''' <value> The Buffer1 Subsystem. </value>
    Public ReadOnly Property Buffer1Subsystem As BufferSubsystem

    ''' <summary> Binds the Buffer1 subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindBuffer1Subsystem(ByVal subsystem As BufferSubsystem)
        If Me.Buffer1Subsystem IsNot Nothing Then
            RemoveHandler Me.Buffer1Subsystem.PropertyChanged, AddressOf Me.BufferSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.Buffer1Subsystem)
            Me.Buffer1Subsystem.Dispose()
            Me._Buffer1Subsystem = Nothing
        End If
        Me._Buffer1Subsystem = subsystem
        If Me.Buffer1Subsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.Buffer1Subsystem)
            AddHandler Me.Buffer1Subsystem.PropertyChanged, AddressOf Me.BufferSubsystemPropertyChanged
        End If
    End Sub

    ''' <summary> Handle the Buffer subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As BufferSubsystemBase, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(VI.BufferSubsystemBase.LastReading)
                Me.MultimeterSubsystem.ParsePrimaryReading(subsystem.LastReading?.Reading)
        End Select
    End Sub

    ''' <summary> Buffer subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event inBufferion. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub BufferSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(BufferSubsystemBase)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, BufferSubsystemBase), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DIGITAL INPUT OUTPUT "

    ''' <summary> Gets or sets the Digital Input Output Subsystem. </summary>
    ''' <value> The Digital Input Output Subsystem. </value>
    Public ReadOnly Property DigitalInputOutputSubsystem As DigitalInputOutputSubsystem

    ''' <summary> Binds the Digital Input Output subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindDigitalInputOutputSubsystem(ByVal subsystem As DigitalInputOutputSubsystem)
        If Me.DigitalInputOutputSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.DigitalInputOutputSubsystem)
            Me._DigitalInputOutputSubsystem = Nothing
        End If
        Me._DigitalInputOutputSubsystem = subsystem
        If Me.DigitalInputOutputSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.DigitalInputOutputSubsystem)
        End If
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Gets or sets the Display Subsystem. </summary>
    ''' <value> The Display Subsystem. </value>
    Public ReadOnly Property DisplaySubsystem As DisplaySubsystem

    ''' <summary> Binds the Display subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindDisplaySubsystem(ByVal subsystem As DisplaySubsystem)
        If Me.DisplaySubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.DisplaySubsystem)
            Me._DisplaySubsystem = Nothing
        End If
        Me._DisplaySubsystem = subsystem
        If Me.DisplaySubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.DisplaySubsystem)
        End If
    End Sub

#End Region

#Region " LOCAL NODE "

    ''' <summary> Gets or sets the Local Node Subsystem. </summary>
    ''' <value> The Local Node Subsystem. </value>
    Public ReadOnly Property LocalNodeSubsystem As LocalNodeSubsystem

    ''' <summary> Binds the Local Node subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindLocalNodeSubsystem(ByVal subsystem As LocalNodeSubsystem)
        If Me.LocalNodeSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.LocalNodeSubsystem)
            Me._LocalNodeSubsystem = Nothing
        End If
        Me._LocalNodeSubsystem = subsystem
        If Me.LocalNodeSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.LocalNodeSubsystem)
        End If
    End Sub

#End Region

#Region " MULTIMETER "

    ''' <summary> Gets or sets the Multimeter Subsystem. </summary>
    ''' <value> The Multimeter Subsystem. </value>
    Public ReadOnly Property MultimeterSubsystem As MultimeterSubsystem

    ''' <summary> Binds the Multimeter subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindMultimeterSubsystem(ByVal subsystem As MultimeterSubsystem)
        If Me.MultimeterSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.MultimeterSubsystem)
            Me._MultimeterSubsystem = Nothing
        End If
        Me._MultimeterSubsystem = subsystem
        If Me.MultimeterSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.MultimeterSubsystem)
        End If
    End Sub

#End Region

#Region " TRIGGER "

    ''' <summary> Gets or sets the Trigger Subsystem. </summary>
    ''' <value> The Trigger Subsystem. </value>
    Public ReadOnly Property TriggerSubsystem As TriggerSubsystem

    ''' <summary> Binds the Trigger subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindTriggerSubsystem(ByVal subsystem As TriggerSubsystem)
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.TriggerSubsystem)
            Me._TriggerSubsystem = Nothing
        End If
        Me._TriggerSubsystem = subsystem
        If Me.TriggerSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.TriggerSubsystem)
        End If
    End Sub

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Processes the service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        ' device errors will be read if the error available bit is set upon reading the status byte.
        Me.Session.ReadStatusRegister() ' this could have lead to a query interrupted error: Me.ReadEventRegisters()
        If Me.ServiceRequestAutoRead Then
            If Me.Session.ErrorAvailable Then
            ElseIf Me.Session.MessageAvailable Then
                TimeSpan.FromMilliseconds(10).SpinWait()
                ' result is also stored in the last message received.
                Me.ServiceRequestReading = Me.Session.ReadFreeLineTrimEnd()
                Me.Session.ReadStatusRegister()
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(K7500Device)} Settings editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadTurnaroundTime))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.StatusReadDelay))

    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} set to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} set to {sender.TraceShowLevel}")
            Case NameOf(My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.ClearRefractoryPeriod}")
            Case NameOf(My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} set to {sender.InitializeTimeout}")
            Case NameOf(My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.InitRefractoryPeriod}")
            Case NameOf(My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.ResetRefractoryPeriod}")
            Case NameOf(My.MySettings.SessionMessageNotificationLevel)
                Me.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} set to {Me.Session.MessageNotificationLevel}")
        End Select
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleSettingsPropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
