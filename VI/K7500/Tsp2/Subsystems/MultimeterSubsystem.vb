''' <summary> Measure subsystem. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-01-03 </para>
''' </remarks>
Public Class MultimeterSubsystem
    Inherits VI.Tsp2.MultimeterSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MultimeterSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="statusSubsystem"> A reference to a <see cref="StatusSubsystemBase">message
    '''                                based session</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading Or ReadingElementTypes.Timestamp Or ReadingElementTypes.Status Or ReadingElementTypes.Units)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Volt)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.CurrentAC).SetRange(0, 10)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.CurrentDC).SetRange(0, 10)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.VoltageDC).SetRange(0, 1000)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.VoltageAC).SetRange(0, 700)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.ResistanceTwoWire).SetRange(0, 1000000000.0)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.ResistanceFourWire).SetRange(0, 1000000000.0)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.Continuity).SetRange(0, 1000)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.Diode).SetRange(0, 10)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.Capacitance).SetRange(0, 0.001)
        MyBase.FunctionModeRanges.Item(MultimeterFunctionModes.Ratio).SetRange(0, 1000)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Dim lineFrequency As Double = Me.StatusSubsystem.LineFrequency.GetValueOrDefault(60)
        Me.PowerLineCyclesRange = If(lineFrequency = 50, New isr.Core.Primitives.RangeR(0.0005, 12), New isr.Core.Primitives.RangeR(0.0005, 15))
        Me.ApertureRange = New isr.Core.Primitives.RangeR(Me.PowerLineCyclesRange.Min / lineFrequency, Me.PowerLineCyclesRange.Max / lineFrequency)
        Me.FilterCountRange = New isr.Core.Primitives.RangeI(1, 100)
        Me.FilterWindowRange = New isr.Core.Primitives.RangeR(0, 0.1)
        Me.QueryFrontTerminalsSelected()
    End Sub

    ''' <summary> Sets the known reset (default) state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Volt
        MyBase.DefineKnownResetState()
        Me.FilterCountRange = New isr.Core.Primitives.RangeI(1, 100)
        Me.FilterWindowRange = New isr.Core.Primitives.RangeR(0, 0.1)
        Me.PowerLineCycles = 0.5
        Me.AutoZeroEnabled = True
        Me.FilterCount = 10
        Me.FilterEnabled = False
        Me.MovingAverageFilterEnabled = False
        Me.OpenDetectorEnabled = False

        Me.FilterWindow = 0.001
        Me.FunctionMode = VI.MultimeterFunctionModes.VoltageDC
        Me.Range = 100
    End Sub

#End Region

#Region " APERTURE "

    ''' <summary> Gets or sets the Aperture query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property ApertureQueryCommand As String = "_G.print(_G.dmm.measure.aperture)"

    ''' <summary> Gets or sets the Aperture command format. </summary>
    ''' <value> The Aperture command format. </value>
    Protected Overrides Property ApertureCommandFormat As String = "_G.dmm.measure.aperture={0}"

#End Region

#Region " AUTO DELAY ENABLED "

    ''' <summary> Gets or sets the automatic delay enabled command Format. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledCommandFormat As String = "_G.dmm.measure.autodelay={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the automatic delay enabled query command. </summary>
    ''' <value> The automatic delay enabled query command. </value>
    Protected Overrides Property AutoDelayEnabledQueryCommand As String = "_G.print(_G.dmm.measure.autodelay==dmm.ON)"

#End Region

#Region " AUTO RANGE ENABLED "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = "_G.dmm.measure.autorange={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = "_G.print(_G.dmm.measure.autorange==dmm.ON)"

#End Region

#Region " AUTO ZERO ENABLED "

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = "_G.dmm.measure.autozero.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = "_G.print(_G.dmm.measure.autozero.enable==dmm.ON)"

#End Region

#Region " AUTO ZERO ONCE "

    ''' <summary> Gets or sets the automatic zero once command. </summary>
    ''' <value> The automatic zero once command. </value>
    Protected Overrides Property AutoZeroOnceCommand As String = "_G.dmm.measure.autozero.once()"

#End Region

#Region " BIAS "

#Region " BIAS ACTUAL "

    ''' <summary> Gets or sets the Bias Actual query command. </summary>
    ''' <value> The BiasActual query command. </value>
    Protected Overrides Property BiasActualQueryCommand As String = "_G.print(_G.dmm.measure.bias.Actual)"

#End Region

#Region " BIAS LEVEL "

    ''' <summary> Gets or sets the Bias Level query command. </summary>
    ''' <value> The BiasLevel query command. </value>
    Protected Overrides Property BiasLevelQueryCommand As String = "_G.dmm.measure.bias.Level={0}"

    ''' <summary> Gets or sets the Bias Level command format. </summary>
    ''' <value> The BiasLevel command format. </value>
    Protected Overrides Property BiasLevelCommandFormat As String = "_G.print(_G.dmm.measure.bias.Level)"

#End Region

#End Region

#Region " FILTER "

#Region " FILTER COUNT "

    ''' <summary> Gets or sets the Filter Count query command. </summary>
    ''' <value> The FilterCount query command. </value>
    Protected Overrides Property FilterCountQueryCommand As String = "_G.print(_G.dmm.measure.filter.count)"

    ''' <summary> Gets or sets the Filter Count command format. </summary>
    ''' <value> The FilterCount command format. </value>
    Protected Overrides Property FilterCountCommandFormat As String = "_G.dmm.measure.filter.count={0}"

#End Region

#Region " FILTER ENABLED "

    ''' <summary> Gets or sets the Filter enabled command Format. </summary>
    ''' <value> The Filter enabled query command. </value>
    Protected Overrides Property FilterEnabledCommandFormat As String = "_G.dmm.measure.filter.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the Filter enabled query command. </summary>
    ''' <value> The Filter enabled query command. </value>
    Protected Overrides Property FilterEnabledQueryCommand As String = "_G.print(_G.dmm.measure.filter.enable==dmm.ON)"

#End Region

#Region " MOVING AVERAGE ENABLED "

    ''' <summary> Gets or sets the moving average filter enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property MovingAverageFilterEnabledCommandFormat As String = "_G.dmm.measure.filter.type={0:'dmm.FILTER_MOVING_AVG';'dmm.FILTER_MOVING_AVG';'dmm.FILTER_REPEAT_AVG'}"

    ''' <summary> Gets or sets the moving average filter enabled query command. </summary>
    ''' <value> The moving average filter enabled query command. </value>
    Protected Overrides Property MovingAverageFilterEnabledQueryCommand As String = "_G.print(_G.dmm.measure.filter.type==dmm.FILTER_MOVING_AVG)"

#End Region

#Region " FILTER WINDOW "

    ''' <summary> Gets or sets the Filter Window query command. </summary>
    ''' <value> The FilterWindow query command. </value>
    Protected Overrides Property FilterWindowQueryCommand As String = "_G.print(_G.dmm.measure.filter.window)"

    ''' <summary> Gets or sets the Filter Window command format. </summary>
    ''' <value> The FilterWindow command format. </value>
    Protected Overrides Property FilterWindowCommandFormat As String = "_G.dmm.measure.filter.window={0}"

#End Region

#End Region

#Region " FRONT TERMINALS SELECTED "

    ''' <summary> Gets or sets the front terminals selected command format. </summary>
    ''' <value> The front terminals selected command format. </value>
    Protected Overrides Property FrontTerminalsSelectedCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the front terminals selected query command. </summary>
    ''' <value> The front terminals selected query command. </value>
    Protected Overrides Property FrontTerminalsSelectedQueryCommand As String = "_G.print(_G.dmm.terminals==dmm.TERMINALS_FRONT)"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Select reading unit. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Arebis.TypedUnits.Unit. </returns>
    Public Shared Function SelectReadingUnit(ByVal value As VI.MultimeterFunctionModes) As Arebis.TypedUnits.Unit
        Select Case value
            Case MultimeterFunctionModes.Capacitance
                Return Arebis.StandardUnits.ElectricUnits.Farad
            Case MultimeterFunctionModes.Continuity
                Return Arebis.StandardUnits.ElectricUnits.Ohm
            Case MultimeterFunctionModes.CurrentAC, MultimeterFunctionModes.CurrentDC
                Return Arebis.StandardUnits.ElectricUnits.Ampere
            Case MultimeterFunctionModes.Diode
                Return Arebis.StandardUnits.ElectricUnits.Ohm
            Case MultimeterFunctionModes.Frequency
                Return Arebis.StandardUnits.FrequencyUnits.Hertz
            Case MultimeterFunctionModes.None
                Return Arebis.StandardUnits.UnitlessUnits.Ratio
            Case MultimeterFunctionModes.Period
                Return Arebis.StandardUnits.TimeUnits.Second
            Case MultimeterFunctionModes.Ratio
                Return Arebis.StandardUnits.UnitlessUnits.Ratio
            Case MultimeterFunctionModes.ResistanceFourWire, MultimeterFunctionModes.ResistanceTwoWire
                Return Arebis.StandardUnits.ElectricUnits.Ohm
            Case MultimeterFunctionModes.VoltageAC, MultimeterFunctionModes.VoltageDC
                Return Arebis.StandardUnits.ElectricUnits.Volt
            Case Else
                Throw New InvalidOperationException($"Unhandled case {value} selecting reading unit")
        End Select
    End Function

    ''' <summary> Select function mode. </summary>
    ''' <remarks> David, 2020-07-28. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub SelectFunctionMode(ByVal value As VI.MultimeterFunctionModes)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading Or ReadingElementTypes.Timestamp Or ReadingElementTypes.Status Or ReadingElementTypes.Units)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(MultimeterSubsystem.SelectReadingUnit(value))
    End Sub

    ''' <summary> Gets or sets the function mode query command. </summary>
    ''' <value> The function mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = "_G.print(_G.dmm.measure.func)"

    ''' <summary> Gets or sets the function mode command format. </summary>
    ''' <value> The function mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = "_G.dmm.measure.func={0}"

#Region " UNIT "

    ''' <summary> Gets or sets the Measure Unit query command. </summary>
    ''' <remarks>
    ''' The query command uses the
    ''' <see cref="M:VI.Pith.SessionBase.QueryPrintTrimEnd(System.Int32,System.String)" />
    ''' </remarks>
    ''' <value> The Unit query command. </value>
    Protected Overrides Property MultimeterMeasurementUnitQueryCommand As String = "_G.print(_G.smu.measure.unit)"

    ''' <summary> Gets or sets the Measure Unit command format. </summary>
    ''' <value> The Unit command format. </value>
    Protected Overrides Property MultimeterMeasurementUnitCommandFormat As String = "_G.smu.measure.unit={0}"

#End Region

#End Region

#Region " INPUT IMPEDANCE "

    ''' <summary> Gets or sets the Input Impedance Mode query command. </summary>
    ''' <value> The Input Impedance Mode query command. </value>
    Protected Overrides Property InputImpedanceModeQueryCommand As String = "_G.print(_G.dmm.measure.inputimpedance)"

    ''' <summary> Gets or sets the Input Impedance Mode command format. </summary>
    ''' <value> The Input Impedance Mode command format. </value>
    Protected Overrides Property InputImpedanceModeCommandFormat As String = "_G.dmm.measure.inputimpedance={0}"

#End Region

#Region " LIMIT 1 "

#Region " LIMIT1 AUTO CLEAR "

    ''' <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
    ''' <value> The Limit1 AutoClear query command. </value>
    Protected Overrides Property Limit1AutoClearCommandFormat As String = "_G.dmm.measure.limit1.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
    ''' <value> The Limit1 AutoClear query command. </value>
    Protected Overrides Property Limit1AutoClearQueryCommand As String = "_G.print(_G.dmm.measure.limit1.autoclear==dmm.ON)"

#End Region

#Region " LIMIT1 ENABLED "

    ''' <summary> Gets or sets the Limit1 enabled command Format. </summary>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overrides Property Limit1EnabledCommandFormat As String = "_G.dmm.measure.limit1.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the Limit1 enabled query command. </summary>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overrides Property Limit1EnabledQueryCommand As String = "_G.print(_G.dmm.measure.limit1.enable==dmm.ON)"

#End Region

#Region " LIMIT1 LOWER LEVEL "

    ''' <summary> Gets or sets the Limit1 Lower Level command format. </summary>
    ''' <value> The Limit1LowerLevel command format. </value>
    Protected Overrides Property Limit1LowerLevelCommandFormat As String = "_G.dmm.measure.limit1.low.value={0}"

    ''' <summary> Gets or sets the Limit1 Lower Level query command. </summary>
    ''' <value> The Limit1LowerLevel query command. </value>
    Protected Overrides Property Limit1LowerLevelQueryCommand As String = "_G.print(_G.dmm.measure.limit1.low.value)"

#End Region

#Region " Limit1 UPPER LEVEL "

    ''' <summary> Gets or sets the Limit1 Upper Level command format. </summary>
    ''' <value> The Limit1UpperLevel command format. </value>
    Protected Overrides Property Limit1UpperLevelCommandFormat As String = "_G.dmm.measure.limit1.high.value={0}"

    ''' <summary> Gets or sets the Limit1 Upper Level query command. </summary>
    ''' <value> The Limit1UpperLevel query command. </value>
    Protected Overrides Property Limit1UpperLevelQueryCommand As String = "_G.print(_G.dmm.measure.limit1.high.value)"

#End Region

#End Region

#Region " LIMIT 2 "

#Region " LIMIT2 AUTO CLEAR "

    ''' <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
    ''' <value> The Limit2 AutoClear query command. </value>
    Protected Overrides Property Limit2AutoClearCommandFormat As String = "_G.dmm.measure.limit2.autoclear={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
    ''' <value> The Limit2 AutoClear query command. </value>
    Protected Overrides Property Limit2AutoClearQueryCommand As String = "_G.print(_G.dmm.measure.limit2.autoclear==dmm.ON)"

#End Region

#Region " LIMIT2 ENABLED "

    ''' <summary> Gets or sets the Limit2 enabled command Format. </summary>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overrides Property Limit2EnabledCommandFormat As String = "_G.dmm.measure.limit2.enable={0:'dmm.ON';'dmm.ON';'dmm.OFF'}"

    ''' <summary> Gets or sets the Limit2 enabled query command. </summary>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overrides Property Limit2EnabledQueryCommand As String = "_G.print(_G.dmm.measure.limit2.enable==dmm.ON)"

#End Region

#Region " LIMIT2 LOWER LEVEL "

    ''' <summary> Gets or sets the Limit2 Lower Level command format. </summary>
    ''' <value> The Limit2LowerLevel command format. </value>
    Protected Overrides Property Limit2LowerLevelCommandFormat As String = "_G.dmm.measure.limit2.low.value={0}"

    ''' <summary> Gets or sets the Limit2 Lower Level query command. </summary>
    ''' <value> The Limit2LowerLevel query command. </value>
    Protected Overrides Property Limit2LowerLevelQueryCommand As String = "_G.print(_G.dmm.measure.limit2.low.value)"

#End Region

#Region " LIMIT2 UPPER LEVEL "

    ''' <summary> Gets or sets the Limit2 Upper Level command format. </summary>
    ''' <value> The Limit2UpperLevel command format. </value>
    Protected Overrides Property Limit2UpperLevelCommandFormat As String = "_G.dmm.measure.limit1.high.value={0}"

    ''' <summary> Gets or sets the Limit2 Upper Level query command. </summary>
    ''' <value> The Limit2UpperLevel query command. </value>
    Protected Overrides Property Limit2UpperLevelQueryCommand As String = "_G.print(_G.dmm.measure.limit2.high.value)"

#End Region
#End Region

#Region " OPEN DETECTOR ENABLED "

    ''' <summary> Gets or sets the open detector enabled command Format. </summary>
    ''' <value> The open detector enabled query command. </value>
    Protected Overrides Property OpenDetectorEnabledCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the open detector enabled query command. </summary>
    ''' <value> The open detector enabled query command. </value>
    Protected Overrides Property OpenDetectorEnabledQueryCommand As String = String.Empty

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = "_G.dmm.measure.nplc={0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = "_G.print(_G.dmm.measure.nplc)"

#End Region

#Region " RANGE "

    ''' <summary> Gets or sets the Range query command. </summary>
    ''' <value> The Range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = "_G.print(_G.dmm.measure.range)"

    ''' <summary> Gets or sets the Range command format. </summary>
    ''' <value> The Range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = "_G.dmm.measure.range={0}"

#End Region

#Region " READ "

    ''' <summary> Gets or sets the read buffer query command format. </summary>
    ''' <value> The read buffer query command format. </value>
    Protected Overrides Property ReadBufferQueryCommandFormat As String = "_G.print(_G.dmm.measure.read({0}))"

    ''' <summary> Gets or sets the Measure query command. </summary>
    ''' <value> The Aperture query command. </value>
    Protected Overrides Property MeasureQueryCommand As String = "_G.print(_G.dmm.measure.read())"

#End Region

End Class
