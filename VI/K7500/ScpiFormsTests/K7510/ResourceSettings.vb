''' <summary> The 7500 Resource Info. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class ResourceSettings
    Inherits isr.VI.DeviceTests.ResourceSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(ResourceSettings)} Editor", ResourceSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As ResourceSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As ResourceSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New ResourceSettings()), ResourceSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " DEVICE RESOURCE INFORMATION "

    ''' <summary> Gets or sets the Model of the resource. </summary>
    ''' <value> The Model of the resource. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("DMM7510")>
    Public Overrides Property ResourceModel As String
        Get
            Return MyBase.ResourceModel
        End Get
        Set(value As String)
            MyBase.ResourceModel = value
        End Set
    End Property

    ''' <summary> Gets or sets the names of the candidate resources. </summary>
    ''' <value> The names of the candidate resources. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
        Global.System.Configuration.DefaultSettingValueAttribute("TCPIP0::192.168.0.144::inst0::INSTR|TCPIP0::192.168.0.144::inst0::INSTR|
TCPIP0::10.1.1.25::inst0::INSTR|TCPIP0::10.1.1.24::inst0::INSTR")>
    Public Overrides Property ResourceNames As String
        Get
            Return MyBase.ResourceNames
        End Get
        Set(value As String)
            MyBase.ResourceNames = value
        End Set
    End Property

    ''' <summary> Gets or sets the Title of the resource. </summary>
    ''' <value> The Title of the resource. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("DMM7510")>
    Public Overrides Property ResourceTitle As String
        Get
            Return MyBase.ResourceTitle
        End Get
        Set(value As String)
            MyBase.ResourceTitle = value
        End Set
    End Property

    ''' <summary> Gets or sets the language. </summary>
    ''' <value> The language. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("SCPI")>
    Public Overrides Property Language As String
        Get
            Return MyBase.Language
        End Get
        Set(value As String)
            MyBase.Language = value
        End Set
    End Property

    ''' <summary> Gets or sets the firmware revision. </summary>
    ''' <value> The firmware revision. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1.6.7")>
    Public Overrides Property FirmwareRevision As String
        Get
            Return MyBase.FirmwareRevision
        End Get
        Set(value As String)
            MyBase.FirmwareRevision = value
        End Set
    End Property

#End Region

End Class

