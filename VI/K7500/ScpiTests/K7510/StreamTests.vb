Imports isr.Core.SplitExtensions

''' <summary> K7000 Scanner Multimeter Scan Card unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k7510")>
Public Class StreamTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            Console.Out.WriteLine(testContext.FullyQualifiedTestClassName)
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()

        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ResourceSettings.Get.Exists, $"{GetType(K7500Tests.ResourceSettings)} settings should exist")
        Assert.IsTrue(SubsystemsSettings.Get.Exists, $"{GetType(K7500Tests.SubsystemsSettings)} settings should exist")
        Assert.IsTrue(StreamSettings.Get.Exists, $"{GetType(K7500Tests.StreamSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            If _TestSite Is Nothing Then
                _TestSite = New TestSite
                _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
                _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            End If
            Return _TestSite
        End Get
    End Property

#End Region

#Region " BUS TRIGGER STREAM: INSTRUMENT CONFIGURATION "

    ''' <summary> Asseret limit binning should configure. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Friend Shared Sub AsseretLimitBinningShouldConfigure(ByVal device As VI.K7500.K7500Device)

        Dim propertyName As String = String.Empty
        Dim title As String = device.OpenResourceTitle

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2LowerLevel)}"
        Dim expectedValue As Double = StreamSettings.Get.ExpectedResistance * (1 - StreamSettings.Get.ResistanceTolerance)
        Dim actualValue As Double = device.BinningResistanceFourWireSubsystem.ApplyLimit2LowerLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2UpperLevel)}"
        expectedValue = StreamSettings.Get.ExpectedResistance * (1 + StreamSettings.Get.ResistanceTolerance)
        actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit2UpperLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2AutoClear)}"
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean = device.BinningResistanceFourWireSubsystem.ApplyLimit2AutoClear(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit2Enabled)}"
        expectedBoolean = True
        actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit2Enabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1LowerLevel)}"
        expectedValue = StreamSettings.Get.ExpectedResistance * StreamSettings.Get.ResistanceTolerance
        actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit1LowerLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1UpperLevel)}"
        expectedValue = StreamSettings.Get.OpenLimit
        actualValue = device.BinningResistanceFourWireSubsystem.ApplyLimit1UpperLevel(expectedValue).GetValueOrDefault(0)
        Assert.AreEqual(expectedValue, actualValue, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1AutoClear)}"
        expectedBoolean = True
        actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit1AutoClear(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.BinningSubsystemBase)}.{NameOf(VI.BinningSubsystemBase.Limit1Enabled)}"
        expectedBoolean = True
        actualBoolean = device.BinningResistanceFourWireSubsystem.ApplyLimit1Enabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        device.TriggerSubsystem.ApplyGradeBinning(StreamSettings.Get.BufferStreamTestBufferSize, StreamSettings.Get.BufferStreamStartDelay,
                                                  StreamSettings.Get.FailOutputValue, StreamSettings.Get.PassOutputValue, StreamSettings.Get.OutsideRangeOutputValue,
                                                  StreamSettings.Get.MeterTriggerSource)

    End Sub

    ''' <summary> Assert measurement should configure. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Friend Shared Sub AssertMeasurementShouldConfigure(ByVal device As VI.K7500.K7500Device)
        Dim propertyName As String = String.Empty
        Dim title As String = device.OpenResourceTitle

        propertyName = $"{GetType(VI.SystemSubsystemBase)}.{NameOf(VI.SystemSubsystemBase.AutoZeroEnabled)}"
        Dim expectedBoolean As Boolean = StreamSettings.Get.AutoZeroEnabled
        Dim actualBoolean As Boolean = device.SystemSubsystem.ApplyAutoZeroEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseSubsystemBase)}.{NameOf(VI.SenseSubsystemBase.FunctionMode)}"
        Dim expectedFunctionMode As VI.SenseFunctionModes = StreamSettings.Get.SenseFunction
        Dim actualFunctionMode As VI.SenseFunctionModes = device.SenseSubsystem.ApplyFunctionMode(expectedFunctionMode).GetValueOrDefault(VI.SenseFunctionModes.None)
        Assert.AreEqual(expectedFunctionMode, actualFunctionMode, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.Aperture)}"

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.PowerLineCycles)}"
        Dim expectedPowerLineCycles As Double = StreamSettings.Get.PowerLineCycles
        Dim actualPowerLineCycles As Double = device.SenseFunctionSubsystem.ApplyPowerLineCycles(expectedPowerLineCycles).GetValueOrDefault(0)
        Assert.AreEqual(expectedPowerLineCycles, actualPowerLineCycles, device.StatusSubsystemBase.LineFrequency.Value / TimeSpan.TicksPerSecond,
                        $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.AutoRangeEnabled)}"
        expectedBoolean = StreamSettings.Get.AutoRangeEnabled
        actualBoolean = device.SenseFunctionSubsystem.ApplyAutoRangeEnabled(expectedBoolean).GetValueOrDefault(Not expectedBoolean)
        Assert.AreEqual(expectedBoolean, actualBoolean, $"[{title}].[{propertyName}] should equal expected value")

        propertyName = $"{GetType(VI.SenseFunctionSubsystemBase)}.{NameOf(VI.SenseFunctionSubsystemBase.ResolutionDigits)}"
        Dim expectedResolution As Double = StreamSettings.Get.ResolutionDigits
        Dim actualResolution As Double = device.SenseFunctionSubsystem.ApplyResolutionDigits(expectedResolution).GetValueOrDefault(0)
        Assert.AreEqual(expectedResolution, actualResolution, $"[{title}].[{propertyName}] should equal expected value")

        If False Then
            device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled(True)
            device.SenseResistanceFourWireSubsystem.ApplyAverageCount(10)
            device.SenseResistanceFourWireSubsystem.ApplyAverageFilterType(VI.AverageFilterTypes.Repeat)
            device.SenseResistanceFourWireSubsystem.ApplyAveragePercentWindow(100 * 0.01)
        Else
            device.SenseResistanceFourWireSubsystem.ApplyAverageEnabled(False)
        End If

        device.SenseResistanceFourWireSubsystem.ApplyOpenLeadDetectorEnabled(True)

    End Sub

    ''' <summary> Assert trace should configure. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertTraceShouldConfigure(ByVal device As VI.K7500.K7500Device)
        Dim propertyName As String = NameOf(VI.TraceSubsystemBase.FeedSource).SplitWords
        Dim title As String = device.OpenResourceTitle
        device.TraceSubsystem.ClearBuffer()

        ' instrument reports: Reading buffer capacity must be between 10 and 8,496,306 readings
        device.TraceSubsystem.ApplyPointsCount(StreamSettings.Get.BufferStreamTestBufferSize)

        ' clear the buffer 
        device.TraceSubsystem.ClearBuffer()


        device.TraceSubsystem.BinningDuration = TimeSpan.FromMilliseconds(100)

    End Sub

    ''' <summary> Assert trigger plan should configure. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    ''' <param name="device"> The device. </param>
    Friend Shared Sub AssertTriggerPlanShouldConfigure(ByVal device As VI.K7500.K7500Device)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String = NameOf(VI.TriggerSubsystemBase.TriggerSource).SplitWords

        device.ClearExecutionState()
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        Dim triggerState As TriggerState = device.TriggerSubsystem.QueryTriggerState.GetValueOrDefault(TriggerState.None)
        If TriggerState.Running = triggerState Then
            device.TriggerSubsystem.Abort()
        End If
        device.TriggerSubsystem.ClearTriggerModel()
        device.Session.EnableServiceRequestWaitComplete()
        device.Session.Wait()
        device.Session.ReadStatusRegister()

        device.TriggerSubsystem.ApplyAutoDelayEnabled(False)

        device.TriggerSubsystem.QueryTriggerState()


    End Sub

    ''' <summary> Assert buffer readings should match. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The 2002. </param>
    Private Shared Sub AssertBufferReadingsShouldMatch(ByVal device As VI.K7500.K7500Device)

        Dim title As String = device.OpenResourceTitle
        Dim propertyName As String
        Dim bufferReadings As IEnumerable(Of BufferReading) = device.TraceSubsystem.DequeueRange(device.TraceSubsystem.NewReadingsCount)
        Dim expectedResistance As Double = StreamSettings.Get.ExpectedResistance
        Dim epsilon As Double = expectedResistance * StreamSettings.Get.ResistanceTolerance

        ' trace (display) the buffer readings amounts
        Dim readingNumber As Integer = 0
        For Each bufferReading As BufferReading In bufferReadings
            readingNumber += 1
            StreamTests.TestInfo.TraceMessage($"Reading #{readingNumber}={bufferReading.Amount} 0x{bufferReading.StatusWord:X4} {bufferReading.FractionalTimespan.TotalMilliseconds:0}ms")
        Next

        Dim measuredResistance As Double
        propertyName = NameOf(measuredResistance).SplitWords
        If device.SenseSubsystem.FunctionMode.Value = StreamSettings.Get.SenseFunction Then
            readingNumber = 0
            For Each bufferReading As BufferReading In bufferReadings
                readingNumber += 1
                Assert.IsNotNull(bufferReading.Amount, $"[{title}].[Reading #{readingNumber}] should not be null")
                measuredResistance = bufferReading.Amount.Value
                Assert.AreEqual(expectedResistance, measuredResistance, epsilon,
                                $"[{title}].[{propertyName}]={measuredResistance} but should equals {expectedResistance} within {epsilon}")
            Next
        End If

        ' validate binding list count
        Assert.AreEqual(device.TraceSubsystem.BufferReadingsCount, bufferReadings.Count, "Buffer readings count should match the binding list count")

    End Sub

#End Region

#Region " BUS TRIGGERS STREAM "

    ''' <summary> Handles the property change. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Shared Sub HandlePropertyChange(ByVal sender As VI.TraceSubsystemBase, ByVal propertyName As String)
        If sender IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(propertyName) Then
            Select Case propertyName
                Case NameOf(VI.TraceSubsystemBase.BufferReadingsCount)
                    If sender.BufferReadingsCount > 0 Then
                        TestInfo.TraceMessage($"Streaming reading #{sender.BufferReadingsCount}: {sender.LastBufferReading.Amount} 0x{sender.LastBufferReading.StatusWord:X4}")
                    End If
            End Select
        End If
    End Sub

    ''' <summary> Handles the trace subsystem property change. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Shared Sub HandleTraceSubsystemPropertyChange(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Try
            If sender IsNot Nothing AndAlso e IsNot Nothing Then
                StreamTests.HandlePropertyChange(TryCast(sender, VI.TraceSubsystemBase), e.PropertyName)
            End If
        Catch ex As Exception
            Assert.Fail(ex.ToString)
        End Try
    End Sub

    ''' <summary> Restore Trigger State. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub RestoreState(ByVal device As K7500.K7500Device)
        device.TriggerSubsystem.Abort()
        device.Session.QueryOperationCompleted()
        device.ResetKnownState()
        device.Session.QueryOperationCompleted()
        device.ClearExecutionState()
        device.Session.QueryOperationCompleted()
        device.TriggerSubsystem.ApplyContinuousEnabled(False)
        device.Session.EnableServiceRequestWaitComplete()
    End Sub

    ''' <summary> Assert buffer streaming should stop. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertBufferStreamingShouldStop(ByVal device As K7500.K7500Device)
        Dim timeout As TimeSpan = VI.TraceSubsystemBase.EstimateStreamStopTimeoutInterval(device.TraceSubsystem.StreamCycleDuration, StreamSettings.Get.BufferStreamPollInteval, 1.5)
        Dim r As (Success As Boolean, Details As String) = device.TraceSubsystem.StopBufferStream(timeout)
        If r.Success Then
            device.TriggerSubsystem.Abort()
            device.Session.ReadStatusRegister()
            TestInfo.TraceMessage($"Streaming ended with { r.Details}")
        Else
            Assert.Fail($"buffer streaming failed; { r.Details}")
        End If
        RemoveHandler device.TraceSubsystem.PropertyChanged, AddressOf StreamTests.HandleTraceSubsystemPropertyChange
    End Sub

    ''' <summary> Assert buffer streaming should start. </summary>
    ''' <remarks> David, 2020-04-11. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertBufferStreamingShouldStart(ByVal device As VI.K7500.K7500Device)
        AddHandler device.TraceSubsystem.PropertyChanged, AddressOf StreamTests.HandleTraceSubsystemPropertyChange
        Assert.IsTrue(device.Session.QueryOperationCompleted().GetValueOrDefault(False), $"Operation should be completed before starting streaming")
        device.TriggerSubsystem.Initiate()
        isr.Core.ApplianceBase.DoEvents()
        device.TraceSubsystem.StartBufferStream(device.TriggerSubsystem, StreamSettings.Get.BufferStreamPollInteval, device.SenseFunctionSubsystem.FunctionUnit)
    End Sub

#End Region

#Region " BUS TRIGGERS STREAM READING "

    ''' <summary> Assert meter bus triggers stream readings should pass. </summary>
    ''' <remarks>
    ''' <list type="bullet">
    ''' testhost.x86-2020-07-29.log <item>
    '''                         Amount   Status </item><item>
    ''' Streaming reading #1: 100.0845 Ω 0x0008 </item><item>
    ''' Streaming reading #2: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #3: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #4: 100.0845 Ω 0x0008 </item><item>
    ''' Streaming reading #5: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #6: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #7: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #8: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #9: 100.0846 Ω 0x0008 </item><item>
    ''' Streaming reading #10: 100.0847 Ω 0x0008 </item><item>
    ''' Streaming ended with Done </item><item>
    ''' Streaming ended with Done </item><item>
    ''' Reading #1=100.0845 Ω 0x0008 449ms </item><item>
    ''' Reading #2=100.0846 Ω 0x0008 919ms </item><item>
    ''' Reading #3=100.0846 Ω 0x0008 428ms </item><item>
    ''' Reading #4=100.0845 Ω 0x0008 922ms </item><item>
    ''' Reading #5=100.0846 Ω 0x0008 414ms </item><item>
    ''' Reading #6=100.0846 Ω 0x0008 922ms </item><item>
    ''' Reading #7=100.0846 Ω 0x0008 431ms </item><item>
    ''' Reading #8=100.0846 Ω 0x0008 938ms </item><item>
    ''' Reading #9=100.0846 Ω 0x0008 425ms </item><item>
    ''' Reading #10=100.0847 Ω 0x0008 932ms </item></list>
    ''' </remarks>
    ''' <param name="k7500">        The Keithley 2002 device. </param>
    ''' <param name="triggerCount"> Number of triggers. </param>
    ''' <param name="delay">        The delay. </param>
    Friend Shared Sub AssertMeterBusTriggersStreamReadingsShouldPass(ByVal k7500 As VI.K7500.K7500Device, ByVal triggerCount As Integer, ByVal delay As TimeSpan)

        Dim K7500Title As String = k7500.OpenResourceTitle

        ' TO_DO: Check Trigger State instead of operation bit masks.
        StreamTests.AssertBufferStreamingShouldStart(k7500)
        TestInfo.TraceMessage("                        Amount   Status")
        Try
            Try
                For i As Integer = 1 To triggerCount
                    ' this delay is used to allow the operator to see the values as they come in.
                    isr.Core.ApplianceBase.DoEventsWait(delay)
                    k7500.TraceSubsystem.BusTriggerRequested = True
                    ' K7500.Session.AssertTrigger()
                    isr.Core.ApplianceBase.DoEventsWait(delay)
                Next
            Catch
                Throw
            Finally
            End Try
        Catch
            Throw
        Finally
            StreamTests.AssertBufferStreamingShouldStop(k7500)
        End Try

    End Sub

    ''' <summary> Assert meter bus triggers stream readings should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub AssertMeterBusTriggersStreamReadingsShouldPass()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using K7500Device As VI.K7500.K7500Device = VI.K7500.K7500Device.Create
            K7500Device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, K7500Device, ResourceSettings.Get)
                Assert.AreEqual(RouteTerminalsModes.Front, K7500Device.RouteSubsystem.QueryTerminalsMode.GetValueOrDefault(RouteTerminalsModes.None), $"Front terminal must be selected")
                StreamTests.AssertTriggerPlanShouldConfigure(K7500Device)
                StreamTests.AssertMeasurementShouldConfigure(K7500Device)
                StreamTests.AsseretLimitBinningShouldConfigure(K7500Device)
                StreamTests.AssertTraceShouldConfigure(K7500Device)
                If TriggerSources.Bus = StreamSettings.Get.MeterTriggerSource Then
                    StreamTests.AssertMeterBusTriggersStreamReadingsShouldPass(K7500Device, StreamSettings.Get.BufferStreamTriggerTestTriggerCount, StreamSettings.Get.BusTriggerTestTriggerDelay)
                End If
                StreamTests.AssertBufferReadingsShouldMatch(K7500Device)
            Catch
                Throw
            Finally
                StreamTests.RestoreState(K7500Device)
                K7500Tests.DeviceManager.CloseSession(TestInfo, K7500Device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) meter bus triggers stream readings should pass. </summary>
    ''' <remarks> David, 2020-04-09. </remarks>
    <TestMethod()>
    Public Sub MeterBusTriggersStreamReadingsShouldPass()
        StreamSettings.Get.MeterTriggerSource = TriggerSources.Bus
        StreamTests.AssertMeterBusTriggersStreamReadingsShouldPass()
    End Sub

#End Region

End Class

