

''' <summary> The Subsystems Test Information. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class StreamSettings
    Inherits isr.VI.DeviceTests.TestSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(StreamSettings)} Editor", StreamSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As StreamSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As StreamSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New StreamSettings()), StreamSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " BUS TRIGGER TEST "

    ''' <summary> Gets or sets the bus trigger test trigger source. </summary>
    ''' <value> The bus trigger test trigger source. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Bus")>
    Public Property BusTriggerTestTriggerSource As VI.TriggerSources
        Get
            Return Me.AppSettingEnum(Of VI.TriggerSources)
        End Get
        Set(value As VI.TriggerSources)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of bus trigger test scans. </summary>
    ''' <value> A list of bus trigger test scans. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1!1:1!10)")>
    Public Property BusTriggerTestScanList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of bus trigger test triggers. </summary>
    ''' <value> The number of bus trigger test triggers. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Property BusTriggerTestTriggerCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the bus trigger test trigger delay. </summary>
    ''' <value> The bus trigger test trigger delay. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.5")>
    Public Property BusTriggerTestTriggerDelay As TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

#Region " METER SETTINGS "

    ''' <summary> Gets or sets the auto zero Enabled settings. </summary>
    ''' <value> The auto zero settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property AutoZeroEnabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the automatic range enabled. </summary>
    ''' <value> The automatic range enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property AutoRangeEnabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set(value As Boolean)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the Sense Function settings. </summary>
    ''' <value> The Sense Function settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("ResistanceFourWire")>
    Public Property SenseFunction As VI.SenseFunctionModes
        Get
            Return Me.AppSettingEnum(Of VI.SenseFunctionModes)
        End Get
        Set(value As VI.SenseFunctionModes)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the power line cycles settings. </summary>
    ''' <value> The power line cycles settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Property PowerLineCycles As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the resolution digits. </summary>
    ''' <value> The resolution digits. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("9")>
    Public Property ResolutionDigits As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the expected resistance. </summary>
    ''' <value> The expected resistance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("100")>
    Public Property ExpectedResistance As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the resistance tolerance. </summary>
    ''' <value> The resistance tolerance. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.01")>
    Public Property ResistanceTolerance As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the open limit. </summary>
    ''' <value> The open limit. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10000")>
    Public Property OpenLimit As Double
        Get
            Return Me.AppSettingGetter(0.0)
        End Get
        Set(value As Double)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the pass output value. </summary>
    ''' <value> The pass output value. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Property PassOutputValue As Byte
        Get
            Return Me.AppSettingGetter(CByte(0))
        End Get
        Set(value As Byte)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the fail output value. </summary>
    ''' <value> The fail output value. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("2")>
    Public Property FailOutputValue As Byte
        Get
            Return Me.AppSettingGetter(CByte(0))
        End Get
        Set(value As Byte)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the outside range (overflow or short) output value. </summary>
    ''' <value> The outside range output value. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("4")>
    Public Property OutsideRangeOutputValue As Byte
        Get
            Return Me.AppSettingGetter(CByte(0))
        End Get
        Set(value As Byte)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the meter arm source. </summary>
    ''' <value> The meter arm source. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Bus")>
    Public Property MeterArmSource As VI.ArmSources
        Get
            Return Me.AppSettingEnum(Of VI.ArmSources)
        End Get
        Set(value As VI.ArmSources)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the meter trigger source. </summary>
    ''' <value> The meter trigger source. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Bus")>
    Public Property MeterTriggerSource As VI.TriggerSources
        Get
            Return Me.AppSettingEnum(Of VI.TriggerSources)
        End Get
        Set(value As VI.TriggerSources)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets a list of scan card scans. </summary>
    ''' <value> A list of scan card scans. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1:3)")>
    Public Property ScanCardScanList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the default scan card scan list. </summary>
    ''' <value> The default scan card scan list. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1:10)")>
    Public Property DefaultScanCardScanList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of scan card samples. </summary>
    ''' <value> The number of scan card samples. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("3")>
    Public Property ScanCardSampleCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property


#End Region

#Region " BUFFER STREAM TEST "

    ''' <summary> Gets or sets a list of buffer stream trigger test scans. </summary>
    ''' <value> A list of buffer stream trigger test scans. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1!1:1!10)")>
    Public Property BufferStreamTriggerTestScanList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of buffer stream trigger test triggers. </summary>
    ''' <value> The number of buffer stream trigger test triggers. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Property BufferStreamTriggerTestTriggerCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the buffer stream poll inteval. </summary>
    ''' <value> The buffer stream poll inteval. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.020")>
    Public Property BufferStreamPollInteval As TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the duration of the binning strobe. </summary>
    ''' <value> The binning strobe duration. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.012")>
    Public Property BinningStrobeDuration As TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of buffer stream completion cycles. </summary>
    ''' <value> The number of buffer stream completion cycles. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("50")>
    Public Property BufferStreamCompletionCycleCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the size of the buffer stream test buffer. </summary>
    ''' <value> The size of the buffer stream test buffer. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1000000")>
    Public Property BufferStreamTestBufferSize As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the buffer stream start delay. </summary>
    ''' <value> The buffer stream start delay. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.009")>
    Public Property BufferStreamStartDelay As TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            Me.AppSettingSetter(value)
        End Set
    End Property


#End Region

#Region " AUTONOMOUS TRIGGER TEST "

    ''' <summary> Gets or sets a list of autonomous trigger test scans. </summary>
    ''' <value> A list of autonomous trigger test scans. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("(@1!1:1!10)")>
    Public Property AutonomousTriggerTestScanList As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the number of autonomous trigger test triggers. </summary>
    ''' <value> The number of autonomous trigger test triggers. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Property AutonomousTriggerTestTriggerCount As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set(value As Integer)
            Me.AppSettingSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the autonomous trigger test trigger delay. </summary>
    ''' <value> The autonomous trigger test trigger delay. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.5")>
    Public Property AutonomousTriggerTestTriggerDelay As TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set(value As TimeSpan)
            Me.AppSettingSetter(value)
        End Set
    End Property

#End Region

End Class

