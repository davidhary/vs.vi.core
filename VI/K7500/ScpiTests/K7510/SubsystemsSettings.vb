''' <summary> The Subsystems Test Information. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-02-12 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
 Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
 Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class SubsystemsSettings
    Inherits isr.VI.DeviceTests.SubsystemsSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Constructor that prevents a default instance of this class from being created.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration($"{GetType(SubsystemsSettings)} Editor", SubsystemsSettings.Get)
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As SubsystemsSettings

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As SubsystemsSettings
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New SubsystemsSettings()), SubsystemsSettings)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " DEVICE SESSION INFORMATION "

    ''' <summary> Gets or sets the keep alive query command. </summary>
    ''' <value> The keep alive query command. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("*OPC?")>
    Public Overrides Property KeepAliveQueryCommand As String
        Get
            Return MyBase.KeepAliveQueryCommand
        End Get
        Set(value As String)
            MyBase.KeepAliveQueryCommand = value
        End Set
    End Property

    ''' <summary> Gets or sets the keep-alive command. </summary>
    ''' <value> The keep-alive command. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("*OPC")>
    Public Overrides Property KeepAliveCommand As String
        Get
            Return MyBase.KeepAliveCommand
        End Get
        Set(value As String)
            MyBase.KeepAliveCommand = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial read termination enabled. </summary>
    ''' <value> The initial read termination enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overrides Property InitialReadTerminationEnabled As Boolean
        Get
            Return MyBase.InitialReadTerminationEnabled
        End Get
        Set(value As Boolean)
            MyBase.InitialReadTerminationEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial read termination character. </summary>
    ''' <value> The initial read termination character. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overrides Property InitialReadTerminationCharacter As Integer
        Get
            Return MyBase.InitialReadTerminationCharacter
        End Get
        Set(value As Integer)
            MyBase.InitialReadTerminationCharacter = value
        End Set
    End Property

    ''' <summary> Gets or sets the read termination enabled. </summary>
    ''' <value> The read termination enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overrides Property ReadTerminationEnabled As Boolean
        Get
            Return MyBase.ReadTerminationEnabled
        End Get
        Set(value As Boolean)
            MyBase.ReadTerminationEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the read termination character. </summary>
    ''' <value> The read termination character. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overrides Property ReadTerminationCharacter As Integer
        Get
            Return MyBase.ReadTerminationCharacter
        End Get
        Set(value As Integer)
            MyBase.ReadTerminationCharacter = value
        End Set
    End Property

#End Region

#Region " STATUS SUBSYSTEM INFORMATION "

    ''' <summary> Gets or sets the Initial power line cycles settings. </summary>
    ''' <value> The power line cycles settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("1")>
    Public Overrides Property InitialPowerLineCycles As Double
        Get
            Return MyBase.InitialPowerLineCycles
        End Get
        Set(value As Double)
            MyBase.InitialPowerLineCycles = value
        End Set
    End Property

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the erroneous command. </summary>
    ''' <value> The erroneous command. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("*CLL")>
    Public Overrides Property ErroneousCommand As String
        Get
            Return MyBase.ErroneousCommand
        End Get
        Set(value As String)
            MyBase.ErroneousCommand = value
        End Set
    End Property

    ''' <summary> Gets or sets the error available milliseconds delay. </summary>
    ''' <value> The error available milliseconds delay. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0")>
    Public Overrides Property ErrorAvailableMillisecondsDelay As Integer
        Get
            Return MyBase.ErrorAvailableMillisecondsDelay
        End Get
        Set(value As Integer)
            MyBase.ErrorAvailableMillisecondsDelay = value
        End Set
    End Property

    ''' <summary> Gets or sets a message describing the expected compound error. </summary>
    ''' <value> A message describing the expected compound error. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
        Global.System.Configuration.DefaultSettingValueAttribute("-285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20")>
    Public Overrides Property ExpectedCompoundErrorMessage As String
        Get
            Return MyBase.ExpectedCompoundErrorMessage
        End Get
        Set(value As String)
            MyBase.ExpectedCompoundErrorMessage = value
        End Set
    End Property

    ''' <summary> Gets or sets a message describing the expected error. </summary>
    ''' <value> A message describing the expected error. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("-285")>
    Public Overrides Property ExpectedErrorMessage As String
        Get
            Return MyBase.ExpectedErrorMessage
        End Get
        Set(value As String)
            MyBase.ExpectedErrorMessage = value
        End Set
    End Property

    ''' <summary> Gets or sets the expected error number. </summary>
    ''' <value> The expected error number. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(),
        Global.System.Configuration.DefaultSettingValueAttribute("TSP Syntax error at line 1: unexpected symbol near `*'")>
    Public Overrides Property ExpectedErrorNumber As Integer
        Get
            Return MyBase.ExpectedErrorNumber
        End Get
        Set(value As Integer)
            MyBase.ExpectedErrorNumber = value
        End Set
    End Property

    ''' <summary> Gets or sets the expected error level. </summary>
    ''' <value> The expected error level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("20")>
    Public Overrides Property ExpectedErrorLevel As Integer
        Get
            Return MyBase.ExpectedErrorLevel
        End Get
        Set(value As Integer)
            MyBase.ExpectedErrorLevel = value
        End Set
    End Property

#End Region

#Region " SOURCE MEASURE UNIT INFORMATION "

    ''' <summary> Gets or sets the maximum output power of the instrument. </summary>
    ''' <value> The maximum output power . </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0")>
    Public Overrides Property MaximumOutputPower As Double
        Get
            Return MyBase.MaximumOutputPower
        End Get
        Set(value As Double)
            MyBase.MaximumOutputPower = value
        End Set
    End Property

    ''' <summary> Gets or sets the line frequency. </summary>
    ''' <value> The line frequency. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("60")>
    Public Overrides Property LineFrequency As Double
        Get
            Return MyBase.LineFrequency
        End Get
        Set(value As Double)
            MyBase.LineFrequency = value
        End Set
    End Property

#End Region

#Region " SENSE SUBSYSTEM INFORMATION "

    ''' <summary> Gets or sets the Initial auto Delay Enabled settings. </summary>
    ''' <value> The auto Delay settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overrides Property InitialAutoDelayEnabled As Boolean
        Get
            Return MyBase.InitialAutoDelayEnabled
        End Get
        Set(value As Boolean)
            MyBase.InitialAutoDelayEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the Initial auto Range enabled settings. </summary>
    ''' <value> The auto Range settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overrides Property InitialAutoRangeEnabled As Boolean
        Get
            Return MyBase.InitialAutoRangeEnabled
        End Get
        Set(value As Boolean)
            MyBase.InitialAutoRangeEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the Initial auto zero Enabled settings. </summary>
    ''' <value> The auto zero settings. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overrides Property InitialAutoZeroEnabled As Boolean
        Get
            Return MyBase.InitialAutoZeroEnabled
        End Get
        Set(value As Boolean)
            MyBase.InitialAutoZeroEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial sense function. </summary>
    ''' <value> The initial sense function. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("VoltageDC")>
    Public Overrides Property InitialSenseFunction As VI.SenseFunctionModes
        Get
            Return MyBase.InitialSenseFunction
        End Get
        Set(value As VI.SenseFunctionModes)
            MyBase.InitialSenseFunction = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial filter enabled. </summary>
    ''' <value> The initial filter enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overrides Property InitialFilterEnabled As Boolean
        Get
            Return MyBase.InitialFilterEnabled
        End Get
        Set(value As Boolean)
            MyBase.InitialFilterEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial moving average filter enabled. </summary>
    ''' <value> The initial moving average filter enabled. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Overrides Property InitialMovingAverageFilterEnabled As Boolean
        Get
            Return MyBase.InitialMovingAverageFilterEnabled
        End Get
        Set(value As Boolean)
            MyBase.InitialMovingAverageFilterEnabled = value
        End Set
    End Property

    ''' <summary> Gets or sets the number of initial filters. </summary>
    ''' <value> The number of initial filters. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("10")>
    Public Overrides Property InitialFilterCount As Integer
        Get
            Return MyBase.InitialFilterCount
        End Get
        Set(value As Integer)
            MyBase.InitialFilterCount = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial filter window. </summary>
    ''' <value> The initial filter window. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("0.001")>
    Public Overrides Property InitialFilterWindow As Double
        Get
            Return MyBase.InitialFilterWindow
        End Get
        Set(value As Double)
            MyBase.InitialFilterWindow = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial remote sense selected. </summary>
    ''' <value> The initial remote sense selected. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overrides Property InitialRemoteSenseSelected As Boolean
        Get
            Return MyBase.InitialRemoteSenseSelected
        End Get
        Set(value As Boolean)
            MyBase.InitialRemoteSenseSelected = value
        End Set
    End Property

    ''' <summary> Gets or sets the initial input impedance mode. </summary>
    ''' <value> The initial input impedance mode. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("TenMegaOhm")>
    Public Property InitialInputImpedanceMode As VI.InputImpedanceModes
        Get
            Return Me.AppSettingEnum(Of VI.InputImpedanceModes)
        End Get
        Set(value As VI.InputImpedanceModes)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

    ''' <summary> Gets or sets the test input impedance mode. </summary>
    ''' <value> The test input impedance mode. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("Automatic")>
    Public Property TestInputImpedanceMode As VI.InputImpedanceModes
        Get
            Return Me.AppSettingEnum(Of VI.InputImpedanceModes)
        End Get
        Set(value As VI.InputImpedanceModes)
            Me.AppSettingEnumSetter(value)
        End Set
    End Property

#End Region

#Region " SENSE SUBSYSTEM INFORMATION "

    ''' <summary> Gets or sets the initial front terminals selected. </summary>
    ''' <value> The initial front terminals selected. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overrides Property InitialFrontTerminalsSelected As Boolean
        Get
            Return MyBase.InitialFrontTerminalsSelected
        End Get
        Set(value As Boolean)
            MyBase.InitialFrontTerminalsSelected = value
        End Set
    End Property


#End Region

End Class
