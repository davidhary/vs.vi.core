Imports System.Threading

''' <summary> K7510 Subsystems unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k7510")>
Public Class SubsystemsTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K7500Tests.ResourceSettings.Get.Exists, $"{GetType(K7500Tests.ResourceSettings)} settings should exist")
        Assert.IsTrue(K7500Tests.SubsystemsSettings.Get.Exists, $"{GetType(K7500Tests.SubsystemsSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " STATUS SUSBSYSTEM "

    ''' <summary> Opens session check status. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="readErrorEnabled"> True to enable, false to disable the read error. </param>
    ''' <param name="resourceInfo">     Information describing the resource. </param>
    ''' <param name="subsystemsInfo">   Information describing the subsystems. </param>
    Private Shared Sub AssertSessionOpenCheckStatusShouldPass(ByVal readErrorEnabled As Boolean, ByVal resourceInfo As ResourceSettings, ByVal subsystemsInfo As SubsystemsSettings)
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.K7500.K7500Device = VI.K7500.K7500Device.Create
            Try
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                isr.VI.DeviceTests.DeviceManager.AssertSessionInitialValuesShouldMatch(device.Session, resourceInfo, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceModelShouldMatch(device.StatusSubsystemBase, resourceInfo)
                isr.VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldMatch(device.StatusSubsystemBase, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertTerminationValuesShouldMatch(device.Session, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertLineFrequencyShouldMatch(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertIntegrationPeriodShouldMatch(device.StatusSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch(device.MeasureSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch(device.SenseSubsystem, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertSessionDeviceErrorsShouldClear(device, subsystemsInfo)
                If readErrorEnabled Then isr.VI.DeviceTests.DeviceManager.AssertDeviceErrorsShouldRead(device, subsystemsInfo)
                isr.VI.DeviceTests.DeviceManager.AssertOrphanMessagesShouldBeEmpty(device.StatusSubsystemBase)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SessionOpenCheckStatusShouldPass()
        SubsystemsTests.AssertSessionOpenCheckStatusShouldPass(False, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) session open check status device errors should pass.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SessionOpenCheckStatusDeviceErrorsShouldPass()
        SubsystemsTests.AssertSessionOpenCheckStatusShouldPass(True, ResourceSettings.Get, SubsystemsSettings.Get)
    End Sub

#End Region

#Region " INIT KNOWN STATE TESTS "

    <TestMethod()>
    Public Sub FunctionModeQueryAfterResetClearInitShouldPass()
        Using device As VI.K7500.K7500Device = VI.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Dim resetClearInitDelay As TimeSpan = TimeSpan.FromMilliseconds(10)
            Dim functionModelDelay As TimeSpan = TimeSpan.FromMilliseconds(10)
            Dim cycleCount As Integer = 10
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                isr.Core.ApplianceBase.DoEventsWait(resetClearInitDelay)
                For i As Integer = 1 To cycleCount
                    Console.Out.WriteLine($"Clear Reset Init #{i}")
                    device.ResetClearInit()
                    isr.Core.ApplianceBase.DoEventsWait(resetClearInitDelay)
                    Dim expectedFunctionMode As SenseFunctionModes = SenseFunctionModes.ResistanceFourWire
                    device.SenseFunctionSubsystem.WriteFunctionMode(expectedFunctionMode)
                    isr.Core.ApplianceBase.DoEventsWait(functionModelDelay)
                    Dim actualFunctionMode As SenseFunctionModes = device.SenseFunctionSubsystem.QueryFunctionMode().GetValueOrDefault(SenseFunctionModes.None)
                    Assert.AreEqual(expectedFunctionMode, actualFunctionMode, "Functions mode should match")
                Next
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " SENSE SUBSYSTEM TESTS "

    ''' <summary> (Unit Test Method) sense subsystem initial values should match. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SenseSubsystemInitialValuesShouldMatch()
        Using device As VI.K7500.K7500Device = VI.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertSubsystemInitialValuesShouldMatch(device.SenseSubsystem, SubsystemsSettings.Get)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

    ''' <summary> (Unit Test Method) sense subsystem function mode should toggle. </summary>
    ''' <remarks> David, 2020-07-29. </remarks>
    <TestMethod()>
    Public Sub SenseSubsystemFunctionModeShouldToggle()
        Using device As VI.K7500.K7500Device = VI.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                isr.VI.DeviceTests.DeviceManager.AssertFunctionModeShouldToggle(device.SenseSubsystem, SenseFunctionModes.ResistanceFourWire, SenseFunctionModes.Resistance)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

#Region " DISPLAY "

    ''' <summary> Assert display functions should pass. </summary>
    ''' <remarks> David, 2020-07-30. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Public Shared Sub AssertDisplayFunctionsShouldPass(ByVal subsystem As VI.DisplaySubsystemBase)
        Dim display As VI.DisplayScreens = subsystem.QueryDisplayScreens().GetValueOrDefault(VI.DisplayScreens.Home)
        subsystem.ApplyDisplayScreen(VI.DisplayScreens.UserSwipe)
        subsystem.ClearDisplay()
        Dim endTime As DateTime = DateTime.UtcNow.AddSeconds(3)
        Do Until endTime < DateTime.UtcNow
            subsystem.DisplayLine(1, endTime.Subtract(DateTime.UtcNow).ToString)
            Thread.Sleep(100)
        Loop
        subsystem.ClearDisplay()
        subsystem.ApplyDisplayScreen(display)
    End Sub

    ''' <summary> (Unit Test Method) displays the functions should pass. </summary>
    ''' <remarks> David, 2020-07-30. </remarks>
    <TestMethod()>
    Public Sub DisplayFunctionsShouldPass()
        Using device As VI.K7500.K7500Device = VI.K7500.K7500Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                SubsystemsTests.AssertDisplayFunctionsShouldPass(device.DisplaySubsystem)
            Catch
                Throw
            Finally
                DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class
