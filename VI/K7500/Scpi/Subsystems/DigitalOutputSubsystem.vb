Imports isr.Core.EnumExtensions

''' <summary>
''' Defines the contract that must be implemented by a SCPI Digital Output Subsystem.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class DigitalOutputSubsystem
    Inherits VI.DigitalOutputSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DigitalOutputSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
   Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.DigitalActiveLevelReadWrites.Clear()
        Me.DigitalActiveLevelReadWrites.Add(VI.DigitalActiveLevels.High, "AHIG", "AHIG", VI.DigitalActiveLevels.High.DescriptionUntil)
        Me.DigitalActiveLevelReadWrites.Add(VI.DigitalActiveLevels.Low, "ALOW", "ALOW", VI.DigitalActiveLevels.Low.DescriptionUntil)
    End Sub

#End Region

#Region " OUTPUT SUBSYSTEM "

    ' TO_DO:
    '     Protected Overrides Property OutputSignalPolarityQueryCommand As String = "OUTP:TTL{0}:LSEN?"
    '
    '    Protected Overrides Property OutputSignalPolarityCommandFormat As String = "OUTP:TTL{0}:LSEN {{0}}"
    '
    '    Protected Overrides Property OutputSignalPolarityQueryCommand As String = "SOUR:TTL4:BST?"
    '
    '    Protected Overrides Property OutputSignalPolarityCommandFormat As String = "SOUR:TTL4:BST {0}"
    '

#End Region

#Region " SOURCE SUBSYSTEM "

    ' TO_DO:
    '     Protected Overrides Property LevelQueryCommand As String = ":SOUR:TTL:LEV?"
    '
    '    Protected Overrides Property LevelCommandFormat As String = ":SOUR:TTL:LEV {0}"
    '
    '    Protected Overrides Property LineLevelQueryCommand As String = ":SOUR:TTL{0}:LEV?"
    '
    '    Protected Overrides Property LineLevelCommandFormat As String = ":SOUR:TTL{0}:LEV {{0}}"
    '

#End Region

End Class
