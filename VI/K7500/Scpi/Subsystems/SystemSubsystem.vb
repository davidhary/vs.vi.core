''' <summary> Defines a System Subsystem for a Keithley 7500 Meter. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class SystemSubsystem
    Inherits VI.SystemSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SystemSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        ' reduce noise when using IDE.
        If Debugger.IsAttached Then Me.ApplyFanLevel(VI.FanLevel.Quiet)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.FanLevel = VI.FanLevel.Normal
    End Sub

#End Region

#Region " COMMAND SYNTAX "

    ''' <summary> Gets or sets the initialize memory command. </summary>
    ''' <value> The initialize memory command. </value>
    Protected Overrides Property InitializeMemoryCommand As String = String.Empty

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <value> The preset command. </value>
    Protected Overrides Property PresetCommand As String = String.Empty

    ''' <summary> Gets or sets the language revision query command. </summary>
    ''' <value> The language revision query command. </value>
    Protected Overrides Property LanguageRevisionQueryCommand As String = VI.Pith.Scpi.Syntax.LanguageRevisionQueryCommand

#End Region

#Region " BEEPER ENABLED + IMMEDIATE "

    ''' <summary> Gets or sets the Beeper enabled query command. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT?". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overrides Property BeeperEnabledQueryCommand As String = ":SYST:BEEP:STAT?"

    ''' <summary> Gets or sets the Beeper enabled command Format. </summary>
    ''' <remarks> SCPI: ":SYST:BEEP:STAT {0:'1';'1';'0'}". </remarks>
    ''' <value> The Beeper enabled query command. </value>
    Protected Overrides Property BeeperEnabledCommandFormat As String = ":SYST:BEEP:STAT {0:'1';'1';'0'}"

    ''' <summary> Gets or sets the beeper immediate command format. </summary>
    ''' <value> The beeper immediate command format. </value>
    Protected Overrides Property BeeperImmediateCommandFormat As String = ":SYST:BEEP:IMM {0}, {1}"

#End Region

#Region " FAN LEVEL "

    ''' <summary> Gets or sets the Fan Level query command. </summary>
    ''' <value> The Fan Level command. </value>
    Protected Overrides Property FanLevelQueryCommand As String = String.Empty

    ''' <summary> Converts the specified value to string. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="P:isr.VI.SystemSubsystemBase.FanLevel">Fan Level</see>. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function FromFanLevel(ByVal value As FanLevel) As String
        Return If(value = VI.FanLevel.Normal, "NORM", "QUIET")
    End Function

    ''' <summary> Gets or sets the Fan Level command format. </summary>
    ''' <value> The Fan Level command format. </value>
    Protected Overrides Property FanLevelCommandFormat As String = ":SYST:FAN:LEV {0}"

#End Region

End Class

