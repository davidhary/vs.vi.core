﻿''' <summary> Display subsystem. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-14 </para>
''' </remarks>
Public Class DisplaySubsystem
    Inherits VI.DisplaySubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="DisplaySubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.Enabled = True
        Me.Exists = True
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " COMMANDS "

    ''' <summary>
    ''' Gets or sets the clear caution messages command. Impedance analyzer command.
    ''' </summary>
    ''' <value> The clear caution messages command. </value>
    Protected Overrides Property ClearCautionMessagesCommand As String = String.Empty

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearCommand As String = ":DISP:CLE"

#End Region

#Region " ENABLED "

    ''' <summary> Gets or sets the display enable command format. </summary>
    ''' <value> The display enable command format. </value>
    Protected Overrides Property DisplayEnableCommandFormat As String = String.Empty

    ''' <summary> Gets or sets the display enabled query command. </summary>
    ''' <value> The display enabled query command. </value>
    Protected Overrides Property DisplayEnabledQueryCommand As String = String.Empty

#End Region

#Region " DISPLAY SCREEN  "

    ''' <summary> Gets or sets the display Screen command format. </summary>
    ''' <value> The display Screen command format. </value>
    Protected Overrides Property DisplayScreenCommandFormat As String = ":DISP:SCR {0}"

    ''' <summary> Gets or sets the display Screen query command. </summary>
    ''' <value> The display Screen query command. </value>
    Protected Overrides Property DisplayScreenQueryCommand As String = String.Empty

#End Region

#Region " DISPLAY TEXT "

    ''' <summary> Gets or sets the Display Line command. </summary>
    ''' <remarks> SCPI: ":DISP:USER{0}:TEXT ""{1}""". </remarks>
    ''' <value> The Display Line command. </value>
    Protected Overrides Property DisplayLineCommandFormat As String = ":DISP:USER{0}:TEXT ""{1}"""

#End Region

#End Region

End Class
