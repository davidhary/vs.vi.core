''' <summary> Defines a Trigger Subsystem for a Keithley 7500 Meter. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class TriggerSubsystem
    Inherits VI.TriggerSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="TriggerSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.SupportedTriggerSources = VI.TriggerSources.Bus Or VI.TriggerSources.External Or
                                     VI.TriggerSources.Hold Or VI.TriggerSources.Immediate Or
                                     VI.TriggerSources.Manual Or VI.TriggerSources.Timer Or
                                     VI.TriggerSources.TriggerLink
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " ABORT / INIT COMMANDS "

    ''' <summary> Gets or sets the Abort command. </summary>
    ''' <value> The Abort command. </value>
    Protected Overrides Property AbortCommand As String = ":ABOR"

    ''' <summary> Gets or sets the initiate command. </summary>
    ''' <value> The initiate command. </value>
    Protected Overrides Property InitiateCommand As String = ":INIT"

    ''' <summary> Gets or sets the clear command. </summary>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearCommand As String = ":TRIG:EXT:IN:CLE"

    ''' <summary> Gets or sets the clear  trigger model command. </summary>
    ''' <remarks> SCPI: ":TRIG:LOAD 'EMPTY'". </remarks>
    ''' <value> The clear command. </value>
    Protected Overrides Property ClearTriggerModelCommand As String = ":TRIG:LOAD 'EMPTY'"

#End Region

#Region " TRIGGER STATE "

    ''' <summary> Gets or sets the Trigger State query command. </summary>
    ''' <remarks>
    '''    <c>SCPI: :TRIG:STAT? <para>
    ''' TSP2: trigger.model.state()
    ''' </para></c>
    ''' </remarks>
    ''' <value> The Trigger State query command. </value>
    Protected Overrides Property TriggerStateQueryCommand As String = ":TRIG:STAT?"

#End Region

#Region " TRIGGER COUNT "

    ''' <summary> Gets or sets trigger count query command. </summary>
    ''' <value> The trigger count query command. </value>
    Protected Overrides Property TriggerCountQueryCommand As String = String.Empty ' ":TRIG:COUN?" 

    ''' <summary> Gets or sets trigger count command format. </summary>
    ''' <value> The trigger count command format. </value>
    Protected Overrides Property TriggerCountCommandFormat As String = String.Empty ' ":TRIG:COUN {0}"

#End Region

#Region " DELAY "

    ''' <summary> Gets or sets the delay command format. </summary>
    ''' <value> The delay command format. </value>
    Protected Overrides Property DelayCommandFormat As String = String.Empty '  ":TRIG:DEL {0:s\.FFFFFFF}"

    ''' <summary> Gets or sets the Delay format for converting the query to time span. </summary>
    ''' <value> The Delay query command. </value>
    Protected Overrides Property DelayFormat As String = "s\.FFFFFFF"

    ''' <summary> Gets or sets the delay query command. </summary>
    ''' <value> The delay query command. </value>
    Protected Overrides Property DelayQueryCommand As String = String.Empty '  ":TRIG:DEL?"

#End Region

#Region " SOURCE "

    ''' <summary> Gets or sets the Trigger Source query command. </summary>
    ''' <value> The Trigger Source query command. </value>
    Protected Overrides Property TriggerSourceQueryCommand As String = String.Empty '  ":TRIG:SOUR?"

    ''' <summary> Gets or sets the Trigger Source command format. </summary>
    ''' <value> The Trigger Source command format. </value>
    Protected Overrides Property TriggerSourceCommandFormat As String = String.Empty '  ":TRIG:SOUR {0}"

#End Region

#End Region

#Region " SIMPLE LOOP "

    ''' <summary> The simple loop model. </summary>
    Private Const _SimpleLoopModel As String = "SimpleLoop"

    ''' <summary> Loads simple loop. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="count"> Number of. </param>
    ''' <param name="delay"> The delay. </param>
    Public Sub LoadSimpleLoop(ByVal count As Integer, ByVal delay As TimeSpan)
        Me.WriteLine(":TRIG:LOAD '{0}',{1},{2}", _SimpleLoopModel, count, delay.TotalSeconds)
    End Sub

#End Region

#Region " GRAD BINNING "

    ''' <summary> The grade binning model. </summary>
    Private Const _GradeBinningModel As String = "GradeBinning"

    ''' <summary> Loads grade binning. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="count">            Number of. </param>
    ''' <param name="triggerOption">    The trigger option (use 7 for external line). </param>
    ''' <param name="startDelay">       The start delay. </param>
    ''' <param name="endDelay">         The end delay. </param>
    ''' <param name="highLimit">        The high limit. </param>
    ''' <param name="lowLimit">         The low limit. </param>
    ''' <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
    ''' <param name="passBitPattern">   A pattern specifying the pass bit. </param>
    Public Sub LoadGradeBinning(ByVal count As Integer, ByVal triggerOption As Integer,
                                ByVal startDelay As TimeSpan, ByVal endDelay As TimeSpan,
                                ByVal highLimit As Double, ByVal lowLimit As Double,
                                ByVal failedBitPattern As Integer, ByVal passBitPattern As Integer)
        Me.WriteLine(":TRIG:LOAD '{0}',{1},{2},{3},{4},{5},{6},{7},{8}",
                 _GradeBinningModel, count, triggerOption, startDelay.TotalSeconds, endDelay.TotalSeconds, highLimit, lowLimit, failedBitPattern, passBitPattern)
    End Sub

    ''' <summary> Loads grade binning. </summary>
    ''' <remarks> Uses external trigger (start line = 7). </remarks>
    ''' <param name="count">            Number of. </param>
    ''' <param name="startDelay">       The start delay. </param>
    ''' <param name="endDelay">         The end delay. </param>
    ''' <param name="highLimit">        The high limit. </param>
    ''' <param name="lowLimit">         The low limit. </param>
    ''' <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
    ''' <param name="passBitPattern">   A pattern specifying the pass bit. </param>
    Public Sub LoadGradeBinning(ByVal count As Integer, ByVal startDelay As TimeSpan, ByVal endDelay As TimeSpan,
                                ByVal highLimit As Double, ByVal lowLimit As Double,
                                ByVal failedBitPattern As Integer, ByVal passBitPattern As Integer)
        Me.LoadGradeBinning(count, 7, startDelay, endDelay, highLimit, lowLimit, failedBitPattern, passBitPattern)
    End Sub

    ''' <summary> Loads grade binning. </summary>
    ''' <remarks> Uses external trigger (start line = 7) and maximum count (268000000). </remarks>
    ''' <param name="startDelay">       The start delay. </param>
    ''' <param name="endDelay">         The end delay. </param>
    ''' <param name="highLimit">        The high limit. </param>
    ''' <param name="lowLimit">         The low limit. </param>
    ''' <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
    ''' <param name="passBitPattern">   A pattern specifying the pass bit. </param>
    Public Sub LoadGradeBinning(ByVal startDelay As TimeSpan, ByVal endDelay As TimeSpan,
                                ByVal highLimit As Double, ByVal lowLimit As Double,
                                ByVal failedBitPattern As Integer, ByVal passBitPattern As Integer)
        Me.LoadGradeBinning(268000000, 7, startDelay, endDelay, highLimit, lowLimit, failedBitPattern, passBitPattern)
    End Sub

#End Region

#Region " CUSTOM BINNING "

    ''' <summary> Applies the grade binning. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="count">            Number of. </param>
    ''' <param name="startDelay">       The start delay. </param>
    ''' <param name="failedBitPattern"> A pattern specifying the failed bit. </param>
    ''' <param name="passBitPattern">   A pattern specifying the pass bit. </param>
    Public Sub ApplyGradeBinning(ByVal count As Integer, ByVal startDelay As TimeSpan,
                                 ByVal failedBitPattern As Integer, ByVal passBitPattern As Integer)

        Dim block As Integer = 0

        ' use the mask to assign digital outputs.
        Dim mask As Integer = failedBitPattern Or passBitPattern
        Dim bitPattern As Integer = 1
        Dim cmd As String
        For i As Integer = 1 To 6
            If (mask And bitPattern) <> 0 Then
                cmd = $"DIG:LINE{i}:MODE DIG,OUT"
                Me.WriteLine(cmd)
            End If
            bitPattern <<= 1
        Next

        ' clear the trigger model
        cmd = ":TRIG:LOAD 'EMPTY'" : Me.WriteLine(cmd)

        ' clear any pending trigger
        cmd = ":TRIG:EXT:IN:CLE" : Me.WriteLine(cmd)

        ' clear the default buffer
        cmd = ":TRAC:CLE" : Me.WriteLine(cmd)

        Dim autonomous As Boolean = count = 1

        ' Block 1: 
        ' -- if autonomous mode, clear the buffer
        ' -- if program control, clear the buffer
        block += 1 : cmd = If(autonomous, $":TRIG:BLOC:BUFF:CLE {block}", $":TRIG:BLOC:NOP {block}")
        Me.WriteLine(cmd)

        ' Block 2: Wait for external trigger; this is the repeat block.
        Dim repeatBlock As Integer = block + 1
        block += 1 : cmd = $":TRIG:BLOC:WAIT {block}, EXT"
        Me.WriteLine(cmd)

        ' Block 3: Pre-Measure Delay
        block += 1 : cmd = $":TRIG:BLOC:DEL:CONS {block}, {0.001 * startDelay.TotalMilliseconds}"
        Me.WriteLine(cmd)

        ' Block 4: Measure and save to the default buffer.
        block += 1 : cmd = $":TRIG:BLOC:MEAS {block}"
        Me.WriteLine(cmd)

        ' Block 5: Limit test and branch; the pass block is 3 blocks ahead (8)
        Dim passBlock As Integer = block + 4
        block += 1 : cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,1,{passBlock}"
        Me.WriteLine(cmd)

        ' Block 6: Output failure bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{failedBitPattern},{mask}"
        Me.WriteLine(cmd)

        ' Block 7: Skip the pass binning block
        block += 1 : cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {block + 2}"
        Me.WriteLine(cmd)

        ' Block 8: Output pass bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{passBitPattern},{mask}"
        Me.WriteLine(cmd)

        Dim notificationId As Integer = 1

        ' Block 9: Notify measurement completed
        block += 1 : cmd = $":TRIG:BLOC:NOT {block},{notificationId}"
        Me.WriteLine(cmd)

        ' set external output setting
        cmd = $":TRIG:EXT:OUT:STIM NOT{notificationId}"
        Me.WriteLine(cmd)

        ' Block 10: Repeat Count times.
        block += 1 : cmd = If(count <= 0, $":TRIG:BLOCk:BRAN:ALW {block},{repeatBlock}", $":TRIG:BLOC:BRAN:COUN {block},{count},{repeatBlock}")
        Me.WriteLine(cmd)

    End Sub

    ''' <summary> Applies the grade binning. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="count">                 Number of. </param>
    ''' <param name="startDelay">            The start delay. </param>
    ''' <param name="failedBitPattern">      A pattern specifying the failed bit. </param>
    ''' <param name="passBitPattern">        A pattern specifying the pass bit. </param>
    ''' <param name="openContactBitPattern"> A pattern specifying the open contact bit. </param>
    ''' <param name="triggerSource">         The trigger source. </param>
    Public Sub ApplyGradeBinning(ByVal count As Integer, ByVal startDelay As TimeSpan,
                                 ByVal failedBitPattern As Integer, ByVal passBitPattern As Integer,
                                 ByVal openContactBitPattern As Integer, ByVal triggerSource As VI.TriggerSources)

        Dim block As Integer = 0

        ' use the mask to assign digital outputs.
        Dim mask As Integer = failedBitPattern Or passBitPattern Or openContactBitPattern
        Dim bitPattern As Integer = 1
        Dim cmd As String
        For i As Integer = 1 To 6
            If (mask And bitPattern) <> 0 Then
                cmd = $"DIG:LINE{i}:MODE DIG,OUT"
                Me.WriteLine(cmd)
            End If
            bitPattern <<= 1
        Next

        ' clear the trigger model
        Me.ClearTriggerModel()

        ' clear any pending trigger
        Me.ClearTriggers()

        Dim autonomous As Boolean = count = 1

        ' Block 1: 
        ' -- if autonomous mode, clear the buffer
        ' -- if program control, clear the buffer
        block += 1 : cmd = If(autonomous, $":TRIG:BLOC:BUFF:CLE {block}", $":TRIG:BLOC:NOP {block}")
        Me.WriteLine(cmd)

        ' Block 2: Wait for external trigger; this is the repeat block.
        Dim repeatBlock As Integer = block + 1
        block += 1
        cmd = $":TRIG:BLOC:WAIT {block}, EXT"
        If 0 <> (triggerSource And VI.TriggerSources.Bus) Then
            cmd = $":TRIG:BLOC:WAIT {block}, COMM"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.Manual) Then
            cmd = $":TRIG:BLOC:WAIT {block}, DISP"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.TriggerLink) Then
            cmd = $":TRIG:BLOC:WAIT {block}, TSPL(1)"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.Timer) Then
            cmd = $":TRIG:BLOC:WAIT {block}, TIM(1)"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.Digital) Then
            cmd = $":TRIG:BLOC:WAIT {block}, DIG(1)"
        End If
        Me.WriteLine(cmd)

        ' Block 3: Pre-Measure Delay
        block += 1 : cmd = $":TRIG:BLOC:DEL:CONS {block}, {0.001 * startDelay.TotalMilliseconds}"
        Me.WriteLine(cmd)

        ' Block 4: Measure and save to the default buffer.
        block += 1 : cmd = $":TRIG:BLOC:MEAS {block}"
        Me.WriteLine(cmd)

        ' Block 5: Limit 2 (open contact) test and branch if INside to the limit block; the limit 1 block is 3 blocks ahead (8)
        Dim limitBlock As Integer = block + 4
        block += 1 : cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,2,{limitBlock}"
        Me.WriteLine(cmd)

        ' Block 6: Output open bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{openContactBitPattern},{mask}"
        Me.WriteLine(cmd)

        ' Block 7: Jump to the notification block number
        Dim notificationBlock As Integer = block + 6 ' = 12
        block += 1 : cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {notificationBlock}"
        Me.WriteLine(cmd)

        ' Block 8: Limit 1 test and branch if INside to the pass block; the pass block is 3 blocks ahead
        Dim passBlock As Integer = block + 4 ' = 11
        block += 1 : cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,1,{passBlock}"
        Me.WriteLine(cmd)

        ' Block 9: Output failure bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{failedBitPattern},{mask}"
        Me.WriteLine(cmd)

        ' Block 10: Jump to the notification block
        block += 1 : cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {notificationBlock}"
        Me.WriteLine(cmd)

        ' Block 11: Output pass bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{passBitPattern},{mask}"
        Me.WriteLine(cmd)

        Dim notificationId As Integer = 1

        ' Block 12: Notify measurement completed
        block += 1 : cmd = $":TRIG:BLOC:NOT {block},{notificationId}"
        Me.WriteLine(cmd)

        ' set external output setting
        cmd = $":TRIG:EXT:OUT:STIM NOT{notificationId}"
        Me.WriteLine(cmd)

        ' Block 13: Repeat Count times.
        block += 1 : cmd = If(count <= 0, $":TRIG:BLOCk:BRAN:ALW {block},{repeatBlock}", $":TRIG:BLOC:BRAN:COUN {block},{count},{repeatBlock}")
        Me.WriteLine(cmd)

    End Sub

    ''' <summary> Applies the meter complete first grade binning. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="count">                 Number of. </param>
    ''' <param name="startDelay">            The start delay. </param>
    ''' <param name="failedBitPattern">      A pattern specifying the failed bit. </param>
    ''' <param name="passBitPattern">        A pattern specifying the pass bit. </param>
    ''' <param name="openContactBitPattern"> A pattern specifying the open contact bit. </param>
    ''' <param name="triggerSource">         The trigger source. </param>
    Public Sub ApplyMeterCompleteFirstGradeBinning(ByVal count As Integer, ByVal startDelay As TimeSpan,
                                                   ByVal failedBitPattern As Integer, ByVal passBitPattern As Integer,
                                                   ByVal openContactBitPattern As Integer, ByVal triggerSource As VI.TriggerSources)

        Dim block As Integer = 0

        ' use the mask to assign digital outputs.
        Dim mask As Integer = failedBitPattern Or passBitPattern Or openContactBitPattern
        Dim bitPattern As Integer = 1
        Dim cmd As String
        For i As Integer = 1 To 6
            If (mask And bitPattern) <> 0 Then
                cmd = $"DIG:LINE{i}:MODE DIG,OUT"
                Me.WriteLine(cmd)
            End If
            bitPattern <<= 1
        Next

        ' clear the trigger model
        Me.ClearTriggerModel()

        ' clear any pending trigger
        Me.ClearTriggers()

        Dim autonomous As Boolean = count = 1

        ' Block 1: 
        ' -- if autonomous mode, clear the buffer
        ' -- if program control, clear the buffer
        block += 1 : cmd = If(autonomous, $":TRIG:BLOC:BUFF:CLE {block}", $":TRIG:BLOC:NOP {block}")
        Me.WriteLine(cmd)

        Dim notificationId As Integer = 1

        ' Block 2: Notify measurement completed; this is the loop start block.
        Dim loopStartBlock As Integer = block + 1
        block += 1 : cmd = $":TRIG:BLOC:NOT {block},{notificationId}"
        Me.WriteLine(cmd)

        ' set external output setting
        cmd = $":TRIG:EXT:OUT:STIM NOT{notificationId}"
        Me.WriteLine(cmd)

        ' Block 3: Wait for external trigger
        block += 1
        cmd = $":TRIG:BLOC:WAIT {block}, EXT"
        If 0 <> (triggerSource And VI.TriggerSources.Bus) Then
            cmd = $":TRIG:BLOC:WAIT {block}, COMM"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.Manual) Then
            cmd = $":TRIG:BLOC:WAIT {block}, DISP"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.TriggerLink) Then
            cmd = $":TRIG:BLOC:WAIT {block}, TSPL(1)"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.Timer) Then
            cmd = $":TRIG:BLOC:WAIT {block}, TIM(1)"
        ElseIf 0 <> (triggerSource And VI.TriggerSources.Digital) Then
            cmd = $":TRIG:BLOC:WAIT {block}, DIG(1)"
        End If
        Me.WriteLine(cmd)

        ' Block 4: Pre-Measure Delay
        block += 1 : cmd = $":TRIG:BLOC:DEL:CONS {block}, {0.001 * startDelay.TotalMilliseconds}"
        Me.WriteLine(cmd)

        ' Block 5: Measure and save to the default buffer.
        block += 1 : cmd = $":TRIG:BLOC:MEAS {block}"
        Me.WriteLine(cmd)

        ' Block 6: Limit 2 (open contact) test and branch if INside to the limit block; the limit 1 block is 3 blocks ahead (8)
        Dim limitBlock As Integer = block + 4
        block += 1 : cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,2,{limitBlock}"
        Me.WriteLine(cmd)

        ' Block 7: Output open bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{openContactBitPattern},{mask}"
        Me.WriteLine(cmd)

        ' Block 8: Jump to the loop back block number
        Dim loopBackBlock As Integer = block + 6 ' = 12
        block += 1 : cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {loopBackBlock}"
        Me.WriteLine(cmd)

        ' Block 8: Limit 1 test and branch if INside to the pass block; the pass block is 3 blocks ahead
        Dim passBlock As Integer = block + 4 ' = 11
        block += 1 : cmd = $":TRIG:BLOC:BRAN:LIM:DYN {block},IN,1,{passBlock}"
        Me.WriteLine(cmd)

        ' Block 9: Output failure bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{failedBitPattern},{mask}"
        Me.WriteLine(cmd)

        ' Block 10: Jump to the loop back block
        block += 1 : cmd = $":TRIG:BLOCk:BRAN:ALW {block}, {loopBackBlock}"
        Me.WriteLine(cmd)

        ' Block 11: Output pass bit pattern
        block += 1 : cmd = $":TRIG:BLOC:DIG:IO {block},{passBitPattern},{mask}"
        Me.WriteLine(cmd)

        ' Block 13: Repeat Count times; Jump to the loop start block
        block += 1 : cmd = If(count <= 0, $":TRIG:BLOCk:BRAN:ALW {block},{loopStartBlock}", $":TRIG:BLOC:BRAN:COUN {block},{count},{loopStartBlock}")
        Me.WriteLine(cmd)

    End Sub

#End Region

End Class
