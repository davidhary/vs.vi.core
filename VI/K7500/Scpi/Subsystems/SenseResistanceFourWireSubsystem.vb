''' <summary> A sense four wire resistance subsystem. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-04-08 </para>
''' </remarks>
Public Class SenseResistanceFourWireSubsystem
    Inherits VI.SenseResistanceSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="SenseSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem, New Readings)
        Me.ReadingAmounts.Initialize(ReadingElementTypes.Reading)
        Me.ReadingAmounts.PrimaryReading.ApplyUnit(Arebis.StandardUnits.ElectricUnits.Ohm)
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm
        Me.DefineFunctionModeReadWrites("{0}", "'{0}'")

        Me.ResistanceRangeCurrents.Clear()
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(0, 0, "Auto Range"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20, 0.0072D, $"20 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 7.2 mA"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200, 0.00096D, $"200 {Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000, 0.00096D, $"2 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 960 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(20000, 0.000096D, $"20 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 96 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(200000, 0.0000096D, $"200 K{Arebis.StandardUnits.UnitSymbols.Omega} range @ 9.6 {Arebis.StandardUnits.UnitSymbols.MU}A"))
        Me.ResistanceRangeCurrents.Add(New ResistanceRangeCurrent(2000000, 0.0000019D, $"2 M{Arebis.StandardUnits.UnitSymbols.Omega} range @ 1.9 {Arebis.StandardUnits.UnitSymbols.MU}A"))
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.PowerLineCyclesRange = If(Me.StatusSubsystem.LineFrequency.GetValueOrDefault(60) = 60,
            New isr.Core.Primitives.RangeR(0.0005, 15),
            New isr.Core.Primitives.RangeR(0.0005, 12))
        Me.FunctionRange = New isr.Core.Primitives.RangeR(1, 1000000000.0)
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.DefaultFunctionUnit = Arebis.StandardUnits.ElectricUnits.Ohm
        MyBase.DefineKnownResetState()
        Me.PowerLineCyclesRange = If(Me.StatusSubsystem.LineFrequency.GetValueOrDefault(60) = 60,
            New isr.Core.Primitives.RangeR(0.0005, 15),
            New isr.Core.Primitives.RangeR(0.0005, 12))
        Me.FunctionRange = New isr.Core.Primitives.RangeR(1, 1000000000.0)
    End Sub

#End Region

#Region " COMMAND SYNTAX "

#Region " AVERAGE "

    ''' <summary> Gets or sets the average enabled command Format. </summary>
    ''' <value> The average enabled query command. </value>
    Protected Overrides Property AverageEnabledCommandFormat As String = ":SENS:FRES:AVER {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the average enabled query command. </summary>
    ''' <value> The average enabled query command. </value>
    Protected Overrides Property AverageEnabledQueryCommand As String = ":SENS:FRES:AVER?"

#End Region

#Region " AVERAGE COUNT "

    ''' <summary> Gets or sets the Average Count command format. </summary>
    ''' <value> The AverageCount command format. </value>
    Protected Overrides Property AverageCountCommandFormat As String = ":SENS:FRES:AVER:COUNT {0}"

    ''' <summary> Gets or sets the Average Count query command. </summary>
    ''' <value> The AverageCount query command. </value>
    Protected Overrides Property AverageCountQueryCommand As String = ":SENS:FRES:AVER:COUNT?"

#End Region

#Region " AVERAGE FILTER TYPE "

    ''' <summary> Gets or sets the Average FilterType command format. </summary>
    ''' <value> The AverageFilterType command format. </value>
    Protected Overrides Property AverageFilterTypeCommandFormat As String = ":SENS:FRES:AVER:TCON {0}"

    ''' <summary> Gets or sets the Average FilterType query command. </summary>
    ''' <value> The AverageFilterType query command. </value>
    Protected Overrides Property AverageFilterTypeQueryCommand As String = ":SENS:FRES:AVER:TCON?"

#End Region

#Region " AVERAGE PERCENT WINDOW "

    ''' <summary> Gets or sets the Average Percent Window command format. </summary>
    ''' <value> The AveragePercentWindow command format. </value>
    Protected Overrides Property AveragePercentWindowCommandFormat As String = ":SENS:FRES:AVER:WIND {0}"

    ''' <summary> Gets or sets the Average Percent Window query command. </summary>
    ''' <value> The AveragePercentWindow query command. </value>
    Protected Overrides Property AveragePercentWindowQueryCommand As String = ":SENS:FRES:AVER:WIND?"

#End Region

#Region " AUTO ZERO "

    ''' <summary> Gets or sets the automatic Zero enabled command Format. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledCommandFormat As String = ":SENS:FRES:AZER {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Zero enabled query command. </summary>
    ''' <value> The automatic Zero enabled query command. </value>
    Protected Overrides Property AutoZeroEnabledQueryCommand As String = ":SENS:FRES:AZER?"

#End Region

#Region " AUTO RANGE "

    ''' <summary> Gets or sets the automatic Range enabled command Format. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledCommandFormat As String = ":SENS:FRES:RANG:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the automatic Range enabled query command. </summary>
    ''' <value> The automatic Range enabled query command. </value>
    Protected Overrides Property AutoRangeEnabledQueryCommand As String = ":SENS:FRES:RANG:AUTO?"

#End Region

#Region " FUNCTION MODE "

    ''' <summary> Gets or sets the Function Mode command format. </summary>
    ''' <value> The Function Mode command format. </value>
    Protected Overrides Property FunctionModeCommandFormat As String = ":SENS:FUNC {0}"

    ''' <summary> Gets or sets the Function Mode query command. </summary>
    ''' <value> The Function Mode query command. </value>
    Protected Overrides Property FunctionModeQueryCommand As String = ":SENS:FUNC?"

#End Region

#Region " OPEN LEAD DETECTOR "

    ''' <summary> Gets or sets the Open Lead Detector enabled command Format. </summary>
    ''' <value> The Open Lead Detector enabled query command. </value>
    Protected Overrides Property OpenLeadDetectorEnabledCommandFormat As String = ":SENS:FRES:ODET {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Open Lead Detector enabled query command. </summary>
    ''' <value> The Open Lead Detector enabled query command. </value>
    Protected Overrides Property OpenLeadDetectorEnabledQueryCommand As String = ":SENS:FRES:ODET?"

#End Region

#Region " POWER LINE CYCLES "

    ''' <summary> Gets or sets The Power Line Cycles command format. </summary>
    ''' <value> The Power Line Cycles command format. </value>
    Protected Overrides Property PowerLineCyclesCommandFormat As String = ":SENS:FRES:NPLC {0}"

    ''' <summary> Gets or sets The Power Line Cycles query command. </summary>
    ''' <value> The Power Line Cycles query command. </value>
    Protected Overrides Property PowerLineCyclesQueryCommand As String = ":SENS:FRES:NPLC?"

#End Region

#Region " RANGE "

    ''' <summary> The maximum Kelvin test resistance. </summary>
    Public Const MaximumKelvinTestResistance As Double = 2100000.0

    ''' <summary> Determine if we can use Kelvin (4-wire) measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="resistance"> The resistance. </param>
    ''' <returns> <c>true</c> if we can use Kelvin; otherwise <c>false</c> </returns>
    Public Shared Function CanUseKelvin(ByVal resistance As Double) As Boolean
        Return resistance <= MaximumKelvinTestResistance
    End Function

    ''' <summary> Gets or sets the range command format. </summary>
    ''' <value> The range command format. </value>
    Protected Overrides Property RangeCommandFormat As String = ":SENS:FRES:RANG {0}"

    ''' <summary> Gets or sets the range query command. </summary>
    ''' <value> The range query command. </value>
    Protected Overrides Property RangeQueryCommand As String = ":SENS:FRES:RANG?"

#End Region

#End Region

End Class

