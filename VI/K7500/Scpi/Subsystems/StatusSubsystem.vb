Imports isr.Core.EscapeSequencesExtensions

''' <summary> Defines a Status Subsystem for a Keithley 7500 Meter. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-09-22, 3.0.5013. </para>
''' </remarks>
Public Class StatusSubsystem
    Inherits VI.StatusSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="StatusSubsystem" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="VI.Pith.SessionBase">message based
    '''                        session</see>. </param>
    Public Sub New(ByVal session As VI.Pith.SessionBase)
        MyBase.New(VI.Pith.SessionBase.Validated(session))
        Me._VersionInfo = New VersionInfo
        Me.VersionInfoBase = Me._VersionInfo
        StatusSubsystem.InitializeSession(session)
    End Sub

    ''' <summary> Creates a new StatusSubsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <returns> A StatusSubsystem. </returns>
    Public Shared Function Create() As StatusSubsystem
        Dim subsystem As StatusSubsystem = Nothing
        Try
            subsystem = New StatusSubsystem(isr.VI.SessionFactory.Get.Factory.Session())
        Catch
            If subsystem IsNot Nothing Then
            End If
            Throw
        End Try
        Return subsystem
    End Function

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Define measurement events bit values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Overloads Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.MeasurementEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        Dim failuresSummaryBitmask As Integer = 0
        Dim bitmask As Integer
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.Questionable, 1 << 0)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.Origin, 1 << 1 Or 1 << 2)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.MeasurementTerminal, 1 << 3)
        bitmask = 1 << 4 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit2, bitmask)
        bitmask = 1 << 5 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit2, bitmask)
        bitmask = 1 << 6 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.LowLimit1, bitmask)
        bitmask = 1 << 7 : failuresSummaryBitmask += bitmask : bitmaskDictionary.Add(MeasurementEventBitmaskKey.HighLimit1, bitmask)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.FirstReadingInGroup, 1 << 8)
        bitmaskDictionary.Add(MeasurementEventBitmaskKey.FailuresSummary, failuresSummaryBitmask)
    End Sub

    ''' <summary> Define measurement events bit values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub DefineMeasurementEventsBitmasks()
        MyBase.DefineMeasurementEventsBitmasks()
        StatusSubsystem.DefineBitmasks(Me.MeasurementEventsBitmasks)
    End Sub

    ''' <summary> Define operation event bitmasks. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="bitmaskDictionary"> The bitmask dictionary. </param>
    Public Overloads Shared Sub DefineBitmasks(ByVal bitmaskDictionary As isr.VI.OperationEventsBitmaskDictionary)
        If bitmaskDictionary Is Nothing Then Throw New ArgumentNullException(NameOf(bitmaskDictionary))
        bitmaskDictionary.Add(OperationEventBitmaskKey.Arming, 1 << 6)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Calibrating, 1 << 0)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Idle, 1 << 10)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Measuring, 1 << 4)
        bitmaskDictionary.Add(OperationEventBitmaskKey.Triggering, 1 << 5)
    End Sub

    ''' <summary> Defines the operation event bit values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub DefineOperationEventsBitmasks()
        MyBase.DefineOperationEventsBitmasks()
        StatusSubsystem.DefineBitmasks(Me.OperationEventsBitmasks)
    End Sub

#End Region

#Region " SESSION "

    ''' <summary> Initializes the session. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="session"> A reference to a <see cref="Session">message based TSP session</see>. </param>
    Private Shared Sub InitializeSession(ByVal session As VI.Pith.SessionBase)
        session.ClearExecutionStateCommand = VI.Pith.Ieee488.Syntax.ClearExecutionStateCommand
        session.ResetKnownStateCommand = VI.Pith.Ieee488.Syntax.ResetKnownStateCommand
        session.ErrorAvailableBit = VI.Pith.ServiceRequests.ErrorAvailable
        session.MeasurementEventBit = VI.Pith.ServiceRequests.MeasurementEvent
        session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        session.StandardEventBit = VI.Pith.ServiceRequests.StandardEvent
        session.OperationCompletedQueryCommand = VI.Pith.Ieee488.Syntax.OperationCompletedQueryCommand
        session.StandardServiceEnableCommandFormat = VI.Pith.Ieee488.Syntax.StandardServiceEnableCommandFormat
        session.StandardServiceEnableCompleteCommandFormat = VI.Pith.Ieee488.Syntax.StandardServiceEnableCompleteCommandFormat
        session.ServiceRequestEnableCommandFormat = VI.Pith.Ieee488.Syntax.ServiceRequestEnableCommandFormat
        session.ServiceRequestEnableQueryCommand = VI.Pith.Ieee488.Syntax.ServiceRequestEnableQueryCommand
        session.StandardEventStatusQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventStatusQueryCommand
        session.StandardEventEnableQueryCommand = VI.Pith.Ieee488.Syntax.StandardEventEnableQueryCommand
        session.WaitCommand = VI.Pith.Ieee488.Syntax.WaitCommand
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Gets or sets the preset command. </summary>
    ''' <remarks>
    ''' SCPI: ":STAT:PRES".
    ''' <see cref="F:isr.VI.Pith.Scpi.Syntax.ScpiSyntax.StatusPresetCommand"></see>
    ''' </remarks>
    ''' <value> The preset command. </value>
    Protected Overrides Property PresetCommand As String = VI.Pith.Scpi.Syntax.StatusPresetCommand

#End Region

#Region " LANGUAGE "

    ''' <summary> Gets or sets the Language query command. </summary>
    ''' <value> The Language query command. </value>
    Protected Overrides Property LanguageQueryCommand As String = VI.Pith.Ieee488.Syntax.LanguageQueryCommand

    ''' <summary> Gets or sets the Language command format. </summary>
    ''' <value> The Language command format. </value>
    Protected Overrides Property LanguageCommandFormat As String = VI.Pith.Ieee488.Syntax.LanguageCommandFormat

#End Region

#Region " DEVICE ERRORS "

    ''' <summary> Gets or sets the clear error queue command. </summary>
    ''' <value> The clear error queue command. </value>
    Protected Overrides Property ClearErrorQueueCommand As String = "SYST:CLE" ' VI.Pith.Scpi.Syntax.ClearErrorQueueCommand

    ''' <summary> Gets or sets the 'Next Error' query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property DequeueErrorQueryCommand As String = VI.Pith.Scpi.Syntax.LastSystemErrorQueryCommand

    ''' <summary> Gets or sets the last error query command. </summary>
    ''' <value> The last error query command. </value>
    Protected Overrides Property DeviceErrorQueryCommand As String = String.Empty '  VI.Pith.Scpi.Syntax.ErrorQueueQueryCommand

    ''' <summary> Gets or sets the error queue query command. </summary>
    ''' <value> The error queue query command. </value>
    Protected Overrides Property NextDeviceErrorQueryCommand As String = String.Empty '  VI.Pith.Scpi.Syntax.ErrorQueueQueryCommand

#End Region

#Region " MEASUREMENT REGISTER EVENTS "

    ''' <summary> Gets or sets the measurement status query command. </summary>
    ''' <value> The measurement status query command. </value>
    Protected Overrides Property MeasurementStatusQueryCommand As String = String.Empty ' VI.Pith.Scpi.Syntax.MeasurementEventQueryCommand

    ''' <summary> Gets or sets the measurement event condition query command. </summary>
    ''' <value> The measurement event condition query command. </value>
    Protected Overrides Property MeasurementEventConditionQueryCommand As String = String.Empty '  VI.Pith.Scpi.Syntax.MeasurementEventConditionQueryCommand

#End Region

#Region " OPERATION REGISTER EVENTS "

    ''' <summary> Gets or sets the operation event enable Query command. </summary>
    ''' <value> The operation event enable Query command. </value>
    Protected Overrides Property OperationEventEnableQueryCommand As String = VI.Pith.Scpi.Syntax.OperationEventEnableQueryCommand

    ''' <summary> Gets or sets the operation event enable command format. </summary>
    ''' <value> The operation event enable command format. </value>
    Protected Overrides Property OperationEventEnableCommandFormat As String = VI.Pith.Scpi.Syntax.OperationEventEnableCommandFormat

    ''' <summary> Gets or sets the operation event status query command. </summary>
    ''' <value> The operation event status query command. </value>
    Protected Overrides Property OperationEventStatusQueryCommand As String = VI.Pith.Scpi.Syntax.OperationEventQueryCommand

#End Region

#Region " QUESTIONABLE REGISTER "

    ''' <summary> Gets or sets the questionable status query command. </summary>
    ''' <value> The questionable status query command. </value>
    Protected Overrides Property QuestionableStatusQueryCommand As String = VI.Pith.Scpi.Syntax.QuestionableEventQueryCommand

#End Region

#Region " IDENTITY "

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Protected Overrides Property IdentityQueryCommand As String = VI.Pith.Ieee488.Syntax.IdentityQueryCommand

    ''' <summary> Queries the Identity. </summary>
    ''' <remarks> Sends the <see cref="IdentityQueryCommand">identity query</see>/&gt;. </remarks>
    ''' <returns> System.String. </returns>
    Public Overrides Function QueryIdentity() As String
        If Not String.IsNullOrWhiteSpace(Me.IdentityQueryCommand) Then
            Me.PublishVerbose("Requesting identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            Me.WriteIdentityQueryCommand()
            Me.PublishVerbose("Trying to read identity;. ")
            isr.Core.ApplianceBase.DoEvents()
            ' wait for the delay time.
            ' Stopwatch.StartNew. Wait(Me.ReadAfterWriteRefractoryPeriod)
            Dim value As String = Me.Session.ReadLineTrimEnd
            value = value.ReplaceCommonEscapeSequences.Trim
            Me.PublishVerbose($"Setting identity to {value};. ")
            Me.VersionInfo.Parse(value)
            MyBase.VersionInfoBase = Me.VersionInfo
            Me.Identity = Me.VersionInfo.Identity
        End If
        Return Me.Identity
    End Function

    ''' <summary> Gets or sets the information describing the version. </summary>
    ''' <value> Information describing the version. </value>
    Public ReadOnly Property VersionInfo As VersionInfo

#End Region

End Class

''' <summary> Gets or sets the status byte flags of the measurement event register. </summary>
''' <remarks> David, 2020-10-12. </remarks>
<System.Flags()>
Public Enum MeasurementEvents

    ''' <summary> An enum constant representing the none option. </summary>
    <ComponentModel.Description("None")>
    None = 0

    ''' <summary> An enum constant representing the reading overflow option. </summary>
    <ComponentModel.Description("Reading Overflow")>
    ReadingOverflow = 1

    ''' <summary> An enum constant representing the low limit 1 failed option. </summary>
    <ComponentModel.Description("Low Limit 1 Failed")>
    LowLimit1Failed = 2

    ''' <summary> An enum constant representing the high limit 1 failed option. </summary>
    <ComponentModel.Description("High Limit 1 Failed")>
    HighLimit1Failed = 4

    ''' <summary> An enum constant representing the low limit 2 failed option. </summary>
    <ComponentModel.Description("Low Limit 2 Failed")>
    LowLimit2Failed = 8

    ''' <summary> An enum constant representing the high limit 2 failed option. </summary>
    <ComponentModel.Description("High Limit 2 Failed")>
    HighLimit2Failed = 16

    ''' <summary> An enum constant representing the reading available option. </summary>
    <ComponentModel.Description("Reading Available")>
    ReadingAvailable = 32

    ''' <summary> An enum constant representing the not used 1 option. </summary>
    <ComponentModel.Description("Not Used 1")>
    NotUsed1 = 64

    ''' <summary> An enum constant representing the buffer available option. </summary>
    <ComponentModel.Description("Buffer Available")>
    BufferAvailable = 128

    ''' <summary> An enum constant representing the buffer half full option. </summary>
    <ComponentModel.Description("Buffer Half Full")>
    BufferHalfFull = 256

    ''' <summary> An enum constant representing the buffer full option. </summary>
    <ComponentModel.Description("Buffer Full")>
    BufferFull = 512

    ''' <summary> An enum constant representing the buffer overflow option. </summary>
    <ComponentModel.Description("Buffer Overflow")>
    BufferOverflow = 1024

    ''' <summary> An enum constant representing the hardware limit event option. </summary>
    <ComponentModel.Description("HardwareLimit Event")>
    HardwareLimitEvent = 2048

    ''' <summary> An enum constant representing the buffer quarter full option. </summary>
    <ComponentModel.Description("Buffer Quarter Full")>
    BufferQuarterFull = 4096

    ''' <summary> An enum constant representing the buffer three quarters full option. </summary>
    <ComponentModel.Description("Buffer Three-Quarters Full")>
    BufferThreeQuartersFull = 8192

    ''' <summary> An enum constant representing the master limit option. </summary>
    <ComponentModel.Description("Master Limit")>
    MasterLimit = 16384

    ''' <summary> An enum constant representing the not used 2 option. </summary>
    <ComponentModel.Description("Not Used 2")>
    NotUsed2 = 32768

    ''' <summary> An enum constant representing all option. </summary>
    <ComponentModel.Description("All")>
    All = 32767
End Enum

