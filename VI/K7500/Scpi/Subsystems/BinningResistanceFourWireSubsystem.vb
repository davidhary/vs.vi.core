''' <summary>
''' Defines a Four Wire Resistance Binning Subsystem for a Keithley 7500 Meter.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2012-09-26, 1.0.4652. </para>
''' </remarks>
Public Class BinningResistanceFourWireSubsystem
    Inherits VI.BinningSubsystemBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="BinningResistanceFourWireSubsystem" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " LIMIT 1 "

#Region " LIMIT1 AUTO CLEAR "

    ''' <summary> Gets or sets the Limit1 Auto Clear command Format. </summary>
    ''' <value> The Limit1 AutoClear query command. </value>
    Protected Overrides Property Limit1AutoClearCommandFormat As String = ":CALC2:FRES:LIM1:CLE:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit1 Auto Clear query command. </summary>
    ''' <value> The Limit1 AutoClear query command. </value>
    Protected Overrides Property Limit1AutoClearQueryCommand As String = ":CALC2:FRES:LIM1:CLE:AUTO?"

#End Region

#Region " LIMIT1 ENABLED "

    ''' <summary> Gets or sets the Limit1 enabled command Format. </summary>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overrides Property Limit1EnabledCommandFormat As String = ":CALC2:FRES:LIM1:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit1 enabled query command. </summary>
    ''' <value> The Limit1 enabled query command. </value>
    Protected Overrides Property Limit1EnabledQueryCommand As String = ":CALC2:FRES:LIM1:STAT?"

#End Region

#Region " LIMIT1 LOWER LEVEL "

    ''' <summary> Gets or sets the Limit1 Lower Level command format. </summary>
    ''' <value> The Limit1LowerLevel command format. </value>
    Protected Overrides Property Limit1LowerLevelCommandFormat As String = ":CALC2:FRES:LIM1:LOW {0}"

    ''' <summary> Gets or sets the Limit1 Lower Level query command. </summary>
    ''' <value> The Limit1LowerLevel query command. </value>
    Protected Overrides Property Limit1LowerLevelQueryCommand As String = ":CALC2:FRES:LIM1:LOW?"

#End Region

#Region " LIMIT1 UPPER LEVEL "

    ''' <summary> Gets or sets the Limit1 Upper Level command format. </summary>
    ''' <value> The Limit1UpperLevel command format. </value>
    Protected Overrides Property Limit1UpperLevelCommandFormat As String = ":CALC2:FRES:LIM1:UPP {0}"

    ''' <summary> Gets or sets the Limit1 Upper Level query command. </summary>
    ''' <value> The Limit1UpperLevel query command. </value>
    Protected Overrides Property Limit1UpperLevelQueryCommand As String = ":CALC2:FRES:LIM1:UPP?"

#End Region

#End Region

#Region " LIMIT 2 "

#Region " LIMIT2 AUTO CLEAR "

    ''' <summary> Gets or sets the Limit2 Auto Clear command Format. </summary>
    ''' <value> The Limit2 AutoClear query command. </value>
    Protected Overrides Property Limit2AutoClearCommandFormat As String = ":CALC2:FRES:LIM2:CLE:AUTO {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit2 Auto Clear query command. </summary>
    ''' <value> The Limit2 AutoClear query command. </value>
    Protected Overrides Property Limit2AutoClearQueryCommand As String = ":CALC2:FRES:LIM2:CLE:AUTO?"

#End Region

#Region " LIMIT2 ENABLED "

    ''' <summary> Gets or sets the Limit2 enabled command Format. </summary>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overrides Property Limit2EnabledCommandFormat As String = ":CALC2:FRES:LIM2:STAT {0:'ON';'ON';'OFF'}"

    ''' <summary> Gets or sets the Limit2 enabled query command. </summary>
    ''' <value> The Limit2 enabled query command. </value>
    Protected Overrides Property Limit2EnabledQueryCommand As String = ":CALC2:FRES:LIM2:STAT?"

#End Region

#Region " LIMIT2 LOWER LEVEL "

    ''' <summary> Gets or sets the Limit2 Lower Level command format. </summary>
    ''' <value> The Limit2LowerLevel command format. </value>
    Protected Overrides Property Limit2LowerLevelCommandFormat As String = ":CALC2:FRES:LIM2:LOW {0}"

    ''' <summary> Gets or sets the Limit2 Lower Level query command. </summary>
    ''' <value> The Limit2LowerLevel query command. </value>
    Protected Overrides Property Limit2LowerLevelQueryCommand As String = ":CALC2:FRES:LIM2:LOW?"

#End Region

#Region " LIMIT2 UPPER LEVEL "

    ''' <summary> Gets or sets the Limit2 Upper Level command format. </summary>
    ''' <value> The Limit2UpperLevel command format. </value>
    Protected Overrides Property Limit2UpperLevelCommandFormat As String = ":CALC2:FRES:LIM2:UPP {0}"

    ''' <summary> Gets or sets the Limit2 Upper Level query command. </summary>
    ''' <value> The Limit2UpperLevel query command. </value>
    Protected Overrides Property Limit2UpperLevelQueryCommand As String = ":CALC2:FRES:LIM2:UPP?"

#End Region
#End Region

End Class
