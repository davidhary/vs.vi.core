## ISR VI K7500<sub>&trade;</sub>: Keithley 7500 Meter VI Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*4.1.6440 2017-08-19*  
Uses modified trace message and logger.

*4.1.6438 2017-08-17*  
Uses the device trace message talker to apply a talker
to the panel.

*4.0.6438 2017-08-17*  
Changes the buffer command to clear the buffer first
and then set the desired size. Adds buffer size numeric control to the
control panel.

*4.0.6431 2017-08-10*  
Initializes value and power line cycle ranges when
resetting the subsystem so that the values are set when changing the
function mode.

*4.0.6264 2017-02-25*  
Adds units and statuses to buffer readings.

*4.0.6263 2017-02-24*  
Adds buffer streaming to the trace subsystem.

*4.0.6256 2017-02-16*  
Adds meter complete first grading and binning mode.

*4.0.6243 2017-02-03*  
Adds trigger plan monitoring and buffer reading.

*4.0.6215 2017-01-06*  
Adds open contact detection. Adds the open contact
blocks to the trigger model.

*4.0.6211 2017-01-02*  
Adds custom grading and binning trigger model

*4.0.6188 2016-12-10*  
Supports external triggering and buffering.

*4.0.6067 2016-08-11*  
Corrects bug in parsing the buffer that missed the
last reading. Adds parsing of timestamp.

*4.0.6020 2016-06-25*  
Created from Multimeter Library.

\(C\) 2013 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
