﻿''' <summary> A device error. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2016-01-12 </para>
''' </remarks>
Public Class DeviceError
    Inherits VI.DeviceError

#Region " CONSTRUCTION "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="DeviceError" /> class specifying no error.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New("0,No Errors")
    End Sub

#End Region

#Region " PARSE "

    ''' <summary> Parses the error message. </summary>
    ''' <remarks>
    ''' TSP2 error: -285,TSP Syntax error at line 1: unexpected symbol near `*',level=1 TSP error: -
    ''' 285,TSP Syntax Error at line 1: unexpected symbol near `*',level=20 SCPI Error: -113,
    ''' "Undefined header;1;2018/05/26 14:00:14.871".
    ''' </remarks>
    ''' <param name="compoundError"> The compound error. </param>
    Public Overrides Sub Parse(ByVal compoundError As String)
        MyBase.Parse(compoundError)
        If Not String.IsNullOrWhiteSpace(compoundError) Then
            ' parse EG Prober Errors.
            If compoundError.StartsWith("E", StringComparison.OrdinalIgnoreCase) Then
                If Integer.TryParse(compoundError.Substring(1), Me.ErrorNumber) Then
                End If
                Me.ErrorMessage = compoundError
            End If
        End If
    End Sub

#End Region

End Class
