
Partial Public Class ProberSubsystem
    Inherits VI.ProberSubsystemBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="StatusSubsystemBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> A reference to a
    '''                                <see cref="T:isr.VI.StatusSubsystemBase">status
    '''                                subsystem</see>. </param>
    Public Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
        Me.Worker = New System.ComponentModel.BackgroundWorker() With {
            .WorkerSupportsCancellation = True
        }
        Me._CommandTrialCount = 3
        Me._CommandPollInterval = TimeSpan.FromMilliseconds(30)
        Me._CommandTimeoutInterval = TimeSpan.FromMilliseconds(200)


        Me.ProberTimer = New Timers.Timer(1.5 * Me._CommandTrialCount * Me._CommandTimeoutInterval.TotalMilliseconds)
        Me.ProberTimer.Stop()

        Me._SupportedReplyPatterns = New List(Of String)()
        Me._SupportedCommandPrefixes = New List(Of String)()
        Me._SupportedCommands = New List(Of String)()
        Me.NewThis()
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived
    ''' class provided proper implementation.
    ''' </summary>
    ''' <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
    Protected Property IsDisposed() As Boolean

    ''' <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
    ''' <remarks>
    ''' Executes in two distinct scenarios as determined by its disposing parameter:<para>
    ''' If True, the method has been called directly or indirectly by a user's code--managed and
    ''' unmanaged resources can be disposed.</para><para>
    ''' If False, the method has been called by the runtime from inside the finalizer and you should
    ''' not reference other objects--only unmanaged resources can be disposed.</para>
    ''' </remarks>
    ''' <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
    '''                                                                            resources;
    '''                                                                            False if this
    '''                                                                            method releases
    '''                                                                            only unmanaged
    '''                                                                            resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.ProberTimer IsNot Nothing Then
                    Me.ProberTimer.Stop()
                    isr.Core.ApplianceBase.DoEvents()
                    Me.ProberTimer.Dispose()
                    Me.ProberTimer = Nothing
                End If
                ' Free managed resources when explicitly called
                If Me.Worker IsNot Nothing Then
                    Me.Worker.CancelAsync()
                    isr.Core.ApplianceBase.DoEvents()
                    If Not (Me.Worker.IsBusy OrElse Me.Worker.CancellationPending) Then
                        Me.Worker.Dispose()
                    End If
                End If
            End If
        Finally
            ' set the sentinel indicating that the class was disposed.
            Me.IsDisposed = True
        End Try
    End Sub

#End Region

#Region " IDENTITY COMPLETE "

    ''' <summary> A pattern specifying the Identity. </summary>
    ''' <value> The identity reply pattern. </value>
    Protected Property IdentityReplyPattern As String

#End Region

#Region " ERROR PATTERN "

    ''' <summary> A pattern specifying the Error Message coming back from the Prober. </summary>
    ''' <value> The error reply pattern. </value>
    Protected Property ErrorReplyPattern As String

#End Region

#Region " MESSAGE FAILED "

    ''' <summary> A pattern specifying the message failed. </summary>
    Private _MessageFailedPattern As String

    ''' <summary> Gets or sets the message failed pattern. </summary>
    ''' <value> The message failed pattern. </value>
    Public Property MessageFailedPattern As String
        Get
            Return Me._MessageFailedPattern
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.MessageFailedPattern, StringComparison.OrdinalIgnoreCase) Then
                Me._MessageFailedPattern = value
                Me.SyncNotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

#End Region

#Region " MESSAGE COMPLETED "

    ''' <summary> A pattern specifying the message completed. </summary>
    Private _MessageCompletedPattern As String

    ''' <summary> Gets or sets the message Completed pattern. </summary>
    ''' <value> The message Completed pattern. </value>
    Public Property MessageCompletedPattern As String
        Get
            Return Me._MessageCompletedPattern
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.MessageCompletedPattern, StringComparison.OrdinalIgnoreCase) Then
                Me._MessageCompletedPattern = value
                Me.SyncNotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

#End Region

#Region " PATTERN COMPLETE "

    ''' <summary> A pattern specifying the pattern complete. </summary>
    ''' <value> The pattern complete reply pattern. </value>
    Public Property PatternCompleteReplyPattern As String

#End Region

#Region " SET MODE "

    ''' <summary> A pattern specifying a mode setup command prefix. </summary>
    ''' <value> The set mode command prefix. </value>
    Protected Property SetModeCommandPrefix As String

    ''' <summary> Gets or sets the set mode command. </summary>
    ''' <value> The set mode command. </value>
    Public Property SetModeCommand As String

    ''' <summary> Gets or sets the set mode query command. </summary>
    ''' <value> The set mode query command. </value>
    Public Property SetModeQueryCommand As String

    ''' <summary> Gets or sets the identity query command. </summary>
    ''' <value> The identity query command. </value>
    Public Property IdentityQueryCommand As String

    ''' <summary> Gets or sets the error query command. </summary>
    ''' <value> The error query command. </value>
    Public Property ErrorQueryCommand As String

#End Region

#Region " TEST COMPLETE "

    ''' <summary> Gets or sets the test complete command. </summary>
    ''' <value> The test complete command. </value>
    Public Property TestCompleteCommand As String

#End Region

#Region " TEST START "

    ''' <summary> Gets or sets the retest start pattern. </summary>
    ''' <value> The test start pattern. </value>
    Public Property RetestStartPattern As String

    ''' <summary> Gets or sets the test again start pattern. </summary>
    ''' <value> The test start pattern. </value>
    Public Property TestAgainStartPattern As String

    ''' <summary> Gets or sets the test start pattern. </summary>
    ''' <value> The test start pattern. </value>
    Public Property TestStartPattern As String

    ''' <summary> Gets or sets the first test start pattern. </summary>
    ''' <value> The first test start pattern. </value>
    Public Property FirstTestStartPattern As String

#End Region

#Region " WAFER START "

    ''' <summary> Gets or sets the wafer start pattern. </summary>
    ''' <value> The wafer start pattern. </value>
    Public Property WaferStartPattern As String

#End Region

#Region " WRITE "

    ''' <summary> The supported command prefixes. </summary>
    Private _SupportedCommandPrefixes As List(Of String)

    ''' <summary> Gets or sets the supported command prefixes. </summary>
    ''' <value> The supported command prefixes. </value>
    Public Property SupportedCommandPrefixes() As IList(Of String)
        Get
            Return Me._SupportedCommandPrefixes
        End Get
        Set(value As IList(Of String))
            Me._SupportedCommandPrefixes = New List(Of String)(value)
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The supported commands. </summary>
    Private _SupportedCommands As List(Of String)

    ''' <summary> Gets or sets the supported commands. </summary>
    ''' <value> The supported commands. </value>
    Public Property SupportedCommands() As IList(Of String)
        Get
            Return Me._SupportedCommands
        End Get
        Set(value As IList(Of String))
            Me._SupportedCommands = New List(Of String)(value)
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The last message sent. </summary>
    Private _LastMessageSent As String

    ''' <summary> Gets or sets (protected) the last Message Sent. </summary>
    ''' <value> The last MessageSent. </value>
    Public Property LastMessageSent As String
        Get
            Return Me._LastMessageSent
        End Get
        Protected Set(value As String)
            Me._LastMessageSent = value
            Me.SyncNotifyPropertyChanged()
            isr.Core.ApplianceBase.DoEvents()
        End Set
    End Property

    ''' <summary> Sends a message to the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <param name="args">   A variable-length parameters list containing arguments. </param>
    Public Sub Send(ByVal format As String, ByVal ParamArray args() As Object)
        Me.Send(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary>
    ''' Synchronously writes and ASCII-encoded string data. Terminates the data with the
    ''' <see cref="VI.Pith.SessionBase.TerminationCharacters">termination character</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="dataToWrite"> The data to write. </param>
    Public Sub Send(ByVal dataToWrite As String)
        Me.ProberTimer.Stop()
        Me.ClearSendSentinels()
        Me.ClearFetchSentinels()
        If Not String.IsNullOrWhiteSpace(dataToWrite) Then
            Me.LastMessageSent = dataToWrite
            Me.Session.WriteLine(dataToWrite)
            If dataToWrite.StartsWith(Me.TestCompleteCommand, StringComparison.OrdinalIgnoreCase) Then
                Me.TestCompleteSent = True
            ElseIf dataToWrite.StartsWith(Me.SetModeCommandPrefix, StringComparison.OrdinalIgnoreCase) Then
                Me.SetModeSent = True
            ElseIf Me._SupportedCommandPrefixes.Contains(dataToWrite.Substring(0, 2), StringComparer.OrdinalIgnoreCase) Then
            ElseIf dataToWrite.StartsWith("*SRE", StringComparison.OrdinalIgnoreCase) Then
                Me.LastReading = Me.MessageCompletedPattern
                Me.ParseReading(Me.LastReading)
            ElseIf dataToWrite.StartsWith("*IDN?", StringComparison.OrdinalIgnoreCase) Then
            Else
                Me.UnhandledMessageSent = True
            End If
        End If
    End Sub

#End Region

#Region " BACKGROUND WORKER "

    ''' <summary> Worker payload. </summary>
    ''' <remarks> David, 2013-10-29. </remarks>
    Private Class WorkerPayLoad

        ''' <summary> Gets or sets the message. </summary>
        ''' <value> The message. </value>
        Public Property Message As String

        ''' <summary> Gets or sets the trial number. </summary>
        ''' <value> The trial number. </value>
        Public Property TrialNumber As Integer

        ''' <summary> Gets or sets the resend on retry. </summary>
        ''' <value> The resend on retry. </value>
        Public Property ResendOnRetry As Boolean

        ''' <summary> Gets or sets the number of trials. </summary>
        ''' <value> The number of trials. </value>
        Public Property TrialCount As Integer

        ''' <summary> Gets or sets the poll interval. </summary>
        ''' <value> The poll interval. </value>
        Public Property PollInterval As TimeSpan

        ''' <summary> Gets or sets the timeout interval. </summary>
        ''' <value> The timeout interval. </value>
        Public Property TimeoutInterval As TimeSpan
    End Class

        Private WithEvents Worker As System.ComponentModel.BackgroundWorker

    ''' <summary> Worker do work. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Do work event information. </param>
    Private Sub Worker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles Worker.DoWork
        If Not (Me.IsDisposed OrElse e Is Nothing OrElse e.Cancel) Then
            Dim payload As WorkerPayLoad = TryCast(e.Argument, WorkerPayLoad)
            payload.TrialNumber = 0
            Dim sw As Stopwatch = Stopwatch.StartNew
            Do
                sw.Restart()
                payload.TrialNumber += 1
                If payload.ResendOnRetry OrElse payload.TrialNumber = 1 Then Me.Send(payload.Message)
                isr.Core.ApplianceBase.DoEventsWaitUntil(payload.PollInterval, payload.TimeoutInterval, Function() Me.MessageCompleted OrElse Me.MessageFailed)
            Loop Until Me.MessageCompleted OrElse Me.MessageFailed OrElse (payload.TrialNumber >= payload.TrialCount)
        End If
    End Sub

    ''' <summary> Worker run worker completed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Run worker completed event information. </param>
    Private Sub Worker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles Worker.RunWorkerCompleted
        If Not (Me.IsDisposed OrElse e Is Nothing OrElse e.Cancelled OrElse e.Error IsNot Nothing) Then
        End If
    End Sub

    ''' <summary> Gets or sets the number of command trials. </summary>
    ''' <value> The number of command trials. </value>
    Public Property CommandTrialCount As Integer

    ''' <summary> Gets or sets the command poll interval. </summary>
    ''' <value> The command poll interval. </value>
    Public Property CommandPollInterval As TimeSpan

    ''' <summary> Gets or sets the command timeout interval. </summary>
    ''' <value> The command timeout interval. </value>
    Public Property CommandTimeoutInterval As TimeSpan

    ''' <summary>
    ''' Try sending a message using a background worker waiting for an asynchronous reply.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>True</c> if message was completed before the trial count expired. </returns>
    Public Function TrySendAsync(value As String) As Boolean
        Return Me.TrySendAsync(value, Me.CommandTrialCount, Me.CommandPollInterval, Me.CommandTimeoutInterval)
    End Function

    ''' <summary> Try sending a message. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">        The value. </param>
    ''' <param name="trialCount">   Number of trials. </param>
    ''' <param name="pollInterval"> The poll interval. </param>
    ''' <param name="timeout">      The timeout. </param>
    ''' <returns> <c>True</c> if message was completed before the trial count expired. </returns>
    Public Function TrySendAsync(ByVal value As String, ByVal trialCount As Integer,
                                 ByVal pollInterval As TimeSpan, ByVal timeout As TimeSpan) As Boolean
        ' wait for previous operation to complete.
        Dim sw As Stopwatch = Stopwatch.StartNew
        Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy OrElse sw.Elapsed > timeout
            isr.Core.ApplianceBase.DoEvents()
        Loop
        If Me.Worker.IsBusy Then
            Me.Worker.CancelAsync()
        End If
        sw.Restart()
        Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy OrElse sw.Elapsed > timeout
            isr.Core.ApplianceBase.DoEvents()
        Loop
        If Me.Worker.IsBusy Then

            Return False
        End If

        Dim payload As New WorkerPayLoad With {.Message = value,
                                               .TrialCount = trialCount,
                                               .PollInterval = pollInterval,
                                               .TimeoutInterval = timeout,
                                               .ResendOnRetry = False}

        If Not (Me.IsDisposed OrElse Me.Worker.IsBusy) Then
            sw.Restart()
            Me.Worker.RunWorkerAsync(payload)
            ' wait for worker to get busy.
            Do While Not (Me.IsDisposed OrElse Me.Worker.IsBusy)
                isr.Core.ApplianceBase.DoEvents()
            Loop
            ' wait till worker is done
            Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy OrElse sw.Elapsed > payload.TimeoutInterval
                isr.Core.ApplianceBase.DoEvents()
            Loop
            Do Until Me.IsDisposed OrElse Not Me.Worker.IsBusy
                isr.Core.ApplianceBase.DoEvents()
            Loop
        End If
        Return Me.MessageCompleted
    End Function

    ''' <summary> Clears the send sentinels. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ClearSendSentinels()
        Me.TestCompleteSent = False
        Me.UnhandledMessageSent = False
    End Sub

#End Region

#Region " EMULATE "

    ''' <summary> Emulates sending a Prober message. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="command">   The command. </param>
    ''' <param name="timeDelay"> The time delay. </param>
    Public Sub Emulate(ByVal command As String, ByVal timeDelay As TimeSpan)
        Me.ProberCommand = String.Empty
        If Me.ProberTimer IsNot Nothing Then
            Me.ProberTimer.Stop()
            Me.ProberCommand = command
            Me.ProberTimer.Interval = timeDelay.TotalMilliseconds
            Me.ProberTimer.Start()
        End If
    End Sub

    ''' <summary> Gets or sets the Prober command. </summary>
    ''' <value> The Prober command. </value>
    Private Property ProberCommand As String

        Private WithEvents ProberTimer As System.Timers.Timer

    ''' <summary> Event handler. Called by the Prober Timer for elapsed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Elapsed event information. </param>
    Private Sub ProberTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles ProberTimer.Elapsed
        If Me.ProberTimer IsNot Nothing Then
            Me.ProberTimer.Stop()
        End If
        If Not String.IsNullOrWhiteSpace(Me.ProberCommand) Then
            Dim value As String = Me.ProberCommand
            Me.ProberCommand = String.Empty
            Me.LastReading = value
            Me.ParseReading(value)
        End If
    End Sub

#End Region

#Region " FETCH "

    ''' <summary> The supported reply patterns. </summary>
    Private _SupportedReplyPatterns As List(Of String)

    ''' <summary> Gets or sets the supported reply patterns. </summary>
    ''' <value> The supported reply patterns. </value>
    Public Property SupportedReplyPatterns() As IList(Of String)
        Get
            Return Me._SupportedReplyPatterns
        End Get
        Set(value As IList(Of String))
            Me._SupportedReplyPatterns = New List(Of String)(value)
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Clears the fetch sentinels. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ClearFetchSentinels()
        Me.ErrorRead = False
        Me.IdentityRead = False
        Me.MessageCompleted = False
        Me.MessageFailed = False
        Me.UnhandledMessageReceived = False
    End Sub

    ''' <summary> Parses the message. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="reading"> The reading. </param>
    ''' <returns> True if valid reading, false if not. </returns>
    Public Function IsValidReading(ByVal reading As String) As Boolean
        Dim result As Boolean = False
        If Not String.IsNullOrWhiteSpace(reading) Then
            result = reading.StartsWith(Me.ErrorReplyPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.IdentityReplyPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.PatternCompleteReplyPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.FirstTestStartPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.TestStartPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.RetestStartPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.TestAgainStartPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.WaferStartPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.MessageCompletedPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith(Me.MessageFailedPattern, StringComparison.OrdinalIgnoreCase) OrElse
               reading.StartsWith("Keithley", StringComparison.OrdinalIgnoreCase)
        End If
        Return result
    End Function

    ''' <summary> Parses the message. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="reading"> The reading. </param>
    Public Overrides Sub ParseReading(ByVal reading As String)
        Me.ClearFetchSentinels()
        If String.IsNullOrWhiteSpace(reading) Then
        ElseIf reading.StartsWith(Me.ErrorReplyPattern, StringComparison.OrdinalIgnoreCase) Then
            ' message is completed if the reply is in response to an error query.
            Me.MessageCompleted = True
            Me.ErrorRead = False
            Me.ErrorRead = True
        ElseIf reading.StartsWith(Me.IdentityReplyPattern, StringComparison.OrdinalIgnoreCase) Then
            Me.IdentityRead = False
            Me.IdentityRead = True
            Me.MessageCompleted = True
        ElseIf reading.StartsWith(Me.PatternCompleteReplyPattern, StringComparison.OrdinalIgnoreCase) Then
            'tag message completed as it is assumed that this is the reply to the test complete command.
            Me.MessageCompleted = True
            Me.PatternCompleteReceived = False
            Me.PatternCompleteReceived = True
        ElseIf reading.StartsWith(Me.FirstTestStartPattern, StringComparison.OrdinalIgnoreCase) Then
            'tag message completed as it is assumed that this is the reply to the test complete command.
            Me.MessageCompleted = True
            Me.RetestRequested = False
            Me.TestAgainRequested = False
            Me.IsFirstTestStart = True
            Me.TestStartReceived = True
        ElseIf reading.StartsWith(Me.TestStartPattern, StringComparison.OrdinalIgnoreCase) Then
            'tag message completed as it is assumed that this is the reply to the test complete command.
            Me.MessageCompleted = True
            Me.RetestRequested = False
            Me.TestAgainRequested = False
            Me.IsFirstTestStart = False
            Me.TestStartReceived = True
        ElseIf reading.StartsWith(Me.RetestStartPattern, StringComparison.OrdinalIgnoreCase) Then
            'tag message completed as it is assumed that this is the reply to the test complete command.
            Me.MessageCompleted = True
            Me.RetestRequested = True
            Me.TestAgainRequested = False
            Me.IsFirstTestStart = False
            Me.TestStartReceived = True
        ElseIf reading.StartsWith(Me.TestAgainStartPattern, StringComparison.OrdinalIgnoreCase) Then
            'tag message completed as it is assumed that this is the reply to the test complete command.
            Me.MessageCompleted = True
            Me.RetestRequested = False
            Me.TestAgainRequested = True
            Me.IsFirstTestStart = False
            Me.TestStartReceived = True
        ElseIf reading.StartsWith(Me.WaferStartPattern, StringComparison.OrdinalIgnoreCase) Then
            Me.WaferStartReceived = False
            Me.WaferStartReceived = True
        ElseIf reading.StartsWith(Me.MessageCompletedPattern, StringComparison.OrdinalIgnoreCase) Then
            Me.MessageCompleted = False
            Me.MessageCompleted = True
        ElseIf reading.StartsWith(Me.MessageFailedPattern, StringComparison.OrdinalIgnoreCase) Then
            Me.MessageFailed = True
        ElseIf reading.StartsWith("Keithley", StringComparison.OrdinalIgnoreCase) Then
            Me.MessageCompleted = True
        Else
            Me.UnhandledMessageReceived = True
        End If
    End Sub

#End Region

End Class

