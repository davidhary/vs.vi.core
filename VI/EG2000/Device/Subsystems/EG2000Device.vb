Imports isr.VI.ExceptionExtensions

''' <summary> Implements an Electroglass 2000 Prober device. </summary>
''' <remarks>
''' An instrument is defined, for the purpose of this library, as a device with a front panel.
''' <para>
''' David, 2018-12-31, Updated. </para><para>
''' David, 2013-10-01, 3.0.5022. </para><para>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class EG2000Device
    Inherits VisaSessionBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="EG2000Device" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        Me.New(StatusSubsystem.Create())
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The Status Subsystem. </param>
    Protected Sub New(ByVal statusSubsystem As StatusSubsystem)
        MyBase.New(statusSubsystem)
        If statusSubsystem IsNot Nothing Then
            AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.HandleSettingsPropertyChanged
        End If
        Me.StatusSubsystem = statusSubsystem
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As EG2000Device
        Dim device As EG2000Device = Nothing
        Try
            device = New EG2000Device
        Catch
            If device IsNot Nothing Then device.Dispose()
            Throw
        End Try
        Return device
    End Function

    ''' <summary> Validated the given device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="device"> The device. </param>
    ''' <returns> A Device. </returns>
    Public Shared Function Validated(ByVal device As EG2000Device) As EG2000Device
        If device Is Nothing Then Throw New ArgumentNullException(NameOf(device))
        Return device
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                If Me.IsDeviceOpen Then
                    Me.OnClosing(New ComponentModel.CancelEventArgs)
                    Me._StatusSubsystem = Nothing
                End If
            End If
            ' release unmanaged-only resources.
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, $"Exception disposing {GetType(EG2000Device)}", ex.ToFullBlownString)
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " SESSION INITIALIZATION PROPERTIES "

    ''' <summary>
    ''' Gets or sets the bits that would be set for detecting if an error is available.
    ''' </summary>
    ''' <value> The error available bits. </value>
    Protected Overrides Property ErrorAvailableBits() As VI.Pith.ServiceRequests = VI.Pith.ServiceRequests.ErrorAvailable

    ''' <summary> Gets or sets the keep alive interval. </summary>
    ''' <remarks> Required only with Non-Standard NI VISA. </remarks>
    ''' <value> The keep alive interval. </value>
    Protected Overrides Property KeepAliveInterval As TimeSpan = TimeSpan.Zero

    ''' <summary> Gets or sets the is alive command. </summary>
    ''' <remarks> Required only with Non-Standard NI VISA. </remarks>
    ''' <value> The is alive command. </value>
    Protected Overrides Property IsAliveCommand As String = String.Empty

    ''' <summary> Gets or sets the is alive query command. </summary>
    ''' <remarks> Required only with Non-Standard NI VISA. </remarks>
    ''' <value> The is alive query command. </value>
    Protected Overrides Property IsAliveQueryCommand As String = String.Empty

#End Region

#Region " SESSION "

    ''' <summary>
    ''' Allows the derived device to take actions before closing. Removes subsystems and event
    ''' handlers.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnClosing(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnClosing(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindProberSubsystem(Nothing)
            Me.BindSystemSubsystem(Nothing)
        End If
    End Sub

    ''' <summary> Allows the derived device to take actions before opening. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overrides Sub OnOpening(ByVal e As ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        MyBase.OnOpening(e)
        If Not e.Cancel AndAlso Me.SubsystemSupportMode = SubsystemSupportMode.Full Then
            Me.BindSystemSubsystem(New SystemSubsystem(Me.StatusSubsystem))
            Me.BindProberSubsystem(New ProberSubsystem(Me.StatusSubsystem))
        End If
    End Sub

#End Region

#Region " SUBSYSTEMS "

#Region " PROBER "

    ''' <summary> Gets or sets the Prober Subsystem. </summary>
    ''' <value> The Prober Subsystem. </value>
    Public ReadOnly Property ProberSubsystem As ProberSubsystem

    ''' <summary> Binds the Prober subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindProberSubsystem(ByVal subsystem As ProberSubsystem)
        If Me.ProberSubsystem IsNot Nothing Then
            RemoveHandler Me.ProberSubsystem.PropertyChanged, AddressOf Me.ProberSubsystemPropertyChanged
            Me.Subsystems.Remove(Me.ProberSubsystem)
            Me.ProberSubsystem.Dispose()
            Me._ProberSubsystem = Nothing
        End If
        Me._ProberSubsystem = subsystem
        If Me.ProberSubsystem IsNot Nothing Then
            AddHandler Me.ProberSubsystem.PropertyChanged, AddressOf Me.ProberSubsystemPropertyChanged
            Me.Subsystems.Add(Me.ProberSubsystem)
            Me.HandlePropertyChanged(Me.ProberSubsystem, NameOf(EG2000.ProberSubsystem.IdentityRead))
            Me.HandlePropertyChanged(Me.ProberSubsystem, NameOf(EG2000.ProberSubsystem.ErrorRead))
        End If
    End Sub

    ''' <summary> Handle the Prober subsystem property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem">    The subsystem. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal subsystem As ProberSubsystem, ByVal propertyName As String)
        If subsystem Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Dim lastReading As String = subsystem.LastReading
        Select Case propertyName
            Case NameOf(EG2000.ProberSubsystem.IdentityRead)
                If subsystem.IdentityRead AndAlso Not String.IsNullOrWhiteSpace(lastReading) Then
                    Me.StatusSubsystem.Identity = lastReading
                End If
            Case NameOf(EG2000.ProberSubsystem.ErrorRead)
                If subsystem.ErrorRead Then
                    Me.StatusSubsystem.EnqueueLastError(subsystem?.LastReading)
                Else
                End If
        End Select
    End Sub

    ''' <summary> Prober subsystem property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ProberSubsystemPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(ProberSubsystem)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, ProberSubsystem), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " STATUS "

    ''' <summary> Gets or sets the Status Subsystem. </summary>
    ''' <value> The Status Subsystem. </value>
    Public ReadOnly Property StatusSubsystem As StatusSubsystem

#End Region

#Region " SYSTEM "

    ''' <summary> Gets or sets the System Subsystem. </summary>
    ''' <value> The System Subsystem. </value>
    Public ReadOnly Property SystemSubsystem As SystemSubsystem

    ''' <summary> Bind the System subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindSystemSubsystem(ByVal subsystem As SystemSubsystem)
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Remove(Me.SystemSubsystem)
            Me._SystemSubsystem = Nothing
        End If
        Me._SystemSubsystem = subsystem
        If Me.SystemSubsystem IsNot Nothing Then
            Me.Subsystems.Add(Me.SystemSubsystem)
        End If
    End Sub

#End Region

#End Region

#Region " SERVICE REQUEST "

    ''' <summary> Reads the event registers after receiving a service request. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ProcessServiceRequest()
        Me.Session.ReadStatusRegister() ' this could have lead to a query unterminated error: Me.ReadEventRegisters()
        If Me.StatusSubsystem.MessageAvailable Then
            Dim firstReading As String = String.Empty
            Dim reading As String = Me.Session.ReadFiniteLine.TrimEnd(Me.Session.TerminationCharacters())
            If Not Me.ProberSubsystem.IsValidReading(reading) Then
                firstReading = reading
                Me.PublishInfo($"Invalid reading '{reading}'; trying again;. ")
                Try
                    Me.Session.StoreCommunicationTimeout(My.MySettings.Default.PostSrqRetryTimeout)
                    reading = Me.Session.ReadFiniteLine.TrimEnd(Me.Session.TerminationCharacters())
                Catch
                    Throw
                Finally
                    Me.Session.RestoreCommunicationTimeout()
                End Try
            End If
            Me.ProberSubsystem.LastReading = reading
            If Me.ProberSubsystem.IsValidReading(reading) Then
                Me.PublishVerbose($"Parsing;. {Me.ProberSubsystem.LastReading};. ")
                Me.ProberSubsystem.ParseReading(Me.ProberSubsystem.LastReading)
            Else
                Me.PublishWarning($"Failed reading valid reply '{reading}' from the Prober after reading '{firstReading}';. ")
            End If
        End If
    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("EG2000 Settings Editor", EG2000.My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ApplySettings()
        Dim settings As EG2000.My.MySettings = EG2000.My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.TraceShowLevel))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.InitializeTimeout))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.ResetRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.DeviceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.InterfaceClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.InitRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.ClearRefractoryPeriod))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.SessionMessageNotificationLevel))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.Default.ReadDelay))
        Me.HandlePropertyChanged(settings, NameOf(EG2000.My.MySettings.Default.StatusDelay))
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As EG2000.My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(EG2000.My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} set to {sender.TraceLogLevel}")
            Case NameOf(EG2000.My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} set to {sender.TraceShowLevel}")
            Case NameOf(EG2000.My.MySettings.ClearRefractoryPeriod)
                Me.Session.ClearRefractoryPeriod = sender.ClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.ClearRefractoryPeriod}")
            Case NameOf(EG2000.My.MySettings.DeviceClearRefractoryPeriod)
                Me.Session.DeviceClearRefractoryPeriod = sender.DeviceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.DeviceClearRefractoryPeriod}")
            Case NameOf(EG2000.My.MySettings.InterfaceClearRefractoryPeriod)
                Me.Session.InterfaceClearRefractoryPeriod = sender.InterfaceClearRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.InterfaceClearRefractoryPeriod}")
            Case NameOf(EG2000.My.MySettings.InitRefractoryPeriod)
                Me.StatusSubsystemBase.InitRefractoryPeriod = sender.InitRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.InitRefractoryPeriod}")
            Case NameOf(EG2000.My.MySettings.InitializeTimeout)
                Me.StatusSubsystemBase.InitializeTimeout = sender.InitializeTimeout
                Me.PublishInfo($"{propertyName} set to {sender.InitializeTimeout}")
            Case NameOf(EG2000.My.MySettings.ResetRefractoryPeriod)
                Me.Session.ResetRefractoryPeriod = sender.ResetRefractoryPeriod
                Me.PublishInfo($"{propertyName} set to {sender.ResetRefractoryPeriod}")
            Case NameOf(EG2000.My.MySettings.SessionMessageNotificationLevel)
                Me.Session.MessageNotificationLevel = CType(sender.SessionMessageNotificationLevel, isr.VI.Pith.NotifySyncLevel)
                Me.PublishInfo($"{propertyName} set to {Me.Session.MessageNotificationLevel}")
            Case NameOf(My.MySettings.Default.ReadDelay)
                Me.Session.ReadDelay = sender.ReadDelay
                Me.PublishInfo($"{propertyName} changed to {sender.ReadDelay}")
            Case NameOf(My.MySettings.Default.StatusDelay)
                Me.Session.StatusReadDelay = sender.StatusDelay
                Me.PublishInfo($"{propertyName} changed to {sender.StatusDelay}")
        End Select
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub HandleSettingsPropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(EG2000.My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, EG2000.My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

