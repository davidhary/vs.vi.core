Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> Defines a Prober Subsystem for a EG2000 Prober. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-10-01, 3.0.5022. </para>
''' </remarks>
Public Class ProberSubsystem

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Creates a new this. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub NewThis()
        Me.ErrorReplyPattern = "E"
        Me.MessageFailedPattern = "MF"
        Me.MessageCompletedPattern = "MC"
        Me.FirstTestStartPattern = "TF"
        Me.PatternCompleteReplyPattern = "PC"
        Me.RetestStartPattern = "TR"
        Me.SetModeCommandPrefix = "SM"
        Me.TestStartPattern = "TS"
        Me.TestAgainStartPattern = "TA"
        Me.WaferStartPattern = "WB"
        Me.TestCompleteCommand = "TC"
        Me.TestCompleteCommand = If(My.MySettings.Default.UsingScpi, String.Empty, "TC")
        Me.IdentityReplyPattern = If(My.MySettings.Default.UsingScpi, "KEITHLEY ", "2001X.")
        Me.ErrorQueryCommand = If(My.MySettings.Default.UsingScpi, ":SYS:ERR?", "?E")
        Me.IdentityQueryCommand = If(My.MySettings.Default.UsingScpi, "*IDN?", "ID")
        Me.SetModeCommand = If(My.MySettings.Default.UsingScpi, String.Empty, "SM15M101110110001000001")
        Me.SetModeQueryCommand = If(My.MySettings.Default.UsingScpi, String.Empty, "?SM15")
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Customizes the reset state. Sets the response mode. </remarks>
    Public Overrides Sub InitKnownState()
        MyBase.InitKnownState()
        Me.Session.ReadDelay = My.MySettings.Default.ReadDelay
        Me.Session.StatusReadDelay = My.MySettings.Default.StatusDelay
        ' enable MF/MC handshake
        ' SM15M101110110001000001
        Dim mode As ResponseModes = ResponseModes.None Or
                ResponseModes.HandshakePosition Or
                ResponseModes.HandshakeDeviceCommands Or
                ResponseModes.HandshakeCommands Or
                ResponseModes.TestStartSent Or
                ResponseModes.PatternCompleteResponse Or
                ResponseModes.PauseContinueResponse Or
                ResponseModes.EnhancedTestStart Or
                ResponseModes.WaferBegin Or
                ResponseModes.None
        ' sends the message to the prober and read the reply.
        If Not My.MySettings.Default.UsingScpi Then Me.WriteResponseModeReadReply(mode)
        ' update the response mode. 
        Me.ResponseMode = mode

        ' set the emulation.
        Me.SupportedCommandPrefixes = New String() {Me.SetModeCommandPrefix, "ID", Me.TestCompleteCommand, "?E"}
        Me.SupportedCommands = New String() {Me.SetModeCommand, Me.IdentityQueryCommand, Me.TestCompleteCommand, Me.ErrorQueryCommand}
        Me.SupportedReplyPatterns = New String() {$"{Me.ErrorReplyPattern}0", $"{Me.ErrorReplyPattern}0", $"{Me.IdentityReplyPattern}CD.249799-011",
                                                  Me.PatternCompleteReplyPattern, Me.TestAgainStartPattern, Me.FirstTestStartPattern,
                                                  Me.RetestStartPattern, Me.TestStartPattern, Me.MessageCompletedPattern, Me.MessageFailedPattern,
                                                  $"{Me.WaferStartPattern}I/-299"}
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.ResponseMode = ResponseModes.EnhancedTestStart Or
                ResponseModes.EnhancedPatternComplete Or
                ResponseModes.HandshakePosition Or
                ResponseModes.HandshakeDeviceCommands Or
                ResponseModes.PatternCompleteResponse Or
                ResponseModes.TestStartSent Or
                ResponseModes.WaferComplete
    End Sub
#End Region

#Region " COMMAND SYNTAX "

#End Region

#Region " RESPONSE MODE "

    ''' <summary> The Response Mode. </summary>
    Private _ResponseMode As ResponseModes?

    ''' <summary> Gets or sets the cached Response Mode. </summary>
    ''' <value> The Response Mode or null if unknown. </value>
    Public Property ResponseMode As ResponseModes?
        Get
            Return Me._ResponseMode
        End Get
        Protected Set(ByVal value As ResponseModes?)
            If Not Nullable.Equals(Me.ResponseMode, value) Then
                Me._ResponseMode = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Queries trim end. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="dataToWrite"> The data to write. </param>
    ''' <returns> The trim end. </returns>
    Public Shadows Function QueryTrimEnd(ByVal dataToWrite As String) As String
        Return MyBase.QueryTrimEnd(Me.Session.StatusReadDelay, Me.Session.ReadDelay, dataToWrite)
    End Function

    ''' <summary>
    ''' Queries the Response Mode. Also sets the <see cref="ResponseMode"></see> cached value.
    ''' </summary>
    ''' <remarks> The 2001x does not returns the expected replay. It returns: 'SZDW0C1'. </remarks>
    ''' <returns> The Response Mode or null if unknown. </returns>
    Public Function QueryResponseMode() As ResponseModes?
        Dim mode As String = Me.ResponseMode.ToString
        Me.Session.MakeEmulatedReplyIfEmpty(mode)
        mode = Me.QueryTrimEnd("?SM15")
        If String.IsNullOrWhiteSpace(mode) Then
            Me.ResponseMode = ResponseModes.None
        Else
            ' reverse the order so as to match the enumeration.
            mode = mode.ToCharArray.Reverse.ToString
            Me.ResponseMode = CType(Convert.ToInt32(mode, 2), ResponseModes)
        End If
        Return Me.ResponseMode
    End Function

    ''' <summary> Writes a response mode read reply. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="mode"> The mode. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function WriteResponseModeReadReply(mode As ResponseModes) As Boolean
        Me.PublishInfo("Setting response mode to 0x{0: X4};. ", CInt(mode))
        Dim affirmative As Boolean
        If Me.WriteResponseMode(mode).HasValue Then
            'Dim bit As VI.Pith.ServiceRequests = Me.StatusSubsystem.MessageAvailableBits
            ' Me.PublishInfo("Awaiting SRQ equal to 0x{0:X2};. ", CInt(bit))
            Me.PublishInfo($"Reading with delay {Me.Session.ReadDelay:hh\:mm\:ss\.fff};. ")
            Me.Session.MakeEmulatedReplyIfEmpty(Me.MessageCompletedPattern)
            Me.FetchAndParse(Me.Session.StatusReadDelay, Me.Session.ReadDelay)
            If Me.MessageCompleted Then
                affirmative = True
                Me.PublishInfo("Response mode set to 0x{0:X4};. ", CInt(Me.ResponseMode))
            ElseIf Me.MessageFailed Then
                Throw New isr.Core.OperationFailedException("Message Failed initializing response mode to 0x{0:X4};.", CInt(mode))
            Else
                Throw New isr.Core.OperationFailedException("Unexpected reply '{0}' initializing response mode;. ", Me.LastReading)
            End If
        Else
            Throw New isr.Core.OperationFailedException("Failed initializing response mode.")
        End If
        Return affirmative
    End Function

    ''' <summary> Response mode command. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The Response Mode. </param>
    ''' <returns> A String. </returns>
    Public Shared Function ResponseModeCommand(ByVal value As ResponseModes) As String
        Dim chars As Char() = Convert.ToString(value, 2).ToCharArray
        Array.Reverse(chars)
        Return $"SM15M{New String(chars)}"
    End Function

    ''' <summary> Writes the Response Mode. Does not read back from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The Response Mode. </param>
    ''' <returns> The Response Mode or null if unknown. </returns>
    Public Function WriteResponseMode(ByVal value As ResponseModes) As ResponseModes?
        'Dim chars As Char() = Convert.ToString(value, 2).ToCharArray
        'Array.Reverse(chars)
        'Me.Session.WriteLine("SM15M{0}", New String(chars))
        Me.Session.WriteLine(ProberSubsystem.ResponseModeCommand(value))
        Me.ResponseMode = value
        Return Me.ResponseMode
    End Function

    ''' <summary> Writes the Response Mode. Does not read back from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The Response Mode. </param>
    ''' <returns> The Response Mode or null if unknown. </returns>
    Public Function WriteAsyncResponseMode(ByVal value As ResponseModes) As ResponseModes?
        Dim chars As Char() = Convert.ToString(value, 2).ToCharArray
        Array.Reverse(chars)
        Dim responseMode As String = New String(chars)
        Dim msg As String = String.Format(Globalization.CultureInfo.InvariantCulture, "SM15M{0}", responseMode)
        Me.ResponseMode = If(Me.TrySendAsync(msg, 3, TimeSpan.FromMilliseconds(100), TimeSpan.FromMilliseconds(1000)), value, New ResponseModes?)
        Return Me.ResponseMode
    End Function

    ''' <summary> Writes and reads back the Response Mode. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The <see cref="ResponseModes">Response Mode</see>. </param>
    ''' <returns> The Response Mode or null if unknown. </returns>
    Public Function ApplyAsyncResponseMode(ByVal value As ResponseModes) As ResponseModes?
        Return If(Me.WriteAsyncResponseMode(value).HasValue, Me.QueryResponseMode(), New ResponseModes?)
    End Function

#End Region

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class

''' <summary> Enumerates the responses. </summary>
''' <remarks> See Electroglass Manual page 4-9 (23) for detailed descriptions. </remarks>
<Flags()>
Public Enum ResponseModes

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not set")>
    None

    ''' <summary> An enum constant representing the handshake position option. </summary>
    <Description("1: MC/MF on X/Y")>
    HandshakePosition = CInt(2 ^ 0)

    ''' <summary> An enum constant representing the handshake chuck option. </summary>
    <Description("2: MC/MF on Z")>
    HandshakeChuck = CInt(2 ^ 1)

    ''' <summary> An enum constant representing the handshake device commands option. </summary>
    <Description("3: MC/MF on device commands")>
    HandshakeDeviceCommands = CInt(2 ^ 2)

    ''' <summary> An enum constant representing the handshake commands option. </summary>
    <Description("4: MC/MF on balance parameter commands")>
    HandshakeCommands = CInt(2 ^ 3)

    ''' <summary> An enum constant representing the test start sent option. </summary>
    <Description("5: TS sent")>
    TestStartSent = CInt(2 ^ 4)

    ''' <summary> An enum constant representing the test complete response option. </summary>
    <Description("6: TC response")>
    TestCompleteResponse = CInt(2 ^ 5)

    ''' <summary> An enum constant representing the pattern complete response option. </summary>
    <Description("7: PC response")>
    PatternCompleteResponse = CInt(2 ^ 6)

    ''' <summary> An enum constant representing the pause continue response option. </summary>
    <Description("8: PA/CO response")>
    PauseContinueResponse = CInt(2 ^ 7)

    ''' <summary> An enum constant representing the alarm response option. </summary>
    <Description("9: Alarm response")>
    AlarmResponse = CInt(2 ^ 8)

    ''' <summary> An enum constant representing the wafer complete option. </summary>
    <Description("10: WC (Wafer complete)")>
    WaferComplete = CInt(2 ^ 9)

    ''' <summary> An enum constant representing the enhanced pattern complete option. </summary>
    <Description("11: Enhanced PC")>
    EnhancedPatternComplete = CInt(2 ^ 10)

    ''' <summary> An enum constant representing the enhanced test start option. </summary>
    <Description("12: Enhanced TS")>
    EnhancedTestStart = CInt(2 ^ 11)

    ''' <summary> An enum constant representing the ugly die report option. </summary>
    <Description("13: Ugly Die Report")>
    UglyDieReport = CInt(2 ^ 12)

    ''' <summary> An enum constant representing the map transfer retries option. </summary>
    <Description("14: Map Transfer Retries")>
    MapTransferRetries = CInt(2 ^ 13)

    ''' <summary> An enum constant representing the send coordinates with test start option. </summary>
    <Description("15: Send Coordinates With Test Start")>
    SendCoordinatesWithTestStart = CInt(2 ^ 14)

    ''' <summary> An enum constant representing the send cassette messages option. </summary>
    <Description("16: Send EC/BC (End/Begin Cassette) Messages")>
    SendCassetteMessages = CInt(2 ^ 15)

    ''' <summary> An enum constant representing the pause pending option. </summary>
    <Description("17: Pause Pending")>
    PausePending = CInt(2 ^ 16)

    ''' <summary> An enum constant representing the wafer begin option. </summary>
    <Description("18: Wafer Begin")>
    WaferBegin = CInt(2 ^ 17)
End Enum

