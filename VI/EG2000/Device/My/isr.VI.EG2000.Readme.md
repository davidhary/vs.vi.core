## ISR VI EG2000<sub>&trade;</sub>: Electroglass Prober VI Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.6915 2018-12-07*  
Validates reading when handling service requests and
read once again on an invalid reading.

*5.0.6914 2018-12-06*  
Validates reading when handling service requests and
retries on invalid readings.

*5.0.6913 2018-12-05*  
Adds a delay before reading the message following the
service request.

*5.0.6912 2018-12-04*  
Uses simple fetch and parse from the event handler.
Removes call to read registers after the request is handled.

*5.0.6911 2018-12-03*  
Sets read and status read delays when Prober subsystem
initializes to its known state.

*5.0.6904 2018-11-26*  
Displays wait time in seconds with fractions. Fixes
how reading the error reply is handled.

*5.0.6897 2018-11-19*  
Adds read and status read delay to the status
subsystem.

*5.0.6892 2018-11-14*  
Defaults to using Keep Alive attributes.

*5.0.6839 2018-09-22*  
Adds settings for poll and timeout intervals. Uses
maximum of refractory period and poll interval for command async.

*4.1.6440 2017-08-19*  
Uses modified trace message and logger.

*4.1.6438 2017-08-17*  
Uses the device trace message talker to apply a talker
to the panel.

*4.0.5907 2016-03-04*  
Disables controls when opening the device panel.

*4.0.5803 2015-11-21 New structures and name spaces.

*3.0.5181 2014-03-09*  
Uses abstract properties to define instrument
commands.

*3.0.5151 2014-02-07*  
Removes library trace log settings. Uses async
notifications of property changes.

*3.0.5126 2014-01-13*  
Tagged as 2014.

*3.0.5067 2013-11-15*  
Clarifies the messages indicating that no resources
were found. Fixes layout of the selector connector. Overrides Device
Open in the Device class and set Device Open in the Status Subsystem.

*3.0.5046 2013-10-25*  
Adds command trial count and poll and timeout
intervals for sending commands.

*3.0.5045 2013-10-24*  
Moves setting default status bits to the constructors.
Adds manual page for response modes. Spells out EC/BC as \'Cassette\'.
Moves setting of default status bits to the instantiation methods.

*3.0.5044 2013-10-23*  
Removes initialization from the controller panel. Uses
background worker to send command.

*3.0.5042 2013-10-21*  
Adds rest of response settings options. Initializes
Prober to SM15M101110110001000001 . Adds interface clear, selective
device clear and initialize known state to the controller panel.

*3.0.5039 2013-10-18*  
Fixes incorrect response mode enumerated item. Fixes
conversion of response mode to binary string. Does no report empty error
as warning. Permits using SCPI messages for enabling service request and
identity to permit rudimentary testing with a Keithley system

*3.0.5031 2013-10-10*  
Initialize Prober to default state when connected.

*3.0.5013 2013-09-23*  
Created based on Multi Meter project.

\(C\) 2013 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Typed Units Libraries](https://bitbucket.org/davidhary/Arebis.UnitsAmounts)  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)

### Closed software  [](#){name=Closed-software}
Closed software used by this software are described and licensed on
the following sites:  
[Core Libraries](https://bitbucket.org/davidhary/vs.core)  
[IVI VISA](http://www.ivifoundation.org)  
[VI Libraries](https://bitbucket.org/davidhary/vs.IOvi)
