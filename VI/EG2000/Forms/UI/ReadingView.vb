Imports System.ComponentModel

Imports isr.VI.ExceptionExtensions

''' <summary> A reading view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class ReadingView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()
        Me.InitializingComponents = False
    End Sub

    ''' <summary> Creates a new <see cref="ReadingView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="ReadingView"/>. </returns>
    Public Shared Function Create() As ReadingView
        Dim view As ReadingView = Nothing
        Try
            view = New ReadingView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                                            <c>False</c> to
    '''                                                                            release only
    '''                                                                            unmanaged resources
    '''                                                                            when called from
    '''                                                                            the runtime
    '''                                                                            finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                ' make sure the device is unbound in case the form is closed without closing the device.
                Me.AssignDeviceThis(Nothing)
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " FORM EVENTS "

    ''' <summary> Handles the <see cref="E:System.Windows.Forms.UserControl.Load" /> event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    Protected Overrides Sub OnLoad(e As EventArgs)
        Try
        Finally
            MyBase.OnLoad(e)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> The device. </summary>
    Private _Device As EG2000Device

    ''' <summary> Gets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As EG2000Device
        Get
            Return Me._Device
        End Get
    End Property

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As EG2000Device)
        If Me._Device IsNot Nothing Then
            ' the device base already clears all or only the private listeners. 
            ' Me._Device.RemovePrivateListeners()
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As EG2000Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " CONTROL EVENTS: READ and WRITE "

    ''' <summary> Event handler. Called by _EmulateButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub EmulateButton_Click(sender As System.Object, ByVal e As System.EventArgs) Handles _EmulateButton.Click
        Dim activity As String = String.Empty
        Try
            Me.InfoProvider.Clear()
            activity = $"{Me.Device.ResourceNameCaption} Getting emulation message"
            Dim message As String = Me._EmulatedReplyComboBox.Text.Trim
            If Not String.IsNullOrWhiteSpace(message) Then
                activity = $"Setting last reading to {message}"
                Me.Device.ProberSubsystem.LastReading = message
                activity = $"Parsing emulated message {message}"
                Me.Device.ProberSubsystem.ParseReading(message)
            End If
        Catch ex As Exception
            Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by _WriteButton for click events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub WriteButton_Click(sender As System.Object, ByVal e As System.EventArgs) Handles _WriteButton.Click
        Dim c As Windows.Forms.Control = TryCast(sender, Windows.Forms.Control)
        If c IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(Me._CommandComboBox.Text) Then
            Dim message As String = Me._CommandComboBox.Text.Trim
            Dim activity As String = String.Empty
            Try
                activity = $"{Me.Device.ResourceNameCaption} sending session message"

                Me.InfoProvider.Clear()
                Me.Cursor = Windows.Forms.Cursors.WaitCursor
                Me.Device.ProberSubsystem.CommandTrialCount = Global.isr.VI.EG2000.My.MySettings.Default.CommandRetrylCount
                Me.Device.ProberSubsystem.CommandPollInterval = If(Global.isr.VI.EG2000.My.MySettings.Default.CommandPollInterval < Global.isr.VI.EG2000.My.MySettings.Default.ReadDelay,
                                                                                Global.isr.VI.EG2000.My.MySettings.Default.ReadDelay, Global.isr.VI.EG2000.My.MySettings.Default.CommandPollInterval)
                Me.Device.ProberSubsystem.CommandTimeoutInterval = Global.isr.VI.EG2000.My.MySettings.Default.CommandTimeoutInterval
                If Me._CommandComboBox.Text.StartsWith(Me.Device.ProberSubsystem.TestCompleteCommand,
                                                       StringComparison.OrdinalIgnoreCase) Then
                    Me.Device.ProberSubsystem.CommandTimeoutInterval = Global.isr.VI.EG2000.My.MySettings.Default.TestCompletedTimeoutInterval
                End If
                If Me.Device.ProberSubsystem.TrySendAsync(message) Then
                    Me.PublishInfo($"Message sent;. Sent: '{Me.Device.ProberSubsystem.LastMessageSent}'; Received: '{Me.Device.ProberSubsystem.LastReading}'.")
                Else
                    If Me.Device.ProberSubsystem.UnhandledMessageSent Then
                        Me.PublishWarning("Failed sending message--unknown message sent;. Sent: {0}", message)
                        Me.InfoProvider.SetError(c, "Failed sending message--unknown message sent.")
                    ElseIf Me.Device.ProberSubsystem.UnhandledMessageReceived Then
                        Me.PublishWarning("Failed sending message--unknown message received;. Sent: {0}", message)
                        Me.InfoProvider.SetError(c, "Failed sending message--unknown message received.")
                    Else
                        Me.PublishWarning("Failed sending message;. Sent: {0}", message)
                        Me.InfoProvider.SetError(c, "Failed sending message.")
                    End If
                End If
            Catch ex As Exception
                Me.InfoProvider.Annunciate(sender, isr.Core.Forma.InfoProviderLevel.Error, $"Exception {activity} {ex.ToFullBlownString}")
                Me.PublishException(activity, ex)
            Finally
                ' TO_DO: added with new view; needs validation.
                Me.ReadStatusRegister()
                Me.Cursor = Windows.Forms.Cursors.Default
            End Try
        End If
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

End Class
