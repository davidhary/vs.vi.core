﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ReadingView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._SentGroupBox = New System.Windows.Forms.GroupBox()
        Me._SendMessageLabel = New System.Windows.Forms.Label()
        Me._UnhandledSendLabel = New System.Windows.Forms.Label()
        Me._ReceivedGroupBox = New System.Windows.Forms.GroupBox()
        Me._ReceivedMessageLabel = New System.Windows.Forms.Label()
        Me._UnhandledMessageLabel = New System.Windows.Forms.Label()
        Me._ReceivedMessagesGroupBox = New System.Windows.Forms.GroupBox()
        Me._WaferStartReceivedLabel = New System.Windows.Forms.Label()
        Me._TestStartAttributeLabel = New System.Windows.Forms.Label()
        Me._TestStartedLabel = New System.Windows.Forms.Label()
        Me._PatternCompleteLabel = New System.Windows.Forms.Label()
        Me._TestCompleteLabel = New System.Windows.Forms.Label()
        Me._EmulatedReplyComboBox = New System.Windows.Forms.ComboBox()
        Me._CommandComboBox = New System.Windows.Forms.ComboBox()
        Me._LastMessageTextBox = New System.Windows.Forms.TextBox()
        Me._LastMessageTextBoxLabel = New System.Windows.Forms.Label()
        Me._EmulateButton = New System.Windows.Forms.Button()
        Me._WriteButton = New System.Windows.Forms.Button()
        Me._SentGroupBox.SuspendLayout()
        Me._ReceivedGroupBox.SuspendLayout()
        Me._ReceivedMessagesGroupBox.SuspendLayout()
        Me.SuspendLayout()
        '
        '_SentGroupBox
        '
        Me._SentGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SentGroupBox.Controls.Add(Me._SendMessageLabel)
        Me._SentGroupBox.Controls.Add(Me._UnhandledSendLabel)
        Me._SentGroupBox.Location = New System.Drawing.Point(153, 143)
        Me._SentGroupBox.Name = "_SentGroupBox"
        Me._SentGroupBox.Size = New System.Drawing.Size(229, 72)
        Me._SentGroupBox.TabIndex = 24
        Me._SentGroupBox.TabStop = False
        Me._SentGroupBox.Text = "Message Sent"
        '
        '_SendMessageLabel
        '
        Me._SendMessageLabel.AutoSize = True
        Me._SendMessageLabel.Location = New System.Drawing.Point(6, 46)
        Me._SendMessageLabel.Name = "_SendMessageLabel"
        Me._SendMessageLabel.Size = New System.Drawing.Size(46, 17)
        Me._SendMessageLabel.TabIndex = 1
        Me._SendMessageLabel.Text = "Sent ..."
        '
        '_UnhandledSendLabel
        '
        Me._UnhandledSendLabel.AutoSize = True
        Me._UnhandledSendLabel.Location = New System.Drawing.Point(6, 22)
        Me._UnhandledSendLabel.Name = "_UnhandledSendLabel"
        Me._UnhandledSendLabel.Size = New System.Drawing.Size(56, 17)
        Me._UnhandledSendLabel.TabIndex = 0
        Me._UnhandledSendLabel.Text = "? Sent: .."
        '
        '_ReceivedGroupBox
        '
        Me._ReceivedGroupBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ReceivedGroupBox.Controls.Add(Me._ReceivedMessageLabel)
        Me._ReceivedGroupBox.Controls.Add(Me._UnhandledMessageLabel)
        Me._ReceivedGroupBox.Location = New System.Drawing.Point(153, 70)
        Me._ReceivedGroupBox.Name = "_ReceivedGroupBox"
        Me._ReceivedGroupBox.Size = New System.Drawing.Size(229, 67)
        Me._ReceivedGroupBox.TabIndex = 23
        Me._ReceivedGroupBox.TabStop = False
        Me._ReceivedGroupBox.Text = "Received Messages"
        '
        '_ReceivedMessageLabel
        '
        Me._ReceivedMessageLabel.AutoSize = True
        Me._ReceivedMessageLabel.Location = New System.Drawing.Point(6, 41)
        Me._ReceivedMessageLabel.Name = "_ReceivedMessageLabel"
        Me._ReceivedMessageLabel.Size = New System.Drawing.Size(73, 17)
        Me._ReceivedMessageLabel.TabIndex = 1
        Me._ReceivedMessageLabel.Text = "Received: .."
        '
        '_UnhandledMessageLabel
        '
        Me._UnhandledMessageLabel.AutoSize = True
        Me._UnhandledMessageLabel.BackColor = System.Drawing.Color.LightGreen
        Me._UnhandledMessageLabel.Location = New System.Drawing.Point(6, 19)
        Me._UnhandledMessageLabel.Name = "_UnhandledMessageLabel"
        Me._UnhandledMessageLabel.Size = New System.Drawing.Size(83, 17)
        Me._UnhandledMessageLabel.TabIndex = 0
        Me._UnhandledMessageLabel.Text = "? Received: .."
        '
        '_ReceivedMessagesGroupBox
        '
        Me._ReceivedMessagesGroupBox.Controls.Add(Me._WaferStartReceivedLabel)
        Me._ReceivedMessagesGroupBox.Controls.Add(Me._TestStartAttributeLabel)
        Me._ReceivedMessagesGroupBox.Controls.Add(Me._TestStartedLabel)
        Me._ReceivedMessagesGroupBox.Controls.Add(Me._PatternCompleteLabel)
        Me._ReceivedMessagesGroupBox.Controls.Add(Me._TestCompleteLabel)
        Me._ReceivedMessagesGroupBox.Location = New System.Drawing.Point(5, 69)
        Me._ReceivedMessagesGroupBox.Name = "_ReceivedMessagesGroupBox"
        Me._ReceivedMessagesGroupBox.Size = New System.Drawing.Size(131, 133)
        Me._ReceivedMessagesGroupBox.TabIndex = 22
        Me._ReceivedMessagesGroupBox.TabStop = False
        Me._ReceivedMessagesGroupBox.Text = "Prober Cycle"
        '
        '_WaferStartReceivedLabel
        '
        Me._WaferStartReceivedLabel.AutoSize = True
        Me._WaferStartReceivedLabel.Location = New System.Drawing.Point(6, 19)
        Me._WaferStartReceivedLabel.Name = "_WaferStartReceivedLabel"
        Me._WaferStartReceivedLabel.Size = New System.Drawing.Size(88, 17)
        Me._WaferStartReceivedLabel.TabIndex = 0
        Me._WaferStartReceivedLabel.Text = "Wafer Started"
        '
        '_TestStartAttributeLabel
        '
        Me._TestStartAttributeLabel.AutoSize = True
        Me._TestStartAttributeLabel.Location = New System.Drawing.Point(6, 41)
        Me._TestStartAttributeLabel.Name = "_TestStartAttributeLabel"
        Me._TestStartAttributeLabel.Size = New System.Drawing.Size(98, 17)
        Me._TestStartAttributeLabel.TabIndex = 1
        Me._TestStartAttributeLabel.Text = "1st Test Started"
        '
        '_TestStartedLabel
        '
        Me._TestStartedLabel.AutoSize = True
        Me._TestStartedLabel.Location = New System.Drawing.Point(6, 63)
        Me._TestStartedLabel.Name = "_TestStartedLabel"
        Me._TestStartedLabel.Size = New System.Drawing.Size(77, 17)
        Me._TestStartedLabel.TabIndex = 2
        Me._TestStartedLabel.Text = "Test Started"
        '
        '_PatternCompleteLabel
        '
        Me._PatternCompleteLabel.AutoSize = True
        Me._PatternCompleteLabel.Location = New System.Drawing.Point(6, 107)
        Me._PatternCompleteLabel.Name = "_PatternCompleteLabel"
        Me._PatternCompleteLabel.Size = New System.Drawing.Size(109, 17)
        Me._PatternCompleteLabel.TabIndex = 4
        Me._PatternCompleteLabel.Text = "Pattern Complete"
        '
        '_TestCompleteLabel
        '
        Me._TestCompleteLabel.AutoSize = True
        Me._TestCompleteLabel.Location = New System.Drawing.Point(6, 85)
        Me._TestCompleteLabel.Name = "_TestCompleteLabel"
        Me._TestCompleteLabel.Size = New System.Drawing.Size(91, 17)
        Me._TestCompleteLabel.TabIndex = 3
        Me._TestCompleteLabel.Text = "Test Complete"
        '
        '_EmulatedReplyComboBox
        '
        Me._EmulatedReplyComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._EmulatedReplyComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._EmulatedReplyComboBox.FormattingEnabled = True
        Me._EmulatedReplyComboBox.Location = New System.Drawing.Point(86, 40)
        Me._EmulatedReplyComboBox.Name = "_EmulatedReplyComboBox"
        Me._EmulatedReplyComboBox.Size = New System.Drawing.Size(296, 25)
        Me._EmulatedReplyComboBox.TabIndex = 21
        '
        '_CommandComboBox
        '
        Me._CommandComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._CommandComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._CommandComboBox.FormattingEnabled = True
        Me._CommandComboBox.Location = New System.Drawing.Point(86, 9)
        Me._CommandComboBox.Name = "_CommandComboBox"
        Me._CommandComboBox.Size = New System.Drawing.Size(296, 25)
        Me._CommandComboBox.TabIndex = 19
        Me._CommandComboBox.Text = "SM15M0000000000000"
        '
        '_LastMessageTextBox
        '
        Me._LastMessageTextBox.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._LastMessageTextBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._LastMessageTextBox.Location = New System.Drawing.Point(5, 226)
        Me._LastMessageTextBox.Multiline = True
        Me._LastMessageTextBox.Name = "_LastMessageTextBox"
        Me._LastMessageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._LastMessageTextBox.Size = New System.Drawing.Size(377, 169)
        Me._LastMessageTextBox.TabIndex = 26
        '
        '_LastMessageTextBoxLabel
        '
        Me._LastMessageTextBoxLabel.AutoSize = True
        Me._LastMessageTextBoxLabel.Location = New System.Drawing.Point(2, 207)
        Me._LastMessageTextBoxLabel.Name = "_LastMessageTextBoxLabel"
        Me._LastMessageTextBoxLabel.Size = New System.Drawing.Size(95, 17)
        Me._LastMessageTextBoxLabel.TabIndex = 25
        Me._LastMessageTextBoxLabel.Text = "Last Message: "
        '
        '_EmulateButton
        '
        Me._EmulateButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._EmulateButton.Location = New System.Drawing.Point(5, 36)
        Me._EmulateButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._EmulateButton.Name = "_EmulateButton"
        Me._EmulateButton.Size = New System.Drawing.Size(74, 30)
        Me._EmulateButton.TabIndex = 20
        Me._EmulateButton.Text = "&Emulate"
        Me._EmulateButton.UseVisualStyleBackColor = True
        '
        '_WriteButton
        '
        Me._WriteButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._WriteButton.Location = New System.Drawing.Point(21, 5)
        Me._WriteButton.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me._WriteButton.Name = "_WriteButton"
        Me._WriteButton.Size = New System.Drawing.Size(58, 30)
        Me._WriteButton.TabIndex = 18
        Me._WriteButton.Text = "&Write"
        Me._WriteButton.UseVisualStyleBackColor = True
        '
        'ReadingView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._SentGroupBox)
        Me.Controls.Add(Me._ReceivedGroupBox)
        Me.Controls.Add(Me._ReceivedMessagesGroupBox)
        Me.Controls.Add(Me._EmulatedReplyComboBox)

        Me.Controls.Add(Me._CommandComboBox)
        Me.Controls.Add(Me._LastMessageTextBox)
        Me.Controls.Add(Me._LastMessageTextBoxLabel)
        Me.Controls.Add(Me._EmulateButton)
        Me.Controls.Add(Me._WriteButton)
        Me.Name = "ReadingView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(390, 408)
        Me._SentGroupBox.ResumeLayout(False)
        Me._SentGroupBox.PerformLayout()
        Me._ReceivedGroupBox.ResumeLayout(False)
        Me._ReceivedGroupBox.PerformLayout()
        Me._ReceivedMessagesGroupBox.ResumeLayout(False)
        Me._ReceivedMessagesGroupBox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _SentGroupBox As Windows.Forms.GroupBox
    Private WithEvents _SendMessageLabel As Windows.Forms.Label
    Private WithEvents _UnhandledSendLabel As Windows.Forms.Label
    Private WithEvents _ReceivedGroupBox As Windows.Forms.GroupBox
    Private WithEvents _ReceivedMessageLabel As Windows.Forms.Label
    Private WithEvents _UnhandledMessageLabel As Windows.Forms.Label
    Private WithEvents _ReceivedMessagesGroupBox As Windows.Forms.GroupBox
    Private WithEvents _WaferStartReceivedLabel As Windows.Forms.Label
    Private WithEvents _TestStartAttributeLabel As Windows.Forms.Label
    Private WithEvents _TestStartedLabel As Windows.Forms.Label
    Private WithEvents _PatternCompleteLabel As Windows.Forms.Label
    Private WithEvents _TestCompleteLabel As Windows.Forms.Label
    Private WithEvents _EmulatedReplyComboBox As Windows.Forms.ComboBox
    Private WithEvents _CommandComboBox As Windows.Forms.ComboBox
    Private WithEvents _LastMessageTextBox As Windows.Forms.TextBox
    Private WithEvents _LastMessageTextBoxLabel As Windows.Forms.Label
    Private WithEvents _EmulateButton As Windows.Forms.Button
    Private WithEvents _WriteButton As Windows.Forms.Button
End Class
