﻿''' <summary> K2600 Visa View unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2600")>
Public Class VisaViewTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K2600FormsTests.ResourceSettings.Get.Exists, $"{GetType(K2600FormsTests.ResourceSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " VISA VIEW: DEVICE OPEN TEST "

    ''' <summary> (Unit Test Method) tests selected resource name. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SelectedResourceNameTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertResourceNameShouldBeSelected(TestInfo, view, ResourceSettings.Get)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests the Visa View talker trace message. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub TalkerTraceMessageTest()
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit(view, TestInfo.TraceMessagesQueueListener)
            End Using
        End Using
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewTraceMessageShouldEmit(view, TestInfo.TraceMessagesQueueListener)
            End Using
        End Using
    End Sub

    ''' <summary> A test for Open connect and disconnect. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTest()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As New isr.VI.Facade.VisaView(device)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen(TestInfo, 0, view, ResourceSettings.Get)
                    isr.VI.FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch(view.VisaSessionBase.Session, ResourceSettings.Get)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 0, view)
                End Try
            End Using
        End Using
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As New isr.VI.Facade.VisaTreeView(device)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpen(TestInfo, 0, view, ResourceSettings.Get)
                    isr.VI.FacadeTests.DeviceManager.AssertSessionResourceNamesShouldMatch(view.VisaSessionBase.Session, ResourceSettings.Get)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 0, view)
                End Try
            End Using
        End Using
    End Sub

    ''' <summary> A test for dual Open. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub OpenSessionTwiceTest()
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, ResourceSettings.Get)
            End Using
            Using view As isr.VI.Facade.IVisaView = New isr.VI.Facade.VisaView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 2, view, ResourceSettings.Get)
            End Using
        End Using
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Using view As New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, ResourceSettings.Get)
            End Using
            Using view As New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 2, view, ResourceSettings.Get)
            End Using
            Using view As New isr.VI.Facade.VisaTreeView(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 3, view, ResourceSettings.Get)
                Threading.Tasks.Task.Delay(TimeSpan.FromMilliseconds(100)).Wait()
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 4, view, ResourceSettings.Get)
            End Using
        End Using
    End Sub

#End Region

#Region " VISA VIEW: ASSIGNED DEVICE TESTS "

    ''' <summary> (Unit Test Method) tests assign device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignDeviceTest()
        Using view As New VI.Tsp.K2600.Forms.K2600View
            Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                view.AssignDevice(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, ResourceSettings.Get)
            End Using
        End Using
        Using view As New VI.Tsp.K2600.Forms.K2600TreeView
            Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                view.AssignDevice(device)
                isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldOpenAndClose(TestInfo, 1, view, ResourceSettings.Get)
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests assign open device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub AssignOpenDeviceTest()
        Using view As New VI.Tsp.K2600.Forms.K2600View
            Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen(TestInfo, 1, device, ResourceSettings.Get)
                    view.AssignDevice(device)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 1, view)
                End Try
            End Using
        End Using
        Using view As New VI.Tsp.K2600.Forms.K2600TreeView
            Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
                device.AddListener(TestInfo.TraceMessagesQueueListener)
                Try
                    isr.VI.FacadeTests.DeviceManager.AssertVisaSessionBaseShouldOpen(TestInfo, 1, device, ResourceSettings.Get)
                    view.AssignDevice(device)
                Catch
                    Throw
                Finally
                    isr.VI.FacadeTests.DeviceManager.AssertVisaViewSessionShouldClose(TestInfo, 1, view)
                End Try
            End Using
        End Using
    End Sub

#End Region

End Class
