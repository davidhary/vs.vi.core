Imports isr.Core.NumericExtensions

''' <summary> Defines a measured shunt resistance element. </summary>
''' <remarks>
''' David, 2012-11-10, 3.1.4697 <para>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public MustInherit Class ShuntResistanceBase
    Inherits ResistanceMeasureBase
    Implements System.IEquatable(Of ShuntResistanceBase)

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clones an existing measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Protected Sub New(ByVal value As ShuntResistanceBase)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me._CurrentRange = value.CurrentRange
        End If
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Restores defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.Aperture = My.MySettings.Default.ShuntResistanceApertureDefault
        Me.CurrentRange = My.MySettings.Default.ShuntResistanceCurrentRangeDefault
        Me.CurrentLevel = My.MySettings.Default.ShuntResistanceCurrentLevelDefault
        Me.LowLimit = My.MySettings.Default.ShuntResistanceLowLimitDefault
        Me.HighLimit = My.MySettings.Default.ShuntResistanceHighLimitDefault
        Me.VoltageLimit = My.MySettings.Default.ShuntResistanceVoltageLimitDefault
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> Check throw if unequal configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="other"> The resistance to compare to this object. </param>
    Public Overloads Sub CheckThrowUnequalConfiguration(ByVal other As ShuntResistanceBase)
        If other IsNot Nothing Then
            MyBase.CheckThrowUnequalConfiguration(other)
            If Not Me.ConfigurationEquals(other) Then
                Dim format As String = "Unequal configuring--instrument {0}={1}.NE.{2}"
                If Not Me.CurrentRange.Approximates(other.CurrentRange, 0.000001) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format,
                                                                              "Current Range", Me.CurrentRange, other.CurrentRange))
                Else
                    Debug.Assert(Not Debugger.IsAttached, "Failed logic")
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ShuntResistanceBase"></see> value is equal to a
    ''' specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="obj"> An object. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, ShuntResistanceBase))
    End Function

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ShuntResistanceBase"></see> value is equal to a
    ''' specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="other"> The cold resistance to compare to this object. </param>
    ''' <returns>
    ''' <c>True</c> if the other parameter is equal to the current
    ''' <see cref="T:ShuntResistanceBase"></see> value;
    ''' otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Function Equals(ByVal other As ShuntResistanceBase) As Boolean Implements System.IEquatable(Of ShuntResistanceBase).Equals
        Return other IsNot Nothing AndAlso MyBase.Equals(other) AndAlso
            Me.CurrentRange.Approximates(other.CurrentRange, 0.000001) AndAlso
            True
    End Function

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ShuntResistanceBase"></see> configuration values
    ''' are equal to a specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="other"> The cold resistance to compare to this object. </param>
    ''' <returns>
    ''' <c>True</c> if the other parameter is equal to the current
    ''' <see cref="T:ShuntResistanceBase"></see> value;
    ''' otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Function ConfigurationEquals(ByVal other As ShuntResistanceBase) As Boolean
        Return other IsNot Nothing AndAlso MyBase.ConfigurationEquals(other) AndAlso
            Me.CurrentRange.Approximates(other.CurrentRange, 0.000001) AndAlso
            True
    End Function

    ''' <summary> Returns a hash code for this instance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A hash code for this object. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return MyBase.GetHashCode
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As ShuntResistanceBase, ByVal right As ShuntResistanceBase) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As ShuntResistanceBase, ByVal right As ShuntResistanceBase) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

#End Region

#Region " CONFIGURATION PROPERTIES "

    ''' <summary> The current range. </summary>
    Private _CurrentRange As Double

    ''' <summary> Gets or sets the current Range. </summary>
    ''' <value> The current Range. </value>
    Public Property CurrentRange() As Double
        Get
            Return Me._CurrentRange
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.CurrentRange) Then
                Me._CurrentRange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class
