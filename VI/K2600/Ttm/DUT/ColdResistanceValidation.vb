
Partial Public Class ColdResistance

    ''' <summary> Validates the aperture described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateAperture(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ColdResistanceApertureMinimum AndAlso value <= My.MySettings.Default.ColdResistanceApertureMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ColdResistanceApertureMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance aperture value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceApertureMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance aperture value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceApertureMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the current level described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateCurrentLevel(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ColdResistanceCurrentMinimum AndAlso value <= My.MySettings.Default.ColdResistanceCurrentMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ColdResistanceCurrentMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance Current value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceCurrentMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance Current value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceCurrentMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the limit described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateLimit(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ColdResistanceMinimum AndAlso value <= My.MySettings.Default.ColdResistanceMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ColdResistanceMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance Limit value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance Limit value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the voltage limit described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateVoltageLimit(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ColdResistanceVoltageMinimum AndAlso value <= My.MySettings.Default.ColdResistanceVoltageMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ColdResistanceVoltageMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance Voltage Limit value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceVoltageMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Cold Resistance Voltage Limit value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ColdResistanceVoltageMaximum)
        End If
        Return affirmative
    End Function

End Class
