Imports isr.Core.Models
Imports isr.Core.NumericExtensions

''' <summary> Defines a measured cold resistance element. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-02-02, 2.1.3320.x. </para>
''' </remarks>
Public MustInherit Class ResistanceMeasureBase
    Inherits ViewModelTalkerBase

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._RandomNumberGenerator = New Random(DateTimeOffset.Now.Second)
    End Sub

    ''' <summary> Clones an existing measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Protected Sub New(ByVal value As ResistanceMeasureBase)
        Me.New()
        If value IsNot Nothing Then
            ' measurement
            Me._ResistanceDisplayFormat = value.ResistanceDisplayFormat
            Me._Outcome = value.Outcome
            Me._Resistance = value.Resistance
            Me._ResistanceCaption = value.ResistanceCaption
            Me._Reading = value.Reading
            Me._Voltage = value.Voltage
            Me._VoltageCaption = value.VoltageCaption
            Me._Timestamp = value.Timestamp
            ' Configuration
            Me._SourceMeasureUnit = value.SourceMeasureUnit
            Me._Aperture = value.Aperture
            Me._CurrentLevel = value.CurrentLevel
            Me._HighLimit = value.HighLimit
            Me._LowLimit = value.LowLimit
            Me._VoltageLimit = value.VoltageLimit
        End If
    End Sub

    ''' <summary> Copies the configuration described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub CopyConfiguration(ByVal value As ResistanceMeasureBase)
        If value IsNot Nothing Then
            Me._SourceMeasureUnit = value.SourceMeasureUnit
            Me._Aperture = value.Aperture
            Me._CurrentLevel = value.CurrentLevel
            Me._HighLimit = value.HighLimit
            Me._LowLimit = value.LowLimit
            Me._VoltageLimit = value.VoltageLimit
        End If
    End Sub

    ''' <summary> Copies the measurement described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub CopyMeasurement(ByVal value As ResistanceMeasureBase)
        If value IsNot Nothing Then
            Me._ResistanceDisplayFormat = value.ResistanceDisplayFormat
            Me._Outcome = value.Outcome
            Me._Resistance = value.Resistance
            Me._ResistanceCaption = value.ResistanceCaption
            Me._Reading = value.Reading
            Me._Voltage = value.Voltage
            Me._VoltageCaption = value.VoltageCaption
            Me._Timestamp = value.Timestamp
        End If
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Sets values to their known clear execution state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub DefineClearExecutionState()
        Me.LastReading = String.Empty
        Me.LastOutcome = String.Empty
        Me.Outcome = MeasurementOutcomes.None
        Me.Timestamp = DateTimeOffset.MinValue
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overridable Sub InitKnownState()
    End Sub

    ''' <summary> Sets the known preset state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub PresetKnownState()
    End Sub

    ''' <summary> Restores defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub ResetKnownState()
        Me.SourceMeasureUnit = My.MySettings.Default.SourceMeasureUnitDefault
        Me.ResistanceDisplayFormat = "0.000"
        Me.VoltageDisplayFormat = "0.0000"
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ColdResistanceBase"></see> value is equal to a
    ''' specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="other"> The cold resistance to compare to this object. </param>
    ''' <returns>
    ''' <c>True</c> if the other parameter is equal to the current
    ''' <see cref="T:ColdResistanceBase"></see> value;
    ''' otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Function Equals(ByVal other As ResistanceMeasureBase) As Boolean
        Return other IsNot Nothing AndAlso
            Me.Reading.Equals(other.Reading) AndAlso
            Me.ConfigurationEquals(other) AndAlso
            True
    End Function

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ResistanceMeasureBase"></see> configuration values
    ''' are equal to a specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="other"> The resistance to compare to this object. </param>
    ''' <returns>
    ''' <c>True</c> if the other parameter is equal to the current
    ''' <see cref="T:ResistanceMeasureBase"></see> value;
    ''' otherwise, <c>False</c>.
    ''' </returns>
    Public Overridable Function ConfigurationEquals(ByVal other As ResistanceMeasureBase) As Boolean
        Return other IsNot Nothing AndAlso
            String.Equals(Me.SourceMeasureUnit, other.SourceMeasureUnit) AndAlso
            Me.Aperture.Approximates(other.Aperture, 0.00001) AndAlso
            Me.CurrentLevel.Approximates(other.CurrentLevel, 0.000001) AndAlso
            Me.HighLimit.Approximates(other.HighLimit, 0.0001) AndAlso
            Me.LowLimit.Approximates(other.LowLimit, 0.0001) AndAlso
            Me.VoltageLimit.Approximates(other.VoltageLimit, 0.0001) AndAlso
            True
    End Function

    ''' <summary> Check throw if unequal configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="other"> The resistance to compare to this object. </param>
    Public Overridable Sub CheckThrowUnequalConfiguration(ByVal other As ResistanceMeasureBase)
        If other IsNot Nothing Then
            If Not Me.ConfigurationEquals(other) Then
                Dim format As String = "Unequal configuring--instrument {0} value of {1} is not {2}"
                If Not String.Equals(Me.SourceMeasureUnit, other.SourceMeasureUnit) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format, "Source Measure Unit", Me.SourceMeasureUnit, other.SourceMeasureUnit))
                ElseIf Not Me.Aperture.Approximates(other.Aperture, 0.00001) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format, "Aperture", Me.Aperture, other.Aperture))
                ElseIf Not Me.CurrentLevel.Approximates(other.CurrentLevel, 0.00001) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format, "Current Level", Me.CurrentLevel, other.CurrentLevel))
                ElseIf Not Me.HighLimit.Approximates(other.HighLimit, 0.00001) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format, "High Limit", Me.HighLimit, other.HighLimit))
                ElseIf Not Me.LowLimit.Approximates(other.LowLimit, 0.00001) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format, "Low Limit", Me.LowLimit, other.LowLimit))
                ElseIf Not Me.VoltageLimit.Approximates(other.VoltageLimit, 0.0001) Then
                    Throw New isr.Core.OperationFailedException(String.Format(Globalization.CultureInfo.CurrentCulture, format, "Voltage Limit", Me.VoltageLimit, other.VoltageLimit))
                Else
                    Debug.Assert(Not Debugger.IsAttached, "Failed logic")
                End If
            End If
        End If
    End Sub

#End Region

#Region " DISPLAY PROPERTIES "

    ''' <summary> The resistance display format. </summary>
    Private _ResistanceDisplayFormat As String

    ''' <summary> Gets or sets the display format for resistance. </summary>
    ''' <value> The display format. </value>
    Public Property ResistanceDisplayFormat() As String
        Get
            Return Me._ResistanceDisplayFormat
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResistanceDisplayFormat, StringComparison.OrdinalIgnoreCase) Then
                Me._ResistanceDisplayFormat = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The voltage display format. </summary>
    Private _VoltageDisplayFormat As String

    ''' <summary> Gets or sets the display format for Voltage. </summary>
    ''' <value> The display format. </value>
    Public Property VoltageDisplayFormat() As String
        Get
            Return Me._VoltageDisplayFormat
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.VoltageDisplayFormat, StringComparison.OrdinalIgnoreCase) Then
                Me._VoltageDisplayFormat = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " PARSER "

    ''' <summary> The last reading. </summary>
    Private _LastReading As String

    ''' <summary> Gets or sets the last reading. </summary>
    ''' <value> The last reading. </value>
    Public Property LastReading() As String
        Get
            Return Me._LastReading
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.LastReading, StringComparison.OrdinalIgnoreCase) Then
                Me._LastReading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last outcome. </summary>
    Private _LastOutcome As String

    ''' <summary> Gets or sets the last outcome. </summary>
    ''' <value> The last outcome. </value>
    Public Property LastOutcome() As String
        Get
            Return Me._LastOutcome
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.LastOutcome, StringComparison.OrdinalIgnoreCase) Then
                Me._LastOutcome = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The last status. </summary>
    Private _LastStatus As String

    ''' <summary> Gets or sets the last status. </summary>
    ''' <value> The last status. </value>
    Public Property LastStatus() As String
        Get
            Return Me._LastStatus
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.LastStatus, StringComparison.OrdinalIgnoreCase) Then
                Me._LastStatus = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#Region " RESISTANCE "

    ''' <summary> Sets the reading and outcome. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="resistanceReading"> Specifies the reading as received from the instrument. </param>
    ''' <param name="outcome">           . </param>
    Public Sub ParseReading(ByVal resistanceReading As String, ByVal outcome As MeasurementOutcomes)
        Dim value As Double = 0
        If String.IsNullOrWhiteSpace(resistanceReading) Then
            Me.Reading = String.Empty
            Me.Outcome = If(outcome <> MeasurementOutcomes.None, MeasurementOutcomes.MeasurementFailed Or outcome, outcome)
        Else
            Dim numberFormat As Globalization.NumberStyles = Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent
            Me.Reading = resistanceReading
            Me.Outcome = If(Double.TryParse(Me.Reading, numberFormat, Globalization.CultureInfo.InvariantCulture, value),
                If(value >= Me.LowLimit AndAlso value <= Me.HighLimit,
                    MeasurementOutcomes.PartPassed,
                    outcome Or MeasurementOutcomes.PartFailed),
                MeasurementOutcomes.MeasurementFailed Or MeasurementOutcomes.UnexpectedReadingFormat)
        End If
        Me.Resistance = value
        Me.Voltage = If(Me.CurrentLevel <> 0, value + Me.CurrentLevel, 0)
        Me.Timestamp = DateTimeOffset.Now
        Me.MeasurementAvailable = True
    End Sub

#End Region

#Region " VOLTAGE READING "

    ''' <summary> Applies the contact check outcome described by outcome. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="outcome"> . </param>
    Public Sub ApplyContactCheckOutcome(ByVal outcome As MeasurementOutcomes)
        Me.Outcome = Me.Outcome Or outcome
    End Sub

    ''' <summary> Sets the readings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="voltageReading"> Specifies the voltage reading. </param>
    ''' <param name="current">        Specifies the current level. </param>
    ''' <param name="outcome">        Specifies the outcome. </param>
    Public Sub ParseReading(ByVal voltageReading As String, ByVal current As Double, ByVal outcome As MeasurementOutcomes)
        Dim value As Double = 0
        If String.IsNullOrWhiteSpace(voltageReading) Then
            Me.Voltage = 0
            Me.Resistance = 0
            Me.Outcome = If(outcome <> MeasurementOutcomes.None, MeasurementOutcomes.MeasurementFailed Or outcome, outcome)
        Else
            Dim numberFormat As Globalization.NumberStyles = Globalization.NumberStyles.Number Or Globalization.NumberStyles.AllowExponent
            Me.Reading = voltageReading
            If Double.TryParse(Me.Reading, numberFormat, Globalization.CultureInfo.InvariantCulture, value) Then
                Me.Resistance = If(current <> 0, value / current, 0)
                Me.Outcome = If(value >= Me.LowLimit AndAlso value <= Me.HighLimit,
                    Me.Outcome Or MeasurementOutcomes.PartPassed,
                    Me.Outcome Or MeasurementOutcomes.PartFailed)
                Me.Voltage = value
            Else
                Me.Voltage = 0
                Me.Resistance = 0
                Me.Outcome = MeasurementOutcomes.MeasurementFailed Or MeasurementOutcomes.UnexpectedReadingFormat
            End If
        End If
        Me.Timestamp = DateTimeOffset.Now
        Me.MeasurementAvailable = True
    End Sub

#End Region

#End Region

#Region " MEASUREMENT PROPERTIES "

    ''' <summary> True if measurement available. </summary>
    Private _MeasurementAvailable As Boolean

    ''' <summary> Gets or sets (protected) the measurement available. </summary>
    ''' <value> The measurement available. </value>
    Public Property MeasurementAvailable As Boolean
        Get
            Return Me._MeasurementAvailable
        End Get
        Set(ByVal value As Boolean)
            Me._MeasurementAvailable = value
            Me.SyncNotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The outcome. </summary>
    Private _Outcome As MeasurementOutcomes

    ''' <summary> Gets or sets the measurement outcome. </summary>
    ''' <value> The outcome. </value>
    Public Property Outcome() As MeasurementOutcomes
        Get
            Return Me._Outcome
        End Get
        Set(ByVal value As MeasurementOutcomes)
            If Not value.Equals(Me.Outcome) Then
                Me._Outcome = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The reading. </summary>
    Private _Reading As String

    ''' <summary>
    ''' Gets or sets  or sets (protected) the reading.  When set, the value is converted to
    ''' resistance.
    ''' </summary>
    ''' <value> The reading. </value>
    Public Property Reading() As String
        Get
            Return Me._Reading
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.Reading, StringComparison.OrdinalIgnoreCase) Then
                Me._Reading = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The resistance. </summary>
    Private _Resistance As Double

    ''' <summary> Gets or sets (protected) the measured resistance. </summary>
    ''' <value> The resistance. </value>
    Public Property Resistance() As Double
        Get
            Return Me._Resistance
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.Resistance) Then
                Me._Resistance = value
                Me.NotifyPropertyChanged()
                If Me.Outcome = MeasurementOutcomes.None OrElse ((Me.Outcome And MeasurementOutcomes.MeasurementNotMade) <> 0) Then
                    Me.ResistanceCaption = String.Empty
                ElseIf String.IsNullOrWhiteSpace(Me.Reading) OrElse ((Me.Outcome And MeasurementOutcomes.MeasurementFailed) <> 0) Then
                    Me.ResistanceCaption = "#null#"
                Else
                    Me.ResistanceCaption = Me._Resistance.ToString(Me.ResistanceDisplayFormat, Globalization.CultureInfo.CurrentCulture)
                End If
            End If
        End Set
    End Property

    ''' <summary> The resistance caption. </summary>
    Private _ResistanceCaption As String

    ''' <summary> Gets or sets (protected) the measured resistance display caption. </summary>
    ''' <value> The resistance caption. </value>
    Public Property ResistanceCaption() As String
        Get
            Return Me._ResistanceCaption
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResistanceCaption, StringComparison.OrdinalIgnoreCase) Then
                Me._ResistanceCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Date/Time of the timestamp. </summary>
    Private _Timestamp As DateTimeOffset

    ''' <summary>
    ''' Gets or sets (protected) the measurement time stamp. Gets set when setting the reading or
    ''' resistance value.
    ''' </summary>
    ''' <value> The timestamp. </value>
    Public Property Timestamp() As DateTimeOffset
        Get
            Return Me._Timestamp
        End Get
        Protected Set(ByVal value As DateTimeOffset)
            If Not value.Equals(Me.Timestamp) Then
                Me._Timestamp = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The random number generator. </summary>
    Private ReadOnly _RandomNumberGenerator As Random

    ''' <summary> Generates a random reading. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The random reading. </returns>
    Public Function GenerateRandomReading() As Double
        Dim k As Double = 1000
        Return Me._RandomNumberGenerator.Next(CInt(k * Me.LowLimit), CInt(k * Me.HighLimit)) / k
    End Function

    ''' <summary> Emulates a reading. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub EmulateReading()
        Me.LastReading = CStr(Me.GenerateRandomReading())
        Me.LastOutcome = "0"
    End Sub

    ''' <summary> The voltage. </summary>
    Private _Voltage As Double

    ''' <summary> Gets or sets the measured Voltage. </summary>
    ''' <value> The Voltage. </value>
    Public Property Voltage() As Double
        Get
            Return Me._Voltage
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.Voltage) Then
                Me._Voltage = value
                Me.NotifyPropertyChanged()
                If (Me.Outcome And MeasurementOutcomes.MeasurementNotMade) <> 0 Then
                    Me.VoltageCaption = String.Empty
                ElseIf String.IsNullOrWhiteSpace(Me.Reading) OrElse ((Me.Outcome And MeasurementOutcomes.MeasurementFailed) <> 0) Then
                    Me.VoltageCaption = "#null#"
                Else
                    Me.VoltageCaption = Me._Voltage.ToString(Me.VoltageDisplayFormat, Globalization.CultureInfo.CurrentCulture)
                End If
            End If
        End Set
    End Property

    ''' <summary> The voltage caption. </summary>
    Private _VoltageCaption As String

    ''' <summary> Gets or sets the measured Voltage display caption. </summary>
    ''' <value> The Voltage caption. </value>
    Public Property VoltageCaption() As String
        Get
            Return Me._VoltageCaption
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.VoltageCaption, StringComparison.OrdinalIgnoreCase) Then
                Me._VoltageCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CONFIGURATION PROPERTIES "

    ''' <summary> Source measure unit. </summary>
    Private _SourceMeasureUnit As String

    ''' <summary> Gets or sets the cached Source Measure Unit. </summary>
    ''' <value> The Source Measure Unit, e.g., 'smua' or 'smub'. </value>
    Public Overridable Property SourceMeasureUnit As String
        Get
            Return Me._SourceMeasureUnit
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.SourceMeasureUnit) Then
                Me._SourceMeasureUnit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The aperture. </summary>
    Private _Aperture As Double

    ''' <summary>
    ''' Gets or sets the integration period in number of power line cycles for measuring the cold
    ''' resistance.
    ''' </summary>
    ''' <value> The aperture. </value>
    Public Property Aperture() As Double
        Get
            Return Me._Aperture
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.Aperture) Then
                Me._Aperture = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The current level. </summary>
    Private _CurrentLevel As Double

    ''' <summary> Gets or sets the current level. </summary>
    ''' <value> The current level. </value>
    Public Property CurrentLevel() As Double
        Get
            Return Me._CurrentLevel
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.CurrentLevel) Then
                Me._CurrentLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The high limit. </summary>
    Private _HighLimit As Double

    ''' <summary>
    ''' Gets or sets the high limit for determining measurement pass fail condition.
    ''' </summary>
    ''' <value> The high limit. </value>
    Public Property HighLimit() As Double
        Get
            Return Me._HighLimit
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.HighLimit) Then
                Me._HighLimit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The low limit. </summary>
    Private _LowLimit As Double

    ''' <summary>
    ''' Gets or sets the low limit for determining measurement pass fail condition.
    ''' </summary>
    ''' <value> The low limit. </value>
    Public Property LowLimit() As Double
        Get
            Return Me._LowLimit
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.LowLimit) Then
                Me._LowLimit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The voltage limit. </summary>
    Private _VoltageLimit As Double

    ''' <summary> Gets or sets the voltage limit. </summary>
    ''' <value> The voltage limit. </value>
    Public Property VoltageLimit() As Double
        Get
            Return Me._VoltageLimit
        End Get
        Set(ByVal value As Double)
            If Not value.Equals(Me.VoltageLimit) Then
                Me._VoltageLimit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

End Class

