
Partial Public Class ShuntResistance

    ''' <summary> Validates the current level or range described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateCurrent(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ShuntResistanceCurrentMinimum AndAlso value <= My.MySettings.Default.ShuntResistanceCurrentMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ShuntResistanceCurrentMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Shunt Resistance Current value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ShuntResistanceCurrentMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Shunt Resistance Current value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ShuntResistanceCurrentMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the limit described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateLimit(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ShuntResistanceMinimum AndAlso value <= My.MySettings.Default.ShuntResistanceMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ShuntResistanceMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Shunt Resistance Limit value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ShuntResistanceMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Shunt Resistance Limit value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ShuntResistanceMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the voltage limit described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateVoltageLimit(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ShuntResistanceVoltageMinimum AndAlso value <= My.MySettings.Default.ShuntResistanceVoltageMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ShuntResistanceVoltageMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Shunt Resistance Voltage Limit value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ShuntResistanceVoltageMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Shunt Resistance Voltage Limit value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ShuntResistanceVoltageMaximum)
        End If
        Return affirmative
    End Function
End Class
