
Partial Public Class ThermalTransient

    ''' <summary> Validates the allowed voltage change described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateVoltageChange(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientVoltageMinimum AndAlso value <= My.MySettings.Default.ThermalTransientVoltageMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientVoltageMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Voltage Change value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientVoltageMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Voltage Change value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientVoltageMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the aperture described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateAperture(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientApertureMinimum AndAlso value <= My.MySettings.Default.ThermalTransientApertureMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientApertureMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient aperture value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientApertureMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient aperture value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientApertureMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the current level described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateCurrentLevel(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientCurrentMinimum AndAlso value <= My.MySettings.Default.ThermalTransientCurrentMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientCurrentMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient CurrentLevel value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientCurrentMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Current Level value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientCurrentMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the limit described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateLimit(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientVoltageChangeMinimum AndAlso value <= My.MySettings.Default.ThermalTransientVoltageChangeMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientVoltageMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Limit value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientVoltageMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Limit value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientVoltageMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the median filter size described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateMedianFilterSize(value As Integer, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientMedianFilterLengthMinimum AndAlso
            value <= My.MySettings.Default.ThermalTransientMedianFilterLengthMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientTracePointsMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Median Filter Length of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientMedianFilterLengthMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Median Filter Length value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientMedianFilterLengthMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the Post Transient Delay described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidatePostTransientDelay(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.PostTransientDelayMinimum AndAlso value <= My.MySettings.Default.PostTransientDelayMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.PostTransientDelayMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Post Transient Delay value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.PostTransientDelayMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Post Transient Delay value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.PostTransientDelayMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the Duration described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="samplingInterval"> The sampling interval. </param>
    ''' <param name="tracePoints">      The trace points. </param>
    ''' <param name="details">          [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidatePulseWidth(ByVal samplingInterval As Double, ByVal tracePoints As Integer, ByRef details As String) As Boolean
        Dim pulseWidth As Double = samplingInterval * tracePoints
        Dim affirmative As Boolean = ThermalTransient.ValidateSamplingInterval(samplingInterval, details) AndAlso
                ThermalTransient.ValidateTracePoints(tracePoints, details) AndAlso
                pulseWidth >= My.MySettings.Default.ThermalTransientDurationMinimum AndAlso pulseWidth <= My.MySettings.Default.ThermalTransientDurationMaximum
        If affirmative Then
            details = String.Empty
        ElseIf Not String.IsNullOrWhiteSpace(details) Then
            ' we have details. nothing to do.
        ElseIf pulseWidth < My.MySettings.Default.ThermalTransientDurationMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Pulse Width of {0}s is lower than the minimum of {1}s.",
                                        pulseWidth, My.MySettings.Default.ThermalTransientDurationMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Pulse Width of {0}s is higher than the maximum of {1}s.",
                                        pulseWidth, My.MySettings.Default.ThermalTransientDurationMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the sampling interval described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateSamplingInterval(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientSamplingIntervalMinimum AndAlso value <= My.MySettings.Default.ThermalTransientSamplingIntervalMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientSamplingIntervalMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Sample Interval value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientSamplingIntervalMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Sample Interval value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientSamplingIntervalMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the trace points described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateTracePoints(value As Integer, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientTracePointsMinimum AndAlso value <= My.MySettings.Default.ThermalTransientTracePointsMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientTracePointsMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Trace Points value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientTracePointsMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Trace Points value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientTracePointsMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the voltage limit described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateVoltageLimit(ByVal value As Double, ByRef details As String) As Boolean
        details = String.Empty
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalTransientVoltageMinimum AndAlso value <= My.MySettings.Default.ThermalTransientVoltageMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalTransientVoltageMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Voltage Limit value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientVoltageMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Transient Voltage Limit value of {0} is high than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalTransientVoltageMaximum)
        End If
        Return affirmative
    End Function

    ''' <summary> Validates the transient voltage limit described by details. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">                 The value. </param>
    ''' <param name="maximumColdResistance"> The maximum cold resistance. </param>
    ''' <param name="currentLevel">          The current level. </param>
    ''' <param name="allowedVoltageChange">  The allowed voltage change. </param>
    ''' <param name="details">               [in,out] The details. </param>
    ''' <returns>
    ''' <c>True</c> if the voltage limit is in rage; <c>False</c> is the limit is too low.
    ''' </returns>
    Public Shared Function ValidateVoltageLimit(ByVal value As Double, ByVal maximumColdResistance As Double,
                                                ByVal currentLevel As Double, ByVal allowedVoltageChange As Double,
                                                ByRef details As String) As Boolean
        Dim expectedMaximumVoltage As Double = maximumColdResistance * currentLevel + allowedVoltageChange
        details = String.Empty
        Dim affirmative As Boolean = ThermalTransient.ValidateVoltageChange(allowedVoltageChange, details) AndAlso
                ThermalTransient.ValidateCurrentLevel(currentLevel, details) AndAlso
                ThermalTransient.ValidateVoltageLimit(value, details) AndAlso value >= expectedMaximumVoltage
        If affirmative OrElse Not String.IsNullOrWhiteSpace(details) Then
            ' we have details. nothing to do.
        ElseIf expectedMaximumVoltage < expectedMaximumVoltage Then
            details = $"A Thermal transient voltage limit of {value} volts is too low;. Based on the cold resistance high limit and thermal transient current level and voltage change, the voltage might reach {expectedMaximumVoltage} volts."
        End If
        Return affirmative
    End Function

#Region " ESTIMATOR "

    ''' <summary> Validates the ThermalCoefficient described by value. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value">   The value. </param>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> <c>True</c> if value is in range; otherwise, <c>False</c>. </returns>
    Public Shared Function ValidateThermalCoefficient(ByVal value As Double, ByRef details As String) As Boolean
        Dim affirmative As Boolean = value >= My.MySettings.Default.ThermalCoefficientMinimum AndAlso value <= My.MySettings.Default.ThermalCoefficientMaximum
        If affirmative Then
            details = String.Empty
        ElseIf value < My.MySettings.Default.ThermalCoefficientMinimum Then
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Coefficient value of {0} is lower than the minimum of {1}.",
                                        value, My.MySettings.Default.ThermalCoefficientMinimum)
        Else
            details = String.Format(Globalization.CultureInfo.CurrentCulture,
                                        "Thermal Coefficient value of {0} is higher than the maximum of {1}.",
                                        value, My.MySettings.Default.ThermalCoefficientMaximum)
        End If
        Return affirmative
    End Function

#End Region

End Class
