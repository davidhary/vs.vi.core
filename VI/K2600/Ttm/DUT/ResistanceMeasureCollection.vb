Imports isr.Core

''' <summary> Collection of resistance measures. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Class ResistanceMeasureCollection
    Inherits ObjectModel.Collection(Of ResistanceMeasureBase)
    Implements ITalker

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary>
    ''' Sets values to their clear exception state. Clears the queues and resets all registers to
    ''' zero. Sets the subsystem properties to the following CLS default values:<para>
    ''' </para>
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub DefineClearExecutionState()
        For Each element As ResistanceMeasureBase In Me.Items
            element.DefineClearExecutionState()
        Next
    End Sub

    ''' <summary>
    ''' Performs a reset and additional custom setting for the subsystem:<para>
    ''' </para>
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub InitKnownState()
        For Each element As ResistanceMeasureBase In Me.Items
            element.InitKnownState()
        Next
    End Sub

    ''' <summary>
    ''' Gets subsystem to the following default system preset values:<para>
    ''' </para>
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub PresetKnownState()
        For Each element As ResistanceMeasureBase In Me.Items
            element.PresetKnownState()
        Next
    End Sub

    ''' <summary>
    ''' Restore member properties to the following RST or System Preset values:<para>
    ''' </para>
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ResetKnownState()
        For Each element As ResistanceMeasureBase In Me.Items
            element.ResetKnownState()
        Next
    End Sub

#End Region

End Class
