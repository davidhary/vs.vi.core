﻿Imports System.ComponentModel

Imports isr.Core.EnumExtensions
Imports isr.Core.NumericExtensions

''' <summary> Thermal transient. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-23 </para>
''' </remarks>
Public Class ThermalTransient
    Inherits ThermalTransientBase

    Implements System.ICloneable

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clones an existing measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As ThermalTransient)
        MyBase.New(value)
        If value IsNot Nothing Then
        End If
    End Sub

    ''' <summary> Creates a new object that is a copy of the current instance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A new object that is a copy of this instance. </returns>
    Public Function Clone() As Object Implements System.ICloneable.Clone
        Return New ThermalTransient(Me)
    End Function

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets values to their known clear execution state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineClearExecutionState()
        MyBase.DefineClearExecutionState()
        Me.Asymptote = New Double?
        Me.TimeConstant = New Double?
        Me.EstimatedVoltage = New Double?
        Me.CorrelationCoefficient = New Double?
        Me.StandardError = New Double?
        Me.Iterations = New Integer?
        Me.OptimizationOutcome = New OptimizationOutcome?
    End Sub

#End Region

#Region " MODEL VALUES "

    ''' <summary> The time constant. </summary>
    Private _TimeConstant As Double?

    ''' <summary> Gets or sets the time constant. </summary>
    ''' <value> The time constant. </value>
    Public Property TimeConstant As Double?
        Get
            Return Me._TimeConstant
        End Get
        Set(value As Double?)
            If Me.TimeConstant.Differs(value, 0.000000001) Then
                Me._TimeConstant = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the time constant caption. </summary>
    ''' <value> The time constant caption. </value>
    Public ReadOnly Property TimeConstantCaption As String
        Get
            Return If(Me.TimeConstant.HasValue, (1000 * Me.TimeConstant.Value).ToString("G4"), String.Empty)
        End Get
    End Property

    ''' <summary> The asymptote. </summary>
    Private _Asymptote As Double?

    ''' <summary> Gets or sets the asymptote. </summary>
    ''' <value> The asymptote. </value>
    Public Property Asymptote As Double?
        Get
            Return Me._Asymptote
        End Get
        Set(value As Double?)
            If Me.Asymptote.Differs(value, 0.000000001) Then
                Me._Asymptote = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the asymptote caption. </summary>
    ''' <value> The asymptote caption. </value>
    Public ReadOnly Property AsymptoteCaption As String
        Get
            Return If(Me.Asymptote.HasValue, (1000 * Me.Asymptote.Value).ToString("G4"), String.Empty)
        End Get
    End Property

    ''' <summary> The estimated voltage. </summary>
    Private _EstimatedVoltage As Double?

    ''' <summary> Gets or sets the estimated voltage. </summary>
    ''' <value> The estimated voltage. </value>
    Public Property EstimatedVoltage As Double?
        Get
            Return Me._EstimatedVoltage
        End Get
        Set(value As Double?)
            If Me.EstimatedVoltage.Differs(value, 0.000000001) Then
                Me._EstimatedVoltage = value
                Me.NotifyPropertyChanged()
            End If
        End Set

    End Property

    ''' <summary> Gets the estimated voltage caption. </summary>
    ''' <value> The estimated voltage caption. </value>
    Public ReadOnly Property EstimatedVoltageCaption As String
        Get
            Return If(Me.EstimatedVoltage.HasValue, (1000 * Me.EstimatedVoltage.Value).ToString("G4"), String.Empty)
        End Get
    End Property

    ''' <summary> The correlation coefficient. </summary>
    Private _CorrelationCoefficient As Double?

    ''' <summary> Gets or sets the correlation coefficient. </summary>
    ''' <value> The correlation coefficient. </value>
    Public Property CorrelationCoefficient As Double?
        Get
            Return Me._CorrelationCoefficient
        End Get
        Set(value As Double?)
            If Me.CorrelationCoefficient.Differs(value, 0.000001) Then
                Me._CorrelationCoefficient = value
                Me.NotifyPropertyChanged()
            End If

        End Set
    End Property

    ''' <summary> Gets the correlation coefficient caption. </summary>
    ''' <value> The correlation coefficient caption. </value>
    Public ReadOnly Property CorrelationCoefficientCaption As String
        Get
            Return If(Me.CorrelationCoefficient.HasValue, Me.CorrelationCoefficient.Value.ToString("G4"), String.Empty)
        End Get
    End Property

    ''' <summary> The standard error. </summary>
    Private _StandardError As Double?

    ''' <summary> Gets or sets the standard error. </summary>
    ''' <value> The standard error. </value>
    Public Property StandardError As Double?
        Get
            Return Me._StandardError
        End Get
        Set(value As Double?)
            If Me.StandardError.Differs(value, 0.000001) Then
                Me._StandardError = value
                Me.NotifyPropertyChanged()

            End If
        End Set
    End Property

    ''' <summary> Gets the standard error caption. </summary>
    ''' <value> The standard error caption. </value>
    Public ReadOnly Property StandardErrorCaption As String
        Get
            Return If(Me.StandardError.HasValue, (1000 * Me.StandardError.Value).ToString("G4"), String.Empty)
        End Get
    End Property

    ''' <summary> The iterations. </summary>
    Private _Iterations As Integer?

    ''' <summary> Gets or sets the iterations. </summary>
    ''' <value> The iterations. </value>
    Public Property Iterations As Integer?
        Get
            Return Me._Iterations
        End Get
        Set(value As Integer?)
            If Not Nullable.Equals(value, Me.Iterations) Then
                Me._Iterations = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the iterations caption. </summary>
    ''' <value> The iterations caption. </value>
    Public ReadOnly Property IterationsCaption As String
        Get
            Return If(Me.Iterations.HasValue, Me.Iterations.Value.ToString(), String.Empty)
        End Get
    End Property

    ''' <summary> The optimization outcome. </summary>
    Private _OptimizationOutcome As OptimizationOutcome?

    ''' <summary> Gets or sets the optimization outcome. </summary>
    ''' <value> The optimization outcome. </value>
    Public Property OptimizationOutcome As OptimizationOutcome?
        Get
            Return Me._OptimizationOutcome
        End Get
        Set(value As OptimizationOutcome?)
            If Not Nullable.Equals(value, Me.OptimizationOutcome) Then
                Me._OptimizationOutcome = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the optimization outcome caption. </summary>
    ''' <value> The optimization outcome caption. </value>
    Public ReadOnly Property OptimizationOutcomeCaption As String
        Get
            Return If(Me.OptimizationOutcome.HasValue, Me.OptimizationOutcome.Value.ToString, String.Empty)
        End Get
    End Property

    ''' <summary> Gets information describing the optimization outcome. </summary>
    ''' <value> Information describing the optimization outcome. </value>
    Public ReadOnly Property OptimizationOutcomeDescription As String
        Get
            Return If(Me.OptimizationOutcome.HasValue, Me.OptimizationOutcome.Value.Description, String.Empty)
        End Get
    End Property

#End Region

End Class

''' <summary> Values that represent Optimization Outcome. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum OptimizationOutcome

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not Specified")>
    None

    ''' <summary> An enum constant representing the exhausted option. </summary>
    <Description("Count Out--reached maximum number of iterations")>
    Exhausted

    ''' <summary> An enum constant representing the optimized option. </summary>
    <Description("Optimized--within the objective function precision range")>
    Optimized

    ''' <summary> An enum constant representing the converged option. </summary>
    <Description("Converged--within the argument values convergence range")>
    Converged
End Enum