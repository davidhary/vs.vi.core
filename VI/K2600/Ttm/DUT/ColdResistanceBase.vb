''' <summary> Defines a measured cold resistance element. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2009-02-02, 2.1.3320.x. </para>
''' </remarks>
Public MustInherit Class ColdResistanceBase
    Inherits ResistanceMeasureBase
    Implements System.IEquatable(Of ColdResistanceBase)

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Clones an existing measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Protected Sub New(ByVal value As ColdResistanceBase)
        MyBase.New(value)
    End Sub

#End Region

#Region " PRESET "

    ''' <summary> Restores defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.Aperture = My.MySettings.Default.ColdResistanceApertureDefault
        Me.CurrentLevel = My.MySettings.Default.ColdResistanceCurrentLevelDefault
        Me.LowLimit = My.MySettings.Default.ColdResistanceLowLimitDefault
        Me.HighLimit = My.MySettings.Default.ColdResistanceHighLimitDefault
        Me.VoltageLimit = My.MySettings.Default.ColdResistanceVoltageLimitDefault
        isr.Core.ApplianceBase.DoEvents()
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ColdResistanceBase"></see> value is equal to a
    ''' specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="obj"> An object. </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="obj" /> and this instance are the same type and represent the
    ''' same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, ColdResistanceBase))
    End Function

    ''' <summary>
    ''' Indicates whether the current <see cref="T:ColdResistanceBase"></see> value is equal to a
    ''' specified object.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="other"> The cold resistance to compare to this object. </param>
    ''' <returns>
    ''' <c>True</c> if the other parameter is equal to the current
    ''' <see cref="T:ColdResistanceBase"></see> value;
    ''' otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Function Equals(ByVal other As ColdResistanceBase) As Boolean Implements System.IEquatable(Of ColdResistanceBase).Equals
        Return other IsNot Nothing AndAlso
            Me.Reading.Equals(other.Reading) AndAlso
            Me.ConfigurationEquals(other) AndAlso
            True
    End Function

    ''' <summary> Returns a hash code for this instance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A hash code for this object. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return MyBase.GetHashCode
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As ColdResistanceBase, ByVal right As ColdResistanceBase) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="left">  The left. </param>
    ''' <param name="right"> The right. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As ColdResistanceBase, ByVal right As ColdResistanceBase) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

#End Region

End Class
