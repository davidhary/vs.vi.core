Imports isr.Core.StackTraceExtensions
Imports isr.Core.NumericExtensions

''' <summary>
''' Defines the contract that must be implemented by Thermal Transient Meter Subsystems.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-12-12, 3.0.5093. </para>
''' </remarks>
Public MustInherit Class MeterSubsystemBase
    Inherits SubsystemPlusStatusBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="SubsystemPlusStatusBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem"> The status subsystem. </param>
   Protected Sub New(ByVal statusSubsystem As VI.StatusSubsystemBase)
        MyBase.New(statusSubsystem)
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets the known clear state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineClearExecutionState()
        Me.PublishVerbose($"Clearing {Me.EntityName};. ")
        MyBase.DefineClearExecutionState()
        Me.LastReading = String.Empty
        Me.LastOutcome = String.Empty
        Me.LastMeasurementStatus = String.Empty
        Me.MeasurementEventCondition = 0
        Me.MeasurementAvailable = False
        ' this is not required -- is done internally 
        ' Me.Session.WriteLine("{0}:clear()", Me.EntityName)
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Sets the known reset (default) state. </summary>
    ''' <remarks>
    ''' Reads instrument defaults, applies the values to the instrument and reads them back as
    ''' configuration values. This ensures that the instrument state is mirrored in code.
    ''' </remarks>
    Public Overrides Sub DefineKnownResetState()
        MyBase.DefineKnownResetState()
        Me.PublishVerbose($"Resetting {Me.EntityName};. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ReadInstrumentDefaults()
        Me.ApplyInstrumentDefaults()
        ' this causes a problem requiring the make an initial resistance measurement before 
        ' the thermal transient.
        ' Me.Session.WriteLine("{0}:init()", Me.EntityName)
        Me.QueryConfiguration()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

#End Region

#Region " DEVICE UNDER TEST RESISTANCE "

    ''' <summary>
    ''' Gets or sets the <see cref="ResistanceMeasureBase">part resistance element</see>.
    ''' </summary>
    ''' <value> The cold resistance. </value>
    Public MustOverride ReadOnly Property Resistance As ResistanceMeasureBase

#End Region

#Region " ENTITY "

    ''' <summary> Name of the global. </summary>
    Public Const GlobalName As String = "_G"

    ''' <summary> Name of the thermal transient base entity. </summary>
    Public Const ThermalTransientBaseEntityName As String = "_G.ttm"

    ''' <summary> Name of the final resistance entity. </summary>
    Public Const FinalResistanceEntityName As String = "_G.ttm.fr"

    ''' <summary> Name of the initial resistance entity. </summary>
    Public Const InitialResistanceEntityName As String = "_G.ttm.ir"

    ''' <summary> Name of the thermal transient entity. </summary>
    Public Const ThermalTransientEntityName As String = "_G.ttm.tr"

    ''' <summary> Name of the thermal transient estimator entity. </summary>
    Public Const ThermalTransientEstimatorEntityName As String = "_G.ttm.est"

    ''' <summary> The meter entity. </summary>
    Private _MeterEntity As ThermalTransientMeterEntity

    ''' <summary> Gets or sets the meter entity. </summary>
    ''' <value> The meter entity. </value>
    Public Property MeterEntity As ThermalTransientMeterEntity
        Get
            Return Me._MeterEntity
        End Get
        Set(ByVal value As ThermalTransientMeterEntity)
            If Not value.Equals(Me.MeterEntity) Then
                Me._MeterEntity = value
                Me.NotifyPropertyChanged()
                Select Case value
                    Case ThermalTransientMeterEntity.FinalResistance
                        Me.EntityName = MeterSubsystemBase.FinalResistanceEntityName
                        Me.BaseEntityName = MeterSubsystemBase.ThermalTransientBaseEntityName
                        Me.DefaultsName = "_G.ttm.coldResistance.Defaults"
                    Case ThermalTransientMeterEntity.InitialResistance
                        Me.EntityName = MeterSubsystemBase.InitialResistanceEntityName
                        Me.BaseEntityName = MeterSubsystemBase.ThermalTransientBaseEntityName
                        Me.DefaultsName = "_G.ttm.coldResistance.Defaults"
                    Case ThermalTransientMeterEntity.Transient
                        Me.EntityName = MeterSubsystemBase.ThermalTransientEntityName
                        Me.BaseEntityName = MeterSubsystemBase.ThermalTransientBaseEntityName
                        Me.DefaultsName = "_G.ttm.trace.Defaults"
                    Case ThermalTransientMeterEntity.Estimator
                        Me.EntityName = MeterSubsystemBase.ThermalTransientEstimatorEntityName
                        Me.BaseEntityName = MeterSubsystemBase.ThermalTransientBaseEntityName
                        Me.DefaultsName = "_G.ttm.estimator.Defaults"
                    Case ThermalTransientMeterEntity.Shunt
                        Me.EntityName = MeterSubsystemBase.GlobalName
                        Me.BaseEntityName = MeterSubsystemBase.GlobalName
                        Me.DefaultsName = String.Empty
                    Case ThermalTransientMeterEntity.None
                        Me.EntityName = MeterSubsystemBase.ThermalTransientBaseEntityName
                        Me.BaseEntityName = MeterSubsystemBase.GlobalName
                        Me.DefaultsName = String.Empty
                    Case Else
                        Debug.Assert(Not Debugger.IsAttached, "Unhandled case")
                End Select
            End If
        End Set
    End Property

    ''' <summary> Name of the defaults. </summary>
    Private _DefaultsName As String

    ''' <summary> Gets or sets the Defaults name. </summary>
    ''' <value> The Source Measure Unit. </value>
    Public Property DefaultsName As String
        Get
            Return Me._DefaultsName
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.DefaultsName) Then
                Me._DefaultsName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the entity. </summary>
    Private _EntityName As String

    ''' <summary> Gets or sets the entity name. </summary>
    ''' <value> The Source Measure Unit. </value>
    Public Property EntityName As String
        Get
            Return Me._EntityName
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.EntityName) Then
                Me._EntityName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the base entity. </summary>
    Private _BaseEntityName As String

    ''' <summary> Gets or sets the Base Entity name. </summary>
    ''' <value> The Meter entity. </value>
    Public Property BaseEntityName As String
        Get
            Return Me._BaseEntityName
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.BaseEntityName) Then
                Me._BaseEntityName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " SOURCE MEASURE UNIT "

    ''' <summary> Queries if a given source measure unit exists. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sourceMeasureUnitName"> Name of the source measure unit, e.g., 'smua' or 'smub'. </param>
    ''' <returns> <c>True</c> if the source measure unit exists; otherwise <c>False</c> </returns>
    Public Function SourceMeasureUnitExists(ByVal sourceMeasureUnitName As String) As Boolean
        Me.Session.MakeTrueFalseReplyIfEmpty(String.Equals(sourceMeasureUnitName, "smua", StringComparison.OrdinalIgnoreCase))
        Return Not Me.Session.IsNil(String.Format("_G.localnode.{0}", sourceMeasureUnitName))
    End Function

    ''' <summary> Requires source measure unit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' <c>True</c> if this entity requires the assignment of the source measure unit.
    ''' </returns>
    Public Function RequiresSourceMeasureUnit() As Boolean
        Return Me.MeterEntity = ThermalTransientMeterEntity.FinalResistance OrElse
            Me.MeterEntity = ThermalTransientMeterEntity.InitialResistance OrElse
            Me.MeterEntity = ThermalTransientMeterEntity.Transient
    End Function

    ''' <summary> Source measure unit. </summary>
    Private _SourceMeasureUnit As String

    ''' <summary> Gets or sets the cached Source Measure Unit. </summary>
    ''' <value> The Source Measure Unit. </value>
    Public Overridable Property SourceMeasureUnit As String
        Get
            Return Me._SourceMeasureUnit
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.SourceMeasureUnit) Then
                Me._SourceMeasureUnit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Queries the Source Measure Unit. Also sets the <see cref="SourceMeasureUnit">Source Measure
    ''' Unit</see>.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <returns> The Source Measure Unit. </returns>
    Public Function QuerySourceMeasureUnit() As String
        If Me.RequiresSourceMeasureUnit Then
            Me.Session.MakeEmulatedReplyIfEmpty(True)
            If Me.Session.IsStatementTrue("{0}.smuI==_G.smua", Me.EntityName) Then
                Me.SourceMeasureUnit = "smua"
            ElseIf Me.Session.IsStatementTrue("{0}.smuI==_G.smub", Me.EntityName) Then
                Me.SourceMeasureUnit = "smub"
            Else
                Me.SourceMeasureUnit = "unknown"
                Throw New isr.Core.OperationFailedException($"Failed reading Source Measure Unit;. {Me.EntityName}.smuI is neither smua or smub.")
            End If
        End If
        Return Me.SourceMeasureUnit
    End Function

    ''' <summary> Programs the Source Measure Unit. Does not read back from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="value"> the Source Measure Unit, e.g., 'smua' or 'smub'. </param>
    ''' <returns> The Source Measure Unit, e.g., 'smua' or 'smub'. </returns>
    Public Function WriteSourceMeasureUnit(ByVal value As String) As String
        If Me.RequiresSourceMeasureUnit Then
            If Me.SourceMeasureUnitExists(value) Then
                Me.Session.WriteLine("{0}:currentSourceChannelSetter('{1}')", Me.EntityName, value)
            Else
                Throw New ArgumentException("This source measure unit name is invalid.", NameOf(value))
            End If
        End If
        Me.SourceMeasureUnit = value
        Return Me.SourceMeasureUnit
    End Function

    ''' <summary> Writes and reads back the Source Measure Unit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> the Source Measure Unit, e.g., 'smua' or 'smub'. </param>
    ''' <returns> The Source Measure Unit, e.g., 'smua' or 'smub'. </returns>
    Public Function ApplySourceMeasureUnit(ByVal value As String) As String
        Me.WriteSourceMeasureUnit(value)
        Return Me.QuerySourceMeasureUnit()
    End Function

#End Region

#Region " APERTURE "

    ''' <summary> The Aperture. </summary>
    Private _Aperture As Double?

    ''' <summary> Gets or sets the cached Source Aperture. </summary>
    ''' <value> The Source Aperture. </value>
    Public Overridable Property Aperture As Double?
        Get
            Return Me._Aperture
        End Get
        Protected Set(ByVal value As Double?)
            Me.Resistance.Aperture = If(value, 0)
            If Me.Aperture.Differs(value, 0.000001) Then
                Me._Aperture = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Aperture. </summary>
    ''' <remarks>
    ''' This command set the immediate output Aperture. The value is in Amperes. The immediate
    ''' Aperture is the output current setting. At *RST, the current values = 0.
    ''' </remarks>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Source Aperture. </returns>
    Public Function ApplyAperture(ByVal value As Double) As Double?
        Me.WriteAperture(value)
        Return Me.QueryAperture
    End Function

    ''' <summary> Queries the Aperture. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Aperture or none if unknown. </returns>
    Public Function QueryAperture() As Double?
        Const printFormat As Decimal = 7.4D
        Me.Aperture = Me.Session.QueryPrint(Me.Aperture.GetValueOrDefault(1 / 60), printFormat, "{0}.aperture", Me.EntityName)
        Return Me.Aperture
    End Function

    ''' <summary> Writes the source Aperture without reading back the value from the device. </summary>
    ''' <remarks>
    ''' This command sets the immediate output Aperture. The value is in Amperes. The immediate
    ''' Aperture is the output current setting. At *RST, the current values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Source Aperture. </returns>
    Public Overridable Function WriteAperture(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Me.MeterEntity = ThermalTransientMeterEntity.Transient Then
            If Not ThermalTransient.ValidateAperture(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        Else
            If Not ColdResistance.ValidateAperture(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        End If
        Me.Session.WriteLine("{0}:apertureSetter({1})", Me.EntityName, value)
        Me.Aperture = value
        Return Me.Aperture
    End Function

#End Region

#Region " CURRENT LEVEL "

    ''' <summary> The Current Level. </summary>
    Private _CurrentLevel As Double?

    ''' <summary> Gets or sets the cached Source Current Level. </summary>
    ''' <value> The Source Current Level. </value>
    Public Overridable Property CurrentLevel As Double?
        Get
            Return Me._CurrentLevel
        End Get
        Protected Set(ByVal value As Double?)
            Me.Resistance.CurrentLevel = If(value, 0)

            If Me.CurrentLevel.Differs(value, 0.000000001) Then
                Me._CurrentLevel = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Current Level. </summary>
    ''' <remarks>
    ''' This command set the immediate output Current Level. The value is in Amperes. The immediate
    ''' Current Level is the output current setting. At *RST, the current values = 0.
    ''' </remarks>
    ''' <param name="value"> The Current Level. </param>
    ''' <returns> The Source Current Level. </returns>
    Public Function ApplyCurrentLevel(ByVal value As Double) As Double?
        Me.WriteCurrentLevel(value)
        Return Me.QueryCurrentLevel
    End Function

    ''' <summary> Queries the Current Level. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Current Level or none if unknown. </returns>
    Public Function QueryCurrentLevel() As Double?
        Const printFormat As Decimal = 9.6D
        Me.CurrentLevel = Me.Session.QueryPrint(Me.CurrentLevel.GetValueOrDefault(0.27), printFormat, "{0}.level", Me.EntityName)
        Return Me.CurrentLevel
    End Function

    ''' <summary>
    ''' Writes the source Current Level without reading back the value from the device.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the immediate output Current Level. The value is in Amperes. The immediate
    ''' CurrentLevel is the output current setting. At *RST, the current values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Current Level. </param>
    ''' <returns> The Source Current Level. </returns>
    Public Function WriteCurrentLevel(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Me.MeterEntity = ThermalTransientMeterEntity.Transient Then
            If Not ThermalTransient.ValidateCurrentLevel(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        Else
            If Not ColdResistance.ValidateCurrentLevel(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        End If
        Me.Session.WriteLine("{0}:levelSetter({1})", Me.EntityName, value)
        Me.CurrentLevel = value
        Return Me.CurrentLevel
    End Function

#End Region

#Region " VOLTAGE LIMIT "

    ''' <summary> The Voltage Limit. </summary>
    Private _VoltageLimit As Double?

    ''' <summary> Gets or sets the cached Source Voltage Limit. </summary>
    ''' <value> The Source Voltage Limit. </value>
    Public Overridable Property VoltageLimit As Double?
        Get
            Return Me._VoltageLimit
        End Get
        Protected Set(ByVal value As Double?)
            Me.Resistance.VoltageLimit = If(value, 0)

            If Me.VoltageLimit.Differs(value, 0.000001) Then
                Me._VoltageLimit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Voltage Limit. </summary>
    ''' <remarks>
    ''' This command set the immediate output Voltage Limit. The value is in Volts. The immediate
    ''' Voltage Limit is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <param name="value"> The Voltage Limit. </param>
    ''' <returns> The Source Voltage Limit. </returns>
    Public Function ApplyVoltageLimit(ByVal value As Double) As Double?
        Me.WriteVoltageLimit(value)
        Return Me.QueryVoltageLimit
    End Function

    ''' <summary> Queries the Voltage Limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Voltage Limit or none if unknown. </returns>
    Public Function QueryVoltageLimit() As Double?
        Const printFormat As Decimal = 9.6D
        Me.VoltageLimit = Me.Session.QueryPrint(Me.VoltageLimit.GetValueOrDefault(0.1), printFormat, "{0}.limit", Me.EntityName)
        Return Me.VoltageLimit
    End Function

    ''' <summary>
    ''' Writes the source Voltage Limit without reading back the value from the device.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the immediate output Voltage Limit. The value is in Volts. The immediate
    ''' VoltageLimit is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Voltage Limit. </param>
    ''' <returns> The Source Voltage Limit. </returns>
    Public Function WriteVoltageLimit(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Me.MeterEntity = ThermalTransientMeterEntity.Transient Then
            If Not ThermalTransient.ValidateVoltageLimit(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        Else
            If Not ColdResistance.ValidateVoltageLimit(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        End If
        Me.Session.WriteLine("{0}:limitSetter({1})", Me.EntityName, value)
        Me.VoltageLimit = value
        Return Me.VoltageLimit
    End Function

#End Region

#Region " HIGH LIMIT "

    ''' <summary> The High Limit. </summary>
    Private _HighLimit As Double?

    ''' <summary> Gets or sets the cached High Limit. </summary>
    ''' <value> The High Limit. </value>
    Public Overridable Property HighLimit As Double?
        Get
            Return Me._HighLimit
        End Get
        Protected Set(ByVal value As Double?)
            Me.Resistance.HighLimit = If(value, 0)

            If Me.HighLimit.Differs(value, 0.000001) Then
                Me._HighLimit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the High Limit. </summary>
    ''' <remarks>
    ''' This command set the immediate output High Limit. The value is in Ohms. The immediate High
    ''' Limit is the output High setting. At *RST, the High values = 0.
    ''' </remarks>
    ''' <param name="value"> The High Limit. </param>
    ''' <returns> The High Limit. </returns>
    Public Function ApplyHighLimit(ByVal value As Double) As Double?
        Me.WriteHighLimit(value)
        Return Me.QueryHighLimit
    End Function

    ''' <summary> Queries the High Limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The High Limit or none if unknown. </returns>
    Public Function QueryHighLimit() As Double?
        Const printFormat As Decimal = 9.6D
        Me.HighLimit = Me.Session.QueryPrint(Me.HighLimit.GetValueOrDefault(0.1), printFormat, "{0}.highLimit", Me.EntityName)
        Return Me.HighLimit
    End Function

    ''' <summary> Writes the High Limit without reading back the value from the device. </summary>
    ''' <remarks>
    ''' This command sets the immediate output High Limit. The value is in Ohms. The immediate
    ''' HighLimit is the output High setting. At *RST, the High values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The High Limit. </param>
    ''' <returns> The High Limit. </returns>
    Public Function WriteHighLimit(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Me.MeterEntity = ThermalTransientMeterEntity.Transient Then
            If Not ThermalTransient.ValidateLimit(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        Else
            If Not ColdResistance.ValidateLimit(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        End If
        Me.Session.WriteLine("{0}:highLimitSetter({1})", Me.EntityName, value)
        Me.HighLimit = value
        Return Me.HighLimit
    End Function

#End Region

#Region " LOW LIMIT "

    ''' <summary> The Low Limit. </summary>
    Private _LowLimit As Double?

    ''' <summary> Gets or sets the cached Low Limit. </summary>
    ''' <value> The Low Limit. </value>
    Public Overridable Property LowLimit As Double?
        Get
            Return Me._LowLimit
        End Get
        Protected Set(ByVal value As Double?)
            Me.Resistance.LowLimit = If(value, 0)

            If Me.LowLimit.Differs(value, 0.000001) Then
                Me._LowLimit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Low Limit. </summary>
    ''' <remarks>
    ''' This command set the immediate output Low Limit. The value is in Ohms. The immediate Low
    ''' Limit is the output Low setting. At *RST, the Low values = 0.
    ''' </remarks>
    ''' <param name="value"> The Low Limit. </param>
    ''' <returns> The Low Limit. </returns>
    Public Function ApplyLowLimit(ByVal value As Double) As Double?
        Me.WriteLowLimit(value)
        Return Me.QueryLowLimit
    End Function

    ''' <summary> Queries the Low Limit. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Low Limit or none if unknown. </returns>
    Public Function QueryLowLimit() As Double?
        Const printFormat As Decimal = 9.6D
        Me.LowLimit = Me.Session.QueryPrint(Me.LowLimit.GetValueOrDefault(0.01), printFormat, "{0}.lowLimit", Me.EntityName)
        Return Me.LowLimit
    End Function

    ''' <summary> Writes the Low Limit without reading back the value from the device. </summary>
    ''' <remarks>
    ''' This command sets the immediate output Low Limit. The value is in Ohms. The immediate
    ''' LowLimit is the output Low setting. At *RST, the Low values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Low Limit. </param>
    ''' <returns> The Low Limit. </returns>
    Public Function WriteLowLimit(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Me.MeterEntity = ThermalTransientMeterEntity.Transient Then
            If Not ThermalTransient.ValidateLimit(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        Else
            If Not ColdResistance.ValidateLimit(value, details) Then
                Throw New ArgumentOutOfRangeException(NameOf(value), details)
            End If
        End If
        Me.Session.WriteLine("{0}:lowLimitSetter({1})", Me.EntityName, value)
        Me.LowLimit = value
        Return Me.LowLimit
    End Function

#End Region

#Region " CONFIGURE "

    ''' <summary> Applies the instrument defaults. </summary>
    ''' <remarks> This is required until the reset command gets implemented. </remarks>
    Public Overridable Sub ApplyInstrumentDefaults()
        Me.PublishVerbose($"Applying {Me.EntityName} defaults;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.StatusSubsystem.WriteLine($"{Me.EntityName}:currentSourceChannelSetter({Me.DefaultsName}.smuI)") : Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine($"{Me.EntityName}:apertureSetter({Me.DefaultsName}.aperture)") : Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine($"{Me.EntityName}:levelSetter({Me.DefaultsName}.level)") : Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine($"{Me.EntityName}:limitSetter({Me.DefaultsName}.limit)") : Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine($"{Me.EntityName}:lowLimitSetter({Me.DefaultsName}.lowLimit)") : Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine($"{Me.EntityName}:highLimitSetter({Me.DefaultsName}.highLimit)") : Me.Session.QueryOperationCompleted()
    End Sub

    ''' <summary> Reads instrument defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub ReadInstrumentDefaults()
        Me.PublishVerbose($"Reading {Me.EntityName} defaults;. ")
        isr.Core.ApplianceBase.DoEvents()
        If Me.RequiresSourceMeasureUnit Then
            My.MySettings.Default.SourceMeasureUnitDefault = Me.Session.QueryTrimEnd($"_G.print({Me.DefaultsName}.smuI)")
            If String.IsNullOrWhiteSpace(My.MySettings.Default.SourceMeasureUnitDefault) Then
                Me.PublishWarning("failed reading default source measure unit name;. Sent:'{0}; Received:'{1}'.",
                                  Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
            End If
        End If
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Applies changed meter configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The resistance. </param>
    Public Overridable Sub ConfigureChanged(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))

        If Not resistance.ConfigurationEquals(Me.Resistance) Then

            Me.PublishVerbose("Configuring {0} resistance measurement;. ", Me.EntityName)
            isr.Core.ApplianceBase.DoEvents()

            If Not String.Equals(Me.Resistance.SourceMeasureUnit, resistance.SourceMeasureUnit) Then
                Me.PublishVerbose("Setting {0} source measure unit to {1};. ", Me.EntityName, resistance.SourceMeasureUnit)
                Me.ApplySourceMeasureUnit(resistance.SourceMeasureUnit)
            End If

            If Not Me.Resistance.Aperture.Equals(resistance.Aperture) Then
                Me.PublishVerbose("Setting {0} aperture to {1};. ", Me.EntityName, resistance.Aperture)
                Me.ApplyAperture(resistance.Aperture)
            End If

            If Not Me.Resistance.CurrentLevel.Equals(resistance.CurrentLevel) Then
                Me.PublishVerbose("Setting {0} Current Level to {1};. ", Me.EntityName, resistance.CurrentLevel)
                Me.ApplyCurrentLevel(resistance.CurrentLevel)
            End If

            If Not Me.Resistance.HighLimit.Equals(resistance.HighLimit) Then
                Me.PublishVerbose("Setting {0} high limit to {1};. ", Me.EntityName, resistance.HighLimit)
                Me.ApplyHighLimit(resistance.HighLimit)
            End If

            If Not Me.Resistance.LowLimit.Equals(resistance.LowLimit) Then
                Me.PublishVerbose("Setting {0} Low limit to {1};. ", Me.EntityName, resistance.LowLimit)
                Me.ApplyLowLimit(resistance.LowLimit)
            End If

            If Not Me.Resistance.VoltageLimit.Equals(resistance.VoltageLimit) Then
                Me.PublishVerbose("Setting {0} Voltage Limit to {1};. ", Me.EntityName, resistance.VoltageLimit)
                Me.ApplyVoltageLimit(resistance.VoltageLimit)
            End If
            isr.Core.ApplianceBase.DoEvents()

        End If

    End Sub

    ''' <summary> Configures the meter for making the resistance measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The resistance. </param>
    Public Overridable Sub Configure(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        Me.PublishVerbose("Configuring {0} resistance measurement;. ", Me.EntityName)
        isr.Core.ApplianceBase.DoEvents()

        Me.PublishVerbose("Setting {0} source measure unit to {1};. ", Me.EntityName, resistance.SourceMeasureUnit)
        Me.ApplySourceMeasureUnit(resistance.SourceMeasureUnit)
        isr.Core.ApplianceBase.DoEvents()

        Me.PublishVerbose("Setting {0} aperture to {1};. ", Me.EntityName, resistance.Aperture)
        Me.ApplyAperture(resistance.Aperture)
        isr.Core.ApplianceBase.DoEvents()

        Me.PublishVerbose("Setting {0} Current Level to {1};. ", Me.EntityName, resistance.CurrentLevel)
        Me.ApplyCurrentLevel(resistance.CurrentLevel)
        isr.Core.ApplianceBase.DoEvents()

        Me.PublishVerbose("Setting {0} high limit to {1};. ", Me.EntityName, resistance.HighLimit)
        Me.ApplyHighLimit(resistance.HighLimit)
        isr.Core.ApplianceBase.DoEvents()

        Me.PublishVerbose("Setting {0} Low limit to {1};. ", Me.EntityName, resistance.LowLimit)
        Me.ApplyLowLimit(resistance.LowLimit)
        isr.Core.ApplianceBase.DoEvents()

        Me.PublishVerbose("Setting {0} Voltage Limit to {1};. ", Me.EntityName, resistance.VoltageLimit)
        Me.ApplyVoltageLimit(resistance.VoltageLimit)
        isr.Core.ApplianceBase.DoEvents()

    End Sub

    ''' <summary> Queries the configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub QueryConfiguration()
        Me.PublishVerbose("Reading {0} meter configuration;. ", Me.EntityName)
        isr.Core.ApplianceBase.DoEvents()
        Me.QuerySourceMeasureUnit()
        Me.QueryAperture()
        Me.QueryCurrentLevel()
        Me.QueryHighLimit()
        Me.QueryLowLimit()
        Me.QueryVoltageLimit()
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Measures the Final resistance and returns. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub Measure()
        Me.DefineClearExecutionState()
        Me.Session.EnableServiceRequestWaitComplete()
        Select Case Me.MeterEntity
            Case ThermalTransientMeterEntity.FinalResistance
                Me.Session.WriteLine("_G.ttm.measureFinalResistance() waitcomplete()")
            Case ThermalTransientMeterEntity.InitialResistance
                Me.Session.WriteLine("_G.ttm.measureInitialResistance() waitcomplete()")
            Case ThermalTransientMeterEntity.Transient
                Me.Session.WriteLine("_G.ttm.measureThermalTransient() waitcomplete()")
            Case Else
        End Select
        Select Case Me.MeterEntity
            Case ThermalTransientMeterEntity.FinalResistance,
                ThermalTransientMeterEntity.InitialResistance,
                ThermalTransientMeterEntity.Transient
                Me.Session.QueryOperationCompleted()
                Me.StatusSubsystem.CheckThrowDeviceException(False, "Measuring Final Resistance;. ")
            Case Else
        End Select
    End Sub

    ''' <summary> Measures the Thermal Transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The Thermal Transient element. </param>
    Public Sub Measure(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        resistance.DefineClearExecutionState()
        Me.Measure()
        Me.StatusSubsystem.CheckThrowDeviceException(False, "Measuring Thermal Transient;. last command: '{0}'", Me.Session.LastMessageSent)
    End Sub

#End Region

#Region " OKAY "

    ''' <summary> Queries the meter okay sentinel. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> <c>True</c> if entity okay status is True; otherwise, <c>False</c>. </returns>
    Public Function QueryOkay() As Boolean
        Me.Session.MakeEmulatedReplyIfEmpty(True)
        Return Me.Session.IsStatementTrue("{0}:isOkay()", Me.EntityName)
    End Function

#End Region

#Region " OUTCOME "

    ''' <summary> Queries the meter outcome status. If nil, measurement was not made. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> <c>True</c> if entity outcome is nil; otherwise, <c>False</c>. </returns>
    Public Function QueryOutcomeNil() As Boolean
        Me.Session.MakeEmulatedReplyIfEmpty(True)
        Return Me.Session.IsStatementTrue("{0}.outcome==nil", Me.EntityName)
    End Function

#End Region

#Region " READ "

    ''' <summary> The last reading. </summary>
    Private _LastReading As String

    ''' <summary> Gets or sets (protected) the last reading. </summary>
    ''' <value> The last reading. </value>
    Public Property LastReading() As String
        Get
            Return Me._LastReading
        End Get
        Protected Set(ByVal value As String)
            Me._LastReading = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The last outcome. </summary>
    Private _LastOutcome As String

    ''' <summary> Gets or sets (protected) the last outcome. </summary>
    ''' <value> The last outcome. </value>
    Public Property LastOutcome() As String
        Get
            Return Me._LastOutcome
        End Get
        Protected Set(ByVal value As String)
            Me._LastOutcome = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The last measurement status. </summary>
    Private _LastMeasurementStatus As String

    ''' <summary> Gets or sets (protected) the last measurement status. </summary>
    ''' <value> The last measurement status. </value>
    Public Property LastMeasurementStatus() As String
        Get
            Return Me._LastMeasurementStatus
        End Get
        Protected Set(ByVal value As String)
            Me._LastMeasurementStatus = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> True if measurement available. </summary>
    Private _MeasurementAvailable As Boolean

    ''' <summary> Gets or sets (protected) the measurement available. </summary>
    ''' <value> The measurement available. </value>
    Public Property MeasurementAvailable As Boolean
        Get
            Return Me._MeasurementAvailable
        End Get
        Set(ByVal value As Boolean)
            Me._MeasurementAvailable = value
            Me.SyncNotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The measurement event Condition. </summary>
    Private _MeasurementEventCondition As Integer

    ''' <summary> Gets or sets the cached Condition of the measurement register events. </summary>
    ''' <value> <c>null</c> if value is not known;. </value>
    Public Property MeasurementEventCondition() As Integer
        Get
            Return Me._MeasurementEventCondition
        End Get
        Protected Set(ByVal value As Integer)
            If Not Me.MeasurementEventCondition.Equals(value) Then
                Me._MeasurementEventCondition = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Reads the condition of the measurement register event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> System.Nullable{System.Int32}. </returns>
    Public Function TryQueryMeasurementEventCondition() As Boolean
        Return Me.Session.TryQueryPrint(1, Me.MeasurementEventCondition, "_G.status.measurement.condition")
    End Function

    ''' <summary> Parses the outcome. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="details"> [in,out] The details. </param>
    ''' <returns> The parsed outcome. </returns>
    Public Function ParseOutcome(ByRef details As String) As MeasurementOutcomes

        Dim outcome As MeasurementOutcomes = MeasurementOutcomes.None
        Dim detailsBuilder As New System.Text.StringBuilder
        Dim measurementCondition As Integer

        Dim numericValue As Integer

        If String.IsNullOrWhiteSpace(Me.LastOutcome) Then
            outcome = outcome Or MeasurementOutcomes.MeasurementNotMade
            detailsBuilder.AppendFormat("Failed parsing outcome -- outcome is nothing indicating measurement not made;. ")
        Else
            If Integer.TryParse(Me.LastOutcome, Globalization.NumberStyles.Number, Globalization.CultureInfo.InvariantCulture, numericValue) Then
                If numericValue < 0 Then
                    If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                    detailsBuilder.AppendFormat("Failed parsing outcome '{0}';. ", Me.LastOutcome)
                    outcome = MeasurementOutcomes.MeasurementFailed
                    outcome = outcome Or MeasurementOutcomes.UnexpectedOutcomeFormat
                Else
                    ' interpret the outcome.
                    If numericValue <> 0 Then
                        outcome = MeasurementOutcomes.MeasurementFailed

                        If (numericValue And 1) = 1 Then
                            ' this is a bad status - could be compliance or other things.
                            If Not String.IsNullOrWhiteSpace(Me.LastMeasurementStatus) Then
                                If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                                detailsBuilder.AppendFormat("Outcome status={0};. ", Me.LastMeasurementStatus)
                            End If
                            If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                            If Me.TryQueryMeasurementEventCondition Then
                                measurementCondition = Me.MeasurementEventCondition
                                detailsBuilder.AppendFormat("Measurement condition=0x{0:X4};. ", measurementCondition)
                            Else
                                measurementCondition = &HFFFF
                                detailsBuilder.AppendFormat("Failed getting measurement condition. Set to 0x{0:X4};. ", measurementCondition)
                            End If
                            outcome = outcome Or MeasurementOutcomes.HitCompliance
                        End If

                        If (numericValue And 2) <> 0 Then
                            If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                            detailsBuilder.AppendFormat("Sampling returned bad time stamps;. Device reported outcome=0x{0:X4} and status measurement condition=0x{1:X4}",
                                                        numericValue, measurementCondition)
                            outcome = outcome Or MeasurementOutcomes.UnspecifiedStatusException
                        End If

                        If (numericValue And 4) <> 0 Then
                            If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                            detailsBuilder.AppendFormat("Configuration failed;. Device reported outcome=0x{0:X4} and measurement condition=0x{1:X4}",
                                                        numericValue, measurementCondition)
                            outcome = outcome Or MeasurementOutcomes.UnspecifiedStatusException
                        End If

                        If (numericValue And 8) <> 0 Then
                            If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                            detailsBuilder.AppendFormat("Initiation failed;. Device reported outcome=0x{0:X4} and status measurement condition=0x{1:X4}",
                                                        numericValue, measurementCondition)
                            outcome = outcome Or MeasurementOutcomes.UnspecifiedStatusException
                        End If

                        If (numericValue And 16) <> 0 Then
                            If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                            detailsBuilder.AppendFormat("Load failed;. Device reported outcome=0x{0:X4} and measurement condition=0x{1:X4}",
                                                        numericValue, measurementCondition)
                            outcome = outcome Or MeasurementOutcomes.UnspecifiedStatusException
                        End If

                        If (numericValue And (Not 31)) <> 0 Then
                            If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                            detailsBuilder.AppendFormat("Unknown failure;. Device reported outcome=0x{0:X4} and measurement condition=0x{1:X4}",
                                                        numericValue, measurementCondition)
                            outcome = outcome Or MeasurementOutcomes.UnspecifiedStatusException
                        End If
                    End If
                End If
            Else
                If detailsBuilder.Length > 0 Then detailsBuilder.AppendLine()
                detailsBuilder.AppendFormat("Failed parsing outcome '{0}';. {1}{2}.", Me.LastOutcome,
                                            Environment.NewLine, New StackFrame(True).UserCallStack())
                outcome = MeasurementOutcomes.MeasurementFailed Or MeasurementOutcomes.UnexpectedReadingFormat
            End If
        End If
        details = detailsBuilder.ToString
        Return outcome
    End Function

#End Region

End Class

