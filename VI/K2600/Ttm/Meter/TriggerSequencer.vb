Imports System.ComponentModel

Imports isr.Core.Models
Imports isr.VI.ExceptionExtensions

''' <summary> Triggered Measurement sequencer. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-03-14 </para>
''' </remarks>
Public Class TriggerSequencer
    Inherits ViewModelBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me._SignalQueueSyncLocker = New Object
        Me.LockedSignalQueue = New Queue(Of TriggerSequenceSignal)
    End Sub

#Region " I Disposable Support "

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the disposed status. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                ' Free managed resources when explicitly called
                If Me.SequencerTimer IsNot Nothing Then
                    Me.SequencerTimer.Enabled = False
                    Me.SequencerTimer.Dispose()
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
    ''' unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

#End Region

#End Region

#Region " SEQUENCED MEASUREMENTS "

    ''' <summary> Gets or sets the assert requested. </summary>
    ''' <value> The assert requested. </value>
    Public Property AssertRequested As Boolean

    ''' <summary> Asserts a trigger to emulate triggering for timing measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub AssertTrigger()
        Me.AssertRequested = True
        Me.SyncNotifyPropertyChanged()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Next status bar. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lastStatusBar"> The last status bar. </param>
    ''' <returns> The next status bar. </returns>
    Public Shared Function NextStatusBar(ByVal lastStatusBar As String) As String
        If String.IsNullOrEmpty(lastStatusBar) Then
            lastStatusBar = "|"
        ElseIf lastStatusBar = "|" Then
            lastStatusBar = "/"
        ElseIf lastStatusBar = "/" Then
            lastStatusBar = "-"
        ElseIf lastStatusBar = "-" Then
            lastStatusBar = "\"
        ElseIf lastStatusBar = "\" Then
            lastStatusBar = "|"
        Else
            lastStatusBar = "|"
        End If
        Return lastStatusBar
    End Function

    ''' <summary> The trigger stopwatch. </summary>
    Private _TriggerStopwatch As Stopwatch

    ''' <summary> Returns the percent progress. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="lastStatusBar"> The last status bar. </param>
    ''' <returns> The status message. </returns>
    Public Function ProgressMessage(ByVal lastStatusBar As String) As String
        Dim message As String = NextStatusBar(lastStatusBar)
        Select Case Me.TriggerSequenceState
            Case TriggerSequenceState.Aborted
                message = "ABORTED"
            Case TriggerSequenceState.Failed
                message = "FAILED"
            Case TriggerSequenceState.Idle
                message = "Inactive"
            Case TriggerSequenceState.MeasurementCompleted
                message = "DATA AVAILABLE"
            Case TriggerSequenceState.None
            Case TriggerSequenceState.ReadingValues
                message = "READING..."
            Case TriggerSequenceState.Starting
                message = "PREPARING"
            Case TriggerSequenceState.Stopped
                message = "STOPPED"
            Case TriggerSequenceState.WaitingForTrigger
                message = String.Format("Waiting for trigger {0:s\.f} s", Me._TriggerStopwatch.Elapsed)
        End Select
        Return message
    End Function

    ''' <summary> State of the trigger sequence. </summary>
    Private _TriggerSequenceState As TriggerSequenceState

    ''' <summary> Gets or sets the state of the measurement. </summary>
    ''' <value> The measurement state. </value>
    Public Property TriggerSequenceState As TriggerSequenceState
        Get
            Return Me._TriggerSequenceState
        End Get
        Protected Set(value As TriggerSequenceState)
            If TriggerSequenceState.WaitingForTrigger = value OrElse Not value.Equals(Me.TriggerSequenceState) Then
                Me._TriggerSequenceState = value
                Me.SyncNotifyPropertyChanged()
                isr.Core.ApplianceBase.DoEvents()
            End If
        End Set
    End Property

    ''' <summary> The signal queue synchronize locker. </summary>
    Private ReadOnly _SignalQueueSyncLocker As Object

    ''' <summary> Gets or sets a queue of signals. </summary>
    ''' <value> A Queue of signals. </value>
    Private Property LockedSignalQueue As Queue(Of TriggerSequenceSignal)

    ''' <summary> Clears the signal queue. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ClearSignalQueue()
        SyncLock Me._SignalQueueSyncLocker
            Me.LockedSignalQueue.Clear()
        End SyncLock
    End Sub

    ''' <summary> Dequeues a  signal. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="signal"> [in,out] The signal. </param>
    ''' <returns> <c>True</c> if a signal was dequeued. </returns>
    Public Function Dequeue(ByRef signal As TriggerSequenceSignal) As Boolean
        SyncLock Me._SignalQueueSyncLocker
            If Me.LockedSignalQueue.Any Then
                signal = Me.LockedSignalQueue.Dequeue()
                Return True
            Else
                Return False
            End If
        End SyncLock
    End Function

    ''' <summary> Enqueues a  signal. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="signal"> The signal. </param>
    Public Sub Enqueue(ByVal signal As TriggerSequenceSignal)
        SyncLock Me._SignalQueueSyncLocker
            Me.LockedSignalQueue.Enqueue(signal)
        End SyncLock
    End Sub

    ''' <summary> Starts a measurement sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub StartMeasurementSequence()

        Me.RestartSignal = TriggerSequenceSignal.None
        If Me.SequencerTimer Is Nothing Then
            Me.SequencerTimer = New Timers.Timer()
        End If
        Me.SequencerTimer.Enabled = False
        Me.SequencerTimer.Interval = 100

        Me.ClearSignalQueue()
        Me.Enqueue(TriggerSequenceSignal.Step)
        Me.SequencerTimer.Enabled = True

    End Sub

    ''' <summary> Gets or sets the timer. </summary>
    Private WithEvents SequencerTimer As Timers.Timer

    ''' <summary> Executes the state sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SequencerTimer_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs) Handles SequencerTimer.Elapsed

        Try
            Me.SequencerTimer.Enabled = False
            isr.Core.ApplianceBase.DoEvents()
            Me.TriggerSequenceState = Me.ExecuteMeasurementSequence(Me.TriggerSequenceState)
        Catch ex As Exception
            Me.Enqueue(TriggerSequenceSignal.Failure)
        Finally
            isr.Core.ApplianceBase.DoEvents()
            Me.SequencerTimer.Enabled = Me.TriggerSequenceState <> TriggerSequenceState.Idle AndAlso
                Me.TriggerSequenceState <> TriggerSequenceState.None
        End Try

    End Sub

    ''' <summary> Gets or sets the restart signal. </summary>
    ''' <value> The restart signal. </value>
    Public Property RestartSignal As TriggerSequenceSignal

    ''' <summary> Executes the measurement sequence returning the next state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="currentState"> The current measurement sequence state. </param>
    ''' <returns> The next state. </returns>
    Private Function ExecuteMeasurementSequence(ByVal currentState As TriggerSequenceState) As TriggerSequenceState

        Dim signal As TriggerSequenceSignal
        Select Case currentState

            Case TriggerSequenceState.Idle

                ' Waiting for the step signal to start.
                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                        Case TriggerSequenceSignal.Step
                            currentState = TriggerSequenceState.Starting
                    End Select
                End If

            Case TriggerSequenceState.Aborted

                ' if failed, no action. The sequencer should stop here
                ' wait for the signal to move to the idle state
                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                        Case TriggerSequenceSignal.Failure
                            currentState = TriggerSequenceState.Failed
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                            ' clear the queue to start a fresh cycle.
                            Me.ClearSignalQueue()
                            currentState = TriggerSequenceState.Idle
                        Case TriggerSequenceSignal.Step
                            ' clear the queue to start a fresh cycle.
                            Me.ClearSignalQueue()
                            currentState = TriggerSequenceState.Idle
                    End Select
                End If

            Case TriggerSequenceState.Failed

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.Step
                            currentState = TriggerSequenceState.Aborted
                    End Select
                End If

            Case TriggerSequenceState.Stopped

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.Failure
                            currentState = TriggerSequenceState.Failed
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                            currentState = TriggerSequenceState.Idle
                        Case TriggerSequenceSignal.Step
                            currentState = TriggerSequenceState.Idle
                    End Select
                End If

            Case TriggerSequenceState.Starting

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.Failure
                            currentState = TriggerSequenceState.Failed
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                            currentState = TriggerSequenceState.Idle
                        Case TriggerSequenceSignal.Step
                            Me._TriggerStopwatch = Stopwatch.StartNew
                            currentState = TriggerSequenceState.WaitingForTrigger
                    End Select
                End If

            Case TriggerSequenceState.WaitingForTrigger

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort, TriggerSequenceSignal.Failure, TriggerSequenceSignal.Stop
                            Me.RestartSignal = signal
                            ' request a trigger. 
                            Me.AssertRequested() = True
                        Case TriggerSequenceSignal.Step
                            currentState = TriggerSequenceState.MeasurementCompleted
                    End Select
                End If

            Case TriggerSequenceState.MeasurementCompleted

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.Failure
                            currentState = TriggerSequenceState.Failed
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                            currentState = TriggerSequenceState.Idle
                        Case TriggerSequenceSignal.Step
                            If Me.RestartSignal = TriggerSequenceSignal.None OrElse
                                Me.RestartSignal = TriggerSequenceSignal.Step Then
                                currentState = TriggerSequenceState.ReadingValues
                            Else
                                Select Case Me.RestartSignal
                                    Case TriggerSequenceSignal.Abort
                                        currentState = TriggerSequenceState.Aborted
                                    Case TriggerSequenceSignal.Failure
                                        currentState = TriggerSequenceState.Failed
                                    Case TriggerSequenceSignal.None
                                    Case TriggerSequenceSignal.Stop
                                        currentState = TriggerSequenceState.Idle
                                End Select
                                Me.RestartSignal = TriggerSequenceSignal.None
                            End If
                    End Select
                End If

            Case TriggerSequenceState.ReadingValues

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case TriggerSequenceSignal.Abort
                            currentState = TriggerSequenceState.Aborted
                        Case TriggerSequenceSignal.Failure
                            currentState = TriggerSequenceState.Failed
                        Case TriggerSequenceSignal.None
                        Case TriggerSequenceSignal.Stop
                            currentState = TriggerSequenceState.Idle
                        Case TriggerSequenceSignal.Step
                            If Me.RestartSignal = TriggerSequenceSignal.None OrElse
                                Me.RestartSignal = TriggerSequenceSignal.Step Then
                                currentState = TriggerSequenceState.Starting
                            Else
                                Select Case Me.RestartSignal
                                    Case TriggerSequenceSignal.Abort
                                        currentState = TriggerSequenceState.Aborted
                                    Case TriggerSequenceSignal.Failure
                                        currentState = TriggerSequenceState.Failed
                                    Case TriggerSequenceSignal.None
                                    Case TriggerSequenceSignal.Stop
                                        currentState = TriggerSequenceState.Idle
                                End Select
                                Me.RestartSignal = TriggerSequenceSignal.None
                            End If
                    End Select
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & currentState.ToString)
                Me.Enqueue(TriggerSequenceSignal.Abort)

        End Select
        Return currentState

    End Function

#End Region

End Class

#Disable Warning CA1027 ' Mark enums with FlagsAttribute

''' <summary> Enumerates the measurement sequence. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum TriggerSequenceState
#Enable Warning CA1027 ' Mark enums with FlagsAttribute

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not Defined")>
    None = 0

    ''' <summary> An enum constant representing the idle option. </summary>
    <Description("Idle")>
    Idle = 0

    ''' <summary> An enum constant representing the aborted option. </summary>
    <Description("Triggered Measurement Sequence Aborted")>
    Aborted

    ''' <summary> An enum constant representing the failed option. </summary>
    <Description("Triggered Measurement Sequence Failed")>
    Failed

    ''' <summary> An enum constant representing the starting option. </summary>
    <Description("Triggered Measurement Sequence Starting")>
    Starting

    ''' <summary> An enum constant representing the waiting for trigger option. </summary>
    <Description("Waiting for Trigger")>
    WaitingForTrigger

    ''' <summary> An enum constant representing the measurement completed option. </summary>
    <Description("Measurement Completed")>
    MeasurementCompleted

    ''' <summary> An enum constant representing the reading values option. </summary>
    <Description("Reading Values")>
    ReadingValues

    ''' <summary> An enum constant representing the stopped option. </summary>
    <Description("Triggered Measurement Sequence Stopped")>
    Stopped
End Enum

''' <summary> Enumerates the measurement signals. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum TriggerSequenceSignal

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not Defined")>
    None = 0

    ''' <summary> An enum constant representing the step] option. </summary>
    <Description("Step Measurement")>
    [Step]

    ''' <summary> An enum constant representing the abort] option. </summary>
    <Description("Abort Measurement")>
    [Abort]

    ''' <summary> An enum constant representing the stop] option. </summary>
    <Description("Stop Measurement")>
    [Stop]

    ''' <summary> An enum constant representing the failure] option. </summary>
    <Description("Report Failure")>
    [Failure]
End Enum


