Imports System.ComponentModel

Imports isr.Core.Models
Imports isr.VI.ExceptionExtensions

''' <summary> Measure sequencer. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-03-14 </para>
''' </remarks>
Public Class MeasureSequencer
    Inherits ViewModelBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:isr.Core.Models.ViewModelBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me._SignalQueueSyncLocker = New Object
        Me.LockedSignalQueue = New Queue(Of MeasurementSequenceSignal)
    End Sub

#Region " I Disposable Support "

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets or sets the disposed status. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                ' Free managed resources when explicitly called
                If Me.SequencerTimer IsNot Nothing Then
                    Me.SequencerTimer.Enabled = False
                    Me.SequencerTimer.Dispose()
                End If
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
    ''' unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

#End Region

#End Region

#Region " SEQUENCED MEASUREMENTS "

    ''' <summary> Returns the percent progress. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="state"> The state. </param>
    ''' <returns> A value between 0 and 100 representing the progress. </returns>
    Private Shared Function PercentProgress(ByVal state As MeasurementSequenceState) As Integer
        Return CInt(100 * Math.Min(1, Math.Max(0, (state - MeasurementSequenceState.Starting) /
                                               (MeasurementSequenceState.Completed - MeasurementSequenceState.Starting))))
    End Function

    ''' <summary> Returns the percent progress. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A value between 0 and 100 representing the progress. </returns>
    Public Function PercentProgress() As Integer
        Return MeasureSequencer.PercentProgress(Me.MeasurementSequenceState)
    End Function

    ''' <summary> State of the measurement sequence. </summary>
    Private _MeasurementSequenceState As MeasurementSequenceState

    ''' <summary> Gets or sets the state of the measurement. </summary>
    ''' <value> The measurement state. </value>
    Public Property MeasurementSequenceState As MeasurementSequenceState
        Get
            Return Me._MeasurementSequenceState
        End Get
        Protected Set(value As MeasurementSequenceState)
            If Not value.Equals(Me.MeasurementSequenceState) Then
                Me._MeasurementSequenceState = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The signal queue synchronization locker. </summary>
    Private ReadOnly _SignalQueueSyncLocker As Object

    ''' <summary> Gets or sets a queue of signals. </summary>
    ''' <value> A Queue of signals. </value>
    Private Property LockedSignalQueue As Queue(Of MeasurementSequenceSignal)

    ''' <summary> Clears the signal queue. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ClearSignalQueue()
        SyncLock Me._SignalQueueSyncLocker
            Me.LockedSignalQueue.Clear()
        End SyncLock
    End Sub

    ''' <summary> Dequeues a  signal. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="signal"> [in,out] The signal. </param>
    ''' <returns> <c>True</c> if a signal was dequeued. </returns>
    Public Function Dequeue(ByRef signal As MeasurementSequenceSignal) As Boolean
        SyncLock Me._SignalQueueSyncLocker
            If Me.LockedSignalQueue.Any Then
                signal = Me.LockedSignalQueue.Dequeue()
                Return True
            Else
                Return False
            End If
        End SyncLock
    End Function

    ''' <summary> Enqueues a  signal. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="signal"> The signal. </param>
    Public Sub Enqueue(ByVal signal As MeasurementSequenceSignal)
        SyncLock Me._SignalQueueSyncLocker
            Me.LockedSignalQueue.Enqueue(signal)
        End SyncLock
    End Sub

    ''' <summary> Starts a measurement sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub StartMeasurementSequence()

        If Me.SequencerTimer Is Nothing Then
            Me.SequencerTimer = New Timers.Timer()
        End If
        Me.SequencerTimer.Enabled = False
        Me.SequencerTimer.Interval = 300

        Me.ClearSignalQueue()
        Me.Enqueue(MeasurementSequenceSignal.Step)
        Me.SequencerTimer.Enabled = True

    End Sub

    ''' <summary> Gets or sets the timer. </summary>
    Private WithEvents SequencerTimer As Timers.Timer

    ''' <summary> Executes the state sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Specifies the object where the call originated. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SequencerTimer_Elapsed(ByVal sender As Object, ByVal e As System.EventArgs) Handles SequencerTimer.Elapsed

        Try
            Me.SequencerTimer.Enabled = False
            Me.MeasurementSequenceState = Me.ExecuteMeasurementSequence(Me.MeasurementSequenceState)
        Catch ex As Exception
            Me.Enqueue(MeasurementSequenceSignal.Failure)
        Finally
            Me.SequencerTimer.Enabled = Me.MeasurementSequenceState <> MeasurementSequenceState.Idle AndAlso
                Me.MeasurementSequenceState <> MeasurementSequenceState.None
        End Try

    End Sub

    ''' <summary> The measurement onset stopwatch. </summary>
    Private _MeasurementOnsetStopwatch As Stopwatch

    ''' <summary> The measurement onset timespan. </summary>
    Private _MeasurementOnsetTimespan As TimeSpan

    ''' <summary> Starts final resistance time. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="pauseTime"> The pause time. </param>
    Public Sub StartFinalResistanceTime(ByVal pauseTime As TimeSpan)
        Me._MeasurementOnsetStopwatch = Stopwatch.StartNew
        Me._MeasurementOnsetTimespan = pauseTime
    End Sub

    ''' <summary> Waits for end of post transient delay time. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> true when delay is done. </returns>
    Private Function DonePostTransientPause() As Boolean
        Return Me._MeasurementOnsetStopwatch.Elapsed >= Me._MeasurementOnsetTimespan
    End Function

    ''' <summary> Executes the measurement sequence returning the next state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="currentState"> The current measurement sequence state. </param>
    ''' <returns> The next state. </returns>
    Private Function ExecuteMeasurementSequence(ByVal currentState As MeasurementSequenceState) As MeasurementSequenceState

        Dim signal As MeasurementSequenceSignal
        Select Case currentState

            Case MeasurementSequenceState.Idle

                ' Waiting for the step signal to start.
                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.Starting
                    End Select
                End If

            Case MeasurementSequenceState.Aborted

                ' if failed, no action. The sequencer should stop here
                ' wait for the signal to move to the idle state
                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            ' clear the queue to start a fresh cycle.
                            Me.ClearSignalQueue()
                            currentState = MeasurementSequenceState.Idle
                    End Select
                End If

            Case MeasurementSequenceState.Failed

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.Aborted
                    End Select
                End If

            Case MeasurementSequenceState.Completed

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.Idle
                    End Select
                End If

            Case MeasurementSequenceState.Starting

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.MeasureInitialResistance
                    End Select
                End If

            Case MeasurementSequenceState.MeasureInitialResistance

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.MeasureThermalTransient
                    End Select
                End If

            Case MeasurementSequenceState.MeasureThermalTransient

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.PostTransientPause
                    End Select
                End If

            Case MeasurementSequenceState.PostTransientPause

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.MeasureFinalResistance
                    End Select
                ElseIf Me.DonePostTransientPause Then
                    currentState = MeasurementSequenceState.MeasureFinalResistance
                End If

            Case MeasurementSequenceState.MeasureFinalResistance

                If Me.Dequeue(signal) Then
                    Select Case signal
                        Case MeasurementSequenceSignal.Abort
                            currentState = MeasurementSequenceState.Aborted
                        Case MeasurementSequenceSignal.Failure
                            currentState = MeasurementSequenceState.Failed
                        Case MeasurementSequenceSignal.None
                        Case MeasurementSequenceSignal.Step
                            currentState = MeasurementSequenceState.Completed
                    End Select
                End If

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & currentState.ToString)
                Me.Enqueue(MeasurementSequenceSignal.Abort)

        End Select
        Return currentState

    End Function

#End Region

End Class

#Disable Warning CA1027 ' Mark enums with FlagsAttribute

''' <summary> Enumerates the measurement sequence. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum MeasurementSequenceState
#Enable Warning CA1027 ' Mark enums with FlagsAttribute

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not Defined")>
    None = 0

    ''' <summary> An enum constant representing the idle option. </summary>
    <Description("Idle")>
    Idle = 0

    ''' <summary> An enum constant representing the aborted option. </summary>
    <Description("Measurement Sequence Aborted")>
    Aborted

    ''' <summary> An enum constant representing the failed option. </summary>
    <Description("Measurement Sequence Failed")>
    Failed

    ''' <summary> An enum constant representing the starting option. </summary>
    <Description("Measurement Sequence Starting")>
    Starting

    ''' <summary> An enum constant representing the measure initial resistance option. </summary>
    <Description("Measure Initial Resistance")>
    MeasureInitialResistance

    ''' <summary> An enum constant representing the measure thermal transient option. </summary>
    <Description("Fetched Initial Resistance")>
    MeasureThermalTransient

    ''' <summary> An enum constant representing the post transient pause option. </summary>
    <Description("Post Transient Pause")>
    PostTransientPause

    ''' <summary> An enum constant representing the measure final resistance option. </summary>
    <Description("Measure Final Resistance")>
    MeasureFinalResistance

    ''' <summary> An enum constant representing the completed option. </summary>
    <Description("Measurement Sequence Completed")>
    Completed
End Enum

''' <summary> Enumerates the measurement signals. </summary>
''' <remarks> David, 2020-10-12. </remarks>
Public Enum MeasurementSequenceSignal

    ''' <summary> An enum constant representing the none option. </summary>
    <Description("Not Defined")>
    None = 0

    ''' <summary> An enum constant representing the step] option. </summary>
    <Description("Step Measurement")>
    [Step]

    ''' <summary> An enum constant representing the abort] option. </summary>
    <Description("Abort Measurement")>
    [Abort]

    ''' <summary> An enum constant representing the failure] option. </summary>
    <Description("Report Failure")>
    [Failure]
End Enum


