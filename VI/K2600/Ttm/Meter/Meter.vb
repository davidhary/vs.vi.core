Imports isr.Core.Models
Imports isr.VI.Tsp
Imports isr.VI.Tsp.K2600
Imports isr.VI.ExceptionExtensions

''' <summary>
''' Defines the thermal transient meter including measurement and configuration. The Thermal
''' Transient meter includes the elements that define a complete set of thermal transient
''' measurements and settings.
''' </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2010-09-17, 2.2.3912.x. </para>
''' </remarks>
Public Class Meter
    Inherits ViewModelTalkerBase
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me._MasterDevice = New K2600Device
        AddHandler My.MySettings.Default.PropertyChanged, AddressOf Me.MySettings_PropertyChanged
    End Sub

    ''' <summary> Creates a new Device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A Device. </returns>
    Public Shared Function Create() As Meter
        Dim device As Meter

        Try
            device = New Meter
        Catch
            Throw
        End Try
        Return device
    End Function

#Region " I Disposable Support "

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary> Gets the disposed status. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Try
                    Me.OnClosing()
                    If Me._MasterDevice Is Nothing Then
                        Me._MasterDevice.Dispose() : Me._MasterDevice = Nothing
                    End If
                Catch ex As Exception
                    Debug.Assert(Not Debugger.IsAttached, ex.ToString)
                End Try
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, ex.ToFullBlownString)
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Finalizes this object. </summary>
    ''' <remarks>
    ''' David, 2015-11-21: Override because Dispose(disposing As Boolean) above has code to free
    ''' unmanaged resources.
    ''' </remarks>
    Protected Overrides Sub Finalize()
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(False)
        MyBase.Finalize()
    End Sub

#End Region


#End Region

#Region " MASTER DEVICE "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _MasterDevice As K2600Device
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets the master device. </summary>
    ''' <value> The master device. </value>
    Public ReadOnly Property MasterDevice As K2600Device
        Get
            Return Me._MasterDevice
        End Get
    End Property

    ''' <summary> Gets a value indicating whether the subsystem has an open Device open. </summary>
    ''' <value> <c>True</c> if the device has an open Device; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return Me._MasterDevice IsNot Nothing AndAlso Me.MasterDevice.IsDeviceOpen
        End Get
    End Property

    ''' <summary> Gets a value indicating whether the subsystem has an open session open. </summary>
    ''' <value> <c>True</c> if the device has an open session; otherwise, <c>False</c>. </value>
    Public ReadOnly Property IsSessionOpen As Boolean
        Get
            Return Me._MasterDevice IsNot Nothing AndAlso Me.MasterDevice.Session.IsSessionOpen
        End Get
    End Property

    ''' <summary> Gets the name of the resource. </summary>
    ''' <value> The name of the resource or &lt;closed&gt; if not open. </value>
    Public ReadOnly Property ResourceName As String
        Get
            If Me.MasterDevice Is Nothing Then
                Return "<closed>"
            ElseIf Me.MasterDevice.IsDeviceOpen Then
                Return Me.MasterDevice.OpenResourceName
            Else
                Return Me.MasterDevice.CandidateResourceName
            End If
        End Get
    End Property

    ''' <summary> Disposes of the local elements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnClosing()
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} handling closing meter"
            Try
                Me.AbortTriggerSequenceIf()
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
            Try
                If Me._MeasureSequencer IsNot Nothing Then Me._MeasureSequencer.Dispose() : Me._MeasureSequencer = Nothing
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
            Try
                If Me._TriggerSequencer IsNot Nothing Then Me._TriggerSequencer.Dispose() : Me._TriggerSequencer = Nothing
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
            Try
                Me.BindShuntResistance(Nothing)
                Me.BindInitialResistance(Nothing)
                Me.BindFinalResistance(Nothing)
                Me.BindThermalTransient(Nothing)
                Me.BindThermalTransientEstimator(Nothing)
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
            Try
                Me._ConfigInfo = Nothing
            Catch ex As Exception
                Debug.Assert(Not Debugger.IsAttached, ex.ToString)
            End Try
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Called by the master device when stating the closing sequence before disposing of the
    ''' subsystems.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MasterDevice_Closing(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MasterDevice.Closing
        If sender IsNot Nothing AndAlso Me.IsDeviceOpen Then Me.OnClosing()
    End Sub

    ''' <summary>
    ''' Event handler. Called by the Master Device when closed after all subsystems were disposed.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MasterDevice_Closed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MasterDevice.Closed
    End Sub

    ''' <summary>
    ''' Event handler. Called by the Master Device before initializing the subsystems.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub MasterDevice_Opening(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MasterDevice.Opening
        Me.MeasureSequencer = New MeasureSequencer
        Me.TriggerSequencer = New TriggerSequencer
    End Sub

    ''' <summary>
    ''' Event handler. Called by MasterDevice when opened after initializing the subsystems.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MasterDevice_Opened(ByVal sender As Object, ByVal e As System.EventArgs) Handles _MasterDevice.Opened
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} handling master device opening event"
            Me.ConfigInfo = New DeviceUnderTest
            ' initial resistance must be first.
            Me.BindInitialResistance(New MeterColdResistance(Me.MasterDevice.StatusSubsystem, Me.ConfigInfo.InitialResistance, ThermalTransientMeterEntity.InitialResistance))
            Me.BindFinalResistance(New MeterColdResistance(Me.MasterDevice.StatusSubsystem, Me.ConfigInfo.FinalResistance, ThermalTransientMeterEntity.FinalResistance))
            Me.BindShuntResistance(New ShuntResistance())
            Me.BindThermalTransientEstimator(New ThermalTransientEstimator(Me.MasterDevice.StatusSubsystem))
            Me.BindThermalTransient(New MeterThermalTransient(Me.MasterDevice.StatusSubsystem, Me.ConfigInfo.ThermalTransient))
            Me.MasterDevice.AddSubsystem(Me.ThermalTransient)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Master device initializing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MasterDevice_Initializing(sender As Object, e As ComponentModel.CancelEventArgs) Handles _MasterDevice.Initializing
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} initializing master device"
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Master device initialized. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MasterDevice_Initialized(sender As Object, e As EventArgs) Handles _MasterDevice.Initialized
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} initialized master device"
            Me.ApplySettings()
            Me.ShuntResistance.AddListeners(Me.Talker)
            ' this is already done.  Me.MasterDevice.AddListeners(Me.Talker)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Raises the system. component model. property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As K2600Device, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(K2600.K2600Device.IsDeviceOpen)
                If sender.IsDeviceOpen Then
                    Me.PublishInfo("{0} open;. ", sender.ResourceNameCaption)
                Else
                    Me.PublishInfo("{0} close;. ", sender.ResourceNameCaption)
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _MasterDevice for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MasterDevice_PropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _MasterDevice.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} handling master device {e?.PropertyName} property changed event"
            Me.OnPropertyChanged(TryCast(sender, K2600Device), e?.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Sets values to their known clear execution state. </summary>
    ''' <remarks> This erases the last reading. </remarks>
    Public Overridable Sub DefineClearExecutionState()

        Me.PublishVerbose("Clearing device under test execution state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ConfigInfo.DefineClearExecutionState()

        Me.PublishVerbose("Clearing shunt execution state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ShuntResistance.DefineClearExecutionState()

        Me.PublishVerbose("Displaying title;. ")
        Me.MasterDevice.DisplaySubsystem.DisplayTitle("TTM 2016", "Integrated Scientific Resources")

        isr.Core.ApplianceBase.DoEvents()

    End Sub

    ''' <summary>
    ''' Clears the queues and resets all registers to zero. Sets system properties to the their Clear
    ''' Execution (CLS) default values.
    ''' </summary>
    ''' <remarks> *CLS. </remarks>
    Public Sub ClearExecutionState()
        Me.PublishVerbose("Clearing master device execution state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.MasterDevice.ClearExecutionState()
        Me.DefineClearExecutionState()
    End Sub

    ''' <summary> Sets the known initial post reset state. </summary>
    ''' <remarks> Use this method to customize the reset. </remarks>
    Public Overridable Sub InitKnownState()
        Me.PublishVerbose("Initializing part configuration  to known state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ConfigInfo.InitKnownState()

        Me.PublishVerbose("Initializing shunt to known state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ShuntResistance.InitKnownState()

        isr.Core.ApplianceBase.DoEvents()
        Me.PublishVerbose("Initializing master device to known state;. ")
        Me.MasterDevice.InitKnownState()

    End Sub

    ''' <summary> Sets the known preset state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub PresetKnownState()
        Me.ConfigInfo.PresetKnownState()
    End Sub

    ''' <summary> Preforms a full reset, initialize and clear. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub ResetClear()

        Me.ResetKnownState()
        Me.InitKnownState()
        Me.ClearExecutionState()

    End Sub

    ''' <summary> Sets the known reset (default) state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub ResetKnownState()

        Me.PublishVerbose("Resetting part configuration to known state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ConfigInfo.ResetKnownState()

        isr.Core.ApplianceBase.DoEvents()
        Me.PublishVerbose("Resetting shunt to known state;. ")
        isr.Core.ApplianceBase.DoEvents()
        Me.ShuntResistance.ResetKnownState()

        isr.Core.ApplianceBase.DoEvents()
        Me.PublishVerbose("Resetting master device to known state;. ")
        Me.MasterDevice.ResetKnownState()

        isr.Core.ApplianceBase.DoEvents()
        Me._IsStarting = True
    End Sub

#End Region

#Region " SUBSYSTEMS: MEASUREMENT ELEMENTS "

    ''' <summary>
    ''' Gets or sets reference to the <see cref="MeterColdResistance">meter initial cold
    ''' resistance</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The initial resistance. </value>
    Public ReadOnly Property InitialResistance As MeterColdResistance

    ''' <summary> Binds the initial resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindInitialResistance(ByVal subsystem As MeterColdResistance)
        If Me.InitialResistance IsNot Nothing Then
            Me.MasterDevice.RemoveSubsystem(Me.InitialResistance)
            Me._InitialResistance = Nothing
        End If
        Me._InitialResistance = subsystem
        If Me.InitialResistance IsNot Nothing Then
            Me.MasterDevice.AddSubsystem(Me.InitialResistance)
        End If
    End Sub

    ''' <summary>
    ''' Gets or sets reference to the <see cref="MeterColdResistance">meter Final cold
    ''' resistance</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The Final resistance. </value>
    Public ReadOnly Property FinalResistance As MeterColdResistance

    ''' <summary> Binds the Final resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindFinalResistance(ByVal subsystem As MeterColdResistance)
        If Me.FinalResistance IsNot Nothing Then
            Me.MasterDevice.RemoveSubsystem(Me.FinalResistance)
            Me._FinalResistance = Nothing
        End If
        Me._FinalResistance = subsystem
        If Me.FinalResistance IsNot Nothing Then
            Me.MasterDevice.AddSubsystem(Me.FinalResistance)
        End If
    End Sub

    ''' <summary>
    ''' Gets or sets reference to the <see cref="ShuntResistance">Shunt resistance</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The shunt resistance. </value>
    Public ReadOnly Property ShuntResistance As ShuntResistance

    ''' <summary> Binds the Shunt resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindShuntResistance(ByVal subsystem As ShuntResistance)
        If Me.ShuntResistance IsNot Nothing Then
            Me._ShuntResistance = Nothing
        End If
        Me._ShuntResistance = subsystem
    End Sub

    ''' <summary>
    ''' Gets or sets reference to the <see cref="MeterThermalTransient">meter thermal transient</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The thermal transient. </value>
    Public ReadOnly Property ThermalTransient As MeterThermalTransient

    ''' <summary> Binds the Final resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindThermalTransient(ByVal subsystem As MeterThermalTransient)
        If Me.ThermalTransient IsNot Nothing Then
            Me.MasterDevice.RemoveSubsystem(Me.ThermalTransient)
            Me._ThermalTransient = Nothing
        End If
        Me._ThermalTransient = subsystem
        If Me.ThermalTransient IsNot Nothing Then
            Me.MasterDevice.AddSubsystem(Me.ThermalTransient)
        End If
    End Sub

    ''' <summary>
    ''' Gets or sets reference to the <see cref="ThermalTransientEstimator">thermal transient
    ''' estiamtor</see>
    ''' </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> The thermal transient estimator. </value>
    Public ReadOnly Property ThermalTransientEstimator As ThermalTransientEstimator

    ''' <summary> Binds the Final resistance subsystem. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="subsystem"> The subsystem. </param>
    Private Sub BindThermalTransientEstimator(ByVal subsystem As ThermalTransientEstimator)
        If Me.ThermalTransientEstimator IsNot Nothing Then
            Me.MasterDevice.RemoveSubsystem(Me.ThermalTransientEstimator)
            Me._ThermalTransientEstimator = Nothing
        End If
        Me._ThermalTransientEstimator = subsystem
        If Me.ThermalTransientEstimator IsNot Nothing Then
            Me.MasterDevice.AddSubsystem(Me.ThermalTransientEstimator)
        End If
    End Sub

#End Region

#Region " CONFIGURE"

    ''' <summary> Gets or sets the <see cref="ConfigInfo">Device under test</see>. </summary>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <value> Information describing the configuration. </value>
    Public Property ConfigInfo As DeviceUnderTest

    ''' <summary> Configure part information. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="configurationInformation"> The <see cref="ConfigInfo">Device under test</see>. </param>
    Public Sub ConfigurePartInfo(ByVal configurationInformation As DeviceUnderTest)
        If configurationInformation Is Nothing Then Throw New ArgumentNullException(NameOf(configurationInformation))
        Me.ConfigInfo.PartNumber = configurationInformation.PartNumber
        Me.ConfigInfo.OperatorId = configurationInformation.OperatorId
        Me.ConfigInfo.LotId = configurationInformation.LotId

        Me.ConfigInfo.ContactCheckEnabled = configurationInformation.ContactCheckEnabled
        Me.ConfigInfo.ContactCheckThreshold = configurationInformation.ContactCheckThreshold
    End Sub

    ''' <summary> Configures the given device under test. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="configurationInformation"> The <see cref="ConfigInfo">Device under test</see>. </param>
    Public Sub Configure(ByVal configurationInformation As DeviceUnderTest)
        If configurationInformation Is Nothing Then
            Throw New ArgumentNullException(NameOf(configurationInformation))
        ElseIf configurationInformation.InitialResistance Is Nothing Then
            Throw New InvalidOperationException("Initial Resistance null detected in device under test.")
        ElseIf configurationInformation.ThermalTransient Is Nothing Then
            Throw New InvalidOperationException("Thermal Transient null detected in device under test.")
        End If
        If Not Me.ConfigInfo.SourceMeasureUnitEquals(configurationInformation) Then
            If Not Me.InitialResistance.SourceMeasureUnitExists(Me.ConfigInfo.InitialResistance.SourceMeasureUnit) Then
                Throw New InvalidOperationException(String.Format("Source measure unit name {0} is invalid.",
                                                                  Me.ConfigInfo.InitialResistance.SourceMeasureUnit))
            End If
        End If
        Dim details As String = String.Empty
        If Not Ttm.ThermalTransient.ValidateVoltageLimit(configurationInformation.ThermalTransient.VoltageLimit,
                                                         configurationInformation.InitialResistance.HighLimit, configurationInformation.ThermalTransient.CurrentLevel, configurationInformation.ThermalTransient.AllowedVoltageChange, details) Then
            Throw New ArgumentOutOfRangeException(NameOf(configurationInformation), details)
        End If

        Me.ConfigurePartInfo(configurationInformation)
        Me.InitialResistance.Configure(configurationInformation.InitialResistance)
        Me.FinalResistance.Configure(configurationInformation.FinalResistance)
        Me.ThermalTransient.Configure(configurationInformation.ThermalTransient)
    End Sub

    ''' <summary> Configures the given device under test for changed values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">       Thrown when one or more required arguments
    '''                                                are null. </exception>
    ''' <exception cref="InvalidOperationException">   Thrown when the requested operation is
    '''                                                invalid. </exception>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="configurationInformation"> The <see cref="ConfigInfo">Device under test</see>. </param>
    Public Sub ConfigureChanged(ByVal configurationInformation As DeviceUnderTest)
        If configurationInformation Is Nothing Then
            Throw New ArgumentNullException(NameOf(configurationInformation))
        ElseIf configurationInformation.InitialResistance Is Nothing Then
            Throw New InvalidOperationException("Initial Resistance null detected in device under test.")
        ElseIf configurationInformation.ThermalTransient Is Nothing Then
            Throw New InvalidOperationException("Thermal Transient null detected in device under test.")
        End If
        If Not Me.ConfigInfo.SourceMeasureUnitEquals(configurationInformation) Then
            If Not Me.InitialResistance.SourceMeasureUnitExists(Me.ConfigInfo.InitialResistance.SourceMeasureUnit) Then
                Throw New InvalidOperationException(String.Format("Source measure unit name {0} is invalid.",
                                                                  Me.ConfigInfo.InitialResistance.SourceMeasureUnit))
            End If
        End If
        Dim details As String = String.Empty
        If Not Ttm.ThermalTransient.ValidateVoltageLimit(configurationInformation.ThermalTransient.VoltageLimit,
                                                            configurationInformation.InitialResistance.HighLimit, configurationInformation.ThermalTransient.CurrentLevel, configurationInformation.ThermalTransient.AllowedVoltageChange, details) Then
            Throw New ArgumentOutOfRangeException(NameOf(configurationInformation), details)
        End If
        Me.ConfigurePartInfo(configurationInformation)
        Me.InitialResistance.ConfigureChanged(configurationInformation.InitialResistance)
        Me.FinalResistance.ConfigureChanged(configurationInformation.FinalResistance)
        Me.ThermalTransient.ConfigureChanged(configurationInformation.ThermalTransient)
    End Sub

#End Region

#Region " TTM FRAMEWORK: SOURCE CHANNEL "

    ''' <summary> Source measure unit. </summary>
    Private _SourceMeasureUnit As String

    ''' <summary> Gets or sets (protected) the source measure unit "smua" or "smub". </summary>
    ''' <value> The source measure unit. </value>
    Public Property SourceMeasureUnit() As String
        Get
            Return Me._SourceMeasureUnit
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.SourceMeasureUnit) Then
                Me._SourceMeasureUnit = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Programs the Source Measure Unit. Does not read back from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="value"> the Source Measure Unit, e.g., 'smua' or 'smub'. </param>
    ''' <returns> The Source Measure Unit, e.g., 'smua' or 'smub'. </returns>
    Public Function WriteSourceMeasureUnit(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then
            Throw New ArgumentNullException(value)
        End If
        Dim unitNumber As String = value.Substring(value.Length - 1)
        Me.MasterDevice.SourceMeasureUnit.UnitNumber = unitNumber
        Me.InitialResistance.WriteSourceMeasureUnit(value)
        Me.FinalResistance.WriteSourceMeasureUnit(value)
        Me.ThermalTransient.WriteSourceMeasureUnit(value)
        Me.SourceMeasureUnit = value
        Return Me.SourceMeasureUnit
    End Function

#End Region

#Region " TTM FRAMEWORK: METERS FIRMWARE "

    ''' <summary> Checks if the firmware version command exists. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' <c>True</c> if the firmware version command exists; otherwise, <c>False</c>.
    ''' </returns>
    Public Function FirmwareVersionQueryCommandExists() As Boolean
        Return Me.IsSessionOpen AndAlso Not Me.MasterDevice.Session.IsNil(TtmSyntax.Meters.VersionQueryCommand.TrimEnd("()".ToCharArray))
    End Function

    ''' <summary> The firmware released version. </summary>
    Private _FirmwareReleasedVersion As String

    ''' <summary> Gets or sets the version of the current firmware release. </summary>
    ''' <value> The firmware released version. </value>
    Public Property FirmwareReleasedVersion() As String
        Get
            If String.IsNullOrWhiteSpace(Me._FirmwareReleasedVersion) Then
                Me.QueryFirmwareVersion()
            End If
            Return Me._FirmwareReleasedVersion
        End Get
        Protected Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not value.Equals(Me.FirmwareReleasedVersion) Then
                Me._FirmwareReleasedVersion = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary>
    ''' Queries the embedded firmware version from a remote node and saves it to
    ''' <see cref="FirmwareReleasedVersion">the firmware version cache.</see>
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The firmware version. </returns>
    Public Function QueryFirmwareVersion() As String
        Me.FirmwareReleasedVersion = If(Me.FirmwareVersionQueryCommandExists(),
            Me.MasterDevice.Session.QueryTrimEnd(TtmSyntax.Meters.VersionQueryCommand), Tsp.Syntax.Lua.NilValue)
        Return Me.FirmwareReleasedVersion
    End Function

#End Region

#Region " TTM FRAMEWORK: SHUNT "

    ''' <summary> Configures the meter for making Shunt Resistance measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The shunt resistance. </param>
    Public Sub ConfigureShuntResistance(ByVal resistance As ShuntResistanceBase)

        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))

        Me.PublishVerbose("Configuring shunt resistance measurement;. ")

        Me.PublishVerbose("Setting {0} to DC current source;. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.SourceSubsystem.ApplySourceFunction(Tsp.SourceFunctionMode.CurrentDC)

        Me.PublishVerbose("Setting {0} to DC current range {1};. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.CurrentSourceSubsystem.ApplyRange(resistance.CurrentRange)
        Me.ShuntResistance.CurrentRange = Me.MasterDevice.CurrentSourceSubsystem.Range.GetValueOrDefault(0)

        Me.PublishVerbose("Setting {0} to DC current Level {1};. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.CurrentSourceSubsystem.ApplyLevel(resistance.CurrentLevel)
        Me.ShuntResistance.CurrentLevel = Me.MasterDevice.CurrentSourceSubsystem.Level.GetValueOrDefault(0)

        Me.PublishVerbose("Setting {0} to DC current Voltage Limit {1};. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.CurrentSourceSubsystem.ApplyVoltageLimit(resistance.VoltageLimit)
        Me.ShuntResistance.VoltageLimit = Me.MasterDevice.CurrentSourceSubsystem.VoltageLimit.GetValueOrDefault(0)

        Me.PublishVerbose("Setting {0} to four wire sense;. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.SenseSubsystem.ApplySenseMode(SenseActionMode.Remote)

        Me.PublishVerbose("Setting {0} to auto voltage range;. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.MeasureVoltageSubsystem.ApplyAutoRangeVoltageEnabled(True)

        Me.MasterDevice.DisplaySubsystem.ClearDisplay()
        Me.MasterDevice.DisplaySubsystem.DisplayLine(2, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                      "SrcI:{0}A LimV:{1}V",
                                                                      Me.ShuntResistance.CurrentLevel,
                                                                      Me.ShuntResistance.VoltageLimit))
        Me.MasterDevice.StatusSubsystem.CheckThrowDeviceException(False, "configuring shunt resistance measurement;. ")

        Me.ShuntResistance.CheckThrowUnequalConfiguration(resistance)

    End Sub

    ''' <summary> Apply changed Shunt Resistance configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The shunt resistance. </param>
    Public Sub ConfigureShuntResistanceChanged(ByVal resistance As ShuntResistanceBase)

        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))

        Me.PublishVerbose("Setting {0} to DC current source;. ", Me.MasterDevice.SourceMeasureUnit)
        Me.MasterDevice.SourceSubsystem.ApplySourceFunction(Tsp.SourceFunctionMode.CurrentDC)

        If Not Me.ShuntResistance.ConfigurationEquals(resistance) Then

            Me.PublishVerbose("Configuring shunt resistance measurement;. ")

            If Not Me.ShuntResistance.CurrentRange.Equals(resistance.CurrentRange) Then
                Me.PublishVerbose("Setting {0} to DC current range {1};. ", Me.MasterDevice.SourceMeasureUnit)
                Me.MasterDevice.CurrentSourceSubsystem.ApplyRange(resistance.CurrentRange)
                Me.ShuntResistance.CurrentRange = Me.MasterDevice.CurrentSourceSubsystem.Range.GetValueOrDefault(0)
            End If

            If Not Me.ShuntResistance.CurrentLevel.Equals(resistance.CurrentLevel) Then
                Me.PublishVerbose("Setting {0} to DC current Level {1};. ", Me.MasterDevice.SourceMeasureUnit)
                Me.MasterDevice.CurrentSourceSubsystem.ApplyLevel(resistance.CurrentLevel)
                Me.ShuntResistance.CurrentLevel = Me.MasterDevice.CurrentSourceSubsystem.Level.GetValueOrDefault(0)
            End If

            If Not Me.ShuntResistance.VoltageLimit.Equals(resistance.VoltageLimit) Then
                Me.PublishVerbose("Setting {0} to DC current Voltage Limit {1};. ", Me.MasterDevice.SourceMeasureUnit)
                Me.MasterDevice.CurrentSourceSubsystem.ApplyVoltageLimit(resistance.VoltageLimit)
                Me.ShuntResistance.VoltageLimit = Me.MasterDevice.CurrentSourceSubsystem.VoltageLimit.GetValueOrDefault(0)
            End If

            Me.PublishVerbose("Setting {0} to four wire sense;. ", Me.MasterDevice.SourceMeasureUnit)
            Me.MasterDevice.SenseSubsystem.ApplySenseMode(SenseActionMode.Remote)

            Me.PublishVerbose("Setting {0} to auto voltage range;. ", Me.MasterDevice.SourceMeasureUnit)
            Me.MasterDevice.MeasureVoltageSubsystem.ApplyAutoRangeVoltageEnabled(True)
        End If

        Me.MasterDevice.DisplaySubsystem.ClearDisplay()
        Me.MasterDevice.DisplaySubsystem.DisplayLine(2, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                      "SrcI:{0}A LimV:{1}V",
                                                                      Me.ShuntResistance.CurrentLevel,
                                                                      Me.ShuntResistance.VoltageLimit))
        Me.MasterDevice.StatusSubsystem.CheckThrowDeviceException(False, "configuring shunt resistance measurement;. ")

        Me.ShuntResistance.CheckThrowUnequalConfiguration(resistance)

    End Sub

    ''' <summary> Measures the Shunt resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resistance"> The shunt resistance. </param>
    Public Sub MeasureShuntResistance(ByVal resistance As ResistanceMeasureBase)
        If resistance Is Nothing Then Throw New ArgumentNullException(NameOf(resistance))
        Me.ShuntResistance.HighLimit = resistance.HighLimit
        Me.ShuntResistance.LowLimit = resistance.LowLimit
        Me.MasterDevice.MeasureResistanceSubsystem.Measure()
        Dim reading As String = Me.MasterDevice.MeasureResistanceSubsystem.Reading
        Me.PublishVerbose("Parsing shunt resistance reading {0};. ", reading)
        Me.MasterDevice.StatusSubsystem.CheckThrowDeviceException(False, "measuring shunt resistance;. ")
        Me.ShuntResistance.ParseReading(reading, MeasurementOutcomes.None)
        resistance.ParseReading(reading, MeasurementOutcomes.None)
        Me.MasterDevice.DisplaySubsystem.DisplayLine(1, reading.Trim)
        Me.MasterDevice.DisplaySubsystem.DisplayCharacter(1, reading.Length, 18)
        Me.ShuntResistance.MeasurementAvailable = True
    End Sub

#End Region

#Region " TTM FRAMEWORK: MEASURE "

    ''' <summary> Makes all measurements. Reading is done after all measurements are done. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub Measure()
        If Me.IsSessionOpen Then
            Me.MasterDevice.Session.WriteLine("_G.ttm.measure()  waitcomplete() ")
        End If
    End Sub

#End Region

#Region " TTM FRAMEWORK: TRIGGER "

    ''' <summary> True if the meter was enabled to respond to trigger events. </summary>
    Private _IsAwaitingTrigger As Boolean

    ''' <summary> Abort triggered measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub AbortTriggerSequenceIf()
        If Me.IsSessionOpen AndAlso Me._IsAwaitingTrigger AndAlso Me.TriggerSequencer IsNot Nothing AndAlso Me.MasterDevice IsNot Nothing Then
            Me.TriggerSequencer.RestartSignal = TriggerSequenceSignal.Stop
            Me.MasterDevice.Session.AssertTrigger()
            ' allow time for the measurement to terminate.
            Threading.Thread.Sleep(CInt(1000 * Me.ThermalTransient.ThermalTransient.PostTransientDelay) + 400)
            Me._IsAwaitingTrigger = False
            Me.MasterDevice.Session.DiscardUnreadData()
        End If
    End Sub

    ''' <summary> true if this object is starting. </summary>
    Private _IsStarting As Boolean

    ''' <summary>
    ''' Primes the instrument to wait for a trigger. This clears the digital outputs and loops until
    ''' trigger or external *TRG command.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="isStarting"> When true, displays the title and locks the local key. </param>
    Private Sub PrepareForTriggerThis(ByVal isStarting As Boolean)
        Me.MasterDevice.Session.MessageAvailableBit = VI.Pith.ServiceRequests.MessageAvailable
        Me.MasterDevice.Session.EnableServiceRequestWaitComplete()
        Me.MasterDevice.Session.WriteLine("prepareForTrigger({0},'{1}') waitcomplete() ", IIf(isStarting, "true", "false"), "OPC")
    End Sub

    ''' <summary>
    ''' Primes the instrument to wait for a trigger. This clears the digital outputs and loops until
    ''' trigger or external TRG command.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub PrepareForTrigger()
        Me._IsStarting = False
        Me._IsAwaitingTrigger = True
        Me.PrepareForTriggerThis(Me._IsStarting)
        Me._IsStarting = False
    End Sub

    ''' <summary> True if measurement completed. </summary>
    Private _MeasurementCompleted As Boolean

    ''' <summary> Gets or sets the measurement completed. </summary>
    ''' <value> The measurement completed. </value>
    Public Property MeasurementCompleted As Boolean
        Get
            Return Me._MeasurementCompleted
        End Get
        Set(ByVal value As Boolean)
            If Not value.Equals(Me.MeasurementCompleted) Then
                Me._MeasurementCompleted = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Monitors the bus to detect completion of measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> True if measurement completed, false if not. </returns>
    Public Function IsMeasurementCompleted() As Boolean
        Me.MasterDevice.Session.ReadStatusRegister()
        Me.MeasurementCompleted = Me.MasterDevice.Session.MessageAvailable '  Me.MasterDevice.Session.IsMessageAvailable(TimeSpan.FromMilliseconds(50), 1)
        Return Me.MeasurementCompleted
    End Function

    ''' <summary> Reads the measurements from the instrument. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="initialResistance"> The initial resistance. </param>
    ''' <param name="finalResistance">   The final resistance. </param>
    ''' <param name="thermalTransient">  The thermal transient. </param>
    Public Sub ReadMeasurements(ByVal initialResistance As ResistanceMeasureBase, ByVal finalResistance As ResistanceMeasureBase,
                                     ByVal thermalTransient As ResistanceMeasureBase)
        Me.PublishVerbose("'{0}' reading measurements;. ", Me.ResourceName)
        Me._IsAwaitingTrigger = False
        Me.MasterDevice.Session.DiscardUnreadData()
        Me.InitialResistance.ReadResistance(initialResistance)
        Me.FinalResistance.ReadResistance(finalResistance)
        Me.ThermalTransient.ReadThermalTransient(thermalTransient)
    End Sub

#End Region

#Region " CHECK CONTACTS "

    ''' <summary> Checks contact resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="isr.Core.OperationFailedException"> Thrown when operation failed to execute. </exception>
    ''' <param name="threshold"> The threshold. </param>
    Public Sub CheckContacts(ByVal threshold As Integer)
        Me.MasterDevice.ContactSubsystem.CheckContacts(threshold)
        If Not Me.MasterDevice.ContactSubsystem.ContactCheckOkay.HasValue Then
            Throw New isr.Core.OperationFailedException("Failed Measuring contacts;. ")
        ElseIf Not Me.MasterDevice.ContactSubsystem.ContactCheckOkay.Value Then
            Throw New isr.Core.OperationFailedException("High contact resistances;. Values: '{0}'", Me.MasterDevice.ContactSubsystem.ContactResistances)
        End If
    End Sub

    ''' <summary> Checks contact resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="threshold"> The threshold. </param>
    ''' <param name="details">   [in,out] The details. </param>
    ''' <returns> <c>True</c> if contacts checked okay. </returns>
    Public Function TryCheckContacts(ByVal threshold As Integer, ByRef details As String) As Boolean
        Me.MasterDevice.ContactSubsystem.CheckContacts(threshold)
        If Not Me.MasterDevice.ContactSubsystem.ContactCheckOkay.HasValue Then
            details = "Failed Measuring contacts;. "
            Return False
        ElseIf Me.MasterDevice.ContactSubsystem.ContactCheckOkay.Value Then
            Return True
        Else
            details = String.Format("High contact resistances;. Values: '{0}'", Me.MasterDevice.ContactSubsystem.ContactResistances)
            Return False
        End If
    End Function

#End Region

#Region " PART MEASUREMENTS "

    ''' <summary> Gets or sets the part. </summary>
    ''' <remarks>
    ''' The part represents the actual device under test. These are the values that gets displayed on
    ''' the header and saved.
    ''' </remarks>
    ''' <value> The part. </value>
    Public Property Part As DeviceUnderTest

    ''' <summary> Measures and fetches the initial resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The cold resistance entity where the results are saved. </param>
    Public Sub MeasureInitialResistance(ByVal value As ColdResistance)
        ' clears display if not in measurement state
        Me.PublishVerbose("Clearing meter display;. ")
        Me.MasterDevice.DisplaySubsystem.ClearDisplayMeasurement()
        Dim contactsOkay As Boolean
        Dim details As String = String.Empty
        If Me.Part.ContactCheckEnabled Then
            Me.PublishVerbose("Checking contacts;. ")
            contactsOkay = Me.TryCheckContacts(Me.Part.ContactCheckThreshold, details)
        Else
            contactsOkay = True
        End If
        If contactsOkay Then
            If Me.MasterDevice.Enabled Then
                Me.PublishVerbose("Measuring Initial Resistance...;. ")
                Me.InitialResistance.Measure(value)
            Else
                Me.PublishVerbose("Emulating Initial Resistance...;. ")
                Me.InitialResistance.EmulateResistance(value)
            End If
            Me.PublishVerbose("Got Initial Resistance;. ")
        Else
            Me.Part.InitialResistance.ApplyContactCheckOutcome(MeasurementOutcomes.FailedContactCheck)
            Me.PublishWarning(details)
        End If
    End Sub

    ''' <summary> Measures and fetches the Final resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The cold resistance entity where the results are saved. </param>
    Public Sub MeasureFinalResistance(ByVal value As ColdResistance)
        If Me.MasterDevice.Enabled Then
            ' clears display if not in measurement state
            Me.PublishVerbose("Clearing meter display;. ")
            Me.MasterDevice.DisplaySubsystem.ClearDisplayMeasurement()
            Me.PublishVerbose("Measuring Final Resistance...;. ")
            Me.FinalResistance.Measure(value)
        Else
            Me.PublishVerbose("Emulating Final Resistance...;. ")
            Me.FinalResistance.EmulateResistance(value)
        End If
        Me.PublishVerbose("Got Final Resistance;. ")
    End Sub

    ''' <summary> Measures and fetches the thermal transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The cold resistance entity where the results are saved. </param>
    Public Sub MeasureThermalTransient(ByVal value As ThermalTransient)
        ' clears display if not in measurement state
        Me.PublishVerbose("Clearing meter display;. ")
        Me.MasterDevice.DisplaySubsystem.ClearDisplayMeasurement()
        If Me.MasterDevice.Enabled Then
            Me.PublishVerbose("Measuring Thermal Transient...;. ")
            Me.ThermalTransient.Measure(value)
        Else
            Me.PublishVerbose("Emulating Thermal Transient...;. ")
            Me.ThermalTransient.EmulateThermalTransient(value)
        End If
        If Me.MeasureSequencer IsNot Nothing Then
            Me.MeasureSequencer.StartFinalResistanceTime(TimeSpan.FromMilliseconds(1000 * Me.Part.ThermalTransient.PostTransientDelay))
        End If
        Me.PublishVerbose("Got Thermal Transient;. ")
    End Sub

#End Region

#Region " SEQUENCED MEASUREMENTS "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _MeasureSequencer As MeasureSequencer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the sequencer. </summary>
    ''' <value> The sequencer. </value>
    Public Property MeasureSequencer As MeasureSequencer
        Get
            Return Me._MeasureSequencer
        End Get
        Protected Set(value As MeasureSequencer)
            Me._MeasureSequencer = value
        End Set
    End Property

    ''' <summary> Handles the measure sequencer property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As MeasureSequencer, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.MeasureSequencer.MeasurementSequenceState)
                Me.OnMeasurementSequenceStateChanged(sender.MeasurementSequenceState)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _MeasureSequencer for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSequencer_PropertyChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _MeasureSequencer.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} handling measure sequencer {e?.PropertyName} property changed event"
            Me.OnPropertyChanged(TryCast(sender, MeasureSequencer), e?.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="currentState"> The current state. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnMeasurementSequenceStateChanged(ByVal currentState As MeasurementSequenceState)

        Select Case currentState

            Case MeasurementSequenceState.Idle

                ' Waiting for the step signal to start.
                Me.PublishVerbose("Ready to start a measurement sequence;. ")

            Case MeasurementSequenceState.Aborted

                Me.PublishInfo("Measurement sequence aborted;. ")
                ' step to the idle state.
                Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)

            Case MeasurementSequenceState.Failed

                Me.PublishInfo("Measurement sequence failed;. ")
                If Me.IsSessionOpen Then
                    Try
                        ' flush the buffer so a time out error that might leave stuff will clear the buffers.
                        Me.MasterDevice.Session.DiscardUnreadData()
                    Catch
                    End Try
                End If
                ' step to the aborted state.
                Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)

            Case MeasurementSequenceState.Completed

                Me.PublishVerbose("Measurement sequence completed;. ")

                ' step to the idle state.
                Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)

            Case MeasurementSequenceState.Starting

                Me.PublishVerbose("Measurement sequence starting;. ")

                Me.Part.ClearMeasurements()

                ' step to the Measure Initial Resistance state.
                Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)

            Case MeasurementSequenceState.MeasureInitialResistance

                Dim activity As String = String.Empty
                Try
                    activity = $"{Me.ResourceName} Measuring initial resistance" : Me.PublishVerbose($"{activity};. ")
                    Me.MeasureInitialResistance(Me.Part.InitialResistance)
                    If Me.Part.InitialResistance.Outcome = MeasurementOutcomes.PartPassed Then
                        ' step to the Measure Thermal Transient.
                        Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)
                    Else
                        ' if failure, do not continue.
                        Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Failure)
                    End If
                Catch ex As Exception
                    Me.PublishException(activity, ex)
                    Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Failure)
                End Try

            Case MeasurementSequenceState.MeasureThermalTransient

                Dim activity As String = String.Empty
                Try
                    activity = $"{Me.ResourceName} measuring thermal resistance" : Me.PublishVerbose($"{activity};. ")
                    Me.MeasureThermalTransient(Me.Part.ThermalTransient)
                    ' step to the post transient delay
                    Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)

                Catch ex As Exception
                    Me.PublishException(activity, ex)
                    Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Failure)
                End Try

            Case MeasurementSequenceState.PostTransientPause

                Me.PublishVerbose("Delaying final resistance measurement;. ")

            Case MeasurementSequenceState.MeasureFinalResistance

                Dim activity As String = String.Empty
                Try
                    activity = $"{Me.ResourceName} measuring final resistance" : Me.PublishVerbose($"{activity};. ")
                    Me.MeasureFinalResistance(Me.Part.FinalResistance)
                    ' step to the completed state
                    Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Step)
                Catch ex As Exception
                    Me.PublishException(activity, ex)
                    Me.MeasureSequencer.Enqueue(MeasurementSequenceSignal.Failure)
                End Try

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & currentState.ToString)

        End Select

    End Sub

#End Region

#Region " TRIGGER SEQUENCE "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _TriggerSequencer As TriggerSequencer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the trigger sequencer. </summary>
    ''' <value> The sequencer. </value>
    Public Property TriggerSequencer As TriggerSequencer
        Get
            Return Me._TriggerSequencer
        End Get
        Set(value As TriggerSequencer)
            Me._TriggerSequencer = value
        End Set
    End Property

    ''' <summary> Handles the trigger sequencer property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As TriggerSequencer, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.TriggerSequencer.AssertRequested)
                If sender.AssertRequested Then
                    Me.MasterDevice.Session.AssertTrigger()
                End If
            Case NameOf(Ttm.TriggerSequencer.TriggerSequenceState)
                Me.OnTriggerSequenceStateChanged(sender.TriggerSequenceState)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _TriggerSequencer for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSequencer_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _TriggerSequencer.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.ResourceName} handling trigger sequence {e?.PropertyName} property changed event"
            Me.OnPropertyChanged(TryCast(sender, TriggerSequencer), e?.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Event handler. Called by  for  events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="currentState"> The current state. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnTriggerSequenceStateChanged(ByVal currentState As TriggerSequenceState)

        Select Case currentState

            Case TriggerSequenceState.Idle

                If Me.IsSessionOpen Then
                    Try
                        ' flush the buffer so a time out error that might leave stuff will clear the buffers.
                        Me.MasterDevice.Session.DiscardUnreadData()
                    Catch
                    End Try
                End If

                ' Waiting for the step signal to start.
                Me.PublishVerbose("Ready to start a trigger sequence;. ")

            Case TriggerSequenceState.Aborted

                Me.PublishInfo("Measurement sequence aborted;. ")

                ' step to the idle state.
                Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)

            Case TriggerSequenceState.Failed

                Me.PublishWarning("Measurement sequence failed;. ")
                ' step to the aborted state.
                Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)

            Case TriggerSequenceState.Stopped

                Me.PublishInfo("Measurement sequence stopped;. ")

                ' step to the idle state.
                Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)

            Case TriggerSequenceState.Starting

                Me.PublishVerbose("Measurement sequence starting;. ")
                Try
                    Me.PrepareForTrigger()
                    Me.PublishVerbose("Monitoring instrument for measurements;. ")
                    ' step to the Waiting state.
                    Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)
                Catch ex As Exception
                    Me.PublishWarning("Exception occurred preparing instrument for waiting for trigger;. {0}", ex.ToFullBlownString)
                    ' step to the failed state.
                    Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Failure)
                End Try

            Case TriggerSequenceState.WaitingForTrigger

                If Me.IsMeasurementCompleted Then
                    ' step to the reading state.
                    Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)
                ElseIf Me.TriggerSequencer.AssertRequested Then
                    Me.TriggerSequencer.AssertRequested = False
                    Me.MasterDevice.Session.AssertTrigger()
                End If

            Case TriggerSequenceState.MeasurementCompleted

                ' clear part measurements.
                Me.Part.ClearMeasurements()

                ' step to the reading state.
                Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)

            Case TriggerSequenceState.ReadingValues

                Try
                    Me.ReadMeasurements(Me.Part.InitialResistance, Me.Part.FinalResistance, Me.Part.ThermalTransient)
                    ' step to the starting state.
                    Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Step)
                Catch ex As Exception
                    Me.PublishWarning("Exception occurred reading measurements;. {0}", ex.ToFullBlownString)
                    ' step to the failed state.
                    Me.TriggerSequencer.Enqueue(TriggerSequenceSignal.Failure)
                End Try

            Case Else

                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & currentState.ToString)

        End Select

    End Sub

#End Region

#Region " MY SETTINGS "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.WindowsForms.EditConfiguration("TTM Settings Editor", My.MySettings.Default)
    End Sub

    ''' <summary> Applies the settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub ApplySettings()
        Dim settings As My.MySettings = My.MySettings.Default
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceLogLevel))
        Me.HandlePropertyChanged(settings, NameOf(My.MySettings.TraceShowLevel))
    End Sub

    ''' <summary> Handles the settings property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Overloads Sub HandlePropertyChanged(ByVal sender As My.MySettings, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(My.MySettings.TraceLogLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Logger, sender.TraceLogLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceLogLevel}")
            Case NameOf(My.MySettings.TraceShowLevel)
                Me.ApplyTalkerTraceLevel(Core.ListenerType.Display, sender.TraceShowLevel)
                Me.PublishInfo($"{propertyName} changed to {sender.TraceShowLevel}")
        End Select
    End Sub

    ''' <summary> My settings property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MySettings_PropertyChanged(sender As Object, e As ComponentModel.PropertyChangedEventArgs)
        If Me.IsDisposed OrElse sender Is Nothing OrElse e Is Nothing Then Return
        Dim activity As String = $"handling {NameOf(My.MySettings)}.{e.PropertyName} change"
        Try
            Me.HandlePropertyChanged(TryCast(sender, My.MySettings), e.PropertyName)
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

End Class
