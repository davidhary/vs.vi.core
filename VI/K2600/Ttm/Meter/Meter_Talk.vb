Imports isr.Core
Imports isr.VI.ExceptionExtensions

Partial Public Class Meter

#Region " TALKER "

    ''' <summary> Identifies talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

#End Region

#Region " CUSTOM FUNCTIONS "

    ''' <summary>
    ''' Removes the private listeners. Removes all listeners if the talker was not assigned.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub RemoveListeners()
        MyBase.RemoveListeners()
        Me.RemovePrivateListeners()
        Me.MasterDevice?.RemoveListeners()
        Me.ShuntResistance?.RemoveListeners()
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        ' this should apply only to the listeners associated with this form
        ' Not this: Me.Talker.ApplyListenerTraceLevel(listenerType, value)
        Me.ShuntResistance.ApplyListenerTraceLevel(listenerType, value)
        Me.MasterDevice.ApplyListenerTraceLevel(listenerType, value)
        MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        MyBase.ApplyTalkerTraceLevel(listenerType, value)
        If listenerType = ListenerType.Logger Then
            Me.NotifyPropertyChanged(NameOf(TraceLogLevel))
        Else
            Me.NotifyPropertyChanged(NameOf(TraceShowLevel))
        End If
        Me.ShuntResistance.ApplyTalkerTraceLevel(listenerType, value)
        Me.MasterDevice.ApplyTalkerTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker)
        MyBase.ApplyTalkerTraceLevels(talker)
        Me.MasterDevice.ApplyTalkerTraceLevels(talker)
        Me.ShuntResistance?.ApplyTalkerTraceLevels(talker)
    End Sub

    ''' <summary> Applies the talker listeners trace levels described by talker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker)
        MyBase.ApplyListenerTraceLevels(talker)
        Me.MasterDevice.ApplyListenerTraceLevels(talker)
        Me.ShuntResistance?.ApplyListenerTraceLevels(talker)
    End Sub

    ''' <summary> Adds subsystem listeners. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub AddSubsystemListeners()
        Me.ShuntResistance.AddListeners(Me.Talker)
        Me.MasterDevice.AddListeners(Me.Talker)
    End Sub

    ''' <summary> Clears the subsystem listeners. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overridable Sub RemoveSubsystemListeners()
        Me.MasterDevice.RemoveListeners()
    End Sub

#End Region

End Class

