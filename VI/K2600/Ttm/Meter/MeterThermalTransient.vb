Imports isr.Core.NumericExtensions

''' <summary> Thermal transient. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2013-12-23 </para>
''' </remarks>
Public Class MeterThermalTransient
    Inherits MeterSubsystemBase

#Region " CONSTRUCTION and CLONING "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="statusSubsystem">  The status subsystem. </param>
    ''' <param name="thermalTransient"> The thermal transient element. </param>
    Public Sub New(ByVal statusSubsystem As StatusSubsystemBase, ByVal thermalTransient As ThermalTransient)
        MyBase.New(statusSubsystem)
        Me.MeterEntity = ThermalTransientMeterEntity.Transient
        Me.ThermalTransient = thermalTransient
    End Sub

#End Region

#Region " I PRESETTABLE "

    ''' <summary> Clears known state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineClearExecutionState()
        Me.PublishVerbose("Clearing {0} thermal transient execution state;. ", Me.EntityName)
        MyBase.DefineClearExecutionState()
        Me.ThermalTransient.DefineClearExecutionState()
    End Sub

    ''' <summary>
    ''' Defines the know reset state (RST) by setting system properties to the their Reset (RST)
    ''' default values.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub DefineKnownResetState()
        Me.PublishVerbose("Resetting {0} thermal transient to known state;. ", Me.EntityName)
        isr.Core.ApplianceBase.DoEvents()
        MyBase.DefineKnownResetState()

        isr.Core.ApplianceBase.DoEvents()
        Me.ThermalTransient.ResetKnownState()
        isr.Core.ApplianceBase.DoEvents()
    End Sub

#End Region


#Region " DEVICE UNDER TEST: THERMAL TRANSIENT "

    ''' <summary> Gets the <see cref="ThermalTransient">part Thermal Transient</see>. </summary>
    ''' <value> The cold resistance. </value>
    Public Property ThermalTransient As ThermalTransient

    ''' <summary> Gets the <see cref="ResistanceMeasureBase">part resistance element</see>. </summary>
    ''' <value> The cold resistance. </value>
    Public Overrides ReadOnly Property Resistance As ResistanceMeasureBase
        Get
            Return Me.ThermalTransient
        End Get
    End Property

#End Region

#Region " PULSE DURATION "

    ''' <summary> Gets the duration of the pulse. </summary>
    ''' <value> The pulse duration. </value>
    Public ReadOnly Property PulseDuration As TimeSpan
        Get
            Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerSecond * Me.TracePoints.GetValueOrDefault(0) * Me.SamplingInterval.GetValueOrDefault(0)))
        End Get
    End Property
#End Region

#Region " APERTURE "

    ''' <summary> Writes the Aperture without reading back the value from the device. </summary>
    ''' <remarks>
    ''' This command sets the immediate output Aperture. The value is in Volts. The immediate
    ''' Aperture is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <param name="value"> The Aperture. </param>
    ''' <returns> The Aperture. </returns>
    Public Overrides Function WriteAperture(ByVal value As Double) As Double?
        MyBase.WriteAperture(value)
        ' update the maximum sampling rage.
        Me.Session.WriteLine("{0}:maxRateSetter()", Me.EntityName)
        ' and read the current period.
        Me.QuerySamplingInterval()
        Return Me.Aperture
    End Function

#End Region

#Region " MEDIAN FILTER SIZE "

    ''' <summary> The Median Filter Size. </summary>
    Private _MedianFilterSize As Integer?

    ''' <summary> Gets or sets the cached Median Filter Size. </summary>
    ''' <value> The Median Filter Size. </value>
    Public Property MedianFilterSize As Integer?
        Get
            Return Me._MedianFilterSize
        End Get
        Protected Set(ByVal value As Integer?)
            Me.ThermalTransient.MedianFilterSize = If(value, 0)

            If Not Nullable.Equals(Me.MedianFilterSize, value) Then
                Me._MedianFilterSize = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Median Filter Size. </summary>
    ''' <remarks>
    ''' This command set the immediate output Median Filter Size. The value is in Volts. The
    ''' immediate Median Filter Size is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <param name="value"> The Median Filter Size. </param>
    ''' <returns> The Median Filter Size. </returns>
    Public Function ApplyMedianFilterSize(ByVal value As Integer) As Integer?
        Me.WriteMedianFilterSize(value)
        Return Me.QueryMedianFilterSize
    End Function

    ''' <summary> Queries the Median Filter Size. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Median Filter Size or none if unknown. </returns>
    Public Function QueryMedianFilterSize() As Integer?
        Const printFormat As Integer = 1
        Me.MedianFilterSize = Me.Session.QueryPrint(Me.MedianFilterSize.GetValueOrDefault(5), printFormat, "{0}.medianFilterSize", Me.EntityName)
        Return Me.MedianFilterSize
    End Function

    ''' <summary>
    ''' Writes the Median Filter Size without reading back the value from the device.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the immediate output Median Filter Size. The value is in Volts. The
    ''' immediate MedianFilterSize is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Median Filter Size. </param>
    ''' <returns> The Median Filter Size. </returns>
    Public Function WriteMedianFilterSize(ByVal value As Integer) As Integer?
        Dim details As String = String.Empty
        If Not ThermalTransient.ValidateMedianFilterSize(value, details) Then
            Throw New ArgumentOutOfRangeException(NameOf(value), details)
        End If
        Me.Session.WriteLine("{0}:medianFilterSizeSetter({1})", Me.EntityName, value)
        Me.MedianFilterSize = value
        Return Me.MedianFilterSize
    End Function

#End Region

#Region " POST TRANSIENT DELAY "

    ''' <summary> The Post Transient Delay. </summary>
    Private _PostTransientDelay As Double?

    ''' <summary> Gets or sets the cached Source Post Transient Delay. </summary>
    ''' <value>
    ''' The Source Post Transient Delay. Actual Voltage depends on the power supply mode.
    ''' </value>
    Public Property PostTransientDelay As Double?
        Get
            Return Me._PostTransientDelay
        End Get
        Protected Set(ByVal value As Double?)
            Me.ThermalTransient.PostTransientDelay = If(value, 0)

            If Me.PostTransientDelay.Differs(value, 0.001) Then
                Me._PostTransientDelay = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the source Post Transient Delay. </summary>
    ''' <remarks>
    ''' This command set the immediate output Post Transient Delay. The value is in Amperes. The
    ''' immediate Post Transient Delay is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <param name="value"> The Post Transient Delay. </param>
    ''' <returns> The Source Post Transient Delay. </returns>
    Public Function ApplyPostTransientDelay(ByVal value As Double) As Double?
        Me.WritePostTransientDelay(value)
        Return Me.QueryPostTransientDelay
    End Function

    ''' <summary> Queries the Post Transient Delay. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Post Transient Delay or none if unknown. </returns>
    Public Function QueryPostTransientDelay() As Double?
        Const printFormat As Decimal = 9.3D
        Me.PostTransientDelay = Me.Session.QueryPrint(Me.PostTransientDelay.GetValueOrDefault(0.5), printFormat, "{0}.postTransientDelayGetter()", Me.BaseEntityName)
        Return Me.PostTransientDelay
    End Function

    ''' <summary>
    ''' Writes the source Post Transient Delay without reading back the value from the device.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the immediate output Post Transient Delay. The value is in Amperes. The
    ''' immediate PostTransientDelay is the output Voltage setting. At *RST, the Voltage values =
    ''' 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Post Transient Delay. </param>
    ''' <returns> The Source Post Transient Delay. </returns>
    Public Function WritePostTransientDelay(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Not ThermalTransient.ValidatePostTransientDelay(value, details) Then
            Throw New ArgumentOutOfRangeException(NameOf(value), details)
        End If
        Me.Session.WriteLine("{0}.postTransientDelaySetter({1})", Me.BaseEntityName, value)
        Me.PostTransientDelay = value
        Return Me.PostTransientDelay
    End Function

#End Region

#Region " SAMPLING INTERVAL "

    ''' <summary> The Sampling Interval. </summary>
    Private _SamplingInterval As Double?

    ''' <summary> Gets or sets the cached Sampling Interval. </summary>
    ''' <value> The Sampling Interval. </value>
    Public Property SamplingInterval As Double?
        Get
            Return Me._SamplingInterval
        End Get
        Protected Set(ByVal value As Double?)
            Me.ThermalTransient.SamplingInterval = If(value, 0)

            If Me.SamplingInterval.Differs(value, 0.000001) Then
                Me._SamplingInterval = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Sampling Interval. </summary>
    ''' <remarks>
    ''' This command set the immediate output Sampling Interval. The value is in Ohms. The immediate
    ''' Sampling Interval is the output Low setting. At *RST, the Low values = 0.
    ''' </remarks>
    ''' <param name="value"> The Sampling Interval. </param>
    ''' <returns> The Sampling Interval. </returns>
    Public Function ApplySamplingInterval(ByVal value As Double) As Double?
        Me.WriteSamplingInterval(value)
        Return Me.QuerySamplingInterval
    End Function

    ''' <summary> Queries the Sampling Interval. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Sampling Interval or none if unknown. </returns>
    Public Function QuerySamplingInterval() As Double?
        Const printFormat As Decimal = 9.6D
        Me.SamplingInterval = Me.Session.QueryPrint(Me.SamplingInterval.GetValueOrDefault(0.0001), printFormat, "{0}.period", Me.EntityName)
        Return Me.SamplingInterval
    End Function

    ''' <summary>
    ''' Writes the Sampling Interval without reading back the value from the device.
    ''' </summary>
    ''' <remarks>
    ''' This command sets the immediate output Sampling Interval. The value is in Ohms. The immediate
    ''' SamplingInterval is the output Low setting. At *RST, the Low values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Sampling Interval. </param>
    ''' <returns> The Sampling Interval. </returns>
    Public Function WriteSamplingInterval(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Not ThermalTransient.ValidateLimit(value, details) OrElse
            (Me.TracePoints.HasValue AndAlso Not ThermalTransient.ValidatePulseWidth(value, Me.TracePoints.Value, details)) Then
            Throw New ArgumentOutOfRangeException(NameOf(value), details)
        End If
        Me.Session.WriteLine("{0}:periodSetter({1})", Me.EntityName, value)
        Me.SamplingInterval = value
        Return Me.SamplingInterval
    End Function

#End Region

#Region " TRACE POINTS "

    ''' <summary> The Trace Points. </summary>
    Private _TracePoints As Integer?

    ''' <summary> Gets or sets the cached Trace Points. </summary>
    ''' <value> The Trace Points. </value>
    Public Property TracePoints As Integer?
        Get
            Return Me._TracePoints
        End Get
        Protected Set(ByVal value As Integer?)
            If Not Nullable.Equals(Me.TracePoints, value) Then
                Me._TracePoints = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Trace Points. </summary>
    ''' <remarks>
    ''' This command set the immediate output Trace Points. The value is in Volts. The immediate
    ''' Trace Points is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <param name="value"> The Trace Points. </param>
    ''' <returns> The Trace Points. </returns>
    Public Function ApplyTracePoints(ByVal value As Integer) As Integer?
        Me.WriteTracePoints(value)
        Return Me.QueryTracePoints
    End Function

    ''' <summary> Queries the Trace Points. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Trace Points or none if unknown. </returns>
    Public Function QueryTracePoints() As Integer?
        Const printFormat As Integer = 1
        Me.TracePoints = Me.Session.QueryPrint(Me.TracePoints.GetValueOrDefault(100), printFormat, "{0}.points", Me.EntityName)
        Return Me.TracePoints
    End Function

    ''' <summary> Writes the Trace Points without reading back the value from the device. </summary>
    ''' <remarks>
    ''' This command sets the immediate output Trace Points. The value is in Volts. The immediate
    ''' TracePoints is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Trace Points. </param>
    ''' <returns> The Trace Points. </returns>
    Public Function WriteTracePoints(ByVal value As Integer) As Integer?
        Dim details As String = String.Empty
        If Not ThermalTransient.ValidateTracePoints(value, details) OrElse
            (Me.SamplingInterval.HasValue AndAlso Not ThermalTransient.ValidatePulseWidth(Me.SamplingInterval.Value, value, details)) Then
            Throw New ArgumentOutOfRangeException(NameOf(value), details)
        End If
        Me.Session.WriteLine("{0}:pointsSetter({1})", Me.EntityName, value)
        Me.TracePoints = value
        Return Me.TracePoints
    End Function

#End Region

#Region " TIME SERIES "

    ''' <summary> The last trace. </summary>
    Private _LastTrace As String

    ''' <summary> Gets or sets (protected) the last trace. </summary>
    ''' <value> The last trace. </value>
    Public Property LastTrace() As String
        Get
            Return Me._LastTrace
        End Get
        Protected Set(ByVal value As String)
            Me._LastTrace = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets or sets the thermal transient time series. </summary>
    ''' <value> The last time series. </value>
    Public ReadOnly Property LastTimeSeries() As IList(Of System.Windows.Point)

#End Region

#Region " VOLTAGE CHANGE "

    ''' <summary> The Voltage Change. </summary>
    Private _VoltageChange As Double?

    ''' <summary> Gets or sets the cached Voltage Change. </summary>
    ''' <value> The Voltage Change. </value>
    Public Property VoltageChange As Double?
        Get
            Return Me._VoltageChange
        End Get
        Protected Set(ByVal value As Double?)
            Me.ThermalTransient.AllowedVoltageChange = If(value, 0)

            If Me.VoltageChange.Differs(value, 0.000001) Then
                Me._VoltageChange = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Writes and reads back the Voltage Change. </summary>
    ''' <remarks>
    ''' This command set the immediate output Voltage Change. The value is in Volts. The immediate
    ''' Voltage Change is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <param name="value"> The Voltage Change. </param>
    ''' <returns> The Voltage Change. </returns>
    Public Function ApplyVoltageChange(ByVal value As Double) As Double?
        Me.WriteVoltageChange(value)
        Return Me.QueryVoltageChange
    End Function

    ''' <summary> Queries the Voltage Change. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The Voltage Change or none if unknown. </returns>
    Public Function QueryVoltageChange() As Double?
        Const printFormat As Decimal = 9.6D
        Me.VoltageChange = Me.Session.QueryPrint(Me.VoltageChange.GetValueOrDefault(0.099), printFormat, "{0}.maxVoltageChange", Me.EntityName)
        Return Me.VoltageChange
    End Function

    ''' <summary> Writes the Voltage Change without reading back the value from the device. </summary>
    ''' <remarks>
    ''' This command sets the immediate output Voltage Change. The value is in Volts. The immediate
    ''' VoltageChange is the output Voltage setting. At *RST, the Voltage values = 0.
    ''' </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="value"> The Voltage Change. </param>
    ''' <returns> The Voltage Change. </returns>
    Public Function WriteVoltageChange(ByVal value As Double) As Double?
        Dim details As String = String.Empty
        If Not ThermalTransient.ValidateVoltageChange(value, details) Then
            Throw New ArgumentOutOfRangeException(NameOf(value), details)
        End If
        Me.Session.WriteLine("{0}:maxVoltageChangeSetter({1})", Me.EntityName, value)
        Me.VoltageChange = value
        Return Me.VoltageChange
    End Function

#End Region

#Region " CONFIGURE "

    ''' <summary> Applies the instrument defaults. </summary>
    ''' <remarks> This is required until the reset command gets implemented. </remarks>
    Public Overrides Sub ApplyInstrumentDefaults()
        MyBase.ApplyInstrumentDefaults()
        Me.StatusSubsystem.WriteLine("{0}:maxRateSetter()", Me.EntityName)
        Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine("{0}:maxVoltageChangeSetter({1}.level)", Me.EntityName, Me.DefaultsName)
        Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine("{0}:latencySetter({1}.latency)", Me.EntityName, Me.DefaultsName)
        Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine("{0}:medianFilterSizeSetter({1}.medianFilterSize)", Me.EntityName, Me.DefaultsName)
        Me.Session.QueryOperationCompleted()
        Me.StatusSubsystem.WriteLine("{0}:pointsSetter({1}.points)", Me.EntityName, Me.DefaultsName)
        Me.Session.QueryOperationCompleted()
    End Sub

    ''' <summary> Reads instrument defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub ReadInstrumentDefaults()
        MyBase.ReadInstrumentDefaults()

        If Not Me.Session.TryQueryPrint(7.4D, My.MySettings.Default.ThermalTransientApertureDefault, "{0}.aperture", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient aperture;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientApertureMinimum, "{0}.minAperture", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient minimum aperture;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientApertureMaximum, "{0}.maxAperture", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient maximum aperture;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientCurrentLevelDefault, "{0}.level", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient current level;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientCurrentMinimum, "{0}.minCurrent", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient minimum current level;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientCurrentMaximum, "{0}.maxCurrent", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient maximum current level;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientVoltageLimitDefault, "{0}.limit", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient voltage limit;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientVoltageMinimum, "{0}.minVoltage", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient minimum voltage;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientVoltageMaximum, "{0}.maxVoltage", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient maximum voltage;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientHighLimitDefault, "{0}.highLimit", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient high limit;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientLowLimitDefault, "{0}.lowLimit", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient low limit;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientVoltageChangeDefault, "{0}.maxVoltageChange", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient maximum voltage change;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(1, My.MySettings.Default.ThermalTransientMedianFilterLengthDefault, "{0}.medianFilterSize", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient median filter size;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientSamplingIntervalDefault, "{0}.period", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient period;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientSamplingIntervalMinimum, "{0}.minPeriod", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient minimum period;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(9.6D, My.MySettings.Default.ThermalTransientSamplingIntervalMaximum, "{0}.maxPeriod", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient maximum period;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(1, My.MySettings.Default.ThermalTransientTracePointsDefault, "{0}.points", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient trace points;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(1, My.MySettings.Default.ThermalTransientTracePointsMinimum, "{0}.minPoints", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient minimum trace points;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

        If Not Me.Session.TryQueryPrint(1, My.MySettings.Default.ThermalTransientTracePointsMaximum, "{0}.maxPoints", Me.DefaultsName) Then
            Me.PublishWarning("failed reading default thermal transient maximum trace points;. Sent:'{0}; Received:'{1}'.",
                                           Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
        End If

    End Sub

    ''' <summary> Configures the meter for making the thermal transient measurement. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="thermalTransient"> The Thermal Transient element. </param>
    Public Overloads Sub Configure(ByVal thermalTransient As ThermalTransientBase)

        If thermalTransient Is Nothing Then Throw New ArgumentNullException(NameOf(thermalTransient))
        MyBase.Configure(thermalTransient)

        Me.PublishVerbose("Setting {0} post transient delay to {1};. ", Me.BaseEntityName, thermalTransient.PostTransientDelay)
        Me.ApplyPostTransientDelay(thermalTransient.PostTransientDelay)
        Me.PublishVerbose("{0} post transient delay set to {1};. ", Me.BaseEntityName, thermalTransient.PostTransientDelay)

        Me.PublishVerbose("Setting {0} Allowed Voltage Change to {1};. ", Me.EntityName, thermalTransient.AllowedVoltageChange)
        Me.ApplyVoltageChange(thermalTransient.AllowedVoltageChange)
        Me.PublishVerbose("{0} Allowed Voltage Change set to {1};. ", Me.EntityName, thermalTransient.AllowedVoltageChange)

        Me.StatusSubsystem.CheckThrowDeviceException(False, "configuring Thermal Transient measurement;. ")
        Me.ThermalTransient.CheckThrowUnequalConfiguration(thermalTransient)

    End Sub

    ''' <summary> Applies changed meter configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="thermalTransient"> The Thermal Transient element. </param>
    Public Overloads Sub ConfigureChanged(ByVal thermalTransient As ThermalTransientBase)

        If thermalTransient Is Nothing Then Throw New ArgumentNullException(NameOf(thermalTransient))
        MyBase.ConfigureChanged(thermalTransient)
        If Not Me.ThermalTransient.ConfigurationEquals(thermalTransient) Then

            If Not Me.ThermalTransient.PostTransientDelay.Equals(thermalTransient.PostTransientDelay) Then
                Me.PublishVerbose("Setting {0} post transient delay to {1};. ", Me.BaseEntityName, thermalTransient.PostTransientDelay)
                Me.ApplyPostTransientDelay(thermalTransient.PostTransientDelay)
                Me.PublishVerbose("{0} post transient delay set to {1};. ", Me.BaseEntityName, thermalTransient.PostTransientDelay)
            End If

            If Not Me.ThermalTransient.AllowedVoltageChange.Equals(thermalTransient.AllowedVoltageChange) Then
                Me.PublishVerbose("Setting {0} Allowed Voltage Change to {1};. ", Me.EntityName, thermalTransient.AllowedVoltageChange)
                Me.ApplyVoltageChange(thermalTransient.AllowedVoltageChange)
                Me.PublishVerbose("{0} Allowed Voltage Change set to {1};. ", Me.EntityName, thermalTransient.AllowedVoltageChange)
            End If

        End If
        Me.StatusSubsystem.CheckThrowDeviceException(False, "configuring Thermal Transient measurement;. ")
        Me.ThermalTransient.CheckThrowUnequalConfiguration(thermalTransient)

    End Sub

    ''' <summary> Queries the configuration. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub QueryConfiguration()
        MyBase.QueryConfiguration()
        Me.QueryMedianFilterSize()
        Me.QueryPostTransientDelay()
        Me.QuerySamplingInterval()
        Me.QueryTracePoints()
        Me.QueryVoltageChange()
        Me.StatusSubsystem.CheckThrowDeviceException(False, "Reading {0} configuration;. ", Me.EntityName)
    End Sub

#End Region

#Region " MEASURE "

    ''' <summary> Measures the Thermal Transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="thermalTransient"> The Thermal Transient element. </param>
    Public Overloads Sub Measure(ByVal thermalTransient As ResistanceMeasureBase)
        If thermalTransient Is Nothing Then Throw New ArgumentNullException(NameOf(thermalTransient))
        If Me.Session.IsSessionOpen Then
            MyBase.Measure(thermalTransient)
            Me.ReadThermalTransient(thermalTransient)
        Else
            Me.EmulateThermalTransient(thermalTransient)
        End If
    End Sub

#End Region

#Region " READ "

    ''' <summary> Gets or sets the trace ready to read sentinel. </summary>
    ''' <value> The trace ready to read. </value>
    Public Property NewTraceReadyToRead As Boolean

    ''' <summary>
    ''' Reads the thermal transient. Sets the last <see cref="LastReading">reading</see>,
    ''' <see cref="LastOutcome">outcome</see> <see cref="LastMeasurementStatus">status</see> and
    ''' <see cref="MeasurementAvailable">Measurement available sentinel</see>.
    ''' The outcome is left empty if measurements were not made.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub ReadThermalTransient()

        Me.NewTraceReadyToRead = False
        If Me.QueryOutcomeNil() Then
            Me.LastReading = String.Empty
            Me.LastOutcome = String.Empty
            Me.LastMeasurementStatus = String.Empty
            Throw New InvalidOperationException("Measurement Not made.")
        Else
            If Me.QueryOkay() Then
                Me.LastReading = Me.Session.QueryPrintStringFormatTrimEnd(9.6D, "{0}.voltageChange", MeterSubsystemBase.ThermalTransientEstimatorEntityName)
                Me.StatusSubsystem.CheckThrowDeviceException(False, "reading voltage change;. Sent: '{0}'; Received: '{1}'.",
                                                             Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
                Me.LastOutcome = "0"
                Me.LastMeasurementStatus = String.Empty
                Me.NewTraceReadyToRead = True
            Else
                ' if outcome failed, read and parse the outcome and status.
                Me.LastOutcome = Me.Session.QueryPrintStringFormatTrimEnd(1, "{0}.outcome", Me.EntityName)
                Me.StatusSubsystem.CheckThrowDeviceException(False, "reading outcome;. Sent: '{0}'; Received: '{1}'.",
                                                             Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
                Me.LastMeasurementStatus = Me.Session.QueryPrintStringFormatTrimEnd(1, "{0}.status", Me.EntityName)
                Me.StatusSubsystem.CheckThrowDeviceException(False, "reading status;. Sent: '{0}'; Received: '{1}'.",
                                                             Me.Session.LastMessageSent, Me.Session.LastMessageReceived)
                Me.LastReading = String.Empty
            End If
        End If
        Me.MeasurementAvailable = True
    End Sub

    ''' <summary> Reads the Thermal Transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="thermalTransient"> The Thermal Transient element. </param>
    Public Sub ReadThermalTransient(ByVal thermalTransient As ResistanceMeasureBase)
        If thermalTransient Is Nothing Then Throw New ArgumentNullException(NameOf(thermalTransient))
        Me.ReadThermalTransient()
        Me.StatusSubsystem.CheckThrowDeviceException(False, "Reading Thermal Transient;. last command: '{0}'", Me.Session.LastMessageSent)
        Dim measurementOutcome As MeasurementOutcomes = MeasurementOutcomes.None
        If Me.LastOutcome <> "0" Then
            Dim details As String = String.Empty
            measurementOutcome = Me.ParseOutcome(details)
            Me.PublishWarning("Measurement failed;. Details: {0}", details)
        End If
        Me.ThermalTransient.ParseReading(Me.LastReading, thermalTransient.CurrentLevel, measurementOutcome)
        thermalTransient.ParseReading(Me.LastReading, thermalTransient.CurrentLevel, measurementOutcome)
    End Sub

    ''' <summary> Emulates a Thermal Transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="thermalTransient"> The Thermal Transient element. </param>
    Public Sub EmulateThermalTransient(ByVal thermalTransient As ResistanceMeasureBase)
        If thermalTransient Is Nothing Then Throw New ArgumentNullException(NameOf(thermalTransient))
        Me.ThermalTransient.EmulateReading()
        Me.LastReading = Me.ThermalTransient.LastReading
        Me.LastOutcome = Me.ThermalTransient.LastOutcome
        Me.LastMeasurementStatus = String.Empty
        Dim measurementOutcome As MeasurementOutcomes = MeasurementOutcomes.None
        Me.ThermalTransient.ParseReading(Me.LastReading, thermalTransient.CurrentLevel, measurementOutcome)
        thermalTransient.ParseReading(Me.LastReading, thermalTransient.CurrentLevel, measurementOutcome)
    End Sub

    ''' <summary>
    ''' Reads the thermal transient trace from the instrument into a
    ''' <see cref="LastTrace">comma-separated array of times in milliseconds and values in
    ''' milli volts.</see> and
    ''' <see cref="LastTimeSeries">collection of times in milliseconds and values in millivolts.</see>
    ''' Also updates the outcome and status.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub ReadThermalTransientTrace()

        ' clear the new trace sentinel
        Me.NewTraceReadyToRead = False
        Dim delimiter As String = ","
        Me._LastTimeSeries = New List(Of System.Windows.Point)
        If Me.LastOutcome <> "0" Then
            Me.LastTrace = String.Empty
            Throw New InvalidOperationException("Measurement failed.")
        Else
            Dim builder As New System.Text.StringBuilder
            Me.Session.WriteLine("{0}:printTimeSeries(4)", Me.EntityName)
            Me.StatusSubsystem.TraceVisaOperation("sent query '{0}';. ", Me.Session.LastMessageSent)
            Do While Me.Session.QueryMessageAvailableStatus(TimeSpan.FromMilliseconds(1), 3)
                Dim reading As String = Me.Session.ReadLine
                If builder.Length > 0 Then
                    builder.Append(delimiter)
                End If
                builder.Append(reading)
                Dim xy As String() = reading.Split(","c)
                Dim x, y As Single
                If Not Single.TryParse(xy(0).Trim, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, x) Then
                    x = -1
                End If
                If Not Single.TryParse(xy(1).Trim, Globalization.NumberStyles.Number, Globalization.CultureInfo.CurrentCulture, y) Then
                    y = -1
                End If
                Dim point As New System.Windows.Point(x, y)
                Me._LastTimeSeries.Add(point)
            Loop
            Me.LastTrace = builder.ToString
        End If
        Me.NotifyPropertyChanged()

    End Sub

#End Region

#Region " ESTIMATE "

    ''' <summary> Gets or sets the model. </summary>
    ''' <value> The model. </value>
    Public ReadOnly Property Model As isr.Numerical.Optima.PulseResponseFunction

    ''' <summary> The simplex. </summary>
    Private _Simplex As isr.Numerical.Optima.Simplex

    ''' <summary> Model transient response. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException">     Thrown when one or more required arguments are
    '''                                              null. </exception>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="thermalTransient"> The thermal transient element. </param>
    Public Sub ModelTransientResponse(ByVal thermalTransient As ThermalTransient)

        If thermalTransient Is Nothing Then
            Throw New ArgumentNullException(NameOf(thermalTransient))
        ElseIf Me.LastTimeSeries Is Nothing Then
            Throw New InvalidOperationException("Last time series not read")
        ElseIf Me.LastTimeSeries.Count = 0 Then
            Throw New InvalidOperationException("Time series has no values")
        ElseIf Me.LastTimeSeries.Count <= 10 Then
            Throw New InvalidOperationException("Time series has less than 10 values")
        End If
        Dim voltageScale As Double = 1000 ' scale to millivolts
        Dim timeScale As Double = 1000 ' scale to milliseconds
        Dim flatnessPrecisionFactor As Double = 0.0001
        Dim pulseDuration As Double = timeScale * Me.PulseDuration.TotalSeconds
        Dim estimatedTau As Double = timeScale * 0.5 * Me.PulseDuration.TotalSeconds
        Dim estimatedAsymptote As Double = voltageScale * Me.ThermalTransient.HighLimit
        Dim voltageRange As Double() = New Double() {0.5 * estimatedAsymptote, 2 * estimatedAsymptote}
        Dim voltagePresision As Double = flatnessPrecisionFactor * estimatedAsymptote
        Dim negativeInverseTauRange As Double() = New Double() {-1 / (0.5 * estimatedTau), -1 / (2 * estimatedTau)}
        Dim inverseTauPrecision As Double = flatnessPrecisionFactor / estimatedTau
        Dim dimension As Integer = 2
        Dim maximumIterations As Integer = 100
        Dim expectedDeviation As Double = 0.05 * estimatedAsymptote
        Dim expectedMaximumSSQ As Double = Me.ThermalTransient.TracePoints * expectedDeviation * expectedDeviation
        Dim objectivePrecisionFactor As Double = 0.0001
        Dim objectivePrecision As Double = objectivePrecisionFactor * expectedMaximumSSQ

        Me.PublishInfo("Instantiating Simplex")
        Me._Simplex = New isr.Numerical.Optima.Simplex("Exponent", dimension,
                                                       New Double() {voltageRange(0), negativeInverseTauRange(0)},
                                                       New Double() {voltageRange(1), negativeInverseTauRange(1)}, maximumIterations,
                                                       New Double() {voltagePresision, inverseTauPrecision},
                                                       objectivePrecision)

        Me.PublishInfo("Instantiating Thermal Transient model")
        Me._Model = New isr.Numerical.Optima.PulseResponseFunction(Me.LastTimeSeries)

        Me.PublishInfo("Initializing simplex")
        Me._Simplex.Initialize(Me._Model)
        Me.PublishVerbose("Initial simplex is {0}", Me._Simplex.ToString)
        Me._Simplex.Solve()
        Me.PublishVerbose("Finished in {0} Iterations", Me._Simplex.IterationNumber)
        Me.PublishVerbose("Final simplex is {0}", Me._Simplex.ToString)
        Me.Model.EvaluateFunctionValues(Me._Simplex.BestSolution.Values)

        Me.PublishVerbose("Estimated Asymptote = {0:G4} mV", Me._Simplex.BestSolution.Values(0))
        thermalTransient.Asymptote = 0.001 * Me._Simplex.BestSolution.Values(0)

        Me.PublishVerbose("Estimated Time Constant = {0:G4} ms", -1 / Me._Simplex.BestSolution.Values(1))
        thermalTransient.TimeConstant = -0.001 / Me._Simplex.BestSolution.Values(1)

        Me.PublishVerbose("Estimated Thermal Transient Voltage = {0:G4} mV", Me._Model.FunctionValue(Me._Simplex.BestSolution.Values,
                                                                                                        New Double() {pulseDuration, 0}))
        thermalTransient.EstimatedVoltage = 0.001 * Me._Model.FunctionValue(Me._Simplex.BestSolution.Values, New Double() {pulseDuration, 0})

        Me.PublishVerbose("Correlation Coefficient = {0:G4}", Me._Model.EvaluateCorrelationCoefficient)
        thermalTransient.CorrelationCoefficient = Me._Model.EvaluateCorrelationCoefficient

        Me.PublishVerbose("Standard Error = {0:G4} mV", Me._Model.EvaluateStandardError(Me._Simplex.BestSolution.Objective))
        thermalTransient.StandardError = 0.001 * Me._Model.EvaluateStandardError(Me._Simplex.BestSolution.Objective)

        thermalTransient.Iterations = Me._Simplex.IterationNumber

        If Me._Simplex.Converged Then
            thermalTransient.OptimizationOutcome = OptimizationOutcome.Converged
        ElseIf Me._Simplex.Optimized Then
            thermalTransient.OptimizationOutcome = OptimizationOutcome.Optimized
        ElseIf Me._Simplex.IterationNumber >= maximumIterations Then
            thermalTransient.OptimizationOutcome = OptimizationOutcome.Exhausted
        Else
            thermalTransient.OptimizationOutcome = OptimizationOutcome.None
        End If

        Me.PublishVerbose("Converged = {0}", Me._Simplex.Converged)
        Me.PublishVerbose("Optimized = {0}", Me._Simplex.Optimized)

    End Sub

#End Region


End Class


