﻿Namespace TtmSyntax

    Namespace Support

        ''' <summary> A syntax of the Support LUA subsystem. </summary>
        ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
        ''' Licensed under The MIT License.</para> </remarks>
        Public Module SupportSyntax
            ''' <summary> The version query command. </summary>
            Public Const VersionQueryCommand As String = "_G.print(_G.isr.version())"
        End Module

    End Namespace

    Namespace Meters


        ''' <summary> Syntax of the thermal transient subsystem. </summary>
        ''' <remarks> (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
        ''' Licensed under The MIT License.</para> </remarks>
        Public Module ThermalTransient
            ''' <summary> The version query command. </summary>
            Public Const VersionQueryCommand As String = "_G.print(_G.isr.meters.thermalTransient.version())"
        End Module

    End Namespace


End Namespace

