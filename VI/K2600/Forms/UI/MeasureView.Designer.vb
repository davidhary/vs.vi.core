﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class MeasureView

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me._Layout = New System.Windows.Forms.TableLayoutPanel()
        Me._Panel = New System.Windows.Forms.Panel()
        Me._OpenDetectorCheckBox = New System.Windows.Forms.CheckBox()
        Me._AutoDelayCheckBox = New System.Windows.Forms.CheckBox()
        Me._AutoZeroCheckBox = New System.Windows.Forms.CheckBox()
        Me._FilterGroupBox = New System.Windows.Forms.GroupBox()
        Me._FilterWindowNumericLabel = New System.Windows.Forms.Label()
        Me._FilterWindowNumeric = New System.Windows.Forms.NumericUpDown()
        Me._RepeatingAverageRadioButton = New System.Windows.Forms.RadioButton()
        Me._MovingAverageRadioButton = New System.Windows.Forms.RadioButton()
        Me._FilterCountNumeric = New System.Windows.Forms.NumericUpDown()
        Me._FilterCountNumericLabel = New System.Windows.Forms.Label()
        Me._FilterEnabledCheckBox = New System.Windows.Forms.CheckBox()
        Me._SenseRangeNumeric = New System.Windows.Forms.NumericUpDown()
        Me._PowerLineCyclesNumeric = New System.Windows.Forms.NumericUpDown()
        Me._SenseRangeNumericLabel = New System.Windows.Forms.Label()
        Me._PowerLineCyclesNumericLabel = New System.Windows.Forms.Label()
        Me._SenseFunctionComboBox = New System.Windows.Forms.ComboBox()
        Me._SenseFunctionComboBoxLabel = New System.Windows.Forms.Label()
        Me._AutoRangeCheckBox = New System.Windows.Forms.CheckBox()
        Me._ApplySenseSettingsButton = New System.Windows.Forms.Button()
        Me._Layout.SuspendLayout()
        Me._Panel.SuspendLayout()
        Me._FilterGroupBox.SuspendLayout()
        CType(Me._FilterWindowNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._FilterCountNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._SenseRangeNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me._PowerLineCyclesNumeric, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_Layout
        '
        Me._Layout.ColumnCount = 3
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me._Layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Controls.Add(Me._Panel, 1, 1)
        Me._Layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._Layout.Location = New System.Drawing.Point(1, 1)
        Me._Layout.Name = "_Layout"
        Me._Layout.RowCount = 3
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me._Layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._Layout.Size = New System.Drawing.Size(365, 341)
        Me._Layout.TabIndex = 0
        '
        '_Panel
        '
        Me._Panel.Controls.Add(Me._OpenDetectorCheckBox)
        Me._Panel.Controls.Add(Me._AutoDelayCheckBox)
        Me._Panel.Controls.Add(Me._AutoZeroCheckBox)
        Me._Panel.Controls.Add(Me._FilterGroupBox)
        Me._Panel.Controls.Add(Me._FilterEnabledCheckBox)
        Me._Panel.Controls.Add(Me._SenseRangeNumeric)
        Me._Panel.Controls.Add(Me._PowerLineCyclesNumeric)
        Me._Panel.Controls.Add(Me._SenseRangeNumericLabel)
        Me._Panel.Controls.Add(Me._PowerLineCyclesNumericLabel)
        Me._Panel.Controls.Add(Me._SenseFunctionComboBox)
        Me._Panel.Controls.Add(Me._SenseFunctionComboBoxLabel)
        Me._Panel.Controls.Add(Me._AutoRangeCheckBox)
        Me._Panel.Controls.Add(Me._ApplySenseSettingsButton)
        Me._Panel.Location = New System.Drawing.Point(10, 46)
        Me._Panel.Name = "_Panel"
        Me._Panel.Size = New System.Drawing.Size(345, 249)
        Me._Panel.TabIndex = 0
        '
        '_OpenDetectorCheckBox
        '
        Me._OpenDetectorCheckBox.AutoSize = True
        Me._OpenDetectorCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._OpenDetectorCheckBox.Location = New System.Drawing.Point(190, 145)
        Me._OpenDetectorCheckBox.Name = "_OpenDetectorCheckBox"
        Me._OpenDetectorCheckBox.Size = New System.Drawing.Size(149, 21)
        Me._OpenDetectorCheckBox.TabIndex = 27
        Me._OpenDetectorCheckBox.Text = "Open Detector (off)"
        Me._OpenDetectorCheckBox.UseVisualStyleBackColor = True
        '
        '_AutoDelayCheckBox
        '
        Me._AutoDelayCheckBox.AutoSize = True
        Me._AutoDelayCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AutoDelayCheckBox.Location = New System.Drawing.Point(190, 119)
        Me._AutoDelayCheckBox.Name = "_AutoDelayCheckBox"
        Me._AutoDelayCheckBox.Size = New System.Drawing.Size(128, 21)
        Me._AutoDelayCheckBox.TabIndex = 26
        Me._AutoDelayCheckBox.Text = "Auto Delay (off)"
        Me._AutoDelayCheckBox.ThreeState = True
        Me._AutoDelayCheckBox.UseVisualStyleBackColor = True
        '
        '_AutoZeroCheckBox
        '
        Me._AutoZeroCheckBox.AutoSize = True
        Me._AutoZeroCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._AutoZeroCheckBox.Location = New System.Drawing.Point(190, 43)
        Me._AutoZeroCheckBox.Name = "_AutoZeroCheckBox"
        Me._AutoZeroCheckBox.Size = New System.Drawing.Size(89, 21)
        Me._AutoZeroCheckBox.TabIndex = 19
        Me._AutoZeroCheckBox.Text = "Auto Zero"
        Me._AutoZeroCheckBox.UseVisualStyleBackColor = True
        '
        '_FilterGroupBox
        '
        Me._FilterGroupBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me._FilterGroupBox.Controls.Add(Me._FilterWindowNumericLabel)
        Me._FilterGroupBox.Controls.Add(Me._FilterWindowNumeric)
        Me._FilterGroupBox.Controls.Add(Me._RepeatingAverageRadioButton)
        Me._FilterGroupBox.Controls.Add(Me._MovingAverageRadioButton)
        Me._FilterGroupBox.Controls.Add(Me._FilterCountNumeric)
        Me._FilterGroupBox.Controls.Add(Me._FilterCountNumericLabel)
        Me._FilterGroupBox.Location = New System.Drawing.Point(6, 102)
        Me._FilterGroupBox.Name = "_FilterGroupBox"
        Me._FilterGroupBox.Size = New System.Drawing.Size(155, 135)
        Me._FilterGroupBox.TabIndex = 24
        Me._FilterGroupBox.TabStop = False
        Me._FilterGroupBox.Text = "Filter"
        '
        '_FilterWindowNumericLabel
        '
        Me._FilterWindowNumericLabel.AutoSize = True
        Me._FilterWindowNumericLabel.Location = New System.Drawing.Point(3, 48)
        Me._FilterWindowNumericLabel.Name = "_FilterWindowNumericLabel"
        Me._FilterWindowNumericLabel.Size = New System.Drawing.Size(81, 17)
        Me._FilterWindowNumericLabel.TabIndex = 3
        Me._FilterWindowNumericLabel.Text = "Window [%]:"
        '
        '_FilterWindowNumeric
        '
        Me._FilterWindowNumeric.DecimalPlaces = 2
        Me._FilterWindowNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilterWindowNumeric.Location = New System.Drawing.Point(87, 44)
        Me._FilterWindowNumeric.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me._FilterWindowNumeric.Name = "_FilterWindowNumeric"
        Me._FilterWindowNumeric.Size = New System.Drawing.Size(54, 25)
        Me._FilterWindowNumeric.TabIndex = 4
        Me._FilterWindowNumeric.Value = New Decimal(New Integer() {10, 0, 0, 0})
        '
        '_RepeatingAverageRadioButton
        '
        Me._RepeatingAverageRadioButton.AutoSize = True
        Me._RepeatingAverageRadioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._RepeatingAverageRadioButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._RepeatingAverageRadioButton.Location = New System.Drawing.Point(6, 107)
        Me._RepeatingAverageRadioButton.Name = "_RepeatingAverageRadioButton"
        Me._RepeatingAverageRadioButton.Size = New System.Drawing.Size(88, 21)
        Me._RepeatingAverageRadioButton.TabIndex = 5
        Me._RepeatingAverageRadioButton.Text = "Repeating"
        Me._RepeatingAverageRadioButton.UseVisualStyleBackColor = True
        '
        '_MovingAverageRadioButton
        '
        Me._MovingAverageRadioButton.AutoSize = True
        Me._MovingAverageRadioButton.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._MovingAverageRadioButton.Checked = True
        Me._MovingAverageRadioButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._MovingAverageRadioButton.Location = New System.Drawing.Point(21, 78)
        Me._MovingAverageRadioButton.Name = "_MovingAverageRadioButton"
        Me._MovingAverageRadioButton.Size = New System.Drawing.Size(73, 21)
        Me._MovingAverageRadioButton.TabIndex = 2
        Me._MovingAverageRadioButton.TabStop = True
        Me._MovingAverageRadioButton.Text = "Moving"
        Me._MovingAverageRadioButton.UseVisualStyleBackColor = True
        '
        '_FilterCountNumeric
        '
        Me._FilterCountNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilterCountNumeric.Location = New System.Drawing.Point(87, 15)
        Me._FilterCountNumeric.Name = "_FilterCountNumeric"
        Me._FilterCountNumeric.Size = New System.Drawing.Size(53, 25)
        Me._FilterCountNumeric.TabIndex = 1
        Me._FilterCountNumeric.Value = New Decimal(New Integer() {100, 0, 0, 0})
        '
        '_FilterCountNumericLabel
        '
        Me._FilterCountNumericLabel.AutoSize = True
        Me._FilterCountNumericLabel.Location = New System.Drawing.Point(39, 18)
        Me._FilterCountNumericLabel.Name = "_FilterCountNumericLabel"
        Me._FilterCountNumericLabel.Size = New System.Drawing.Size(45, 17)
        Me._FilterCountNumericLabel.TabIndex = 0
        Me._FilterCountNumericLabel.Text = "Count:"
        Me._FilterCountNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_FilterEnabledCheckBox
        '
        Me._FilterEnabledCheckBox.AutoSize = True
        Me._FilterEnabledCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._FilterEnabledCheckBox.Location = New System.Drawing.Point(190, 93)
        Me._FilterEnabledCheckBox.Name = "_FilterEnabledCheckBox"
        Me._FilterEnabledCheckBox.Size = New System.Drawing.Size(112, 21)
        Me._FilterEnabledCheckBox.TabIndex = 23
        Me._FilterEnabledCheckBox.Text = "Filter Enabled"
        Me._FilterEnabledCheckBox.UseVisualStyleBackColor = True
        '
        '_SenseRangeNumeric
        '
        Me._SenseRangeNumeric.DecimalPlaces = 3
        Me._SenseRangeNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._SenseRangeNumeric.Location = New System.Drawing.Point(105, 71)
        Me._SenseRangeNumeric.Maximum = New Decimal(New Integer() {1010, 0, 0, 0})
        Me._SenseRangeNumeric.Name = "_SenseRangeNumeric"
        Me._SenseRangeNumeric.Size = New System.Drawing.Size(79, 25)
        Me._SenseRangeNumeric.TabIndex = 21
        Me._SenseRangeNumeric.Value = New Decimal(New Integer() {105, 0, 0, 196608})
        '
        '_PowerLineCyclesNumeric
        '
        Me._PowerLineCyclesNumeric.DecimalPlaces = 3
        Me._PowerLineCyclesNumeric.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._PowerLineCyclesNumeric.Increment = New Decimal(New Integer() {1, 0, 0, 65536})
        Me._PowerLineCyclesNumeric.Location = New System.Drawing.Point(105, 41)
        Me._PowerLineCyclesNumeric.Maximum = New Decimal(New Integer() {25, 0, 0, 0})
        Me._PowerLineCyclesNumeric.Minimum = New Decimal(New Integer() {1, 0, 0, 196608})
        Me._PowerLineCyclesNumeric.Name = "_PowerLineCyclesNumeric"
        Me._PowerLineCyclesNumeric.Size = New System.Drawing.Size(79, 25)
        Me._PowerLineCyclesNumeric.TabIndex = 18
        Me._PowerLineCyclesNumeric.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        '_SenseRangeNumericLabel
        '
        Me._SenseRangeNumericLabel.AutoSize = True
        Me._SenseRangeNumericLabel.Location = New System.Drawing.Point(35, 75)
        Me._SenseRangeNumericLabel.Name = "_SenseRangeNumericLabel"
        Me._SenseRangeNumericLabel.Size = New System.Drawing.Size(68, 17)
        Me._SenseRangeNumericLabel.TabIndex = 20
        Me._SenseRangeNumericLabel.Text = "Range [V]:"
        Me._SenseRangeNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_PowerLineCyclesNumericLabel
        '
        Me._PowerLineCyclesNumericLabel.AutoSize = True
        Me._PowerLineCyclesNumericLabel.Location = New System.Drawing.Point(5, 45)
        Me._PowerLineCyclesNumericLabel.Name = "_PowerLineCyclesNumericLabel"
        Me._PowerLineCyclesNumericLabel.Size = New System.Drawing.Size(98, 17)
        Me._PowerLineCyclesNumericLabel.TabIndex = 17
        Me._PowerLineCyclesNumericLabel.Text = "Aperture [nplc]:"
        Me._PowerLineCyclesNumericLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_SenseFunctionComboBox
        '
        Me._SenseFunctionComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._SenseFunctionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._SenseFunctionComboBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._SenseFunctionComboBox.Items.AddRange(New Object() {"I", "V"})
        Me._SenseFunctionComboBox.Location = New System.Drawing.Point(105, 11)
        Me._SenseFunctionComboBox.Name = "_SenseFunctionComboBox"
        Me._SenseFunctionComboBox.Size = New System.Drawing.Size(231, 25)
        Me._SenseFunctionComboBox.TabIndex = 15
        '
        '_SenseFunctionComboBoxLabel
        '
        Me._SenseFunctionComboBoxLabel.AutoSize = True
        Me._SenseFunctionComboBoxLabel.Location = New System.Drawing.Point(37, 15)
        Me._SenseFunctionComboBoxLabel.Name = "_SenseFunctionComboBoxLabel"
        Me._SenseFunctionComboBoxLabel.Size = New System.Drawing.Size(59, 17)
        Me._SenseFunctionComboBoxLabel.TabIndex = 14
        Me._SenseFunctionComboBoxLabel.Text = "Function:"
        Me._SenseFunctionComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_AutoRangeCheckBox
        '
        Me._AutoRangeCheckBox.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._AutoRangeCheckBox.Location = New System.Drawing.Point(190, 67)
        Me._AutoRangeCheckBox.Name = "_AutoRangeCheckBox"
        Me._AutoRangeCheckBox.Size = New System.Drawing.Size(103, 21)
        Me._AutoRangeCheckBox.TabIndex = 22
        Me._AutoRangeCheckBox.Text = "Auto Range"
        Me._AutoRangeCheckBox.UseVisualStyleBackColor = True
        '
        '_ApplySenseSettingsButton
        '
        Me._ApplySenseSettingsButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._ApplySenseSettingsButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold)
        Me._ApplySenseSettingsButton.Location = New System.Drawing.Point(278, 207)
        Me._ApplySenseSettingsButton.Name = "_ApplySenseSettingsButton"
        Me._ApplySenseSettingsButton.Size = New System.Drawing.Size(58, 30)
        Me._ApplySenseSettingsButton.TabIndex = 25
        Me._ApplySenseSettingsButton.Text = "&Apply"
        Me._ApplySenseSettingsButton.UseVisualStyleBackColor = True
        '
        'MeasureView
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Controls.Add(Me._Layout)
        Me.Name = "MeasureView"
        Me.Padding = New System.Windows.Forms.Padding(1)
        Me.Size = New System.Drawing.Size(367, 343)
        Me._Layout.ResumeLayout(False)
        Me._Panel.ResumeLayout(False)
        Me._Panel.PerformLayout()
        Me._FilterGroupBox.ResumeLayout(False)
        Me._FilterGroupBox.PerformLayout()
        CType(Me._FilterWindowNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._FilterCountNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._SenseRangeNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me._PowerLineCyclesNumeric, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents _Layout As Windows.Forms.TableLayoutPanel
    Private WithEvents _Panel As Windows.Forms.Panel
    Private WithEvents _OpenDetectorCheckBox As Windows.Forms.CheckBox
    Private WithEvents _AutoDelayCheckBox As Windows.Forms.CheckBox
    Private WithEvents _AutoZeroCheckBox As Windows.Forms.CheckBox
    Private WithEvents _FilterGroupBox As Windows.Forms.GroupBox
    Private WithEvents _FilterWindowNumericLabel As Windows.Forms.Label
    Private WithEvents _FilterWindowNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _RepeatingAverageRadioButton As Windows.Forms.RadioButton
    Private WithEvents _MovingAverageRadioButton As Windows.Forms.RadioButton
    Private WithEvents _FilterCountNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _FilterCountNumericLabel As Windows.Forms.Label
    Private WithEvents _FilterEnabledCheckBox As Windows.Forms.CheckBox
    Private WithEvents _SenseRangeNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _PowerLineCyclesNumeric As Windows.Forms.NumericUpDown
    Private WithEvents _SenseRangeNumericLabel As Windows.Forms.Label
    Private WithEvents _PowerLineCyclesNumericLabel As Windows.Forms.Label
    Private WithEvents _SenseFunctionComboBox As Windows.Forms.ComboBox
    Private WithEvents _SenseFunctionComboBoxLabel As Windows.Forms.Label
    Private WithEvents _AutoRangeCheckBox As Windows.Forms.CheckBox
    Private WithEvents _ApplySenseSettingsButton As Windows.Forms.Button
End Class
