''' <summary> K2600 Current Source unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2600")>
Public Class CurrentSourceTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K2600Tests.CurrentSourceSettings.Get.Exists, $"{GetType(K2600Tests.CurrentSourceSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SOURCE CURRENT MEASURE VOLTAGE "

    ''' <summary> Assert source current measure voltage should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="device"> The device. </param>
    Private Shared Sub AssertSourceCurrentMeasureVoltageShouldPass(ByVal device As VI.Tsp.K2600.K2600Device)
        Dim expectedFunctionMode As VI.Tsp.SourceFunctionMode = CurrentSourceSettings.Get.SourceFunction
        Dim SourceFunction As VI.Tsp.SourceFunctionMode = device.SourceSubsystem.ApplySourceFunction(expectedFunctionMode).GetValueOrDefault(VI.Tsp.SourceFunctionMode.None)
        Assert.AreEqual(expectedFunctionMode, SourceFunction, $"{GetType(VI.Tsp.SourceSubsystemBase)}.{NameOf(VI.Tsp.SourceSubsystemBase.SourceFunction)} is {SourceFunction} ; expected {expectedFunctionMode}")
    End Sub

    ''' <summary> (Unit Test Method) source current measure voltage should pass. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestMethod()>
    Public Sub SourceCurrentMeasureVoltageShouldPass()
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            device.AddListener(TestInfo.TraceMessagesQueueListener)
            Try
                isr.VI.DeviceTests.DeviceManager.AssertDeviceShouldOpenWithoutDeviceErrors(TestInfo, device, ResourceSettings.Get)
                Try
                    CurrentSourceTests.AssertSourceCurrentMeasureVoltageShouldPass(device)
                Catch
                    Throw
                Finally
                    ' To_DO: change source subsystem to inherit from vi.source subsystem base have the SMU pass the session to this class with the 
                    ' and use a builder to build the commands adding the SMU prefix. 
                    ' isr.VI.DeviceTests.DeviceManager.ToggleOutput(device.SourceSubsystem, False)
                End Try
            Catch
                Throw
            Finally
                K2600Tests.DeviceManager.CloseSession(TestInfo, device)
            End Try
        End Using
    End Sub

#End Region

End Class

