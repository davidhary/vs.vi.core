''' <summary> K2600 resource manager only unit tests. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2017-10-10 </para>
''' </remarks>
<TestClass(), TestCategory("k2600")>
Public Class ResourceManagerTests

#Region " CONSTRUCTION and CLEANUP "

#Disable Warning IDE0060 ' Remove unused parameter

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
#Enable Warning IDE0060 ' Remove unused parameter
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.VI.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
        Assert.IsTrue(K2600Tests.ResourceSettings.Get.Exists, $"{GetType(K2600Tests.ResourceSettings)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " VISA RESOURCE TESTS "

    ''' <summary> (Unit Test Method) visa resource manager should include resource. </summary>
    ''' <remarks> Finds the resource using the session factory resources manager. </remarks>
    <TestMethod()>
    Public Sub VisaResourceManagerShouldIncludeResource()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Dim resourcesFilter As String = VI.SessionFactory.Get.Factory.ResourcesProvider.ResourceFinder.BuildMinimalResourcesFilter
        Dim resources As String()
        Using rm As VI.Pith.ResourcesProviderBase = VI.SessionFactory.Get.Factory.ResourcesProvider()
            resources = rm.FindResources(resourcesFilter).ToArray
        End Using
        Assert.IsTrue(resources.Any, $"VISA Resources {If(resources.Any, "", "not")} found among {resourcesFilter}")
        Assert.IsTrue(resources.Contains(ResourceSettings.Get.ResourceName), $"Resource {ResourceSettings.Get.ResourceName} not found among {resourcesFilter}")
    End Sub

    ''' <summary> (Unit Test Method) visa session base should find resource. </summary>
    ''' <remarks> Finds the resource using the device class. </remarks>
    <TestMethod()>
    Public Sub VisaSessionBaseShouldFindResource()
        If Not ResourceSettings.Get.ResourcePinged Then Assert.Inconclusive($"{ResourceSettings.Get.ResourceTitle} not found")
        Using device As VI.Tsp.K2600.K2600Device = VI.Tsp.K2600.K2600Device.Create
            Assert.IsTrue(VI.Tsp.K2600.K2600Device.Find(ResourceSettings.Get.ResourceName, device.Session.ResourcesFilter),
                          $"VISA Resource {ResourceSettings.Get.ResourceName} not found among {device.Session.ResourcesFilter}")
        End Using
    End Sub

#End Region

End Class
