Namespace My

    ''' <summary> my application. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Partial Friend Class MyApplication

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = 4 'TraceEventIds.IsrVITtmRegister

        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "TTM Register"

        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "TTM Register"

        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "TTM.Register"

    End Class

End Namespace



