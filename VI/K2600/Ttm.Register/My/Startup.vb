Friend Module Startup

    ''' <summary> Application entry procedure. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <STAThread()>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Sub Main()

        Dim lastAction As String = String.Empty
        Dim success As Boolean = True
        Try

            lastAction = "initializing"
            My.Application.InitializeKnownState()

            lastAction = "parsing command line"
            My.MyApplication.ConsoleWriteLine(lastAction)
            If CommandLineInfo.TryParseCommandLine(My.Application.CommandLineCollection) Then

                If CommandLineInfo.DisplayHelp.GetValueOrDefault(False) Then
                    CommandLineInfo.DisplayCommandLine()
                    Console.ReadKey()
                End If

                If CommandLineInfo.DisplayCertification.GetValueOrDefault(False) Then
                    My.Application.DisplayCertification(CommandLineInfo.Password, CommandLineInfo.SerialNumber, CommandLineInfo.Salt)
                End If

                If CommandLineInfo.Attended OrElse CommandLineInfo.InteractiveEnabled Then

                    Console.Write("Enter any key to exit: ")
                    Console.ReadKey()

                End If

            Else

                success = False
                My.MyApplication.ConsoleErrorWriteLine("Failed {0}.", lastAction)

                Console.Write("Enter any key to exit: ")
                Console.ReadKey()

            End If

        Catch ex As Exception

            success = False

            ' log the exception
            My.Application.Log.WriteException(ex, TraceEventType.Error, "Failed " & lastAction)
            My.MyApplication.ConsoleErrorWriteLine("Exception occurred {0} with error '{1}'.", lastAction, ex.Message)

        Finally

            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()

            ' For some reason the event handling set in the Settings class dos not really work.
            My.MySettings.Default.Save()

            Console.Write("Enter any key to exit: ")
            Console.ReadKey()

            If success Then
                ' exit with success code
                Environment.Exit(0)
            Else
                ' exit with an error code
                Environment.Exit(-1)
            End If
        End Try

    End Sub

End Module
