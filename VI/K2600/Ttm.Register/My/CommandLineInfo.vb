''' <summary>
''' A sealed class the parses the command line and provides the command line values.
''' </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 2013-02-02, x.x.5093.x. </para>
''' </remarks>
Public NotInheritable Class CommandLineInfo

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="CommandLineInfo" /> class. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub New()
        MyBase.New()
    End Sub

#End Region

#Region " OPTIONS "

    ''' <summary> Displays a command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Friend Shared Sub DisplayCommandLine()

        Console.WriteLine("{9} [{0}|{1}] [{2}] [{3}] [{4}] [{5}<address>] [{6}<password>]  [{7}<serial number>] [{8}]",
                           CommandLineInfo.AttendedOption, CommandLineInfo.InteractiveEnabledOption,
                           CommandLineInfo.CertifyRequestedOption, CommandLineInfo.DisplayCertificationOption,
                           CommandLineInfo.QuiteModeEnabledOption, CommandLineInfo.InstrumentResourceNameOption, CommandLineInfo.PasswordOption,
                           CommandLineInfo.SerialNumberOption, CommandLineInfo.DisplayHelpOption,
                           My.Application.Info.AssemblyName)
        Console.WriteLine("Option Switches:")
        Console.WriteLine(CommandLineInfo.AttendedOption)
        Console.WriteLine("Attended mode.  Messages are output to the console. The program exists after the operator enters a key at the prompt.")
        Console.WriteLine(InteractiveEnabledOption)
        Console.WriteLine("Interactive mode.  User gets to approve or cancel each task.")
        Console.WriteLine(CommandLineInfo.CertifyRequestedOption)
        Console.WriteLine("Certify the instrument connected at the specified address.")
        Console.WriteLine(CommandLineInfo.DisplayCertificationOption)
        Console.WriteLine("Displays the certification. Will request password and serial number.")
        Console.WriteLine(CommandLineInfo.QuiteModeEnabledOption)
        Console.WriteLine("Quite mode.  Only errors are output.")
        Console.WriteLine("{0}<address>", CommandLineInfo.InstrumentResourceNameOption)
        Console.WriteLine("Specifies the instrument resource name.")
        Console.WriteLine("{0}<password>", CommandLineInfo.PasswordOption)
        Console.WriteLine("Specifies the password.")
        Console.WriteLine("{0}<serial number>", CommandLineInfo.SerialNumberOption)
        Console.WriteLine("Specifies the instrument serial number.")
        Console.WriteLine(CommandLineInfo.DisplayHelpOption)
        Console.WriteLine("Displays help")

    End Sub

    ''' <summary> Gets the 'Attended' command line option. </summary>
    ''' <value> The Attended' option. </value>
    Public Shared ReadOnly Property AttendedOption As String
        Get
            Return "-a"
        End Get
    End Property

    ''' <summary> Gets the Certify-Requested option. </summary>
    ''' <value> The Certify Requested option. </value>
    Public Shared ReadOnly Property CertifyRequestedOption As String
        Get
            Return "-n"
        End Get
    End Property

    ''' <summary> Gets the 'Display Certification' command line option. </summary>
    ''' <value> The Display Certification' option. </value>
    Public Shared ReadOnly Property DisplayCertificationOption As String
        Get
            Return "-d"
        End Get
    End Property

    ''' <summary> Gets the Interactive-Enabled option. </summary>
    ''' <value> The Interactive enabled option. </value>
    Public Shared ReadOnly Property InteractiveEnabledOption As String
        Get
            Return "-n"
        End Get
    End Property

    ''' <summary> Gets the Instrument Resource Name command line option. </summary>
    ''' <value> The Instrument Resource Name option. </value>
    Public Shared ReadOnly Property InstrumentResourceNameOption As String
        Get
            Return "-i"
        End Get
    End Property

    ''' <summary> Gets the Quite Mode Enabled option. </summary>
    ''' <value> <c>True</c> if quite mode is enabled. </value>
    Public Shared ReadOnly Property QuiteModeEnabledOption As String
        Get
            Return "-q"
        End Get
    End Property

    ''' <summary> Gets the Password command line option. </summary>
    ''' <value> The Password option. </value>
    Public Shared ReadOnly Property PasswordOption As String
        Get
            Return "-p"
        End Get
    End Property

    ''' <summary> Gets the salt option. </summary>
    ''' <value> The salt option. </value>
    Public Shared ReadOnly Property SaltOption As String
        Get
            Return "-S"
        End Get
    End Property

    ''' <summary> Gets the SerialNumber command line option. </summary>
    ''' <value> The SerialNumber option. </value>
    Public Shared ReadOnly Property SerialNumberOption As String
        Get
            Return "-s"
        End Get
    End Property

    ''' <summary> Gets the Display Help option. </summary>
    ''' <value> The display help option. </value>
    Public Shared ReadOnly Property DisplayHelpOption As String
        Get
            Return "-?"
        End Get
    End Property

#End Region

#Region " PARSER "

    ''' <summary> Validates the command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentException"> This exception is raised if a command line argument is
    '''                                      not handled. </exception>
    ''' <param name="commandLineArguments"> The command line arguments. </param>
    Public Shared Sub ValidateCommandLine(ByVal commandLineArguments As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArguments IsNot Nothing Then
            CommandLineInfo.QuiteModeEnabled = True
            CommandLineInfo.DisplayHelp = True
            For Each argument As String In commandLineArguments
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.AttendedOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.CertifyRequestedOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.DisplayCertificationOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.InteractiveEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.InstrumentResourceNameOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.QuiteModeEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.PasswordOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.SerialNumberOption, StringComparison.OrdinalIgnoreCase) Then
                ElseIf argument.StartsWith(CommandLineInfo.DisplayHelpOption, StringComparison.OrdinalIgnoreCase) Then
                Else
                    Throw New ArgumentException(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Unknown command line argument '{0}' was detected. Should be Ignored.", argument),
                                                          NameOf(commandLineArguments))
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="System.IO.FileNotFoundException"> Thrown when the requested file is not present. </exception>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    Public Shared Sub ParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String))

        If commandLineArgs IsNot Nothing Then
            For Each argument As String In commandLineArgs
                If False Then
                ElseIf argument.StartsWith(CommandLineInfo.AttendedOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.Attended = True
                ElseIf argument.StartsWith(CommandLineInfo.CertifyRequestedOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.CertifyRequested = True
                ElseIf argument.StartsWith(CommandLineInfo.DisplayCertificationOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.DisplayCertification = True
                ElseIf argument.StartsWith(CommandLineInfo.InteractiveEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                    ' Dim value As String = argument.Substring(CommandLineInfo.InteractiveEnabledOption.Length)
                    ' CommandLineInfo.InteractiveEnabled = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                    CommandLineInfo.InteractiveEnabled = True
                ElseIf argument.StartsWith(CommandLineInfo.InstrumentResourceNameOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.InstrumentResourceName = argument.Substring(CommandLineInfo.InstrumentResourceNameOption.Length)
                ElseIf argument.StartsWith(CommandLineInfo.QuiteModeEnabledOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.QuiteModeEnabled = True
                ElseIf argument.StartsWith(CommandLineInfo.PasswordOption, StringComparison.OrdinalIgnoreCase) Then
                    Dim pw As String = argument.Substring(CommandLineInfo.PasswordOption.Length)
                    If String.Equals(pw, "file", StringComparison.OrdinalIgnoreCase) AndAlso
                        My.Computer.FileSystem.FileExists(My.MySettings.Default.OkayFileName) Then
                        If My.Computer.FileSystem.FileExists(My.MySettings.Default.OkayFileName) Then
                            pw = My.Computer.FileSystem.ReadAllText(My.MySettings.Default.OkayFileName)
                        Else
                            Throw New System.IO.FileNotFoundException($"{NameOf(My.MySettings.OkayFileName)} file not found", My.MySettings.Default.OkayFileName)
                        End If
                    End If
                    CommandLineInfo.Password = pw
                ElseIf argument.StartsWith(CommandLineInfo.SaltOption, StringComparison.Ordinal) Then
                    Dim salt As String = argument.Substring(CommandLineInfo.SaltOption.Length)
                    If String.Equals(salt, "file", StringComparison.OrdinalIgnoreCase) AndAlso
                        My.Computer.FileSystem.FileExists(My.MySettings.Default.SaltFileName) Then
                        If My.Computer.FileSystem.FileExists(My.MySettings.Default.SaltFileName) Then
                            salt = My.Computer.FileSystem.ReadAllText(My.MySettings.Default.SaltFileName)
                        Else
                            Throw New System.IO.FileNotFoundException($"{NameOf(My.MySettings.SaltFileName)} file not found", My.MySettings.Default.SaltFileName)
                        End If
                    End If
                    CommandLineInfo.Salt = salt.Trim
                ElseIf argument.StartsWith(CommandLineInfo.SerialNumberOption, StringComparison.Ordinal) Then
                    CommandLineInfo.SerialNumber = argument.Substring(CommandLineInfo.SerialNumberOption.Length)
                ElseIf argument.StartsWith(CommandLineInfo.DisplayHelpOption, StringComparison.OrdinalIgnoreCase) Then
                    CommandLineInfo.DisplayHelp = True
                Else
                    ' do nothing
                End If
            Next
        End If

    End Sub

    ''' <summary> Parses the command line. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="commandLineArgs"> The command line arguments. </param>
    ''' <returns> True if success or false if Exception occurred. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function TryParseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

        Try

            CommandLineInfo.ParseCommandLine(commandLineArgs)
            Return True

        Catch ex As ArgumentException

            My.Application.Log.WriteException(ex, TraceEventType.Information, "Unknown argument ignored")
            Return True

        Catch ex As System.Exception

            If commandLineArgs Is Nothing Then
                My.Application.Log.WriteException(ex, TraceEventType.Error, "Failed parsing empty command line")

            Else
#Disable Warning CA1825 ' Avoid zero-length array allocations.
                Dim args As String() = {}
#Enable Warning CA1825 ' Avoid zero-length array allocations.
                commandLineArgs.CopyTo(args, 0)
                My.Application.Log.WriteException(ex, TraceEventType.Error,
                                                  String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                "Failed parsing command line '{0}'", String.Join(" ", args)))
            End If

            Return False
        End Try
    End Function

#End Region

#Region " COMMAND LINE ELEMENTS "

    ''' <summary> Gets or sets the 'attended ' option. </summary>
    ''' <remarks>
    ''' Messages are output to the console. The program exists after the operator enters a key at the
    ''' prompt.
    ''' </remarks>
    ''' <value>
    ''' <c>True</c> if user attended are enabled; otherwise, <c>False</c> or <c>null</c>.
    ''' </value>
    Public Shared Property Attended As Boolean?

    ''' <summary> Gets or sets a value indicating whether certification is requested. </summary>
    ''' <remarks> Certify the instrument connected at the specified address. </remarks>
    ''' <value>
    ''' <c>True</c> if certification is requested; otherwise, <c>False</c> or <c>null</c>.
    ''' </value>
    Public Shared Property CertifyRequested() As Boolean?

    ''' <summary> Gets or sets the 'DisplayCertification ' option. </summary>
    ''' <remarks> Displays the certification. Will request password and serial number. </remarks>
    ''' <value>
    ''' <c>True</c> if user DisplayCertification are enabled; otherwise, <c>False</c> or <c>null</c>.
    ''' </value>
    Public Shared Property DisplayCertification As Boolean?

    ''' <summary> Gets or sets a value indicating whether user interactions are enabled. </summary>
    ''' <remarks> Interactive mode.  User gets to approve or cancel each task. </remarks>
    ''' <value>
    ''' <c>True</c> if user interactions are enabled; otherwise, <c>False</c> or <c>null</c>.
    ''' </value>
    Public Shared Property InteractiveEnabled() As Boolean?

    ''' <summary> Gets or sets the instrument resource name. </summary>
    ''' <value> The instrument resource name. </value>
    Public Shared Property InstrumentResourceName As String

    ''' <summary> Gets or sets the quite mode sentinel. </summary>
    ''' <remarks> Quite mode.  Only errors are output. </remarks>
    ''' <value> <c>True</c> if quite mode; otherwise, <c>False</c> or <c>null</c>. </value>
    Public Shared Property QuiteModeEnabled() As Boolean?

    ''' <summary> Gets or sets the Password. </summary>
    ''' <value> The Password. </value>
    Public Shared Property Password As String

    ''' <summary> Gets or sets the salt. </summary>
    ''' <value> The salt. </value>
    Public Shared Property Salt As String

    ''' <summary> Gets or sets the SerialNumber. </summary>
    ''' <value> The SerialNumber. </value>
    Public Shared Property SerialNumber As String

    ''' <summary> Gets or sets a value indicating whether to display help. </summary>
    ''' <value> <c>True</c> to display help; otherwise, <c>False</c> or <c>null</c>. </value>
    Public Shared Property DisplayHelp() As Boolean?

#End Region

#Region " PASSWORD "

    ''' <summary> The password hash. </summary>
    Private Const _PasswordHash As String = "EtXykU6sTnHsX3XksXVrZFNwfyA="

    ''' <summary> Returns true if the password equals the password hash. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="password"> The password. </param>
    ''' <param name="salt">     The salt. </param>
    ''' <returns> <c>True</c> if passed; otherwise, <c>False</c>. </returns>
    Friend Shared Function IsPass(ByVal password As String, ByVal salt As String) As Boolean
        Return CommandLineInfo._PasswordHash = isr.Core.HashExtensions.HashExtensionMethods.ToBase64Hash($"{password}{salt}")
    End Function

#End Region

End Class
