Imports isr.Core
Namespace My

    Partial Friend Class MyApplication

#Region " APPLICATION EXTENSIONS "

#End Region

#Region " APPLICATION EVENTS "

        ''' <summary> Collection of command lines. </summary>
        Private _CommandLineCollection As Collections.ObjectModel.ReadOnlyCollection(Of String)

        ''' <summary>
        ''' Gets command line arguments so that custom arguments could be used in design mode or
        ''' interactive mode.
        ''' </summary>
        ''' <value> A Collection of command lines. </value>
        Public ReadOnly Property CommandLineCollection() As Collections.ObjectModel.ReadOnlyCollection(Of String)
            Get
                Return Me._CommandLineCollection
            End Get
        End Property

        ''' <summary>
        ''' Replaces the default trace listener with the modified listener. Updates the minimum splash
        ''' screen display time.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        Friend Sub InitializeKnownState()
            Me.CreateLogger()
            Dim commandLine As String = String.Empty
            If Debugger.IsAttached AndAlso My.Application.CommandLineArgs.Count = 0 Then
                Console.Write("Enter command line arguments (e.g., -? -n -a26 -pPassword): ")
                commandLine = Console.ReadLine()
                ' make sure the command line presume interactive mode.
                If commandLine.IndexOf("-n", 0, StringComparison.OrdinalIgnoreCase) < 0 Then
                    commandLine = "-n " & commandLine
                End If
                Me._CommandLineCollection = If(String.IsNullOrWhiteSpace(commandLine),
                    My.Application.CommandLineArgs(),
                    New Collections.ObjectModel.ReadOnlyCollection(Of String)(commandLine.Split(" "c)))
            Else
                Me._CommandLineCollection = My.Application.CommandLineArgs()
                commandLine = String.Join(" ", Me._CommandLineCollection.ToArray)
            End If
            MyApplication.ConsoleWriteLine("Using the following command line:")
            MyApplication.ConsoleWriteLine(commandLine)
        End Sub

        ''' <summary> Executes shut down operations. </summary>
        ''' <remarks> Saves user settings for all related libraries. </remarks>
        Friend Sub Shutdown()

            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()

            ' For some reason the event handling set in the Settings class dos not really work.
            My.MySettings.Default.Save()

            ' do some garbage collection
            System.GC.Collect()

        End Sub

#End Region

#Region " CONSOLE MANAGEMENT "

        ''' <summary>
        ''' Writes a line to the console Error output. Output line to the application error log.
        ''' </summary>
        ''' <remarks> Use this method for all error reporting. </remarks>
        ''' <param name="format"> Specifies the message format. </param>
        ''' <param name="args">   Specified the message arguments. </param>
        Friend Shared Sub ConsoleErrorWriteLine(ByVal format As String, ByVal ParamArray args() As Object)
            MyApplication.ConsoleErrorWriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Sub

        ''' <summary>
        ''' Writes a line to the console Error output. Output line to the application error log.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="message"> The message. </param>
        Friend Shared Sub ConsoleErrorWriteLine(ByVal message As String)
            Console.Error.WriteLine(message)
            My.Application.Log.WriteEntry(message, TraceEventType.Error)
        End Sub

        ''' <summary>
        ''' Writes a line to the console output if not quite mode. Outputs a line to the application log.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        Friend Shared Sub ConsoleOutWriteLine(ByVal format As String, ByVal ParamArray args() As Object)
            MyApplication.ConsoleOutWriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Sub

        ''' <summary>
        ''' Writes a line to the console output if not quite mode. Outputs a line to the application log.
        ''' </summary>
        ''' <remarks> This method is called when output to the console. </remarks>
        ''' <param name="message"> The message. </param>
        ''' <param name="isQuiet"> true if this object is quiet. </param>
        Friend Sub ConsoleOutWriteLine(ByVal message As String, ByVal isQuiet As Boolean)
            If Not isQuiet Then
                Console.Out.WriteLine(message)
            End If
            My.Application.Log.WriteEntry(message, TraceEventType.Verbose)
        End Sub

        ''' <summary>
        ''' Queries the user from the console. In attended mode, this just displays the message. In
        ''' interactive mode, this adds the query 'Continue?' and waits of a key.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="format"> Specifies the message format. </param>
        ''' <param name="args">   Specified the message arguments. </param>
        ''' <returns> <c>True</c> if Y; otherwise, <c>False</c>. </returns>
        Friend Shared Function ConsoleQueryLine(ByVal format As String, ByVal ParamArray args() As Object) As Boolean
            Return MyApplication.ConsoleQueryLine(String.Format(Globalization.CultureInfo.CurrentCulture, format, args), False, False)
        End Function

        ''' <summary>
        ''' Queries the user from the console. In attended mode, this just displays the message. In
        ''' interactive mode, this adds the query 'Continue?' and waits for a key.
        ''' </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="message">       The message. </param>
        ''' <param name="isInteractive"> true if this object is interactive. </param>
        ''' <param name="attended">      true if attended. </param>
        ''' <returns> <c>True</c> if Y; otherwise, <c>False</c>. </returns>
        Friend Shared Function ConsoleQueryLine(ByVal message As String, ByVal isInteractive As Boolean, ByVal attended As Boolean) As Boolean
            If isInteractive Then
                Console.WriteLine(message)
                Console.Write("Continue (y/n)? ")
                Dim reply As System.ConsoleKeyInfo = Console.ReadKey()
                Console.WriteLine()
                Return Char.Equals(reply.KeyChar, "y"c) OrElse Char.Equals(reply.KeyChar, "Y"c)
            ElseIf attended Then
                Console.WriteLine(message)
                Return True
            Else
                Return True
            End If
        End Function

        ''' <summary> Writes a line to the console in interactive or attended modes. </summary>
        ''' <remarks> Use this method for outputting to the console and standard output. </remarks>
        ''' <param name="format"> Describes the format to use. </param>
        ''' <param name="args">   A variable-length parameters list containing arguments. </param>
        ''' <returns> The written text. </returns>
        Friend Function ConsoleWriteLine(ByVal format As String, ByVal ParamArray args() As Object) As String
            Return MyApplication.ConsoleWriteLine(String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End Function

        ''' <summary> Writes a line to the console. </summary>
        ''' <remarks> Use this method for outputting to the console and standard output. </remarks>
        ''' <param name="message"> The message. </param>
        ''' <returns> The written text. </returns>
        Friend Shared Function ConsoleWriteLine(ByVal message As String) As String
            Console.WriteLine(message)
            Return message
        End Function

        ''' <summary> Writes a line to the console in interactive or attended modes. </summary>
        ''' <remarks> Use this method for outputting to the console and standard output. </remarks>
        ''' <param name="message">       The message. </param>
        ''' <param name="isInteractive"> true if this object is interactive. </param>
        ''' <param name="attended">      true if attended. </param>
        ''' <returns> The written text. </returns>
        Friend Function ConsoleWriteLine(ByVal message As String, ByVal isInteractive As Boolean, ByVal attended As Boolean) As String
            If isInteractive OrElse attended Then
                Console.WriteLine(message)
            Else
                ConsoleOutWriteLine(message)
            End If
            Return message
        End Function

        ''' <summary> Read a password from the console into a string. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="prompt"> . </param>
        ''' <returns> The password text. </returns>
        Public Shared Function ReadPassword(ByVal prompt As String) As String

            Dim password As String = String.Empty

            Console.Write(prompt)

            ' get the first character of the password
            Dim nextKey As ConsoleKeyInfo = Console.ReadKey(True)

            While nextKey.Key <> ConsoleKey.Enter

                If nextKey.Key = ConsoleKey.Backspace Then

                    If password.Length > 0 Then

                        ' erase the last * as well
                        Console.Write(nextKey.KeyChar)
                        Console.Write(" ")
                        Console.Write(nextKey.KeyChar)

                    End If

                    If password.Length <= 0 Then

                    ElseIf password.Length = 1 Then

                        password = String.Empty

                    ElseIf password.Length >= 2 Then

                        password = password.Substring(0, password.Length - 1)

                    End If

                Else

                    password &= nextKey.KeyChar
                    Console.Write("*")

                End If

                nextKey = Console.ReadKey(True)

            End While

            Console.WriteLine()

            Return password.Trim

        End Function

        ''' <summary> Enters a password from the console. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <returns> The password text. </returns>
        Friend Shared Function EnterPassword() As String
            Dim password As String = ReadPassword("Enter Password: ")
            Dim retryPassword As String = ReadPassword("Confirm Password: ")
            Return If(String.Equals(password, retryPassword), password, MyApplication.EnterPassword())
            Return password
        End Function

#End Region

#Region " CERTIFICATION MANAGEMENT "

        ''' <summary> Creates default local or remote jobs. </summary>
        ''' <remarks> David, 2020-10-12. </remarks>
        ''' <param name="password">     The password. </param>
        ''' <param name="serialNumber"> The serial number. </param>
        ''' <param name="salt">         The salt. </param>
        Friend Sub DisplayCertification(ByVal password As String, ByVal serialNumber As String, ByVal salt As String)

            ' select Windows Principal policy
            System.AppDomain.CurrentDomain.SetPrincipalPolicy(Security.Principal.PrincipalPolicy.WindowsPrincipal)
            Dim accountName As String = System.Threading.Thread.CurrentPrincipal.Identity.Name

            If String.IsNullOrWhiteSpace(password) Then
                password = MyApplication.EnterPassword()
            End If

            If CommandLineInfo.IsPass(password, salt) Then

                If String.IsNullOrWhiteSpace(serialNumber) Then
                    Console.WriteLine("Enter serial number")
                    serialNumber = Console.ReadLine()
                End If

                Console.WriteLine(isr.Core.HashExtensions.HashExtensionMethods.ToBase64Hash($"{serialNumber}{salt}"))
                Console.WriteLine("Enter a key:")
                Console.ReadKey()

            End If

        End Sub

#End Region

    End Class

End Namespace

