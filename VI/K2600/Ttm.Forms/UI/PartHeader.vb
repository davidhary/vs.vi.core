Imports System.ComponentModel

Imports isr.Core.Forma
Imports isr.VI.ExceptionExtensions

''' <summary> Part header. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-04-12 </para>
''' </remarks>
Public Class PartHeader
    Inherits ModelViewBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        Me._PartNumberTextBox.Text = String.Empty
        Me._PartSerialNumberTextBox.Text = String.Empty

    End Sub

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub ReleaseResources()
        Me._DeviceUnderTest = Nothing
    End Sub

#End Region

#Region " DUT "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _DeviceUnderTest As DeviceUnderTest
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the device under test. </summary>
    ''' <value> The device under test. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property DeviceUnderTest As DeviceUnderTest
        Get
            Return Me._DeviceUnderTest
        End Get
        Set(value As DeviceUnderTest)
            Me._DeviceUnderTest = value
        End Set
    End Property

    ''' <summary> Executes the property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As DeviceUnderTest, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.DeviceUnderTest.PartNumber)
                If String.IsNullOrWhiteSpace(sender.PartNumber) Then
                    Me.Visible = False
                Else
                    Me.Visible = True
                    Me._PartNumberTextBox.Text = sender.PartNumber
                End If
            Case NameOf(Ttm.DeviceUnderTest.SerialNumber)
                Me._PartSerialNumberTextBox.Text = sender.SerialNumber.ToString
        End Select
    End Sub

    ''' <summary> Event handler. Called by _DeviceUnderTest for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub DeviceUnderTest_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _DeviceUnderTest.PropertyChanged
        Try
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.DeviceUnderTest_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, DeviceUnderTest), e?.PropertyName)
            End If
        Catch ex As Exception
            Debug.Assert(Not Debugger.IsAttached, "Exception handling property", "Exception handling '{0}' property change. {1}.", e.PropertyName, ex.ToFullBlownString)

        End Try
    End Sub

#End Region

End Class
