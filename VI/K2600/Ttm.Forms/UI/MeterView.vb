Imports System.ComponentModel
Imports System.Windows.Forms
Imports isr.Core
Imports isr.Core.EnumExtensions
Imports isr.Core.NumericExtensions
Imports isr.Core.Forma
Imports isr.VI.Tsp.K2600
Imports isr.VI.ExceptionExtensions

''' <summary> A meter view. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2018-12-31 </para>
''' </remarks>
Public Class MeterView
    Inherits isr.Core.Forma.ModelViewTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Sub New()
        MyBase.New()
        Me.InitializingComponents = True
        ' This call is required by the Windows Form Designer.
        Me.InitializeComponent()

        ' note that the caption is not set if this is run inside the On Load function.
        ' set defaults for the messages box.
        Me._TraceMessagesBox.ResetCount = 500
        Me._TraceMessagesBox.PresetCount = 250
        Me._TraceMessagesBox.ContainerPanel = Me._MessagesTabPage
        Me._TraceMessagesBox.AlertsToggleControl = Me._MessagesTabPage
        Me._TraceMessagesBox.CommenceUpdates()
        Me._Tabs.DrawMode = TabDrawMode.OwnerDrawFixed
        MyBase.AddPrivateListener(Me._TraceMessagesBox)
        Me.ApplyShuntResistanceButtonCaption = Me._ApplyShuntResistanceConfigurationButton.Text
        Me.ApplyNewShuntResistanceButtonCaption = Me._ApplyNewShuntResistanceConfigurationButton.Text

        ' hide the alerts
        Me._MeasurementsHeader.ShowAlerts(False, False)
        Me._MeasurementsHeader.ShowOutcome(False, False)

        Me.InitializingComponents = False

    End Sub

    ''' <summary> Creates a new <see cref="MeterView"/> </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> A <see cref="MeterView"/>. </returns>
    Public Shared Function Create() As MeterView
        Dim view As MeterView = Nothing
        Try
            view = New MeterView
            Return view
        Catch
            view.Dispose()
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:System.Windows.Forms.Control" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
    '''                                                   <c>False</c> to release only unmanaged
    '''                                                   resources when called from the runtime
    '''                                                   finalize. </param>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If Me.IsDisposed Then Return
        Try
            If disposing Then
                Me.InitializingComponents = True
                Me._TraceMessagesBox.SuspendUpdatesReleaseIndicators()
                Me.BindPart(Nothing)
                Me.AssignMeter(Nothing)
                If Me._Meter IsNot Nothing Then Me._Meter.Dispose() : Me._Meter = Nothing
                If Me.components IsNot Nothing Then Me.components.Dispose() : Me.components = Nothing
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

#End Region

#Region " EVENT HANDLERS:  FORM "

    ''' <summary>
    ''' Handles the container form <see cref="E:System.Windows.Forms.Closing" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> An <see cref="T:ComponentModel.CancelEventArgs" /> that contains the event
    '''                  data. </param>
    Protected Overrides Sub OnFormClosing(e As CancelEventArgs)
        MyBase.OnFormClosing(e)
        If e IsNot Nothing AndAlso Not e.Cancel Then
            Me.RemovePrivateListener(Me._TraceMessagesBox)
        End If
    End Sub

    ''' <summary>
    ''' Called upon receiving the <see cref="E:System.Windows.Forms.Form.Load" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="e"> An <see cref="T:System.EventArgs" /> that contains the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        Dim activity As String = String.Empty
        Try
            Me.Cursor = Cursors.WaitCursor
            activity = $"Loading the driver console form"
            Trace.CorrelationManager.StartLogicalOperation(Reflection.MethodInfo.GetCurrentMethod.Name)

            If Not Me.DesignMode Then
                ' add listeners before the first talker publish command
                Me.AddPrivateListeners()
                Me.AssignMeter(New Meter)
            End If

            Me.PublishVerbose($"{activity};. ")

            ' build the navigator tree.
            Me.BuildNavigatorTreeView()

        Catch ex As Exception
            Me.PublishException(activity, ex)
            If isr.Core.MyDialogResult.Abort = isr.Core.WindowsForms.ShowDialogAbortIgnore(ex) Then
                Application.Exit()
            End If
        Finally

            MyBase.OnLoad(e)
            Me.Cursor = System.Windows.Forms.Cursors.Default
            Trace.CorrelationManager.StopLogicalOperation()

            isr.Core.ApplianceBase.DoEvents()
            Me.SelectNavigatorTreeViewNode(TreeViewNode.ConnectNode)
            Me._ResourceSelectorConnector.Focus()
            isr.Core.ApplianceBase.DoEvents()

        End Try

    End Sub

#End Region

#Region " DEVICE UNDER TEST "

    ''' <summary> Gets the device under test. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The device under test. </value>
    Private Property DeviceUnderTest As DeviceUnderTest

#End Region

#Region " SHUNT "

#Region " CONFIGURE SHUNT"

    ''' <summary> Bind controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub BindShuntControls()

        ' set the GUI based on the current defaults.
        Dim instrumentSettings As isr.VI.Ttm.My.MySettings = isr.VI.Ttm.My.MySettings.Default

        Me._ShuntResistanceCurrentRangeNumeric.Minimum = instrumentSettings.ShuntResistanceCurrentMinimum
        Me._ShuntResistanceCurrentRangeNumeric.Maximum = instrumentSettings.ShuntResistanceCurrentMaximum
        Me._ShuntResistanceCurrentRangeNumeric.DataBindings.Clear()
        Me._ShuntResistanceCurrentRangeNumeric.DataBindings.Add(New Binding("Value", Me.Part.ShuntResistance, "CurrentRange", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ShuntResistanceCurrentRangeNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentRange.Clip(Me._ShuntResistanceCurrentRangeNumeric.Minimum, Me._ShuntResistanceCurrentRangeNumeric.Maximum)
        Me.Part.ShuntResistance.CurrentRange = Me._ShuntResistanceCurrentRangeNumeric.Value

        Me._ShuntResistanceCurrentLevelNumeric.Minimum = instrumentSettings.ShuntResistanceCurrentMinimum
        Me._ShuntResistanceCurrentLevelNumeric.Maximum = instrumentSettings.ShuntResistanceCurrentMaximum
        Me._ShuntResistanceCurrentLevelNumeric.DataBindings.Clear()
        Me._ShuntResistanceCurrentLevelNumeric.DataBindings.Add(New Binding("Value", Me.Part.ShuntResistance, "CurrentLevel", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ShuntResistanceCurrentLevelNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel.Clip(Me._ShuntResistanceCurrentLevelNumeric.Minimum, Me._ShuntResistanceCurrentLevelNumeric.Maximum)
        Me.Part.ShuntResistance.CurrentLevel = Me._ShuntResistanceCurrentLevelNumeric.Value

        Me._ShuntResistanceHighLimitNumeric.Minimum = instrumentSettings.ShuntResistanceMinimum
        Me._ShuntResistanceHighLimitNumeric.Maximum = instrumentSettings.ShuntResistanceMaximum
        Me._ShuntResistanceHighLimitNumeric.DataBindings.Clear()
        Me._ShuntResistanceHighLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.ShuntResistance, "HighLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ShuntResistanceHighLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceHighLimit.Clip(Me._ShuntResistanceHighLimitNumeric.Minimum, Me._ShuntResistanceHighLimitNumeric.Maximum)
        Me.Part.ShuntResistance.HighLimit = Me._ShuntResistanceHighLimitNumeric.Value

        Me._ShuntResistanceLowLimitNumeric.Minimum = instrumentSettings.ShuntResistanceMinimum
        Me._ShuntResistanceLowLimitNumeric.Maximum = instrumentSettings.ShuntResistanceMaximum
        Me._ShuntResistanceLowLimitNumeric.DataBindings.Clear()
        Me._ShuntResistanceLowLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.ShuntResistance, "LowLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ShuntResistanceLowLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceLowLimit.Clip(Me._ShuntResistanceLowLimitNumeric.Minimum, Me._ShuntResistanceLowLimitNumeric.Maximum)
        Me.Part.ShuntResistance.LowLimit = Me._ShuntResistanceLowLimitNumeric.Value

        Me._ShuntResistanceVoltageLimitNumeric.Minimum = instrumentSettings.ShuntResistanceVoltageMinimum
        Me._ShuntResistanceVoltageLimitNumeric.Maximum = instrumentSettings.ShuntResistanceVoltageMaximum
        Me._ShuntResistanceVoltageLimitNumeric.DataBindings.Clear()
        Me._ShuntResistanceVoltageLimitNumeric.DataBindings.Add(New Binding("Value", Me.Part.ShuntResistance, "VoltageLimit", True, DataSourceUpdateMode.OnPropertyChanged))
        Me._ShuntResistanceVoltageLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit.Clip(Me._ShuntResistanceVoltageLimitNumeric.Minimum, Me._ShuntResistanceVoltageLimitNumeric.Maximum)
        Me.Part.ShuntResistance.VoltageLimit = Me._ShuntResistanceVoltageLimitNumeric.Value

    End Sub

    ''' <summary> Restore defaults. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Shared Sub RestoreShuntDefaults()
        isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentRange = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentRangeDefault
        isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentLevelDefault
        isr.VI.Ttm.My.MySettings.Default.ShuntResistanceHighLimit = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceHighLimitDefault
        isr.VI.Ttm.My.MySettings.Default.ShuntResistanceLowLimit = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceLowLimitDefault
        isr.VI.Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceVoltageLimitDefault
    End Sub

    ''' <summary> Copy shunt values to the settings store. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub CopyShuntSettings()
        If Me.Part IsNot Nothing Then
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentRange = CDec(Me.Part.ShuntResistance.CurrentRange)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel = CDec(Me.Part.ShuntResistance.CurrentLevel)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceHighLimit = CDec(Me.Part.ShuntResistance.HighLimit)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceLowLimit = CDec(Me.Part.ShuntResistance.LowLimit)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit = CDec(Me.Part.ShuntResistance.VoltageLimit)
        End If
    End Sub

    ''' <summary> Updates the shunt bound values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub UpdateShuntBoundValues()
        If Me.Meter IsNot Nothing AndAlso Me.Meter.ShuntResistance IsNot Nothing Then
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel = CDec(Me.Meter.ShuntResistance.CurrentLevel)
            Me._ShuntResistanceCurrentLevelNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentLevel.Clip(Me._ShuntResistanceCurrentLevelNumeric.Minimum, Me._ShuntResistanceCurrentLevelNumeric.Maximum)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentRange = CDec(Me.Meter.ShuntResistance.CurrentRange)
            Me._ShuntResistanceCurrentRangeNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceCurrentRange.Clip(Me._ShuntResistanceCurrentRangeNumeric.Minimum, Me._ShuntResistanceCurrentRangeNumeric.Maximum)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit = CDec(Me.Meter.ShuntResistance.VoltageLimit)
            Me._ShuntResistanceVoltageLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceVoltageLimit.Clip(Me._ShuntResistanceVoltageLimitNumeric.Minimum, Me._ShuntResistanceVoltageLimitNumeric.Maximum)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceHighLimit = CDec(Me.Meter.ShuntResistance.HighLimit)
            Me._ShuntResistanceHighLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceHighLimit.Clip(Me._ShuntResistanceHighLimitNumeric.Minimum, Me._ShuntResistanceHighLimitNumeric.Maximum)
            isr.VI.Ttm.My.MySettings.Default.ShuntResistanceLowLimit = CDec(Me.Meter.ShuntResistance.LowLimit)
            Me._ShuntResistanceLowLimitNumeric.Value = isr.VI.Ttm.My.MySettings.Default.ShuntResistanceLowLimit.Clip(Me._ShuntResistanceLowLimitNumeric.Minimum, Me._ShuntResistanceLowLimitNumeric.Maximum)
        End If
    End Sub

    ''' <summary> Gets the apply new shunt resistance button caption. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The apply shunt resistance button caption. </value>
    Private Property ApplyNewShuntResistanceButtonCaption As String

    ''' <summary> Gets the apply shunt resistance button caption. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The apply shunt resistance button caption. </value>
    Private Property ApplyShuntResistanceButtonCaption As String

    ''' <summary> Is new shunt resistance settings. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns>
    ''' <c>True</c> if settings where updated so that meter settings needs to be updated.
    ''' </returns>
    Private Function IsNewShuntResistanceSettings() As Boolean
        Return Me.DeviceUnderTest IsNot Nothing AndAlso
               Not Me._Part.ShuntResistance.ConfigurationEquals(Me.DeviceUnderTest.ShuntResistance)
    End Function

    ''' <summary> Updates the shunt configuration button caption. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub UpdateShuntConfigButtonCaption()
        Dim caption As String = Me.ApplyShuntResistanceButtonCaption
        Dim changedCaption As String = Me.ApplyNewShuntResistanceButtonCaption
        If Me.IsNewShuntResistanceSettings Then
            caption &= " !"
            changedCaption &= " !"
        End If
        If Not caption.Equals(Me._ApplyShuntResistanceConfigurationButton.Text) Then
            Me._ApplyShuntResistanceConfigurationButton.Text = caption
        End If
        If Not changedCaption.Equals(Me._ApplyNewShuntResistanceConfigurationButton.Text) Then
            Me._ApplyNewShuntResistanceConfigurationButton.Text = changedCaption
        End If
    End Sub

    ''' <summary>
    ''' Handles the Click event of the _ApplyShuntResistanceConfigurationButton control. Saves the
    ''' configuration settings and sends them to the meter.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.EventArgs" /> instance containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyShuntResistanceConfigurationButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _ApplyShuntResistanceConfigurationButton.Click

        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} Configuring Shunt Resistance"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.PublishVerbose("Configuring Shunt Resistance;. ")

            If Me.Meter.IsDeviceOpen Then

                ' not required. 
                ' Me.Meter.ClearExecutionState()
                Me.PublishVerbose("Sending Shunt resistance configuration settings to the meter;. ")
                Me.Meter.ConfigureShuntResistance(Me.Part.ShuntResistance)
                Me.PublishVerbose("Shunt resistance measurement configured successfully;. ")

            Else

                Me.InfoProvider.SetError(Me._ApplyShuntResistanceConfigurationButton, "Meter not connected")
                Me.PublishWarning("Meter not connected;. ")

            End If

        Catch ex As Exception
            Me.InfoProvider.SetError(Me._ApplyShuntResistanceConfigurationButton,
                                       "Failed configuring shunt resistance")
            Me.PublishException(activity, ex)
        Finally
            Me.UpdateShuntConfigButtonCaption()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Applies new shunt resistance settings.
    '''           Event handler. Called by _ApplyNewShuntResistanceConfigurationButton for click
    ''' events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ApplyNewShuntResistanceConfigurationButton_Click(sender As Object, e As System.EventArgs) Handles _ApplyNewShuntResistanceConfigurationButton.Click
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} Configuring Shunt Resistance"
            Me.Cursor = Cursors.WaitCursor
            Me.InfoProvider.Clear()
            Me.PublishVerbose("Configuring Shunt Resistance;. ")

            If Me.Meter.IsDeviceOpen Then

                ' not required. 
                ' Me.Meter.ClearExecutionState()
                Me.PublishVerbose("Sending Shunt resistance configuration settings to the meter;. ")
                Me.Meter.ConfigureShuntResistanceChanged(Me.Part.ShuntResistance)
                Me.PublishVerbose("Shunt resistance measurement configured successfully;. ")
            Else

                Me.InfoProvider.SetError(Me._ApplyShuntResistanceConfigurationButton, "Meter not connected")
                Me.PublishWarning("Meter not connected;. ")

            End If

        Catch ex As Exception
            Me.InfoProvider.SetError(Me._ApplyShuntResistanceConfigurationButton, "Failed configuring shunt resistance")
            Me.PublishException(activity, ex)
        Finally
            Me.UpdateShuntConfigButtonCaption()
            Me.Cursor = Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Event handler. Called by _RestoreShuntResistanceDefaultsButton for click events.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    Private Sub RestoreShuntResistanceDefaultsButton_Click(sender As System.Object, e As System.EventArgs) Handles _RestoreShuntResistanceDefaultsButton.Click

        ' read the instrument default settings.
        MeterView.RestoreShuntDefaults()

        ' reset part to know state based on the current defaults
        Me.Part.ShuntResistance.ResetKnownState()
        isr.Core.ApplianceBase.DoEvents()

        ' bind.
        Me.BindShuntControls()

    End Sub


#End Region

#Region " MEASURE SHUNT "

    ''' <summary> Measures Shunt resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureShuntResistanceButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _MeasureShuntResistanceButton.Click
        SyncLock Me.Meter
            Dim activity As String = String.Empty
            Try
                activity = $"{Me.Meter.ResourceName} measuring Shunt Resistance"
                Me.Cursor = Cursors.WaitCursor
                Me.InfoProvider.SetError(Me._ShuntResistanceTextBox, "")
                Me.InfoProvider.SetError(Me._MeasureShuntResistanceButton, "")
                Me.InfoProvider.SetIconPadding(Me._MeasureShuntResistanceButton, -15)
                Me.PublishVerbose("Measuring Shunt Resistance...;. ")
                Me.Meter.MeasureShuntResistance(Me.Part.ShuntResistance)
                Me.PublishVerbose("Shunt Resistance measured;. ")
            Catch ex As Exception
                Me._ShuntResistanceTextBox.Text = String.Empty
                Me.InfoProvider.SetError(Me._MeasureShuntResistanceButton, "Failed Measuring Shunt Resistance")
                Me.PublishException(activity, ex)
            Finally
                Me.Cursor = Cursors.Default
            End Try
        End SyncLock
    End Sub

#End Region

#End Region

#Region " DISPLAY "

    ''' <summary> Displays the thermal transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox">    The text box control. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub SetErrorProvider(ByVal textBox As TextBox, ByVal resistance As ResistanceMeasureBase)
        If (resistance.Outcome And MeasurementOutcomes.PartFailed) <> 0 Then
            Me.InfoProvider.SetError(textBox, "Value out of range")
        ElseIf (resistance.Outcome And MeasurementOutcomes.MeasurementFailed) <> 0 Then
            Me.InfoProvider.SetError(textBox, "Measurement failed")
        ElseIf (resistance.Outcome And MeasurementOutcomes.MeasurementNotMade) <> 0 Then
            Me.InfoProvider.SetError(textBox, "Measurement not made")
        End If
    End Sub

    ''' <summary> Displays the resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox">    The text box control. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub ShowResistance(ByVal textBox As TextBox, ByVal resistance As ResistanceMeasureBase)
        textBox.Text = resistance.ResistanceCaption
        Me.SetErrorProvider(textBox, resistance)
    End Sub

    ''' <summary> Displays the thermal transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="textBox">    The text box control. </param>
    ''' <param name="resistance"> The resistance. </param>
    Private Sub ShowThermalTransient(ByVal textBox As TextBox, ByVal resistance As ResistanceMeasureBase)
        textBox.Text = resistance.VoltageCaption
        Me.SetErrorProvider(textBox, resistance)
    End Sub

#End Region

#Region " PART "

    ''' <summary> Gets the part. </summary>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <value> The part. </value>
    Private ReadOnly Property Part As DeviceUnderTest

    ''' <summary> Bind part. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub BindPart(ByVal value As DeviceUnderTest)
        If Me.Part IsNot Nothing Then
            Me._ResourceSelectorConnector.Enabled = False
        End If
        Me._Part = value
        If value IsNot Nothing Then
            Me._ResourceSelectorConnector.Openable = True : Me._ResourceSelectorConnector.Clearable = True : Me._ResourceSelectorConnector.Searchable = True
            Me.PublishInfo("Enabling controls;. ")
        End If
        Me.BindShuntResistance(value)
        Me.OnMeasurementStatusChanged()
        If value IsNot Nothing Then
            Me.PublishInfo("Ready - List resources and select one to connect too;. ")
            Me._ResourceSelectorConnector.Enabled = True
        End If
    End Sub

    ''' <summary> Query if 'e' is closing ready. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Cancel event information. </param>
    ''' <returns> True if closing ready, false if not. </returns>
    Public Function IsClosingReady(ByVal e As System.ComponentModel.CancelEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Try
            If Me._PartsPanel.SaveEnabled Then
                Dim dialogResult As Windows.Forms.DialogResult = System.Windows.Forms.MessageBox.Show("Data not saved. Select Yes to save, no to skip saving or cancel to cancel closing the program.",
                                                                   "SAVE DATA?", MessageBoxButtons.YesNoCancel, Windows.Forms.MessageBoxIcon.Question,
                                                                   MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
                If dialogResult = Windows.Forms.DialogResult.Yes Then
                    Me._PartsPanel.SaveParts()
                ElseIf dialogResult = Windows.Forms.DialogResult.No Then
                ElseIf dialogResult = Windows.Forms.DialogResult.Cancel Then
                    If e IsNot Nothing Then
                        e.Cancel = True
                    End If
                Else
                    If e IsNot Nothing Then
                        e.Cancel = True
                    End If
                End If
            End If

            If e Is Nothing OrElse Not e.Cancel Then

                Me._PartsPanel.CopySettings()
                Me._TTMConfigurationPanel.CopySettings()
                Me.CopyShuntSettings()
                isr.VI.Ttm.My.MySettings.Default.ResourceName = Me.ResourceName
                isr.VI.Ttm.My.MySettings.Default.Save()

                ' flush the log.
                My.Application.Log.TraceSource.Flush()

                ' wait for timer to terminate all is actions
                isr.Core.ApplianceBase.DoEventsWait(TimeSpan.FromMilliseconds(400))

                ' allow all events requiring the panel to execute on their thread.
                ' this allows all timer events that where in progress to be consummated before closing the form.
                ' this does not prevent timer exceptions in design mode.
                For i As Integer = 1 To 1000
                    Windows.Forms.Application.DoEvents()
                Next
            End If

        Finally


            isr.Core.ApplianceBase.DoEvents()
        End Try
        Return Not e.Cancel
    End Function

#End Region

#Region " PART: SHUNT RESISTANCE "

    ''' <summary> Gets the shunt resistance. </summary>
    ''' <value> The shunt resistance. </value>
    Private ReadOnly Property ShuntResistance As ShuntResistance

    ''' <summary> Bind shunt resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub BindShuntResistance(ByVal value As DeviceUnderTest)
        If Me.ShuntResistance IsNot Nothing Then
            RemoveHandler Me.ShuntResistance.PropertyChanged, AddressOf Me.ShuntResistancePropertyChanged
            Me._ShuntResistance = Nothing
        End If
        If value IsNot Nothing Then
            Me._ShuntResistance = value.ShuntResistance
            AddHandler Me.ShuntResistance.PropertyChanged, AddressOf Me.ShuntResistancePropertyChanged
            Me.HandlePropertyChanged(Me.ShuntResistance, NameOf(Ttm.ShuntResistance.MeasurementAvailable))
        End If
    End Sub

    ''' <summary> Raises the property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal sender As ShuntResistance, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Me.UpdateShuntConfigButtonCaption()
        Select Case propertyName
            Case NameOf(Ttm.ShuntResistance.MeasurementAvailable)
                If sender.MeasurementAvailable Then
                    Me.ShowResistance(Me._ShuntResistanceTextBox, sender)
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _ShuntResistance for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub ShuntResistancePropertyChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling shunt resistance {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.ShuntResistancePropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, ShuntResistance), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " PARTS "

    ''' <summary> Parts panel property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub PartsPanel_PropertyChanged(sender As Object, e As System.ComponentModel.PropertyChangedEventArgs) Handles _PartsPanel.PropertyChanged
        If sender IsNot Nothing AndAlso e IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(e.PropertyName) Then
        End If
    End Sub

    ''' <summary> Raises the property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(sender As ConfigurationView, propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.Forms.ConfigurationView.IsNewConfigurationSettingAvailable)
                If Me._NavigatorTreeView.Nodes IsNot Nothing AndAlso Me._NavigatorTreeView.Nodes.Count > 0 Then
                    Dim caption As String = TreeViewNode.ConfigureNode.Description
                    If sender.IsNewConfigurationSettingAvailable Then
                        caption &= " *"
                    End If
                    Me._NavigatorTreeView.Nodes(TreeViewNode.ConfigureNode.ToString).Text = caption
                End If
        End Select
    End Sub

    ''' <summary> Ttm configuration panel property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TTMConfigurationPanel_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TTMConfigurationPanel.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling configuration panel {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TTMConfigurationPanel_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, ConfigurationView), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " TTM METER "

    ''' <summary> Gets reference to the thermal transient meter device. </summary>
    ''' <value> The meter. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Meter() As Meter

    ''' <summary> Assigns a Meter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Private Sub AssignMeterThis(ByVal value As Meter)
        If Me._Meter IsNot Nothing Then
            Me.AssignTalker(Nothing)
            RemoveHandler Me.Meter.PropertyChanged, AddressOf Me.MeterPropertyChanged
            Me._Meter.Dispose()
            Me._Meter = Nothing
        End If
        Me._Meter = value
        If Me.Meter IsNot Nothing Then
            Me.AssignTalker(Me.Meter.Talker)
            Me.Meter.AddListeners(Me.Talker)
            Me.AddPrivateListeners()
            AddHandler Me.Meter.PropertyChanged, AddressOf Me.MeterPropertyChanged
            Me._Meter.MasterDevice.Enabled = True
        End If
        Me.BindPart(New DeviceUnderTest)
        Me.AssignDevice(Me.Meter?.MasterDevice)
        isr.Core.ApplianceBase.DoEvents()
    End Sub

    ''' <summary> Gets the is meter that owns this item. </summary>
    ''' <value> The is meter owner. </value>
    Private Property IsMeterOwner As Boolean

    ''' <summary> Assigns a Meter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Overloads Sub AssignMeter(ByVal value As Meter)
        Me.IsMeterOwner = False
        Me.AssignMeterThis(value)
    End Sub

    ''' <summary> Releases the Meter. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Sub ReleaseMeter()
        If Me.IsMeterOwner Then
            Me._Meter?.Dispose() : Me._Meter = Nothing
        Else
            Me._Meter = Nothing
        End If
    End Sub

    ''' <summary> Gets the is Meter assigned. </summary>
    ''' <value> The is Meter assigned. </value>
    Public ReadOnly Property IsMeterAssigned As Boolean
        Get
            Return Me.Meter IsNot Nothing AndAlso Not Me.Meter.IsDisposed
        End Get
    End Property

    ''' <summary> Raises the property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HanldePropertyChanged(ByVal sender As Meter, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.Meter.MeasurementCompleted)
                If sender.MeasurementCompleted Then
                    Me.PublishInfo("{0} measurement completed;. ", sender.ResourceName)
                End If
        End Select
    End Sub

    ''' <summary> Event handler. Called by _meter for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeterPropertyChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling meter {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MeterPropertyChanged), New Object() {sender, e})
            Else
                Me.HanldePropertyChanged(TryCast(sender, Meter), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE "

    ''' <summary> Gets the master device. </summary>
    ''' <value> The master device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property MasterDevice As K2600Device
        Get
            Return Me.Device
        End Get
    End Property

    ''' <summary> Gets or sets the device. </summary>
    ''' <value> The device. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public ReadOnly Property Device As K2600Device

    ''' <summary> Assigns the device and binds the relevant subsystem values. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub AssignDeviceThis(ByVal value As K2600Device)
        If Me._Device IsNot Nothing Then
            RemoveHandler Me.Device.Opening, AddressOf Me.DeviceOpening
            RemoveHandler Me.Device.Opened, AddressOf Me.DeviceOpened
            RemoveHandler Me.Device.Closing, AddressOf Me.DeviceClosing
            RemoveHandler Me.Device.Closed, AddressOf Me.DeviceClosed
            RemoveHandler Me.Device.Initialized, AddressOf Me.DeviceInitialized
            RemoveHandler Me.Device.Initializing, AddressOf Me.DeviceInitializing
            RemoveHandler Me.Device.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged

            Me._ResourceSelectorConnector.AssignSelectorViewModel(Nothing)
            Me._ResourceSelectorConnector.AssignOpenerViewModel(Nothing)
            Me._ResourceSelectorConnector.AssignTalker(Nothing)
            Me.AssignTalker(Nothing)
            Me._Device = Nothing
        End If
        Me._Device = value
        If value IsNot Nothing Then
            Me.AssignTalker(Me.Device.Talker)
            Me._ResourceSelectorConnector.AssignSelectorViewModel(Me.Device.SessionFactory)
            Me._ResourceSelectorConnector.AssignOpenerViewModel(value)
            Me._ResourceSelectorConnector.AssignTalker(Me.Device.Talker)
            AddHandler Me.Device.Opening, AddressOf Me.DeviceOpening
            AddHandler Me.Device.Opened, AddressOf Me.DeviceOpened
            AddHandler Me.Device.Closing, AddressOf Me.DeviceClosing
            AddHandler Me.Device.Closed, AddressOf Me.DeviceClosed
            AddHandler Me.Device.Initialized, AddressOf Me.DeviceInitialized
            AddHandler Me.Device.Initializing, AddressOf Me.DeviceInitializing
            AddHandler Me.Device.SessionFactory.PropertyChanged, AddressOf Me.SessionFactoryPropertyChanged
            Me.Device.SessionFactory.CandidateResourceName = isr.VI.Ttm.My.MySettings.Default.ResourceName
            If Me.Device.IsDeviceOpen Then
                Me.DeviceOpened(Me.Device, System.EventArgs.Empty)
            Else
                Me.DeviceClosed(Me.Device, System.EventArgs.Empty)
            End If
        End If
    End Sub

    ''' <summary> Assigns a device. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="value"> True to show or False to hide the control. </param>
    Public Sub AssignDevice(ByVal value As K2600Device)
        Me.AssignDeviceThis(value)
    End Sub

    ''' <summary> Reads the status register. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub ReadStatusRegister()
        Dim activity As String = $"{Me.Device.ResourceNameCaption} reading service request"
        Try
            Me.Device.Session.ReadStatusRegister()
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#Region " SESSION FACTORY "

    ''' <summary> Gets or sets the name of the resource. </summary>
    ''' <value> The name of the resource. </value>
    Public Property ResourceName As String
        Get
            Return Me.Device.SessionFactory.CandidateResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceName) Then
                Me.Device.SessionFactory.CandidateResourceName = value
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the Search Pattern of the resource. </summary>
    ''' <value> The Search Pattern of the resource. </value>
    Public Property ResourceFilter As String
        Get
            Return Me.Device.SessionFactory.ResourcesFilter
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            If Not String.Equals(value, Me.ResourceFilter) Then
                Me.Device.SessionFactory.ResourcesFilter = value
            End If
        End Set
    End Property

    ''' <summary> Executes the session factory property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       Specifies the object where the call originated. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(ByVal sender As SessionFactory, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(SessionFactory.ValidatedResourceName)
                If sender.IsOpen Then
                    Me._IdentityTextBox.Text = $"Resource {sender.ValidatedResourceName} connected"
                    Me.PublishInfo($"Resource connected;. {sender.ValidatedResourceName}")
                Else
                    Me._IdentityTextBox.Text = $"Resource {sender.ValidatedResourceName} located"
                    Me.PublishInfo($"Resource locate;. {sender.ValidatedResourceName}")
                End If
            Case NameOf(SessionFactory.CandidateResourceName)
                If Not sender.IsOpen Then
                    Me._IdentityTextBox.Text = $"Resource {sender.ValidatedResourceName}"
                    Me.PublishInfo($"Candidate resource;. {sender.ValidatedResourceName}")
                End If
        End Select
    End Sub

    ''' <summary> Session factory property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub SessionFactoryPropertyChanged(ByVal sender As Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs)
        Dim activity As String = String.Empty
        Try
            activity = $"handling session factory {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.SessionFactoryPropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, VI.SessionFactory), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

#Region " DEVICE EVENTS "

    ''' <summary>
    ''' Event handler. Called upon device opening so as to instantiated all subsystems.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                       <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Sub DeviceOpening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Updates the availability of the controls. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub OnMeasurementStatusChanged()

        Dim measurementSequenceState As MeasurementSequenceState = MeasurementSequenceState.Idle
        If Me.MeasureSequencer IsNot Nothing Then
            measurementSequenceState = Me.MeasureSequencer.MeasurementSequenceState
        End If

        Dim triggerSequenceState As TriggerSequenceState = TriggerSequenceState.Idle
        If Me.TriggerSequencer IsNot Nothing Then
            triggerSequenceState = Me.TriggerSequencer.TriggerSequenceState
        End If

        Dim enabled As Boolean = Me.Meter IsNot Nothing AndAlso Me.Meter.IsDeviceOpen AndAlso
                                 TriggerSequenceState.Idle = triggerSequenceState AndAlso
                                 MeasurementSequenceState.Idle = measurementSequenceState
        Me._ConnectGroupBox.Enabled = TriggerSequenceState.Idle = triggerSequenceState AndAlso
                                      MeasurementSequenceState.Idle = measurementSequenceState
        Me._TTMConfigurationPanel.Enabled = enabled

        Me._MeasureShuntResistanceButton.Enabled = enabled
        Me._ApplyShuntResistanceConfigurationButton.Enabled = enabled
        Me._ApplyNewShuntResistanceConfigurationButton.Enabled = enabled

    End Sub

    ''' <summary>
    ''' Event handler. Called after the device opened and all subsystems were defined.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                                             <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Sub DeviceOpened(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim outcome As TraceEventType = TraceEventType.Information
        If Me.Device.Session.Enabled And Not Me.Device.Session.IsSessionOpen Then outcome = TraceEventType.Warning
        Me.Publish(outcome, "{0} {1:enabled;enabled;disabled} and {2:open;open;closed}; session {3:open;open;closed};. ",
                   Me.Device.ResourceTitleCaption, Me.Device.Session.Enabled.GetHashCode,
                   Me.Device.Session.IsDeviceOpen.GetHashCode, Me.Device.Session.IsSessionOpen.GetHashCode)
        Dim activity As String = String.Empty
        Try
            activity = "applying device under test configuration"
            ' set reference tot he device under test.
            Me.DeviceUnderTest = Me.Meter.ConfigInfo
            If Me.SupportsParts Then Me._PartHeader.DeviceUnderTest = Me.Part
            Me._MeasurementsHeader.DeviceUnderTest = Me.Part
            Me._MeasurementsHeader.Clear()
            Me._ThermalTransientHeader.DeviceUnderTest = Me.Part
            Me._ThermalTransientHeader.Clear()
            Me._TTMConfigurationPanel.Meter = Me.Meter
            Me._MeasurementPanel.Meter = Me.Meter
            If Me.SupportsParts Then Me._PartsPanel.Part = Me.Part

            Me.Part.ClearPartInfo()
            Me.Part.ClearMeasurements()
            If Me.SupportsParts Then Me._PartsPanel.ClearParts()
            If Me.SupportsParts Then Me._PartsPanel.ApplySettings()

            Me.Meter.Part = Me.Part
            Me.MeasureSequencer = Me.Meter.MeasureSequencer
            Me.TriggerSequencer = Me.Meter.TriggerSequencer

            ' initialize the device system state.
            activity = "Clearing master device active state" : Me.PublishVerbose($"{activity};. ")
            Me.Meter.MasterDevice.ClearActiveState()
            isr.Core.ApplianceBase.DoEvents()

            activity = $"Reading identity from {Me.ResourceName}" : Me.PublishVerbose($"{activity};. ")
            Me._IdentityTextBox.Text = Me.Meter.MasterDevice.StatusSubsystem.QueryIdentity
            isr.Core.ApplianceBase.DoEvents()

            activity = $"Resetting and Clearing meter" : Me.PublishVerbose($"{activity};. ")
            Me.Meter.ResetClear()
            isr.Core.ApplianceBase.DoEvents()

            ' reset part to know state based on the current defaults
            Me.Part.ResetKnownState()
            isr.Core.ApplianceBase.DoEvents()

            Me._TTMConfigurationPanel.Part = Me.Part
            isr.Core.ApplianceBase.DoEvents()

            activity = $"binding controls" : Me.PublishVerbose($"{activity};. ")
            Me.BindShuntControls()

            activity = $"enables controls" : Me.PublishVerbose($"{activity};. ")
            Me.OnMeasurementStatusChanged()

            activity = $"Connected to {Me.ResourceName}" : Me.PublishVerbose($"{activity};. ")
            isr.Core.ApplianceBase.DoEvents()

        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub


#End Region

#Region " OPENING / OPEN "

    ''' <summary>
    ''' Attempts to open a session to the device using the specified resource name.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The title. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function TryOpenDeviceSession(ByVal resourceName As String, ByVal resourceTitle As String) As (Success As Boolean, Details As String)
        Return Me.Device.TryOpenSession(resourceName, resourceTitle)
    End Function

#End Region

#Region " INITALIZING / INITIALIZED  "

    ''' <summary> Device initializing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Cancel event information. </param>
    Protected Overridable Sub DeviceInitializing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    End Sub

    ''' <summary> Device initialized. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="System.Object"/> instance of this
    '''                                             <see cref="System.Windows.Forms.Control"/> </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub DeviceInitialized(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub

#End Region

#Region " CLOSING / CLOSED "

    ''' <summary> Event handler. Called when device is closing. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
        If Me.Device IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                activity = $"Disabling meter timer" : Me.PublishVerbose($"{activity};. ")
                Me._MeterTimer.Enabled = False
                activity = $"Disconnecting from {Me.ResourceName}" : Me.PublishInfo($"{activity};. ")
                If Me.Device.Session IsNot Nothing Then Me.Device.Session.DisableServiceRequestEventHandler()
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        End If
    End Sub

    ''' <summary> Event handler. Called when device is closed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> <see cref="Object"/> instance of this
    '''                       <see cref="Control"/> </param>
    ''' <param name="e">      Event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub DeviceClosed(ByVal sender As Object, ByVal e As System.EventArgs)
        If Me.Device IsNot Nothing Then
            Dim activity As String = String.Empty
            Try
                If Me.Device.Session.IsSessionOpen Then
                    Me.PublishWarning($"{Me.Device.Session.ResourceNameCaption} closed but session still open;. ")
                ElseIf Me.Device.Session.IsDeviceOpen Then
                    Me.PublishWarning($"{Me.Device.Session.ResourceNameCaption} closed but emulated session still open;. ")
                Else
                    Me.PublishVerbose("Disconnected; Device access closed.")
                End If
                activity = $"disables controls" : Me.PublishVerbose($"{activity};. ")
                Me.OnMeasurementStatusChanged()
            Catch ex As Exception
                Me.PublishException(activity, ex)
            End Try
        Else
            Me.PublishInfo("Disconnected; Device disposed.")
        End If
    End Sub

#End Region

#End Region

#Region " SEQUENCED MEASUREMENTS "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _MeasureSequencer As MeasureSequencer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the sequencer. </summary>
    ''' <value> The sequencer. </value>
    Private Property MeasureSequencer As MeasureSequencer
        Get
            Return Me._MeasureSequencer
        End Get
        Set(value As MeasureSequencer)
            Me._MeasureSequencer = value
        End Set
    End Property

    ''' <summary> Handles the measure sequencer property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As MeasureSequencer, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.MeasureSequencer.MeasurementSequenceState)
                Me.OnMeasurementSequenceStateChanged(sender.MeasurementSequenceState)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _MeasureSequencer for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub MeasureSequencer_PropertyChanged(ByVal sender As System.Object, ByVal e As PropertyChangedEventArgs) Handles _MeasureSequencer.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling measure sequencer {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.MeasureSequencer_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, MeasureSequencer), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> Ends a completed sequence. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub OnMeasurementSequenceCompleted()

        Me.PublishInfo("Measurement completed;. ")
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling measure completed event"
            ' add part if auto add is enabled.
            If Me._PartsPanel.AutoAddEnabled Then
                activity = $"{Me.Meter.ResourceName} adding part"
                Me._PartsPanel.AddPart()
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try

    End Sub

    ''' <summary> State of the last measurement sequence. </summary>
    Private _LastMeasurementSequenceState As MeasurementSequenceState

    ''' <summary> Handles the change in measurement state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="state"> The state. </param>
    Private Sub OnMeasurementSequenceStateChanged(ByVal state As MeasurementSequenceState)
        If Me._LastMeasurementSequenceState <> state Then
            Me.PublishInfo("Processing the {0} state;. ", state.Description)
            Me._LastMeasurementSequenceState = state
        End If
        Select Case state
            Case MeasurementSequenceState.Aborted
            Case MeasurementSequenceState.Completed
            Case MeasurementSequenceState.Failed
            Case MeasurementSequenceState.MeasureInitialResistance
            Case MeasurementSequenceState.MeasureThermalTransient
            Case MeasurementSequenceState.Idle
                Me.OnMeasurementStatusChanged()
            Case MeasurementSequenceState.None
            Case MeasurementSequenceState.PostTransientPause
            Case MeasurementSequenceState.MeasureFinalResistance
            Case MeasurementSequenceState.Starting
                Me.OnMeasurementStatusChanged()
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & state.ToString)
        End Select
    End Sub

#End Region

#Region " TRIGGERED MEASUREMENTS "

#Disable Warning IDE1006 ' Naming Styles
    Private WithEvents _TriggerSequencer As TriggerSequencer
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> Gets or sets the trigger sequencer. </summary>
    ''' <value> The sequencer. </value>
    Private Property TriggerSequencer As TriggerSequencer
        Get
            Return Me._TriggerSequencer
        End Get
        Set(value As TriggerSequencer)
            Me._TriggerSequencer = value
        End Set
    End Property

    ''' <summary> Handles the trigger sequencer property changed event. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The source of the event. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub OnPropertyChanged(ByVal sender As TriggerSequencer, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        Select Case propertyName
            Case NameOf(Ttm.TriggerSequencer.TriggerSequenceState)
                Me.OnTriggerSequenceStateChanged(sender.TriggerSequenceState)
        End Select
    End Sub

    ''' <summary> Event handler. Called by _TriggerSequencer for property changed events. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TriggerSequencer_PropertyChanged(ByVal sender As System.Object, ByVal e As System.ComponentModel.PropertyChangedEventArgs) Handles _TriggerSequencer.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling trigger sequencer {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TriggerSequencer_PropertyChanged), New Object() {sender, e})
            Else
                Me.OnPropertyChanged(TryCast(sender, TriggerSequencer), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

    ''' <summary> State of the last trigger sequence. </summary>
    Private _LastTriggerSequenceState As TriggerSequenceState

    ''' <summary> Handles the change in measurement state. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="state"> The state. </param>
    Private Sub OnTriggerSequenceStateChanged(ByVal state As TriggerSequenceState)
        If Me._LastTriggerSequenceState <> state Then
            Me.PublishInfo("Processing the {0} state;. ", state.Description)
            Me._LastTriggerSequenceState = state
        End If
        Select Case state
            Case TriggerSequenceState.Aborted
                Me.OnMeasurementStatusChanged()
            Case TriggerSequenceState.Stopped
            Case TriggerSequenceState.Failed
            Case TriggerSequenceState.WaitingForTrigger
            Case TriggerSequenceState.MeasurementCompleted
            Case TriggerSequenceState.ReadingValues
            Case TriggerSequenceState.Idle
                Me.OnMeasurementStatusChanged()
            Case TriggerSequenceState.None
            Case TriggerSequenceState.Starting
                Me.OnMeasurementStatusChanged()
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "Unhandled state: " & state.ToString)
        End Select
    End Sub

#End Region

#Region " TAB CONTROL EVENTS "

    ''' <summary> Tabs draw item. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Draw item event information. </param>
    Private Sub Tabs_DrawItem(sender As Object, e As DrawItemEventArgs) Handles _Tabs.DrawItem
        Dim page As TabPage = Me._Tabs.TabPages(e.Index)
        Dim paddedBounds As Drawing.Rectangle = e.Bounds
        Dim backClr As Drawing.Color = If(e.State = DrawItemState.Selected, Drawing.SystemColors.ControlDark, page.BackColor)
        Using brush As Drawing.Brush = New Drawing.SolidBrush(backClr)
            e.Graphics.FillRectangle(brush, paddedBounds)
        End Using
        Dim yOffset As Integer = If(e.State = DrawItemState.Selected, -2, 1)
        paddedBounds = e.Bounds
        paddedBounds.Offset(1, yOffset)
        TextRenderer.DrawText(e.Graphics, page.Text, page.Font, paddedBounds, page.ForeColor)
    End Sub

#End Region

#Region " NAVIGATION "

    ''' <summary> Gets the support parts. </summary>
    ''' <value> The support parts. </value>
    Public Property SupportsParts As Boolean

    ''' <summary> Gets the supports shunt. </summary>
    ''' <value> The supports shunt. </value>
    Public Property SupportsShunt As Boolean

    ''' <summary> Enumerates the nodes. Each item is the same as the node name. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Enum TreeViewNode

        ''' <summary> An enum constant representing the connect node option. </summary>
        <Description("CONNECT")>
        ConnectNode

        ''' <summary> An enum constant representing the configure node option. </summary>
        <Description("CONFIGURE")>
        ConfigureNode

        ''' <summary> An enum constant representing the measure node option. </summary>
        <Description("MEASURE")>
        MeasureNode

        ''' <summary> An enum constant representing the shunt node option. </summary>
        <Description("SHUNT")>
        ShuntNode

        ''' <summary> An enum constant representing the parts node option. </summary>
        <Description("PARTS")>
        PartsNode

        ''' <summary> An enum constant representing the messages node option. </summary>
        <Description("MESSAGES")>
        MessagesNode
    End Enum

    ''' <summary> Builds the navigator tree view. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Private Sub BuildNavigatorTreeView()

        Me._PartHeader.Visible = Me.SupportsParts

        Me._SplitContainer.Dock = DockStyle.Fill
        Me._SplitContainer.SplitterDistance = 120
        Me._NavigatorTreeView.Enabled = False
        Me._NavigatorTreeView.Nodes.Clear()

        Dim nodes As New List(Of TreeNode)

        For Each node As TreeViewNode In [Enum].GetValues(GetType(TreeViewNode))
            If node = TreeViewNode.PartsNode AndAlso Not Me.SupportsParts Then Continue For
            If node = TreeViewNode.ShuntNode AndAlso Not Me.SupportsShunt Then Continue For
            nodes.Add(New System.Windows.Forms.TreeNode(node.Description))
            nodes(nodes.Count - 1).Name = node.ToString
            nodes(nodes.Count - 1).Text = node.Description
            If node = TreeViewNode.MessagesNode Then
                Me._TraceMessagesBox.ContainerTreeNode = nodes(nodes.Count - 1)
                Me._TraceMessagesBox.TabCaption = "MESSAGES"
            End If
        Next

        Me._NavigatorTreeView.Nodes.AddRange(nodes.ToArray)
        Me._NavigatorTreeView.Enabled = True

    End Sub

    ''' <summary> The last node selected. </summary>
    Private _LastNodeSelected As Windows.Forms.TreeNode

    ''' <summary> Gets the last tree view node selected. </summary>
    ''' <value> The last tree view node selected. </value>
    Private ReadOnly Property LastTreeViewNodeSelected As TreeViewNode
        Get
            Return If(Me._LastNodeSelected Is Nothing, DirectCast(0, TreeViewNode), CType([Enum].Parse(GetType(TreeViewNode), Me._LastNodeSelected.Name), TreeViewNode))
        End Get
    End Property

    ''' <summary> The nodes visited. </summary>
    Private _NodesVisited As List(Of TreeViewNode)

    ''' <summary> Called after a node is selected. Displays to relevant screen. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="node"> The node. </param>
    Private Sub OnNodeSelected(ByVal node As TreeViewNode)

        If Me._NodesVisited Is Nothing Then
            Me._NodesVisited = New List(Of TreeViewNode)
        End If
        Dim activeControl As Control = Nothing
        Dim focusControl As Control = Nothing
        Dim activeDisplay As DataGridView = Nothing

        Select Case node

            Case TreeViewNode.ConnectNode

                activeControl = Me._ConnectTabLayout
                focusControl = Nothing
                activeDisplay = Nothing

            Case TreeViewNode.ConfigureNode

                activeControl = Me._MainLayout
                focusControl = Nothing
                activeDisplay = Nothing

            Case TreeViewNode.MeasureNode

                activeControl = Me._TtmLayout
                focusControl = Nothing
                activeDisplay = Nothing

            Case TreeViewNode.ShuntNode

                activeControl = Me._ShuntLayout
                focusControl = Nothing
                activeDisplay = Nothing

            Case TreeViewNode.PartsNode

                activeControl = Me._PartsLayout
                focusControl = Nothing
                activeDisplay = Nothing

            Case TreeViewNode.MessagesNode

                activeControl = Me._TraceMessagesBox
                focusControl = Nothing
                activeDisplay = Nothing

        End Select

        ' turn off the visibility of the current panel, this turns off the visibility of the
        ' contained controls, which will now be removed from the panel.
        Me._SplitContainer.Panel2.Hide()
        Me._SplitContainer.Panel2.Controls.Clear()
        ' If Me._SplitContainer.Panel2.HasChildren Then
        '     For Each Control As Control In .Controls
        '         Me._SplitContainer.Panel2.Controls.Remove(Control)
        '     Next
        ' End If

        If activeControl IsNot Nothing Then
            activeControl.Dock = DockStyle.None
            Me._SplitContainer.Panel2.Controls.Add(activeControl)
            activeControl.Dock = DockStyle.Fill
        End If

        ' turn on visibility on the panel -- this toggles the visibility of the contained controls,
        ' which is required for the messages boxes.
        Me._SplitContainer.Panel2.Show()

        If Not Me._NodesVisited.Contains(node) Then
            If focusControl IsNot Nothing Then
                focusControl.Focus()
            End If
            Me._NodesVisited.Add(node)
        End If

        If activeDisplay IsNot Nothing Then
            '  DataDirector.UpdateColumnDisplayOrder(activeDisplay, columnOrder)
        End If

    End Sub

    ''' <summary>
    ''' Gets or sets a value indicating whether this <see cref="Console"/> is navigating.
    ''' </summary>
    ''' <remarks>
    ''' Used to ignore changes in grids during the navigation. The grids go through selecting their
    ''' rows when navigating.
    ''' </remarks>
    ''' <value> <c>True</c> if navigating; otherwise, <c>False</c>. </value>
    Private Property Navigating As Boolean

    ''' <summary> Handles the BeforeSelect event of the _NavigatorTreeView control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.TreeViewCancelEventArgs"/> instance
    '''                       containing the event data. </param>
    Private Sub NavigatorTreeView_BeforeSelect(sender As Object, e As System.Windows.Forms.TreeViewCancelEventArgs) Handles _NavigatorTreeView.BeforeSelect
        Me.Navigating = True
    End Sub

    ''' <summary> Handles the AfterSelect event of the Me._NavigatorTreeView control. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The source of the event. </param>
    ''' <param name="e">      The <see cref="System.Windows.Forms.TreeViewEventArgs" /> instance
    '''                       containing the event data. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub NavigatorTreeView_AfterSelect(sender As System.Object, e As System.Windows.Forms.TreeViewEventArgs) Handles _NavigatorTreeView.AfterSelect
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter.ResourceName} handling navigator after select event"
            If Me._LastNodeSelected IsNot Nothing Then
                Me._LastNodeSelected.BackColor = Me._NavigatorTreeView.BackColor
            End If
            If sender IsNot Nothing AndAlso TryCast(sender, Control).Enabled AndAlso
                e IsNot Nothing AndAlso e.Node IsNot Nothing AndAlso e.Node.IsSelected Then
                Me._LastNodeSelected = e.Node
                e.Node.BackColor = System.Drawing.SystemColors.Highlight
                Me.OnNodeSelected(Me.LastTreeViewNodeSelected)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        Finally
            Me.Navigating = False
        End Try
    End Sub

    ''' <summary> Selects the navigator tree view node. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="node"> The node. </param>
    Private Sub SelectNavigatorTreeViewNode(ByVal node As TreeViewNode)
        Me._NavigatorTreeView.SelectedNode = Me._NavigatorTreeView.Nodes(node.ToString)
    End Sub

#End Region

#Region " TALKER "

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Public Overrides Sub IdentifyTalkers()
        MyBase.IdentifyTalkers()
        My.MyLibrary.Appliance.Identify(Me.Talker)
    End Sub

    ''' <summary> Adds the listeners such as the current trace messages box. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overloads Sub AddPrivateListeners()
        Me.AddPrivateListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Assign talker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="talker"> The talker. </param>
    Private Sub AssignTalkerThis(ByVal talker As ITraceMessageTalker)
        Me._PartsPanel.AssignTalker(talker)
        Me._TTMConfigurationPanel.AssignTalker(talker)
    End Sub

    ''' <summary> Assign talker. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="talker"> The talker. </param>
    Public Overrides Sub AssignTalker(ByVal talker As ITraceMessageTalker)
        Me.AssignTalkerThis(talker)
        MyBase.AssignTalker(talker)
        Me.AddListener(Me._TraceMessagesBox)
    End Sub

    ''' <summary> Applies the trace level to all listeners to the specified type. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    Public Overrides Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType)
        If listenerType = Me._TraceMessagesBox.ListenerType Then Me._TraceMessagesBox.ApplyTraceLevel(value)
        Me._PartsPanel.ApplyListenerTraceLevel(listenerType, value)
        Me._TTMConfigurationPanel.ApplyListenerTraceLevel(listenerType, value)
        ' this should apply only to the listeners associated with this form
        ' MyBase.ApplyListenerTraceLevel(listenerType, value)
    End Sub

    ''' <summary>
    ''' Uses the <see cref="isr.Core.ITalker.Talker"/> to publish or the default logger to log the message.
    ''' </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="activity">  The activity. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function Publish(ByVal eventType As TraceEventType, ByVal activity As String) As String
        Return Me.Publish(New Core.TraceMessage(eventType, My.MyLibrary.TraceEventId, activity))
    End Function

    ''' <summary> Publish exception. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="activity"> The activity. </param>
    ''' <param name="ex">       The ex. </param>
    ''' <returns> A String. </returns>
    Protected Overrides Function PublishException(ByVal activity As String, ByVal ex As Exception) As String
        Return Me.Publish(TraceEventType.Error, $"Exception {activity};. {ex.ToFullBlownString}")
    End Function

    ''' <summary> Executes the trace messages box property changed action. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender">       The sender. </param>
    ''' <param name="propertyName"> Name of the property. </param>
    Private Sub HandlePropertyChanged(sender As TraceMessagesBox, ByVal propertyName As String)
        If sender Is Nothing OrElse String.IsNullOrWhiteSpace(propertyName) Then Return
        If String.Equals(propertyName, NameOf(isr.Core.Forma.TraceMessagesBox.StatusPrompt)) Then
            Me._StatusLabel.Text = isr.Core.WinForms.CompactExtensions.CompactExtensionMethods.Compact(sender.StatusPrompt, Me._StatusLabel)
            Me._StatusLabel.ToolTipText = sender.StatusPrompt
        End If
    End Sub

    ''' <summary> Trace messages box property changed. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="sender"> The sender. </param>
    ''' <param name="e">      Property Changed event information. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Private Sub TraceMessagesBox_PropertyChanged(sender As Object, e As PropertyChangedEventArgs) Handles _TraceMessagesBox.PropertyChanged
        Dim activity As String = String.Empty
        Try
            activity = $"{Me.Meter?.ResourceName} handling trace message box {e?.PropertyName} property changed event"
            If Me.InvokeRequired Then
                Me.Invoke(New Action(Of Object, PropertyChangedEventArgs)(AddressOf Me.TraceMessagesBox_PropertyChanged), New Object() {sender, e})
            Else
                Me.HandlePropertyChanged(TryCast(sender, TraceMessagesBox), e?.PropertyName)
            End If
        Catch ex As Exception
            Me.PublishException(activity, ex)
        End Try
    End Sub

#End Region

End Class
