Imports System.Windows.Forms
Imports System.ComponentModel
Imports System.Windows.Forms.DataVisualization.Charting

''' <summary> Panel for editing the measurement. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2014-03-17 </para>
''' </remarks>
Public Class MeasurementView
    Inherits MeasurementViewBase

    ''' <summary> Releases the resources. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ReleaseResources()
        Me.Meter = Nothing
        MyBase.ReleaseResources()
    End Sub

    ''' <summary> Gets the is device open. </summary>
    ''' <value> The is device open. </value>
    <DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden), Browsable(False)>
    Public Overrides ReadOnly Property IsDeviceOpen As Boolean
        Get
            Return If(Me.Meter Is Nothing, False, Me.Meter.IsDeviceOpen)
        End Get
    End Property

    ''' <summary> The meter. </summary>
    Private _Meter As Meter

    ''' <summary> Gets or sets the meter. </summary>
    ''' <value> The meter. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property Meter As Meter
        Get
            Return Me._Meter
        End Get
        Set(value As Meter)
            Me._Meter = value
            If value IsNot Nothing Then
                Me.MasterDevice = Me.Meter.MasterDevice
                Me.TriggerSequencer = value.TriggerSequencer
                Me.MeasureSequencer = value.MeasureSequencer
                MyBase.OnStateChanged()
            End If
        End Set
    End Property

    ''' <summary> Abort Trigger Sequence if started. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub AbortTriggerSequenceIf()
        Me.PublishVerbose("Aborting measurements;. ")
        Me.Meter.AbortTriggerSequenceIf()
    End Sub

    ''' <summary> Clears the part measurements. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    Protected Overrides Sub ClearPartMeasurements()
        Me.PublishVerbose("Clearing part measurements;. ")
        Me.Meter.Part.ClearMeasurements()
    End Sub

    ''' <summary> Measure initial resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The measurement <see cref="MeasurementOutcomes">outcome</see>. </returns>
    Protected Overrides Function MeasureInitialResistance() As MeasurementOutcomes
        Me.Meter.MeasureInitialResistance(Me.Meter.Part.InitialResistance)
        Return Me.Meter.Part.Outcome
    End Function

    ''' <summary> Measure thermal transient. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The measurement <see cref="MeasurementOutcomes">outcome</see>. </returns>
    Protected Overrides Function MeasureThermalTransient() As MeasurementOutcomes
        Me.Meter.MeasureThermalTransient(Me.Meter.Part.ThermalTransient)
        Return Me.Meter.Part.Outcome
    End Function

    ''' <summary> Measure final resistance. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <returns> The measurement <see cref="MeasurementOutcomes">outcome</see>. </returns>
    Protected Overrides Function MeasureFinalResistance() As MeasurementOutcomes
        Me.Meter.MeasureFinalResistance(Me.Meter.Part.FinalResistance)
        Return Me.Meter.Part.Outcome
    End Function

    ''' <summary> Displays a thermal transient trace described by chart. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The chart. </param>
    ''' <returns> A list of trace values. </returns>
    Protected Overrides Function DisplayThermalTransientTrace(chart As System.Windows.Forms.DataVisualization.Charting.Chart) As IList(Of System.Windows.Point)
        Me.ChartThermalTransientTrace(chart)
        Return Me.Meter.ThermalTransient.LastTimeSeries
    End Function

    ''' <summary> Models the thermal transient trace. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The chart. </param>
    Protected Overrides Sub ModelThermalTransientTrace(chart As System.Windows.Forms.DataVisualization.Charting.Chart)
        Me.Meter.ThermalTransient.ModelTransientResponse(Me.Meter.Part.ThermalTransient)
        Me.DisplayModel(chart)
    End Sub

#Region " CHARTING "

    ''' <summary> Displays a thermal transient trace. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The <see cref="DataVisualization.Charting.Chart">Chart</see>. </param>
    Public Sub ChartThermalTransientTrace(ByVal chart As DataVisualization.Charting.Chart)

        If Me.Meter.ThermalTransient.NewTraceReadyToRead Then
            Me.PublishVerbose("Reading trace;. ")
            Dim st As Diagnostics.Stopwatch = Diagnostics.Stopwatch.StartNew
            Me.Meter.ThermalTransient.ReadThermalTransientTrace()
            Dim t As TimeSpan = st.Elapsed
            Me.PublishVerbose("Read trace in {0} ms;. ", t.TotalMilliseconds)
        Else
            Me.PublishVerbose("Displaying thermal transient trace;. ")
        End If
        MeasurementView.ConfigureTraceChart(chart)
        MeasurementView.DisplayTrace(chart, Me.Meter.ThermalTransient.LastTimeSeries)
    End Sub

    ''' <summary> Displays a trace described by timeSeries. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="chart"> The <see cref="DataVisualization.Charting.Chart">Chart</see>. </param>
    Public Sub DisplayModel(ByVal chart As DataVisualization.Charting.Chart)
        If chart Is Nothing Then Throw New ArgumentNullException(NameOf(chart))
        If chart.Series.Count = 1 Then
            chart.Series.Add("Model")
            chart.Series(1).ChartType = DataVisualization.Charting.SeriesChartType.Line
        End If

        Dim s As Series = chart.Series(1)
        s.Points.SuspendUpdates()
        s.Points.Clear()
        s.Color = Drawing.Color.Chocolate
        For i As Integer = 0 To Me.Meter.ThermalTransient.Model.FunctionValues.Count - 1
            Dim x As Double = Me.Meter.ThermalTransient.LastTimeSeries(i).X
            Dim y As Double = Me.Meter.ThermalTransient.Model.FunctionValues(i)
            If x > 0 AndAlso y > 0 Then
                s.Points.AddXY(x, y)
            End If
        Next
        s.Points.ResumeUpdates()
        s.Points.Invalidate()
    End Sub

    ''' <summary> Configure trace chart. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The <see cref="DataVisualization.Charting.Chart">Chart</see>. </param>
    Private Shared Sub ConfigureTraceChart(ByVal chart As DataVisualization.Charting.Chart)
        If chart.ChartAreas.Count = 0 Then
            Dim chartArea As New DataVisualization.Charting.ChartArea("Area")
            chart.ChartAreas.Add(chartArea)
            chartArea.AxisY.Title = "Voltage, mV"
            chartArea.AxisY.Minimum = 0
            chartArea.AxisY.TitleFont = New Drawing.Font(chart.Parent.Font, Drawing.FontStyle.Bold)
            chartArea.AxisX.Title = "Time, ms"
            chartArea.AxisX.Minimum = 0
            chartArea.AxisX.TitleFont = New Drawing.Font(chart.Parent.Font, Drawing.FontStyle.Bold)
            chartArea.AxisX.IsLabelAutoFit = True
            chartArea.AxisX.LabelStyle.Format = "#.0"
            chartArea.AxisX.LabelStyle.Enabled = True
            chartArea.AxisX.LabelStyle.IsEndLabelVisible = True

            chart.Location = New System.Drawing.Point(82, 16)
            chart.Series.Add(New System.Windows.Forms.DataVisualization.Charting.Series("Trace"))
            Dim s As Series = chart.Series("Trace")
            s.SmartLabelStyle.Enabled = True
            s.ChartArea = "Area"
            ' s.Legend = "Legend"
            s.ChartType = DataVisualization.Charting.SeriesChartType.Line
            s.XAxisType = DataVisualization.Charting.AxisType.Primary
            s.YAxisType = DataVisualization.Charting.AxisType.Primary
            s.XValueType = DataVisualization.Charting.ChartValueType.Double
            s.YValueType = DataVisualization.Charting.ChartValueType.Double

            chart.Text = "Thermal Transient Trace"
            chart.Titles.Add("Thermal Transient Trace").Font = New Drawing.Font(chart.Parent.Font.FontFamily, 11, Drawing.FontStyle.Bold)

        End If
    End Sub

    ''' <summary> Gets a simulate time series. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="asymptote">    The asymptote. </param>
    ''' <param name="timeConstant"> The time constant. </param>
    ''' <returns> null if it fails, else a list of. </returns>
    Private Shared Function Simulate(ByVal asymptote As Double, ByVal timeConstant As Double) As IList(Of System.Windows.Point)
        Dim l As New List(Of System.Windows.Point)
        Dim vc As Double = 0
        Dim deltaT As Double = 0.1
        For i As Integer = 0 To 99
            Dim deltaV As Double = deltaT * (asymptote - vc) / timeConstant
            l.Add(New System.Windows.Point(CSng(deltaT * (i + 1)), CSng(vc)))
            vc += deltaV
        Next
        Return l
    End Function

    ''' <summary> Displays a trace described by timeSeries. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart">      The <see cref="DataVisualization.Charting.Chart">Chart</see>. </param>
    ''' <param name="timeSeries"> The time series in millivolts and milliseconds. </param>
    Private Shared Sub DisplayTrace(ByVal chart As DataVisualization.Charting.Chart, ByVal timeSeries As IList(Of System.Windows.Point))
        Dim s As Series = chart.Series(0)
        s.Points.SuspendUpdates()
        s.Points.Clear()
        s.Points.AddXY(0, 0)
        Dim deltaT As Double = 0
        If timeSeries.Count > 1 Then
            deltaT = timeSeries.Item(1).X - timeSeries.Item(0).X
        End If
        For Each v As System.Windows.Point In timeSeries
            s.Points.AddXY(v.X, v.Y)
        Next
        ' Add one more point
        s.Points.AddXY(timeSeries.Item(timeSeries.Count - 1).X + deltaT, 2 * timeSeries(timeSeries.Count - 1).Y - timeSeries(timeSeries.Count - 2).Y)
        s.Points.ResumeUpdates()
        s.Points.Invalidate()
    End Sub

    ''' <summary> Displays a thermal transient trace. </summary>
    ''' <remarks> David, 2020-10-12. </remarks>
    ''' <param name="chart"> The <see cref="DataVisualization.Charting.Chart">Chart</see>. </param>
    Public Shared Sub EmulateThermalTransientTrace(ByVal chart As DataVisualization.Charting.Chart)
        MeasurementView.ConfigureTraceChart(chart)
        MeasurementView.DisplayTrace(chart, Simulate(100, 5))
    End Sub

#End Region

End Class
